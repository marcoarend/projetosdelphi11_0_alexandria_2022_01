object FmVSGerArtDdImp1: TFmVSGerArtDdImp1
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-242 :: Gera'#231#227'o de Artigos por Dia'
  ClientHeight = 375
  ClientWidth = 759
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 759
    Height = 201
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 759
      Height = 201
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 4
        Top = 4
        Width = 745
        Height = 73
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox13: TGroupBox
          Left = 0
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora compra:'
          Enabled = False
          TabOrder = 0
          object TPCompraIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPCompraIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPCompraIniRedefInPlace
          end
          object CkCompraIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
            OnClick = CkCompraIniClick
          end
          object CkCompraFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
            OnClick = CkCompraFimClick
          end
          object TPCompraFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPCompraFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPCompraFimRedefInPlace
          end
        end
        object GroupBox1: TGroupBox
          Left = 248
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / sa'#237'da:'
          Enabled = False
          TabOrder = 1
          object TPViagemIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPViagemIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPViagemIniRedefInPlace
          end
          object CkViagemIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
            OnClick = CkViagemIniClick
          end
          object CkViagemFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
            OnClick = CkViagemFimClick
          end
          object TPViagemFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPViagemFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPViagemFimRedefInPlace
          end
        end
        object GroupBox2: TGroupBox
          Left = 496
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora chegada:'
          TabOrder = 2
          object TPEntradaIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPEntradaIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPEntradaIniRedefInPlace
          end
          object CkEntradaIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CkEntradaIniClick
          end
          object CkEntradaFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            Checked = True
            State = cbChecked
            TabOrder = 2
            OnClick = CkEntradaFimClick
          end
          object TPEntradaFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPEntradaFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPEntradaFimRedefInPlace
          end
        end
      end
      object CkTemIMEIMrt: TCheckBox
        Left = 8
        Top = 80
        Width = 149
        Height = 17
        Caption = 'Incluir arquivo morto.'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CkTemIMEIMrtClick
      end
      object GroupBox3: TGroupBox
        Left = 220
        Top = 100
        Width = 225
        Height = 73
        Caption = ' Impress'#227'o: '
        TabOrder = 2
        object RGFonte: TRadioGroup
          Left = 8
          Top = 20
          Width = 209
          Height = 41
          Caption = ' Fonte: '
          Columns = 5
          ItemIndex = 1
          Items.Strings = (
            '6'
            '7'
            '8'
            '9'
            '10')
          TabOrder = 0
        end
      end
      object RGPesquisa: TRadioGroup
        Left = 8
        Top = 100
        Width = 209
        Height = 73
        Caption = ' Pesquisa: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Rendimento'
          'Produ'#231#227'o')
        TabOrder = 3
        OnClick = RGPesquisaClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 711
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 663
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 249
    Width = 759
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 755
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 755
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 305
    Width = 759
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 613
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 611
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 20
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BitBtn1: TBitBtn
        Left = 308
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Jul/22'
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BitBtn3: TBitBtn
        Left = 500
        Top = 12
        Width = 75
        Height = 25
        Caption = 'frx 2'
        TabOrder = 2
        OnClick = BitBtn3Click
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65523
  end
  object QrPend: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPendAfterScroll
    SQL.Strings = (
      'SELECT  '
      '(vmi.Pecas - vmi.SdoVrtPeca) / vmi.Pecas * 100 PercPc, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,   '
      ' '
      'vmi.Pecas, vmi.SdoVrtPeca, vmi.*   '
      'FROM vsmovits vmi  '
      'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo  '
      ' '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vmi.MovimID=1  ')
    Left = 152
    object QrPendPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrPendSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrPendCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPendControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPendMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrPendMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrPendMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrPendEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPendTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrPendCliVenda: TIntegerField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrPendMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrPendLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrPendLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrPendDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrPendPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPendGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPendPecas_1: TFloatField
      FieldName = 'Pecas_1'
      Required = True
    end
    object QrPendPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrPendAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrPendAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrPendValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrPendSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrPendSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrPendSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrPendSdoVrtPeca_1: TFloatField
      FieldName = 'SdoVrtPeca_1'
      Required = True
    end
    object QrPendSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrPendObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrPendFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrPendMisturou: TSmallintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrPendCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      Required = True
    end
    object QrPendCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
    end
    object QrPendLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPendDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPendDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPendUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPendUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPendAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPendAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPendSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrPendSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrPendSerieFch: TIntegerField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrPendFornecMO: TIntegerField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrPendValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrPendDstMovID: TIntegerField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrPendDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrPendDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrPendDstGGX: TIntegerField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrPendQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      Required = True
    end
    object QrPendQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      Required = True
    end
    object QrPendQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      Required = True
    end
    object QrPendQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      Required = True
    end
    object QrPendQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      Required = True
    end
    object QrPendQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      Required = True
    end
    object QrPendQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      Required = True
    end
    object QrPendQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      Required = True
    end
    object QrPendAptoUso: TSmallintField
      FieldName = 'AptoUso'
      Required = True
    end
    object QrPendNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrPendMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrPendTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
      Required = True
    end
    object QrPendZerado: TSmallintField
      FieldName = 'Zerado'
      Required = True
    end
    object QrPendEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
      Required = True
    end
    object QrPendLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
      Required = True
    end
    object QrPendCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      Required = True
    end
    object QrPendNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
      Required = True
    end
    object QrPendFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
      Required = True
    end
    object QrPendFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
      Required = True
    end
    object QrPendPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrPendPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrPendPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrPendGSPInnNiv2: TIntegerField
      FieldName = 'GSPInnNiv2'
      Required = True
    end
    object QrPendGSPArtNiv2: TIntegerField
      FieldName = 'GSPArtNiv2'
      Required = True
    end
    object QrPendReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrPendStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrPendItemNFe: TIntegerField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrPendVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
      Required = True
    end
    object QrPendVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrPendClientMO: TIntegerField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrPendCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrPendKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
      Required = True
    end
    object QrPendNFeSer: TSmallintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrPendNFeNum: TIntegerField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrPendVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrPendGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrPendJmpMovID: TIntegerField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrPendJmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrPendJmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrPendJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrPendRmsMovID: TIntegerField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrPendRmsNivel1: TIntegerField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrPendRmsNivel2: TIntegerField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrPendRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrPendGSPSrcMovID: TIntegerField
      FieldName = 'GSPSrcMovID'
      Required = True
    end
    object QrPendGSPSrcNiv2: TIntegerField
      FieldName = 'GSPSrcNiv2'
      Required = True
    end
    object QrPendGSPJmpMovID: TIntegerField
      FieldName = 'GSPJmpMovID'
      Required = True
    end
    object QrPendGSPJmpNiv2: TIntegerField
      FieldName = 'GSPJmpNiv2'
      Required = True
    end
    object QrPendDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrPendMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
      Required = True
    end
    object QrPendIxxMovIX: TSmallintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrPendIxxFolha: TIntegerField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrPendIxxLinha: TIntegerField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrPendAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPendAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPendIuvpei: TIntegerField
      FieldName = 'Iuvpei'
      Required = True
    end
    object QrPendCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrPendCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      Required = True
    end
    object QrPendCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      Required = True
    end
    object QrPendCustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
      Required = True
    end
    object QrPendCustoComiss: TFloatField
      FieldName = 'CustoComiss'
      Required = True
    end
    object QrPendPerceComiss: TFloatField
      FieldName = 'PerceComiss'
      Required = True
    end
    object QrPendCusKgComiss: TFloatField
      FieldName = 'CusKgComiss'
      Required = True
    end
    object QrPendCredValrImposto: TFloatField
      FieldName = 'CredValrImposto'
      Required = True
    end
    object QrPendCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
      Required = True
    end
    object QrPendRpICMS: TFloatField
      FieldName = 'RpICMS'
      Required = True
    end
    object QrPendRpICMSST: TFloatField
      FieldName = 'RpICMSST'
      Required = True
    end
    object QrPendRpPIS: TFloatField
      FieldName = 'RpPIS'
      Required = True
    end
    object QrPendRpCOFINS: TFloatField
      FieldName = 'RpCOFINS'
      Required = True
    end
    object QrPendRvICMS: TFloatField
      FieldName = 'RvICMS'
      Required = True
    end
    object QrPendRvICMSST: TFloatField
      FieldName = 'RvICMSST'
      Required = True
    end
    object QrPendRvPIS: TFloatField
      FieldName = 'RvPIS'
      Required = True
    end
    object QrPendRvCOFINS: TFloatField
      FieldName = 'RvCOFINS'
      Required = True
    end
    object QrPendRpIPI: TFloatField
      FieldName = 'RpIPI'
      Required = True
    end
    object QrPendRvIPI: TFloatField
      FieldName = 'RvIPI'
      Required = True
    end
    object QrPendGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPendNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPendPercPc: TFloatField
      FieldName = 'PercPc'
    end
  end
  object DsPend: TDataSource
    DataSet = QrPend
    Left = 152
    Top = 48
  end
  object frxWET_CURTI_241_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_241_01GetValue
    Left = 152
    Top = 96
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPend
        DataSetName = 'frxDsPend'
      end
      item
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'In Natura Pendente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo compra:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoCompra]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 60.472480000000000000
          Width = 298.582821180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Marca]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 120.944960000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#231' entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 120.944960000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo sa'#237'da:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoViagem]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo entrada:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 60.472480000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoEntrada]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 120.944960000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#231' saldo')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 120.944960000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'a')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPend
        DataSetName = 'frxDsPend'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPend."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Marca'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPend."Marca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 7.559060000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo gerado')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 26.456710000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 26.456710000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 26.456710000000000000
          Width = 124.724387480000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'PercPc'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."PercPc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 26.456710000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% P'#231)
          ParentFont = False
          WordWrap = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
        RowCount = 0
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Marca'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."Marca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'AreaM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."AreaM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'MediaM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."MediaM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'kgM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."kgM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 124.724387480000000000
          Height = 18.897650000000000000
          DataField = 'Observ'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."Observ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          DataField = 'PercPc'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PercPc"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 24.566929133858270000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#231' saldo')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Sdo P'#231)
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 24.566929133858270000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 24.566929133858270000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PecasP]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KgP]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SdoPcP]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PercPcP]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PecasI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_AreaM2I]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_MediaM2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PesoKgI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KgM2I]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 166.299288270000000000
          Height = 13.228346456692910000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 11.338590000000000000
          Width = 257.008008270000000000
          Height = 13.228346456692910000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gera'#231#227'o de processados')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 11.338590000000000000
          Width = 120.944928270000000000
          Height = 13.228346456692910000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gelatina')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 24.566929133858270000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PecasG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PesoKgG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 24.566929133858270000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PercG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 11.338590000000000000
          Width = 120.944928270000000000
          Height = 13.228346456692910000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tapete')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 24.566929133858270000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 24.566929133858270000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PecasT]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PesoKgT]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 24.566929133858270000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PercT]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 24.566929133858270000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 37.795300000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PercI]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsPend: TfrxDBDataset
    UserName = 'frxDsPend'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pecas=Pecas'
      'SdoVrtPeca=SdoVrtPeca'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas_1=Pecas_1'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca_1=SdoVrtPeca_1'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'KgCouPQ=KgCouPQ'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'GGXRcl=GGXRcl'
      'JmpMovID=JmpMovID'
      'JmpNivel1=JmpNivel1'
      'JmpNivel2=JmpNivel2'
      'JmpGGX=JmpGGX'
      'RmsMovID=RmsMovID'
      'RmsNivel1=RmsNivel1'
      'RmsNivel2=RmsNivel2'
      'RmsGGX=RmsGGX'
      'GSPSrcMovID=GSPSrcMovID'
      'GSPSrcNiv2=GSPSrcNiv2'
      'GSPJmpMovID=GSPJmpMovID'
      'GSPJmpNiv2=GSPJmpNiv2'
      'DtCorrApo=DtCorrApo'
      'MovCodPai=MovCodPai'
      'IxxMovIX=IxxMovIX'
      'IxxFolha=IxxFolha'
      'IxxLinha=IxxLinha'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Iuvpei=Iuvpei'
      'CusFrtAvuls=CusFrtAvuls'
      'CusFrtMOEnv=CusFrtMOEnv'
      'CusFrtMORet=CusFrtMORet'
      'CustoMOPc=CustoMOPc'
      'CustoComiss=CustoComiss'
      'PerceComiss=PerceComiss'
      'CusKgComiss=CusKgComiss'
      'CredValrImposto=CredValrImposto'
      'CredPereImposto=CredPereImposto'
      'RpICMS=RpICMS'
      'RpICMSST=RpICMSST'
      'RpPIS=RpPIS'
      'RpCOFINS=RpCOFINS'
      'RvICMS=RvICMS'
      'RvICMSST=RvICMSST'
      'RvPIS=RvPIS'
      'RvCOFINS=RvCOFINS'
      'RpIPI=RpIPI'
      'RvIPI=RvIPI'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercPc=PercPc')
    DataSet = QrPend
    BCDToCurrency = False
    DataSetOptions = []
    Left = 152
    Top = 144
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT -vmi.Pecas Pecas, -vmi.PesoKg PesoKg, '
      '-vmi.QtdGerArM2 AreaM2, '
      'vmi.QtdGerArM2 / vmi.Pecas MediaM2, '
      'vmi.PesoKg / vmi.QtdGerArM2 kgM2,   '
      'vmi.Codigo, vmi.MovimCod, vmi.Controle,   '
      'vmi.DstGGX, vmi.SrcGGX, vmi.SrcNivel1, vmi.SrcNivel2,  '
      'ggx.GraGru1, CONCAT(gg1.Nome,   '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),    '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    '
      'NO_PRD_TAM_COR,  '
      'vmi.Marca, vmi.Observ  '
      'FROM vsmovits vmi    '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.DstGGX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'WHERE SrcMovID=1 '
      'AND SrcNivel1=9222 ')
    Left = 316
    Top = 4
    object QrItensPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrItensPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrItensAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrItensMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrItenskgM2: TFloatField
      FieldName = 'kgM2'
    end
    object QrItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItensMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrItensDstGGX: TIntegerField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrItensSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrItensSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrItensSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrItensGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrItensNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrItensObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrItensMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrItensPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
  end
  object frxDsItens: TfrxDBDataset
    UserName = 'frxDsItens'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'MediaM2=MediaM2'
      'kgM2=kgM2'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'DstGGX=DstGGX'
      'SrcGGX=SrcGGX'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Marca=Marca'
      'PercPc=PercPc')
    DataSet = QrItens
    BCDToCurrency = False
    DataSetOptions = []
    Left = 316
    Top = 56
  end
  object QrSumP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(vmi.SdoVrtPeca) / SUM(vmi.Pecas) * 100 PercPc,  '
      'SUM(vmi.Pecas) Pecas, SUM(vmi.SdoVrtPeca) SdoVrtPeca,'
      'SUM(vmi.PesoKg) PesoKg '
      'FROM vsmovits vmi   '
      'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo   '
      'WHERE vmi.MovimID=1   '
      
        'AND vic.DtEntrada  BETWEEN "2023-01-21" AND "2023-02-20 23:59:59' +
        '" '
      'AND vmi.SdoVrtPeca > 0   ')
    Left = 390
    Top = 2
    object QrSumPPercPc: TFloatField
      FieldName = 'PercPc'
    end
    object QrSumPPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumPPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrSumCur: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
      'SUM(-vmi.QtdGerArM2) AreaM2, '
      'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
      'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
      'FROM vsmovits vmi   ')
    Left = 392
    Top = 57
    object QrSumCurPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumCurPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumCurAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumCurMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrSumCurkgM2: TFloatField
      FieldName = 'kgM2'
    end
  end
  object QrSumGel: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
      'SUM(-vmi.QtdGerArM2) AreaM2, '
      'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
      'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
      'FROM vsmovits vmi   ')
    Left = 392
    Top = 105
    object QrSumGelPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumGelPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumGelAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumGelMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrSumGelkgM2: TFloatField
      FieldName = 'kgM2'
    end
  end
  object QrSumTap: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
      'SUM(-vmi.QtdGerArM2) AreaM2, '
      'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
      'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
      'FROM vsmovits vmi   ')
    Left = 392
    Top = 157
    object QrSumTapPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTapPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumTapAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumTapMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrSumTapkgM2: TFloatField
      FieldName = 'kgM2'
    end
  end
  object QrVSInn: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT AnoMesDiaIn, '
      'DATE_FORMAT(DtEntrada, "%Y-%m-%d") Data,'
      'SUM(Pecas) InnPecas, '
      'SUM(PesoKg) InnPesoKg, '
      'SUM(PesoKg) / SUM(Pecas) InnKgPeca, '
      'SUM(InfPecas) NFePecas, SUM(Pecas) - SUM(SdoVrtPeca) CalPecas  '
      'FROM _vs_in_cab_e_its '
      'GROUP BY AnoMesDiaIn '
      'ORDER BY AnoMesDiaIn ')
    Left = 496
    Top = 125
    object QrVSInnAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSInnData: TWideStringField
      FieldName = 'Data'
      Size = 10
    end
    object QrVSInnInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
    object QrVSInnInnPesoKg: TFloatField
      FieldName = 'InnPesoKg'
    end
    object QrVSInnInnKgPeca: TFloatField
      FieldName = 'InnKgPeca'
    end
    object QrVSInnNFePecas: TFloatField
      FieldName = 'NFePecas'
    end
    object QrVSInnCalPecas: TFloatField
      FieldName = 'CalPecas'
    end
    object QrVSInnGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
  end
  object QrExec: TMySQLQuery
    Left = 472
    Top = 4
  end
  object QrVSGer: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT AnoMesDiaIn, ArtGeComodty,'
      'SUM(-Pecas) Pecas, SUM(-QtdGerPeso) QtdGerPeso,'
      'SUM(-QtdGerArP2) QtdGerArM2, SUM(-QtdGerArP2) QtdGerArP2,'
      'SUM(-PesoKg) / SUM(-QtdGerArM2) RendKgM2,'
      'SUM(-QtdGerArM2) / SUM(-Pecas) MediaM2'
      'FROM _vs_ga_cab_e_its'
      'GROUP BY AnoMesDiaIn, ArtGeComodty'
      'ORDER BY AnoMesDiaIn, ArtGeComodty')
    Left = 494
    Top = 175
    object QrVSGerGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSGerAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSGerArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrVSGerPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSGerQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSGerRendKgM2: TFloatField
      FieldName = 'RendKgM2'
    end
    object QrVSGerMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
  end
  object QrDia: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 536
    Top = 4
    object QrDiaInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
  end
  object QrArtGeComodty: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gci.ArtGeComodty, agc.Sigla, agc.VisuRel,'
      'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,'
      'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,'
      'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2'
      'FROM _vs_ga_cab_e_its gci'
      
        'LEFT JOIN bluederm_ciaanterior.artgecomodty agc ON agc.Codigo=gc' +
        'i.ArtGeComodty'
      'GROUP BY ArtGeComodty'
      'ORDER BY ArtGeComOrd, ArtGeComodty ')
    Left = 580
    Top = 120
    object QrArtGeComodtyArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrArtGeComodtySigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrArtGeComodtyVisuRel: TIntegerField
      FieldName = 'VisuRel'
    end
    object QrArtGeComodtyShowPecas: TLargeintField
      FieldName = 'ShowPecas'
    end
    object QrArtGeComodtyShowPesoKg: TLargeintField
      FieldName = 'ShowPesoKg'
    end
    object QrArtGeComodtyShowAreaM2: TLargeintField
      FieldName = 'ShowAreaM2'
    end
    object QrArtGeComodtyShowPercPc: TLargeintField
      FieldName = 'ShowPercPc'
    end
    object QrArtGeComodtyShowKgM2: TLargeintField
      FieldName = 'ShowKgM2'
    end
    object QrArtGeComodtyShowMediaM2: TLargeintField
      FieldName = 'ShowMediaM2'
    end
  end
  object frxWET_CURTI_242_1_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44981.814375798610000000
    ReportOptions.LastChange = 44981.814375798610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 176
    Top = 220
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object QrGafid: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gg1.Nome  NO_GGXInn, afi.* '
      'FROM _vs_ger_art_from_in_dd afi'
      
        'LEFT JOIN bluederm_ciaanterior.gragrux ggx ON ggx.Controle=afi.G' +
        'GXInn'
      
        'LEFT JOIN bluederm_ciaanterior.gragru1 gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1')
    Left = 50
    Top = 151
  end
  object frxReport2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_241_01GetValue
    Left = 296
    Top = 128
    Datasets = <
      item
        DataSet = frxDsPend
        DataSetName = 'frxDsPend'
      end
      item
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708710240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object MeDono: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object MeTitulo: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 744.567410000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de Produ'#231#227'o de Artigo Gerado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeData: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 1046.929150940000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object MeTitPeriCompra: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo compra:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPeriCompra: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoCompra]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitEntrada: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 136.062982360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
        end
        object MeTitCaleiro: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 60.472480000000000000
          Width = 26.456661180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Caleiro')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitCaleiroPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 75.590600000000000000
          Width = 26.456678270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object MeTitArtigo1: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 60.472480000000000000
          Width = 154.960690940000000000
          Height = 15.118110240000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo de artigo 1')
          ParentFont = False
          WordWrap = False
        end
        object MeTitPeriViagem: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo sa'#237'da:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPeriViagem: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoViagem]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPeriEntrada: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315400000000000000
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo entrada:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPeriEntrada: TfrxMemoView
          AllowVectorExport = True
          Left = 774.803650000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoEntrada]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSubTitEntradaData: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitEntradaPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 75.590600000000000000
          Width = 26.456612360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitEntradaPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 75.590600000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitEntradaKgPc: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 75.590600000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitEntradaNFePc: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 75.590600000000000000
          Width = 26.456612360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'NFe p'#231)
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1Pecas: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 75.590600000000000000
          Width = 26.456612360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 75.590600000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1AreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 75.590600000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #224'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1PercPc: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 75.590600000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% p'#231)
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1KgM2: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 75.590600000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
        object MeSubTitArtigo1MediaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 75.590600000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'M.m'#178)
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 396.850650000000000000
        Width = 1046.929810000000000000
        object MemoUrlDmk: TfrxMemoView
          AllowVectorExport = True
          Width = 744.567410000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 725.669760000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 215.433210000000000000
        Width = 1046.929810000000000000
        DataSetName = 'frxDsGafit'
        RowCount = 0
        object MeValEntradaData: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236142360000000000
          Height = 15.118120000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGafit."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValEntradaPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 26.456661180000000000
          Height = 15.118110240000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '10.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValEntradaKgPc: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677148270000000000
          Height = 15.118110240000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '40,23')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValEntradaPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '400.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValEntradaNFePc: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 26.456661180000000000
          Height = 15.118110240000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '10.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCaleiroPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 26.456661180000000000
          Height = 15.118110240000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '10.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1AreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '50.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1PercPc: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '88,88')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1KgM2: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '12,00')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1MediaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 22.677082360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '8,88')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1Pecas: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 26.456661180000000000
          Height = 15.118110240000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '10.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValArtigo1PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 30.236142360000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '400.000')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 170.078850000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDsGafit."GGXInn"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 1046.929712360000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsGafit."NO_GGXInn"]Data')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236220470000000000
        Top = 253.228510000000000000
        Width = 1046.929810000000000000
        object MeSumEntradaPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 26.456661180000000000
          Height = 30.236220470000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."InnPecas">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumEntradaKgPc: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677148270000000000
          Height = 30.236220470000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGafit."InnPecas">,MasterData1) = 0, 0, SUM(<frxDs' +
              'Gafit."InnPesoKg">,MasterData1) / SUM(<frxDsGafit."InnPecas">,Ma' +
              'sterData1))]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumEntradaPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 30.236142360000000000
          Height = 30.236220470000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."InnPesoKg">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumEntradaNFePc: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 26.456661180000000000
          Height = 30.236220470000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."NFePecas">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumCaleiroPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 26.456661180000000000
          Height = 30.236220470000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."CalPecas">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1AreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 30.236142360000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."AreaM2_1">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1PercPc: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 22.677082360000000000
          Height = 30.236220470000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGafit."InnPecas">,MasterData1) = 0, 0, SUM(<frxDs' +
              'Gafit."Pecas_1">,MasterData1) / SUM(<frxDsGafit."InnPecas">,Mast' +
              'erData1) * 100 )]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1KgM2: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 22.677082360000000000
          Height = 30.236220470000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGafit."AreaM2_1">,MasterData1) = 0, 0, SUM(<frxDs' +
              'Gafit."PesoKg_1">,MasterData1) / SUM(<frxDsGafit."AreaM2_1">,Mas' +
              'terData1))]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1MediaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 22.677082360000000000
          Height = 30.236220470000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGafit."Pecas_1">,MasterData1) = 0, 0, SUM(<frxDsG' +
              'afit."AreaM2_1">,MasterData1) / SUM(<frxDsGafit."Pecas_1">,Maste' +
              'rData1))]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1Pecas: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 26.456661180000000000
          Height = 30.236220470000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."Pecas_1">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumArtigo1PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 30.236142360000000000
          Height = 30.236220470000000000
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGafit."PesoKg_1">,MasterData1)]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeSumTextoTotal: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236142360000000000
          Height = 30.236240000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236220470000000000
        Top = 343.937230000000000000
        Width = 1046.929810000000000000
      end
    end
  end
  object frxDsGafid: TfrxDBDataset
    UserName = 'frxDBGafid'
    CloseDataSource = False
    DataSet = QrGafid
    BCDToCurrency = False
    DataSetOptions = []
    Left = 48
    Top = 200
  end
end
