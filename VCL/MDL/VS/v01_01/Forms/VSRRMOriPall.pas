unit VSRRMOriPall;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSRRMOriPall = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaPecas: TLabel;
    LaPesoKg: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    DsEstqR4: TDataSource;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrEstqR4Ficha: TIntegerField;
    QrEstqR4SerieFch: TIntegerField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4MediaM2: TFloatField;
    QrEstqR4DataHora: TDateTimeField;
    CkSemi: TCheckBox;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    CkPalEspecificos: TCheckBox;
    EdPalEspecificos: TdmkEdit;
    EdSelPecas: TdmkEdit;
    EdSelPesoKg: TdmkEdit;
    EdSelAreaM2: TdmkEdit;
    EdSelAreaP2: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    QrVSMovItsClientMO: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel3: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrEstqR4NFeNum: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPecasChange(Sender: TObject);
    procedure EdPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure CkSemiClick(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure EdAreaM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    //
    procedure AdicionaPallet();
    procedure InsereIMEI_Atual(InsereTudo: Boolean;
              ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenVSGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
  public
    { Public declarations }
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FOrigMovimNiv, FOrigMovimCod, FOrigCodigo, FNewGraGruX,
    FTipoArea, FStqCenLoc, FFornecMO: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    function  BaixaPalletTotal(): Integer;
    function  BaixaPalletParcial(): Integer;
    procedure DefineTipoArea();
    procedure ReopenItensAptos();
    procedure ReopenGraGruX();
  end;

  var
  FmVSRRMOriPall: TFmVSRRMOriPall;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSRRMCab, UnVS_PF, AppListas, UnGrade_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSRRMOriPall.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSRRMOriPall.AdicionaPallet();
begin
  VS_CRC_PF.ReopenVSMovIts_Pallet2(QrVSMovIts, QrEstqR4);
  QrVSMovIts.First;
  while not QrVSMovIts.Eof do
  begin
    InsereIMEI_Atual(True, 0, 0, 0, 0);
    QrVSMovIts.Next;
  end;
end;

function TFmVSRRMOriPall.BaixaPalletParcial(): Integer;
var
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
begin
  Result := 0;
  Pecas  := EdPecas.ValueVariant;
  PesoKg := EdPesoKg.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  SobraPecas  := Pecas;
  SobraPesoKg := PesoKg;
  SobraAreaM2 := AreaM2;
  SobraAreaP2 := AreaP2;
  //
  if MyObjects.FIC(Pecas <= 0, EdPecas, 'Informe a quantidade de pe�as') then
    Exit;
  VS_CRC_PF.ReopenVSMovIts_Pallet2(QrVSMovIts, QrEstqR4);
  QrVSMovIts.Last;
  while not QrVSMovIts.Bof do
  begin
    if SobraPecas > 0 then
    begin
      if SobraPecas >= QrVSMovItsSdoVrtPeca.Value then
      begin
        InsereIMEI_Atual(True, 0, 0, 0, 0);
        SobraPecas  := SobraPecas  - QrVSMovItsSdoVrtPeca.Value;
        SobraPesoKg := SobraPesoKg - QrVSMovItsSdoVrtPeso.Value;
        SobraAreaM2 := SobraAreaM2 - QrVSMovItsSdoVrtArM2.Value;
        SobraAreaP2 := SobraAreaP2 - Geral.ConverteArea(QrVSMovItsSdoVrtArM2.Value, ctM2toP2, cfQuarto);
        Result := Result + 1;
      end else
      if SobraPecas < QrVSMovItsSdoVrtPeca.Value then
      begin
        InsereIMEI_Atual(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2);
        SobraPecas  := 0;
        SobraPesoKg := 0;
        SobraAreaM2 := 0;
        SobraAreaP2 := 0;
        Result := Result + 1;
        //
        QrVSMovIts.Prior;
        Exit;
      end;
    end;
    QrVSMovIts.Prior;
  end;
  // Inserir se ficar sobra mesmo que a origem fique negativa!
  if SobraPecas > 0 then
  begin
    InsereIMEI_Atual(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2);
    Result := Result + 1;
  end;
end;

function TFmVSRRMOriPall.BaixaPalletTotal(): Integer;
var
  N, I, Codigo, MovimCod: Integer;
begin
  QrEstqR4.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      AdicionaPallet();
    end;
(*
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSRRMcab', MovimCod);
    MovimCod := FmVSRRMCab.QrVSRRMCabMovimCod.Value;
    VS_PF.AtualizaTotaisVSRRMCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmVSRRMCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
  finally
    QrEstqR4.EnableControls;
  end;
  Result := N;
end;

procedure TFmVSRRMOriPall.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  if FParcial then
    N := BaixaPalletParcial()
  else
    N := BaixaPalletTotal();
  //
  // Nao se aplica. Calcula com function propria a seguir.
  //VS_PF.AtualizaTotaisVSXxxCab('VSRRMcab', MovimCod);
  MovimCod := FmVSRRMCab.QrVSRRMCabMovimCod.Value;
  VS_PF.AtualizaTotaisVSRRMCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmVSRRMCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      EdControle.ValueVariant := 0;
      EdPecas.ValueVariant := 0;
      EdPesoKg.ValueVariant := 0;
      EdAreaM2.ValueVariant := 0;
      EdAreaP2.ValueVariant := 0;
      EdObserv.ValueVariant := '';
      //
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
end;

procedure TFmVSRRMOriPall.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRRMOriPall.CkSemiClick(Sender: TObject);
begin
  ReopenGraGruX();
end;

procedure TFmVSRRMOriPall.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  I: Integer;
  Pecas, AreaM2, AreaP2, PesoKg: Double;
begin
  Pecas  := 0;
  AreaM2 := 0;
  AreaP2 := 0;
  PesoKg := 0;
  //
  if DBG04Estq.SelectedRows.Count > 0 then
  begin
    with DBG04Estq.DataSource.DataSet do
    for i:= 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[i]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[i]);
      Pecas  := Pecas  + QrEstqR4Pecas.Value;
      AreaM2 := AreaM2 + QrEstqR4AreaM2.Value;
      AreaP2 := AreaP2 + QrEstqR4AreaP2.Value;
      PesoKg := PesoKg + QrEstqR4PesoKg.Value;
      //
    end;
  end;
  EdSelPecas.ValueVariant   := Pecas;
  EdSelAreaM2.ValueVariant  := AreaM2;
  EdSelAreaP2.ValueVariant  := AreaP2;
  EdSelPesoKg.ValueVariant  := PesoKg;
end;

procedure TFmVSRRMOriPall.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSRRMOriPall.DefineTipoArea();
begin
  LaAreaM2.Enabled := FTipoArea = 0;
  EdAreaM2.Enabled := FTipoArea = 0;

  LaAreaP2.Enabled := FTipoArea = 1;
  EdAreaP2.Enabled := FTipoArea = 1;
end;

procedure TFmVSRRMOriPall.EdAreaM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PecaPar, PecaTot, ArM2Par, ArM2Tot: Double;
begin
  if Key = VK_F3 then
  begin
    PecaPar := EdPecas.ValueVariant;
    PecaTot := QrEstqR4SdoVrtPeca.Value;
    ArM2Tot := QrEstqR4SdoVrtArM2.Value;
    if PecaTot > 0 then
      ArM2Par := PecaPar / PecaTot * ArM2Tot
    else
      ArM2Par := 0;
    EdAreaM2.ValueVariant := ArM2Par;
  end;
end;

procedure TFmVSRRMOriPall.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSRRMOriPall.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSRRMOriPall.EdPecasChange(Sender: TObject);
(*
var
  Pecas: Double;
*)
begin
(*
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdPecas.ValueVariant;
  if (QrAptos.State <> dsInactive) and (QrAptos.RecordCount > 0)
  and (Pecas >= QrAptosSdoVrtPeca.Value) then
  begin
    EdPesoKg.ValueVariant  := QrAptosSdoVrtPeso.Value;
    LaPesoKg.Enabled  := False;
    EdPesoKg.Enabled  := False;
  end else
  begin
    LaPesoKg.Enabled  := GBGerar.Visible;
    EdPesoKg.Enabled  := GBGerar.Visible;
  end;
*)
end;

procedure TFmVSRRMOriPall.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdPecas.ValueVariant  := QrEstqR4SdoVrtPeca.Value;
    EdPesoKg.ValueVariant := QrEstqR4SdoVrtPeso.Value;
    EdAreaM2.ValueVariant := QrEstqR4SdoVrtArM2.Value;
  end;
end;

procedure TFmVSRRMOriPall.EdPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Pecas, PesoKg: Double;
*)
begin
(*
  if Key = VK_F4 then
  begin
    Pecas  := EdPecas.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoVrtPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtPeso.Value;
    //
    EdPesoKg.ValueVariant := PesoKg;
  end;
*)
end;

procedure TFmVSRRMOriPall.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSRRMOriPall.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSRRMOriPall.FechaPesquisa();
begin
  QrEstqR4.Close;
end;

procedure TFmVSRRMOriPall.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmVSRRMOriPall.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  VS_PF.AbreVSSerFch(QrVSSerFch);
  //ReopenGraGruX();
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
end;

procedure TFmVSRRMOriPall.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRRMOriPall.InsereIMEI_Atual(InsereTudo: Boolean;
 ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2: Double);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, VSMulFrnCab,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, ReqMovEstq, ClientMO, GGXRcl, StqCenLoc, FornecMO, VmiPai: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
begin
  SrcMovID       := QrVSMovItsMovimID.Value;
  SrcNivel1      := QrVSMovItsCodigo.Value;
  SrcNivel2      := QrVSMovItsControle.Value;
  SrcGGX         := QrVSMovItsGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmReprRM;
  MovimNiv       := eminSorcRRM;
  Pallet         := QrVSMovItsPallet.Value;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  if not InsereTudo then
  begin
    Pecas        := -ParcPecas;
    PesoKg       := -ParcPesoKg;
    AreaM2       := -ParcAreaM2;
    AreaP2       := -ParcAreaP2;
  end else
  begin
    Pecas          := -QrVSMovItsSdoVrtPeca.Value;
    PesoKg         := -QrVSMovItsSdoVrtPeso.Value;
    AreaM2         := -QrVSMovItsSdoVrtArM2.Value;
    AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  end;
  if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
    Valor := (-AreaM2) *
    (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
  else
  if QrVSMovItsPecas.Value > 0 then
    Valor := (-Pecas) *
    (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value)
  else
    Valor := 0;
  //
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := 0; //QrVSMovItsSerieFch.Value;
  Ficha          := 0;  //QrVSMovItsFicha.Value;
  Marca          := ''; //QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  ClientMO       := QrVSMovItsClientMO.Value;
  StqCenLoc      := FStqCenLoc;
  //
  VmiPai := 0; //FmVSRRMCab.QrVSRRMAtuControle.Value;
  FornecMo       := FFornecMO;
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_SEL_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei089(*Baixa de couro em reprocesso/reparo por pallet*)) then
  begin
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, True);
  end;
end;

procedure TFmVSRRMOriPall.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstqR4.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled := not Status;
    EdFicha.Enabled := not Status;
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdPecas.Enabled) and (EdPecas.Visible) then
      EdPecas.SetFocus;
  end else
    if Avisa then
      Geral.MB_Aviso(
      'Para inclu�o parcial feche a janela e selecione a op��o apropriada!');
end;

procedure TFmVSRRMOriPall.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSRRMOriPall.ReopenGraGruX();
(*
var
  SQL_Semi: String;
*)
begin
(*
  if CkSemi.Checked then
    SQL_Semi := ',' + Geral.FF0(CO_GraGruY_6144_VSFinCla)
  else
    SQL_Semi := '';
*)
  VS_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY<>0');
(*    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +  SQL_Semi +
    ')');
*)
  VS_PF.AbreGraGruXY(QrGGXRcl, 'AND ggx.GragruY<>0');
(*
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +  SQL_Semi +
    ')');
*)
end;

procedure TFmVSRRMOriPall.ReopenItensAptos();
const
  //GraGruX   = 0;
  Pallet    = 0;
  Terceiro  = 0;
  CouNiv2   = 0;
  CouNiv1   = 0;
  GraGruYs  = '';
  StqCenCad = 0;
  StqCenLoc = 0;
  MovimID   = 0;
  MovimCod  = 0;
var
  GraGruX: TPallArr;
  SQL_Especificos: String;
begin
  VS_CRC_PF.SetaGGXUnicoEmLista(EdGraGruX.ValueVariant, GraGruX);
  //
  if CkPalEspecificos.Checked and (Trim(EdPalEspecificos.Text) <> '') then
    SQL_Especificos := ' vmi.Pallet IN (' + Trim(EdPalEspecificos.Text) + ') '
  else
    SQL_Especificos := '';
  //
  if VS_PF.PesquisaPallets(FEmpresa, FClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqCenLoc, Self.Name, GraGruYs, GraGruX, [], SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FVSMovImp4)
  then
    VS_CRC_PF.ReopenVSListaPallets(QrEstqR4, FVSMovImp4,
    FEmpresa, EdGraGruX.ValueVariant, Pallet, Terceiro, '');
end;

{
procedure TFmVSRRMOri.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FVSRRMOriIMEI <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FVSRRMOriIMEI, Dmod.MyDB);
    if Controle <> 0 then
      FVSRRMOriIMEI.Locate('Controle', Controle, []);
  end;
  if FVSRRMOriPallet <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FVSRRMOriPallet, Dmod.MyDB);
    if Controle <> 0 then
      FVSRRMOriPallet.Locate('Pallet', Pallet, []);
  end;
end;
}

{
function TFmVSRRMOri.ValorTParcial(): Double;
begin
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKg.ValueVariant / QrAptosPesoKg.Value * QrAptosValorT.Value
  else
    Result := 0;
end;
}

end.
