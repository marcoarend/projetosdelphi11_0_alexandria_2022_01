unit VSPwdDd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmVSPwdDd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrSenhas: TmySQLQuery;
    QrSenhasNumero: TIntegerField;
    QrSenhasLogin: TWideStringField;
    EdVSPwdDdUser: TdmkEditCB;
    Label2: TLabel;
    CbVSPwdDdUser: TdmkDBLookupComboBox;
    EdVSPwdDdClas: TdmkEdit;
    Label1: TLabel;
    DsSenhas: TDataSource;
    QrControle: TmySQLQuery;
    QrControleVSPwdDdClas: TWideStringField;
    QrControleVSPwdDdData: TDateField;
    QrControleVSPwdDdUser: TIntegerField;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RegeraSenha();
    function  AlteraDados(): Boolean;
  end;

  var
  FmVSPwdDd: TFmVSPwdDd;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UMySQLModule;

{$R *.DFM}

function TFmVSPwdDd.AlteraDados: Boolean;
var
  VSPwdDdClas, VSPwdDdData: String;
  VSPwdDdUser: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  VSPwdDdClas    := EdVSPwdDdClas.Text;
  VSPwdDdData    := Geral.FDT(DModG.ObtemAgora(), 1);
  VSPwdDdUser    := EdVSPwdDdUser.ValueVariant;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'controle', False, [
  'VSPwdDdClas', 'VSPwdDdData', 'VSPwdDdUser'], [
  'Codigo'
  ], [
  VSPwdDdClas, VSPwdDdData, VSPwdDdUser], [
  1
  ], False);
end;

procedure TFmVSPwdDd.BtOKClick(Sender: TObject);
begin
  if AlteraDados() then
    Close;
end;

procedure TFmVSPwdDd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPwdDd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPwdDd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSenhas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
  EdVSPwdDdUser.ValueVariant := QrControleVSPwdDdUser.Value;
  CBVSPwdDdUser.KeyValue     := QrControleVSPwdDdUser.Value;
  EdVSPwdDdClas.ValueVariant := QrControleVSPwdDdClas.Value;
end;

procedure TFmVSPwdDd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPwdDd.RegeraSenha();
var
  Atual, Nova: Integer;
begin
  Atual := Geral.IMV('0' + Geral.SoNumero_TT(EdVSPwdDdClas.Text));
  Nova := Random(9999);
  while (Nova < 10) or (Nova = Atual) do
  begin
    Randomize();
    Nova := Random(9999);
  end;
  EdVSPwdDdClas.Text := Geral.FFF(Nova, 4);
end;

procedure TFmVSPwdDd.SpeedButton1Click(Sender: TObject);
begin
  RegeraSenha();
end;

end.
