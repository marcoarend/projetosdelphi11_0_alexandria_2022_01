object FmVSGerArtItsCurMul: TFmVSGerArtItsCurMul
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-152 :: Item de Gera'#231#227'o de Artigo de Ribeira do Curtime' +
    'nto - Mul'
  ClientHeight = 701
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 61
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 483
      Height = 44
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label5: TLabel
        Left = 8
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label2: TLabel
        Left = 144
        Top = 0
        Width = 58
        Height = 13
        Caption = 'Red. It.Ger.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 212
        Top = 0
        Width = 51
        Height = 13
        Caption = 'ID Movim.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label1: TLabel
        Left = 280
        Top = 0
        Width = 58
        Height = 13
        Caption = 'ID Gera'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 348
        Top = 0
        Width = 61
        Height = 13
        Caption = 'ID It.Gerado:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 416
        Top = 0
        Width = 62
        Height = 13
        Caption = 'ID Mov Twn:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 76
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSrcGGX: TdmkEdit
        Left = 144
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSrcMovID: TdmkEdit
        Left = 212
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSrcNivel1: TdmkEdit
        Left = 280
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSrcNivel2: TdmkEdit
        Left = 348
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 416
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimTwn'
        UpdCampo = 'MovimTwn'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel5: TPanel
      Left = 485
      Top = 15
      Width = 140
      Height = 44
      Align = alLeft
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label10: TLabel
        Left = 4
        Top = 0
        Width = 47
        Height = 13
        Caption = 'Pesagem:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 72
        Top = 0
        Width = 29
        Height = 13
        Caption = 'Ful'#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object dmkEdit1: TdmkEdit
        Left = 4
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit2: TdmkEdit
        Left = 72
        Top = 18
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel7: TPanel
      Left = 625
      Top = 15
      Width = 381
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 2
      object Label15: TLabel
        Left = 4
        Top = 2
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label16: TLabel
        Left = 92
        Top = 2
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label18: TLabel
        Left = 176
        Top = 2
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label19: TLabel
        Left = 252
        Top = 2
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object EdSumPeca: TdmkEdit
        Left = 4
        Top = 18
        Width = 85
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdTotDstPecaKeyDown
      end
      object EdSumPeso: TdmkEdit
        Left = 92
        Top = 18
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSumArM2: TdmkEditCalc
        Left = 176
        Top = 18
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdSumArP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdSumArP2: TdmkEditCalc
        Left = 249
        Top = 18
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdSumArM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 699
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - Mul'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 699
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - Mul'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 699
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - Mul'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 587
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 631
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 152
        Top = 16
        Width = 137
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 1
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 109
    Width = 1008
    Height = 252
    Align = alClient
    TabOrder = 4
    object Splitter1: TSplitter
      Left = 433
      Top = 15
      Width = 5
      Height = 235
      ExplicitLeft = 345
    end
    object DBGItensFicha: TdmkDBGridZTO
      Left = 438
      Top = 15
      Width = 568
      Height = 235
      Align = alClient
      DataSource = DsItensFicha
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clGray
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnAfterMultiselect = DBGItensFichaAfterMultiselect
      OnDblClick = DBGItensFichaDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'MovimCod'
          Title.Caption = 'IME-C'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Width = 96
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Pele In Natura'
          Width = 118
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeca'
          Title.Caption = 'Sdo. P'#231
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeso'
          Title.Caption = 'Sdo kg'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Codi. entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie RMP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Title.Caption = 'Ficha RMP'
          Visible = True
        end>
    end
    object DBGSumFicha: TDBGrid
      Left = 2
      Top = 15
      Width = 431
      Height = 235
      Align = alLeft
      DataSource = DsSumFicha
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGSumFichaDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerFicha'
          Title.Caption = 'S'#233'rie'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeca'
          Title.Caption = 'Sdo P'#231
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeso'
          Title.Caption = 'Sdo Kg'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoArM2'
          Title.Caption = 'Sdo m'#178
          Visible = True
        end>
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 361
    Width = 1008
    Height = 226
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 226
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object GroupBox2: TGroupBox
        Left = 2
        Top = 15
        Width = 1004
        Height = 146
        Align = alTop
        Caption = ' Dados da baixa do couro In Natura: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 129
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object LaPecasBxa: TLabel
            Left = 96
            Top = 4
            Width = 72
            Height = 13
            Caption = 'Pe'#231'as: [F4][F5]'
          end
          object LaPesoKgBxa: TLabel
            Left = 172
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
            Enabled = False
          end
          object SbPesoKgBxa: TSpeedButton
            Left = 244
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbPesoKgBxaClick
          end
          object LaQtdGerArM2Bxa: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            Enabled = False
          end
          object LaQtdGerArP2Bxa: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
            Enabled = False
          end
          object Label9: TLabel
            Left = 12
            Top = 44
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object Label49: TLabel
            Left = 428
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object Label50: TLabel
            Left = 754
            Top = 4
            Width = 74
            Height = 13
            Caption = 'Req.Mov.Estq.:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label20: TLabel
            Left = 416
            Top = 44
            Width = 103
            Height = 13
            Caption = 'Mat'#233'ria-prima jumped:'
          end
          object SbStqCenLoc: TSpeedButton
            Left = 731
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbStqCenLocClick
          end
          object EdControleBxa: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTotSrcPeca: TdmkEdit
            Left = 96
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdTotSrcPecaChange
            OnKeyDown = EdTotSrcPecaKeyDown
          end
          object EdTotSrcPeso: TdmkEdit
            Left = 172
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTotSrcArM2: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdTotSrcArP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdTotSrcArP2: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdTotSrcArM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservBxa: TdmkEdit
            Left = 12
            Top = 60
            Width = 401
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CGTpCalcAutoBxa: TdmkCheckGroup
            Left = 12
            Top = 84
            Width = 837
            Height = 41
            Caption = ' C'#225'lculos autom'#225'ticos: '
            Columns = 4
            Enabled = False
            Items.Strings = (
              'Pe'#231'as'
              'Peso kg'
              'Area m'#178
              'Area ft'#178)
            TabOrder = 9
            QryCampo = 'TpCalcAuto'
            UpdCampo = 'TpCalcAuto'
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 428
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'StqCenLoc'
            UpdCampo = 'StqCenLoc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 483
            Top = 20
            Width = 246
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_LOC_CEN'
            ListSource = DsStqCenLoc
            TabOrder = 6
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdReqMovEstq: TdmkEdit
            Left = 755
            Top = 20
            Width = 93
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdJmpGGX: TdmkEditCB
            Left = 416
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBJmpGGX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBJmpGGX: TdmkDBLookupComboBox
            Left = 472
            Top = 60
            Width = 377
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGGXJmp
            TabOrder = 11
            dmkEditCB = EdJmpGGX
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 161
        Width = 1004
        Height = 63
        Align = alClient
        Caption = ' Dados do artigo gerado: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 46
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label4: TLabel
            Left = 96
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label7: TLabel
            Left = 184
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
            Enabled = False
          end
          object LaQtdGerArM2Src: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object LaQtdGerArP2Src: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label12: TLabel
            Left = 428
            Top = 4
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object Label8: TLabel
            Left = 132
            Top = 4
            Width = 39
            Height = 13
            Caption = ' [F5][F3]'
            Enabled = False
          end
          object EdControleNew: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTotDstPeca: TdmkEdit
            Left = 96
            Top = 20
            Width = 85
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdTotDstPecaKeyDown
          end
          object EdTotDstPeso: TdmkEdit
            Left = 184
            Top = 20
            Width = 81
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTotDstArM2: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdTotDstArP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdTotDstArP2: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdTotDstArM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservNew: TdmkEdit
            Left = 428
            Top = 20
            Width = 421
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrItensFicha: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItensFichaAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vsmovits wmi '
      'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE wmi.MovimID=1'
      'AND SdoVrtPeca>0 '
      'AND Empresa=-11'
      'ORDER BY wmi.Controle ')
    Left = 36
    Top = 192
    object QrItensFichaNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrItensFichaNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrItensFichaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItensFichaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItensFichaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrItensFichaMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrItensFichaMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrItensFichaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItensFichaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrItensFichaCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrItensFichaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrItensFichaLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrItensFichaLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrItensFichaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrItensFichaPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrItensFichaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItensFichaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItensFichaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrItensFichaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrItensFichaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrItensFichaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrItensFichaSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrItensFichaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrItensFichaObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrItensFichaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrItensFichaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItensFichaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItensFichaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrItensFichaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrItensFichaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrItensFichaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrItensFichaFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrItensFichaMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrItensFichaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrItensFichaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrItensFichaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrItensFichaSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrItensFichaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrItensFichaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrItensFichaValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrItensFichaCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrItensFichaSdoPeca: TFloatField
      FieldName = 'SdoPeca'
    end
    object QrItensFichaSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrItensFichaSdoArM2: TFloatField
      FieldName = 'SdoArM2'
    end
    object QrItensFichaSdoArP2: TFloatField
      FieldName = 'SdoArP2'
    end
    object QrItensFichaVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrItensFichaDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
  end
  object DsItensFicha: TDataSource
    DataSet = QrItensFicha
    Left = 36
    Top = 240
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 192
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 188
    Top = 240
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 260
    Top = 192
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 260
    Top = 240
  end
  object QrNiv1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 552
    Top = 232
    object QrNiv1FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrNiv1MediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrNiv1MediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 336
    Top = 196
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 336
    Top = 244
  end
  object QrSumFicha: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSumFichaBeforeClose
    AfterScroll = QrSumFichaAfterScroll
    SQL.Strings = (
      'SELECT SerieFch, vsf.Nome NO_SerFicha,'
      'Ficha, SUM(QtdAntPeca-QtdGerPeca) SdoPeca,'
      'SUM(QtdAntPeso-QtdGerPeso) SdoPeso,'
      'SUM(QtdAntArM2-QtdGerArM2) SdoArM2,'
      'SUM(QtdAntArP2-QtdGerArP2) SdoArP2,'
      '(SUM(ValorMP + CustoPQ) / SUM(QtdAntPeso)) CustoPeso,'
      '(SUM(ValorMP + CustoPQ) / SUM(QtdAntArM2)) CustoArM2,'
      '(SUM(ValorMP + CustoPQ) / SUM(QtdAntPeca)) CustoPeca,'
      '(SUM(ValorMP) / SUM(QtdAntPeso)) ValMPPeso,'
      '(SUM(ValorMP) / SUM(QtdAntArM2)) ValMPArM2,'
      '(SUM(ValorMP) / SUM(QtdAntPeca)) ValMPPeca,'
      'SUM(ValorMP) ValorMP'
      'FROM vsmovits wmi'
      'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'WHERE wmi.MovimID=27'
      'AND wmi.MovimNiv=34'
      'AND QtdAntPeca>QtdGerPeca'
      'AND Empresa=-11'
      'AND ClientMO=38'
      'GROUP BY wmi.SerieFch, wmi.Ficha'
      'ORDER BY wmi.SerieFch, wmi.Ficha')
    Left = 464
    Top = 184
    object QrSumFichaSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrSumFichaNO_SerFicha: TWideStringField
      FieldName = 'NO_SerFicha'
      Size = 60
    end
    object QrSumFichaFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrSumFichaSdoPeca: TFloatField
      FieldName = 'SdoPeca'
    end
    object QrSumFichaSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumFichaSdoArM2: TFloatField
      FieldName = 'SdoArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumFichaSdoArP2: TFloatField
      FieldName = 'SdoArP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumFichaCustoArM2: TFloatField
      FieldName = 'CustoArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumFichaCustoPeca: TFloatField
      FieldName = 'CustoPeca'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumFichaCustoPeso: TFloatField
      FieldName = 'CustoPeso'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumFichaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrSumFichaValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrSumFichaValMPPeso: TFloatField
      FieldName = 'ValMPPeso'
    end
    object QrSumFichaValMPArM2: TFloatField
      FieldName = 'ValMPArM2'
    end
    object QrSumFichaValMPPeca: TFloatField
      FieldName = 'ValMPPeca'
    end
  end
  object DsSumFicha: TDataSource
    DataSet = QrSumFicha
    Left = 468
    Top = 228
  end
  object QrGGXJmp: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGGXJmpAfterOpen
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 656
    Top = 216
    object QrGGXJmpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXJmpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXJmpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXJmpSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXJmpCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXJmpNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXJmp: TDataSource
    DataSet = QrGGXJmp
    Left = 656
    Top = 264
  end
end
