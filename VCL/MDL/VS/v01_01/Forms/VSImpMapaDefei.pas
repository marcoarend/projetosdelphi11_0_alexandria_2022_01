unit VSImpMapaDefei;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Data.DB, mySQLDbTables, frxDBSet,
  UnDmkEnums, UnGrl_Consts, dmkGeral;

type
  TFmVSImpMapaDefei = class(TForm)
    frxWET_CURTI_231_01: TfrxReport;
    ImgCouroSrc: TImage;
    QrVSDefeiCaC: TmySQLQuery;
    QrVSDefeiCaCDefeitLocX: TIntegerField;
    QrVSDefeiCaCDefeitLocY: TIntegerField;
    QrVSDefeiCaCDefeitTipo: TIntegerField;
    QrVSInnIts: TmySQLQuery;
    QrVSInnItsCusFrtAvuls: TFloatField;
    QrVSInnItsCusFrtMOEnv: TFloatField;
    QrVSInnItsCodigo: TLargeintField;
    QrVSInnItsControle: TLargeintField;
    QrVSInnItsMovimCod: TLargeintField;
    QrVSInnItsMovimNiv: TLargeintField;
    QrVSInnItsMovimTwn: TLargeintField;
    QrVSInnItsEmpresa: TLargeintField;
    QrVSInnItsTerceiro: TLargeintField;
    QrVSInnItsCliVenda: TLargeintField;
    QrVSInnItsMovimID: TLargeintField;
    QrVSInnItsDataHora: TDateTimeField;
    QrVSInnItsPallet: TLargeintField;
    QrVSInnItsGraGruX: TLargeintField;
    QrVSInnItsPecas: TFloatField;
    QrVSInnItsPesoKg: TFloatField;
    QrVSInnItsAreaM2: TFloatField;
    QrVSInnItsAreaP2: TFloatField;
    QrVSInnItsValorT: TFloatField;
    QrVSInnItsSrcMovID: TLargeintField;
    QrVSInnItsSrcNivel1: TLargeintField;
    QrVSInnItsSrcNivel2: TLargeintField;
    QrVSInnItsSrcGGX: TLargeintField;
    QrVSInnItsSdoVrtPeca: TFloatField;
    QrVSInnItsSdoVrtPeso: TFloatField;
    QrVSInnItsSdoVrtArM2: TFloatField;
    QrVSInnItsObserv: TWideStringField;
    QrVSInnItsSerieFch: TLargeintField;
    QrVSInnItsFicha: TLargeintField;
    QrVSInnItsMisturou: TLargeintField;
    QrVSInnItsFornecMO: TLargeintField;
    QrVSInnItsCustoMOKg: TFloatField;
    QrVSInnItsCustoMOM2: TFloatField;
    QrVSInnItsCustoMOTot: TFloatField;
    QrVSInnItsValorMP: TFloatField;
    QrVSInnItsDstMovID: TLargeintField;
    QrVSInnItsDstNivel1: TLargeintField;
    QrVSInnItsDstNivel2: TLargeintField;
    QrVSInnItsDstGGX: TLargeintField;
    QrVSInnItsQtdGerPeca: TFloatField;
    QrVSInnItsQtdGerPeso: TFloatField;
    QrVSInnItsQtdGerArM2: TFloatField;
    QrVSInnItsQtdGerArP2: TFloatField;
    QrVSInnItsQtdAntPeca: TFloatField;
    QrVSInnItsQtdAntPeso: TFloatField;
    QrVSInnItsQtdAntArM2: TFloatField;
    QrVSInnItsQtdAntArP2: TFloatField;
    QrVSInnItsNotaMPAG: TFloatField;
    QrVSInnItsPedItsFin: TLargeintField;
    QrVSInnItsMarca: TWideStringField;
    QrVSInnItsStqCenLoc: TLargeintField;
    QrVSInnItsNO_PALLET: TWideStringField;
    QrVSInnItsNO_PRD_TAM_COR: TWideStringField;
    QrVSInnItsNO_TTW: TWideStringField;
    QrVSInnItsID_TTW: TLargeintField;
    QrVSInnItsReqMovEstq: TLargeintField;
    QrVSInnItsRendKgm2: TFloatField;
    QrVSInnItsNotaMPAG_TXT: TWideStringField;
    QrVSInnItsRendKgm2_TXT: TWideStringField;
    QrVSInnItsMisturou_TXT: TWideStringField;
    QrVSInnItsNOMEUNIDMED: TWideStringField;
    QrVSInnItsSIGLAUNIDMED: TWideStringField;
    QrVSInnItsm2_CouroTXT: TWideStringField;
    QrVSInnItsKgMedioCouro: TFloatField;
    QrVSInnItsVSMulFrnCab: TLargeintField;
    QrVSInnItsClientMO: TLargeintField;
    QrVSInnItsNO_SerieFch: TWideStringField;
    QrVSInnItsDtCorrApo: TDateTimeField;
    frxDsVSInnIts: TfrxDBDataset;
    ImgCouroDst: TImage;
    QrVSInnCab: TmySQLQuery;
    QrVSInnCabCodigo: TIntegerField;
    QrVSInnCabMovimCod: TIntegerField;
    QrVSInnCabEmpresa: TIntegerField;
    QrVSInnCabDtCompra: TDateTimeField;
    QrVSInnCabDtViagem: TDateTimeField;
    QrVSInnCabDtEntrada: TDateTimeField;
    QrVSInnCabFornecedor: TIntegerField;
    QrVSInnCabTransporta: TIntegerField;
    QrVSInnCabPecas: TFloatField;
    QrVSInnCabPesoKg: TFloatField;
    QrVSInnCabAreaM2: TFloatField;
    QrVSInnCabAreaP2: TFloatField;
    QrVSInnCabLk: TIntegerField;
    QrVSInnCabDataCad: TDateField;
    QrVSInnCabDataAlt: TDateField;
    QrVSInnCabUserCad: TIntegerField;
    QrVSInnCabUserAlt: TIntegerField;
    QrVSInnCabAlterWeb: TSmallintField;
    QrVSInnCabAtivo: TSmallintField;
    QrVSInnCabNO_EMPRESA: TWideStringField;
    QrVSInnCabNO_FORNECE: TWideStringField;
    QrVSInnCabNO_TRANSPORTA: TWideStringField;
    QrVSInnCabValorT: TFloatField;
    QrVSInnCabClienteMO: TIntegerField;
    QrVSInnCabProcednc: TIntegerField;
    QrVSInnCabMotorista: TIntegerField;
    QrVSInnCabPlaca: TWideStringField;
    QrVSInnCabNO_CLIENTEMO: TWideStringField;
    QrVSInnCabNO_PROCEDNC: TWideStringField;
    QrVSInnCabNO_MOTORISTA: TWideStringField;
    QrVSInnCabTemIMEIMrt: TIntegerField;
    QrVSInnCabide_nNF: TIntegerField;
    QrVSInnCabide_serie: TSmallintField;
    QrVSInnCabemi_serie: TIntegerField;
    QrVSInnCabemi_nNF: TIntegerField;
    QrVSInnCabNFeStatus: TIntegerField;
    QrVSInnCabVSVmcWrn: TSmallintField;
    QrVSInnCabVSVmcObs: TWideStringField;
    QrVSInnCabVSVmcSeq: TWideStringField;
    QrVSInnCabVSVmcSta: TSmallintField;
    QrVSInnCabDtEnceRend: TDateTimeField;
    QrVSInnCabTpEnceRend: TSmallintField;
    QrVSInnCabDtEnceRend_TXT: TWideStringField;
    frxDsVSInnCab: TfrxDBDataset;
    QrSumDefei: TmySQLQuery;
    frxDsSumDefei: TfrxDBDataset;
    QrSumDefeiDefeitTipo: TIntegerField;
    QrSumDefeiDefeitQtde: TFloatField;
    QrSumDefeiNO_DEFEITIP: TWideStringField;
    QrSumDefeiPERCENT: TFloatField;
    ImgSimbolo: TImage;
    QrSumDefeiSimbolo: TIntegerField;
    QrVSDefeiCaCSimbolo: TIntegerField;
    QrSumDefeiCorBorda: TIntegerField;
    QrSumDefeiCorMiolo: TIntegerField;
    QrVSDefeiCaCCorBorda: TIntegerField;
    QrVSDefeiCaCCorMiolo: TIntegerField;
    procedure QrVSInnItsAfterOpen(DataSet: TDataSet);
    procedure frxWET_CURTI_231_01GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeMapaDeDefeitos(SerieFch, Ficha: Integer);
    procedure ReopemVSInnIts(SerieFch, Ficha: Integer);
    procedure GeraImagemMapa(SerieFch, Ficha: Integer);
  end;

var
  FmVSImpMapaDefei: TFmVSImpMapaDefei;

implementation

uses
  UnVS_CRC_PF, UnMyObjects, DmkDAC_PF, Module, ModuleGeral;

{$R *.dfm}

//C:\_Sincro\Projetos_Aux\Glyfs\SVG

const
  FArquivoMapa = 'C:\Dermatek\Temp\Couro_Defeitos.bmp';

procedure TFmVSImpMapaDefei.frxWET_CURTI_231_01GetValue(const VarName: string;
  var Value: Variant);
var
  PictureView: TfrxPictureView;
  Arquivo: String;
  Simbolo, CorBorda, CorMiolo: Integer;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PECAS_TOTAL' then
    Value := QrVSInnItsPecas.Value
  else
  if VarName = 'LOAD_SIMBOLO' then
  begin
    Simbolo  := QrSumDefeiSimbolo.Value;
    CorBorda := QrSumDefeiCorBorda.Value;
    CorMiolo := QrSumDefeiCorMiolo.Value;
    Arquivo := 'C:\Dermatek\Imagens\BlueDerm\SimboloDefeitoCouro' + Geral.FFN(Simbolo, 3) + '.bmp';
    //MyObjects.Informa
    if not FileExists(Arquivo) then
    begin
      try
        ForceDirectories('C:\Dermatek\Imagens\BlueDerm');
        Arquivo := 'C:\Dermatek\Imagens\BlueDerm\SimboloDefeitoCouro' + Geral.FFN(Simbolo, 3) + '.bmp';
        MyObjects.CarregaBitmapBranco(ImgSimbolo, 19, 19);
        VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgSimbolo, 10, 10, Simbolo,
          CorBorda, CorMiolo);
        ImgSimbolo.Picture.SaveToFile(Arquivo);
      except
      //
      end;
    end;
    PictureView := frxWET_CURTI_231_01.FindObject('Picture2') as TfrxPictureView;
    PictureView.Picture.LoadFromFile(Arquivo);
    Value := True;
  end;
end;

procedure TFmVSImpMapaDefei.GeraImagemMapa(SerieFch, Ficha: Integer);
var
  PictureView: TfrxPictureView;
  X, Y, Simbolo: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSDefeiCaC, Dmod.MyDB, [
  'SELECT vdc.DefeitTipo, vdc.DefeitLocX, DefeitLocY, ',
  'vdt.Simbolo, vdt.CorBorda, vdt.CorMiolo ',
  'FROM vsdefeicac vdc',
  'LEFT JOIN defeitostip vdt ON vdt.Codigo=vdc.DefeitTipo ',
  'WHERE vdc.SerieFch=' + Geral.FF0(SerieFch),
  'AND vdc.Ficha=' + Geral.FF0(Ficha),
  '']);
  //
  ImgCouroDst.Picture.Bitmap.Assign(ImgCouroSrc.Picture.Bitmap);
  //
  QrVSDefeiCaC.First;
  while not QrVSDefeiCaC.Eof do
  begin
    VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgCouroDst,
      QrVSDefeiCaCDefeitLocX.Value, QrVSDefeiCaCDefeitLocY.Value,
      QrVSDefeiCaCSimbolo.Value, QrVSDefeiCaCCorBorda.Value,
      QrVSDefeiCaCCorMiolo.Value);

    //
    QrVSDefeiCaC.Next;
  end;
  if FileExists(FArquivoMapa) then
    DeleteFile(FArquivoMapa);
  //
  ImgCouroDst.Picture.SaveToFile(FArquivoMapa);
  PictureView := frxWET_CURTI_231_01.FindObject('Picture1') as TfrxPictureView;
  PictureView.Picture.LoadFromFile(FArquivoMapa);
  //
  //VS_CRC_PF.DesenhaSimboloDefeito_Picture(PictureView.Picture, (*X*)25, (*Y*)25, 1);
  //
end;

procedure TFmVSImpMapaDefei.ImprimeMapaDeDefeitos(SerieFch, Ficha: Integer);
begin
  ReopemVSInnIts(SerieFch, Ficha);
  //
  GeraImagemMapa(SerieFch, Ficha);
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrSumDefei, Dmod.MyDB, [
  'SELECT DefeitTipo, SUM(DefeitQtde) DefeitQtde,',
  'vdt.Nome NO_DEFEITIP, vdt.Simbolo, vdt.CorBorda, vdt.CorMiolo, ',
  'SUM(DefeitQtde) / ' + Geral.FFT_Dot(QrVSInnItsPecas.Value, 2, siNegativo) +
    ' * 100 PERCENT ',
  'FROM vsdefeicac vdc',
  'LEFT JOIN defeitostip vdt ON vdt.Codigo=vdc.DefeitTipo ',
  'WHERE SerieFch=' + Geral.FF0(SerieFch),
  'AND Ficha=' + Geral.FF0(Ficha),
  'GROUP BY DefeitTipo ',
    '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_231_01, [
    DModG.frxDsDono,
    frxDsSumDefei,
    frxDsVSInnCab,
    frxDsVSInnIts
    ]);
  MyObjects.frxMostra(frxWET_CURTI_231_01, '');
  //
end;

procedure TFmVSImpMapaDefei.QrVSInnItsAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnCab, Dmod.MyDB, [
  'SELECT wic.*, ',
  'IF(wic.DtEnceRend < "1900-01-01", "", DATE_FORMAT(wic.DtEnceRend, "%d/%m/%y %h:%i")) DtEnceRend_TXT, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, ',
  'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA ',
  'FROM vsinncab wic ',
  'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa ',
  'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor ',
  'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta ',
  'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO ',
  'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc ',
  'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista ',
  'WHERE wic.MovimCod=' + Geral.FF0(QrVSInnItsMovimCod.Value),
  ' ']);
end;

procedure TFmVSImpMapaDefei.ReopemVSInnIts(SerieFch, Ficha: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := 1;
  //
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'vmi.ClientMO, fch.Nome NO_SerieFch ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN vsserfch   fch ON fch.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  '']);
  SQL_Group := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVsInnIts, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
end;

end.
