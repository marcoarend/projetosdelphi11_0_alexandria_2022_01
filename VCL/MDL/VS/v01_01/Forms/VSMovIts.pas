unit VSMovIts;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, AppListas,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, UnProjGroup_Consts, UnGrl_Consts,
  UnAppEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmVSMovIts = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovSrc: TmySQLQuery;
    DsVSMovSrc: TDataSource;
    QrVSMovDst: TmySQLQuery;
    DsVSMovDst: TDataSource;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Panel10: TPanel;
    GroupBox2: TGroupBox;
    Panel11: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox3: TGroupBox;
    Panel12: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdSrcNivel2: TdmkDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    GroupBox4: TGroupBox;
    Panel13: TPanel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdDstNivel2: TdmkDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    GroupBox5: TGroupBox;
    Panel14: TPanel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    Panel15: TPanel;
    DBEdit40: TDBEdit;
    Label40: TLabel;
    Label34: TLabel;
    DBEdit34: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit41: TDBEdit;
    Label41: TLabel;
    Label33: TLabel;
    DBEdit32: TDBEdit;
    Label32: TLabel;
    Label31: TLabel;
    DBEdit31: TDBEdit;
    GroupBox6: TGroupBox;
    Panel16: TPanel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    GroupBox7: TGroupBox;
    Panel17: TPanel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    Panel7: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    BtIMEI: TBitBtn;
    PMIMEI: TPopupMenu;
    ExcluiIMEI1: TMenuItem;
    GBMovSrc: TGroupBox;
    DGMovSrc: TDBGrid;
    GBMovDst: TGroupBox;
    Splitter1: TSplitter;
    DGMovDst: TDBGrid;
    Recalculasaldos1: TMenuItem;
    AlteradadosdoartigodoIMEI1: TMenuItem;
    N1: TMenuItem;
    QrInacab: TmySQLQuery;
    QrInacabVMI_Baix: TIntegerField;
    QrInacabVSPaClaIts: TIntegerField;
    QrInacabBxaPecas: TFloatField;
    QrInacabVMI_Pecas: TFloatField;
    QrInacabSALDO: TFloatField;
    QrInacabBxaAreaM2: TFloatField;
    QrInacabBxaAreaP2: TFloatField;
    QrInacabVMI_Sorc: TIntegerField;
    PB1: TProgressBar;
    Recalcula1: TMenuItem;
    RecalculaReduzidodeorigem1: TMenuItem;
    Label46: TLabel;
    EdSenha: TEdit;
    Label51: TLabel;
    DBEdit46: TDBEdit;
    IrparagerenciamentodoMovimento1: TMenuItem;
    PMIMIEOrig: TPopupMenu;
    IrparaajaneladoMovimento1: TMenuItem;
    PMIMEIDest: TPopupMenu;
    MenuItem1: TMenuItem;
    Irparajaneladopallet1: TMenuItem;
    IrparajaneladaFichaRMP1: TMenuItem;
    rvoredoMovimento1: TMenuItem;
    Label52: TLabel;
    DBEdit51: TDBEdit;
    AlteradadosdoitemdeIMEI1: TMenuItem;
    DBEdit39: TDBEdit;
    Label39: TLabel;
    Label53: TLabel;
    DBEdit52: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Label38: TLabel;
    Label37: TLabel;
    DBEdit36: TDBEdit;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label35: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    PMImprime: TPopupMenu;
    PackingListVertical1: TMenuItem;
    PackingListHorizontal1: TMenuItem;
    QrVSMovSrcCodigo: TLargeintField;
    QrVSMovSrcControle: TLargeintField;
    QrVSMovSrcMovimCod: TLargeintField;
    QrVSMovSrcMovimNiv: TLargeintField;
    QrVSMovSrcMovimTwn: TLargeintField;
    QrVSMovSrcEmpresa: TLargeintField;
    QrVSMovSrcTerceiro: TLargeintField;
    QrVSMovSrcCliVenda: TLargeintField;
    QrVSMovSrcMovimID: TLargeintField;
    QrVSMovSrcDataHora: TDateTimeField;
    QrVSMovSrcPallet: TLargeintField;
    QrVSMovSrcGraGruX: TLargeintField;
    QrVSMovSrcPecas: TFloatField;
    QrVSMovSrcPesoKg: TFloatField;
    QrVSMovSrcAreaM2: TFloatField;
    QrVSMovSrcAreaP2: TFloatField;
    QrVSMovSrcValorT: TFloatField;
    QrVSMovSrcSrcMovID: TLargeintField;
    QrVSMovSrcSrcNivel1: TLargeintField;
    QrVSMovSrcSrcNivel2: TLargeintField;
    QrVSMovSrcSrcGGX: TLargeintField;
    QrVSMovSrcSdoVrtPeca: TFloatField;
    QrVSMovSrcSdoVrtPeso: TFloatField;
    QrVSMovSrcSdoVrtArM2: TFloatField;
    QrVSMovSrcObserv: TWideStringField;
    QrVSMovSrcSerieFch: TLargeintField;
    QrVSMovSrcFicha: TLargeintField;
    QrVSMovSrcMisturou: TLargeintField;
    QrVSMovSrcFornecMO: TLargeintField;
    QrVSMovSrcCustoMOKg: TFloatField;
    QrVSMovSrcCustoMOM2: TFloatField;
    QrVSMovSrcCustoMOTot: TFloatField;
    QrVSMovSrcValorMP: TFloatField;
    QrVSMovSrcDstMovID: TLargeintField;
    QrVSMovSrcDstNivel1: TLargeintField;
    QrVSMovSrcDstNivel2: TLargeintField;
    QrVSMovSrcDstGGX: TLargeintField;
    QrVSMovSrcQtdGerPeca: TFloatField;
    QrVSMovSrcQtdGerPeso: TFloatField;
    QrVSMovSrcQtdGerArM2: TFloatField;
    QrVSMovSrcQtdGerArP2: TFloatField;
    QrVSMovSrcQtdAntPeca: TFloatField;
    QrVSMovSrcQtdAntPeso: TFloatField;
    QrVSMovSrcQtdAntArM2: TFloatField;
    QrVSMovSrcQtdAntArP2: TFloatField;
    QrVSMovSrcNotaMPAG: TFloatField;
    QrVSMovSrcStqCenLoc: TLargeintField;
    QrVSMovSrcReqMovEstq: TLargeintField;
    QrVSMovSrcGraGru1: TLargeintField;
    QrVSMovSrcNO_EstqMovimID: TWideStringField;
    QrVSMovSrcNO_MovimID: TWideStringField;
    QrVSMovSrcNO_MovimNiv: TWideStringField;
    QrVSMovSrcNO_PALLET: TWideStringField;
    QrVSMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrVSMovSrcNO_FORNECE: TWideStringField;
    QrVSMovSrcNO_SerieFch: TWideStringField;
    QrVSMovSrcNO_TTW: TWideStringField;
    QrVSMovSrcID_TTW: TLargeintField;
    QrVSMovSrcNO_LOC_CEN: TWideStringField;
    QrVSMovDstCodigo: TLargeintField;
    QrVSMovDstControle: TLargeintField;
    QrVSMovDstMovimCod: TLargeintField;
    QrVSMovDstMovimNiv: TLargeintField;
    QrVSMovDstMovimTwn: TLargeintField;
    QrVSMovDstEmpresa: TLargeintField;
    QrVSMovDstTerceiro: TLargeintField;
    QrVSMovDstCliVenda: TLargeintField;
    QrVSMovDstMovimID: TLargeintField;
    QrVSMovDstDataHora: TDateTimeField;
    QrVSMovDstPallet: TLargeintField;
    QrVSMovDstGraGruX: TLargeintField;
    QrVSMovDstPecas: TFloatField;
    QrVSMovDstPesoKg: TFloatField;
    QrVSMovDstAreaM2: TFloatField;
    QrVSMovDstAreaP2: TFloatField;
    QrVSMovDstValorT: TFloatField;
    QrVSMovDstSrcMovID: TLargeintField;
    QrVSMovDstSrcNivel1: TLargeintField;
    QrVSMovDstSrcNivel2: TLargeintField;
    QrVSMovDstSrcGGX: TLargeintField;
    QrVSMovDstSdoVrtPeca: TFloatField;
    QrVSMovDstSdoVrtPeso: TFloatField;
    QrVSMovDstSdoVrtArM2: TFloatField;
    QrVSMovDstObserv: TWideStringField;
    QrVSMovDstSerieFch: TLargeintField;
    QrVSMovDstFicha: TLargeintField;
    QrVSMovDstMisturou: TLargeintField;
    QrVSMovDstFornecMO: TLargeintField;
    QrVSMovDstCustoMOKg: TFloatField;
    QrVSMovDstCustoMOM2: TFloatField;
    QrVSMovDstCustoMOTot: TFloatField;
    QrVSMovDstValorMP: TFloatField;
    QrVSMovDstDstMovID: TLargeintField;
    QrVSMovDstDstNivel1: TLargeintField;
    QrVSMovDstDstNivel2: TLargeintField;
    QrVSMovDstDstGGX: TLargeintField;
    QrVSMovDstQtdGerPeca: TFloatField;
    QrVSMovDstQtdGerPeso: TFloatField;
    QrVSMovDstQtdGerArM2: TFloatField;
    QrVSMovDstQtdGerArP2: TFloatField;
    QrVSMovDstQtdAntPeca: TFloatField;
    QrVSMovDstQtdAntPeso: TFloatField;
    QrVSMovDstQtdAntArM2: TFloatField;
    QrVSMovDstQtdAntArP2: TFloatField;
    QrVSMovDstNotaMPAG: TFloatField;
    QrVSMovDstStqCenLoc: TLargeintField;
    QrVSMovDstReqMovEstq: TLargeintField;
    QrVSMovDstGraGru1: TLargeintField;
    QrVSMovDstNO_EstqMovimID: TWideStringField;
    QrVSMovDstNO_MovimID: TWideStringField;
    QrVSMovDstNO_MovimNiv: TWideStringField;
    QrVSMovDstNO_PALLET: TWideStringField;
    QrVSMovDstNO_PRD_TAM_COR: TWideStringField;
    QrVSMovDstNO_FORNECE: TWideStringField;
    QrVSMovDstNO_SerieFch: TWideStringField;
    QrVSMovDstNO_TTW: TWideStringField;
    QrVSMovDstID_TTW: TLargeintField;
    QrVSMovDstNO_LOC_CEN: TWideStringField;
    Classesgeradas1: TMenuItem;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_EstqMovimID: TWideStringField;
    QrVSMovItsNO_DstMovID: TWideStringField;
    QrVSMovItsNO_SrcMovID: TWideStringField;
    QrVSMovItsNO_LOC_CEN: TWideStringField;
    QrVSMovItsGraGru1: TLargeintField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TLargeintField;
    QrVSMovItsPedItsFin: TLargeintField;
    QrVSMovItsPedItsVda: TLargeintField;
    QrVSMovItsStqCenLoc: TLargeintField;
    QrVSMovItsItemNFe: TLargeintField;
    QrVSMovItsLnkNivXtr1: TLargeintField;
    QrVSMovItsLnkNivXtr2: TLargeintField;
    Label54: TLabel;
    DBEdit53: TDBEdit;
    QrVSMovItsClientMO: TLargeintField;
    QrVSMovItsCustoPQ: TFloatField;
    QrVSMovItsVSMulFrnCab: TLargeintField;
    QrVSMovItsNFeSer: TLargeintField;
    QrVSMovItsNFeNum: TLargeintField;
    QrVSMovItsVSMulNFeCab: TLargeintField;
    QrVSMovItsJmpMovID: TLargeintField;
    QrVSMovItsJmpNivel1: TLargeintField;
    QrVSMovItsJmpNivel2: TLargeintField;
    QrVSMovItsRmsMovID: TLargeintField;
    QrVSMovItsRmsNivel1: TLargeintField;
    QrVSMovItsRmsNivel2: TLargeintField;
    QrVSMovItsGGXRcl: TLargeintField;
    QrVSMovItsRmsGGX: TLargeintField;
    QrVSMovItsJmpGGX: TLargeintField;
    QrVSMovItsDtCorrApo: TDateTimeField;
    DBEdit54: TDBEdit;
    Label55: TLabel;
    Label56: TLabel;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    Label57: TLabel;
    DBEdit57: TDBEdit;
    Label58: TLabel;
    DBEdit58: TDBEdit;
    Panel18: TPanel;
    Label59: TLabel;
    DBEdit59: TDBEdit;
    Label60: TLabel;
    DBEdit60: TDBEdit;
    Label61: TLabel;
    DBEdit61: TDBEdit;
    Label62: TLabel;
    DBEdit62: TDBEdit;
    Label63: TLabel;
    DBEdit63: TDBEdit;
    Label65: TLabel;
    DBEdit65: TDBEdit;
    Label64: TLabel;
    DBEdit64: TDBEdit;
    Label66: TLabel;
    DBEdit66: TDBEdit;
    Label67: TLabel;
    DBEdit67: TDBEdit;
    Label68: TLabel;
    DBEdit68: TDBEdit;
    Label69: TLabel;
    DBEdit69: TDBEdit;
    Recriarbaixa1: TMenuItem;
    QrVSMovSrcCustoM2: TFloatField;
    QrVSMovDstCustoM2: TFloatField;
    QrVSMovSrcCustoPQ: TFloatField;
    QrVSMovDstCustoPQ: TFloatField;
    QrVSMovItsIxxMovIX: TLargeintField;
    QrVSMovItsIxxFolha: TLargeintField;
    QrVSMovItsIxxLinha: TLargeintField;
    QrVSMovItsCusFrtAvuls: TFloatField;
    QrVSMovItsCusFrtMOEnv: TFloatField;
    QrVSMovItsCusFrtMORet: TFloatField;
    QrVSMovItsCustoMOPc: TFloatField;
    QrVSMovItsGSPInnNiv2: TLargeintField;
    QrVSMovItsGSPArtNiv2: TLargeintField;
    QrVSMovItsGSPSrcMovID: TLargeintField;
    QrVSMovItsGSPSrcNiv2: TLargeintField;
    QrVSMovItsGSPJmpMovID: TLargeintField;
    QrVSMovItsGSPJmpNiv2: TLargeintField;
    Label70: TLabel;
    DBEdit70: TDBEdit;
    Label71: TLabel;
    DBEdit71: TDBEdit;
    Label72: TLabel;
    DBEdit72: TDBEdit;
    Label73: TLabel;
    DBEdit73: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSMovItsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSMovItsBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSMovItsAfterScroll(DataSet: TDataSet);
    procedure QrVSMovItsBeforeClose(DataSet: TDataSet);
    procedure QrVSMovItsCalcFields(DataSet: TDataSet);
    procedure BtIMEIClick(Sender: TObject);
    procedure ExcluiIMEI1Click(Sender: TObject);
    procedure Recalculasaldos1Click(Sender: TObject);
    procedure AlteradadosdoartigodoIMEI1Click(Sender: TObject);
    procedure RecalculaReduzidodeorigem1Click(Sender: TObject);
    procedure IrparagerenciamentodoMovimento1Click(Sender: TObject);
    procedure IrparaajaneladoMovimento1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure IrparajaneladaFichaRMP1Click(Sender: TObject);
    procedure Irparajaneladopallet1Click(Sender: TObject);
    procedure rvoredoMovimento1Click(Sender: TObject);
    procedure AlteradadosdoitemdeIMEI1Click(Sender: TObject);
    procedure PackingListVertical1Click(Sender: TObject);
    procedure PackingListHorizontal1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Classesgeradas1Click(Sender: TObject);
    procedure PMIMEIPopup(Sender: TObject);
    procedure Recriarbaixa1Click(Sender: TObject);
    procedure DBEdSrcNivel2DblClick(Sender: TObject);
    procedure DBEdDstNivel2DblClick(Sender: TObject);
    procedure DGMovSrcDblClick(Sender: TObject);
    procedure DGMovDstDblClick(Sender: TObject);
  private
    FAtualizando: Boolean;
    FVSVMIClaSdo: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormPorIMEI(IMEI: Integer);
    procedure MostraFormPorPallet(Pallet: Integer);
    procedure ImprimePackList(Vertical: Boolean);
    function  IndiceIDNiv(): Integer;
    procedure CriaBaixaDeCaleado();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenVSMovSrc(Controle: Integer);
    procedure ReopenVSMovDst(Controle: Integer);
    //procedure ReopenVSMovXXX(Qry: TmySQLQuery; Campo: String; IMEI, CtrlLoc:
    //          Integer);
  end;

var
  FmVSMovIts: TFmVSMovIts;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnVS_CRC_PF, CreateVS,
  {$IFDEF sAllVS} UnVS_PF, {$ENDIF}
  ModuleGeral, UnProjGroup_Vars, UnGrl_Geral, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSMovIts.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSMovIts.MenuItem1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrVSMovItsDstNivel2.Value);
end;

procedure TFmVSMovIts.MenuItem2Click(Sender: TObject);
begin
  MostraFormPorPallet(QrVSMovItsControle.Value);
end;

procedure TFmVSMovIts.MostraFormPorIMEI(IMEI: Integer);
begin
  if (QrVSMovIts.State <> dsInactive) and (QrVSMovIts.RecordCount > 0) then
  begin
    VS_CRC_PF.MostraFormVS_Do_IMEI(IMEI);
    DefParams();
  end;
end;

procedure TFmVSMovIts.MostraFormPorPallet(Pallet: Integer);
begin
  if (QrVSMovIts.State <> dsInactive) and (QrVSMovIts.RecordCount > 0) then
    VS_CRC_PF.MostraFormVSPallet(Pallet);
end;

procedure TFmVSMovIts.PackingListHorizontal1Click(Sender: TObject);
begin
  ImprimePackList(True);
end;

procedure TFmVSMovIts.PackingListVertical1Click(Sender: TObject);
begin
  ImprimePackList(False);
end;

procedure TFmVSMovIts.PMIMEIPopup(Sender: TObject);
begin
  Recriarbaixa1.Enabled := IndiceIDNiv() = 29031;
end;

procedure TFmVSMovIts.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSMovItsControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSMovIts.DefParams;
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  VAR_GOTOTABELA := CO_TAB_TAB_VMI;
  VAR_GOTOMYSQLTABLE := QrVSMovIts;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'Controle';
  VAR_GOTONOME := 'Observ';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
(*
  VAR_SQLx.Add('SELECT vmi.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome, ');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ');
  VAR_SQLx.Add('NO_PRD_TAM_COR, ');
  VAR_SQLx.Add('CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ');
  VAR_SQLx.Add('FROM v s m o v i t s vmi ');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ');
  VAR_SQLx.Add('LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  VAR_SQLx.Add('LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ');
  VAR_SQLx.Add('LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ');
  VAR_SQLx.Add('WHERE vmi.Controle > 0');
*)
  //VS_CRC_PF.ReopenVSOpeOriIMEI(QrVSOpeOriIMEI, QrVSOpeCabMovimCod.Value, Controle);
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  //'CAST(vmi.Marca AS CHAR) Marca,',
  'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,',
  'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, ',

  'CAST(vmi.GSPInnNiv2 AS SIGNED) GSPInnNiv2, ',
  'CAST(vmi.GSPArtNiv2 AS SIGNED) GSPArtNiv2, ',
  'CAST(vmi.GSPSrcMovID AS SIGNED) GSPSrcMovID, ',
  'CAST(vmi.GSPSrcNiv2 AS SIGNED) GSPSrcNiv2, ',
  'CAST(vmi.GSPJmpMovID AS SIGNED) GSPJmpMovID, ',
  'CAST(vmi.GSPJmpNiv2 AS SIGNED) GSPJmpNiv2, ',

  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
  'CAST(vsp.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Controle > 0' ,
  '']);
  SQL_Group := '';
  VAR_SQLx.Add(Geral.ATS([
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']));
  //
  VAR_SQL1.Add('AND vmi.Controle=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vmi.Observ Like :P0');
  //
end;

procedure TFmVSMovIts.DGMovDstDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGMovDst.Columns[THackDBGrid(DGMovDst).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      GOTOy.LC(QrVSMovItsControle.Value, QrVSMovDstControle.Value)
end;

procedure TFmVSMovIts.DGMovSrcDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGMovSrc.Columns[THackDBGrid(DGMovSrc).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      GOTOy.LC(QrVSMovItsControle.Value, QrVSMovSrcControle.Value)
end;

procedure TFmVSMovIts.ExcluiIMEI1Click(Sender: TObject);
var
  Controle, SrcNivel2, DstNivel2: Integer;
begin
  Controle := QrVSMovItsControle.Value;
  SrcNivel2 := QrVSMovItsSrcNivel2.Value;
  DstNivel2 := QrVSMovItsDstNivel2.Value;
  //
  if (QrVSMovSrc.RecordCount > 1 )
  or (QrVSMovDst.RecordCount > 1 ) then
  begin
    Geral.MB_Aviso('O IME-I ' + Geral.FF0(Controle) +
    ' n�o pode ser exclu�do pois tem atrelamentos com um ou mais outros IME-Is!');
    Exit;
  end;
  if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
  Integer(TEstqMotivDel.emtdWetCurti045),
  //SrcNivel2, DstNivel2,
  Dmod.QrUpd, Dmod.MyDB, EdSenha.Text) then
  begin
    LocCod(Controle, Controle);
  end;
end;

procedure TFmVSMovIts.Classesgeradas1Click(Sender: TObject);
var
  IMEIs, Mortos: array of Integer;
begin
  SetLength(IMEIs, 1);
  IMEIs[0] := QrVSMovItsControle.Value;
  if QrVSMovItsID_TTW.Value = 1 then
  begin
    SetLength(Mortos, 1);
    Mortos[0] := 1;
  end else
    SetLength(Mortos, 0);
  VS_CRC_PF.ImprimeClassIMEIs(IMEIs, Mortos, True);
end;

procedure TFmVSMovIts.CriaBaixaDeCaleado();
var
  ResVar: Variant;
  Controle, MovimID, MovimNiv, SrcNivel1, SrcNivel2, CtrlExiste, QtdReg1: Integer;
  Pecas, AreaM2, AreaP2, PesoKg: Double;
  CamposZ: String;
  IsOk: Boolean;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  ResVar := QrVSMovItsControle.Value + 1;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  ResVar, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    Controle := Geral.IMV(ResVar);
    CtrlExiste := Dmod.MyDB.SelectInteger(
    'SELECT Controle FROM ' + CO_SEL_TAB_VMI + ' WHERE Controle=' + Geral.FF0(Controle), IsOk,
    'Controle');
    if IsOk and (CtrlExiste = Controle) then
      Geral.MB_Info('O IME-I ' + Geral.FF0(Controle) +
      ' j� existe e n�o pode ser criado!')
    else
    begin
       MovimID  := QrVSMovItsMovimID.Value;
      MovimNiv := QrVSMovItsMovimNiv.Value;
      SrcNivel1 := QrVSMovItsSrcNivel1.Value;
      SrcNivel2 := QrVSMovItsSrcNivel2.Value;
      Pecas := QrVSMovItsPecas.Value;
      AreaM2 := QrVSMovItsAreaM2.Value;
      AreaP2 := QrVSMovItsAreaP2.Value;
      PesoKg := QrVSMovItsPesoKg.Value;
      //
      CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, CO_SEL_TAB_VMI, '', QtdReg1);
      CamposZ := StringReplace(CamposZ, ' Controle',
        ' ' + Geral.FF0(Controle) + ' Controle', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' MovimNiv',
        ' ' + Geral.FF0(Integer(eminEmCalBxa)) + ' MovimNiv', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' SrcNivel1',
        ' ' + Geral.FF0(SrcNivel1) + ' SrcNivel1', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' SrcNivel2',
        ' ' + Geral.FF0(SrcNivel2) + ' SrcNivel2', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' Pecas',
        ' ' + Geral.FFT_Dot(-Pecas, 3, sinegativo) + ' Pecas', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' AreaM2',
        ' ' + Geral.FFT_Dot(-AreaM2, 3, sinegativo) + ' AreaM2', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' AreaP2',
        ' ' + Geral.FFT_Dot(-AreaP2, 3, sinegativo) + ' AreaP2', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' PesoKg',
        ' ' + Geral.FFT_Dot(-PesoKg, 3, sinegativo) + ' PesoKg', [rfReplaceAll]);
      //
      CamposZ := 'INSERT INTO ' + CO_SEL_TAB_VMI + ' SELECT ' + sLineBreak +
      CamposZ + sLineBreak +
      ' FROM ' + CO_SEL_TAB_VMI + ' '+ sLineBreak +
      'WHERE Controle=' + Geral.FF0(QrVSMovItsControle.Value);
      //
      //Geral.MB_Info(CamposZ);
      DModG.MyCompressDB.Execute(CamposZ);
      LocCod(QrVSMovItsControle.Value, Controle);
    end;
  end;
end;

procedure TFmVSMovIts.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSMovIts.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSMovIts.RecalculaReduzidodeorigem1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle, GraGruX: Integer;
begin
(*
  Qry := TmySQLQuery.Create(Dmod);
  try
    Controle := QrVSMovItsControle.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Controle, vmi.GraGruX ',
    'FROM v s m o v i t s vmi ',
    'WHERE vmi.Controle = ( ',
    '  SELECT SrcNivel2   ',
    '  FROM v s m o v i t s vm2 ',
    '  WHERE Controle=' + Geral.FF0(Controle),
    ') ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Qry.FieldByName('Controle').AsInteger <> 0 then
      begin
        GraGruX := Qry.FieldByName('GraGruX').AsInteger;
        if (GraGruX <> 0) and (GraGruX <> QrVSMovItsGraGruX.Value) then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovitz', False, [
          'GraGruX'], ['Controle'], [
          GraGruX], [Controle], True) then
            LocCod(Controle, Controle);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmVSMovIts.Recalculasaldos1Click(Sender: TObject);
const
  Gera = False;
var
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  Controle: Integer;
begin
  // ver se tem classificados sem dar baixa!
// 2016-01-11
  //VS_CRC_PF.AtualizaSaldoIMEI(QrVSMovItsControle.Value, Gera);
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrVSMovItsControle.Value,
  QrVSMovItsMovimID.Value, QrVSMovItsMovimNiv.Value);
// FIM 2016-01-11
  LocCod(QrVSMovItsControle.Value, QrVSMovItsControle.Value);
end;

procedure TFmVSMovIts.Recriarbaixa1Click(Sender: TObject);
const
  sProcName = 'TFmVSMovIts.Recriarbaixa1Click()';
begin
  case IndiceIDNiv() of
    29031: CriaBaixaDeCaleado();
    else Geral.MB_ERRO('"ID+Niv" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmVSMovIts.ReopenVSMovDst(Controle: Integer);
const
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  // O campo deve ser SrcNivel2 pois a query tem os detinos deste Controle
  VS_CRC_PF.ReopenVSMovXXX(QrVSMovDst, 'SrcNivel2', QrVSMovItsControle.Value, TemIMEIMrt, Controle);
end;

procedure TFmVSMovIts.ReopenVSMovSrc(Controle: Integer);
const
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  // O campo deve ser DstNivel2 pois a query tem as origens deste Controle
  VS_CRC_PF.ReopenVSMovXXX(QrVSMovSrc, 'DstNivel2', QrVSMovItsControle.Value, TemIMEIMrt, Controle);
end;

procedure TFmVSMovIts.rvoredoMovimento1Click(Sender: TObject);
begin
  {$IfDef sAllVS}
  VS_PF.MostraFormVSArvoreArtigos(QrVSMovItsControle.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSMovIts.DBEdDstNivel2DblClick(Sender: TObject);
begin
  if QrVSMovItsDstNivel2.Value <> 0 then
    GOTOy.LC(QrVSMovItsControle.Value, QrVSMovItsDstNivel2.Value)
end;

procedure TFmVSMovIts.DBEdSrcNivel2DblClick(Sender: TObject);
begin
  if QrVSMovItsSrcNivel2.Value <> 0 then
    GOTOy.LC(QrVSMovItsControle.Value, QrVSMovItsSrcNivel2.Value)
end;

procedure TFmVSMovIts.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSMovIts.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSMovIts.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSMovIts.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSMovIts.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSMovIts.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovIts.AlteradadosdoartigodoIMEI1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrVSMovItsControle.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbDadosArtigo]);
  LocCod(Controle, Controle);
end;

procedure TFmVSMovIts.AlteradadosdoitemdeIMEI1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrVSMovItsControle.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  LocCod(Controle, Controle);
end;

procedure TFmVSMovIts.BtIMEIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIMEI, BtIMEI);
end;

procedure TFmVSMovIts.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSMovItsControle.Value;
  Close;
end;

procedure TFmVSMovIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBMovDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSMovIts.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSMovItsControle.Value, LaRegistro.Caption);
end;

procedure TFmVSMovIts.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSMovIts.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSMovIts.SbNovoClick(Sender: TObject);
const
  Codigo = 0;
  Ativo  = 1;
var
  QryAll, QryUni: TmySQLQuery;
  Controle, MovimID, MovimNiv, Status, IMEI: Integer;
  DataEmis: String;
  PecasAnt, PesoKgAnt, AreaM2Ant, SdoVrtPecaAnt, SdoVrtPesoAnt, SdoVrtArM2Ant,
  PecasDep, PesoKgDep, AreaM2Dep, SdoVrtPecaDep, SdoVrtPesoDep, SdoVrtArM2Dep:
  Double;
  Agora: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta(
  'Confirma a atualiza��o dos saldos dos IME-Is ativos?') <> ID_YES then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    Agora := DModG.ObtemAgora;
    DataEmis := Geral.FDT(Agora, 109);
    VAR_NOME_TAB_VERIF_IMEI :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSVerifIMEI, DModG.QrUpdPID1,
      False, 0, '_VSVerifIMEI_' + FormatDateTime('yymmddhhmmss', Agora));
    QryAll := TmySQLQuery.Create(Dmod);
    QryUni := TmySQLQuery.Create(Dmod);
    try
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryAll, Dmod.MyDB, [
      'SELECT *  ',
      'FROM ' + CO_SEL_TAB_VMI,
      'ORDER BY Controle ',
      '']);
      PB1.Position := 0;
      PB1.Max := QryAll.RecordCount;
      QryAll.First;
      while not QryAll.Eof do
      begin
        Controle := QryAll.FieldByName('Controle').AsInteger;
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Verificando IMEI ' + Geral.FF0(Controle));
        Application.ProcessMessages;
        MovimID        := QryAll.FieldByName('MovimID').AsInteger;
        MovimNiv       := QryAll.FieldByName('MovimNiv').AsInteger;
        PecasAnt       := QryAll.FieldByName('Pecas').AsFloat;
        PesoKgAnt      := QryAll.FieldByName('PesoKg').AsFloat;
        AreaM2Ant      := QryAll.FieldByName('AreaM2').AsFloat;
        SdoVrtPecaAnt  := QryAll.FieldByName('SdoVrtPeca').AsFloat;
        SdoVrtPesoAnt  := QryAll.FieldByName('SdoVrtPeso').AsFloat;
        SdoVrtArM2Ant  := QryAll.FieldByName('SdoVrtArM2').AsFloat;
        //
        VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
        UnDmkDAC_PF.AbreMySQLQuery0(QryUni, Dmod.MyDB, [
        'SELECT *  ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        PecasDep       := QryUni.FieldByName('Pecas').AsFloat;
        PesoKgDep      := QryUni.FieldByName('PesoKg').AsFloat;
        AreaM2Dep      := QryUni.FieldByName('AreaM2').AsFloat;
        SdoVrtPecaDep  := QryUni.FieldByName('SdoVrtPeca').AsFloat;
        SdoVrtPesoDep  := QryUni.FieldByName('SdoVrtPeso').AsFloat;
        SdoVrtArM2Dep  := QryUni.FieldByName('SdoVrtArM2').AsFloat;
        Status := 0;
        if PecasAnt       <> PecasDep      then Status := Status + 1;
        if PesoKgAnt      <> PesoKgDep     then Status := Status + 2;
        if AreaM2Ant      <> AreaM2Dep     then Status := Status + 4;
        if SdoVrtPecaAnt  <> SdoVrtPecaDep then Status := Status + 8;
        if SdoVrtPesoAnt  <> SdoVrtPesoDep then Status := Status + 16;
        if SdoVrtArM2Ant  <> SdoVrtArM2Dep then Status := Status + 32;
        //
        if Status > 0 then
        begin
          IMEI := Controle;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, VAR_NOME_TAB_VERIF_IMEI,False, [
          'Codigo', 'DataEmis', 'IMEI',
          'Status', 'Ativo'], [
          ], [
          Codigo, DataEmis, IMEI,
          Status, Ativo], [
          ], False);
        end;
        //
        QryAll.Next;
      end;
      Geral.MB_Info('Verifica��o finalizada!');
      PB1.Position := 0;
      PB1.Max := 0;
    finally
      QryUni.Free;
    end;
    finally
      QryAll.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSMovIts.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSMovIts.QrVSMovItsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //Geral.MB_Teste(QrVSMovIts.SQL.Text);
end;

procedure TFmVSMovIts.QrVSMovItsAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMovSrc(0);
  ReopenVSMovDst(0);
end;

procedure TFmVSMovIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
(*
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSMovItsControle??.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
*)
end;

procedure TFmVSMovIts.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSMovItsControle.Value,
  CuringaLoc.CriaForm('Controle', CO_NOME, CO_SEL_TAB_VMI, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSMovIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovIts.ImprimePackList(Vertical: Boolean);
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
begin
  SetLength(MyArr, 1);
  MyArr[0] := QrVSMovItsControle.Value;
  VS_CRC_PF.ImprimePackListsIMEIs(Empresa, MyArr, Vertical)
end;

function TFmVSMovIts.IndiceIDNiv(): Integer;
begin
  Result := VS_PF.GetIDNiv(QrVSMovItsMovimID.Value, QrVSMovItsMovimNiv.Value);
end;

procedure TFmVSMovIts.IrparaajaneladoMovimento1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrVSMovItsSrcNivel2.Value);
end;

procedure TFmVSMovIts.IrparagerenciamentodoMovimento1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrVSMovItsControle.Value);
end;

procedure TFmVSMovIts.IrparajaneladaFichaRMP1Click(Sender: TObject);
begin
  if (QrVSMovIts.State <> dsInactive) and (QrVSMovIts.RecordCount > 0) then
  {$IFDEF sAllVS}
    VS_PF.MostraFormVSFchGerCab(QrVSMovItsSerieFch.Value, QrVSMovItsFicha.Value);
  {$ELSE}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$ENDIF}
end;

procedure TFmVSMovIts.Irparajaneladopallet1Click(Sender: TObject);
begin
  if QrVSMovItsPallet.Value <> 0 then
    VS_CRC_PF.MostraFormVSPallet(QrVSMovItsPallet.Value)
  else
    Geral.MB_Aviso('Este IME-I n�o pertence a nenhum pallet!');
end;

procedure TFmVSMovIts.QrVSMovItsBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMovSrc.Close;
  QrVSMovDst.Close;
end;

procedure TFmVSMovIts.QrVSMovItsBeforeOpen(DataSet: TDataSet);
begin
  QrVSMovItsControle.DisplayFormat := FFormatFloat;
end;

procedure TFmVSMovIts.QrVSMovItsCalcFields(DataSet: TDataSet);
begin
  QrVSMovItsNO_EstqMovimID.Value := sEstqMovimID[QrVSMovItsMovimID.Value];
  QrVSMovItsNO_SrcMovID.Value    := sEstqMovimID[QrVSMovItsSrcMovID.Value];
  QrVSMovItsNO_DstMovID.Value    := sEstqMovimID[QrVSMovItsDstMovID.Value];
end;

end.

