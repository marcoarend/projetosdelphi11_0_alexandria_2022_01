unit VSRtbIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSRtbIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label4: TLabel;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    QrVSPallet: TmySQLQuery;
    DsVSPallet: TDataSource;
    QrVSPalletCodigo: TIntegerField;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdMovimCod: TdmkDBEdit;
    Label3: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Panel5: TPanel;
    EdValorT: TdmkEdit;
    Label7: TLabel;
    Label10: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    Label8: TLabel;
    EdValorMP: TdmkEdit;
    Label12: TLabel;
    EdCustoMOKg: TdmkEdit;
    EdCustoMOTot: TdmkEdit;
    Label13: TLabel;
    EdFornecMO: TdmkEditCB;
    Label14: TLabel;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdFicha: TdmkEdit;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label9: TLabel;
    EdMarca: TdmkEdit;
    EdObserv: TdmkEdit;
    Label16: TLabel;
    SBNewPallet: TSpeedButton;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    EdCustoMOM2: TdmkEdit;
    Label17: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    QrGraGruXCouNiv2: TFloatField;
    QrGraGruXGrandeza: TFloatField;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenVSPallet();
    procedure ReopenVSRtbIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO: Integer;
  end;

  var
  FmVSRtbIts: TFmVSRtbIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSRtbCab, Principal, UnVS_PF, ModuleGeral, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmVSRtbIts.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  SrcGGX     = 0;
  DstGGX     = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  EdFichaRMP = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0;
  JmpNivel2: Integer = 0;
  JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0;
  RmsNivel2: Integer = 0;
  RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste;
  GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, ClientMO, GraGruX, Terceiro,
  Ficha, GraGruY, FornecMO, SerieFch, StqCenLoc, ReqMovEstq: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdPalletX, EdAreaM2X, EdPecasX: TdmkEdit;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := FClientMO;
  Terceiro       := EdFornecedor.ValueVariant;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidRetrabalho;
  MovimNiv       := eminSemNiv;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Ficha          := EdFicha.ValueVariant;
  GraGruY        := QrGraGruXGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  SerieFch       := EdSerieFch.ValueVariant;
  Marca          := EdMarca.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  if ((QrGraGruXGrandeza.Value) =  Integer(gumPesoKg))
  and (QrGraGruXCouNiv2.Value = Integer(cn2SubPrd))then
  begin
    EdPalletX := nil;
    EdAreaM2X := nil;
    EdPecasX  := nil
  end else
  begin
    EdPecasX := EdPecas;
    case GraGruY of
      CO_GraGruY_0512_VSSubPrd,
      CO_GraGruY_0683_VSPSPPro,
      CO_GraGruY_0853_VSPSPEnd,
      CO_GraGruY_1024_VSNatCad,
      CO_GraGruY_1072_VSNatInC,
      CO_GraGruY_1088_VSNatCon:
      begin
        EdPalletX := nil;
        EdAreaM2X := nil;
      end;
      CO_GraGruY_2048_VSRibCad:
      begin
        EdPalletX := EdPallet;
        EdAreaM2X := EdPallet;
      end;
      CO_GraGruY_3072_VSRibCla:
      begin
        EdPalletX := EdPallet;
        EdAreaM2X := EdAreaM2;
      end;
      CO_GraGruY_4096_VSRibOpe,
      CO_GraGruY_5120_VSWetEnd,
      CO_GraGruY_6144_VSFinCla:
      begin
        EdPalletX := nil;
        EdAreaM2X := EdAreaM2;
      end;
      else
      begin
        Geral.MB_Erro('"GraGruY" n�o implementado em "FmVSRtbIts.BtOKClick()"');
      end;
    end;
  end;
  //
  if Pallet <> 0 then
    if MyObjects.FIC(GraGruX <> QrVSPalletGraGruX.Value, EdPallet,
    'Artigo do pallet selecionado difere do artigo selecionado!') then
      Exit;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  if VS_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha) then
    Exit;
  //
  if VS_PF.VSFic(GraGruX, Empresa, ClientMO, Terceiro, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPalletX, EdFichaRMP, EdPecasX,
    EdAreaM2X, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaOuPeca,
    EdStqCenLoc)
  then
    Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei086(*Entrada por devolu��o de cliente para retrabalho*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsrtbcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    FmVSRtbCab.AtualizaNFeItens();
    //
    FmVSRtbCab.LocCod(Codigo, Codigo);
    FmVSRtbCab.ReopenVSRtbIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      //EdFornecedor.ValueVariant  := 0;
      //CBFornecedor.KeyValue      := Null;
      EdSerieFch.ValueVariant    := 0;
      CBSerieFch.KeyValue        := Null;
      EdFicha.ValueVariant       := 0;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      EdValorT.ValueVariant      := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdCustoMOKg.ValueVariant   := 0;
      EdFornecMO.ValueVariant    := 0;
      CBFornecMO.KeyValue        := Null;
      EdObserv.Text              := '';
      //
      EdValorT.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOTot.ValueVariant  := 0;
      //
      SBNewPallet.Enabled := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSRtbIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRtbIts.CalculaCustos();
var
  ValorMP, CustoMOKg, CustoMOM2, CustoMOTot, ValorT, PesoKg, AreaM2: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOKg    := EdCustoMOKg.ValueVariant;
  PesoKg       := EdPesoKg.ValueVariant;
  CustoMOM2    := EdCustoMOM2.ValueVariant;
  AreaM2       := EdAreaM2.ValueVariant;
  //
  CustoMOTot := (CustoMOKg * PesoKg) + (CustoMOM2 * AreaM2);
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSRtbIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSRtbIts.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSRtbIts.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ficha: Integer;
begin
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
end;

procedure TFmVSRtbIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSRtbIts.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSRtbIts.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSRtbIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSRtbIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_Rtb_VS + ') ');
  ReopenVSPallet();
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  VS_PF.AbreVSSerFch(QrVSSerFch);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSRtbIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRtbIts.ReopenVSRtbIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSRtbIts.ReopenVSPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM vspalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;

procedure TFmVSRtbIts.SBNewPalletClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    VS_PF.AtualizaStatPall(Codigo);
    ReopenVSPallet();
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
  end;
end;

procedure TFmVSRtbIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSRtbIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
