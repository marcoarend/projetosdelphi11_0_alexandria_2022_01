object FmVSCorrigeCusReclas: TFmVSCorrigeCusReclas
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Corre'#231#227'o de Custo de Reclassifica'#231#245'es'
  ClientHeight = 493
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 741
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 693
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 645
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Corre'#231#227'o de Custo de Reclassifica'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Corre'#231#227'o de Custo de Reclassifica'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Corre'#231#227'o de Custo de Reclassifica'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 360
    Width = 741
    Height = 63
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 737
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 29
        Width = 737
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 423
    Width = 741
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 595
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 741
    Height = 312
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 236
      Width = 741
      Height = 76
      Align = alBottom
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 741
      Height = 236
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 741
        Height = 236
        Align = alClient
        Caption = 'Custo m'#237'nimo aceit'#225'vel:'
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 737
          Height = 46
          Align = alTop
          TabOrder = 0
          object SbCustoMinimo: TSpeedButton
            Left = 68
            Top = 20
            Width = 23
            Height = 22
            Caption = '>'
            OnClick = SbCustoMinimoClick
          end
          object Label2: TLabel
            Left = 8
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Reduzido:'
          end
          object Label1: TLabel
            Left = 96
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Custo m'#237'nimo:'
          end
          object Label3: TLabel
            Left = 204
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Custo Default:'
          end
          object EdCusMinPsq: TdmkEdit
            Left = 96
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdGGXCusMin: TdmkEdit
            Left = 8
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '43'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 43
            ValWarn = False
          end
          object EdCusDefPsq: TdmkEdit
            Left = 208
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object BtCorrigeSohVVPalPreCab: TBitBtn
            Left = 316
            Top = 18
            Width = 273
            Height = 25
            Caption = 'BtCorrigeSohVVPalPreCab'
            TabOrder = 3
            Visible = False
            OnClick = BtCorrigeSohVVPalPreCabClick
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 61
          Width = 737
          Height = 173
          Align = alClient
          DataSource = DsCusMin
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 36
    Top = 219
  end
  object QrVSPrePalCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM vsprepalcab')
    Left = 36
    Top = 120
    object QrVSPrePalCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPrePalCabControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPrePalCab: TDataSource
    DataSet = QrVSPrePalCab
    Left = 36
    Top = 168
  end
  object QrSum11: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 204
    Top = 120
    object QrSum11ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrCusMin: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '  SELECT Controle, GraGruX, Pecas, AreaM2, ValorT, '
      '  ValorT/AreaM2 CustoM2 '
      ' FROM vsmovits '
      ' WHERE MovimID=6 '
      '  AND MovimNiv=13 ')
    Left = 132
    Top = 120
    object QrCusMinControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCusMinGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrCusMinPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrCusMinAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrCusMinValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrCusMinCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
  end
  object DsCusMin: TDataSource
    DataSet = QrCusMin
    Left = 132
    Top = 172
  end
  object QrPPCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM vsprepalcab')
    Left = 612
    Top = 8
    object QrPPCabCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
    end
    object QrPPCabControle_1: TIntegerField
      FieldName = 'Controle_1'
    end
    object QrPPCabMovimNiv_1: TIntegerField
      FieldName = 'MovimNiv_1'
    end
  end
  object QrPPSrc: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT crs.*, vmi.Codigo Codigo_A, '
      'vmi.Controle Controle_A, vmi.MovimID MovimID_A,'
      'vmi.AreaM2 AreaM2_A, vmi.ValorT ValorT_A, '
      'vmi.ValorT/vmi.AreaM2 CustoM2_A'
      'FROM _fix_recl_src_2304051241'
      
        'LEFT JOIN bluederm_cialeather.vsmovits vmi ON vmi.Controle=crs.S' +
        'rcNivel2_1'
      'WHERE crs.Codigo_1=28')
    Left = 612
    Top = 60
    object QrPPSrcCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
      Required = True
    end
    object QrPPSrcControle_1: TIntegerField
      FieldName = 'Controle_1'
      Required = True
    end
    object QrPPSrcSrcNivel2_1: TIntegerField
      FieldName = 'SrcNivel2_1'
      Required = True
    end
    object QrPPSrcAreaM2_1: TFloatField
      FieldName = 'AreaM2_1'
      Required = True
    end
    object QrPPSrcValorT_1: TFloatField
      FieldName = 'ValorT_1'
      Required = True
    end
    object QrPPSrcCustoM2_1: TFloatField
      FieldName = 'CustoM2_1'
    end
    object QrPPSrcCodigo_A: TIntegerField
      FieldName = 'Codigo_A'
    end
    object QrPPSrcControle_A: TIntegerField
      FieldName = 'Controle_A'
    end
    object QrPPSrcMovimID_A: TIntegerField
      FieldName = 'MovimID_A'
    end
    object QrPPSrcAreaM2_A: TFloatField
      FieldName = 'AreaM2_A'
    end
    object QrPPSrcValorT_A: TFloatField
      FieldName = 'ValorT_A'
    end
    object QrPPSrcCustoM2_A: TFloatField
      FieldName = 'CustoM2_A'
    end
  end
  object QrPPDst: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT crs.*, vmi.Codigo Codigo_A, '
      'vmi.Controle Controle_A, vmi.MovimID MovimID_A,'
      'vmi.AreaM2 AreaM2_A, vmi.ValorT ValorT_A, '
      'vmi.ValorT/vmi.AreaM2 CustoM2_A'
      'FROM _fix_recl_src_2304051241'
      
        'LEFT JOIN bluederm_cialeather.vsmovits vmi ON vmi.Controle=crs.S' +
        'rcNivel2_1'
      'WHERE crs.Codigo_1=28')
    Left = 612
    Top = 116
    object QrPPDstControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCCDst: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT crs.*, vmi.Codigo Codigo_A, '
      'vmi.Controle Controle_A, vmi.MovimID MovimID_A,'
      'vmi.AreaM2 AreaM2_A, vmi.ValorT ValorT_A, '
      'vmi.ValorT/vmi.AreaM2 CustoM2_A'
      'FROM _fix_recl_src_2304051241'
      
        'LEFT JOIN bluederm_cialeather.vsmovits vmi ON vmi.Controle=crs.S' +
        'rcNivel2_1'
      'WHERE crs.Codigo_1=28')
    Left = 612
    Top = 168
    object QrCCDstVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object QrVenda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTs _fix_venda_vlr;'
      'CREATE TABLE _fix_venda_vlr'
      'SELECT Controle Controle_V, SrcNivel2 SrcNivel2_V, '
      'AreaM2 AreaM2_V, ValorT ValorT_V, '
      'ValorT / AreaM2 CustoM2_V'
      'FROM bluederm.vsmovits'
      'WHERE MovimID=2'
      'AND AreaM2 < 0;'
      'SELECT fvv.*, vmi.AreaM2 AreaM2_O, '
      'vmi.ValorT ValorV_O, '
      'vmi.ValorT / vmi.AreaM2 CustoM2_O '
      'FROM _fix_venda_vlr fvv'
      'LEFT JOIN bluederm.vsmovits vmi ON vmi.Controle=fvv.SrcNivel2_V;')
    Left = 612
    Top = 220
    object QrVendaControle_V: TIntegerField
      FieldName = 'Controle_V'
      Required = True
    end
    object QrVendaSrcNivel2_V: TIntegerField
      FieldName = 'SrcNivel2_V'
      Required = True
    end
    object QrVendaAreaM2_V: TFloatField
      FieldName = 'AreaM2_V'
      Required = True
    end
    object QrVendaValorT_V: TFloatField
      FieldName = 'ValorT_V'
      Required = True
    end
    object QrVendaCustoM2_V: TFloatField
      FieldName = 'CustoM2_V'
    end
    object QrVendaAreaM2_O: TFloatField
      FieldName = 'AreaM2_O'
    end
    object QrVendaValorV_O: TFloatField
      FieldName = 'ValorV_O'
    end
    object QrVendaCustoM2_O: TFloatField
      FieldName = 'CustoM2_O'
    end
  end
end
