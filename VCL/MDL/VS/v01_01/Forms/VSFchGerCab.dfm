object FmVSFchGerCab: TFmVSFchGerCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-057 :: Gerenciamento de Fichas'
  ClientHeight = 743
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 691
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 628
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 691
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 405
      Top = 278
      Width = 5
      Height = 349
      ExplicitTop = 57
      ExplicitHeight = 234
    end
    object Splitter2: TSplitter
      Left = 737
      Top = 278
      Width = 5
      Height = 349
      ExplicitLeft = 569
      ExplicitTop = 57
      ExplicitHeight = 234
    end
    object DBGIDs: TDBGrid
      Left = 0
      Top = 278
      Width = 405
      Height = 349
      Align = alLeft
      DataSource = DsVSFchGerIDs
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_MovimID'
          Title.Caption = 'Movimento'
          Width = 107
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ITENS'
          Title.Caption = 'Itens'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RefFirst'
          Title.Caption = 'Primeira ocorr'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RefLast'
          Title.Caption = #218'ltima ocorr'#234'ncia'
          Visible = True
        end>
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 299
        Height = 84
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 299
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label2: TLabel
            Left = 140
            Top = 0
            Width = 56
            Height = 13
            Caption = 'Ficha RMP:'
            FocusControl = DBEdit3
          end
          object Label21: TLabel
            Left = 224
            Top = 0
            Width = 39
            Height = 13
            Caption = 'Arquivo:'
            FocusControl = DBEdit29
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 16
            Width = 32
            Height = 21
            DataField = 'SerieFch'
            DataSource = DsLotes
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 36
            Top = 16
            Width = 100
            Height = 21
            DataField = 'NO_SERIEFCH'
            DataSource = DsVSFchGerCab
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 140
            Top = 16
            Width = 80
            Height = 21
            DataField = 'Ficha'
            DataSource = DsLotes
            TabOrder = 2
          end
          object DBEdit29: TDBEdit
            Left = 224
            Top = 16
            Width = 69
            Height = 21
            DataField = 'NO_TTW'
            DataSource = DsLotes
            TabOrder = 3
          end
        end
        object GBAvisos1: TGroupBox
          Left = 0
          Top = 40
          Width = 299
          Height = 44
          Align = alBottom
          Caption = ' Avisos: '
          TabOrder = 1
          object Panel4: TPanel
            Left = 2
            Top = 15
            Width = 295
            Height = 27
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAviso1: TLabel
              Left = 13
              Top = 2
              Width = 12
              Height = 16
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2: TLabel
              Left = 12
              Top = 1
              Width = 12
              Height = 16
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
      object DBGrid2: TDBGrid
        Left = 301
        Top = 15
        Width = 705
        Height = 84
        Align = alClient
        DataSource = DsVSFchGerCab
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Arquivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodID'
            Title.Caption = 'C'#243'digo'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Title.Caption = 'IMEC'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SERIEFCH'
            Title.Caption = 'S'#233'rie'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ITENS'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RefFirst'
            Title.Caption = 'Primeira ocorr'#234'ncia'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RefLast'
            Title.Caption = #218'ltima ocorr'#234'ncia'
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 627
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
        object BtClassesGeradas: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Classes Geradas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtClassesGeradasClick
        end
      end
    end
    object DBGIMEIs: TDBGrid
      Left = 742
      Top = 278
      Width = 266
      Height = 349
      Align = alClient
      DataSource = DsIMEIs
      PopupMenu = PMIMEIs
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Arquivo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Artigo'
          Width = 264
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso Kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtArM2'
          Title.Caption = 'Sdo m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = #218'ltima data/hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcMovID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstMovID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel2'
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 410
      Top = 278
      Width = 327
      Height = 349
      Align = alLeft
      DataSource = DsVSFchGerNivs
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_MovimNiv'
          Title.Caption = 'N'#237'vel do movimento'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end>
    end
    object GBCabecalho: TGroupBox
      Left = 0
      Top = 101
      Width = 1008
      Height = 177
      Align = alTop
      Caption = ' Dados do cabe'#231'alho da entrada In Natura: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 5
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 740
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 592
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Visible = False
      end
      object Label18: TLabel
        Left = 664
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Visible = False
      end
      object Label19: TLabel
        Left = 828
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label14: TLabel
        Left = 916
        Top = 136
        Width = 50
        Height = 13
        Caption = 'Valor total:'
      end
      object Label15: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label20: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 508
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSXxxCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit7: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSXxxCab
        TabOrder = 1
      end
      object DBEdit8: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSXxxCab
        TabOrder = 2
      end
      object DBEdit9: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsVSXxxCab
        TabOrder = 3
      end
      object DBEdit10: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsVSXxxCab
        TabOrder = 4
      end
      object DBEdit11: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsVSXxxCab
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSXxxCab
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsVSXxxCab
        TabOrder = 7
      end
      object DBEdit14: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSXxxCab
        TabOrder = 8
      end
      object DBEdit15: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsVSXxxCab
        TabOrder = 9
      end
      object DBEdit16: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsVSXxxCab
        TabOrder = 10
      end
      object DBEdit17: TDBEdit
        Left = 740
        Top = 152
        Width = 84
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSXxxCab
        TabOrder = 11
      end
      object DBEdit18: TDBEdit
        Left = 828
        Top = 152
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSXxxCab
        TabOrder = 12
      end
      object DBEdit19: TDBEdit
        Left = 592
        Top = 152
        Width = 68
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSXxxCab
        TabOrder = 13
        Visible = False
      end
      object DBEdit20: TDBEdit
        Left = 664
        Top = 152
        Width = 72
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSXxxCab
        TabOrder = 14
        Visible = False
      end
      object DBEdit21: TDBEdit
        Left = 916
        Top = 152
        Width = 84
        Height = 21
        DataField = 'ValorT'
        DataSource = DsVSXxxCab
        TabOrder = 15
      end
      object DBEdit22: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsVSXxxCab
        TabOrder = 16
      end
      object DBEdit23: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsVSXxxCab
        TabOrder = 17
      end
      object DBEdit24: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'Motorista'
        DataSource = DsVSXxxCab
        TabOrder = 18
      end
      object DBEdit25: TDBEdit
        Left = 72
        Top = 152
        Width = 432
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsVSXxxCab
        TabOrder = 19
      end
      object DBEdit26: TDBEdit
        Left = 508
        Top = 152
        Width = 80
        Height = 21
        DataField = 'Placa'
        DataSource = DsVSXxxCab
        TabOrder = 20
      end
      object DBEdit27: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsVSXxxCab
        TabOrder = 21
      end
      object DBEdit28: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsVSXxxCab
        TabOrder = 22
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 312
        Height = 32
        Caption = 'Gerenciamento de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 312
        Height = 32
        Caption = 'Gerenciamento de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 312
        Height = 32
        Caption = 'Gerenciamento de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSFchGerCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSFchGerCabAfterOpen
    BeforeClose = QrVSFchGerCabBeforeClose
    AfterScroll = QrVSFchGerCabAfterScroll
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.SerieFch, vmi.Ficha,   '
      '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   '
      'COUNT(vmi.Codigo) ITENS,   '
      'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  '
      'vsf.Nome NO_SERIEFCH   '
      'FROM vsmovits vmi  '
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=6440 '
      'GROUP BY vmi.SerieFch, vmi.Ficha')
    Left = 236
    Top = 424
    object QrVSFchGerCabMovimID: TLargeintField
      FieldName = 'MovimID'
    end
    object QrVSFchGerCabMovimCod: TLargeintField
      FieldName = 'MovimCod'
    end
    object QrVSFchGerCabCodID: TLargeintField
      FieldName = 'CodID'
    end
    object QrVSFchGerCabCodigo01: TLargeintField
      FieldName = 'Codigo01'
    end
    object QrVSFchGerCabCodigo13: TLargeintField
      FieldName = 'Codigo13'
    end
    object QrVSFchGerCabCodigo16: TLargeintField
      FieldName = 'Codigo16'
    end
    object QrVSFchGerCabSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSFchGerCabFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSFchGerCabITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrVSFchGerCabNO_SERIEFCH: TWideStringField
      FieldName = 'NO_SERIEFCH'
      Size = 60
    end
    object QrVSFchGerCabRefLast: TDateTimeField
      FieldName = 'RefLast'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerCabRefFirst: TDateTimeField
      FieldName = 'RefFirst'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerCabNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSFchGerCabID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsVSFchGerCab: TDataSource
    DataSet = QrVSFchGerCab
    Left = 236
    Top = 472
  end
  object QrVSFchGerIDs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSFchGerIDsBeforeClose
    AfterScroll = QrVSFchGerIDsAfterScroll
    SQL.Strings = (
      'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,  '
      '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,  '
      'COUNT(vmi.Codigo) ITENS,  '
      'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, '
      'vsf.Nome NO_SERIEFCH  '
      'FROM vsmovits vmi '
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch '
      'WHERE vmi.SerieFch=2'
      'AND vmi.Ficha=6440'
      'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID ')
    Left = 400
    Top = 424
    object QrVSFchGerIDsMovimID: TLargeintField
      FieldName = 'MovimID'
    end
    object QrVSFchGerIDsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrVSFchGerIDsRefFirst: TDateTimeField
      FieldName = 'RefFirst'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerIDsRefLast: TDateTimeField
      FieldName = 'RefLast'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerIDsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 60
    end
    object QrVSFchGerIDsSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSFchGerIDsFicha: TLargeintField
      FieldName = 'Ficha'
    end
  end
  object DsVSFchGerIDs: TDataSource
    DataSet = QrVSFchGerIDs
    Left = 400
    Top = 472
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.GraGruX, wmi.Pecas,'
      'wmi.PesoKg, wmi.AreaM2, wmi.AreaP2, wmi.ValorT,'
      'wmi.SdoVrtPeca, wmi.SdoVrtPeso, wmi.SdoVrtArM2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, wmi.DataHora,'
      'wmi.SrcNivel1, wmi.SrcNivel2,'
      'wmi.DstNivel1, wmi.DstNivel2'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 384
    Top = 196
    object QrIMEIsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEIsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEIsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEIsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrIMEIsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrIMEIsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrIMEIsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrIMEIsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrIMEIsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrIMEIsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIsNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 384
    Top = 244
  end
  object QrVSFchGerNivs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSFchGerNivsBeforeClose
    AfterScroll = QrVSFchGerNivsAfterScroll
    SQL.Strings = (
      'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,  '
      '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,  '
      'COUNT(vmi.Codigo) ITENS,  '
      'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, '
      'vsf.Nome NO_SERIEFCH  '
      'FROM vsmovits vmi '
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch '
      'WHERE vmi.SerieFch=2'
      'AND vmi.Ficha=6440'
      'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID ')
    Left = 492
    Top = 424
    object QrVSFchGerNivsMovimID: TLargeintField
      FieldName = 'MovimID'
    end
    object QrVSFchGerNivsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
    end
    object QrVSFchGerNivsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrVSFchGerNivsRefFirst: TDateTimeField
      FieldName = 'RefFirst'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerNivsRefLast: TDateTimeField
      FieldName = 'RefLast'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrVSFchGerNivsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrVSFchGerNivsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSFchGerNivsSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSFchGerNivsFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSFchGerNivsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSFchGerNivs: TDataSource
    DataSet = QrVSFchGerNivs
    Left = 492
    Top = 472
  end
  object PMIMEIs: TPopupMenu
    Left = 168
    Top = 376
    object Irparajaneladomovimento1: TMenuItem
      Caption = 'Ir para janela do movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object IrparajaneladedadosdoIMEI1: TMenuItem
      Caption = 'Ir para janela de dados do IME-I '
      OnClick = IrparajaneladedadosdoIMEI1Click
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEIselecionado1: TMenuItem
      Caption = 'Exclui IME-I selecionado'
      Enabled = False
      Visible = False
      OnClick = ExcluiIMEIselecionado1Click
    end
    object AlteraIMEIorigemdoIMEISelecionado1: TMenuItem
      Caption = 'Altera IME-I origem do IME-I Selecionado'
      Enabled = False
      Visible = False
      OnClick = AlteraIMEIorigemdoIMEISelecionado1Click
    end
    object teste1: TMenuItem
      Caption = '&teste'
      OnClick = teste1Click
    end
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLotesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT '
      'CAST(1 AS SIGNED) TemIMEIMrt, '
      'CAST(SerieFch AS SIGNED) SerieFch, '
      'CAST(Ficha AS SIGNED) Ficha,   '
      'CONCAT(SerieFch, ".", Ficha) SerieEFicha   '
      'FROM vsmovitb  '
      'WHERE Ficha <> 0  '
      '  '
      'UNION  '
      '  '
      'SELECT DISTINCT '
      'CAST(1 AS SIGNED) TemIMEIMrt, '
      'CAST(SerieFch AS SIGNED) SerieFch, '
      'CAST(Ficha AS SIGNED) Ficha,   '
      'CONCAT(SerieFch, ".", Ficha) SerieEFicha   '
      'FROM vsmovits  '
      'WHERE Ficha <> 0  '
      'AND NOT (  '
      '  CONCAT(SerieFch, ".", Ficha) IN   '
      '  (  '
      '    SELECT   '
      '      CONCAT(SerieFch, ".", Ficha)  '
      '      FROM vsmovitb  '
      '      WHERE Ficha <> 0  '
      '  )  '
      ')  '
      '  '
      'ORDER BY Ficha, SerieFch  '
      '  '
      '')
    Left = 168
    Top = 424
    object QrLotesSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrLotesFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrLotesSerieEFicha: TWideStringField
      FieldName = 'SerieEFicha'
      Size = 23
    end
    object QrLotesNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrLotesID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object QrVSXxxCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0')
    Left = 320
    Top = 424
    object QrVSXxxCabCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrVSXxxCabMovimCod: TLargeintField
      FieldName = 'MovimCod'
    end
    object QrVSXxxCabEmpresa: TLargeintField
      FieldName = 'Empresa'
    end
    object QrVSXxxCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrVSXxxCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSXxxCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrVSXxxCabFornecedor: TLargeintField
      FieldName = 'Fornecedor'
    end
    object QrVSXxxCabTransporta: TLargeintField
      FieldName = 'Transporta'
    end
    object QrVSXxxCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSXxxCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSXxxCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSXxxCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSXxxCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSXxxCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSXxxCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSXxxCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSXxxCabClienteMO: TLargeintField
      FieldName = 'ClienteMO'
    end
    object QrVSXxxCabProcednc: TLargeintField
      FieldName = 'Procednc'
    end
    object QrVSXxxCabMotorista: TLargeintField
      FieldName = 'Motorista'
    end
    object QrVSXxxCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
    object QrVSXxxCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrVSXxxCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSXxxCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
  end
  object DsVSXxxCab: TDataSource
    DataSet = QrVSXxxCab
    Left = 320
    Top = 472
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 168
    Top = 472
  end
  object PMImprime: TPopupMenu
    Left = 52
    Top = 428
    object PorArtigo1: TMenuItem
      Caption = 'Por Artigo'
      OnClick = PorArtigo1Click
    end
    object PorArtigoeMartelo1: TMenuItem
      Caption = 'Por Artigo e Martelo'
      OnClick = PorArtigoeMartelo1Click
    end
  end
end
