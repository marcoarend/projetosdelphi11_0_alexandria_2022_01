unit VSImpEstqReduz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, frxClass, frxDBSet;

type
  TFmVSImpEstqReduz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrClientMO: TMySQLQuery;
    QrClientMONOMECI: TWideStringField;
    QrClientMOCodigo: TIntegerField;
    DsClientMO: TDataSource;
    QrFornecedor: TMySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrStqCenCad: TMySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label53: TLabel;
    Label55: TLabel;
    Label10: TLabel;
    EdTerceiro: TdmkEditCB;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    CBTerceiro: TdmkDBLookupComboBox;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrEstoque: TMySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoquePecas: TFloatField;
    QrEstoquePesoKg: TFloatField;
    QrEstoqueAreaM2: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    QrEstoqueGraGruY: TIntegerField;
    QrEstoqueVSMulFrnCab: TIntegerField;
    QrEstoqueClientMO: TIntegerField;
    QrEstoqueSerieFch: TIntegerField;
    QrEstoqueFicha: TIntegerField;
    QrEstoqueMarca: TWideStringField;
    QrEstoqueNO_SerieFch: TWideStringField;
    QrEstoqueSdoVrtArM2: TFloatField;
    QrEstoqueSdoVrtArP2: TFloatField;
    QrEstoqueSdoVrtPeca: TFloatField;
    QrEstoqueSdoVrtPeso: TFloatField;
    QrEstoqueGGXInProc: TIntegerField;
    DsEstoque: TDataSource;
    DBG04Estq: TdmkDBGridZTO;
    frxWET_CURTI_266_1: TfrxReport;
    BtImprime: TBitBtn;
    frxDsEstoque: TfrxDBDataset;
    QrEstoqueSdoVrtValr: TFloatField;
    QrEstoqueKg_Peca: TFloatField;
    QrEstoqueValor_kg: TFloatField;
    QrGraGruXGraGruY: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_266_1GetValue(const VarName: string; var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClientMOChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReabrePesquisa();
    procedure FechaPesquisa();
  public
    { Public declarations }
  end;

  var
  FmVSImpEstqReduz: TFmVSImpEstqReduz;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnVS_CRC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmVSImpEstqReduz.BtImprimeClick(Sender: TObject);
var
  GraGruX, GraGruY: Integer;
begin
  ReabrePesquisa();
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    GraGruY := QrEstoqueGraGruY.Value
  else GraGruY := 0;
  //
  if GraGruY < 2048 then
  begin
    MyObjects.frxDefineDatasets(frxWET_CURTI_266_1, [
      DMOdG.frxDsDono,
      frxDsEstoque
     ]);
  end else
  begin
    // Parei aqui! fazer para m2!
    MyObjects.frxDefineDatasets(frxWET_CURTI_266_1, [
      DMOdG.frxDsDono,
      frxDsEstoque
     ]);
  end;
  //
  MyObjects.frxMostra(frxWET_CURTI_266_1, 'Estoque de Reduzido');
end;

procedure TFmVSImpEstqReduz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpEstqReduz.EdClientMOChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSImpEstqReduz.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSImpEstqReduz.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSImpEstqReduz.EdStqCenCadChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSImpEstqReduz.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSImpEstqReduz.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmVSImpEstqReduz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpEstqReduz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
end;

procedure TFmVSImpEstqReduz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpEstqReduz.frxWET_CURTI_266_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_GRAGRUX' then
    Value :=  dmkPF.ParValueCodTxt(
      'Artigo: ', CBGraGruX.Text, EdGraGruX.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_STQCENCAD' then
    Value :=  dmkPF.ParValueCodTxt(
      'Centro de estoque: ', CBStqCenCad.Text, EdStqCenCad.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_CLIENTMO' then
    Value :=  dmkPF.ParValueCodTxt(
      'Cliente M.O.: ', CBClientMO.Text, EdClientMO.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_FORNECEDOR' then
    Value :=  dmkPF.ParValueCodTxt(
      'Forneceor: ', CBTerceiro.Text, EdTerceiro.ValueVariant, 'TODOS')
end;

procedure TFmVSImpEstqReduz.ReabrePesquisa();
var
  Empresa, ClientMO, GraGruX, Fornecedor, StqcenCad: Integer;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  GraGruX := EdGraGruX.ValueVariant;
  ClientMO := EdClientMO.ValueVariant;
  Fornecedor := EdTerceiro.ValueVariant;
  StqCenCad := EdStqCenCad.ValueVariant;
  //
  VS_CRC_PF.ReopenItensAptos_Cal(QrEstoque, Empresa, StqCenCad, Fornecedor,
    ClientMO, GraGruX, TModoBxaEstq.mbePeca);
end;

end.
