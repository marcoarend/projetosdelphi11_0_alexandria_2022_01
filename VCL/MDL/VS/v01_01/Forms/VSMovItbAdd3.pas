unit VSMovItbAdd3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  mySQLDbTables, dmkEdit, UnProjGroup_Consts;

type
  TFmVSMovItbAdd3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrIMEIs: TmySQLQuery;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    TPDataLimite: TdmkEditDateTimePicker;
    SpeedButton1: TSpeedButton;
    EdIMEILimite: TdmkEdit;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    EdIMEIsCopiados: TdmkEdit;
    Label3: TLabel;
    EdCaCsCopiados: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdIMEIsExcluidos: TdmkEdit;
    EdCaCsExcluidos: TdmkEdit;
    Label7: TLabel;
    EdIMEIsPesquisados: TdmkEdit;
    EdCaCsPesquisados: TdmkEdit;
    BtExecuta: TBitBtn;
    QrCaCs: TmySQLQuery;
    BtAtualiza: TBitBtn;
    Query: TmySQLQuery;
    Memo1: TMemo;
    Label8: TLabel;
    EdStepInc: TdmkEdit;
    Label10: TLabel;
    Label9: TLabel;
    TPDataMinima: TdmkEditDateTimePicker;
    EdPalletMenor: TdmkEdit;
    BtExecSQL: TBitBtn;
    QrPallets: TMySQLQuery;
    QrPalletsPallet: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TPDataLimiteChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure TPDataLimiteClick(Sender: TObject);
    procedure BtExecSQLClick(Sender: TObject);
    procedure EdPalletMenorDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTemVSMorCab(Itens: String);
    procedure Reseta();
  public
    { Public declarations }
  end;

  var
  FmVSMovItbAdd3: TFmVSMovItbAdd3;

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, UMySQLModule,
  UnVS_PF, ModVS;

{$R *.DFM}

procedure TFmVSMovItbAdd3.AtualizaTemVSMorCab(Itens: String);
begin
  VS_PF.AtualizaTemIMEIMrt2('vsajscab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsbxacab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsdvlcab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsexbcab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsgerarta', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsgerrcla', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsinncab', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsmovdif', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsopecab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsoutcab', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsoutnfi', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vspaclacaba', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vspaclaitsa', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vspalleta', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vspamulcaba', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vspamulcabr', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vspamulitsa', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vspamulitsr', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsparclbxaa', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsparclcaba', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsparclitsa', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vsplccab', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsprepalcab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vspwecab', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('vsrclcab', LaAviso1, LaAviso2, Itens);  Nao usa para nada???
  VS_PF.AtualizaTemIMEIMrt2('vsrtbcab', LaAviso1, LaAviso2, Itens);
  VS_PF.AtualizaTemIMEIMrt2('vssubprdcab', LaAviso1, LaAviso2, Itens);
  //VS_PF.AtualizaTemIMEIMrt2('', LaAviso1, LaAviso2, Itens);
  //
  VS_PF.AtualizaTemIMEIMrt2('vsmovcab', LaAviso1, LaAviso2, Itens, 'Codigo');
  //VS_PF.AtualizaTemIMEIMrt2('vscaccab', LaAviso1, LaAviso2, Itens); ??? Como fazer???

(*
vsajscab
vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsopecab
vsoutcab
vspamulcaba
vspamulcabr
vsplccab
vspwecab
vsrtbcab
vssubprdcab

SELECT 1  Tipo, Codigo FROM vsajscab WHERE TemIMEIMrt = 1 UNION
SELECT 2  Tipo, Codigo FROM vsbxacab WHERE TemIMEIMrt = 1 UNION
SELECT 3  Tipo, Codigo FROM vsdvlcab WHERE TemIMEIMrt = 1 UNION
SELECT 4  Tipo, Codigo FROM vsexbcab WHERE TemIMEIMrt = 1 UNION
SELECT 5  Tipo, Codigo FROM vsgerarta WHERE TemIMEIMrt = 1 UNION
SELECT 6  Tipo, Codigo FROM vsgerrcla WHERE TemIMEIMrt = 1 UNION
SELECT 7  Tipo, Codigo FROM vsinncab WHERE TemIMEIMrt = 1 UNION
SELECT 8  Tipo, Codigo FROM vsopecab WHERE TemIMEIMrt = 1 UNION
SELECT 9  Tipo, Codigo FROM vsoutcab WHERE TemIMEIMrt = 1 UNION
SELECT 10  Tipo, Codigo FROM vspamulcaba WHERE TemIMEIMrt = 1 UNION
SELECT 11  Tipo, Codigo FROM vspamulcabr WHERE TemIMEIMrt = 1 UNION
SELECT 12  Tipo, Codigo FROM vsplccab WHERE TemIMEIMrt = 1 UNION
SELECT 13  Tipo, Codigo FROM vspwecab WHERE TemIMEIMrt = 1 UNION
SELECT 14  Tipo, Codigo FROM vsrtbcab WHERE TemIMEIMrt = 1 UNION
SELECT 15  Tipo, Codigo FROM vssubprdcab WHERE TemIMEIMrt = 1 UNION
SELECT 16  Tipo, Codigo FROM vsmovcab WHERE TemIMEIMrt = 1

vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsopecab
vsoutcab
vspamulcaba
vspamulcabr
vsplccab
vspwecab
vsrtbcab
vssubprdcab
*)

end;

procedure TFmVSMovItbAdd3.BtAtualizaClick(Sender: TObject);
var
  StrMovCod: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod ',
  'FROM vsmovitb ',
  '']);
  StrMovCod := MyObjects.CordaDeQuery(Query, 'MovimCod', EmptyStr);
  AtualizaTemVSMorCab(StrMovCod);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSMovItbAdd3.BtExecSQLClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  Query.Database := Dmod.MyDB;
  Query.SQL.Text := Memo1.Text;
  Query.ExecSQL;
  Screen.Cursor := crDefault;
end;

procedure TFmVSMovItbAdd3.BtExecutaClick(Sender: TObject);
var
  DtaLimite, IMEILimite, TabOrig, TabDest, Campos: String;
  VSMorCab: Integer;
var
  Nome, DataHora: String;
  Codigo: Integer;
  //
  //Novo:
  IMax, IAtu, IInc, RAfc: Integer;
  Corda, StrMovCod, Texto: String;
  T1, T2: Cardinal;
  Total, MediaSeg, QtdReg1: Integer;
  Segundos: Double;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  T1 := GetTickCount;
  //
  Total          := 0;
  MediaSeg       := 0;
  Codigo         := 0;
  Nome           := '';
  DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  DtaLimite      := Geral.FDT(TPDataLimite.Date + 1, 1);
  IMEILimite     :=  Geral.FF0(EdIMEILimite.ValueVariant);
  //
  Codigo := UMyMod.BPGS1I32('vsmorcab', 'Codigo', '', '', tsPos, stIns, 0);
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmorcab', False, [
  'Nome', CO_DATA_HORA_GRL, 'DtaLimite',
  'IMEILimite'], [
  'Codigo'], [
  Nome, DataHora, DtaLimite,
  IMEILimite], [
  Codigo], True) then
    Exit;
  //
  VSMorCab := Codigo;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  IMEIS >> VS Mov Its para VS Mov Itb
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Transferindo dados para arquivo morto: IME-Is');
  TabOrig := CO_DEL_TAB_VMI;
  TabDest := 'vsmovitb';

  Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabDest, '', QtdReg1);
  Campos := Geral.Substitui(Campos,
    ', VSMorCab', ', ' + FormatFloat('0', VSMorCab) + ' VSMorCab');
  //
  {
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ' + TabDest,
  'SELECT ' + Campos,
  'FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  ') ',
  'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
  '']);
  EdIMEIsCopiados.ValueVariant := Dmod.QrUpd.RowsAffected;
  }
  IMax := EdIMEIsPesquisados.ValueVariant;
  IAtu := 0;
  IInc := EdStepInc.ValueVariant;
  RAfc := 0;
  while IAtu < IMax do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'SELECT Controle ',
    'FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
    'LIMIT ' + Geral.FF0(IAtu) + ', ' + Geral.FF0(IInc),
    '']);
    if Query.RecordCount > 0 then
    begin
      Corda := MyObjects.CordaDeQuery(Query, 'Controle', EmptyStr);
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'INSERT INTO ' + TabDest,
      'SELECT ' + Campos,
      'FROM ' + TabOrig,
      'WHERE Controle IN (' + Corda + ')',
      'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
      '']);
      //
      RAfc := RAfc + Dmod.QrUpd.RowsAffected;
    end;
    IAtu := IAtu + IInc;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Transferindo dados para arquivo morto: IME-Is. Afetados: ' + Geral.FF0(RAfc) +
    '. ' + Geral.FF0(IAtu) + ' IMEIs de ' + Geral.FF0(IMax));
  end;
  EdIMEIsCopiados.ValueVariant := RAfc;
  Total := Total + RAfc;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  CaCs >> VSCaCItsA para VSCaCItsB
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Transferindo dados para arquivo morto: CaCs');
  TabOrig := 'vscacitsa';
  TabDest := 'vscacitsb';
  //VSMorCab := definido acima
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabDest, '', QtdReg1);
  Campos := Geral.Substitui(Campos,
    ', VSMorCab', ', ' + FormatFloat('0', VSMorCab) + ' VSMorCab');
  //
{
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ' + TabDest,
  'SELECT ' + Campos,
  'FROM ' + TabOrig,
  'WHERE VSPallet > 0  ',
  'AND (VSPallet IN ( ',
  '  SELECT Pallet ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  '  ) ',
  ') ',
  'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
  '']);
  EdCaCsCopiados.ValueVariant := Dmod.QrUpd.RowsAffected;
}
  //IMax := EdCaCsPesquisados.ValueVariant;
  IAtu := 0;
  IInc := EdStepInc.ValueVariant;
  RAfc := 0;
  while IAtu < IMax do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'SELECT Pallet ',
    'FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
    'LIMIT ' + Geral.FF0(IAtu) + ', ' + Geral.FF0(IInc),
    '']);
    if Query.RecordCount > 0 then
    begin
      Corda := MyObjects.CordaDeQuery(Query, 'Pallet', EmptyStr);
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'INSERT INTO ' + TabDest,
      'SELECT ' + Campos,
      'FROM ' + TabOrig,
      'WHERE VSPallet > 0  ',
      'AND VSPallet IN ( ' + Corda + ')',
      'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
      '']);
      RAfc := RAfc + Dmod.QrUpd.RowsAffected;
    end;
    IAtu := IAtu + IInc;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Transferindo dados para arquivo morto: CaCs. Afetados: ' + Geral.FF0(RAfc) +
    '. ' + Geral.FF0(IAtu) + ' pallets de ' + Geral.FF0(IMax));
  end;
  EdCaCsCopiados.ValueVariant := RAfc;
  Total := Total + RAfc;
  //
  //
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  IMEIS >> Excluir dados do VS Mov Its movidos para VS Mov Itb
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados do arquivo ativo: IME-Is');
  TabOrig := CO_DEL_TAB_VMI;
  TabDest := 'vsmovitb';
  //VSMorCab := definido acima
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + TabDest,
  '  WHERE VSMorCab=' + Geral.FF0(VSMorCab),
  ') ',
  '']);
  EdIMEIsExcluidos.ValueVariant := Dmod.QrUpd.RowsAffected;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  CaCs >> Excluir dados do VSCaCItsA movidos para VSCaCItsB
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados do arquivo ativo: CaCs');
  TabOrig := 'vscacitsa';
  TabDest := 'vscacitsb';
  //VSMorCab := definido acima
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + TabDest,
  '  WHERE VSMorCab=' + Geral.FF0(VSMorCab),
  ') ',
  '']);
  EdCaCsExcluidos.ValueVariant := Dmod.QrUpd.RowsAffected;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod ',
  'FROM vsmovitb ',
  '']);
  StrMovCod := MyObjects.CordaDeQuery(Query, 'MovimCod', EmptyStr);
  AtualizaTemVSMorCab(StrMovCod);
(*
vsajscab
vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsmovdif
vsopecab
vsoutcab
vsoutnfi
vspaclacaba
vspaclaitsa
vspalleta
vspamulcaba
vspamulcabr
vspamulitsa
vspamulitsr
vsparclbxaa
vsparclcaba
vsparclitsa
vsplccab
vsprepalcab
vspwecab
vsrtbcab
vssubprdcab


????
vscgicab
vscgiits
vsdsnits
vspedcab
vspedits
vseqzits
vsrclcab
*)
  //
  BtPesquisa.Enabled := False;
  BtExecuta.Enabled := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  T2 := GetTickCount;
  Segundos := (T2-T1) / 1000;
  MediaSeg := Round(Total / Segundos);
  Texto := 'Step inc: ' + Geral.FF0(IInc) + ' > Tempo = ' +
  'Tempo: ' + FloatToStr(Segundos) + ' s. M�dia: ' + Geral.FF0(MediaSeg) + ' reg/seg';
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
  Memo1.Lines.Add(Texto);
  //
  BtPesquisa.Enabled := True;
end;

procedure TFmVSMovItbAdd3.BtPesquisaClick(Sender: TObject);
const
  GraGruX = 0;
  Empresa = -11;
  TemIMEiMrt = 0;
  MargemErro = 0.01;
  Avisa = True;
  ForcaMostrarForm = False;
  SelfCall = False;
var
  DtaLimite, IMEILimite, Corda, SQL_Pallets: String;
  PalletMenor: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando inconsistencias que impedem o arquivamento');
  if DmModVS.QtdPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2) then Exit;
  if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False,
    LaAviso1, LaAviso2) then Exit;
  if VS_PF.RegistrosComProblema(Empresa, GraGruX, LaAviso1, LaAviso2) then Exit;
  if DmModVS.VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall, TemIMEIMrt, LaAviso1, LaAviso2) then Exit;
  if DmModVS.CustosDiferentesSaldosVirtuais3(Empresa, GraGruX, TemIMEiMrt,
    MargemErro, Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando / pesquisando');
  //
  EdIMEIsPesquisados.ValueVariant := 0;
  EdIMEIsCopiados.ValueVariant    := 0;
  EdIMEIsExcluidos.ValueVariant   := 0;
  EdCaCsPesquisados.ValueVariant  := 0;
  EdCaCsCopiados.ValueVariant     := 0;
  EdCaCsExcluidos.ValueVariant    := 0;
  //
  DtaLimite   := Geral.FDT(TPDataLimite.Date + 1, 1);
  IMEILimite  := Geral.FF0(EdIMEILimite.ValueVariant);
  PalletMenor := EdPalletMenor.ValueVariant;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Pallets antigos com saldo');
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, DModG.MyPID_DB, [
  'SELECT Pallet',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE (',
  '  SdoVrtPeca > 0',
  '  OR',
  '  DataHora >= "' + DtaLimite + '" ',
  ')',
  'AND Pallet <> 0',
  '']);
  SQL_Pallets := EmptyStr;
  if QrPallets.RecordCount > 0 then
  begin
    Corda := MyObjects.CordaDeQuery(Query, 'Pallet', EmptyStr);
    if Corda <> EmptyStr then
    begin
      SQL_Pallets := Geral.ATS([
      'AND (',
      '  NOT Pallet IN ( ' + Corda + ') ',
      '  ) ']);
    end;
  end;
  //
  Corda := EmptyStr;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEIs a exportar');
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIS, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS  ' + CO_TAB_VM_MOV + ' ; ',
  'CREATE TABLE  ' + CO_TAB_VM_MOV + '  ',
  'SELECT Controle, Pallet, DataHora, 1 Tipo ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE ',
  '  (Controle <= ' + IMEILimite + ') ',
  'AND ',
  //////////////////////////////////////////////////////////////////////////////
  //'  (SdoVrtPeca <= 0) ', N�o mandar saldo negativo para o arquivo morto!!!
  //
  //'  (SdoVrtPeca = 0) ', N�o mandar saldo positivo de raspa (kg) para o arquivo morto!!!
  //
  '(',
  '  (SdoVrtPeca<=0)',
  '  OR',
  '  (Pecas=0 AND SdoVrtPeso<=0)',
  ')',
  //////////////////////////////////////////////////////////////////////////////
  'AND ',
  '  (DataHora < "' + DtaLimite + '") ',
  SQL_Pallets,
{
  'AND ',
  '  (NOT Pallet IN ',
  '    ( ',
  '      SELECT Pallet ',
  '      FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  '      WHERE ( ',
  '        SdoVrtPeca > 0 ',
  '        OR ',
  '        DataHora >= "' + DtaLimite + '" ',
  '      ) ',
  '      AND Pallet <> 0 ',
  '    ) ',
  '  ) ',
}
  '; ',
  //
  'INSERT INTO ' + CO_TAB_VM_MOV + '  ',
  'SELECT Controle, Pallet, DataHora, 2 Tipo ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE Pallet > 0 ',
  'AND Pallet < ' + Geral.FF0(PalletMenor),
  'AND DataHora < "' + DtaLimite + '" ',
  '; ',
  'SELECT * FROM  ' + CO_TAB_VM_MOV + ' ; ',
  '']);
  EdIMEIsPesquisados.ValueVariant := QrIMEIS.RecordCount;
  //
  // Criar lista de pallets dos CaCs a serem exportados
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Pallets envolvidos');
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS  ' + CO_TAB_PL_LST + ' ; ',
  'CREATE TABLE  ' + CO_TAB_PL_LST + '  ',
  '  SELECT DISTINCT Pallet ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  '; ',
  'SELECT * FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_PL_LST + '  ',
  '']);
  {
  //Geral.MB_SQL(Self, Query);
  Geral.MB_Info(Corda);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaCS, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet > 0  ',
  'AND (VSPallet IN ( ',
  '  SELECT Pallet ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  '  ) ',
  ') ',
  '']);
  //Geral.MB_SQL(Self, QrCaCS);
  }

  if Query.RecordCount > 0 then
  begin
    // Criar lista de pallets
    Corda := MyObjects.CordaDeQuery(Query, 'Pallet', EmptyStr);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando CaCs a exportar');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCaCS, Dmod.MyDB, [
    'SELECT * ',
    'FROM vscacitsa ',
    'WHERE VSPallet > 0  ',
    'AND VSPallet IN ( ' + Corda + ')',
    '']);
    EdCaCsPesquisados.ValueVariant := QrCaCs.RecordCount;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  BtExecuta.Enabled := QrIMEIs.RecordCount > 0;
  EdIMEIsCopiados.ValueVariant    := 0;
  EdIMEIsExcluidos.ValueVariant   := 0;
  EdCaCsCopiados.ValueVariant     := 0;
  EdCaCsExcluidos.ValueVariant    := 0;
end;

procedure TFmVSMovItbAdd3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovItbAdd3.EdPalletMenorDblClick(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(EdPalletMenor.ValueVariant);
end;

procedure TFmVSMovItbAdd3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMovItbAdd3.FormCreate(Sender: TObject);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  Data := DmodG.ObtemAgora();
  Data := IncMonth(Data, - 5);
  TPDataLimite.Date := Geral.UltimoDiaDoMes(Data);
  Data := DmodG.ObtemAgora();
  Data := IncMonth(Data, - 3);
  TPDataLimite.MaxDate := Geral.UltimoDiaDoMes(Data);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT MIN(DataHora) MinData ',
  'FROM vscacitsa ',
  '']);
  TPDataMinima.Date := Query.FieldByName('MinData').AsDateTime;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT MIN(Pallet) Pallet  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Pallet > 0  ',
  'AND (',
  '  (SdoVrtPeca>0)',
  '  OR',
  '  (Pecas=0 AND SdoVrtPeso>0)',
  ')',
  '']);
  EdPalletMenor.ValueVariant := Query.FieldByName('Pallet').AsInteger;
end;

procedure TFmVSMovItbAdd3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovItbAdd3.Reseta();
var
  Qry: TmySQLQuery;
  Data: String;
begin
  BtExecuta.Enabled               := False;
  EdIMEIsPesquisados.ValueVariant := 0;
  EdIMEIsCopiados.ValueVariant    := 0;
  EdIMEIsExcluidos.ValueVariant   := 0;
  EdCaCsPesquisados.ValueVariant  := 0;
  EdCaCsCopiados.ValueVariant     := 0;
  EdCaCsExcluidos.ValueVariant    := 0;
  //
  Data := Geral.FDT(TPDataLimite.Date + 1, 1);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Controle) Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE DataHora <= "' + Data + '" ',
    '']);
    //
    EdIMEILimite.ValueVariant := Qry.FieldByName('Controle').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSMovItbAdd3.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    TPDataLimite.Enabled := True;
    TPDataLimite.SetFocus;
  end;
end;

procedure TFmVSMovItbAdd3.TPDataLimiteChange(Sender: TObject);
begin
  Reseta();
end;

procedure TFmVSMovItbAdd3.TPDataLimiteClick(Sender: TObject);
begin
  Reseta();
end;

end.
