unit VSOutPedPes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkEdit, dmkEditCB, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, Grids, DBGrids, dmkDBGrid, DB, mySQLDbTables,
  dmkImage, UnDmkEnums;

type
  TFmVSOutPedPes = class(TForm)
    Panel1: TPanel;
    DBGItens: TdmkDBGrid;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    Label2: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    DsClientes: TDataSource;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    QrVSPedCab: TMySQLQuery;
    QrVSPedCabNOMEVENDEDOR: TWideStringField;
    QrVSPedCabNOMECLIENTE: TWideStringField;
    QrVSPedCabNOMETRANSP: TWideStringField;
    QrVSPedCabNO_CONDICAOPG: TWideStringField;
    QrVSPedCabCodigo: TIntegerField;
    QrVSPedCabEmpresa: TIntegerField;
    QrVSPedCabCliente: TIntegerField;
    QrVSPedCabVendedor: TIntegerField;
    QrVSPedCabDataF: TDateField;
    QrVSPedCabValor: TFloatField;
    QrVSPedCabObz: TWideStringField;
    QrVSPedCabTransp: TIntegerField;
    QrVSPedCabCondicaoPg: TIntegerField;
    QrVSPedCabPedidCli: TWideStringField;
    QrVSPedCabNome: TWideStringField;
    QrVSPedCabLk: TIntegerField;
    QrVSPedCabDataCad: TDateField;
    QrVSPedCabDataAlt: TDateField;
    QrVSPedCabUserCad: TIntegerField;
    QrVSPedCabUserAlt: TIntegerField;
    QrVSPedCabAlterWeb: TSmallintField;
    QrVSPedCabPecas: TFloatField;
    QrVSPedCabPesoKg: TFloatField;
    QrVSPedCabAreaM2: TFloatField;
    QrVSPedCabAreaP2: TFloatField;
    QrVSPedCabAtivo: TSmallintField;
    QrVSPedCabNOMEEMP: TWideStringField;
    DsVSPedCab: TDataSource;
    QrCorda: TMySQLQuery;
    QrCordaCodigo: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGItensDblClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCorda: String;
    procedure ReabrePedidos();
  public
    { Public declarations }
    FEdPedido: TdmkEdit;
  end;

  var
  FmVSOutPedPes: TFmVSOutPedPes;

implementation

uses UnMyObjects, Module, ModuleGeral, UnInternalConsts, FatPedCab2,
  UnMySQLCuringa;

{$R *.DFM}

procedure TFmVSOutPedPes.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutPedPes.BtOKClick(Sender: TObject);
begin
  if FEdPedido <> nil then
    FEdPedido.ValueVariant := QrVSPedCabCodigo.Value
  else
    FmFatPedCab2.EdPedido.ValueVariant := QrVSPedCabCodigo.Value;
  Close;
end;

procedure TFmVSOutPedPes.DBGItensDblClick(Sender: TObject);
begin
  if QrVSPedCab.RecordCount > 0 then
    BtOKClick(Self);
end;

procedure TFmVSOutPedPes.EdClienteChange(Sender: TObject);
begin
  ReabrePedidos();
end;

procedure TFmVSOutPedPes.EdEmpresaChange(Sender: TObject);
begin
  ReabrePedidos();
end;

procedure TFmVSOutPedPes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutPedPes.FormCreate(Sender: TObject);
begin
  FEdPedido := nil;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmVSOutPedPes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutPedPes.FormShow(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT DISTINCT Codigo ',
  'FROM vspedits ',
  'WHERE Status=' + Geral.FF0(Integer(TStausPedVda.spvLiberado)),
  '']);
  FCorda := MyObjects.CordaDeQuery(QrCorda, 'Codigo', EmptyStr);
  if FCorda = EmptyStr then
  begin
    Geral.MB_Aviso('N�o h� pedidos com o status liberado (Status=3)!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT DISTINCT cli.Codigo,   ',
  'IF(cli.Tipo=0,RazaoSocial,Cli.Nome) NOMEENT   ',
  'FROM vspedcab vpc  ',
  'LEFT JOIN entidades cli ON cli.Codigo=vpc.Cliente  ',
  'WHERE vpc.Codigo IN (' + FCorda + ')  ',
  'ORDER BY NOMEENT  ',
  '']);
  //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSOutPedPes.ReabrePedidos;
var
  Filial, Empresa, Cliente: Integer;
begin
  Screen.Cursor := crHourGlass;
  QrVSPedCab.Close;
  Filial  := EdEmpresa.ValueVariant;
  Cliente := EdCliente.ValueVariant;
  if (Filial <> 0) and (Cliente <> 0) then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    if FCorda = EmptyStr then
    begin
      BtOK.Enabled := False;
      Geral.MB_Info('N�o existem pedidos em aberto!');
      Exit
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSPedCab, Dmod.MyDB, [
      'SELECT ',
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR, ',
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ',
      'IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP, ',
      'pc.Nome NO_CONDICAOPG, ',
      'vpc.* ',
      'FROM vspedcab vpc ',
      'LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa ',
      'LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente ',
      'LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor ',
      'LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp ',
      'LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg ',
      'WHERE vpc.Codigo IN (' + FCorda + ')',
      'AND vpc.Empresa=' + Geral.FF0(Empresa),
      'AND vpc.Cliente=' + Geral.FF0(Cliente),
      '']);
      BtOK.Enabled := QrVSPedCab.RecordCount > 0;
    end;
  end else
    BtOK.Enabled := False;
  Screen.Cursor := crDefault;
end;

procedure TFmVSOutPedPes.SpeedButton1Click(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := CuringaLoc.CriaForm('Codigo', 'Nome', 'Entidades', Dmod.MyDB, '', True);
  if Cliente <> 0 then
  begin
    EdCliente.ValueVariant := Cliente;
    CBCliente.KeyValue     := Cliente;
  end;
end;

end.
