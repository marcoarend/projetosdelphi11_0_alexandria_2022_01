unit VSInfIEC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, mySQLDbTables, dmkDBGridZTO;

type
  TFmVSInfIEC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    TbVSInfIEC: TmySQLTable;
    DsVSInfIEC: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    TbVSInfIECCodigo: TIntegerField;
    TbVSInfIECVSInfFol: TIntegerField;
    TbVSInfIECVSInfLin: TIntegerField;
    TbVSInfIECData: TDateField;
    TbVSInfIECNF: TIntegerField;
    TbVSInfIECFornecedor: TIntegerField;
    TbVSInfIECSerFch: TIntegerField;
    TbVSInfIECFicha: TIntegerField;
    TbVSInfIECPallet: TIntegerField;
    TbVSInfIECGraGruX: TIntegerField;
    TbVSInfIECPecas: TFloatField;
    TbVSInfIECPesoKg: TFloatField;
    TbVSInfIECAreaM2: TFloatField;
    TbVSInfIECAreaP2: TFloatField;
    TbVSInfIECLk: TIntegerField;
    TbVSInfIECDataCad: TDateField;
    TbVSInfIECDataAlt: TDateField;
    TbVSInfIECUserCad: TIntegerField;
    TbVSInfIECUserAlt: TIntegerField;
    TbVSInfIECAlterWeb: TSmallintField;
    TbVSInfIECAtivo: TSmallintField;
    QrVSInfIEC: TmySQLQuery;
    QrVSInfIECCodigo: TIntegerField;
    QrVSInfIECVSInfFol: TIntegerField;
    QrVSInfIECVSInfLin: TIntegerField;
    QrVSInfIECData: TDateField;
    QrVSInfIECNF: TIntegerField;
    QrVSInfIECFornecedor: TIntegerField;
    QrVSInfIECSerFch: TIntegerField;
    QrVSInfIECFicha: TIntegerField;
    QrVSInfIECPallet: TIntegerField;
    QrVSInfIECGraGruX: TIntegerField;
    QrVSInfIECPecas: TFloatField;
    QrVSInfIECPesoKg: TFloatField;
    QrVSInfIECAreaM2: TFloatField;
    QrVSInfIECAreaP2: TFloatField;
    QrVSInfIECLk: TIntegerField;
    QrVSInfIECDataCad: TDateField;
    QrVSInfIECDataAlt: TDateField;
    QrVSInfIECUserCad: TIntegerField;
    QrVSInfIECUserAlt: TIntegerField;
    QrVSInfIECAlterWeb: TSmallintField;
    QrVSInfIECAtivo: TSmallintField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    TbVSInfIECNO_Fornece: TWideStringField;
    TbVSInfIECNO_SerFch: TWideStringField;
    TbVSInfIECNO_PRD_TAM_COR: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TbVSInfIECBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure CopiaUltimo();
  public
    { Public declarations }
  end;

  var
  FmVSInfIEC: TFmVSInfIEC;

implementation

uses
  MyGlyfs, Principal, UnMyObjects, Module, DmkDAC_PF, UMySQLModule, UnVS_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmVSInfIEC.BtSaidaClick(Sender: TObject);
begin
{
  if TFmFormBaseTab(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
}
end;

procedure TFmVSInfIEC.CopiaUltimo();
var
  Data: TDateTime;
  //Codigo,
  VSInfFol, VSInfLin, NF, Fornecedor, SerFch, Ficha, Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SQLType: TSQLType;
begin
  if TbVSInfIEC.State = dsBrowse then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSInfIEC, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsinfiec ',
    'ORDER BY Codigo DESC ',
    'LIMIT 0,1 ',
    '']);
    TbVSInfIEC.Insert;
(*
    //SQLType        := ImgTipo.SQLType?;
    //Codigo         := QrVSInfIECCodigo.Value + 1;
    VSInfFol       := TbVSInfIECVSInfFol.Value;
    VSInfLin       := TbVSInfIECVSInfLin.Value;
    Data           := TbVSInfIECData.Value;
    NF             := TbVSInfIECNF.Value;
    Fornecedor     := TbVSInfIECFornecedor.Value;
    SerFch         := TbVSInfIECSerFch.Value;
    Ficha          := TbVSInfIECFicha.Value;
    Pallet         := TbVSInfIECPallet.Value;
    GraGruX        := TbVSInfIECGraGruX.Value;
    Pecas          := TbVSInfIECPecas.Value;
    PesoKg         := TbVSInfIECPesoKg.Value;
    AreaM2         := TbVSInfIECAreaM2.Value;
    AreaP2         := TbVSInfIECAreaP2.Value;
    //
*)
    VSInfFol       := QrVSInfIECVSInfFol.Value;
    VSInfLin       := QrVSInfIECVSInfLin.Value + 1;
(*    if (VSInfFol = 0) and (VSInfLin = 0) then
    begin
      VSInfFol       := QrVSInfIECVSInfFol.Value;
      VSInfLin       := QrVSInfIECVSInfLin.Value + 1;
*)
      if VSInfLin = 25 then
      begin
        VSInfFol := VSInfFol + 1;
        VSInfLin := 1;
      end;
//    end;
    TbVSInfIECVSInfFol.Value := VSInfFol;
    TbVSInfIECVSInfLin.Value := VSInfLin;
    //
(*
    if Data < 2 then
      Data           := QrVSInfIECData.Value;
    if NF < 2 then
    //NF             := QrVSInfIECNF.Value;
    Fornecedor     := QrVSInfIECFornecedor.Value;
    SerFch         := QrVSInfIECSerFch.Value;
    Ficha          := QrVSInfIECFicha.Value;
    Pallet         := QrVSInfIECPallet.Value + 1;
    GraGruX        := QrVSInfIECGraGruX.Value;
    Pecas          := QrVSInfIECPecas.Value;
    PesoKg         := QrVSInfIECPesoKg.Value;
    if (AreaM2 = 0) and (AreaP2 <> 0) then
      AreaM2         := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento)
    else
    if (AreaM2 <> 0) then
      AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    //
*)
    TbVSInfIECData.Value       := QrVSInfIECData.Value;
    TbVSInfIECNF.Value         := QrVSInfIECNF.Value;
    TbVSInfIECFornecedor.Value := QrVSInfIECFornecedor.Value;
    TbVSInfIECSerFch.Value     := QrVSInfIECSerFch.Value;
    TbVSInfIECFicha.Value      := QrVSInfIECFicha.Value;
    TbVSInfIECPallet.Value     := QrVSInfIECPallet.Value + 1;
    TbVSInfIECGraGruX.Value    := QrVSInfIECGraGruX.Value;
    TbVSInfIECPecas.Value      := QrVSInfIECPecas.Value;
    TbVSInfIECPesoKg.Value     := 0;//QrVSInfIECPesoKg.Value;
    TbVSInfIECAreaM2.Value     := 0;//QrVSInfIECAreaM2.Value;
    TbVSInfIECAreaP2.Value     := 0;//QrVSInfIECAreaP2.Value;
  end;
end;

procedure TFmVSInfIEC.FormActivate(Sender: TObject);
begin
{
  if TFmFormBaseTab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
}
end;

procedure TFmVSInfIEC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  VS_PF.AbreGraGruXY(QrGraGruX, '');
  VS_PF.AbreVSSerFch(QrVSSerFch);
  //
  TbVSInfIEC.Active := True;
end;

procedure TFmVSInfIEC.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key in ([Ord('i'), Ord('I')])) then
    CopiaUltimo();
end;

procedure TFmVSInfIEC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInfIEC.FormShow(Sender: TObject);
begin
(*
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
*)
end;

procedure TFmVSInfIEC.TbVSInfIECBeforePost(DataSet: TDataSet);
var
  Codigo: Integer;
{
  Data: String;
  Codigo, VSInfFol, VSInfLin, NF, Fornecedor, SerFch, Ficha, Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SQLType: TSQLType;
}
begin
  if TbVSInfIEC.State = dsInsert then
  begin
    Codigo := UMyMod.BPGS1I32('vsinfiec', 'Codigo', '', '', tsPos, stIns, 0);
    //
    TbVSInfIECCodigo.Value   := Codigo;
    TbVSInfIECAtivo.Value    := 1;
    TbVSInfIECAlterWeb.Value := 1;
    TbVSInfIECUserCad.Value  := VAR_USUARIO;
    TbVSInfIECDataCad.Value  := DModG.ObtemAgora();
    //
{
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSInfIEC, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsinfiec ',
    'ORDER BY Codigo DESC ',
    'LIMIT 0,1 ',
    '']);
    //SQLType        := ImgTipo.SQLType?;
    Codigo         := QrVSInfIECCodigo.Value + 1;
    VSInfFol       := TbVSInfIECVSInfFol.Value;
    VSInfLin       := TbVSInfIECVSInfLin.Value;
    Data           := TbVSInfIECData.Value;
    NF             := TbVSInfIECNF.Value;
    Fornecedor     := TbVSInfIECFornecedor.Value;
    SerFch         := TbVSInfIECSerFch.Value;
    Ficha          := TbVSInfIECFicha.Value;
    Pallet         := TbVSInfIECPallet.Value;
    GraGruX        := TbVSInfIECGraGruX.Value;
    Pecas          := TbVSInfIECPecas.Value;
    PesoKg         := TbVSInfIECPesoKg.Value;
    AreaM2         := TbVSInfIECAreaM2.Value;
    AreaP2         := TbVSInfIECAreaP2.Value;
    //
    if (VSInfFol = 0) and (VSInfLin = 0) then
    begin
      VSInfFol       := QrVSInfIECVSInfFol.Value;
      VSInfLin       := QrVSInfIECVSInfLin.Value + 1;
      if VSInfLin = 25 then
      begin
        VSInfFol := VSInfFol + 1;
        VSInfLin := 1;
      end;
    end;
    if Data < 2 then
      Data           := QrVSInfIECData.Value;
    if NF < 2 then
    //NF             := QrVSInfIECNF.Value;
    Fornecedor     := QrVSInfIECFornecedor.Value;
    SerFch         := QrVSInfIECSerFch.Value;
    Ficha          := QrVSInfIECFicha.Value;
    Pallet         := QrVSInfIECPallet.Value + 1;
    GraGruX        := QrVSInfIECGraGruX.Value;
    Pecas          := QrVSInfIECPecas.Value;
    PesoKg         := QrVSInfIECPesoKg.Value;
    if (AreaM2 = 0) and (AreaP2 <> 0) then
      AreaM2         := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento)
    else
    if (AreaM2 <> 0) then
      AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    //
? := UMyMod.BuscaEmLivreY_Def('vsinfiec', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('vsinfiec', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'vsinfiec', auto_increment?[
'VSInfFol', 'VSInfLin', 'Data',
'NF', 'Fornecedor', 'SerFch',
'Ficha', 'Pallet', 'GraGruX',
'Pecas', 'PesoKg', 'AreaM2',
'AreaP2'], [
'Codigo'], [
VSInfFol, VSInfLin, Data,
NF, Fornecedor, SerFch,
Ficha, Pallet, GraGruX,
Pecas, PesoKg, AreaM2,
AreaP2], [
Codigo], UserDataAlterweb?, IGNORE?
}
  end;
end;

end.
