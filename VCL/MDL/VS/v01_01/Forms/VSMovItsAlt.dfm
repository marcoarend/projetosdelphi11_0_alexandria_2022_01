object FmVSMovItsAlt: TFmVSMovItsAlt
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-056 :: Altera'#231#227'o Manual de IME-I'
  ClientHeight = 669
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GBQtdOriginal: TGroupBox
    Left = 0
    Top = 389
    Width = 1008
    Height = 96
    Align = alTop
    Caption = ' Dados do item: '
    Enabled = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 79
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaPecas: TLabel
        Left = 8
        Top = 1
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object LaAreaM2: TLabel
        Left = 80
        Top = 1
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object LaAreaP2: TLabel
        Left = 156
        Top = 1
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object LaPeso: TLabel
        Left = 240
        Top = 1
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label1: TLabel
        Left = 316
        Top = 1
        Width = 71
        Height = 13
        Caption = 'Valor total [F4]:'
      end
      object Label6: TLabel
        Left = 8
        Top = 41
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object Label56: TLabel
        Left = 900
        Top = 1
        Width = 74
        Height = 13
        Caption = 'Req.Mov.Estq.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 400
        Top = 1
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label58: TLabel
        Left = 736
        Top = 1
        Width = 86
        Height = 13
        Caption = 'Data / hora baixa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdPecas: TdmkEdit
        Left = 8
        Top = 17
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 80
        Top = 17
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 156
        Top = 17
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 240
        Top = 17
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 8
        Top = 57
        Width = 985
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdValorT: TdmkEdit
        Left = 316
        Top = 17
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValorT'
        UpdCampo = 'ValorT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdValorTKeyDown
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 400
        Top = 17
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdReqMovEstq: TdmkEdit
        Left = 900
        Top = 17
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 456
        Top = 17
        Width = 277
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 6
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataHora: TdmkEditDateTimePicker
        Left = 736
        Top = 17
        Width = 108
        Height = 21
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtaVisPrv'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDataHora: TdmkEdit
        Left = 844
        Top = 17
        Width = 53
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 555
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 599
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBDados: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 341
    Align = alTop
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 324
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 597
        Top = 0
        Width = 407
        Height = 324
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Gera'#231#227'o: '
          TabOrder = 0
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label15: TLabel
              Left = 4
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Pecas'
              FocusControl = DBEdit15
            end
            object Label16: TLabel
              Left = 64
              Top = 0
              Width = 37
              Height = 13
              Caption = 'PesoKg'
              FocusControl = DBEdit16
            end
            object Label17: TLabel
              Left = 148
              Top = 0
              Width = 37
              Height = 13
              Caption = 'AreaM2'
              FocusControl = DBEdit17
            end
            object Label18: TLabel
              Left = 232
              Top = 0
              Width = 35
              Height = 13
              Caption = 'AreaP2'
              FocusControl = DBEdit18
            end
            object Label19: TLabel
              Left = 316
              Top = 0
              Width = 31
              Height = 13
              Caption = 'ValorT'
              FocusControl = DBEdit19
            end
            object DBEdit15: TDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Pecas'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit16: TDBEdit
              Left = 64
              Top = 16
              Width = 80
              Height = 21
              DataField = 'PesoKg'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit17: TDBEdit
              Left = 148
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit18: TDBEdit
              Left = 232
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaP2'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit19: TDBEdit
              Left = 316
              Top = 16
              Width = 80
              Height = 21
              DataField = 'ValorT'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
          end
        end
        object GroupBox5: TGroupBox
          Left = 0
          Top = 57
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Saldo atual: '
          TabOrder = 1
          object Panel14: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label28: TLabel
              Left = 4
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Pecas'
              FocusControl = DBEdit28
            end
            object Label29: TLabel
              Left = 64
              Top = 0
              Width = 37
              Height = 13
              Caption = 'PesoKg'
              FocusControl = DBEdit29
            end
            object Label30: TLabel
              Left = 148
              Top = 0
              Width = 37
              Height = 13
              Caption = 'AreaM2'
              FocusControl = DBEdit30
            end
            object DBEdit28: TDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              DataField = 'SdoVrtPeca'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit29: TDBEdit
              Left = 64
              Top = 16
              Width = 80
              Height = 21
              DataField = 'SdoVrtPeso'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit30: TDBEdit
              Left = 148
              Top = 16
              Width = 80
              Height = 21
              DataField = 'SdoVrtArM2'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 0
          Top = 114
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Gera'#231#227'o a partir deste IME-I: '
          TabOrder = 2
          object Panel16: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label42: TLabel
              Left = 4
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Pecas'
              FocusControl = DBEdit42
            end
            object Label43: TLabel
              Left = 64
              Top = 0
              Width = 37
              Height = 13
              Caption = 'PesoKg'
              FocusControl = DBEdit43
            end
            object Label44: TLabel
              Left = 148
              Top = 0
              Width = 37
              Height = 13
              Caption = 'AreaM2'
              FocusControl = DBEdit44
            end
            object Label45: TLabel
              Left = 232
              Top = 0
              Width = 35
              Height = 13
              Caption = 'AreaP2'
              FocusControl = DBEdit45
            end
            object DBEdit42: TDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              DataField = 'QtdGerPeca'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit43: TDBEdit
              Left = 64
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdGerPeso'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit44: TDBEdit
              Left = 148
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdGerArM2'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit45: TDBEdit
              Left = 232
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdGerArP2'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
          end
        end
        object GroupBox7: TGroupBox
          Left = 0
          Top = 171
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Estoque Anterior ???: '
          TabOrder = 3
          object Panel17: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label47: TLabel
              Left = 4
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Pecas'
              FocusControl = DBEdit47
            end
            object Label48: TLabel
              Left = 64
              Top = 0
              Width = 37
              Height = 13
              Caption = 'PesoKg'
              FocusControl = DBEdit48
            end
            object Label49: TLabel
              Left = 148
              Top = 0
              Width = 37
              Height = 13
              Caption = 'AreaM2'
              FocusControl = DBEdit49
            end
            object Label50: TLabel
              Left = 232
              Top = 0
              Width = 35
              Height = 13
              Caption = 'AreaP2'
              FocusControl = DBEdit50
            end
            object DBEdit47: TDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              DataField = 'QtdAntPeca'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit48: TDBEdit
              Left = 64
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdAntPeso'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit49: TDBEdit
              Left = 148
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdAntArM2'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit50: TDBEdit
              Left = 232
              Top = 16
              Width = 80
              Height = 21
              DataField = 'QtdAntArP2'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 228
          Width = 407
          Height = 96
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 4
          object Label7: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'MovimNiv'
            FocusControl = DBEdit6
          end
          object Label8: TLabel
            Left = 68
            Top = 4
            Width = 52
            Height = 13
            Caption = 'MovimTwn'
            FocusControl = DBEdit7
          end
          object Label9: TLabel
            Left = 128
            Top = 4
            Width = 42
            Height = 13
            Caption = 'MovimID'
            FocusControl = DBEdit8
          end
          object Label10: TLabel
            Left = 188
            Top = 4
            Width = 53
            Height = 13
            Caption = 'LnkNivXtr1'
            FocusControl = DBEdit9
          end
          object Label11: TLabel
            Left = 248
            Top = 4
            Width = 53
            Height = 13
            Caption = 'LnkNivXtr2'
            FocusControl = DBEdit10
          end
          object Label39: TLabel
            Left = 308
            Top = 4
            Width = 54
            Height = 13
            Caption = 'NotaMPAG'
            FocusControl = DBEdit39
          end
          object Label38: TLabel
            Left = 8
            Top = 44
            Width = 40
            Height = 13
            Caption = 'ValorMP'
            FocusControl = DBEdit38
          end
          object Label37: TLabel
            Left = 176
            Top = 44
            Width = 60
            Height = 13
            Caption = 'CustoMOTot'
            FocusControl = DBEdit37
          end
          object Label36: TLabel
            Left = 92
            Top = 44
            Width = 57
            Height = 13
            Caption = 'CustoMOKg'
            FocusControl = DBEdit36
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimNiv'
            DataSource = DsVSMovIts
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 68
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimTwn'
            DataSource = DsVSMovIts
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 128
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimID'
            DataSource = DsVSMovIts
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 188
            Top = 20
            Width = 56
            Height = 21
            DataField = 'LnkNivXtr1'
            DataSource = DsVSMovIts
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 248
            Top = 20
            Width = 56
            Height = 21
            DataField = 'LnkNivXtr2'
            DataSource = DsVSMovIts
            TabOrder = 4
          end
          object DBEdit38: TDBEdit
            Left = 8
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsVSMovIts
            TabOrder = 5
          end
          object DBEdit39: TDBEdit
            Left = 308
            Top = 20
            Width = 80
            Height = 21
            DataField = 'NotaMPAG'
            DataSource = DsVSMovIts
            TabOrder = 6
          end
          object DBEdit36: TDBEdit
            Left = 92
            Top = 60
            Width = 80
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSMovIts
            TabOrder = 7
          end
          object DBEdit37: TDBEdit
            Left = 176
            Top = 60
            Width = 80
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSMovIts
            TabOrder = 8
          end
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 597
        Height = 324
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 597
          Height = 121
          Align = alTop
          Caption = ' Dados Gerais: '
          TabOrder = 0
          object Panel11: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 104
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label2: TLabel
              Left = 4
              Top = 0
              Width = 28
              Height = 13
              Caption = 'IME-I:'
              FocusControl = DBEdCodigo
            end
            object Label13: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              FocusControl = DBEdit12
            end
            object Label3: TLabel
              Left = 124
              Top = 0
              Width = 167
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de gera'#231#227'o:'
            end
            object Label12: TLabel
              Left = 472
              Top = 0
              Width = 46
              Height = 13
              Caption = 'DataHora'
              FocusControl = DBEdit11
            end
            object Label4: TLabel
              Left = 4
              Top = 40
              Width = 32
              Height = 13
              Caption = 'IME-C:'
              FocusControl = dmkDBEdit1
            end
            object Label5: TLabel
              Left = 64
              Top = 40
              Width = 44
              Height = 13
              Caption = 'Empresa:'
              FocusControl = DBEdit3
            end
            object Label14: TLabel
              Left = 124
              Top = 40
              Width = 42
              Height = 13
              Caption = 'Terceiro:'
              FocusControl = DBEdit4
            end
            object Label20: TLabel
              Left = 184
              Top = 40
              Width = 41
              Height = 13
              Caption = 'Cli. pref.:'
              FocusControl = DBEdit5
            end
            object Label21: TLabel
              Left = 4
              Top = 80
              Width = 33
              Height = 13
              Caption = 'Artigo: '
              FocusControl = DBEdit13
            end
            object Label52: TLabel
              Left = 244
              Top = 40
              Width = 90
              Height = 13
              Caption = 'N'#237'vel do processo:'
              FocusControl = DBEdit46
            end
            object DBEdCodigo: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'Controle'
              DataSource = DsVSMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit12: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Pallet'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit1: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'MovimCod'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit2: TDBEdit
              Left = 180
              Top = 16
              Width = 289
              Height = 21
              DataField = 'NO_EstqMovimID'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit11: TDBEdit
              Left = 472
              Top = 16
              Width = 112
              Height = 21
              DataField = 'DataHora'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
            object dmkDBEdit1: TdmkDBEdit
              Left = 4
              Top = 56
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'Codigo'
              DataSource = DsVSMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 5
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit3: TDBEdit
              Left = 64
              Top = 56
              Width = 56
              Height = 21
              DataField = 'Empresa'
              DataSource = DsVSMovIts
              TabOrder = 6
            end
            object DBEdit4: TDBEdit
              Left = 124
              Top = 56
              Width = 56
              Height = 21
              DataField = 'Terceiro'
              DataSource = DsVSMovIts
              TabOrder = 7
            end
            object DBEdit5: TDBEdit
              Left = 184
              Top = 56
              Width = 56
              Height = 21
              DataField = 'CliVenda'
              DataSource = DsVSMovIts
              TabOrder = 8
            end
            object DBEdit13: TDBEdit
              Left = 44
              Top = 80
              Width = 56
              Height = 21
              DataField = 'GraGruX'
              TabOrder = 9
            end
            object DBEdit14: TDBEdit
              Left = 104
              Top = 80
              Width = 481
              Height = 21
              DataField = 'NO_PRD_TAM_COR'
              TabOrder = 10
            end
            object DBEdit46: TDBEdit
              Left = 244
              Top = 56
              Width = 341
              Height = 21
              DataField = 'NO_MovimNiv'
              TabOrder = 11
            end
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 121
          Width = 597
          Height = 60
          Align = alTop
          Caption = ' Dados de Origem: '
          TabOrder = 1
          object Panel12: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label22: TLabel
              Left = 4
              Top = 0
              Width = 51
              Height = 13
              Caption = 'IME-I orig.:'
              FocusControl = dmkDBEdit2
            end
            object Label23: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              Enabled = False
              FocusControl = DBEdit20
            end
            object Label24: TLabel
              Left = 124
              Top = 0
              Width = 159
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de origem:'
            end
            object Label25: TLabel
              Left = 528
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Artigo:'
              FocusControl = DBEdit23
            end
            object dmkDBEdit2: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'SrcNivel2'
              DataSource = DsVSMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit20: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              TabOrder = 1
              Visible = False
            end
            object DBEdit21: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'SrcNivel1'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit22: TDBEdit
              Left = 180
              Top = 16
              Width = 345
              Height = 21
              DataField = 'NO_SrcMovID'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit23: TDBEdit
              Left = 528
              Top = 16
              Width = 56
              Height = 21
              DataField = 'SrcGGX'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
          end
        end
        object GroupBox8: TGroupBox
          Left = 0
          Top = 181
          Width = 597
          Height = 60
          Align = alTop
          Caption = ' Dados de Destino '
          TabOrder = 2
          object Panel13: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label26: TLabel
              Left = 4
              Top = 0
              Width = 54
              Height = 13
              Caption = 'IME-I dest.:'
              FocusControl = dmkDBEdit3
            end
            object Label27: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              Enabled = False
              FocusControl = DBEdit24
            end
            object Label31: TLabel
              Left = 124
              Top = 0
              Width = 162
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de destino:'
            end
            object Label32: TLabel
              Left = 528
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Artigo:'
              FocusControl = DBEdit27
            end
            object dmkDBEdit3: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'DstNivel2'
              DataSource = DsVSMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit24: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              TabOrder = 1
              Visible = False
            end
            object DBEdit25: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'DstNivel1'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit26: TDBEdit
              Left = 180
              Top = 16
              Width = 345
              Height = 21
              DataField = 'NO_DstMovID'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit27: TDBEdit
              Left = 528
              Top = 16
              Width = 56
              Height = 21
              DataField = 'DstGGX'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 241
          Width = 597
          Height = 83
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object Label40: TLabel
            Left = 240
            Top = 44
            Width = 30
            Height = 13
            Caption = 'Marca'
            FocusControl = DBEdit40
          end
          object Label34: TLabel
            Left = 188
            Top = 44
            Width = 43
            Height = 13
            Caption = 'Misturou:'
            FocusControl = DBEdit34
          end
          object Label41: TLabel
            Left = 128
            Top = 44
            Width = 40
            Height = 13
            Caption = 'GraGru1'
            FocusControl = DBEdit41
          end
          object Label33: TLabel
            Left = 68
            Top = 44
            Width = 26
            Height = 13
            Caption = 'Ficha'
            FocusControl = DBEdit33
          end
          object Label35: TLabel
            Left = 360
            Top = 44
            Width = 50
            Height = 13
            Caption = 'FornecMO'
            FocusControl = DBEdit35
          end
          object Label46: TLabel
            Left = 8
            Top = 44
            Width = 42
            Height = 13
            Caption = 'SerieFch'
            FocusControl = DBEdit32
          end
          object Label51: TLabel
            Left = 8
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Observ'
            FocusControl = DBEdit31
          end
          object DBEdit40: TDBEdit
            Left = 239
            Top = 60
            Width = 118
            Height = 21
            DataField = 'Marca'
            DataSource = DsVSMovIts
            TabOrder = 0
          end
          object DBEdit34: TDBEdit
            Left = 188
            Top = 60
            Width = 48
            Height = 21
            DataField = 'Misturou'
            DataSource = DsVSMovIts
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 68
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Ficha'
            DataSource = DsVSMovIts
            TabOrder = 2
          end
          object DBEdit41: TDBEdit
            Left = 128
            Top = 60
            Width = 56
            Height = 21
            DataField = 'GraGru1'
            DataSource = DsVSMovIts
            TabOrder = 3
          end
          object DBEdit35: TDBEdit
            Left = 360
            Top = 60
            Width = 56
            Height = 21
            DataField = 'FornecMO'
            DataSource = DsVSMovIts
            TabOrder = 4
          end
          object DBEdit32: TDBEdit
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            DataField = 'SerieFch'
            DataSource = DsVSMovIts
            TabOrder = 5
          end
          object DBEdit31: TDBEdit
            Left = 8
            Top = 20
            Width = 580
            Height = 21
            DataField = 'Observ'
            DataSource = DsVSMovIts
            TabOrder = 6
          end
        end
      end
    end
  end
  object GBDadosArtigo: TGroupBox
    Left = 0
    Top = 485
    Width = 1008
    Height = 70
    Align = alClient
    Caption = 'Dados do artigo: '
    Enabled = False
    TabOrder = 5
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label53: TLabel
        Left = 7
        Top = 0
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Ficha RMP:'
      end
      object Label54: TLabel
        Left = 224
        Top = 0
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
      end
      object Label55: TLabel
        Left = 324
        Top = 0
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object CBSerieFch: TdmkDBLookupComboBox
        Left = 47
        Top = 16
        Width = 169
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSSerFch
        TabOrder = 1
        dmkEditCB = EdSerieFch
        QryCampo = 'SerieFch'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSerieFch: TdmkEditCB
        Left = 7
        Top = 16
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSerieFch
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdFicha: TdmkEdit
        Left = 224
        Top = 16
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ficha'
        UpdCampo = 'Ficha'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMarca: TdmkEdit
        Left = 324
        Top = 16
        Width = 93
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Marca'
        UpdCampo = 'Marca'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkMisturou: TdmkCheckBox
        Left = 424
        Top = 16
        Width = 73
        Height = 17
        Caption = 'Misturou?'
        TabOrder = 4
        QryCampo = 'Misturou'
        UpdCampo = 'Misturou'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CGNotFluxo: TdmkCheckGroup
        Left = 752
        Top = 0
        Width = 252
        Height = 53
        Align = alRight
        Caption = ' N'#227'o considersr o fluxo de...:'
        Columns = 2
        Items.Strings = (
          '1024 - In Natura'
          '2048 - Artigo gerado'
          '3072 - Classificado'
          '4096 - Em opera'#231#227'o')
        TabOrder = 5
        QryCampo = 'NotFluxo'
        UpdCampo = 'NotFluxo'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CkZerado: TdmkCheckBox
        Left = 500
        Top = 16
        Width = 201
        Height = 17
        Caption = 'Considerar o estoque como zerado.'
        TabOrder = 6
        QryCampo = 'Zerado'
        UpdCampo = 'Zerado'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
  end
  object QrVSMovSrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 492
    Top = 65525
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovSrcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovSrcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovSrcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovSrcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovSrcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovSrcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovSrcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovSrcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovSrcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovSrcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovSrcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovSrcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovSrcPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovSrcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovSrcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovSrcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovSrcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovSrcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovSrcSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovSrcFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovSrcMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovSrcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovSrcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovSrcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovSrcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovSrcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovSrcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovSrcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovSrcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovSrcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovSrcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovSrcGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovSrcNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovSrcNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrVSMovSrcNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrVSMovSrcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovSrcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovSrcNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovSrcNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovSrcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovSrcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovSrcNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsVSMovSrc: TDataSource
    DataSet = QrVSMovSrc
    Left = 492
    Top = 37
  end
  object QrVSMovDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 568
    Top = 65525
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovDstGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovDstNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrVSMovDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrVSMovDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovDstNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsVSMovDst: TDataSource
    DataSet = QrVSMovDst
    Left = 568
    Top = 37
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSMovItsAfterOpen
    BeforeClose = QrVSMovItsBeforeClose
    OnCalcFields = QrVSMovItsCalcFields
    SQL.Strings = (
      'SELECT wmi.*,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Controle=1087')
    Left = 408
    Top = 65525
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVSMovItsNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 408
    Top = 37
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 636
    Top = 65524
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 633
    Top = 33
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 708
    Top = 65524
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 708
    Top = 36
  end
end
