object FmVSCalCab: TFmVSCalCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-142 :: Artigo em Processo de Caleiro'
  ClientHeight = 791
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 686
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 182
        Height = 13
        Caption = 'Observa'#231#227'o sobre a opera'#231#227'o (IMEC):'
      end
      object Label56: TLabel
        Left = 448
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 644
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 44660.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 776
        Top = 16
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 10
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 72
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 676
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 644
        Top = 72
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 623
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 136
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label3: TLabel
        Left = 420
        Top = 16
        Width = 117
        Height = 13
        Caption = 'Artigo em processo  [F4]:'
      end
      object Label10: TLabel
        Left = 904
        Top = 56
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
        Enabled = False
      end
      object LaPecas: TLabel
        Left = 852
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 928
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 372
        Top = 96
        Width = 413
        Height = 13
        Caption = 
          'Observa'#231#227'o sobre o artigo em opera'#231#227'o (IMEI - Ser'#225' impresso na O' +
          'rdem de Opera'#231#227'o):'
      end
      object Label19: TLabel
        Left = 776
        Top = 56
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label48: TLabel
        Left = 756
        Top = 16
        Width = 66
        Height = 13
        Caption = '$/m'#178' MO [F4]:'
      end
      object Label49: TLabel
        Left = 372
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label50: TLabel
        Left = 836
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label54: TLabel
        Left = 16
        Top = 96
        Width = 56
        Height = 13
        Caption = 'Cliente [F4]:'
      end
      object SbStqCenLoc: TSpeedButton
        Left = 753
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbStqCenLocClick
      end
      object EdControle: TdmkEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 420
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGraGruXKeyDown
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 476
        Top = 32
        Width = 277
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 2
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOTot: TdmkEdit
        Left = 904
        Top = 72
        Width = 94
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 852
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 928
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 372
        Top = 112
        Width = 557
        Height = 21
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 776
        Top = 72
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 9
        dmkEditCB = EdFornecMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOM2: TdmkEdit
        Left = 756
        Top = 32
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdCustoMOM2KeyDown
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 372
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 428
        Top = 72
        Width = 325
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 11
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReqMovEstq: TdmkEdit
        Left = 836
        Top = 72
        Width = 62
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdClienteKeyDown
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 14
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBVSPedIts: TGroupBox
      Left = 0
      Top = 233
      Width = 1008
      Height = 64
      Align = alTop
      TabOrder = 2
      object LaPedItsLib: TLabel
        Left = 16
        Top = 12
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label62: TLabel
        Left = 456
        Top = 12
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 72
        Top = 28
        Width = 381
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPedIts
        TabOrder = 1
        dmkEditCB = EdPedItsLib
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClientMO: TdmkEditCB
        Left = 456
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 512
        Top = 28
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 3
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 686
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 437
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 161
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 316
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label22: TLabel
        Left = 440
        Top = 16
        Width = 25
        Height = 13
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 476
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Data/hora gera'#231#227'o:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Fim classifica'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label55: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit42
      end
      object Label58: TLabel
        Left = 680
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsVSCalCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSCalCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 301
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSCalCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsVSCalCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 316
        Top = 72
        Width = 361
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSCalCab
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 440
        Top = 32
        Width = 32
        Height = 21
        DataField = 'NO_TIPO'
        DataSource = DsVSCalCab
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsVSCalCab
        TabOrder = 6
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSCalCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsVSCalCab
        TabOrder = 8
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsVSCalCab
        TabOrder = 9
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 96
        Width = 245
        Height = 61
        Caption = ' Origem:'
        TabOrder = 10
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label37: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label38: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasINI'
            DataSource = DsVSCalCab
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgINI'
            DataSource = DsVSCalCab
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaINIM2'
            DataSource = DsVSCalCab
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 264
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Destino:'
        TabOrder = 11
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label41: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasDst'
            DataSource = DsVSCalCab
            TabOrder = 0
          end
          object DBEdit32: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgDst'
            DataSource = DsVSCalCab
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaDstM2'
            DataSource = DsVSCalCab
            TabOrder = 2
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 760
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Saldo em opera'#231#227'o:'
        TabOrder = 12
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label45: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label46: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasSdo'
            DataSource = DsVSCalCab
            TabOrder = 0
          end
          object DBEdit36: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgSdo'
            DataSource = DsVSCalCab
            TabOrder = 1
          end
          object DBEdit37: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaSdoM2'
            DataSource = DsVSCalCab
            TabOrder = 2
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 512
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 13
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label43: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label47: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasBxa'
            DataSource = DsVSCalCab
            TabOrder = 0
          end
          object DBEdit34: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgBxa'
            DataSource = DsVSCalCab
            TabOrder = 1
          end
          object DBEdit38: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaBxaM2'
            DataSource = DsVSCalCab
            TabOrder = 2
          end
        end
      end
      object DBEdit42: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSCalCab
        TabOrder = 14
      end
      object DBEdit43: TDBEdit
        Left = 72
        Top = 72
        Width = 240
        Height = 21
        DataField = 'NO_Cliente'
        DataSource = DsVSCalCab
        TabOrder = 15
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 72
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsVSCalCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 16
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 72
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsVSCalCab
        TabOrder = 17
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 72
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsVSCalCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 18
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 622
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 122
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 296
        Top = 15
        Width = 710
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 577
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 296
          Left = 4
          Top = 4
          Width = 112
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Processo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 421
          Left = 116
          Top = 4
          Width = 112
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 447
          Left = 452
          Top = 4
          Width = 112
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDstClick
        end
        object BtPesagem: TBitBtn
          Tag = 194
          Left = 228
          Top = 4
          Width = 112
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pesagem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPesagemClick
        end
        object BtSubProduto: TBitBtn
          Tag = 605
          Left = 340
          Top = 4
          Width = 112
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sub Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtSubProdutoClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 161
      Width = 1008
      Height = 96
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label13: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 100
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label15: TLabel
        Left = 576
        Top = 16
        Width = 45
        Height = 13
        Caption = '$/m'#178' MO:'
      end
      object Label16: TLabel
        Left = 672
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label17: TLabel
        Left = 748
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Peso (origem):'
        Enabled = False
      end
      object Label20: TLabel
        Left = 836
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object Label21: TLabel
        Left = 916
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label24: TLabel
        Left = 132
        Top = 56
        Width = 137
        Height = 13
        Caption = 'Fornecedor da met'#233'ria prima:'
        FocusControl = DBEdit16
      end
      object Label26: TLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = 'S'#233'rie / Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label51: TLabel
        Left = 648
        Top = 56
        Width = 148
        Height = 13
        Caption = 'Local do (e) centro de estoque:'
        FocusControl = DBEdit39
      end
      object Label52: TLabel
        Left = 932
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        FocusControl = DBEdit41
      end
      object Label60: TLabel
        Left = 532
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
        FocusControl = DBEdit45
      end
      object Label61: TLabel
        Left = 432
        Top = 57
        Width = 92
        Height = 13
        Caption = 'Fornecedor de MO:'
        FocusControl = DBEdit47
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 32
        Width = 81
        Height = 21
        DataField = 'Controle'
        DataSource = DsVSCalAtu
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 100
        Top = 32
        Width = 57
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSCalAtu
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 156
        Top = 32
        Width = 373
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSCalAtu
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 576
        Top = 32
        Width = 94
        Height = 21
        DataField = 'CustoMOM2'
        DataSource = DsVSCalAtu
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 672
        Top = 32
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSCalAtu
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 748
        Top = 32
        Width = 84
        Height = 21
        DataField = 'QtdAntPeso'
        DataSource = DsVSCalAtu
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 836
        Top = 32
        Width = 76
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSCalAtu
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 916
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSCalAtu
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 132
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVSCalAtu
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 188
        Top = 72
        Width = 240
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSCalAtu
        TabOrder = 9
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 72
        Width = 113
        Height = 21
        DataField = 'NO_FICHA'
        DataSource = DsVSCalAtu
        TabOrder = 10
      end
      object DBEdit39: TDBEdit
        Left = 704
        Top = 72
        Width = 225
        Height = 21
        DataField = 'NO_LOC_CEN'
        DataSource = DsVSCalAtu
        TabOrder = 11
      end
      object DBEdit40: TDBEdit
        Left = 648
        Top = 72
        Width = 56
        Height = 21
        DataField = 'StqCenLoc'
        DataSource = DsVSCalAtu
        TabOrder = 12
      end
      object DBEdit41: TDBEdit
        Left = 932
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ReqMovEstq'
        DataSource = DsVSCalAtu
        TabOrder = 13
      end
      object DBEdit45: TDBEdit
        Left = 532
        Top = 32
        Width = 40
        Height = 21
        DataField = 'NO_TTW'
        DataSource = DsVSCalAtu
        TabOrder = 14
      end
      object DBEdit46: TDBEdit
        Left = 432
        Top = 72
        Width = 56
        Height = 21
        DataField = 'FornecMO'
        DataSource = DsVSCalAtu
        TabOrder = 15
      end
      object DBEdit47: TDBEdit
        Left = 488
        Top = 72
        Width = 157
        Height = 21
        DataField = 'NO_FORNEC_MO'
        DataSource = DsVSCalAtu
        TabOrder = 16
      end
    end
    object PnDst: TPanel
      Left = 0
      Top = 464
      Width = 1008
      Height = 158
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object Splitter2: TSplitter
        Left = 0
        Top = 93
        Width = 1008
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object Splitter3: TSplitter
        Left = 591
        Top = 98
        Width = 5
        Height = 60
        Align = alRight
        ExplicitLeft = 593
      end
      object DGDadosDst: TDBGrid
        Left = 0
        Top = 98
        Width = 591
        Height = 60
        Align = alClient
        DataSource = DsVSCalDst
        PopupMenu = PMDst
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Arquivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ReqMovEstq'
            Title.Caption = 'N'#176' RME'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = 'Data / hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SeqSifDipoa'
            Title.Caption = 'SIF/DIPOA'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SerieFch'
            Title.Caption = 'S'#233'rie RMP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigos DESTINO deste processo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'm'#178' gerado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaMPAG'
            Title.Caption = 'Nota MPAG'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Hist'#243'rico'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 596
        Top = 98
        Width = 412
        Height = 60
        Align = alRight
        DataSource = DsVSSubPrdIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Tabela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SeqSifDipoa'
            Title.Caption = 'SIF/DIPOA'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeso'
            Title.Caption = 'Sdo virtual kg'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SerieFch'
            Title.Caption = 'S'#233'rie RMP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Title.Caption = 'Ficha RMP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Marca'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Width = 72
            Visible = True
          end>
      end
      object DBGrid5: TDBGrid
        Left = 0
        Top = 0
        Width = 1008
        Height = 93
        Align = alTop
        DataSource = DsVSCalInd
        PopupMenu = PMDst
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Arquivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ReqMovEstq'
            Title.Caption = 'N'#176' RME'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = 'Data / hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SerieFch'
            Title.Caption = 'S'#233'rie RMP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigos processados no curtimento'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'm'#178' gerado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaMPAG'
            Title.Caption = 'Nota MPAG'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Hist'#243'rico'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end>
      end
    end
    object PCCalOri: TPageControl
      Left = 0
      Top = 317
      Width = 1008
      Height = 120
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 4
      object TabSheet1: TTabSheet
        Caption = 'Palltets'
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 730
          Height = 95
          Align = alClient
          DataSource = DsVSCalOriPallet
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end>
        end
        object GroupBox7: TGroupBox
          Left = 730
          Top = 0
          Width = 272
          Height = 95
          Align = alRight
          Caption = ' Baixas for'#231'adas: '
          TabOrder = 1
          object DBGrid3: TDBGrid
            Left = 2
            Top = 15
            Width = 269
            Height = 78
            Align = alClient
            DataSource = DsForcados
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso Kg'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'IME-Is'
        ImageIndex = 1
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          DataSource = DsVSCalOriIMEI
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -12
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerPeca'
              Title.Caption = 'P'#231' gerado'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerPeso'
              Title.Caption = 'Kg gerado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoPQ'
              Title.Caption = 'Custo PQ'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOKg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Pesagens de caleiro'
        ImageIndex = 2
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          DataSource = DsEmit
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Pesagem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataEmis'
              Title.Caption = 'Dta emiss'#227'o'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaBaixa_TXT'
              Title.Caption = 'Dta Baixa'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtCorrApo_TXT'
              Title.Caption = 'Dta CorrApo'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Caption = 'Peso KG'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fulao'
              Title.Caption = 'Ful'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Receita'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Nome da Receita'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECI'
              Title.Caption = 'Dono dos produtos'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Custo'
              Title.Caption = 'Custo Total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Width = 600
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Outras pesagens'
        ImageIndex = 3
        object DBGrid7: TDBGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 95
          Align = alLeft
          DataSource = DsPQO
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataB'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = 'Custo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmitGru'
              Title.Caption = 'Grupo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMITGRU'
              Title.Caption = 'Nome Grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Width = 90
              Visible = True
            end>
        end
        object DBGIts: TDBGrid
          Left = 517
          Top = 0
          Width = 485
          Height = 95
          Align = alClient
          DataSource = DsPQOIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Insumo'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliDest'
              Title.Caption = 'Empresa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Mercadoria'
              Width = 366
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 92
              Visible = True
            end>
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 257
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 5
      object Label18: TLabel
        Left = 644
        Top = 16
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 640
        Height = 60
        Align = alLeft
        Caption = ' Valores: '
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 636
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label25: TLabel
            Left = 220
            Top = 0
            Width = 62
            Height = 13
            Caption = '$/kg M.obra:'
            Enabled = False
          end
          object Label27: TLabel
            Left = 324
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Total M.O.:'
            Enabled = False
            FocusControl = DBEdit20
          end
          object Label28: TLabel
            Left = 428
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            FocusControl = DBEdit21
          end
          object Label29: TLabel
            Left = 12
            Top = 0
            Width = 49
            Height = 13
            Caption = 'Valor M.P.'
            FocusControl = DBEdit22
          end
          object Label30: TLabel
            Left = 532
            Top = 0
            Width = 40
            Height = 13
            Caption = 'R$ / kg:'
            FocusControl = DBEdit23
          end
          object Label31: TLabel
            Left = 116
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Custo PQ:'
            FocusControl = DBEdit24
          end
          object DBEdit19: TDBEdit
            Left = 220
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSCalAtu
            Enabled = False
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 324
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSCalAtu
            Enabled = False
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 428
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorT'
            DataSource = DsVSCalAtu
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsVSCalAtu
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 532
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CUSTO_KG'
            DataSource = DsVSCalAtu
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 116
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoPQ'
            DataSource = DsVSCalAtu
            TabOrder = 5
          end
        end
      end
      object DBEdit11: TDBEdit
        Left = 644
        Top = 32
        Width = 356
        Height = 21
        DataField = 'Observ'
        DataSource = DsVSCalAtu
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 369
        Height = 32
        Caption = 'Artigo em Processo de Caleiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 369
        Height = 32
        Caption = 'Artigo em Processo de Caleiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 369
        Height = 32
        Caption = 'Artigo em Processo de Caleiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSCalCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSCalCabBeforeOpen
    AfterOpen = QrVSCalCabAfterOpen
    BeforeClose = QrVSCalCabBeforeClose
    AfterScroll = QrVSCalCabAfterScroll
    OnCalcFields = QrVSCalCabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 120
    Top = 461
    object QrVSCalCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrVSCalCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrVSCalCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrVSCalCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSCalCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrVSCalCabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrVSCalCabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrVSCalCabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrVSCalCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrVSCalCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
    object QrVSCalCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrVSCalCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrVSCalCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrVSCalCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrVSCalCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrVSCalCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrVSCalCabPecasMan: TFloatField
      FieldName = 'PecasMan'
      Origin = 'vsopecab.PecasMan'
    end
    object QrVSCalCabAreaManM2: TFloatField
      FieldName = 'AreaManM2'
      Origin = 'vsopecab.AreaManM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaManP2: TFloatField
      FieldName = 'AreaManP2'
      Origin = 'vsopecab.AreaManP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsopecab.TipoArea'
    end
    object QrVSCalCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSCalCabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrVSCalCabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrVSCalCabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrVSCalCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrVSCalCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsopecab.GraGruX'
    end
    object QrVSCalCabCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
      Origin = 'vsopecab.CustoManMOKg'
    end
    object QrVSCalCabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrVSCalCabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPecasSrc: TFloatField
      FieldName = 'PecasSrc'
      Origin = 'vsopecab.PecasSrc'
    end
    object QrVSCalCabAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
      Origin = 'vsopecab.AreaSrcM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
      Origin = 'vsopecab.AreaSrcP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPecasDst: TFloatField
      FieldName = 'PecasDst'
      Origin = 'vsopecab.PecasDst'
    end
    object QrVSCalCabAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
      Origin = 'vsopecab.AreaDstM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
      Origin = 'vsopecab.AreaDstP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPecasSdo: TFloatField
      FieldName = 'PecasSdo'
      Origin = 'vsopecab.PecasSdo'
    end
    object QrVSCalCabAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
      Origin = 'vsopecab.AreaSdoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
      Origin = 'vsopecab.AreaSdoP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
      Origin = 'vsopecab.PesoKgSrc'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
      Origin = 'vsopecab.PesoKgMan'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
      Origin = 'vsopecab.PesoKgDst'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
      Origin = 'vsopecab.PesoKgSdo'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSCalCabAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      Origin = 'vsopecab.PesoKgBxa'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSCalCabPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrVSCalCabAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
      Origin = 'vsopecab.AreaBxaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
      Origin = 'vsopecab.AreaBxaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCalCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSCalCabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVSCalCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSCalCabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSCalCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSCalCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSCalCabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSCalCabValorTDst: TFloatField
      FieldName = 'ValorTDst'
    end
    object QrVSCalCabKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrVSCalCabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSCalCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSCalCabGGXSrc: TIntegerField
      FieldName = 'GGXSrc'
    end
    object QrVSCalCabOperacoes: TIntegerField
      FieldName = 'Operacoes'
    end
    object QrVSCalCabVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
  end
  object DsVSCalCab: TDataSource
    DataSet = QrVSCalCab
    Left = 120
    Top = 509
  end
  object QrVSCalAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 204
    Top = 461
    object QrVSCalAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSCalAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSCalAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSCalAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSCalAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSCalAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSCalAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSCalAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSCalAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSCalAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSCalAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSCalAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSCalAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSCalAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSCalAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSCalAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSCalAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSCalAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSCalAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSCalAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSCalAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSCalAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSCalAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSCalAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSCalAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSCalAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSCalAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSCalAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSCalAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSCalAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSCalAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSCalAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSCalAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCalAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSCalAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSCalAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSCalAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSCalAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSCalAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSCalAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSCalAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSCalAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSCalAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSCalAtuCUSTO_KG: TFloatField
      FieldName = 'CUSTO_KG'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSCalAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSCalAtuCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
  end
  object DsVSCalAtu: TDataSource
    DataSet = QrVSCalAtu
    Left = 204
    Top = 509
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 632
    Top = 516
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      Visible = False
      object porPallet1: TMenuItem
        Caption = 'por &Pallet'
        object Parcial1: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial1Click
        end
        object otal1: TMenuItem
          Caption = '&Total'
          OnClick = otal1Click
        end
      end
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I'
      end
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Parcial2: TMenuItem
      Caption = 'Adiciona IME-I &Parcial - Pe'#231'as'
      OnClick = Parcial2Click
    end
    object Total2: TMenuItem
      Caption = 'Adiciona IME-I &Total - Pe'#231'as'
      OnClick = Total2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AdicionaIMEIParcialPeso1: TMenuItem
      Caption = 'Adiciona IME-I &Parcial - Peso'
      OnClick = AdicionaIMEIParcialPeso1Click
    end
    object AdicionaIMEITotalPeso1: TMenuItem
      Caption = 'Adiciona IME-I &Total - Peso'
      OnClick = AdicionaIMEITotalPeso1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Corrigepesodebaixa1: TMenuItem
      Caption = 'Corrige peso de baixa'
      OnClick = Corrigepesodebaixa1Click
    end
    object ItsAlteraOri: TMenuItem
      Caption = '&Edita artigo de origem'
      Enabled = False
      Visible = False
      OnClick = ItsAlteraOriClick
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object ItsExcluiOriIMEI2: TMenuItem
      Caption = '&Remove IMEI de origem (incondicional)'
      OnClick = ItsExcluiOriIMEI2Click
    end
    object ItsExcluiOriPallet: TMenuItem
      Caption = 'Remo&ve Pallet de origem'
      Enabled = False
      OnClick = ItsExcluiOriPalletClick
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 632
    Top = 468
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EmitePesagem1: TMenuItem
      Caption = 'Emite Pesagem'
      OnClick = EmitePesagem1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmProcesso1: TMenuItem
      Caption = 'Atualiza estoque "Em &Processo"'
      OnClick = AtualizaestoqueEmProcesso1Click
    end
    object CorrigirFornecedor1: TMenuItem
      Caption = 'Corrigir dados autom'#225'ticos'
      OnClick = CorrigirFornecedor1Click
    end
    object AlteraFornecedorMO1: TMenuItem
      Caption = 'Altera'#231#227'o Parcial'
      object Localdoestoque1: TMenuItem
        Caption = '&Local do estoque'
        OnClick = Localdoestoque1Click
      end
      object Cliente1: TMenuItem
        Caption = '&Cliente'
        OnClick = Cliente1Click
      end
      object Datadeabertura1: TMenuItem
        Caption = '&Data de abertura'
        OnClick = Datadeabertura1Click
      end
    end
    object Atualizacusto1: TMenuItem
      Caption = 'Atualiza custo'
      OnClick = Atualizacusto1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 108
  end
  object QrVSCalOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSCalOriIMEIBeforeClose
    AfterScroll = QrVSCalOriIMEIAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 288
    Top = 461
    object QrVSCalOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSCalOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSCalOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSCalOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSCalOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSCalOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSCalOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSCalOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSCalOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSCalOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSCalOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSCalOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSCalOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSCalOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSCalOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSCalOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSCalOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSCalOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSCalOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSCalOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSCalOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSCalOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSCalOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSCalOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSCalOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSCalOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSCalOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSCalOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSCalOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSCalOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSCalOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCalOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCalOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSCalOriIMEICustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSCalOriIMEIClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSCalOriIMEIRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
    end
    object QrVSCalOriIMEIRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
    end
    object QrVSCalOriIMEIRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
    end
    object QrVSCalOriIMEIMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object DsVSCalOriIMEI: TDataSource
    DataSet = QrVSCalOriIMEI
    Left = 288
    Top = 509
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 420
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 420
    Top = 100
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OP (Ordem de Produ'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 16
    Top = 20
    object Rtuloderastreabilidade1: TMenuItem
      Caption = 'R'#243'tulo de rastreabilidade'
      OnClick = Rtuloderastreabilidade1Click
      object Desteprocesso1: TMenuItem
        Caption = 'Deste processo'
        OnClick = Desteprocesso1Click
      end
      object Qualquerprocesso1: TMenuItem
        Caption = 'Qualquer processo'
        OnClick = Qualquerprocesso1Click
      end
    end
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      OnClick = IMEIArtigodeRibeiragerado1Click
    end
    object OrdensdeProduoemAberto1: TMenuItem
      Caption = 'Ordens de produ'#231#227'o em &aberto'
      OnClick = OrdensdeProduoemAberto1Click
    end
    object CourosemprocessoBH1: TMenuItem
      Caption = 'Couros em processo BH'
      OnClick = CourosemprocessoBH1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
  end
  object QrVSCalDst: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSCalDstBeforeClose
    AfterScroll = QrVSCalDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 464
    Top = 461
    object QrVSCalDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSCalDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSCalDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSCalDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSCalDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSCalDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSCalDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSCalDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSCalDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSCalDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSCalDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSCalDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSCalDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSCalDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSCalDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSCalDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSCalDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSCalDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSCalDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSCalDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSCalDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSCalDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSCalDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSCalDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSCalDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSCalDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSCalDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSCalDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSCalDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSCalDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCalDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCalDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSCalDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSCalDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSCalDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSCalDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSCalDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrVSCalDstSeqSifDipoa: TLargeintField
      FieldName = 'SeqSifDipoa'
      DisplayFormat = '000;-000; '
    end
  end
  object DsVSCalDst: TDataSource
    DataSet = QrVSCalDst
    Left = 464
    Top = 509
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 632
    Top = 564
    object ItsIncluiDst: TMenuItem
      Caption = '&Adiciona artigo de destino'
      Enabled = False
      Visible = False
      OnClick = ItsIncluiDstClick
    end
    object ItsIncluiMul: TMenuItem
      Caption = 'Adiciona artigo de destino - Rateio em &todos itens de origem'
      OnClick = ItsIncluiMulClick
    end
    object ItsIncluiUni: TMenuItem
      Caption = 'Adiciona artigo de destino - &Item de origem selecionado'
      OnClick = ItsIncluiUniClick
    end
    object ItsAlteraDst: TMenuItem
      Caption = '&Edita artigo de destino'
      Enabled = False
      OnClick = ItsAlteraDstClick
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      Enabled = False
      OnClick = ItsExcluiDstClick
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = 'Informa &N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME2Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object irparajaneladomovimento1: TMenuItem
      Caption = 'ir para janela do &movimento'
      OnClick = irparajaneladomovimento1Click
    end
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 384
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSCalOriPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 376
    Top = 461
    object QrVSCalOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrVSCalOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSCalOriPalletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCalOriPalletAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriPalletAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSCalOriPalletSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSCalOriPalletFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSCalOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVSCalOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVSCalOriPalletSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrVSCalOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrVSCalOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSCalOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalOriPalletNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCalOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSCalOriPalletSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalOriPalletSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriPalletSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSCalOriPallet: TDataSource
    DataSet = QrVSCalOriPallet
    Left = 376
    Top = 509
  end
  object QrForcados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=9 '
      'AND SrcMovID=11'
      'AND SrcNivel1=5'
      '')
    Left = 908
    Top = 420
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrForcadosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrForcadosFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrForcadosMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrForcadosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrForcadosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 908
    Top = 468
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 708
    Top = 468
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 708
    Top = 512
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 208
    Top = 68
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 280
    Top = 104
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = '&Corrige Fornecedores destino a partir deste processo'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM emit')
    Left = 772
    Top = 468
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrEmitDtCorrApo_TXT: TWideStringField
      FieldName = 'DtCorrApo_TXT'
      Size = 10
    end
    object QrEmitDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrEmitDtaBaixa_TXT: TWideStringField
      FieldName = 'DtaBaixa_TXT'
      Size = 10
    end
    object QrEmitDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 772
    Top = 516
  end
  object PMSubProduto: TPopupMenu
    OnPopup = PMSubProdutoPopup
    Left = 716
    Top = 608
    object IncluiSubProdutorateandoentretodasorigens1: TMenuItem
      Caption = 'Inclui Sub Produto rateando entre todas origens'
      OnClick = IncluiSubProdutorateandoentretodasorigens1Click
    end
    object IncluiSubProduto1: TMenuItem
      Caption = '&Inclui Sub Produto do Item de entrada selecionado'
      OnClick = IncluiSubProduto1Click
    end
    object AlteraSubProduto1: TMenuItem
      Caption = '&Altera Sub Produto Atual'
      OnClick = AlteraSubProduto1Click
    end
    object ExcluiSubProduto1: TMenuItem
      Caption = '&Exclui SubProduto Atual'
      OnClick = ExcluiSubProduto1Click
    end
  end
  object QrVSSubPrdIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'WHERE vmi.GSPInnNiv2=1'
      'ORDER BY vmi.DataHora, vmi.Controle ')
    Left = 840
    Top = 469
    object QrVSSubPrdItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSSubPrdItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSSubPrdItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSSubPrdItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSSubPrdItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSSubPrdItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSSubPrdItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSSubPrdItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSSubPrdItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSSubPrdItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSSubPrdItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSSubPrdItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSSubPrdItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSSubPrdItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSSubPrdItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSSubPrdItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSSubPrdItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSSubPrdItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSSubPrdItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSSubPrdItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSSubPrdItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSSubPrdItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSSubPrdItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSSubPrdItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSSubPrdItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSSubPrdItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSSubPrdItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSSubPrdItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSSubPrdItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSSubPrdItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSSubPrdItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVSSubPrdItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSSubPrdItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSSubPrdItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSSubPrdItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSSubPrdItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSSubPrdItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSSubPrdItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSSubPrdItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSSubPrdItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSSubPrdItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSSubPrdItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSSubPrdItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSSubPrdItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSSubPrdItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSSubPrdItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSSubPrdItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSSubPrdItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSSubPrdItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSSubPrdItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSSubPrdItsSeqSifDipoa: TLargeintField
      FieldName = 'SeqSifDipoa'
      DisplayFormat = '000;-000; '
    end
  end
  object DsVSSubPrdIts: TDataSource
    DataSet = QrVSSubPrdIts
    Left = 840
    Top = 513
  end
  object PMPesagem: TPopupMenu
    OnPopup = PMPesagemPopup
    Left = 568
    Top = 636
    object EmitePesagem2: TMenuItem
      Caption = '&Pesagem por receita'
      OnClick = EmitePesagem2Click
      object MenuItem1: TMenuItem
        Caption = '&Emite pesagem'
        OnClick = EmitePesagem1Click
      end
      object Reimprimereceita1: TMenuItem
        Caption = '&Reimprime receita'
        OnClick = Reimprimereceita1Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object ExcluiPesagem1: TMenuItem
        Caption = 'E&xclui Pesagem'
        OnClick = ExcluiPesagem1Click
      end
    end
    object Emiteoutrasbaixas1: TMenuItem
      Caption = '&Outras baixas'
      OnClick = Emiteoutrasbaixas1Click
      object Novogrupo1: TMenuItem
        Caption = 'Nova pesagem'
        OnClick = Novogrupo1Click
      end
      object Novoitem1: TMenuItem
        Caption = 'Novo item'
        OnClick = Novoitem1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Excluiitem1: TMenuItem
        Caption = 'Exclui item'
        OnClick = Excluiitem1Click
      end
      object Excluigrupo1: TMenuItem
        Caption = 'Exclui toda pesagem'
        OnClick = Excluigrupo1Click
      end
    end
    object Recalculacusto1: TMenuItem
      Caption = 'Recalcula custo'
      OnClick = Recalculacusto1Click
    end
  end
  object QrPQO: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQOBeforeClose
    AfterScroll = QrPQOAfterScroll
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR,'
      'emg.Nome NO_EMITGRU, pqo.*'
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru'
      'WHERE pqo.Codigo > 0'
      '')
    Left = 548
    Top = 221
    object QrPQOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQOSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQOCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQOCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQODataB: TDateField
      FieldName = 'DataB'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQONOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQONO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQOEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQONome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQO: TDataSource
    DataSet = QrPQO
    Left = 548
    Top = 265
  end
  object QrPQOIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 596
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQOItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQOItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQOItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQOItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQOItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQOItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQOItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQOItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQOItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQOItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQOItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQOItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object DsPQOIts: TDataSource
    DataSet = QrPQOIts
    Left = 596
    Top = 273
  end
  object QrVSCalInd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 532
    Top = 461
    object QrVSCalIndCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSCalIndControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSCalIndMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSCalIndMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSCalIndMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSCalIndEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSCalIndTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSCalIndCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSCalIndMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSCalIndDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSCalIndPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSCalIndGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSCalIndPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalIndPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalIndAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSCalIndSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSCalIndSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSCalIndSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSCalIndSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalIndSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalIndSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSCalIndSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSCalIndFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSCalIndMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSCalIndFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSCalIndCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSCalIndDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSCalIndDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSCalIndDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSCalIndQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSCalIndQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalIndQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSCalIndQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalIndQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalIndNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSCalIndNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalIndNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSCalIndID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCalIndNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalIndNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCalIndReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSCalIndPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSCalIndMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSCalIndStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSCalIndNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 50
    end
    object QrVSCalIndNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
  end
  object DsVSCalInd: TDataSource
    DataSet = QrVSCalInd
    Left = 532
    Top = 509
  end
end
