unit VSInnIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup,
  UnProjGroup_Consts, UnGrl_Geral, UnAppEnums;

type
  TFmVSInnIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label8: TLabel;
    DBEdFornecedor: TdmkDBEdit;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    Label10: TLabel;
    EdValorMP: TdmkEdit;
    EdFicha: TdmkEdit;
    Label4: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdInfPecas: TdmkEdit;
    EdInfPesoKg: TdmkEdit;
    EdInfValorT: TdmkEdit;
    GroupBox4: TGroupBox;
    Panel6: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    EdPerQbrViag: TdmkEdit;
    EdPerQbrSal: TdmkEdit;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel7: TPanel;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    GroupBox5: TGroupBox;
    Panel8: TPanel;
    GroupBox6: TGroupBox;
    Panel9: TPanel;
    Label18: TLabel;
    EdPesoSalKg: TdmkEdit;
    GroupBox7: TGroupBox;
    Panel10: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EdRstCouPc: TdmkEdit;
    EdRstCouKg: TdmkEdit;
    EdRstCouVl: TdmkEdit;
    GroupBox8: TGroupBox;
    Panel11: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    EdRstSalKg: TdmkEdit;
    EdRstSalVl: TdmkEdit;
    GroupBox9: TGroupBox;
    Panel12: TPanel;
    Label24: TLabel;
    EdRstTotVl: TdmkEdit;
    LaTribDefSel: TLabel;
    EdTribDefSel: TdmkEditCB;
    CBTribDefSel: TdmkDBLookupComboBox;
    QrTribDefCab: TmySQLQuery;
    DsTribDefCab: TDataSource;
    QrTribDefCabCodigo: TIntegerField;
    QrTribDefCabNome: TWideStringField;
    SbTribDefSel: TSpeedButton;
    EdCustoMOKg: TdmkEdit;
    Label25: TLabel;
    Label26: TLabel;
    EdCustoMOTot: TdmkEdit;
    QrGraGruXCouNiv2: TFloatField;
    QrGraGruXGrandeza: TFloatField;
    SbStqCenLoc: TSpeedButton;
    Label29: TLabel;
    EdCusFrtAvuls: TdmkEdit;
    EdValorT: TdmkEdit;
    Label32: TLabel;
    SbCustoComiss: TSpeedButton;
    GBRecuImpost: TGroupBox;
    Panel13: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    EdRpICMS: TdmkEdit;
    EdRvICMS: TdmkEdit;
    EdRpPIS: TdmkEdit;
    EdRvPIS: TdmkEdit;
    EdRpCOFINS: TdmkEdit;
    EdRvCOFINS: TdmkEdit;
    EdRpIPI: TdmkEdit;
    EdRvIPI: TdmkEdit;
    EdCredPereImposto: TdmkEdit;
    EdCredValrImposto: TdmkEdit;
    Label31: TLabel;
    Label30: TLabel;
    EdCusKgComiss: TdmkEdit;
    LaCusKgComiss: TLabel;
    LaCustoComiss: TLabel;
    EdCustoComiss: TdmkEdit;
    SpeedButton1: TSpeedButton;
    QrGraGruXCou: TMySQLQuery;
    QrGraGruXCouPrecoMin: TFloatField;
    QrGraGruXCouPrecoMax: TFloatField;
    SpeedButton2: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdInfValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorMPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRstCouPcRedefinido(Sender: TObject);
    procedure EdRstCouKgRedefinido(Sender: TObject);
    procedure EdRstCouVlRedefinido(Sender: TObject);
    procedure EdRstSalKgRedefinido(Sender: TObject);
    procedure EdRstSalVlRedefinido(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure EdInfPecasRedefinido(Sender: TObject);
    procedure EdInfPesoKgRedefinido(Sender: TObject);
    procedure EdPerQbrViagRedefinido(Sender: TObject);
    procedure EdPerQbrSalRedefinido(Sender: TObject);
    procedure EdPesoSalKgRedefinido(Sender: TObject);
    procedure EdInfValorTRedefinido(Sender: TObject);
    procedure SbTribDefSelClick(Sender: TObject);
    procedure EdCustoMOKgRedefinido(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure EdValorMPRedefinido(Sender: TObject);
    procedure SbCustoComissClick(Sender: TObject);
    procedure EdCredPereImpostoRedefinido(Sender: TObject);
    procedure EdCredValrImpostoRedefinido(Sender: TObject);
    procedure EdCusKgComissRedefinido(Sender: TObject);
    procedure EdCustoComissRedefinido(Sender: TObject);
    procedure EdCusFrtAvulsRedefinido(Sender: TObject);
    procedure EdCusKgComissChange(Sender: TObject);
    // meu
    procedure RecalculaRetTributos(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenVSInnIts(Controle: Integer);
    procedure Restituicao(Restituicao: TVSRestituicaoInn);
    procedure CalculaMO();
    procedure CalculaValorT();
    function  ObtemQtde(var Qtde: Double): Boolean;


  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts, FQrTribIncIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FFornece, FClientMO: Integer;
  end;

  var
  FmVSInnIts: TFmVSInnIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSInnCab, Principal, AppListas, UnVS_CRC_PF, (* UnGrade_PF,*)
{$IfDef sAllVS}UnTributos_PF, {$EndIf}
  ModVS_CRC, GetValor;

{$R *.DFM}

procedure TFmVSInnIts.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  //CustoMOKg  = 0;
  CustoMOM2  = 0;
  //CustoMOTot = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 1;
  FornecMO   = 0;
  NotaMPAG   = 0;
  //
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  //
  InfAreaM2 = 0;
  InfAreaP2 = 0;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  PerceComiss = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Fornecedor, SerieFch, Ficha(*,
  Misturou*), StqCenLoc, ReqMovEstq, TribDefSel, ClientMO: Integer;
  Pecas, PesoKg, RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl,
  RstTotVl, ValorFat, BaseCalc, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  CustoMOKg, CustoMOTot,
  InfPecas, InfPesoKg, InfValorT, PerQbrViag, PerQbrSal, PesoSalKg, //PerceComiss,
  CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls: Double;
  EdPecasX: TdmkEdit;
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI, Preco: Double;
  PrecoTxt: String;
begin
  Codigo          := Geral.IMV(DBEdCodigo.Text);
  Controle        := EdControle.ValueVariant;
  MovimCod        := Geral.IMV(DBEdMovimCod.Text);
  Empresa         := FEmpresa;
  ClienteMO       := FmVSInnCab.QrVSInnCabClienteMO.Value;
  Fornecedor      := Geral.IMV(DBEdFornecedor.Text);
  DataHora        := Geral.FDT(FDataHora, 109);
  MovimID         := emidCompra;
  MovimNiv        := eminSemNiv;
  GraGruX         := EdGragruX.ValueVariant;
  FUltGGX         := GraGruX;
  Pecas           := EdPecas.ValueVariant;
  PesoKg          := EdPesoKg.ValueVariant;
  ValorMP         := EdValorMP.ValueVariant;
  Observ          := EdObserv.Text;
  SerieFch        := EdSerieFch.ValueVariant;
  Ficha           := EdFicha.ValueVariant;
  Marca           := EdMarca.Text;
  PerQbrViag      := EdPerQbrViag.ValueVariant;
  PerQbrSal       := EdPerQbrSal.ValueVariant;
  //Misturou        := RGMisturou.ItemIndex;
  //
  InfPecas        := EdInfPecas.ValueVariant;
  InfPesoKg       := EdInfPesoKg.ValueVariant;
  //InfAreaM2       := EdInfAreaM2.ValueVariant;
  //InfAreaP2       := EdInfAreaP2.ValueVariant;
  InfValorT       := EdInfValorT.ValueVariant;
  //
  StqCenLoc       := EdStqCenLoc.ValueVariant;
  ReqMovEstq      := EdReqMovEstq.ValueVariant;
  //
  RstCouPc        := EdRstCouPc.ValueVariant;
  RstCouKg        := EdRstCouKg.ValueVariant;
  RstCouVl        := EdRstCouVl.ValueVariant;
  RstSalKg        := EdRstSalKg.ValueVariant;
  RstSalVl        := EdRstSalVl.ValueVariant;
  RstTotVl        := EdRstTotVl.ValueVariant;
  //
  TribDefSel      := EdTribDefSel.ValueVariant;
  PesoSalKg       := EdPesoSalKg.ValueVariant;
  //
  ClientMO        := FClientMO;
  CustoMOKg       := EdCustoMOKg.ValueVariant;
  CustoMOTot      := EdCustoMOTot.ValueVariant;
  //
  //PerceComiss     := EdPerceComiss.ValueVariant;
  CusKgComiss     := EdCusKgComiss.ValueVariant;
  CustoComiss     := EdCustoComiss.ValueVariant;
  CredPereImposto := EdCredPereImposto.ValueVariant;

    RpICMS     := EdRpICMS.ValueVariant;
    RpPIS      := EdRpPIS.ValueVariant;
    RpCOFINS   := EdRpCOFINS.ValueVariant;
    RpIPI      := EdRpIPI.ValueVariant;

    RvICMS     := EdRvICMS.ValueVariant;
    RvPIS      := EdRvPIS.ValueVariant;
    RvCOFINS   := EdRvCOFINS.ValueVariant;
    RvIPI      := EdRvIPI.ValueVariant;


  CredValrImposto := EdCredValrImposto.ValueVariant;
  CusFrtAvuls     := EdCusFrtAvuls.ValueVariant;
  //
  ValorT          := EdValorT.ValueVariant;
  //
  if VS_CRC_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha, False) then
    Exit;
  //
  EdPecasX := EdPecas;
  if ((QrGraGruXGrandeza.Value) =  Integer(gumPesoKg))
  and (QrGraGruXCouNiv2.Value = Integer(cn2SubPrd))then
    EdPecasX := nil;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorMP, EdGraGruX, EdPallet, EdFicha, EdPecasX, EdAreaM2,
    EdPesoKg, EdValorMP, ExigeFornecedor, CO_GraGruY_1024_VSNatCad,
    ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  if MyObjects.FIC(Trim(Marca) = '', EdMarca, 'Informe a marca!') then
    Exit;
  //
  if PesoKg > 0 then
    Preco := ValorMP / PesoKg
  else
  if Pecas > 0 then
    Preco := ValorMP / Pecas
  else
    Preco := 0.0000;
  PrecoTxt := Geral.FFT(Preco, 6, siNegativo);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruXCou, Dmod.MyDB, [
  'SELECT PrecoMin, PrecoMax ',
  'FROM gragruxcou gxc ',
  'WHERE GraGruX=' + Geral.FF0(GraGruX),
  '']);
  if QrGraGruXCouPrecoMin.Value > 0 then
    if Preco < QrGraGruXCouPrecoMin.Value then
      if Geral.MB_Pergunta('O pre�o informado: ' + PrecoTxt + sLineBreak +
      ' � MENOR que o M�NIMO: ' + Geral.FFT(QrGraGruXCouPrecoMin.Value, 6, siNegativo) +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
        Exit;
  if QrGraGruXCouPrecoMax.Value > 0 then
    if Preco > QrGraGruXCouPrecoMax.Value then
      if Geral.MB_Pergunta('O pre�o informado: ' + PrecoTxt + sLineBreak +
      ' � MAIOR que o M�XIMO: ' + Geral.FFT(QrGraGruXCouPrecoMax.Value, 6, siNegativo) +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
        Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  PerceComiss, CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls,
  CO_0_GGXRcl,
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei001(*Item de entrada de mat�ria prima*)) then
  begin
    //DmModVS_CRC.AtualizaImeiValorT(Controle);
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
    VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
    VS_CRC_PF.InsereVSMovDif(Controle, Pecas, PesoKg, AreaM2, AreaP2, ValorMP,
    InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT, PerQbrViag, PerQbrSal,
    RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl, RstTotVl, TribDefSel,
    PesoSalKg);
    //
    VS_CRC_PF.InsereVSFchRMPCab(SerieFch, Ficha, Integer(MovimID), Fornecedor);
    FmVSInnCab.LocCod(Codigo, Codigo);
    //
    FmVSInnCab.AtualizaNFeItens();
    //
    if FmVSInnCab.QrVSInnCabClienteMO.Value <> 0 then
    begin
      DmModVS_CRC.AtualizaVSMovIts_CusEmit(Controle);
    end;
    if TribDefSel <> 0 then
    begin
      ReopenVSInnIts(Controle);
{$IfDef sAllVS}
      if (ImgTipo.SQLType = stIns) or (FQrTribIncIts.RecordCount = 0) then
        Tributos_PF.Tributos_Insere(VAR_FATID_1003, Codigo, Controle, Empresa,
        TribDefSel, InfValorT, InfValorT, FDataHora, siPositivo)
      else
      begin
        //
        ValorFat := ValorMP;
        BaseCalc := ValorMP;
        Tributos_PF.Tributos_GerenciaAlt(FQrTribIncIts, ValorFat, BaseCalc);
      end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
    end;
    //
    ReopenVSInnIts(Controle);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdObserv.Text              := '';
      EdFicha.ValueVariant       := 0;
      EdMarca.ValueVariant       := '';
      //
      EdInfPecas.ValueVariant    := 0;
      EdInfPesoKg.ValueVariant   := 0;
      //EdInfAreaM2.ValueVariant := 0;
      //EdInfAreaP2.ValueVariant := 0;
      EdInfValorT.ValueVariant   := 0;
      //
      EdRstCouPc.ValueVariant    := 0;
      EdRstCouKg.ValueVariant    := 0;
      EdRstCouVl.ValueVariant    := 0;
      EdRstSalKg.ValueVariant    := 0;
      EdRstSalVl.ValueVariant    := 0;
      EdRstTotVl.ValueVariant    := 0;
      //
      EdGraGruX.Enabled  := True;
      CBGraGruX.Enabled  := True;
      EdSerieFch.Enabled := True;
      CBSerieFch.Enabled := True;
      EdFicha.Enabled    := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSInnIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnIts.CalculaMO;
begin
  EdCustoMOTot.ValueVariant := EdCustoMOKg.ValueVariant * EdPesoKg.ValueVariant;
end;

procedure TFmVSInnIts.CalculaValorT();
var
  //CusKgComiss,
  CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls,
  ValorMP, ValorT: Double;
begin
  //CusKgComiss       := EdCusKgComiss.ValueVariant;
  CustoComiss       := EdCustoComiss.ValueVariant;
  CredPereImposto   := EdCredPereImposto.ValueVariant;
  CusFrtAvuls       := EdCusFrtAvuls.ValueVariant;
  ValorMP           := EdValorMP.ValueVariant;
  ValorT            := EdValorT.ValueVariant;
  //
  CredValrImposto   := ValorMP * CredPereImposto / 100; // Sem arredondar
  EdCredValrImposto.ValueVariant := CredValrImposto;    // Arredonda
  CredValrImposto   := EdCredValrImposto.ValueVariant;  // Arredondado!
  //
  ValorT := ValorMP - CredValrImposto + CustoComiss + CusFrtAvuls;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSInnIts.EdPesoSalKgRedefinido(Sender: TObject);
begin
  Restituicao(rivsSalKg);
end;

procedure TFmVSInnIts.EdCredPereImpostoRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.EdCredValrImpostoRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.EdCusFrtAvulsRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.EdCusKgComissChange(Sender: TObject);
begin
  if EdCusKgComiss.Focused then
    EdCustoComiss.ValueVariant := EdPesoKg.ValueVariant * EdCusKgComiss.ValueVariant;
end;

procedure TFmVSInnIts.EdCusKgComissRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.EdCustoComissRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.EdCustoMOKgRedefinido(Sender: TObject);
begin
  CalculaMO();
end;

procedure TFmVSInnIts.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ficha: Integer;
begin
  if Key = VK_F4 then
    if VS_CRC_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
end;

procedure TFmVSInnIts.EdInfPecasRedefinido(Sender: TObject);
begin
  Restituicao(rivsPecas);
end;

procedure TFmVSInnIts.EdInfPesoKgRedefinido(Sender: TObject);
begin
  Restituicao(rivsPesoKg);
end;

procedure TFmVSInnIts.EdInfValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Peso, Pecs: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Peso  := EdInfPesoKg.ValueVariant;
      Pecs  := EdInfPecas.ValueVariant;
      if Peso >= 0.01 then
        EdInfValorT.ValueVariant := Peso * Preco
      else
        EdInfValorT.ValueVariant := Pecs * Preco
    end;
  end;
  if EdValorMP.ValueVariant < 0.01 then
    EdValorMP.ValueVariant := EdInfValorT.ValueVariant;
end;

procedure TFmVSInnIts.EdInfValorTRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if (Key = VK_F5) then
  begin
    if ObtemQtde(Valor) then
    begin
      EdPecas.ValueVariant := EdPecas.ValueVariant + Valor;
      EdPesoKg.SetFocus;
    end;
  end;
end;

procedure TFmVSInnIts.EdPecasRedefinido(Sender: TObject);
begin
  Restituicao(rivsPecas);
end;

procedure TFmVSInnIts.EdPerQbrSalRedefinido(Sender: TObject);
begin
  Restituicao(rivsSalKg);
end;

procedure TFmVSInnIts.EdPerQbrViagRedefinido(Sender: TObject);
begin
  Restituicao(rivsPesoKg);
end;

procedure TFmVSInnIts.EdPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if (Key = VK_F5) then
  begin
    if ObtemQtde(Valor) then
    begin
      EdPesoKg.ValueVariant := EdPesoKg.ValueVariant + Valor;
      EdValorMP.SetFocus;
    end;
  end;
end;

procedure TFmVSInnIts.EdPesoKgRedefinido(Sender: TObject);
begin
  if EdCusKgComiss.Enabled then
    EdCustoComiss.ValueVariant := EdPesoKg.ValueVariant * EdCusKgComiss.ValueVariant;
  //
  Restituicao(rivsPesoKg);
  CalculaMO();
end;

procedure TFmVSInnIts.EdRstCouKgRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdRstCouPcRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdRstCouVlRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdRstSalKgRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdRstSalVlRedefinido(Sender: TObject);
begin
  Restituicao(rivsValor);
end;

procedure TFmVSInnIts.EdValorMPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Peso, Pecs: Double;
var
  Valor: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Peso  := EdPesoKg.ValueVariant;
      Pecs  := EdPecas.ValueVariant;
      if Peso >= 0.01 then
        EdValorMP.ValueVariant := Peso * Preco
      else
        EdValorMP.ValueVariant := Pecs * Preco
    end;
  end;
  if (Key = VK_F5) then
  begin
    if ObtemQtde(Valor) then
    begin
      EdValorMP.ValueVariant := EdValorMP.ValueVariant + Valor;
    end;
  end;
end;

procedure TFmVSInnIts.EdValorMPRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSInnIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdFornecedor.DataSource := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));

  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, TEstqMovimID.emidCompra);
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreQuery(QrTribDefCab, Dmod.MyDB);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSInnIts.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
  CasasDecimais: Integer;
begin
  Qtde          := 0;
  Result        := False;
  CasasDecimais := 3;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, CasasDecimais, 0, '', '', True, 'Quantidade a Acrescentar', 'Informe a quantidade a acrescentar: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    Result := True;
  end;
end;

procedure TFmVSInnIts.RecalculaRetTributos(Sender: TObject);
var
  ValorMP, RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
  CredPereImposto, CredValrImposto: Double;
begin
  ValorMP := EdValorMP.ValueVariant;
  //
    RpICMS     := EdRpICMS.ValueVariant;
    RpPIS      := EdRpPIS.ValueVariant;
    RpCOFINS   := EdRpCOFINS.ValueVariant;
    RpIPI      := EdRpIPI.ValueVariant;

    RvICMS     := Geral.RoundC(RpICMS   * ValorMP / 100, 2); //EdRvICMS.ValueVariant;
    RvPIS      := Geral.RoundC(RpPIS    * ValorMP / 100, 2); //EdRvPIS.ValueVariant;
    RvCOFINS   := Geral.RoundC(RpCOFINS * ValorMP / 100, 2); //EdRvCOFINS.ValueVariant;
    RvIPI      := Geral.RoundC(RpIPI    * ValorMP / 100, 2); //EdRvIPI.ValueVariant;

    EdRvICMS.ValueVariant   := RvICMS;
    EdRvPIS.ValueVariant    := RvPIS;
    EdRvCOFINS.ValueVariant := RvCOFINS;
    EdRvIPI.ValueVariant    := RvIPI;

  CredPereImposto := RpICMS + RpPIS + RpCOFINS + RpIPI;
  CredValrImposto := RvICMS + RvPIS + RvCOFINS + RvIPI;

  EdCredPereImposto.ValueVariant := CredPereImposto;
  EdCredValrImposto.ValueVariant := CredValrImposto;
end;

procedure TFmVSInnIts.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSInnIts.Restituicao(Restituicao: TVSRestituicaoInn);
var
  Pecas, PesoKg, SalKg, Aceito, Perc, (*Valor, *)Preco, ValorCou, ValorSal: Double;
begin
  case Restituicao of
   rivsPecas:
   begin
     Pecas := EdPecas.ValueVariant - EdInfPecas.ValueVariant;
     if Pecas < 0 then
       Pecas := 0;
     EdRstCouPc.ValueVariant := Pecas;
   end;
   rivsPesoKg:
   begin
     PesoKg := EdInfPesoKg.ValueVariant - EdPesoKg.ValueVariant;
     Perc   := EdPerQbrViag.ValueVariant;
     Aceito := Perc * EdInfPesoKg.ValueVariant / 100;
     PesoKg := PesoKg - Aceito;
     if PesoKg < 0 then
       PesoKg := 0;
     EdRstCouKg.ValueVariant := PesoKg;
   end;
   rivsSalKg:
   begin
     SalKg := EdPesoSalKg.ValueVariant;
     Perc   := EdPerQbrSal.ValueVariant;
     Aceito := Perc * EdInfPesoKg.ValueVariant / 100;
     SalKg := SalKg - Aceito;
     if SalKg < 0 then
       SalKg := 0;
     EdRstSalKg.ValueVariant := SalKg;
   end;
   rivsValor:
   begin
     PesoKg := EdRstCouKg.ValueVariant;
     if (PesoKg > 0) and (EdInfPesoKg.ValueVariant > 0) then
       Preco := EdInfValorT.ValueVariant / EdInfPesoKg.ValueVariant
     else
       Preco := 0;
     ValorCou := Preco * PesoKg;
     EdRstCouVl.ValueVariant := ValorCou;
     //
     SalKg := EdRstSalKg.ValueVariant;
     ValorSal := SalKg * Preco;
     EdRstSalVl.ValueVariant := ValorSal;
     //
     EdRstTotVl.ValueVariant := ValorCou + ValorSal;
   end;
  end;
end;

procedure TFmVSInnIts.SbCustoComissClick(Sender: TObject);
const
  Casas       = 2;
  LeftZeros   = 0;
  ValMin      = '';
  ValMax      = '';
  Obrigatorio = False;
  FormCaption = 'Comiss�o';
  ValCaption  = 'Comiss�o total';
  WidthVal    = 100;
var
  Valor: Variant;
  CustoComiss, PesoKg: Double;
begin
  Valor := EdCustoComiss.ValueVariant;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Valor, Casas, LeftZeros, ValMin, ValMax, Obrigatorio, FormCaption,
  ValCaption, WidthVal, Valor) then
  begin
    LaCusKgComiss.Enabled := False;
    EdCusKgComiss.Enabled := False;
    //
    LaCustoComiss.Enabled := True;
    EdCustoComiss.Enabled := True;
    //
    CustoComiss := Valor;
    PesoKg      := EdPesoKg.ValueVariant;
    if PesoKg > 0 then
      EdCusKgComiss.ValueVariant := CustoComiss / PesoKg;
    EdCustoComiss.ValueVariant := CustoComiss;
  end;
end;

procedure TFmVSInnIts.SbStqCenLocClick(Sender: TObject);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidCompra);
end;

procedure TFmVSInnIts.SbTribDefSelClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
{$IfDef sAllVS}
  Tributos_PF.MostraFormTribDefCab(EdTribDefSel.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdTribDefSel, CBTribDefSel, QrTribDefCab, VAR_CADASTRO);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnIts.SpeedButton1Click(Sender: TObject);
begin
  EdInfPecas.ValueVariant := EdPecas.ValueVariant;
  EdInfPesoKg.ValueVariant := EdPesoKg.ValueVariant;
  EdInfValorT.ValueVariant := EdValorMP.ValueVariant;
end;

procedure TFmVSInnIts.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
    EdFicha.ReadOnly := False;
end;

end.
