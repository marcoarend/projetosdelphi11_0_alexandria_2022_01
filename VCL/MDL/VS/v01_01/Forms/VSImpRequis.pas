unit VSImpRequis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  frxClass, StdCtrls, dmkGeral, UnDmkProcFunc, UnMyObjects, UnDmkEnums, Data.DB,
  mySQLDbTables, frxDBSet, AppListas, UnProjGroup_Consts, UnGrl_Consts, frxCross;

type
  TFmVSImpRequis = class(TForm)
    frxUdRME: TfrxUserDataSet;
    frxRME1: TfrxReport;
    QrReqIts: TmySQLQuery;
    QrReqItsNO_EstqMovimID: TWideStringField;
    QrReqItsNO_MovimID: TWideStringField;
    QrReqItsNO_MovimNiv: TWideStringField;
    QrReqItsNO_LOC_CEN: TWideStringField;
    frxWET_CURTI_117_01: TfrxReport;
    frxDsReqIts: TfrxDBDataset;
    QrReqItsNO_SERIE_FICHA: TWideStringField;
    QrReqItsInteiros: TFloatField;
    QrVSAjsItsCodigo: TLargeintField;
    QrVSAjsItsControle: TLargeintField;
    QrVSAjsItsMovimCod: TLargeintField;
    QrVSAjsItsMovimNiv: TLargeintField;
    QrVSAjsItsMovimTwn: TLargeintField;
    QrVSAjsItsEmpresa: TLargeintField;
    QrVSAjsItsTerceiro: TLargeintField;
    QrVSAjsItsCliVenda: TLargeintField;
    QrVSAjsItsMovimID: TLargeintField;
    QrVSAjsItsDataHora: TDateTimeField;
    QrVSAjsItsPallet: TLargeintField;
    QrVSAjsItsGraGruX: TLargeintField;
    QrVSAjsItsPecas: TFloatField;
    QrVSAjsItsPesoKg: TFloatField;
    QrVSAjsItsAreaM2: TFloatField;
    QrVSAjsItsAreaP2: TFloatField;
    QrVSAjsItsValorT: TFloatField;
    QrVSAjsItsSrcMovID: TLargeintField;
    QrVSAjsItsSrcNivel1: TLargeintField;
    QrVSAjsItsSrcNivel2: TLargeintField;
    QrVSAjsItsSrcGGX: TLargeintField;
    QrVSAjsItsSdoVrtPeca: TFloatField;
    QrVSAjsItsSdoVrtPeso: TFloatField;
    QrVSAjsItsSdoVrtArM2: TFloatField;
    QrVSAjsItsObserv: TWideStringField;
    QrVSAjsItsSerieFch: TLargeintField;
    QrVSAjsItsFicha: TLargeintField;
    QrVSAjsItsMisturou: TLargeintField;
    QrVSAjsItsFornecMO: TLargeintField;
    QrVSAjsItsCustoMOKg: TFloatField;
    QrVSAjsItsCustoMOM2: TFloatField;
    QrVSAjsItsCustoMOTot: TFloatField;
    QrVSAjsItsValorMP: TFloatField;
    QrVSAjsItsDstMovID: TLargeintField;
    QrVSAjsItsDstNivel1: TLargeintField;
    QrVSAjsItsDstNivel2: TLargeintField;
    QrVSAjsItsDstGGX: TLargeintField;
    QrVSAjsItsQtdGerPeca: TFloatField;
    QrVSAjsItsQtdGerPeso: TFloatField;
    QrVSAjsItsQtdGerArM2: TFloatField;
    QrVSAjsItsQtdGerArP2: TFloatField;
    QrVSAjsItsQtdAntPeca: TFloatField;
    QrVSAjsItsQtdAntPeso: TFloatField;
    QrVSAjsItsQtdAntArM2: TFloatField;
    QrVSAjsItsQtdAntArP2: TFloatField;
    QrVSAjsItsNotaMPAG: TFloatField;
    QrVSAjsItsNO_PALLET: TWideStringField;
    QrVSAjsItsNO_PRD_TAM_COR: TWideStringField;
    QrVSAjsItsID_TTW: TLargeintField;
    QrVSAjsItsNO_TTW: TWideStringField;
    QrReqItsStqCenLoc: TLargeintField;
    QrReqItsReqMovEstq: TLargeintField;
    frxRME2: TfrxReport;
    frxRDC1: TfrxReport;
    frxUdRDC: TfrxUserDataSet;
    frxFER: TfrxReport;
    frxICR1: TfrxReport;
    frxISC1: TfrxReport;
    frxIEC1: TfrxReport;
    frxLSP: TfrxReport;
    frxIPM1: TfrxReport;
    procedure frxRME1GetValue(const VarName: String; var Value: Variant);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_117_01GetValue(const VarName: string;
      var Value: Variant);
    procedure frxRDC1GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    sl: TStringList;
  public
    { Public declarations }
    FEmpresa_Cod, FQuantidade, FNumInicial: Integer;
    FEmpresa_Txt, FResponsa: String;
    //
    FTP17DataIni_Date, FTP17DataFim_Date: TdateTime;
    FCk17DataIni_Checked, FCk17DataFim_Checked: Boolean;
    FNumVias: Integer;
    FReduzido: Boolean;
    //
    procedure ImprimeRME();
    procedure ImprimeRDC();
    procedure ImprimeICR();
    procedure ImprimeISC();
    procedure ImprimeIEC();
    procedure ImprimeLSP();
    procedure ImprimeIPM();
    procedure ImprimeFichaEstoque();
    procedure ImprimeListaRequisicoes(UsaMortoVMI: Boolean);
    procedure ReopenReqIts(UsaMortoVMI: Boolean);
  end;

var
  FmVSImpRequis: TFmVSImpRequis;

implementation

uses ModuleGeral, UMySQLModule, Module, DmkDAC_PF, UnVS_PF;

{$R *.DFM}

procedure TFmVSImpRequis.FormCreate(Sender: TObject);
begin
  sl := TStringList.Create;
end;

procedure TFmVSImpRequis.frxRDC1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if CompareText(VarName, 'element') = 0 then
    Value := sl[frxUdRDC.RecNo];
end;

procedure TFmVSImpRequis.frxRME1GetValue(const VarName: String; var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if CompareText(VarName, 'element') = 0 then
    Value := sl[frxUdRME.RecNo];
end;

procedure TFmVSImpRequis.frxWET_CURTI_117_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(FTP17DataIni_Date, FTP17DataFim_Date,
    FCk17DataIni_Checked, FCk17DataFim_Checked, '', 'at�', '')

end;

procedure TFmVSImpRequis.ImprimeFichaEstoque();
begin
  MyObjects.frxMostra(frxFER, 'Ficha de Estoque de Raspas');
end;

procedure TFmVSImpRequis.ImprimeICR();
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3));
(*
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
*)
  end;
  frxUDRME.RangeEnd := reCount;
  frxUDRME.RangeEndCount := sl.Count;
(*  if FReduzido then
    MyObjects.frxMostra(frxICR2, 'Informa��o de Classe/Reclasse de Couros')
  else*)
    MyObjects.frxMostra(frxICR1, 'Informa��o de Classe/Reclasse de Couros');
  //
end;

procedure TFmVSImpRequis.ImprimeIEC;
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3));
(*
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
*)
  end;
  frxUDRME.RangeEnd := reCount;
  frxUDRME.RangeEndCount := sl.Count;
(*  if FReduzido then
    MyObjects.frxMostra(frxIEC2, 'Informa��o de Classe/Reclasse de Couros')
  else*)
    MyObjects.frxMostra(frxIEC1, 'Informa��o de Entrada de Couros');
  //
end;

procedure TFmVSImpRequis.ImprimeIPM();
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3));
(*
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
*)
  end;
  frxUDRME.RangeEnd := reCount;
  frxUDRME.RangeEndCount := sl.Count;
(*  if FReduzido then
    MyObjects.frxMostra(frxIPM2, 'Informa��o de Classe/Reclasse de Couros')
  else*)
    MyObjects.frxMostra(frxIPM1, 'Informa��o de Pallets Movimentados');
  //
end;

procedure TFmVSImpRequis.ImprimeISC;
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3));
(*
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
*)
  end;
  frxUDRME.RangeEnd := reCount;
  frxUDRME.RangeEndCount := sl.Count;
(*  if FReduzido then
    MyObjects.frxMostra(frxISC2, 'Informa��o de Classe/Reclasse de Couros')
  else*)
    MyObjects.frxMostra(frxISC1, 'Informa��o de Sa�da de Couros');
  //
end;

procedure TFmVSImpRequis.ImprimeListaRequisicoes(UsaMortoVMI: Boolean);
begin
  ReopenReqIts(UsaMortoVMI);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_117_01, [
  DmodG.frxDsDono,
  frxDsReqIts
  ]);
  //
  MyObjects.frxMostra(frxWET_CURTI_117_01, 'Lista de Requis��es de Movimenta��o de Estoque');
end;

procedure TFmVSImpRequis.ImprimeLSP();
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3));
(*
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
*)
  end;
  frxUDRME.RangeEnd := reCount;
  frxUDRME.RangeEndCount := sl.Count;
(*  if FReduzido then
    MyObjects.frxMostra(frxLSP2, 'Informa��o de Classe/Reclasse de Couros')
  else*)
    MyObjects.frxMostra(frxLSP, 'Lista Sequencial de Pallets');
  //
end;

procedure TFmVSImpRequis.ImprimeRDC();
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 1� via');
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
  end;
  frxUdRDC.RangeEnd := reCount;
  frxUdRDC.RangeEndCount := sl.Count;
  if FReduzido then
    //MyObjects.frxMostra(frxRDC1, 'Requis��es de Divis�o de Couros')
  else
    MyObjects.frxMostra(frxRDC1, 'Requis��es de Divis�o de Couros');
end;

procedure TFmVSImpRequis.ImprimeRME();
var
  I: Integer;
begin
  sl.Clear;
  for I := 0 to FQuantidade -1 do
  begin
    sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 1� via');
    if FNumVias > 1 then
      sl.Add(Geral.FFF(FNumInicial + I, 3) + ' - 2� via');
  end;
  frxUdRME.RangeEnd := reCount;
  frxUdRME.RangeEndCount := sl.Count;
  if FReduzido then
    MyObjects.frxMostra(frxRME2, 'Requis��es de Movimenta��o de Couros')
  else
    MyObjects.frxMostra(frxRME1, 'Requis��es de Movimenta��o de Couros');
  //
end;

procedure TFmVSImpRequis.ReopenReqIts(UsaMortoVMI: Boolean);
var
  SQL_Periodo, ATT_MovimID, ATT_MovimNiv: String;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
    FTP17DataIni_Date, FTP17DataFim_Date, FCk17DataIni_Checked, FCk17DataFim_Checked);
  //
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrReqIts, Dmod.MyDB, [
  'SELECT vmi.*,',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  'FROM v s m o v i t s vmi',   // ' + CO_???_TAB_VMI + '
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'WHERE vmi.ReqMovEstq <> 0 ',
  SQL_Periodo,
  'ORDER BY vmi.ReqMovEstq, vmi.Controle ',
  '']);
*)
  TemIMEIMrt := dmkPF.EscolhaDe2Int(UsaMortoVMI, 1, 0);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) AS DECIMAL(15,3)) Inteiros , ',
  'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  //'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.ReqMovEstq <> 0 ',
  SQL_Periodo,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrReqIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY ReqMovEstq, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrReqIts);
  //
end;

end.
