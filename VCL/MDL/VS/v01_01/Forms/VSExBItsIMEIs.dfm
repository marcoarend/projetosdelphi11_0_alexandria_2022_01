object FmVSExBItsIMEIs: TFmVSExBItsIMEIs
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-080 :: Baixas Extras de IMEIs Completos'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 499
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 499
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 467
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 259
          Height = 482
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 450
          object DBGGraGruY: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 259
            Height = 271
            Align = alClient
            DataSource = DsGraGruY
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnAfterMultiselect = DBGGraGruYAfterMultiselect
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 239
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 0
            Top = 376
            Width = 259
            Height = 106
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitTop = 344
            object Label2: TLabel
              Left = 8
              Top = 4
              Width = 140
              Height = 13
              Caption = 'Data / hora "informada aqui":'
            end
            object Label1: TLabel
              Left = 8
              Top = 44
              Width = 123
              Height = 13
              Caption = 'Senha p/ uso IME-I vago:'
            end
            object CkContinuar: TCheckBox
              Left = 8
              Top = 84
              Width = 141
              Height = 17
              Caption = 'Continuar inserindo.'
              TabOrder = 0
            end
            object TPDataSenha: TdmkEditDateTimePicker
              Left = 7
              Top = 20
              Width = 112
              Height = 21
              Date = 42131.000000000000000000
              Time = 0.794453055546910000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdHoraSenha: TdmkEdit
              Left = 122
              Top = 20
              Width = 61
              Height = 21
              TabOrder = 2
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '20:00:00'
              QryCampo = 'DtCompra'
              UpdCampo = 'DtCompra'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.833333333333333400
              ValWarn = False
            end
            object EdSenha: TEdit
              Left = 6
              Top = 61
              Width = 125
              Height = 20
              Font.Charset = SYMBOL_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              PasswordChar = 'l'
              TabOrder = 3
            end
          end
          object RGDataHora: TRadioGroup
            Left = 0
            Top = 271
            Width = 259
            Height = 105
            Align = alBottom
            Caption = ' Fonte da data / hora de baixa:'
            ItemIndex = 3
            Items.Strings = (
              'N'#227'o definido'
              'Do cabe'#231'alho'
              'Informada aqui (abaixo)'
              'Conforme '#250'ltima movimenta'#231#227'o de cada item')
            TabOrder = 2
            OnClick = RGDataHoraClick
            ExplicitTop = 239
          end
        end
        object Panel8: TPanel
          Left = 261
          Top = 15
          Width = 745
          Height = 482
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = 360
          ExplicitTop = 80
          ExplicitWidth = 185
          ExplicitHeight = 41
          object DBGIMEIs: TdmkDBGridZTO
            Left = 0
            Top = 41
            Width = 745
            Height = 441
            Align = alClient
            DataSource = DsIMEIs
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDrawColumnCell = DBGIMEIsDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IMEI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SerieFch'
                Title.Caption = 'S'#233'rie'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'MediaSdoVrtArM2'
                Title.Caption = 'M'#233'd.sdo m'#178'/pc'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FaixaMediaM2'
                Title.Caption = 'Media?'
                Title.Font.Charset = ANSI_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DstGGX'
                Title.Caption = 'Reduzido'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigo'
                Width = 245
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoVrtPeca'
                Title.Caption = 'Sdo Pe'#231'as'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SdoVrtArM2'
                Title.Caption = 'Sdo '#193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoVrtPeso'
                Title.Caption = 'Sdo Peso kg'
                Visible = True
              end>
          end
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 745
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitLeft = 84
            ExplicitTop = 184
            ExplicitWidth = 185
            object Label3: TLabel
              Left = 176
              Top = 0
              Width = 86
              Height = 13
              Caption = 'M'#225'ximo de pe'#231'as:'
            end
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 165
              Height = 41
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object RGPosiNega: TRadioGroup
                Left = 0
                Top = 0
                Width = 165
                Height = 41
                Align = alClient
                Caption = ' Estoque: '
                Columns = 2
                Enabled = False
                ItemIndex = 0
                Items.Strings = (
                  'Positivo'
                  'Negativo')
                TabOrder = 0
                OnClick = RGPosiNegaClick
                ExplicitWidth = 745
              end
            end
            object EdMaxPecas: TdmkEdit
              Left = 176
              Top = 16
              Width = 89
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '10'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 10
              ValWarn = False
              OnRedefinido = EdMaxPecasRedefinido
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 547
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 736
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtNenhumClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 616
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Todos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtProvisorio: TBitBtn
        Left = 648
        Top = -8
        Width = 75
        Height = 25
        Caption = 'Provisorio'
        TabOrder = 3
        Visible = False
        OnClick = BtProvisorioClick
      end
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 92
    Top = 232
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 92
    Top = 280
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.SdoVrtArM2 / vmi.SdoVrtPeca  MediaSdoVrtArM2,'
      'CASE '
      'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca < cou.MediaMinM2 THEN 0.000'
      'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca > cou.MediaMaxM2 THEN 2.000'
      'ELSE 1.000 END FaixaMediaM2,'
      'CONCAT(gg1.Nome,       '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vmi.* '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX       '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC       '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad       '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI       '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'WHERE vmi.SdoVrtPeca>0'
      'AND ggx.GraGruY IN (2048)'
      'ORDER BY vmi.Controle')
    Left = 524
    Top = 376
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEIsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEIsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEIsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrIMEIsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrIMEIsPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrIMEIsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrIMEIsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrIMEIsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEIsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEIsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEIsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEIsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEIsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrIMEIsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEIsSerieFch: TIntegerField
      FieldName = 'SerieFch'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsFicha: TIntegerField
      FieldName = 'Ficha'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrIMEIsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrIMEIsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrIMEIsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrIMEIsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrIMEIsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrIMEIsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrIMEIsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEIsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEIsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrIMEIsMediaSdoVrtArM2: TFloatField
      FieldName = 'MediaSdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrIMEIsFaixaMediaM2: TFloatField
      FieldName = 'FaixaMediaM2'
    end
    object QrIMEIsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrIMEIsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrIMEIsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 524
    Top = 424
  end
  object PMProvisorio: TPopupMenu
    Left = 740
    Top = 348
    object ObterIMEIs11: TMenuItem
      Caption = 'ObterIMEIs'
      OnClick = ObterIMEIs11Click
    end
    object Selecionarimeisfixos1: TMenuItem
      Caption = 'Selecionar imeis fixos'
      OnClick = Selecionarimeisfixos1Click
    end
  end
  object QrLastMov: TMySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 168
    object QrLastMovDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
end
