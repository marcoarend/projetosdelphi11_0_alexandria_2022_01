unit VSDsnSub;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmVSDsnSub = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdBasNota: TdmkEdit;
    Label7: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    RGInteresse: TdmkRadioGroup;
    EdSubClass: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSDsnSub(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSDsnSub: TFmVSDsnSub;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnDmkProcFunc;

{$R *.DFM}

procedure TFmVSDsnSub.BtOKClick(Sender: TObject);
var
  Codigo, Controle, GraGruX(*, Interesse*): Integer;
  SubClass: String;
  //BasNota: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  //BasNota        := EdBasNota.ValueVariant;
  //Interesse      := RGInteresse.ItemIndex;
  SubClass       := dmkPF.SoTextoLayout(EdSubClass.Text);
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina um artigo!') then
    Exit;
(*
  if MyObjects.FIC(Interesse = 0, RGInteresse, 'Defina se o cliente vai levar o couro!') then
    Exit;
*)
  //
  Controle       := UMyMod.BPGS1I32('vsdsnsub', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsdsnsub', False, [
    'Codigo', 'GraGruX'(*, 'BasNota',
    'Interesse'*), 'SubClass'], [
    'Controle'], [
    Codigo, GraGruX(*, BasNota,
    Interesse*), SubClass], [
    Controle], True) then
    begin
      ReopenVSDsnSub(Controle);
      if CkContinuar.Checked then
      begin
        ImgTipo.SQLType          := stIns;
        EdControle.ValueVariant  := 0;
        EdGraGruX.ValueVariant   := 0;
        CBGraGruX.KeyValue       := Null;
        EdBasNota.ValueVariant   := 0;
        RGInteresse.ItemIndex    := 2;
        EdSubClass.Text          := '';
        //
        EdGraGruX.SetFocus;
      end else Close;
    end;
  except
    Geral.MB_Aviso('Sigla n�o pode ser duplicada!');
    raise;
  end;
end;

procedure TFmVSDsnSub.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSDsnSub.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSDsnSub.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmVSDsnSub.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSDsnSub.ReopenVSDsnSub(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSDsnSub.SpeedButton1Click(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
}
end;

end.
