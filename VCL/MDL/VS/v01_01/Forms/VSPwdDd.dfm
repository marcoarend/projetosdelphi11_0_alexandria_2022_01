object FmVSPwdDd: TFmVSPwdDd
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-139 :: Senha VS do Dia'
  ClientHeight = 219
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 461
    object GB_R: TGroupBox
      Left = 523
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 413
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 475
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 365
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 207
        Height = 32
        Caption = 'Senha VS do Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 207
        Height = 32
        Caption = 'Senha VS do Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 207
        Height = 32
        Caption = 'Senha VS do Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 571
    Height = 57
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 461
    ExplicitHeight = 115
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 571
      Height = 57
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 461
      ExplicitHeight = 115
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 571
        Height = 57
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 461
        ExplicitHeight = 115
        object Label2: TLabel
          Left = 20
          Top = 8
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label1: TLabel
          Left = 252
          Top = 8
          Width = 66
          Height = 13
          Caption = 'Senha do dia:'
        end
        object SpeedButton1: TSpeedButton
          Left = 532
          Top = 24
          Width = 23
          Height = 22
          Caption = '+'
          OnClick = SpeedButton1Click
        end
        object EdVSPwdDdUser: TdmkEditCB
          Left = 20
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CbVSPwdDdUser
          IgnoraDBLookupComboBox = False
        end
        object CbVSPwdDdUser: TdmkDBLookupComboBox
          Left = 76
          Top = 24
          Width = 172
          Height = 21
          KeyField = 'Numero'
          ListField = 'Login'
          ListSource = DsSenhas
          TabOrder = 1
          dmkEditCB = EdVSPwdDdUser
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdVSPwdDdClas: TdmkEdit
          Left = 252
          Top = 24
          Width = 278
          Height = 21
          MaxLength = 30
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 105
    Width = 571
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 163
    ExplicitWidth = 461
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 567
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 457
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 149
    Width = 571
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 207
    ExplicitWidth = 461
    object PnSaiDesis: TPanel
      Left = 425
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 315
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 423
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 313
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Login'
      'FROM senhas'
      'ORDER BY Numero')
    Left = 424
    Top = 40
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 424
    Top = 88
  end
  object QrControle: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VSPwdDdClas, VSPwdDdData, VSPwdDdUser '
      'FROM controle '
      'WHERE Codigo=1 ')
    Left = 216
    Top = 132
    object QrControleVSPwdDdClas: TWideStringField
      FieldName = 'VSPwdDdClas'
      Size = 30
    end
    object QrControleVSPwdDdData: TDateField
      FieldName = 'VSPwdDdData'
    end
    object QrControleVSPwdDdUser: TIntegerField
      FieldName = 'VSPwdDdUser'
    end
  end
end
