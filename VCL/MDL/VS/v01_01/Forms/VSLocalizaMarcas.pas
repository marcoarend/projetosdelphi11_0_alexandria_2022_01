unit VSLocalizaMarcas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Menus, UnGrl_Consts;

type
  TFmVSLocalizaMarcas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    EdPesq: TdmkEdit;
    QrVSMovIts: TMySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_EstqMovimID: TWideStringField;
    QrVSMovItsNO_DstMovID: TWideStringField;
    QrVSMovItsNO_SrcMovID: TWideStringField;
    QrVSMovItsNO_LOC_CEN: TWideStringField;
    QrVSMovItsGraGru1: TLargeintField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TLargeintField;
    QrVSMovItsPedItsFin: TLargeintField;
    QrVSMovItsPedItsVda: TLargeintField;
    QrVSMovItsStqCenLoc: TLargeintField;
    QrVSMovItsItemNFe: TLargeintField;
    QrVSMovItsLnkNivXtr1: TLargeintField;
    QrVSMovItsLnkNivXtr2: TLargeintField;
    QrVSMovItsClientMO: TLargeintField;
    QrVSMovItsCustoPQ: TFloatField;
    QrVSMovItsVSMulFrnCab: TLargeintField;
    QrVSMovItsNFeSer: TLargeintField;
    QrVSMovItsNFeNum: TLargeintField;
    QrVSMovItsVSMulNFeCab: TLargeintField;
    QrVSMovItsJmpMovID: TLargeintField;
    QrVSMovItsJmpNivel1: TLargeintField;
    QrVSMovItsJmpNivel2: TLargeintField;
    QrVSMovItsRmsMovID: TLargeintField;
    QrVSMovItsRmsNivel1: TLargeintField;
    QrVSMovItsRmsNivel2: TLargeintField;
    QrVSMovItsGGXRcl: TLargeintField;
    QrVSMovItsRmsGGX: TLargeintField;
    QrVSMovItsJmpGGX: TLargeintField;
    QrVSMovItsDtCorrApo: TDateTimeField;
    QrVSMovItsCustoM2: TFloatField;
    QrVSMovItsCustoKg: TFloatField;
    DsVSMovIts: TDataSource;
    BtIMEI: TBitBtn;
    PMIMEI: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    Irparajaneladomovimento1: TMenuItem;
    MostrarjaneladoIMEI1: TMenuItem;
    RGPrecisao: TRadioGroup;
    DGDados: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure QrVSMovItsCalcFields(DataSet: TDataSet);
    procedure EdPesqChange(Sender: TObject);
    procedure QrVSMovItsAfterOpen(DataSet: TDataSet);
    procedure QrVSMovItsBeforeClose(DataSet: TDataSet);
    procedure MostrarjaneladoIMEI1Click(Sender: TObject);
    procedure RGPrecisaoClick(Sender: TObject);
    procedure BtIMEIClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSMovIts(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmVSLocalizaMarcas: TFmVSLocalizaMarcas;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, AppListas;

{$R *.DFM}

procedure TFmVSLocalizaMarcas.BtIMEIClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMIMEI, BtIMEI);
end;

procedure TFmVSLocalizaMarcas.BtPesquisaClick(Sender: TObject);
begin
   ReopenVSMovIts(0);
end;

procedure TFmVSLocalizaMarcas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSLocalizaMarcas.EdPesqChange(Sender: TObject);
begin
  QrVSMovIts.Close;
end;

procedure TFmVSLocalizaMarcas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSLocalizaMarcas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSLocalizaMarcas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSLocalizaMarcas.Irparajaneladomovimento1Click(Sender: TObject);
var
  MovimID, Codigo, Controle: Integer;
begin
  MovimID  := QrVSMovItsMovimID.Value;
  Codigo   := QrVSMovItsCodigo.Value;
  Controle := QrVSMovItsControle.Value;
  VS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
end;

procedure TFmVSLocalizaMarcas.MostrarjaneladoIMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(QrVSMovItsControle.Value);
end;

procedure TFmVSLocalizaMarcas.QrVSMovItsAfterOpen(DataSet: TDataSet);
begin
  BtIMEI.Enabled := QrVSMovIts.RecordCount > 0;
end;

procedure TFmVSLocalizaMarcas.QrVSMovItsBeforeClose(DataSet: TDataSet);
begin
  BtIMEI.Enabled := False;
end;

procedure TFmVSLocalizaMarcas.QrVSMovItsCalcFields(DataSet: TDataSet);
begin
  QrVSMovItsNO_EstqMovimID.Value := sEstqMovimID[QrVSMovItsMovimID.Value];
  QrVSMovItsNO_SrcMovID.Value    := sEstqMovimID[QrVSMovItsSrcMovID.Value];
  QrVSMovItsNO_DstMovID.Value    := sEstqMovimID[QrVSMovItsDstMovID.Value];
end;

procedure TFmVSLocalizaMarcas.ReopenVSMovIts(Controle: Integer);
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Marca, sMarca: String;
begin
  sMarca := EdPesq.Text;
  case RGPrecisao.ItemIndex of
    0:
    begin
      Geral.MB_Aviso('Selecione a precis�o da pesquisa!');
      Exit;
    end;
    1: SQL_Marca := 'WHERE vmi.Marca = "' + sMarca + '"';
    2: SQL_Marca := 'WHERE vmi.Marca LIKE "%' + sMarca + '%"';
    else
    begin
      Geral.MB_Aviso('Precis�o n�o implementada!');
      Exit;
    end;
  end;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  //'CAST(vmi.Marca AS CHAR) Marca,',
  'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,',
  'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
  'CAST(vsp.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN, ',
  'IF(vmi.AreaM2 <> 0, vmi.ValorT/vmi.AreaM2, 0.0000) CustoM2, ',
  'IF(vmi.PesoKg <> 0, vmi.ValorT/vmi.PesoKg, 0.0000) CustoKg ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  SQL_Marca,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_SQL(Self, QrVSMovIts);
  //
  QrVSMovIts.Locate('Controle', Controle, []);
end;

procedure TFmVSLocalizaMarcas.RGPrecisaoClick(Sender: TObject);
begin
  QrVSMovIts.Close;
end;

end.
