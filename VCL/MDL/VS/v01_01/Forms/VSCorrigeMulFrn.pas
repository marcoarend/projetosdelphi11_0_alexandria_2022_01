unit VSCorrigeMulFrn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, AppListas,
  UnDmkProcFunc, UnProjGroup_Consts, Vcl.Mask, Vcl.Menus,
  dmkDBGridZTO, UnProjGroup_Vars, dmkEdit, UnGrl_Consts, UnAppEnums;

type
  TFmVSCorrigeMulFrn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    QrPesquisa: TmySQLQuery;
    DsPesquisa: TDataSource;
    QrTotal: TmySQLQuery;
    QrTotalITENS: TLargeintField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DsTotal: TDataSource;
    QrPesquisaNO_MovimID: TWideStringField;
    QrPesquisaMovimID: TIntegerField;
    QrPesquisaMovimCod: TIntegerField;
    QrPesquisaCodigo: TIntegerField;
    QrPesquisaITENS: TLargeintField;
    QrSemIni: TmySQLQuery;
    DsSemIni: TDataSource;
    GroupBox2: TGroupBox;
    DBGSemIni: TdmkDBGridZTO;
    GroupBox3: TGroupBox;
    Splitter1: TSplitter;
    QrSemIniNO_MovimID: TWideStringField;
    QrSemIniMovimID: TIntegerField;
    QrSemIniMovimCod: TIntegerField;
    QrSemIniCodigo: TIntegerField;
    QrSemIniITENS: TLargeintField;
    PMAcao: TPopupMenu;
    DefinirFornecedorinicial1: TMenuItem;
    Rastrearecorrigirfornecedornostiposfilhos1: TMenuItem;
    DBGPesquisa: TDBGrid;
    QrFilhos: TmySQLQuery;
    QrFilhosControle: TIntegerField;
    QrIMEIsIni: TmySQLQuery;
    QrIMEIsIniCodigo: TLargeintField;
    QrIMEIsIniControle: TLargeintField;
    QrIMEIsIniMovimCod: TLargeintField;
    QrIMEIsIniMovimNiv: TLargeintField;
    QrIMEIsIniMovimTwn: TLargeintField;
    QrIMEIsIniEmpresa: TLargeintField;
    QrIMEIsIniTerceiro: TLargeintField;
    QrIMEIsIniCliVenda: TLargeintField;
    QrIMEIsIniMovimID: TLargeintField;
    QrIMEIsIniDataHora: TDateTimeField;
    QrIMEIsIniPallet: TLargeintField;
    QrIMEIsIniGraGruX: TLargeintField;
    QrIMEIsIniPecas: TFloatField;
    QrIMEIsIniPesoKg: TFloatField;
    QrIMEIsIniAreaM2: TFloatField;
    QrIMEIsIniAreaP2: TFloatField;
    QrIMEIsIniValorT: TFloatField;
    QrIMEIsIniSrcMovID: TLargeintField;
    QrIMEIsIniSrcNivel1: TLargeintField;
    QrIMEIsIniSrcNivel2: TLargeintField;
    QrIMEIsIniSrcGGX: TLargeintField;
    QrIMEIsIniSdoVrtPeca: TFloatField;
    QrIMEIsIniSdoVrtPeso: TFloatField;
    QrIMEIsIniSdoVrtArM2: TFloatField;
    QrIMEIsIniObserv: TWideStringField;
    QrIMEIsIniSerieFch: TLargeintField;
    QrIMEIsIniFicha: TLargeintField;
    QrIMEIsIniMisturou: TLargeintField;
    QrIMEIsIniFornecMO: TLargeintField;
    QrIMEIsIniCustoMOKg: TFloatField;
    QrIMEIsIniCustoMOM2: TFloatField;
    QrIMEIsIniCustoMOTot: TFloatField;
    QrIMEIsIniValorMP: TFloatField;
    QrIMEIsIniDstMovID: TLargeintField;
    QrIMEIsIniDstNivel1: TLargeintField;
    QrIMEIsIniDstNivel2: TLargeintField;
    QrIMEIsIniDstGGX: TLargeintField;
    QrIMEIsIniQtdGerPeca: TFloatField;
    QrIMEIsIniQtdGerPeso: TFloatField;
    QrIMEIsIniQtdGerArM2: TFloatField;
    QrIMEIsIniQtdGerArP2: TFloatField;
    QrIMEIsIniQtdAntPeca: TFloatField;
    QrIMEIsIniQtdAntPeso: TFloatField;
    QrIMEIsIniQtdAntArM2: TFloatField;
    QrIMEIsIniQtdAntArP2: TFloatField;
    QrIMEIsIniNotaMPAG: TFloatField;
    QrIMEIsIniPedItsFin: TLargeintField;
    QrIMEIsIniMarca: TWideStringField;
    QrIMEIsIniStqCenLoc: TLargeintField;
    QrIMEIsIniNO_FORNECE: TWideStringField;
    QrIMEIsIniNO_PALLET: TWideStringField;
    QrIMEIsIniNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsIniNO_TTW: TWideStringField;
    QrIMEIsIniID_TTW: TLargeintField;
    QrIMEIsIniReqMovEstq: TLargeintField;
    DsIMEIsIni: TDataSource;
    DGDados: TdmkDBGridZTO;
    MostraJanela1: TMenuItem;
    Descendente1: TMenuItem;
    QrIMEIsPsq: TmySQLQuery;
    QrIMEIsPsqCodigo: TLargeintField;
    QrIMEIsPsqControle: TLargeintField;
    QrIMEIsPsqMovimCod: TLargeintField;
    QrIMEIsPsqMovimNiv: TLargeintField;
    QrIMEIsPsqMovimTwn: TLargeintField;
    QrIMEIsPsqEmpresa: TLargeintField;
    QrIMEIsPsqTerceiro: TLargeintField;
    QrIMEIsPsqCliVenda: TLargeintField;
    QrIMEIsPsqMovimID: TLargeintField;
    QrIMEIsPsqDataHora: TDateTimeField;
    QrIMEIsPsqPallet: TLargeintField;
    QrIMEIsPsqGraGruX: TLargeintField;
    QrIMEIsPsqPecas: TFloatField;
    QrIMEIsPsqPesoKg: TFloatField;
    QrIMEIsPsqAreaM2: TFloatField;
    QrIMEIsPsqAreaP2: TFloatField;
    QrIMEIsPsqValorT: TFloatField;
    QrIMEIsPsqSrcMovID: TLargeintField;
    QrIMEIsPsqSrcNivel1: TLargeintField;
    QrIMEIsPsqSrcNivel2: TLargeintField;
    QrIMEIsPsqSrcGGX: TLargeintField;
    QrIMEIsPsqSdoVrtPeca: TFloatField;
    QrIMEIsPsqSdoVrtPeso: TFloatField;
    QrIMEIsPsqSdoVrtArM2: TFloatField;
    QrIMEIsPsqObserv: TWideStringField;
    QrIMEIsPsqSerieFch: TLargeintField;
    QrIMEIsPsqFicha: TLargeintField;
    QrIMEIsPsqMisturou: TLargeintField;
    QrIMEIsPsqFornecMO: TLargeintField;
    QrIMEIsPsqCustoMOKg: TFloatField;
    QrIMEIsPsqCustoMOM2: TFloatField;
    QrIMEIsPsqCustoMOTot: TFloatField;
    QrIMEIsPsqValorMP: TFloatField;
    QrIMEIsPsqDstMovID: TLargeintField;
    QrIMEIsPsqDstNivel1: TLargeintField;
    QrIMEIsPsqDstNivel2: TLargeintField;
    QrIMEIsPsqDstGGX: TLargeintField;
    QrIMEIsPsqQtdGerPeca: TFloatField;
    QrIMEIsPsqQtdGerPeso: TFloatField;
    QrIMEIsPsqQtdGerArM2: TFloatField;
    QrIMEIsPsqQtdGerArP2: TFloatField;
    QrIMEIsPsqQtdAntPeca: TFloatField;
    QrIMEIsPsqQtdAntPeso: TFloatField;
    QrIMEIsPsqQtdAntArM2: TFloatField;
    QrIMEIsPsqQtdAntArP2: TFloatField;
    QrIMEIsPsqNotaMPAG: TFloatField;
    QrIMEIsPsqPedItsFin: TLargeintField;
    QrIMEIsPsqMarca: TWideStringField;
    QrIMEIsPsqStqCenLoc: TLargeintField;
    QrIMEIsPsqNO_FORNECE: TWideStringField;
    QrIMEIsPsqNO_PALLET: TWideStringField;
    QrIMEIsPsqNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsPsqNO_TTW: TWideStringField;
    QrIMEIsPsqID_TTW: TLargeintField;
    QrIMEIsPsqReqMovEstq: TLargeintField;
    DsIMEIsPsq: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Inicial1: TMenuItem;
    Memo1: TMemo;
    Splitter2: TSplitter;
    BtParar: TBitBtn;
    QrVMI: TmySQLQuery;
    QrVMICodigo: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    EdMovimIDs: TdmkEdit;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Setarfornecmultiplocomo11: TMenuItem;
    N1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Rastrearecorrigirfornecedornostiposfilhos1Click(Sender: TObject);
    procedure DefinirFornecedorinicial1Click(Sender: TObject);
    procedure QrSemIniBeforeClose(DataSet: TDataSet);
    procedure QrSemIniAfterScroll(DataSet: TDataSet);
    procedure Descendente1Click(Sender: TObject);
    procedure QrPesquisaAfterScroll(DataSet: TDataSet);
    procedure QrPesquisaBeforeClose(DataSet: TDataSet);
    procedure Inicial1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtPararClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Setarfornecmultiplocomo11Click(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FPenuti, FUltimo: Integer;
    procedure AtualizaFornecedorRclMul(MovimCod: Integer);
    procedure CorrigeFornecedoresOut(MovimCod: Integer);
    procedure CorrigeFornecedoresBxa(MovimCod: Integer);
    procedure CorrigeFornecedoresExB(MovimCod: Integer);
    procedure CorrigeFornecedoresResRcl(MovimCod: Integer);
    procedure ExecutaTudo();
    //procedure ReopenSemIni();
    procedure ReopenIMEIs(QrIMEIS: TmySQLQuery; MovimCod, Controle: Integer);
    function  ReopenPesquisa(): Integer;
    procedure ReopenVMI_MovimCod(MovimCod: Integer; MovimID: TEstqMovimID);
  public
    { Public declarations }
    FProblemas: Integer;
  end;

  var
  FmVSCorrigeMulFrn: TFmVSCorrigeMulFrn;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, VSOutCab, UnVS_PF, UnEntities,
  UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSCorrigeMulFrn.AtualizaFornecedorRclMul(MovimCod: Integer);
var
  Qr1, Qr2: TmySQLQuery;
  Controle, MovimNiv, SrcNivel2, DstNivel2, Terceiro, VSMulFrnCab: Integer;
begin
  Qr1 := TmySQLQuery.Create(Dmod);
  try
    Qr2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qr1, Dmod.MyDB, [
      'SELECT Controle, MovimNiv, SrcNivel2, DstNivel2',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimID=' + Geral.FF0(Integer(emidReclasXXMul)),
      'AND Terceiro=0',
      'AND VSMulFrnCab=0',
      '']);
      Qr1.First;
      while not Qr1.Eof do
      begin
        Controle  := Qr1.FieldByName('Controle').AsInteger;
        MovimNiv  := Qr1.FieldByName('MovimNiv').AsInteger;
        SrcNivel2 := Qr1.FieldByName('SrcNivel2').AsInteger;
        DstNivel2 := Qr1.FieldByName('DstNivel2').AsInteger;
        if (MovimNiv = 2) and (DstNivel2 <> 0) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
          'SELECT Terceiro, VSMulFrnCab',
          'FROM ' + CO_SEL_TAB_VMI,
          'WHERE Controle=' + Geral.FF0(DstNivel2),
          '']);
          Terceiro    := Qr2.FieldByName('Terceiro').AsInteger;
          VSMulFrnCab := Qr2.FieldByName('VSMulFrnCab').AsInteger;
          if (Terceiro = 0) or (VSMulFrnCab = 0) then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Terceiro', 'VSMulFrnCab'], ['Controle'], [
            Terceiro, VSMulFrnCab], [Controle], True);
          end;
        end;
        Qr1.Next;
      end;
    finally
      Qr2.Free;
    end;
  finally
    Qr1.Free;
  end;
end;

procedure TFmVSCorrigeMulFrn.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmVSCorrigeMulFrn.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmVSCorrigeMulFrn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCorrigeMulFrn.CorrigeFornecedoresBxa(MovimCod: Integer);
begin
  ReopenVMI_MovimCod(MovimCod, emidForcado);
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    VS_PF.AtualizaFornecedorBxa(QrVMIControle.Value);
    //
    QrVMI.Next;
  end;
end;

procedure TFmVSCorrigeMulFrn.CorrigeFornecedoresExB(MovimCod: Integer);
begin
  ReopenVMI_MovimCod(MovimCod, emidExtraBxa);
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    VS_PF.AtualizaFornecedorExB(QrVMIControle.Value);
    //
    QrVMI.Next;
  end;
end;

procedure TFmVSCorrigeMulFrn.CorrigeFornecedoresOut(MovimCod: Integer);
begin
  ReopenVMI_MovimCod(MovimCod, emidVenda);
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    VS_CRC_PF.AtualizaFornecedorOut(QrVMIControle.Value);
    //
    QrVMI.Next;
  end;
end;

procedure TFmVSCorrigeMulFrn.CorrigeFornecedoresResRcl(MovimCod: Integer);
begin
  ReopenVMI_MovimCod(MovimCod, emidResiduoReclas);
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    VS_PF.AtualizaFornecedorResRcl(QrVMIControle.Value);
    //
    QrVMI.Next;
  end;
end;

procedure TFmVSCorrigeMulFrn.DefinirFornecedorinicial1Click(Sender: TObject);
const
  MostraPnDados = False;
  Campo  = 'Descricao';
  VSMulFrnCab = 0;
var
  I, Terceiro, (*MovimCod,*) Controle: Integer;
  SQL: String;
begin
  if DBGSemIni.SelectedRows.Count = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  SQL := Geral.ATS([
    'SELECT ent.Codigo,  ',
    'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome), " (", ',
    '  IF(vem.Codigo IS NULL, "?????", vem.SiglaVS), ")")  ' + Campo,
    'FROM entidades ent  ',
    'LEFT JOIN vsentimp vem ON ent.Codigo=vem.Codigo ',
    'WHERE ent.Fornece1="V" ',
    'ORDER BY ' + Campo,
    '']);
  if not Entities.SelecionaEntidade(MostraPnDados, SQL, Terceiro) then
    Exit;
  if Terceiro = 0 then
    Exit;
  with DBGSemIni.DataSource.DataSet do
  begin
    for I := 0 to DBGSemIni.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGSemIni.SelectedRows.Items[i]));
      GotoBookmark(DBGSemIni.SelectedRows.Items[i]);
      //
      QrIMEIsIni.First;
      while not QrIMEIsIni.Eof do
      begin
        Controle := QrIMEIsIniControle.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        //'Terceiro'], ['MovimCod', 'Terceiro', 'VSMulFrnCab'], [
        //Terceiro], [MovimCod, 0, 0], True) then
        'Terceiro', 'VSMulFrnCab'], ['Controle'], [
        Terceiro, VSMulFrnCab], [Controle], True) then
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_VMI,
          'SET Terceiro=' + Geral.FF0(Terceiro),
          'WHERE SrcNivel2<>0 ',
          'AND SrcNivel2=' + Geral.FF0(Controle),
          'AND Terceiro=0 ',
          'AND VSMulFrnCab=0 ',
          '']);
          //
        end;
        QrIMEIsIni.Next;
      end;
    end;
    //end;
  end;
  ReopenPesquisa();
end;

procedure TFmVSCorrigeMulFrn.Descendente1Click(Sender: TObject);
begin
  VS_PF.MostraFormVS_Do_IMEI(QrIMEIsPsqControle.Value);
end;

procedure TFmVSCorrigeMulFrn.ExecutaTudo();
var
  Codigo, MovimCod: Integer;
begin
  FParar := False;
  BtParar.Enabled := True;
  QrPesquisa.First;
  while not QrPesquisa.Eof do
  begin
    //MyObjects.Informa2(LaAviso1, LaAviso2, 'IMEI: ' + Geral.FF0());
    Application.ProcessMessages();
    if FParar then
    begin
      FParar := False;
      BtParar.Enabled := False;
      Screen.Cursor := crDefault;
      Exit;
    end;
    Codigo   := QrPesquisaCodigo.Value;
    MovimCod := QrPesquisaMovimCod.Value;
    //
    VS_PF.AdicionarNovosVS_emid();
    case TEstqMovimID(QrPesquisaMovimID.Value) of
      (*emidAjuste=0,
      emidCompra=1,*)
      (*02*)emidVenda: CorrigeFornecedoresOut(MovimCod);
      (*emidReclasWE=3,
      emidBaixa=4,
      emidIndsWE=5,*)
      (*06*)emidIndsXX: VS_PF.AtualizaFornecedorInd(MovimCod);
      (*emidClassArtXXUni=7,*)
      (*08*)emidReclasXXUni: VS_PF.AtualizaFornecedorRclUni(MovimCod);
      (*09*)emidForcado: CorrigeFornecedoresBxa(MovimCod);
      (*emidSemOrigem=10,*)
      (*11*)emidEmOperacao: VS_PF.AtualizaFornecedorOpe(MovimCod);
      (*12*)emidResiduoReclas: CorrigeFornecedoresResRcl(MovimCod);
      (*emidInventario=13,
      emidClassArtXXMul=14,*)
      (*15*)emidPreReclasse: VS_PF.AtualizaFornecedorPreRcl(MovimCod);
      (*emidEntradaPlC=16,
      (*17*)emidExtraBxa: CorrigeFornecedoresExB(MovimCod);
      (*emidSaldoAnterior=18,*)
      (*19*)emidEmProcWE: VS_PF.AtualizaFornecedorPWE(MovimCod);
      (*emidFinished=20,
      emidDevolucao=21,
      emidRetrabalho=22,
      emidGeraSubProd=23,*)
      (*24*)emidReclasXXMul: AtualizaFornecedorRclMul(MovimCod);
      //(*28*)emidDesclasse: VS_PF.AtualizaFornecedor??(MovimCod); Falta fazer!
      //(*29*)emidCaleado: VS_PF.AtualizaFornecedor????(MovimCod); Falta fazer!
      //(*30*) //  Falta fazer!!!
      //(*31*) //  Falta fazer!!!
      (*32*)emidEmProcSP: VS_PF.AtualizaFornecedorPSP(MovimCod);
      (*33*)emidEmReprRM: VS_PF.AtualizaFornecedorRRM(MovimCod);
      //(*34*) //  Falta fazer!!!
      //(*34*) emidMixInsum
      else
      begin
        if VAR_MEMO_DEF_VS_MUL_FRN <> nil then
           VAR_MEMO_DEF_VS_MUL_FRN.Text := 'MovimID n�o implem.: ' +
             Geral.FF0(QrPesquisaMovimID.Value) +
           sLineBreak + VAR_MEMO_DEF_VS_MUL_FRN.Text
        else
          Geral.MB_Erro('Defini��o de fornecedor abortada!' + sLineBreak +
          'MovimID n�o implemtado: ' + Geral.FF0(QrPesquisaMovimID.Value));
      end;
    end;
    //
    QrPesquisa.Next;
  end;
  BtParar.Enabled := False;
  ReopenPesquisa();
  if Trim(EdMovimIDs.Text) = '' then
  begin
    FPenuti := FUltimo;
    FUltimo := QrTotalITENS.Value;
  end;
end;

procedure TFmVSCorrigeMulFrn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCorrigeMulFrn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MEMO_DEF_VS_MUL_FRN := nil;
end;

procedure TFmVSCorrigeMulFrn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  VAR_MEMO_DEF_VS_MUL_FRN := Memo1;
  FPenuti := 0;
  FUltimo := 0;
  //
  FProblemas := ReopenPesquisa();
end;

procedure TFmVSCorrigeMulFrn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCorrigeMulFrn.Inicial1Click(Sender: TObject);
begin
  VS_PF.MostraFormVS_Do_IMEI(QrIMEIsIniControle.Value);
end;

procedure TFmVSCorrigeMulFrn.PMAcaoPopup(Sender: TObject);
begin
  Setarfornecmultiplocomo11.Enabled := (FPenuti = FUltimo) and (FUltimo > 0);
end;

procedure TFmVSCorrigeMulFrn.QrPesquisaAfterScroll(DataSet: TDataSet);
begin
  ReopenIMEIs(QrIMEIsPsq, QrPesquisaMovimCod.Value, 0);
end;

procedure TFmVSCorrigeMulFrn.QrPesquisaBeforeClose(DataSet: TDataSet);
begin
  QrIMEIsPsq.Close;
end;

procedure TFmVSCorrigeMulFrn.QrSemIniAfterScroll(DataSet: TDataSet);
begin
  ReopenIMEIs(QrIMEIsIni, QrSemIniMovimCod.Value, 0);
end;

procedure TFmVSCorrigeMulFrn.QrSemIniBeforeClose(DataSet: TDataSet);
begin
  QrIMEIsIni.Close;
end;

procedure TFmVSCorrigeMulFrn.Rastrearecorrigirfornecedornostiposfilhos1Click(
  Sender: TObject);
begin
  ExecutaTudo();
end;

procedure TFmVSCorrigeMulFrn.ReopenIMEIs(QrIMEIS: TmySQLQuery; MovimCod, Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := 0;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.Terceiro=0 ',
  'AND vmi.VSMulFrnCab=0 ',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrIMEIs.Locate('Controle', Controle, []);
end;

function TFmVSCorrigeMulFrn.ReopenPesquisa(): Integer;
var
  ATT_MovimID, SQL_SemIni, SQL_FiltroIDs: String;
  //
  procedure ReopenSemelhante(Qry: TmySQLQuery; SQL_Extra: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    ATT_MovimID,
    'vmi.MovimID, vmi.MovimCod, vmi.Codigo, COUNT(vmi.Controle) ITENS  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.Terceiro=0 ',
    'AND vmi.VSMulFrnCab=0 ',
    'AND vmi.Pecas <> 0 ',
    SQL_Extra,
    'GROUP BY vmi.MovimID, vmi.MovimCod ',
    'ORDER BY vmi.Controle ',
    '']);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  if EdMovimIDs.Text <> '' then
    SQL_FiltroIDs := 'AND MovimID IN (' + EdMovimIDs.Text + ') '
  else
    SQL_FiltroIDs := '';
  ReopenSemelhante(QrPesquisa, SQL_FiltroIDs);
  Result := QrPesquisa.RecordCount;
  if Result > 0 then
  begin
    ReopenSemelhante(QrSemIni, 'AND MovimID IN (1, 10, 13, 16, 18, 21, 22) ');
    //Geral.MB_SQL(Self, QrPesquisa);
    UnDmkDAC_PF.AbreMySQLQuery0(QrTotal, Dmod.MyDB, [
    'SELECT CAST(COUNT(vmi.Controle) AS SIGNED) ITENS   ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.Terceiro=0  ',
    'AND vmi.VSMulFrnCab=0  ',
    'AND vmi.Pecas <> 0 ',
    '']);
    Result := QrPesquisa.RecordCount;
  end;
end;

procedure TFmVSCorrigeMulFrn.ReopenVMI_MovimCod(MovimCod: Integer; MovimID:
  TEstqMovimID);
begin
  UnDmkDAC_PF.AbreMySQLQUery0(QrVMI, Dmod.MyDB, [
  'SELECT vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.MovimID, vmi.MovimNiv ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimID=' + Geral.FF0(Integer(MovimID)),
  'AND vmi.Terceiro=0  ',
  'AND vmi.VSMulFrnCab=0  ',
  'ORDER BY vmi.Controle ',
  '']);
end;

procedure TFmVSCorrigeMulFrn.Setarfornecmultiplocomo11Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if Geral.MB_Pergunta(
  'Confirma a altera��o de todos IME-Is sem fornecedor �nico/m�ltiplo?') =
  ID_YES then
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI + ' ',
    'SET VSMulFrnCab=-1 ',
    'WHERE Terceiro=0 ',
    'AND VSMulFrnCab=0 ',
    '']);
  end;
  ReopenPesquisa();
end;

procedure TFmVSCorrigeMulFrn.SpeedButton1Click(Sender: TObject);
begin
  ReopenPesquisa();
end;

end.
