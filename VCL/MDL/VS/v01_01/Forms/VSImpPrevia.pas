unit VSImpPrevia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  dmkGeral, UnInternalConsts, Data.DB, mySQLDbTables, frxDBSet;

type
  TFmVSImpPrevia = class(TForm)
    frxWET_CURTI_026_02: TfrxReport;
    Qr07PrevPal: TmySQLQuery;
    Qr07PrevPalVSPallet: TIntegerField;
    Qr07PrevPalPecas: TFloatField;
    Qr07PrevPalAreaM2: TFloatField;
    Qr07PrevPalAreaP2: TFloatField;
    Qr07PrevPalNO_PRD_TAM_COR: TWideStringField;
    Qr07PrevPalCodigo: TIntegerField;
    Qr07PrevPalNome: TWideStringField;
    Qr07PrevPalEmpresa: TIntegerField;
    Qr07PrevPalStatus: TIntegerField;
    Qr07PrevPalCliStat: TIntegerField;
    Qr07PrevPalGraGruX: TIntegerField;
    Qr07PrevPalDtHrEndAdd: TDateTimeField;
    Qr07PrevPalGerRclCab: TIntegerField;
    Qr07PrevPalDtHrFimRcl: TDateTimeField;
    Qr07PrevPalMovimIDGer: TIntegerField;
    Qr07PrevPalLk: TIntegerField;
    Qr07PrevPalDataCad: TDateField;
    Qr07PrevPalDataAlt: TDateField;
    Qr07PrevPalUserCad: TIntegerField;
    Qr07PrevPalUserAlt: TIntegerField;
    Qr07PrevPalAlterWeb: TSmallintField;
    Qr07PrevPalAtivo: TSmallintField;
    Qr07PrevPalQtdPrevPc: TIntegerField;
    frxDs07PrevPal: TfrxDBDataset;
    procedure frxWET_CURTI_026_02GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    //FVSMovImp5: String;
    //
    function  SQL_ListaPallets(): String;
  public
    { Public declarations }
    FEmpresa: Integer;
    FPallets: array of Integer;
    //
    procedure ImprimePallet();
  end;

var
  FmVSImpPrevia: TFmVSImpPrevia;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, CreateVS;

{$R *.dfm}

procedure TFmVSImpPrevia.frxWET_CURTI_026_02GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
(*
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro.Text, Ed01Terceiro.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PALLET_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Pallet.Text, Ed01Pallet.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_GRAGRUX_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01GraGruX.Text, Ed01GraGruX.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni.Date, TP01DataFim.Date,
    Ck01DataIni.Checked, Ck01DataFim.Checked, '', 'at�', '')
  else
*)
end;

procedure TFmVSImpPrevia.ImprimePallet;
var
  SQL_Empresa: String;
begin
(*
  FVSMovImp5 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp5, DModG.QrUpdPID1, False);
  if FEmpresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(FEmpresa)
  else
    SQL_Empresa := '';
  //
  //fazer ficha e testar
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR5, DModG.MyPID_DB, [
  'DELETE FROM  ' + FVSMovImp5 + '; ',
  'INSERT INTO  ' + FVSMovImp5,
  'SELECT wmi.Empresa, wmi.GraGruX, SUM(wmi.Pecas) Pecas,  ',
  'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2,  ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,  ',
  'SUM(wmi.SdoVrtPeca) SdoVrtPeca, SUM(wmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(wmi.SdoVrtArM2) SdoVrtArM2, ',
  '0 LmbPeca, 0 LmbVrtPeso, 0 LmbVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, wmi.Pallet, vsp.Nome NO_Pallet,  ',
  'IF(wmi.Terceiro IS NULL, 0, wmi.Terceiro), ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
  'IF(vsp.Status IS NULL, 0, vsp.Status), ',
  'IF((wmi.Terceiro=0) OR (wmi.Terceiro IS NULL), "",   ',
  'IF(trc.Tipo=0, trc.RazaoSocial, trc.Nome)) NO_FORNECE,  ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,  ',
  'vps.Nome NO_STATUS, ',
  'MAX(wmi.DataHora) DataHora, 0 OrdGGX,  ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, 0 PalStat, 1 Ativo  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' wmi  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=wmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=wmi.Pallet  ',
  'LEFT JOIN ' + TMeuDB + '.entidades  trc ON trc.Codigo=wmi.Terceiro  ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat  ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=wmi.Empresa  ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta  vps ON vps.Codigo=vsp.Status  ',
  'WHERE wmi.Pallet IN (' + SQL_ListaPallets() + ') ',
  //SQL_Empresa,
  'GROUP BY wmi.Empresa, wmi.Pallet, wmi.GraGruX ',
  '; ',
  'SELECT * ',
  'FROM ' + FVSMovImp5,
  'ORDER BY Pallet; ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_026_01, [
  DModG.frxDsDono,
  frxDsEstqR5
  ]);
  MyObjects.frxMostra(frxWET_CURTI_026_01, 'Pallet de Artigo de Ribeira');
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qr07PrevPal, Dmod.MyDB, [
  'SELECT cia.VSPallet,  ',
  'SUM(cia.Pecas) Pecas,  ',
  'SUM(cia.AreaM2) AreaM2, ',
  'SUM(cia.AreaP2) AreaP2, ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),  ',
  'IF(pal.Nome <> "", CONCAT(" (", pal.Nome, ")"), ""))   ',
  'NO_PRD_TAM_COR, pal.*   ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=cia.VSPallet ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX   ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE cia.VSPallet IN (' + SQL_ListaPallets() + ') ',
  'GROUP BY cia.VSPallet ',
  'ORDER BY cia.VSPallet DESC ',
  ' ']);
  MyObjects.frxDefineDataSets(frxWET_CURTI_026_02, [
  DModG.frxDsDono,
  frxDs07PrevPal
  ]);
  MyObjects.frxMostra(frxWET_CURTI_026_02, 'Previa de Pallet de Artigo de Ribeira');
end;

function TFmVSImpPrevia.SQL_ListaPallets(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FPallets);
  for I := Low(FPallets) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FPallets[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

end.
