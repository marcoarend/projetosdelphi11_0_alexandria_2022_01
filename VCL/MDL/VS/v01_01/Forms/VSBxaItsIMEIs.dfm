object FmVSBxaItsIMEIs: TFmVSBxaItsIMEIs
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-075 :: Baixas For'#231'adas de IMEIs Completos'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 457
        Height = 32
        Caption = 'Baixas For'#231'adas de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 457
        Height = 32
        Caption = 'Baixas For'#231'adas de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 457
        Height = 32
        Caption = 'Baixas For'#231'adas de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 452
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 452
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 237
          Top = 15
          Width = 5
          Height = 435
          ExplicitLeft = 417
          ExplicitHeight = 450
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 235
          Height = 435
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object DBGGraGruY: TdmkDBGridZTO
            Left = 0
            Top = 41
            Width = 235
            Height = 394
            Align = alClient
            DataSource = DsGraGruY
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnAfterMultiselect = DBGGraGruYAfterMultiselect
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 239
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 235
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object RGPosiNega: TRadioGroup
              Left = 0
              Top = 0
              Width = 235
              Height = 41
              Align = alClient
              Caption = ' Estoque: '
              Columns = 2
              Enabled = False
              ItemIndex = 0
              Items.Strings = (
                'Positivo'
                'Negativo')
              TabOrder = 0
              OnClick = RGPosiNegaClick
            end
          end
        end
        object Panel7: TPanel
          Left = 242
          Top = 15
          Width = 764
          Height = 435
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object DBGIMEIs: TdmkDBGridZTO
            Left = 0
            Top = 181
            Width = 764
            Height = 254
            Align = alClient
            DataSource = DsIMEIs
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDrawColumnCell = DBGIMEIsDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IMEI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SerieFch'
                Title.Caption = 'S'#233'rie'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'MediaSdoVrtArM2'
                Title.Caption = 'M'#233'd.sdo m'#178'/pc'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FaixaMediaM2'
                Title.Caption = 'Media?'
                Title.Font.Charset = ANSI_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DstGGX'
                Title.Caption = 'Reduzido'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigo'
                Width = 245
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoVrtPeca'
                Title.Caption = 'Sdo Pe'#231'as'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SdoVrtArM2'
                Title.Caption = 'Sdo '#193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoVrtPeso'
                Title.Caption = 'Sdo Peso kg'
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 764
            Height = 181
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object GBDadosItem1: TGroupBox
              Left = 0
              Top = 0
              Width = 764
              Height = 181
              Align = alTop
              Caption = ' Dados do item: '
              TabOrder = 0
              object Label6: TLabel
                Left = 12
                Top = 16
                Width = 14
                Height = 13
                Caption = 'ID:'
              end
              object Label2: TLabel
                Left = 96
                Top = 16
                Width = 225
                Height = 13
                Caption = 'Material a baixar do estoque (F4 - '#250'ltima usada):'
              end
              object Label53: TLabel
                Left = 12
                Top = 56
                Width = 90
                Height = 13
                Caption = 'Centro de estoque:'
              end
              object Label49: TLabel
                Left = 12
                Top = 96
                Width = 60
                Height = 13
                Caption = 'Localiza'#231#227'o:'
              end
              object Label38: TLabel
                Left = 12
                Top = 136
                Width = 78
                Height = 13
                Caption = 'Tipo de material:'
              end
              object Label39: TLabel
                Left = 276
                Top = 136
                Width = 82
                Height = 13
                Caption = 'Parte do material:'
              end
              object EdControle: TdmkEdit
                Left = 12
                Top = 32
                Width = 80
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Controle'
                UpdCampo = 'Controle'
                UpdType = utInc
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdControleRedefinido
              end
              object CBGraGruX: TdmkDBLookupComboBox
                Left = 152
                Top = 32
                Width = 380
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = DsGraGruX
                TabOrder = 2
                dmkEditCB = EdGraGruX
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdGraGruX: TdmkEditCB
                Left = 96
                Top = 32
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdGraGruXRedefinido
                DBLookupComboBox = CBGraGruX
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdStqCenCad: TdmkEditCB
                Left = 12
                Top = 72
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdStqCenCadRedefinido
                DBLookupComboBox = CBStqCenCad
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBStqCenCad: TdmkDBLookupComboBox
                Left = 68
                Top = 72
                Width = 465
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsStqCenCad
                TabOrder = 4
                dmkEditCB = EdStqCenCad
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdStqCenLoc: TdmkEditCB
                Left = 12
                Top = 112
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'StqCenLoc'
                UpdCampo = 'StqCenLoc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdStqCenLocChange
                DBLookupComboBox = CBStqCenLoc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBStqCenLoc: TdmkDBLookupComboBox
                Left = 71
                Top = 112
                Width = 462
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_LOC_CEN'
                ListSource = DsStqCenLoc
                TabOrder = 6
                dmkEditCB = EdStqCenLoc
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdCouNiv2: TdmkEditCB
                Left = 12
                Top = 152
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv2'
                UpdCampo = 'CouNiv2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdCouNiv2Redefinido
                DBLookupComboBox = CBCouNiv2
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdCouNiv1: TdmkEditCB
                Left = 276
                Top = 152
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv1'
                UpdCampo = 'CouNiv1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdCouNiv1Redefinido
                DBLookupComboBox = CBCouNiv1
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCouNiv1: TdmkDBLookupComboBox
                Left = 333
                Top = 152
                Width = 200
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCouNiv1
                TabOrder = 9
                dmkEditCB = EdCouNiv1
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv1'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CBCouNiv2: TdmkDBLookupComboBox
                Left = 68
                Top = 152
                Width = 205
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCouNiv2
                TabOrder = 10
                dmkEditCB = EdCouNiv2
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv2'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 500
    Width = 1008
    Height = 59
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 42
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 25
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 284
        Top = 4
        Width = 148
        Height = 13
        Caption = 'Senha para uso de IME-I vago:'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 148
        Top = 16
        Width = 125
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 1
      end
      object EdSenha: TEdit
        Left = 284
        Top = 21
        Width = 149
        Height = 20
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        PasswordChar = 'l'
        TabOrder = 2
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 440
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Todos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 560
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtNenhumClick
      end
      object BtProvisorio: TBitBtn
        Left = 716
        Top = 20
        Width = 75
        Height = 25
        Caption = 'Provisorio'
        TabOrder = 5
        Visible = False
        OnClick = BtProvisorioClick
      end
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 92
    Top = 232
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 92
    Top = 280
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.SdoVrtArM2 / vmi.SdoVrtPeca  MediaSdoVrtArM2,'
      'CASE '
      'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca < cou.MediaMinM2 THEN 0.000'
      'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca > cou.MediaMaxM2 THEN 2.000'
      'ELSE 1.000 END FaixaMediaM2,'
      'CONCAT(gg1.Nome,       '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vmi.* '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX       '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC       '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad       '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI       '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'WHERE vmi.SdoVrtPeca>0'
      'AND ggx.GraGruY IN (2048)'
      'ORDER BY vmi.Controle')
    Left = 524
    Top = 376
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEIsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEIsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEIsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrIMEIsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrIMEIsPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrIMEIsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrIMEIsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrIMEIsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEIsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEIsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEIsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEIsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEIsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrIMEIsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEIsSerieFch: TIntegerField
      FieldName = 'SerieFch'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsFicha: TIntegerField
      FieldName = 'Ficha'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrIMEIsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrIMEIsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrIMEIsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrIMEIsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrIMEIsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrIMEIsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrIMEIsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEIsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEIsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrIMEIsMediaSdoVrtArM2: TFloatField
      FieldName = 'MediaSdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrIMEIsFaixaMediaM2: TFloatField
      FieldName = 'FaixaMediaM2'
    end
    object QrIMEIsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrIMEIsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrIMEIsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 524
    Top = 424
  end
  object PMProvisorio: TPopupMenu
    Left = 740
    Top = 348
    object ObterIMEIs11: TMenuItem
      Caption = 'ObterIMEIs'
      OnClick = ObterIMEIs11Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 108
    Top = 412
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 108
    Top = 464
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrStqCenCadAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 260
    Top = 416
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 260
    Top = 460
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 352
    Top = 420
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 352
    Top = 468
  end
  object QrCouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 684
    Top = 432
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 684
    Top = 480
  end
  object QrCouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 760
    Top = 432
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 760
    Top = 480
  end
end
