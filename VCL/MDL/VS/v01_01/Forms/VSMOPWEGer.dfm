object FmVSMOPWEGer: TFmVSMOPWEGer
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-165 :: Gerenciamento de MO de Semi Acabado'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 496
        Height = 32
        Caption = 'Gerenciamento de MO de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 496
        Height = 32
        Caption = 'Gerenciamento de MO de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 496
        Height = 32
        Caption = 'Gerenciamento de MO de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 629
          Top = 97
          Height = 368
          ExplicitLeft = 672
          ExplicitTop = 340
          ExplicitHeight = 100
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 82
          Align = alTop
          TabOrder = 0
          object Label55: TLabel
            Left = 8
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object EdTerceiro: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTerceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBTerceiro: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 313
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = Ds00Fornecedor
            TabOrder = 1
            dmkEditCB = EdTerceiro
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox2: TGroupBox
            Left = 384
            Top = 4
            Width = 245
            Height = 73
            Caption = ' Per'#237'odo: '
            TabOrder = 2
            object TPDataIni: TdmkEditDateTimePicker
              Left = 8
              Top = 40
              Width = 112
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkDataIni: TCheckBox
              Left = 8
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data inicial'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object CkDataFim: TCheckBox
              Left = 124
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data final'
              TabOrder = 2
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 124
              Top = 40
              Width = 112
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object BtReabre: TBitBtn
            Tag = 14
            Left = 656
            Top = 24
            Width = 120
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtReabreClick
          end
        end
        object DBG19_22: TdmkDBGridZTO
          Left = 2
          Top = 97
          Width = 627
          Height = 368
          Align = alLeft
          DataSource = Ds19_22
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
        object dmkDBGridZTO2: TdmkDBGridZTO
          Left = 632
          Top = 97
          Width = 178
          Height = 368
          Align = alClient
          DataSource = DsDstA
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 0
        Width = 74
        Height = 13
        Caption = 'Novo $ MO/m'#178':'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 108
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdMOM2: TdmkEdit
        Left = 16
        Top = 16
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object Qr00Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'OR ent.Fornece2="V"'
      'OR ent.Fornece3="V"'
      'OR ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 508
    Top = 180
    object Qr00FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds00Fornecedor: TDataSource
    DataSet = Qr00Fornecedor
    Left = 508
    Top = 228
  end
  object Qr19_22: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr19_22BeforeClose
    AfterScroll = Qr19_22AfterScroll
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.MovimCod, '
      'vmi.Controle, vmi.FornecMO,  '
      'vmi.DataHora, vmi.GraGruX, '
      'vmi.CustoMOTot/vmi.AreaM2 CustoA, '
      'vmi.AreaM2, vmi.PesoKg, vmi.ValorT, '
      'vmi.CustoMOTot, vmi.ValorMP, vmi.CustoPQ  '
      'FROM vsmovits vmi'
      'WHERE vmi.DataHora >= "2016-11-24"   '
      'AND vmi.MovimID=20 '
      'AND vmi.MovimNiv=22')
    Left = 116
    Top = 216
    object Qr19_22Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr19_22MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object Qr19_22Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr19_22FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object Qr19_22DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr19_22GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr19_22CustoA: TFloatField
      FieldName = 'CustoA'
    end
    object Qr19_22AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr19_22PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr19_22ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr19_22CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object Qr19_22ValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object Qr19_22CustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
  end
  object Ds19_22: TDataSource
    DataSet = Qr19_22
    Left = 116
    Top = 264
  end
  object QrDstA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.MovimCod, vmi.MovimID,'
      'vmi.MovimNiv, vmi.Controle,  '
      'vmi.DataHora, vmi.GraGruX, '
      'vmi.CustoMOTot/vmi.AreaM2 CustoA, '
      'vmi.AreaM2, vmi.PesoKg, vmi.ValorT, '
      'vmi.CustoMOTot, vmi.ValorMP, vmi.CustoPQ  '
      'FROM vsmovits vmi '
      'WHERE vmi.SrcNivel2=5037 ')
    Left = 212
    Top = 216
    object QrDstACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDstAMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrDstAMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrDstAMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrDstAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDstADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrDstAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrDstACustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
    object QrDstAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrDstAPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrDstAValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrDstACustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrDstAValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrDstACustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrDstACustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrDstAMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
  end
  object DsDstA: TDataSource
    DataSet = QrDstA
    Left = 212
    Top = 264
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 420
    Top = 340
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
