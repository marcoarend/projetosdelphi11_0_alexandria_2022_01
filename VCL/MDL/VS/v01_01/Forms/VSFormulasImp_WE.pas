unit VSFormulasImp_WE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs, ZCF2,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, Grids, DBGrids, AppListas,
  mySQLDbTables, ComCtrls, Menus, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkImage, MyListas, UnDmkProcFunc, dmkEditDateTimePicker,
  UnDmkEnums, frxClass, frxDBSet, dmkCheckBox, dmkEditCalc, UnProjGroup_Consts,
  UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSFormulasImp_WE = class(TForm)
    QrEspessura1: TmySQLQuery;
    QrEspessura1EMCM: TFloatField;
    QrEspessuras: TmySQLQuery;
    QrDefPecas: TmySQLQuery;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    QrEspessurasLk: TIntegerField;
    DsEspessuras: TDataSource;
    DsDefPecas: TDataSource;
    DsFormulas: TDataSource;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasLk: TIntegerField;
    Panel2: TPanel;
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasPeso: TFloatField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasHHMM_P: TWideStringField;
    QrFormulasHHMM_R: TWideStringField;
    QrFormulasHHMM_T: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    Panel3: TPanel;
    RGImpRecRib: TRadioGroup;
    GBkgTon: TGroupBox;
    RBTon: TRadioButton;
    RBkg: TRadioButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    PainelConfig: TPanel;
    RGTipoPreco: TRadioGroup;
    RGImprime: TRadioGroup;
    QrFormulasAtivo: TSmallintField;
    CkMatricial: TCheckBox;
    CkGrade: TCheckBox;
    CkContinua: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtCancela: TBitBtn;
    ProgressBar1: TProgressBar;
    LaSP_A: TLabel;
    LaSP_B: TLabel;
    LaSP_C: TLabel;
    QrWBMovIts: TmySQLQuery;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsNO_PALLET: TWideStringField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsValorT: TFloatField;
    DsWBMovIts: TDataSource;
    PainelEscolhas: TPanel;
    Label10: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    Label12: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeso: TdmkEdit;
    EdPecas: TdmkEdit;
    CBPeca: TdmkDBLookupComboBox;
    EdMedia: TdmkEdit;
    EdFulao: TdmkEdit;
    EdPeca: TdmkEditCB;
    CBEspessura: TdmkDBLookupComboBox;
    EdAreaM2: TdmkEdit;
    EdAreaP2: TdmkEdit;
    PainelReceita: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    LaData: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    TPDataP: TdmkEditDateTimePicker;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrEmitGru: TmySQLQuery;
    DsEmitGru: TDataSource;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    Label14: TLabel;
    CkRetrabalho: TdmkCheckBox;
    QrFormulasRetrabalho: TSmallintField;
    EdEspessura: TdmkEditCB;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label21: TLabel;
    EdSemiAreaM2: TdmkEditCalc;
    Label22: TLabel;
    EdSemiAreaP2: TdmkEditCalc;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    TPDtaCambio: TdmkEditDateTimePicker;
    Label17: TLabel;
    EdBRL_USD: TdmkEdit;
    EdBRL_EUR: TdmkEdit;
    SpeedButton2: TSpeedButton;
    PainelEscolhe: TPanel;
    Panel5: TPanel;
    Label6: TLabel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    EdMemo: TMemo;
    QrLotes: TmySQLQuery;
    QrLotesControle: TIntegerField;
    QrLotesVSMovIts: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    DsLotes: TDataSource;
    QrLotesAreaM2: TFloatField;
    QrSoma: TmySQLQuery;
    QrSomaPeso: TFloatField;
    QrSomaPecas: TFloatField;
    QrSomaAreaM2: TFloatField;
    QrLotesOP: TIntegerField;
    QrVMIAtu: TmySQLQuery;
    QrVMIAtuCodigo: TLargeintField;
    QrVMIAtuControle: TLargeintField;
    QrVMIAtuMovimCod: TLargeintField;
    QrVMIAtuMovimNiv: TLargeintField;
    QrVMIAtuMovimTwn: TLargeintField;
    QrVMIAtuEmpresa: TLargeintField;
    QrVMIAtuTerceiro: TLargeintField;
    QrVMIAtuCliVenda: TLargeintField;
    QrVMIAtuMovimID: TLargeintField;
    QrVMIAtuDataHora: TDateTimeField;
    QrVMIAtuPallet: TLargeintField;
    QrVMIAtuGraGruX: TLargeintField;
    QrVMIAtuPecas: TFloatField;
    QrVMIAtuPesoKg: TFloatField;
    QrVMIAtuAreaM2: TFloatField;
    QrVMIAtuAreaP2: TFloatField;
    QrVMIAtuValorT: TFloatField;
    QrVMIAtuSrcMovID: TLargeintField;
    QrVMIAtuSrcNivel1: TLargeintField;
    QrVMIAtuSrcNivel2: TLargeintField;
    QrVMIAtuSrcGGX: TLargeintField;
    QrVMIAtuSdoVrtPeca: TFloatField;
    QrVMIAtuSdoVrtPeso: TFloatField;
    QrVMIAtuSdoVrtArM2: TFloatField;
    QrVMIAtuObserv: TWideStringField;
    QrVMIAtuSerieFch: TLargeintField;
    QrVMIAtuFicha: TLargeintField;
    QrVMIAtuMisturou: TLargeintField;
    QrVMIAtuFornecMO: TLargeintField;
    QrVMIAtuCustoMOKg: TFloatField;
    QrVMIAtuCustoMOM2: TFloatField;
    QrVMIAtuCustoMOTot: TFloatField;
    QrVMIAtuValorMP: TFloatField;
    QrVMIAtuDstMovID: TLargeintField;
    QrVMIAtuDstNivel1: TLargeintField;
    QrVMIAtuDstNivel2: TLargeintField;
    QrVMIAtuDstGGX: TLargeintField;
    QrVMIAtuQtdGerPeca: TFloatField;
    QrVMIAtuQtdGerPeso: TFloatField;
    QrVMIAtuQtdGerArM2: TFloatField;
    QrVMIAtuQtdGerArP2: TFloatField;
    QrVMIAtuQtdAntPeca: TFloatField;
    QrVMIAtuQtdAntPeso: TFloatField;
    QrVMIAtuQtdAntArM2: TFloatField;
    QrVMIAtuQtdAntArP2: TFloatField;
    QrVMIAtuNotaMPAG: TFloatField;
    QrVMIAtuNO_PALLET: TWideStringField;
    QrVMIAtuNO_PRD_TAM_COR: TWideStringField;
    QrVMIAtuNO_TTW: TWideStringField;
    QrVMIAtuID_TTW: TLargeintField;
    QrVMIAtuNO_FORNECE: TWideStringField;
    QrVMIAtuReqMovEstq: TLargeintField;
    QrVMIAtuCUSTO_M2: TFloatField;
    QrVMIAtuCUSTO_P2: TFloatField;
    QrVMIAtuNO_LOC_CEN: TWideStringField;
    QrVMIAtuMarca: TWideStringField;
    QrVMIAtuPedItsLib: TLargeintField;
    QrVMIAtuStqCenLoc: TLargeintField;
    QrVMIAtuNO_FICHA: TWideStringField;
    QrVMIAtuNO_FORNEC_MO: TWideStringField;
    QrVMIAtuClientMO: TLargeintField;
    DsVMIAtu: TDataSource;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrVMIOriIMEI: TmySQLQuery;
    QrVMIOriIMEICodigo: TLargeintField;
    QrVMIOriIMEIControle: TLargeintField;
    QrVMIOriIMEIMovimCod: TLargeintField;
    QrVMIOriIMEIMovimNiv: TLargeintField;
    QrVMIOriIMEIMovimTwn: TLargeintField;
    QrVMIOriIMEIEmpresa: TLargeintField;
    QrVMIOriIMEITerceiro: TLargeintField;
    QrVMIOriIMEICliVenda: TLargeintField;
    QrVMIOriIMEIMovimID: TLargeintField;
    QrVMIOriIMEIDataHora: TDateTimeField;
    QrVMIOriIMEIPallet: TLargeintField;
    QrVMIOriIMEIGraGruX: TLargeintField;
    QrVMIOriIMEIPecas: TFloatField;
    QrVMIOriIMEIPesoKg: TFloatField;
    QrVMIOriIMEIAreaM2: TFloatField;
    QrVMIOriIMEIAreaP2: TFloatField;
    QrVMIOriIMEIValorT: TFloatField;
    QrVMIOriIMEISrcMovID: TLargeintField;
    QrVMIOriIMEISrcNivel1: TLargeintField;
    QrVMIOriIMEISrcNivel2: TLargeintField;
    QrVMIOriIMEISrcGGX: TLargeintField;
    QrVMIOriIMEISdoVrtPeca: TFloatField;
    QrVMIOriIMEISdoVrtPeso: TFloatField;
    QrVMIOriIMEISdoVrtArM2: TFloatField;
    QrVMIOriIMEIObserv: TWideStringField;
    QrVMIOriIMEISerieFch: TLargeintField;
    QrVMIOriIMEIFicha: TLargeintField;
    QrVMIOriIMEIMisturou: TLargeintField;
    QrVMIOriIMEIFornecMO: TLargeintField;
    QrVMIOriIMEICustoMOKg: TFloatField;
    QrVMIOriIMEICustoMOM2: TFloatField;
    QrVMIOriIMEICustoMOTot: TFloatField;
    QrVMIOriIMEIValorMP: TFloatField;
    QrVMIOriIMEIDstMovID: TLargeintField;
    QrVMIOriIMEIDstNivel1: TLargeintField;
    QrVMIOriIMEIDstNivel2: TLargeintField;
    QrVMIOriIMEIDstGGX: TLargeintField;
    QrVMIOriIMEIQtdGerPeca: TFloatField;
    QrVMIOriIMEIQtdGerPeso: TFloatField;
    QrVMIOriIMEIQtdGerArM2: TFloatField;
    QrVMIOriIMEIQtdGerArP2: TFloatField;
    QrVMIOriIMEIQtdAntPeca: TFloatField;
    QrVMIOriIMEIQtdAntPeso: TFloatField;
    QrVMIOriIMEIQtdAntArM2: TFloatField;
    QrVMIOriIMEIQtdAntArP2: TFloatField;
    QrVMIOriIMEINotaMPAG: TFloatField;
    QrVMIOriIMEINO_PALLET: TWideStringField;
    QrVMIOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVMIOriIMEINO_TTW: TWideStringField;
    QrVMIOriIMEIID_TTW: TLargeintField;
    QrVMIOriIMEINO_FORNECE: TWideStringField;
    QrVMIOriIMEINO_SerieFch: TWideStringField;
    QrVMIOriIMEIReqMovEstq: TLargeintField;
    QrVMIOriIMEIVSMovIts: TLargeintField;
    DsVMIOriIMEI: TDataSource;
    LaHora: TLabel;
    EdHoraIni: TdmkEdit;
    DBGrid1: TDBGrid;
    QrVMIOriIMEIPECAS_POSIT: TFloatField;
    QrVMIOriIMEIAREAM2_POSIT: TFloatField;
    QrVMIOriIMEIPESOKG_POSIT: TFloatField;
    QrRebaixe: TmySQLQuery;
    QrRebaixeCodigo: TIntegerField;
    QrRebaixeLinhas: TWideStringField;
    QrRebaixeEMCM: TFloatField;
    QrRebaixeLk: TIntegerField;
    DsRebaixe: TDataSource;
    EdSemiCodEspReb: TdmkEditCB;
    CBSemiCodEspReb: TdmkDBLookupComboBox;
    Label13: TLabel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label18: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtCancelaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdAreaPAfterExit(Sender: TObject);
    procedure EdAreaMAfterExit(Sender: TObject);
    procedure CBEspessuraClick(Sender: TObject);
    procedure CBEspessuraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoChange(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdReceitaChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtInsWBMovItsClick(Sender: TObject);
    procedure BtDelWBMovItsClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure QrVMIOriIMEICalcFields(DataSet: TDataSet);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    { Private declarations }
    FAlturaInicial: Integer;
    //
    procedure InsereEmitCus(GraGruX: Integer);
    procedure ConfiguraCambio();
  public
    { Public declarations }
    FEmit, FMovimCod, FIMEI, FTemIMEIMrt: Integer;
    FNomeForm: String;
    FMovimInn, FMovimSrc: TEstqMovimNiv;
    procedure ReopenVMIAtu();

    procedure AtualizaPesoEPecas();
    //procedure CalculaArea;
    //procedure ReopenEmitCus(Controle: Integer);
    //procedure ReopenWBMovIts(Controle: Integer);
    procedure ReopenLotes();
  end;

var
  FmVSFormulasImp_WE: TFmVSFormulasImp_WE;
  REICalArea : Boolean;
  REISetor : ShortInt;

implementation

uses UnMyObjects, Principal, Formulas, Module, FormulasImpShow, DmkDAC_PF,
  BlueDermConsts, FormulasImpOSs, UMySQLModule, ModEmit, ModuleGeral,
  FormulasImpShowNew, UnVS_PF, UnPQ_PF, PQx;

{$R *.DFM}

{
procedure TFmFormulasImpWE.CalculaArea;
begin
  if CBEspessura.KeyValue = NULL then CBEspessura.KeyValue := 0;
  QrEspessura1.Close;
  QrEspessura1.Params[0].AsInteger := CBEspessura.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrEspessura1, Dmod.MyDB);
  EdAreaM2.ValueVariant :=
    DmModEmit.CalculaAreaPelokg(EdPeso.ValueVariant,
    QrEspessurasEMCM.Value, IC2_PadroesPECouro,  1);
  QrEspessura1.Close;
end;
}

procedure TFmVSFormulasImp_WE.BtCancelaClick(Sender: TObject);
begin
  if FEmit <> 0 then
  begin
    DmModEmit.ExcluiLoteCus(FEmit, emidIndsWE);
    if ImgTipo.SQLType <> stUpd then
       UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Emit', FEmit);
  end;
  Close;
end;

procedure TFmVSFormulasImp_WE.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FMovimCod := 0;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //

  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 6);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecAcabto.Value;
  //
  //STSP.Caption := '';
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], '', False, taCenter, 2, 10, 20);

  PageControl1.ActivePageIndex := 0;
  Agora := DmodG.ObtemAgora();
  TPDataP.Date := Agora;
  EdHoraIni.ValueVariant := Agora - Int(Agora);
  FAlturaInicial := Height;
  if VAR_NOMEFORMIMP_ANCESTRAL = 'FmPrincipal' then
  begin
    PageControl2.Visible := IC2_WopcoesImpRecShowLotes;
    EdMemo.Visible := IC2_WopcoesImpRecShowLotes;

    Label6.Visible := IC2_WopcoesImpRecShowMedia;
    EdMedia.Visible := IC2_WopcoesImpRecShowMedia;

    Label2.Visible := IC2_WopcoesImpRecShowEspessura;
    CBEspessura.Visible := IC2_WopcoesImpRecShowEspessura;

    Label4.Visible := IC2_WopcoesImpRecShowM2;
    EdAreaM2.Visible := IC2_WopcoesImpRecShowM2;

    Label5.Visible := IC2_WopcoesImpRecShowP2;
    EdAreaP2.Visible := IC2_WopcoesImpRecShowP2;

  end;

  REICalArea := False;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRebaixe, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  if VAR_SETOR = CO_CALEIRO then REISetor := -3;
  if VAR_SETOR = CO_CURTIM then REISetor := -2;
  if VAR_SETOR = CO_RECURT then REISetor := -1;
  if VAR_SETOR = CO_ACABTO then REISetor := -4;
  case REISetor of
    -3 : CBPeca.KeyValue := -10;
    -2 : CBPeca.KeyValue := -10;
    -1 : CBPeca.KeyValue := -9;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT * FROM formulas ',
  'WHERE Ativo=1 ',
  Geral.ATS_If(VAR_SETOR <> CO_VAZIO, ['AND Setor=' + Geral.FF0(REISetor)]),
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  ConfiguraCambio();
end;

procedure TFmVSFormulasImp_WE.BtConfirmaClick(Sender: TObject);
const
  GraGruX = 0; // Deprecado?
var
  Codigo, Formula, EmitGru, Empresa: Integer;
  DataEmis: String;
begin
  //Parei Aqui
  //Tipificacao em vez de setor
  //arrumar Custo
  //
  EmitGru := EdEmitGru.ValueVariant;
  Formula := EdReceita.ValueVariant;
  //
  if Dmod.ObrigaInformarEmitGru(FEmit <> 0, EmitGru, EdEmitGru) then Exit;
  if MyObjects.FIC(Formula = 0, EdReceita, 'Informe a receita!') then Exit;
  //
  if not PQ_PF.ValidaProdutosFormulasRibeira(Formula) then
  begin
    if Geral.MB_Pergunta('A receita informada possui produtos irregulares ' +
      sLineBreak + 'e por isso n�o poder� ser impressa!' + sLineBreak +
      'Deseja localizar a receita para regularizar o cadastro?') = ID_YES then
    begin
      VAR_CADASTRO := Formula;
      Close;
    end;
    Exit;
  end;
  //
  if RGTipoPreco.ItemIndex = 2 then
    if not DmModEmit.MostraPrecosAlternativos(dmktfrmSetrEmi_MOLHADO, Formula) then
      Exit;
  //
  // ini 2024-01-01
  if UnPQx.ImpedePeloBalanco(TPDataP.Date, True) then Exit;
  // fim 2024-01-01
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //

  if Geral.MB_Pergunta('Confirma que a receita a ser utilizada �:' +
    sLineBreak + QrFormulasNome.Value + sLineBreak + 'E o peso �: ' +
    EdPeso.Text + ' ?') = ID_YES then
  begin
    if FEmit > 0 then
    begin
      DataEmis := Geral.FDT(TPDataP.Date, 1);
      Codigo := FEmit;
      InsereEmitCus(GraGruX);
      (*
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
      'Formula', 'DataEmis',
      'BxaEstqVS', 'GraGruX'], ['Codigo'], [
      Formula, DataEmis,
      BxaEstqVS, GraGruX], [Codigo], True);
      *)
    end;
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    //
    // Configuracoes


    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        FrFormulasImpShow.FReImprime    := False;
        FrFormulasImpShow.FEmit         := FEmit;
        FrFormulasImpShow.FEmitGru      := EmitGru;
        FrFormulasImpShow.FVSMovCod     := FMovimCod;
        FrFormulasImpShow.FVSMovimSrc   := FMovimSrc;
        FrFormulasImpShow.FTemIMEIMrt   := FTemIMEIMrt;
        FrFormulasImpShow.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShow.FRetrabalho   := Geral.BoolToInt(CkRetrabalho.Checked);
        //FrFormulasImpShow.FSourcMP      := CO_SourcMP_WetSome; // Recurtimento!
        FrFormulasImpShow.FSourcMP      := CO_SourcMP_VS_WE; // Recurtimento
        FrFormulasImpShow.FDataP        := TPDataP.Date;
        FrFormulasImpShow.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShow.FEmpresa      := Empresa;
        FrFormulasImpShow.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShow.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShow.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShow.FFormula      := Formula;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShow.FQtde         := EdPecas.ValueVariant;
        FrFormulasImpShow.FArea         := EdAreaM2.ValueVariant;
        FrFormulasImpShow.FLinhasRebx   := CBEspessura.Text;
        FrFormulasImpShow.FLinhasSemi   := CBSemiCodEspReb.Text;
        FrFormulasImpShow.FFulao        := EdFulao.Text;
        FrFormulasImpShow.FDefPeca      := CBPeca.Text;
        FrFormulasImpShow.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShow.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShow.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShow.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShow.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := EdEspessura.ValueVariant;
        FrFormulasImpShow.FCod_Rebaix   := EdSemiCodEspReb.ValueVariant;
        FrFormulasImpShow.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShow.FSemiAreaM2   := EdSemiAreaM2.ValueVariant;
        FrFormulasImpShow.FSemiRendim   := EdSemiAreaP2.ValueVariant;
        FrFormulasImpShow.FBRL_USD      := EdBRL_USD.ValueVariant;
        FrFormulasImpShow.FBRL_EUR      := EdBRL_EUR.ValueVariant;
        FrFormulasImpShow.FDtaCambio    := TPDtaCambio.Date;
        //
        FrFormulasImpShow.FObs := FmVSFormulasImp_WE.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_WE.EdMemo.Lines[6];
        //
        if FmVSFormulasImp_WE.CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
          else FrFormulasImpShow.FMatricialNovo := False;
        if FmVSFormulasImp_WE.CkGrade.Checked then FrFormulasImpShow.FGrade := True
          else FrFormulasImpShow.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if FmVSFormulasImp_WE.RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
          else FrFormulasImpShow.FPesoCalc := 10;
        //
        { Desabilitado em 2013-12-09 :: Errado!!!
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
          FrFormulasImpShow.FMedia := QrEspessurasEMCM.Value
        else begin
        }
        if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia :=
          FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
        else
          FrFormulasImpShow.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShow.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FQtde <= 0, EdPecas,
        'Defina a quantidade!') then
          Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShow.CalculaReceita(QrVMIAtu); // QrEmitCus
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShow.Show;
      end;
(*
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira deprecada!' +
        sLineBreak +
        'Altere para "1-Nova" a "Vers�o de impress�o de receita de ribeira".' +
        sLineBreak +
        'Na guia "Miscel�nea" das op��es do aplicativo!');
        Exit;
      end;
*)
      1:
      begin
        FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRecurtimento;
        FrFormulasImpShowNew.FReImprime    := False;
        FrFormulasImpShowNew.FEmit         := FEmit;
        FrFormulasImpShowNew.FEmitGru      := EmitGru;
        FrFormulasImpShowNew.FVSMovCod     := FMovimCod;
        FrFormulasImpShowNew.FVSMovimSrc   := FMovimSrc;
        FrFormulasImpShowNew.FTemIMEIMrt   := FTemIMEIMrt;
        FrFormulasImpShowNew.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShowNew.FRetrabalho   := Geral.BoolToInt(CkRetrabalho.Checked);
        FrFormulasImpShowNew.FSourcMP      := CO_SourcMP_WetSome; // Recurtimento!
        FrFormulasImpShowNew.FDataP        := TPDataP.Date;
        FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FEmpresa      := Empresa;
        FrFormulasImpShowNew.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShowNew.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShowNew.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShowNew.FFormula      := Formula;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShowNew.FQtde         := EdPecas.ValueVariant;
        FrFormulasImpShowNew.FArea         := EdAreaM2.ValueVariant;
        FrFormulasImpShowNew.FLinhasRebx   := CBEspessura.Text;
        FrFormulasImpShowNew.FLinhasSemi   := CBSemiCodEspReb.Text;
        FrFormulasImpShowNew.FFulao        := EdFulao.Text;
        FrFormulasImpShowNew.FDefPeca      := CBPeca.Text;
        FrFormulasImpShowNew.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShowNew.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShowNew.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShowNew.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := EdEspessura.ValueVariant;
        FrFormulasImpShowNew.FCod_Rebaix   := EdSemiCodEspReb.ValueVariant;
        FrFormulasImpShowNew.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShowNew.FSemiAreaM2   := EdSemiAreaM2.ValueVariant;
        FrFormulasImpShowNew.FSemiRendim   := EdSemiAreaP2.ValueVariant;
        FrFormulasImpShowNew.FBRL_USD      := EdBRL_USD.ValueVariant;
        FrFormulasImpShowNew.FBRL_EUR      := EdBRL_EUR.ValueVariant;
        FrFormulasImpShowNew.FDtaCambio    := TPDtaCambio.Date;
        //
        FrFormulasImpShowNew.FObs := FmVSFormulasImp_WE.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_WE.EdMemo.Lines[6];
        //
        if FmVSFormulasImp_WE.CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
          else FrFormulasImpShowNew.FMatricialNovo := False;
        if FmVSFormulasImp_WE.CkGrade.Checked then FrFormulasImpShowNew.FGrade := True
          else FrFormulasImpShowNew.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if FmVSFormulasImp_WE.RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
          else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        { Desabilitado em 2013-12-09 :: Errado!!!
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
          FrFormulasImpShowNew.FMedia := QrEspessurasEMCM.Value
        else begin
        }
        if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia :=
          FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
        else
          FrFormulasImpShowNew.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShowNew.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FQtde <= 0, EdPecas,
        'Defina a quantidade!') then
          Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShowNew.CalculaReceita(QrLotes); // QrEmitCus
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShowNew.Show;




      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [1]!');
        Exit;
      end;
    end;


(*

    if CkContinua.Checked = False then Close else
    begin
    if FEmit > 0 then FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
      //STSP.Caption := IntToStr(FEmit);
      MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
      [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
      Show;
    end;
  end;
*)
    Close;
  end;
end;

procedure TFmVSFormulasImp_WE.BtDelWBMovItsClick(Sender: TObject);
(*
var
  Codigo, MovimCod: Integer;
*)
begin
(*
  Codigo   := QrWBMovItsCodigo.Value;
  MovimCod := QrWBMovItsMovimCod.Value;
  //
  if Dmod.ExcluiControleWBMovIts(QrWBMovIts, QrWBMovItsControle,
  QrWBMovItsControle.Value, QrWBMovItsSrcNivel2.Value) then
  begin
    //Dmod.AtualizaTotaisWBXxxCab('wbinncab', MovimCod);
    ReopenWBMovIts(0);
  end;
*)
end;

procedure TFmVSFormulasImp_WE.EdAreaM2Change(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaM2.ValueVariant;
  EdAreaP2.ValueVariant := Area / CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmVSFormulasImp_WE.EdAreaP2Change(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaP2.ValueVariant;
  EdAreaM2.ValueVariant := Area * CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmVSFormulasImp_WE.EdAreaPAfterExit(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaP2.ValueVariant;
  EdAreaM2.ValueVariant := Area * CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmVSFormulasImp_WE.EdAreaMAfterExit(Sender: TObject);
var
  Area : Double;
begin
  if REICalArea then exit;
  REICalArea := True;
  Area := EdAreaM2.ValueVariant;
  EdAreaP2.ValueVariant := Area / CO_M2toFT2;
  REICalArea := False;
end;

procedure TFmVSFormulasImp_WE.CBEspessuraClick(Sender: TObject);
begin
  //CalculaArea;
end;

procedure TFmVSFormulasImp_WE.CBEspessuraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_DELETE then
    CBEspessura.KeyValue := 0;
end;

procedure TFmVSFormulasImp_WE.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmVSFormulasImp_WE.ConfiguraCambio();
begin
  if VAR_CAMBIO_DATA <> Trunc(Now()) then
    DModG.ReopenCambio();
  //
  EdBRL_USD.ValueVariant := VAR_CAMBIO_USD;
  EdBRL_EUR.ValueVariant := VAR_CAMBIO_EUR;
  TPDtaCambio.Date       := VAR_CAMBIO_DATA;
end;

procedure TFmVSFormulasImp_WE.EdPesoChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdPecas.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
  //CalculaArea;
end;

procedure TFmVSFormulasImp_WE.EdPecasChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdPecas.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmVSFormulasImp_WE.EdReceitaChange(Sender: TObject);
begin
  EdCliInt.ValueVariant := QrFormulasClienteI.Value;
  CBCliInt.KeyValue := QrFormulasClienteI.Value;
  CkRetrabalho.Checked := QrFormulasRetrabalho.Value > 0;
end;

procedure TFmVSFormulasImp_WE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
end;

procedure TFmVSFormulasImp_WE.FormDestroy(Sender: TObject);
begin
  VAR_NOMEFORMIMP_ANCESTRAL := CO_VAZIO;
end;

procedure TFmVSFormulasImp_WE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSFormulasImp_WE.InsereEmitCus(GraGruX: Integer);
var
  DataEmis: String;
  Codigo, Controle, MPIn, Formula, MPVIts, VSPedIts, VSMovIts, BxaEstqVS: Integer;
  Pecas, Peso, Custo, AreaM2, AreaP2, PercTotCus: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns; // ImgTipo.SQLType;
  //
  Codigo         := FEmit;
  Controle       := 0;
  MPIn           := 0; // Deprecado
  Formula        := EdReceita.ValueVariant; // Depois? QrEmitNumero.Value;
  BxaEstqVS      := Integer(TBxaEstqVS.bevsNao);  // Deprecado
  DataEmis       := Geral.FDT(TPDataP.Date, 1); //'0000-00-00'; // Depois? QrEmitDataEmis.Value;
  Pecas          := EdPecas.ValueVariant;
  Peso           := EdPeso.ValueVariant;
  AreaM2         := 0.00;
  AreaP2         := 0.00;
  Custo          := 0; // Calcula Depois ?
  MPVIts         := 0; // Deprecado?
  PercTotCus     := 0; // Depois? Peso do Item / Peso Pesagem * 100 >> % do custo da pesagem;
  VSPedIts       := 0; // ????
  VSMovIts       := FIMEI;

  //
  Controle := UMyMod.BuscaEmLivreY_Def('emitcus', 'Controle', SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitcus', False, [
  'Codigo', 'MPIn', 'Formula',
  'DataEmis', 'Pecas', 'Peso',
  'Custo', 'MPVIts', 'AreaM2',
  'AreaP2', 'PercTotCus', 'VSPedIts',
  CO_FLD_TAB_VMI, 'BxaEstqVS', 'GraGruX'], [
  'Controle'], [
  Codigo, MPIn, Formula,
  DataEmis, Pecas, Peso,
  Custo, MPVIts, AreaM2,
  AreaP2, PercTotCus, VSPedIts,
  VSMovIts, BxaEstqVS, GraGruX], [
  Controle], True) then
  ;
end;

procedure TFmVSFormulasImp_WE.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasHHMM_P.Value := dmkPF.HorasMH(QrFormulasTempoP.Value, False);
  QrFormulasHHMM_R.Value := dmkPF.HorasMH(QrFormulasTempoR.Value, False);
  QrFormulasHHMM_T.Value := dmkPf.HorasMH(QrFormulasTempoT.Value, False);
end;

procedure TFmVSFormulasImp_WE.QrVMIOriIMEICalcFields(DataSet: TDataSet);
begin
  // Compatibilidade TDmModEmit.EfetuaBaixa(
  QrVMIOriIMEIVSMovIts.Value := QrVMIOriIMEIControle.Value;
  //
  QrVMIOriIMEIPECAS_POSIT.Value := -QrVMIOriIMEIPecas.Value;
  QrVMIOriIMEIAREAM2_POSIT.Value := -QrVMIOriIMEIAreaM2.Value;
  QrVMIOriIMEIPESOKG_POSIT.Value := -QrVMIOriIMEIPesoKg.Value;
end;

(***
procedure TFmVSFormulasImp_WE.ReopenEmitCus(Controle: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM emitcus ecu',
  'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(FEmit),
  '']);
  //
  QrEmitCus.Locate('Controle', controle, []);
end;
***)

procedure TFmVSFormulasImp_WE.ReopenLotes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas, ',
  'emc.AreaM2, vmi.Codigo OP',
  'FROM emitcus emc ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts ',
  'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch ',
  'WHERE emc.Codigo=' + Geral.FF0(FEmit),
  '']);
end;

procedure TFmVSFormulasImp_WE.ReopenVMIAtu();
const
  Controle   = 0;
  TemIMEIMrt = 0;
  SQL_Limit  = '';
begin
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVMIAtu, FMovimCod, FIMEI, FTemIMEIMrt, FMovimInn);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVMIOriIMEI, FMovimCod, Controle, TemIMEIMrt, FMovimSrc, SQL_Limit);
  //EdPeso.ValueVariant := QrVMIAtuPesoKg.Value;
  EdPecas.ValueVariant := QrVMIAtuPecas.Value;
  //EdPeca.ValueVariant := ;
  //CBPeca
  TPDataP.Date        := QrVMIAtuDataHora.Value;
end;

(*
procedure TFmVSFormulasImp_WE.ReopenWBMovIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  'FROM wbmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN wbpallet   wbp ON wbp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidIndsWE)),
  'AND wmi.Codigo=' + Geral.FF0(QrEmitCusMPVIts.Value),
  'AND wmi.LnkNivXtr1=' + Geral.FF0(QrEmitCusCodigo.Value),
  'AND wmi.LnkNivXtr2=' + Geral.FF0(QrEmitCusControle.Value),
  //'ORDER BY NO_Pallet, wmi.Controle ',
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrWBMovIts.Locate('Controle', Controle, []);
end;
*)
procedure TFmVSFormulasImp_WE.AtualizaPesoEPecas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso,  ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM emitcus ',
  'WHERE Codigo=' + Geral.FF0(FEmit),
  '']);
  //
  if (QrSomaPeso.Value >= 0.001) then
  begin
    EdPeso.ValueVariant   := QrSomaPeso.Value;
    EdPecas.ValueVariant  := QrSomaPecas.Value;
    EdAreaM2.ValueVariant := QrSomaAreaM2.Value;
  end;
end;

procedure TFmVSFormulasImp_WE.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CadastrodeDefPecas();
  QrDefPecas.Close;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
end;

procedure TFmVSFormulasImp_WE.SpeedButton2Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeCambio();
  ConfiguraCambio();
end;

procedure TFmVSFormulasImp_WE.BtInsWBMovItsClick(Sender: TObject);
(*
const
  Controle = 0;
var
  Emit, EmitCus, MPVIts, Empresa: Integer;
*)
begin
(*
  Emit    := QrEmitCusCodigo.Value;
  EmitCus := QrEmitCusControle.Value;
  MPVIts  := QrEmitCusMPVIts.Value;
  Empresa := DModG.ObtemFilialDeEntidade(EdCliInt.ValueVariant);
  FmPrincipal.MostraFormWBIndsWE(stIns, Controle, MPVIts, Emit, EmitCus,
  Empresa, QrWBMovIts, fiwPesagem);
*)
end;

//Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!

end.


