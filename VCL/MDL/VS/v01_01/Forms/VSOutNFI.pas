unit VSOutNFI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup,
  UnProjGroup_Consts;

type
  TFmVSOutNFI = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdItemNFe: TdmkEdit;
    Label2: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    RGTpCalcVal: TdmkRadioGroup;
    EdValorU: TdmkEdit;
    Label4: TLabel;
    QrTribDefCab: TmySQLQuery;
    QrTribDefCabCodigo: TIntegerField;
    QrTribDefCabNome: TWideStringField;
    DsTribDefCab: TDataSource;
    LaTribDefSel: TLabel;
    EdTribDefSel: TdmkEditCB;
    CBTribDefSel: TdmkDBLookupComboBox;
    SbTribDefSel: TSpeedButton;
    BtSomas: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTribDefSelClick(Sender: TObject);
    procedure CalculaTotal(Sender: TObject);
    procedure BtSomasClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSOutNFI(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts, FQrTribIncIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FMovimCod: Integer;
    FDataHora: TDateTime;
  end;

  var
  FmVSOutNFI: TFmVSOutNFI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF, UnTributos_PF, VSOutCab;

{$R *.DFM}

procedure TFmVSOutNFI.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ItemNFe, GraGruX, TpCalcVal, TribDefSel: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, ValorU, ValorFat, BaseCalc: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  ItemNFe        := EdItemNFe.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  TpCalcVal      := RGTpCalcVal.ItemIndex;
  ValorU         := EdValorU.ValueVariant;
  TribDefSel     := EdTribDefSel.ValueVariant;
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o reduzido do artigo!') then
    Exit;
  if MyObjects.FIC(TpCalcVal = 0, RGTpCalcVal, 'Informe a unidade de faturamento!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('vsoutnfi', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsoutnfi', False, [
  'Codigo', 'ItemNFe', 'GraGruX',
  'Pecas', 'PesoKg', 'AreaM2',
  'AreaP2', 'ValorT', 'TpCalcVal',
  'ValorU', 'TribDefSel'], [
  'Controle'], [
  Codigo, ItemNFe, GraGruX,
  Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, TpCalcVal,
  ValorU, TribDefSel], [
  Controle], True) then
  begin
    if FmVSOutCab.QrTribIncIts.RecordCount = 0 then
    begin
      Tributos_PF.Tributos_Insere(VAR_FATID_1009, Codigo, ItemNFe, FEmpresa,
      TribDefSel, ValorT, ValorT, FDataHora, siNegativo);
    end else
    begin
      ReopenVSOutNFI(Controle);
      //
      ValorFat := ValorT;
      BaseCalc := ValorT;
      Tributos_PF.Tributos_GerenciaAlt(FQrTribIncIts, ValorFat, BaseCalc);
    end;
    //
    ReopenVSOutNFI(Controle);
    //
    Close;
  end;
end;

procedure TFmVSOutNFI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutNFI.BtSomasClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  ItemNFe: Integer;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
begin
  ItemNFe        := EdItemNFe.ValueVariant;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(PesoKg) PesoKg ',
    'FROM ' + CO_TAB_VMI + ' vmi  ',
    'WHERE vmi.MovimCod=' + Geral.FF0(FMovimCod),
    'AND ItemNFe=' + Geral.FF0(ItemNFe),
    '']);
    //
    EdPecas.ValueVariant      := -Qry.FieldByName('Pecas').AsFloat;
    EdPesoKg.ValueVariant     := -Qry.FieldByName('PesoKg').AsFloat;
    EdAreaM2.ValueVariant     := -Qry.FieldByName('AreaM2').AsFloat;
    EdAreaP2.ValueVariant     := -Qry.FieldByName('AreaP2').AsFloat;
    //
    EdValorU.SetFocus;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSOutNFI.CalculaTotal(Sender: TObject);
var
  Qtde, ValorU, ValorT: Double;
begin
  ValorU := EdValorU.ValueVariant;
  ValorT := 0;
  case TTipoCalcCouro(RGTpCalcVal.ItemIndex) of
    ptcNaoInfo(*0*): Qtde := 0;
    ptcPecas(*1*)  : Qtde := EdPecas.ValueVariant;
    ptcPesoKg(*2*) : Qtde := EdPesoKg.ValueVariant;
    ptcAreaM2(*3*) : Qtde := EdAreaM2.ValueVariant;
    ptcAreaP2(*4*) : Qtde := EdAreaP2.ValueVariant;
    ptcTotal(*5*)  : Qtde := 1;
    else
    begin
      Geral.MB_Erro('Unidade de faturamento indefida em "CalculaTotal()"');
      Qtde := 0;
    end;
  end;
  if TTipoCalcCouro(RGTpCalcVal.ItemIndex) <> ptcTotal then
  begin
    ValorT := ValorU * Qtde;
    EdValorT.ValueVariant := ValorT;
  end;
end;

procedure TFmVSOutNFI.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutNFI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_PF.AbreGraGruXY(QrGraGruX, 'WHERE ggx.Controle > 0');
  //  'WHERE ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));
  UnDmkDAC_PF.AbreQuery(QrTribDefCab, Dmod.MyDB);
end;

procedure TFmVSOutNFI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutNFI.ReopenVSOutNFI(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutNFI.SbTribDefSelClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Tributos_PF.MostraFormTribDefCab(EdTribDefSel.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdTribDefSel, CBTribDefSel, QrTribDefCab, VAR_CADASTRO);
end;

end.
