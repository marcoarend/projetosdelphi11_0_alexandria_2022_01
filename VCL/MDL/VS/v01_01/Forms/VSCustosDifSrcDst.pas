unit VSCustosDifSrcDst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSCustosDifSrcDst = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrGraGruXGrandeza: TSmallintField;
    DsGraGruX: TDataSource;
    PnPesquisa: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSCustosDifSrcDst: TFmVSCustosDifSrcDst;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF, ModuleGeral;

{$R *.DFM}

procedure TFmVSCustosDifSrcDst.BtOKClick(Sender: TObject);
var
  sControle, Tabela: String;
  ValorT: Double;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrCustosDifSrcDst.First;
  while not DmModVS.QrCustosDifSrcDst.Eof do
  begin
    if DmModVS.QrCustosDifSrcDstRnControle.Value <> 0 then
    begin
      case DmModVS.QrCustosDifSrcDstRnID_TTW.Value of
        0: Tabela := CO_TAB_VMI;
        1: Tabela := CO_TAB_VMB;
        else Tabela := '???';
      end;
      sControle := Geral.FF0(DmModVS.QrCustosDifSrcDstRnControle.Value);
      // ini 2023-04-15
      (*
      ValorT := DmModVS.QrCustosDifSrcDstRpCusValr.Value * DmModVS.QrCustosDifSrcDstRnCusQtde.Value;
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE ' + Tabela + ' SET ' +
        ' ValorT=' + Geral.FFT_Dot(ValorT, 10, siNegativo) +
        ' WHERE Controle=' + sControle);
      *)
      //Campo := DmModVS.QrCustosDifSrcDstCampo.Value;

      //Fazer upd no Total e Valor MP por Diferenša se elfor diferente de 0!
      ValorT := DmModVS.QrCustosDifSrcDstRpCusValr.Value * DmModVS.QrCustosDifSrcDstRnCusQtde.Value;
      //if DmModVS.QrCustosDifSrcDstValorT.Value * DmModVS.QrCustosDifSrcDstRnCusQtde.Value;
      if DmModVS.QrCustosDifSrcDstValorMP.Value <> 0 then
      begin
        (*
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE ' + Tabela + ' SET ' +
        ' ValorT = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) +
        ', ValorMP = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) + ' - (CustoPQ + CustoMOTot) ' +
        ' WHERE Controle=' + sControle);
        *)
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE ' + Tabela + ' SET ' +
        ' ValorMP = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) +
        ', ValorT = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) + ' + (CustoPQ + CustoMOTot) ' +
        ' WHERE Controle=' + sControle);
      end else
      begin
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE ' + Tabela + ' SET ' +
        ' ValorT = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) +
        ' WHERE Controle=' + sControle);
      end;
      // fim 2023-04-15
      //
    end;
    DmModVS.QrCustosDifSrcDst.Next;
  end;
  DmModVS.QrCustosDifSrcDst.Close;
  DmModVS.QrCustosDifSrcDst.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSrcDst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCustosDifSrcDst.BtTeste2Click(Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = False;
  SelfCall = True;
  MargemErro = 0.001;
var
  Filial, Empresa, GraGruX, TemIMEIMrt: Integer;
begin
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  GraGruX := EdGraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
  //
  DmModVS.CustosDiferentesOrigemParaDestino2(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEIMrt, LaAviso1, LaAviso2);
end;

procedure TFmVSCustosDifSrcDst.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'RpControle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCustosDifSrcDstRpControle.Value)
  else
  if Campo = 'RnControle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCustosDifSrcDstRnControle.Value);
end;

procedure TFmVSCustosDifSrcDst.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCustosDifSrcDst.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSCustosDifSrcDst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
