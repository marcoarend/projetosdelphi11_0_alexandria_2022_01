unit VSRibCla;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO, BlueDermConsts;

type
  TFmVSRibCla = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSRibCla: TmySQLQuery;
    DsVSRibCla: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSRibClaGraGruX: TIntegerField;
    QrVSRibClaLk: TIntegerField;
    QrVSRibClaDataCad: TDateField;
    QrVSRibClaDataAlt: TDateField;
    QrVSRibClaUserCad: TIntegerField;
    QrVSRibClaUserAlt: TIntegerField;
    QrVSRibClaAlterWeb: TSmallintField;
    QrVSRibClaAtivo: TSmallintField;
    QrVSRibClaGraGru1: TIntegerField;
    QrVSRibClaNO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    QrVSRibArt: TmySQLQuery;
    DsVSRibArt: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    QrVSRibArtGraGruX: TIntegerField;
    QrVSRibArtNO_PRD_TAM_COR: TWideStringField;
    QrVSRibArtCodigo: TIntegerField;
    QrVSRibArtVSRibCla: TIntegerField;
    QrVSRibArtVSRibCad: TIntegerField;
    QrVSRibArtLk: TIntegerField;
    QrVSRibArtDataCad: TDateField;
    QrVSRibArtDataAlt: TDateField;
    QrVSRibArtUserCad: TIntegerField;
    QrVSRibArtUserAlt: TIntegerField;
    QrVSRibArtAlterWeb: TSmallintField;
    QrVSRibArtAtivo: TSmallintField;
    Incluinovoartigoclassificado1: TMenuItem;
    AlteraArtigoClassificadoAtual1: TMenuItem;
    ExcluiArtigoClassificadoAtual1: TMenuItem;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    DsGraGruXCou: TDataSource;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSRibClaArtigoImp: TWideStringField;
    QrVSRibClaClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label11: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    EdMediaMinM2: TdmkEdit;
    Label13: TLabel;
    EdMediaMaxM2: TdmkEdit;
    Label14: TLabel;
    QrVSRibClaMediaMinM2: TFloatField;
    QrVSRibClaMediaMaxM2: TFloatField;
    Label17: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label18: TLabel;
    DBEdit2: TDBEdit;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    Label19: TLabel;
    EdMediaMinKg: TdmkEdit;
    EdMediaMaxKg: TdmkEdit;
    Label20: TLabel;
    QrVSRibClaMediaMinKg: TFloatField;
    QrVSRibClaMediaMaxKg: TFloatField;
    Label21: TLabel;
    Label22: TLabel;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    Label23: TLabel;
    EdPrevAMPal: TdmkEdit;
    EdPrevKgPal: TdmkEdit;
    Label24: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    Label25: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label26: TLabel;
    QrGraGruXCouGrandeza: TSmallintField;
    RGBastidao: TdmkRadioGroup;
    QrGraGruXCouBastidao: TSmallintField;
    Label27: TLabel;
    EdFatrClase: TdmkEdit;
    QrGraGruXCouFatrClase: TFloatField;
    Label28: TLabel;
    QrVSRibClaFatrClase: TFloatField;
    DBEdit14: TDBEdit;
    Label29: TLabel;
    EdBaseValVenda: TdmkEdit;
    QrGraGruXCouBaseValVenda: TFloatField;
    Label30: TLabel;
    EdBaseCliente: TdmkEdit;
    EdBaseImpostos: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    EdBasePerComis: TdmkEdit;
    Label33: TLabel;
    EdBasFrteVendM2: TdmkEdit;
    QrGraGruXCouBaseCliente: TWideStringField;
    QrGraGruXCouBaseImpostos: TFloatField;
    QrGraGruXCouBasePerComis: TFloatField;
    QrGraGruXCouBasFrteVendM2: TFloatField;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    EdBaseValLiq: TdmkEdit;
    Label34: TLabel;
    QrGraGruXCouBaseValLiq: TFloatField;
    DBEdit20: TDBEdit;
    Label35: TLabel;
    EdBaseValCusto: TdmkEdit;
    Label36: TLabel;
    DBEdit21: TDBEdit;
    Label37: TLabel;
    QrGraGruXCouBaseValCusto: TFloatField;
    QrGraGruXCouArtGeComodty: TIntegerField;
    LaArtGerComodty: TLabel;
    EdArtGeComodty: TdmkEditCB;
    CBArtGeComodty: TdmkDBLookupComboBox;
    SbArtGeComodty: TSpeedButton;
    QrArtGeComodty: TMySQLQuery;
    QrArtGeComodtyCodigo: TIntegerField;
    QrArtGeComodtyNome: TWideStringField;
    DsArtGeComodty: TDataSource;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRibClaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRibClaBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrVSRibClaBeforeClose(DataSet: TDataSet);
    procedure QrVSRibClaAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure Incluinovoartigoclassificado1Click(Sender: TObject);
    procedure AlteraArtigoClassificadoAtual1Click(Sender: TObject);
    procedure ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
    procedure EdBaseValVendaRedefinido(Sender: TObject);
    procedure EdBaseImpostosRedefinido(Sender: TObject);
    procedure EdBasePerComisRedefinido(Sender: TObject);
    procedure EdBasFrteVendM2Redefinido(Sender: TObject);
    procedure SbArtGeComodtyClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSRibArt(SQLType: TSQLType);
    procedure ReopenVSRibArt(Codigo: Integer);
    procedure CalculaBaseValLiq();

  public
    { Public declarations }
    FSeq: Integer;
    FQryMul: TmySQLQuery;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSRibCla: TFmVSRibCla;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSRibArt, UnVS_PF, AppListas, UnGrade_PF, UnGrade_Jan, UnVS_Jan, ArtGeComodty;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSRibCla.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSRibCla.MostraVSRibArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSRibArt, FmVSRibArt, afmoNegarComAviso) then
  begin
    FmVSRibArt.ImgTipo.SQLType := SQLType;
    FmVSRibArt.FQrCab := QrVSRibCla;
    FmVSRibArt.FDsCab := DsVSRibCla;
    FmVSRibArt.FQrIts := QrVSRibArt;
    //
    FmVSRibArt.LaVSRibCla.Enabled := False;
    FmVSRibArt.EdVSRibCla.Enabled := False;
    FmVSRibArt.CBVSRibCla.Enabled := False;
    //
    FmVSRibArt.LaVSRibCad.Enabled := True;
    FmVSRibArt.EdVSRibCad.Enabled := True;
    FmVSRibArt.CBVSRibCad.Enabled := True;
    //

    FmVSRibArt.EdVSRibCla.ValueVariant := QrVSRibClaGraGruX.Value;
    FmVSRibArt.CBVSRibCla.KeyValue     := QrVSRibClaGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSRibArt.FCodigo := 0;
    end else
    begin
      FmVSRibArt.FCodigo := QrVSRibArtCodigo.Value;
      //
      FmVSRibArt.EdVSRibCad.ValueVariant := QrVSRibArtVSRibCad.Value;
      FmVSRibArt.CBVSRibCad.KeyValue     := QrVSRibArtVSRibCad.Value;
      //
    end;
    FmVSRibArt.ShowModal;
    FmVSRibArt.Destroy;
  end;
end;

procedure TFmVSRibCla.PMArtigoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrVSRibCla);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSRibArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSRibArt);
end;

procedure TFmVSRibCla.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraArtigoClassificadoAtual1, QrVSRibCla);
  //MyObjects.HabilitaMenuItemCabDel(ExcluiArtigoClassificadoAtual1, QrVSRibCla);
end;

procedure TFmVSRibCla.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRibClaGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRibCla.DefParams;
begin
  VAR_GOTOTABELA := 'vsribcla';
  VAR_GOTOMYSQLTABLE := QrVSRibCla;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR, cou.FatrClase, BaseValVenda, BaseCliente,');
  VAR_SQLx.Add('BaseImpostos, BasePerComis, BasFrteVendM2, BaseValLiq ');
  VAR_SQLx.Add('FROM vsribcla wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSRibCla.EdBaseImpostosRedefinido(Sender: TObject);
begin
  CalculaBaseValLiq();
end;

procedure TFmVSRibCla.EdBasePerComisRedefinido(Sender: TObject);
begin
  CalculaBaseValLiq();
end;

procedure TFmVSRibCla.EdBaseValVendaRedefinido(Sender: TObject);
begin
  CalculaBaseValLiq();
end;

procedure TFmVSRibCla.EdBasFrteVendM2Redefinido(Sender: TObject);
begin
  CalculaBaseValLiq();
end;

procedure TFmVSRibCla.ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
begin
//
end;

procedure TFmVSRibCla.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de artigo selecionado?',
  'VSRibArt', 'Codigo', QrVSRibArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSRibArt,
      QrVSRibArtCodigo, QrVSRibArtCodigo.Value);
    ReopenVSRibArt(Codigo);
  end;
end;

procedure TFmVSRibCla.CalculaBaseValLiq();
var
  FatrClase, BaseValVenda, BaseImpostos, BasePerComis, BasFrteVendM2,
  BaseValLiq: Double;
begin
  BaseValVenda   := EdBaseValVenda.ValueVariant;
  BaseImpostos   := EdBaseImpostos.ValueVariant;
  BasePerComis   := EdBasePerComis.ValueVariant;
  BasFrteVendM2  := EdBasFrteVendM2.ValueVariant;
  //
  //BaseValLiq     := BaseValVenda - (BaseValVenda * (BaseImpostos + BasePerComis) / 100) - BasFrteVendM2;
  BaseValLiq := VS_PF.CalculaBaseValLiq(BaseValVenda, BaseImpostos, BasePerComis, BasFrteVendM2);
  EdBaseValLiq.ValueVariant := BaseValLiq;
end;

procedure TFmVSRibCla.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSRibCla.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRibCla.ReopenVSRibArt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsribart vna ',
  'LEFT JOIN vsribcad wmp ON wmp.GraGruX=vna.VSRibCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSRibCla=' + Geral.FF0(QrVSRibClaGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSRibCla.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRibCla.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRibCla.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRibCla.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRibCla.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRibCla.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRibCla.AlteraArtigoClassificadoAtual1Click(Sender: TObject);
begin
  EdMediaMinM2.ValueVariant := QrVSRibClaMediaMinM2.Value;
  EdMediaMaxM2.ValueVariant := QrVSRibClaMediaMaxM2.Value;
  EdMediaMinKg.ValueVariant := QrVSRibClaMediaMinKg.Value;
  EdMediaMaxKg.ValueVariant := QrVSRibClaMediaMaxKg.Value;
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSRibClaGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdCouNiv2, ImgTipo, 'gragruxcou');
  //EdBastidao.ItemIndex := ?
end;

procedure TFmVSRibCla.Alteralinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stUpd);
end;

procedure TFmVSRibCla.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRibClaGraGruX.Value;
  Close;
end;

procedure TFmVSRibCla.BtConfirmaClick(Sender: TObject);
(*
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
  Grandeza      = -1;
  MediaMinKg    = 0;
  MediaMaxKg    = 0;
  //GGXPronto     = 0;
  FatrClase     = 0;
  BaseValCusto  = 0;
  BaseValVenda  = 0;
  BaseCliente   = '';
  BaseImpostos  = 0;
  BasePerComis  = 0;
  BasFrteVendM2 = 0;
  BaseValLiq    = 0;
*)
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, Bastidao, GGXPronto: Integer;
  ArtigoImp, ClasseImp, Nome, BaseCliente: String;
  MediaMinM2, MediaMaxM2, MediaMinKg, MediaMaxKg, PrevAMPal, PrevKgPal,
  FatrClase, BaseValVenda, BaseImpostos, BasePerComis, BasFrteVendM2,
  BaseValLiq, BaseValCusto: Double;
  ArtGeComodty: Integer;
begin
  GraGruX        := QrVSRibClaGraGruX.Value;
  GraGru1        := QrVSRibClaGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.ValueVariant;
  ClasseImp      := EdClasseImp.ValueVariant;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  MediaMinM2     := EdMediaMinM2.ValueVariant;
  MediaMaxM2     := EdMediaMaxM2.ValueVariant;
  MediaMinKg     := EdMediaMinKg.ValueVariant;
  MediaMaxKg     := EdMediaMaxKg.ValueVariant;
  Nome           := EdNome.Text;
  Bastidao       := RGBastidao.ItemIndex;
  GGXPronto      := 0;
  FatrClase      := EdFatrClase.ValueVariant;
  BaseValCusto   := EdBaseValCusto.ValueVariant;
  BaseValVenda   := EdBaseValVenda.ValueVariant;
  BaseCliente    := EdBaseCliente.ValueVariant;
  BaseImpostos   := EdBaseImpostos.ValueVariant;
  BasePerComis   := EdBasePerComis.ValueVariant;
  BasFrteVendM2  := EdBasFrteVendM2.ValueVariant;
  BaseValLiq     := EdBaseValLiq.ValueVariant;
  ArtGeComodty   := EdArtGeComodty.ValueVariant;
  //
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  if CouNiv2 = CO_COU_NIV2_001_COURO then
  begin
    if MyObjects.FIC(MediaMinM2 = 0, EdMediaMinM2, 'Informe a "M�dia m�n. m�"!') then
      Exit;
    if MyObjects.FIC(MediaMaxM2 = 0, EdMediaMaxM2, 'Informe a "M�dia m�x. m�"!') then
      Exit;
    if MyObjects.FIC(MediaMaxM2 <= MediaMinM2, EdMediaMaxM2,
    'A m�dia m�xima m� deve se superior a m�dia m�nima!') then
      Exit;
  end else
  begin
    if MyObjects.FIC(MediaMinKg = 0, EdMediaMinKg, 'Informe a "M�dia m�n. kg"!') then
      Exit;
    if MyObjects.FIC(MediaMaxKg = 0, EdMediaMaxKg, 'Informe a "M�dia m�x. kg"!') then
      Exit;
    if MyObjects.FIC(MediaMaxKg <= MediaMinKg, EdMediaMaxKg,
    'A m�dia m�xima kg deve se superior a m�dia m�nima!') then
      Exit;
  end;
  //
  VS_PF.ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1,
    Bastidao, PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2,
    MediaMaxM2, MediaMinKg, MediaMaxKg, GGXPronto, FatrClase, BaseValCusto,
    BaseValVenda, BaseCliente, BaseImpostos, BasePerComis, BasFrteVendM2, BaseValLiq,
    ArtGeComodty);
  //
  if FQryMul <> nil then
  begin
    FQryMul.First;
    while not FQryMul.Eof do
    begin
      GraGruX := FQryMul.FieldByName('Controle').AsInteger;
      VS_PF.ReIncluiCouNivs(
        GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, Bastidao,
        PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2,
        MediaMinKg, MediaMaxKg, GGXPronto, FatrClase, BaseValCusto, BaseValVenda,
        BaseCliente, BaseImpostos, BasePerComis, BasFrteVendM2, BaseValLiq,
        ArtGeComodty);
      FQryMul.Next;
    end;
  //
  end;
  Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  if FSeq = 0 then
  begin
    LocCod(GraGruX, GraGruX);
  end else
    Close;
end;

procedure TFmVSRibCla.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX        := QrVSRibClaGraGruX.Value;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'vsribcla', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSRibCla.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmVSRibCla.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSRibCla.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  VS_PF.ConfiguraRGVSBastidao(RGBastidao, True(*Habilita*), (*Default*)-1);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrArtGeComodty, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmVSRibCla.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRibClaGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSRibCla.SbArtGeComodtyClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_Jan.MostraFormArtGeComodty(EdArtGeComodty.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdArtGeComodty, CBArtGeComodty, QrArtGeComodty, VAR_CADASTRO);
end;

procedure TFmVSRibCla.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsribcla wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmVSRibCla.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  SbQueryClick(Self);
end;

procedure TFmVSRibCla.SbNovoClick(Sender: TObject);
begin
  VS_Jan.MostraFormGraGruYPrecos(CO_GraGruY_3072_VSRibCla);
end;

procedure TFmVSRibCla.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRibCla.QrVSRibClaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRibCla.QrVSRibClaAfterScroll(DataSet: TDataSet);
begin
  ReopenVSRibArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSRibClaGraGruX.Value);
end;

procedure TFmVSRibCla.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRibCla.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
  Filtro: String;
  SQL: String;
begin
  (*
  LocCod(QrVSRibClaGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vsribcla', Dmod.MyDB, CO_VAZIO));
  *)
  SQL := Geral.ATS([
  'SELECT wmp.GraGruX _CODIGO, gg1.Nome _NOME ',
  'FROM vsribcla wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE gg1.Nome LIKE "%' + CO_JOKE_SQL + '%" ',
  'OR gg1.Referencia LIKE "%' + CO_JOKE_SQL + '%" ',
  'OR gg1.Patrimonio LIKE "%' + CO_JOKE_SQL + '%" ',
  'OR ggx.EAN13 LIKE "%' + CO_JOKE_SQL + '%" ',
  '']);
  FmMeuDBUses.PesquisaNomeSQLeJoke(SQL, CO_JOKE_SQL);
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmVSRibCla.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRibCla.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stIns);
end;

procedure TFmVSRibCla.Incluinovoartigoclassificado1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
(*
  DefinedSQL := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE %s ',
  'ORDER BY _Nome ',
  '']);
  FmMeuDBUses.PesquisaNome('', '', '', DefinedSQL);
  GraGruX := VAR_CADASTRO;
*)
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_3072_VSRibCla);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      //ShowMessage(Geral.FF0(VAR_CADASTRO));
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsribcla ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsribcla', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True) then
        begin
          //VS_PF.DefineSubProd(GraGruX, 'vsribcla');  'SubProd'  ???
        end;
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSRibCla.QrVSRibClaBeforeClose(DataSet: TDataSet);
begin
  QrVSRibArt.Close;
  QrGraGruXCou.Close;
end;

procedure TFmVSRibCla.QrVSRibClaBeforeOpen(DataSet: TDataSet);
begin
  QrVSRibClaGraGruX.DisplayFormat := FFormatFloat;
end;

end.

