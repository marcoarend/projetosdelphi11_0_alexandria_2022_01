object FmVSPaMulItsR: TFmVSPaMulItsR
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-061 :: Item de Classifica'#231#227'o de Artigo de Ribeira - M'#250 +
    'ltiplo'
  ClientHeight = 670
  ClientWidth = 736
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 506
    Width = 736
    Height = 50
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object RGModoContinuarInserindo: TRadioGroup
      Left = 153
      Top = 0
      Width = 452
      Height = 50
      Align = alLeft
      Caption = ' Modo de continuar inserindo: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Mais seguro'
        'Mais r'#225'pido')
      TabOrder = 1
    end
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 153
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object CkContinuar: TCheckBox
        Left = 8
        Top = 20
        Width = 141
        Height = 17
        Caption = 'Continuar inserindo: >>'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 736
    Height = 61
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    TabOrder = 0
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 732
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 0
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 180
        Top = 0
        Width = 55
        Height = 13
        Caption = 'ID estoque:'
        FocusControl = DBEdMovimCod
      end
      object Label3: TLabel
        Left = 264
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object Label9: TLabel
        Left = 312
        Top = 0
        Width = 64
        Height = 13
        Caption = 'S'#233'rie / Ficha:'
      end
      object Label15: TLabel
        Left = 96
        Top = 0
        Width = 50
        Height = 13
        Caption = 'ID Classif.:'
        FocusControl = DBEdCacCod
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdMovimCod: TdmkDBEdit
        Left = 180
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 264
        Top = 16
        Width = 45
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 316
        Top = 16
        Width = 37
        Height = 21
        DataField = 'SerieFch'
        DataSource = DsOrig
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 352
        Top = 16
        Width = 80
        Height = 21
        DataField = 'Ficha'
        DataSource = DsOrig
        TabOrder = 4
      end
      object DBEdCacCod: TdmkDBEdit
        Left = 96
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'CacCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 5
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 109
    Width = 736
    Height = 312
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 732
      Height = 46
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label6: TLabel
        Left = 12
        Top = 4
        Width = 58
        Height = 13
        Caption = 'ID Gera'#231#227'o:'
      end
      object Label10: TLabel
        Left = 96
        Top = 4
        Width = 43
        Height = 13
        Caption = 'ID Baixa:'
      end
      object Label11: TLabel
        Left = 180
        Top = 4
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
      end
      object Label17: TLabel
        Left = 264
        Top = 4
        Width = 30
        Height = 13
        Caption = 'CAC-I:'
      end
      object Label18: TLabel
        Left = 348
        Top = 4
        Width = 88
        Height = 13
        Caption = 'Data / hora CAC-I:'
        FocusControl = DBEdit3
      end
      object EdCtrlGera: TdmkEdit
        Left = 12
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCtrlBaix: TdmkEdit
        Left = 96
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 180
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCACI: TdmkEdit
        Left = 264
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object DBEdit3: TDBEdit
        Left = 348
        Top = 20
        Width = 257
        Height = 21
        DataField = 'DataHora'
        DataSource = DataSource1
        TabOrder = 4
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 61
      Width = 732
      Height = 249
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 119
        Height = 13
        Caption = 'Artigo (F4 - '#250'ltima usada):'
      end
      object Label4: TLabel
        Left = 12
        Top = 44
        Width = 235
        Height = 13
        Caption = 'Pallet [Insert='#39'+'#39'] (Ctrl + D = desfaz encerramento):'
      end
      object LaPecas: TLabel
        Left = 12
        Top = 84
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object LaAreaM2: TLabel
        Left = 116
        Top = 84
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object LaAreaP2: TLabel
        Left = 220
        Top = 84
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object LaPeso: TLabel
        Left = 324
        Top = 84
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label8: TLabel
        Left = 440
        Top = 124
        Width = 76
        Height = 13
        Caption = '$ Total M-prima:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 524
        Top = 124
        Width = 62
        Height = 13
        Caption = '$/kg M-obra:'
        Enabled = False
      end
      object Label13: TLabel
        Left = 12
        Top = 124
        Width = 68
        Height = 13
        Caption = '$ total M-obra:'
        Enabled = False
      end
      object Label14: TLabel
        Left = 96
        Top = 124
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label7: TLabel
        Left = 428
        Top = 84
        Width = 36
        Height = 13
        Caption = '$ Total:'
        Enabled = False
      end
      object Label16: TLabel
        Left = 12
        Top = 164
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object SBPallet: TSpeedButton
        Left = 306
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBPalletClick
      end
      object SBNewPallet: TSpeedButton
        Left = 327
        Top = 60
        Width = 21
        Height = 21
        Caption = '+'
        OnClick = SBNewPalletClick
      end
      object Label71: TLabel
        Left = 12
        Top = 204
        Width = 45
        Height = 13
        Caption = 'Digitador:'
      end
      object Label70: TLabel
        Left = 312
        Top = 204
        Width = 62
        Height = 13
        Caption = 'Classificador:'
      end
      object SbValorT: TSpeedButton
        Left = 584
        Top = 100
        Width = 21
        Height = 21
        OnClick = SbValorTClick
      end
      object Label53: TLabel
        Left = 456
        Top = 44
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object SBNewByInfo: TSpeedButton
        Left = 348
        Top = 60
        Width = 21
        Height = 21
        Caption = 'i'
        OnClick = SBNewByInfoClick
      end
      object EdGraGruX: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGraGruXKeyDown
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 537
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSRibCla
        TabOrder = 1
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7TableName = 'gragrux'
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPallet: TdmkEditCB
        Left = 12
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pallet'
        UpdCampo = 'Pallet'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdPalletKeyDown
        OnRedefinido = EdPalletRedefinido
        DBLookupComboBox = CBPallet
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPallet: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 237
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPallet
        TabOrder = 3
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdPallet
        QryCampo = 'Pallet'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPecas: TdmkEdit
        Left = 12
        Top = 100
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 116
        Top = 100
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdAreaM2Change
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 220
        Top = 100
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 324
        Top = 100
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPesoKgChange
      end
      object EdValorMP: TdmkEdit
        Left = 440
        Top = 140
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'ValorMP'
        UpdCampo = 'ValorMP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValorMPChange
      end
      object EdCustoMOKg: TdmkEdit
        Left = 524
        Top = 140
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'CustoMOKg'
        UpdCampo = 'CustoMOKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdCustoMOKgChange
      end
      object EdCustoMOTot: TdmkEdit
        Left = 12
        Top = 140
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'CustoMOTot'
        UpdCampo = 'CustoMOTot'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValorMPChange
      end
      object EdFornecMO: TdmkEditCB
        Left = 96
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FornecMO'
        UpdCampo = 'FornecMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 152
        Top = 140
        Width = 285
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 14
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdFornecMO
        QryCampo = 'Pallet'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdValorT: TdmkEdit
        Left = 428
        Top = 100
        Width = 153
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValorT'
        UpdCampo = 'ValorT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 12
        Top = 180
        Width = 593
        Height = 21
        TabOrder = 17
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 68
        Top = 220
        Width = 236
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        TabOrder = 19
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDigitador: TdmkEditCB
        Left = 12
        Top = 220
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object EdRevisor: TdmkEditCB
        Left = 312
        Top = 220
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 368
        Top = 220
        Width = 236
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        TabOrder = 21
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkEncerraPallet: TdmkCheckBox
        Left = 374
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Encer. Pallet.'
        TabOrder = 4
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object TPData: TdmkEditDateTimePicker
        Left = 456
        Top = 60
        Width = 108
        Height = 21
        Date = 44772.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 564
        Top = 60
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 736
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 688
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 640
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 627
        Height = 32
        Caption = 'Item de Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 627
        Height = 32
        Caption = 'Item de Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 627
        Height = 32
        Caption = 'Item de Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 556
    Width = 736
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 732
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 600
    Width = 736
    Height = 70
    Align = alBottom
    TabOrder = 7
    object PnSaiDesis: TPanel
      Left = 590
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 588
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 421
    Width = 736
    Height = 44
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label49: TLabel
      Left = 12
      Top = 3
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 510
      Top = 3
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object SbStqCenLoc: TSpeedButton
      Left = 487
      Top = 18
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbStqCenLocClick
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 67
      Top = 19
      Width = 418
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 1
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReqMovEstq: TdmkEdit
      Left = 511
      Top = 19
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 465
    Width = 736
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Label20: TLabel
      Left = 423
      Top = 16
      Width = 29
      Height = 13
      Caption = 'Folha:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label21: TLabel
      Left = 515
      Top = 16
      Width = 29
      Height = 13
      Caption = 'Linha:'
      Color = clBtnFace
      ParentColor = False
    end
    object RGIxxMovIX: TRadioGroup
      Left = 0
      Top = 0
      Width = 416
      Height = 41
      Align = alLeft
      Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Entrada de couro'
        'Classe / reclasse'
        'Sa'#237'da de couro')
      TabOrder = 0
    end
    object EdIxxFolha: TdmkEdit
      Left = 456
      Top = 12
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdIxxLinha: TdmkEdit
      Left = 548
      Top = 12
      Width = 33
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Grandeza'
      'FROM defpecas')
    Left = 696
    Top = 148
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 700
    Top = 196
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 632
    Top = 240
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 632
    Top = 288
  end
  object QrOrig: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovits'
      'WHERE Controle=0')
    Left = 628
    Top = 52
    object QrOrigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOrigControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOrigMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrOrigMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrOrigMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrOrigEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOrigTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrOrigCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrOrigMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrOrigLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrOrigLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrOrigDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrOrigPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrOrigGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOrigPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrOrigPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrOrigAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrOrigAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrOrigValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrOrigSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrOrigSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrOrigSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrOrigSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrOrigSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrOrigSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrOrigSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrOrigObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrOrigSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrOrigFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrOrigMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrOrigFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrOrigCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrOrigCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrOrigValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrOrigDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrOrigDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrOrigDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrOrigDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrOrigQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrOrigQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrOrigQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrOrigQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrOrigQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrOrigQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrOrigQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrOrigQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrOrigAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrOrigNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrOrigMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrOrigLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOrigDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOrigDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOrigUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOrigUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOrigAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOrigAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOrigTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrOrigVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrOrigClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrOrigNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrOrigNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrOrigVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
  end
  object DsOrig: TDataSource
    DataSet = QrOrig
    Left = 628
    Top = 100
  end
  object QrVSRibCla: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmp.GraGruX, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, cou.PrevPcPal, ggx.GraGruY'
      'FROM vsribcla wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'ORDER BY NO_PRD_TAM_COR'
      '')
    Left = 632
    Top = 148
    object QrVSRibClaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSRibClaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSRibClaPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrVSRibClaGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsVSRibCla: TDataSource
    DataSet = QrVSRibCla
    Left = 632
    Top = 196
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 700
    Top = 53
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 700
    Top = 101
  end
  object QrRevisores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 700
    Top = 332
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 700
    Top = 376
  end
  object QrDigitadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 632
    Top = 332
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 632
    Top = 376
  end
  object QrVSCacItsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vscacitsa'
      'WHERE Controle=0')
    Left = 700
    Top = 236
    object QrVSCacItsACacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSCacItsACacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrVSCacItsACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCacItsAControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrVSCacItsAClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrVSCacItsARclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrVSCacItsARclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrVSCacItsAVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrVSCacItsAVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrVSCacItsAVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSCacItsAVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSCacItsAVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSCacItsAVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSCacItsABox: TIntegerField
      FieldName = 'Box'
    end
    object QrVSCacItsAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacItsAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSCacItsARevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrVSCacItsADigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrVSCacItsADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSCacItsASumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrVSCacItsAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCacItsAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCacItsAMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrVSCacItsAFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
  end
  object DataSource1: TDataSource
    DataSet = QrVSCacItsA
    Left = 632
    Top = 424
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
end
