unit VSGerArtMarcaImpBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet, UnGrl_Consts,
  System.UITypes, AppListas;

type
  TArrTipo = (arrtipoNenhum = 0, arrtipoOutrosEmids = 1, arrtipoEmid6 = 2);
  TFmVSGerArtMarcaImpBar = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrExec: TMySQLQuery;
    PB1: TProgressBar;
    QrDia: TMySQLQuery;
    QrDiaInnPecas: TFloatField;
    QrArtGeComodty: TMySQLQuery;
    QrArtGeComodtyArtGeComodty: TIntegerField;
    QrArtGeComodtySigla: TWideStringField;
    QrArtGeComodtyVisuRel: TIntegerField;
    QrArtGeComodtyShowPecas: TLargeintField;
    QrArtGeComodtyShowPesoKg: TLargeintField;
    QrArtGeComodtyShowAreaM2: TLargeintField;
    QrArtGeComodtyShowPercPc: TLargeintField;
    QrArtGeComodtyShowKgM2: TLargeintField;
    QrArtGeComodtyShowMediaM2: TLargeintField;
    frxWET_CURTI_242_1_A: TfrxReport;
    QrGafid: TMySQLQuery;
    BtImprime: TBitBtn;
    frxDsGafid: TfrxDBDataset;
    QrEmids: TMySQLQuery;
    QrEmidsNO_MovimID: TWideStringField;
    QrEmidsDstMovID: TLargeintField;
    QrVSInn: TMySQLQuery;
    QrVSInnGGXInn: TLargeintField;
    QrVSInnAnoMesDiaIn: TIntegerField;
    QrVSInnData: TWideStringField;
    QrVSInnInnPecas: TFloatField;
    QrVSInnInnPesoKg: TFloatField;
    QrVSInnInnKgPeca: TFloatField;
    QrVSInnNFePecas: TFloatField;
    QrVSInnCalPecas: TFloatField;
    QrVSInnInnSdoVrtPeso: TFloatField;
    QrVSInnInnSdoVrtPeca: TFloatField;
    QrNaoProc: TMySQLQuery;
    QrNaoProcInnSdoVrtPeca: TFloatField;
    QrOrfaos: TMySQLQuery;
    QrOrfaosGraGruX: TIntegerField;
    QrOrfaosGraGruY: TIntegerField;
    QrOrfaosNome: TWideStringField;
    DsOrfaos: TDataSource;
    QrVSGerCalCon: TMySQLQuery;
    QrVSGerCalConGGXInn: TLargeintField;
    QrVSGerCalConGraGruX: TLargeintField;
    QrVSGerCalConDstMovID: TLargeintField;
    QrVSGerCalConDstNivel1: TLargeintField;
    QrVSGerCalConAnoMesDiaIn: TIntegerField;
    QrVSGerCalConArtGeComodty: TIntegerField;
    QrVSGerCalConPecas: TFloatField;
    QrVSGerCalConPesoKg: TFloatField;
    QrVSGerCalConQtdGerArM2: TFloatField;
    QrVSGerCalConQtdGerArP2: TFloatField;
    QrVSGerCalConRendKgM2: TFloatField;
    QrVSGerCalConMediaM2: TFloatField;
    QrVSGerCalConInnPecas: TFloatField;
    QrVSGerCalConInnPesoKg: TFloatField;
    QrVSCalDst: TMySQLQuery;
    QrVSCalDstGGXInn: TLargeintField;
    QrVSCalDstGraGruX: TLargeintField;
    QrVSCalDstDstMovID: TLargeintField;
    QrVSCalDstDstNivel1: TLargeintField;
    QrVSCalDstAnoMesDiaIn: TIntegerField;
    QrVSCalDstArtGeComodty: TIntegerField;
    QrVSCalDstPecas: TFloatField;
    QrVSCalDstPesoKg: TFloatField;
    QrVSEmC: TMySQLQuery;
    QrVSEmCGGXInn: TLargeintField;
    QrVSEmCGraGruX: TLargeintField;
    QrVSEmCDstMovID: TLargeintField;
    QrVSEmCDstNivel1: TLargeintField;
    QrVSEmCAnoMesDiaIn: TIntegerField;
    QrVSEmCArtGeComodty: TIntegerField;
    QrVSEmCPecas: TFloatField;
    QrVSEmCPesoKg: TFloatField;
    QrG13Err: TMySQLQuery;
    DsG13Err: TDataSource;
    QrG13ErrCodigo: TIntegerField;
    QrG13ErrMovimCod: TIntegerField;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel5: TPanel;
    SbMesAnt: TSpeedButton;
    SpeedButton2: TSpeedButton;
    GroupBox2: TGroupBox;
    TPEntradaIni: TdmkEditDateTimePicker;
    CkEntradaIni: TCheckBox;
    CkEntradaFim: TCheckBox;
    TPEntradaFim: TdmkEditDateTimePicker;
    CkTemIMEIMrt: TCheckBox;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    MeMarcas: TMemo;
    QrVSEmCMarca: TWideStringField;
    QrVSCalDstMarca: TWideStringField;
    QrVSGerCalConMarca: TWideStringField;
    QrVSInnMarca: TWideStringField;
    QrClasses: TMySQLQuery;
    DsClasses: TDataSource;
    frxDsClasses: TfrxDBDataset;
    QrClassesReduzido: TIntegerField;
    QrClassesNome: TWideStringField;
    QrClassesPecas: TFloatField;
    QrClassesAreaM2: TFloatField;
    QrClassesAreaP2: TFloatField;
    QrClassesPercM2: TFloatField;
    QrClassesInteiros: TFloatField;
    Panel9: TPanel;
    Panel10: TPanel;
    MeLetras: TMemo;
    CkLetras: TCheckBox;
    QrVSGerCalConMovimID: TLargeintField;
    CkMarcas: TCheckBox;
    QrInnCon_: TMySQLQuery;
    QrInnCon_Controle: TIntegerField;
    QrInnCon_SrcNivel2: TIntegerField;
    QrInnCon_DataHora: TDateTimeField;
    QrInnCon_DtEntrada_TXT: TWideStringField;
    QrInnCon_AnoMesDiaIn: TLargeintField;
    QrVSMpC: TMySQLQuery;
    QrVSMpCGGXInn: TLargeintField;
    QrVSMpCGraGruX: TLargeintField;
    QrVSMpCDstMovID: TLargeintField;
    QrVSMpCDstNivel1: TLargeintField;
    QrVSMpCAnoMesDiaIn: TIntegerField;
    QrVSMpCArtGeComodty: TIntegerField;
    QrVSMpCPecas: TFloatField;
    QrVSMpCPesoKg: TFloatField;
    QrVSMpCMarca: TWideStringField;
    Query: TMySQLQuery;
    BitBtn1: TBitBtn;
    QrInnNat_: TMySQLQuery;
    QrInnNat_Controle: TIntegerField;
    QrInnNat_GraGruX: TIntegerField;
    QrInnNat_Marca: TWideStringField;
    QrInnNat_DtEntrada_TXT: TWideStringField;
    QrInnNat_AnoMesDiaIn: TLargeintField;
    QrVSCalSub: TMySQLQuery;
    QrVSCalSubGGXInn: TLargeintField;
    QrVSCalSubGraGruX: TLargeintField;
    QrVSCalSubDstMovID: TLargeintField;
    QrVSCalSubDstNivel1: TLargeintField;
    QrVSCalSubAnoMesDiaIn: TIntegerField;
    QrVSCalSubArtGeComodty: TIntegerField;
    QrVSCalSubPecas: TFloatField;
    QrVSCalSubPesoKg: TFloatField;
    QrVSCalSubMarca: TWideStringField;
    QrEmissoes: TMySQLQuery;
    QrEmissoesVSMovCod: TIntegerField;
    QrEmissoesFuloes: TWideMemoField;
    QrArtGeComodtyTipoTab: TFloatField;
    QrVSEmCurtim: TMySQLQuery;
    QrVSEmCurtimGGXInn: TLargeintField;
    QrVSEmCurtimGraGruX: TLargeintField;
    QrVSEmCurtimDstMovID: TLargeintField;
    QrVSEmCurtimDstNivel1: TLargeintField;
    QrVSEmCurtimAnoMesDiaIn: TIntegerField;
    QrVSEmCurtimArtGeComodty: TIntegerField;
    QrVSEmCurtimPecas: TFloatField;
    QrVSEmCurtimPesoKg: TFloatField;
    QrVSEmCurtimMarca: TWideStringField;
    QrVSGerCalConFuloes: TWideMemoField;
    QrVSEmCurtimFuloes: TWideMemoField;
    QrVSGerDeEmCur: TMySQLQuery;
    QrVSGerDeEmCurGGXInn: TLargeintField;
    QrVSGerDeEmCurGraGruX: TLargeintField;
    QrVSGerDeEmCurDstMovID: TLargeintField;
    QrVSGerDeEmCurDstNivel1: TLargeintField;
    QrVSGerDeEmCurAnoMesDiaIn: TIntegerField;
    QrVSGerDeEmCurPecas: TFloatField;
    QrVSGerDeEmCurPesoKg: TFloatField;
    QrVSGerDeEmCurQtdGerArM2: TFloatField;
    QrVSGerDeEmCurQtdGerArP2: TFloatField;
    QrVSGerDeEmCurRendKgM2: TFloatField;
    QrVSGerDeEmCurMediaM2: TFloatField;
    QrVSGerDeEmCurMarca: TWideStringField;
    QrVSGerDeEmCurArtGeComodty: TIntegerField;
    DsGafid: TDataSource;
    BtDescendencia: TBitBtn;
    QrFaltaMarca: TMySQLQuery;
    QrFaltaMarcaControle: TIntegerField;
    QrFaltaMarcaMarca: TWideStringField;
    QrFaltaMarcaMovimID: TIntegerField;
    QrFaltaMarcaMovimNiv: TIntegerField;
    QrFaltaMarcaGraGruX: TIntegerField;
    QrFaltaMarcaGraGruY: TIntegerField;
    PCPesquisas: TPageControl;
    TabSheet1: TTabSheet;
    DBGGafid: TDBGrid;
    DBGrid2: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DsFaltaMarca: TDataSource;
    QrVSClaDeCur: TMySQLQuery;
    QrVSClaDeCurSumPecas: TFloatField;
    QrVSClaDeCurSumAreaM2: TFloatField;
    QrVSClaDeCurDataHora: TDateTimeField;
    QrVSClaDeCurDtEntrada_TXT: TWideStringField;
    QrVSClaDeCurAnoMesDiaIn: TIntegerField;
    QrVSClaDeCurSrcNivel2: TIntegerField;
    QrVSClaDeCurGGXInn: TLargeintField;
    QrVSClaDeCurMarca: TWideStringField;
    QrVSClaDeCurArtGeComodty: TIntegerField;
    QrVSClaDeCurMovimID: TLargeintField;
    QrVSClaDeCurSumPesoKg: TFloatField;
    QrVSClaDeCurMediaM2Peca: TFloatField;
    QrVSClaDeCurInnKg: TFloatField;
    QrVSClaDeCurInnPc: TFloatField;
    QrVSClaDeCurKgCouro: TFloatField;
    QrVSClaDeCurRendKgm2: TFloatField;
    Panel11: TPanel;
    RGFonte: TRadioGroup;
    RGSepGrup: TRadioGroup;
    RGRelatorio: TRadioGroup;
    RadioGroup1: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_241_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CkCompraIniClick(Sender: TObject);
    procedure CkCompraFimClick(Sender: TObject);
    procedure CkViagemIniClick(Sender: TObject);
    procedure CkViagemFimClick(Sender: TObject);
    procedure CkEntradaIniClick(Sender: TObject);
    procedure CkEntradaFimClick(Sender: TObject);
    procedure TPCompraIniClick(Sender: TObject);
    procedure TPCompraFimClick(Sender: TObject);
    procedure TPViagemIniClick(Sender: TObject);
    procedure TPViagemFimClick(Sender: TObject);
    procedure TPEntradaIniClick(Sender: TObject);
    procedure TPEntradaFimClick(Sender: TObject);
    procedure TPCompraIniRedefInPlace(Sender: TObject);
    procedure TPCompraFimRedefInPlace(Sender: TObject);
    procedure TPViagemIniRedefInPlace(Sender: TObject);
    procedure TPViagemFimRedefInPlace(Sender: TObject);
    procedure TPEntradaIniRedefInPlace(Sender: TObject);
    procedure TPEntradaFimRedefInPlace(Sender: TObject);
    procedure CkTemIMEIMrtClick(Sender: TObject);
    procedure RGPesquisaClick(Sender: TObject);
    procedure SbMesAntClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtDescendenciaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure RGRelatorioClick(Sender: TObject);
  private
    { Private declarations }
    FCordaCtrlsBar, FCordaMovCntrsInCur: String;
    //FVSInnIts, FVSGerMpC, FVSGerEmC, FVSGerArt,
    FGerFromIn: String;
    FArrAGC: array of array[0..7] of Integer;
    FArrCur: array of Integer;
    FArrCla: array of Integer;
    FArrSigla: array of String;
    FArrTipo: array of array[0..1] of Integer;
    FMarcas1, FLetras: String;
    FMarcasN: TStringList;
    //
    PageHeader1: TfrxPageHeader;
    Header1, Header2: TfrxHeader;
    Footer1, Footer2: TfrxFooter;
    GroupHeader1: TfrxGroupHeader;
    GroupFooter1: TfrxGroupFooter;
    PageFooter1: TfrxPageFooter;
    //ReportSummary1: TfrxReportSummary;
    MeTitEntrada, MeTitCaleiro, MeSubTitEntradaData, MeSubTitEntradaPecas,
    MeSubTitEntradaPesoKg, MeSubTitEntradaKgPc, MeSubTitEntradaNFePc,
    MeSubTitCaleiroPecas: TfrxMemoView;
    MeValEntradaData, MeValEntradaPecas,
    MeValEntradaPesoKg, MeValEntradaKgPc, MeValEntradaNFePc,
    MeValCaleiroPecas,
    MeSumTextoTotal, MeSumEntradaPecas,
    MeSumEntradaPesoKg, MeSumEntradaKgPc, MeSumEntradaNFePc,
    MeSumCaleiroPecas: TfrxMemoView;

    MasterData0, MasterData1, MasterData2: TfrxMasterData;

    //
    procedure ReopenVSGerArtCalCon(Corda: String);
    procedure ReopenVSGerArtDeEmCur(Corda: String);
    procedure ReopenVSGerEmConserva(Corda: String);
    procedure ReopenVSGerMpC(Corda: String);
    procedure ReopenVSGerEmCaleiro(Corda: String);
    procedure ReopenVSGerCalDst(Corda: String);
    procedure ReopenVSGerSubProd(Corda: String);
    procedure ReopenVSGerEmCurtimento(Corda: String);
    procedure ReopenVSClaDeGerArtDeEmCur(Corda: String);

    procedure ReopenVSInnIts();
    function  ConversaoCm(Redimensiona, HalfH: Boolean; Centimetros: Extended): Extended;
    procedure DesfazPesquisa();
    function  Desconversao(Medidafxt: Extended): Extended;
    procedure PesquisaClassificados();
    function  DefineFiltros(): Boolean;
    //
    procedure MeHed(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String;FontStyle:
      (*Vcl.Graphics.TFont.*)TFontStyles; FontSize: Integer;
      FrameTyp:(* frxClass.TfrxFrame.*)TfrxFrameTypes; const Texto: String);
    procedure MeHed2(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String;FontStyle: TFontStyles;
      FontSize: Integer; FrameTyp: TfrxFrameTypes; const Texto: String);
    procedure MeTit(var Memo: TfrxMemoView; var Left: Extended; const Top, Width,
      Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
    procedure MeSub(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String);
    procedure MeSub2(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String);
    procedure MeVal(var Memo: TfrxMemoView; var Left: Extended; const Top,
      Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
      const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeVal2(var Memo: TfrxMemoView; var Left: Extended; const Top,
      Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
      const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeGru(var Memo: TfrxMemoView; var Left: Extended; const Top,
      Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
      const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeSum(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeTot(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeTot2(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String);
    function GetFontHeightFromFontSize(FontSize: Integer): Integer;
    function VerificaOrfaos(): Integer;
    function VerificaGerArtErro(): Integer;
    procedure GeraArvoreDeIMEIs(Parte: Integer);

  public
    { Public declarations }
  end;

  var
  FmVSGerArtMarcaImpBar: TFmVSGerArtMarcaImpBar;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, ModuleGeral, UnVS_PF,
  CreateVS, UMySQLModule, MyDBCheck, UnVS_Jan, UnGrade_Jan, UnVS_CRC_PF;

{$R *.DFM}

const
  FCmFrx = 37.7953;
  FGapBan = 1.10; // cm
  FEspecoTexto = 0.10; // cm
  FFonteMinima = 4;
  FGapCol = 0.05;

procedure TFmVSGerArtMarcaImpBar.BitBtn1Click(Sender: TObject);
begin
  VS_Jan.MostraFormVSMarcaAjusta();
end;

procedure TFmVSGerArtMarcaImpBar.BtDescendenciaClick(Sender: TObject);
var
  CordaPais, sNivel: String;
  Nivel: Integer;
  //
  procedure InseredadosPsqDeTabelaTmp(Nivel: Integer; TabelaTmp: String);
  begin
    sNivel := Geral.FF0(Nivel);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados da tabela tempor�ria ' + sNivel);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DModG.MyPID_DB, [
    'INSERT INTO _vs_marcas_desc_psq_ ',
    'SELECT Controle, ' + sNivel + ' Nivel, Marca, 1 Ativo  ',
    'FROM ' + TabelaTmp,
    '; ',
    '']);
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
    UnCreateVS.RecriaTempTableNovo(ntrttVSMarcasDesc, DModG.QrUpdPID1, False, 1, '_vs_marcas_desc_all_');
    UnCreateVS.RecriaTempTableNovo(ntrttVSMarcasDesc, DModG.QrUpdPID1, False, 1, '_vs_marcas_desc_psq_');
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Nivel 0');
    CordaPais := FCordaCtrlsBar;
    Nivel := 0;
    sNivel := Geral.FF0(Nivel);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'INSERT INTO _vs_marcas_desc_all_ ',
    'SELECT Controle, ' + sNivel + ' Nivel, Marca, 1 Ativo  ',
    'FROM ' + TMeuDB + '.vsmovits ',
    'WHERE Controle IN ( ' + CordaPais + ') ',
    //'WHERE SrcNivel2 IN ( ' + CordaPais + ') ',
    '; ',
    // Evitar erro de abertura?
    'SELECT * FROM _vs_marcas_desc_all_ ',
    'WHERE Nivel=' + sNivel,
    '; ',
    '']);
    //CordaPais := MyObjects.CordaDeQuery(Query, 'Controle', '');
    Nivel := Nivel + 1;
    //
    while CordaPais <> EmptyStr do
    begin
      sNivel := Geral.FF0(Nivel);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Nivel ' +
      sNivel + '. Itens do nivel anterior: ' + Geral.FF0(Query.RecordCount));
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
      'INSERT INTO _vs_marcas_desc_all_ ',
      'SELECT Controle, ' + sNivel + ' Nivel, Marca, 1 Ativo  ',
      'FROM ' + TMeuDB + '.vsmovits ',
      //'WHERE Controle IN ( ' + CordaPais + ') ',
      'WHERE SrcNivel2 IN ( ' + CordaPais + ') ',
      'OR GSPSrcNiv2 IN ( ' + CordaPais + ') ',
      '; ',
      'SELECT * FROM _vs_marcas_desc_all_ ',
      'WHERE Nivel=' + sNivel,
      '; ',
      '']);
      //Geral.MB_Teste(Query.SQL.Text);
      CordaPais := MyObjects.CordaDeQuery(Query, 'Controle', '');
      //
      Nivel := Nivel + 1;
      //Sleep(500);
    end;
    //
    InseredadosPsqDeTabelaTmp(1, '_vs_entrada_in_natura_cab_e_its');  // '_vs_entrada_in_natura_cab_e_its'
    InseredadosPsqDeTabelaTmp(2, '_vs_em_conserva_cab_e_its'); // Em conserva��o
    InseredadosPsqDeTabelaTmp(3, '_vs_mp_conservado_cab_e_its'); // Conservados
    InseredadosPsqDeTabelaTmp(4, '_vs_em_caleiro_cab_e_its'); // Em caleiro
    InseredadosPsqDeTabelaTmp(5, '_vs_dest_proc_cal_cab_e_its'); // Destino de processo de caleiro
    InseredadosPsqDeTabelaTmp(6, '_vs_subprod_cal_cab_e_its'); // Subprodutos de caleiro
    InseredadosPsqDeTabelaTmp(7, '_vs_gerartbar_emproccalcon_cab_e_it2'); // '_vs_gerartbar_emproccalcon_cab_e_it2'
    InseredadosPsqDeTabelaTmp(8, '_vs_em_curtimento_cab_e_it2'); // Em curtimento
    InseredadosPsqDeTabelaTmp(9,'_vs_gerat_de_emcurt_cab_e_its'); // Artigo gerado a partir de Em curtimento
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Comparando dados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrFaltaMarca, DModG.MyPID_DB, [
    'SELECT a.Controle, p.Marca, vmi.MovimID, vmi.MovimNiv,  ',
    'vmi.GraGruX, ggx.GraGruY ',
    'FROM _vs_marcas_desc_all_ a ',
    'LEFT JOIN _vs_marcas_desc_psq_ p ON p.Controle=a.Controle ',
    'LEFT JOIN ' + TMeuDB + '.vsmovits vmi  ',
    '  ON vmi.Controle=a.Controle ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ',
    '  ON ggx.Controle=vmi.GraGruX ',
    'WHERE ( ',
    '  a.Marca <> p.Marca ',
    '  OR p.Controle IS NULL ',
    '  OR p.Marca="" ',
    ') ',
    'AND (NOT vmi.MovimNiv IN (32)) ',  // eminEmCalBxa
    //'/* Subproduto em processo de caleiro */ ',
    'AND (NOT (vmi.MovimNiv=29 AND (ggx.GraGruY=512))) ',
    'ORDER BY a.Controle ',
    '']);
    //
    if QrFaltaMarca.RecordCount > 0 then
      PCPesquisas.ActivePageIndex := 1;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSGerArtMarcaImpBar.BtImprimeClick(Sender: TObject);
const
(*
  ArrMasterData0:  array[0..8] of Double = ( 3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70);
  ArrHeader1:      array[0..8] of Double = ( 4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70);
  ArrGroupHeader1: array[0..8] of Double = ( 5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40);
  ArrMasterData1:  array[0..8] of Double = ( 7.10,  7.10,  7.20,  7.30,  7.40,  7.50,  7.60,  7.60,  7.70);
  ArrGroupFooter1: array[0..8] of Double = ( 8.00,  8.00,  8.20,  8.30,  8.40,  8.60,  8.70,  8.70,  8.80);
  ArrFooter1:      array[0..8] of Double = ( 9.50,  9.50,  9.50,  9.70,  9.90, 10.20, 10.40, 10.40, 10.60);
  ArrHeader2:      array[0..8] of Double = (10.70, 10.70, 11.20, 11.50, 11.80, 12.30, 12.60, 12.80, 13.10);
  ArrMasterData2:  array[0..8] of Double = (12.20, 12.20, 12.70, 13.00, 13.40, 14.00, 14.40, 14.70, 15.10);
  ArrFooter2:      array[0..8] of Double = (13.20, 13.20, 13.70, 14.00, 14.40, 15.00, 15.40, 15.70, 16.10);
  ArrPageFooter1:  array[0..8] of Double = (15.40, 15.40, 16.00, 16.40, 16.90, 17.60, 18.10, 18.40, 18.90);
  //
*)
  ArrMasterData0:  array[0..8] of Double = ( 3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70,  3.70);
  ArrHeader1:      array[0..8] of Double = ( 4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70,  4.70);
  ArrGroupHeader1: array[0..8] of Double = ( 5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40,  5.40);
  ArrMasterData1:  array[0..8] of Double = ( 6.90,  7.10,  7.20,  7.30,  7.40,  7.60,  7.70,  7.90,  8.00);
  ArrGroupFooter1: array[0..8] of Double = ( 7.80,  7.90,  8.20,  8.30,  8.40,  8.70,  8.80,  9.10,  9.20);
  ArrFooter1:      array[0..8] of Double = ( 8.90,  9.10,  9.50,  9.70,  9.90, 10.30, 10.50, 10.90, 11.10);
  ArrHeader2:      array[0..8] of Double = (10.20, 10.60, 11.10, 11.50, 11.90, 12.40, 12.80, 13.30, 13.70);
  ArrMasterData2:  array[0..8] of Double = (11.70, 12.10, 12.60, 13.00, 13.50, 14.10, 16.60, 15.30, 15.80);
  ArrFooter2:      array[0..8] of Double = (12.70, 13.10, 13.60, 14.00, 14.50, 15.10, 15.60, 16.30, 16.80);
  ArrPageFooter1:  array[0..8] of Double = (14.80, 15.30, 15.90, 16.40, 17.00, 17.70, 18.30, 19.10, 19.70);
  //
  function FmtItem(Campo: String; Casas, Item: Integer): String;
  var
    sItem, sCasas: String;
  begin
    if Item > 0 then
      sItem := Geral.FF0(Item)
    else
      sItem := '';
    case casas of
      1: sCasas := '.0';
      2: sCasas := '.00';
      3: sCasas := '.000';
      4: sCasas := '.0000';
      else sCasas := '';
    end;
    Result := '[FormatFloat(''#,###,###,##0' + sCasas + ';-#,###,###,##0' + sCasas + '; '',<frxDsGafid."' + Campo + sItem + '">)]';
  end;
var
  DataPage: TfrxDataPage;
  Page: TfrxReportPage;
  Shape1, Shape2: TfrxShapeView;
  MeDono, MeTitulo, MeData, MePagina: TfrxMemoView;
  Line1: TfrxLineView;
  MeValLetras, MeTitPeriEntrada, MeValPeriEntrada,
  MeGroupHeader: TfrxMemoView;
  L, W, LTit, TLin, HBan, T, Linha, WTit: Extended;
  Memo: TfrxMemoView;
  I, Item: Integer;
  x, s, OrderBy, SepMarca, SQL_GrupoMarca, sMeGroupHeader1,
  sGrouHeaderCondition: String;
  TemNaoProcessados: Boolean;
  RGFonte_ItemIndex: Integer;
begin
  if MyObjects.FIC(RGRelatorio.ItemIndex < 1, RGRelatorio,
  'Selecione o relat�rio!') then Exit;
  case RGRelatorio.ItemIndex of
    1:
    begin
      OrderBy := 'ORDER BY GGXInn, Data, Marca';
      sMeGroupHeader1 := '[frxDsGafid."NO_GGXInn"]';
      sGrouHeaderCondition := 'frxDsGafid."GGXInn"';
    end;
    2:
    begin
      OrderBy := 'ORDER BY GrupoMarca, GGXInn, Data, Marca';
      sMeGroupHeader1 := '[frxDsGafid."GrupoMarca"]';
      sGrouHeaderCondition := 'frxDsGafid."GrupoMarca"';
    end;
    3:
    begin
      OrderBy := 'ORDER BY AnoMesDiaIn, GGXInn, Marca';
      sMeGroupHeader1 := '[frxDsGafid."Data"]';
      sGrouHeaderCondition := 'frxDsGafid."AnoMesDiaIn"';
    end;
    else
    begin
      Geral.MB_Erro('Relat�rio n�o implementado!');
      Exit;
    end;
  end;
  if RGSepGrup.Enabled = True then
  begin
    case RGSepGrup.ItemIndex of
      0: SepMarca := ' ';
      1: SepMarca := '-';
      2: SepMarca := '/';
      3: SepMarca := '\';
      else
      begin
        Geral.MB_Erro('Separador para agrupar marcas n�o implementado!');
        Exit;
      end;
    end;
  end;
  SQL_GrupoMarca := 'SUBSTRING_INDEX(Marca, "' + SepMarca + '", 1) GrupoMarca, ';
  //
  RGFonte_ItemIndex := RGFonte.ItemIndex;
  QrGafid.DisableControls;
  try
    frxDsGafid.FieldAliases.Clear;
    frxDsClasses.FieldAliases.Clear;
    //frxDsGafid.FieldAliases.Clear;
    UnDmkDAC_PF.AbreMySQLQuery0(QrGafid, DModG.MyPID_DB, [
    'SELECT gg1.Nome  NO_GGXInn, ',
    SQL_GrupoMarca,
    'afi.*  ',
    'FROM ' + FGerFromIn + ' afi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=afi.GGXInn ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    //'ORDER BY GGXInn, Data, Marca'
    OrderBy,
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNaoProc, DModG.MyPID_DB, [
    'SELECT SUM(InnSdoVrtPeca) InnSdoVrtPeca ',
    'FROM ' + FGerFromIn + ' _dd ',
    '']);
    TemNaoProcessados := QrNaoProcInnSdoVrtPeca.Value >= 0.1;
    { clear a report }
    frxWET_CURTI_242_1_A.Clear;
    frxWET_CURTI_242_1_A.OnGetValue := frxWET_CURTI_241_01GetValue;
    { add a dataset to the list of ones accessible for a report }
    frxWET_CURTI_242_1_A.DataSets.Add(DModG.frxDsDono);
    frxWET_CURTI_242_1_A.DataSets.Add(frxDsGafid);
    frxWET_CURTI_242_1_A.DataSets.Add(frxDsClasses);
    { add the "Data" page }
    DataPage := TfrxDataPage.Create(frxWET_CURTI_242_1_A);
    { add a page }
    Page := TfrxReportPage.Create(frxWET_CURTI_242_1_A);
    { create a unique name }
    Page.CreateUniqueName;
    { set sizes of fields, paper and orientation by default }
    Page.SetDefaults;
    { modify paper�s orientation }
    Page.Orientation := System.UITypes.TPrinterOrientation.poLandscape;

    // Margens da p�gina em mm
    Page.LeftMargin := 10;
    Page.RightMargin := 10;
    Page.TopMargin := 10;
    Page.BottomMargin := 10;

    // Cabe�alho da(s) p�gina(s)
    PageHeader1 := TfrxPageHeader.Create(Page);
    PageHeader1.CreateUniqueName;
    //PageHeader1.Name := 'PageHeader1';
    PageHeader1.FillType := ftBrush;
    PageHeader1.FillGap.Top := 0;
    PageHeader1.FillGap.Left := 0;
    PageHeader1.FillGap.Bottom := 0;
    PageHeader1.FillGap.Right := 0;
    PageHeader1.Frame.Typ := [];
    Linha := ConversaoCm(True, True, 0.40);
                          // duas linhas de t�tulo
    //PageHeader1.Height := ConversaoCm(False, False, 1.60) + (2 * Linha); // 90.708710240000000000;
    PageHeader1.Height := ConversaoCm(False, False, 1.60); // 90.708710240000000000;
    PageHeader1.Top := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
    HBan := Desconversao(PageHeader1.Height + PageHeader1.Top + FGapBan);
    { this object will be stretched according to band�s width }
    //PageHeader1.Align := baWidth;
    PageHeader1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;

  ////////////////////////////////////////////////////////////////////////////////
  /////////  D A D O S   G E N � R I C O S  C A B E � A L H O  ///////////////////
  ////////////////////////////////////////////////////////////////////////////////

    Shape1 := TfrxShapeView.Create(PageHeader1);
    Shape1.AllowVectorExport := True;
    Shape1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
    Shape1.Height := ConversaoCm(False, False, 1.0); // 37.795300000000000000;
    Shape1.Frame.Typ := [];
    Shape1.Frame.Width := 0.100000000000000000;
    Shape1.Shape := skRoundRectangle;

    Line1 := TfrxLineView.Create(PageHeader1);
    Line1.AllowVectorExport := True;
    Line1.Top := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
    Line1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
    Line1.Color := clBlack;
    Line1.Frame.Typ := [ftTop];
    Line1.Frame.Width := 0.100000000000000000;

    MeHed(MeDono,       0.20,  0.00, 27.30, 0.50, haCenter, fkText, '', [fsBold], 08, [], '[frxDsDono."NOMEDONO"]');
    MeHed(MeTitulo,     4.00,  0.50, 19.70, 0.50, haCenter, fkText, '', [fsBold], 09, [], 'Controle de Produ��o de Artigo Gerado');
    MeHed(MeData,       0.20,  0.50,  3.80, 0.50, haLeft, fkDateTime, 'dd/mm/yyyy hh:nn:ss', [], 08, [], '[VARF_DATA]');
    MeHed(MePagina,    23.70,  0.50,  3.80, 0.50, haRight, fkText, '', [], 08, [], 'P'#225'gina [Page#] de [TotalPages#]');

  ////////////////////////////////////////////////////////////////////////////////
  /////////  F I L T R O S   D E  P E S Q U I S A   ( D A T A  S )  //////////////
  ////////////////////////////////////////////////////////////////////////////////

    Shape2 := TfrxShapeView.Create(PageHeader1);
    Shape2.AllowVectorExport := True;
    Shape2.Top := ConversaoCm(False, False, 1.1); // 41.574830000000000000;
    Shape2.Width := ConversaoCm(False, False, 27.7); // 1046.9291509400000000;
    Shape2.Height := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
    Shape2.Frame.Typ := [];
    Shape2.Frame.Width := 0.100000000000000000;

    MeHed(MeTitPeriEntrada,     0.00,  1.10,  2.50, 0.50, haLeft, fkText, '', [], 08, [], 'Per'#237'odo compra:');
    MeHed(MeValPeriEntrada,     2.50,  1.10,  6.40, 0.50, haLeft, fkText, '', [fsBold], 08, [], '[VARF_PeriodoEntrada]');
    //
    //MeHed(MeValPeriEntrada,     9.00,  1.10,  17.80, 0.50, haRight, fkText, '', [], 08, [], 'Letras: ' + FLetras);
    //MeHed(MeTitPeriViagem,     9.00,  1.10,  17.80, 0.50, haRight, fkText, '', [], 08, [], 'Letras: ' + FLetras);
    MeHed(MeValLetras,     9.00,  1.10,  17.80, 0.50, haRight, fkText, '', [], 08, [], 'Letras: ' + FLetras);

  ////////////////////////////////////////////////////////////////////////////////
  ///////  D A T A B A N D   D A  L I S T A   D E   M A R C A S  /////////////////
  ////////////////////////////////////////////////////////////////////////////////

    MasterData0 := TfrxMasterData.Create(Page);
    MasterData0.Name := 'MasterData0';
    MasterData0.FillType := ftBrush;
    MasterData0.FillGap.Top := 0;
    MasterData0.FillGap.Left := 0;
    MasterData0.FillGap.Bottom := 0;
    MasterData0.FillGap.Right := 0;
    MasterData0.Frame.Typ := [];
    MasterData0.Height := ConversaoCm(False, True, 0.4); // 15.118120000000000000;
    //MasterData0.Top := ConversaoCm(False, False, HBan); // 170.078850000000000000;
    MasterData0.Top := ConversaoCm(False, False, ArrMasterData0[RGFonte_ItemIndex]); // teste 2023/12/07
    MasterData0.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
    //MasterData0.DataSet := frxDsGafid;
    //MasterData0.DataSetName := '';
    MasterData0.RowCount := 1;
    MasterData0.Stretched := True;
    HBan := Desconversao(MasterData0.Top + MasterData0.Height + FGapBan);

    // Lista de marcas
    Memo := TfrxMemoView.Create(MasterData0);
    Memo.AllowVectorExport := True;
    Memo.Left := ConversaoCm(False, False, 0);
    Memo.Top := ConversaoCm(False, False, 0); // 60.472480000000000000;
    Memo.Width := ConversaoCm(False, False, 27.7); // 136.062982360000000000;
    Memo.Height := ConversaoCm(False, True, 0.4); // 15.118110240000000000;
    Memo.StretchMode := smMaxHeight;
    //Memo.DisplayFormat.FormatStr := FormatStr;
    Memo.DisplayFormat.Kind := fkText;
    Memo.Font.Charset := DEFAULT_CHARSET;
    Memo.Font.Color := clBlack;
    Memo.Font.Size := 10; //RGFonte.ItemIndex + FFonteMinima;
    //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
    Memo.Font.Name := 'Univers Light Condensed';
    Memo.Font.Style := [];
    Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
    Memo.Frame.Width := 0.500000000000000000;
    Memo.HAlign := haCenter;
    Memo.Memo.Text := 'Marcas: ' + FMarcas1 + '' + FMarcasN.Text;
    Memo.ParentFont := False;
    Memo.WordWrap := True;
    Memo.VAlign := vaCenter;

  ////////////////////////////////////////////////////////////////////////////////
  ///////  D A T A B A N D   D E   A R T I G O S   G E R A D O S  ////////////////
  ////////////////////////////////////////////////////////////////////////////////

    Header1 := TfrxHeader.Create(Page);
    Header1.Name := 'Header1';
    Header1.FillType := ftBrush;
    Header1.FillGap.Top := 0;
    Header1.FillGap.Left := 0;
    Header1.FillGap.Bottom := 0;
    Header1.FillGap.Right := 0;
    Header1.Frame.Typ := [];
    Header1.Height := ConversaoCm(True, True, 0.1);
    //Header1.Top := ConversaoCm(True, True, HBan); // 170.078850000000000000
    Header1.Top := ConversaoCm(False, False, ArrHeader1[RGFonte_ItemIndex]); // teste 2023/12/07
    Header1.Width := ConversaoCm(True, True, 27.7); // 1046.929810000000000000
    HBan := Desconversao(Header1.Top + Header1.Height + FGapBan);

    TLin := ConversaoCm(True, True, 0.5);
    GroupHeader1 := TfrxGroupHeader.Create(Page);
    GroupHeader1.Name := 'GroupHeader1';
    GroupHeader1.FillType := ftBrush;
    GroupHeader1.FillGap.Top := 0;
    GroupHeader1.FillGap.Left := 0;
    GroupHeader1.FillGap.Bottom := 0;
    GroupHeader1.FillGap.Right := 0;
    GroupHeader1.Frame.Typ := [];
    GroupHeader1.Height := TLin + (2 * Linha); // 22.677180000000000000
    //GroupHeader1.Top := ConversaoCm(True, True, HBan); // 170.078850000000000000
    GroupHeader1.Top := ConversaoCm(False, False, ArrGroupHeader1[RGFonte_ItemIndex]); // teste 2023/12/07
    GroupHeader1.Width := ConversaoCm(True, True, 27.7); // 1046.929810000000000000
    //GroupHeader1.Condition := 'frxDsGafid."GGXInn"';
    GroupHeader1.Condition := sGrouHeaderCondition;
    HBan := Desconversao(GroupHeader1.Top + GroupHeader1.Height + FGapBan);

    L := 0.00;
    //MeGru(MeGroupHeader,       L, 0.00, 27.7, 0.50, haLeft, '[frxDsGafid."NO_GGXInn"]', fkText, '');
    MeGru(MeGroupHeader,       L, 0.00, 27.7, 0.50, haLeft, sMeGroupHeader1, fkText, '');

    MasterData1 := TfrxMasterData.Create(Page);
    MasterData1.Name := 'MasterData1';
    MasterData1.FillType := ftBrush;
    MasterData1.FillGap.Top := 0;
    MasterData1.FillGap.Left := 0;
    MasterData1.FillGap.Bottom := 0;
    MasterData1.FillGap.Right := 0;
    MasterData1.Frame.Typ := [];
    MasterData1.Height := ConversaoCm(True, True, 0.4); // 15.118120000000000000;
    //MasterData1.Top := ConversaoCm(False, False, HBan); // 170.078850000000000000;
    MasterData1.Top := ConversaoCm(False, False, ArrMasterData1[RGFonte_ItemIndex]); // teste 2023/12/07
    MasterData1.Width := ConversaoCm(True, False, 27.7); // 1046.929810000000000000;
    MasterData1.DataSet := frxDsGafid;
    MasterData1.DataSetName := 'frxDsGafid';
    MasterData1.RowCount := 0;
    HBan := Desconversao(MasterData1.Top + MasterData1.Height + FGapBan);


    GroupFooter1 := TfrxGroupFooter.Create(Page);
    GroupFooter1.Name := 'GroupFooter1';
    GroupFooter1.FillType := ftBrush;
    GroupFooter1.FillGap.Top := 0;
    GroupFooter1.FillGap.Left := 0;
    GroupFooter1.FillGap.Bottom := 0;
    GroupFooter1.FillGap.Right := 0;
    GroupFooter1.Frame.Typ := [];
    GroupFooter1.Height := ConversaoCm(True, True, 0.80); // := 253.228510000000000000
    //GroupFooter1.Top := ConversaoCm(False, False, HBan); // 170.078850000000000000;
    GroupFooter1.Top := ConversaoCm(False, False, ArrGroupFooter1[RGFonte_ItemIndex]); // teste 2023/12/07
    GroupFooter1.Width := ConversaoCm(True, True, 27.7); // := 1046.929810000000000000
    HBan := Desconversao(GroupFooter1.Top + GroupFooter1.Height + FGapBan);

    Footer1 := TfrxFooter.Create(Page);
    Footer1.Name := 'Footer1';
    Footer1.FillType := ftBrush;
    Footer1.FillGap.Top := 0;
    Footer1.FillGap.Left := 0;
    Footer1.FillGap.Bottom := 0;
    Footer1.FillGap.Right := 0;
    Footer1.Frame.Typ := [];
    Footer1.Height := ConversaoCm(True, True, 1.20); // := 30.236220470000000000
    //Footer1.Top := ConversaoCm(True, True, HBan); // := 253.228510000000000000
    Footer1.Top := ConversaoCm(False, False, ArrFooter1[RGFonte_ItemIndex]); // teste 2023/12/07
    Footer1.Width := ConversaoCm(True, True, 27.7); // := 1046.929810000000000000
    HBan := Desconversao(Footer1.Top + Footer1.Height + FGapBan);


  ////////////////////////////////////////////////////////////////////////////////
  /////////  D A T A B A N D   D E   C L A S S I F I C A D O S  //////////////////
  ////////////////////////////////////////////////////////////////////////////////

    // Por que?????
    HBan := HBan + FGapBan;
    //
    Header2 := TfrxHeader.Create(Page);
    Header2.Name := 'Header2';
    Header2.FillType := ftBrush;
    Header2.FillGap.Top := 0;
    Header2.FillGap.Left := 0;
    Header2.FillGap.Bottom := 0;
    Header2.FillGap.Right := 0;
    Header2.Frame.Typ := [];
    Header2.Height := ConversaoCm(True, True, 0.9);
    //Header2.Top := ConversaoCm(True, True, HBan); // 170.078850000000000000
    Header2.Top := ConversaoCm(False, False, ArrHeader2[RGFonte_ItemIndex]); // teste 2023/12/07
    Header2.Width := ConversaoCm(True, True, 27.7); // 1046.929810000000000000
    HBan := Desconversao(Header2.Top + Header2.Height + FGapBan);

    L := 0.00;
    MeHed2(Memo,             0.20, 0.00, 27.30, 0.50, haLeft, fkText, '', [fsBold], 08, [], 'Classes Geradas');

    MasterData2 := TfrxMasterData.Create(Page);
    MasterData2.Name := 'MasterData2';
    MasterData2.FillType := ftBrush;
    MasterData2.FillGap.Top := 0;
    MasterData2.FillGap.Left := 0;
    MasterData2.FillGap.Bottom := 0;
    MasterData2.FillGap.Right := 0;
    MasterData2.Frame.Typ := [];
    MasterData2.Height := ConversaoCm(False, True, 0.4); // 15.118120000000000000;
    //MasterData2.Top := ConversaoCm(False, False, HBan); // 170.078850000000000000;
    MasterData2.Top := ConversaoCm(False, False, ArrMasterData2[RGFonte_ItemIndex]); // teste 2023/12/07
    MasterData2.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
    MasterData2.DataSet := frxDsGafid;
    MasterData2.DataSetName := 'frxDsClasses';
    MasterData2.RowCount := 0;
    HBan := Desconversao(MasterData2.Top + MasterData2.Height + FGapBan);

    Footer2 := TfrxFooter.Create(Page);
    Footer2.Name := 'Footer2';
    Footer2.FillType := ftBrush;
    Footer2.FillGap.Top := 0;
    Footer2.FillGap.Left := 0;
    Footer2.FillGap.Bottom := 0;
    Footer2.FillGap.Right := 0;
    Footer2.Frame.Typ := [];
    Footer2.Height := ConversaoCm(True, True, 0.80);
    //Footer2.Top := ConversaoCm(True, True, HBan); // := 253.228510000000000000
    Footer2.Top := ConversaoCm(False, False, ArrFooter2[RGFonte_ItemIndex]); // teste 2023/12/07
    Footer2.Width := ConversaoCm(True, True, 27.7); // := 1046.929810000000000000

    HBan := Desconversao(Footer2.Top + Footer2.Height + FGapBan);


  (*
  ////////////////////////////////////////////////////////////////////////////////
  /////////  R E P O R T   S U M M A R Y   D E   T O T A I S  ////////////////////
  ////////////////////////////////////////////////////////////////////////////////

    ReportSummary1 := TfrxReportSummary.Create(Page);
    ReportSummary1.FillType := ftBrush;
    ReportSummary1.FillGap.Top := 0;
    ReportSummary1.FillGap.Left := 0;
    ReportSummary1.FillGap.Bottom := 0;
    ReportSummary1.FillGap.Right := 0;
    ReportSummary1.Frame.Typ := [];
    ReportSummary1.Height := ConversaoCm(True, True, 1.00); //30.236220470000000000
    ReportSummary1.Top := ConversaoCm(True, True, HBan); //343.937230000000000000
    ReportSummary1.Width := ConversaoCm(True, True, 0.8); //1046.929810000000000000

    HBan := Desconversao(ReportSummary1.Top + ReportSummary1.Height + FGapBan);

  *)

  ////////////////////////////////////////////////////////////////////////////////
  /////////  T I T U L O S  E  D A D O S  D E   I N   N A T U R A  ///////////////
  ////////////////////////////////////////////////////////////////////////////////
    LTit := 0.00;
    TLin := DesConversao(TLin);
    //MeTit(MeTitEntrada,           LTit, 0.50, 3.65, 0.40, haCenter, 'Entrada');
    MeTit(MeTitEntrada,           LTit, TLin, 5.15, 0.40, haCenter, 'Entrada');
    LTit := LTit + FGapCol;
    MeTit(MeTitCaleiro,           LTit, TLin, 0.70, 0.40, haCenter, 'Caleiro');
    LTit := LTit + FGapCol;
    if TemNaoProcessados then
    begin
      MeTit(Memo,                   LTit, TLin, 1.50, 0.40, haCenter, 'N�o Processados');
    end;
    LTit := LTit + FGapCol;
    L := 0.00;
    T := Desconversao(MeTitEntrada.Top + MeTitEntrada.Height);

    Item := 0;

    MeSub(MeSubTitEntradaData,    L,   T , 0.85, 0.40, haCenter, 'Data');
    MeSum(MeSumTextoTotal,        L, 0.00, 2.35, 0.40, haLeft, 'Subtotal: ', fkText, '');
    MeTot(MeSumTextoTotal,        L, 0.20, 2.35, 0.40, haLeft, 'TOTAL GERAL: ', fkText, '');
    Meval(MeValEntradaData,       L, 0.00, 0.85, 0.40, haCenter, '[frxDsGafid."Data"]', fkDateTime, 'dd/mm/yy');

    MeSub(MeSubTitEntradaData,    L,   T , 1.50, 0.40, haCenter, 'Marca');
    //MeSum(MeSumTextoTotal,        L, 0.00, 2.35, 0.40, haLeft, 'Subtotal: ', fkText, '');
    //MeTot(MeSumTextoTotal,        L, 0.20, 2.35, 0.40, haLeft, 'TOTAL GERAL: ', fkText, '');
    Meval(MeValEntradaData,       L, 0.00, 1.50, 0.40, haCenter, '[frxDsGafid."Marca"]', fkText, '');

    MeSub(MeSubTitEntradaPecas,   L,   T , 0.70, 0.40, haRight, 'Pe�as');
    MeSum(MeSumEntradaPecas,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnPecas">,MasterData1)]', fkNumeric, '%2.1n');
    MeTot(MeSumEntradaPecas,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnPecas">,MasterData1)]', fkNumeric, '%2.1n');
    Meval(MeValEntradaPecas,      L, 0.00, 0.70, 0.40, haRight, FmtItem('InnPecas', 0, Item), fkText, '');

    MeSub(MeSubTitEntradaPesoKg,  L,   T , 0.80, 0.40, haRight, 'Peso kg');
    MeSum(MeSumEntradaPesoKg,     L, 0.00, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnPesoKg">,MasterData1)]', fkNumeric, '%2.0n');
    MeTot(MeSumEntradaPesoKg,     L, 0.20, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnPesoKg">,MasterData1)]', fkNumeric, '%2.0n');
    Meval(MeValEntradaPesoKg,     L, 0.00, 0.80, 0.40, haRight, FmtItem('InnPesoKg', 0, Item), fkText, '');

    MeSub(MeSubTitEntradaKgPc,    L,   T , 0.60, 0.40, haRight, 'kg/p�');
    MeSum(MeSumEntradaKgPc,       L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."InnPesoKg">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1))]', fkNumeric, '%2.2n');
    MeTot(MeSumEntradaKgPc,       L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."InnPesoKg">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1))]', fkNumeric, '%2.2n');
    Meval(MeValEntradaKgPc,       L, 0.00, 0.60, 0.40, haRight, FmtItem('InnKgPeca', 2, Item), fkText, '');

    MeSub(MeSubTitEntradaNFePc,   L,   T , 0.70, 0.40, haRight, 'NFe p�');
    MeSum(MeSumEntradaNFePc,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."NFePecas">,MasterData1)]', fkNumeric, '%2.0n');
    MeTot(MeSumEntradaNFePc,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."NFePecas">,MasterData1)]', fkNumeric, '%2.0n');
    Meval(MeValEntradaNFePc,      L, 0.00, 0.70, 0.40, haRight, FmtItem('NFePecas', 0, Item), fkText, '');

    L := L + FGapCol;

    MeSub(MeSubTitCaleiroPecas,   L,   T , 0.70, 0.40, haRight, 'Pe�as');
    MeSum(MeSumCaleiroPecas,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CalPecas">,MasterData1)]', fkNumeric, '%2.1n');
    MeTot(MeSumCaleiroPecas,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CalPecas">,MasterData1)]', fkNumeric, '%2.1n');
    Meval(MeValCaleiroPecas,      L, 0.00, 0.70, 0.40, haRight, FmtItem('CalPecas', 0, Item), fkText, '');

    L := L + FGapCol;

    if TemNaoProcessados then
    begin
      MeSub(Memo, L,   T , 0.70, 0.40, haRight, 'Pe�as');
      MeSum(Memo, L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeca">,MasterData1)]', fkNumeric, '%2.1n');
      MeTot(Memo, L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeca">,MasterData1)]', fkNumeric, '%2.1n');
      Meval(Memo, L, 0.00, 0.70, 0.40, haRight, FmtItem('InnSdoVrtPeca', 0, Item), fkText, '');

      MeSub(Memo, L,   T , 0.80, 0.40, haRight, 'Peso kg');
      MeSum(Memo, L, 0.00, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeso">,MasterData1)]', fkNumeric, '%2.0n');
      MeTot(Memo, L, 0.20, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeso">,MasterData1)]', fkNumeric, '%2.0n');
      Meval(Memo, L, 0.00, 0.80, 0.40, haRight, FmtItem('InnSdoVrtPeso', 0, Item), fkText, '');
    end;

    for I := 0 to Length(FArrAGC) - 1 do
    begin

      //FArrAGC[I][1] := QrArtGeComodtyVisuRel.Value;
      if FArrAGC[I][1] > 0 then
      begin
        L := L + FGapCol;
        x := Geral.FF0(I + 1);
        Item := I + 1;
        WTit := 0.00;
        if FArrAGC[I][2] > 0 then WTit := WTit + 0.80; // Pe�as
        if FArrAGC[I][3] > 0 then WTit := WTit + 0.80; // PesoKg
        if FArrAGC[I][4] > 0 then WTit := WTit + 1.00; // �rea m2
        if FArrAGC[I][5] > 0 then WTit := WTit + 0.60; // % P�
        if FArrAGC[I][6] > 0 then WTit := WTit + 0.60; // kg/m2
        if FArrAGC[I][7] > 0 then WTit := WTit + 0.60; // m2/p�

        //MeTit(Memo, LTit, 0.50, WTit, 0.40, haCenter, FArrSigla[I] );
        MeTit(Memo, LTit, TLin, WTit, 0.40, haCenter, FArrSigla[I] );
        LTit := LTit + FGapCol;

        //FArrAGC[I][2] := QrArtGeComodtyShowPecas.Value;
        if FArrAGC[I][2] > 0 then
        begin
          s := 'frxDsGafid."Pecas_' + x + '"';
          MeSub(Memo, L,   T , 0.80, 0.40, haRight, 'Pe�as');
          MeSum(Memo, L, 0.00, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.1n');
          MeTot(Memo, L, 0.20, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.1n');
          //Meval(Memo, L, 0.00, 0.80, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 0.80, 0.40, haRight, FmtItem('Pecas_', 1, Item), fkText, '');
        end;
        //FArrAGC[I][3] := QrArtGeComodtyShowPesoKg.Value;
        if FArrAGC[I][3] > 0 then
        begin
          s := 'frxDsGafid."PesoKg_' + x + '"';
          MeSub(Memo, L,   T , 0.80, 0.40, haRight, 'Peso kg');
          MeSum(Memo, L, 0.00, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
          MeTot(Memo, L, 0.20, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
          //Meval(Memo, L, 0.00, 0.80, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 0.80, 0.40, haRight, FmtItem('PesoKg_', 0, Item), fkText, '');
        end;
        //FArrAGC[I][4] := QrArtGeComodtyShowAreaM2.Value;
        if FArrAGC[I][4] > 0 then
        begin
          s := 'frxDsGafid."AreaM2_' + x + '"';
          MeSub(Memo, L,   T , 1.00, 0.40, haRight, '�rea m�');
          MeSum(Memo, L, 0.00, 1.00, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.2n');
          MeTot(Memo, L, 0.20, 1.00, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.2n');
          //Meval(Memo, L, 0.00, 0.10, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 1.00, 0.40, haRight, FmtItem('AreaM2_', 2, Item), fkText, '');
        end;
        //FArrAGC[I][5] := QrArtGeComodtyShowPercPc.Value;
        if FArrAGC[I][5] > 0 then
        begin
          s := 'frxDsGafid."PercCo_' + x + '"';
          MeSub(Memo, L,   T , 0.60, 0.40, haRight, '% P�');
          MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
          MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
          //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('PercCo_', 1, Item), fkText, '');
        end;
        //FArrAGC[I][6] := QrArtGeComodtyShowKgM2.Value;
        if FArrAGC[I][6] > 0 then
        begin
          s := 'frxDsGafid."RendKgM2_' + x + '"';
          MeSub(Memo, L,   T , 0.60, 0.40, haRight, 'kg/m�');

          MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."PesoKg_' + x + '">,MasterData1) / SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
          MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."PesoKg_' + x + '">,MasterData1) / SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');

          MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, (SUM(<frxDsGafid."InnKgPeca"> * <frxDsGafid."Pecas_' + x + '">,MasterData1))/SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
          MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, (SUM(<frxDsGafid."InnKgPeca"> * <frxDsGafid."Pecas_' + x + '">,MasterData1))/SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');

          //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('RendKgM2_', 2, Item), fkText, '');
        end;
        //FArrAGC[I][7] := QrArtGeComodtyShowMediaM2.Value;
        if FArrAGC[I][7] > 0 then
        begin
          s := 'frxDsGafid."MediaM2_' + x + '"';
          MeSub(Memo, L,   T , 0.60, 0.40, haRight, 'm�/p�');
          MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) / SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
          MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) / SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
          //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
          Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('MediaM2_', 2, Item), fkText, '');
        end;

        //
        // Ful�es de caleiro
        if (FArrTipo[I][0] = Integer(arrtipoOutrosEmids))  // Emid
        and (FArrAGC[I][0] = 26) then //  QrEmidsDstMovID.Value = Em Caleiro
        begin
          //if FArrAGC[I][7] > 0 then
          WTit := WTit + 1.00; // Ful�es

          //MeTit(Memo, LTit, 0.50, WTit, 0.40, haCenter, FArrSigla[I] );
          MeTit(Memo, LTit, TLin, 1.00, 0.40, haCenter, 'Ful�es'); //FArrSigla[I] );
          LTit := LTit + FGapCol;

          L := L + FGapCol;
          //if FArrAGC[I][2] > 0 then
          begin
            s := 'frxDsGafid."FuloesCal"';
            MeSub(Memo, L,   T , 1.00, 0.40, haCenter, 'Caleiro'); // 'Pe�as');
            MeSum(Memo, L, 0.00, 1.00, 0.40, haLeft, '', fkText, ''); //'[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
            MeTot(Memo, L, 0.20, 1.00, 0.40, haLeft, '', fkText, ''); //'[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
            Meval(Memo, L, 0.00, 1.00, 0.40, haLeft, '[' + s + ']', fkText, ''); //fkNumeric, '%2.0n');
            //Meval(Memo, L, 0.00, 1.00, 0.40, haRight, FmtItem('Pecas_', 0, Item), fkText, '');
            //Meval(Memo, L, 0.00, 1.00, 0.40, haLeft, s, fkText, '');
          end;
        end;
        //
        // Ful�es de curtimento
        if (FArrTipo[I][0] = Integer(arrtipoEmid6))
        and (FArrTipo[I][1] = 21) then //  TipoTab=21.0 = Em curtimento
        begin
          //if FArrAGC[I][7] > 0 then
          WTit := WTit + 1.50; // Ful�es

          //MeTit(Memo, LTit, 0.50, WTit, 0.40, haCenter, FArrSigla[I] );
          MeTit(Memo, LTit, TLin, 1.50, 0.40, haCenter, 'Ful�es'); //FArrSigla[I] );
          LTit := LTit + FGapCol;

          L := L + FGapCol;
          //if FArrAGC[I][2] > 0 then
          begin
            s := 'frxDsGafid."FuloesCur"';
            MeSub(Memo, L,   T , 1.50, 0.40, haCenter, 'Curtimento'); // 'Pe�as');
            MeSum(Memo, L, 0.00, 1.50, 0.40, haLeft, '', fkText, ''); //'[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
            MeTot(Memo, L, 0.20, 1.50, 0.40, haLeft, '', fkText, ''); //'[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
            Meval(Memo, L, 0.00, 1.50, 0.40, haLeft, '[' + s + ']', fkText, ''); //fkNumeric, '%2.0n');
            //Meval(Memo, L, 0.00, 1.50, 0.40, haRight, FmtItem('Pecas_', 0, Item), fkText, '');
            //Meval(Memo, L, 0.00, 1.50, 0.40, haLeft, s, fkText, '');
          end;
        end;
      end;
      //
    end;


  ////////////////////////////////////////////////////////////////////////////////
  //////  T I T U L O S  E  D A D O S  D E   C L A S S I F I C A D O S  //////////
  ////////////////////////////////////////////////////////////////////////////////

    L := 0.00;
    W := 1.20;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, 'Reduzido');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '', fkNumeric, '%2.0n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."Reduzido"]', fkNumeric, '%2.0n');
    //
    //L := L + W;
    W := 7.00;
    MeSub2(Memo,       L, 0.50, W, 0.40, haLeft, 'Artigo');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, 'Total', fkNumeric, '%2.0n');
    Meval2(Memo,       L, 0.00, W, 0.40, haLeft, '[frxDsClasses."Nome"]', fkText, '');
    //
    //L := L + W;
    W := 1.40;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, 'Pe�as');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '[SUM(<frxDsClasses."Pecas">,MasterData2)]', fkNumeric, '%2.1n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."Pecas"]', fkNumeric, '%2.1n');
    //
    //L := L + W;
    W := 1.40;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, 'Inteiros');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '[SUM(<frxDsClasses."Inteiros">,MasterData2)]', fkNumeric, '%2.1n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."Inteiros"]', fkNumeric, '%2.1n');
    //
    //L := L + W;
    W := 2.00;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, '�rea m�');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '[SUM(<frxDsClasses."AreaM2">,MasterData2)]', fkNumeric, '%2.2n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."AreaM2"]', fkNumeric, '%2.2n');
    //
    //L := L + W;
    W := 2.40;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, '�rea ft�');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '[SUM(<frxDsClasses."AreaP2">,MasterData2)]', fkNumeric, '%2.2n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."AreaP2"]', fkNumeric, '%2.2n');
    //
    //L := L + W;
    W := 1.20;
    MeSub2(Memo,       L, 0.50, W, 0.40, haRight, '% m�');
    MeTot2(Memo,       L, 0.20, W, 0.40, haRight, '[SUM(<frxDsClasses."PercM2">,MasterData2)]', fkNumeric, '%2.2n');
    Meval2(Memo,       L, 0.00, W, 0.40, haRight, '[frxDsClasses."PercM2"]', fkNumeric, '%2.2n');

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////  P A G E   F O O T E R  ///////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////

    PageFooter1 := TfrxPageFooter.Create(Page);
    PageFooter1.FillType := ftBrush;
    PageFooter1.FillGap.Top := 0;
    PageFooter1.FillGap.Left := 0;
    PageFooter1.FillGap.Bottom := 0;
    PageFooter1.FillGap.Right := 0;
    PageFooter1.Frame.Typ := [];
    PageFooter1.Height := ConversaoCm(False, False, 0.40); //  := 15.118110240000000000
    //PageFooter1.Top := ConversaoCm(True, True, HBan); //  := 343.937230000000000000
    PageFooter1.Top := ConversaoCm(False, False, ArrPageFooter1[RGFonte_ItemIndex]); // teste 2023/12/07
    PageFooter1.Width := ConversaoCm(False, False, 27.80); //   := 1046.929810000000000000

    HBan := Desconversao(PageFooter1.Top + PageFooter1.Height + FGapBan);

    Memo := TfrxMemoView.Create(PageFooter1);
    Memo.AllowVectorExport := True;
    Memo.Left := ConversaoCm(False, False, 0.00);
    Memo.Width := ConversaoCm(False, False, 19.70); //744.567410000000000000
    Memo.Height := ConversaoCm(False, False, 0.40); //15.118110240000000000
    Memo.DisplayFormat.DecimalSeparator := ',';
    Memo.Font.Charset := DEFAULT_CHARSET;
    Memo.Font.Color := clBlack;
    Memo.Font.Height := -7;
    Memo.Font.Name := 'Arial';
    Memo.Font.Style := [] ;
    Memo.Frame.Typ := [ftTop];
    Memo.Frame.Width := 0.100000000000000000;
    Memo.Memo.Text := CONST_DMK_SOFTWARE_CUSTOMOZADO;
    Memo.ParentFont := False;

    Memo := TfrxMemoView.Create(PageFooter1);
    Memo.AllowVectorExport := True;
    Memo.Left := ConversaoCm(False, False, 19.20); //725.669760000000000000;
    Memo.Width := ConversaoCm(False, False, 8.50); //321.260050000000000000;
    Memo.Height := ConversaoCm(False, False, 0.35); //:= 13.228346460000000000;
    Memo.Font.Charset := DEFAULT_CHARSET;
    Memo.Font.Color := clBlack;
    Memo.Font.Height := -8;
    Memo.Font.Name := 'Univers Light Condensed';
    Memo.Font.Style := [];
    Memo.Frame.Typ := [ftTop];
    Memo.Frame.Width := 0.100000000000000000;
    Memo.HAlign := haRight;
    Memo.Memo.Text := '[VARF_CODI_FRX]';
    Memo.ParentFont := False;
    Memo.VAlign := vaCenter;
    //
    MyObjects.frxMostra(frxWET_CURTI_242_1_A, 'Controle de Produ��o de Artigo Gerado');
  finally
    QrGafid.EnableControls;
  end;
end;

procedure TFmVSGerArtMarcaImpBar.BtPesquisaClick(Sender: TObject);
var
  CordaCodsBar, CordaCon, CordaEmConserva, CordaEmCaleiro, CordaMovCodsInCal,
  CordaMovCntrlsInCal, CordaMovCodsInCur, CordaCtrlCladeCur,
  SQL, Virgula, sAGC, SQL_GGXInn, SQL_GroupInn, SQL_GGXDst, SQL_GroupDst, SQLpc,
  SQLkg, SQLM2: String;
  Repeticoes, AMD, I, GGXInn, Item, N, Q: Integer;
  Dia: TDateTime;
  Pecas_X, PesoKg_X, AreaM2_X, PercCo_X, RendKgM2_X, MediaM2_X(*, PercCo*): Double;
  ArtGeComodty, AnoMesDiaIn: Integer;
  ATT_MovimID, Marca: String;
  Emids: Integer;
  Pecas, PesoKg: Double;
  Teste: String;
begin
  if DefineFiltros() = False then
    Exit;
  Screen.Cursor := crHourGlass;
  try

////////////////////////////////////////////////////////////////////////////////
///// Verificar se tem cadastro de artigo sem definir o artigo commodity ///////
////////////////////////////////////////////////////////////////////////////////
  if VerificaOrfaos() > 0 then
    Exit;

////////////////////////////////////////////////////////////////////////////////
//////////////// Cria��o das tabelas das movimenta��es dos artigos /////////////
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  //Entrada In Natura    -   FVSInnIts :=
  UnCreateVS.RecriaTempTableNovo(ntrttVSInCabEIts, DModG.QrUpdPID1, False, 1, '_vs_entrada_in_natura_cab_e_its');  // '_vs_entrada_in_natura_cab_e_its'
  //Em Conserva��o    -    FVSGerEmC :=
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_em_conserva_cab_e_its'); // Em conserva��o
  //Conservados    -    FVSGerMpC :=
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_mp_conservado_cab_e_its'); // Conservados
  //Em Caleiro
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_em_caleiro_cab_e_its'); // Em caleiro
  // Destino de processo de caleiro
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_dest_proc_cal_cab_e_its'); // Destino de processo de caleiro
  // Subprodutos de caleiro
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_subprod_cal_cab_e_its'); // Subprodutos de caleiro
  //Gera��o de Artigo, Couros em Caleiro, Couros em Conserva��o    -    FVSGerArt :=
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIt2, DModG.QrUpdPID1, False, 1, '_vs_gerartbar_emproccalcon_cab_e_it2'); // '_vs_gerartbar_emproccalcon_cab_e_it2'
  //Em Curtimento
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIt2, DModG.QrUpdPID1, False, 1, '_vs_em_curtimento_cab_e_it2'); // Em curtimento
  //Artigo gerado a partir de "Em Curtimento"
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_gerat_de_emcurt_cab_e_its'); // Artigo gerado a partir de Em curtimento
  //Artigo classificado a partir de artigo gerado a partir de "Em Curtimento"
  UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_artcla_de_gerart_de_emcurt_cab_e_its'); // Artigo classificado a partir de artigo gerado a partir de Em curtimento

  //...
  // Tabela geral. Ser� criada mais abaixo!
  //FGerFromIn := UnCreateVS.RecriaTempTableNovo(ntrttVSGerArtFromInD2, DModG.QrUpdPID1, False, Repeticoes); // _vs_ger_art_from_in_d2

////////////////////////////////////////////////////////////////////////////////
//////////////// Entrada dos couros In Natura //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando in natura');
  ReopenVSInnIts();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados do in natura');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT * FROM _vs_entrada_in_natura_cab_e_its ', // + FVSInnIts,
  'ORDER BY DtEntrada, Codigo, Controle ',
  '']);
  CordaCodsBar := MyObjects.CordaDeQuery(QrExec, 'Codigo', '0');
  FCordaCtrlsBar := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');

  //Geral.MB_Teste('Corda dos c�digos In Natura ' + sLineBreak + CordaCodsBar);
  //Geral.MB_Teste('Corda dos controles In Natura ' + sLineBreak + FCordaCtrlsBar);
  //


////////////////////////////////////////////////////////////////////////////////
////////////////// Couros em conserva��o e Conservados /////////////////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros em conserva��o');
  ReopenVSGerEmConserva(CordaCodsBar);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros em conserva��o');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT * FROM _vs_em_conserva_cab_e_its ', // + FVSGerEmC,
  'ORDER BY AnoMesDiaIn, Codigo, Controle ',
  '']);
  CordaEmConserva := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //Geral.MB_Teste(CordaEmConserva);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros conservados');
  ReopenVSGerMpC(CordaEmConserva);


////////////////////////////////////////////////////////////////////////////////
///////////// Couros de destino de caleiro que n�o ser�o curtidos //////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros em caleiro');
  ReopenVSGerEmCaleiro(CordaCodsBar);

 // MovimCods de couros em caleiro
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT DISTINCT MovimCod, Controle',
  'FROM _vs_em_caleiro_cab_e_its ',
  '']);
  CordaMovCodsInCal := MyObjects.CordaDeQuery(QrExec, 'MovimCod', '0');
  //Geral.MB_Teste(CordaMovCodsInCal);
  CordaMovCntrlsInCal := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //Geral.MB_Teste(CordaMovCntrlsInCal);

  //
  //Parei Aqui! ReopenVSGerCalDst
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros em caleiro');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT * FROM _vs_em_caleiro_cab_e_its ',
  'ORDER BY AnoMesDiaIn, Codigo, Controle ',
  '']);
  // IME-Is de origem do caleiro
  CordaEmCaleiro := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //Geral.MB_Teste(CordaEmCaleiro);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros de destino de caleiro');
  ReopenVSGerCalDst(CordaEmCaleiro);


////////////////////////////////////////////////////////////////////////////////
///////////////////////// Subprodutos de in natura /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de subprodutos de in natura');
  ReopenVSGerSubProd(FCordaCtrlsBar);


////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Couros em curtimento ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de subprodutos de in natura');
  ReopenVSGerEmCurtimento(CordaMovCntrlsInCal);
 // MovimCods de couros em curtimento
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT DISTINCT MovimCod, Controle',
  'FROM _vs_em_curtimento_cab_e_it2 ',
  '']);
  CordaMovCodsInCur := MyObjects.CordaDeQuery(QrExec, 'MovimCod', '0');
  //Geral.MB_Teste(CordaMovCodsInCur);
  FCordaMovCntrsInCur := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');


////////////////////////////////////////////////////////////////////////////////
//////////////// Gera��o de Artigos a partir do "Em Curtimento" ////////////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de artigos gerados a partir do "Em Curtimento"');
  ReopenVSGerArtDeEmCur(FCordaMovCntrsInCur);

  //
  // ????
  //



////////////////////////////////////////////////////////////////////////////////
////////////////// adicionar outros aqui ????????? /////////////////////////////
////////////////////////////////////////////////////////////////////////////////


  //
  // ????
  //

////////////////////////////////////////////////////////////////////////////////
////////////////// Artigos gerados ap�s curtimento /////////////////////////////
/////////// verifica conflitos de GraGruX e DstGGX nas movimnta��oes ///////////
////////////////////////////////////////////////////////////////////////////////

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando artigos gerados');
  ReopenVSGerArtCalCon(CordaCodsBar);
  //
  if VerificaGerArtErro() > 0 then
  begin
    Exit;
  end;


////////////////////////////////////////////////////////////////////////////////
///////// Artigos classificados de artigos gerados ap�s curtimento /////////////
///////// verifica conflitos de GraGruX e DstGGX nas movimnta��oes /////////////
////////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando �rvore de IMEI-s para gera��o e classifica��o');
  GeraArvoreDeIMEIs(2);
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
  'SELECT DISTINCT(Controle) Controle ',
  'FROM _vs_gerartdecur_cab_e_it_j ',
  '']);
  CordaCtrlCladeCur := MyObjects.CordaDeQuery(Query, 'Controle', '-999999999');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando artigos classificados');
  ReopenVSClaDeGerArtDeEmCur(CordaCtrlCladeCur);
  //
  (* ???? Fazer parecido?
  if VerificaGerArtErro2() > 0 then
  begin
    Exit;
  end;
  *)




////////////////////////////////////////////////////////////////////////////////
///////// Parte 1 de 2 de defini��o das colunas din�micas do relat�rio /////////
////////////////// Artigos em processo (cal, conserva, etc?) ///////////////////
////////////////////////////////////////////////////////////////////////////////
///  NO_MovimID         | DstMovID |
///=====================|==========|
///  Em proc. cal.      |    26    |
///  Em proc. conserv.  |    39    |
///                     |          |
///=====================|==========|

  ATT_MovimID := dmkPF.ArrayToTexto('dst.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmids, DModG.MyPID_DB, [
  'SELECT ' + ATT_MovimID + ' CAST(dst.MovimID AS SIGNED) DstMovID ',
  'FROM _vs_entrada_in_natura_cab_e_its inn ',
  'LEFT JOIN _vs_gerartbar_emproccalcon_cab_e_it2 dst ON  ',
  '  dst.SrcMovID=inn.MovimID ',
  '  AND dst.SrcNivel1=inn.Codigo ',
  '  AND dst.SrcNivel2=inn.Controle ',
  'WHERE  ',
(*
  '  dst.MovimID IS NULL ',
  '  OR dst.MovimID <> 6 ',
*)
  '  dst.MovimID <> 6 ',
  '  AND NOT (dst.MovimID IS NULL) ',
  'GROUP BY dst.MovimID',
  'ORDER BY dst.MovimID',
  '']);
  //Geral.MB_Teste(QrEmids.SQL.Text);

  Emids := QrEmids.RecordCount;
  //if (Emids = 0) or (QrEmidsDstMovID.Value <> 0) then
  if QrEmidsDstMovID.Value <> 0 then
  begin
    Emids := Emids + 1;
    Item := 0;
  end else
    Item := -1;
  //
  // A inclus�o dos registros desta tabela ser� deita abaixo pois � necess�rio
  // saber antes a quantidade de registros da tabela QrArtGeComodty (Repeticoes)

////////////////////////////////////////////////////////////////////////////////
///////// Parte 2 de 2 de defini��o das colunas din�micas do relat�rio /////////
///////////// Juntar as outras movimenta��oes em uma �nica SQL /////////////////
/////// para definir os outros Artigos Commodities ArtGeComodty movimentados ///
////////////////////////////////////////////////////////////////////////////////

//TipoTab/ArtGeComodty|Ordem|Sigla      |VisuRel|ShowPecas|ShowPesoKg|ShowAreaM2|ShowPercPc|ShowKgM2|ShowMediaM2
//       /     0	    |  0	|  ??	      |  0	  |   0	    |    0	   |    0	    |    0	   |   0	  |    0
//  11   /     5	    | 999	|Tapete	    |  1	  |   1	    |    2	   |    0	    |    8	   |   0	  |    0
//  12   /	   9     	| 999	|SOLA   	  |  3	  |   1	    |    2	   |    0	    |    0	   |   0	  |    0
//  13   /     11	    | 999	|Aparas ver |  2	  |   0	    |    2	   |    0	    |    8	   |   0	  |    0
//  21   /	   15    	| 999	|Proc.em cu |  3	  |   1	    |    0	   |    0	    |    0	   |   0	  |    0
//  22   /     2	    |99999|WB Dividi  |  5	  |   1	    |    0	   |    4	    |    8	   |   0	  |    0
//  22   /	   3     	|99999|WB Integra |  5	  |   1	    |    0	   |    4	    |    0	   |   0	  |    0

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela de jun��o');
  UnDmkDAC_PF.AbreMySQLQuery0(QrArtGeComodty, DmodG.MyPID_DB, [
  'SELECT 1.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_gerartbar_emproccalcon_cab_e_it2 gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  // 2023-11-28 Evitar artgecomodty = 0 de couros em caleiro e em conserva��o
  // como fica quando tira couro direto do caleiro para GerArt???
  'WHERE NOT (MovimID IN (26,39)) ',
  'GROUP BY ArtGeComodty ',
  '',
  'UNION',
  '',
  'SELECT 11.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_mp_conservado_cab_e_its gci',  // FVSGerMpC
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  'UNION',
  '',
  'SELECT 12.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_dest_proc_cal_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  'UNION',
  '',
  'SELECT 13.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_subprod_cal_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  'UNION',
  '',
  'SELECT 21.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_em_curtimento_cab_e_it2 gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  '',
  'UNION',
  '',
  'SELECT 22.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_gerat_de_emcurt_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  '',
  'UNION',
  '',
  'SELECT 32.0 TipoTab, ',
  'gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_artcla_de_gerart_de_emcurt_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  //
  'ORDER BY ArtGeComOrd, ArtGeComodty',
  '']);
  //Geral.MB_Teste(QrArtGeComodty.SQL.Text);

////////////////////////////////////////////////////////////////////////////////
  // Inclus�o dos registros no array de colunas parte 1/2
////////////////////////////////////////////////////////////////////////////////
  Repeticoes := QrArtGeComodty.RecordCount + Emids;
  QrEmids.First;
  //Item := 0;
  SetLength(FArrCur, Repeticoes);
  SetLength(FArrCla, Repeticoes);
  for I := 0 to Repeticoes - 1 do
  begin
    FArrCur[I] := 0;
    FArrCla[I] := 0;
  end;
  SetLength(FArrAGC, Repeticoes);
  SetLength(FArrSigla, Repeticoes);
  SetLength(FArrTipo, Repeticoes);
  while not QrEmids.Eof do
  begin
    if QrEmidsDstMovID.Value <> 0 then // Exclui n�o processados 100%
    begin
      Item := Item + 1;
      FArrTipo[Item][0] := Integer(arrtipoOutrosEmids); // Emid
      FArrTipo[Item][1] := 0; // TipoTab
      FArrSigla[Item] := QrEmidsNO_MovimID.Value;
      FArrAGC[Item][0] := QrEmidsDstMovID.Value;
      FArrAGC[Item][1] := 1;
      FArrAGC[Item][2] := 1;
      FArrAGC[Item][3] := 1;
      FArrAGC[Item][4] := 0;
      FArrAGC[Item][5] := 0;
      FArrAGC[Item][6] := 0;
      FArrAGC[Item][7] := 0;
    end;
    //
    QrEmids.Next;
  end;
////////////////////////////////////////////////////////////////////////////////
  // Inclus�o dos registros no array de colunas parte 2/2
////////////////////////////////////////////////////////////////////////////////
  while not QrArtGeComodty.Eof do
  begin
    //Item := Emids + QrArtGeComodty.RecNo - 1;
    Item := Item + 1;
    FArrTipo[Item][0] := Integer(arrtipoEmid6); // Artigo de Emid 6
    FArrTipo[Item][1] := Trunc(QrArtGeComodtyTipoTab.Value);
    FArrSigla[Item] := QrArtGeComodtySigla.Value;
    FArrAGC[Item][0] := QrArtGeComodtyArtGeComodty.Value;
    FArrAGC[Item][1] := 1; //QrArtGeComodtyVisuRel.Value; Obrigat�rio
    // Obrigar a mostrar pe�as se n�o mostrar peso para aparecer pelo menos um
    // dos dois para que o item n�o fique sem mostrar nada.
    if QrArtGeComodtyShowPesoKg.Value = 0 then
      FArrAGC[Item][2] := 1
    else
      FArrAGC[Item][2] := (*1*)QrArtGeComodtyShowPecas.Value;
    FArrAGC[Item][3] := (*2*)QrArtGeComodtyShowPesoKg.Value;
    FArrAGC[Item][4] := (*4*)QrArtGeComodtyShowAreaM2.Value;
    FArrAGC[Item][5] := (*8*)QrArtGeComodtyShowPercPc.Value;
    FArrAGC[Item][6] := (*16*)QrArtGeComodtyShowKgM2.Value;
    FArrAGC[Item][7] := (*32*)QrArtGeComodtyShowMediaM2.Value;
    //
    QrArtGeComodty.Next;
  end;
  //
  if Repeticoes = 0 then
    Repeticoes := 1;
  //


////////////////////////////////////////////////////////////////////////////////
// Cria��o da tabela que receber� abaixo todos os registros de movimenta��es a
// serem impressas. Inclus�o dos registros com preencimento das colunas de entrada
// de In Natura. As movimenta��es ser�o colocadas nas colunas depois, tabela por
// tabela dos couros em processo, destinos de processo e gera��es de artigos.
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria de dados compilados');
  FGerFromIn := UnCreateVS.RecriaTempTableNovo(ntrttVSGerArtFromInD2, DModG.QrUpdPID1, False, Repeticoes);  // _vs_ger_art_from_in_d2
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dias do per�odo');
  //
(*
  case RGPesquisa.ItemIndex of
    0: // Rendimento*)
    begin
      // Inn
      SQL_GGXInn := 'CAST(GraGruX AS SIGNED)';
      SQL_GroupInn := 'GGXInn, Marca, AnoMesDiaIn';
      // Ger
      //SQL_GGXDst := 'CAST(dst.GraGruX AS SIGNED)';
      SQL_GGXDst := 'CAST(dst.DstGGX AS SIGNED)';
      SQL_GroupDst := 'AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty';
    end;
    (*1: // Produ��o
    begin
      // Inn
      SQL_GGXInn := 'CAST(0 AS SIGNED)'; //'0';
      SQL_GroupInn := 'AnoMesDiaIn';
      // Ger
      SQL_GGXDst := 'CAST(0 AS SIGNED)';
      SQL_GroupDst := 'AnoMesDiaIn';
    end;
    else
    begin
      SQL_GGXInn := '???';
      SQL_GroupInn := '? ? ?';
      SQL_GGXDst := '???';
      SQL_GroupDst := '? ? ?';
    end;
  end;*)

  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInn, DmodG.MyPID_DB, [
  'SELECT ' + SQL_GGXInn + ' GGXInn, AnoMesDiaIn, ',
  'DATE_FORMAT(DtEntrada, "%Y-%m-%d") Data, Marca, ',
  'SUM(Pecas) InnPecas, ',
  'SUM(PesoKg) InnPesoKg, ',
  'SUM(SdoVrtPeca) InnSdoVrtPeca, ',
  'SUM(SdoVrtPeso) InnSdoVrtPeso, ',
  'SUM(PesoKg) / SUM(Pecas) InnKgPeca, ',
  'SUM(InfPecas) NFePecas, SUM(Pecas) - SUM(SdoVrtPeca) CalPecas  ',
  'FROM _vs_entrada_in_natura_cab_e_its', // + FVSInnIts,
  'GROUP BY ' + SQL_GroupInn,
  'ORDER BY ' + SQL_GroupInn,
  '']);
  //Geral.MB_Teste(QrVSInn.SQL.Text);
  SQL := 'INSERT INTO ' + FGerFromIn +
  '(GGXInn, AnoMesDiaIn, Data, Marca, InnPecas, InnPesoKg, InnKgPeca, NFePecas, CalPecas) VALUES '
  + sLineBreak;
  if QrVSInn.RecordCount > 0 then
  begin
    QrVSInn.First;
    //Geral.MB_Teste(IntToStr(QrVSInn.RecordCount));

    while not QrVSInn.Eof do
    begin
      SQL := SQL + Virgula + '(' +
        Geral.FF0(QrVSInnGGXInn.Value) + ', ' +
        //Geral.FF0(QrVSInnDstMovID.Value) + ', ' +
        Geral.FF0(QrVSInnAnoMesDiaIn.Value) + ', ' +
        '"' + QrVSInnData.Value + '", ' +
        '"' + QrVSInnMarca.Value + '", ' +
        Geral.FFT_Dot(QrVSInnInnPecas.Value, 3, siNegativo) + ', ' +
        Geral.FFT_Dot(QrVSInnInnPesoKg.Value, 3, siNegativo) + ', ' +
        Geral.FFT_Dot(QrVSInnInnKgPeca.Value, 10, siNegativo) + ', ' +
        //Geral.FFT_Dot(QrVSInnInnSdoVrtPeca.Value, 3, siNegativo) + ', ' +
        //Geral.FFT_Dot(QrVSInnInnSdoVrtPeso.Value, 3, siNegativo) + ', ' +
        Geral.FFT_Dot(QrVSInnNFePecas.Value, 3, siNegativo) + ', ' +
        Geral.FFT_Dot(QrVSInnCalPecas.Value, 3, siNegativo) +
        ') ';
      Virgula := ',' + sLineBreak;
      //
      QrVSInn.Next;
    end;
     //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    end else
    begin
      Geral.MB_Aviso('N�o h� entrada de In Natura neste per�odo!');
      Exit;
    end;
   //
   //
  ///
////////////////////////////////////////////////////////////////////////////////
/////////////// Preencimento de colunas com dados de: //////////////////////////
///////////////////  E M   C O N S E R V A  ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros conservados nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEmC, DmodG.MyPID_DB, [
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty, dst.Marca, ',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_em_conserva_cab_e_its dst',
  'GROUP BY Marca, AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY Marca, AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  //Geral.MB_Teste(QrVSEmC.SQL.Text);
  QrVSEmC.First;
  PB1.Position := 0;
  PB1.Max := QrVSEmC.RecordCount;
  while not QrVSEmC.Eof do
  begin
    AnoMesDiaIn := QrVSEmCAnoMesDiaIn.Value;
    //GGXInn := QrVSEmCGraGruX.Value;
    GGXInn := QrVSEmCGGXInn.Value;
    Marca  := QrVSEmCMarca.Value;
    //abrir sql com dados Inn
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSEmCDstMovID.Value in ([6,39... quantos outros???]) then
    if QrVSEmCDstNivel1.Value > 0 then // >> tem Gera��o
    begin
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSEmCPecas.Value / QrDiaInnPecas.Value * 100;
    end;
*)
    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' EmCPecas' + sAGC + '=' + Geral.FFT_Dot(QrVSEmCPecas.Value, 3, siNegativo) + ', ' +
    ' EmCPesoKg' + sAGC + '=' +  Geral.FFT_Dot(QrVSEmCPesoKg.Value, 3, siNegativo) + //', ' +
    //' EmCPercCo' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    ' ';
    //Geral.MB_Teste(SQL);
      try
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
      except
        Geral.MB_Erro('Nivel1 > 0');
      end;
    QrVSEmC.Next;
  end;
  //
   //
  ///
////////////////////////////////////////////////////////////////////////////////
///////////////////// Preencimento de colunas com dados de: ////////////////////
///  G E R A � � O   D E   A R T I G O   D I R E T O   D A   B A R R A C A  ////
/////////////////  C O U R O S   E M   C A L E I R O  //////////////////////////
//////////////  C O U R O S   E M   C O N S E R V A � � O  /////////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando ful�es nos itens de processo de caleiro');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissoes, Dmod.MyDB, [
  'SELECT VSMovCod,',
  'GROUP_CONCAT(DISTINCT(emi.Fulao)) Fuloes',
  'FROM emit emi ',
  'WHERE emi.VSMovCod IN (' + CordaMovCodsInCal + ')',
  'GROUP BY VSMovCod ',
  '']);
  //Geral.MB_Teste(QrEmissoes.SQL.Text);
  //
  QrEmissoes.First;
  while not QrEmissoes.Eof do
  begin
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    'UPDATE _vs_gerartbar_emproccalcon_cab_e_it2 SET ' +
    ' Fuloes="' + QrEmissoesFuloes.Value + '" ' +
    ' WHERE MovimCod=' + Geral.FF0(QrEmissoesVSMovCod.Value));
    //
    QrEmissoes.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de artigos gerados nos Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerCalCon, DmodG.MyPID_DB, [
  'SELECT CAST(inn.GraGruX AS SIGNED) GGXInn,  ',
  SQL_GGXDst + ' GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'inn.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,',
  'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2,',
  'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,',
  'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2,',
  '',
  'SUM(inn.Pecas) InnPecas,  ',
  'SUM(inn.PesoKg) InnPesoKg,  ',
  'SUM(inn.PesoKg) / SUM(inn.Pecas) InnKgPeca,  ',
  'SUM(inn.InfPecas) NFePecas,  ',
  'SUM(inn.Pecas) - SUM(inn.SdoVrtPeca) CalPecas, ',
  'SUM(inn.SdoVrtPeca) ResVrtPeca, ',
  'SUM(inn.SdoVrtPeso) ResVrtPeso, ',
  'inn.Marca, dst.MovimID, ',
  //'dst.Fuloes ',
  'GROUP_CONCAT(DISTINCT(dst.Fuloes)) Fuloes ',
  'FROM _vs_entrada_in_natura_cab_e_its inn ', // + FVSInnIts + ' inn ',
  //'LEFT JOIN ' + FVSGerArt + ' dst ON  ',
  'LEFT JOIN _vs_gerartbar_emproccalcon_cab_e_it2 dst ON  ',
  '  dst.SrcMovID=inn.MovimID ',
  '  AND dst.SrcNivel1=inn.Codigo ',
  '  AND dst.SrcNivel2=inn.Controle ',
  'GROUP BY ' + SQL_GroupDst,
  'ORDER BY ' + SQL_GroupDst,
  '']);
  //
  //Geral.MB_Teste(QrVSGerCalCon.SQL.Text);
  QrVSGerCalCon.First;
  PB1.Position := 0;
  PB1.Max := QrVSGerCalCon.RecordCount;
  while not QrVSGerCalCon.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de artigos gerados nos dias do per�odo');
    ArtGeComodty := QrVSGerCalConArtGeComodty.Value;
    sAGC := '0';
    if QrVSGerCalConDstMovID.Value <> 6 then
    begin
      for I := 0 to Emids - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerCalConDstMovID.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          Break;
        end;
      end;
    end else
    begin
      for I := Emids to Length(FArrAGC) - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerCalConArtGeComodty.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          Break;
        end;
      end;
    end;
    //
    AnoMesDiaIn := QrVSGerCalConAnoMesDiaIn.Value;
    //GGXInn := QrVSGerCalConGraGruX.Value;
    GGXInn := QrVSGerCalConGGXInn.Value;
    //abrir sql com dados Inn
    Marca := QrVSGerCalConMarca.Value;
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSGerCalConDstMovID.Value in ([6,39... quantos outros???]) then
*)
    if QrVSGerCalConDstNivel1.Value > 0 then // >> tem Gera��o
    begin
(*
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSGerCalConPecas.Value / QrDiaInnPecas.Value * 100;
*)
      case QrVSGerCalConDstMovID.Value of
        26: // Em caleiro
        begin
          SQL :=
          'UPDATE ' + FGerFromIn + ' SET ' +
          ' FuloesCal="' + QrVSGerCalConFuloes.Value + '", ' +
          ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConPecas.Value, 3, siNegativo) + ', ' +
          ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerCalConPesoKg.Value, 3, siNegativo) + ', ' +
          ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConQtdGerArM2.Value, 2, siNegativo) + ', ' +
          //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
          ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConRendKgM2.Value, 3, siNegativo) + ', ' +
          ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConMediaM2.Value, 3, siNegativo) +
    //AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty
          ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
          ' AND Marca="' + Marca + '"' +
          ' AND GGXInn=' + Geral.FF0(GGXInn) +
          //' AND MovimID=' + Geral.FF0(QrVSGerCalConMovimID.Value) +
          //' AND ArtGeComodty=' + Geral.FF0(QrVSGerCalConArtGeComodty.Value) +
          ' ';
          //testar
          //Geral.MB_Teste(SQL);
        end;
        39: // Em conserva��o
        begin
          SQL :=
          'UPDATE ' + FGerFromIn + ' SET ' +
          ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConPecas.Value, 3, siNegativo) + ', ' +
          ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerCalConPesoKg.Value, 3, siNegativo) + ', ' +
          ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConQtdGerArM2.Value, 2, siNegativo) + ', ' +
          //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
          ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConRendKgM2.Value, 3, siNegativo) + ', ' +
          ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConMediaM2.Value, 3, siNegativo) +
    //AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty
          ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
          ' AND Marca="' + Marca + '"' +
          ' AND GGXInn=' + Geral.FF0(GGXInn) +
          //' AND MovimID=' + Geral.FF0(QrVSGerCalConMovimID.Value) +
          //' AND ArtGeComodty=' + Geral.FF0(QrVSGerCalConArtGeComodty.Value) +
          ' ';
          //testar
          //Geral.MB_Teste(SQL);
        end;
        else
        begin
          SQL :=
          'UPDATE ' + FGerFromIn + ' SET ' +
          ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConPecas.Value, 3, siNegativo) + ', ' +
          ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerCalConPesoKg.Value, 3, siNegativo) + ', ' +
          ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConQtdGerArM2.Value, 2, siNegativo) + ', ' +
          //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
          ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConRendKgM2.Value, 3, siNegativo) + ', ' +
          ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConMediaM2.Value, 3, siNegativo) +
    //AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty
          ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
          ' AND Marca="' + Marca + '"' +
          ' AND GGXInn=' + Geral.FF0(GGXInn) +
          //' AND MovimID=' + Geral.FF0(QrVSGerCalConMovimID.Value) +
          //' AND ArtGeComodty=' + Geral.FF0(QrVSGerCalConArtGeComodty.Value) +
          ' ';
          //testar
          //Geral.MB_Teste(SQL);
        end;
      end;
      try
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
      except
        Geral.MB_Erro('Nivel1 > 0');
      end;
    end else
    begin
      //PercCo := 100;
      if QrVSGerCalConDstMovID.Value <> 0 then
      begin
        SQL :=
        'UPDATE ' + FGerFromIn + ' SET ' +
        ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerCalConPecas.Value, 3, siNegativo) + ', ' +
        ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerCalConPesoKg.Value, 3, siNegativo) + ', ' +
        ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000, 2, siNegativo) + ', ' +
        //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
        ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000000, 3, siNegativo) + ', ' +
        ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(0.00000000000, 3, siNegativo) +
        ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
        ' AND Marca="' + Marca + '"' +
        ' AND GGXInn=' + Geral.FF0(GGXInn) +
        //' AND MovimID=' + Geral.FF0(QrVSGerCalConMovimID.Value) +
        //' AND ArtGeComodty=' + Geral.FF0(QrVSGerCalConArtGeComodty.Value) +
        ' ';
        //Geral.MB_Teste(SQL);
        try
          UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
        except
          Geral.MB_Erro('Nivel1 <= 0');
        end;
      end;
    end;

    QrVSGerCalCon.Next;
  end;

   //
  ///
////////////////////////////////////////////////////////////////////////////////
////////////// Preenchimento de colunas com dados de: //////////////////////////
//////////////////////  C O N S E R V A D O S  /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros conservados nas Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMpC, DmodG.MyPID_DB, [
  'SELECT cou.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.Marca, dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_mp_conservado_cab_e_its dst', // FVSGerMpC
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  '']);
  //Geral.MB_Teste(QrVSMpC.SQL.Text);
  //
  while not QrVSMpC.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de peles conservadas nos dias do per�odo');
    ArtGeComodty := QrVSMpCArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSMpCArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSMpCAnoMesDiaIn.Value;
    //GGXInn := QrVSMpCGraGruX.Value;
    GGXInn := QrVSMpCGGXInn.Value;
    Marca  := QrVSMpCMarca.Value;
    Pecas  := - QrVSMpCPecas.Value;
    PesoKg := - QrVSMpCPesoKg.Value;

    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' Pecas_'  + sAGC + ' = Pecas_'  + sAGC + ' + ' + Geral.FFT_Dot(Pecas, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + ' = PesoKg_' + sAGC + ' + ' + Geral.FFT_Dot(PesoKg, 3, siNegativo) + // ', ' +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSMpC.Next;
  end;

   //
  ///
////////////////////////////////////////////////////////////////////////////////
////////////////// Preenchimento de colunas com dados de: //////////////////////
//////////////// D E S T I N O   D E   E M   C A L E I R O   ///////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros de destino de caleiro nas Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalDst, DmodG.MyPID_DB, [
  'SELECT cou.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.Marca, dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_dest_proc_cal_cab_e_its dst',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  '']);
  //Geral.MB_Teste(QrVSCalDst.SQL.Text);
  //
  while not QrVSCalDst.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de destino de caleiro nos dias do per�odo');
    ArtGeComodty := QrVSCalDstArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSCalDstArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSCalDstAnoMesDiaIn.Value;
    //GGXInn := QrVSCalDstGraGruX.Value;
    GGXInn := QrVSCalDstGGXInn.Value;
    Marca  := QrVSCalDstMarca.Value;
    Pecas  := - QrVSCalDstPecas.Value;
    PesoKg := - QrVSCalDstPesoKg.Value;

    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' Pecas_'  + sAGC + ' = Pecas_'  + sAGC + ' + ' + Geral.FFT_Dot(Pecas, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + ' = PesoKg_' + sAGC + ' + ' + Geral.FFT_Dot(PesoKg, 3, siNegativo) + // ', ' +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSCalDst.Next;
  end;
  //


   //
  ///
////////////////////////////////////////////////////////////////////////////////
////////////////// Preenchimento de colunas com dados de: //////////////////////
///////////////////////// S U B P R O D U T O S   //////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de subprodutos de in natura nas Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalSub, DmodG.MyPID_DB, [
  'SELECT cou.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.Marca, dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_subprod_cal_cab_e_its dst',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  '']);
  //Geral.MB_Teste(QrVSCalSub.SQL.Text);
  //
  while not QrVSCalSub.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de subprodutos de in natura nos dias do per�odo');
    ArtGeComodty := QrVSCalSubArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSCalSubArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSCalSubAnoMesDiaIn.Value;
    //GGXInn := QrVSCalSubGraGruX.Value;
    GGXInn := QrVSCalSubGGXInn.Value;
    Marca  := QrVSCalSubMarca.Value;
    Pecas  := - QrVSCalSubPecas.Value;
    PesoKg := - QrVSCalSubPesoKg.Value;

    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' Pecas_'  + sAGC + ' = Pecas_'  + sAGC + ' + ' + Geral.FFT_Dot(Pecas, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + ' = PesoKg_' + sAGC + ' + ' + Geral.FFT_Dot(PesoKg, 3, siNegativo) + // ', ' +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSCalSub.Next;
  end;
  //

////////////////////////////////////////////////////////////////////////////////
/////////////// Preencimento de colunas com dados de: //////////////////////////
/////////Final dos couros de caleiro, conserva��o e seus destinos? e  //////////
///// Mais abaixo ser�o preenchidos os couros gerados ap�s curtimento ?? ///////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
//////////  S O M A   D O S   N � O   P R O C E S S A D O S  ///////////////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Totalizando n�o processados');
  SQLpc := '';
  SQLkg := '';
  for I := 0 to Length(FArrSigla) - 1 do
  begin
    if I = 0 then
    begin
      SQLpc := SQLpc + 'pecas_' + Geral.FF0(I + 1);
      SQLkg := SQLkg + 'pesoKg_' + Geral.FF0(I + 1);
    end else
    begin
      SQLpc := SQLpc + ' + ' + 'pecas_' + Geral.FF0(I + 1) ;
      SQLkg := SQLkg + ' + ' + 'pesoKg_' + Geral.FF0(I + 1);
    end;
  end;
  if SQLpc <> '' then
  begin
    SQL := 'UPDATE ' + FGerFromIn +
    ' SET InnSdoVrtPeca = InnPecas - (' + SQLpc + '), ' +
    ' InnSdoVrtPeso = InnPesoKg - (' + SQLkg + ') ' +
    '';
    UnDmkDAC_PF.ExecutaDB(DmodG.MyPID_DB, SQL);
  end;



   //
  ///
////////////////////////////////////////////////////////////////////////////////
////////////////// Preenchimento de colunas com dados de: //////////////////////
///////////////////////// E M   C U R T I M E N T O   //////////////////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando ful�es nos itens de processo de curtimento');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissoes, Dmod.MyDB, [
  'SELECT VSMovCod,',
  'GROUP_CONCAT(DISTINCT(emi.Fulao)) Fuloes',
  'FROM emit emi ',
  'WHERE emi.VSMovCod IN (' + CordaMovCodsInCur + ')',
  'GROUP BY VSMovCod ',
  '']);
  //
  QrEmissoes.First;
  while not QrEmissoes.Eof do
  begin
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    'UPDATE _vs_em_curtimento_cab_e_it2 SET ' +
    ' Fuloes="' + QrEmissoesFuloes.Value + '" ' +
    ' WHERE MovimCod=' + Geral.FF0(QrEmissoesVSMovCod.Value));
    //
    QrEmissoes.Next;
  end;
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros em curtimento nas Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEmCurtim, DmodG.MyPID_DB, [
  //'SELECT cou.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'SELECT dst.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.Marca, dst.AnoMesDiaIn, ', //dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, ',
  //'Fuloes ',
  'GROUP_CONCAT(DISTINCT(dst.Fuloes)) Fuloes ',
  'FROM _vs_em_curtimento_cab_e_it2 dst',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  '']);
  //Geral.MB_Teste(QrVSEmCurtim.SQL.Text);
  //
  while not QrVSEmCurtim.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de courosem curtimento nos dias do per�odo');
    ArtGeComodty := QrVSEmCurtimArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSEmCurtimArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSEmCurtimAnoMesDiaIn.Value;
    //GGXInn := QrVSEmCurtimGraGruX.Value;
    GGXInn := QrVSEmCurtimGGXInn.Value;
    Marca  := QrVSEmCurtimMarca.Value;
    Pecas  := QrVSEmCurtimPecas.Value;
    PesoKg := QrVSEmCurtimPesoKg.Value;

    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' FuloesCur="' + QrVSEmCurtimFuloes.Value + '", ' +
    ' Pecas_'  + sAGC + ' = Pecas_'  + sAGC + ' + ' + Geral.FFT_Dot(Pecas, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + ' = PesoKg_' + sAGC + ' + ' + Geral.FFT_Dot(PesoKg, 3, siNegativo) + // ', ' +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSEmCurtim.Next;
  end;
  //
   //
  ///
////////////////////////////////////////////////////////////////////////////////
///////////////////// Preencimento de colunas com dados de: ////////////////////
//////////////  G E R A � � O   D E   E M   C U R T I M E N T O  ///////////////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de artigos gerados de em curtimento nos Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerDeEmCur, DmodG.MyPID_DB, [
  'SELECT dst.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.Marca, dst.AnoMesDiaIn, ',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, ',
  //'Fuloes ',
  //'GROUP_CONCAT(DISTINCT(dst.Fuloes)) Fuloes ',
  'SUM(-QtdGerArM2) QtdGerArM2, SUM(-QtdGerArP2) QtdGerArP2, ',
  'SUM(-QtdGerArM2) / SUM(-dst.Pecas)  MediaM2, ',
  'SUM(-dst.PesoKg) / SUM(-QtdGerArM2) RendKgM2 ',
  //
  'FROM _vs_gerat_de_emcurt_cab_e_its dst',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty, dst.Marca',
  '']);
  //
  //Geral.MB_Teste(QrVSGerDeEmCur.SQL.Text);
  QrVSGerDeEmCur.First;
  PB1.Position := 0;
  PB1.Max := QrVSGerDeEmCur.RecordCount;
  while not QrVSGerDeEmCur.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de artigos gerados de em curtimento nos dias do per�odo');
    ArtGeComodty := QrVSGerDeEmCurArtGeComodty.Value;
    sAGC := '0';
    if QrVSGerDeEmCurDstMovID.Value <> 6 then
    begin
      for I := 0 to Emids - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerDeEmCurDstMovID.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          FArrCur[I] := I + 1;
          Break;
        end;
      end;
    end else
    begin
      for I := Emids to Length(FArrAGC) - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerDeEmCurArtGeComodty.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          FArrCur[I] := I + 1;
          Break;
        end;
      end;
    end;
    //
    AnoMesDiaIn := QrVSGerDeEmCurAnoMesDiaIn.Value;
    //GGXInn := QrVSGerDeEmCurGraGruX.Value;
    GGXInn := QrVSGerDeEmCurGGXInn.Value;
    //abrir sql com dados Inn
    Marca := QrVSGerDeEmCurMarca.Value;
    //
    if QrVSGerDeEmCurDstNivel1.Value > 0 then // >> tem Gera��o
    begin
      SQL :=
      'UPDATE ' + FGerFromIn + ' SET ' +
      ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerDeEmCurPecas.Value, 3, siNegativo) + ', ' +
      ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerDeEmCurPesoKg.Value, 3, siNegativo) + ', ' +
      ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerDeEmCurQtdGerArM2.Value, 2, siNegativo) + ', ' +
      //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
      ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerDeEmCurRendKgM2.Value, 3, siNegativo) + ', ' +
      ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerDeEmCurMediaM2.Value, 3, siNegativo) +
//AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty
      ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
      ' AND Marca="' + Marca + '"' +
      ' AND GGXInn=' + Geral.FF0(GGXInn) +
      //' AND MovimID=' + Geral.FF0(QrVSGerDeEmCurMovimID.Value) +
      //' AND ArtGeComodty=' + Geral.FF0(QrVSGerDeEmCurArtGeComodty.Value) +
      ' ';
      //Geral.MB_Teste(SQL);
      try
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
      except
        Geral.MB_Erro('Nivel1 > 0');
      end;
    end else
    begin
      //PercCo := 100;
      //if QrVSGerDeEmCurDstMovID.Value <> 0 then
      begin
        SQL :=
        'UPDATE ' + FGerFromIn + ' SET ' +
        ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerDeEmCurPecas.Value, 3, siNegativo) + ', ' +
        ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerDeEmCurPesoKg.Value, 3, siNegativo) + ', ' +
        ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000, 2, siNegativo) + ', ' +
        //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
        ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000000, 3, siNegativo) + ', ' +
        ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(0.00000000000, 3, siNegativo) +
        ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
        ' AND Marca="' + Marca + '"' +
        ' AND GGXInn=' + Geral.FF0(GGXInn) +
        //' AND MovimID=' + Geral.FF0(QrVSGerDeEmCurMovimID.Value) +
        //' AND ArtGeComodty=' + Geral.FF0(QrVSGerDeEmCurArtGeComodty.Value) +
        ' ';
        //Geral.MB_Teste(SQL);
        try
          UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
        except
          Geral.MB_Erro('Nivel1 <= 0');
        end;
      end;
    end;
    //
    QrVSGerDeEmCur.Next;
  end;
(*
  Teste := '';
  for I := 0 to Length(FArrCur) - 1 do
  begin
    if FArrCur[I] > 0 then
      Teste := teste + '[' + Geral.FF0(FArrCur[I]) + '] ';
  end;
  //Geral.MB_Teste(Teste);
*)
   //
  //

////////////////////////////////////////////////////////////////////////////////
///////////////////// Preencimento de colunas com dados de: ////////////////////
///  C L A S S E S   D E   G E R A D O S   D E   E M   C U R T I M E N T O  ////
////////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de artigos clssificados de gerados de em curtimento nos Marcas x dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSClaDeCur, DmodG.MyPID_DB, [
  'SELECT SUM(i.AreaM2) / SUM(IF(i.AreaM2 <> 0, ',
  '  i.Pecas, 0.00)) MediaM2Peca, inn.PesoKg InnKg, ',
  'inn.Pecas InnPc, inn.PesoKg / inn.Pecas KgCouro, ',
  '(inn.PesoKg / inn.Pecas) / ',
  '(',
  'SUM(i.AreaM2) / SUM(IF(i.AreaM2 <> 0, ',
  '  i.Pecas, 0.00)) ',
  ') RendKgm2, SUM(i.AreaM2) SumAreaM2,',
  'SUM(IF(i.AreaM2 <> 0, i.Pecas, 0.00)) SumPecas, ',
  'SUM(IF(i.AreaM2 <> 0, i.PesoKg, 0.00)) SumPesoKg,',
  'i.DataHora, i.DtEntrada_TXT, i.AnoMesDiaIn,',
  'j.SrcNivel2, inn.GraGruX GGXInn, inn.Marca,',
  'cla.ArtGeComodty, cla.MovimID',
  'FROM _vs_artcla_de_gerart_de_emcurt_cab_e_its cla',
  'LEFT JOIN _vs_gerartdecur_cab_e_it_j j',
  '  ON j.Controle=cla.Controle',
  'LEFT JOIN _vs_gerartdecur_cab_e_it_i i',
  '  ON i.ClaCtrl=j.SrcNivel2',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its inn',
  '  ON inn.Controle=i.SrcNiv2Inn',
  'GROUP BY AnoMesDiaIn, MovimID, GGXInn, ArtGeComodty, Marca',
  '']);

  //Geral.MB_Teste(QrVSClaDeCur.SQL.Text);
  //
  while not QrVSClaDeCur.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de courosem curtimento nos dias do per�odo');
    ArtGeComodty := QrVSClaDeCurArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSClaDeCurArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        FArrCla[I] := I + 1;
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSClaDeCurAnoMesDiaIn.Value;
    //GGXInn := QrVSClaDeCurGraGruX.Value;
    GGXInn := QrVSClaDeCurGGXInn.Value;
    Marca  := QrVSClaDeCurMarca.Value;
    //
    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSClaDeCurSumPecas.Value, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSClaDeCurSumPesoKg.Value, 3, siNegativo) + ', ' +


    ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSClaDeCurSumAreaM2.Value, 2, siNegativo) + ', ' +
    //' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
    ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSClaDeCurRendKgM2.Value, 3, siNegativo) + ', ' +
    ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSClaDeCurMediaM2Peca.Value, 3, siNegativo) +
//AnoMesDiaIn, inn.Marca, dst.MovimID, GGXInn, dst.ArtGeComodty
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' AND Marca="' + Marca + '"' +
    //' AND MovimID=' + Geral.FF0(QrVSClaDeCurMovimID.Value) +
    //' AND ArtGeComodty=' + Geral.FF0(QrVSClaDeCurArtGeComodty.Value) +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSClaDeCur.Next;
  end;
(*
  Teste := '';
  for I := 0 to Length(FArrCla) - 1 do
  begin
    if FArrCla[I] > 0 then
      Teste := teste + '[' + Geral.FF0(FArrCla[I]) + '] ';
  end;
  //Geral.MB_Teste(Teste);
*)
///
////////////////////////////////////////////////////////////////////////////////
////////////  C � L C U L O   D O S   P E R C E N T U A I S  ///////////////////
////////////////////////////////////////////////////////////////////////////////
///

////////////////////////  Percentuais de couros  ///////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando percentuais de couros');
  SQL := '';
  N := Length(FArrAGC) - 1;
  for I := 0 to Length(FArrAGC) - 1 do
  begin
    sAGC := Geral.FF0(I + 1);
    //
    SQL := SQL + ' PercCo_' + sAGC + ' = Pecas_' + sAGC + ' / InnPecas * 100 ';
    if i < N then
      SQL := SQL + ', ' + sLineBreak;
  end;
  if SQL <> '' then
  begin
    SQL := Geral.ATS([
      'UPDATE ' + FGerFromIn,
      'SET ' + SQL +
      'WHERE InnPecas > 0']);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  end;

////////////////////////  Percentuais de classes  //////////////////////////////
/// TOTAIS DE CLASSIFICADOS
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando percentuais de classes. Totais de classificadis');
  SQL := '';
  SQLPc := '';
  SQLM2 := '';
  N :=0;
  for I := 0 to Length(FArrCla) - 1 do
  begin
    if FArrCla[I] > 0 then
    begin
      if N > 0 then
      begin
        SQLPc := SQLPc + ' + ';
        SQLM2 := SQLM2 + ' + ';
      end;
      sAGC := Geral.FF0(I + 1);
      //
      SQLPc := SQLPc + ' Pecas_' + sAGC;
      SQLM2 := SQLM2 + ' AreaM2_' + sAGC;
      //
      N := N + 1;
    end;
  end;
  if SQLPc <> '' then
  begin
    SQL := Geral.ATS([
      'UPDATE ' + FGerFromIn,
      'SET ClaPecas=' + SQLPc + ', ' + sLineBreak +
      'ClaAreaM2=' + SQLM2 ]);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  end;
/// PERCENTUAIS DE CLASSES
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando percentuais de classes. Percentuais das classes');
  SQL := '';
  N := 0;
  for I := 0 to Length(FArrCla) - 1 do
  begin
    if FArrCla[I] > 0 then
    begin
      if N > 0 then
        SQL := SQL + ', ' + sLineBreak;
      sAGC := Geral.FF0(I + 1);
      //
      SQL := SQL + ' PercCo_' + sAGC + ' = Pecas_' + sAGC + ' / ClaPecas * 100 ';
      //
      N := N + 1;
    end;
  end;
  if SQL <> '' then
  begin
    SQL := Geral.ATS([
      'UPDATE ' + FGerFromIn,
      'SET ' + SQL +
      //'WHERE ClaPecas > 0'
      '']);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  end;
  //
  ///
////////////////////////////////////////////////////////////////////////////////
//////////////  D A D O S   D E   C L A S S I F I C A � � O  ///////////////////
////////////////////////////////////////////////////////////////////////////////
///
  PesquisaClassificados();
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  //
  BtImprime.Enabled := True;
  BtDescendencia.Enabled := True;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '...');


  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSGerArtMarcaImpBar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtMarcaImpBar.CkCompraFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkCompraIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkEntradaFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkEntradaIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkTemIMEIMrtClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkViagemFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.CkViagemIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

function TFmVSGerArtMarcaImpBar.ConversaoCm(Redimensiona, HalfH: Boolean; Centimetros: Extended): Extended;
var
  Fator: Extended;
begin
  if Redimensiona then
    Fator := (RGFonte.ItemIndex + FFonteMinima) / 7
  else
    Fator := 1;
  if HalfH then
  begin
    Result := (((Centimetros - FEspecoTexto) * Fator) + FEspecoTexto) * FCmFrx;
  end else
    Result := Centimetros * Fator * FCmFrx;
end;

procedure TFmVSGerArtMarcaImpBar.DBGrid1DblClick(Sender: TObject);
begin
  if (QrFaltaMarca.State <> dsInactive) and (QrFaltaMarca.RecordCount > 0) then
    VS_PF.MostraFormVSMovIts(QrFaltaMarcaControle.Value);
end;

function TFmVSGerArtMarcaImpBar.DefineFiltros(): Boolean;
var
  I: Integer;
  Letra, Marca: String;
begin
  Result := False;
  //
  FMarcas1 := EmptyStr;
  FMarcasN.Clear;
  if CkMarcas.Checked then
  begin
    for I := 0 to MeMarcas.Lines.Count - 1 do
    begin
      Marca := Trim(MeMarcas.Lines[I]);
      if Length(Marca) > 0 then
      begin
        if Pos('%', Marca) > 0 then
        begin
          FMarcasN.Append('"' + Marca + '"')
        end
        else
        begin
          if FMarcas1 <> EmptyStr then
            FMarcas1 := FMarcas1 + ', ';
          FMarcas1 := FMarcas1 + '"' + Marca + '"';
        end;
      end;
    end;
    if MyObjects.FIC(FMarcas1 + FMarcasN.Text = EmptyStr, MeMarcas,
    'Informe a(s) Marca(s) ou desmarque a op��o Marcas!') then Exit;
  end;
  //
  FLetras := EmptyStr;
  if CkLetras.Checked then
  begin
    for I := 0 to MeLetras.Lines.Count - 1 do
    begin
      Letra := Trim(MeLetras.Lines[I]);
      if Length(Letra) > 0 then
      begin
        if FLetras <> EmptyStr then
          FLetras := FLetras + ', ';
        FLetras := FLetras + '"' + Letra + '"';
      end;
    end;
    if MyObjects.FIC(FLetras = EmptyStr, MeLetras,
    'Informe a(s) Letra(s) ou desmarque a op��o Letras!') then Exit;
  end;
  //
  Result := True;
end;

function TFmVSGerArtMarcaImpBar.Desconversao(Medidafxt: Extended): Extended;
begin
  Result := Medidafxt / FCmFrx;
end;

procedure TFmVSGerArtMarcaImpBar.DesfazPesquisa();
begin
  BtImprime.Enabled := False;
  BtPesquisa.Enabled := True;
  BtDescendencia.Enabled := False;
end;

procedure TFmVSGerArtMarcaImpBar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtMarcaImpBar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FMarcasN);
end;

procedure TFmVSGerArtMarcaImpBar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCordaCtrlsBar := '-999999999';
  FMarcasN := TStringList.Create;
  //
(*
  TPCompraIni.Date := Date - 30;
  TPCompraFim.Date := Date;
  //
  TPViagemIni.Date := Date - 30;
  TPViagemFim.Date := Date;
  //
*)
  TPEntradaIni.Date := Date - 30;
  TPEntradaFim.Date := Date;
  //
  // 2023-11-28 - Evitar erro na hora de imprimir
  QrClasses.SQL.Text := Geral.ATS([
  'SELECT vmi.GraGruX Reduzido, gg1.Nome, ',
  'SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, ',
  'SUM(cia.AreaP2) AreaP2, ',
  'SUM(cia.AreaM2) / 1.000000 PercM2, ',
  'SUM(cia.Pecas) * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros ',
  'FROM ' + TMeuDB + '.vscacitsa cia ',
  'LEFT JOIN ' + TMeuDB + '.vsmrtcad mrt ON mrt.Codigo=cia.Martelo ',
  'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=cia.VMI_Dest ',
  'LEFT JOIN ' + TMeuDB + '.gragrux  ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1  gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou  xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.couniv1     cn1 ON cn1.Codigo=xco.CouNiv1  ',
  'WHERE VMI_Sorc IN ( ',
  '-999999999 ',
  ') ',
  'AND mrt.Nome="" ',
  'GROUP BY vmi.GraGruX ',
  '']);
  //
  PCPesquisas.ActivePageIndex := 0;
end;

procedure TFmVSGerArtMarcaImpBar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtMarcaImpBar.frxWET_CURTI_241_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PeriodoCompra' then
    Value := EmptyStr (*dmkPF.PeriodoImp1(TPCompraIni.Date, TPCompraFim.Date,
    CkCompraIni.Checked, CkCompraFim.Checked, '', 'at�', '')
*)  else
  if VarName = 'VARF_PeriodoViagem' then
    Value := EmptyStr (*dmkPF.PeriodoImp1(TPViagemIni.Date, TPViagemFim.Date,
    CkViagemIni.Checked, CkViagemFim.Checked, '', 'at�', '')*)
  else
  if VarName = 'VARF_PeriodoEntrada' then
    Value := dmkPF.PeriodoImp1(TPEntradaIni.Date, TPEntradaFim.Date,
    CkEntradaIni.Checked, CkEntradaFim.Checked, '', 'at�', '')
  else
end;

procedure TFmVSGerArtMarcaImpBar.GeraArvoreDeIMEIs(Parte: Integer);
var
  CordaABC, Corda1, Corda2, CordaF1, CordaF2: String;
begin
////////////////////////////////////////////////////////////////////////////////
/////////////  PARTE 1/2 BASE PARA COUROS CURTIDOS E SUA AREA  /////////////////
////////////////////////////////////////////////////////////////////////////////
  if Parte = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_a; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_a ',
    ' ',
    'SELECT Controle, MovimCod, SrcNivel2 SrcNiv2Cur, ',
    'Pecas OriPc, QtdGerArM2 OriM2, QtdGerArP2 OriP2, ',
    'QtdGerPeso OriKg ',
    'FROM ' + TMeuDB + '.vsmovitb vmi ',
    'WHERE vmi.SrcMovID = 27 ',
    'AND vmi.SrcNivel2 IN (' + FCordaMovCntrsInCur + ') ',
    'UNION ',
    'SELECT Controle, MovimCod, SrcNivel2 SrcNiv2Cur, ',
    'Pecas OriPc, QtdGerArM2 OriM2, QtdGerArP2 OriP2, ',
    'QtdGerPeso OriKg ',
    'FROM ' + TMeuDB + '.vsmovits vmi ',
    'WHERE vmi.SrcMovID = 27 ',
    'AND vmi.SrcNivel2 IN (' + FCordaMovCntrsInCur + ') ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_b; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_b ',
    ' ',
    'SELECT Controle, SrcNivel2 SrcNiv2Cal ',
    'FROM ' + TMeuDB + '.vsmovitb vmi ',
    'WHERE vmi.Controle IN (' + FCordaMovCntrsInCur + ') ',
    'UNION ',
    'SELECT Controle, SrcNivel2 SrcNiv2Cal ',
    'FROM ' + TMeuDB + '.vsmovits vmi ',
    'WHERE vmi.Controle IN (' + FCordaMovCntrsInCur + ') ',
    '; ',
    '',
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_c;',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_c',
    '',
    'SELECT a.*, b.SrcNiv2Cal',
    'FROM _vs_gerartdecur_cab_e_it_b b',
    'LEFT JOIN _vs_gerartdecur_cab_e_it_a a ON a.SrcNiv2Cur=b.Controle',
    ';',
    '',
    'SELECT DISTINCT(SrcNiv2Cal) SrcNiv2Cal ',
    'FROM _vs_gerartdecur_cab_e_it_c',
    ';',
    '']);
    //Geral.MB_Teste(Query.SQL.Text);
    CordaABC := MyObjects.CordaDeQuery(Query, 'SrcNiv2Cal', '-999999999');
    //
    UnDmkDAC_PF.ExecutaDB(DmodG.MyPID_DB, Geral.ATS([
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_d; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_d ',
    ' ',
    'SELECT Controle, SrcNivel2 SrcNiv2Inn ',
    'FROM ' + TMeuDB + '.vsmovitb vmi ',
    'WHERE vmi.Controle IN ( ' + CordaABC + ') ',
    'UNION ',
    'SELECT Controle, SrcNivel2 SrcNiv2Inn ',
    'FROM ' + TMeuDB + '.vsmovits vmi ',
    'WHERE vmi.Controle IN ( ' + CordaABC + ') ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_e; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_e ',
    ' ',
    'SELECT c.*, d.SrcNiv2Inn, inn.DataHora, ',
    'DATE_FORMAT(inn.DataHora, "%d/%m/%Y") DtEntrada_TXT, ',
    'YEAR(inn.DataHora)*10000 + MONTH(inn.DataHora)*100 + ',
    'DAY(inn.DataHora) AnoMesDiaIn ',
    'FROM _vs_gerartdecur_cab_e_it_d d ',
    'LEFT JOIN _vs_gerartdecur_cab_e_it_c c ON d.Controle=c.SrcNiv2Cal ',
    'LEFT JOIN ' + TMeuDB + '.vsmovits inn ON inn.Controle=d.SrcNiv2Inn ',
    '; ',
    '']));
  end;
////////////////////////////////////////////////////////////////////////////////
///////////  PARTE 2/2 BASE PARA COUROS CLASSIFICADOS E SUA AREA  //////////////
////////////////////////////////////////////////////////////////////////////////
  if Parte = 2 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
    'SELECT DISTINCT(MovimCod) MovimCod ',
    'FROM _vs_gerartdecur_cab_e_it_a  ',
    '']);
    Corda1 := MyObjects.CordaDeQuery(Query, 'MovimCod', '-999999999');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT DISTINCT(Controle) Controle ',
    'FROM  vsmovits  ',
    'WHERE MovimCod IN (' + Corda1 + ') ',
    'AND MovimNiv=13 ',
    '']);
    Corda2 := MyObjects.CordaDeQuery(Query, 'Controle', '-999999999');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_f; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_f ',
    ' ',
    'SELECT Controle ClaCtrl, MovimNiv ClaNiv,  ',
    'SrcNivel2 ClaSN2, Pecas ClaPc, AreaM2 ClaM2,  ',
    'AreaP2 ClaP2, PesoKg ClaKg ',
    'FROM  ' + TMeuDB + '.vsmovits ',
    'WHERE SrcNivel2 IN (' + Corda2 + ') ',
    '; ',
    'SELECT DISTINCT(ClaSN2) ClaSN2 ',
    'FROM _vs_gerartdecur_cab_e_it_f; ',
    '']);
    CordaF1 := MyObjects.CordaDeQuery(Query, 'ClaSN2', '-999999999');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
    'SELECT DISTINCT(ClaCtrl) ClaCtrl ',
    'FROM _vs_gerartdecur_cab_e_it_f; ',
    '']);
    CordaF2 := MyObjects.CordaDeQuery(Query, 'ClaCtrl', '-999999999');

    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DmodG.MyPID_DB, [
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_g; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_g ',
    ' ',
    'SELECT Controle TotCtrl, MovimCod TotMovCod,  ',
    'MovimNiv TonNiv, Pecas TotPc, AreaM2 TotM2,  ',
    'AreaP2 TotP2, PesoKg TotKg ',
    'FROM  ' + TMeuDB + '.vsmovits ',
    'WHERE Controle IN (',
    CordaF1,
    ') ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_h; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_h ',
    ' ',
    'SELECT g.*, f.*  ',
    'FROM _vs_gerartdecur_cab_e_it_g g ',
    'LEFT JOIN _vs_gerartdecur_cab_e_it_f f ',
    '  ON g.TotCtrl=f.ClaSN2 ',
    '; ',
    ' ',
    '/* ',
    'SELECT e.*, h.*  ',
    'FROM _vs_gerartdecur_cab_e_it_e e ',
    'LEFT JOIN _vs_gerartdecur_cab_e_it_h h ',
    '  ON h.TotMovCod=e.MovimCod ',
    '24 registros*/ ',
    ' ',
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_i; ',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_i ',
    ' ',
    'SELECT h.ClaM2 / h.TotM2 * e.OriM2 AreaM2, ',
    'h.ClaPc / h.TotPc * e.OriPc Pecas, ',
    'h.ClaKg / h.TotKg * e.OriKg PesoKg, ',
    'e.*, h.*  ',
    'FROM _vs_gerartdecur_cab_e_it_h h ',
    'LEFT JOIN _vs_gerartdecur_cab_e_it_e e ',
    '  ON e.MovimCod=h.TotMovCod ',
    '; ',
    'SELECT *  ',
    'FROM _vs_gerartdecur_cab_e_it_i ',
    '; ',
    '']);
    //
    // Buscar controle do registro que contem o GraGruX do couro classificado
    UnDmkDAC_PF.ExecutaDB(DmodG.MyPID_DB, Geral.ATS([
    'DROP TABLE IF EXISTS _vs_gerartdecur_cab_e_it_j;',
    'CREATE TABLE _vs_gerartdecur_cab_e_it_j',
    '',
    'SELECT Controle, SrcNivel2, GraGruX ',
    'FROM  ' + TMeuDB + '.vsmovits',
    'WHERE SrcNivel2 IN (' + CordaF2 + ')',
    '']));
  end;
end;

function TFmVSGerArtMarcaImpBar.GetFontHeightFromFontSize(
  FontSize: Integer): Integer;
begin
  case FontSize of
     6: Result := -8;
     7: Result := -9;
     8: Result := -11;
     9: Result := -12;
    10: Result := -13;
    else Result := -9;
  end;
end;

procedure TFmVSGerArtMarcaImpBar.MeGru(var Memo: TfrxMemoView; var Left: Extended;
  const Top, Width, Height: Extended; const HAlign: TfrxHAlign;
  const Texto: String; const FormatKind: frxClass.TfrxFormatKind;
  const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := 10;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-13;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := []; //[ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0; //0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeHed(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String;
  FontStyle: (*Vcl.Graphics.TFont.*)TFontStyles; FontSize: Integer;
  FrameTyp: (*frxClass.TfrxFrame.*)TfrxFrameTypes; const Texto: String);
begin
  Memo := TfrxMemoView.Create(PageHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top);
  Memo.Width := ConversaoCm(False, False, Width);
  Memo.Height := ConversaoCm(False, False, Height);
  //Memo.DisplayFormat.DecimalSeparator := ',';
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  //Memo.Font.Height := -11;
  Memo.Font.Size := FontSize;
  Memo.Font.Name := 'Arial';
  Memo.Font.Style := FontStyle; //[fsBold];
  Memo.Frame.Typ := FrameTyp; //[];
  Memo.HAlign := haCenter;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeHed2(var Memo: TfrxMemoView; const Left, Top,
  Width, Height: Extended; const HAlign: TfrxHAlign;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String;
  FontStyle: TFontStyles; FontSize: Integer; FrameTyp: TfrxFrameTypes;
  const Texto: String);
begin
  Memo := TfrxMemoView.Create(Header2);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top);
  Memo.Width := ConversaoCm(False, False, Width);
  Memo.Height := ConversaoCm(False, False, Height);
  //Memo.DisplayFormat.DecimalSeparator := ',';
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  //Memo.Font.Height := -11;
  Memo.Font.Size := FontSize;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := FontStyle; //[fsBold];
  Memo.Frame.Typ := FrameTyp; //[];
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeSub(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
begin
  //trocar medidas por variu�veis! 1 cm = 37.7953
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeSub2(var Memo: TfrxMemoView; const Left, Top,
  Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
begin
  Memo := TfrxMemoView.Create(Header2);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(False, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(False, True, Height); // 15.118110240000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := 10; //RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeSum(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(GroupFooter1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height * 2); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := True;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeTit(var Memo: TfrxMemoView; var Left: Extended; const
  Top, Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
begin
  //trocar medidas por variu�veis! 1 cm = 37.7953
  //Memo := TfrxMemoView.Create(PageHeader1);
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
  //
  Left := Left + Width;
end;

procedure TFmVSGerArtMarcaImpBar.MeTot(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  //Memo := TfrxMemoView.Create(ReportSummary1);
  Memo := TfrxMemoView.Create(Footer1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height * 2); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := True;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeTot2(var Memo: TfrxMemoView; const Left, Top,
  Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(Footer2);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(False, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(False, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := 10; //RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := True;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtMarcaImpBar.MeVal(var Memo: TfrxMemoView; var Left: Extended;
  const Top, Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(MasterData1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
  //
  Left := Left + Width;
end;

procedure TFmVSGerArtMarcaImpBar.MeVal2(var Memo: TfrxMemoView;
  var Left: Extended; const Top, Width, Height: Extended;
  const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(MasterData2);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(False, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(False, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := 10; //RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
  //
  Left := Left + Width;
end;

procedure TFmVSGerArtMarcaImpBar.PesquisaClassificados();
var
  MovimCods, OrigIMEIs, ArM2Total, SQL_Letra: String;
begin
  QrClasses.Close;
///
////////////////////////////////////////////////////////////////////////////////
////////////  G E R A � � E S   D E   A R T I G O //////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Obtendo dados de classificados. 1');
  UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAuxPID1, DModG.MyPID_DB, [
  'SELECT GROUP_CONCAT(DISTINCT MovimCod) ',
  'FROM _vs_gerartbar_emproccalcon_cab_e_it2  ',
  'UNION',
  'SELECT GROUP_CONCAT(DISTINCT MovimCod) ',
  'FROM _vs_gerat_de_emcurt_cab_e_its  ',
  '']);
  //Geral.MB_Teste(DModG.QrAuxPID1.SQL.Text);
  MovimCods := DModG.QrAuxPID1.Fields[0].AsString;
  if MovimCods = EmptyStr then
  begin
    // ???
    Exit;
  end;

///
////////////////////////////////////////////////////////////////////////////////
//////  I M E I s   D E   O R I G E M   D E   C L A S S I F I C A � � O  ///////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Obtendo dados de classificados. 2');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT GROUP_CONCAT(DISTINCT(Controle)) ',
  'FROM vsmovits ',
  'WHERE MovimCod IN ( ',
  MovimCods,
  ') ',
  'AND MovimNiv=13 ',
  '']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  OrigIMEIs := Dmod.QrAux.Fields[0].AsString;
  if OrigIMEIs = EmptyStr then
  begin
    // ???
    Exit;
  end;



///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  if CkLetras.Checked then
    SQL_Letra := 'AND mrt.Nome IN (' + FLetras + ') '
  else
    SQL_Letra := '';
///
////////////////////////////////////////////////////////////////////////////////
//////////  T O T A L I Z A � � O   D E   C L A S S I F I C A � � O  ///////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Obtendo dados de classificados. 3');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(cia.AreaM2) AreaM2 ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo ',
  'WHERE VMI_Sorc IN ( ',
  OrigIMEIs,
  ') ',
  SQL_Letra,
  '']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  ArM2Total := Geral.FFT_Dot(Dmod.QrAux.Fields[0].AsFloat / 100, 6, siNegativo);
///
////////////////////////////////////////////////////////////////////////////////
//////////////  D A D O S   D E   C L A S S I F I C A � � O  ///////////////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Obtendo dados de classificados. 3');
  UnDmkDAC_PF.AbreMySQLQuery0(QrClasses, Dmod.MyDB, [
  'SELECT vmi.GraGruX Reduzido, gg1.Nome, ',
  'SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, ',
  'SUM(cia.AreaP2) AreaP2, ',
  'SUM(cia.AreaM2) / ' + ArM2Total + ' PercM2, ',
  'SUM(cia.Pecas) * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros ',
  'FROM ' + TMeuDB + '.vscacitsa cia ',
  'LEFT JOIN ' + TMeuDB + '.vsmrtcad mrt ON mrt.Codigo=cia.Martelo ',
  'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=cia.VMI_Dest ',
  'LEFT JOIN ' + TMeuDB + '.gragrux  ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1  gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou  xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.couniv1     cn1 ON cn1.Codigo=xco.CouNiv1  ',
  'WHERE VMI_Sorc IN ( ',
  OrigIMEIs,
  ') ',
  SQL_Letra,
  'GROUP BY vmi.GraGruX ',
  '']);
  //Geral.MB_Teste(QrClasses.SQL.Text);

end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSClaDeGerArtDeEmCur(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  //
  //'"" Fuloes, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  //'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=vmi.GraGruX',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Controle IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_artcla_de_gerart_de_emcurt_cab_e_its ', // + FVSGerArt,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerArtCalCon(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  //
  '"" Fuloes, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID = 1 ',
  'AND vmi.SrcNivel1 IN (' + Corda + ')',
  // Parei Aqui
  // Colocar aqui somente couros da barraca direto para o gerado????
  //'AND vmi.MovimID=6',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_gerartbar_emproccalcon_cab_e_it2 ', // + FVSGerArt,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerArtDeEmCur(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  // precisa ser antes por causa dos couros classificados
  GeraArvoreDeIMEIs(1);
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  //
  //'"" Fuloes, ',
  // no fim
(*
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
*)
  'DATE_FORMAT(_in.DataHora, "%d/%m/%Y") DataHora_TXT, ',
  'YEAR(_in.DataHora)*10000 + MONTH(_in.DataHora)*100 + ',
  'DAY(_in.DataHora) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_gerartdecur_cab_e_it_e _in ON _in.Controle=vmi.Controle ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID = 27 ', // Em curtimento
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_gerat_de_emcurt_cab_e_its ',
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerCalDst(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  // ini 2023-11-23
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  // fim 2023-11-23
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Exec: String;
  TemIMEIMrt: Integer;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs_caldst_cab_e_it_src_con;',
  'CREATE TABLE _vs_caldst_cab_e_it_src_con',
  'SELECT Controle, SrcNivel2',
  'FROM ' + TMeuDB + '.vsmovitb',
  'WHERE Controle IN (' + Corda + ')',
  'UNION ',
  'SELECT Controle, SrcNivel2',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE Controle IN (' + Corda + ')',
  ';',
  'DROP TABLE IF EXISTS _vs_caldst_cab_e_it_dst_con;',
  'CREATE TABLE _vs_caldst_cab_e_it_dst_con',
  'SELECT cns.*, inn.DataHora,',
  'DATE_FORMAT(inn.DataHora, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(inn.DataHora)*10000 + MONTH(inn.DataHora)*100 + ',
  'DAY(inn.DataHora) AnoMesDiaIn ',
  'FROM _vs_caldst_cab_e_it_src_con cns',
  'LEFT JOIN ' + TMeuDB + '.vsmovits inn ON inn.Controle=cns.SrcNivel2',
  ';',
  //'SELECT * FROM _vs_caldst_cab_e_it_dst_con',
  //';',
  '']);
  //Geral.MB_Teste(Query.SQL.Text);
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  // ini 2023-11-23
  //'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'inn.DtEntrada_TXT DtEntrada_TXT, ',
  //'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + DAY(_in.DtEntrada) AnoMesDiaIn ',
  'inn.AnoMesDiaIn AnoMesDiaIn ',
  // fim 2023-11-23
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  // ini 2023-11-23
  //'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  // ver aqui porque precisava a senten�a de cima!
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=vmi.GraGruX',
  // fim 2023-11-23
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  //'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  'LEFT JOIN _vs_caldst_cab_e_it_dst_con inn ON inn.Controle=vmi.SrcNivel2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID = 26 ', // emidEmProcCal
  'AND vmi.MovimNiv = 31 ', // eminDestCal
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  SQL_Exec := Geral.ATS([
  'INSERT INTO _vs_dest_proc_cal_cab_e_its',
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [SQL_Exec]);
  //Geral.MB_Teste(SQL_Exec);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerEmCaleiro(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=1 ',
  'AND vmi.MovimNiv=29 ', // eminSorcCal
  'AND vmi.SrcNivel1 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_em_caleiro_cab_e_its ', // + FVSGerEmC,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerEmConserva(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=1 ',
  'AND vmi.MovimNiv=65 ', // eminSorcCon
  'AND vmi.SrcNivel1 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_em_conserva_cab_e_its ', // + FVSGerEmC,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerEmCurtimento(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs_emcurtim_cab_e_it_src_con;',
  'CREATE TABLE _vs_emcurtim_cab_e_it_src_con',
  'SELECT Controle, SrcNivel2',
  'FROM ' + TMeuDB + '.vsmovitb',
  'WHERE Controle IN (' + Corda + ')',
  'UNION ',
  'SELECT Controle, SrcNivel2',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE Controle IN (' + Corda + ')',
  ';',
  'DROP TABLE IF EXISTS _vs_emcurtim_cab_e_it_dst_con;',
  'CREATE TABLE _vs_emcurtim_cab_e_it_dst_con',
  'SELECT cns.*, inn.DataHora,',
  'DATE_FORMAT(inn.DataHora, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(inn.DataHora)*10000 + MONTH(inn.DataHora)*100 + ',
  'DAY(inn.DataHora) AnoMesDiaIn ',
  'FROM _vs_emcurtim_cab_e_it_src_con cns',
  'LEFT JOIN ' + TMeuDB + '.vsmovits inn ON inn.Controle=cns.SrcNivel2',
  ';',
  '']);
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  //
  '"" Fuloes, ',
  // no fim
  //'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'inn.DtEntrada_TXT DtEntrada_TXT, ',
  //'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + DAY(_in.DtEntrada) AnoMesDiaIn ',
  'inn.AnoMesDiaIn AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  //'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  'LEFT JOIN _vs_emcurtim_cab_e_it_dst_con inn ON inn.Controle=vmi.SrcNivel2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=27 ',
  'AND vmi.SrcNivel2 IN(' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_em_curtimento_cab_e_it2 ',
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerMpC(Corda: String); // Materia prima conservada
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  // ini 2023-11-23
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  // fim 2023-11-23
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Exec: String;
  TemIMEIMrt: Integer;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DmodG.MyPID_DB, [
 'DROP TABLE IF EXISTS _vs_mpc_cab_e_it_src_con;',
 'CREATE TABLE _vs_mpc_cab_e_it_src_con',
 'SELECT Controle, SrcNivel2',
 'FROM ' + TMeuDB + '.vsmovitb',
 'WHERE Controle IN (' + Corda + ')',
 'UNION ',
 'SELECT Controle, SrcNivel2',
 'FROM ' + TMeuDB + '.vsmovits',
 'WHERE Controle IN (' + Corda + ')',
 ';',
 'DROP TABLE IF EXISTS _vs_mpc_cab_e_it_dst_con;',
 'CREATE TABLE _vs_mpc_cab_e_it_dst_con',
 'SELECT cns.*, inn.DataHora,',
 'DATE_FORMAT(inn.DataHora, "%d/%m/%Y") DtEntrada_TXT, ',
 'YEAR(inn.DataHora)*10000 + MONTH(inn.DataHora)*100 + ',
 'DAY(inn.DataHora) AnoMesDiaIn ',
 'FROM _vs_mpc_cab_e_it_src_con cns',
 'LEFT JOIN ' + TMeuDB + '.vsmovits inn ON inn.Controle=cns.SrcNivel2',
 ';',
 //'SELECT * FROM _vs_mpc_cab_e_it_dst_con',
 //';',
 '']);
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  // ini 2023-11-23
  //'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'inn.DtEntrada_TXT DtEntrada_TXT, ',
  //'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + DAY(_in.DtEntrada) AnoMesDiaIn ',
  'inn.AnoMesDiaIn AnoMesDiaIn ',
  // fim 2023-11-23
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  // ini 2023-11-23
  //'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  // ver aqui porque precisava a senten�a de cima!
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=vmi.GraGruX',
  // fim 2023-11-23
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  //'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  'LEFT JOIN _vs_mpc_cab_e_it_dst_con inn ON inn.Controle=vmi.SrcNivel2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID = 39 ', // emidEmProcCon
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  // Parei Aqui
  // Colocar aqui somente couros da barraca direto para o gerado????
  //'AND vmi.MovimID=6',
  '']);
  SQL_Group := '';
  SQL_Exec := Geral.ATS([
  'INSERT INTO _vs_mp_conservado_cab_e_its', // FVSGerMpC,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [SQL_Exec]);
  //Geral.MB_Teste(SQL_Exec);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSGerSubProd(Corda: String);
  function GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left,
    SQL_Wher, SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
  (*
  var
    SQL_SEL: String;
  begin
    if SQL_Select <> then
      SQL_SEL := SQL_Select
    else
      SQL_SEL := 'SELECT ';
    //
  *)
  begin
    if VS_CRC_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
    SQL_Select + VS_CRC_PF.TabMovVS_Fld_IMEI(tab),
  //  Codigo                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Codigo AS SIGNED) Codigo, ',
  //  Controle                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Controle AS SIGNED) Controle, ',
  //  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
  //MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
  //MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, ',
  //Empresa                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Empresa AS SIGNED) Empresa, ',
  //ClientMO                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, ',
  //Terceiro                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
  //CliVenda                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, ',
  //MovimID                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  //LnkIDXtr                       int(11)      NOT NULL  DEFAULT "0"
  //LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"
  //LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"
  //DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
      'CAST(vmi.DataHora AS DATETIME) DataHora, ',
  //Pallet                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Pallet AS SIGNED) Pallet, ',
  //GraGruX                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
  //Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, ',
  //PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, ',
  //AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, ',
  //AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, ',
  //ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, ',
  //SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, ',
  //SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
  //SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
  //SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
      (*'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, ',*) 'CAST(_in.GraGruX AS SIGNED) SrcGGX, ',
  //SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, ',
  //SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, ',
  //SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, ',
  //Observ                         varchar(255) NOT NULL
      'CAST(vmi.Observ AS CHAR) Observ, ',
  //SerieFch                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  //Ficha                          int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  //Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, ',
  //FornecMO                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, ',
  //CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, ',
  //CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, ',
  //ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
  //CustoPQ                        double(15,4) NOT NULL  DEFAULT "0.0000"
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, ',
  //DstMovID                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, ',
  //DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, ',
  //DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, ',
  //DstGGX                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, ',
  //QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, ',
  //QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
  //QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
  //QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
  //QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, ',
  //QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, ',
  //QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, ',
  //QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, ',
  //AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
  //NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000"
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, ',
  //Marca                          varchar(20)
      'CAST(vmi.Marca AS CHAR) Marca, ',
  //TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
  //Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
  //EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"
  //NotFluxo                       int(11)      NOT NULL  DEFAULT "0"
  //FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
  //FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
  //PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, ',
  //PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, ',
  //PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, ',
  //Lk                             int(11)                DEFAULT "0"
  //DataCad                        date
  //DataAlt                        date
  //UserCad                        int(11)                DEFAULT "0"
  //UserAlt                        int(11)                DEFAULT "0"
  //AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
  //Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
  //CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, ',
  //ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
  //StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, ',
  //ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, ',
  //VSMorCab                       int(11)      NOT NULL  DEFAULT "0"
  //VSMulFrnCab
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, ',
  //NFeSer
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, ',
  //NFeNum
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, ',
  //VSMulNFeCab
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, ',
  //JmpMovID                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, ',
  //JmpNivel1                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, ',
  //JmpNivel2                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, ',
  //RmsMovID                       int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, ',
  //RmsNivel1                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, ',
  //RmsNivel2                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, ',
  //GGXRcl                      int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, ',
  //RmsGGX                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, ',
  //JmpGGX                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, ',
  //DtCorrApo                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, ',


  //IxxMovIX                        tinyint(1)      NOT NULL  DEFAULT "0"
      'CAST(vmi.IxxMovIX AS UNSIGNED) IxxMovIX, ',
  //IxxFolha                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.IxxFolha AS SIGNED) IxxFolha, ',
  //IxxLinha                        int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.IxxLinha AS SIGNED) IxxLinha, ',


  //CusFrtAvuls                     double(15,4) NOT NULL  DEFAULT "0.0000"
      'CAST(vmi.CusFrtAvuls AS DECIMAL (15,4)) CusFrtAvuls, ',
  //CusFrtMOEnv                     double(15,4) NOT NULL  DEFAULT "0.0000"
      'CAST(vmi.CusFrtMOEnv AS DECIMAL (15,4)) CusFrtMOEnv, ',
  //CusFrtMORet                     double(15,4) NOT NULL  DEFAULT "0.0000"
      'CAST(vmi.CusFrtMORet AS DECIMAL (15,4)) CusFrtMORet, ',
  //CusFrtTrnsf                     double(15,4) NOT NULL  DEFAULT "0.0000"
      //'CAST(vmi.CusFrtTrnsf AS DECIMAL (15,4)) CusFrtTrnsf, ',


  //CustoMOPc                      double(15,6) NOT NULL  DEFAULT "0.000000"
      'CAST(vmi.CustoMOPc AS DECIMAL (15,6)) CustoMOPc, ',

      //'vmi.*,
      SQL_Flds,
      'FROM ' + TMeuDB + '.' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
      SQL_Left,
      SQL_Wher,
      SQL_Group,
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;

  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_entrada_in_natura_cab_e_its _in ON _in.Controle=vmi.GSPSrcNiv2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.GSPSrcNiv2 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_subprod_cal_cab_e_its ',
  (*VS_PF.*)GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  (*VS_PF.*)GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA, 0),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.ReopenVSInnIts();
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  I, TemIMEIMrt: Integer;
  SQL_Compra, SQL_Viagem, SQL_Entrada, SQL_Marcas, Marca: String;
begin
  SQL_Compra := EmptyStr;
  SQL_Viagem := EmptyStr;
  SQL_Marcas := EmptyStr;
(*
  SQL_Compra := dmkPF.SQL_Periodo('AND cab.DtCompra ', // vmi.DataHora '
      TPCompraIni.Date, TPCompraFim.Date, CkCompraIni.Checked, CkCompraFim.Checked);
  SQL_Viagem := dmkPF.SQL_Periodo('AND cab.DtViagem ',
      TPViagemIni.Date, TPViagemFim.Date, CkViagemIni.Checked, CkViagemFim.Checked);
*)
  SQL_Entrada := dmkPF.SQL_Periodo('AND cab.DtEntrada ',
      TPEntradaIni.Date, TPEntradaFim.Date, CkEntradaIni.Checked, CkEntradaFim.Checked);
  //
  //if CkMarca.Checked then
  if (Trim(FMarcas1) <> EmptyStr) or (FMarcasN.Text <> EmptyStr) then
  begin
    SQL_Marcas := 'AND (';
    if Trim(FMarcas1) <> EmptyStr then
      SQL_Marcas := SQL_Marcas + 'vmi.Marca IN (' + FMarcas1 + ')';
    for I := 0 to FMarcasN.Count - 1 do
    begin
      if (I > 0) or (Trim(FMarcas1) <> EmptyStr) then
      SQL_Marcas := SQL_Marcas + sLineBreak + ' OR ';
      SQL_Marcas := SQL_Marcas + sLineBreak + ' vmi.Marca LIKE ' + FMarcasN[I];
    end;
    SQL_Marcas := SQL_Marcas + ')';
  end else
    SQL_Marcas := '';
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.QtdGerArM2 / vmi.Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.PesoKg/vmi.QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE( ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'fch.Nome NO_SerieFch, ',
  //
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, ',
  'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA, ',
  //
  'cab.DtCompra, cab.DtViagem, cab.DtEntrada, cab.Fornecedor, ',
  'cab.Transporta, cab.Pecas CabPecas, cab.PesoKg CabPesoKg, ',
  'cab.AreaM2 CabAreaM2, cab.AreaP2 CabAreaP2,  ',
  'cab.ide_serie, cab.ide_nNF, cab.emi_serie, cab.emi_nNF, ',
  'cab.ValorT CabValorT, cab.ValorMP CabValorMP, vmd.InfPecas, ',
  // No fim
  'DATE_FORMAT(cab.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(cab.DtEntrada)*10000 + MONTH(cab.DtEntrada)*100 + ',
  'DAY(cab.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.vsmovdif  vmd ON vmd.Controle=vmi.Controle ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=1 ',
  SQL_Compra,
  SQL_Viagem,
  SQL_Entrada,
  SQL_Marcas,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'INSERT INTO _vs_entrada_in_natura_cab_e_its ', // + FVSInnIts,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtMarcaImpBar.RGPesquisaClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.RGRelatorioClick(Sender: TObject);
begin
  RGSepGrup.Enabled := RGRelatorio.ItemIndex = 2;
end;

procedure TFmVSGerArtMarcaImpBar.SbMesAntClick(Sender: TObject);
begin
  MyObjects.IncMesTPIniEFim(TPEntradaIni, TPEntradaFim, -1);
end;

procedure TFmVSGerArtMarcaImpBar.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.IncMesTPIniEFim(TPEntradaIni, TPEntradaFim, +1);
end;

procedure TFmVSGerArtMarcaImpBar.TPCompraFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPCompraFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPCompraIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPCompraIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPEntradaFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPEntradaFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPEntradaIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPEntradaIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPViagemFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPViagemFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPViagemIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtMarcaImpBar.TPViagemIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

function TFmVSGerArtMarcaImpBar.VerificaGerArtErro(): Integer;
const
  Aviso  = 'As gera��es abaixo est�o com conflito do artigo gerado! Corrija para prosseguir!';
  Titulo = 'Gera��es em conflito de  artigo';
  Prompt = 'Gera��es em conflito de  artigo';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrG13Err, DModG.MyPID_DB, [
  'SELECT DISTINCT g13.Codigo, g13.MovimCod ',
  'FROM _vs_gerartbar_emproccalcon_cab_e_it2 dst ',
  'LEFT JOIN ' + TMeuDB + '.vsmovits g13 ON ',
  '  dst.MovimID=g13.MovimID  ',
  '  AND dst.Codigo=g13.Codigo  ',
  '  AND g13.MovimNiv=13 ',
  'WHERE g13.GraGruX<>dst.DstGGX  ',
  '']);
  Result := QrG13Err.RecordCount;
  if Result > 0 then
  begin
    if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DsG13Err, False, False,
    True, False, False ) then
    begin
      VS_PF.MostraFormVSGeraArt(QrG13ErrCodigo.Value, (*Controle*)0);
    end;
  end;
end;

function TFmVSGerArtMarcaImpBar.VerificaOrfaos(): Integer;
const
  Aviso  = 'Os reduzidos abaixo devem ser configurados informando seu Artigo Commodity!';
  Titulo = 'Reduzidos sem Artigo Commodity';
  Prompt = 'Reduzidos sem Artigo Commodity';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT cou.GraGruX, ggx.GraGruY, gg1.Nome  ',
  'FROM gragruxcou cou ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cou.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE cou.ArtGeComodty = 0 ',
  'AND ggx.GraGruY in (512,1024,1088,1536,1707,2048,3072) ',
  '']);
  Result := QrOrfaos.RecordCount;
  if Result > 0 then
  begin
    if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DsOrfaos, False, False,
    True, False, False ) then
    begin
      Grade_Jan.MostraFormGraGruY(QrOrfaosGragruY.Value, QrOrfaosGragruX.Value, '');
    end;
  end;
end;

//fazer IniMovID, IniNivel1, IniNivel2 e IniGGX? para processos?

//414 fazer letra
end.
