unit VSExcCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnProjGroup_Consts, UnAppEnums,
  UnGrl_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSExcCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSExcCab: TMySQLQuery;
    DsVSExcCab: TDataSource;
    QrVSExcIts: TMySQLQuery;
    DsVSExcIts: TDataSource;
    PMIts: TPopupMenu;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    EdNome: TdmkEdit;
    Label3: TLabel;
    N1: TMenuItem;
    ItsInclui1: TMenuItem;
    QrVSExcCabCodigo: TIntegerField;
    QrVSExcCabNome: TWideStringField;
    QrVSExcCabMovimCod: TIntegerField;
    QrVSExcCabEmpresa: TIntegerField;
    QrVSExcCabDtEntrada: TDateTimeField;
    QrVSExcCabPecas: TFloatField;
    QrVSExcCabPesoKg: TFloatField;
    QrVSExcCabAreaM2: TFloatField;
    QrVSExcCabAreaP2: TFloatField;
    QrVSExcCabValorT: TFloatField;
    QrVSExcCabLk: TIntegerField;
    QrVSExcCabDataCad: TDateField;
    QrVSExcCabDataAlt: TDateField;
    QrVSExcCabUserCad: TIntegerField;
    QrVSExcCabUserAlt: TIntegerField;
    QrVSExcCabAlterWeb: TSmallintField;
    QrVSExcCabAtivo: TSmallintField;
    QrVSExcCabNO_EMPRESA: TWideStringField;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    QrVSExcItsCodigo: TLargeintField;
    QrVSExcItsControle: TLargeintField;
    QrVSExcItsMovimCod: TLargeintField;
    QrVSExcItsMovimNiv: TLargeintField;
    QrVSExcItsMovimTwn: TLargeintField;
    QrVSExcItsEmpresa: TLargeintField;
    QrVSExcItsTerceiro: TLargeintField;
    QrVSExcItsCliVenda: TLargeintField;
    QrVSExcItsMovimID: TLargeintField;
    QrVSExcItsDataHora: TDateTimeField;
    QrVSExcItsPallet: TLargeintField;
    QrVSExcItsGraGruX: TLargeintField;
    QrVSExcItsPecas: TFloatField;
    QrVSExcItsPesoKg: TFloatField;
    QrVSExcItsAreaM2: TFloatField;
    QrVSExcItsAreaP2: TFloatField;
    QrVSExcItsValorT: TFloatField;
    QrVSExcItsSrcMovID: TLargeintField;
    QrVSExcItsSrcNivel1: TLargeintField;
    QrVSExcItsSrcNivel2: TLargeintField;
    QrVSExcItsSrcGGX: TLargeintField;
    QrVSExcItsSdoVrtPeca: TFloatField;
    QrVSExcItsSdoVrtPeso: TFloatField;
    QrVSExcItsSdoVrtArM2: TFloatField;
    QrVSExcItsObserv: TWideStringField;
    QrVSExcItsSerieFch: TLargeintField;
    QrVSExcItsFicha: TLargeintField;
    QrVSExcItsMisturou: TLargeintField;
    QrVSExcItsFornecMO: TLargeintField;
    QrVSExcItsCustoMOKg: TFloatField;
    QrVSExcItsCustoMOM2: TFloatField;
    QrVSExcItsCustoMOTot: TFloatField;
    QrVSExcItsValorMP: TFloatField;
    QrVSExcItsDstMovID: TLargeintField;
    QrVSExcItsDstNivel1: TLargeintField;
    QrVSExcItsDstNivel2: TLargeintField;
    QrVSExcItsDstGGX: TLargeintField;
    QrVSExcItsQtdGerPeca: TFloatField;
    QrVSExcItsQtdGerPeso: TFloatField;
    QrVSExcItsQtdGerArM2: TFloatField;
    QrVSExcItsQtdGerArP2: TFloatField;
    QrVSExcItsQtdAntPeca: TFloatField;
    QrVSExcItsQtdAntPeso: TFloatField;
    QrVSExcItsQtdAntArM2: TFloatField;
    QrVSExcItsQtdAntArP2: TFloatField;
    QrVSExcItsNotaMPAG: TFloatField;
    QrVSExcItsNO_PALLET: TWideStringField;
    QrVSExcItsNO_PRD_TAM_COR: TWideStringField;
    QrVSExcItsNO_TTW: TWideStringField;
    QrVSExcItsID_TTW: TLargeintField;
    QrVSExcCabTemIMEIMrt: TSmallintField;
    RemoveSelecionados1: TMenuItem;
    QrVSExcItsCustoM2: TFloatField;
    QrVSExcItsCustoKg: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSExcCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSExcCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSExcCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSExcCabBeforeClose(DataSet: TDataSet);
    procedure ItsAltera1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure RemoveSelecionados1Click(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSExcIts(Controle: Integer);

  end;

var
  FmVSExcCab: TFmVSExcCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
UnVS_PF, VSExcItsIMEIs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSExcCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSExcCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSExcCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSExcCab, QrVSExcIts);
end;

procedure TFmVSExcCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSExcCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSExcIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSExcIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSExcItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSExcCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSExcCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSExcCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsexccab';
  VAR_GOTOMYSQLTABLE := QrVSExcCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM vsexccab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSExcCab.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  //if Column.FieldName = 'Controle' then
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(QrVSExcItsControle.Value)
  else
  if Campo = 'SrcNivel2' then
      VS_PF.MostraFormVSMovIts(QrVSExcItsSrcNivel2.Value);
end;

procedure TFmVSExcCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSExcCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSExcCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSExcCab.ItsAltera1Click(Sender: TObject);
begin
// Nao fazer!
end;

procedure TFmVSExcCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSExcItsCodigo.Value;
  MovimCod := QrVSExcItsMovimCod.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSExcIts, TIntegerField(QrVSExcItsControle),
  QrVSExcItsControle.Value, CtrlBaix, QrVSExcItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti078)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexccab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSExcCab.RemoveSelecionados1Click(Sender: TObject);
const
  CtrlBaix = 0;
  Pergunta = False;
  Reabre = False;
var
  I, Codigo, MovimCod: Integer;
begin
  if DGDados.SelectedRows.Count > 0 then
  begin
    Codigo   := QrVSExcItsCodigo.Value;
    MovimCod := QrVSExcItsMovimCod.Value;
    //
    with DGDados.DataSource.DataSet do
    for I := 0 to DGDados.SelectedRows.Count-1 do
    begin
      GotoBookmark(DGDados.SelectedRows.Items[I]);
      //
      if VS_PF.ExcluiControleVSMovIts(QrVSExcIts, TIntegerField(QrVSExcItsControle),
      QrVSExcItsControle.Value, CtrlBaix, QrVSExcItsSrcNivel2.Value, False,
      Integer(TEstqMotivDel.emtdWetCurti078), Pergunta, Reabre) then
      begin
      end;
      //
    end;
    VS_PF.AtualizaTotaisVSXxxCab('vsexccab', MovimCod);
    LocCod(Codigo, Codigo);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSExcCab.ReopenVSExcIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSExcIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSExcCabMovimCod.Value),
  'ORDER BY vmi.Controle ',
  '']);
  //
  QrVSExcIts.Locate('Controle', Controle, []);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSExcCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(vmi.AreaM2 <> 0.00, vmi.ValorT/vmi.AreaM2, 0.00) CustoM2, ',
  'IF(vmi.PesoKg <> 0.00, vmi.ValorT/vmi.PesoKg, 0.00) CustoKg, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSExcCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSExcIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrVSExcIts.Locate('Controle', Controle, []);
end;


procedure TFmVSExcCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSExcCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSExcCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSExcCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSExcCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSExcCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSExcCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSExcCabCodigo.Value;
  Close;
end;

procedure TFmVSExcCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSExcCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsexccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSExcCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSExcCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtEntrada, DataHora: String;
  Codigo, MovimCod, Empresa: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtEntrada        := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtEntrada.DateTime < 2, TPDtEntrada,
    'Defina uma data de baixa!') then Exit;

  Codigo := UMyMod.BPGS1I32('vsexccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsexccab', False, [
  'Nome', 'MovimCod', 'Empresa',
  'DtEntrada', 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'], [
  'Codigo'], [
  Nome, MovimCod, Empresa,
  DtEntrada, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if FSeq = 0 then
        VS_PF.InsereVSMovCab(MovimCod, emidEntraExced, Codigo);
    end
    else
    begin
(*
      N�o fazer! Data / hora � individual do IME=I!
      DataHora := DtEntrada;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
*)
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if Codigo = QrVSExcCabCodigo.Value then
    begin
      if FSeq = 1 then
      begin
        FSeq := 2; // Inseriu = True
        Close;
      end;
    end;
  end;
end;

procedure TFmVSExcCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsexccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsexccab', 'Codigo');
end;

procedure TFmVSExcCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSExcCab.ItsInclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmVSExcItsIMEIs, FmVSExcItsIMEIs, afmoNegarComAviso) then
  begin
    FmVSExcItsIMEIs.ImgTipo.SQLType := stIns;
    //
    FmVSExcItsIMEIs.FCodigo   := QrVSExcCabCodigo.Value;
    FmVSExcItsIMEIs.FMovimCod := QrVSExcCabMovimCod.Value;
    FmVSExcItsIMEIs.FEmpresa  := QrVSExcCabEmpresa.Value;
    //
    FmVSExcItsIMEIs.FDataHora := QrVSExcCabDtEntrada.Value;
    FmVSExcItsIMEIs.TPDataSenha.Date := Int(QrVSExcCabDtEntrada.Value);
    FmVSExcItsIMEIs.RGDataHora.Items[1] := 'Do cabe�alho: ' +
      Geral.FDT(QrVSExcCabDtEntrada.Value, 109);
    //
    FmVSExcItsIMEIs.ShowModal;
    Controle := FmVSExcItsIMEIs.FControle;
    FmVSExcItsIMEIs.Destroy;
    //
    ReopenVSExcIts(Controle);
  end;
end;

procedure TFmVSExcCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSExcCab, QrVSExcCabDtEntrada.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSExcCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSExcCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSExcCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSExcCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmVSExcCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExcCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSExcCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExcCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSExcCab.QrVSExcCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSExcCab.QrVSExcCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSExcIts(0);
end;

procedure TFmVSExcCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSExcCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSExcCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSExcCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsexccab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExcCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSExcCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSExcCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsexccab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDtEntrada.Date := DModG.ObtemAgora();
  EdDtEntrada.ValueVariant := DModG.ObtemAgora();
  if EdEmpresa.ValueVariant > 0 then
    TPDtEntrada.SetFocus;
end;

procedure TFmVSExcCab.QrVSExcCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSExcIts.Close;
end;

procedure TFmVSExcCab.QrVSExcCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSExcCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

