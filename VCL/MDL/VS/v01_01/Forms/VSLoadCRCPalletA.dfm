object FmVSLoadCRCPalletA: TFmVSLoadCRCPalletA
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-210 :: ClaReCo - Pallet'
  ClientHeight = 485
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 203
        Height = 32
        Caption = 'ClaReCo - Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 203
        Height = 32
        Caption = 'ClaReCo - Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 203
        Height = 32
        Caption = 'ClaReCo - Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 323
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 323
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 323
        Align = alClient
        TabOrder = 0
        object GBDados: TGroupBox
          Left = 2
          Top = 15
          Width = 780
          Height = 137
          Align = alTop
          Caption = ' Pallet cadastrado no ERP (dados de destino): '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdCodigo
          end
          object Label2: TLabel
            Left = 76
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdNome
          end
          object Label3: TLabel
            Left = 360
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label6: TLabel
            Left = 16
            Top = 56
            Width = 194
            Height = 13
            Caption = 'Artigo de Ribeira Classificado (Reduzido):'
          end
          object Label10: TLabel
            Left = 360
            Top = 56
            Width = 93
            Height = 13
            Caption = 'Cliente preferencial:'
          end
          object Label11: TLabel
            Left = 572
            Top = 56
            Width = 33
            Height = 13
            Caption = 'Status:'
          end
          object Label13: TLabel
            Left = 640
            Top = 16
            Width = 126
            Height = 13
            Caption = 'Data / hora encerramento:'
            FocusControl = DBEdit9
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 16
            Top = 32
            Width = 56
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsVSPalletDst
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdNome: TdmkDBEdit
            Left = 76
            Top = 32
            Width = 281
            Height = 21
            Color = clWhite
            DataField = 'Nome'
            DataSource = DsVSPalletDst
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit1: TDBEdit
            Left = 360
            Top = 32
            Width = 56
            Height = 21
            DataField = 'Empresa'
            DataSource = DsVSPalletDst
            TabOrder = 2
          end
          object DBEdit2: TDBEdit
            Left = 420
            Top = 32
            Width = 217
            Height = 21
            DataField = 'NO_EMPRESA'
            DataSource = DsVSPalletDst
            TabOrder = 3
          end
          object DBEdit3: TDBEdit
            Left = 16
            Top = 72
            Width = 56
            Height = 21
            DataField = 'GraGruX'
            DataSource = DsVSPalletDst
            TabOrder = 4
          end
          object DBEdit4: TDBEdit
            Left = 72
            Top = 72
            Width = 285
            Height = 21
            DataField = 'NO_PRD_TAM_COR'
            DataSource = DsVSPalletDst
            TabOrder = 5
          end
          object DBEdit5: TDBEdit
            Left = 360
            Top = 72
            Width = 56
            Height = 21
            DataField = 'CliStat'
            DataSource = DsVSPalletDst
            TabOrder = 6
          end
          object DBEdit6: TDBEdit
            Left = 416
            Top = 72
            Width = 153
            Height = 21
            DataField = 'NO_CLISTAT'
            DataSource = DsVSPalletDst
            TabOrder = 7
          end
          object DBEdit7: TDBEdit
            Left = 572
            Top = 72
            Width = 56
            Height = 21
            DataField = 'Status'
            DataSource = DsVSPalletDst
            TabOrder = 8
          end
          object DBEdit8: TDBEdit
            Left = 628
            Top = 72
            Width = 141
            Height = 21
            DataField = 'NO_STATUS'
            DataSource = DsVSPalletDst
            TabOrder = 9
          end
          object DBEdit9: TDBEdit
            Left = 640
            Top = 32
            Width = 129
            Height = 21
            DataField = 'DtHrEndAdd_TXT'
            DataSource = DsVSPalletDst
            TabOrder = 10
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 152
          Width = 780
          Height = 137
          Align = alTop
          Caption = ' Pallet cadastrado no Reposit'#243'rio (dados de origem): '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          object Label4: TLabel
            Left = 16
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = dmkDBEdit1
          end
          object Label5: TLabel
            Left = 76
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = dmkDBEdit2
          end
          object Label7: TLabel
            Left = 360
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label8: TLabel
            Left = 16
            Top = 56
            Width = 194
            Height = 13
            Caption = 'Artigo de Ribeira Classificado (Reduzido):'
          end
          object Label9: TLabel
            Left = 360
            Top = 56
            Width = 93
            Height = 13
            Caption = 'Cliente preferencial:'
          end
          object Label12: TLabel
            Left = 572
            Top = 56
            Width = 33
            Height = 13
            Caption = 'Status:'
          end
          object Label14: TLabel
            Left = 640
            Top = 16
            Width = 126
            Height = 13
            Caption = 'Data / hora encerramento:'
            FocusControl = DBEdit18
          end
          object dmkDBEdit1: TdmkDBEdit
            Left = 16
            Top = 32
            Width = 56
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsVSPalletOri
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            UpdType = utYes
            Alignment = taRightJustify
          end
          object dmkDBEdit2: TdmkDBEdit
            Left = 76
            Top = 32
            Width = 281
            Height = 21
            Color = clWhite
            DataField = 'Nome'
            DataSource = DsVSPalletOri
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit10: TDBEdit
            Left = 360
            Top = 32
            Width = 56
            Height = 21
            DataField = 'Empresa'
            DataSource = DsVSPalletOri
            TabOrder = 2
          end
          object DBEdit11: TDBEdit
            Left = 420
            Top = 32
            Width = 217
            Height = 21
            DataField = 'NO_EMPRESA'
            DataSource = DsVSPalletOri
            TabOrder = 3
          end
          object DBEdit12: TDBEdit
            Left = 16
            Top = 72
            Width = 56
            Height = 21
            DataField = 'GraGruX'
            DataSource = DsVSPalletOri
            TabOrder = 4
          end
          object DBEdit13: TDBEdit
            Left = 72
            Top = 72
            Width = 285
            Height = 21
            DataField = 'NO_PRD_TAM_COR'
            DataSource = DsVSPalletOri
            TabOrder = 5
          end
          object DBEdit14: TDBEdit
            Left = 360
            Top = 72
            Width = 56
            Height = 21
            DataField = 'CliStat'
            DataSource = DsVSPalletOri
            TabOrder = 6
          end
          object DBEdit15: TDBEdit
            Left = 416
            Top = 72
            Width = 153
            Height = 21
            DataField = 'NO_CLISTAT'
            DataSource = DsVSPalletOri
            TabOrder = 7
          end
          object DBEdit16: TDBEdit
            Left = 572
            Top = 72
            Width = 56
            Height = 21
            DataField = 'Status'
            DataSource = DsVSPalletOri
            TabOrder = 8
          end
          object DBEdit17: TDBEdit
            Left = 628
            Top = 72
            Width = 141
            Height = 21
            DataField = 'NO_STATUS'
            DataSource = DsVSPalletOri
            TabOrder = 9
          end
          object DBEdit18: TDBEdit
            Left = 640
            Top = 32
            Width = 129
            Height = 21
            DataField = 'DtHrEndAdd_TXT'
            DataSource = DsVSPalletOri
            TabOrder = 10
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 371
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 415
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtAcao: TBitBtn
        Tag = 294
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAcaoClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65535
  end
  object QrVSPalletDst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspalleta let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'WHERE let.Codigo > 0')
    Left = 236
    Top = 69
    object QrVSPalletDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletDstNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletDstLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDstDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDstDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletDstUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletDstUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletDstAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletDstAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletDstEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletDstNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletDstStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletDstCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletDstGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletDstNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletDstNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletDstDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrVSPalletDstDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrVSPalletDstQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
    object QrVSPalletDstGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object QrVSPalletDstDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object QrVSPalletDstMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object QrVSPalletDstClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsVSPalletDst: TDataSource
    DataSet = QrVSPalletDst
    Left = 236
    Top = 113
  end
  object QrVSPalletOri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspalleta let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'WHERE let.Codigo > 0')
    Left = 536
    Top = 13
    object QrVSPalletOriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletOriNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletOriLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletOriDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletOriDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletOriUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletOriUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletOriAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletOriAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletOriEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletOriNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletOriStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletOriCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletOriGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletOriNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletOriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletOriNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletOriDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrVSPalletOriDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrVSPalletOriQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
    object QrVSPalletOriGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object QrVSPalletOriDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object QrVSPalletOriMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object QrVSPalletOriClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSPalletOriAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSPalletOriAWStatSinc: TIntegerField
      FieldName = 'AWStatSinc'
    end
  end
  object DsVSPalletOri: TDataSource
    DataSet = QrVSPalletOri
    Left = 536
    Top = 57
  end
  object PMAcao: TPopupMenu
    Left = 156
    Top = 336
    object ApenasAtrelar1: TMenuItem
      Caption = 'Apenas Atrelar'
      OnClick = ApenasAtrelar1Click
    end
  end
end
