unit VSClaPalPrpQnz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, Vcl.Mask, UnProjGroup_Vars, UnProjGroup_Consts,
  Variants, UnDmkProcFunc, AppListas;

type
  TFmVSClaPalPrpQnz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    PnPartida: TPanel;
    QrVSPallet01: TmySQLQuery;
    QrVSPallet01Codigo: TIntegerField;
    QrVSPallet01Nome: TWideStringField;
    QrVSPallet01Lk: TIntegerField;
    QrVSPallet01DataCad: TDateField;
    QrVSPallet01DataAlt: TDateField;
    QrVSPallet01UserCad: TIntegerField;
    QrVSPallet01UserAlt: TIntegerField;
    QrVSPallet01AlterWeb: TSmallintField;
    QrVSPallet01Ativo: TSmallintField;
    QrVSPallet01Empresa: TIntegerField;
    QrVSPallet01NO_EMPRESA: TWideStringField;
    QrVSPallet01Status: TIntegerField;
    QrVSPallet01CliStat: TIntegerField;
    QrVSPallet01GraGruX: TIntegerField;
    QrVSPallet01NO_CLISTAT: TWideStringField;
    QrVSPallet01NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet01NO_STATUS: TWideStringField;
    DsVSPallet01: TDataSource;
    QrVSESCCabNew: TmySQLQuery;
    QrVSESCCabNewCodigo: TIntegerField;
    QrVSESCCabNewControle: TIntegerField;
    QrVSESCCabNewMovimCod: TIntegerField;
    QrVSESCCabNewEmpresa: TIntegerField;
    QrVSESCCabNewMovimID: TIntegerField;
    QrVSESCCabNewGraGruX: TIntegerField;
    QrVSESCCabNewPecas: TFloatField;
    QrVSESCCabNewPesoKg: TFloatField;
    QrVSESCCabNewAreaM2: TFloatField;
    QrVSESCCabNewAreaP2: TFloatField;
    QrVSESCCabNewLk: TIntegerField;
    QrVSESCCabNewDataCad: TDateField;
    QrVSESCCabNewDataAlt: TDateField;
    QrVSESCCabNewUserCad: TIntegerField;
    QrVSESCCabNewUserAlt: TIntegerField;
    QrVSESCCabNewAlterWeb: TSmallintField;
    QrVSESCCabNewAtivo: TSmallintField;
    QrVSESCCabNewNO_PRD_TAM_COR: TWideStringField;
    QrVSESCCabNewSrcMovID: TIntegerField;
    QrVSESCCabNewSrcNivel1: TIntegerField;
    QrVSESCCabNewSrcNivel2: TIntegerField;
    QrVSESCCabNewPallet: TIntegerField;
    QrVSESCCabNewNO_PALLET: TWideStringField;
    QrVSESCCabNewSdoVrtArM2: TFloatField;
    QrVSESCCabNewSdoVrtPeca: TFloatField;
    QrVSESCCabNewObserv: TWideStringField;
    QrVSESCCabNewValorT: TFloatField;
    QrVSESCCabNewMovimTwn: TIntegerField;
    QrVSESCCabNewCustoMOKg: TFloatField;
    QrVSESCCabNewMovimNiv: TIntegerField;
    QrVSESCCabNewTerceiro: TIntegerField;
    QrVSESCCabNewCliVenda: TIntegerField;
    QrVSESCCabNewLnkNivXtr1: TIntegerField;
    QrVSESCCabNewFicha: TIntegerField;
    QrVSESCCabNewLnkNivXtr2: TIntegerField;
    QrVSESCCabNewDataHora: TDateTimeField;
    QrVSESCCabNewMisturou: TSmallintField;
    QrVSESCCabNewCustoMOTot: TFloatField;
    QrVSESCCabNewSdoVrtPeso: TFloatField;
    QrVSESCCabNewValorMP: TFloatField;
    QrVSESCCabNewDstMovID: TIntegerField;
    QrVSESCCabNewDstNivel1: TIntegerField;
    QrVSESCCabNewDstNivel2: TIntegerField;
    QrVSESCCabNewQtdGerPeca: TFloatField;
    QrVSESCCabNewQtdGerPeso: TFloatField;
    QrVSESCCabNewQtdGerArM2: TFloatField;
    QrVSESCCabNewQtdGerArP2: TFloatField;
    QrVSESCCabNewQtdAntPeca: TFloatField;
    QrVSESCCabNewQtdAntPeso: TFloatField;
    QrVSESCCabNewQtdAntArM2: TFloatField;
    QrVSESCCabNewQtdAntArP2: TFloatField;
    QrVSESCCabNewNO_FORNECE: TWideStringField;
    QrVSESCCabNewNO_FICHA: TWideStringField;
    QrVSESCCabNewCUSTO_M2: TFloatField;
    QrVSESCCabNewCUSTO_P2: TFloatField;
    DsVSESCCabNew: TDataSource;
    PnPallets: TPanel;
    QrVSPallet02: TmySQLQuery;
    DsVSPallet02: TDataSource;
    QrVSPallet02Codigo: TIntegerField;
    QrVSPallet02Nome: TWideStringField;
    QrVSPallet02Lk: TIntegerField;
    QrVSPallet02DataCad: TDateField;
    QrVSPallet02DataAlt: TDateField;
    QrVSPallet02UserCad: TIntegerField;
    QrVSPallet02UserAlt: TIntegerField;
    QrVSPallet02AlterWeb: TSmallintField;
    QrVSPallet02Ativo: TSmallintField;
    QrVSPallet02Empresa: TIntegerField;
    QrVSPallet02NO_EMPRESA: TWideStringField;
    QrVSPallet02Status: TIntegerField;
    QrVSPallet02CliStat: TIntegerField;
    QrVSPallet02GraGruX: TIntegerField;
    QrVSPallet02NO_CLISTAT: TWideStringField;
    QrVSPallet02NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet02NO_STATUS: TWideStringField;
    QrVSPallet03: TmySQLQuery;
    DsVSPallet03: TDataSource;
    QrVSPallet04: TmySQLQuery;
    DsVSPallet04: TDataSource;
    QrVSPallet05: TmySQLQuery;
    DsVSPallet05: TDataSource;
    QrVSPallet06: TmySQLQuery;
    DsVSPallet06: TDataSource;
    QrVSPallet03Codigo: TIntegerField;
    QrVSPallet03Nome: TWideStringField;
    QrVSPallet03Lk: TIntegerField;
    QrVSPallet03DataCad: TDateField;
    QrVSPallet03DataAlt: TDateField;
    QrVSPallet03UserCad: TIntegerField;
    QrVSPallet03UserAlt: TIntegerField;
    QrVSPallet03AlterWeb: TSmallintField;
    QrVSPallet03Ativo: TSmallintField;
    QrVSPallet03Empresa: TIntegerField;
    QrVSPallet03NO_EMPRESA: TWideStringField;
    QrVSPallet03Status: TIntegerField;
    QrVSPallet03CliStat: TIntegerField;
    QrVSPallet03GraGruX: TIntegerField;
    QrVSPallet03NO_CLISTAT: TWideStringField;
    QrVSPallet03NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet03NO_STATUS: TWideStringField;
    QrVSPallet04Codigo: TIntegerField;
    QrVSPallet04Nome: TWideStringField;
    QrVSPallet04Lk: TIntegerField;
    QrVSPallet04DataCad: TDateField;
    QrVSPallet04DataAlt: TDateField;
    QrVSPallet04UserCad: TIntegerField;
    QrVSPallet04UserAlt: TIntegerField;
    QrVSPallet04AlterWeb: TSmallintField;
    QrVSPallet04Ativo: TSmallintField;
    QrVSPallet04Empresa: TIntegerField;
    QrVSPallet04NO_EMPRESA: TWideStringField;
    QrVSPallet04Status: TIntegerField;
    QrVSPallet04CliStat: TIntegerField;
    QrVSPallet04GraGruX: TIntegerField;
    QrVSPallet04NO_CLISTAT: TWideStringField;
    QrVSPallet04NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet04NO_STATUS: TWideStringField;
    QrVSPallet05Codigo: TIntegerField;
    QrVSPallet05Nome: TWideStringField;
    QrVSPallet05Lk: TIntegerField;
    QrVSPallet05DataCad: TDateField;
    QrVSPallet05DataAlt: TDateField;
    QrVSPallet05UserCad: TIntegerField;
    QrVSPallet05UserAlt: TIntegerField;
    QrVSPallet05AlterWeb: TSmallintField;
    QrVSPallet05Ativo: TSmallintField;
    QrVSPallet05Empresa: TIntegerField;
    QrVSPallet05NO_EMPRESA: TWideStringField;
    QrVSPallet05Status: TIntegerField;
    QrVSPallet05CliStat: TIntegerField;
    QrVSPallet05GraGruX: TIntegerField;
    QrVSPallet05NO_CLISTAT: TWideStringField;
    QrVSPallet05NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet05NO_STATUS: TWideStringField;
    QrVSPallet06Codigo: TIntegerField;
    QrVSPallet06Nome: TWideStringField;
    QrVSPallet06Lk: TIntegerField;
    QrVSPallet06DataCad: TDateField;
    QrVSPallet06DataAlt: TDateField;
    QrVSPallet06UserCad: TIntegerField;
    QrVSPallet06UserAlt: TIntegerField;
    QrVSPallet06AlterWeb: TSmallintField;
    QrVSPallet06Ativo: TSmallintField;
    QrVSPallet06Empresa: TIntegerField;
    QrVSPallet06NO_EMPRESA: TWideStringField;
    QrVSPallet06Status: TIntegerField;
    QrVSPallet06CliStat: TIntegerField;
    QrVSPallet06GraGruX: TIntegerField;
    QrVSPallet06NO_CLISTAT: TWideStringField;
    QrVSPallet06NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet06NO_STATUS: TWideStringField;
    Panel13: TPanel;
    LaVSRibCad: TLabel;
    EdIMEI: TdmkEditCB;
    CBIMEI: TdmkDBLookupComboBox;
    SbIMEI: TSpeedButton;
    Panel17: TPanel;
    EdCodigo: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    EdMovimCod: TdmkEdit;
    EdCacCod: TdmkEdit;
    Label32: TLabel;
    QrVSESCCabNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    BtReclasif: TBitBtn;
    Label33: TLabel;
    EdDVIMEI: TdmkEdit;
    Label34: TLabel;
    MeLeitura: TMemo;
    QrVSESCCabNewVSMulFrnCab: TIntegerField;
    Panel21: TPanel;
    PnTecla: TPanel;
    Panel25: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    LaVSRibCla: TLabel;
    SbPallet01: TSpeedButton;
    EdPallet01: TdmkEditCB;
    CBPallet01: TdmkDBLookupComboBox;
    Panel9: TPanel;
    Panel7: TPanel;
    PnTecla2: TPanel;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    SBPallet02: TSpeedButton;
    EdPallet02: TdmkEditCB;
    CBPallet02: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    SBPallet03: TSpeedButton;
    EdPallet03: TdmkEditCB;
    CBPallet03: TdmkDBLookupComboBox;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    SBPallet04: TSpeedButton;
    EdPallet04: TdmkEditCB;
    CBPallet04: TdmkDBLookupComboBox;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    SBPallet05: TSpeedButton;
    EdPallet05: TdmkEditCB;
    CBPallet05: TdmkDBLookupComboBox;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    SBPallet06: TSpeedButton;
    EdPallet06: TdmkEditCB;
    CBPallet06: TdmkDBLookupComboBox;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    SBPallet07: TSpeedButton;
    EdPallet07: TdmkEditCB;
    CBPallet07: TdmkDBLookupComboBox;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    SBPallet09: TSpeedButton;
    EdPallet09: TdmkEditCB;
    CBPallet09: TdmkDBLookupComboBox;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    SBPallet10: TSpeedButton;
    EdPallet10: TdmkEditCB;
    CBPallet10: TdmkDBLookupComboBox;
    Panel37: TPanel;
    Panel38: TPanel;
    Panel39: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    SBPallet11: TSpeedButton;
    EdPallet11: TdmkEditCB;
    CBPallet11: TdmkDBLookupComboBox;
    Panel40: TPanel;
    Panel41: TPanel;
    Panel42: TPanel;
    Label14: TLabel;
    Label17: TLabel;
    SBPallet12: TSpeedButton;
    EdPallet12: TdmkEditCB;
    CBPallet12: TdmkDBLookupComboBox;
    Panel43: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    SBPallet13: TSpeedButton;
    EdPallet13: TdmkEditCB;
    CBPallet13: TdmkDBLookupComboBox;
    Panel46: TPanel;
    Panel47: TPanel;
    Panel48: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    SBPallet14: TSpeedButton;
    EdPallet14: TdmkEditCB;
    CBPallet14: TdmkDBLookupComboBox;
    Panel49: TPanel;
    Panel50: TPanel;
    Panel51: TPanel;
    Label24: TLabel;
    Label27: TLabel;
    SBPallet15: TSpeedButton;
    EdPallet15: TdmkEditCB;
    CBPallet15: TdmkDBLookupComboBox;
    Panel52: TPanel;
    Panel53: TPanel;
    Panel54: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    SBPallet08: TSpeedButton;
    EdPallet08: TdmkEditCB;
    CBPallet08: TdmkDBLookupComboBox;
    Panel55: TPanel;
    QrVSPallet07: TmySQLQuery;
    QrVSPallet07Codigo: TIntegerField;
    QrVSPallet07Nome: TWideStringField;
    QrVSPallet07Lk: TIntegerField;
    QrVSPallet07DataCad: TDateField;
    QrVSPallet07DataAlt: TDateField;
    QrVSPallet07UserCad: TIntegerField;
    QrVSPallet07UserAlt: TIntegerField;
    QrVSPallet07AlterWeb: TSmallintField;
    QrVSPallet07Ativo: TSmallintField;
    QrVSPallet07Empresa: TIntegerField;
    QrVSPallet07NO_EMPRESA: TWideStringField;
    QrVSPallet07Status: TIntegerField;
    QrVSPallet07CliStat: TIntegerField;
    QrVSPallet07GraGruX: TIntegerField;
    QrVSPallet07NO_CLISTAT: TWideStringField;
    QrVSPallet07NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet07NO_STATUS: TWideStringField;
    DsVSPallet07: TDataSource;
    QrVSPallet08: TmySQLQuery;
    QrVSPallet08Codigo: TIntegerField;
    QrVSPallet08Nome: TWideStringField;
    QrVSPallet08Lk: TIntegerField;
    QrVSPallet08DataCad: TDateField;
    QrVSPallet08DataAlt: TDateField;
    QrVSPallet08UserCad: TIntegerField;
    QrVSPallet08UserAlt: TIntegerField;
    QrVSPallet08AlterWeb: TSmallintField;
    QrVSPallet08Ativo: TSmallintField;
    QrVSPallet08Empresa: TIntegerField;
    QrVSPallet08NO_EMPRESA: TWideStringField;
    QrVSPallet08Status: TIntegerField;
    QrVSPallet08CliStat: TIntegerField;
    QrVSPallet08GraGruX: TIntegerField;
    QrVSPallet08NO_CLISTAT: TWideStringField;
    QrVSPallet08NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet08NO_STATUS: TWideStringField;
    DsVSPallet08: TDataSource;
    QrVSPallet09: TmySQLQuery;
    QrVSPallet09Codigo: TIntegerField;
    QrVSPallet09Nome: TWideStringField;
    QrVSPallet09Lk: TIntegerField;
    QrVSPallet09DataCad: TDateField;
    QrVSPallet09DataAlt: TDateField;
    QrVSPallet09UserCad: TIntegerField;
    QrVSPallet09UserAlt: TIntegerField;
    QrVSPallet09AlterWeb: TSmallintField;
    QrVSPallet09Ativo: TSmallintField;
    QrVSPallet09Empresa: TIntegerField;
    QrVSPallet09NO_EMPRESA: TWideStringField;
    QrVSPallet09Status: TIntegerField;
    QrVSPallet09CliStat: TIntegerField;
    QrVSPallet09GraGruX: TIntegerField;
    QrVSPallet09NO_CLISTAT: TWideStringField;
    QrVSPallet09NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet09NO_STATUS: TWideStringField;
    DsVSPallet09: TDataSource;
    QrVSPallet10: TmySQLQuery;
    QrVSPallet10Codigo: TIntegerField;
    QrVSPallet10Nome: TWideStringField;
    QrVSPallet10Lk: TIntegerField;
    QrVSPallet10DataCad: TDateField;
    QrVSPallet10DataAlt: TDateField;
    QrVSPallet10UserCad: TIntegerField;
    QrVSPallet10UserAlt: TIntegerField;
    QrVSPallet10AlterWeb: TSmallintField;
    QrVSPallet10Ativo: TSmallintField;
    QrVSPallet10Empresa: TIntegerField;
    QrVSPallet10NO_EMPRESA: TWideStringField;
    QrVSPallet10Status: TIntegerField;
    QrVSPallet10CliStat: TIntegerField;
    QrVSPallet10GraGruX: TIntegerField;
    QrVSPallet10NO_CLISTAT: TWideStringField;
    QrVSPallet10NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet10NO_STATUS: TWideStringField;
    DsVSPallet10: TDataSource;
    QrVSPallet11: TmySQLQuery;
    QrVSPallet11Codigo: TIntegerField;
    QrVSPallet11Nome: TWideStringField;
    QrVSPallet11Lk: TIntegerField;
    QrVSPallet11DataCad: TDateField;
    QrVSPallet11DataAlt: TDateField;
    QrVSPallet11UserCad: TIntegerField;
    QrVSPallet11UserAlt: TIntegerField;
    QrVSPallet11AlterWeb: TSmallintField;
    QrVSPallet11Ativo: TSmallintField;
    QrVSPallet11Empresa: TIntegerField;
    QrVSPallet11NO_EMPRESA: TWideStringField;
    QrVSPallet11Status: TIntegerField;
    QrVSPallet11CliStat: TIntegerField;
    QrVSPallet11GraGruX: TIntegerField;
    QrVSPallet11NO_CLISTAT: TWideStringField;
    QrVSPallet11NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet11NO_STATUS: TWideStringField;
    DsVSPallet11: TDataSource;
    QrVSPallet12: TmySQLQuery;
    QrVSPallet12Codigo: TIntegerField;
    QrVSPallet12Nome: TWideStringField;
    QrVSPallet12Lk: TIntegerField;
    QrVSPallet12DataCad: TDateField;
    QrVSPallet12DataAlt: TDateField;
    QrVSPallet12UserCad: TIntegerField;
    QrVSPallet12UserAlt: TIntegerField;
    QrVSPallet12AlterWeb: TSmallintField;
    QrVSPallet12Ativo: TSmallintField;
    QrVSPallet12Empresa: TIntegerField;
    QrVSPallet12NO_EMPRESA: TWideStringField;
    QrVSPallet12Status: TIntegerField;
    QrVSPallet12CliStat: TIntegerField;
    QrVSPallet12GraGruX: TIntegerField;
    QrVSPallet12NO_CLISTAT: TWideStringField;
    QrVSPallet12NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet12NO_STATUS: TWideStringField;
    DsVSPallet12: TDataSource;
    QrVSPallet13: TmySQLQuery;
    QrVSPallet13Codigo: TIntegerField;
    QrVSPallet13Nome: TWideStringField;
    QrVSPallet13Lk: TIntegerField;
    QrVSPallet13DataCad: TDateField;
    QrVSPallet13DataAlt: TDateField;
    QrVSPallet13UserCad: TIntegerField;
    QrVSPallet13UserAlt: TIntegerField;
    QrVSPallet13AlterWeb: TSmallintField;
    QrVSPallet13Ativo: TSmallintField;
    QrVSPallet13Empresa: TIntegerField;
    QrVSPallet13NO_EMPRESA: TWideStringField;
    QrVSPallet13Status: TIntegerField;
    QrVSPallet13CliStat: TIntegerField;
    QrVSPallet13GraGruX: TIntegerField;
    QrVSPallet13NO_CLISTAT: TWideStringField;
    QrVSPallet13NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet13NO_STATUS: TWideStringField;
    DsVSPallet13: TDataSource;
    QrVSPallet14: TmySQLQuery;
    QrVSPallet14Codigo: TIntegerField;
    QrVSPallet14Nome: TWideStringField;
    QrVSPallet14Lk: TIntegerField;
    QrVSPallet14DataCad: TDateField;
    QrVSPallet14DataAlt: TDateField;
    QrVSPallet14UserCad: TIntegerField;
    QrVSPallet14UserAlt: TIntegerField;
    QrVSPallet14AlterWeb: TSmallintField;
    QrVSPallet14Ativo: TSmallintField;
    QrVSPallet14Empresa: TIntegerField;
    QrVSPallet14NO_EMPRESA: TWideStringField;
    QrVSPallet14Status: TIntegerField;
    QrVSPallet14CliStat: TIntegerField;
    QrVSPallet14GraGruX: TIntegerField;
    QrVSPallet14NO_CLISTAT: TWideStringField;
    QrVSPallet14NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet14NO_STATUS: TWideStringField;
    DsVSPallet14: TDataSource;
    QrVSPallet15: TmySQLQuery;
    QrVSPallet15Codigo: TIntegerField;
    QrVSPallet15Nome: TWideStringField;
    QrVSPallet15Lk: TIntegerField;
    QrVSPallet15DataCad: TDateField;
    QrVSPallet15DataAlt: TDateField;
    QrVSPallet15UserCad: TIntegerField;
    QrVSPallet15UserAlt: TIntegerField;
    QrVSPallet15AlterWeb: TSmallintField;
    QrVSPallet15Ativo: TSmallintField;
    QrVSPallet15Empresa: TIntegerField;
    QrVSPallet15NO_EMPRESA: TWideStringField;
    QrVSPallet15Status: TIntegerField;
    QrVSPallet15CliStat: TIntegerField;
    QrVSPallet15GraGruX: TIntegerField;
    QrVSPallet15NO_CLISTAT: TWideStringField;
    QrVSPallet15NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet15NO_STATUS: TWideStringField;
    DsVSPallet15: TDataSource;
    Label35: TLabel;
    EdVSPwdDdClas: TEdit;
    QrVSESCCabNewClientMO: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label36: TLabel;
    EdPallet: TdmkEdit;
    QrVSESCCabNewGraGruY: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPallet01Click(Sender: TObject);
    procedure SbIMEIClick(Sender: TObject);
    procedure SBPallet02Click(Sender: TObject);
    procedure SBPallet03Click(Sender: TObject);
    procedure SBPallet04Click(Sender: TObject);
    procedure SBPallet05Click(Sender: TObject);
    procedure SBPallet06Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdIMEIRedefinido(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure MeLeituraChange(Sender: TObject);
    procedure EdIMEIChange(Sender: TObject);
    procedure SBPallet08Click(Sender: TObject);
    procedure SBPallet09Click(Sender: TObject);
    procedure SBPallet10Click(Sender: TObject);
    procedure SBPallet11Click(Sender: TObject);
    procedure SBPallet12Click(Sender: TObject);
    procedure SBPallet13Click(Sender: TObject);
    procedure SBPallet14Click(Sender: TObject);
    procedure SBPallet15Click(Sender: TObject);
    procedure SBPallet07Click(Sender: TObject);
    procedure EdPalletChange(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure CadastraPallet(EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
    procedure LiberaConfig();
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery);
    procedure ReopenVSGerArtDst(Controle: Integer);
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    FEdPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TdmkEditCB;
    FCBPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TdmkDBLookupComboBox;
    //
  end;

  var
  FmVSClaPalPrpQnz: TFmVSClaPalPrpQnz;

implementation

uses UnMyObjects, Module, DmkDAC_PF, VSPalletAdd, MyDBCheck, ModuleGeral,
  MyListas, UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSClaPalPrpQnz.BtOKClick(Sender: TObject);
const
  sProcName = 'FmVSClaPalPrpQnz.BtOKClick()';
var
  Empresa, ClientMO, Fornecedor, Codigo, MovimCod, VSESCCab, VsMovIts, I, Controle,
  LstPal01, LstPal02, LstPal03, LstPal04, LstPal05,
  LstPal06, LstPal07, LstPal08, LstPal09, LstPal10,
  LstPal11, LstPal12, LstPal13, LstPal14, LstPal15,
  GraGruX01, CtrlSorc01, CtrlDest01,
  GraGruX02, CtrlSorc02, CtrlDest02,
  GraGruX03, CtrlSorc03, CtrlDest03,
  GraGruX04, CtrlSorc04, CtrlDest04,
  GraGruX05, CtrlSorc05, CtrlDest05,
  GraGruX06, CtrlSorc06, CtrlDest06,
  GraGruX07, CtrlSorc07, CtrlDest07,
  GraGruX08, CtrlSorc08, CtrlDest08,
  GraGruX09, CtrlSorc09, CtrlDest09,
  GraGruX10, CtrlSorc10, CtrlDest10,
  GraGruX11, CtrlSorc11, CtrlDest11,
  GraGruX12, CtrlSorc12, CtrlDest12,
  GraGruX13, CtrlSorc13, CtrlDest13,
  GraGruX14, CtrlSorc14, CtrlDest14,
  GraGruX15, CtrlSorc15, CtrlDest15,
  BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, CacCod, BxaGraGruX,
  VSMulFrnCab, StqCenLoc: Integer;
  DtHrIni, DtHrCfgCla: String;
  //
begin
  if MyObjects.FIC(EdIMEI.ValueVariant = 0, EdIMEI, 'IME-I inv�lido!') then
    Exit;
  if VS_CRC_PF.SenhaVSPwdDdNaoConfere(EdVSPwdDdClas.Text) then
    Exit;
  //
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  CacCod         := EdCacCod.ValueVariant;
  VSESCCab       := QrVSESCCabNewCodigo.Value;
  VSMovIts       := QrVSESCCabNewControle.Value;
  Empresa        := QrVSESCCabNewEmpresa.Value;
  ClientMO       := QrVSESCCabNewClientMO.Value;
  Fornecedor     := QrVSESCCabNewTerceiro.Value;
  VSMulFrnCab    := QrVSESCCabNewVSMulFrnCab.Value;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local!') then
    Exit;
  //

  if MyObjects.FIC(EdIMEI.ValueVariant <> VSMovIts, EdIMEI, 'Processo corrompido!' +
  'Feche a janela e tente novamente!') then
    Exit;
  case TEstqMovimID(QrVSESCCabNewMovimID.Value) of
    (*06*)emidIndsXX: FMovimID := emidClassArtXXUni;
    (*07*)emidClassArtXXUni,
    (*08*)emidReclasXXUni: FMovimID := emidReclasXXUni;
    (*36*)emidInnSemCob:
    begin
      case QrVSESCCabNewGraGruY.Value of
        (*2048*)CO_GraGruY_2048_VSRibCad: FMovimID := emidClassArtXXUni;
        (*3072*)CO_GraGruY_3072_VSRibCla: FMovimID := emidReclasXXUni;
        else
        begin
          Geral.MB_Aviso('"MovimID" n�o implementado (2) em ' + sProcName);
          Exit;
        end;
      end;
    end;
    else
    begin
      Geral.MB_Aviso('"MovimID" n�o implementado (1) em ' + sProcName);
      Exit;
    end;
  end;
  // Nao permitir duas vezes o mesmo pallet
  LstPal01 := EdPallet01.ValueVariant;
  LstPal02 := EdPallet02.ValueVariant;
  LstPal03 := EdPallet03.ValueVariant;
  LstPal04 := EdPallet04.ValueVariant;
  LstPal05 := EdPallet05.ValueVariant;
  LstPal06 := EdPallet06.ValueVariant;
  LstPal07 := EdPallet07.ValueVariant;
  LstPal08 := EdPallet08.ValueVariant;
  LstPal09 := EdPallet09.ValueVariant;
  LstPal10 := EdPallet10.ValueVariant;
  LstPal11 := EdPallet11.ValueVariant;
  LstPal12 := EdPallet12.ValueVariant;
  LstPal13 := EdPallet13.ValueVariant;
  LstPal14 := EdPallet14.ValueVariant;
  LstPal15 := EdPallet15.ValueVariant;
  //
  GraGruX01 := QrVSPallet01GraGruX.Value;
  GraGruX02 := QrVSPallet02GraGruX.Value;
  GraGruX03 := QrVSPallet03GraGruX.Value;
  GraGruX04 := QrVSPallet04GraGruX.Value;
  GraGruX05 := QrVSPallet05GraGruX.Value;
  GraGruX06 := QrVSPallet06GraGruX.Value;
  GraGruX07 := QrVSPallet07GraGruX.Value;
  GraGruX08 := QrVSPallet08GraGruX.Value;
  GraGruX09 := QrVSPallet09GraGruX.Value;
  GraGruX10 := QrVSPallet10GraGruX.Value;
  GraGruX11 := QrVSPallet11GraGruX.Value;
  GraGruX12 := QrVSPallet12GraGruX.Value;
  GraGruX13 := QrVSPallet13GraGruX.Value;
  GraGruX14 := QrVSPallet14GraGruX.Value;
  GraGruX15 := QrVSPallet15GraGruX.Value;
  //
  if VS_CRC_PF.PalletDuplicad3(VAR_CLA_ART_RIB_MAX_BOX_15,
    [LstPal01, LstPal02, LstPal03, LstPal04, LstPal05,
    LstPal06, LstPal07, LstPal08, LstPal09, LstPal10,
    LstPal11, LstPal12, LstPal13, LstPal14, LstPal15]) then
      Exit;
  //
  // Ver se selecionou pelo menos um pallet!
  if MyObjects.FIC((LstPal01 = 0) and (LstPal02 = 0) and (LstPal03 = 0)
  and (LstPal04 = 0) and (LstPal05 = 0) and (LstPal06 = 0), nil,
  'Informe pelo menos um pallet!') then
     Exit;
  //
  //
  //Veificar se a empresa dos pallets selecionados eh a mesma do pallet a classificar!
  if MyObjects.FIC((LstPal01 <> 0) and (QrVSPallet01Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 01!') then Exit;
  if MyObjects.FIC((LstPal02 <> 0) and (QrVSPallet02Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 02!') then Exit;
  if MyObjects.FIC((LstPal03 <> 0) and (QrVSPallet03Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 03!') then Exit;
  if MyObjects.FIC((LstPal04 <> 0) and (QrVSPallet04Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 04!') then Exit;
  if MyObjects.FIC((LstPal05 <> 0) and (QrVSPallet05Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 05!') then Exit;
  if MyObjects.FIC((LstPal06 <> 0) and (QrVSPallet06Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 06!') then Exit;
  if MyObjects.FIC((LstPal07 <> 0) and (QrVSPallet07Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 07!') then Exit;
  if MyObjects.FIC((LstPal08 <> 0) and (QrVSPallet08Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 08!') then Exit;
  if MyObjects.FIC((LstPal09 <> 0) and (QrVSPallet09Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 09!') then Exit;
  if MyObjects.FIC((LstPal10 <> 0) and (QrVSPallet10Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 10!') then Exit;
  if MyObjects.FIC((LstPal11 <> 0) and (QrVSPallet11Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 11!') then Exit;
  if MyObjects.FIC((LstPal12 <> 0) and (QrVSPallet12Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 12!') then Exit;
  if MyObjects.FIC((LstPal13 <> 0) and (QrVSPallet13Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 13!') then Exit;
  if MyObjects.FIC((LstPal14 <> 0) and (QrVSPallet14Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 14!') then Exit;
  if MyObjects.FIC((LstPal15 <> 0) and (QrVSPallet15Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 15!') then Exit;
  //
  DtHrCfgCla     := Geral.FDT(DModG.ObtemAgora(), 109);
  CacCod :=
    UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, CacCod);
  MovimCod :=
    UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Codigo :=
    UMyMod.BPGS1I32('vspaclacaba', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  if ImgTipo.SQLType = stIns then
    VS_CRC_PF.InsereVSCacCab(CacCod, emidClassArtXXUni, Codigo);
  //
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vspaclacaba', False, [
  'VSESCCab', CO_FLD_TAB_VMI, 'CacCod',
  'LstPal01', 'LstPal02', 'LstPal03',
  'LstPal04', 'LstPal05', 'LstPal06',
  'LstPal07', 'LstPal08', 'LstPal09',
  'LstPal10', 'LstPal11', 'LstPal12',
  'LstPal13', 'LstPal14', 'LstPal15'], [
  'Codigo'], [
  VSESCCab, VSMovIts, CacCod,
  LstPal01, LstPal02, LstPal03,
  LstPal04, LstPal05, LstPal06,
  LstPal07, LstPal08, LstPal09,
  LstPal10, LstPal11, LstPal12,
  LstPal13, LstPal14, LstPal15], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidClassArtXXUni, Codigo);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsesccab', False, [
    'DtHrCfgCla', 'CacCod'
    ], ['Codigo'], [
    DtHrCfgCla, CacCod
    ], [VSESCCab], True);
    //
    BxaGraGruX   := QrVSESCCabNewGraGruX.Value;
    BxaMovimNiv  := QrVSESCCabNewMovimNiv.Value;
    BxaSrcNivel1 := QrVSESCCabNewCodigo.Value;
    BxaSrcNivel2 := QrVSESCCabNewControle.Value;
    BxaMovimID   := QrVSESCCabNewMovimID.Value;
    //
    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      01, LstPal01, GraGruX01, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc01, CtrlDest01);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      02, LstPal02, GraGruX02, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc02, CtrlDest02);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      03, LstPal03, GraGruX03, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc03, CtrlDest03);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      04, LstPal04, GraGruX04, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc04, CtrlDest04);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      05, LstPal05, GraGruX05, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc05, CtrlDest05);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      06, LstPal06, GraGruX06, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc06, CtrlDest06);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      07, LstPal07, GraGruX07, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc07, CtrlDest07);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      08, LstPal08, GraGruX08, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc08, CtrlDest08);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      09, LstPal09, GraGruX09, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc09, CtrlDest09);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      10, LstPal10, GraGruX10, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc10, CtrlDest10);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      11, LstPal11, GraGruX11, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc11, CtrlDest11);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      12, LstPal12, GraGruX12, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc12, CtrlDest12);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      13, LstPal13, GraGruX13, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc13, CtrlDest13);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      14, LstPal14, GraGruX14, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc14, CtrlDest14);

    VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      15, LstPal15, GraGruX15, StqCenLoc, LaAviso1, LaAviso2, CtrlSorc15, CtrlDest15);

    //
    FCodigo := Codigo;
    FCacCod := CacCod;
    Close;
  end;
end;

procedure TFmVSClaPalPrpQnz.BtReclasifClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
  ReopenVSGerArtDst(0);
end;

procedure TFmVSClaPalPrpQnz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSClaPalPrpQnz.CadastraPallet(EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
const
  GraGruX = 0;
begin
  VS_CRC_PF.CadastraPalletRibCla(QrVSESCCabNewEmpresa.Value,
    QrVSESCCabNewClientMO.Value, EdPallet, CBPallet, QrVSPallet,
    emidClassArtXXUni, GraGruX);
  //ReopenVSPallet(QrVSPallet);
end;

procedure TFmVSClaPalPrpQnz.EdIMEIChange(Sender: TObject);
begin
(*
  if MeLeitura.Focused then
  begin
    EdIMEI.SetFocus;
    CBIMEI.SetFocus;
    if (EdIMEI.ValueVariant <> 0) and
    (QrVSESCCabNew.Locate('Controle', EdIMEI.ValueVariant, [])) then
    begin
      if SbIMEI.Enabled then
      begin
        SbIMEIClick(Self);
        if PnPallets.Visible then
        begin
          EdPallet1.SetFocus;
        end;
      end;
    end else
  end;
*)
end;

procedure TFmVSClaPalPrpQnz.EdIMEIRedefinido(Sender: TObject);
begin
  SbIMEI.Enabled := EdIMEI.ValueVariant > 0;
end;

procedure TFmVSClaPalPrpQnz.EdPalletChange(Sender: TObject);
begin
  if QrVSESCCabNew.Locate('Pallet', EdPallet.ValueVariant, []) then
  begin
    EdIMEI.ValueVariant := QrVSESCCabNewControle.Value;
    CBIMEI.KeyValue     := QrVSESCCabNewControle.Value;
  end;
end;

procedure TFmVSClaPalPrpQnz.EdPalletRedefinido(Sender: TObject);
begin
  if QrVSESCCabNew.Locate('Pallet', EdPallet.ValueVariant, []) then
  begin
    EdIMEI.ValueVariant := QrVSESCCabNewControle.Value;
    CBIMEI.KeyValue     := QrVSESCCabNewControle.Value;
  end else
  begin
    EdIMEI.ValueVariant := 0;
    CBIMEI.KeyValue     := Null;
  end;
end;

procedure TFmVSClaPalPrpQnz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClaPalPrpQnz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  //
  FEdPallet[01] := EdPallet01;
  FEdPallet[02] := EdPallet02;
  FEdPallet[03] := EdPallet03;
  FEdPallet[04] := EdPallet04;
  FEdPallet[05] := EdPallet05;
  FEdPallet[06] := EdPallet06;
  FEdPallet[07] := EdPallet07;
  FEdPallet[08] := EdPallet08;
  FEdPallet[09] := EdPallet09;
  FEdPallet[10] := EdPallet10;
  FEdPallet[11] := EdPallet11;
  FEdPallet[12] := EdPallet12;
  FEdPallet[13] := EdPallet13;
  FEdPallet[14] := EdPallet14;
  FEdPallet[15] := EdPallet15;
  //
  FCBPallet[01] := CBPallet01;
  FCBPallet[02] := CBPallet02;
  FCBPallet[03] := CBPallet03;
  FCBPallet[04] := CBPallet04;
  FCBPallet[05] := CBPallet05;
  FCBPallet[06] := CBPallet06;
  FCBPallet[07] := CBPallet07;
  FCBPallet[08] := CBPallet08;
  FCBPallet[09] := CBPallet09;
  FCBPallet[10] := CBPallet10;
  FCBPallet[11] := CBPallet11;
  FCBPallet[12] := CBPallet12;
  FCBPallet[13] := CBPallet13;
  FCBPallet[14] := CBPallet14;
  FCBPallet[15] := CBPallet15;
  //
  ReopenVSGerArtDst(0);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSClaPalPrpQnz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClaPalPrpQnz.LiberaConfig();
begin
  if (EdIMEI.ValueVariant <> 0) and
  (QrVSESCCabNew.Locate('Controle', EdIMEI.ValueVariant, [])) then
  begin
    if SbIMEI.Enabled then
    begin
      SbIMEIClick(Self);
      if PnPallets.Visible then
      begin
        EdPallet01.SetFocus;
      end;
    end;
  end;
end;

procedure TFmVSClaPalPrpQnz.MeLeituraChange(Sender: TObject);
var
  Leitura: String;
begin
  if MeLeitura.Lines.Count > 1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Processando dados da leitura');
      Leitura := MeLeitura.Lines[0];
      while Length(Leitura) < 13 do
      begin
        Leitura := '0' + Leitura;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, Leitura);
      MeLeitura.Text := Leitura;
      //
      EdIMEI.ValueVariant := 0;
      //
      if DmkPF.CheckSumEAN13(Leitura, True) then
      begin
        EdDVIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 1, 3));
        EdIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 4, 9));
        //
        LiberaConfig();
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVSClaPalPrpQnz.ReopenVSGerArtDst(Controle: Integer);
const
  Pallet = 0;
begin
  VS_CRC_PF.ReopenVSESCPalDst_ToClassPorPallet(QrVSESCCabNew, Pallet);
end;

procedure TFmVSClaPalPrpQnz.ReopenVSPallet(QrVSPallet: TmySQLQuery);
begin
  VS_CRC_PF.ReopenVSPallet(QrVSPallet, QrVSESCCabNewEmpresa.Value,
    QrVSESCCabNewClientMO.Value, 0, '', []);
end;

procedure TFmVSClaPalPrpQnz.SbPallet01Click(Sender: TObject);
begin
  CadastraPallet(EdPallet01, CBPallet01, QrVSPallet01);
end;

procedure TFmVSClaPalPrpQnz.SBPallet02Click(Sender: TObject);
begin
  CadastraPallet(EdPallet02, CBPallet02, QrVSPallet02);
end;

procedure TFmVSClaPalPrpQnz.SBPallet03Click(Sender: TObject);
begin
  CadastraPallet(EdPallet03, CBPallet03, QrVSPallet03);
end;

procedure TFmVSClaPalPrpQnz.SBPallet04Click(Sender: TObject);
begin
  CadastraPallet(EdPallet04, CBPallet04, QrVSPallet04);
end;

procedure TFmVSClaPalPrpQnz.SBPallet05Click(Sender: TObject);
begin
  CadastraPallet(EdPallet05, CBPallet05, QrVSPallet05);
end;

procedure TFmVSClaPalPrpQnz.SBPallet06Click(Sender: TObject);
begin
  CadastraPallet(EdPallet06, CBPallet06, QrVSPallet06);
end;

procedure TFmVSClaPalPrpQnz.SBPallet07Click(Sender: TObject);
begin
  CadastraPallet(EdPallet07, CBPallet07, QrVSPallet07);
end;

procedure TFmVSClaPalPrpQnz.SBPallet08Click(Sender: TObject);
begin
  CadastraPallet(EdPallet08, CBPallet08, QrVSPallet08);
end;

procedure TFmVSClaPalPrpQnz.SBPallet09Click(Sender: TObject);
begin
  CadastraPallet(EdPallet09, CBPallet09, QrVSPallet09);
end;

procedure TFmVSClaPalPrpQnz.SBPallet10Click(Sender: TObject);
begin
  CadastraPallet(EdPallet10, CBPallet10, QrVSPallet10);
end;

procedure TFmVSClaPalPrpQnz.SBPallet11Click(Sender: TObject);
begin
  CadastraPallet(EdPallet11, CBPallet11, QrVSPallet11);
end;

procedure TFmVSClaPalPrpQnz.SBPallet12Click(Sender: TObject);
begin
  CadastraPallet(EdPallet12, CBPallet12, QrVSPallet12);
end;

procedure TFmVSClaPalPrpQnz.SBPallet13Click(Sender: TObject);
begin
  CadastraPallet(EdPallet13, CBPallet13, QrVSPallet13);
end;

procedure TFmVSClaPalPrpQnz.SBPallet14Click(Sender: TObject);
begin
  CadastraPallet(EdPallet14, CBPallet14, QrVSPallet14);
end;

procedure TFmVSClaPalPrpQnz.SBPallet15Click(Sender: TObject);
begin
  CadastraPallet(EdPallet15, CBPallet15, QrVSPallet15);
end;

procedure TFmVSClaPalPrpQnz.SbIMEIClick(Sender: TObject);
begin
  if MyObjects.FIC(EdStqCenLoc.ValueVariant = 0, EdStqCenLoc,
    'Informe o local!') then
    Exit;
  //
  if MyObjects.FIC(EdIMEI.ValueVariant = 0, EdIMEI,
    'Informe o pallet de artigo de ribeira!') then
    Exit;
  //
  if DmkPF.DigitoVerificardorDmk3CasasInteger(EdIMEI.ValueVariant) = EdDVIMEI.ValueVariant then
  begin
    PnPartida.Enabled  := False;
    BtReclasif.Enabled := False; //Desabilitar para n�o zerar o IME-I quando j� tiver pallets
    PnPallets.Visible  := True;
    BtOK.Enabled       := True;
    //
    ReopenVSPallet(QrVSPallet01);
    ReopenVSPallet(QrVSPallet02);
    ReopenVSPallet(QrVSPallet03);
    ReopenVSPallet(QrVSPallet04);
    ReopenVSPallet(QrVSPallet05);
    ReopenVSPallet(QrVSPallet06);
    ReopenVSPallet(QrVSPallet07);
    ReopenVSPallet(QrVSPallet08);
    ReopenVSPallet(QrVSPallet09);
    ReopenVSPallet(QrVSPallet10);
    ReopenVSPallet(QrVSPallet11);
    ReopenVSPallet(QrVSPallet12);
    ReopenVSPallet(QrVSPallet13);
    ReopenVSPallet(QrVSPallet14);
    ReopenVSPallet(QrVSPallet15);
  end else
  begin
    Geral.MB_Aviso('DV do IME-I inv�lido!');
    EdDVIMEI.SetFocus;
  end;
end;

end.
