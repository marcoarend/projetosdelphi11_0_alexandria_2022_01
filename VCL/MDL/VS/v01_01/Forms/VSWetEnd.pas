unit VSWetEnd;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmVSWetEnd = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    PnNavi: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSWetEnd: TmySQLQuery;
    DsVSWetEnd: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSWetEndGraGruX: TIntegerField;
    QrVSWetEndLk: TIntegerField;
    QrVSWetEndDataCad: TDateField;
    QrVSWetEndDataAlt: TDateField;
    QrVSWetEndUserCad: TIntegerField;
    QrVSWetEndUserAlt: TIntegerField;
    QrVSWetEndAlterWeb: TSmallintField;
    QrVSWetEndAtivo: TSmallintField;
    QrVSWetEndGraGru1: TIntegerField;
    QrVSWetEndNO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    QrVSClaWet: TmySQLQuery;
    DsVSClaWet: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    QrVSClaWetGraGruX: TIntegerField;
    QrVSClaWetNO_PRD_TAM_COR: TWideStringField;
    QrVSClaWetCodigo: TIntegerField;
    QrVSClaWetVSRibCla: TIntegerField;
    QrVSClaWetLk: TIntegerField;
    QrVSClaWetDataCad: TDateField;
    QrVSClaWetDataAlt: TDateField;
    QrVSClaWetUserCad: TIntegerField;
    QrVSClaWetUserAlt: TIntegerField;
    QrVSClaWetAlterWeb: TSmallintField;
    QrVSClaWetAtivo: TSmallintField;
    Incluinovoartigoclassificado1: TMenuItem;
    AlteraArtigoClassificadoAtual1: TMenuItem;
    ExcluiArtigoClassificadoAtual1: TMenuItem;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    DsGraGruXCou: TDataSource;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    Label9: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSWetEndArtigoImp: TWideStringField;
    QrVSWetEndClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label11: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    EdMediaMinM2: TdmkEdit;
    Label13: TLabel;
    EdMediaMaxM2: TdmkEdit;
    Label14: TLabel;
    QrVSWetEndMediaMinM2: TFloatField;
    QrVSWetEndMediaMaxM2: TFloatField;
    Label17: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label18: TLabel;
    EdNome: TdmkEdit;
    DBEdit2: TDBEdit;
    QrVSClaWetVSWetEnd: TIntegerField;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    EdPrevAMPal: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    EdPrevKgPal: TdmkEdit;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    Label19: TLabel;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    Label20: TLabel;
    QrGraGruXCouGrandeza: TSmallintField;
    RGBastidao: TdmkRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSWetEndAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSWetEndBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrVSWetEndBeforeClose(DataSet: TDataSet);
    procedure QrVSWetEndAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure Incluinovoartigoclassificado1Click(Sender: TObject);
    procedure AlteraArtigoClassificadoAtual1Click(Sender: TObject);
    procedure ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSClaWet(SQLType: TSQLType);
    procedure ReopenVSClaWet(Codigo: Integer);

  public
    { Public declarations }
    FQryMul: TmySQLQuery;
    FSeq: Integer;
    //
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSWetEnd: TFmVSWetEnd;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSClaWet, UnVS_PF, AppListas, UnGrade_PF, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSWetEnd.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSWetEnd.MostraVSClaWet(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSClaWet, FmVSClaWet, afmoNegarComAviso) then
  begin
    FmVSClaWet.ImgTipo.SQLType := SQLType;
    FmVSClaWet.FQrCab := QrVSWetEnd;
    FmVSClaWet.FDsCab := DsVSWetEnd;
    FmVSClaWet.FQrIts := QrVSClaWet;
    //
    FmVSClaWet.LaVSWetEnd.Enabled := False;
    FmVSClaWet.EdVSRibCla.Enabled := False;
    FmVSClaWet.CBVSRibCla.Enabled := False;
    //
    FmVSClaWet.LaVSRibCla.Enabled := True;
    FmVSClaWet.EdVSRibCla.Enabled := True;
    FmVSClaWet.CBVSRibCla.Enabled := True;
    //

    FmVSClaWet.EdVSRibCla.ValueVariant := QrVSWetEndGraGruX.Value;
    FmVSClaWet.CBVSRibCla.KeyValue     := QrVSWetEndGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSClaWet.FCodigo := 0;
    end else
    begin
      FmVSClaWet.FCodigo := QrVSClaWetCodigo.Value;
      //
      FmVSClaWet.EdVSRibCla.ValueVariant := QrVSClaWetVSRibCla.Value;
      FmVSClaWet.CBVSRibCla.KeyValue     := QrVSClaWetVSRibCla.Value;
      //
    end;
    FmVSClaWet.ShowModal;
    FmVSClaWet.Destroy;
  end;
end;

procedure TFmVSWetEnd.PMArtigoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrVSWetEnd);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSClaWet);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSClaWet);
end;

procedure TFmVSWetEnd.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraArtigoClassificadoAtual1, QrVSWetEnd);
  //MyObjects.HabilitaMenuItemCabDel(ExcluiArtigoClassificadoAtual1, QrVSWetEnd);
end;

procedure TFmVSWetEnd.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSWetEndGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSWetEnd.DefParams;
begin
  VAR_GOTOTABELA := 'vswetend';
  VAR_GOTOMYSQLTABLE := QrVSWetEnd;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vswetend wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSWetEnd.ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
begin
//
end;

procedure TFmVSWetEnd.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de artigo selecionado?',
  'VSClaWet', 'Codigo', QrVSClaWetCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSClaWet,
      QrVSClaWetCodigo, QrVSClaWetCodigo.Value);
    ReopenVSClaWet(Codigo);
  end;
end;

procedure TFmVSWetEnd.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSWetEnd.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSWetEnd.ReopenVSClaWet(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSClaWet, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsclawet vna ',
  'LEFT JOIN vsribcla wmp ON wmp.GraGruX=vna.VSRibCla ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSWetEnd=' + Geral.FF0(QrVSWetEndGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSWetEnd.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSWetEnd.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSWetEnd.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSWetEnd.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSWetEnd.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSWetEnd.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSWetEnd.AlteraArtigoClassificadoAtual1Click(Sender: TObject);
begin
  EdMediaMinM2.ValueVariant := QrVSWetEndMediaMinM2.Value;
  EdMediaMaxM2.ValueVariant := QrVSWetEndMediaMaxM2.Value;
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSWetEndGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdCouNiv2, ImgTipo, 'gragruxcou');
end;

procedure TFmVSWetEnd.Alteralinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSClaWet(stUpd);
end;

procedure TFmVSWetEnd.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSWetEndGraGruX.Value;
  Close;
end;

procedure TFmVSWetEnd.BtConfirmaClick(Sender: TObject);
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, Bastidao: Integer;
  ArtigoImp, ClasseImp, Nome: String;
  MediaMinM2, MediaMaxM2, PrevAMPal, PrevKgPal: Double;
begin
  GraGruX        := QrVSWetEndGraGruX.Value;
  GraGru1        := QrVSWetEndGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.ValueVariant;
  ClasseImp      := EdClasseImp.ValueVariant;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  MediaMinM2     := EdMediaMinM2.ValueVariant;
  MediaMaxM2     := EdMediaMaxM2.ValueVariant;
  Nome           := EdNome.Text;
  //
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  if MyObjects.FIC(MediaMinM2 = 0, EdMediaMinM2, 'Informe a "M�dia m�n. m�"!') then
    Exit;
  if MyObjects.FIC(MediaMaxM2 = 0, EdMediaMaxM2, 'Informe a "M�dia m�x. m�"!') then
    Exit;
  if MyObjects.FIC(MediaMaxM2 <= MediaMinM2, EdMediaMaxM2,
  'A m�dia m�xima deve se superior a m�dia m�nima!') then
    Exit;
  //
  VS_PF.ReIncluiCouNivs(
    GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, Bastidao, PrevAMPal,
    PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
  if FQryMul <> nil then
  begin
    FQryMul.First;
    while not FQryMul.Eof do
    begin
      GraGruX := FQryMul.FieldByName('Controle').AsInteger;
      VS_PF.ReIncluiCouNivs(
        GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, Bastidao,
        PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
      //
      FQryMul.Next;
    end;
  end;
  //
  Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  if FSeq = 0 then
  begin
    LocCod(GraGruX, GraGruX);
  end else
    Close;
end;

procedure TFmVSWetEnd.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX        := QrVSWetEndGraGruX.Value;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'vswetend', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSWetEnd.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmVSWetEnd.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSWetEnd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  VS_PF.ConfiguraRGVSBastidao(RGBastidao, True(*Habilita*), (*Default*)-1);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmVSWetEnd.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSWetEndGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSWetEnd.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vswetend wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmVSWetEnd.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSWetEnd.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSWetEndGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSWetEnd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSWetEnd.QrVSWetEndAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSWetEnd.QrVSWetEndAfterScroll(DataSet: TDataSet);
begin
  ReopenVSClaWet(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSWetEndGraGruX.Value);
end;

procedure TFmVSWetEnd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSWetEnd.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSWetEndGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vswetend', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSWetEnd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSWetEnd.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSClaWet(stIns);
end;

procedure TFmVSWetEnd.Incluinovoartigoclassificado1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
(*
  DefinedSQL := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE %s ',
  'ORDER BY _Nome ',
  '']);
  FmMeuDBUses.PesquisaNome('', '', '', DefinedSQL);
  GraGruX := VAR_CADASTRO;
*)
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_5120_VSWetEnd);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      //ShowMessage(Geral.FF0(VAR_CADASTRO));
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vswetend ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vswetend', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True) then
        begin
          //VS_PF.DefineSubProd(GraGruX, 'vswetend');  'SubProd'  ???
        end;
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSWetEnd.QrVSWetEndBeforeClose(DataSet: TDataSet);
begin
  QrVSClaWet.Close;
  QrGraGruXCou.Close;
end;

procedure TFmVSWetEnd.QrVSWetEndBeforeOpen(DataSet: TDataSet);
begin
  QrVSWetEndGraGruX.DisplayFormat := FFormatFloat;
end;

end.

