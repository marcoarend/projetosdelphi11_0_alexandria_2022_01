object FmVSInfIEC: TFmVSInfIEC
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-198 :: IEC - Informa'#231#227'o de Entrada de Couros'
  ClientHeight = 629
  ClientWidth = 958
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 958
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 910
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 862
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 481
        Height = 32
        Caption = 'IEC - Informa'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 481
        Height = 32
        Caption = 'IEC - Informa'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 481
        Height = 32
        Caption = 'IEC - Informa'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 958
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 958
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 958
        Height = 467
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 954
          Height = 450
          Align = alClient
          DataSource = DsVSInfIEC
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSInfFol'
              Title.Caption = 'Folha'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSInfLin'
              Title.Caption = 'Linha'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fornecedor'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Fornece'
              Title.Caption = 'Nome do fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerFch'
              Title.Caption = 'S'#233'rie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerFch'
              Title.Caption = 'Nome s'#233'rie'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo / tamanho / cor'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 958
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 954
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 958
    Height = 70
    Align = alBottom
    TabOrder = 3
    Visible = False
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 812
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 460
    Top = 443
  end
  object TbVSInfIEC: TmySQLTable
    Database = Dmod.MyDB
    BeforePost = TbVSInfIECBeforePost
    SortFieldNames = 'Codigo DESC'
    TableName = 'vsinfiec'
    Left = 500
    Top = 204
    object TbVSInfIECCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbVSInfIECVSInfFol: TIntegerField
      FieldName = 'VSInfFol'
      Required = True
    end
    object TbVSInfIECVSInfLin: TIntegerField
      FieldName = 'VSInfLin'
      Required = True
    end
    object TbVSInfIECData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object TbVSInfIECNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object TbVSInfIECFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object TbVSInfIECSerFch: TIntegerField
      FieldName = 'SerFch'
      Required = True
    end
    object TbVSInfIECFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object TbVSInfIECPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object TbVSInfIECGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object TbVSInfIECPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object TbVSInfIECPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object TbVSInfIECAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbVSInfIECAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbVSInfIECLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbVSInfIECDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbVSInfIECDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbVSInfIECUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbVSInfIECUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbVSInfIECAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbVSInfIECAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbVSInfIECNO_Fornece: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_Fornece'
      LookupDataSet = QrFornecedor
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOMEENTIDADE'
      KeyFields = 'Fornecedor'
      Size = 100
      Lookup = True
    end
    object TbVSInfIECNO_SerFch: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_SerFch'
      LookupDataSet = QrVSSerFch
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'SerFch'
      Size = 60
      Lookup = True
    end
    object TbVSInfIECNO_PRD_TAM_COR: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_PRD_TAM_COR'
      LookupDataSet = QrGraGruX
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 255
      Lookup = True
    end
  end
  object DsVSInfIEC: TDataSource
    DataSet = TbVSInfIEC
    Left = 500
    Top = 252
  end
  object QrVSInfIEC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 204
    Top = 244
    object QrVSInfIECCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSInfIECVSInfFol: TIntegerField
      FieldName = 'VSInfFol'
      Required = True
    end
    object QrVSInfIECVSInfLin: TIntegerField
      FieldName = 'VSInfLin'
      Required = True
    end
    object QrVSInfIECData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrVSInfIECNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrVSInfIECFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrVSInfIECSerFch: TIntegerField
      FieldName = 'SerFch'
      Required = True
    end
    object QrVSInfIECFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSInfIECPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSInfIECGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSInfIECPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVSInfIECPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrVSInfIECAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVSInfIECAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrVSInfIECLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSInfIECDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSInfIECDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSInfIECUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSInfIECUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSInfIECAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrVSInfIECAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 628
    Top = 224
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 628
    Top = 272
  end
  object QrVSSerFch: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 700
    Top = 224
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 700
    Top = 272
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 224
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 404
    Top = 268
  end
end
