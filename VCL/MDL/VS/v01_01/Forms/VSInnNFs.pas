unit VSInnNFs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmVSInnNFs = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    CBMotorista: TdmkDBLookupComboBox;
    EdMotorista: TdmkEditCB;
    Label1: TLabel;
    SBMotorista: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdCodigo: TdmkEdit;
    EdControle: TdmkEdit;
    Label2: TLabel;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    Edemi_serie: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    Edemi_nNF: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBMotoristaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSInnNFs: TFmVSInnNFs;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, VSInnCab;

{$R *.DFM}

procedure TFmVSInnNFs.BtOKClick(Sender: TObject);
var
  Placa: String;
  Codigo, Controle, Conta, Motorista, ide_serie, ide_nNF, emi_serie, emi_nNF: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Conta          := EdConta.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := 0;
  AreaP2         := 0;
  ValorT         := EdValorT.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_Serie      := Edide_Serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_Serie      := Edemi_Serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
  //
  if MyObjects.FIC(ide_nNF = 0, Edide_nNF, 'Informe a NF-e!') then
    Exit;
  if MyObjects.FIC(Pecas = 0, EdPecas, 'Informe a quantidade de pe�as!') then
    Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Informe o peso (kg)!') then
    Exit;
  Conta := UMyMod.BPGS1I32('vsinnnfs', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinnnfs', False, [
  'Codigo', 'Controle', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'ValorT', 'Motorista', 'Placa',
  'ide_serie', 'ide_nNF', 'emi_serie',
  'emi_nNF'], [
  'Conta'], [
  Codigo, Controle, Pecas,
  PesoKg, AreaM2, AreaP2,
  ValorT, Motorista, Placa,
  ide_serie, ide_nNF, emi_serie,
  emi_nNF], [
  Conta], True) then
  begin
    FmVSInnCab.AtualizaSaldoIts(Controle);
    FmVSInnCab.ReopenVSInnIts(Controle);
    FmVSInnCab.ReopenVSInnNFs(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdConta.ValueVariant     := 0;
      EdPecas.ValueVariant     := 0;
      EdPesoKg.ValueVariant    := 0;
      EdValorT.ValueVariant    := 0;
      EdMotorista.ValueVariant     := 0;
      CBMotorista.KeyValue         := Null;
      EdPlaca.ValueVariant     := 0;
      //Edide_serie.ValueVariant := 0;
      Edide_nNF.ValueVariant   := Edide_nNF.ValueVariant + 1;
      //Edemi_serie.ValueVariant := 0;
      Edemi_nNF.ValueVariant   := Edemi_nNF.ValueVariant + 1;
      //
      EdPecas.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSInnNFs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnNFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnNFs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
end;

procedure TFmVSInnNFs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInnNFs.SBMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

end.
