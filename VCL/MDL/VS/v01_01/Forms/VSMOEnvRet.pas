unit VSMOEnvRet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc, mySQLDbTables, UnProjGroup_Vars, dmkDBLookupComboBox, dmkEditCB,
  dmkDBGridZTO, Vcl.Menus, dmkEditDateTimePicker, dmkMemo;

type
  TFmVSMOEnvRet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdNFRMP_FatID: TdmkEdit;
    EdNFRMP_FatNum: TdmkEdit;
    EdNFRMP_Empresa: TdmkEdit;
    EdNFRMP_SerNF: TdmkEdit;
    Label9: TLabel;
    EdNFRMP_nNF: TdmkEdit;
    Label12: TLabel;
    EdNFRMP_nItem: TdmkEdit;
    Label13: TLabel;
    LaPecas: TLabel;
    EdNFRMP_Pecas: TdmkEdit;
    EdNFRMP_AreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdNFRMP_AreaP2: TdmkEditCalc;
    EdNFRMP_PesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label14: TLabel;
    EdNFRMP_ValorT: TdmkEdit;
    SbRMP: TSpeedButton;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    SbCMO: TSpeedButton;
    EdNFCMO_FatID: TdmkEdit;
    EdNFCMO_FatNum: TdmkEdit;
    EdNFCMO_Empresa: TdmkEdit;
    EdNFCMO_SerNF: TdmkEdit;
    EdNFCMO_nNF: TdmkEdit;
    EdNFCMO_nItem: TdmkEdit;
    EdNFCMO_Pecas: TdmkEdit;
    EdNFCMO_AreaM2: TdmkEditCalc;
    EdNFCMO_AreaP2: TdmkEditCalc;
    EdNFCMO_PesoKg: TdmkEdit;
    EdNFCMO_ValorT: TdmkEdit;
    QrCabA: TmySQLQuery;
    DsCabA: TDataSource;
    EdNFCMO_CusMOKG: TdmkEdit;
    Label28: TLabel;
    EdNFCMO_CusMOM2: TdmkEdit;
    Label29: TLabel;
    BtMONaoCobr: TBitBtn;
    BtContinoaCobranca: TBitBtn;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    SbCF: TSpeedButton;
    Label43: TLabel;
    Label44: TLabel;
    EdCFTPA_FatID: TdmkEdit;
    EdCFTPA_FatNum: TdmkEdit;
    EdCFTPA_Empresa: TdmkEdit;
    EdCFTPA_SerCT: TdmkEdit;
    EdCFTPA_nCT: TdmkEdit;
    EdCFTPA_nItem: TdmkEdit;
    EdCFTPA_Pecas: TdmkEdit;
    EdCFTPA_AreaM2: TdmkEditCalc;
    EdCFTPA_AreaP2: TdmkEditCalc;
    EdCFTPA_PesoKg: TdmkEdit;
    EdCFTPA_ValorT: TdmkEdit;
    EdCFTPA_PesTrKg: TdmkEdit;
    EdCFTPA_CusTrKg: TdmkEdit;
    QrTransporta: TmySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    DsTransporta: TDataSource;
    Label45: TLabel;
    EdCFTPA_Terceiro: TdmkEditCB;
    CBCFTPA_Terceiro: TdmkDBLookupComboBox;
    EdNFRMP_Terceiro: TdmkEditCB;
    Label46: TLabel;
    CBNFRMP_Terceiro: TdmkDBLookupComboBox;
    QrPrestador1: TmySQLQuery;
    QrPrestador1Codigo: TIntegerField;
    QrPrestador1NOMEENTIDADE: TWideStringField;
    DsPrestador1: TDataSource;
    QrPrestador2: TmySQLQuery;
    QrPrestador2Codigo: TIntegerField;
    QrPrestador2NOMEENTIDADE: TWideStringField;
    DsPrestador2: TDataSource;
    EdNFCMO_Terceiro: TdmkEditCB;
    Label31: TLabel;
    CBNFCMO_Terceiro: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    BtItem: TBitBtn;
    Panel13: TPanel;
    BtProntosRetornados: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    QrVSMOEnvGVmi: TmySQLQuery;
    QrVSMOEnvGVmiControle: TIntegerField;
    QrVSMOEnvGVmiGraGruX: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiNO_PRDA_TAM_COR: TWideStringField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiValorT: TFloatField;
    QrVSMOEnvGVmiIDItem: TIntegerField;
    DsVSMOEnvGVmi: TDataSource;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    QrVSMOEnvRVmi: TmySQLQuery;
    DsVSMOEnvRVmi: TDataSource;
    Splitter1: TSplitter;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiNFeSer: TIntegerField;
    QrVSMOEnvRVmiNFeNum: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorT: TFloatField;
    QrVSMOEnvRVmiIDItem: TIntegerField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrSumG: TmySQLQuery;
    QrSumGPecas: TFloatField;
    QrSumGPesoKg: TFloatField;
    QrSumGAreaM2: TFloatField;
    QrSumGAreaP2: TFloatField;
    QrSumGAreaKg: TFloatField;
    QrSumGValorT: TFloatField;
    QrSumR: TmySQLQuery;
    QrSumRPecas: TFloatField;
    QrSumRPesoKg: TFloatField;
    QrSumRAreaM2: TFloatField;
    QrSumRAreaP2: TFloatField;
    QrSumRAreaKg: TFloatField;
    QrSumRValorT: TFloatField;
    QrEnvR: TmySQLQuery;
    QrEnvRVSMOEnvEnv: TIntegerField;
    QrVSMOEnvGVmiMovimCod: TIntegerField;
    PMProntosRetornados: TPopupMenu;
    Incluiitemprontoretornado1: TMenuItem;
    Alteraitemprontoretornadoselecionado1: TMenuItem;
    Excluiitemprontoretornadoselecionado1: TMenuItem;
    Label4: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    LaHora: TLabel;
    Label3: TLabel;
    MeNome: TdmkMemo;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbRMPClick(Sender: TObject);
    procedure SbCMOClick(Sender: TObject);
    procedure BtMONaoCobrClick(Sender: TObject);
    procedure BtContinoaCobrancaClick(Sender: TObject);
    procedure EdCFTPA_PesTrKgRedefinido(Sender: TObject);
    procedure EdCFTPA_CusTrKgRedefinido(Sender: TObject);
    procedure EdNFCMO_AreaM2Redefinido(Sender: TObject);
    procedure EdNFCMO_PesoKgRedefinido(Sender: TObject);
    procedure EdNFCMO_CusMOKGRedefinido(Sender: TObject);
    procedure EdNFCMO_CusMOM2Redefinido(Sender: TObject);
    procedure BtProntosRetornadosClick(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure QrVSMOEnvRVmiAfterOpen(DataSet: TDataSet);
    procedure QrVSMOEnvGVmiAfterOpen(DataSet: TDataSet);
    procedure Alteraitemprontoretornadoselecionado1Click(Sender: TObject);
    procedure Incluiitemprontoretornado1Click(Sender: TObject);
    procedure Excluiitemprontoretornadoselecionado1Click(Sender: TObject);
    procedure EdCFTPA_nCTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    function  IncluiVSMOEnvGVMIs(): Boolean;
    function  IncluiVSMOEnvRVMIs(): Boolean;
    procedure PesquisaNFe(EdFatID, EdFatNum, EdEmpresa, EdTerceiro, EdSerNF,
              EdnNF: TdmkEdit);
    procedure CustoFretePeloPeso();
    procedure ValorTotal();
  public
    { Public declarations }
    FTabVMIQtdEnvGVMI, FTabVMIQtdEnvRVMI: String;
    FListaEnvEnv: TMyGrlArrInt;
    FCodigo: Integer;
    //
    procedure IncrementaListaEnvEnv();
    procedure ReopenVSMOEnvGVMI(Controle: Integer);
    procedure ReopenVSMOEnvRVMI(VSMOEnvEnv: Integer);
  end;
  var
  FmVSMOEnvRet: TFmVSMOEnvRet;

implementation

uses UnMyObjects, UMySQLModule, Module, MyDBCheck, DmkDAC_PF, NFe_PF, ModVS_CRC,
  ModuleGeral, VSMOEnvGVMI, VSMOEnvRVMI, UnDmkProcFunc;

{$R *.DFM}

procedure TFmVSMOEnvRet.BtProntosRetornadosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProntosRetornados, BtProntosRetornados);
 //
end;

procedure TFmVSMOEnvRet.Alteraitemprontoretornadoselecionado1Click(
  Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrVSMOEnvGVMIControle.Value;
  if DBCheck.CriaFm(TFmVSMOEnvGVMI, FmVSMOEnvGVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvGVMI.ImgTipo.SQLType  := stUpd;
    FmVSMOEnvGVMI.FTabVMIQtdEnvGVMI := FTabVMIQtdEnvGVMI;
    FmVSMOEnvGVMI.EdControle.ValueVariant := Controle;
    FmVSMOEnvGVMI.EdGraGruX.ValueVariant  := QrVSMOEnvGVMIGraGruX.Value;
    FmVSMOEnvGVMI.EdNO_PRD_TAM_COR.Text   := QrVSMOEnvGVMINO_PRDA_TAM_COR.Value;
    FmVSMOEnvGVMI.EdPecas.ValueVariant    := QrVSMOEnvGVMIPecas.Value;
    FmVSMOEnvGVMI.EdPesoKg.ValueVariant   := QrVSMOEnvGVMIPesoKg.Value;
    FmVSMOEnvGVMI.EdAreaM2.ValueVariant   := QrVSMOEnvGVMIAreaM2.Value;
    FmVSMOEnvGVMI.EdAreaP2.ValueVariant   := QrVSMOEnvGVMIAreaP2.Value;
    //
    FmVSMOEnvGVMI.ShowModal;
    //
    FmVSMOEnvGVMI.Destroy;
    //
    ReopenVSMOEnvGVMI(Controle);
  end;
end;

procedure TFmVSMOEnvRet.BtContinoaCobrancaClick(Sender: TObject);
begin
  EdNFCMO_SerNF.ValueVariant := VAR_NFCMO_SerNF;
  EdNFCMO_nNF.ValueVariant   := VAR_NFCMO_nNF;
  EdNFRMP_SerNF.ValueVariant := VAR_NFRMP_SerNF;
  EdNFRMP_nNF.ValueVariant   := VAR_NFRMP_nNF;
  if VAR_NFRMP_nNF > 0 then
    SbRMPClick(Self);
  if VAR_NFCMO_nNF > 0 then
    SbCMOClick(Self);
end;

procedure TFmVSMOEnvRet.BtItemClick(Sender: TObject);
var
  VSMOEnvEnv: Integer;
begin
  VSMOEnvEnv := QrVSMOEnvRVmiVSMOEnvEnv.Value;
  if DBCheck.CriaFm(TFmVSMOEnvRVMI, FmVSMOEnvRVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvRVMI.ImgTipo.SQLType            := stUpd;
    FmVSMOEnvRVMI.FTabVMIQtdEnvRVMI          := FTabVMIQtdEnvRVMI;
    //
    FmVSMOEnvRVMI.EdVSMOEnvEnv.ValueVariant  := VSMOEnvEnv;
    FmVSMOEnvRVMI.EdNFeSer.ValueVariant      := QrVSMOEnvRVmiNFeSer.Value;
    FmVSMOEnvRVMI.EdNFeNum.ValueVariant      := QrVSMOEnvRVmiNFeNum.Value;

    FmVSMOEnvRVMI.EdPecas.ValueVariant       := QrVSMOEnvRVMIPecas.Value;
    FmVSMOEnvRVMI.EdPesoKg.ValueVariant      := QrVSMOEnvRVMIPesoKg.Value;
    FmVSMOEnvRVMI.EdAreaM2.ValueVariant      := QrVSMOEnvRVMIAreaM2.Value;
    FmVSMOEnvRVMI.EdAreaP2.ValueVariant      := QrVSMOEnvRVMIAreaP2.Value;
    FmVSMOEnvRVMI.EdValorT.ValueVariant      := QrVSMOEnvRVMIValorT.Value;
    //
    FmVSMOEnvRVMI.ShowModal;
    //
    FmVSMOEnvRVMI.Destroy;
    //
    ReopenVSMOEnvRVMI(VSMOEnvEnv);
  end;
end;

procedure TFmVSMOEnvRet.BtMONaoCobrClick(Sender: TObject);
begin
  EdNFRMP_FatID.ValueVariant  := -1;
  EdNFRMP_FatNum.ValueVariant := -1;
  EdNFRMP_nNF.ValueVariant    := -1;
  //
  EdNFCMO_FatID.ValueVariant  := -1;
  EdNFCMO_FatNum.ValueVariant := -1;
  EdNFCMO_nNF.ValueVariant    := -1;
  //
  BtOKClick(Self);
end;

procedure TFmVSMOEnvRet.BtOKClick(Sender: TObject);
var
  Codigo, NFCMO_FatID, NFCMO_FatNum, NFCMO_Empresa, NFCMO_Terceiro, NFCMO_nItem,
  NFCMO_SerNF, NFCMO_nNF, NFRMP_FatID, NFRMP_FatNum, NFRMP_Empresa,
  NFRMP_Terceiro, NFRMP_nItem, NFRMP_SerNF, NFRMP_nNF: Integer;
(*
  VSVMI_Controle,
  VSVMI_Codigo, VSVMI_MovimID, VSVMI_MovimNiv, VSVMI_MovimCod, VSVMI_Empresa,
  VSVMI_SerNF, VSVMI_nNF: Integer;
*)
  NFCMO_Pecas, NFCMO_PesoKg, NFCMO_AreaM2, NFCMO_AreaP2, NFCMO_ValorT,
  NFRMP_Pecas, NFRMP_PesoKg, NFRMP_AreaM2, NFRMP_AreaP2, NFRMP_ValorT,
  NFCMO_CusMOM2, NFCMO_CusMOKG: Double;
  SQLType: TSQLType;
  //
  CFTPA_FatID, CFTPA_FatNum, CFTPA_Empresa, CFTPA_Terceiro, CFTPA_nItem,
  CFTPA_SerCT, CFTPA_nCT: Integer;
(*
  VSMOEnvEnv, VSSrc_Controle, VSSrc_Codigo,
  VSSrc_MovimID, VSSrc_MovimNiv, VSSrc_MovimCod, VSSrc_Empresa: Integer;
*)
  CFTPA_Pecas, CFTPA_PesoKg, CFTPA_AreaM2, CFTPA_AreaP2, CFTPA_PesTrKg,
  CFTPA_CusTrKg, CFTPA_ValorT: Double;
  Nome, DataHora: String;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := dmkPF.LimpaSeSohCRLF(MeNome.Text);
  NFCMO_FatID    := EdNFCMO_FatID.ValueVariant;
  NFCMO_FatNum   := EdNFCMO_FatNum.ValueVariant;
  NFCMO_Empresa  := EdNFCMO_Empresa.ValueVariant;
  NFCMO_Terceiro := EdNFCMO_Terceiro.ValueVariant;
  NFCMO_nItem    := EdNFCMO_nItem.ValueVariant;
  NFCMO_SerNF    := EdNFCMO_SerNF.ValueVariant;
  NFCMO_nNF      := EdNFCMO_nNF.ValueVariant;
  NFCMO_Pecas    := EdNFCMO_Pecas.ValueVariant;
  NFCMO_PesoKg   := EdNFCMO_PesoKg.ValueVariant;
  NFCMO_AreaM2   := EdNFCMO_AreaM2.ValueVariant;
  NFCMO_AreaP2   := EdNFCMO_AreaP2.ValueVariant;
  NFCMO_ValorT   := EdNFCMO_ValorT.ValueVariant;
  NFRMP_FatID    := EdNFRMP_FatID.ValueVariant;
  NFRMP_FatNum   := EdNFRMP_FatNum.ValueVariant;
  NFRMP_Empresa  := EdNFRMP_Empresa.ValueVariant;
  NFRMP_Terceiro := EdNFRMP_Terceiro.ValueVariant;
  NFRMP_nItem    := EdNFRMP_nItem.ValueVariant;
  NFRMP_SerNF    := EdNFRMP_SerNF.ValueVariant;
  NFRMP_nNF      := EdNFRMP_nNF.ValueVariant;
  NFRMP_Pecas    := EdNFRMP_Pecas.ValueVariant;
  NFRMP_PesoKg   := EdNFRMP_PesoKg.ValueVariant;
  NFRMP_AreaM2   := EdNFRMP_AreaM2.ValueVariant;
  NFRMP_AreaP2   := EdNFRMP_AreaP2.ValueVariant;
  NFRMP_ValorT   := EdNFRMP_ValorT.ValueVariant;
(*
  VSVMI_Controle := EdVSVMI_Controle.ValueVariant;
  VSVMI_Codigo   := EdVSVMI_Codigo.ValueVariant;
  VSVMI_MovimID  := EdVSVMI_MovimID.ValueVariant;
  VSVMI_MovimNiv := EdVSVMI_MovimNiv.ValueVariant;
  VSVMI_MovimCod := EdVSVMI_MovimCod.ValueVariant;
  VSVMI_Empresa  := EdVSVMI_Empresa.ValueVariant;
  VSVMI_SerNF    := EdVSVMI_SerNF.ValueVariant;
  VSVMI_nNF      := EdVSVMI_nNF.ValueVariant;
*)
  NFCMO_CusMOM2  := EdNFCMO_CusMOM2.ValueVariant;
  NFCMO_CusMOKG  := EdNFCMO_CusMOKG.ValueVariant;
  //
  CFTPA_FatID    := EdCFTPA_FatID.ValueVariant;
  CFTPA_FatNum   := EdCFTPA_FatNum.ValueVariant;
  CFTPA_Empresa  := EdCFTPA_Empresa.ValueVariant;
  CFTPA_Terceiro := EdCFTPA_Terceiro.ValueVariant;
  CFTPA_nItem    := EdCFTPA_nItem.ValueVariant;
  CFTPA_SerCT    := EdCFTPA_SerCT.ValueVariant;
  CFTPA_nCT      := EdCFTPA_nCT.ValueVariant;
  CFTPA_Pecas    := EdCFTPA_Pecas.ValueVariant;
  CFTPA_PesoKg   := EdCFTPA_PesoKg.ValueVariant;
  CFTPA_AreaM2   := EdCFTPA_AreaM2.ValueVariant;
  CFTPA_AreaP2   := EdCFTPA_AreaP2.ValueVariant;
  CFTPA_PesTrKg  := EdCFTPA_PesTrKg.ValueVariant;
  CFTPA_CusTrKg  := EdCFTPA_CusTrKg.ValueVariant;
  CFTPA_ValorT   := EdCFTPA_ValorT.ValueVariant;
(*
  VSMOEnvEnv     := EdVSMOEnvEnv.ValueVariant;
  //
  VSSrc_Controle := EdVSSrc_Controle.ValueVariant;
  VSSrc_Codigo   := EdVSSrc_Codigo.ValueVariant;
  VSSrc_MovimID  := EdVSSrc_MovimID.ValueVariant;
  VSSrc_MovimNiv := EdVSSrc_MovimNiv.ValueVariant;
  VSSrc_MovimCod := EdVSSrc_MovimCod.ValueVariant;
  VSSrc_Empresa  := EdVSSrc_Empresa.ValueVariant;
*)
  //
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  //
  if MyObjects.FIC(TPData.Date < 2, TPData, 'Data/hora n�o definida!')
  //if MyObjects.FIC(VSVMI_SerNF = 0, EdVSVMI_SerNF, 'Informe a s�rie da NF m�e do envio da mat�ria-prima!')
  //or MyObjects.FIC(VSVMI_nNF = 0, EdVSVMI_nNF, 'Informe o n�mero da NF m�e do envio da mat�ria-prima!')
  or MyObjects.FIC(NFRMP_Empresa = 0, EdNFRMP_Empresa, 'Informe a empresa do retorno da mat�ria-prima!')
  or MyObjects.FIC(NFRMP_Terceiro = 0, EdNFRMP_Terceiro, 'Informe o Fornecedor de MO!')
  or MyObjects.FIC(NFRMP_FatID = 0, EdNFRMP_FatID, 'Informe o ID do retorno da mat�ria-prima!')
  or MyObjects.FIC(NFRMP_FatNum = 0, EdNFRMP_FatNum, 'Informe o c�digo do retorno da mat�ria-prima!')
  //or MyObjects.FIC(NFRMP_SerNF = 0, EdNFRMP_SerNF, 'Informe a s�rie da NF do retorno da mat�ria-prima!')
  or MyObjects.FIC(NFRMP_nNF = 0, EdNFRMP_nNF, 'Informe o n�mero da NF do retorno da mat�ria-prima!')
  //
  or MyObjects.FIC(NFCMO_Empresa = 0, EdNFCMO_Empresa, 'Informe a empresa da cobran�a de MO!')
  or MyObjects.FIC(NFCMO_FatID = 0, EdNFCMO_FatID, 'Informe o ID da cobran�a de MO!')
  or MyObjects.FIC(NFCMO_FatNum = 0, EdNFCMO_FatNum, 'Informe o c�digo da cobran�a de MO!')
  //or MyObjects.FIC(NFCMO_SerNF = 0, EdNFCMO_SerNF, 'Informe a s�rie da NF da cobran�a de MO!')
  or MyObjects.FIC(NFCMO_nNF = 0, EdNFCMO_nNF, 'Informe o n�mero da NF da cobran�a de MO!')
  //
  //or MyObjects.FIC(CFTPA_SerCT = 0, EdCFTPA_SerCT, 'Informe a s�rie do CT do envio do couro!')
  or MyObjects.FIC(CFTPA_nCT = 0, EdCFTPA_nCT, 'Informe o n�mero do CT do envio do couro!')
  or MyObjects.FIC(CFTPA_Empresa = 0, EdCFTPA_Empresa, 'Informe a empresa para o frete do couro!')
  or MyObjects.FIC(CFTPA_FatID = 0, EdCFTPA_FatID, 'Informe o ID do frete do couro!')
  or MyObjects.FIC(CFTPA_FatNum = 0, EdCFTPA_FatNum, 'Informe o c�digo do frete do couro!')
  or MyObjects.FIC(CFTPA_Terceiro = 0, EdCFTPA_Terceiro, 'Informe o transportador do couro!')
  then Exit;
  //
  if Codigo = 0 then
  begin
    Codigo := UMyMod.BPGS1I32('vsmoenvret', 'Codigo', '', '', tsPos, SQLType, Codigo);
    EdCodigo.ValueVariant := Codigo;
  end;
  //
  if not IncluiVSMOEnvGVMIs() then exit;
  if not IncluiVSMOEnvRVMIs() then exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmoenvret', False, [
  'Nome', 'DataHora',
  'NFCMO_FatID', 'NFCMO_FatNum', 'NFCMO_Empresa',
  'NFCMO_Terceiro', 'NFCMO_nItem', 'NFCMO_SerNF',
  'NFCMO_nNF', 'NFCMO_Pecas', 'NFCMO_PesoKg',
  'NFCMO_AreaM2', 'NFCMO_AreaP2', 'NFCMO_CusMOM2',
  'NFCMO_CusMOKG', 'NFCMO_ValorT', 'NFRMP_FatID',
  'NFRMP_FatNum', 'NFRMP_Empresa', 'NFRMP_Terceiro',
  'NFRMP_nItem', 'NFRMP_SerNF', 'NFRMP_nNF',
  'NFRMP_Pecas', 'NFRMP_PesoKg', 'NFRMP_AreaM2',
  'NFRMP_AreaP2', 'NFRMP_ValorT',
(*
  'VSVMI_Controle',
  'VSVMI_Codigo', 'VSVMI_MovimID', 'VSVMI_MovimNiv',
  'VSVMI_MovimCod', 'VSVMI_Empresa', 'VSVMI_SerNF',
  'VSVMI_nNF',
*)
  'CFTPA_FatID', 'CFTPA_FatNum',
  'CFTPA_Empresa', 'CFTPA_Terceiro', 'CFTPA_nItem',
  'CFTPA_Pecas', 'CFTPA_PesoKg', 'CFTPA_AreaM2',
  'CFTPA_AreaP2', 'CFTPA_PesTrKg', 'CFTPA_CusTrKg',
  'CFTPA_ValorT',
(*
  'VSMOEnvEnv', 'VSSrc_Controle',
  'VSSrc_Codigo', 'VSSrc_MovimID', 'VSSrc_MovimNiv',
  'VSSrc_MovimCod', 'VSSrc_Empresa',
*)
  'CFTPA_SerCT',
  'CFTPA_nCT'], [
  'Codigo'], [
  Nome, DataHora,
  NFCMO_FatID, NFCMO_FatNum, NFCMO_Empresa,
  NFCMO_Terceiro, NFCMO_nItem, NFCMO_SerNF,
  NFCMO_nNF, NFCMO_Pecas, NFCMO_PesoKg,
  NFCMO_AreaM2, NFCMO_AreaP2, NFCMO_CusMOM2,
  NFCMO_CusMOKG, NFCMO_ValorT, NFRMP_FatID,
  NFRMP_FatNum, NFRMP_Empresa, NFRMP_Terceiro,
  NFRMP_nItem, NFRMP_SerNF, NFRMP_nNF,
  NFRMP_Pecas, NFRMP_PesoKg, NFRMP_AreaM2,
  NFRMP_AreaP2, NFRMP_ValorT,
(*
  VSVMI_Controle,
  VSVMI_Codigo, VSVMI_MovimID, VSVMI_MovimNiv,
  VSVMI_MovimCod, VSVMI_Empresa, VSVMI_SerNF,
  VSVMI_nNF,
*)
  CFTPA_FatID, CFTPA_FatNum,
  CFTPA_Empresa, CFTPA_Terceiro, CFTPA_nItem,
  CFTPA_Pecas, CFTPA_PesoKg, CFTPA_AreaM2,
  CFTPA_AreaP2, CFTPA_PesTrKg, CFTPA_CusTrKg,
  CFTPA_ValorT,
(*
  VSMOEnvEnv, VSSrc_Controle,
  VSSrc_Codigo, VSSrc_MovimID, VSSrc_MovimNiv,
  VSSrc_MovimCod, VSSrc_Empresa,
*)
  CFTPA_SerCT,
  CFTPA_nCT], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    VAR_NFCMO_SerNF := NFCMO_SerNF;
    VAR_NFRMP_SerNF := NFRMP_SerNF;
    VAR_NFCMO_nNF   := NFCMO_nNF;
    VAR_NFRMP_nNF   := NFRMP_nNF;
    //
    IncrementaListaEnvEnv();
    //
    DmModVS_CRC.AtualizaSaldoVSMOEnvRet(FListaEnvEnv);
    //
    //DmModVS_CRC.CalculaFreteVMIdeMOEnvRet(?);
    //
    Close;
  end;
end;

procedure TFmVSMOEnvRet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvRet.CustoFretePeloPeso();
var
  CFTPA_CusTrKg, CFTPA_PesTrKg: Double;
begin
  CFTPA_CusTrKg := EdCFTPA_CusTrKg.ValueVariant;
  CFTPA_PesTrKg := EdCFTPA_PesTrKg.ValueVariant;
  if (CFTPA_CusTrKg >= 0.000001) and (CFTPA_PesTrKg >= 0.001) then
  begin
    //EdNFCMO_CusMOM2.ValueVariant := 0;
    EdCFTPA_ValorT.ValueVariant := CFTPA_PesTrKg * CFTPA_CusTrKg;
  end;
end;

procedure TFmVSMOEnvRet.EdCFTPA_CusTrKgRedefinido(Sender: TObject);
begin
  CustoFretePeloPeso();
end;

procedure TFmVSMOEnvRet.EdCFTPA_nCTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nCTe: Integer;
begin
  if Key = VK_F4 then
  begin
    EdCFTPA_nCT.ValueVariant := DmModVS_CRC.ObtemProximoCTe(
      EdCFTPA_Terceiro.ValueVariant, EdCFTPA_SerCT.ValueVariant);
    if nCTe > 0 then
      EdCFTPA_nCT.ValueVariant := nCTe;
  end;
end;

procedure TFmVSMOEnvRet.EdCFTPA_PesTrKgRedefinido(Sender: TObject);
begin
  CustoFretePeloPeso();
end;

procedure TFmVSMOEnvRet.EdNFCMO_AreaM2Redefinido(Sender: TObject);
begin
  ValorTotal();
end;

procedure TFmVSMOEnvRet.EdNFCMO_CusMOKGRedefinido(Sender: TObject);
begin
  ValorTotal();
end;

procedure TFmVSMOEnvRet.EdNFCMO_CusMOM2Redefinido(Sender: TObject);
begin
  ValorTotal();
end;

procedure TFmVSMOEnvRet.EdNFCMO_PesoKgRedefinido(Sender: TObject);
begin
  ValorTotal();
end;

procedure TFmVSMOEnvRet.Excluiitemprontoretornadoselecionado1Click(
  Sender: TObject);
var
 CtrlTxt: String;
begin
  CtrlTxt := Geral.FF0(QrVSMOEnvGVmiControle.Value);
  if Geral.MB_Pergunta('Deseja remover o IME-I ' + CtrlTxt +
  ' dos couros prontos retornados neste retorno?') = ID_YES then
  begin
    DModG.MyPID_DB.Execute(
    'DELETE FROM _vmi_qtd_env_gvmi WHERE Controle=' + CtrlTxt);
    //
    ReopenVSMOEnvGVMI(0);
  end;
end;

procedure TFmVSMOEnvRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvRet.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  FCodigo := 0;
  //
  SetLength(FListaEnvEnv, 0);
  UnDmkDAC_PF.AbreQuery(QrPrestador1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
end;

procedure TFmVSMOEnvRet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvRet.Incluiitemprontoretornado1Click(Sender: TObject);
begin
//
end;

function TFmVSMOEnvRet.IncluiVSMOEnvGVMIs(): Boolean;
var
  VSMOEnvRet: Integer;
  FListaIMECs: TMyGrlArrInt;
  //
  procedure IncluiAtual();
  var
    Codigo, (*VSMOEnvRet,*) VSMovIts, VSMovimCod: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValFreteE, ValFreteR, ValorFrete: Double;
    SQLType: TSQLType;
  begin
    if QrVSMOEnvGVmiAtivo.Value = 0 then
      Exit;
    SQLType        := stIns;
    Codigo         := QrVSMOEnvGVmiIDItem.Value;
    //VSMOEnvRet     := ;
    VSMovIts       := QrVSMOEnvGVmiControle.Value;
    VSMovimCod     := QrVSMOEnvGVmiMovimCod.Value;
    Pecas          := QrVSMOEnvGVmiPecas.Value;
    PesoKg         := QrVSMOEnvGVmiPesoKg.Value;
    AreaM2         := QrVSMOEnvGVmiAreaM2.Value;
    AreaP2         := QrVSMOEnvGVmiAreaP2.Value;
    if QrSumGAreaKg.Value = 0 then
    begin
      ValFreteE     := 0;
      ValFreteR     := 0;
    end else
    begin
      ValFreteE     := 0;
      ValFreteR     := ((AreaM2 * 4) + PesoKg) / QrSumGAreaKg.Value *
        EdCFTPA_ValorT.ValueVariant;
    end;
    //
    ValorFrete := ValFreteE + ValFreteR;
    //
    if Codigo = 0 then
      Codigo := UMyMod.BPGS1I32('vsmoenvgvmi', 'Codigo', '', '', tsPos, stIns, Codigo);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmoenvgvmi', False, [
    'VSMOEnvRet', 'VSMovIts', 'VSMovimCod',
    'Pecas', 'PesoKg', 'AreaM2',
    'AreaP2', 'ValorFrete'(*,
    'ValFreteE', 'ValFreteR'*)], [
    'Codigo'], [
    VSMOEnvRet, VSMovIts, VSMovimCod,
    Pecas, PesoKg, AreaM2,
    AreaP2, ValorFrete(*,
    ValFreteE, ValFreteR*)], [
    Codigo], True);
    //
    DmModVS_CRC.AtualizaFreteVMIdeMOEnvRet(QrVSMOEnvGVmiControle.Value);
  end;
const
  sProcName = 'TFmVSMOEnvRet.IncluiVSMOEnvGVMIs()';
var
  J: Integer;
begin
  Result := False;
  SetLength(FListaIMECs, 0);
  VSMOEnvRet     := EdCodigo.ValueVariant;
  Dmod.MyDB.Execute('DELETE FROM vsmoenvgvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
  QrVSMOEnvGVmi.First;
  while not QrVSMOEnvGVmi.Eof do
  begin
    IncluiAtual();
    dmkPF.IncrementaListaDeInteiros(FListaIMECs, QrVSMOEnvGVmiMovimCod.Value);
    //
    QrVSMOEnvGVmi.Next;
  end;
  //
  for J := Low(FListaIMECs) to High(FListaIMECs) do
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(FListaIMECs[J]);
  //
  Result := True;
end;

function TFmVSMOEnvRet.IncluiVSMOEnvRVMIs(): Boolean;
var
  VSMOEnvRet: Integer;
  //
  procedure IncluiAtual();
  var
    Codigo, (*VSMOEnvRet,*) VSMOEnvEnv: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, ValorFrete: Double;
    SQLType: TSQLType;
  begin
    if QrVSMOEnvRVmiAtivo.Value = 0 then
      Exit;
    SQLType        := stIns;
    Codigo         := QrVSMOEnvRVmiIDItem.Value;
    //VSMOEnvRet     := ;
    VSMOEnvEnv     := QrVSMOEnvRVmiVSMOEnvEnv.Value;
    Pecas          := QrVSMOEnvRVmiPecas.Value;
    PesoKg         := QrVSMOEnvRVmiPesoKg.Value;
    AreaM2         := QrVSMOEnvRVmiAreaM2.Value;
    AreaP2         := QrVSMOEnvRVmiAreaP2.Value;
    ValorT         := QrVSMOEnvRVmiValorT.Value;
    if QrSumGAreaKg.Value = 0 then
      ValorFrete     := 0
    else
      ValorFrete := ((AreaM2 * 4) + PesoKg) / QrSumGAreaKg.Value *
        EdCFTPA_ValorT.ValueVariant;
    //
    if Codigo = 0 then
      Codigo := UMyMod.BPGS1I32('vsmoenvrvmi', 'Codigo', '', '', tsPos, stIns, Codigo);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmoenvrvmi', False, [
    'VSMOEnvRet', 'VSMOEnvEnv', 'Pecas',
    'PesoKg', 'AreaM2', 'AreaP2',
    'ValorT', 'ValorFrete'], [
    'Codigo'], [
    VSMOEnvRet, VSMOEnvEnv, Pecas,
    PesoKg, AreaM2, AreaP2,
    ValorT, ValorFrete], [
    Codigo], True);
  end;
const
  sProcName = 'TFmVSMOEnvRet.IncluiVSMOEnvRVMIs()';
begin
  Result := False;
  VSMOEnvRet     := EdCodigo.ValueVariant;
  Dmod.MyDB.Execute('DELETE FROM vsmoenvrvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
  QrVSMOEnvRVmi.First;
  while not QrVSMOEnvRVmi.Eof do
  begin
    IncluiAtual();
    //
    QrVSMOEnvRVmi.Next;
  end;
  Result := True;
end;

procedure TFmVSMOEnvRet.IncrementaListaEnvEnv();
var
  I, EnvEnv, N: Integer;
  Achou: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEnvR, DModG.MyPID_DB, [
  'SELECT * FROM ' + FTabVMIQtdEnvRVMI,
  '']);
  QrEnvR.First;
  while not QrEnvR.Eof do
  begin
    EnvEnv := QrEnvRVSMOEnvEnv.Value;
    Achou := False;
    for I := Low(FListaEnvEnv) to High(FListaEnvEnv) do
    begin
      if FListaEnvEnv[I] = EnvEnv then
      begin
        Achou := True;
        Exit;
      end;
    end;
    if Achou = False then
    begin
      N := Length(FListaEnvEnv);
      SetLength(FListaEnvEnv, N + 1);
      FListaEnvEnv[N] := EnvEnv;
    end;
    //
    QrEnvR.Next;
  end;
end;

procedure TFmVSMOEnvRet.MeNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    Key := 0;
    EdNFCMO_SerNF.SetFocus;
  end;
end;

procedure TFmVSMOEnvRet.PesquisaNFe(EdFatID, EdFatNum, EdEmpresa, EdTerceiro,
  EdSerNF, EdnNF: TdmkEdit);
const
  Aviso  = '...';
  Titulo = 'Sele��o de NFe';
  Prompt = 'Selecione a NFe';
  //DataSource = nil;
var
  Codigo: Integer;
  Resp: Variant;
  SQL: array of String;
  Serie, nNF, Terceiro, xSerie, xNF: Integer;
  SQL_Serie, SQL_nNF, SQL_Terceiro: String;
begin
  SQL_Serie    := '';
  SQL_nNF      := '';
  SQL_Terceiro := '';
  //
  Serie    := EdSerNF.ValueVariant;
  nNF      := EdnNF.ValueVariant;
  Terceiro := EdTerceiro.ValueVariant;
  //
  if Serie <> -1 then
    SQL_Serie := 'AND SUBSTRING(Id, 23, 3)=' + Geral.FF0(Serie);
  if nNF <> 0 then
    SQL_nNF     := 'AND SUBSTRING(Id, 26, 9)=' + Geral.FF0(nNF);
  if Terceiro <> 0 then
    SQL_Terceiro := 'AND CodInfoEmit=' + Geral.FF0(Terceiro);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
  'SELECT FatID, FatNum, Empresa, IDCtrl, ',
  'ide_dEmi, ide_hEmi, ide_Serie, ide_nNF, ',
  'ICMSTot_vNF, Id, emit_xNome, emit_CNPJ, ',
  'CodInfoEmit ',
  'FROM nfecaba ',
  'WHERE FatID=53 ',
  SQL_Serie,
  SQL_nNF,
  SQL_Terceiro,
  '']);
  if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DsCabA, False, False) then
  begin
    UnNFe_PF.ObtemSerieNumeroByID(QrCabA.FieldByName('Id').AsString, xSerie, xNF);
    //
    EdFatID.ValueVariant    := QrCabA.FieldByName('FatID').AsInteger;
    EdFatNum.ValueVariant   := QrCabA.FieldByName('FatNum').AsInteger;
    EdEmpresa.ValueVariant  := QrCabA.FieldByName('Empresa').AsInteger;
    EdTerceiro.ValueVariant := QrCabA.FieldByName('CodInfoEmit').AsInteger;
    //EdnItem.ValueVariant    := QrCabA.FieldByName('').AsInteger;
    //
    if xSerie <> 0 then
      EdSerNF.ValueVariant := xSerie;
    if xNF <> 0 then
      EdnNF.ValueVariant := xNF;
(*
    EdPecas.ValueVariant    := QrCabA.FieldByName('').AsFloat;
    EdPesoKg.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdAreaM2.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdAreaP2.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdValorT.ValueVariant   := QrCabA.FieldByName('').AsFloat;
*)
  end;
end;

procedure TFmVSMOEnvRet.QrVSMOEnvGVmiAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumG, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, SUM((AreaM2*4) + PesoKg) AreaKg ',
  'FROM ' + FTabVMIQtdEnvGVMI,
  'WHERE Ativo=1',
  '']);
  //
  EdNFCMO_Pecas.ValueVariant  := QrSumGPecas.Value;
  EdNFCMO_PesoKg.ValueVariant := QrSumGPesoKg.Value;
  EdNFCMO_AreaM2.ValueVariant := QrSumGAreaM2.Value;
  EdNFCMO_AreaP2.ValueVariant := QrSumGAreaP2.Value;
  EdNFCMO_ValorT.ValueVariant := QrSumGValorT.Value;
  //
  EdCFTPA_Pecas.ValueVariant  := QrSumGPecas.Value;
  EdCFTPA_PesoKg.ValueVariant := QrSumGPesoKg.Value;
  EdCFTPA_AreaM2.ValueVariant := QrSumGAreaM2.Value;
  EdCFTPA_AreaP2.ValueVariant := QrSumGAreaP2.Value;
  //
  QrVSMOEnvGVmi.DisableControls;
  try
    MyObjects.SetaItensBookmarkAtivos(Self, TDBGrid(DBGVSMOEnvGVmi), 'Ativo');
  finally
    QrVSMOEnvGVmi.EnableControls;
  end;
end;

procedure TFmVSMOEnvRet.QrVSMOEnvRVmiAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumR, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, SUM((AreaM2*4) + PesoKg) AreaKg ',
  'FROM ' + FTabVMIQtdEnvRVMI,
  'WHERE Ativo=1',
  '']);
  //
  EdNFRMP_Pecas.ValueVariant  := QrSumRPecas.Value;
  EdNFRMP_PesoKg.ValueVariant := QrSumRPesoKg.Value;
  EdNFRMP_AreaM2.ValueVariant := QrSumRAreaM2.Value;
  EdNFRMP_AreaP2.ValueVariant := QrSumRAreaP2.Value;
  EdNFRMP_ValorT.ValueVariant := QrSumRValorT.Value;
  //
end;

procedure TFmVSMOEnvRet.ReopenVSMOEnvGVMI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvGVmi, DModG.MyPID_DB, [
  'SELECT * FROM ' + FTabVMIQtdEnvGVMI,
  '']);
  if Controle <> 0 then
    QrVSMOEnvGVmi.Locate('Controle', Controle, []);
end;

procedure TFmVSMOEnvRet.ReopenVSMOEnvRVMI(VSMOEnvEnv: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvRVmi, DModG.MyPID_DB, [
  'SELECT * FROM ' + FTabVMIQtdEnvRVMI,
  '']);
  if VSMOEnvEnv <> 0 then
    QrVSMOEnvRVmi.Locate('VSMOEnvEnv', VSMOEnvEnv, []);
end;

procedure TFmVSMOEnvRet.SbRMPClick(Sender: TObject);
begin
  PesquisaNFe(EdNFRMP_FatID, EdNFRMP_FatNum, EdNFRMP_Empresa, EdNFRMP_Terceiro,
  EdNFRMP_SerNF, EdNFRMP_nNF);
end;

procedure TFmVSMOEnvRet.ValorTotal();
var
 NFCMO_AreaM2, NFCMO_PesoKg, NFCMO_CusMOKG, NFCMO_CusMOM2, NFCMO_ValorT: Double;
begin
  NFCMO_AreaM2  := EdNFCMO_AreaM2.ValueVariant;
  NFCMO_PesoKg  := EdNFCMO_PesoKg.ValueVariant;
  NFCMO_CusMOKG := EdNFCMO_CusMOKG.ValueVariant;
  NFCMO_CusMOM2 := EdNFCMO_CusMOM2.ValueVariant;
  //NFCMO_ValorT  := EdNFCMO_ValorT.ValueVariant;
  if (NFCMO_AreaM2 > 0) and (NFCMO_CusMOM2 > 0) then
    NFCMO_ValorT := NFCMO_AreaM2 * NFCMO_CusMOM2
  else
  if(NFCMO_PesoKg > 0) and (NFCMO_CusMOKG > 0) then
    NFCMO_ValorT := NFCMO_PesoKg * NFCMO_CusMOKG;
  //
  if NFCMO_ValorT > 0 then
    EdNFCMO_ValorT.ValueVariant := NFCMO_ValorT;
end;

procedure TFmVSMOEnvRet.SbCMOClick(Sender: TObject);
begin
  PesquisaNFe(EdNFCMO_FatID, EdNFCMO_FatNum, EdNFCMO_Empresa, EdNFCMO_Terceiro,
  EdNFCMO_SerNF, EdNFCMO_nNF);
end;

{
object GroupBox2: TGroupBox
  Left = 0
  Top = 0
  Width = 1008
  Height = 72
  Align = alTop
  Caption = ' Item produzido: '
  TabOrder = 0
  ExplicitTop = 45
  ExplicitWidth = 741
  object Panel5: TPanel
    Left = 2
    Top = 15
    Width = 1004
    Height = 55
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 737
    object Label2: TLabel
      Left = 72
      Top = 0
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label3: TLabel
      Left = 104
      Top = 0
      Width = 29
      Height = 13
      Caption = 'N'#237'vel:'
      Enabled = False
    end
    object Label4: TLabel
      Left = 136
      Top = 0
      Width = 28
      Height = 13
      Caption = 'IME-I:'
      Enabled = False
    end
    object Label5: TLabel
      Left = 212
      Top = 0
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 288
      Top = 0
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      Enabled = False
    end
    object Label8: TLabel
      Left = 12
      Top = 0
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      Enabled = False
    end
    object Label26: TLabel
      Left = 364
      Top = 0
      Width = 44
      Height = 13
      Caption = 'S'#233'rie NF:'
    end
    object Label27: TLabel
      Left = 416
      Top = 0
      Width = 42
      Height = 13
      Caption = 'Num NF:'
    end
    object EdVSVMI_MovimID: TdmkEdit
      Left = 72
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_MovimNiv: TdmkEdit
      Left = 104
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Controle: TdmkEdit
      Left = 136
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Codigo: TdmkEdit
      Left = 212
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_MovimCod: TdmkEdit
      Left = 288
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Empresa: TdmkEdit
      Left = 12
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_SerNF: TdmkEdit
      Left = 363
      Top = 16
      Width = 48
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '-1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = -1
      ValWarn = False
    end
    object EdVSVMI_nNF: TdmkEdit
      Left = 415
      Top = 16
      Width = 72
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Memo1: TMemo
      Left = 492
      Top = 2
      Width = 181
      Height = 53
      Enabled = False
      Lines.Strings = (
        '<-- S'#233'rie e NF do cabe'#231'alho.'
        '<-- Deprecado!'
        '<-- Apenas Informativo.')
      ReadOnly = True
      TabOrder = 8
    end
  end
end
}

//////////////////////////////////////////////////////////////////////////////////


{
object GroupBox6: TGroupBox
  Left = 0
  Top = 0
  Width = 1008
  Height = 60
  Align = alTop
  Caption = ' IME-I de origem (Item enviado para MO): '
  TabOrder = 0
  ExplicitTop = 117
  ExplicitWidth = 741
  object Panel9: TPanel
    Left = 2
    Top = 15
    Width = 1004
    Height = 43
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 737
    object Label30: TLabel
      Left = 12
      Top = 0
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      Enabled = False
    end
    object Label47: TLabel
      Left = 72
      Top = 0
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label48: TLabel
      Left = 104
      Top = 0
      Width = 29
      Height = 13
      Caption = 'N'#237'vel:'
      Enabled = False
    end
    object Label49: TLabel
      Left = 136
      Top = 0
      Width = 28
      Height = 13
      Caption = 'IME-I:'
      Enabled = False
    end
    object Label50: TLabel
      Left = 212
      Top = 0
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      Enabled = False
    end
    object Label51: TLabel
      Left = 288
      Top = 0
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      Enabled = False
    end
    object Label52: TLabel
      Left = 364
      Top = 0
      Width = 58
      Height = 13
      Caption = 'ID do envio:'
      Enabled = False
    end
    object EdVSSrc_Empresa: TdmkEdit
      Left = 12
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSSrc_MovimID: TdmkEdit
      Left = 72
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSSrc_MovimNiv: TdmkEdit
      Left = 104
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSSrc_Controle: TdmkEdit
      Left = 136
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSSrc_Codigo: TdmkEdit
      Left = 212
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSSrc_MovimCod: TdmkEdit
      Left = 288
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSMOEnvEnv: TdmkEdit
      Left = 364
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
end
}


end.
