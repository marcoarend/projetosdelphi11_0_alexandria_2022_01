unit VSReclassifOneOld;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkEditCB, dmkDBLookupComboBox, UnInternalConsts, ShellApi;

type
  TFmVSReclassifOneOld = class(TForm)
    Panel1: TPanel;
    PnBoxesAll: TPanel;
    PnBoxesT01: TPanel;
    PnBoxesT02: TPanel;
    PnBoxT1L1: TPanel;
    Panel2: TPanel;
    QrVSPaRclCab: TmySQLQuery;
    DsVSPaRclCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    DsVSGerArtNew: TDataSource;
    QrVSPaRclCabNO_TIPO: TWideStringField;
    QrVSPaRclCabGraGruX: TIntegerField;
    QrVSPaRclCabNome: TWideStringField;
    QrVSPaRclCabTipoArea: TSmallintField;
    QrVSPaRclCabEmpresa: TIntegerField;
    QrVSPaRclCabMovimCod: TIntegerField;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabVSGerRcl: TIntegerField;
    QrVSPaRclCabLstPal01: TIntegerField;
    QrVSPaRclCabLstPal02: TIntegerField;
    QrVSPaRclCabLstPal03: TIntegerField;
    QrVSPaRclCabLstPal04: TIntegerField;
    QrVSPaRclCabLstPal05: TIntegerField;
    QrVSPaRclCabLstPal06: TIntegerField;
    QrVSPaRclCabLk: TIntegerField;
    QrVSPaRclCabDataCad: TDateField;
    QrVSPaRclCabDataAlt: TDateField;
    QrVSPaRclCabUserCad: TIntegerField;
    QrVSPaRclCabUserAlt: TIntegerField;
    QrVSPaRclCabAlterWeb: TSmallintField;
    QrVSPaRclCabAtivo: TSmallintField;
    QrVSPaRclCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaRclCabNO_EMPRESA: TWideStringField;
    QrVSPallet1: TmySQLQuery;
    QrVSPallet1Codigo: TIntegerField;
    QrVSPallet1Nome: TWideStringField;
    QrVSPallet1Lk: TIntegerField;
    QrVSPallet1DataCad: TDateField;
    QrVSPallet1DataAlt: TDateField;
    QrVSPallet1UserCad: TIntegerField;
    QrVSPallet1UserAlt: TIntegerField;
    QrVSPallet1AlterWeb: TSmallintField;
    QrVSPallet1Ativo: TSmallintField;
    QrVSPallet1Empresa: TIntegerField;
    QrVSPallet1Status: TIntegerField;
    QrVSPallet1CliStat: TIntegerField;
    QrVSPallet1GraGruX: TIntegerField;
    QrVSPallet1NO_CLISTAT: TWideStringField;
    QrVSPallet1NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet1NO_STATUS: TWideStringField;
    DsVSPallet1: TDataSource;
    QrVSPallet2: TmySQLQuery;
    QrVSPallet2Codigo: TIntegerField;
    QrVSPallet2Nome: TWideStringField;
    QrVSPallet2Lk: TIntegerField;
    QrVSPallet2DataCad: TDateField;
    QrVSPallet2DataAlt: TDateField;
    QrVSPallet2UserCad: TIntegerField;
    QrVSPallet2UserAlt: TIntegerField;
    QrVSPallet2AlterWeb: TSmallintField;
    QrVSPallet2Ativo: TSmallintField;
    QrVSPallet2Empresa: TIntegerField;
    QrVSPallet2Status: TIntegerField;
    QrVSPallet2CliStat: TIntegerField;
    QrVSPallet2GraGruX: TIntegerField;
    QrVSPallet2NO_CLISTAT: TWideStringField;
    QrVSPallet2NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet2NO_STATUS: TWideStringField;
    DsVSPallet2: TDataSource;
    QrVSPallet3: TmySQLQuery;
    QrVSPallet3Codigo: TIntegerField;
    QrVSPallet3Nome: TWideStringField;
    QrVSPallet3Lk: TIntegerField;
    QrVSPallet3DataCad: TDateField;
    QrVSPallet3DataAlt: TDateField;
    QrVSPallet3UserCad: TIntegerField;
    QrVSPallet3UserAlt: TIntegerField;
    QrVSPallet3AlterWeb: TSmallintField;
    QrVSPallet3Ativo: TSmallintField;
    QrVSPallet3Empresa: TIntegerField;
    QrVSPallet3Status: TIntegerField;
    QrVSPallet3CliStat: TIntegerField;
    QrVSPallet3GraGruX: TIntegerField;
    QrVSPallet3NO_CLISTAT: TWideStringField;
    QrVSPallet3NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet3NO_STATUS: TWideStringField;
    DsVSPallet3: TDataSource;
    QrVSPallet4: TmySQLQuery;
    QrVSPallet4Codigo: TIntegerField;
    QrVSPallet4Nome: TWideStringField;
    QrVSPallet4Lk: TIntegerField;
    QrVSPallet4DataCad: TDateField;
    QrVSPallet4DataAlt: TDateField;
    QrVSPallet4UserCad: TIntegerField;
    QrVSPallet4UserAlt: TIntegerField;
    QrVSPallet4AlterWeb: TSmallintField;
    QrVSPallet4Ativo: TSmallintField;
    QrVSPallet4Empresa: TIntegerField;
    QrVSPallet4Status: TIntegerField;
    QrVSPallet4CliStat: TIntegerField;
    QrVSPallet4GraGruX: TIntegerField;
    QrVSPallet4NO_CLISTAT: TWideStringField;
    QrVSPallet4NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet4NO_STATUS: TWideStringField;
    DsVSPallet4: TDataSource;
    QrVSPallet5: TmySQLQuery;
    QrVSPallet5Codigo: TIntegerField;
    QrVSPallet5Nome: TWideStringField;
    QrVSPallet5Lk: TIntegerField;
    QrVSPallet5DataCad: TDateField;
    QrVSPallet5DataAlt: TDateField;
    QrVSPallet5UserCad: TIntegerField;
    QrVSPallet5UserAlt: TIntegerField;
    QrVSPallet5AlterWeb: TSmallintField;
    QrVSPallet5Ativo: TSmallintField;
    QrVSPallet5Empresa: TIntegerField;
    QrVSPallet5Status: TIntegerField;
    QrVSPallet5CliStat: TIntegerField;
    QrVSPallet5GraGruX: TIntegerField;
    QrVSPallet5NO_CLISTAT: TWideStringField;
    QrVSPallet5NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet5NO_STATUS: TWideStringField;
    DsVSPallet5: TDataSource;
    QrVSPallet6: TmySQLQuery;
    QrVSPallet6Codigo: TIntegerField;
    QrVSPallet6Nome: TWideStringField;
    QrVSPallet6Lk: TIntegerField;
    QrVSPallet6DataCad: TDateField;
    QrVSPallet6DataAlt: TDateField;
    QrVSPallet6UserCad: TIntegerField;
    QrVSPallet6UserAlt: TIntegerField;
    QrVSPallet6AlterWeb: TSmallintField;
    QrVSPallet6Ativo: TSmallintField;
    QrVSPallet6Empresa: TIntegerField;
    QrVSPallet6Status: TIntegerField;
    QrVSPallet6CliStat: TIntegerField;
    QrVSPallet6GraGruX: TIntegerField;
    QrVSPallet6NO_CLISTAT: TWideStringField;
    QrVSPallet6NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet6NO_STATUS: TWideStringField;
    DsVSPallet6: TDataSource;
    QrVSPalRclIts1: TmySQLQuery;
    QrVSPalRclIts1Controle: TIntegerField;
    QrVSPalRclIts1DtHrIni: TDateTimeField;
    DsVSPalRclIts1: TDataSource;
    QrVSPalRclIts2: TmySQLQuery;
    QrVSPalRclIts2Controle: TIntegerField;
    QrVSPalRclIts2DtHrIni: TDateTimeField;
    DsVSPalRclIts2: TDataSource;
    QrVSPalRclIts3: TmySQLQuery;
    QrVSPalRclIts3Controle: TIntegerField;
    QrVSPalRclIts3DtHrIni: TDateTimeField;
    DsVSPalRclIts3: TDataSource;
    QrVSPalRclIts4: TmySQLQuery;
    QrVSPalRclIts4Controle: TIntegerField;
    QrVSPalRclIts4DtHrIni: TDateTimeField;
    DsVSPalRclIts4: TDataSource;
    QrVSPalRclIts5: TmySQLQuery;
    QrVSPalRclIts5Controle: TIntegerField;
    QrVSPalRclIts5DtHrIni: TDateTimeField;
    DsVSPalRclIts5: TDataSource;
    QrVSPalRclIts6: TmySQLQuery;
    QrVSPalRclIts6Controle: TIntegerField;
    QrVSPalRclIts6DtHrIni: TDateTimeField;
    DsVSPalRclIts6: TDataSource;
    QrItens1: TmySQLQuery;
    DsItens1: TDataSource;
    QrSum1: TmySQLQuery;
    DsSum1: TDataSource;
    QrSum1Pecas: TFloatField;
    QrSum1AreaM2: TFloatField;
    QrSum1AreaP2: TFloatField;
    QrItens1Controle: TLargeintField;
    QrItens1Pecas: TFloatField;
    QrItens1AreaM2: TFloatField;
    QrItens1AreaP2: TFloatField;
    QrItens2: TmySQLQuery;
    QrItens2Controle: TLargeintField;
    QrItens2Pecas: TFloatField;
    QrItens2AreaM2: TFloatField;
    QrItens2AreaP2: TFloatField;
    DsItens2: TDataSource;
    QrSum2: TmySQLQuery;
    QrSum2Pecas: TFloatField;
    QrSum2AreaM2: TFloatField;
    QrSum2AreaP2: TFloatField;
    DsSum2: TDataSource;
    QrItens3: TmySQLQuery;
    QrItens3Controle: TLargeintField;
    QrItens3Pecas: TFloatField;
    QrItens3AreaM2: TFloatField;
    QrItens3AreaP2: TFloatField;
    DsItens3: TDataSource;
    QrSum3: TmySQLQuery;
    QrSum3Pecas: TFloatField;
    QrSum3AreaM2: TFloatField;
    QrSum3AreaP2: TFloatField;
    DsSum3: TDataSource;
    QrItens4: TmySQLQuery;
    QrItens4Controle: TLargeintField;
    QrItens4Pecas: TFloatField;
    QrItens4AreaM2: TFloatField;
    QrItens4AreaP2: TFloatField;
    DsItens4: TDataSource;
    QrSum4: TmySQLQuery;
    QrSum4Pecas: TFloatField;
    QrSum4AreaM2: TFloatField;
    QrSum4AreaP2: TFloatField;
    DsSum4: TDataSource;
    QrItens5: TmySQLQuery;
    QrItens5Controle: TLargeintField;
    QrItens5Pecas: TFloatField;
    QrItens5AreaM2: TFloatField;
    QrItens5AreaP2: TFloatField;
    DsItens5: TDataSource;
    QrSum5: TmySQLQuery;
    QrSum5Pecas: TFloatField;
    QrSum5AreaM2: TFloatField;
    QrSum5AreaP2: TFloatField;
    DsSum5: TDataSource;
    QrItens6: TmySQLQuery;
    QrItens6Controle: TLargeintField;
    QrItens6Pecas: TFloatField;
    QrItens6AreaM2: TFloatField;
    QrItens6AreaP2: TFloatField;
    DsItens6: TDataSource;
    QrSum6: TmySQLQuery;
    QrSum6Pecas: TFloatField;
    QrSum6AreaM2: TFloatField;
    QrSum6AreaP2: TFloatField;
    DsSum6: TDataSource;
    QrSumT: TmySQLQuery;
    QrSumTPecas: TFloatField;
    QrSumTAreaM2: TFloatField;
    QrSumTAreaP2: TFloatField;
    DsSumT: TDataSource;
    QrSumTFALTA_PECA: TFloatField;
    QrSumTFALTA_AREA: TFloatField;
    PnBoxT1L2: TPanel;
    DBGAll: TdmkDBGridZTO;
    QrAll: TmySQLQuery;
    QrAllControle: TLargeintField;
    QrAllPecas: TFloatField;
    QrAllAreaM2: TFloatField;
    QrAllAreaP2: TFloatField;
    DsAll: TDataSource;
    QrAllBox: TIntegerField;
    QrAllVSPaRclIts: TIntegerField;
    QrAllVSPallet: TIntegerField;
    PnBoxT1L3: TPanel;
    PnBoxT2L3: TPanel;
    PnBoxT2L2: TPanel;
    PnBoxT2L1: TPanel;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    PnBox: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    Panel12: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEDAreaT: TDBEdit;
    Panel13: TPanel;
    Label19: TLabel;
    Panel14: TPanel;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdit24: TDBEdit;
    PMItens01: TPopupMenu;
    EncerrarPallet01: TMenuItem;
    RemoverPallet01: TMenuItem;
    AdicionarPallet01: TMenuItem;
    PMItens02: TPopupMenu;
    AdicionarPallet02: TMenuItem;
    EncerrarPallet02: TMenuItem;
    RemoverPallet02: TMenuItem;
    QrVSPalRclIts1VMI_Sorc: TIntegerField;
    QrVSPalRclIts1VMI_Dest: TIntegerField;
    QrVSPalRclIts2VMI_Sorc: TIntegerField;
    QrVSPalRclIts2VMI_Dest: TIntegerField;
    QrVSPalRclIts3VMI_Sorc: TIntegerField;
    QrVSPalRclIts3VMI_Dest: TIntegerField;
    QrVSPalRclIts4VMI_Sorc: TIntegerField;
    QrVSPalRclIts4VMI_Dest: TIntegerField;
    QrVSPalRclIts5VMI_Sorc: TIntegerField;
    QrVSPalRclIts5VMI_Dest: TIntegerField;
    QrVSPalRclIts6VMI_Sorc: TIntegerField;
    QrVSPalRclIts6VMI_Dest: TIntegerField;
    QrAllVMI_Sorc: TIntegerField;
    QrAllVMI_Dest: TIntegerField;
    QrSumPal1: TmySQLQuery;
    DsSumPal1: TDataSource;
    QrSumPal1Pecas: TFloatField;
    QrSumPal1AreaM2: TFloatField;
    QrSumPal1AreaP2: TFloatField;
    QrSumPal2: TmySQLQuery;
    QrSumPal2Pecas: TFloatField;
    QrSumPal2AreaM2: TFloatField;
    QrSumPal2AreaP2: TFloatField;
    DsSumPal2: TDataSource;
    QrSumPal3: TmySQLQuery;
    QrSumPal3Pecas: TFloatField;
    QrSumPal3AreaM2: TFloatField;
    QrSumPal3AreaP2: TFloatField;
    DsSumPal3: TDataSource;
    QrSumPal4: TmySQLQuery;
    QrSumPal4Pecas: TFloatField;
    QrSumPal4AreaM2: TFloatField;
    QrSumPal4AreaP2: TFloatField;
    DsSumPal4: TDataSource;
    QrSumPal5: TmySQLQuery;
    QrSumPal5Pecas: TFloatField;
    QrSumPal5AreaM2: TFloatField;
    QrSumPal5AreaP2: TFloatField;
    DsSumPal5: TDataSource;
    QrSumPal6: TmySQLQuery;
    QrSumPal6Pecas: TFloatField;
    QrSumPal6AreaM2: TFloatField;
    QrSumPal6AreaP2: TFloatField;
    DsSumPal6: TDataSource;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    BtEncerra: TBitBtn;
    PMItens03: TPopupMenu;
    AdicionarPallet03: TMenuItem;
    EncerrarPallet03: TMenuItem;
    RemoverPallet03: TMenuItem;
    PMItens04: TPopupMenu;
    AdicionarPallet04: TMenuItem;
    EncerrarPallet04: TMenuItem;
    RemoverPallet04: TMenuItem;
    PMItens05: TPopupMenu;
    AdicionarPallet05: TMenuItem;
    EncerrarPallet05: TMenuItem;
    RemoverPallet05: TMenuItem;
    PMItens06: TPopupMenu;
    AdicionarPallet06: TMenuItem;
    EncerrarPallet06: TMenuItem;
    RemoverPallet06: TMenuItem;
    BtImprime: TBitBtn;
    BtDigitacao: TButton;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    Palletdobox11: TMenuItem;
    Palletdobox21: TMenuItem;
    Palletdobox31: TMenuItem;
    Palletdobox41: TMenuItem;
    Palletdobox51: TMenuItem;
    Palletdobox61: TMenuItem;
    QrVSPaRclCabCacCod: TIntegerField;
    Panel42: TPanel;
    DBGItensACP: TdmkDBGridZTO;
    Panel43: TPanel;
    Panel44: TPanel;
    QrVSPaRclCabVSPallet: TIntegerField;
    QrItensACP: TmySQLQuery;
    DsItensACP: TDataSource;
    QrVSPallet0: TmySQLQuery;
    QrVSPallet0Codigo: TIntegerField;
    QrVSPallet0Nome: TWideStringField;
    QrVSPallet0Lk: TIntegerField;
    QrVSPallet0DataCad: TDateField;
    QrVSPallet0DataAlt: TDateField;
    QrVSPallet0UserCad: TIntegerField;
    QrVSPallet0UserAlt: TIntegerField;
    QrVSPallet0AlterWeb: TSmallintField;
    QrVSPallet0Ativo: TSmallintField;
    QrVSPallet0Empresa: TIntegerField;
    QrVSPallet0Status: TIntegerField;
    QrVSPallet0CliStat: TIntegerField;
    QrVSPallet0GraGruX: TIntegerField;
    QrVSPallet0NO_CLISTAT: TWideStringField;
    QrVSPallet0NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet0NO_STATUS: TWideStringField;
    DsVSPallet0: TDataSource;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewNO_Pallet: TWideStringField;
    QrVSGerArtNewNO_TERCEIRO: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrItensACPAreaM2: TFloatField;
    QrItensACPCodigo: TIntegerField;
    QrItensACPControle: TLargeintField;
    RGFrmaIns: TRadioGroup;
    QrItensACPAreaP2: TFloatField;
    QrItensACPCacID: TIntegerField;
    Panel6: TPanel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label6: TLabel;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    Label20: TLabel;
    DBEdit12: TDBEdit;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    QrItensACPVMI_Dest: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrVSPalRclIts1VMI_Baix: TIntegerField;
    QrVSPalRclIts2VMI_Baix: TIntegerField;
    QrVSPalRclIts3VMI_Baix: TIntegerField;
    QrVSPalRclIts4VMI_Baix: TIntegerField;
    QrVSPalRclIts5VMI_Baix: TIntegerField;
    QrVSPalRclIts6VMI_Baix: TIntegerField;
    QrSorces: TmySQLQuery;
    QrVsiDest: TmySQLQuery;
    QrVsiDestMovimTwn: TIntegerField;
    QrVsiDestMovimID: TIntegerField;
    QrVsiDestCodigo: TIntegerField;
    QrVsiDestControle: TIntegerField;
    QrVsiDestGraGruX: TIntegerField;
    QrVsiDestMovimCod: TIntegerField;
    QrVsiDestEmpresa: TIntegerField;
    QrVsiDestTerceiro: TIntegerField;
    QrSorcesPecas: TFloatField;
    QrSorcesAreaM2: TFloatField;
    QrVsiSorc: TmySQLQuery;
    QrVsiSorcGraGruX: TIntegerField;
    QrSorcesVMI_Sorc: TIntegerField;
    QrVsiSorcAreaM2: TFloatField;
    QrVsiSorcValorT: TFloatField;
    QrVsiSorcCodigo: TIntegerField;
    QrVsiSorcControle: TIntegerField;
    QrVsiSorcFicha: TIntegerField;
    QrVsiSorcSerieFch: TIntegerField;
    QrVsiSorcMarca: TWideStringField;
    QrSorcesAreaP2: TFloatField;
    QrSumSorc: TmySQLQuery;
    QrSumSorcPecas: TFloatField;
    QrSumSorcAreaM2: TFloatField;
    QrSumSorcAreaP2: TFloatField;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    Panel11: TPanel;
    Label70: TLabel;
    EdRevisor: TdmkEditCB;
    CBRevisor: TdmkDBLookupComboBox;
    Label71: TLabel;
    EdDigitador: TdmkEditCB;
    CBDigitador: TdmkDBLookupComboBox;
    BtReabre: TBitBtn;
    QrVSPaRclCabVSGerRclA: TIntegerField;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    Mensagemdedados1: TMenuItem;
    Mensagemdedados2: TMenuItem;
    Mensagemdedados3: TMenuItem;
    Mensagemdedados4: TMenuItem;
    Mensagemdedados5: TMenuItem;
    Mensagemdedados6: TMenuItem;
    Memo1: TMemo;
    PnDesnate: TPanel;
    Panel45: TPanel;
    EdDesnate: TdmkEdit;
    SbDesnate: TSpeedButton;
    Label77: TLabel;
    CkDesnate: TCheckBox;
    DBGrid1: TDBGrid;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsGraGruX: TIntegerField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsGraGru1: TIntegerField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacItsMediaM2PC: TFloatField;
    QrVSCacItsAgrupaTudo: TFloatField;
    QrVSCacSum: TmySQLQuery;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    DsVSCacIts: TDataSource;
    Splitter1: TSplitter;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSPaRclCabVSMovIts: TIntegerField;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    DBEdit5: TDBEdit;
    Label84: TLabel;
    EdTempo: TEdit;
    Label85: TLabel;
    PnSubClass: TPanel;
    Label86: TLabel;
    EdSubClass: TdmkEdit;
    Label87: TLabel;
    DBEdit35: TDBEdit;
    QrAllSubClass: TWideStringField;
    PnEqualize: TPanel;
    Panel70: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    DBGrid2: TDBGrid;
    Splitter2: TSplitter;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnNota: TPanel;
    DBEdit42: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    CkNota: TCheckBox;
    AlteraitematualSubClasse1: TMenuItem;
    N3: TMenuItem;
    PnBox01: TPanel;
    PnBox04: TPanel;
    PnBox05: TPanel;
    PnBox02: TPanel;
    PnBox03: TPanel;
    PnBox06: TPanel;
    Panel4: TPanel;
    Panel8: TPanel;
    Panel49: TPanel;
    DBEdit3: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit1: TDBEdit;
    Panel50: TPanel;
    Label79: TLabel;
    Panel51: TPanel;
    Panel64: TPanel;
    CkSubClass1: TCheckBox;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label54: TLabel;
    DBEdit4: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdArea1: TDBEdit;
    EdPercent1: TdmkEdit;
    DBEdit50: TDBEdit;
    GroupBox2: TGroupBox;
    Panel16: TPanel;
    Label52: TLabel;
    Label55: TLabel;
    Label53: TLabel;
    Label13: TLabel;
    DBEdit36: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit57: TDBEdit;
    EdMedia1: TdmkEdit;
    DBGPallet1: TdmkDBGridZTO;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel46: TPanel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit51: TDBEdit;
    Panel47: TPanel;
    Label78: TLabel;
    Panel48: TPanel;
    Panel65: TPanel;
    CkSubClass4: TCheckBox;
    Panel33: TPanel;
    GroupBox7: TGroupBox;
    Panel36: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    DBEdit28: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdArea4: TDBEdit;
    EdPercent4: TdmkEdit;
    DBEdit56: TDBEdit;
    GroupBox8: TGroupBox;
    Panel37: TPanel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label74: TLabel;
    DBEdit65: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    EdMedia4: TdmkEdit;
    DBGPallet4: TdmkDBGridZTO;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel52: TPanel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit22: TDBEdit;
    Panel53: TPanel;
    Label80: TLabel;
    Panel54: TPanel;
    Panel66: TPanel;
    CkSubClass2: TCheckBox;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel55: TPanel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit44: TDBEdit;
    Panel56: TPanel;
    Label81: TLabel;
    Panel57: TPanel;
    Panel67: TPanel;
    CkSubClass5: TCheckBox;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel58: TPanel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit30: TDBEdit;
    Panel59: TPanel;
    Label82: TLabel;
    Panel60: TPanel;
    Panel68: TPanel;
    CkSubClass3: TCheckBox;
    Panel23: TPanel;
    Panel62: TPanel;
    Label83: TLabel;
    Panel63: TPanel;
    Panel24: TPanel;
    Panel61: TPanel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit37: TDBEdit;
    Panel69: TPanel;
    CkSubClass6: TCheckBox;
    Panel19: TPanel;
    GroupBox3: TGroupBox;
    Panel26: TPanel;
    Label5: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit15: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdArea2: TDBEdit;
    EdPercent2: TdmkEdit;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel30: TPanel;
    Label31: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label72: TLabel;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    EdMedia2: TdmkEdit;
    Panel29: TPanel;
    GroupBox9: TGroupBox;
    Panel38: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdArea5: TDBEdit;
    EdPercent5: TdmkEdit;
    DBEdit68: TDBEdit;
    GroupBox10: TGroupBox;
    Panel39: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label75: TLabel;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    EdMedia5: TdmkEdit;
    DBGPallet5: TdmkDBGridZTO;
    DBGPallet2: TdmkDBGridZTO;
    Panel22: TPanel;
    GroupBox5: TGroupBox;
    Panel34: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdArea3: TDBEdit;
    EdPercent3: TdmkEdit;
    DBEdit61: TDBEdit;
    GroupBox6: TGroupBox;
    Panel35: TPanel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label73: TLabel;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    EdMedia3: TdmkEdit;
    Panel25: TPanel;
    GroupBox11: TGroupBox;
    Panel40: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdArea6: TDBEdit;
    EdPercent6: TdmkEdit;
    DBEdit72: TDBEdit;
    GroupBox12: TGroupBox;
    Panel41: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label76: TLabel;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    EdMedia6: TdmkEdit;
    DBGPallet6: TdmkDBGridZTO;
    DBGPallet3: TdmkDBGridZTO;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaRclCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaRclCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaRclCabBeforeClose(DataSet: TDataSet);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDigitacaoClick(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure RGFrmaInsClick(Sender: TObject);
    procedure QrItensACPBeforeClose(DataSet: TDataSet);
    procedure QrItensACPAfterScroll(DataSet: TDataSet);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure QrItensACPAfterOpen(DataSet: TDataSet);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure SbDesnateClick(Sender: TObject);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSubClassExit(Sender: TObject);
    procedure CBRevisorClick(Sender: TObject);
    procedure CBDigitadorClick(Sender: TObject);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure AlteraitematualSubClasse1Click(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
  private
    { Private declarations }
    FDifTime: TDateTime;
    //
    procedure AdicionaPallet(Box: Integer);
    procedure AtualizaInfoOC();
    procedure AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst: Integer);
    function  BoxDeComp(Compo: TObject): Integer;
    function  BoxInBoxes(Box: Integer): Boolean;

    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure ExcluiUltimoCouro();
    procedure InsereCouroAtual();

    procedure LiberaDigitacao();
    procedure LiberaDigitacaoManual(Libera: Boolean);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    function  ObtemDadosBox(const Box: Integer; var VSPaRclIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
              Boolean): Boolean;
    function  ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri, VMI_Sorc,
              VSCacItsAOri: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet, QrItens, QrSum,
              QrSumPal: TmySQLQuery): Boolean;
    procedure RealinhaBoxes();
    procedure ReopenVSGerArtDst();
    procedure ReopenItens(VSPaRclIts, VSPallet: Integer; QrIts, QrSum,
              QrSumPal: TmySQLQuery);
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure ReopenSumPal(QrSumPal: TmySQLQuery; VSPallet: Integer);
    procedure ReopenItensAClassificarPallet();
    procedure TentarFocarEdArea();
    procedure VerificaAreaAutomatica();
    procedure VerificaBoxes();
    // VSMod ???
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalRclIts:
              TmySQLQuery; Pallet: Integer);
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
    function  ZeraEstoquePalletOrigem(): Boolean;
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenDesnate(Automatico: Boolean);
    procedure ReopenEqualize(Automatico: Boolean);
    function  SubstituiOC(): Boolean;
  public
    { Public declarations }
    FCriando: Boolean;
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    //
    procedure ReopenVSPaRclCab();
  end;

var
  FmVSReclassifOneOld: TFmVSReclassifOneOld;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAdd,
  MyDBCheck, UnVS_PF, VSRclArtPrpNew, AppListas, VSReclassPrePal, UnDmkProcFunc;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX] of Boolean;

procedure TFmVSReclassifOneOld.AdicionaPallet(Box: Integer);
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAdd, FmVSClaArtPalAdd, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAdd.FMovimIDGer              := emidReclasVS;
    FmVSClaArtPalAdd.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAdd.FFornecedor              := Trunc(QrVSGerArtNewTerceiro.Value);
    FmVSClaArtPalAdd.FMovimID                 := FMovimID;
    FmVSClaArtPalAdd.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAdd.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAdd.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAdd.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAdd.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAdd.EdCodigo.ValueVariant    := QrVSPaRclCabCodigo.Value;
    FmVSClaArtPalAdd.EdSrcPallet.ValueVariant := QrVSPaRclCabVSPallet.Value;
    FmVSClaArtPalAdd.FExigeSrcPallet          := True;
    FmVSClaArtPalAdd.EdMovimCod.ValueVariant  := QrVSPaRclCabMovimCod.Value;
    FmVSClaArtPalAdd.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAdd.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAdd.FBox := Box;
    FmVSClaArtPalAdd.PnBox.Caption := Geral.FF0(Box);
    //
    FmVSClaArtPalAdd.FPal01 := QrVSPaRclCabLstPal01.Value;
    FmVSClaArtPalAdd.FPal02 := QrVSPaRclCabLstPal02.Value;
    FmVSClaArtPalAdd.FPal03 := QrVSPaRclCabLstPal03.Value;
    FmVSClaArtPalAdd.FPal04 := QrVSPaRclCabLstPal04.Value;
    FmVSClaArtPalAdd.FPal05 := QrVSPaRclCabLstPal05.Value;
    FmVSClaArtPalAdd.FPal06 := QrVSPaRclCabLstPal06.Value;
    //
    VS_PF.ReopenVSPallet(FmVSClaArtPalAdd.QrVSPallet,
      FmVSClaArtPalAdd.FEmpresa, 0, '');
    //
    FmVSClaArtPalAdd.ShowModal;
    FmVSClaArtPalAdd.Destroy;
    //
    ReopenVSPaRclCab();
  end;
end;

procedure TFmVSReclassifOneOld.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneOld.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, False);
end;

procedure TFmVSReclassifOneOld.AlteraitematualSubClasse1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, True);
end;

procedure TFmVSReclassifOneOld.AtualizaInfoOC();
var
  AreaT: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT Controle, VSPaRclIts, VSPallet, Pecas, ',
  'AreaM2, AreaP2, VMI_Sorc, VMI_Dest, Box, SubClass ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY Controle DESC ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  '']);
  //
  if QrSumTAreaM2.Value > 0 then
  begin
    if QrVSPaRclCabTipoArea.Value = 0 (*m2*) then
    begin
      AreaT := QrSumTAreaM2.Value;
      //
      EdPercent1.ValueVariant := QrSum1AreaM2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaM2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaM2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaM2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaM2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaM2.Value / AreaT * 100;
    end else
    begin
      AreaT := QrSumTAreaP2.Value;
      //
      EdPercent1.ValueVariant := QrSum1AreaP2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaP2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaP2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaP2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaP2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaP2.Value / AreaT * 100;
    end;
  end else
  begin
    EdPercent1.ValueVariant := 0;
    EdPercent2.ValueVariant := 0;
    EdPercent3.ValueVariant := 0;
    EdPercent4.ValueVariant := 0;
    EdPercent5.ValueVariant := 0;
    EdPercent6.ValueVariant := 0;
  end;
  //
  if QrSumPal1AreaM2.Value > 0 then
    EdMedia1.ValueVariant := QrSumPal1AreaM2.Value / QrSumPal1Pecas.Value
  else
    EdMedia1.ValueVariant := 0;
  //
  if QrSumPal2AreaM2.Value > 0 then
    EdMedia2.ValueVariant := QrSumPal2AreaM2.Value / QrSumPal2Pecas.Value
  else
    EdMedia2.ValueVariant := 0;
  //
  if QrSumPal3AreaM2.Value > 0 then
    EdMedia3.ValueVariant := QrSumPal3AreaM2.Value / QrSumPal3Pecas.Value
  else
    EdMedia3.ValueVariant := 0;
  //
  if QrSumPal4AreaM2.Value > 0 then
    EdMedia4.ValueVariant := QrSumPal4AreaM2.Value / QrSumPal4Pecas.Value
  else
    EdMedia4.ValueVariant := 0;
  //
  if QrSumPal5AreaM2.Value > 0 then
    EdMedia5.ValueVariant := QrSumPal5AreaM2.Value / QrSumPal5Pecas.Value
  else
    EdMedia5.ValueVariant := 0;
  //
  if QrSumPal6AreaM2.Value > 0 then
    EdMedia6.ValueVariant := QrSumPal6AreaM2.Value / QrSumPal6Pecas.Value
  else
    EdMedia6.ValueVariant := 0;
  //
  if CkDesnate.Checked then
    ReopenDesnate(True);
  if CkEqualize.Checked then
    ReopenEqualize(True);
end;

procedure TFmVSReclassifOneOld.AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst:
  Integer);
var
  Controle: Integer;
begin
  if ClaAPalOri <> 0 then
  begin
    Controle   := ClaAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], False);
  end;
  //
  if RclAPalOri <> 0 then
  begin
    Controle   := RclAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], False);
  end;
end;

function TFmVSReclassifOneOld.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  Result := Geral.IMV(CompName[Length(CompName)]);
end;

function TFmVSReclassifOneOld.BoxInBoxes(Box: Integer): Boolean;
(*
var
  I: Integer;
*)
begin
(*
  Result := False;
  if Box <> 0 then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if FBoxes[I] then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
*)
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSReclassifOneOld.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSReclassifOneOld.BtImprimeClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(4);
end;

procedure TFmVSReclassifOneOld.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaRclCab();
end;

procedure TFmVSReclassifOneOld.CBDigitadorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CBRevisorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSReclassifOneOld.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneOld.BtClassesGeradasClick(Sender: TObject);
begin
  VS_PF.ImprimeReclassOC(QrVSPaRclCabCodigo.Value, QrVSPaRclCabCacCod.Value);
end;

procedure TFmVSReclassifOneOld.BtDigitacaoClick(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdit21.Text;
  //
  if InputQuery('Itens a reclassificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    for I := 1 to Vezes do
    begin
      if QrSumTFALTA_PECA.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Random(7);
        if BoxInBoxes(Box) then
        begin
          OK := True;
          EdBox.ValueVariant := Box;
        end;
      end;
      //
      if RGFrmaIns.Itemindex = 3 then
      begin
        if QrSumTFALTA_PECA.Value > 1 then
        begin
          Area := QrSumTFALTA_AREA.Value / QrSumTFALTA_PECA.Value;
          Randomize;
          Desvio := Random(3000);
          Fator := 1 + ((Desvio - 1500) / 10000);
          Area := Area * Fator;
        end else
          Area := QrSumTFALTA_AREA.Value;
        EdArea.ValueVariant := Area * 100;
        InsereCouroAtual();
      end;
      //
    end;
  end;
end;

procedure TFmVSReclassifOneOld.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
    EdSubClass.SetFocus
  else
  if (EdBox.Text = '-') then
    ExcluiUltimoCouro();
(*
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if RGFrmaIns.ItemIndex < 3 then
    begin
      if EdArea.ValueVariant >= 0.01 then
        //InsereCouroAtual()
      else
        Geral.MB_Erro('�rea n�o definida!');
    end else
      //EdArea.SetFocus;
      //InsereCouroAtual();
      ;
  end else
  if (EdBox.Text = '-') then
    ExcluiUltimoCouro();
*)
end;

procedure TFmVSReclassifOneOld.EdBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Box: Integer;
*)
begin
(*
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
        InsereCouroAtual();
  end;
*)
end;

procedure TFmVSReclassifOneOld.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneOld.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneOld.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSReclassifOneOld.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSReclassifOneOld.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
  ExigeSubClass: Boolean;
begin
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
      //
      InsereCouroAtual();
    end;
  end;
end;

function TFmVSReclassifOneOld.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  ExigeSubClass: Boolean;
(*
  FromBox: Variant;
  ValorSum: Double;
*)
{
  procedure InsereBaixa();
  const
    Observ       = '';
    LnkNivXtr1   = 0;
    LnkNivXtr2   = 0;
    CliVenda     = 0;
    CustoMOKg    = 0;
    CustoMOTot   = 0;
    ValorMP      = 0;
    QtdGerPeca   = 0;
    QtdGerPeso   = 0;
    QtdGerArM2   = 0;
    QtdGerArP2   = 0;
    AptoUso      = 0;
    FornecMO     = 0;
    NotaMPAG     = 0;
var
    Codigo, MovimCod, MovimTwn, Empresa, Fornecedor, Pallet, GraGruX, Controle,
    Ficha, DstNivel1, DstNivel2, SerieFch, SrcGGX, DstGGX, SrcNivel1,
    SrcNivel2, CacCod: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, CustoM2: Double;
    DataHora, Marca: String;
    //
    MovimNiv: TEstqMovimNiv;
    MovimID, SrcMovID, DstMovID: TEstqMovimID;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVsiDest, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovits ',
    'WHERE Controle=' + Geral.FF0(VMI_Dest),
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVsiSorc, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovits ',
    'WHERE Controle=' + Geral.FF0(QrSorcesVMI_Sorc.Value),
    '']);
    //
    Codigo       := QrVsiDestCodigo.Value;
    Controle     := 0;
    MovimCod     := QrVsiDestMovimCod.Value;
    MovimTwn     := QrVsiDestMovimTwn.Value;
    Empresa      := QrVsiDestEmpresa.Value;
    Fornecedor   := QrVsiDestTerceiro.Value;
    MovimID      := TEstqMovimID(QrVsiDestMovimID.Value);
    MovimNiv     := eminSorcReclass;
    Pallet       := 0;
    GraGruX      := 0; // O que fazer?
    Pecas        := -QrSorcesPecas.Value;
    PesoKg       := -0;
    AreaM2       := -QrSorcesAreaM2.Value;
    AreaP2       := -QrSorcesAreaP2.Value;
    ValorT       := 0; // Calculo abaixo!
    DataHora     := Geral.FDT(DModG.ObtemAgora(), 109);
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Ficha        := 0;
    SerieFch     := 0;
    SrcGGX       := 0;
    Marca        := '';
    //SrcMovimNiv := TEstqMovimNiv(BxaMovimNiv);
    //
    DstMovID     := TEstqMovimID(QrVsiDestMovimID.Value);
    DstNivel1    := QrVsiDestCodigo.Value;
    DstNivel2    := VMI_Dest;
    DstGGX       := QrVsiDestGraGruX.Value;
    //
    if QrSorcesVMI_Sorc.Value <> 0 then
    begin
      GraGruX    := QrVsiSorcGraGruX.Value;
      SrcNivel1  := QrVsiSorcCodigo.Value;
      SrcNivel2  := QrVsiSorcControle.Value;
      Ficha      := QrVsiSorcFicha.Value;
      SerieFch   := QrVsiSorcSerieFch.Value;
      SrcGGX     := QrVsiSorcGraGruX.Value;
      Marca      := QrVsiSorcMarca.Value;
      //
      if QrVsiSorcAreaM2.Value <> 0 then
        CustoM2 := QrVsiSorcValorT.Value / QrVsiSorcAreaM2.Value
      else
        CustoM2 := 0;
      ValorT  := AreaM2 * CustoM2;
      //
      ValorSum := ValorSum + ValorT;
    end else
    begin
      //cuidado com o sorce = 0 !!
      GraGruX    := QrVSPallet0GraGruX.Value;
      MovimID    := TEstqMovimID(emidSemOrigem);
    end;
    //
    // Gerar baixas!
    Pecas     := -QrSorcesPecas.Value;
    PesoKg    := -0;
    AreaM2    := -QrSorcesAreaM2.Value;
    AreaP2    := -QrSorcesAreaP2.Value;

    Controle  := UMyMod.BPGS1I32('vsmovits', 'Controle', '', '', tsPos, stIns, Controle);
    //
    if VS_PF.InsUpdVSMovIts1(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CustoMOKg,
    CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca) then
    begin
      // Atualizar VMI_Bxa do vscacitsa
      CacCod    := QrVSPaRclCabCacCod.Value;
      Codigo    := QrVSPaRclCabCodigo.Value;
      VSPallet  := QrVSPaRclCabVSPallet.Value;
      VMI_Sorc  := QrSorcesVMI_Sorc.Value;
      //VMI_Dest  := VMI_Dest;
      //
      VMI_Baix  := Controle;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
      'VMI_Baix'], [
      'CacCod', 'Codigo',
      'VMI_Sorc', 'VMI_Dest'], [
      VMI_Baix], [
      CacCod, Codigo,
      VMI_Sorc, VMI_Dest], False);
      //
      //Atualizar saldo Virtual dosVMI de origem!
      VS_PF.AtualizaSaldoIMEI(VMI_Sorc, True);
    end;
  end;
}
  var
(*
    VMI_Validos, Marca: String;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
    //Pallet,
    SerieFch, Ficha, Controle, Terceiro: Integer;
    QrValidos: TmySQLQuery;
*)
    //
    ReabreVSPaRclCab: Boolean;
begin
  Result := False;
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  Result := VS_PF.EncerraPalletReclassificacaoNew(QrVSPaRclCabCacCod.Value,
  QrVSPaRclCabCodigo.Value, QrVSPaRclCabVSPallet.Value, Box, VSPaClaIts,
  VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, EncerrandoTodos, Pergunta,
  ReabreVSPaRclCab);
  if ReabreVSPaRclCab then
      ReopenVSPaRclCab();
{
  FromBox := Box;
  //
  if Geral.MB_Pergunta('Deseja realmente encerrar o pallet ' +
  Geral.FF0(VSPallet) +  ' ?') = ID_YES then
  begin
    ValorSum := 0;
    VMI_Validos := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrSorces, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
    'SUM(AreaP2) AreaP2,',
    'VSPaRclIts, VSPallet, VMI_Sorc',
    'FROM vscacitsa ',
    'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
    'GROUP BY VMI_Sorc ',
    'ORDER BY VMI_Sorc DESC']);
    QrSorces.First;
    while not QrSorces.Eof do
    begin
      if QrSorcesVMI_Sorc.Value <> 0 then
      begin
        if VMI_Validos <> '' then
          VMI_Validos := VMI_Validos + ', ';
        VMI_Validos := VMI_Validos + Geral.FF0(QrSorcesVMI_Sorc.Value);
      end;
      //
      InsereBaixa();
      //
      QrSorces.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumSorc, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
    'SUM(AreaP2) AreaP2 ',
    'FROM vscacitsa ',
    'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
    '']);
    //
    Pecas        := QrSumSorcPecas.Value;
    PesoKg       := 0;
    AreaM2       := QrSumSorcAreaM2.Value;
    AreaP2       := QrSumSorcAreaP2.Value;
    ValorT       := - ValorSum;
    //
    SerieFch   := 0;
    Ficha      := 0;
    Marca      := '';
    //
    if VMI_Validos <> '' then
    begin
      QrValidos := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(QrValidos, Dmod.MyDB, [
        'SELECT ',
        'IF(COUNT(DISTINCT wmi.SerieFch, wmi.Ficha) <> 1, "0", "1") Series_E_Fichas, ',
        'IF(COUNT(DISTINCT wmi.SerieFch) <> 1, 0.000, wmi.SerieFch) SerieFch, ',
        'IF(COUNT(DISTINCT wmi.Ficha) <> 1, 0.000, wmi.Ficha) Ficha, ',
        'IF(COUNT(DISTINCT wmi.Terceiro) <> 1, 0.000, wmi.Terceiro) Terceiro, ',
        'IF(COUNT(DISTINCT wmi.Marca) <> 1, "", wmi.Marca) Marca ',
        'FROM vsmovits wmi ',
        'WHERE wmi.Controle IN (' + VMI_Validos + ')',
        '']);
        if QrValidos.FieldByName('Series_E_Fichas').AsString = '1' then
        begin
          SerieFch := Trunc(QrValidos.FieldByName('SerieFch').AsFloat);
          Ficha    := Trunc(QrValidos.FieldByName('Ficha').AsFloat);
        end;
        Terceiro   := Trunc(QrValidos.FieldByName('Terceiro').AsFloat);
        Marca      := QrValidos.FieldByName('Marca').AsString;
      finally
        QrValidos.Free;
      end;
    end;
    //
    Controle := VMI_Dest;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
    (*'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa',*) 'Terceiro',
    (*'CliVenda', 'MovimID', 'LnkNivXtr1',
    'LnkNivXtr2', 'DataHora', 'Pallet',*)
    (*'GraGruX',*) 'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    (*'SrcMovID', 'SrcNivel1', 'SrcNivel2',
    'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2',
    'Observ',*) 'Ficha', (*'Misturou',
    'FornecMO', 'CustoMOKg', 'CustoMOTot',
    'ValorMP', 'DstMovID', 'DstNivel1',
    'DstNivel2', 'QtdGerPeca', 'QtdGerPeso',
    'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
    'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
    'AptoUso',*) 'SerieFch', (*'NotaMPAG',
    'SrcGGX', 'DstGGX',*) 'Marca'], [
    'Controle'], [
    (*Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa,*) Terceiro,
    (*CliVenda, MovimID, LnkNivXtr1,
    LnkNivXtr2, DataHora, Pallet,*)
    (*GraGruX,*) Pecas, PesoKg,
    AreaM2, AreaP2, ValorT,
    (*SrcMovID, SrcNivel1, SrcNivel2,
    SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
    Observ,*) Ficha, (*Misturou,
    FornecMO, CustoMOKg, CustoMOTot,
    ValorMP, DstMovID, DstNivel1,
    DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, QtdAntPeca,
    QtdAntPeso, QtdAntArM2, QtdAntArP2,
    AptoUso,*) SerieFch, (*NotaMPAG,
    SrcGGX, DstGGX,*) Marca], [
    Controle], True) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Atualizar saldo virtual do arrtigo novo gerado
      VS_PF.AtualizaSaldoIMEI(Controle, True);
      //
    end;
    if VS_PF.EncerraPallet(VSPallet, EncerrandoTodos, FromBox, emidReclasVS, False) then
    begin
      RemovePallet(Box, not EncerrandoTodos);
      Result := True;
      //EdArea.SetFocus;  gerou erro! ver!!!
    end;
  end;
}
end;

procedure TFmVSReclassifOneOld.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaRclCab();
  end;
end;

procedure TFmVSReclassifOneOld.EstaOCOrdemdeclassificao1Click(Sender: TObject);
var
  DtHrFimCla: String;
  //
  function DefPalRclIts(Qry: TmySQLQuery): Integer;
  begin
    if (Qry <> nil) and (Qry.State <> dsInactive) then
      Result := Qry.FieldByName('Controle').AsInteger
    else
      Result := 0;
  end;
  procedure EncerraBox(Box, PalRclIts: Integer);
  var
    VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
    ExigeSubClass: Boolean;
  begin
    if PalRclIts <> 0 then
    begin
      if ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
      ExigeSubClass) then
        VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      // Encerra IME-I
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
      'DtHrFim'], ['Controle'], [DtHrFimCla], [PalRclIts], True) then
        VS_PF.AtualizaStatPall(VSPallet);
    end;
  end;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
var
  VSPalRclIts1, VSPalRclIts2, VSPalRclIts3, VSPalRclIts4, VSPalRclIts5,
  VSPalRclIts6, Codigo, I, Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
  Fecha: Boolean;
begin
  if Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES then
  begin
    VSPalRclIts1 := DefPalRclIts(QrVSPalRclIts1);
    VSPalRclIts2 := DefPalRclIts(QrVSPalRclIts2);
    VSPalRclIts3 := DefPalRclIts(QrVSPalRclIts3);
    VSPalRclIts4 := DefPalRclIts(QrVSPalRclIts4);
    VSPalRclIts5 := DefPalRclIts(QrVSPalRclIts5);
    VSPalRclIts6 := DefPalRclIts(QrVSPalRclIts6);
    //
    ZeraEstoquePalletOrigem();
     //
    (* DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaRclCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    *)
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    //Codigo     := QrVSGerArtNewCodigo.Value;
    Codigo := QrVSPaRclCabVSGerRclA.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerrcla', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaRclCabCodigo.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06
      ], [Codigo], True) then
      begin
(*
        if not SubstituiOC() then
          Close;
*)
        Fecha := not SubstituiOC();
        //
        EncerraBox(1, VSPalRclIts1);
        EncerraBox(2, VSPalRclIts2);
        EncerraBox(3, VSPalRclIts3);
        EncerraBox(4, VSPalRclIts4);
        EncerraBox(5, VSPalRclIts5);
        EncerraBox(6, VSPalRclIts6);
        //
        if Fecha then
          Close
        else
          ReopenVSPaRclCab();
      end;
    end;
  end;
end;

procedure TFmVSReclassifOneOld.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel, False);
end;

procedure TFmVSReclassifOneOld.ExcluiUltimoCouro();
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Controle, Box, All_VSPaRclIts, All_VSPallet,
  Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  ExigeSubClass: Boolean;
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  if VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) =
  ID_YES then
  begin
    EdBox.ValueVariant := 0;
    EdArea.ValueVariant := 0;
    //
    (*
    if PnDigitacao.Enabled then
    begin
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    *)
    if not FCriando and EdArea.CanFocus then
      EdArea.SetFocus;
    if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus
    else
    if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
    EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
      EdBox.SetFocus;
    //
    ReopenItens(All_VSPaRclIts, All_VSPallet, QrItens, QrSum, QrSumPal);
    AtualizaInfoOC();
  end;
end;

procedure TFmVSReclassifOneOld.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSReclassifOneOld.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriando := True;
  //
  FDifTime := DModG.ObtemAgora() - Now();
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    FBoxes[I] := False;
  RealinhaBoxes();
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  //
  RGFrmaIns.ItemIndex := 3;
end;

procedure TFmVSReclassifOneOld.FormResize(Sender: TObject);
begin
  RealinhaBoxes();
end;

procedure TFmVSReclassifOneOld.InsereCouroAtual();
const
  VSPaClaIts = 0;
var
  Box, Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  CacCod, CacID, ClaAPalOri, RclAPalOri, Revisor, Digitador, VSCacItsAOri,
  FrmaIns: Integer;
  Pecas, AreaM2, AreaP2: Double;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  DataHora, SubClass: String;
  Controle: Int64;
  Tempo: TDateTime;
  ExigeSubClass: Boolean;
begin
  //Geral.MB_Aviso('Fazer classificacao');
  if EdArea.ValueVariant < 0.01 then
  begin
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Enabled and EdArea.CanFocus then
    begin
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    Exit;
  end;
  Box := EdBox.ValueVariant;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    CacCod         := FCacCod;
    CacID          := Integer(emidReclasVS);
    Codigo         := FCodigo;
    Controle       := 0;
    ObtemDadosCouroOrigem(ClaAPalOri, RclAPalOri, VMI_Sorc, VSCacItsAOri);
    //VSPaRclIts     := ;   ver abaixo!!!
    //VSPallet       := ;   ver abaixo!!!
    //VMI_Sorc       := ;   ver abaixo!!!
    //VMI_Dest       := ;   ver abaixo!!!
    Box            := EdBox.ValueVariant;
    Revisor        := EdRevisor.ValueVariant;
    Digitador      := EdDigitador.ValueVariant;
    DataHora       := Geral.FDT(Now() + FDifTime, 109);
    FrmaIns        := RGFrmaIns.ItemIndex;
    SubClass       := dmkPF.SoTextoLayout(EdSubClass.Text);
    //AreaM2         := ;
    //AreaP2         := ;
    Pecas          := 1;
    if QrVSPaRclCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
      Exit;
    if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
    ExigeSubClass) then
      Exit;
    if MyObjects.FIC(ExigeSubClass and (Trim(EdSubClass.Text) = ''), nil,
    'Informe a sub classe!') then
      Exit;
    //
    //VMI_Dest :=
    Controle := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'DataHora'(*, 'Sumido',
    'RclAPalDst'*), 'FrmaIns', 'SubClass'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix,
    VMI_Dest, Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, DataHora(*, Sumido,
    RclAPalDst*), FrmaIns, SubClass], [
    Controle], False) then
    begin
      EdSubClass.Text := '';
      Tempo := Now();
      VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
        QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      Tempo := Now() - Tempo;
      EdTempo.Text := FormatDateTime('ss:zzz', Tempo);
      //
      AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, Controle);
      ReopenItensAClassificarPallet();
      //
      EdBox.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
        EdArea.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
      begin
        EdArea.SetFocus;
        EdArea.SelectAll;
      end else
      begin
        EdBox.SetFocus;
        EdBox.SelectAll;
      end;
      //
      ReopenItens(VSPaRclIts, VSPallet, QrItens, QrSum, QrSumPal);
      AtualizaInfoOC();
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSReclassifOneOld.LiberaDigitacao;
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  BtDigitacao.Enabled := VAR_USUARIO = -1;
  BtDigitacao.Visible := Libera;
  //
  if not FCriando and EdArea.CanFocus then
    EdArea.SetFocus;
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
  else
  if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
  EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
    EdBox.SetFocus
end;

procedure TFmVSReclassifOneOld.LiberaDigitacaoManual(Libera: Boolean);
begin
  PnArea.Enabled := Libera;
end;

procedure TFmVSReclassifOneOld.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  QrSum, QrSumPal: TmySQLQuery;
  EdPercent, EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass);
  case Box of
    1:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    3:
    begin
      QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      EdPercent := EdPercent3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      EdPercent := EdPercent4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      EdPercent := EdPercent5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      EdPercent := EdPercent6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaRclCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O DO PALLET  ' + Geral.FF0(QrVSPaRclCabVSPallet.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
end;

procedure TFmVSReclassifOneOld.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSReclassifOneOld.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSReclassifOneOld.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  case Box of
    1: Result := CkSubClass1.Checked;
    2: Result := CkSubClass2.Checked;
    3: Result := CkSubClass3.Checked;
    4: Result := CkSubClass4.Checked;
    5: Result := CkSubClass5.Checked;
    6: Result := CkSubClass6.Checked;
  end;
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSReclassifOneOld.ObtemDadosBox(const Box: Integer; var
VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
Boolean): Boolean;
begin
  (*if (QrItensACP.State <> dsInactive) and (QrItensACP.RecordCount > 0) then
    VMI_Sorc := QrItensACPVMI_Dest.Value
  else
  *)
    VMI_Sorc := QrVSPaRclCabVSMovIts.Value;
  //
  case Box of
    1:
    begin
      VSPaRclIts := QrVSPalRclIts1Controle.Value;
      VSPallet   := QrVSPallet1Codigo.Value;
      VMI_Baix   := QrVSPalRclIts1VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts1VMI_Dest.Value;
      ExigeSubClass := CkSubClass1.Checked;
    end;
    2:
    begin
      VSPaRclIts := QrVSPalRclIts2Controle.Value;
      VSPallet   := QrVSPallet2Codigo.Value;
      VMI_Baix   := QrVSPalRclIts2VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts2VMI_Dest.Value;
      ExigeSubClass := CkSubClass2.Checked;
    end;
    3:
    begin
      VSPaRclIts := QrVSPalRclIts3Controle.Value;
      VSPallet   := QrVSPallet3Codigo.Value;
      VMI_Baix   := QrVSPalRclIts3VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts3VMI_Dest.Value;
      ExigeSubClass := CkSubClass3.Checked;
    end;
    4:
    begin
      VSPaRclIts := QrVSPalRclIts4Controle.Value;
      VSPallet   := QrVSPallet4Codigo.Value;
      VMI_Baix   := QrVSPalRclIts4VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts4VMI_Dest.Value;
      ExigeSubClass := CkSubClass4.Checked;
    end;
    5:
    begin
      VSPaRclIts := QrVSPalRclIts5Controle.Value;
      VSPallet   := QrVSPallet5Codigo.Value;
      VMI_Baix   := QrVSPalRclIts5VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts5VMI_Dest.Value;
      ExigeSubClass := CkSubClass5.Checked;
    end;
    6:
    begin
      VSPaRclIts := QrVSPalRclIts6Controle.Value;
      VSPallet   := QrVSPallet6Codigo.Value;
      VMI_Baix   := QrVSPalRclIts6VMI_Baix.Value;
      VMI_Dest   := QrVSPalRclIts6VMI_Dest.Value;
      ExigeSubClass := CkSubClass6.Checked;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemDadosBox"');
      Exit;
    end;
  end;
  Result := True;
end;

function TFmVSReclassifOneOld.ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri,
  VMI_Sorc, VSCacItsAOri: Integer): Boolean;
begin
  Result         := False;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  VMI_Sorc       := 0;
  VSCacItsAOri   := 0;

  if RGFrmaIns.ItemIndex < 3 then
  begin
    if (QrItensACP.State = dsInactive)
    or (QrItensACP.RecordCount = 0) then
      Exit;
  end;
  //?
  //VMI_Sorc := QrItensACPVMI_Dest.Value;
  VMI_Sorc := QrVSPaRclCabVSMovIts.Value;
  case RGFrmaIns.ItemIndex of
    0,
    1,
    2:
    begin
      Result := True;
      case TEstqMovimID(QrItensACPCacID.Value) of
        (*7*)emidClassArtXXUni: ClaAPalOri := QrItensACPControle.Value;
        (*8*)emidReclasVS:      RclAPalOri := QrItensACPControle.Value;
        else
        begin
          Result := False;
          Geral.MB_Erro(
          '"TEstqMovimID" n�o definido em "ObtemDadosCouroOrigem()"');
        end;
      end;
    end;
    3:
    begin
      Result := True;
    end;
    else Geral.MB_Aviso(
      'Forma de reclassifica��o n�o definida em "ObtemDadosCouroOrigem()"');
  end;
end;

function TFmVSReclassifOneOld.ObtemQryesBox(const Box: Integer; var QrVSPallet,
  QrItens, QrSum, QrSumPal: TmySQLQuery): Boolean;
begin
  case Box of
    1:
    begin
      QrVSPallet := QrVSPallet1;
      QrItens    := QrItens1;
      QrSum      := QrSum1;
      QrSumPal   := QrSumPal1;
    end;
    2:
    begin
      QrVSPallet := QrVSPallet2;
      QrItens    := QrItens2;
      QrSum      := QrSum2;
      QrSumPal   := QrSumPal2;
    end;
    3:
    begin
      QrVSPallet := QrVSPallet3;
      QrItens    := QrItens3;
      QrSum      := QrSum3;
      QrSumPal   := QrSumPal3;
    end;
    4:
    begin
      QrVSPallet := QrVSPallet4;
      QrItens    := QrItens4;
      QrSum      := QrSum4;
      QrSumPal   := QrSumPal4;
    end;
    5:
    begin
      QrVSPallet := QrVSPallet5;
      QrItens    := QrItens5;
      QrSum      := QrSum5;
      QrSumPal   := QrSumPal5;
    end;
    6:
    begin
      QrVSPallet := QrVSPallet6;
      QrItens    := QrItens6;
      QrSum      := QrSum6;
      QrSumPal   := QrSumPal6;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemQryesBox"');
      Exit;
    end;
  end;
  Result := True;
end;

procedure TFmVSReclassifOneOld.PMEncerraPopup(Sender: TObject);
begin
  Palletdobox11.Enabled :=
    (QrVSPallet1.State <> dsInactive) and (QrVSPallet1.RecordCount > 0);
  Palletdobox21.Enabled :=
    (QrVSPallet2.State <> dsInactive) and (QrVSPallet2.RecordCount > 0);
  Palletdobox31.Enabled :=
    (QrVSPallet3.State <> dsInactive) and (QrVSPallet3.RecordCount > 0);
  Palletdobox41.Enabled :=
    (QrVSPallet4.State <> dsInactive) and (QrVSPallet4.RecordCount > 0);
  Palletdobox51.Enabled :=
    (QrVSPallet5.State <> dsInactive) and (QrVSPallet5.RecordCount > 0);
  Palletdobox61.Enabled :=
    (QrVSPallet6.State <> dsInactive) and (QrVSPallet6.RecordCount > 0);
end;

procedure TFmVSReclassifOneOld.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  //AdicionarPallet1.Enabled := (QrVSPallet1.State = dsInactive) or (QrVSPallet1.RecordCount = 0);
  //MyObjects.HabilitaMenuItemCabUpd(EncerrarPallet1, QrVSPallet1);
  //MyObjects.HabilitaMenuItemCabDel(RemoverPallet1, QrVSPallet1, QrItens1);
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSReclassifOneOld.QrItensACPAfterOpen(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneOld.QrItensACPAfterScroll(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneOld.QrItensACPBeforeClose(DataSet: TDataSet);
begin
  if RGFrmaIns.ItemIndex < 3 then
    EdArea.ValueVariant := 0;
end;

procedure TFmVSReclassifOneOld.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrSumTFALTA_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTAreaM2.Value;
    1: QrSumTFALTA_AREA.Value := QrVSGerArtNewAreaP2.Value - QrSumTAreaP2.Value;
    else QrSumTFALTA_AREA.Value := 0;
  end;
  QrSumTFALTA_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTPecas.Value;
end;

procedure TFmVSReclassifOneOld.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSReclassifOneOld.QrVSPaRclCabAfterScroll(DataSet: TDataSet);
var
  Campo: String;
  I: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
  begin
    if QrVSPaRclCab.FieldByName(VS_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  case QrVSPaRclCabTipoArea.Value of
    0: Campo := 'AreaM2';
    1: Campo := 'AreaP2';
    else Campo := 'Area?2';
  end;
  DBGItensACP.Columns[2].FieldName := Campo;
  //
  DBGAll.Columns[2].FieldName := Campo;
  //
  DBGPallet1.Columns[0].FieldName := Campo;
  DBGPallet2.Columns[0].FieldName := Campo;
  DBGPallet3.Columns[0].FieldName := Campo;
  DBGPallet4.Columns[0].FieldName := Campo;
  DBGPallet5.Columns[0].FieldName := Campo;
  DBGPallet6.Columns[0].FieldName := Campo;
  //
  DBEdArea1.DataField := Campo;
  DBEdArea2.DataField := Campo;
  DBEdArea3.DataField := Campo;
  DBEdArea4.DataField := Campo;
  DBEdArea5.DataField := Campo;
  DBEdArea6.DataField := Campo;
  //
  DBEDAreaT.DataField := Campo;
  //
  VerificaBoxes();
  //
  ReopenVSGerArtDst();
  //
  ReopenVSPallet(1, QrVSPallet1, QrVSPalRclIts1, QrVSPaRclCabLstPal01.Value);
  ReopenVSPallet(2, QrVSPallet2, QrVSPalRclIts2, QrVSPaRclCabLstPal02.Value);
  ReopenVSPallet(3, QrVSPallet3, QrVSPalRclIts3, QrVSPaRclCabLstPal03.Value);
  ReopenVSPallet(4, QrVSPallet4, QrVSPalRclIts4, QrVSPaRclCabLstPal04.Value);
  ReopenVSPallet(5, QrVSPallet5, QrVSPalRclIts5, QrVSPaRclCabLstPal05.Value);
  ReopenVSPallet(6, QrVSPallet6, QrVSPalRclIts6, QrVSPaRclCabLstPal06.Value);
  //
  ReopenItens(QrVSPalRclIts1Controle.Value, QrVSPallet1Codigo.Value, QrItens1, QrSum1, QrSumPal1);
  ReopenItens(QrVSPalRclIts2Controle.Value, QrVSPallet2Codigo.Value, QrItens2, QrSum2, QrSumPal2);
  ReopenItens(QrVSPalRclIts3Controle.Value, QrVSPallet3Codigo.Value, QrItens3, QrSum3, QrSumPal3);
  ReopenItens(QrVSPalRclIts4Controle.Value, QrVSPallet4Codigo.Value, QrItens4, QrSum4, QrSumPal4);
  ReopenItens(QrVSPalRclIts5Controle.Value, QrVSPallet5Codigo.Value, QrItens5, QrSum5, QrSumPal5);
  ReopenItens(QrVSPalRclIts6Controle.Value, QrVSPallet6Codigo.Value, QrItens6, QrSum6, QrSumPal6);
  //
  AtualizaInfoOC();
  //
  ReopenVSPallet(0, QrVSPallet0, nil, QrVSPaRclCabVSPallet.Value);
  ReopenItensAClassificarPallet();
end;

procedure TFmVSReclassifOneOld.QrVSPaRclCabBeforeClose(DataSet: TDataSet);
begin
  QrVSGerArtNew.Close;
  //
  QrVSPallet1.Close;
  QrVSPallet2.Close;
  QrVSPallet3.Close;
  QrVSPallet4.Close;
  QrVSPallet5.Close;
  QrVSPallet6.Close;
  //
  QrVSPalRclIts1.Close;
  QrVSPalRclIts2.Close;
  QrVSPalRclIts3.Close;
  QrVSPalRclIts4.Close;
  QrVSPalRclIts5.Close;
  QrVSPalRclIts6.Close;
  //
  QrVSPallet2.Close;
  QrVSPallet3.Close;
  QrVSPallet4.Close;
  QrVSPallet5.Close;
  QrVSPallet6.Close;
  //
  QrItens1.Close;
  QrItens2.Close;
  QrItens3.Close;
  QrItens4.Close;
  QrItens5.Close;
  QrItens6.Close;
  //
  QrSum1.Close;
  QrSum2.Close;
  QrSum3.Close;
  QrSum4.Close;
  QrSum5.Close;
  QrSum6.Close;
  //
end;

procedure TFmVSReclassifOneOld.QrVSPaRclCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrVSPaRclCabNO_TIPO.Value := 'm�';
    1: QrVSPaRclCabNO_TIPO.Value := 'ft�';
    else QrVSPaRclCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaRclCabNO_TIPO.Value;
end;

procedure TFmVSReclassifOneOld.RealinhaBoxes();
var
  H_T, W_T, H_1, W_1: Integer;
begin
  H_T := PnBoxesAll.Height;
  W_T := PnBoxesAll.Width;
  //
  H_1 := H_T div 2;
  W_1 := W_T div 3;
  //
  PnBoxesT01.Height := H_1;
  //PnBoxesT02.Height := H_1;

  PnBoxT1L1.Width := W_1;
  PnBoxT1L2.Width := W_1;
  PnBoxT1L3.Width := W_1;

  PnBoxT2L1.Width := W_1;
  PnBoxT2L2.Width := W_1;
  PnBoxT2L3.Width := W_1;
end;


procedure TFmVSReclassifOneOld.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo, DtHrFim: String;
  Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
  Campo := VS_PF.CampoLstPal(Box);
  Codigo := QrVSPaRclCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  if VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaRclIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaRclCab();
      end;
    end;
  end;
end;

procedure TFmVSReclassifOneOld.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneOld.Reopendesnate(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  Codigo := EdDesnate.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do desnate!');
  end else
    VS_PF.ReopenDesnate(Codigo, GraGruX, QrVSCacIts, QrVSCacSum);
end;

procedure TFmVSReclassifOneOld.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
end;

procedure TFmVSReclassifOneOld.ReopenItens(VSPaRclIts, VSPallet: Integer; QrIts,
  QrSum, QrSumPal: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaRclIts=' + Geral.FF0(VSPaRclIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  'ORDER BY Controle DESC ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaRclIts=' + Geral.FF0(VSPaRclIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  '']);
  //
  ReopenSumPal(QrSumPal, VSPallet);
end;

procedure TFmVSReclassifOneOld.ReopenItensAClassificarPallet();
var
  Forma: Integer;
  SQL_Forma: String;
begin
  LiberaDigitacaoManual(False);
  case RGFrmaIns.ItemIndex of
    0: SQL_Forma := Geral.ATS(['AND Sumido=0 ','AND RclAPalDst=0 ']);
    1: SQL_Forma := Geral.ATS(['AND Sumido=1 ','AND RclAPalDst=0 ']);
    2: SQL_Forma := Geral.ATS(['AND RclAPalDst=0 ']);
    3: LiberaDigitacaoManual(True);
    else Geral.MB_Aviso(
    'Forma de digita��o n�o definida em "ReopenItensAClassificarPallet()"');
  end;
  //
  if RGFrmaIns.ItemIndex = 3 then
  begin
    QrItensACP.Close;
    EdArea.ValueVariant := 0;
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrItensACP, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(QrVSPaRclCabVSPallet.Value),
  SQL_Forma,
  'ORDER BY Controle DESC ',
  '']);
end;

procedure TFmVSReclassifOneOld.ReopenSumPal(QrSumPal: TmySQLQuery;
  VSPallet: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPal, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
  '']);
end;

procedure TFmVSReclassifOneOld.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(wmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_TERCEIRO, ',
  'IF(wmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 ',
  'FROM vsmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.Controle=' + Geral.FF0(QrVSPaRclCabVSMovIts.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSReclassifOneOld.ReopenVSPaRclCab();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vsparclcaba pcc',
  'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
*)
(*
SELECT vga.GraGruX, vga.Nome,
vga.TpAreaRcl, vga.Empresa, vga.MovimCod, pcc.*,
CONCAT(gg1.Nome,
IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),
IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))
NO_PRD_TAM_COR,
IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA
FROM vsparclcaba pcc
LEFT JOIN vspalleta vga ON vga.Codigo=pcc.vsPallet
LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX
LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC
LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad
LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa
WHERE pcc.Codigo=1
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vga.VSPallet, vga.Codigo VSGerRClA ',
  'FROM vsparclcaba pcc',
  'LEFT JOIN vsgerrcla  vga ON vga.Codigo=pcc.vsgerrcl',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
end;

procedure TFmVSReclassifOneOld.RGFrmaInsClick(Sender: TObject);
begin
  if RGFrmaIns.ItemIndex <> 3 then
  begin
    RGFrmaIns.ItemIndex := 3;
    Exit;
  end;
  ReopenItensAClassificarPallet();
end;

procedure TFmVSReclassifOneOld.SbDesnateClick(Sender: TObject);
begin
  ReopenDesnate(False);
end;

procedure TFmVSReclassifOneOld.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

function TFmVSReclassifOneOld.SubstituiOC(): Boolean;
  function MostraFormVSReclassPrePal(SQLType: TSQLType; Pallet1, Pallet2,
  Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer):
  Integer;
  begin
    Result := 0;
    if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
    begin
      FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
      //
      FmVSReclassPrePal.ShowModal;
      Result := FmVSReclassPrePal.FPallet;
      FmVSReclassPrePal.Destroy;
    end;
  end;
var
  Codigo, CacCod, Pallet, Digitador, Revisor: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
(*
  VS_PF.MostraFormVSReclassPrePal(stIns,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant, );
  EXIT;
*)
  Pallet := MostraFormVSReclassPrePal(stIns,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant);
  //
  if Pallet <> 0 then
  begin
    if DBCheck.CriaFm(TFmVSRclArtPrpNew, FmVSRclArtPrpNew, afmoNegarComAviso) then
    begin
      FmVSRclArtPrpNew.ImgTipo.SQLType := stIns;
      //
      FmVSRclArtPrpNew.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNew.CBPallet.KeyValue     := Pallet;
      //
      FmVSRclArtPrpNew.EdPallet1.ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.EdPallet2.ValueVariant := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.EdPallet3.ValueVariant := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.EdPallet4.ValueVariant := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.EdPallet5.ValueVariant := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.EdPallet6.ValueVariant := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.CBPallet1.KeyValue := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.CBPallet2.KeyValue := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.CBPallet3.KeyValue := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.CBPallet4.KeyValue := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.CBPallet5.KeyValue := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.CBPallet6.KeyValue := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.SbPalletClick(Self);
      FmVSRclArtPrpNew.ShowModal;
      Codigo := FmVSRclArtPrpNew.FCodigo;
      CacCod := FmVSRclArtPrpNew.FCacCod;
      MovimID := FmVSRclArtPrpNew.FMovimID;
      FmVSRclArtPrpNew.Destroy;
      //
      if Codigo <> 0 then
      begin
        FCodigo := Codigo;
        FCacCod := CacCod;
        FMovimID := MovimID;
        ReopenVSPaRclCab();
        //
        EdDigitador.ValueVariant := Digitador;
        EdRevisor.ValueVariant   := Revisor;
        Result := True;
      end else
        Result := False;
    end;
  end;
end;

procedure TFmVSReclassifOneOld.TentarFocarEdArea();
begin
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

procedure TFmVSReclassifOneOld.UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaRclIts, All_VSPallet, Box_VSPaRclIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt, SubClass: String;
  Continua: Boolean;
  ExigeSubClass: Boolean;
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      if CampoSubClas then
      begin
        Txt := QrAllSubClass.Value;
        if InputQuery('Altera��o de dados', 'Altera��o da sub classe', Txt) then
        begin
          Subclass := Uppercase(dmkPF.SoTextoLayout(Txt));
          //
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'SubClass'], ['Controle'], [SubClass], [Controle], False);
        end;
      end else
      begin
        Pecas  := 1;
        //
        if QrVSPaRclCabTipoArea.Value = 0 then
          Txt := FloatToStr(QrAllAreaM2.Value * 100)
        else
          Txt := FloatToStr(QrAllAreaP2.Value * 100);
        //
        if InputQuery('Altera��o de dados', 'Altera��o de �rea: (Apenas n�meros sem v�rgula)', Txt) then
        begin
          AreaM2 := Geral.IMV(Txt);
          //
          if QrVSPaRclCabTipoArea.Value = 0 then
          begin
            AreaM2 := AreaM2 / 100;
            AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
          end else
          begin
            AreaP2 := AreaM2 / 100;
            AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
          end;
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
          [Controle], False);
        end;
      end;
    end;
    stDel:
    begin
      Continua := VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      //EdVSMrtCad.Text     := '';
      //CBVSMrtCad.KeyValue := Null;
      if not FCriando and EdArea.CanFocus then
        EdArea.SetFocus;
      if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
      EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
        EdArea.SetFocus
      else
      if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
      EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
        EdBox.SetFocus
    end;
    //
    ReopenItens(All_VSPaRclIts, All_VSPallet, QrItens, QrSum, QrSumPal);
    AtualizaInfoOC();
  end;
end;

procedure TFmVSReclassifOneOld.VerificaAreaAutomatica;
begin
  case QrVSPaRclCabTipoArea.Value of
    0: EdArea.ValueVariant := (QrItensACPAreaM2.Value * 100);
    1: EdArea.ValueVariant := (QrItensACPAreaP2.Value * 100);
    else Geral.MB_Aviso(
      'Tipo de �rea n�o definida em "QrItensACPAfterScroll()"');
  end;
  //
  if not FCriando and EdBox.CanFocus then
    EdBox.SetFocus;
end;

procedure TFmVSReclassifOneOld.VerificaBoxes;
begin
  if not Mostrartodosboxes1.Checked then
  begin
    PnBox01.Visible := QrVSPaRclCabLstPal01.Value <> 0;
    PnBox02.Visible := QrVSPaRclCabLstPal02.Value <> 0;
    PnBox03.Visible := QrVSPaRclCabLstPal03.Value <> 0;
    PnBox04.Visible := QrVSPaRclCabLstPal04.Value <> 0;
    PnBox05.Visible := QrVSPaRclCabLstPal05.Value <> 0;
    PnBox06.Visible := QrVSPaRclCabLstPal06.Value <> 0;
  end else
  begin
    PnBox01.Visible := True;
    PnBox02.Visible := True;
    PnBox03.Visible := True;
    PnBox04.Visible := True;
    PnBox05.Visible := True;
    PnBox06.Visible := True;
  end;
  if ((PnBoxT1L3.Visible = False) or (PnBoxT2L1.Visible = False)) and (PnBoxT1L2.Visible = True) then
    PnBoxT1L2.Align := alLeft
  else
    PnBoxT1L2.Align := alClient;
  //
  if ((PnBoxT2L3.Visible = False) or (PnBoxT2L1.Visible = False)) and (PnBoxT2L2.Visible = True) then
    PnBoxT2L2.Align := alLeft
  else
    PnBoxT2L2.Align := alClient;
  //
  RealinhaBoxes();
end;

function TFmVSReclassifOneOld.ZeraEstoquePalletOrigem(): Boolean;
var
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
begin
  Result := True;
  //
  if QrSumTFALTA_PECA.Value > 0  then
  if Geral.MB_Pergunta(
  'Deseja zerar o estoque residual de ' + FloatToStr(QrSumTFALTA_PECA.Value) +
  ' pe�as do pallet de origem ' + Geral.FF0(QrVSPaRclCabVSPallet.Value) + '?') =
  ID_YES then
  begin
    BxaPecas    := QrSumTFALTA_PECA.Value;
    BxaPesoKg   := 0;
    //
    case QrVSPaRclCabTipoArea.Value of
      0:
      begin
        BxaAreaM2 := QrSumTFALTA_AREA.Value;
        BxaAreaP2 := Geral.ConverteArea(QrSumTFALTA_AREA.Value, ctM2toP2, cfQuarto);
      end;
      1:
      begin
        BxaAreaP2 := QrSumTFALTA_AREA.Value;
        BxaAreaM2 := Geral.ConverteArea(QrSumTFALTA_AREA.Value, ctP2toM2, cfCento);
      end
      else
      begin
        Geral.MB_Aviso(
        'Tipo de �rea desconhecido! Ser� considerada �rea = zero!');
        BxaAreaP2 := 0;
        BxaAreaM2 := 0;
      end
    end;
    BxaValorT   := 0; // Ver o que fazer!
    Result := VS_PF.ZeraEstoquePalletOrigemReclas(QrVSPaRclCabVSPallet.Value,
      QrVSPaRclCabCodigo.Value, QrVSPaRclCabMovimCod.Value,
      QrVSPaRclCabEmpresa.Value, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
      BxaValorT);
  end;
end;

procedure TFmVSReclassifOneOld.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalRclIts: TmySQLQuery; Pallet: Integer);
begin
  if Pallet > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT let.*, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS ',
    'FROM vspalleta let ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'WHERE let.Codigo=' + Geral.FF0(Pallet),
    '']);
    //
    if QrVSPalRclIts <> nil then
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalRclIts, Dmod.MyDB, [
      'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
      'FROM vsparclitsa ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND Tecla=' + Geral.FF0(Tecla),
      'AND VSPallet=' + Geral.FF0(Pallet),
      'ORDER BY DtHrIni DESC, Controle DESC ',
      'LIMIT 1 ',
      '']);
  end else
  begin
    QrVSPallet.Close;
  end;
  //
end;

end.
