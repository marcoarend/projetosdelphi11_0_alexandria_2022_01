object FmVSReclassPrePal: TFmVSReclassPrePal
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-063 :: Prepara'#231#227'o de Pallet para Reclassifica'#231#227'o'
  ClientHeight = 336
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 648
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 600
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 552
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 512
        Height = 32
        Caption = 'Prepara'#231#227'o de Pallet para Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 512
        Height = 32
        Caption = 'Prepara'#231#227'o de Pallet para Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 512
        Height = 32
        Caption = 'Prepara'#231#227'o de Pallet para Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 648
    Height = 174
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 648
      Height = 174
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 648
        Height = 174
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 97
          Width = 644
          Height = 75
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object LaVSRibCad: TLabel
            Left = 8
            Top = 0
            Width = 29
            Height = 13
            Caption = 'Pallet:'
          end
          object SbPallet: TSpeedButton
            Left = 592
            Top = 16
            Width = 21
            Height = 21
            Caption = '>'
            Enabled = False
            Visible = False
          end
          object EdPallet: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPallet
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBPallet: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 529
            Height = 21
            KeyField = 'Pallet'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsPallets
            TabOrder = 1
            dmkEditCB = EdPallet
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CkReclasse: TCheckBox
            Left = 8
            Top = 40
            Width = 513
            Height = 17
            TabStop = False
            Caption = 'Mostrar tamb'#233'm pallets j'#225' em reclassifica'#231#227'o.'
            TabOrder = 2
            OnClick = CkReclasseClick
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 644
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label49: TLabel
            Left = 8
            Top = 40
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object Label35: TLabel
            Left = 8
            Top = 2
            Width = 34
            Height = 13
            Caption = 'Senha:'
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'StqCenLoc'
            UpdCampo = 'StqCenLoc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 63
            Top = 56
            Width = 530
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_LOC_CEN'
            ListSource = DsStqCenLoc
            TabOrder = 2
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7NameFldName = 'Nome'
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdVSPwdDdClas: TEdit
            Left = 8
            Top = 18
            Width = 421
            Height = 20
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 0
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 222
    Width = 648
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 266
    Width = 648
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 502
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 500
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPallets: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Empresa, wmi.MovimID, wmi.GraGruX, '
      'wmi.Pallet, wmi.Codigo VMI_Codi, wmi.Controle VMI_Ctrl, '
      'wmi.MovimNiv VMI_MovimNiv, wmi.Terceiro, wmi.Ficha, '
      'MAX(DataHora) DataHora, SUM(wmi.Pecas) Pecas,   '
      'IF(MIN(AreaM2) > 0.01, 0, SUM(wmi.AreaM2)) AreaM2,   '
      'IF(MIN(AreaP2) > 0.01, 0, SUM(wmi.AreaP2)) AreaP2,   '
      'IF(MIN(PesoKg) > 0.001, 0, SUM(wmi.PesoKg)) PesoKg,   '
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),    '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,   '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE   '
      'FROM vsmovits wmi    '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet    '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro   '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11  '
      'AND wmi.Pallet > 0 '
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa '
      'ORDER BY NO_PRD_TAM_COR')
    Left = 388
    Top = 53
    object QrPalletsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPalletsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPalletsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPalletsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrPalletsVMI_Codi: TIntegerField
      FieldName = 'VMI_Codi'
    end
    object QrPalletsVMI_Ctrl: TIntegerField
      FieldName = 'VMI_Ctrl'
    end
    object QrPalletsVMI_MovimNiv: TIntegerField
      FieldName = 'VMI_MovimNiv'
    end
    object QrPalletsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrPalletsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrPalletsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPalletsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPalletsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrPalletsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrPalletsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 388
    Top = 101
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, ggx.GraGruY'
      'FROM vsmovits wmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11'
      'AND wmi.Pallet > 0 '
      'AND wmi.Pallet=505'
      'AND wmi.SdoVrtPeca > 0 '
      'AND MovimID IN (0,7,8,11,13,14) '
      'ORDER BY Controle ')
    Left = 476
    Top = 53
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 476
    Top = 101
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 252
    Top = 64
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
    object QrStqCenLocEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 252
    Top = 112
  end
end
