unit VSCPMRSBER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, mySQLDbTables, DmkDAC_PF, UnProjGroup_Consts;

type
  TFmVSCPMRSBER = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    QrAbertos: TmySQLQuery;
    QrLista: TmySQLQuery;
    QrListaMovimCod: TIntegerField;
    QrListaSerieFch: TIntegerField;
    QrListaFicha: TIntegerField;
    TPData: TdmkEditDateTimePicker;
    QrFechou: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExecItemAtual(MovimCod, Serie, Ficha: Integer);
    function  ReopenPesquisa(Serie, Ficha: Integer): Integer;
  public
    { Public declarations }
    FModoExec: TdmkModoExec;
    FMovimCod, FSerie, FFicha, FGraGruYIni, FGraGruYFim, FGraGruYPos: Integer;
    FPB: TProgressBar;
    FLaAviso1, FLaAviso2: TLabel;
    //
    procedure Executa();
  end;

  var
  FmVSCPMRSBER: TFmVSCPMRSBER;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmVSCPMRSBER.BtOKClick(Sender: TObject);
begin
  ExecItemAtual(FMovimCod, FSerie, FFicha);
  Close;
end;

procedure TFmVSCPMRSBER.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCPMRSBER.ExecItemAtual(MovimCod, Serie, Ficha: Integer);
const
  sprocName = 'FmVSCPMRSBER.ExecItemAtual()';
var
  DtEnceRend: String;
  TpEnceRend: Integer;
  SQLType: TSQLType;
  Continua: Boolean;
  DataHora: TDateTime;
begin
  Continua := False;
  if (Ficha <> 0) or (FModoExec = TdmkModoExec.dmodexManual) then
  begin
    case FModoExec of
      TdmkModoExec.dmodexAutomatico:
      begin
        if ReopenPesquisa(Serie, Ficha) = 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrFechou, Dmod.MyDB, [
          'SELECT MAX(vmi.DataHora) DataHora',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
          'WHERE SerieFch=' + Geral.FF0(Serie),
          'AND Ficha=' + Geral.FF0(Ficha),
          '']);
          DataHora := QrFechou.FieldByName('DataHora').AsDateTime;
          Continua := DataHora > 2;
        end;
      end;
      TdmkModoExec.dmodexManual:
      begin
        DataHora := TPData.Date;
        Continua := True;
      end;
      else Geral.MB_Erro('Modoexec n�o implementado em ' + sProcName);
    end;
    if Continua then
    begin
      DtEnceRend     := Geral.FDT(DataHora, 109);
      TpEnceRend     := Integer(FModoExec);
      SQLType        := stUpd;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinncab', False, [
      'DtEnceRend', 'TpEnceRend'], [
      'MovimCod'], [
      DtEnceRend, TpEnceRend], [
      MovimCod], True);
    end;
  end;
end;

procedure TFmVSCPMRSBER.Executa();
const
  sProcName = 'FmVSCPMRSBER.Executa()';
begin
  case FModoExec of
    //TdmkModoExec.dmodexIndef=0,
    dmodexAutomatico:
    begin
      if FFicha <> 0 then
        ExecItemAtual(FMovimCod, FSerie, FFicha)
      else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLista, Dmod.MyDB, [
        'SELECT cab.MovimCod, vmi.SerieFch, vmi.Ficha',
        'FROM vsinncab cab',
        'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.MovimCod=cab.MovimCod',
        'WHERE cab.TpEnceRend=0  ',
        'AND (',
        '  vmi.SdoVrtPeca=0 ',
        '  OR ',
        '  (vmi.SdoVrtPeso=0 AND vmi.Pecas=0) ',
        ') ',
        '']);
        if FPB <> nil then
        begin
          FPB.Position := 0;
          FPB.Max := QrLista.RecordCount;
        end;
        QrLista.First;
        while not QrLista.Eof do
        begin
          MyObjects.UpdPB(FPB, FLaAviso1, FLaAviso2);
          ExecItemAtual(QrListaMovimCod.Value, QrListaSerieFch.Value, QrListaFicha.Value);
          //
          QrLista.Next;
        end;
      end;
    end;
    dmodexManual:
    begin
      ExecItemAtual(FMovimCod, FSerie, FFicha);
    end;
    else Geral.MB_Erro('Modoexec n�o implementado em ' + sProcName);
  end;
end;

procedure TFmVSCPMRSBER.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCPMRSBER.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPData.Date := DModG.ObtemAgora();
end;

procedure TFmVSCPMRSBER.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSCPMRSBER.ReopenPesquisa(Serie, Ficha: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAbertos, Dmod.MyDB, [
  'SELECT COUNT(vmi.Controle) ITENS ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'WHERE vmi.SerieFch=' + Geral.FF0(Serie),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'AND ggx.GraGruY BETWEEN ' + Geral.FF0(FGraGruYIni) + ' AND ' + Geral.FF0(FGraGruYFim),
  'AND ( ',
  '  vmi.SdoVrtPeca>0 ',
  '  OR ',
  '  (vmi.SdoVrtPeso>0 AND vmi.Pecas=0) ',
  ') ',
  '']);
  //Geral.MB_SQL(self, QrAbertos);
  Result := QrAbertos.FieldByName('ITENS').AsInteger;
end;

end.
