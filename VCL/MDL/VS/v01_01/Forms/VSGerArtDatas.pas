unit VSGerArtDatas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  dmkEditDateTimePicker, UnProjGroup_Consts, mySQLDbTables, UnGrl_Consts;

type
  TFmVSGerArtDatas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    TPDtHrAberto: TdmkEditDateTimePicker;
    EdDtHrAberto: TdmkEdit;
    Label23: TLabel;
    Label11: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    TPDtHrLibCla: TdmkEditDateTimePicker;
    EdDtHrLibCla: TdmkEdit;
    TPDtHrCfgCla: TdmkEditDateTimePicker;
    EdDtHrCfgCla: TdmkEdit;
    TPDtHrFimCla: TdmkEditDateTimePicker;
    EdDtHrFimCla: TdmkEdit;
    QrVSPaClaCab: TmySQLQuery;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabLstPal07: TIntegerField;
    QrVSPaClaCabLstPal08: TIntegerField;
    QrVSPaClaCabLstPal09: TIntegerField;
    QrVSPaClaCabLstPal10: TIntegerField;
    QrVSPaClaCabLstPal11: TIntegerField;
    QrVSPaClaCabLstPal12: TIntegerField;
    QrVSPaClaCabLstPal13: TIntegerField;
    QrVSPaClaCabLstPal14: TIntegerField;
    QrVSPaClaCabLstPal15: TIntegerField;
    QrVSPaClaCabDtHrFimCla: TDateTimeField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaCabDtHrFimCla_TXT: TWideStringField;
    QrVSPaClaCabFatorInt: TFloatField;
    QrVSPaClaIts: TmySQLQuery;
    QrVSPaClaItsFatorInt: TFloatField;
    QrVSPaClaItsCodigo: TIntegerField;
    QrVSPaClaItsControle: TIntegerField;
    QrVSPaClaItsVSPallet: TIntegerField;
    QrVSPaClaItsVMI_Sorc: TIntegerField;
    QrVSPaClaItsVMI_Baix: TIntegerField;
    QrVSPaClaItsVMI_Dest: TIntegerField;
    QrVSPaClaItsTecla: TIntegerField;
    QrVSPaClaItsDtHrIni: TDateTimeField;
    QrVSPaClaItsDtHrFim: TDateTimeField;
    QrVSPaClaItsLk: TIntegerField;
    QrVSPaClaItsDataCad: TDateField;
    QrVSPaClaItsDataAlt: TDateField;
    QrVSPaClaItsUserCad: TIntegerField;
    QrVSPaClaItsUserAlt: TIntegerField;
    QrVSPaClaItsAlterWeb: TSmallintField;
    QrVSPaClaItsAtivo: TSmallintField;
    QrVSPaClaItsDtHrFim_TXT: TWideStringField;
    QrVSPaMulCab: TmySQLQuery;
    QrVSPaMulCabCodigo: TIntegerField;
    QrVSPaMulCabMovimCod: TIntegerField;
    QrVSPaMulCabCacCod: TIntegerField;
    QrVSPaMulCabEmpresa: TIntegerField;
    QrVSPaMulCabVMI_Sorc: TIntegerField;
    QrVSPaMulCabDataHora: TDateTimeField;
    QrVSPaMulCabPecas: TFloatField;
    QrVSPaMulCabPesoKg: TFloatField;
    QrVSPaMulCabAreaM2: TFloatField;
    QrVSPaMulCabAreaP2: TFloatField;
    QrVSPaMulCabValorT: TFloatField;
    QrVSPaMulCabVSGerArt: TIntegerField;
    QrVSPaMulCabPallet: TIntegerField;
    QrVSPaMulCabTemIMEIMrt: TSmallintField;
    QrVSPaMulCabSerieRem: TSmallintField;
    QrVSPaMulCabNFeRem: TIntegerField;
    QrVSPaMulCabLk: TIntegerField;
    QrVSPaMulCabDataCad: TDateField;
    QrVSPaMulCabDataAlt: TDateField;
    QrVSPaMulCabUserCad: TIntegerField;
    QrVSPaMulCabUserAlt: TIntegerField;
    QrVSPaMulCabAlterWeb: TSmallintField;
    QrVSPaMulCabAtivo: TSmallintField;
    QrVSPaMulCabFatorInt: TFloatField;
    QrVSPaMulIts: TmySQLQuery;
    QrVSPaMulItsCodigo: TLargeintField;
    QrVSPaMulItsControle: TLargeintField;
    QrVSPaMulItsMovimCod: TLargeintField;
    QrVSPaMulItsMovimNiv: TLargeintField;
    QrVSPaMulItsMovimTwn: TLargeintField;
    QrVSPaMulItsEmpresa: TLargeintField;
    QrVSPaMulItsTerceiro: TLargeintField;
    QrVSPaMulItsCliVenda: TLargeintField;
    QrVSPaMulItsMovimID: TLargeintField;
    QrVSPaMulItsDataHora: TDateTimeField;
    QrVSPaMulItsPallet: TLargeintField;
    QrVSPaMulItsGraGruX: TLargeintField;
    QrVSPaMulItsPecas: TFloatField;
    QrVSPaMulItsPesoKg: TFloatField;
    QrVSPaMulItsAreaM2: TFloatField;
    QrVSPaMulItsAreaP2: TFloatField;
    QrVSPaMulItsValorT: TFloatField;
    QrVSPaMulItsSrcMovID: TLargeintField;
    QrVSPaMulItsSrcNivel1: TLargeintField;
    QrVSPaMulItsSrcNivel2: TLargeintField;
    QrVSPaMulItsSrcGGX: TLargeintField;
    QrVSPaMulItsSdoVrtPeca: TFloatField;
    QrVSPaMulItsSdoVrtPeso: TFloatField;
    QrVSPaMulItsSdoVrtArM2: TFloatField;
    QrVSPaMulItsObserv: TWideStringField;
    QrVSPaMulItsSerieFch: TLargeintField;
    QrVSPaMulItsFicha: TLargeintField;
    QrVSPaMulItsMisturou: TLargeintField;
    QrVSPaMulItsFornecMO: TLargeintField;
    QrVSPaMulItsCustoMOKg: TFloatField;
    QrVSPaMulItsCustoMOM2: TFloatField;
    QrVSPaMulItsCustoMOTot: TFloatField;
    QrVSPaMulItsValorMP: TFloatField;
    QrVSPaMulItsDstMovID: TLargeintField;
    QrVSPaMulItsDstNivel1: TLargeintField;
    QrVSPaMulItsDstNivel2: TLargeintField;
    QrVSPaMulItsDstGGX: TLargeintField;
    QrVSPaMulItsQtdGerPeca: TFloatField;
    QrVSPaMulItsQtdGerPeso: TFloatField;
    QrVSPaMulItsQtdGerArM2: TFloatField;
    QrVSPaMulItsQtdGerArP2: TFloatField;
    QrVSPaMulItsQtdAntPeca: TFloatField;
    QrVSPaMulItsQtdAntPeso: TFloatField;
    QrVSPaMulItsQtdAntArM2: TFloatField;
    QrVSPaMulItsQtdAntArP2: TFloatField;
    QrVSPaMulItsNotaMPAG: TFloatField;
    QrVSPaMulItsNO_PALLET: TWideStringField;
    QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPaMulItsNO_TTW: TWideStringField;
    QrVSPaMulItsID_TTW: TLargeintField;
    QrVSPaMulItsNO_FORNECE: TWideStringField;
    QrVSPaMulItsNO_SerieFch: TWideStringField;
    QrVSPaMulItsReqMovEstq: TLargeintField;
    QrVSPaMulItsStqCenLoc: TLargeintField;
    QrVSPaMulItsDtCorrApo: TDateTimeField;
    QrVMIs: TmySQLQuery;
    QrVMIsVMI: TIntegerField;
    QrVMIsDataHora: TDateTimeField;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure QrVSPaMulCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaMulCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenVSPaClaCab();
    procedure ReopenVSPaClaIts();
    procedure ReopenVSPaMulCab();
    procedure ReopenVSPaMulIts(Controle: Integer);
  public
    { Public declarations }
    FCodigo, FMovimCod, FMovimID: Integer;
    FDtHrCfgCla, FDtHrFimCla: TDateTime;
  end;

  var
  FmVSGerArtDatas: TFmVSGerArtDatas;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnVS_CRC_PF, UnVS_PF;

{$R *.DFM}

procedure TFmVSGerArtDatas.BtOKClick(Sender: TObject);
var
  DataHora, DtHrAberto, DtHrLibCla, DtHrCfgCla, DtHrFimCla, DtHrIni, DtHrFim: String;
  Codigo, Controle: Integer;
  MovimCod, MovimID: Integer;
  NewCfgCla, DifCfgCla, NewFimCla, DifFimCla, HrIni, HrFim: TDateTime;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando geração do artigo!');
  Codigo         := FCodigo;
  DtHrAberto     := Geral.FDT_TP_Ed(TPDtHrAberto.Date, EdDtHrAberto.Text);
  DtHrLibCla     := Geral.FDT_TP_Ed(TPDtHrLibCla.Date, EdDtHrLibCla.Text);
  DtHrCfgCla     := Geral.FDT_TP_Ed(TPDtHrCfgCla.Date, EdDtHrCfgCla.Text);
  DtHrFimCla     := Geral.FDT_TP_Ed(TPDtHrFimCla.Date, EdDtHrFimCla.Text);
  //
  MovimCod := FMovimCod;
  MovimID  := FMovimID;
  DataHora := DtHrAberto;
  //
  NewCfgCla      := Trunc(TPDtHrCfgCla.Date) + EdDtHrCfgCla.ValueVariant;
  DifCfgCla      := NewCfgCla - FDtHrCfgCla;
  //
  NewFimCla      := Trunc(TPDtHrFimCla.Date) + EdDtHrFimCla.ValueVariant;
  DifFimCla      := NewFimCla - FDtHrFimCla;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
  'DtHrAberto', 'DtHrLibCla', 'DtHrCfgCla',
  'DtHrFimCla'], [
  'Codigo'], [
  DtHrAberto, DtHrLibCla, DtHrCfgCla,
  DtHrFimCla], [
  Codigo], True) then
  begin
    ////////////////////////////// Classe Couro a Couro ////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando classificação do artigo!');
    ReopenVSPaClaCab();
    while not QrVSPaClaCab.Eof do
    begin
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspaclacaba', False, [
      'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True);
      //
      PB1.Position := 0;
      PB1.Max := QrVSPaClaIts.RecordCount;
      QrVSPaClaIts.First;
      while not QrVSPaClaIts.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando classificação do artigo!');
        Controle := QrVSPaClaItsControle.Value;
        HrIni    := QrVSPaClaItsDtHrIni.Value + DifCfgCla;
        DtHrIni  := Geral.FDT(HrIni, 109);
        if QrVSPaClaItsDtHrFim.Value > 2 then
          HrFim    := QrVSPaClaItsDtHrFim.Value + DifCfgCla
        else
          HrFim    := 0;
        //
        DtHrFim  := Geral.FDT(HrFim, 109);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspaclaitsa', False, [
        'DtHrIni', 'DtHrFim'], ['Controle'], [
        DtHrIni, DtHrFim], [Controle], True);
        //
        QrVSPaClaIts.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando IMEIs gerados!');
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
      'SELECT DISTINCT pri.VMI_Baix VMI, vmi.DataHora ',
      'FROM vspaclaitsa pri ',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Baix ',
      'WHERE pri.Codigo=' + Geral.FF0(QrVSPaClaCabCodigo.Value),
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT pri.VMI_Dest VMI, vmi.DataHora ',
      'FROM vspaclaitsa pri ',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Dest ',
      'WHERE pri.Codigo=' + Geral.FF0(QrVSPaClaCabCodigo.Value),
      ' ',
      'ORDER BY VMI ',
      '']);
      //
      PB1.Position := 0;
      PB1.Max := QrVMIs.RecordCount;
      QrVMIs.First;
      while not QrVMIs.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando IMEIs gerados!');
        Controle := QrVMIsVMI.Value;
        HrIni    := QrVMIsDataHora.Value + DifCfgCla;
        DataHora := Geral.FDT(HrIni, 109);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_UPD_TAB_VMI, False, [
        'DataHora'], ['Controle'], [DataHora], [Controle], True);
        //
        QrVMIs.Next;
      end;
      //
      QrVSPaClaCab.Next;
    end;
    //
    /////////////////////////////////// Classe Massiva /////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando classificação massiva!');
    // Já faz manual!
{
    ReopenVSPaMulCab();
    while not QrVSPaClaCab.Eof do
    begin
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspamulcaba', False, [
      'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True);
      //
      PB1.Position := 0;
      PB1.Max := QrVSPaClaIts.RecordCount;
      QrVSPaClaIts.First;
}
    //
    //
    //////////////////////////////////////// FIM  /////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Close;
  end;
end;

procedure TFmVSGerArtDatas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtDatas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtDatas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSGerArtDatas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtDatas.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaClaIts();
end;

procedure TFmVSGerArtDatas.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaClaIts.Close;
end;

procedure TFmVSGerArtDatas.QrVSPaMulCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaMulIts(0);
end;

procedure TFmVSGerArtDatas.QrVSPaMulCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaMulIts.Close;
end;

procedure TFmVSGerArtDatas.ReopenVSPaClaCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  ',
  '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_TXT,',
///// Usa apenas CO_SEL_TAB_VMI porque usa cn1.FatorInt apenas para corrigir baixas!
  'cn1.FatorInt  ',
  'FROM vspaclacaba pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VSMovIts',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
////////////////////////////////////////////////////////////////////////////////
  'WHERE pra.VSGerArt=' + Geral.FF0(FCodigo),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerArtDatas.ReopenVSPaClaIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT, ',
  'cn1.FatorInt  ',
  'FROM vspaclaitsa pri ',
  'LEFT JOIN vspalleta  let ON let.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pri.Codigo=' + Geral.FF0(QrVSPaclaCabCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
end;

procedure TFmVSGerArtDatas.ReopenVSPaMulCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulCab, Dmod.MyDB, [
  'SELECT pra.*,',
  'cn1.FatorInt  ',
  'FROM vspamulcaba pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VMI_Sorc',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pra.VSGerArt=' + Geral.FF0(FCodigo),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerArtDatas.ReopenVSPaMulIts(Controle: Integer);
var
  TemIMEIMrt, MovimCod: Integer;
begin
{$IfDef sAllVS}
  TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  MovimCod   := QrVSPaMulCabMovimCod.Value;
  VS_PF.ReopenVSPaMulIts(QrVSPaMulIts, TemIMEIMrt, MovimCod, Controle);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

end.
