unit VSGerClaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, UnGrl_Geral, UnGrl_Vars, UnAppEnums;

type
  TFmVSGerClaCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSGerCla: TmySQLQuery;
    DsVSGerCla: TDataSource;
    QrVSGerClaNO_TIPO: TWideStringField;
    QrVSGerClaNO_DtHrFimCla: TWideStringField;
    QrVSGerClaNO_DtHrLibCla: TWideStringField;
    QrVSGerClaNO_DtHrCfgCla: TWideStringField;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    Outrasimpresses1: TMenuItem;
    QrVSGerClaCodigo: TIntegerField;
    QrVSGerClaMovimCod: TIntegerField;
    QrVSGerClaCacCod: TIntegerField;
    QrVSGerClaGraGruX: TIntegerField;
    QrVSGerClaNome: TWideStringField;
    QrVSGerClaEmpresa: TIntegerField;
    QrVSGerClaDtHrLibCla: TDateTimeField;
    QrVSGerClaDtHrCfgCla: TDateTimeField;
    QrVSGerClaDtHrFimCla: TDateTimeField;
    QrVSGerClaTipoArea: TSmallintField;
    QrVSGerClaLk: TIntegerField;
    QrVSGerClaDataCad: TDateField;
    QrVSGerClaDataAlt: TDateField;
    QrVSGerClaUserCad: TIntegerField;
    QrVSGerClaUserAlt: TIntegerField;
    QrVSGerClaAlterWeb: TSmallintField;
    QrVSGerClaAtivo: TSmallintField;
    QrVSGerClaNO_EMPRESA: TWideStringField;
    QrVSGerClaNO_PRD_TAM_COR: TWideStringField;
    QrVSPaClaCab: TmySQLQuery;
    DsVSPaClaCab: TDataSource;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabDtHrFimCla: TDateTimeField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaIts: TmySQLQuery;
    DsVSPaClaIts: TDataSource;
    QrVSPaClaItsCodigo: TIntegerField;
    QrVSPaClaItsControle: TIntegerField;
    QrVSPaClaItsVSPallet: TIntegerField;
    QrVSPaClaItsVMI_Sorc: TIntegerField;
    QrVSPaClaItsVMI_Baix: TIntegerField;
    QrVSPaClaItsVMI_Dest: TIntegerField;
    QrVSPaClaItsTecla: TIntegerField;
    QrVSPaClaItsDtHrIni: TDateTimeField;
    QrVSPaClaItsDtHrFim: TDateTimeField;
    QrVSPaClaItsLk: TIntegerField;
    QrVSPaClaItsDataCad: TDateField;
    QrVSPaClaItsDataAlt: TDateField;
    QrVSPaClaItsUserCad: TIntegerField;
    QrVSPaClaItsUserAlt: TIntegerField;
    QrVSPaClaItsAlterWeb: TSmallintField;
    QrVSPaClaItsAtivo: TSmallintField;
    QrVSPaClaItsDtHrFim_TXT: TWideStringField;
    QrVSPaClaCabDtHrFimCla_TXT: TWideStringField;
    PMVSPaClaIts: TPopupMenu;
    EncerraPallet1: TMenuItem;
    QrVSPaClaCabPecas: TFloatField;
    QrVSPaClaCabAreaM2: TFloatField;
    QrVSPaClaCabAreaP2: TFloatField;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    QrSumAreaM2: TFloatField;
    QrSumAreaP2: TFloatField;
    PelaOC1: TMenuItem;
    QrVMI_Sorc: TmySQLQuery;
    DsVMI_Sorc: TDataSource;
    QrVMI_Baix: TmySQLQuery;
    DsVMI_Baix: TDataSource;
    QrVMI_SorcVMI_Sorc: TIntegerField;
    QrVMI_SorcPecas: TFloatField;
    QrVMI_SorcAreaM2: TFloatField;
    QrVMI_BaixVMI_Baix: TIntegerField;
    QrVMI_BaixPecas: TFloatField;
    QrVMI_BaixAreaM2: TFloatField;
    N1: TMenuItem;
    IrparajaneladedadosdoIMEI1: TMenuItem;
    AlteradadosdoartigodoIMEI1: TMenuItem;
    QrVMI_SorcVMI_Baix: TIntegerField;
    QrVMI_SorcVMI_Dest: TIntegerField;
    QrVMI_BaixVMI_Sorc: TIntegerField;
    QrVMI_BaixVMI_Dest: TIntegerField;
    BtVoltaAClassificar: TBitBtn;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSGerArtA: TmySQLQuery;
    QrVSGerArtACodigo: TIntegerField;
    QrVSPaClaCabFatorInt: TFloatField;
    QrVSPaClaItsFatorInt: TFloatField;
    QrVCI: TmySQLQuery;
    QrVCIPecas: TFloatField;
    QrVCIAreaM2: TFloatField;
    QrVCIAreaP2: TFloatField;
    QrVCIPcBxa: TFloatField;
    BtCorrigeBaixas: TBitBtn;
    QrVMI_Orig: TmySQLQuery;
    DsVMI_Orig: TDataSource;
    QrVMI_OrigCodigo: TLargeintField;
    QrVMI_OrigControle: TLargeintField;
    QrVMI_OrigMovimCod: TLargeintField;
    QrVMI_OrigMovimNiv: TLargeintField;
    QrVMI_OrigMovimTwn: TLargeintField;
    QrVMI_OrigEmpresa: TLargeintField;
    QrVMI_OrigTerceiro: TLargeintField;
    QrVMI_OrigCliVenda: TLargeintField;
    QrVMI_OrigMovimID: TLargeintField;
    QrVMI_OrigDataHora: TDateTimeField;
    QrVMI_OrigPallet: TLargeintField;
    QrVMI_OrigGraGruX: TLargeintField;
    QrVMI_OrigPecas: TFloatField;
    QrVMI_OrigPesoKg: TFloatField;
    QrVMI_OrigAreaM2: TFloatField;
    QrVMI_OrigAreaP2: TFloatField;
    QrVMI_OrigValorT: TFloatField;
    QrVMI_OrigSrcMovID: TLargeintField;
    QrVMI_OrigSrcNivel1: TLargeintField;
    QrVMI_OrigSrcNivel2: TLargeintField;
    QrVMI_OrigSrcGGX: TLargeintField;
    QrVMI_OrigSdoVrtPeca: TFloatField;
    QrVMI_OrigSdoVrtPeso: TFloatField;
    QrVMI_OrigSdoVrtArM2: TFloatField;
    QrVMI_OrigObserv: TWideStringField;
    QrVMI_OrigSerieFch: TLargeintField;
    QrVMI_OrigFicha: TLargeintField;
    QrVMI_OrigMisturou: TLargeintField;
    QrVMI_OrigFornecMO: TLargeintField;
    QrVMI_OrigCustoMOKg: TFloatField;
    QrVMI_OrigCustoMOM2: TFloatField;
    QrVMI_OrigCustoMOTot: TFloatField;
    QrVMI_OrigValorMP: TFloatField;
    QrVMI_OrigDstMovID: TLargeintField;
    QrVMI_OrigDstNivel1: TLargeintField;
    QrVMI_OrigDstNivel2: TLargeintField;
    QrVMI_OrigDstGGX: TLargeintField;
    QrVMI_OrigQtdGerPeca: TFloatField;
    QrVMI_OrigQtdGerPeso: TFloatField;
    QrVMI_OrigQtdGerArM2: TFloatField;
    QrVMI_OrigQtdGerArP2: TFloatField;
    QrVMI_OrigQtdAntPeca: TFloatField;
    QrVMI_OrigQtdAntPeso: TFloatField;
    QrVMI_OrigQtdAntArM2: TFloatField;
    QrVMI_OrigQtdAntArP2: TFloatField;
    QrVMI_OrigNotaMPAG: TFloatField;
    QrVMI_OrigNO_PALLET: TWideStringField;
    QrVMI_OrigNO_PRD_TAM_COR: TWideStringField;
    QrVMI_OrigNO_TTW: TWideStringField;
    QrVMI_OrigID_TTW: TLargeintField;
    QrVMI_OrigNO_FORNECE: TWideStringField;
    QrVMI_OrigNO_SerieFch: TWideStringField;
    QrVMI_OrigReqMovEstq: TLargeintField;
    QrVMI_OrigNO_EstqMovimID: TWideStringField;
    QrVMI_OrigNO_DstMovID: TWideStringField;
    QrVMI_OrigNO_SrcMovID: TWideStringField;
    QrVMI_OrigNO_LOC_CEN: TWideStringField;
    QrVMI_OrigGraGru1: TLargeintField;
    QrVMI_OrigMarca: TWideStringField;
    QrVMI_OrigPedItsLib: TLargeintField;
    QrVMI_OrigPedItsFin: TLargeintField;
    QrVMI_OrigPedItsVda: TLargeintField;
    QrVMI_OrigStqCenLoc: TLargeintField;
    QrVMI_OrigItemNFe: TLargeintField;
    QrVMI_OrigMarca_1: TWideStringField;
    QrVMI_OrigLnkNivXtr1: TLargeintField;
    QrVMI_OrigLnkNivXtr2: TLargeintField;
    QrVSPaClaCabLstPal07: TIntegerField;
    QrVSPaClaCabLstPal08: TIntegerField;
    QrVSPaClaCabLstPal09: TIntegerField;
    QrVSPaClaCabLstPal10: TIntegerField;
    QrVSPaClaCabLstPal11: TIntegerField;
    QrVSPaClaCabLstPal12: TIntegerField;
    QrVSPaClaCabLstPal13: TIntegerField;
    QrVSPaClaCabLstPal14: TIntegerField;
    QrVSPaClaCabLstPal15: TIntegerField;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label23: TLabel;
    Label2: TLabel;
    Label22: TLabel;
    Label11: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Label37: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit0: TdmkDBEdit;
    DBEdit4: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit28: TDBEdit;
    PCFormaClas: TPageControl;
    TabSheet1: TTabSheet;
    Panel8: TPanel;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    GroupBox3: TGroupBox;
    DGVSPaClaCab: TdmkDBGridZTO;
    Panel7: TPanel;
    Splitter1: TSplitter;
    GroupBox2: TGroupBox;
    DGVMI_Sorc: TdmkDBGridZTO;
    GroupBox4: TGroupBox;
    DGVMI_Baix: TdmkDBGridZTO;
    Splitter2: TSplitter;
    DGVSPaClaIts: TdmkDBGridZTO;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit5: TDBEdit;
    QrVSPaMulCab: TmySQLQuery;
    DsVSPaMulCab: TDataSource;
    QrVSPaMulCabCodigo: TIntegerField;
    QrVSPaMulCabMovimCod: TIntegerField;
    QrVSPaMulCabCacCod: TIntegerField;
    QrVSPaMulCabEmpresa: TIntegerField;
    QrVSPaMulCabVMI_Sorc: TIntegerField;
    QrVSPaMulCabDataHora: TDateTimeField;
    QrVSPaMulCabPecas: TFloatField;
    QrVSPaMulCabPesoKg: TFloatField;
    QrVSPaMulCabAreaM2: TFloatField;
    QrVSPaMulCabAreaP2: TFloatField;
    QrVSPaMulCabValorT: TFloatField;
    QrVSPaMulCabVSGerArt: TIntegerField;
    QrVSPaMulCabPallet: TIntegerField;
    QrVSPaMulCabTemIMEIMrt: TSmallintField;
    QrVSPaMulCabSerieRem: TSmallintField;
    QrVSPaMulCabNFeRem: TIntegerField;
    QrVSPaMulCabLk: TIntegerField;
    QrVSPaMulCabDataCad: TDateField;
    QrVSPaMulCabDataAlt: TDateField;
    QrVSPaMulCabUserCad: TIntegerField;
    QrVSPaMulCabUserAlt: TIntegerField;
    QrVSPaMulCabAlterWeb: TSmallintField;
    QrVSPaMulCabAtivo: TSmallintField;
    QrVSPaMulCabFatorInt: TFloatField;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    DBEdit6: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    QrVSPaMulIts: TmySQLQuery;
    QrVSPaMulItsCodigo: TLargeintField;
    QrVSPaMulItsControle: TLargeintField;
    QrVSPaMulItsMovimCod: TLargeintField;
    QrVSPaMulItsMovimNiv: TLargeintField;
    QrVSPaMulItsMovimTwn: TLargeintField;
    QrVSPaMulItsEmpresa: TLargeintField;
    QrVSPaMulItsTerceiro: TLargeintField;
    QrVSPaMulItsCliVenda: TLargeintField;
    QrVSPaMulItsMovimID: TLargeintField;
    QrVSPaMulItsDataHora: TDateTimeField;
    QrVSPaMulItsPallet: TLargeintField;
    QrVSPaMulItsGraGruX: TLargeintField;
    QrVSPaMulItsPecas: TFloatField;
    QrVSPaMulItsPesoKg: TFloatField;
    QrVSPaMulItsAreaM2: TFloatField;
    QrVSPaMulItsAreaP2: TFloatField;
    QrVSPaMulItsValorT: TFloatField;
    QrVSPaMulItsSrcMovID: TLargeintField;
    QrVSPaMulItsSrcNivel1: TLargeintField;
    QrVSPaMulItsSrcNivel2: TLargeintField;
    QrVSPaMulItsSrcGGX: TLargeintField;
    QrVSPaMulItsSdoVrtPeca: TFloatField;
    QrVSPaMulItsSdoVrtPeso: TFloatField;
    QrVSPaMulItsSdoVrtArM2: TFloatField;
    QrVSPaMulItsObserv: TWideStringField;
    QrVSPaMulItsSerieFch: TLargeintField;
    QrVSPaMulItsFicha: TLargeintField;
    QrVSPaMulItsMisturou: TLargeintField;
    QrVSPaMulItsFornecMO: TLargeintField;
    QrVSPaMulItsCustoMOKg: TFloatField;
    QrVSPaMulItsCustoMOM2: TFloatField;
    QrVSPaMulItsCustoMOTot: TFloatField;
    QrVSPaMulItsValorMP: TFloatField;
    QrVSPaMulItsDstMovID: TLargeintField;
    QrVSPaMulItsDstNivel1: TLargeintField;
    QrVSPaMulItsDstNivel2: TLargeintField;
    QrVSPaMulItsDstGGX: TLargeintField;
    QrVSPaMulItsQtdGerPeca: TFloatField;
    QrVSPaMulItsQtdGerPeso: TFloatField;
    QrVSPaMulItsQtdGerArM2: TFloatField;
    QrVSPaMulItsQtdGerArP2: TFloatField;
    QrVSPaMulItsQtdAntPeca: TFloatField;
    QrVSPaMulItsQtdAntPeso: TFloatField;
    QrVSPaMulItsQtdAntArM2: TFloatField;
    QrVSPaMulItsQtdAntArP2: TFloatField;
    QrVSPaMulItsNotaMPAG: TFloatField;
    QrVSPaMulItsNO_PALLET: TWideStringField;
    QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPaMulItsNO_TTW: TWideStringField;
    QrVSPaMulItsID_TTW: TLargeintField;
    QrVSPaMulItsNO_FORNECE: TWideStringField;
    QrVSPaMulItsNO_SerieFch: TWideStringField;
    QrVSPaMulItsReqMovEstq: TLargeintField;
    QrVSPaMulItsStqCenLoc: TLargeintField;
    QrVSPaMulItsDtCorrApo: TDateTimeField;
    DsVSPaMulIts: TDataSource;
    BtAlterarDatas: TBitBtn;
    QrVSGerClaDtHrAberto: TDateTimeField;
    QrVSGerClaPecasMan: TFloatField;
    QrVSGerClaAreaManM2: TFloatField;
    QrVSGerClaAreaManP2: TFloatField;
    QrVSGerClaCustoManMOKg: TFloatField;
    QrVSGerClaCustoManMOTot: TFloatField;
    QrVSGerClaValorManMP: TFloatField;
    QrVSGerClaValorManT: TFloatField;
    QrVSGerClaTemIMEIMrt: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSGerClaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSGerClaBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSGerClaAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure QrVSGerClaBeforeClose(DataSet: TDataSet);
    procedure QrVSGerClaCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure EncerraPallet1Click(Sender: TObject);
    procedure QrVSPaClaCabCalcFields(DataSet: TDataSet);
    procedure Qr_AfterScroll(DataSet: TDataSet);
    procedure Qr_BeforeClose(DataSet: TDataSet);
    procedure Qr_CalcFields(DataSet: TDataSet);
    procedure PelaOC1Click(Sender: TObject);
    procedure QrVSPaClaItsBeforeClose(DataSet: TDataSet);
    procedure QrVSPaClaItsAfterScroll(DataSet: TDataSet);
    procedure IrparajaneladedadosdoIMEI1Click(Sender: TObject);
    procedure AlteradadosdoartigodoIMEI1Click(Sender: TObject);
    procedure BtCorrigeBaixasClick(Sender: TObject);
    procedure BtVoltaAClassificarClick(Sender: TObject);
    procedure QrVMI_OrigBeforeClose(DataSet: TDataSet);
    procedure QrVMI_OrigAfterOpen(DataSet: TDataSet);
    procedure QrVSPaClaCabAfterOpen(DataSet: TDataSet);
    procedure QrVSPaMulCabAfterOpen(DataSet: TDataSet);
    procedure QrVSPaMulCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaMulCabBeforeClose(DataSet: TDataSet);
    procedure BtAlterarDatasClick(Sender: TObject);
  private
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);

    procedure CoriigeBaixasDaOCAtual();
    procedure ReopenSum(_AND: String);
    //procedure ReopenSumPall();
    procedure ReopenVMI_Baix();
    procedure ReopenVMI_Sorc();
    procedure ReopenVMI_Orig(VMI_Orig: Integer);
    procedure ReopenVSPallet();
    procedure ReopenVSPaClaCab();
    procedure ReopenVSPaClaIts();
    procedure ReopenVSPaMulCab();
    procedure ReopenVSPaMulIts(Controle: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmVSGerClaCab: TFmVSGerClaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral,
  {$IfDef sAllVS}UnVS_PF,{$EndIf}
VSGerArtEnc, UnVS_CRC_PF, UnVS_Jan, GerlShowGrid;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSGerClaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSGerClaCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSGerClaCab.PelaOC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPelaOC(emidClassArtXXUni);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerClaCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSGerClaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSGerClaCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEC(emidClassArtXXUni);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerClaCab.PeloIMEI1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
  ImeiTxt: String;
  Codigo, Controle: Integer;
  Result: Boolean;
begin
  Result := False;
  Codigo := 0;
  if InputQuery('Localiza��o por IME-I', 'informe o IME-I', ImeiTxt) then
  begin
    Controle := Geral.IMV(ImeiTxt);
    if Controle <> 0 then
    begin
      ImeiTxt := Geral.FF0(Controle);
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT VSGerArt Codigo ',
        'FROM vspaclacaba',
        'WHERE ' + CO_SEL_TAB_VMI + '=' + ImeiTxt,
        '']);
        //
        Result := Qry.RecordCount > 0;
        //
        if Result then
        begin
          if Qry.RecordCount = 1 then
            Codigo := Qry.FieldByName('Codigo').AsInteger
          else
          begin
            Ds         := TDataSource.Create(Dmod);
            try
              Ds.DataSet := Qry;
              //
              Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
              FmGerlShowGrid.MeAvisos.Lines.Add('C�digos localizados para o IME-I: ' + ImeiTxt);
              //FmGerlShowGrid.MeAvisos.Lines.Add('
              FmGerlShowGrid.DBGSel.DataSource := Ds;
              FmGerlShowGrid.ShowModal;
              if FmGerlShowGrid.FSelecionou then
              begin
                Codigo := Qry.FieldByName('Codigo').Value;
              end;
              FmGerlShowGrid.Destroy;
            finally
              Ds.Free;
            end;
          end;
        end
        else
          Geral.MB_Aviso('Nenhum c�digo localizado!');
      finally
        Qry.Free;
      end;
    end;
  end;
  if Codigo <> 0 then
    LocCod(Codigo, Codigo)
end;

procedure TFmVSGerClaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSGerClaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSGerClaCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsgerarta';
  VAR_GOTOMYSQLTABLE := QrVSGerCla;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vgr.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('CONCAT(gg1.Nome, ');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ');
  //VAR_SQLx.Add('NO_PRD_TAM_COR, pal.Nome NO_PALLET');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vsgerarta vgr');
  VAR_SQLx.Add('LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  //VAR_SQLx.Add('LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet ');
  VAR_SQLx.Add('WHERE vgr.Codigo > 0');
  //
  VAR_SQL1.Add('AND vgr.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vgr.Nome Like :P0');
  //
end;

procedure TFmVSGerClaCab.EncerraPallet1Click(Sender: TObject);
{
const
  EncerrandoTodos = False;
var
  VSPaClaIts, Box, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
    ReabreVSPaClaCab: Boolean;
begin
  Box        := QrVSPaClaItsTecla.Value;
  VSPaClaIts := QrVSPaClaItsControle.Value;
  VSPallet   := QrVSPaClaItsVSPallet.Value;
  VMI_Sorc   := QrVSPaClaItsVMI_Sorc.Value;
  VMI_Baix   := QrVSPaClaItsVMI_Baix.Value;
  VMI_Dest   := QrVSPaClaItsVMI_Dest.Value;
  //
  (*Result := *)VS_CRC_PF.EncerraPalletClassificacao(QrVSPaClaCabCacCod.Value,
  QrVSPaClaCabCodigo.Value, QrVSPaClaCabVSPallet.Value, Box, VSPaClaIts,
  VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, EncerrandoTodos, ReabreVSPaClaCab);
  if ReabreVSPaClaCab then
      ReopenVSPaClaIts();
}
const
  EncerrandoTodos = False;
  FromRcl = False;
  OC = 0;
  Pergunta = True;
var
  Codigo: Integer;
  FromBox: Variant;
begin
  FromBox := Null;
  Codigo := QrVSPaClaItsVSPallet.Value;
  (*
  VS_CRC_PF.EncerraPalletOld(Codigo, OC, EncerrandoTodos, FromBox, emidAjuste, True, True,
  FromRcl);
  *)
  VS_CRC_PF.EncerraPalletNew(Codigo, Pergunta);
  LocCod(Codigo, Codigo);
end;

procedure TFmVSGerClaCab.CabInclui1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  ' SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  ',
  '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_TXT ',
  'FROM vspaclacaba pra ',
  'WHERE pra.VSGerArt=' + Geral.FF0(QrVSGerClaCodigo.Value),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerClaCab.CoriigeBaixasDaOCAtual();
const
  Gera = False;
var
  SrcMovID, SrcNivel1, SrcNivel2, SrcGGX, Controle: Integer;
  //
  VSPaClaIts: Integer;
  FatorIntSrc, FatorIntDst, Pecas: Double;
begin
  Screen.Cursor := crHourGlass;
  try
  if Geral.MB_Pergunta(
  'Deseja verificar tamb�m o fator de convers�o de inteiros para meios?') =
  ID_YES then
  begin




    QrVSPaClaIts.First;
    while not QrVSPaClaIts.Eof do
    begin
      VSPaClaIts := QrVSPaClaItsControle.Value;
      if VSPaClaIts > 0 then
      begin
        FatorIntSrc := QrVSPaClaCabFatorInt.Value;
        FatorIntDst := QrVSPaClaItsFatorInt.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
        'FatorIntSrc', 'FatorIntDst'], [
        'VSPaRclIts'], [
        FatorIntSrc, FatorIntDst], [
        VSPaClaIts], VAR_InsUpd_AWServerID);
        //
        QrVMI_Baix.First;
        while not QrVMI_Baix.Eof do
        begin
          if QrVMI_BaixVMI_Baix.Value > 0 then
          begin
            Controle := QrVMI_BaixVMI_Baix.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrVCI, Dmod.MyDB, [
            'SELECT VMI_Baix, SUM(Pecas) Pecas, ',
            'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2,',
            'SUM(Pecas / FatorIntSrc * FatorIntDst) PcBxa  ',
            'FROM vscacitsa ',
            'WHERE VMI_Baix=' + Geral.FF0(Controle),
            '']);
            Pecas := -QrVCIPcBxa.Value;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Pecas'], ['Controle'], [
            Pecas], [Controle], True);
            //
            VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Dest.Value, False);
            VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Sorc.Value, False);
            //
          end;
          QrVMI_Baix.Next;
        end;
      end;
      //
      QrVSPaClaIts.Next;
    end;
  end;









    QrVSPaClaCab.First;
    while not QrVSPaClaCab.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
      'SELECT MovimID, Codigo, Controle, GraGruX ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrVSPaClaCabVSMovIts.Value),
      '']);
      SrcMovID  := QrVSMovItsMovimID.Value;
      SrcNivel1 := QrVSMovItsCodigo.Value;
      SrcNivel2 := QrVSMovItsControle.Value;
      SrcGGX    := QrVSMovItsGraGruX.Value;
      //
      QrVSPaClaIts.First;
      while not QrVSPaClaIts.Eof do
      begin
        QrVMI_Baix.First;
        while not QrVMI_Baix.Eof do
        begin
          Controle := QrVMI_BaixVMI_Baix.Value;
          //
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          (*'Codigo', 'MovimCod', 'MovimNiv',
          'MovimTwn', 'Empresa', 'Terceiro',
          'CliVenda', 'MovimID', 'LnkNivXtr1',
          'LnkNivXtr2', CO_DATA_HORA_VMI, 'Pallet',
          'GraGruX', 'Pecas', 'PesoKg',
          'AreaM2', 'AreaP2', 'ValorT',*)
          'SrcMovID', 'SrcNivel1', 'SrcNivel2',
          'SrcGGX'(*, 'SdoVrtPeca', 'SdoVrtPeso',
          'SdoVrtArM2', 'Observ', 'SerieFch',
          'Ficha', 'Misturou', 'FornecMO',
          'CustoMOKg', 'CustoMOTot', 'ValorMP',
          'DstMovID', 'DstNivel1', 'DstNivel2',
          'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
          'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
          'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
          'AptoUso', 'NotaMPAG', 'Marca',
          'TpCalcAuto'*)], [
          'Controle'], [
          (*Codigo, MovimCod, MovimNiv,
          MovimTwn, Empresa, Terceiro,
          CliVenda, MovimID, LnkNivXtr1,
          LnkNivXtr2, DataHora, Pallet,
          GraGruX, Pecas, PesoKg,
          AreaM2, AreaP2, ValorT,*)
          SrcMovID, SrcNivel1, SrcNivel2,
          SrcGGX(*, SdoVrtPeca, SdoVrtPeso,
          SdoVrtArM2, Observ, SerieFch,
          Ficha, Misturou, FornecMO,
          CustoMOKg, CustoMOTot, ValorMP,
          DstMovID, DstNivel1, DstNivel2,
          DstGGX, QtdGerPeca, QtdGerPeso,
          QtdGerArM2, QtdGerArP2, QtdAntPeca,
          QtdAntPeso, QtdAntArM2, QtdAntArP2,
          AptoUso, NotaMPAG, Marca,
          TpCalcAuto*)], [
          Controle], True);
          Application.ProcessMessages;
          //
          QrVMI_Baix.Next;
        end;
        //
        QrVSPaClaIts.Next;
      end;
      //
      QrVSPaClaCab.Next;
    end;
    VS_CRC_PF.AtualizaSaldoIMEI(QrVSMovItsControle.Value, Gera);
    Application.ProcessMessages;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSGerClaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSGerClaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSGerClaCab.ReopenSum(_AND: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(Aream2) AreaM2, SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(QrVSGerClaCacCod.Value),
  _AND,
  ' ']);
end;

(*
procedure TFmVSGerClaCab.ReopenSumPall;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPall, Dmod.MyDB, [
  'SELECT wmi.GraGruX, SUM(wmi.Pecas) Pecas, ',
  'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT, ',
  'SUM(wmi.SdoVrtPeca) SdoVrtPeca, SUM(wmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(wmi.SdoVrtArM2) SdoVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, MAX(wmi.DataHora) DataHora ',
  'FROM  v s m o v i t s wmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE wmi.Pallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'GROUP BY wmi.GraGruX ',
  '']);
end;
*)

procedure TFmVSGerClaCab.ReopenVMI_Baix();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Baix, Dmod.MyDB, [
  'SELECT VMI_Sorc, VMI_Baix, VMI_Dest, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPaClaIts=' + Geral.FF0(QrVSPaClaItsControle.Value),
  'GROUP BY VMI_Baix ',
  '']);
end;

procedure TFmVSGerClaCab.ReopenVMI_Orig(VMI_Orig: Integer);
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'CAST(vmi.Marca AS CHAR) Marca,',
  'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,',
  'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
  'CAST(vsp.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Controle=' + Geral.FF0(VMI_Orig),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Orig, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
end;

procedure TFmVSGerClaCab.ReopenVMI_Sorc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Sorc, Dmod.MyDB, [
  'SELECT VMI_Sorc, VMI_Baix, VMI_Dest, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPaClaIts=' + Geral.FF0(QrVSPaClaItsControle.Value),
  'GROUP BY VMI_Sorc ',
  '']);
end;

procedure TFmVSGerClaCab.ReopenVSPallet;
begin
(*  Parei Aqui
 Nao tem Pallet!!
 fazer vspaclacaba.VSMovIts do Artigo Gerado!
*)
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT let.*,   ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
  ' CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, vps.Nome NO_STATUS    ',
  'FROM vspalleta let   ',
  'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   ',
  'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
  'WHERE let.Codigo=' + Geral.FF0(QrVSGerClaVSPallet.Value),
  ' ']);
*)
end;

procedure TFmVSGerClaCab.ReopenVSPaMulCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulCab, Dmod.MyDB, [
  'SELECT pra.*,',
  'cn1.FatorInt  ',
  'FROM vspamulcaba pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VMI_Sorc',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pra.VSGerArt=' + Geral.FF0(QrVSGerClaCodigo.Value),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerClaCab.ReopenVSPaMulIts(Controle: Integer);
var
  TemIMEIMrt, MovimCod: Integer;
begin
{$IfDef sAllVS}
  TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  MovimCod   := QrVSPaMulCabMovimCod.Value;
  VS_PF.ReopenVSPaMulIts(QrVSPaMulIts, TemIMEIMrt, MovimCod, Controle);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;


procedure TFmVSGerClaCab.ReopenVSPaClaCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  ',
  '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_TXT,',
///// Usa apenas CO_SEL_TAB_VMI porque usa cn1.FatorInt apenas para corrigir baixas!
  'cn1.FatorInt  ',
  'FROM vspaclacaba pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VSMovIts',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
////////////////////////////////////////////////////////////////////////////////
  'WHERE pra.VSGerArt=' + Geral.FF0(QrVSGerClaCodigo.Value),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerClaCab.ReopenVSPaClaIts();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT ',
  'FROM vspaclaitsa pri ',
  'WHERE pri.Codigo=' + Geral.FF0(QrVSPaClaCabCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT, ',
  'cn1.FatorInt  ',
  'FROM vspaclaitsa pri ',
  'LEFT JOIN vspalleta  let ON let.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pri.Codigo=' + Geral.FF0(QrVSPaclaCabCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
end;

procedure TFmVSGerClaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSGerClaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSGerClaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSGerClaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSGerClaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSGerClaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerClaCab.BtVoltaAClassificarClick(Sender: TObject);
const
  DtHrFimCla = '0000-00-00 00:00:00';
var
  Qry: TmySQLQuery;
  Pecas: Double;
  Codigo: Integer;
begin
(*  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM v s m o v i t s ',
    'WHERE Controle=' + Geral.FF0(QrVSPaClaCabVSMovIts.Value),
    '']);
    Pecas := Qry.FieldByName('SdoVrtPeca').AsFloat;
    if Pecas <=0 then
    begin
      Geral.MB_Aviso('Altera��o cancelada! IME-I com estoque zerado ou negativo!');
      Exit;
    end;
*)
      Codigo := QrVSPaClaCabCodigo.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      'DtHrFimCla'], ['Codigo'], [
      DtHrFimCla], [Codigo], True);
      //
      Codigo := QrVSGerClaCodigo.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
      'DtHrFimCla'], ['Codigo'], [
      DtHrFimCla], [Codigo], True);
      //
      LocCod(Codigo, Codigo);
(*
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmVSGerClaCab.AlteradadosdoartigodoIMEI1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
begin
  VS_CRC_PF.MostraFormVSMovItsAlt(QrVSPaClaItsVMI_Dest.Value, AtualizaSaldoModoGenerico,
    [eegbDadosArtigo]);
end;

procedure TFmVSGerClaCab.BtAlterarDatasClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSGerClaCodigo.Value;
  //
  VS_Jan.MostraFormVSGerArtDatas(Codigo, QrVSGerClaMovimCod.Value,
  QrVSGerClaDtHrAberto.Value, QrVSGerClaDtHrLibCla.Value,
  QrVSGerClaDtHrCfgCla.Value, QrVSGerClaDtHrFimCla.Value, emidIndsXX);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSGerClaCab.BtCorrigeBaixasClick(Sender: TObject);
begin
  CoriigeBaixasDaOCAtual();
  LocCod(QrVSGerClaCodigo.Value, QrVSGerClaCodigo.Value);
end;

procedure TFmVSGerClaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSGerClaCodigo.Value;
  Close;
end;

procedure TFmVSGerClaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  DGVSPaClaIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSGerClaCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSGerClaCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSGerClaCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSGerClaCodigo.Value, LaRegistro.Caption);
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtA, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM vsgerarta ',
  'ORDER BY Codigo ',
  '']);
  QrVSGerArtA.First;
  while not QrVSGerArtA.Eof do
  begin
    LocCod(QrVSGerArtACodigo.Value, QrVSGerArtACodigo.Value);
    Application.ProcessMessages;
    CoriigeBaixasDaOCAtual();
    //
    QrVSGerArtA.Next;
  end;
end;

procedure TFmVSGerClaCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSGerClaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSGerClaCab.QrVMI_OrigAfterOpen(DataSet: TDataSet);
begin
  BtVoltaAClassificar.Enabled :=
    (QrVMI_OrigID_TTW.Value = 0) and (QrVMI_OrigSdoVrtPeca.Value > 0);
  BtCorrigeBaixas.Enabled := BtVoltaAClassificar.Enabled;
end;

procedure TFmVSGerClaCab.QrVMI_OrigBeforeClose(DataSet: TDataSet);
begin
  BtVoltaAClassificar.Enabled := False;
  BtVoltaAClassificar.Enabled := False;
end;

procedure TFmVSGerClaCab.QrVSGerClaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtAlterarDatas.Enabled := QrVSGerCla.RecordCount > 0;
end;

procedure TFmVSGerClaCab.QrVSGerClaAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPallet();
  ReopenVSPaClaCab();
  ReopenVSPaMulCab()
end;

procedure TFmVSGerClaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSGerClaCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSGerClaCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSGerClaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsgerarta', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSGerClaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerClaCab.IrparajaneladedadosdoIMEI1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovIts(QrVSPaClaItsVMI_Dest.Value);
end;

procedure TFmVSGerClaCab.QrVSGerClaBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPaClaCab.Close;
  //QrVSPallet.Close;
  BtAlterarDatas.Enabled := False;
end;

procedure TFmVSGerClaCab.QrVSGerClaBeforeOpen(DataSet: TDataSet);
begin
  QrVSGerClaCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSGerClaCab.QrVSGerClaCalcFields(DataSet: TDataSet);
begin
  case QrVSGerClaTipoArea.Value of
    0: QrVSGerClaNO_TIPO.Value := 'm�';
    1: QrVSGerClaNO_TIPO.Value := 'ft�';
    else QrVSGerClaNO_TIPO.Value := '???';
  end;
  QrVSGerClaNO_DtHrLibCla.Value := Geral.FDT(QrVSGerClaDtHrLibCla.Value, 106, True);
  QrVSGerClaNO_DtHrFimCla.Value := Geral.FDT(QrVSGerClaDtHrFimCla.Value, 106, True);
  QrVSGerClaNO_DtHrCfgCla.Value := Geral.FDT(QrVSGerClaDtHrCfgCla.Value, 106, True);
end;

procedure TFmVSGerClaCab.Qr_AfterScroll(DataSet: TDataSet);
begin
  //ReopenSumPall();
end;

procedure TFmVSGerClaCab.Qr_BeforeClose(DataSet: TDataSet);
begin
  //QrSumPall.Close;
end;

procedure TFmVSGerClaCab.Qr_CalcFields(DataSet: TDataSet);
begin
  //QrVSPalletDtHrEndAdd_TXT.Value := Geral.FDT(QrVSPalletDtHrEndAdd.Value, 106, True);
end;

procedure TFmVSGerClaCab.QrVSPaClaCabAfterOpen(DataSet: TDataSet);
begin
  if QrVSPaClaCab.RecordCount > 0 then
    PCFormaClas.ActivePageIndex := 0;
end;

procedure TFmVSGerClaCab.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVMI_Orig(QrVSPaClaCabVSMovIts.Value);
  ReopenVSPaClaIts();
end;

procedure TFmVSGerClaCab.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaClaIts.Close;
  QrVMI_Orig.Close;
end;

procedure TFmVSGerClaCab.QrVSPaClaCabCalcFields(DataSet: TDataSet);
begin
  ReopenSum('AND Codigo=' + Geral.FF0(QrVSPaClaCabCodigo.Value));
  QrVSPaClaCabPecas.Value  := QrSumPecas.Value;
  QrVSPaClaCabAreaM2.Value := QrSumAreaM2.Value;
  QrVSPaClaCabAreaP2.Value := QrSumAreaP2.Value;
end;

procedure TFmVSGerClaCab.QrVSPaClaItsAfterScroll(DataSet: TDataSet);
begin
  ReopenVMI_Baix();
  ReopenVMI_Sorc();
end;

procedure TFmVSGerClaCab.QrVSPaClaItsBeforeClose(DataSet: TDataSet);
begin
  QrVMI_Baix.Close;
  QrVMI_Sorc.Close;
end;

procedure TFmVSGerClaCab.QrVSPaMulCabAfterOpen(DataSet: TDataSet);
begin
  if QrVSPaMulCab.RecordCount > 0 then
    PCFormaClas.ActivePageIndex := 1;
end;

procedure TFmVSGerClaCab.QrVSPaMulCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaMulIts(0);
end;

procedure TFmVSGerClaCab.QrVSPaMulCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaMulIts.Close;
end;

/////////////// OC
(*
SELECT VSPaClaIts, VMI_Dest, VMI_Sorc, VMI_Baix,
SUM(Pecas) Pecas, SUM(AreaM2) AreaM2
FROM vscacitsa
WHERE CacCod=65
GROUP BY VSPaClaIts, VMI_Dest, VMI_Sorc, VMI_Baix
ORDER BY VSPaClaIts, VMI_Dest, VMI_Sorc, VMI_Baix
*)

//Fazer Qr_ pelo vspaclacaba.VSMovIts
end.

