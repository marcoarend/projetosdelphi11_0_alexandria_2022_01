unit VSGerArtDdImpCur;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet, UnGrl_Consts,
  System.UITypes, AppListas;

type
  TArrTipo = (arrtipoNenhum = 0, arrtipoOutrosEmids = 1, arrtipoEmid6 = 2);
  TFmVSGerArtDdImpCur = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    GroupBox13: TGroupBox;
    TPCompraIni: TdmkEditDateTimePicker;
    CkCompraIni: TCheckBox;
    CkCompraFim: TCheckBox;
    TPCompraFim: TdmkEditDateTimePicker;
    GroupBox1: TGroupBox;
    TPViagemIni: TdmkEditDateTimePicker;
    CkViagemIni: TCheckBox;
    CkViagemFim: TCheckBox;
    TPViagemFim: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    TPEntradaIni: TdmkEditDateTimePicker;
    CkEntradaIni: TCheckBox;
    CkEntradaFim: TCheckBox;
    TPEntradaFim: TdmkEditDateTimePicker;
    QrExec: TMySQLQuery;
    PB1: TProgressBar;
    QrVSGer: TMySQLQuery;
    QrDia: TMySQLQuery;
    QrDiaInnPecas: TFloatField;
    QrArtGeComodty: TMySQLQuery;
    QrArtGeComodtyArtGeComodty: TIntegerField;
    QrArtGeComodtySigla: TWideStringField;
    QrArtGeComodtyVisuRel: TIntegerField;
    QrArtGeComodtyShowPecas: TLargeintField;
    QrArtGeComodtyShowPesoKg: TLargeintField;
    QrArtGeComodtyShowAreaM2: TLargeintField;
    QrArtGeComodtyShowPercPc: TLargeintField;
    QrArtGeComodtyShowKgM2: TLargeintField;
    QrArtGeComodtyShowMediaM2: TLargeintField;
    frxWET_CURTI_243_1_A: TfrxReport;
    QrGafid: TMySQLQuery;
    BtImprime: TBitBtn;
    frxDsGafid: TfrxDBDataset;
    QrEmids: TMySQLQuery;
    QrEmidsNO_MovimID: TWideStringField;
    QrEmidsDstMovID: TLargeintField;
    DBGrid1: TDBGrid;
    DsVSGer: TDataSource;
    QrVSInn: TMySQLQuery;
    QrVSInnGGXInn: TLargeintField;
    QrVSInnAnoMesDiaIn: TIntegerField;
    QrVSInnData: TWideStringField;
    QrVSInnInnPecas: TFloatField;
    QrVSInnInnPesoKg: TFloatField;
    QrVSInnInnKgPeca: TFloatField;
    QrVSInnNFePecas: TFloatField;
    QrVSInnInnSdoVrtPeso: TFloatField;
    QrVSInnInnSdoVrtPeca: TFloatField;
    QrNaoProc: TMySQLQuery;
    QrNaoProcInnSdoVrtPeca: TFloatField;
    CkTemIMEIMrt: TCheckBox;
    GroupBox3: TGroupBox;
    RGFonte: TRadioGroup;
    QrOrfaos: TMySQLQuery;
    QrOrfaosGraGruX: TIntegerField;
    QrOrfaosGraGruY: TIntegerField;
    QrOrfaosNome: TWideStringField;
    DsOrfaos: TDataSource;
    SbMesAnt: TSpeedButton;
    SpeedButton2: TSpeedButton;
    QrVSCal: TMySQLQuery;
    QrVSCalGGXInn: TLargeintField;
    QrVSCalGraGruX: TLargeintField;
    QrVSCalDstMovID: TLargeintField;
    QrVSCalDstNivel1: TLargeintField;
    QrVSCalAnoMesDiaIn: TIntegerField;
    QrVSCalArtGeComodty: TIntegerField;
    QrVSCalPecas: TFloatField;
    QrVSCalPesoKg: TFloatField;
    QrVSCur: TMySQLQuery;
    QrVSCurGGXInn: TLargeintField;
    QrVSCurGraGruX: TLargeintField;
    QrVSCurDstMovID: TLargeintField;
    QrVSCurDstNivel1: TLargeintField;
    QrVSCurAnoMesDiaIn: TIntegerField;
    QrVSCurArtGeComodty: TIntegerField;
    QrVSCurPecas: TFloatField;
    QrVSCurPesoKg: TFloatField;
    QrVSGerGGXInn: TLargeintField;
    QrVSGerGraGruX: TLargeintField;
    QrVSGerDstMovID: TLargeintField;
    QrVSGerDstNivel1: TLargeintField;
    QrVSGerAnoMesDiaIn: TIntegerField;
    QrVSGerArtGeComodty: TIntegerField;
    QrVSGerPecas: TFloatField;
    QrVSGerPesoKg: TFloatField;
    QrVSGerQtdGerArM2: TFloatField;
    QrVSGerQtdGerArP2: TFloatField;
    QrVSGerRendKgM2: TFloatField;
    QrVSGerMediaM2: TFloatField;
    QrVSMpC: TMySQLQuery;
    QrVSMpCGGXInn: TLargeintField;
    QrVSMpCGraGruX: TLargeintField;
    QrVSMpCDstMovID: TLargeintField;
    QrVSMpCDstNivel1: TLargeintField;
    QrVSMpCAnoMesDiaIn: TIntegerField;
    QrVSMpCArtGeComodty: TIntegerField;
    QrVSMpCPecas: TFloatField;
    QrVSMpCPesoKg: TFloatField;
    QrVSEmC: TMySQLQuery;
    QrVSEmCGGXInn: TLargeintField;
    QrVSEmCGraGruX: TLargeintField;
    QrVSEmCDstMovID: TLargeintField;
    QrVSEmCDstNivel1: TLargeintField;
    QrVSEmCAnoMesDiaIn: TIntegerField;
    QrVSEmCArtGeComodty: TIntegerField;
    QrVSEmCPecas: TFloatField;
    QrVSEmCPesoKg: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_241_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CkCompraIniClick(Sender: TObject);
    procedure CkCompraFimClick(Sender: TObject);
    procedure CkViagemIniClick(Sender: TObject);
    procedure CkViagemFimClick(Sender: TObject);
    procedure CkEntradaIniClick(Sender: TObject);
    procedure CkEntradaFimClick(Sender: TObject);
    procedure TPCompraIniClick(Sender: TObject);
    procedure TPCompraFimClick(Sender: TObject);
    procedure TPViagemIniClick(Sender: TObject);
    procedure TPViagemFimClick(Sender: TObject);
    procedure TPEntradaIniClick(Sender: TObject);
    procedure TPEntradaFimClick(Sender: TObject);
    procedure TPCompraIniRedefInPlace(Sender: TObject);
    procedure TPCompraFimRedefInPlace(Sender: TObject);
    procedure TPViagemIniRedefInPlace(Sender: TObject);
    procedure TPViagemFimRedefInPlace(Sender: TObject);
    procedure TPEntradaIniRedefInPlace(Sender: TObject);
    procedure TPEntradaFimRedefInPlace(Sender: TObject);
    procedure CkTemIMEIMrtClick(Sender: TObject);
    procedure RGPesquisaClick(Sender: TObject);
    procedure SbMesAntClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }

    FVSInnIts, FVSGerEmC, FVSGerMpC, FVSGerArt, FGerFromIn: String;
    FVSGerCal, FVSGerCur: String;
    FArrAGC: Array of array[0..7] of Integer;
    FArrSigla: Array of String;
    FArrTipo: Array of Integer;
    //
    PageHeader1: TfrxPageHeader;
    GroupHeader1: TfrxGroupHeader;
    GroupFooter1: TfrxGroupFooter;
    PageFooter1: TfrxPageFooter;
    ReportSummary1: TfrxReportSummary;
    MeTitEntrada, MeTitCaleiro, MeSubTitEntradaData, MeSubTitEntradaPecas,
    MeSubTitEntradaPesoKg, MeSubTitEntradaKgPc, MeSubTitEntradaNFePc,
    MeSubTitCaleiroPecas: TfrxMemoView;
    MeValEntradaData, MeValEntradaPecas,
    MeValEntradaPesoKg, MeValEntradaKgPc, MeValEntradaNFePc,
    MeValCaleiroPecas,
    MeSumTextoTotal, MeSumEntradaPecas,
    MeSumEntradaPesoKg, MeSumEntradaKgPc, MeSumEntradaNFePc,
    MeSumCaleiroPecas: TfrxMemoView;

    MasterData1: TfrxMasterData;

    //
    procedure ReopenVSGerEmC(Corda: String);
    procedure ReopenVSGerMpC(Corda: String);
    procedure ReopenVSGerCal(Corda: String);
    procedure ReopenVSGerCur(Corda: String);
    procedure ReopenVSGerArt(Corda: String);
    procedure ReopenVSInnIts();
    function  ConversaoCm(Redimensiona, HalfH: Boolean; Centimetros: Extended): Extended;
    procedure DesfazPesquisa();
    function  Desconversao(Medidafxt: Extended): Extended;
    //
    procedure MeHed(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String;FontStyle:
      (*Vcl.Graphics.TFont.*)TFontStyles; FontSize: Integer;
      FrameTyp:(* frxClass.TfrxFrame.*)TfrxFrameTypes; const Texto: String);
    procedure MeTit(var Memo: TfrxMemoView; var Left: Extended; const Top, Width,
      Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
    procedure MeSub(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String);
    procedure MeVal(var Memo: TfrxMemoView; var Left: Extended; const Top,
      Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
      const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeGru(var Memo: TfrxMemoView; var Left: Extended; const Top,
      Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
      const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeSum(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String);
    procedure MeTot(var Memo: TfrxMemoView; const Left, Top, Width, Height:
      Extended; const HAlign: TfrxHAlign; const Texto: String; const FormatKind:
      frxClass.TfrxFormatKind; const FormatStr: String);
    function  GetFontHeightFromFontSize(FontSize: Integer): Integer;
    function  VerificaOrfaos(): Integer;

  public
    { Public declarations }
  end;

  var
  FmVSGerArtDdImpCur: TFmVSGerArtDdImpCur;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, ModuleGeral, UnVS_PF,
  CreateVS, UMySQLModule, MyDBCheck, UnVS_Jan, UnGrade_Jan;

{$R *.DFM}

const
  FCmFrx = 37.7953;
  FGapBan = 1.10; // cm
  FEspecoTexto = 0.20; // cm
  FFonteMinima = 6;
  FGapCol = 0.05;

procedure TFmVSGerArtDdImpCur.BtImprimeClick(Sender: TObject);
  function FmtItem(Campo: String; Casas, Item: Integer): String;
  var
    sItem, sCasas: String;
  begin
    if Item > 0 then
      sItem := Geral.FF0(Item)
    else
      sItem := '';
    case casas of
      1: sCasas := '.0';
      2: sCasas := '.00';
      3: sCasas := '.000';
      4: sCasas := '.0000';
      else sCasas := '';
    end;
    Result := '[FormatFloat(''#,###,###,##0' + sCasas + ';-#,###,###,##0' + sCasas + '; '',<frxDsGafid."' + Campo + sItem + '">)]';
  end;
var
  DataPage: TfrxDataPage;
  Page: TfrxReportPage;
  Shape1, Shape2: TfrxShapeView;
  MeDono, MeTitulo, MeData, MePagina: TfrxMemoView;
  Line1: TfrxLineView;
  MeTitPeriCompra, MeValPeriCompra, MeTitPeriViagem, MeValPeriViagem,
  MeTitPeriEntrada, MeValPeriEntrada,
  MeGroupHeader: TfrxMemoView;
  L, LTit, TLin, HBan, T, Linha, WTit: Extended;
  Memo: TfrxMemoView;
  I, Item: Integer;
  x, s: String;
  TemNaoProcessados: Boolean;
begin
  frxDsGafid.FieldAliases.Clear;
  //frxDsGafid.FieldAliases.Clear;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGafid, DModG.MyPID_DB, [
  'SELECT gg1.Nome  NO_GGXInn, afi.*  ',
  'FROM _vs_ger_art_from_in_dd afi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=afi.GGXInn ',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNaoProc, DModG.MyPID_DB, [
  'SELECT SUM(InnSdoVrtPeca) InnSdoVrtPeca ',
  'FROM _vs_ger_art_from_in_dd ',
  '']);
  TemNaoProcessados := QrNaoProcInnSdoVrtPeca.Value >= 0.1;
  { clear a report }
  frxWET_CURTI_243_1_A.Clear;
  frxWET_CURTI_243_1_A.OnGetValue := frxWET_CURTI_241_01GetValue;
  { add a dataset to the list of ones accessible for a report }
  frxWET_CURTI_243_1_A.DataSets.Add(frxDsGafid);
  frxWET_CURTI_243_1_A.DataSets.Add(DModG.frxDsDono);
  { add the "Data" page }
  DataPage := TfrxDataPage.Create(frxWET_CURTI_243_1_A);
  { add a page }
  Page := TfrxReportPage.Create(frxWET_CURTI_243_1_A);
  { create a unique name }
  Page.CreateUniqueName;
  { set sizes of fields, paper and orientation by default }
  Page.SetDefaults;
  { modify paper�s orientation }
  Page.Orientation := System.UITypes.TPrinterOrientation.poLandscape;

  // Margens da p�gina em mm
  Page.LeftMargin := 10;
  Page.RightMargin := 10;
  Page.TopMargin := 10;
  Page.BottomMargin := 10;

  // Cabe�alho da(s) p�gina(s)
  PageHeader1 := TfrxPageHeader.Create(Page);
  PageHeader1.CreateUniqueName;
  //PageHeader1.Name := 'PageHeader1';
  PageHeader1.FillType := ftBrush;
  PageHeader1.FillGap.Top := 0;
  PageHeader1.FillGap.Left := 0;
  PageHeader1.FillGap.Bottom := 0;
  PageHeader1.FillGap.Right := 0;
  PageHeader1.Frame.Typ := [];
  Linha := ConversaoCm(True, True, 0.40);
                        // duas linhas de t�tulo
  //PageHeader1.Height := ConversaoCm(False, False, 1.60) + (2 * Linha); // 90.708710240000000000;
  PageHeader1.Height := ConversaoCm(False, False, 1.60); // 90.708710240000000000;
  PageHeader1.Top := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
  HBan := Desconversao(PageHeader1.Height + PageHeader1.Top + FGapBan);
  { this object will be stretched according to band�s width }
  //PageHeader1.Align := baWidth;
  PageHeader1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;

////////////////////////////////////////////////////////////////////////////////
/////////  D A D O S   G E N � R I C O S  C A B E � A L H O  ///////////////////
////////////////////////////////////////////////////////////////////////////////

  Shape1 := TfrxShapeView.Create(PageHeader1);
  Shape1.AllowVectorExport := True;
  Shape1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
  Shape1.Height := ConversaoCm(False, False, 1.0); // 37.795300000000000000;
  Shape1.Frame.Typ := [];
  Shape1.Frame.Width := 0.100000000000000000;
  Shape1.Shape := skRoundRectangle;

  Line1 := TfrxLineView.Create(PageHeader1);
  Line1.AllowVectorExport := True;
  Line1.Top := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
  Line1.Width := ConversaoCm(False, False, 27.7); // 1046.929810000000000000;
  Line1.Color := clBlack;
  Line1.Frame.Typ := [ftTop];
  Line1.Frame.Width := 0.100000000000000000;

  MeHed(MeDono,       0.20,  0.00, 27.30, 0.50, haCenter, fkText, '', [fsBold], 08, [], '[frxDsDono."NOMEDONO"]');
  MeHed(MeTitulo,     4.00,  0.50, 19.70, 0.50, haCenter, fkText, '', [fsBold], 09, [], 'Controle de Produ��o de Artigo Gerado');
  MeHed(MeData,       0.20,  0.50,  3.80, 0.50, haLeft, fkDateTime, 'dd/mm/yyyy hh:nn:ss', [], 08, [], '[VARF_DATA]');
  MeHed(MePagina,    23.70,  0.50,  3.80, 0.50, haRight, fkText, '', [], 08, [], 'P'#225'gina [Page#] de [TotalPages#]');

////////////////////////////////////////////////////////////////////////////////
/////////  F I L T R O S   D E  P E S Q U I S A   ( D A T A  S )  //////////////
////////////////////////////////////////////////////////////////////////////////

  Shape2 := TfrxShapeView.Create(PageHeader1);
  Shape2.AllowVectorExport := True;
  Shape2.Top := ConversaoCm(False, False, 1.1); // 41.574830000000000000;
  Shape2.Width := ConversaoCm(False, False, 27.7); // 1046.9291509400000000;
  Shape2.Height := ConversaoCm(False, False, 0.5); // 18.897650000000000000;
  Shape2.Frame.Typ := [];
  Shape2.Frame.Width := 0.100000000000000000;

  MeHed(MeTitPeriCompra,     0.00,  1.10,  2.50, 0.50, haLeft, fkText, '', [], 08, [], 'Per'#237'odo compra:');
  MeHed(MeValPeriCompra,     2.50,  1.10,  6.40, 0.50, haLeft, fkText, '', [fsBold], 08, [], '[VARF_PeriodoCompra]');
  MeHed(MeTitPeriViagem,     9.00,  1.10,  2.50, 0.50, haLeft, fkText, '', [], 08, [], 'Per'#237'odo sa'#237'da:');
  MeHed(MeValPeriViagem,    11.50,  1.10,  6.40, 0.50, haLeft, fkText, '', [fsBold], 08, [], '[VARF_PeriodoViagem]');
  MeHed(MeTitPeriEntrada,   18.00,  1.10,  2.50, 0.50, haLeft, fkText, '', [], 08, [], 'Per'#237'odo entrada:');
  MeHed(MeValPeriEntrada,   20.50,  1.10,  6.40, 0.50, haLeft, fkText, '', [fsBold], 08, [], '[VARF_PeriodoEntrada]');

////////////////////////////////////////////////////////////////////////////////
/////////  D A T A B A N D   D E   D A D O S  E  T O T A I S  //////////////////
////////////////////////////////////////////////////////////////////////////////

  TLin := ConversaoCm(True, True, 0.5);
  GroupHeader1 := TfrxGroupHeader.Create(Page);
  GroupHeader1.Name := 'GroupHeader1';
  GroupHeader1.FillType := ftBrush;
  GroupHeader1.FillGap.Top := 0;
  GroupHeader1.FillGap.Left := 0;
  GroupHeader1.FillGap.Bottom := 0;
  GroupHeader1.FillGap.Right := 0;
  GroupHeader1.Frame.Typ := [];
  GroupHeader1.Height := TLin + (2 * Linha); // 22.677180000000000000
  GroupHeader1.Top := ConversaoCm(True, True, HBan); // 170.078850000000000000
  GroupHeader1.Width := ConversaoCm(True, True, 27.7); // 1046.929810000000000000
  GroupHeader1.Condition := 'frxDsGafid."GGXInn"';
  HBan := Desconversao(GroupHeader1.Top + GroupHeader1.Height + FGapBan);

  L := 0.00;
  MeGru(MeGroupHeader,       L, 0.00, 27.7, 0.50, haLeft, '[frxDsGafid."NO_GGXInn"]', fkText, '');

  MasterData1 := TfrxMasterData.Create(Page);
  MasterData1.Name := 'MasterData1';
  MasterData1.FillType := ftBrush;
  MasterData1.FillGap.Top := 0;
  MasterData1.FillGap.Left := 0;
  MasterData1.FillGap.Bottom := 0;
  MasterData1.FillGap.Right := 0;
  MasterData1.Frame.Typ := [];
  MasterData1.Height := ConversaoCm(True, True, 0.4); // 15.118120000000000000;
  MasterData1.Top := ConversaoCm(False, False, HBan); // 170.078850000000000000;
  MasterData1.Width := ConversaoCm(True, False, 27.7); // 1046.929810000000000000;
  MasterData1.DataSet := frxDsGafid;
  MasterData1.DataSetName := 'frxDsGafid';
  MasterData1.RowCount := 0;
  HBan := Desconversao(MasterData1.Top + MasterData1.Height + FGapBan);


  GroupFooter1 := TfrxGroupFooter.Create(Page);
  GroupFooter1.Name := 'GroupFooter1';
  GroupFooter1.FillType := ftBrush;
  GroupFooter1.FillGap.Top := 0;
  GroupFooter1.FillGap.Left := 0;
  GroupFooter1.FillGap.Bottom := 0;
  GroupFooter1.FillGap.Right := 0;
  GroupFooter1.Frame.Typ := [];
  GroupFooter1.Height := ConversaoCm(True, True, 0.80); // := 30.236220470000000000
  GroupFooter1.Top := ConversaoCm(True, True, HBan); // := 253.228510000000000000
  GroupFooter1.Width := ConversaoCm(True, True, 27.7); // := 1046.929810000000000000

  HBan := Desconversao(GroupFooter1.Top + GroupFooter1.Height + FGapBan);

  ReportSummary1 := TfrxReportSummary.Create(Page);
  ReportSummary1.FillType := ftBrush;
  ReportSummary1.FillGap.Top := 0;
  ReportSummary1.FillGap.Left := 0;
  ReportSummary1.FillGap.Bottom := 0;
  ReportSummary1.FillGap.Right := 0;
  ReportSummary1.Frame.Typ := [];
  ReportSummary1.Height := ConversaoCm(True, True, 1.00); //30.236220470000000000
  ReportSummary1.Top := ConversaoCm(True, True, HBan); //343.937230000000000000
  ReportSummary1.Width := ConversaoCm(True, True, 0.8); //1046.929810000000000000

  HBan := Desconversao(ReportSummary1.Top + ReportSummary1.Height + FGapBan);

////////////////////////////////////////////////////////////////////////////////
/////////  T I T U L O S  E  D A D A O S  D E   I N   N A T U R A  /////////////
////////////////////////////////////////////////////////////////////////////////
  LTit := 0.00;
  TLin := DesConversao(TLin);
  MeTit(MeTitEntrada,           LTit, TLin, 3.65, 0.40, haCenter, 'Entrada');
  LTit := LTit + FGapCol;
  MeTit(MeTitCaleiro,           LTit, TLin, 1.30, 0.40, haCenter, 'Caleiro');
  LTit := LTit + FGapCol;
  MeTit(Memo,                   LTit, TLin, 1.30, 0.40, haCenter, 'Curtimento');
  LTit := LTit + FGapCol;
  if TemNaoProcessados then
  begin
    MeTit(Memo,                   LTit, TLin, 1.50, 0.40, haCenter, 'N�o Processados');
  end;
  LTit := LTit + FGapCol;
  L := 0.00;
  T := Desconversao(MeTitEntrada.Top + MeTitEntrada.Height);

  Item := 0;
  MeSub(MeSubTitEntradaData,    L,   T , 0.85, 0.40, haCenter, 'Data');
  MeSum(MeSumTextoTotal,        L, 0.00, 0.85, 0.40, haLeft, 'Subtotal: ', fkText, '');
  MeTot(MeSumTextoTotal,        L, 0.20, 0.85, 0.40, haLeft, 'TOTAL GERAL: ', fkText, '');
  Meval(MeValEntradaData,       L, 0.00, 0.85, 0.40, haCenter, '[frxDsGafid."Data"]', fkDateTime, 'dd/mm/yy');

  MeSub(MeSubTitEntradaPecas,   L,   T , 0.70, 0.40, haRight, 'Pe�as');
  MeSum(MeSumEntradaPecas,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnPecas">,MasterData1)]', fkNumeric, '%2.0n');
  MeTot(MeSumEntradaPecas,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnPecas">,MasterData1)]', fkNumeric, '%2.0n');
  Meval(MeValEntradaPecas,      L, 0.00, 0.70, 0.40, haRight, FmtItem('InnPecas', 0, Item), fkText, '');

  MeSub(MeSubTitEntradaPesoKg,  L,   T , 0.80, 0.40, haRight, 'Peso kg');
  MeSum(MeSumEntradaPesoKg,     L, 0.00, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnPesoKg">,MasterData1)]', fkNumeric, '%2.0n');
  MeTot(MeSumEntradaPesoKg,     L, 0.20, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnPesoKg">,MasterData1)]', fkNumeric, '%2.0n');
  Meval(MeValEntradaPesoKg,     L, 0.00, 0.80, 0.40, haRight, FmtItem('InnPesoKg', 0, Item), fkText, '');

  MeSub(MeSubTitEntradaKgPc,    L,   T , 0.60, 0.40, haRight, 'kg/p�');
  MeSum(MeSumEntradaKgPc,       L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."InnPesoKg">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1))]', fkNumeric, '%2.2n');
  MeTot(MeSumEntradaKgPc,       L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."InnPesoKg">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1))]', fkNumeric, '%2.2n');
  Meval(MeValEntradaKgPc,       L, 0.00, 0.60, 0.40, haRight, FmtItem('InnKgPeca', 2, Item), fkText, '');

  MeSub(MeSubTitEntradaNFePc,   L,   T , 0.70, 0.40, haRight, 'NFe p�');
  MeSum(MeSumEntradaNFePc,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."NFePecas">,MasterData1)]', fkNumeric, '%2.0n');
  MeTot(MeSumEntradaNFePc,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."NFePecas">,MasterData1)]', fkNumeric, '%2.0n');
  Meval(MeValEntradaNFePc,      L, 0.00, 0.70, 0.40, haRight, FmtItem('NFePecas', 0, Item), fkText, '');

  L := L + FGapCol;

  MeSub(MeSubTitCaleiroPecas,   L,   T , 0.70, 0.40, haRight, 'Pe�as');
  MeSum(MeSumCaleiroPecas,      L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CalPecas">,MasterData1)]', fkNumeric, '%2.0n');
  MeTot(MeSumCaleiroPecas,      L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CalPecas">,MasterData1)]', fkNumeric, '%2.0n');
  Meval(MeValCaleiroPecas,      L, 0.00, 0.70, 0.40, haRight, FmtItem('CalPecas', 0, Item), fkText, '');

  MeSub(Memo,                   L,   T , 0.60, 0.40, haRight, '% P�');
  MeSum(Memo,                   L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."CalPecas' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
  MeTot(Memo,                   L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."CalPecas' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
  Meval(Memo,                   L, 0.00, 0.60, 0.40, haRight, FmtItem('CalPercCo', 1, Item), fkText, '');

  L := L + FGapCol;

  MeSub(Memo,                   L,   T , 0.70, 0.40, haRight, 'Pe�as');
  MeSum(Memo,                   L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CurPecas">,MasterData1)]', fkNumeric, '%2.0n');
  MeTot(Memo,                   L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."CurPecas">,MasterData1)]', fkNumeric, '%2.0n');
  Meval(Memo,                   L, 0.00, 0.70, 0.40, haRight, FmtItem('CurPecas', 1, Item), fkText, '');

  MeSub(Memo,                   L,   T , 0.60, 0.40, haRight, '% P�');
  MeSum(Memo,                   L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."CurPecas' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
  MeTot(Memo,                   L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."CurPecas' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
  Meval(Memo,                   L, 0.00, 0.60, 0.40, haRight, FmtItem('CurPercCo', 1, Item), fkText, '');

  L := L + FGapCol;

  if TemNaoProcessados then
  begin
    MeSub(Memo, L,   T , 0.70, 0.40, haRight, 'Pe�as');
    MeSum(Memo, L, 0.00, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeca">,MasterData1)]', fkNumeric, '%2.0n');
    MeTot(Memo, L, 0.20, 0.70, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeca">,MasterData1)]', fkNumeric, '%2.0n');
    Meval(Memo, L, 0.00, 0.70, 0.40, haRight, FmtItem('InnSdoVrtPeca', 0, Item), fkText, '');

    MeSub(Memo, L,   T , 0.80, 0.40, haRight, 'Peso kg');
    MeSum(Memo, L, 0.00, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeso">,MasterData1)]', fkNumeric, '%2.0n');
    MeTot(Memo, L, 0.20, 0.80, 0.40, haRight, '[SUM(<frxDsGafid."InnSdoVrtPeso">,MasterData1)]', fkNumeric, '%2.0n');
    Meval(Memo, L, 0.00, 0.80, 0.40, haRight, FmtItem('InnSdoVrtPeso', 0, Item), fkText, '');
  end;

  for I := 0 to Length(FArrAGC) - 1 do
  begin

    //FArrAGC[I][1] := QrArtGeComodtyVisuRel.Value;
    if FArrAGC[I][1] > 0 then
    begin
      L := L + FGapCol;
      x := Geral.FF0(I + 1);
      Item := I + 1;
      WTit := 0.00;
      if FArrAGC[I][2] > 0 then WTit := WTit + 0.70; // Pe�as
      if FArrAGC[I][3] > 0 then WTit := WTit + 0.80; // PesoKg
      if FArrAGC[I][4] > 0 then WTit := WTit + 1.00; // �rea m2
      if FArrAGC[I][5] > 0 then WTit := WTit + 0.60; // % P�
      if FArrAGC[I][6] > 0 then WTit := WTit + 0.60; // kg/m2
      if FArrAGC[I][7] > 0 then WTit := WTit + 0.60; // m2/p�

      MeTit(Memo, LTit, TLin, WTit, 0.40, haCenter, FArrSigla[I] );
      LTit := LTit + FGapCol;

      //FArrAGC[I][2] := QrArtGeComodtyShowPecas.Value;
      if FArrAGC[I][2] > 0 then
      begin
        s := 'frxDsGafid."Pecas_' + x + '"';
        MeSub(Memo, L,   T , 0.70, 0.40, haRight, 'Pe�as');
        MeSum(Memo, L, 0.00, 0.70, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
        MeTot(Memo, L, 0.20, 0.70, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
        //Meval(Memo, L, 0.00, 0.70, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 0.70, 0.40, haRight, FmtItem('Pecas_', 0, Item), fkText, '');
      end;
      //FArrAGC[I][3] := QrArtGeComodtyShowPesoKg.Value;
      if FArrAGC[I][3] > 0 then
      begin
        s := 'frxDsGafid."PesoKg_' + x + '"';
        MeSub(Memo, L,   T , 0.80, 0.40, haRight, 'Peso kg');
        MeSum(Memo, L, 0.00, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
        MeTot(Memo, L, 0.20, 0.80, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.0n');
        //Meval(Memo, L, 0.00, 0.80, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 0.80, 0.40, haRight, FmtItem('PesoKg_', 0, Item), fkText, '');
      end;
      //FArrAGC[I][4] := QrArtGeComodtyShowAreaM2.Value;
      if FArrAGC[I][4] > 0 then
      begin
        s := 'frxDsGafid."AreaM2_' + x + '"';
        MeSub(Memo, L,   T , 1.00, 0.40, haRight, '�rea m�');
        MeSum(Memo, L, 0.00, 1.00, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.2n');
        MeTot(Memo, L, 0.20, 1.00, 0.40, haRight, '[SUM(<' + s+ '>,MasterData1)]', fkNumeric, '%2.2n');
        //Meval(Memo, L, 0.00, 0.10, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 1.00, 0.40, haRight, FmtItem('AreaM2_', 2, Item), fkText, '');
      end;
      //FArrAGC[I][5] := QrArtGeComodtyShowPercPc.Value;
      if FArrAGC[I][5] > 0 then
      begin
        s := 'frxDsGafid."PercCo_' + x + '"';
        MeSub(Memo, L,   T , 0.60, 0.40, haRight, '% P�');
        MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
        MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."InnPecas">,MasterData1) = 0, 0, SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) / SUM(<frxDsGafid."InnPecas">,MasterData1) * 100 )]', fkNumeric, '%2.1n');
        //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('PercCo_', 1, Item), fkText, '');
      end;
      //FArrAGC[I][6] := QrArtGeComodtyShowKgM2.Value;
      if FArrAGC[I][6] > 0 then
      begin
        s := 'frxDsGafid."RendKgM2_' + x + '"';
        MeSub(Memo, L,   T , 0.60, 0.40, haRight, 'kg/m�');
        MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."PesoKg_' + x + '">,MasterData1) / SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
        MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."PesoKg_' + x + '">,MasterData1) / SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
        //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('RendKgM2_', 2, Item), fkText, '');
      end;
      //FArrAGC[I][7] := QrArtGeComodtyShowMediaM2.Value;
      if FArrAGC[I][7] > 0 then
      begin
        s := 'frxDsGafid."MediaM2_' + x + '"';
        MeSub(Memo, L,   T , 0.60, 0.40, haRight, 'm�/p�');
        MeSum(Memo, L, 0.00, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) / SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
        MeTot(Memo, L, 0.20, 0.60, 0.40, haRight, '[IIF(SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1) = 0, 0, SUM(<frxDsGafid."AreaM2_' + x + '">,MasterData1) / SUM(<frxDsGafid."Pecas_' + x + '">,MasterData1))]', fkNumeric, '%2.2n');
        //Meval(Memo, L, 0.00, 0.60, 0.40, haRight, '[' + s + ']', fkNumeric, '%2.0n');
        Meval(Memo, L, 0.00, 0.60, 0.40, haRight, FmtItem('MediaM2_', 2, Item), fkText, '');
      end;
    end;
  end;
  PageFooter1 := TfrxPageFooter.Create(Page);
  PageFooter1.FillType := ftBrush;
  PageFooter1.FillGap.Top := 0;
  PageFooter1.FillGap.Left := 0;
  PageFooter1.FillGap.Bottom := 0;
  PageFooter1.FillGap.Right := 0;
  PageFooter1.Frame.Typ := [];
  PageFooter1.Height := ConversaoCm(False, False, 0.40); //  := 15.118110240000000000
  PageFooter1.Top := ConversaoCm(True, True, HBan); //  := 343.937230000000000000
  PageFooter1.Width := ConversaoCm(False, False, 27.80); //   := 1046.929810000000000000

  HBan := Desconversao(PageFooter1.Top + PageFooter1.Height + FGapBan);

  Memo := TfrxMemoView.Create(PageFooter1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, 0.00);
  Memo.Width := ConversaoCm(False, False, 19.70); //744.567410000000000000
  Memo.Height := ConversaoCm(False, False, 0.40); //15.118110240000000000
  Memo.DisplayFormat.DecimalSeparator := ',';
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Height := -7;
  Memo.Font.Name := 'Arial';
  Memo.Font.Style := [] ;
  Memo.Frame.Typ := [ftTop];
  Memo.Frame.Width := 0.100000000000000000;
  Memo.Memo.Text := CONST_DMK_SOFTWARE_CUSTOMOZADO;
  Memo.ParentFont := False;

  Memo := TfrxMemoView.Create(PageFooter1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, 19.20); //725.669760000000000000;
  Memo.Width := ConversaoCm(False, False, 8.50); //321.260050000000000000;
  Memo.Height := ConversaoCm(False, False, 0.35); //:= 13.228346460000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Height := -8;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftTop];
  Memo.Frame.Width := 0.100000000000000000;
  Memo.HAlign := haRight;
  Memo.Memo.Text := '[VARF_CODI_FRX]';
  Memo.ParentFont := False;
  Memo.VAlign := vaCenter;
  //
  MyObjects.frxMostra(frxWET_CURTI_243_1_A, 'Controle de Produ��o de Artigo Gerado');
end;

procedure TFmVSGerArtDdImpCur.BtPesquisaClick(Sender: TObject);
var
  CordaBar, CordaEmC, CordaCal, CordaCur, SQL, Virgula, sAGC, SQL_GGXInn, SQL_GroupInn, SQL_GGXDst,
  SQL_GroupDst, SQLpc, SQLkg: String;
  Repeticoes, AMD, I, GGXInn, Item: Integer;
  Dia: TDateTime;
  Pecas_X, PesoKg_X, AreaM2_X, PercCo_X, RendKgM2_X, MediaM2_X, PercCo: Double;
  ArtGeComodty, AnoMesDiaIn: Integer;
  ATT_MovimID: String;
  Emids: Integer;
  Pecas, PesoKg: Double;
  SQL_Wher: String;
begin
  Screen.Cursor := crHourGlass;
  try

  if VerificaOrfaos() > 0 then
    Exit;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  FVSInnIts := UnCreateVS.RecriaTempTableNovo(ntrttVSInCabEIts, DModG.QrUpdPID1, False, 1);
  FVSGerEmC := UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_emc_cab_e_its'); // Em conserva��o
  FVSGerMpC := UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1, '_vs_mpc_cab_e_its'); // Conservados
  FVSGerArt := UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1, False, 1);
  //FGerFromIn := UnCreateVS.RecriaTempTableNovo(ntrttVSGerArtFromInDd, DModG.QrUpdPID1, False, Repeticoes);

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando in natura');
  ReopenVSInnIts();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados do in natura');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT * FROM ' + FVSInnIts,
  'ORDER BY DtEntrada, Codigo, Controle ',
  '']);
  CordaBar := MyObjects.CordaDeQuery(QrExec, 'Codigo', '0');
  //Geral.MB_Teste(CordaBar);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros conservados');
  ReopenVSGerEmC(CordaBar);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros em conserva��o');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT * FROM ' + FVSGerEmC,
  'ORDER BY AnoMesDiaIn, Codigo, Controle ',
  '']);
  CordaEmC := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //Geral.MB_Teste(CordaCon);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados de couros conservados');
  // _vs_sa_cab_e_its
  ReopenVSGerMpC(CordaEmC);
  //
  // ????
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros caleirados');
  FVSGerCal := UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1,
    False, 1, '_vs_ca_cab_e_its');
  ReopenVSGerCal(CordaBar);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta de couros curtidos');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT DISTINCT Controle ',
  'FROM ' + FVSGerCal,
  'ORDER BY Controle ',
  '']);
  CordaCal := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros curtidos');
  FVSGerCur := UnCreateVS.RecriaTempTableNovo(ntrttVSGACabEIts, DModG.QrUpdPID1,
    False, 1, '_vs_cu_cab_e_its');
  ReopenVSGerCur(CordaCal);

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta de couros curtidos');
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  'SELECT DISTINCT Controle ',
  'FROM ' + FVSGerCur,
  'ORDER BY Controle ',
  '']);
  CordaCur := MyObjects.CordaDeQuery(QrExec, 'Controle', '0');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando artigos gerados');
  ReopenVSGerArt(CordaCur);
  //
  ATT_MovimID := dmkPF.ArrayToTexto('dst.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmids, DModG.MyPID_DB, [
  'SELECT ' + ATT_MovimID + ' CAST(dst.MovimID AS SIGNED) DstMovID ',
  'FROM _vs_in_cab_e_its inn ',
  'LEFT JOIN _vs_ga_cab_e_its dst ON  ',
  '  dst.SrcMovID=inn.MovimID ',
  '  AND dst.SrcNivel1=inn.Codigo ',
  '  AND dst.SrcNivel2=inn.Controle ',
  'WHERE  ',
  '  dst.MovimID IS NULL ',
  '  OR dst.MovimID <> 6 ',
  'GROUP BY dst.MovimID',
  'ORDER BY dst.MovimID',
  '']);
  Emids := QrEmids.RecordCount;
  //if (Emids = 0) or (QrEmidsDstMovID.Value <> 0) then
  if QrEmidsDstMovID.Value <> 0 then
    Emids := Emids + 1;
  //Geral.MB_Teste(QrEmids.SQL.Text);

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela de jun��o');
  UnDmkDAC_PF.AbreMySQLQuery0(QrArtGeComodty, DmodG.MyPID_DB, [
(*
  'SELECT gci.ArtGeComodty, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM ' + FVSGerArt + ' gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty',
  'ORDER BY ArtGeComOrd, ArtGeComodty ',
*)
  'SELECT gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_ga_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  'UNION',
  '',
  'SELECT gci.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla, agc.VisuRel,',
  'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,',
  'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,',
  'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2',
  'FROM _vs_mpc_cab_e_its gci',
  'LEFT JOIN ' + TMeuDB + '.artgecomodty agc ON agc.Codigo=gci.ArtGeComodty',
  'GROUP BY ArtGeComodty ',
  '',
  'ORDER BY ArtGeComOrd, ArtGeComodty',
  '']);
  //Geral.MB_Teste(QrArtGeComodty.SQL.Text);
  
  Repeticoes := QrArtGeComodty.RecordCount + Emids;
  QrEmids.First;
  Item := 0;
  SetLength(FArrAGC, Repeticoes);
  SetLength(FArrSigla, Repeticoes);
  SetLength(FArrTipo, Repeticoes);
  while not QrEmids.Eof do
  begin
    if QrEmidsDstMovID.Value <> 0 then // Exclui n�o processados 100%
    begin
      Item := Item + 1;
      FArrTipo[Item] := Integer(arrtipoOutrosEmids); // Emid
      FArrSigla[Item] := QrEmidsNO_MovimID.Value;
      FArrAGC[Item][0] := QrEmidsDstMovID.Value;
      FArrAGC[Item][1] := 1;
      FArrAGC[Item][2] := 1;
      FArrAGC[Item][3] := 1;
      FArrAGC[Item][4] := 0;
      FArrAGC[Item][5] := 0;
      FArrAGC[Item][6] := 0;
      FArrAGC[Item][7] := 0;
    end;
    //
    QrEmids.Next;
  end;
  while not QrArtGeComodty.Eof do
  begin
    //Item := Emids + QrArtGeComodty.RecNo - 1;
    Item := Item + 1;
    FArrTipo[Item] := Integer(arrtipoEmid6); // Artigo de Emid 6
    FArrSigla[Item] := QrArtGeComodtySigla.Value;
    FArrAGC[Item][0] := QrArtGeComodtyArtGeComodty.Value;
    FArrAGC[Item][1] := 1; //QrArtGeComodtyVisuRel.Value; Obrigat�rio
    FArrAGC[Item][2] := 1; //QrArtGeComodtyShowPecas.Value; Obrigat�rio
    FArrAGC[Item][3] := QrArtGeComodtyShowPesoKg.Value;
    FArrAGC[Item][4] := QrArtGeComodtyShowAreaM2.Value;
    FArrAGC[Item][5] := QrArtGeComodtyShowPercPc.Value;
    FArrAGC[Item][6] := QrArtGeComodtyShowKgM2.Value;
    FArrAGC[Item][7] := QrArtGeComodtyShowMediaM2.Value;
    //
    QrArtGeComodty.Next;
  end;
  //
  if Repeticoes = 0 then
    Repeticoes := 1;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria de dados compilados');
  FGerFromIn := UnCreateVS.RecriaTempTableNovo(ntrttVSGerArtFromInDd, DModG.QrUpdPID1, False, Repeticoes);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dias do per�odo');
  //
(*
  case RGPesquisa.ItemIndex of
    0: // Rendimento*)
    begin
      // Inn
      SQL_GGXInn := 'CAST(GraGruX AS SIGNED)';
      SQL_GroupInn := 'GGXInn, AnoMesDiaIn';
      // Ger
      SQL_GGXDst := 'CAST(dst.GraGruX AS SIGNED)';
      SQL_GroupDst := 'AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty';
    end;
    (*1: // Produ��o
    begin
      // Inn
      SQL_GGXInn := 'CAST(0 AS SIGNED)'; //'0';
      SQL_GroupInn := 'AnoMesDiaIn';
      // Ger
      SQL_GGXDst := 'CAST(0 AS SIGNED)';
      SQL_GroupDst := 'AnoMesDiaIn';
    end;
    else
    begin
      SQL_GGXInn := '???';
      SQL_GroupInn := '? ? ?';
      SQL_GGXDst := '???';
      SQL_GroupDst := '? ? ?';
    end;
  end;*)

  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInn, DmodG.MyPID_DB, [
  'SELECT ' + SQL_GGXInn + ' GGXInn, AnoMesDiaIn, ',
  'DATE_FORMAT(DtEntrada, "%Y-%m-%d") Data,',
  'SUM(Pecas) InnPecas, ',
  'SUM(PesoKg) InnPesoKg, ',
  'SUM(SdoVrtPeca) InnSdoVrtPeca, ',
  'SUM(SdoVrtPeso) InnSdoVrtPeso, ',
  'SUM(PesoKg) / SUM(Pecas) InnKgPeca, ',
  //'SUM(InfPecas) NFePecas, SUM(Pecas) - SUM(SdoVrtPeca) CalPecas  ',
  'SUM(InfPecas) NFePecas ',
  'FROM ' + FVSInnIts,
  'GROUP BY ' + SQL_GroupInn,
  'ORDER BY ' + SQL_GroupInn,
  '']);
  //Geral.MB_Teste(QrVSInn.SQL.Text);
  SQL := 'INSERT INTO ' + FGerFromIn +
  //'(GGXInn, AnoMesDiaIn, Data, InnPecas, InnPesoKg, InnKgPeca, NFePecas, CalPecas) VALUES '
  '(GGXInn, AnoMesDiaIn, Data, InnPecas, InnPesoKg, InnKgPeca, NFePecas) VALUES '
  + sLineBreak;
  if QrVSInn.RecordCount > 0 then
  begin
    QrVSInn.First;
    //Geral.MB_Teste(IntToStr(QrVSInn.RecordCount));

    while not QrVSInn.Eof do
    begin
     SQL := SQL + Virgula + '(' +
       Geral.FF0(QrVSInnGGXInn.Value) + ', ' +
       //Geral.FF0(QrVSInnDstMovID.Value) + ', ' +
       Geral.FF0(QrVSInnAnoMesDiaIn.Value) + ', ' +
       '"' + QrVSInnData.Value + '", ' +
       Geral.FFT_Dot(QrVSInnInnPecas.Value, 3, siNegativo) + ', ' +
       Geral.FFT_Dot(QrVSInnInnPesoKg.Value, 3, siNegativo) + ', ' +
       Geral.FFT_Dot(QrVSInnInnKgPeca.Value, 10, siNegativo) + ', ' +
       //Geral.FFT_Dot(QrVSInnInnSdoVrtPeca.Value, 3, siNegativo) + ', ' +
       //Geral.FFT_Dot(QrVSInnInnSdoVrtPeso.Value, 3, siNegativo) + ', ' +
       Geral.FFT_Dot(QrVSInnNFePecas.Value, 3, siNegativo) (*+ ', ' +
       Geral.FFT_Dot(QrVSInnCalPecas.Value, 3, siNegativo)*) +
       ') ';
     Virgula := ',' + sLineBreak;
     //
     QrVSInn.Next;
    end;
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  end else
    Geral.MB_Aviso('N�o h� entrada de In Natura neste per�odo!');
   //
   //
  ///
  ///////////////////  E M   C O N S E R V A  //////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros conservados nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEmC, DmodG.MyPID_DB, [
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_emc_cab_e_its dst',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  //Geral.MB_Teste(QrVSEmC.SQL.Text);
  QrVSEmC.First;
  PB1.Position := 0;
  PB1.Max := QrVSEmC.RecordCount;
  while not QrVSEmC.Eof do
  begin
    AnoMesDiaIn := QrVSEmCAnoMesDiaIn.Value;
    //GGXInn := QrVSEmCGraGruX.Value;
    GGXInn := QrVSEmCGGXInn.Value;
    //abrir sql com dados Inn
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSEmCDstMovID.Value in ([6,39... quantos outros???]) then
    if QrVSEmCDstNivel1.Value > 0 then // >> tem Gera��o
    begin
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSEmCPecas.Value / QrDiaInnPecas.Value * 100;
    end;
*)
    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' EmCPecas' + sAGC + '=' + Geral.FFT_Dot(QrVSEmCPecas.Value, 3, siNegativo) + ', ' +
    ' EmCPesoKg' + sAGC + '=' +  Geral.FFT_Dot(QrVSEmCPesoKg.Value, 3, siNegativo) + //', ' +
    //' EmCPercCo' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    QrVSEmC.Next;
  end;
  //
   //
  ///
  ///////////////////  C A L E I R O  //////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros caleados nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCal, DmodG.MyPID_DB, [
(*
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX,  ',
  'CAST(dst.MovimID AS SIGNED) DstMovID,  ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty, ',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, ',
  'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2, ',
  'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, ',
  'SUM(inn.Pecas) InnPecas  ',
  'FROM _vs_in_cab_e_its inn  ',
  'LEFT JOIN _vs_ca_cab_e_its dst ON   ',
  '  dst.SrcMovID=inn.MovimID  ',
  '  AND dst.SrcNivel1=inn.Codigo  ',
  '  AND dst.SrcNivel2=inn.Controle  ',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ',
*)
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_ca_cab_e_its dst',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  //Geral.MB_Teste(QrVSCal.SQL.Text);
  QrVSCal.First;
  PB1.Position := 0;
  PB1.Max := QrVSCal.RecordCount;
  while not QrVSCal.Eof do
  begin
    AnoMesDiaIn := QrVSCalAnoMesDiaIn.Value;
    //GGXInn := QrVSCalGraGruX.Value;
    GGXInn := QrVSCalGGXInn.Value;
    //abrir sql com dados Inn
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSCalDstMovID.Value in ([6,39... quantos outros???]) then
    if QrVSCalDstNivel1.Value > 0 then // >> tem Cala��o
    begin
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSCalPecas.Value / QrDiaInnPecas.Value * 100;
    end;
    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' CalPecas' + sAGC + '=' + Geral.FFT_Dot(QrVSCalPecas.Value, 3, siNegativo) + ', ' +
    ' CalPesoKg' + sAGC + '=' +  Geral.FFT_Dot(QrVSCalPesoKg.Value, 3, siNegativo) + ', ' +
    ' CalPercCo' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    QrVSCal.Next;
  end;
  //

  //
  ///
  ///////////////////  C U R T I M E N T O  ////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros curtidos nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCur, DmodG.MyPID_DB, [
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_cu_cab_e_its dst',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  //Geral.MB_Teste(QrVSCur.SQL.Text);
  QrVSCur.First;
  PB1.Position := 0;
  PB1.Max := QrVSCur.RecordCount;
  while not QrVSCur.Eof do
  begin
    AnoMesDiaIn := QrVSCurAnoMesDiaIn.Value;
    //GGXInn := QrVSCurGraGruX.Value;
    GGXInn := QrVSCurGGXInn.Value;
    //abrir sql com dados Inn
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSCurDstMovID.Value in ([6,39... quantos outros???]) then
    if QrVSCurDstNivel1.Value > 0 then // >> tem Cura��o
    begin
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSCurPecas.Value / QrDiaInnPecas.Value * 100;
    end;
    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' CurPecas' + sAGC + '=' + Geral.FFT_Dot(QrVSCurPecas.Value, 3, siNegativo) + ', ' +
    ' CurPesoKg' + sAGC + '=' +  Geral.FFT_Dot(QrVSCurPesoKg.Value, 3, siNegativo) + ', ' +
    ' CurPercCo' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    QrVSCur.Next;
  end;
  //

  ///
  ///////////////////  G E R A � � O  //////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de artigos gerados nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGer, DmodG.MyPID_DB, [
(*
  'SELECT CAST(inn.GraGruX AS SIGNED) GGXInn,  ',
  SQL_GGXDst + ' GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'inn.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,',
  'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2,',
  'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,',
  'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2,',
  '',
  'SUM(inn.Pecas) InnPecas,  ',
  'SUM(inn.PesoKg) InnPesoKg,  ',
  'SUM(inn.PesoKg) / SUM(inn.Pecas) InnKgPeca,  ',
  'SUM(inn.InfPecas) NFePecas,  ',
  'SUM(inn.Pecas) - SUM(inn.SdoVrtPeca) CalPecas, ',
  'SUM(inn.SdoVrtPeca) ResVrtPeca, ',
  'SUM(inn.SdoVrtPeso) ResVrtPeso  ',
  'FROM ' + FVSInnIts + ' inn ',
  'LEFT JOIN ' + FVSGerArt + ' dst ON  ',
  '  dst.SrcMovID=inn.MovimID ',
  '  AND dst.SrcNivel1=inn.Codigo ',
  '  AND dst.SrcNivel2=inn.Controle ',
  'GROUP BY ' + SQL_GroupDst,
  'ORDER BY ' + SQL_GroupDst,
*)
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,',
  'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2,',
  'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,',
  'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2',
  'FROM _vs_ga_cab_e_its dst',
  'WHERE SrcMovID=27 /* n�o pode ser 1 */',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  //Geral.MB_Teste(QrVSGer.SQL.Text);
  QrVSGer.First;
  PB1.Position := 0;
  PB1.Max := QrVSGer.RecordCount;
  while not QrVSGer.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de artigos gerados nos dias do per�odo');
    ArtGeComodty := QrVSGerArtGeComodty.Value;
    sAGC := '0';
    if QrVSGerDstMovID.Value <> 6 then
    begin
      for I := 0 to Emids - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerDstMovID.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          Break;
        end;
      end;
    end else
    begin
      for I := Emids to Length(FArrAGC) - 1 do
      begin
        if FArrAGC[I][0] = QrVSGerArtGeComodty.Value then
        begin
          sAGC := Geral.FF0(I + 1);
          Break;
        end;
      end;
    end;
    //
    AnoMesDiaIn := QrVSGerAnoMesDiaIn.Value;
    //GGXInn := QrVSGerGraGruX.Value;
    GGXInn := QrVSGerGGXInn.Value;
    //abrir sql com dados Inn
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSGerDstMovID.Value in ([6,39... quantos outros???]) then
    if QrVSGerDstNivel1.Value > 0 then // >> tem Gera��o
    begin
      if QrDiaInnPecas.Value = 0 then
        PercCo := 0
      else
        PercCo := QrVSGerPecas.Value / QrDiaInnPecas.Value * 100;
      SQL :=
      'UPDATE ' + FGerFromIn + ' SET ' +
      ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerPecas.Value, 3, siNegativo) + ', ' +
      ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerPesoKg.Value, 3, siNegativo) + ', ' +
      ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerQtdGerArM2.Value, 2, siNegativo) + ', ' +
      ' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
      ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerRendKgM2.Value, 3, siNegativo) + ', ' +
      ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerMediaM2.Value, 3, siNegativo) +
      ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
      ' AND GGXInn=' + Geral.FF0(GGXInn) +
      ' ';
      //testar
      //Geral.MB_Teste(SQL);
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    end else
    begin
      //PercCo := 100;
      if QrVSGerDstMovID.Value <> 0 then
      begin
        SQL :=
        'UPDATE ' + FGerFromIn + ' SET ' +
        ' Pecas_' + sAGC + '=' + Geral.FFT_Dot(QrVSGerPecas.Value, 3, siNegativo) + ', ' +
        ' PesoKg_' + sAGC + '=' +  Geral.FFT_Dot(QrVSGerPesoKg.Value, 3, siNegativo) + ', ' +
        ' AreaM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000, 2, siNegativo) + ', ' +
        ' PercCo_' + sAGC + '=' + Geral.FFT_Dot(PercCo, 3, siNegativo) + ', ' +
        ' RendKgM2_' + sAGC + '=' + Geral.FFT_Dot(0.0000000000000, 3, siNegativo) + ', ' +
        ' MediaM2_' + sAGC + '=' + Geral.FFT_Dot(0.00000000000, 3, siNegativo) +
        ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
        ' AND GGXInn=' + Geral.FF0(GGXInn) +
        ' ';
        //testar
        //Geral.MB_Teste(SQL);
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
      end;
    end;
    QrVSGer.Next;
  end;


   //
  ///
  ///////////////////  C O N S E R V A D O S  //////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados de couros conservados nos dias do per�odo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMpC, DmodG.MyPID_DB, [
(*
  'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_mpc_cab_e_its dst',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
*)
  'SELECT cou.ArtGeComodty, CAST(dst.SrcGGX AS SIGNED) GGXInn,  ',
  'CAST(dst.DstGGX AS SIGNED) GraGruX, ',
  'CAST(dst.MovimID AS SIGNED) DstMovID, ',
  'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, ',
  'dst.AnoMesDiaIn, dst.ArtGeComodty,',
  'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg',
  'FROM _vs_mpc_cab_e_its dst',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=dst.GraGruX',
  'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty',
  '']);
  //
  while not QrVSMpC.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de peles conservadas nos dias do per�odo');
    ArtGeComodty := QrVSMpCArtGeComodty.Value;
    sAGC := '0';

    for I := Emids to Length(FArrAGC) - 1 do
    begin
      if FArrAGC[I][0] = QrVSMpCArtGeComodty.Value then
      begin
        sAGC := Geral.FF0(I + 1);
        Break;
      end;
    end;
    //
    AnoMesDiaIn := QrVSMpCAnoMesDiaIn.Value;
    //GGXInn := QrVSMpCGraGruX.Value;
    GGXInn := QrVSMpCGGXInn.Value;
    //abrir sql com dados Inn
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrDia, DmodG.MyPID_DB, [
    'SELECT InnPecas  ',
    'FROM ' + FGerFromIn, // FVSGerMpC,
    'WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn),
    'AND GGXInn=' + Geral.FF0(GGXInn),
    '']);
    //if QrVSMpCDstMovID.Value in ([6,39... quantos outros???]) then
    PercCo := QrVSMpCPecas.Value / QrDiaInnPecas.Value * 100;
*)


    Pecas  := - QrVSMpCPecas.Value;
    PesoKg := - QrVSMpCPesoKg.Value;

    SQL :=
    'UPDATE ' + FGerFromIn + ' SET ' +
    ' Pecas_'  + sAGC + ' = Pecas_'  + sAGC + ' + ' + Geral.FFT_Dot(Pecas, 3, siNegativo) + ', ' +
    ' PesoKg_' + sAGC + ' = PesoKg_' + sAGC + ' + ' + Geral.FFT_Dot(PesoKg, 3, siNegativo) + // ', ' +
    ' WHERE AnoMesDiaIn=' + Geral.FF0(AnoMesDiaIn) +
    ' AND GGXInn=' + Geral.FF0(GGXInn) +
    ' ';
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

    QrVSMpC.Next;
  end;
  //


///
////////////////////////////////////////////////////////////////////////////////
//////////  S O M A   D O S   N � O   P R O C E S S A D O S  ///////////////////
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Totalizando n�o processados');
    SQLpc := '';
    SQLkg := '';
    for I := 0 to Length(FArrSigla) - 1 do
    begin
      if I = 0 then
      begin
        SQLpc := SQLpc + 'pecas_' + Geral.FF0(I + 1);
        SQLkg := SQLkg + 'pesoKg_' + Geral.FF0(I + 1);
      end else
      begin
        SQLpc := SQLpc + ' + ' + 'pecas_' + Geral.FF0(I + 1) ;
        SQLkg := SQLkg + ' + ' + 'pesoKg_' + Geral.FF0(I + 1);
      end;
    end;
    if SQLpc <> '' then
    begin
      SQL := 'UPDATE ' + FGerFromIn +
      ' SET InnSdoVrtPeca = InnPecas - (' + SQLpc + '), ' +
      ' InnSdoVrtPeso = InnPesoKg - (' + SQLkg + ') ' +
      '';
      UnDmkDAC_PF.ExecutaDB(DmodG.MyPID_DB, SQL);
    end;
    //

  BtImprime.Enabled := True;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '...');

  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSGerArtDdImpCur.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtDdImpCur.CkCompraFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkCompraIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkEntradaFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkEntradaIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkTemIMEIMrtClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkViagemFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.CkViagemIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

function TFmVSGerArtDdImpCur.ConversaoCm(Redimensiona, HalfH: Boolean; Centimetros: Extended): Extended;
var
  Fator: Extended;
begin
  if Redimensiona then
    Fator := (RGFonte.ItemIndex + FFonteMinima) / 7
  else
    Fator := 1;
  if HalfH then
  begin
    Result := (((Centimetros - FEspecoTexto) * Fator) + FEspecoTexto) * FCmFrx;
  end else
    Result := Centimetros * Fator * FCmFrx;
end;

function TFmVSGerArtDdImpCur.Desconversao(Medidafxt: Extended): Extended;
begin
  Result := Medidafxt / FCmFrx;
end;

procedure TFmVSGerArtDdImpCur.DesfazPesquisa();
begin
  BtImprime.Enabled := False;
  BtPesquisa.Enabled := True;
end;

procedure TFmVSGerArtDdImpCur.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtDdImpCur.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPCompraIni.Date := Date - 30;
  TPCompraFim.Date := Date;
  //
  TPEntradaIni.Date := Date - 30;
  TPEntradaFim.Date := Date;
  //
  TPViagemIni.Date := Date - 30;
  TPViagemFim.Date := Date;
  //
end;

procedure TFmVSGerArtDdImpCur.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtDdImpCur.frxWET_CURTI_241_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PeriodoCompra' then
    Value := dmkPF.PeriodoImp1(TPCompraIni.Date, TPCompraFim.Date,
    CkCompraIni.Checked, CkCompraFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_PeriodoViagem' then
    Value := dmkPF.PeriodoImp1(TPViagemIni.Date, TPViagemFim.Date,
    CkViagemIni.Checked, CkViagemFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_PeriodoEntrada' then
    Value := dmkPF.PeriodoImp1(TPEntradaIni.Date, TPEntradaFim.Date,
    CkEntradaIni.Checked, CkEntradaFim.Checked, '', 'at�', '')
  else
end;

function TFmVSGerArtDdImpCur.GetFontHeightFromFontSize(
  FontSize: Integer): Integer;
begin
  case FontSize of
     6: Result := -8;
     7: Result := -9;
     8: Result := -11;
     9: Result := -12;
    10: Result := -13;
    else Result := -9;
  end;
end;

procedure TFmVSGerArtDdImpCur.MeGru(var Memo: TfrxMemoView; var Left: Extended;
  const Top, Width, Height: Extended; const HAlign: TfrxHAlign;
  const Texto: String; const FormatKind: frxClass.TfrxFormatKind;
  const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := 10;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-13;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := []; //[ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0; //0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtDdImpCur.MeHed(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String;
  FontStyle: (*Vcl.Graphics.TFont.*)TFontStyles; FontSize: Integer;
  FrameTyp: (*frxClass.TfrxFrame.*)TfrxFrameTypes; const Texto: String);
begin
  Memo := TfrxMemoView.Create(PageHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(False, False, Left);
  Memo.Top := ConversaoCm(False, False, Top);
  Memo.Width := ConversaoCm(False, False, Width);
  Memo.Height := ConversaoCm(False, False, Height);
  //Memo.DisplayFormat.DecimalSeparator := ',';
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  //Memo.Font.Height := -11;
  Memo.Font.Size := FontSize;
  Memo.Font.Name := 'Arial';
  Memo.Font.Style := FontStyle; //[fsBold];
  Memo.Frame.Typ := FrameTyp; //[];
  Memo.HAlign := haCenter;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtDdImpCur.MeSub(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
begin
  //trocar medidas por variu�veis! 1 cm = 37.7953
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtDdImpCur.MeSum(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(GroupFooter1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height * 2); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := True;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtDdImpCur.MeTit(var Memo: TfrxMemoView; var Left: Extended; const
  Top, Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String);
begin
  //trocar medidas por variu�veis! 1 cm = 37.7953
  //Memo := TfrxMemoView.Create(PageHeader1);
  Memo := TfrxMemoView.Create(GroupHeader1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
  //
  Left := Left + Width;
end;

procedure TFmVSGerArtDdImpCur.MeTot(var Memo: TfrxMemoView; const Left, Top, Width,
  Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(ReportSummary1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height * 2); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [fsBold];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := True;
  Memo.VAlign := vaCenter;
end;

procedure TFmVSGerArtDdImpCur.MeVal(var Memo: TfrxMemoView; var Left: Extended;
  const Top, Width, Height: Extended; const HAlign: TfrxHAlign; const Texto: String;
  const FormatKind: frxClass.TfrxFormatKind; const FormatStr: String);
begin
  Memo := TfrxMemoView.Create(MasterData1);
  Memo.AllowVectorExport := True;
  Memo.Left := ConversaoCm(True, False, Left);
  Memo.Top := ConversaoCm(False, False, Top); // 60.472480000000000000;
  Memo.Width := ConversaoCm(True, False, Width); // 136.062982360000000000;
  Memo.Height := ConversaoCm(True, True, Height); // 15.118110240000000000;
  Memo.DisplayFormat.FormatStr := FormatStr;
  Memo.DisplayFormat.Kind := FormatKind;
  Memo.Font.Charset := DEFAULT_CHARSET;
  Memo.Font.Color := clBlack;
  Memo.Font.Size := RGFonte.ItemIndex + FFonteMinima;
  //Memo.Font.Height := GetFontHeightFromFontSize(RGFonte.ItemIndex); //-9;
  Memo.Font.Name := 'Univers Light Condensed';
  Memo.Font.Style := [];
  Memo.Frame.Typ := [ftLeft, ftRight, ftTop, ftBottom];
  Memo.Frame.Width := 0.500000000000000000;
  Memo.HAlign := HAlign;
  Memo.Memo.Text := Texto;
  Memo.ParentFont := False;
  Memo.WordWrap := False;
  Memo.VAlign := vaCenter;
  //
  Left := Left + Width;
end;

procedure TFmVSGerArtDdImpCur.ReopenVSGerCal(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_in_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=1 ',
  'AND vmi.MovimNiv=29 ', // eminSorcCal
  'AND vmi.SrcNivel1 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSGerCal + ';',
  //'CREATE TABLE ' + FVSGerCal,
  'INSERT INTO ' + FVSGerCal,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtDdImpCur.ReopenVSGerCur(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'cal.AnoMesDiaIn DtEntrada_TXT, ',
  'cal.AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  //'LEFT JOIN _vs_in_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  'LEFT JOIN _vs_ca_cab_e_its cal ON cal.Controle=vmi.SrcNivel2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=26 ', // EmProcCal
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSGerCur + ';',
  //'CREATE TABLE ' + FVSGerCur,
  'INSERT INTO ' + FVSGerCur,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtDdImpCur.ReopenVSGerEmC(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_in_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=1 ',
  'AND vmi.MovimNiv=65 ', // eminSorcCon
  'AND vmi.SrcNivel1 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSGerEmC + ';',
  //'CREATE TABLE ' + FVSGerEmC,
  'INSERT INTO ' + FVSGerEmC,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtDdImpCur.ReopenVSGerMpC(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'DATE_FORMAT(_in.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(_in.DtEntrada)*10000 + MONTH(_in.DtEntrada)*100 + ',
  'DAY(_in.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_in_cab_e_its _in ON _in.Codigo=vmi.SrcNivel1 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID = 39 ',
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  // Parei Aqui
  // Colocar aqui somente couros da barraca direto para o gerado????
  //'AND vmi.MovimID=6',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSGerMpC + ';',
  //'CREATE TABLE ' + FVSGerMpC,
  'INSERT INTO ' + FVSGerMpC,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);

(*
   coloquei no gerart, mas n�o tem link com inn
   fazer insert na tabela final diretoi?
*)
  //Geral.MB_Teste(QrExec.SQL.Text);

end;

procedure TFmVSGerArtDdImpCur.ReopenVSGerArt(Corda: String);
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vmi.QtdGerArM2 / vmi.Pecas Mediam2Peca,  ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.QtdGerArM2 / vmi.Pecas, 2),  ',
  '  ",", ""), ".", ",")) Mediam2Peca_TXT,  ',
  'vmi.PesoKg/vmi.QtdGerArM2 RendKgm2, ',
  'IF(vmi.QtdGerArM2=0, "", REPLACE(REPLACE(FORMAT(  ',
  '  vmi.PesoKg/vmi.QtdGerArM2, 3),  ',
  '  ",", ""), ".", ",")) RendKgm2_TXT,  ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(  ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,  ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro,  ',
  'fch.Nome NO_SerieFch, ',
  //
  'cou.ArtGeComodty, agc.Ordem ArtGeComOrd, agc.Sigla ArtGeComSgl, ',
  // no fim
  'cur.AnoMesDiaIn DtEntrada_TXT, ',
  'cur.AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.gragruxcou cou ON cou.GraGruX=IF(vmi.DstGGX <>0, vmi.DstGGX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB +'.artgecomodty agc ON agc.Codigo=cou.ArtGeComodty ',
  'LEFT JOIN _vs_cu_cab_e_its cur ON cur.Controle=vmi.SrcNivel2 ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=27 ',
  'AND vmi.SrcNivel2 IN (' + Corda + ')',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSGerArt + ';',
  //'CREATE TABLE ' + FVSGerArt,
  'INSERT INTO ' + FVSGerArt,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtDdImpCur.ReopenVSInnIts();
  function   SQL_LJ_GGX_TMeuDB(): String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
  SQL_Compra, SQL_Viagem, SQL_Entrada: String;
begin
  //
  SQL_Compra := dmkPF.SQL_Periodo('AND cab.DtCompra ', // vmi.DataHora '
      TPCompraIni.Date, TPCompraFim.Date, CkCompraIni.Checked, CkCompraFim.Checked);
  SQL_Viagem := dmkPF.SQL_Periodo('AND cab.DtViagem ',
      TPViagemIni.Date, TPViagemFim.Date, CkViagemIni.Checked, CkViagemFim.Checked);
  SQL_Entrada := dmkPF.SQL_Periodo('AND cab.DtEntrada ',
      TPEntradaIni.Date, TPEntradaFim.Date, CkEntradaIni.Checked, CkEntradaFim.Checked);
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEIMrt.Checked);//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.QtdGerArM2 / vmi.Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.PesoKg/vmi.QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE( ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'fch.Nome NO_SerieFch, ',
  //
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, ',
  'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA, ',
  //
  'cab.DtCompra, cab.DtViagem, cab.DtEntrada, cab.Fornecedor, ',
  'cab.Transporta, cab.Pecas CabPecas, cab.PesoKg CabPesoKg, ',
  'cab.AreaM2 CabAreaM2, cab.AreaP2 CabAreaP2,  ',
  'cab.ide_serie, cab.ide_nNF, cab.emi_serie, cab.emi_nNF, ',
  'cab.ValorT CabValorT, cab.ValorMP CabValorMP, vmd.InfPecas, ',
  // No fim
  'DATE_FORMAT(cab.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, ',
  'YEAR(cab.DtEntrada)*10000 + MONTH(cab.DtEntrada)*100 + ',
  'DAY(cab.DtEntrada) AnoMesDiaIn ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX_TMeuDB(),
  'LEFT JOIN ' + TMeuDB +'.vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB +'.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB +'.vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB +'.vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN ' + TMeuDB +'.entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN ' + TMeuDB +'.entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN ' + TMeuDB +'.entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN ' + TMeuDB +'.entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN ' + TMeuDB +'.entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN ' + TMeuDB +'.entidades mot ON mot.Codigo=cab.Motorista ',
  'LEFT JOIN ' + TMeuDB +'.vsmovdif  vmd ON vmd.Controle=vmi.Controle ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=1 ',
  SQL_Compra,
  SQL_Viagem,
  SQL_Entrada,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, DmodG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVSInnIts + ';',
  //'CREATE TABLE ' + FVSInnIts,
  'INSERT INTO ' + FVSInnIts,
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_Teste(QrExec.SQL.Text);
end;

procedure TFmVSGerArtDdImpCur.RGPesquisaClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.SbMesAntClick(Sender: TObject);
begin
  MyObjects.IncMesTPIniEFim(TPEntradaIni, TPEntradaFim, -1);
end;

procedure TFmVSGerArtDdImpCur.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.IncMesTPIniEFim(TPEntradaIni, TPEntradaFim, +1);
end;

procedure TFmVSGerArtDdImpCur.TPCompraFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPCompraFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPCompraIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPCompraIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPEntradaFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPEntradaFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPEntradaIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPEntradaIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPViagemFimClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPViagemFimRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPViagemIniClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmVSGerArtDdImpCur.TPViagemIniRedefInPlace(Sender: TObject);
begin
  DesfazPesquisa();
end;

function TFmVSGerArtDdImpCur.VerificaOrfaos(): Integer;
const
  Aviso  = 'Os reduzidos abaixo devem ser configurados informando seu Artigo Commodity!';
  Titulo = 'Reduzidos sem Artigo Commodity';
  Prompt = 'Reduzidos sem Artigo Commodity';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT cou.GraGruX, ggx.GraGruY, gg1.Nome  ',
  'FROM gragruxcou cou ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cou.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE cou.ArtGeComodty = 0 ',
  'AND ggx.GraGruY in (1024,1088,2048) ',
  '']);
  Result := QrOrfaos.RecordCount;
  if Result > 0 then
  begin
    if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DsOrfaos, False, False,
    True, False, False ) then
    begin
      Grade_Jan.MostraFormGraGruY(QrOrfaosGragruY.Value, QrOrfaosGragruX.Value, '');
    end;
  end;
end;

//fazer IniMovID, IniNivel1, IniNivel2 e IniGGX? para processos?

end.
