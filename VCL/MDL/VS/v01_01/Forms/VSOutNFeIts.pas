unit VSOutNFeIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup,
  UnProjGroup_Consts, UnDmkProcFunc, UnAppEnums;

type
  TFmVSOutNFeIts = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdItemNFe: TdmkEdit;
    LaItemNFe: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    RGTpCalcVal: TdmkRadioGroup;
    EdValorU: TdmkEdit;
    Label4: TLabel;
    QrTribDefCab: TmySQLQuery;
    QrTribDefCabCodigo: TIntegerField;
    QrTribDefCabNome: TWideStringField;
    DsTribDefCab: TDataSource;
    LaTribDefSel: TLabel;
    EdTribDefSel: TdmkEditCB;
    CBTribDefSel: TdmkDBLookupComboBox;
    SbTribDefSel: TSpeedButton;
    BtSomas: TBitBtn;
    EdCodigo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTribDefSelClick(Sender: TObject);
    procedure CalculaTotal(Sender: TObject);
    procedure BtSomasClick(Sender: TObject);
  private
    { Private declarations }
    FBuscou: Boolean;
    FGraGruX: Integer;
    FPecas, FPesoKg, FAreaM2, FAreaP2: Double;
    //
    procedure ReopenVSOutNFeIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts, FQrTribIncIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FMovimCod: Integer;
    FDataHora: TDateTime;
    //
    FSinalOrig: TSinal;
    FDefMulNFe: TEstqDefMulFldEMxx;
    FMovTypeCod: Integer;
    FMovimID: TEstqMovimID;
    FMovimNivsOri, FMovimNivsDst: array of TEstqMovimNiv;
    //
    procedure BuscaDadosPallets();
  end;

  var
  FmVSOutNFeIts: TFmVSOutNFeIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF, UnTributos_PF(*, VSOutCab Nao pode!!!*), ModVS, ModVS_CRC;

{$R *.DFM}

procedure TFmVSOutNFeIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ItemNFe, GraGruX, TpCalcVal, TribDefSel: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, ValorU, ValorFat, BaseCalc: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  ItemNFe        := EdItemNFe.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  TpCalcVal      := RGTpCalcVal.ItemIndex;
  ValorU         := EdValorU.ValueVariant;
  TribDefSel     := EdTribDefSel.ValueVariant;
  //
  if MyObjects.FIC(ItemNFe = 0, EditemNFe, 'Informe o item da NFe!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o reduzido do artigo!') then
    Exit;
  if MyObjects.FIC(TpCalcVal = 0, RGTpCalcVal, 'Informe a unidade de faturamento!') then
    Exit;
  //
  if (AreaM2 <> 0) and (PesoKg <> 0) then
  begin
    if Geral.MB_Pergunta('Foi informado peso e �rea ao mesmo tempo!' +
    sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('vsoutnfeits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsoutnfeits', False, [
  'Codigo', 'ItemNFe', 'GraGruX',
  'Pecas', 'PesoKg', 'AreaM2',
  'AreaP2', 'ValorT', 'TpCalcVal',
  'ValorU', 'TribDefSel'], [
  'Controle'], [
  Codigo, ItemNFe, GraGruX,
  Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, TpCalcVal,
  ValorU, TribDefSel], [
  Controle], True) then
  begin
    //FmVSOutCab.AtualizaNFeItens(FmVSOutCab.QrVSOutCabMovimCod.Value);
    DmModVS_CRC.AtualizaVSMulNFeCab(FSinalOrig, FDefMulNFe, FMovTypeCod, FMovimID,
      FMovimNivsOri, FMovimNivsDst);
    //
    ReopenVSOutNFeIts(Controle);
    //
    if (ImgTipo.SQLType = stIns) or (FQrTribIncIts.RecordCount = 0) then
    begin
      Tributos_PF.Tributos_Insere(VAR_FATID_1009, Codigo, ItemNFe, FEmpresa,
      TribDefSel, ValorT, ValorT, FDataHora, siNegativo);
    end else
    begin
      ValorFat := ValorT;
      BaseCalc := ValorT;
      Tributos_PF.Tributos_GerenciaAlt(FQrTribIncIts, ValorFat, BaseCalc);
    end;
    //
    ReopenVSOutNFeIts(Controle);
    //
    Close;
  end;
end;

procedure TFmVSOutNFeIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutNFeIts.BtSomasClick(Sender: TObject);
begin
  BuscaDadosPallets();
  //
  EdValorU.SetFocus;
end;

procedure TFmVSOutNFeIts.BuscaDadosPallets();
begin
  VS_PF.BuscaDadosItemNF(EdItemNFe, EdGraGruX, EdPecas, EdPesoKg, EdAreaM2,
  EdAreaP2, CBGraGruX, FMovimCod, FBuscou, FGraGruX, FPecas, FPesoKg, FAreaM2,
  FAreaP2);
{
var
  Qry: TmySQLQuery;
  ItemNFe: Integer;
  Item: String;
begin
  ItemNFe        := EdItemNFe.ValueVariant;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    ' SELECT vmi.ItemNFe, vmi.GraGruX,  ',
    '-SUM(vmi.Pecas) Pecas, -SUM(vmi.PesoKg) PesoKg,  ',
    '-SUM(vmi.AreaM2) AreaM2, -SUM(vmi.AreaP2) AreaP2   ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'WHERE vmi.MovimCod=' + Geral.FF0(FMovimCod),
    'GROUP BY vmi.ItemNFe  ',
    'ORDER BY vmi.ItemNFe ',
    '']);
    if Qry.RecordCount > 1 then
    begin
      Item := '1';
      if InputQuery('Item de NFe', 'Informe o item:', Item) then
        if Qry.Locate('ItemNFe', Item, []) then
          EdItemNFe.Text := Item;
    end else
      EdItemNFe.ValueVariant := 1;
    //
    FBuscou  := True;
    FGraGruX := Qry.FieldByName('GraGruX').AsInteger;
    FPecas   := dmkPF.PositFlu(Qry.FieldByName('Pecas').AsFloat);
    FPesoKg  := dmkPF.PositFlu(Qry.FieldByName('PesoKg').AsFloat);
    FAreaM2  := dmkPF.PositFlu(Qry.FieldByName('AreaM2').AsFloat);
    FAreaP2  := dmkPF.PositFlu(Qry.FieldByName('AreaP2').AsFloat);
    //
    //if (Qry.RecordCount = 1) and (FGraGruX <> 0) then
    begin
      EdGraGruX.ValueVariant := FGraGruX;
      CBGraGruX.KeyValue     := FGraGruX;
    end;
    //
    EdPecas.ValueVariant      := FPecas;
    EdPesoKg.ValueVariant     := FPesoKg;
    EdAreaP2.ValueVariant     := FAreaP2; // Fazer antes para evitar erro na area m2.
    EdAreaM2.ValueVariant     := FAreaM2;
  finally
    Qry.Free;
  end;
}
end;

procedure TFmVSOutNFeIts.CalculaTotal(Sender: TObject);
var
  Qtde, ValorU, ValorT: Double;
begin
  ValorU := EdValorU.ValueVariant;
  ValorT := 0;
  case TTipoCalcCouro(RGTpCalcVal.ItemIndex) of
    ptcNaoInfo(*0*): Qtde := 0;
    ptcPecas(*1*)  : Qtde := EdPecas.ValueVariant;
    ptcPesoKg(*2*) : Qtde := EdPesoKg.ValueVariant;
    ptcAreaM2(*3*) : Qtde := EdAreaM2.ValueVariant;
    ptcAreaP2(*4*) : Qtde := EdAreaP2.ValueVariant;
    ptcTotal(*5*)  : Qtde := 1;
    else
    begin
      Geral.MB_Erro('Unidade de faturamento indefida em "CalculaTotal()"');
      Qtde := 0;
    end;
  end;
  if TTipoCalcCouro(RGTpCalcVal.ItemIndex) <> ptcTotal then
  begin
    ValorT := ValorU * Qtde;
    EdValorT.ValueVariant := ValorT;
  end;
end;

procedure TFmVSOutNFeIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutNFeIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FBuscou := False;
  //
  VS_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.Controle > 0');
  //  'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));
  UnDmkDAC_PF.AbreQuery(QrTribDefCab, Dmod.MyDB);
end;

procedure TFmVSOutNFeIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutNFeIts.ReopenVSOutNFeIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutNFeIts.SbTribDefSelClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Tributos_PF.MostraFormTribDefCab(EdTribDefSel.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdTribDefSel, CBTribDefSel, QrTribDefCab, VAR_CADASTRO);
end;

end.
