object FmVSClaArtPrp: TFmVSClaArtPrp
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-011 :: Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribe' +
    'ira'
  ClientHeight = 633
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 613
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 613
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 613
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 471
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 490
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 471
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 490
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 471
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 490
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 454
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 473
          object PnPartida: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 81
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 729
              Height = 81
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object LaVSRibCad: TLabel
                Left = 48
                Top = 40
                Width = 28
                Height = 13
                Caption = 'IME-I:'
              end
              object SbIMEI: TSpeedButton
                Left = 704
                Top = 56
                Width = 21
                Height = 21
                Caption = '>'
                Enabled = False
                OnClick = SbIMEIClick
              end
              object Label33: TLabel
                Left = 48
                Top = 0
                Width = 83
                Height = 13
                Caption = 'C'#243'digo de barras:'
              end
              object Label34: TLabel
                Left = 644
                Top = 0
                Width = 46
                Height = 13
                Caption = 'DV IME-I:'
              end
              object EdIMEI: TdmkEditCB
                Left = 48
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdIMEIChange
                OnRedefinido = EdIMEIRedefinido
                DBLookupComboBox = CBIMEI
                IgnoraDBLookupComboBox = False
              end
              object CBIMEI: TdmkDBLookupComboBox
                Left = 104
                Top = 56
                Width = 597
                Height = 21
                KeyField = 'Controle'
                ListField = 'IMEI_NO_PRD_TAM_COR_FICHA'
                ListSource = DsVSGerArtNew
                TabOrder = 3
                dmkEditCB = EdIMEI
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDVIMEI: TdmkEdit
                Left = 644
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object MeLeitura: TMemo
                Left = 48
                Top = 16
                Width = 593
                Height = 21
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnChange = MeLeituraChange
              end
            end
            object Panel17: TPanel
              Left = 729
              Top = 0
              Width = 275
              Height = 81
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Label30: TLabel
                Left = 20
                Top = 0
                Width = 47
                Height = 13
                Caption = 'ID Config:'
                Enabled = False
              end
              object Label31: TLabel
                Left = 104
                Top = 0
                Width = 32
                Height = 13
                Caption = 'IME-C:'
                Enabled = False
              end
              object Label32: TLabel
                Left = 188
                Top = 0
                Width = 18
                Height = 13
                Caption = 'OC:'
                Enabled = False
              end
              object EdCodigo: TdmkEdit
                Left = 20
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdMovimCod: TdmkEdit
                Left = 104
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCacCod: TdmkEdit
                Left = 188
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object PnPallets: TPanel
            Left = 0
            Top = 81
            Width = 1004
            Height = 373
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            ExplicitLeft = 140
            ExplicitTop = 109
            ExplicitHeight = 457
            object GBTecla: TGroupBox
              Left = 0
              Top = 0
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "1": '
              TabOrder = 0
              object PnTecla: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel6: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label1: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object LaVSRibCla: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SbPallet1: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SbPallet1Click
                  end
                  object EdPallet1: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet1Change
                    DBLookupComboBox = CBPallet1
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet1: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet1
                    TabOrder = 1
                    dmkEditCB = EdPallet1
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla1: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label3: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit2
                  end
                  object Label4: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit3
                  end
                  object Label2: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit1
                  end
                  object DBEdit2: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet1
                    TabOrder = 0
                  end
                  object DBEdit3: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet1
                    TabOrder = 1
                  end
                  object DBEdit1: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet1
                    TabOrder = 2
                  end
                end
                object Panel9: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '1'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 60
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "2": '
              TabOrder = 1
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel8: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label5: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object Label6: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SBPallet2: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SBPallet2Click
                  end
                  object EdPallet2: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet2Change
                    DBLookupComboBox = CBPallet2
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet2: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet2
                    TabOrder = 1
                    dmkEditCB = EdPallet2
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla2: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label7: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit4
                  end
                  object Label8: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit5
                  end
                  object Label9: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit6
                  end
                  object DBEdit4: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet2
                    TabOrder = 0
                  end
                  object DBEdit5: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet2
                    TabOrder = 1
                  end
                  object DBEdit6: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet2
                    TabOrder = 2
                  end
                end
                object Panel10: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '2'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
            object GroupBox3: TGroupBox
              Left = 0
              Top = 120
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "3": '
              TabOrder = 2
              object Panel11: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel12: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label10: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object Label11: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SBPallet3: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SBPallet3Click
                  end
                  object EdPallet3: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet3Change
                    DBLookupComboBox = CBPallet3
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet3: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet3
                    TabOrder = 1
                    dmkEditCB = EdPallet3
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla3: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label12: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit7
                  end
                  object Label13: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit8
                  end
                  object Label14: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit9
                  end
                  object DBEdit7: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet3
                    TabOrder = 0
                  end
                  object DBEdit8: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet3
                    TabOrder = 1
                  end
                  object DBEdit9: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet3
                    TabOrder = 2
                  end
                end
                object Panel14: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '3'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 180
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "4": '
              TabOrder = 3
              object Panel15: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel16: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label15: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object Label16: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SBPallet4: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SBPallet4Click
                  end
                  object EdPallet4: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet4Change
                    DBLookupComboBox = CBPallet4
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet4: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet4
                    TabOrder = 1
                    dmkEditCB = EdPallet4
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla4: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label17: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit10
                  end
                  object Label18: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit11
                  end
                  object Label19: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit12
                  end
                  object DBEdit10: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet4
                    TabOrder = 0
                  end
                  object DBEdit11: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet4
                    TabOrder = 1
                  end
                  object DBEdit12: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet4
                    TabOrder = 2
                  end
                end
                object Panel18: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '4'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 240
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "5": '
              TabOrder = 4
              object Panel19: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel20: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label20: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object Label21: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SBPallet5: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SBPallet5Click
                  end
                  object EdPallet5: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet5Change
                    DBLookupComboBox = CBPallet5
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet5: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet5
                    TabOrder = 1
                    dmkEditCB = EdPallet5
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla5: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label22: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit13
                  end
                  object Label23: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit14
                  end
                  object Label24: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit15
                  end
                  object DBEdit13: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet5
                    TabOrder = 0
                  end
                  object DBEdit14: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet5
                    TabOrder = 1
                  end
                  object DBEdit15: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet5
                    TabOrder = 2
                  end
                end
                object Panel22: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '5'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 300
              Width = 1004
              Height = 60
              Align = alTop
              Caption = ' Configura'#231#227'o da tecla "6": '
              TabOrder = 5
              object Panel23: TPanel
                Left = 2
                Top = 15
                Width = 1000
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel24: TPanel
                  Left = 43
                  Top = 0
                  Width = 480
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'Panel6'
                  TabOrder = 0
                  object Label25: TLabel
                    Left = 4
                    Top = 0
                    Width = 29
                    Height = 13
                    Caption = 'Pallet:'
                  end
                  object Label26: TLabel
                    Left = 60
                    Top = 0
                    Width = 186
                    Height = 13
                    Caption = 'Nome do Artigo de Ribeira Classificado:'
                  end
                  object SBPallet6: TSpeedButton
                    Left = 456
                    Top = 16
                    Width = 23
                    Height = 22
                    Caption = '...'
                    OnClick = SBPallet6Click
                  end
                  object EdPallet6: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdPallet6Change
                    DBLookupComboBox = CBPallet6
                    IgnoraDBLookupComboBox = False
                  end
                  object CBPallet6: TdmkDBLookupComboBox
                    Left = 59
                    Top = 16
                    Width = 396
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_PRD_TAM_COR'
                    ListSource = DsVSPallet6
                    TabOrder = 1
                    dmkEditCB = EdPallet6
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object PnTecla6: TPanel
                  Left = 523
                  Top = 0
                  Width = 477
                  Height = 43
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label27: TLabel
                    Left = 4
                    Top = 0
                    Width = 33
                    Height = 13
                    Caption = 'Status:'
                    FocusControl = DBEdit16
                  end
                  object Label28: TLabel
                    Left = 108
                    Top = 0
                    Width = 93
                    Height = 13
                    Caption = 'Cliente preferencial:'
                    FocusControl = DBEdit17
                  end
                  object Label29: TLabel
                    Left = 292
                    Top = 0
                    Width = 66
                    Height = 13
                    Caption = 'Observa'#231#245'es:'
                    FocusControl = DBEdit18
                  end
                  object DBEdit16: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'NO_STATUS'
                    DataSource = DsVSPallet6
                    TabOrder = 0
                  end
                  object DBEdit17: TDBEdit
                    Left = 108
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'NO_CLISTAT'
                    DataSource = DsVSPallet6
                    TabOrder = 1
                  end
                  object DBEdit18: TDBEdit
                    Left = 292
                    Top = 16
                    Width = 180
                    Height = 21
                    TabStop = False
                    DataField = 'Nome'
                    DataSource = DsVSPallet6
                    TabOrder = 2
                  end
                end
                object Panel26: TPanel
                  Left = 0
                  Top = 0
                  Width = 43
                  Height = 43
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = '6'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 10485760
                  Font.Height = -37
                  Font.Name = 'Arial Black'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 519
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 538
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 563
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 582
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReclasif: TBitBtn
        Tag = 421
        Left = 400
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Gera artigos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtReclasifClick
      end
    end
  end
  object QrVSPallet1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 724
    Top = 217
    object QrVSPallet1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet1NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet1Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet1CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet1: TDataSource
    DataSet = QrVSPallet1
    Left = 724
    Top = 261
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 724
    Top = 117
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField
      FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
      Size = 255
    end
    object QrVSGerArtNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 724
    Top = 165
  end
  object QrVSPallet2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 788
    Top = 217
    object QrVSPallet2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet2NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet2Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet2CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet2NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet2NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet2: TDataSource
    DataSet = QrVSPallet2
    Left = 788
    Top = 261
  end
  object QrVSPallet3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 852
    Top = 217
    object QrVSPallet3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet3Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet3UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet3UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet3AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet3NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet3Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet3CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet3NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet3NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet3NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet3: TDataSource
    DataSet = QrVSPallet3
    Left = 852
    Top = 261
  end
  object QrVSPallet4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 724
    Top = 313
    object QrVSPallet4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet4Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet4DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet4DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet4UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet4UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet4AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet4Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet4NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet4Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet4CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet4: TDataSource
    DataSet = QrVSPallet4
    Left = 724
    Top = 357
  end
  object QrVSPallet5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 788
    Top = 313
    object QrVSPallet5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet5Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet5DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet5DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet5UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet5UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet5AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet5Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet5Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet5NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet5Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet5CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet5GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet5NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet5NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet5NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet5: TDataSource
    DataSet = QrVSPallet5
    Left = 788
    Top = 357
  end
  object QrVSPallet6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 852
    Top = 313
    object QrVSPallet6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet6Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet6Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet6DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet6DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet6UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet6UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet6AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet6Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet6Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet6NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet6Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet6CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet6GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet6NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet6NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet6NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet6: TDataSource
    DataSet = QrVSPallet6
    Left = 852
    Top = 357
  end
end
