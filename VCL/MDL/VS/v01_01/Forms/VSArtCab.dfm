object FmVSArtCab: TFmVSArtCab
  Left = 368
  Top = 194
  Caption = 'WET-RECUR-008 :: Configura'#231#227'o de Artigo Semi / Acabado'
  ClientHeight = 725
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 629
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 565
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 312
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 486
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DBMemo1: TDBMemo
      Left = 0
      Top = 353
      Width = 1008
      Height = 60
      Align = alTop
      DataField = 'Observa'
      DataSource = DsVSArtCab
      TabOrder = 1
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 353
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 589
        Height = 353
        Align = alLeft
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label12: TLabel
          Left = 16
          Top = 136
          Width = 116
          Height = 13
          Caption = 'Receita de recurtimento:'
        end
        object Label13: TLabel
          Left = 16
          Top = 176
          Width = 189
          Height = 13
          Caption = 'Receita de recurtimento - segunda fase:'
        end
        object Label14: TLabel
          Left = 16
          Top = 216
          Width = 117
          Height = 13
          Caption = 'Receita de acabamento:'
        end
        object Label15: TLabel
          Left = 16
          Top = 256
          Width = 165
          Height = 13
          Caption = 'Classes de mat'#233'ria-prima utilizadas:'
        end
        object Label16: TLabel
          Left = 16
          Top = 336
          Width = 66
          Height = 13
          Caption = 'Observa'#231#245'es:'
        end
        object Label17: TLabel
          Left = 16
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = dmkDBEdit1
        end
        object Label18: TLabel
          Left = 76
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = dmkDBEdit2
        end
        object Label1: TLabel
          Left = 452
          Top = 336
          Width = 123
          Height = 13
          Caption = 'Reduzidos atrelados >>>>'
        end
        object Label11: TLabel
          Left = 16
          Top = 56
          Width = 159
          Height = 13
          Caption = 'Fluxo acr'#244'nico de produ'#231#227'o (OS):'
        end
        object Label19: TLabel
          Left = 16
          Top = 96
          Width = 182
          Height = 13
          Caption = 'Fluxo consecutivo de produ'#231#227'o (PCP):'
        end
        object DBEdit1: TDBEdit
          Left = 16
          Top = 72
          Width = 56
          Height = 21
          DataField = 'FluxProCab'
          DataSource = DsVSArtCab
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 16
          Top = 152
          Width = 56
          Height = 21
          DataField = 'ReceiRecu'
          DataSource = DsVSArtCab
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 16
          Top = 192
          Width = 56
          Height = 21
          DataField = 'ReceiRefu'
          DataSource = DsVSArtCab
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 16
          Top = 232
          Width = 56
          Height = 21
          DataField = 'ReceiAcab'
          DataSource = DsVSArtCab
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 16
          Top = 272
          Width = 569
          Height = 21
          DataField = 'TxtMPs'
          DataSource = DsVSArtCab
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 72
          Top = 72
          Width = 513
          Height = 21
          DataField = 'NO_FluxProCab'
          DataSource = DsVSArtCab
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 72
          Top = 152
          Width = 513
          Height = 21
          DataField = 'NO_ReceiRecu'
          DataSource = DsVSArtCab
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 72
          Top = 192
          Width = 513
          Height = 21
          DataField = 'NO_ReceiRefu'
          DataSource = DsVSArtCab
          TabOrder = 7
        end
        object DBEdit9: TDBEdit
          Left = 72
          Top = 232
          Width = 513
          Height = 21
          DataField = 'NO_ReceiAcab'
          DataSource = DsVSArtCab
          TabOrder = 8
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSArtCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 9
          UpdType = utYes
          Alignment = taRightJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 76
          Top = 32
          Width = 509
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsVSArtCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit10: TDBEdit
          Left = 16
          Top = 112
          Width = 56
          Height = 21
          DataField = 'FluxPcpCab'
          DataSource = DsVSArtCab
          TabOrder = 11
        end
        object DBEdit11: TDBEdit
          Left = 72
          Top = 112
          Width = 513
          Height = 21
          DataField = 'NO_FluxPcpCab'
          DataSource = DsVSArtCab
          TabOrder = 12
        end
      end
      object DBGVSArtGGX: TdmkDBGridZTO
        Left = 589
        Top = 0
        Width = 419
        Height = 353
        Align = alClient
        DataSource = DsVSArtGGX
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Descri'#231#227'o'
            Width = 308
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 629
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 566
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 869
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 357
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 16
        Top = 56
        Width = 159
        Height = 13
        Caption = 'Fluxo acr'#244'nico de produ'#231#227'o (OS):'
      end
      object Label3: TLabel
        Left = 16
        Top = 136
        Width = 116
        Height = 13
        Caption = 'Receita de recurtimento:'
      end
      object Label4: TLabel
        Left = 16
        Top = 176
        Width = 189
        Height = 13
        Caption = 'Receita de recurtimento - segunda fase:'
      end
      object Label5: TLabel
        Left = 16
        Top = 216
        Width = 117
        Height = 13
        Caption = 'Receita de acabamento:'
      end
      object Label6: TLabel
        Left = 16
        Top = 256
        Width = 165
        Height = 13
        Caption = 'Classes de mat'#233'ria-prima utilizadas:'
      end
      object Label10: TLabel
        Left = 16
        Top = 340
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object Label2: TLabel
        Left = 16
        Top = 96
        Width = 182
        Height = 13
        Caption = 'Fluxo consecutivo de produ'#231#227'o (PCP):'
      end
      object SbFluxProCab: TSpeedButton
        Left = 748
        Top = 72
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbFluxProCabClick
      end
      object SbFluxPcpCab: TSpeedButton
        Left = 748
        Top = 112
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbFluxPcpCabClick
      end
      object Label20: TLabel
        Left = 16
        Top = 300
        Width = 104
        Height = 13
        Caption = 'Espessura de rebaixe:'
      end
      object Label21: TLabel
        Left = 64
        Top = 320
        Width = 5
        Height = 13
        Caption = '/'
      end
      object Label22: TLabel
        Left = 132
        Top = 300
        Width = 91
        Height = 13
        Caption = 'Espessura do semi:'
      end
      object Label23: TLabel
        Left = 180
        Top = 320
        Width = 5
        Height = 13
        Caption = '/'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFluxProCab: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FluxProCab'
        UpdCampo = 'FluxProCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFluxProCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFluxProCab: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 673
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFluxProCab
        TabOrder = 3
        dmkEditCB = EdFluxProCab
        QryCampo = 'FluxProCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceiRecu: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FluxPcpCab'
        UpdCampo = 'FluxPcpCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRecu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiRecu: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRecu
        TabOrder = 5
        dmkEditCB = EdReceiRecu
        QryCampo = 'ReceiRecu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceiRefu: TdmkEditCB
        Left = 16
        Top = 192
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiRefu'
        UpdCampo = 'ReceiRefu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRefu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiRefu: TdmkDBLookupComboBox
        Left = 72
        Top = 192
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRefu
        TabOrder = 7
        dmkEditCB = EdReceiRefu
        QryCampo = 'ReceiRefu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceiAcab: TdmkEditCB
        Left = 16
        Top = 232
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiAcab'
        UpdCampo = 'ReceiAcab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiAcab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBReceiAcab: TdmkDBLookupComboBox
        Left = 72
        Top = 232
        Width = 697
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiAcab
        TabOrder = 9
        dmkEditCB = EdReceiAcab
        QryCampo = 'ReceiAcab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTxtMPs: TdmkEdit
        Left = 16
        Top = 272
        Width = 753
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'TxtMPs'
        UpdCampo = 'TxtMPs'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFluxPcpCab: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FluxPcpCab'
        UpdCampo = 'FluxPcpCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFluxPcpCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFluxPcpCab: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 673
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFluxPcpCab
        TabOrder = 12
        dmkEditCB = EdFluxPcpCab
        QryCampo = 'FluxPcpCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdLinCulReb: TdmkEdit
        Left = 16
        Top = 316
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCulReb'
        UpdCampo = 'LinCulReb'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCabReb: TdmkEdit
        Left = 72
        Top = 316
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCabReb'
        UpdCampo = 'LinCabReb'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCulSem: TdmkEdit
        Left = 132
        Top = 316
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCulSem'
        UpdCampo = 'LinCulSem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCabSem: TdmkEdit
        Left = 188
        Top = 316
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCabSem'
        UpdCampo = 'LinCabSem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object MeObserva: TdmkMemo
      Left = 0
      Top = 357
      Width = 1008
      Height = 72
      Align = alTop
      TabOrder = 2
      QryCampo = 'Observa'
      UpdCampo = 'Observa'
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Configura'#231#227'o de Artigo Semi / Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrVSArtCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSArtCabBeforeOpen
    AfterOpen = QrVSArtCabAfterOpen
    BeforeClose = QrVSArtCabBeforeClose
    AfterScroll = QrVSArtCabAfterScroll
    SQL.Strings = (
      'SELECT fo1.Nome NO_ReceiRecu, '
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab, '
      'fro.Nome NO_FluxProCab, fpc.Nome NO_FluxPcpCab, vac.* '
      'FROM vsartcab vac '
      'LEFT JOIN fluxos       fro ON fro.Codigo=vac.FluxProCab '
      'LEFT JOIN fluxpcpcab   fpc ON fpc.Codigo=vac.FluxPcpCab '
      'LEFT JOIN formulas fo1 ON fo1.Numero=vac.ReceiRecu '
      'LEFT JOIN formulas fo2 ON fo2.Numero=vac.ReceiRefu '
      'LEFT JOIN formulas ti1 ON ti1.Numero=vac.ReceiAcab ')
    Left = 92
    Top = 20
    object QrVSArtCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSArtCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSArtCabNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrVSArtCabNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrVSArtCabNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 30
    end
    object QrVSArtCabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrVSArtCabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrVSArtCabReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrVSArtCabTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrVSArtCabObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
    end
    object QrVSArtCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSArtCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSArtCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSArtCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSArtCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSArtCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSArtCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSArtCabFluxProCab: TIntegerField
      FieldName = 'FluxProCab'
    end
    object QrVSArtCabFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
    end
    object QrVSArtCabNO_FluxProCab: TWideStringField
      FieldName = 'NO_FluxProCab'
      Size = 100
    end
    object QrVSArtCabNO_FluxPcpCab: TWideStringField
      FieldName = 'NO_FluxPcpCab'
      Size = 100
    end
    object QrVSArtCabLinCulReb: TIntegerField
      FieldName = 'LinCulReb'
      Required = True
    end
    object QrVSArtCabLinCabReb: TIntegerField
      FieldName = 'LinCabReb'
      Required = True
    end
    object QrVSArtCabLinCulSem: TIntegerField
      FieldName = 'LinCulSem'
      Required = True
    end
    object QrVSArtCabLinCabSem: TIntegerField
      FieldName = 'LinCabSem'
      Required = True
    end
    object QrVSArtCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSArtCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
  end
  object DsVSArtCab: TDataSource
    DataSet = QrVSArtCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtIts
    CanDel01 = BtCab
    Left = 168
    Top = 20
  end
  object QrFluxProCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxos'
      'ORDER BY Nome')
    Left = 260
    Top = 48
    object QrFluxProCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxProCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFluxProCab: TDataSource
    DataSet = QrFluxProCab
    Left = 260
    Top = 96
  end
  object QrReceiRecu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 397
    Top = 50
    object QrReceiRecuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceiRecuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRecu: TDataSource
    DataSet = QrReceiRecu
    Left = 397
    Top = 98
  end
  object QrReceirefu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 461
    Top = 50
    object QrReceirefuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceirefuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRefu: TDataSource
    DataSet = QrReceirefu
    Left = 461
    Top = 98
  end
  object QrReceiAcab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome'
      'FROM tintascab'
      'ORDER BY Nome'
      ''
      '')
    Left = 528
    Top = 48
    object QrReceiAcabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrReceiAcabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsReceiAcab: TDataSource
    DataSet = QrReceiAcab
    Left = 528
    Top = 96
  end
  object QrVSArtGGX: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSArtCabBeforeOpen
    AfterOpen = QrVSArtCabAfterOpen
    SQL.Strings = (
      'SELECT vag.*, ggx.GraGru1, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, '
      'IF(xco.CouNiv2 IS NULL, -1.000, xco.CouNiv2+0.000) CouNiv2, '
      'IF(unm.Grandeza IS NULL, -1.000, unm.Grandeza+0.000) Grandeza, '
      'ggx.GraGruY '
      'FROM vsartggx vag'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vag.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ')
    Left = 192
    Top = 48
    object QrVSArtGGXCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSArtGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSArtGGXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSArtGGXLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSArtGGXDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSArtGGXDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSArtGGXUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSArtGGXUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSArtGGXAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSArtGGXAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSArtGGXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSArtGGXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSArtGGXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSArtGGXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrVSArtGGXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSArtGGXCouNiv2: TFloatField
      FieldName = 'CouNiv2'
    end
    object QrVSArtGGXGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
    object QrVSArtGGXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSArtGGXAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSArtGGXAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
  end
  object DsVSArtGGX: TDataSource
    DataSet = QrVSArtGGX
    Left = 192
    Top = 92
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 660
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object QrFluxPcpCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxpcpcab'
      'ORDER BY Nome')
    Left = 324
    Top = 48
    object QrFluxPcpCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxPcpCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFluxPcpCab: TDataSource
    DataSet = QrFluxPcpCab
    Left = 324
    Top = 96
  end
end
