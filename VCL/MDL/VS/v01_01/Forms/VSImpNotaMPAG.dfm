object FmVSImpNotaMPAG: TFmVSImpNotaMPAG
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-121 :: Impress'#227'o de Nota MPAG'
  ClientHeight = 228
  ClientWidth = 563
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxWET_CURTI_018_14_A: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure FT_02OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MeNotaMPAG_2.Memo.Text := FormatFloat('#39'#,###,##0.00'#39','
      '    MyFunc('
      '      SUM(<frxDs14VSMovIts."Inteiros">,MD002,1),      '
      '      SUM(<frxDs14VSMovIts."PesoKg">,MD002,1),      '
      
        '      SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1),              ' +
        '  '
      '      SUM(<frxDs14VSMovIts."SumVRC">,MD002,1), '
      '      SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)'
      '    )'
      '  );'
      'end;'
      ''
      'procedure FT_01OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MeNotaMPAG_1.Memo.Text := FormatFloat('#39'#,###,##0.00'#39','
      '    MyFunc('
      '      SUM(<frxDs14VSMovIts."Inteiros">,MD002,1),      '
      '      SUM(<frxDs14VSMovIts."PesoKg">,MD002,1),      '
      
        '      SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1),              ' +
        '  '
      '      SUM(<frxDs14VSMovIts."SumVRC">,MD002,1), '
      '      SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)'
      '    )'
      '  );'
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MeNotaMPAG_T.Memo.Text := FormatFloat('#39'#,###,##0.00'#39','
      '    MyFunc('
      '      SUM(<frxDs14VSMovIts."Inteiros">,MD002,1),      '
      '      SUM(<frxDs14VSMovIts."PesoKg">,MD002,1),      '
      
        '      SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1),              ' +
        '  '
      '      SUM(<frxDs14VSMovIts."SumVRC">,MD002,1), '
      '      SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)'
      '    )'
      '  );'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_018_14_AGetValue
    OnUserFunction = frxWET_CURTI_018_14_AUserFunction
    Left = 80
    Top = 8
    Datasets = <
      item
        DataSet = frxDs14VSMovIts
        DataSetName = 'frxDs14VSMovIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 117.165322600000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 377.953000000000000000
          Top = 102.047212360000000000
          Width = 136.062992125984300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 343.937230000000000000
          Top = 102.047212360000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 582.047620000000000000
          Top = 102.047212360000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 929.764380000000000000
          Top = 102.047212360000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MPAG m'#233'dia')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 514.016080000000000000
          Top = 102.047212360000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 196.535560000000000000
          Top = 102.047212360000000000
          Width = 147.401584570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 162.519790000000000000
          Top = 102.047212360000000000
          Width = 34.015748031496060000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 702.992580000000000000
          Top = 102.047212360000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'd. kg/Int')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 699.213050000000000000
          Top = 60.472480000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 759.685530000000000000
          Top = 60.472480000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_FICHA_RMP_14]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 71.811070000000000000
          Top = 79.370130000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_14]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 71.811070000000000000
          Top = 60.472480000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_GRAGRUX_14]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 699.213050000000000000
          Top = 79.370130000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 759.685530000000000000
          Top = 79.370130000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO_14]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 755.906000000000000000
          Top = 102.047310000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 823.937540000000000000
          Top = 102.047310000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'd. m'#178'/Int')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 49.133890000000000000
          Top = 102.047310000000000000
          Width = 113.385824330000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 7.559060000000000000
          Top = 102.047310000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 642.520100000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 876.850960000000000000
          Top = 102.047310000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MPAG calc.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118207880000000000
        Top = 279.685220000000000000
        Width = 990.236860000000000000
        DataSet = frxDs14VSMovIts
        DataSetName = 'frxDs14VSMovIts'
        RowCount = 0
        object MeMD_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeMD_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          Left = 377.953000000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 343.937230000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 582.047620000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 929.764380000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 196.535560000000000000
          Width = 147.401584570000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 162.519790000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."MediaKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 755.906000000000000000
          Top = 0.000097640000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'QtdGerArM2'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."QtdGerArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 823.937540000000000000
          Top = 0.000097640000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."MediaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 49.133890000000000000
          Width = 113.385824330000000000
          Height = 15.118110240000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_SERIE_FICHA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 7.559060000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 642.520100000000000000
          Top = 0.000097640000000010
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 876.850960000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDs14VSMovIts
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 498.897960000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Left = 7.559060000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 657.638220000000000000
          Width = 340.157700000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 453.543600000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Me_FTT: TfrxMemoView
          Top = 3.779530000000000000
          Width = 514.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE ITENS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_T: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_T: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_T: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_T: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.5n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."MediaKg">,MD002,1) > 0, SUM(<frxDs14V' +
              'SMovIts."SumVNC">,MD002,1) / SUM(<frxDs14VSMovIts."MediaKg">,MD0' +
              '02,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_T: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.5n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_T: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 990.236860000000000000
        Condition = 'frxDs14VSMovIts."GraGruX"'
        object Me_GH1: TfrxMemoView
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456692910000000000
        Top = 366.614410000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'FT_01OnBeforePrint'
        object MeFT_01: TfrxMemoView
          Width = 990.236371810000000000
          Height = 22.677167800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_FT1: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 423.307164720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_1: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_1: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_1: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVNC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_1: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVRC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_1: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 238.110390000000000000
        Width = 990.236860000000000000
        Condition = 'frxDs14VSMovIts."GraGruX"'
        object MeGH_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677311810000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 317.480520000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'FT_02OnBeforePrint'
        object MeFT_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 26.456695350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeFT_02_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 22.677167800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 423.307164720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_2: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_2: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779627640000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779627640000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_2: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_2: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVNC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_2: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVRC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_2: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDs14VSMovIts: TfrxDBDataset
    UserName = 'frxDs14VSMovIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'VSP_CliStat=VSP_CliStat'
      'VSP_STATUS=VSP_STATUS'
      'NO_FORNECE=NO_FORNECE'
      'NO_CliStat=NO_CliStat'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'NO_SerieFch=NO_SerieFch'
      'NO_SERIE_FICHA=NO_SERIE_FICHA'
      'AnoSemana=AnoSemana'
      'AnoMes=AnoMes'
      'Data=Data'
      'Ano=Ano'
      'BaseMPAG=BaseMPAG'
      'MediaM2=MediaM2'
      'Inteiros=Inteiros'
      'SumVNC=SumVNC'
      'SumVRC=SumVRC'
      'MediaKg=MediaKg'
      'VNC_Kg=VNC_Kg')
    DataSet = Qr14VsMovIts
    BCDToCurrency = False
    Left = 80
    Top = 104
  end
  object Qr14VsMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.*, '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse","For'#231'ado","Sem O' +
        'rigem","Em Operacao","Residual","Ajuste","Classe Mult.","Pr'#233' rec' +
        'lasse","Compra de Classificado","Baixa extra") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Origem gera'#231#227'o de art' +
        'igo","Destino gera'#231#227'o de artigo","Baixa gera'#231#227'o de artigo") NO_M' +
        'ovimNiv,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, '
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, '
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'vps.Nome NO_STATUS, '
      
        'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_' +
        'FICHA, '
      
        'vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorI' +
        'nt)) MediaKg, '
      
        'vmi.QtdGerArM2 / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.Fa' +
        'torInt)) MediaM2, '
      'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data,   '
      
        'CONCAT(YEAR(vmi.DataHora), ".", LPAD(WEEK(vmi.DataHora), 2, "0")' +
        ') AnoSemana, '
      'DATE_FORMAT(vmi.DataHora, "%m/%Y") AnoMes,   '
      'DATE_FORMAT(vmi.DataHora, "%Y") Ano, '
      'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, '
      
        'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) * vmi.Nota' +
        'MPAG BaseMPAG, '
      
        '(vmi.FatNotaVNC * (vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS ' +
        'NULL, 0, cn1.FatorInt)))) SumVNC, '
      'vmi.FatNotaVNC * vmi.PesoKg VNC_Kg,'
      
        'vmi.FatNotaVNC * (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.Fa' +
        'torInt)) VNC_Pc, '
      
        '(vmi.FatNotaVRC * IF(vmi.QtdGerArM2 < 0, -QtdGerArM2, QtdGerArM2' +
        ')) SumVRC '
      'FROM vsmovits vmi '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   '
      
        'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp' +
        '.GraGruX, vmi.GraGruX) '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro '
      'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  '
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'WHERE vmi.Empresa=-11'
      'AND vmi.MovimID=1'
      'AND vmi.MovimNiv=0'
      'AND vmi.NotaMPAG > 0'
      'ORDER BY Terceiro, Terceiro, AnoMes, DataHora, Controle '
      '')
    Left = 80
    Top = 56
    object Qr14VsMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr14VsMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object Qr14VsMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object Qr14VsMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object Qr14VsMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object Qr14VsMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr14VsMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object Qr14VsMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object Qr14VsMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object Qr14VsMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object Qr14VsMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object Qr14VsMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object Qr14VsMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object Qr14VsMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object Qr14VsMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object Qr14VsMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object Qr14VsMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object Qr14VsMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object Qr14VsMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr14VsMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object Qr14VsMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object Qr14VsMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object Qr14VsMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object Qr14VsMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object Qr14VsMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object Qr14VsMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object Qr14VsMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object Qr14VsMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object Qr14VsMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object Qr14VsMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object Qr14VsMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object Qr14VsMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object Qr14VsMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object Qr14VsMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object Qr14VsMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object Qr14VsMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object Qr14VsMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object Qr14VsMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object Qr14VsMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object Qr14VsMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object Qr14VsMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object Qr14VsMovItsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object Qr14VsMovItsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 31
    end
    object Qr14VsMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr14VsMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object Qr14VsMovItsVSP_CliStat: TLargeintField
      FieldName = 'VSP_CliStat'
    end
    object Qr14VsMovItsVSP_STATUS: TLargeintField
      FieldName = 'VSP_STATUS'
    end
    object Qr14VsMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr14VsMovItsNO_CliStat: TWideStringField
      FieldName = 'NO_CliStat'
      Size = 100
    end
    object Qr14VsMovItsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr14VsMovItsNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object Qr14VsMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr14VsMovItsNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 72
    end
    object Qr14VsMovItsAnoSemana: TWideStringField
      DisplayWidth = 20
      FieldName = 'AnoSemana'
    end
    object Qr14VsMovItsAnoMes: TWideStringField
      FieldName = 'AnoMes'
      Size = 7
    end
    object Qr14VsMovItsData: TWideStringField
      FieldName = 'Data'
    end
    object Qr14VsMovItsAno: TWideStringField
      FieldName = 'Ano'
      Size = 4
    end
    object Qr14VsMovItsBaseMPAG: TFloatField
      FieldName = 'BaseMPAG'
    end
    object Qr14VsMovItsMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object Qr14VsMovItsInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr14VsMovItsSumVNC: TFloatField
      FieldName = 'SumVNC'
    end
    object Qr14VsMovItsSumVRC: TFloatField
      FieldName = 'SumVRC'
    end
    object Qr14VsMovItsMediaKg: TFloatField
      FieldName = 'MediaKg'
    end
    object Qr14VsMovItsVNC_Kg: TFloatField
      FieldName = 'VNC_Kg'
    end
  end
end
