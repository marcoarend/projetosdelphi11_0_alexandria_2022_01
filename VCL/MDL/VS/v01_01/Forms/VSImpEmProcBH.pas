unit VSImpEmProcBH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  frxClass, frxDBSet, UnProjGroup_Consts;

type
  TFmVSImpEmProcBH = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    frxWET_CURTI_151_01: TfrxReport;
    QrEmit: TMySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    QrEmitHoraIni: TTimeField;
    frxDsEmit: TfrxDBDataset;
    QrEmProc: TMySQLQuery;
    QrEmProcCodigo: TIntegerField;
    QrEmProcControle: TIntegerField;
    QrEmProcMovimCod: TIntegerField;
    QrEmProcMovimNiv: TIntegerField;
    QrEmProcMovimTwn: TIntegerField;
    QrEmProcEmpresa: TIntegerField;
    QrEmProcTerceiro: TIntegerField;
    QrEmProcCliVenda: TIntegerField;
    QrEmProcMovimID: TIntegerField;
    QrEmProcLnkNivXtr1: TIntegerField;
    QrEmProcLnkNivXtr2: TIntegerField;
    QrEmProcDataHora: TDateTimeField;
    QrEmProcPallet: TIntegerField;
    QrEmProcGraGruX: TIntegerField;
    QrEmProcPecas: TFloatField;
    QrEmProcPesoKg: TFloatField;
    QrEmProcAreaM2: TFloatField;
    QrEmProcAreaP2: TFloatField;
    QrEmProcValorT: TFloatField;
    QrEmProcSrcMovID: TIntegerField;
    QrEmProcSrcNivel1: TIntegerField;
    QrEmProcSrcNivel2: TIntegerField;
    QrEmProcSdoVrtPeca: TFloatField;
    QrEmProcSdoVrtArM2: TFloatField;
    QrEmProcObserv: TWideStringField;
    QrEmProcFicha: TIntegerField;
    QrEmProcMisturou: TSmallintField;
    QrEmProcCustoMOKg: TFloatField;
    QrEmProcCustoMOTot: TFloatField;
    QrEmProcLk: TIntegerField;
    QrEmProcDataCad: TDateField;
    QrEmProcDataAlt: TDateField;
    QrEmProcUserCad: TIntegerField;
    QrEmProcUserAlt: TIntegerField;
    QrEmProcAlterWeb: TSmallintField;
    QrEmProcAtivo: TSmallintField;
    QrEmProcSrcGGX: TIntegerField;
    QrEmProcSdoVrtPeso: TFloatField;
    QrEmProcSerieFch: TIntegerField;
    QrEmProcFornecMO: TIntegerField;
    QrEmProcValorMP: TFloatField;
    QrEmProcDstMovID: TIntegerField;
    QrEmProcDstNivel1: TIntegerField;
    QrEmProcDstNivel2: TIntegerField;
    QrEmProcDstGGX: TIntegerField;
    QrEmProcQtdGerPeca: TFloatField;
    QrEmProcQtdGerPeso: TFloatField;
    QrEmProcQtdGerArM2: TFloatField;
    QrEmProcQtdGerArP2: TFloatField;
    QrEmProcQtdAntPeca: TFloatField;
    QrEmProcQtdAntPeso: TFloatField;
    QrEmProcQtdAntArM2: TFloatField;
    QrEmProcQtdAntArP2: TFloatField;
    QrEmProcAptoUso: TSmallintField;
    QrEmProcNotaMPAG: TFloatField;
    QrEmProcMarca: TWideStringField;
    QrEmProcTpCalcAuto: TIntegerField;
    QrEmProcZerado: TSmallintField;
    QrEmProcEmFluxo: TSmallintField;
    QrEmProcLnkIDXtr: TIntegerField;
    QrEmProcCustoMOM2: TFloatField;
    QrEmProcNotFluxo: TIntegerField;
    QrEmProcFatNotaVNC: TFloatField;
    QrEmProcFatNotaVRC: TFloatField;
    QrEmProcPedItsLib: TIntegerField;
    QrEmProcPedItsFin: TIntegerField;
    QrEmProcPedItsVda: TIntegerField;
    QrEmProcReqMovEstq: TIntegerField;
    QrEmProcStqCenLoc: TIntegerField;
    QrEmProcItemNFe: TIntegerField;
    QrEmProcVSMorCab: TIntegerField;
    QrEmProcVSMulFrnCab: TIntegerField;
    QrEmProcClientMO: TIntegerField;
    QrEmProcCustoPQ: TFloatField;
    QrEmProcNO_SERIE_FICHA: TWideStringField;
    QrEmProcNO_CLIENTMO: TWideStringField;
    frxDsEmProc: TfrxDBDataset;
    QrOrigem: TMySQLQuery;
    QrOrigemNO_SERIE_FICHA: TWideStringField;
    QrOrigemCodigo: TIntegerField;
    QrOrigemControle: TIntegerField;
    QrOrigemMovimCod: TIntegerField;
    QrOrigemMovimNiv: TIntegerField;
    QrOrigemMovimTwn: TIntegerField;
    QrOrigemEmpresa: TIntegerField;
    QrOrigemTerceiro: TIntegerField;
    QrOrigemCliVenda: TIntegerField;
    QrOrigemMovimID: TIntegerField;
    QrOrigemLnkNivXtr1: TIntegerField;
    QrOrigemLnkNivXtr2: TIntegerField;
    QrOrigemDataHora: TDateTimeField;
    QrOrigemPallet: TIntegerField;
    QrOrigemGraGruX: TIntegerField;
    QrOrigemPecas: TFloatField;
    QrOrigemPesoKg: TFloatField;
    QrOrigemAreaM2: TFloatField;
    QrOrigemAreaP2: TFloatField;
    QrOrigemValorT: TFloatField;
    QrOrigemSrcMovID: TIntegerField;
    QrOrigemSrcNivel1: TIntegerField;
    QrOrigemSrcNivel2: TIntegerField;
    QrOrigemSdoVrtPeca: TFloatField;
    QrOrigemSdoVrtArM2: TFloatField;
    QrOrigemObserv: TWideStringField;
    QrOrigemFicha: TIntegerField;
    QrOrigemMisturou: TSmallintField;
    QrOrigemCustoMOKg: TFloatField;
    QrOrigemCustoMOTot: TFloatField;
    QrOrigemLk: TIntegerField;
    QrOrigemDataCad: TDateField;
    QrOrigemDataAlt: TDateField;
    QrOrigemUserCad: TIntegerField;
    QrOrigemUserAlt: TIntegerField;
    QrOrigemAlterWeb: TSmallintField;
    QrOrigemAtivo: TSmallintField;
    QrOrigemSrcGGX: TIntegerField;
    QrOrigemSdoVrtPeso: TFloatField;
    QrOrigemSerieFch: TIntegerField;
    QrOrigemFornecMO: TIntegerField;
    QrOrigemValorMP: TFloatField;
    QrOrigemDstMovID: TIntegerField;
    QrOrigemDstNivel1: TIntegerField;
    QrOrigemDstNivel2: TIntegerField;
    QrOrigemDstGGX: TIntegerField;
    QrOrigemQtdGerPeca: TFloatField;
    QrOrigemQtdGerPeso: TFloatField;
    QrOrigemQtdGerArM2: TFloatField;
    QrOrigemQtdGerArP2: TFloatField;
    QrOrigemQtdAntPeca: TFloatField;
    QrOrigemQtdAntPeso: TFloatField;
    QrOrigemQtdAntArM2: TFloatField;
    QrOrigemQtdAntArP2: TFloatField;
    QrOrigemAptoUso: TSmallintField;
    QrOrigemNotaMPAG: TFloatField;
    QrOrigemMarca: TWideStringField;
    QrOrigemTpCalcAuto: TIntegerField;
    QrOrigemZerado: TSmallintField;
    QrOrigemEmFluxo: TSmallintField;
    QrOrigemLnkIDXtr: TIntegerField;
    QrOrigemCustoMOM2: TFloatField;
    QrOrigemNotFluxo: TIntegerField;
    QrOrigemFatNotaVNC: TFloatField;
    QrOrigemFatNotaVRC: TFloatField;
    QrOrigemPedItsLib: TIntegerField;
    QrOrigemPedItsFin: TIntegerField;
    QrOrigemPedItsVda: TIntegerField;
    QrOrigemReqMovEstq: TIntegerField;
    QrOrigemStqCenLoc: TIntegerField;
    QrOrigemItemNFe: TIntegerField;
    QrOrigemVSMorCab: TIntegerField;
    QrOrigemVSMulFrnCab: TIntegerField;
    QrOrigemClientMO: TIntegerField;
    QrOrigemNO_CLIENTMO: TWideStringField;
    QrOrigemCustoPQ: TFloatField;
    frxDsorigem: TfrxDBDataset;
    QrMCs: TMySQLQuery;
    QrMCsMovimCod: TIntegerField;
    DsEmit: TDataSource;
    QrEmitDtaBaixa: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_151_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEmitAfterScroll(DataSet: TDataSet);
    procedure QrEmProcAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ImprimeVSEmProcBH();
  public
    { Public declarations }
  end;

  var
  FmVSImpEmProcBH: TFmVSImpEmProcBH;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UndmkProcFunc;

{$R *.DFM}

procedure TFmVSImpEmProcBH.BtOKClick(Sender: TObject);
begin
  ImprimeVSEmProcBH();
end;

procedure TFmVSImpEmProcBH.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpEmProcBH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpEmProcBH.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  Agora := DModG.ObtemAgora();
  TPDataIni.Date := Agora - 10;
  TPDataFim.Date := Agora + 1;
end;

procedure TFmVSImpEmProcBH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpEmProcBH.frxWET_CURTI_151_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPDataIni.Date, TPDataFim.Date,
    CkDataIni.Checked, CkDataIni.Checked, 'Per�odo: ', '', '')
  else
end;

procedure TFmVSImpEmProcBH.ImprimeVSEmProcBH();
var
  Corda, Movims, SQL_Periodo: String;
begin
  Movims := Geral.FF0(Integer(TEstqMovimID.emidEmProcCal)) + ',' +  // 26
            Geral.FF0(Integer(TEstqMovimID.emidEmProcCur));         // 27
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _CAL_CUR_EM_PROCESSO_; ',
  ' ',
  'CREATE TABLE _CAL_CUR_EM_PROCESSO_ ',
  'SELECT *  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID IN (' + Movims + ') ',
  'AND SdoVrtPeca > 0 ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _CAL_CUR_VS_ORIGEM_; ',
  ' ',
  'CREATE TABLE _CAL_CUR_VS_ORIGEM_ ',
  'SELECT *  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID IN (' + Movims + ') ',
  'AND QtdAntPeca > QtdGerPeca ',
  '; ',
  ' ']));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMCs, DModG.MyPID_DB, [
  ' SELECT DISTINCT MovimCod  ',
  ' FROM _CAL_CUR_VS_ORIGEM_ ',
  '']);
  Corda := MyObjects.CordaDeQuery(QrMCs, 'MovimCod', '-999999999');
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND DtaBaixa ',
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, DModG.MyPID_DB, [
  'SELECT *  ',
  'FROM ' + TMeuDB + '.emit ',
  'WHERE VSMovCod IN ( ',
  Corda,
  ') ',
  SQL_Periodo,
  'ORDER BY NOMESETOR, DataEmis, Codigo',
  '']);
  //
  //Geral.MB_Teste(QrEmit.SQL.Text);
  //
  if QrEmit.RecordCount > 0 then
  begin
    MyObjects.frxDefineDataSets(frxWET_CURTI_151_01, [
    DMOdG.frxDsDono,
    frxDsEmit,
    frxDsEmProc,
    frxDsOrigem
    ]);
    QrEmit.DisableControls;
    try
      MyObjects.frxMostra(frxWET_CURTI_151_01, 'Couros em Processo BH');
    finally
      QrEmit.EnableControls;
    end;
  end else
    Geral.MB_Info('N�o foi encontrado nenhum processo em andamento para o per�odo selecionado!');
end;

procedure TFmVSImpEmProcBH.QrEmitAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmProc, DModG.MyPID_DB, [
  'SELECT cep.*, ',
  'CONCAT(vsf.Nome, " ", cep.Ficha) NO_SERIE_FICHA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTMO  ',
  'FROM _cal_cur_em_processo_ cep ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=cep.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=cep.ClientMO ',
  'WHERE cep.MovimCod=' + Geral.FF0(QrEmitVSMovCod.Value),
  '']);
end;

procedure TFmVSImpEmProcBH.QrEmProcAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrigem, DModG.MyPID_DB, [
  'SELECT cvo.*, ',
  'CONCAT(vsf.Nome, " ", cvo.Ficha) NO_SERIE_FICHA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTMO ',
  'FROM _cal_cur_vs_origem_ cvo ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch vsf ON vsf.Codigo=cvo.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=cvo.ClientMO ',
  'WHERE cvo.MovimCod=' + Geral.FF0(QrEmitVSMovCod.Value),
  '']);
end;

end.
