unit VSAjsCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, UnAppEnums;

type
  TFmVSAjsCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSAjsCab: TmySQLQuery;
    DsVSAjsCab: TDataSource;
    QrVSAjsIts: TmySQLQuery;
    DsVSAjsIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSAjsCabCodigo: TIntegerField;
    QrVSAjsCabMovimCod: TIntegerField;
    QrVSAjsCabEmpresa: TIntegerField;
    QrVSAjsCabDataHora: TDateTimeField;
    QrVSAjsCabPecas: TFloatField;
    QrVSAjsCabPesoKg: TFloatField;
    QrVSAjsCabAreaM2: TFloatField;
    QrVSAjsCabAreaP2: TFloatField;
    QrVSAjsCabLk: TIntegerField;
    QrVSAjsCabDataCad: TDateField;
    QrVSAjsCabDataAlt: TDateField;
    QrVSAjsCabUserCad: TIntegerField;
    QrVSAjsCabUserAlt: TIntegerField;
    QrVSAjsCabAlterWeb: TSmallintField;
    QrVSAjsCabAtivo: TSmallintField;
    QrVSAjsCabNO_EMPRESA: TWideStringField;
    QrVSAjsItsPecas: TFloatField;
    QrVSAjsItsPesoKg: TFloatField;
    QrVSAjsItsAreaM2: TFloatField;
    QrVSAjsItsAreaP2: TFloatField;
    QrVSAjsItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    QrVSAjsItsNO_PALLET: TWideStringField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    frxWET_RECUR_010_01: TfrxReport;
    frxDsVSAjsIts: TfrxDBDataset;
    frxDsVSAjsCab: TfrxDBDataset;
    QrVSAjsItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaPecas: TFloatField;
    QrPalletClaAreaM2: TFloatField;
    QrPalletClaAreaP2: TFloatField;
    QrPalletClaPesoKg: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxWET_RECUR_010_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrVSAjsItsNO_FORNECE: TWideStringField;
    QrVSAjsItsDataHora: TDateTimeField;
    QrVSAjsItsValorT: TFloatField;
    QrVSAjsItsSdoVrtPeca: TFloatField;
    QrVSAjsItsSdoVrtArM2: TFloatField;
    QrVSAjsItsSdoVrtPeso: TFloatField;
    QrVSAjsItsCustoMOKg: TFloatField;
    QrVSAjsItsCustoMOTot: TFloatField;
    QrVSAjsItsValorMP: TFloatField;
    QrVSAjsItsQtdGerPeca: TFloatField;
    QrVSAjsItsQtdGerPeso: TFloatField;
    QrVSAjsItsQtdGerArM2: TFloatField;
    QrVSAjsItsQtdGerArP2: TFloatField;
    QrVSAjsItsQtdAntPeca: TFloatField;
    QrVSAjsItsQtdAntPeso: TFloatField;
    QrVSAjsItsQtdAntArM2: TFloatField;
    QrVSAjsItsQtdAntArP2: TFloatField;
    QrVSAjsItsNotaMPAG: TFloatField;
    QrVSAjsItsCustoMOM2: TFloatField;
    QrVSAjsItsCUS_PesoKg: TFloatField;
    QrVSAjsItsCUS_AreaM2: TFloatField;
    QrVSAjsItsCUS_AreaP2: TFloatField;
    QrVSAjsItsCUS_Peca: TFloatField;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSAjsCabTemIMEIMrt: TSmallintField;
    QrVSAjsItsFornecMO: TLargeintField;
    QrVSAjsItsNO_TTW: TWideStringField;
    QrVSAjsItsCodigo: TLargeintField;
    QrVSAjsItsControle: TLargeintField;
    QrVSAjsItsMovimCod: TLargeintField;
    QrVSAjsItsMovimNiv: TLargeintField;
    QrVSAjsItsMovimTwn: TLargeintField;
    QrVSAjsItsEmpresa: TLargeintField;
    QrVSAjsItsTerceiro: TLargeintField;
    QrVSAjsItsCliVenda: TLargeintField;
    QrVSAjsItsMovimID: TLargeintField;
    QrVSAjsItsPallet: TLargeintField;
    QrVSAjsItsGraGruX: TLargeintField;
    QrVSAjsItsSrcMovID: TLargeintField;
    QrVSAjsItsSrcNivel1: TLargeintField;
    QrVSAjsItsSrcNivel2: TLargeintField;
    QrVSAjsItsSrcGGX: TLargeintField;
    QrVSAjsItsSerieFch: TLargeintField;
    QrVSAjsItsFicha: TLargeintField;
    QrVSAjsItsMisturou: TLargeintField;
    QrVSAjsItsDstMovID: TLargeintField;
    QrVSAjsItsDstNivel1: TLargeintField;
    QrVSAjsItsDstNivel2: TLargeintField;
    QrVSAjsItsDstGGX: TLargeintField;
    QrVSAjsItsID_TTW: TLargeintField;
    QrVSAjsItsStqCenLoc: TLargeintField;
    QrVSAjsItsReqMovEstq: TLargeintField;
    QrVSAjsCabClienteMO: TIntegerField;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    QrVSAjsCabNO_CLIENTEMO: TWideStringField;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrVSAjsItsIxxMovIX: TLargeintField;
    QrVSAjsItsIxxFolha: TLargeintField;
    QrVSAjsItsIxxLinha: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSAjsCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSAjsCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSAjsCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSAjsCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_RECUR_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSAjsIts(SQLType: TSQLType);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSAjsIts(Controle: Integer);

  end;

var
  FmVSAjsCab: TFmVSAjsCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSAjsIts, ModuleGeral,
  Principal, VSMovImp, UnVS_PF, CreateVS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSAjsCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSAjsCab.MostraFormVSAjsIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSAjsIts, FmVSAjsIts, afmoNegarComAviso) then
  begin
    FmVSAjsIts.ImgTipo.SQLType := SQLType;
    FmVSAjsIts.FQrCab := QrVSAjsCab;
    FmVSAjsIts.FDsCab := DsVSAjsCab;
    FmVSAjsIts.FQrIts := QrVSAjsIts;
    FmVSAjsIts.FDataHora := QrVSAjsCabDataHora.Value;
    FmVSAjsIts.FEmpresa  := QrVSAjsCabEmpresa.Value;
    FmVSAjsIts.FClientMO := QrVSAjsCabClienteMO.Value;
    if SQLType = stIns then
    begin
      //FmVSAjsIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSAjsIts.EdControle.ValueVariant := QrVSAjsItsControle.Value;
      //
      FmVSAjsIts.EdGragruX.ValueVariant    := QrVSAjsItsGraGruX.Value;
      FmVSAjsIts.CBGragruX.KeyValue        := QrVSAjsItsGraGruX.Value;
      FmVSAjsIts.EdFornecedor.ValueVariant := QrVSAjsItsTerceiro.Value;
      FmVSAjsIts.CBFornecedor.KeyValue     := QrVSAjsItsTerceiro.Value;
      FmVSAjsIts.EdSerieFch.ValueVariant   := QrVSAjsItsSerieFch.Value;
      FmVSAjsIts.CBSerieFch.KeyValue       := QrVSAjsItsSerieFch.Value;
      FmVSAjsIts.EdFicha.ValueVariant      := QrVSAjsItsFicha.Value;
      FmVSAjsIts.EdPallet.ValueVariant     := QrVSAjsItsPallet.Value;
      FmVSAjsIts.CBPallet.KeyValue         := QrVSAjsItsPallet.Value;
      FmVSAjsIts.EdPecas.ValueVariant      := QrVSAjsItsPecas.Value;
      FmVSAjsIts.EdPesoKg.ValueVariant     := QrVSAjsItsPesoKg.Value;
      FmVSAjsIts.EdAreaM2.ValueVariant     := QrVSAjsItsAreaM2.Value;
      FmVSAjsIts.EdAreaP2.ValueVariant     := QrVSAjsItsAreaP2.Value;
      FmVSAjsIts.EdValorMP.ValueVariant    := QrVSAjsItsValorMP.Value;
      FmVSAjsIts.EdCustoMOKg.ValueVariant  := QrVSAjsItsCustoMOKg.Value;
      FmVSAjsIts.EdCustoMOM2.ValueVariant  := QrVSAjsItsCustoMOM2.Value;
      FmVSAjsIts.EdFornecMO.ValueVariant   := QrVSAjsItsFornecMO.Value;
      FmVSAjsIts.CBFornecMO.KeyValue       := QrVSAjsItsFornecMO.Value;
      FmVSAjsIts.EdObserv.ValueVariant     := QrVSAjsItsObserv.Value;
      FmVSAjsIts.EdStqCenLoc.ValueVariant  := QrVSAjsItsStqCenLoc.Value;
      FmVSAjsIts.CBStqCenLoc.KeyValue      := QrVSAjsItsStqCenLoc.Value;
      FmVSAjsIts.EdReqMovEstq.ValueVariant := QrVSAjsItsReqMovEstq.Value;
      FmVSAjsIts.RGIxxMovIX.ItemIndex      := QrVSAjsItsIxxMovIX.Value;
      FmVSAjsIts.EdIxxFolha.ValueVariant   := QrVSAjsItsIxxFolha.Value;
      FmVSAjsIts.EdIxxLinha.ValueVariant   := QrVSAjsItsIxxLinha.Value;
      //
      if QrVSAjsItsDstNivel2.Value <> 0 then
      begin
        FmVSAjsIts.EdNivel2.ValueVariant     := QrVSAjsItsDstNivel2.Value;
        FmVSAjsIts.CBNivel2.KeyValue         := QrVSAjsItsDstNivel2.Value;
      end else
      begin
        FmVSAjsIts.EdNivel2.ValueVariant     := QrVSAjsItsSrcNivel2.Value;
        FmVSAjsIts.CBNivel2.KeyValue         := QrVSAjsItsSrcNivel2.Value;
      end;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSAjsIts.EdCustoMOTot.ValueVariant := QrVSAjsItsCustoMOTot.Value;
      FmVSAjsIts.EdValorT.ValueVariant     := QrVSAjsItsValorT.Value;
    end;
    FmVSAjsIts.ShowModal;
    FmVSAjsIts.Destroy;
  end;
end;

procedure TFmVSAjsCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSAjsCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSAjsCab, QrVSAjsIts);
end;

procedure TFmVSAjsCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSAjsCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSAjsIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSAjsIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSAjsItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSAjsCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSAjsCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSAjsCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsajscab';
  VAR_GOTOMYSQLTABLE := QrVSAjsCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_CLIENTEMO ');
  VAR_SQLx.Add('FROM vsajscab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cmo ON cmo.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  VAR_SQLx.Add('AND wic.Codigo=:P0');  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSAjsCab.Estoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSAjsCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSAjsCabEmpresa.Value;
  N := 0;
  QrVSAjsIts.First;
  while not QrVSAjsIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSAjsItsPallet.Value;
    //
    QrVSAjsIts.Next;
  end;
  if N > 0 then
    VS_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSAjsCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSAjsIts(stUpd);
end;

procedure TFmVSAjsCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSAjsCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSAjsCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSAjsCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSAjsItsCodigo.Value;
  MovimCod := QrVSAjsItsMovimCod.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSAjsIts, TIntegerField(QrVSAjsItsMovimCod),
  QrVSAjsItsControle.Value, CtrlBaix, QrVSAjsItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti023)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsajscab', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSAjsCab.ReopenVSAjsIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSAjsCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.PesoKg=0, 0, vmi.ValorT / vmi.PesoKg) CUS_PesoKg, ',
  'IF(vmi.AreaM2=0, 0, vmi.ValorT / vmi.AreaM2) CUS_AreaM2, ',
  'IF(vmi.AreaP2=0, 0, vmi.ValorT / vmi.AreaP2) CUS_AreaP2, ',
  'IF(vmi.Pecas=0, 0, vmi.ValorT / vmi.Pecas) CUS_Peca',
  '']);
  //'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSAjsCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAjsIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSAjsIts);
  //
  QrVSAjsIts.Locate('Controle', Controle, []);
end;


procedure TFmVSAjsCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSAjsCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSAjsCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSAjsCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSAjsCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSAjsCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSAjsCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSAjsCabCodigo.Value;
  Close;
end;

procedure TFmVSAjsCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSAjsIts(stIns);
end;

procedure TFmVSAjsCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsajscab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSAjsCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSAjsCab.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  Codigo, MovimCod, Empresa, ClienteMO, Terceiro: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  ClienteMO      := EdClienteMO.ValueVariant;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina uma data de compra!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsajscab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsajscab', False, [
  'MovimCod', 'Empresa', CO_DATA_HORA_GRL,
  'ClienteMO'
  (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',*)], [
  'Codigo'], [
  MovimCod, Empresa, DataHora,
  ClienteMO
  (*, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidInventario, Codigo)
    else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_VMI, 'ClientMO'
      ], ['MovimCod'], [
      Empresa, DataHora, ClienteMO
      ], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSAjsCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsajscab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsajscab', 'Codigo');
end;

procedure TFmVSAjsCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSAjsCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSAjsCab, QrVSAjsCabDataHora.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSAjsCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
end;

procedure TFmVSAjsCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSAjsCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSAjsCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSAjsCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSAjsCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSAjsCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE ' + CO_UPD_TAB_VMI,
  'SET MovimID=' + Geral.FF0(Integer(emidInventario)),
  'WHERE MovimID=' + Geral.FF0(Integer(emidAjuste)),
  'AND MovimCod IN ( ',
  '     SELECT MovimCod ',
  '     FROM vsajscab ',
  ') ',
  '']));
  Geral.MB_Aviso('Atualiza��o 0 > 13 OK!');
end;

procedure TFmVSAjsCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSAjsCab.QrVSAjsCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSAjsCab.QrVSAjsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSAjsIts(0);
end;

procedure TFmVSAjsCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSAjsCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSAjsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSAjsCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSAjsCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSAjsCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsajscab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSAjsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSAjsCab.frxWET_RECUR_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSAjsCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsajscab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSAjsCab.Classificao1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxWET_RECUR_010_01, 'Ajuste estoque Couros');
end;

procedure TFmVSAjsCab.QrVSAjsCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSAjsIts.Close;
end;

procedure TFmVSAjsCab.QrVSAjsCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSAjsCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

