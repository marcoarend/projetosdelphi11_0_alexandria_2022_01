unit VSSifDipoaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBLookupComboBox, UnGrl_Consts,
  dmkEditCB, dmkEditDateTimePicker, dmkDBGridZTO, frxClass, frxDBSet, Vcl.Menus;

type
  TFmVSSifDipoaImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXPrevPcPal: TIntegerField;
    QrGraGruXUsaSifDipoa: TSmallintField;
    DsGraGruX: TDataSource;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    LaVSRibCla: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    BtImprime: TBitBtn;
    DsPesq: TDataSource;
    QrVSPallet: TMySQLQuery;
    DBGBags: TDBGrid;
    QrPals: TMySQLQuery;
    QrPalsPallet: TIntegerField;
    BtNenhum: TBitBtn;
    BtTudo: TBitBtn;
    QrVSPalletPallet: TIntegerField;
    QrVSPalletPecas: TFloatField;
    QrVSPalletPesoKg: TFloatField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletSeqSifDipoa: TIntegerField;
    QrVSPalletObsSifDipoa: TWideStringField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    frxSIF_DIPOA_003_A: TfrxReport;
    frxDsVSPallet: TfrxDBDataset;
    QrVSPalletArtigoImp: TWideStringField;
    frxDsVsSifDipoa: TfrxDBDataset;
    QrVSPalletDataFab: TDateField;
    QrVSPalletMARCAS: TWideMemoField;
    QrVSPalletMIN_DataHora: TDateTimeField;
    QrVSPalletMAX_DataHora: TDateTimeField;
    QrVMI: TMySQLQuery;
    QrVMINO_TTW: TWideStringField;
    QrVMIID_TTW: TLargeintField;
    QrVMICodigo: TLargeintField;
    QrVMIControle: TLargeintField;
    QrVMIMovimCod: TLargeintField;
    QrVMIMovimNiv: TLargeintField;
    QrVMIMovimTwn: TLargeintField;
    QrVMIEmpresa: TLargeintField;
    QrVMIClientMO: TLargeintField;
    QrVMITerceiro: TLargeintField;
    QrVMICliVenda: TLargeintField;
    QrVMIMovimID: TLargeintField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TLargeintField;
    QrVMIGraGruX: TLargeintField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMISrcMovID: TLargeintField;
    QrVMISrcNivel1: TLargeintField;
    QrVMISrcNivel2: TLargeintField;
    QrVMISrcGGX: TLargeintField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMISerieFch: TLargeintField;
    QrVMIFicha: TLargeintField;
    QrVMIMisturou: TLargeintField;
    QrVMIFornecMO: TLargeintField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMIValorMP: TFloatField;
    QrVMICustoPQ: TFloatField;
    QrVMIDstMovID: TLargeintField;
    QrVMIDstNivel1: TLargeintField;
    QrVMIDstNivel2: TLargeintField;
    QrVMIDstGGX: TLargeintField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMINotaMPAG: TFloatField;
    QrVMIMarca: TWideStringField;
    QrVMIPedItsLib: TLargeintField;
    QrVMIPedItsFin: TLargeintField;
    QrVMIPedItsVda: TLargeintField;
    QrVMICustoMOM2: TFloatField;
    QrVMIReqMovEstq: TLargeintField;
    QrVMIStqCenLoc: TLargeintField;
    QrVMIItemNFe: TLargeintField;
    QrVMIVSMulFrnCab: TLargeintField;
    QrVMINFeNum: TLargeintField;
    QrVMIVSMulNFeCab: TLargeintField;
    QrVMIJmpMovID: TLargeintField;
    QrVMIJmpNivel1: TLargeintField;
    QrVMIJmpNivel2: TLargeintField;
    QrVMIRmsMovID: TLargeintField;
    QrVMIRmsNivel1: TLargeintField;
    QrVMIRmsNivel2: TLargeintField;
    QrVMIGGXRcl: TLargeintField;
    QrVMIRmsGGX: TLargeintField;
    QrVMIJmpGGX: TLargeintField;
    QrVMIDtCorrApo: TDateTimeField;
    QrVMIIxxMovIX: TLargeintField;
    QrVMIIxxFolha: TLargeintField;
    QrVMIIxxLinha: TLargeintField;
    QrVMICusFrtAvuls: TFloatField;
    QrVMICusFrtMOEnv: TFloatField;
    QrVMICusFrtMORet: TFloatField;
    QrVMICustoMOPc: TFloatField;
    QrVMINO_PRD_TAM_COR: TWideStringField;
    QrVMINO_FORNECE: TWideStringField;
    QrVMINO_ClientMO: TWideStringField;
    QrVMINO_SerieFch: TWideStringField;
    QrVMINO_Pallet: TWideStringField;
    frxDSVMI: TfrxDBDataset;
    QrPesq: TMySQLQuery;
    QrPesqPallet: TIntegerField;
    QrPesqPecas: TFloatField;
    QrPesqPesoKg: TFloatField;
    QrPesqGraGruX: TIntegerField;
    QrPesqSeqSifDipoa: TIntegerField;
    QrPesqObsSifDipoa: TWideStringField;
    QrPesqNO_EMPRESA: TWideStringField;
    QrPesqNO_CLISTAT: TWideStringField;
    QrPesqNO_PRD_TAM_COR: TWideStringField;
    QrPesqNO_STATUS: TWideStringField;
    QrPesqArtigoImp: TWideStringField;
    QrPesqDataFab: TDateField;
    QrPesqMARCAS: TWideMemoField;
    QrPesqMIN_DataHora: TDateTimeField;
    QrPesqMAX_DataHora: TDateTimeField;
    PMImprime: TPopupMenu;
    Selecionados1: TMenuItem;
    odosdapesquisa1: TMenuItem;
    Itematual1: TMenuItem;
    RGModelo: TRadioGroup;
    Label1: TLabel;
    EdPallets: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure CkDataIniClick(Sender: TObject);
    procedure CkDataFimClick(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure frxSIF_DIPOA_003_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrVSPalletAfterScroll(DataSet: TDataSet);
    procedure odosdapesquisa1Click(Sender: TObject);
    procedure Itematual1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
  private
    { Private declarations }
    FEnderecoLinha1, FEnderecoLinha2, FEnderecoLinha3: String;
    //
    procedure ReopenGraGruX();
    procedure FechaPesquisa();
    procedure Imprime(Quais: TSelType);
  public
    { Public declarations }
  end;

  var
  FmVSSifDipoaImp: TFmVSSifDipoaImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkProcFunc, UnVS_CRC_PF,
  ModVS;

{$R *.DFM}

procedure TFmVSSifDipoaImp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, BtImprime);
end;

procedure TFmVSSifDipoaImp.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(DBGBags, False);
end;

procedure TFmVSSifDipoaImp.BtPesquisaClick(Sender: TObject);
var
  Empresa, GraGruX: Integer;
  SQL_Periodo, SQL_Pallets, SQL_GraGruX, SQL_Especificos, Corda1: String;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  SQL_Pallets := '';
  SQL_GraGruX := '';
  SQL_Especificos := '';
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND let.GraGruX=' + Geral.FF0(GraGruX);
  if Trim(EdPallets.Text) <> EmptyStr then
    SQL_Especificos := 'AND let.Codigo IN (' + Trim(EdPallets.Text) + ')';
  //
  if (CkDataIni.Checked = True) or (CkDataFim.Checked = True) then
  begin
    SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPals, Dmod.MyDB, [
    'SELECT DISTINCT vmi.Pallet   ',
    'FROM vsmovits vmi   ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle   ',
    'WHERE cou.UsaSifDipoa=1 ',
    'AND vmi.Pallet<>0',
    'AND vmi.Empresa=' + Geral.FF0(Empresa),
    SQL_Periodo,
    '']);
    Corda1 := MyObjects.CordaDeQuery(QrPals, 'Pallet', '');
    //
    if Corda1 <> EmptyStr then
      SQL_Pallets := 'AND let.Codigo IN (' + Corda1 + ')';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT cou.ArtigoImp, vmi.Pallet, ',
  'GROUP_CONCAT(DISTINCT Marca ORDER BY Marca ASC SEPARATOR ", ") MARCAS, ',
  'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, let.GraGruX, ',
  'MIN(vmi.DataHora) MIN_DataHora, MAX(vmi.DataHora) MAX_DataHora, ',
  'let.SeqSifDipoa, let.DataFab, let.ObsSifDipoa,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
  ' CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, vps.Nome NO_STATUS  ',
  'FROM vsmovits vmi',
  'LEFT JOIN vspalleta let ON let.Codigo=vmi.Pallet  ',
  'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   ',
  'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle',
  'WHERE vmi.Pallet > 0 ',
  'AND cou.UsaSifDipoa=1 ',
  'AND let.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  SQL_Pallets,
  SQL_Especificos,
  'GROUP BY vmi.Pallet ',
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
  //Geral.MB_Teste(QrPesq.SQL.Text);
end;

procedure TFmVSSifDipoaImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSSifDipoaImp.BtTudoClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(DBGBags, True);
end;

procedure TFmVSSifDipoaImp.CkDataFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.CkDataIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.EdEmpresaRedefinido(Sender: TObject);
var
  Empresa: Integer;
begin
  FechaPesquisa();
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DmModVS.ReopenVSSifDipoa(Empresa);
  DModG.ObtemEnderecoEtiqueta3Linhas(Empresa,
    FEnderecoLinha1, FEnderecoLinha2, FEnderecoLinha3);
end;

procedure TFmVSSifDipoaImp.EdGraGruXRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.FechaPesquisa();
begin
  QrPesq.Close;
end;

procedure TFmVSSifDipoaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSSifDipoaImp.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  Data := DModG.ObtemAgora();
  TPDataIni.Date := Data - 30;
  TPDataFim.Date := Data + 3;
  //
  ReopenGraGruX();
  //
  frxDsVsSifDipoa.DataSet := DmModVS.QrVsSifDipoa;
end;

procedure TFmVSSifDipoaImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSSifDipoaImp.frxSIF_DIPOA_003_AGetValue(const VarName: string;
  var Value: Variant);
var
  Dds: Integer;
begin
  if VarName = 'VARF_EMPRESA_CNPJ_CPF' then
    Value := DModG.QrEmpresasCNPJ_CPF.Value
  else
  if VarName = 'VARF_EMPRESA_NOMEFILIAL' then
    Value := DModG.QrEmpresasNOMEFILIAL.Value
  else
  if VarName = 'VARF_NOME_ARTIGO' then
  begin
    if Trim(QrVSPalletArtigoImp.Value) <> EmptyStr then
      Value := QrVSPalletArtigoImp.Value
    else
      Value := QrVSPalletNO_PRD_TAM_COR.Value;
  end else
  if VarName = 'VARF_DATA_PRODUCAO' then
  begin
    case DmModVS.QrVsSifDipoaQualDtFab.Value of
      1: Value := '';
      2: Value := Geral.FDT(QrVSPalletMIN_DataHora.Value, 2);
      3: Value := Geral.FDT(QrVSPalletMAX_DataHora.Value, 2);
      4: Value := Geral.FDT(QrVSPalletDataFab.Value, 2);
      else Value := '';
    end;
  end else
  if VarName = 'VARF_DATA_VALIDADE' then
  begin
    Dds := DmModVS.QrVsSifDipoaDdValid.Value;
    case DmModVS.QrVsSifDipoaQualDtFab.Value of
      1: Value := '';
      2: Value := Geral.FDT(Dds + QrVSPalletMIN_DataHora.Value, 2);
      3: Value := Geral.FDT(Dds + QrVSPalletMAX_DataHora.Value, 2);
      4: Value := Geral.FDT(Dds + QrVSPalletDataFab.Value, 2);
      else Value := '';
    end;
  end else
  if VarName = 'VARF_PESO' then
    Value := Geral.FFT(QrVSPalletPesoKg.Value, 0, siPositivo) + ' Kg'
  else
  if VarName = 'VARF_LOTES' then
    Value := QrVSPalletMARCAS.Value
  else
  if VarName = 'VARF_ENDERECO' then
    Value := FEnderecoLinha1 + sLineBreak +
             FEnderecoLinha2 + sLineBreak +
             FEnderecoLinha3
  else
  if VarName = 'VARF_LogoFilial_Find' then
    Value := FileExists(DmModVS.QrVSSifDipoaArqLogoFilial.Value)
  else
  if VarName = 'VARF_LogoFilial_Path' then
    Value := DmModVS.QrVSSifDipoaArqLogoFilial.Value
  else
  if VarName = 'VARF_LogoSifDipoa_Find' then
    Value := FileExists(DmModVS.QrVSSifDipoaArqLogoSIF.Value)
  else
  if VarName = 'VARF_LogoSifDipoa_Path' then
    Value := DmModVS.QrVSSifDipoaArqLogoSIF.Value
  else

end;

procedure TFmVSSifDipoaImp.Imprime(Quais: TSelType);
const
  sProcName = 'TFmVSSifDipoaImp.Imprime()';
var
  Corda: String;
begin
  if MyObjects.FIC(DmModVS.QrVSSifDipoa.RecordCount = 0, nil,
  'Op��es SIF/DIPOA n�o definidos para a empresa selecionada!' + sLineBreak +
  'V� em Op��es -> Op��es SIF DIPOA e defina antes de imprimir.') then Exit;
  //
  if MyObjects.FIC(RGModelo.ItemIndex < 1, RGModelo,
  'Informe o modelo de impress�o!') then Exit;
  //
  Corda := EmptyStr;
  case Quais of
    istAtual: Corda := Geral.FF0(QrPesqPallet.Value);
    istSelecionados: Corda := MyObjects.CordaDeDBGridSelectedRows(DBGBags, QrPesq, 'Pallet', True);
    istTodos: Corda := MyObjects.CordaDeQuery(QrPesq, 'Pallet', '');
    else
    begin
     Geral.MB_Erro('"Quais imprime" n�o implementado em ' + sProcName);
     Exit;
    end;
  end;
  if Corda <> EmptyStr then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT cou.ArtigoImp, vmi.Pallet, ',
    'GROUP_CONCAT(DISTINCT Marca ORDER BY Marca ASC SEPARATOR ", ") MARCAS, ',
    'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, let.GraGruX, ',
    'MIN(vmi.DataHora) MIN_DataHora, MAX(vmi.DataHora) MAX_DataHora, ',
    'let.SeqSifDipoa, let.DataFab, let.ObsSifDipoa,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS  ',
    'FROM vsmovits vmi',
    'LEFT JOIN vspalleta let ON let.Codigo=vmi.Pallet  ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle',
    'WHERE vmi.Pallet > 0 ',
    'AND vmi.Pallet IN (' + Corda + ')',
    'GROUP BY vmi.Pallet ',
    'ORDER BY NO_PRD_TAM_COR ',
    '']);
    //Geral.MB_Teste(QrVSPallet.SQL.Text);
    //
    MyObjects.frxDefineDataSets(frxSIF_DIPOA_003_A, [
      frxDsVSPallet,
      frxDsVsSifDipoa,
      frxDSVMI
    ]);
    MyObjects.frxMostra(frxSIF_DIPOA_003_A, 'R�tulos de rastreabilidade');
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSSifDipoaImp.Itematual1Click(Sender: TObject);
begin
  Imprime(istAtual);
end;

procedure TFmVSSifDipoaImp.odosdapesquisa1Click(Sender: TObject);
begin
  Imprime(istTodos);
end;

procedure TFmVSSifDipoaImp.QrVSPalletAfterScroll(DataSet: TDataSet);
const
  TemIMEIMrt = 1;
(*
procedure TUnProjGroup_PF.ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_CMO(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_CMO(),
  'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Pallet=' + Geral.FF0(QrVSPalletPallet.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_Teste(QrVMI.SQL.Text);
  //
end;

procedure TFmVSSifDipoaImp.ReopenGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cou.PrevPcPal, UsaSifDipoa, ',
  'ggx.GraGruY ',
  'FROM gragrux ggx ',
  'LEFT JOIN vsribcla wmp ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'WHERE cou.UsaSifDipoa=1 ',
  'ORDER BY ggx.GragruY DESC, NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSSifDipoaImp.Selecionados1Click(Sender: TObject);
begin
  Imprime(istSelecionados);
end;

procedure TFmVSSifDipoaImp.TPDataFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.TPDataFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.TPDataIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSSifDipoaImp.TPDataIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
