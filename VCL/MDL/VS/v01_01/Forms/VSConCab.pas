unit VSConCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects,
  UnProjGroup_Consts, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSConCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSConCab: TMySQLQuery;
    DsVSConCab: TDataSource;
    QrVSConAtu: TMySQLQuery;
    DsVSConAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSConCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSConCabEmpresa: TIntegerField;
    QrVSConCabDtHrAberto: TDateTimeField;
    QrVSConCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSConCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSConOriIMEI: TMySQLQuery;
    DsVSConOriIMEI: TDataSource;
    QrVSConCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSConCabLk: TIntegerField;
    QrVSConCabDataCad: TDateField;
    QrVSConCabDataAlt: TDateField;
    QrVSConCabUserCad: TIntegerField;
    QrVSConCabUserAlt: TIntegerField;
    QrVSConCabAlterWeb: TSmallintField;
    QrVSConCabAtivo: TSmallintField;
    QrVSConCabPecasMan: TFloatField;
    QrVSConCabAreaManM2: TFloatField;
    QrVSConCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSConCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSConCabNO_TIPO: TWideStringField;
    QrVSConCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSConCabNO_DtHrLibOpe: TWideStringField;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSConCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSConCabCacCod: TIntegerField;
    QrVSConCabGraGruX: TIntegerField;
    QrVSConCabCustoManMOKg: TFloatField;
    QrVSConCabCustoManMOTot: TFloatField;
    QrVSConCabValorManMP: TFloatField;
    QrVSConCabValorManT: TFloatField;
    QrVSConCabDtHrLibOpe: TDateTimeField;
    QrVSConCabDtHrCfgOpe: TDateTimeField;
    QrVSConCabDtHrFimOpe: TDateTimeField;
    QrVSConCabPecasSrc: TFloatField;
    QrVSConCabAreaSrcM2: TFloatField;
    QrVSConCabAreaSrcP2: TFloatField;
    QrVSConCabPecasDst: TFloatField;
    QrVSConCabAreaDstM2: TFloatField;
    QrVSConCabAreaDstP2: TFloatField;
    QrVSConCabPecasSdo: TFloatField;
    QrVSConCabAreaSdoM2: TFloatField;
    QrVSConCabAreaSdoP2: TFloatField;
    QrVSConDst: TMySQLQuery;
    DsVSConDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    AtualizaestoqueEmProcesso1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSConCabPesoKgSrc: TFloatField;
    QrVSConCabPesoKgMan: TFloatField;
    QrVSConCabPesoKgDst: TFloatField;
    QrVSConCabPesoKgSdo: TFloatField;
    QrVSConCabValorTMan: TFloatField;
    QrVSConCabValorTSrc: TFloatField;
    QrVSConCabValorTSdo: TFloatField;
    QrVSConCabPecasINI: TFloatField;
    QrVSConCabAreaINIM2: TFloatField;
    QrVSConCabAreaINIP2: TFloatField;
    QrVSConCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSConCabPesoKgBxa: TFloatField;
    QrVSConCabPecasBxa: TFloatField;
    QrVSConCabAreaBxaM2: TFloatField;
    QrVSConCabAreaBxaP2: TFloatField;
    QrVSConCabValorTBxa: TFloatField;
    PCConOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSConOriPallet: TMySQLQuery;
    DsVSConOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    otal2: TMenuItem;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdCustoMOM2: TdmkEdit;
    Label48: TLabel;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label51: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label52: TLabel;
    DBEdit41: TDBEdit;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrVSConCabCliente: TIntegerField;
    QrVSConCabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrVSConCabNFeRem: TIntegerField;
    QrVSConCabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrVSConCabTemIMEIMrt: TIntegerField;
    QrVSConAtuCodigo: TLargeintField;
    QrVSConAtuControle: TLargeintField;
    QrVSConAtuMovimCod: TLargeintField;
    QrVSConAtuMovimNiv: TLargeintField;
    QrVSConAtuMovimTwn: TLargeintField;
    QrVSConAtuEmpresa: TLargeintField;
    QrVSConAtuTerceiro: TLargeintField;
    QrVSConAtuCliVenda: TLargeintField;
    QrVSConAtuMovimID: TLargeintField;
    QrVSConAtuDataHora: TDateTimeField;
    QrVSConAtuPallet: TLargeintField;
    QrVSConAtuGraGruX: TLargeintField;
    QrVSConAtuPecas: TFloatField;
    QrVSConAtuPesoKg: TFloatField;
    QrVSConAtuAreaM2: TFloatField;
    QrVSConAtuAreaP2: TFloatField;
    QrVSConAtuValorT: TFloatField;
    QrVSConAtuSrcMovID: TLargeintField;
    QrVSConAtuSrcNivel1: TLargeintField;
    QrVSConAtuSrcNivel2: TLargeintField;
    QrVSConAtuSrcGGX: TLargeintField;
    QrVSConAtuSdoVrtPeca: TFloatField;
    QrVSConAtuSdoVrtPeso: TFloatField;
    QrVSConAtuSdoVrtArM2: TFloatField;
    QrVSConAtuObserv: TWideStringField;
    QrVSConAtuSerieFch: TLargeintField;
    QrVSConAtuFicha: TLargeintField;
    QrVSConAtuMisturou: TLargeintField;
    QrVSConAtuFornecMO: TLargeintField;
    QrVSConAtuCustoMOKg: TFloatField;
    QrVSConAtuCustoMOM2: TFloatField;
    QrVSConAtuCustoMOTot: TFloatField;
    QrVSConAtuValorMP: TFloatField;
    QrVSConAtuDstMovID: TLargeintField;
    QrVSConAtuDstNivel1: TLargeintField;
    QrVSConAtuDstNivel2: TLargeintField;
    QrVSConAtuDstGGX: TLargeintField;
    QrVSConAtuQtdGerPeca: TFloatField;
    QrVSConAtuQtdGerPeso: TFloatField;
    QrVSConAtuQtdGerArM2: TFloatField;
    QrVSConAtuQtdGerArP2: TFloatField;
    QrVSConAtuQtdAntPeca: TFloatField;
    QrVSConAtuQtdAntPeso: TFloatField;
    QrVSConAtuQtdAntArM2: TFloatField;
    QrVSConAtuQtdAntArP2: TFloatField;
    QrVSConAtuNotaMPAG: TFloatField;
    QrVSConAtuNO_PALLET: TWideStringField;
    QrVSConAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSConAtuNO_TTW: TWideStringField;
    QrVSConAtuID_TTW: TLargeintField;
    QrVSConAtuNO_FORNECE: TWideStringField;
    QrVSConAtuReqMovEstq: TLargeintField;
    QrVSConAtuCUSTO_M2: TFloatField;
    QrVSConAtuCUSTO_P2: TFloatField;
    QrVSConAtuNO_LOC_CEN: TWideStringField;
    QrVSConAtuMarca: TWideStringField;
    QrVSConAtuPedItsLib: TLargeintField;
    QrVSConAtuStqCenLoc: TLargeintField;
    QrVSConAtuNO_FICHA: TWideStringField;
    QrVSConOriIMEICodigo: TLargeintField;
    QrVSConOriIMEIControle: TLargeintField;
    QrVSConOriIMEIMovimCod: TLargeintField;
    QrVSConOriIMEIMovimNiv: TLargeintField;
    QrVSConOriIMEIMovimTwn: TLargeintField;
    QrVSConOriIMEIEmpresa: TLargeintField;
    QrVSConOriIMEITerceiro: TLargeintField;
    QrVSConOriIMEICliVenda: TLargeintField;
    QrVSConOriIMEIMovimID: TLargeintField;
    QrVSConOriIMEIDataHora: TDateTimeField;
    QrVSConOriIMEIPallet: TLargeintField;
    QrVSConOriIMEIGraGruX: TLargeintField;
    QrVSConOriIMEIPecas: TFloatField;
    QrVSConOriIMEIPesoKg: TFloatField;
    QrVSConOriIMEIAreaM2: TFloatField;
    QrVSConOriIMEIAreaP2: TFloatField;
    QrVSConOriIMEIValorT: TFloatField;
    QrVSConOriIMEISrcMovID: TLargeintField;
    QrVSConOriIMEISrcNivel1: TLargeintField;
    QrVSConOriIMEISrcNivel2: TLargeintField;
    QrVSConOriIMEISrcGGX: TLargeintField;
    QrVSConOriIMEISdoVrtPeca: TFloatField;
    QrVSConOriIMEISdoVrtPeso: TFloatField;
    QrVSConOriIMEISdoVrtArM2: TFloatField;
    QrVSConOriIMEIObserv: TWideStringField;
    QrVSConOriIMEISerieFch: TLargeintField;
    QrVSConOriIMEIFicha: TLargeintField;
    QrVSConOriIMEIMisturou: TLargeintField;
    QrVSConOriIMEIFornecMO: TLargeintField;
    QrVSConOriIMEICustoMOKg: TFloatField;
    QrVSConOriIMEICustoMOM2: TFloatField;
    QrVSConOriIMEICustoMOTot: TFloatField;
    QrVSConOriIMEIValorMP: TFloatField;
    QrVSConOriIMEIDstMovID: TLargeintField;
    QrVSConOriIMEIDstNivel1: TLargeintField;
    QrVSConOriIMEIDstNivel2: TLargeintField;
    QrVSConOriIMEIDstGGX: TLargeintField;
    QrVSConOriIMEIQtdGerPeca: TFloatField;
    QrVSConOriIMEIQtdGerPeso: TFloatField;
    QrVSConOriIMEIQtdGerArM2: TFloatField;
    QrVSConOriIMEIQtdGerArP2: TFloatField;
    QrVSConOriIMEIQtdAntPeca: TFloatField;
    QrVSConOriIMEIQtdAntPeso: TFloatField;
    QrVSConOriIMEIQtdAntArM2: TFloatField;
    QrVSConOriIMEIQtdAntArP2: TFloatField;
    QrVSConOriIMEINotaMPAG: TFloatField;
    QrVSConOriIMEINO_PALLET: TWideStringField;
    QrVSConOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSConOriIMEINO_TTW: TWideStringField;
    QrVSConOriIMEIID_TTW: TLargeintField;
    QrVSConOriIMEINO_FORNECE: TWideStringField;
    QrVSConOriIMEINO_SerieFch: TWideStringField;
    QrVSConOriIMEIReqMovEstq: TLargeintField;
    QrVSConOriPalletPallet: TLargeintField;
    QrVSConOriPalletGraGruX: TLargeintField;
    QrVSConOriPalletPecas: TFloatField;
    QrVSConOriPalletAreaM2: TFloatField;
    QrVSConOriPalletAreaP2: TFloatField;
    QrVSConOriPalletPesoKg: TFloatField;
    QrVSConOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSConOriPalletNO_Pallet: TWideStringField;
    QrVSConOriPalletSerieFch: TLargeintField;
    QrVSConOriPalletFicha: TLargeintField;
    QrVSConOriPalletNO_TTW: TWideStringField;
    QrVSConOriPalletID_TTW: TLargeintField;
    QrVSConOriPalletSeries_E_Fichas: TWideStringField;
    QrVSConOriPalletTerceiro: TLargeintField;
    QrVSConOriPalletMarca: TWideStringField;
    QrVSConOriPalletNO_FORNECE: TWideStringField;
    QrVSConOriPalletNO_SerieFch: TWideStringField;
    QrVSConOriPalletValorT: TFloatField;
    QrVSConOriPalletSdoVrtPeca: TFloatField;
    QrVSConOriPalletSdoVrtPeso: TFloatField;
    QrVSConOriPalletSdoVrtArM2: TFloatField;
    QrVSConDstCodigo: TLargeintField;
    QrVSConDstControle: TLargeintField;
    QrVSConDstMovimCod: TLargeintField;
    QrVSConDstMovimNiv: TLargeintField;
    QrVSConDstMovimTwn: TLargeintField;
    QrVSConDstEmpresa: TLargeintField;
    QrVSConDstTerceiro: TLargeintField;
    QrVSConDstCliVenda: TLargeintField;
    QrVSConDstMovimID: TLargeintField;
    QrVSConDstDataHora: TDateTimeField;
    QrVSConDstPallet: TLargeintField;
    QrVSConDstGraGruX: TLargeintField;
    QrVSConDstPecas: TFloatField;
    QrVSConDstPesoKg: TFloatField;
    QrVSConDstAreaM2: TFloatField;
    QrVSConDstAreaP2: TFloatField;
    QrVSConDstValorT: TFloatField;
    QrVSConDstSrcMovID: TLargeintField;
    QrVSConDstSrcNivel1: TLargeintField;
    QrVSConDstSrcNivel2: TLargeintField;
    QrVSConDstSrcGGX: TLargeintField;
    QrVSConDstSdoVrtPeca: TFloatField;
    QrVSConDstSdoVrtPeso: TFloatField;
    QrVSConDstSdoVrtArM2: TFloatField;
    QrVSConDstObserv: TWideStringField;
    QrVSConDstSerieFch: TLargeintField;
    QrVSConDstFicha: TLargeintField;
    QrVSConDstMisturou: TLargeintField;
    QrVSConDstFornecMO: TLargeintField;
    QrVSConDstCustoMOKg: TFloatField;
    QrVSConDstCustoMOM2: TFloatField;
    QrVSConDstCustoMOTot: TFloatField;
    QrVSConDstValorMP: TFloatField;
    QrVSConDstDstMovID: TLargeintField;
    QrVSConDstDstNivel1: TLargeintField;
    QrVSConDstDstNivel2: TLargeintField;
    QrVSConDstDstGGX: TLargeintField;
    QrVSConDstQtdGerPeca: TFloatField;
    QrVSConDstQtdGerPeso: TFloatField;
    QrVSConDstQtdGerArM2: TFloatField;
    QrVSConDstQtdGerArP2: TFloatField;
    QrVSConDstQtdAntPeca: TFloatField;
    QrVSConDstQtdAntPeso: TFloatField;
    QrVSConDstQtdAntArM2: TFloatField;
    QrVSConDstQtdAntArP2: TFloatField;
    QrVSConDstNotaMPAG: TFloatField;
    QrVSConDstNO_PALLET: TWideStringField;
    QrVSConDstNO_PRD_TAM_COR: TWideStringField;
    QrVSConDstNO_TTW: TWideStringField;
    QrVSConDstID_TTW: TLargeintField;
    QrVSConDstNO_FORNECE: TWideStringField;
    QrVSConDstNO_SerieFch: TWideStringField;
    QrVSConDstReqMovEstq: TLargeintField;
    QrVSConDstPedItsFin: TLargeintField;
    QrVSConDstMarca: TWideStringField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    Label60: TLabel;
    DBEdit45: TDBEdit;
    N4: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    QrVSConAtuNO_FORNEC_MO: TWideStringField;
    Localdoestoque1: TMenuItem;
    Cliente1: TMenuItem;
    QrVSConDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSConAtuClientMO: TLargeintField;
    EmitePesagem1: TMenuItem;
    N5: TMenuItem;
    BtPesagem: TBitBtn;
    QrVSConOriIMEICustoPQ: TFloatField;
    TabSheet3: TTabSheet;
    QrEmit: TmySQLQuery;
    DsEmit: TDataSource;
    DBGrid4: TDBGrid;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    PB1: TProgressBar;
    QrVSConAtuCUSTO_KG: TFloatField;
    Label31: TLabel;
    DBEdit24: TDBEdit;
    QrVSConAtuCustoPQ: TFloatField;
    QrVSConDstNO_MovimNiv: TWideStringField;
    QrVSConDstNO_MovimID: TWideStringField;
    BtSubProduto: TBitBtn;
    PMSubProduto: TPopupMenu;
    IncluiSubProduto1: TMenuItem;
    AlteraSubProduto1: TMenuItem;
    ExcluiSubProduto1: TMenuItem;
    QrVSSubPrdIts: TmySQLQuery;
    QrVSSubPrdItsCodigo: TLargeintField;
    QrVSSubPrdItsControle: TLargeintField;
    QrVSSubPrdItsMovimCod: TLargeintField;
    QrVSSubPrdItsMovimNiv: TLargeintField;
    QrVSSubPrdItsMovimTwn: TLargeintField;
    QrVSSubPrdItsEmpresa: TLargeintField;
    QrVSSubPrdItsTerceiro: TLargeintField;
    QrVSSubPrdItsCliVenda: TLargeintField;
    QrVSSubPrdItsMovimID: TLargeintField;
    QrVSSubPrdItsDataHora: TDateTimeField;
    QrVSSubPrdItsPallet: TLargeintField;
    QrVSSubPrdItsGraGruX: TLargeintField;
    QrVSSubPrdItsPecas: TFloatField;
    QrVSSubPrdItsPesoKg: TFloatField;
    QrVSSubPrdItsAreaM2: TFloatField;
    QrVSSubPrdItsAreaP2: TFloatField;
    QrVSSubPrdItsValorT: TFloatField;
    QrVSSubPrdItsSrcMovID: TLargeintField;
    QrVSSubPrdItsSrcNivel1: TLargeintField;
    QrVSSubPrdItsSrcNivel2: TLargeintField;
    QrVSSubPrdItsSrcGGX: TLargeintField;
    QrVSSubPrdItsSdoVrtPeca: TFloatField;
    QrVSSubPrdItsSdoVrtPeso: TFloatField;
    QrVSSubPrdItsSdoVrtArM2: TFloatField;
    QrVSSubPrdItsObserv: TWideStringField;
    QrVSSubPrdItsSerieFch: TLargeintField;
    QrVSSubPrdItsFicha: TLargeintField;
    QrVSSubPrdItsMisturou: TLargeintField;
    QrVSSubPrdItsFornecMO: TLargeintField;
    QrVSSubPrdItsCustoMOKg: TFloatField;
    QrVSSubPrdItsCustoMOM2: TFloatField;
    QrVSSubPrdItsCustoMOTot: TFloatField;
    QrVSSubPrdItsValorMP: TFloatField;
    QrVSSubPrdItsDstMovID: TLargeintField;
    QrVSSubPrdItsDstNivel1: TLargeintField;
    QrVSSubPrdItsDstNivel2: TLargeintField;
    QrVSSubPrdItsDstGGX: TLargeintField;
    QrVSSubPrdItsQtdGerPeca: TFloatField;
    QrVSSubPrdItsQtdGerPeso: TFloatField;
    QrVSSubPrdItsQtdGerArM2: TFloatField;
    QrVSSubPrdItsQtdGerArP2: TFloatField;
    QrVSSubPrdItsQtdAntPeca: TFloatField;
    QrVSSubPrdItsQtdAntPeso: TFloatField;
    QrVSSubPrdItsQtdAntArM2: TFloatField;
    QrVSSubPrdItsQtdAntArP2: TFloatField;
    QrVSSubPrdItsNotaMPAG: TFloatField;
    QrVSSubPrdItsNO_FORNECE: TWideStringField;
    QrVSSubPrdItsNO_PRD_TAM_COR: TWideStringField;
    QrVSSubPrdItsID_TTW: TLargeintField;
    QrVSSubPrdItsNO_TTW: TWideStringField;
    DsVSSubPrdIts: TDataSource;
    DBGrid1: TDBGrid;
    QrVSConOriIMEIVSMulFrnCab: TLargeintField;
    QrVSConOriIMEIClientMO: TLargeintField;
    IncluiSubProdutorateandoentretodasorigens1: TMenuItem;
    CourosemprocessoBH1: TMenuItem;
    Label57: TLabel;
    EdSerieRem: TdmkEdit;
    Label59: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    QrVSConCabSerieRem: TSmallintField;
    irparajaneladomovimento1: TMenuItem;
    Corrigepesodebaixa1: TMenuItem;
    PMPesagem: TPopupMenu;
    EmitePesagem2: TMenuItem;
    MenuItem1: TMenuItem;
    Reimprimereceita1: TMenuItem;
    N8: TMenuItem;
    ExcluiPesagem1: TMenuItem;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    N7: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    Recalculacusto1: TMenuItem;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    TabSheet4: TTabSheet;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    QrVSConCabEmitGru: TIntegerField;
    QrVSConCabValorTDst: TFloatField;
    QrVSConCabKgCouPQ: TFloatField;
    QrVSConCabGGXDst: TIntegerField;
    QrVSConCabNFeStatus: TIntegerField;
    QrVSConCabGGXSrc: TIntegerField;
    QrVSConCabOperacoes: TIntegerField;
    QrVSConCabVSCOPCab: TIntegerField;
    QrVSConOriIMEIRmsMovID: TLargeintField;
    QrVSConOriIMEIRmsNivel1: TLargeintField;
    QrVSConOriIMEIRmsNivel2: TLargeintField;
    ItsExcluiOriIMEI2: TMenuItem;
    QrEmitDtCorrApo_TXT: TWideStringField;
    QrEmitDtaBaixa: TDateField;
    QrEmitDtaBaixa_TXT: TWideStringField;
    QrEmitDtCorrApo: TDateTimeField;
    QrVSConInd: TMySQLQuery;
    QrVSConIndCodigo: TLargeintField;
    QrVSConIndControle: TLargeintField;
    QrVSConIndMovimCod: TLargeintField;
    QrVSConIndMovimNiv: TLargeintField;
    QrVSConIndMovimTwn: TLargeintField;
    QrVSConIndEmpresa: TLargeintField;
    QrVSConIndTerceiro: TLargeintField;
    QrVSConIndCliVenda: TLargeintField;
    QrVSConIndMovimID: TLargeintField;
    QrVSConIndDataHora: TDateTimeField;
    QrVSConIndPallet: TLargeintField;
    QrVSConIndGraGruX: TLargeintField;
    QrVSConIndPecas: TFloatField;
    QrVSConIndPesoKg: TFloatField;
    QrVSConIndAreaM2: TFloatField;
    QrVSConIndAreaP2: TFloatField;
    QrVSConIndValorT: TFloatField;
    QrVSConIndSrcMovID: TLargeintField;
    QrVSConIndSrcNivel1: TLargeintField;
    QrVSConIndSrcNivel2: TLargeintField;
    QrVSConIndSrcGGX: TLargeintField;
    QrVSConIndSdoVrtPeca: TFloatField;
    QrVSConIndSdoVrtPeso: TFloatField;
    QrVSConIndSdoVrtArM2: TFloatField;
    QrVSConIndObserv: TWideStringField;
    QrVSConIndSerieFch: TLargeintField;
    QrVSConIndFicha: TLargeintField;
    QrVSConIndMisturou: TLargeintField;
    QrVSConIndFornecMO: TLargeintField;
    QrVSConIndCustoMOKg: TFloatField;
    QrVSConIndCustoMOM2: TFloatField;
    QrVSConIndCustoMOTot: TFloatField;
    QrVSConIndValorMP: TFloatField;
    QrVSConIndDstMovID: TLargeintField;
    QrVSConIndDstNivel1: TLargeintField;
    QrVSConIndDstNivel2: TLargeintField;
    QrVSConIndDstGGX: TLargeintField;
    QrVSConIndQtdGerPeca: TFloatField;
    QrVSConIndQtdGerPeso: TFloatField;
    QrVSConIndQtdGerArM2: TFloatField;
    QrVSConIndQtdGerArP2: TFloatField;
    QrVSConIndQtdAntPeca: TFloatField;
    QrVSConIndQtdAntPeso: TFloatField;
    QrVSConIndQtdAntArM2: TFloatField;
    QrVSConIndQtdAntArP2: TFloatField;
    QrVSConIndNotaMPAG: TFloatField;
    QrVSConIndNO_PALLET: TWideStringField;
    QrVSConIndNO_PRD_TAM_COR: TWideStringField;
    QrVSConIndNO_TTW: TWideStringField;
    QrVSConIndID_TTW: TLargeintField;
    QrVSConIndNO_FORNECE: TWideStringField;
    QrVSConIndNO_SerieFch: TWideStringField;
    QrVSConIndReqMovEstq: TLargeintField;
    QrVSConIndPedItsFin: TLargeintField;
    QrVSConIndMarca: TWideStringField;
    QrVSConIndStqCenLoc: TLargeintField;
    QrVSConIndNO_MovimNiv: TWideStringField;
    QrVSConIndNO_MovimID: TWideStringField;
    DsVSConInd: TDataSource;
    DBGrid5: TDBGrid;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    SbStqCenLoc: TSpeedButton;
    Datadeabertura1: TMenuItem;
    QrVSConOriIMEIMarca: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSConCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSConCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSConCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSConCabBeforeClose(DataSet: TDataSet);
    procedure QrVSConCabCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmProcesso1Click(Sender: TObject);
    procedure QrVSConDstBeforeClose(DataSet: TDataSet);
    procedure QrVSConDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure otal2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Localdoestoque1Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure EmitePesagem1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure Reimprimereceita1Click(Sender: TObject);
    procedure ExcluiPesagem1Click(Sender: TObject);
    procedure BtSubProdutoClick(Sender: TObject);
    procedure PMSubProdutoPopup(Sender: TObject);
    procedure IncluiSubProduto1Click(Sender: TObject);
    procedure AlteraSubProduto1Click(Sender: TObject);
    procedure ExcluiSubProduto1Click(Sender: TObject);
    procedure QrVSConOriIMEIAfterScroll(DataSet: TDataSet);
    procedure QrVSConOriIMEIBeforeClose(DataSet: TDataSet);
    procedure IncluiSubProdutorateandoentretodasorigens1Click(Sender: TObject);
    procedure CourosemprocessoBH1Click(Sender: TObject);
    procedure irparajaneladomovimento1Click(Sender: TObject);
    procedure Corrigepesodebaixa1Click(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure ItsExcluiOriIMEI2Click(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure Datadeabertura1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSConOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSConOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSConDst(SQLType: TSQLType);
    procedure InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX, CliVenda: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSConOris(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
    procedure PesquisaArtigoJaFeito(Key: Word);
    procedure ReopenVSSubPrdIts(Controle: Integer);
    procedure RecalculaCusto();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEmit();
  end;

var
  FmVSConCab: TFmVSConCab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, ModuleGeral, VSConDst, UnVS_PF, UnVS_CRC_PF,
  VSConOriIMEI, AppListas, UnGrade_PF, ModVS, UnPQ_PF, UnEntities, ModVS_CRC,
  UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSConCab.Localdoestoque1Click(Sender: TObject);
begin
  if VS_PF.AlteraVMI_StqCenLoc(QrVSConAtuControle.Value,
  QrVSConAtuStqCenLoc.Value) then
    LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSConCab.MostraVSConDst(SQLType: TSQLType);
  function CalculoValorM2(): Double;
  begin
    if QrVSConAtuAreaM2.Value > 0 then
      Result := QrVSConAtuValorT.Value / QrVSConAtuAreaM2.Value
    else
      Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmVSConDst, FmVSConDst, afmoNegarComAviso) then
  begin
    FmVSConDst.ImgTipo.SQLType := SQLType;
    FmVSConDst.FQrCab := QrVSConCab;
    FmVSConDst.FDsCab := DsVSConCab;
    FmVSConDst.FQrIts := QrVSConDst;
    //
    //FmVSConDst.FDataHora    := QrVSConCabDtHrAberto.Value;
    FmVSConDst.FEmpresa     := QrVSConCabEmpresa.Value;
    FmVSConDst.FClientMO    := QrVSConAtuClientMO.Value;
    FmVSConDst.FFornecMO    := QrVSConAtuFornecMO.Value;
    FmVSConDst.FValM2       := CalculoValorM2();
    //FmVSConDst.FFatorIntSrc := QrVSConAtuFatorInt.Value;
    FmVSConDst.FGraGruXSrc  := QrVSConAtuGraGruX.Value;
    //
    FmVSConDst.FOrigMovID     := QrVSConOriIMEIMovimID.Value;
    FmVSConDst.FOrigCodigo    := QrVSCOnOriIMEICodigo.Value;
    FmVSConDst.FOrigControle  := QrVSConOriIMEIControle.Value;
    FmVSConDst.FOrigGGX       := QrVSConOriIMEIGraGruX.Value;

    //
    //FmVSConDst.EdCustoMOM2.ValueVariant := QrVSConAtuCustoMOM2.Value;
    FmVSConDst.EdCustoMOKg.ValueVariant := QrVSConAtuCustoMOKg.Value;
    if SQLType = stIns then
    begin
      FmVSConDst.FSdoPecas  := QrVSConAtuSdoVrtPeca.Value;
      FmVSConDst.FSdoPesoKg := QrVSConAtuSdoVrtPeso.Value;
      FmVSConDst.FSdoReaM2  := QrVSConAtuSdoVrtArM2.Value;
      //
      if  QrVSConAtuPesoKg.Value <> 0 then
        FmVSConDst.FCustoKg := QrVSConAtuValorT.Value / QrVSConAtuPesoKg.Value
      else
        FmVSConDst.FCustoKg := 0;
      //
      if  QrVSConAtuAreaM2.Value <> 0 then
        FmVSConDst.FCustoM2 := QrVSConAtuValorT.Value / QrVSConAtuAreaM2.Value
      else
        FmVSConDst.FCustoM2 := 0;
      //
      //FmVSConDst.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSConDst.QrVSPedIts, QrVSConAtuControle.Value,
      QrVSConAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSConDst.EdPedItsFin.ValueVariant := QrVSConAtuPedItsLib.Value;
      FmVSConDst.CBPedItsFin.KeyValue     := QrVSConAtuPedItsLib.Value;
      //
      FmVSConDst.TPData.Date              := Now();
      FmVSConDst.EdHora.ValueVariant      := Now();
    end else
    begin
      FmVSConDst.FSdoPecas  := 0;
      FmVSConDst.FSdoPesoKg := 0;
      FmVSConDst.FSdoReaM2  := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSConDst.QrVSPedIts, QrVSConAtuControle.Value,
       QrVSConDstControle.Value);
      FmVSConDst.EdCtrl1.ValueVariant      := QrVSConDstControle.Value;
      //FmVSConDst.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSConDstMovimCod.Value, QrVSConDstMovimTwn.Value, emidEmProcCon,
        //eminEmConBxa);
      FmVSConDst.EdMovimTwn.ValueVariant   := QrVSConDstMovimTwn.Value;
      FmVSConDst.EdGragruX.ValueVariant    := QrVSConDstGraGruX.Value;
      FmVSConDst.CBGragruX.KeyValue        := QrVSConDstGraGruX.Value;
      FmVSConDst.EdPecas.ValueVariant      := QrVSConDstPecas.Value;
      FmVSConDst.EdPesoKg.ValueVariant     := QrVSConDstPesoKg.Value;
      FmVSConDst.EdAreaM2.ValueVariant     := QrVSConDstAreaM2.Value;
      FmVSConDst.EdAreaP2.ValueVariant     := QrVSConDstAreaP2.Value;
      FmVSConDst.EdValorT.ValueVariant     := QrVSConDstValorT.Value;
      FmVSConDst.EdObserv.ValueVariant     := QrVSConDstObserv.Value;
      FmVSConDst.EdSerieFch.ValueVariant   := QrVSConDstSerieFch.Value;
      FmVSConDst.CBSerieFch.KeyValue       := QrVSConDstSerieFch.Value;
      FmVSConDst.EdFicha.ValueVariant      := QrVSConDstFicha.Value;
      FmVSConDst.EdMarca.ValueVariant      := QrVSConDstMarca.Value;
      //FmVSConDst.RGMisturou.ItemIndex    := QrVSConDstMisturou.Value;
      FmVSConDst.EdPedItsFin.ValueVariant  := QrVSConDstPedItsFin.Value;
      FmVSConDst.CBPedItsFin.KeyValue      := QrVSConDstPedItsFin.Value;
      //
      FmVSConDst.TPData.Date               := QrVSConDstDataHora.Value;
      FmVSConDst.EdHora.ValueVariant       := QrVSConDstDataHora.Value;
      FmVSConDst.EdPedItsFin.ValueVariant  := QrVSConDstPedItsFin.Value;
      FmVSConDst.CBPedItsFin.KeyValue      := QrVSConDstPedItsFin.Value;
      FmVSConDst.EdPallet.ValueVariant     := QrVSConDstPallet.Value;
      FmVSConDst.CBPallet.KeyValue         := QrVSConDstPallet.Value;
      FmVSConDst.EdStqCenLoc.ValueVariant  := QrVSConDstStqCenLoc.Value;
      FmVSConDst.CBStqCenLoc.KeyValue      := QrVSConDstStqCenLoc.Value;
      FmVSConDst.EdReqMovEstq.ValueVariant := QrVSConDstReqMovEstq.Value;
      //FmVSConDst.EdCustoMOM2.ValueVariant  := QrVSConDstCustoMOM2.Value;
      FmVSConDst.EdCustoMOkg.ValueVariant  := QrVSConDstCustoMOKg.Value;
      FmVSConDst.EdCustoMOTot.ValueVariant := QrVSConDstCustoMOTot.Value;
      FmVSConDst.EdValorT.ValueVariant     := QrVSConDstValorT.Value;
      //FmVSConDst.EdRendimento.ValueVariant := QrVSConDstRendimento.Value;
      //
      if QrVSConDstSdoVrtPeca.Value < QrVSConDstPecas.Value then
      begin
        FmVSConDst.EdGraGruX.Enabled        := False;
        FmVSConDst.CBGraGruX.Enabled        := False;
      end;
(*
      if QrVSConBxa.RecordCount > 0 then
      begin
        FmVSConDst.CkBaixa.Checked          := True;
        FmVSConDst.EdBxaPecas.ValueVariant  := -QrVSConBxaPecas.Value;
        FmVSConDst.EdBxaPesoKg.ValueVariant := -QrVSConBxaPesoKg.Value;
        FmVSConDst.EdBxaAreaM2.ValueVariant := -QrVSConBxaAreaM2.Value;
        FmVSConDst.EdBxaAreaP2.ValueVariant := -QrVSConBxaAreaP2.Value;
        FmVSConDst.EdBxaValorT.ValueVariant := -QrVSConBxaValorT.Value;
        FmVSConDst.EdCtrl2.ValueVariant     := QrVSConBxaControle.Value;
        FmVSConDst.EdBxaObserv.ValueVariant := QrVSConBxaObserv.Value;
      end;
*)
      if QrVSConDstSdoVrtPeca.Value < QrVSConDstPecas.Value then
      begin
        FmVSConDst.EdGraGruX.Enabled  := False;
        FmVSConDst.CBGraGruX.Enabled  := False;
        FmVSConDst.EdSerieFch.Enabled := False;
        FmVSConDst.CBSerieFch.Enabled := False;
        FmVSConDst.EdFicha.Enabled    := False;
      end;
    end;
    //
    FmVSConDst.LaBxaPesoKg.Enabled  := False; //QrVSConCabPesoKgINI.Value <> 0;
    FmVSConDst.EdBxaPesoKg.Enabled  := False; //QrVSConCabPesoKgINI.Value <> 0;
    FmVSConDst.LaBxaAreaM2.Enabled  := False; //QrVSConCabAreaINIM2.Value <> 0;
    FmVSConDst.EdBxaAreaM2.Enabled  := False; //QrVSConCabAreaINIM2.Value <> 0;
    FmVSConDst.LaBxaAreaP2.Enabled  := False; //QrVSConCabAreaINIM2.Value <> 0;
    FmVSConDst.EdBxaAreaP2.Enabled  := False; //QrVSConCabAreaINIM2.Value <> 0;
    //
    FmVSConDst.ShowModal;
    FmVSConDst.Destroy;
    //
    VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSConCab.MostraVSConOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if QrVSConOriIMEI.RecordCount > 0 then
  begin
    Geral.MB_Aviso(
    '� permitido apenas um item de origem para manuten��o de rastreabilidade de artigo gerado!');
    Exit;
  end;

  if DBCheck.CriaFm(TFmVSConOriIMEI, FmVSConOriIMEI, afmoNegarComAviso) then
  begin
    FmVSConOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSConOriIMEI.FQrCab          := QrVSConCab;
    FmVSConOriIMEI.FDsCab          := DsVSConCab;
    //FmVSConOriIMEI.FVSConOriIMEIIMEI   := QrVSConOriIMEIIMEI;
    //FmVSConOriIMEI.FVSConOriIMEIIMEIet := QrVSConOriIMEIIMEIet;
    FmVSConOriIMEI.FEmpresa        := QrVSConCabEmpresa.Value;
    FmVSConOriIMEI.FTipoArea       := QrVSConCabTipoArea.Value;
    FmVSConOriIMEI.FOrigMovimNiv   := QrVSConAtuMovimNiv.Value;
    FmVSConOriIMEI.FOrigMovimCod   := QrVSConAtuMovimCod.Value;
    FmVSConOriIMEI.FOrigCodigo     := QrVSConAtuCodigo.Value;
    FmVSConOriIMEI.FNewGraGruX     := QrVSConAtuGraGruX.Value;
    //
    FmVSConOriIMEI.EdCodigo.ValueVariant    := QrVSConCabCodigo.Value;
    FmVSConOriIMEI.EdMovimCod.ValueVariant  := QrVSConCabMovimCod.Value;
    FmVSConOriIMEI.FDataHora                := QrVSConCabDtHrAberto.Value;
    //
    FmVSConOriIMEI.EdSrcMovID.ValueVariant  := QrVSConAtuMovimID.Value;
    FmVSConOriIMEI.EdSrcNivel1.ValueVariant := QrVSConAtuCodigo.Value;
    FmVSConOriIMEI.EdSrcNivel2.ValueVariant := QrVSConAtuControle.Value;
    FmVSConOriIMEI.EdSrcGGX.ValueVariant    := QrVSConAtuGraGruX.Value;
    //
    FmVSConOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSConOriIMEI.EdControle.ValueVariant := QrVSConCabOldControle.Value;
      //
      FmVSConOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSConCabOldCNPJ_CPF.Value);
      FmVSConOriIMEI.EdNomeEmiSac.Text := QrVSConCabOldNome.Value;
      FmVSConOriIMEI.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSConOriIMEI.FParcial := True;
        FmVSConOriIMEI.DBG04Estq.Options := FmVSConOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
(*    N�o pode selecionar mais de um pois tem rastreabilidade de artigo gerado!
      ptTotal:
      begin
        FmVSConOriIMEI.FParcial := False;
        FmVSConOriIMEI.DBG04Estq.Options := FmVSConOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
*)
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSConOriIMEI.FParcial := False;
        FmVSConOriIMEI.DBG04Estq.Options := FmVSConOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSConOriIMEI.ReopenItensAptos();
    FmVSConOriIMEI.ShowModal;
    FmVSConOriIMEI.Destroy;
    VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSConCab.MostraVSConOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
{
  if DBCheck.CriaFm(TFmVSConOriPall, FmVSConOriPall, afmoNegarComAviso) then
  begin
    FmVSConOriPall.ImgTipo.SQLType := SQLType;
    FmVSConOriPall.FQrCab          := QrVSConCab;
    FmVSConOriPall.FDsCab          := DsVSConCab;
    //FmVSConOriPall.FVSConOriPallIMEI   := QrVSConOriPallIMEI;
    //FmVSConOriPall.FVSConOriPallPallet := QrVSConOriPallPallet;
    FmVSConOriPall.FEmpresa        := QrVSConCabEmpresa.Value;
    FmVSConOriPall.FClientMO       := QrVSConAtuClientMO.Value;
    FmVSConOriPall.FTipoArea       := QrVSConCabTipoArea.Value;
    FmVSConOriPall.FOrigMovimNiv   := QrVSConAtuMovimNiv.Value;
    FmVSConOriPall.FOrigMovimCod   := QrVSConAtuMovimCod.Value;
    FmVSConOriPall.FOrigCodigo     := QrVSConAtuCodigo.Value;
    FmVSConOriPall.FNewGraGruX     := QrVSConAtuGraGruX.Value;
    //
    FmVSConOriPall.EdCodigo.ValueVariant    := QrVSConCabCodigo.Value;
    FmVSConOriPall.EdMovimCod.ValueVariant  := QrVSConCabMovimCod.Value;
    FmVSConOriPall.FDataHora                := QrVSConCabDtHrAberto.Value;
    //
    FmVSConOriPall.EdSrcMovID.ValueVariant  := QrVSConAtuMovimID.Value;
    FmVSConOriPall.EdSrcNivel1.ValueVariant := QrVSConAtuCodigo.Value;
    FmVSConOriPall.EdSrcNivel2.ValueVariant := QrVSConAtuControle.Value;
    FmVSConOriPall.EdSrcGGX.ValueVariant    := QrVSConAtuGraGruX.Value;
    //
    FmVSConOriPall.ReopenItensAptos();
    FmVSConOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
(*
      FmVSConOriPall.EdControle.ValueVariant := QrVSConCabOldControle.Value;
      //
      FmVSConOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSConCabOldCNPJ_CPF.Value);
      FmVSConOriPall.EdNomeEmiSac.Text := QrVSConCabOldNome.Value;
      FmVSConOriPall.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSConOriPall.FParcial := True;
        FmVSConOriPall.DBG04Estq.Options := FmVSConOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSConOriPall.FParcial := False;
        FmVSConOriPall.DBG04Estq.Options := FmVSConOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSConOriPall.FParcial := False;
        FmVSConOriPall.DBG04Estq.Options := FmVSConOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSConOriPall.ShowModal;
    FmVSConOriPall.Destroy;
    VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmVSConCab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = False;
var
  Codigo: Integer;
begin
  PCConOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSConCabMovimCod.Value,
  QrVSConCabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSConCabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
end;

procedure TFmVSConCab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
  PCConOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
end;

procedure TFmVSConCab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
  VS_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmProcCon], True, False, '', 0);
end;

procedure TFmVSConCab.otal1Click(Sender: TObject);
begin
  MostraVSConOriPall(stIns, ptTotal);
end;

procedure TFmVSConCab.otal2Click(Sender: TObject);
begin
  MostraVSConOriIMEI(stIns, ptTotal);
end;

procedure TFmVSConCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSConCab.Parcial1Click(Sender: TObject);
begin
  MostraVSConOriPall(stIns, ptParcial);
end;

procedure TFmVSConCab.Parcial2Click(Sender: TObject);
begin
  MostraVSConOriIMEI(stIns, ptParcial);
end;

procedure TFmVSConCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSConCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSConCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEC(emidEmProcCon);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSConCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEI(emidEmProcCon);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSConCab.PesquisaArtigoJaFeito(Key: Word);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  if Key = VK_F4 then
  begin
    Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCon, [eminEmConInn], stCpy,
    Codigo, Controle, False);
    //
    if (Controle <> 0) and (DmModVS.QrIMEI.State <> dsInactive) then
    begin
      EdGraGruX.ValueVariant   := DmModVS.QrIMEIGraGruX.Value;
      CBGraGruX.KeyValue       := DmModVS.QrIMEIGraGruX.Value;
      EdFornecMO.ValueVariant  := DmModVS.QrIMEIFornecMO.Value;
      CBFornecMO.KeyValue      := DmModVS.QrIMEIFornecMO.Value;
      EdStqCenLoc.ValueVariant := DmModVS.QrIMEIStqCenLoc.Value;
      CBStqCenLoc.KeyValue     := DmModVS.QrIMEIStqCenLoc.Value;
      EdCustoMOM2.ValueVariant := DmModVS.QrIMEICustoMOM2.Value;
      EdCliente.ValueVariant   := DmModVS.QrIMEICliVenda.Value;
      CBCliente.KeyValue       := DmModVS.QrIMEICliVenda.Value;
    end;
  end;
end;

procedure TFmVSConCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSConCab);
  MyObjects.HabilitaMenuItemCabDelC1I4(CabExclui1, QrVSConCab, QrVSConOriIMEI,
    QrEmit, QrVSConDst, QrVSConInd);
  DataDeAbertura1.Enabled := (QrVSConCabDtHrFimOpe.Value > 0) and
    (QrVSConCabDtHrAberto.Value > QrVSConCabDtHrFimOpe.Value);
end;

procedure TFmVSConCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSConCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSConDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSConDst);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSConDst);
  if (QrVSConCabDtHrLibOpe.Value > 2) or (QrVSConCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsAlteraDst.Enabled := False;
    //ItsExcluiDst.Enabled := False;
  end;
  VS_PF.HabilitaComposVSAtivo(QrVSConDstID_TTW.Value, [ItsExcluiDst]);
end;

procedure TFmVSConCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSConCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI2, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(Corrigepesodebaixa1, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSConOriPallet);
  if (QrVSConCabDtHrLibOpe.Value > 2) or (QrVSConCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  case PCConOri.ActivePageIndex of
    0:
    begin
      ItsExcluiOriIMEI.Enabled := False;
      Corrigepesodebaixa1.Enabled := False;
    end;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCConOri.ActivePageIndex = 1);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSConOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_PF.HabilitaComposVSAtivo(QrVSConOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  ItsExcluiOriIMEI2.Enabled := ItsExcluiOriIMEI2.Enabled and (ItsExcluiOriIMEI.Enabled = False);
end;

procedure TFmVSConCab.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSConCab);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSConCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSConCab.PMSubProdutoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSubProduto1, QrVSConOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSubProduto1, QrVSSubPrdIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSubProduto1, QrVSSubPrdIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSSubPrdItsID_TTW.Value, [AlteraSubProduto1, ExcluiSubProduto1]);
end;

procedure TFmVSConCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSConCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSConCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsconcab';
  VAR_GOTOMYSQLTABLE := QrVSConCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(PecasMan<>0, PecasMan, -PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(AreaManM2<>0, AreaManM2, -AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(AreaManP2<>0, AreaManP2, -AreaSrcM2) AreaINIP2,');
  VAR_SQLx.Add('IF(PesoKgMan<>0, PesoKgMan, -PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vsconcab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSConCab.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSConCab.EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSConCab.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSConCab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCConOri.ActivePageIndex := 3;
end;

procedure TFmVSConCab.EmitePesagem1Click(Sender: TObject);
begin
  //VAR_SETOR := CO_CALEIRO;
  PQ_PF.MostraFormVSFormulasImp_BH(QrVSConCabMovimCod.Value,
    QrVSConAtuControle.Value, QrVSConCabTemIMEIMrt.Value,
    eminEmConInn, eminSorcCon, recribsetCaleiro);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.EmitePesagem2Click(Sender: TObject);
begin
  PCConOri.ActivePageIndex := 2;
end;

procedure TFmVSConCab.Excluigrupo1Click(Sender: TObject);
begin
  PCConOri.ActivePageIndex := 3;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
end;

procedure TFmVSConCab.ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimID, MovimNiv, MovimCod: Integer;
  Qry: TmySQLQuery;
begin
  if ExigeSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  //
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrVSConCabCodigo.Value;
    CtrlDel   := QrVSConOriIMEIControle.Value;
    CtrlOri   := QrVSConOriIMEISrcNivel2.Value;
    MovimID   := QrVSConAtuMovimID.Value;
    MovimNiv  := QrVSConAtuMovimNiv.Value;
    MovimCod  := QrVSConAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti237), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsconcab', MovimCod);
      VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
      //
      if QrVSConOriIMEIRmsNivel2.Value <> 0 then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE RmsMovID=' + Geral.FF0(MovimID),
          'AND RmsNivel1=' + Geral.FF0(Codigo),
          'AND RmsNivel2=' + Geral.FF0(CtrlDel),
          '']);
          Qry.First;
          while not Qry.Eof do
          begin
            CtrlDel := Qry.FieldByName('Controle').AsInteger;
            VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti237), Dmod.QrUpd, Dmod.MyDB,
            CO_MASTER);
            //
            Qry.Next;
          end;
          //
        finally
          Qry.Free;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSConOriIMEI,
        TIntegerField(QrVSConOriIMEIControle), QrVSConOriIMEIControle.Value);
      //
      VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSConOris(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSConCab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
  PCConOri.ActivePageIndex := 3;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
end;

procedure TFmVSConCab.ExcluiPesagem1Click(Sender: TObject);
begin
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, PB1) then
    LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.ExcluiSubProduto1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  Controle := QrVSSubPrdItsControle.Value;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '',
    'UNION',
    '',
    'SELECT Controle ',
    'FROM ' + CO_TAB_VMB,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '']);
    //
    if MyObjects.FIC(Qry.RecordCount > 0, nil,
    'O item n�o pode ser exclu�do pois j� possui baixas!') then
      Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do sub produto?') <> ID_YES then
      Exit;
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti237), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSConCab.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSConCab.Cliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSConCabCodigo.Value;
  if VS_PF.AlteraVMI_CliVenda(QrVSConAtuControle.Value,
  QrVSConAtuCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsconcab', False, [
    'Cliente'], [
    'Codigo'], [
    QrVSConAtuCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSConCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSConAtuControle.Value;
  MovimCod    := QrVSConCabMovimCod.Value;
  Codigo      := QrVSConCabCodigo.Value;
  MovimNivSrc := eminSorcCon;
  MovimNivDst := eminDestCon;
  VS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSConCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSConCab.Corrigepesodebaixa1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  //
  if VS_PF.AlteraVMI_PesoKg('PesoKg', QrVSConOriIMEIMovimID.Value,
  QrVSConOriIMEIMovimNiv.Value, QrVSConOriIMEIControle.Value,
  QrVSConOriIMEIPesoKg.Value, siNegativo) then
    LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.CorrigirFornecedor1Click(Sender: TObject);
begin
  VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcCon, QrVSConCabMovimCod.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
  //
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.CourosemprocessoBH1Click(Sender: TObject);
begin
  // ini 2023-12-31
  //VS_PF.ImprimeVSEmProcBH();
  VS_Jan.MostraFormVSImpEmProcBH();
  // fim 2023-12-31
end;

procedure TFmVSConCab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSConCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSConCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSConAtuControle.Value], viikOrdemOperacao,
    QrVSConCabLPFMO.Value, NFeRem, QrVSConCab);
end;

procedure TFmVSConCab.IncluiSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  Controle   = 0;
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  VSMulFrnCab: Integer;
  Qry: TmySQLQuery;
  Marca: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(QrVSConOriIMEISrcNivel2.Value),
    '']);
    DataHora    := DModG.ObtemAgora();
    GSPSrcMovID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    GSPSrcNiv2  := Qry.FieldByName('Controle').AsInteger;
    Codigo      := 0;
    MovimCod    := 0;
    Empresa     := Qry.FieldByName('Empresa').AsInteger;
    ClientMO    := Qry.FieldByName('ClientMO').AsInteger;
    Terceiro    := Qry.FieldByName('Terceiro').AsInteger;
    VSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
    Ficha       := Qry.FieldByName('Ficha').AsInteger;
    SerieFch    := Qry.FieldByName('SerieFch').AsInteger;
    Marca       := Qry.FieldByName('Marca').AsString;
    //
    VS_PF.MostraFormVSInnSubPrdIts_Uni(stIns, DataHora, QrCab, QrVSSubPrdIts, DsCab,
    GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
    VSMulFrnCab, Ficha, SerieFch, Controle, Marca);
    //ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSConCab.IncluiSubProdutorateandoentretodasorigens1Click(
  Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  Controle   = 0;
var
  DataHora: TDateTime;
  GSPSrcMovID, GSPJmpMovID: TEstqMovimID;
  GSPSrcNiv2, GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha,
  SerieFch, VSMulFrnCab, MulOriMovimCod, TemIMEIMrt: Integer;
  Qry: TmySQLQuery;
  Marca: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(QrVSConOriIMEISrcNivel2.Value),
    '']);
    DataHora    := DModG.ObtemAgora();
    GSPSrcMovID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    GSPSrcNiv2  := Qry.FieldByName('Controle').AsInteger;
    GSPJmpMovID := TEstqMovimID(QrVSConOriIMEIMovimID.Value);
    GSPJmpNiv2  := QrVSConOriIMEIControle.Value;
    Codigo      := 0;
    MovimCod    := 0;
    Empresa     := Qry.FieldByName('Empresa').AsInteger;
    ClientMO    := Qry.FieldByName('ClientMO').AsInteger;
    Terceiro    := Qry.FieldByName('Terceiro').AsInteger;
    VSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
    Ficha       := Qry.FieldByName('Ficha').AsInteger;
    SerieFch    := Qry.FieldByName('SerieFch').AsInteger;
    Marca       := Qry.FieldByName('Marca').AsString;
    //
    MulOriMovimCod := QrVSConCabMovimCod.Value;
    TemIMEIMrt     := QrVSConCabTemIMEIMrt.Value;
    //
    VS_PF.MostraFormVSInnSubPrdIts_Mul(stIns, DataHora, QrCab, QrVSSubPrdIts,
    DsCab, GSPSrcMovID, GSPSrcNiv2, GSPJmpMovID, GSPJmpNiv2, Codigo, MovimCod,
    Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha, SerieFch, Controle,
    MulOriMovimCod, TemIMEIMrt, True, Marca);
    //ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSConCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSConOriIMEIControle.Value,
    QrVSConOriIMEIReqMovEstq.Value, QrVSConOriIMEI);
end;

procedure TFmVSConCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSConDstControle.Value,
    QrVSConDstReqMovEstq.Value, QrVSConDst);
end;

procedure TFmVSConCab.InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
GraGruX, CliVenda: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOM2, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmProcCon;
  MovimNiv       := eminEmConInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei111(*Cabe�alho de processo de conserva��o*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('vsconcab', MovimCod);
    VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
    if GBVSPedIts.Visible then
      VS_PF.AtualizaVSPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;

procedure TFmVSConCab.irparajaneladomovimento1Click(Sender: TObject);
begin
  if (QrVSConDst.State <> dsInactive) and (QrVSConDst.RecordCount > 0) then
    VS_PF.MostraFormVS_Do_IMEI(QrVSConDstControle.Value);
end;

procedure TFmVSConCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrVSConDstPallet.Value);
end;

procedure TFmVSConCab.ItsAlteraDstClick(Sender: TObject);
begin
  MostraVSConDst(stUpd);
end;

procedure TFmVSConCab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraVSGArtOri(stUpd);
end;

procedure TFmVSConCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSConCabCodigo.Value;
  VS_PF.ExcluiCabEIMEI_OpeCab(QrVSConCabMovimCod.Value,
    QrVSConAtuControle.Value, emidEmProcCon, TEstqMotivDel.emtdWetCurti237);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSConCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSConCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSConCab.RecalculaCusto;
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.Recalculacusto1Click(Sender: TObject);
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.Reimprimereceita1Click(Sender: TObject);
begin
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TReceitaTipoSetor.rectipsetrRibeira, PB1);
end;

procedure TFmVSConCab.ReopenEmit();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0(QrVSConCabMovimCod.Value),
  '']);
end;

procedure TFmVSConCab.ReopenVSConOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSConCabTemIMEIMrt.Value;
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVSConOriIMEI, QrVSConCabMovimCod.Value, Controle,
  TemIMEIMrt, eminSorcCon, SQL_Limit);
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSConCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCon)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSConOriPallet, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSConOriPallet.Locate('Pallet', Pallet, []);
end;

procedure TFmVSConCab.ReopenVSSubPrdIts(Controle: Integer);
var
  GSPInnNiv2, TemIMEIMrt: Integer;
begin
  GSPInnNiv2 := QrVSConOriIMEISrcNivel2.Value;
  TemIMEIMrt := QrVSConCabTemIMEIMrt.Value;
  VS_PF.ReopenVSSubPrdIts(QrVSSubPrdIts, GSPInnNiv2, TemIMEIMrt, Controle);
end;

procedure TFmVSConCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
(*
  if QrVSConAtuSdoVrtPeca.Value < QrVSConAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSConCabCodigo.Value;
    CtrlDel  := QrVSConDstControle.Value;
    CtrlDst  := QrVSConDstSrcNivel2.Value;
    MovimNiv := QrVSConAtuMovimNiv.Value;
    MovimCod := QrVSConAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti237), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSConDstMovimCod.Value, QrVSConDstMovimTwn.Value, emidConservado,
        eminEmConBxa);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti237), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSConDst,
        TIntegerField(QrVSConDstControle), QrVSConDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsconcab', MovimCod);
      VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      (*
      VS_PF.ReopenVSOpePrcDst(QrVSConDst, QrVSConCabMovimCod.Value, CtrlNxt,
      QrVSConCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestCon);
      *)
      VS_EFD_ICMS_IPI.ReopenVSPrcPrcDst(QrVSConDst, QrVSConCabCodigo.Value,
      QrVSConCabMovimCod.Value, CtrlNxt,
      QrVSConCabTemIMEIMrt.Value, TEstqMovimID.emidConservado, eminDestCon);
    end;
  end;
end;

procedure TFmVSConCab.ItsExcluiOriIMEI2Click(Sender: TObject);
begin
  ExcluiImeiDeOrigem(True);
end;

procedure TFmVSConCab.ItsExcluiOriIMEIClick(Sender: TObject);
begin
  ExcluiImeiDeOrigem(False);
end;

procedure TFmVSConCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if MyObjects.FIC(QrVSConOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrVSConAtuSdoVrtPeca.Value < QrVSConAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSConOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSConOriIMEI.First;
    while not QrVSConOriIMEI.Eof do
    begin
      if QrVSConOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSConOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSConOriIMEI.First;
        while not QrVSConOriIMEI.Eof do
        begin
          if QrVSConOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSConCabCodigo.Value;
            CtrlDel   := QrVSConOriIMEIControle.Value;
            CtrlOri   := QrVSConOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSConAtuMovimNiv.Value;
            MovimCod  := QrVSConAtuMovimCod.Value;
            //
            if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti237),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_PF.AtualizaTotaisVSXxxCab('vsconcab', MovimCod);
              VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
            end;
            //
          end;
          QrVSConOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSConOriIMEI,
      TIntegerField(QrVSConOriIMEIControle), QrVSConOriIMEIControle.Value);
      //
      VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSConOris(CtrlNxt, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSConCab.Datadeabertura1Click(Sender: TObject);
var
  Data, Hora: TDateTime;
  Codigo: Integer;
  DtHrAberto: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  if not DBCheck.ObtemData(Date, Data, Date, Hora, True) then Exit;
  DtHrAberto := Geral.FDT(Data, 109);
  //
  Codigo := QrVSConCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsconcab', False, [
  'DtHrAberto'], ['Codigo'], [DtHrAberto], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSConCab.DefineONomeDoForm;
begin
end;

procedure TFmVSConCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSConCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSConCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSConCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSConCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSConCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSConCab.BtSubProdutoClick(Sender: TObject);
begin
  PCConOri.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMSubProduto, BtSubProduto);
end;

procedure TFmVSConCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSConCabCodigo.Value;
  Close;
end;

procedure TFmVSConCab.ItsIncluiDstClick(Sender: TObject);
begin
  MostraVSConDst(stIns);
end;

procedure TFmVSConCab.CabAltera1Click(Sender: TObject);
var
  Empresa(*, VSPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSConCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsconcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSConCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrVSConAtuClientMO.Value;
  CBClientMO.KeyValue        := QrVSConAtuClientMO.Value;
  //
  EdCliente.ValueVariant    := QrVSConCabCliente.Value;
  CBCliente.KeyValue        := QrVSConCabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrVSConCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSConCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSConCabLPFMO.Value;
  //
  EdControle.ValueVariant   := QrVSConAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSConAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSConAtuGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrVSConAtuCustoMOTot.Value;
  EdCustoMOM2.ValueVariant  := QrVSConAtuCustoMOM2.Value;
  EdPecas.ValueVariant      := QrVSConAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSConAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSConAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSConAtuObserv.Value;
  //
  VS_PF.ReopenPedItsXXX(QrVSPedIts, QrVSConAtuControle.Value, QrVSConAtuControle.Value);
  Habilita := QrVSConAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSConAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSConAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSConAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSConAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSConAtuReqMovEstq.Value;
  //
  EdFornecMO.ValueVariant   := QrVSConAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSConAtuFornecMO.Value;
end;

procedure TFmVSConCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, (*GGXSrc,*)
  GGXDst, Forma, Quant, Cliente, CliVenda, StqCenLoc, SerieRem, NFeRem: Integer;
  Nome, DtHrAberto, DataHora, LPFMO: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  //GGXSrc         := EdGGXSrc.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  //if MyObjects.FIC(GGXSrc = 0, EdGGXSrc, 'Informe o artigo ap�s divisora!') then
    //Exit;
  //
  if not VS_PF.ValidaCampoNF(7, EdNFeRem, True) then Exit;
  //
  GGXDst := GraGruX;
  Codigo := UMyMod.BPGS1I32('vsconcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsconcab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', (*'GGXSrc',*) 'GGXDst'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, (*GGXSrc,*) GGXDst], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidEmProcCon, Codigo)
    else
    begin
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO, GraGruX, Cliente, DtHrAberto);

    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSConOriIMEI.RecordCount = 0) then
    begin
      //MostraVSConOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      //'Pallet Total', 'Pallet Parcial',
      'IME-I Total', 'IME-I Parcial'], -1);
      case Forma of
        //0: MostraVSConOriPall(stIns, ptTotal);
        //1: MostraVSConOriPall(stIns, ptParcial);
        0: MostraVSConOriIMEI(stIns, ptTotal);
        1: MostraVSConOriIMEI(stIns, ptParcial);
      end;
    end;
    VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
  end;
end;

procedure TFmVSConCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsconcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsconcab', 'Codigo');
end;

procedure TFmVSConCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSConCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSConCab.BtPesagemClick(Sender: TObject);
begin
  if not PCConOri.ActivePageIndex in [2,3] then
    PCConOri.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSConCab.AlteraSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  (*
  OriGGX      = 0;
  OriSerieFch = 0;
  OriFicha    = 0;
  OriMarca    = '';
  *)
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  Controle, VSMulFrnCab: Integer;
  OriMarca: String;
begin
  DataHora    := 0;
  GSPSrcMovID := TEstqMovimID(0);
  GSPSrcNiv2  := 0;
  Codigo      := 0;
  MovimCod    := 0;
  Empresa     := 0;
  ClientMO    := 0;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  Ficha       := 0;
  SerieFch    := 0;
  Controle    := QrVSSubPrdItsControle.Value;
  OriMarca    := '';
  //
  VS_PF.MostraFormVSInnSubPrdIts_Uni(stUpd, DataHora, QrCab, QrVSSubPrdIts, DsCab,
  GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
  VSMulFrnCab, Ficha, SerieFch, Controle, (*OriGGX, OriSerieFch, OriFicha,*) OriMarca);
  //ReopenVSSubPrdIts(0);
end;

procedure TFmVSConCab.AtualizaestoqueEmProcesso1Click(Sender: TObject);
begin
  VS_PF.AtualizaTotaisVSConCab(QrVSConCabMovimCod.Value);
  LocCod(QrVSConCabCodigo.Value, QrVSConCabCodigo.Value);
end;

procedure TFmVSConCab.AtualizaFornecedoresDeDestino;
var
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsconcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSConCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      if not FAtualizando then
        Qry1.Last
      else
      begin
        Codigo := Qry1.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_PF.AtualizaFornecedorCon(QrVSConCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSConCabMovimCod.Value);
        VS_PF.AtualizaVSConCabGGxSrc(QrVSConCabMovimCod.Value);
        //
      end;
      Qry1.Next;
    end;
  finally
    Qry1.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSConCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSConCab, QrVSConCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSConCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  QrVSConCab.Database  := Dmod.MyDB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PCConOri.ActivePageIndex := 1;
  //GBEdita.Align := alClient;
  PnDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1072_VSNatInC));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, emidEmProcCon);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
end;

procedure TFmVSConCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSConCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSConCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSConCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSConCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSConCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSConCab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSConCab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSConCab.QrVSConCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSConCab.QrVSConCabAfterScroll(DataSet: TDataSet);
{
const
  Controle = 0;
begin
  // Em processo
  VS_PF.ReopenVSOpePrcAtu(QrVSConAtu, QrVSConCabMovimCod.Value, Controle,
  QrVSConCabTemIMEIMrt.Value, eminEmConInn);
  // Origem
  ReopenVSConOris(0, 0);
  // Destino
  VS_PF.ReopenVSPrcPrcDst(QrVSConDst, QrVSConCabCodigo.Value,
  QrVSConCabMovimCod.Value, Controle,
  QrVSConCabTemIMEIMrt.Value, TEstqMovimID.emidConservado, eminDestCon);
  //ReopenForcados(0);
  VS_PF.ReopenVSOpePrcForcados(QrForcados, 0, QrVSConCabCodigo.Value,
    QrVSConAtuControle.Value, QrVSConCabTemIMEIMrt.Value, emidEmProcCon);
  // Insumos
  ReopenEmit();
  PQ_PF.ReopenPQOVS(QrPQO, QrVSConCabMovimCod.Value, 0);
}
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSConAtu, QrVSConOriIMEI, QrVSConOriPallet,
  QrVSConDst, QrVSConInd, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  QrEmit, QrPQO, QrVSConCabCodigo.Value,
  QrVSConCabMovimCod.Value, QrVSConCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmProcCon, emidConservado, emidConservado,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmConInn, eminSorcCon, eminDestCon);
end;

procedure TFmVSConCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSConCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSConCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCon, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //ReopenVSConIts(Controle);
  end;
end;

procedure TFmVSConCab.SbStqCenLocClick(Sender: TObject);
begin
  VS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidEmProcCon);
end;

procedure TFmVSConCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSConCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSConCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  if QrGraGruX.RecordCount > 1 then
  begin
    EdGraGruX.ValueVariant    := 0;
    CBGraGruX.KeyValue        := 0;
  end;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  if QrPrestador.RecordCount > 1 then
  begin
    EdFornecMO.ValueVariant   := 0;
    CBFornecMO.KeyValue       := 0;
  end;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSConCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsconcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  Agora := DModG.ObtemAgora();
  //TPData.Date := Agora;
  //EdHora.ValueVariant := Agora;
  VS_PF.DefineDataHoraOuDtMinima(Agora, TPData, EdHora);
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSConCab.QrVSConCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSConAtu.Close;
  QrVSConOriIMEI.Close;
  QrVSConOriPallet.Close;
  QrVSConDst.Close;
  QrVSConInd.Close;
  QrForcados.Close;
  QrEmit.Close;
  QrPQO.Close;
end;

procedure TFmVSConCab.QrVSConCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSConCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSConCab.QrVSConCabCalcFields(DataSet: TDataSet);
begin
  case QrVSConCabTipoArea.Value of
    0: QrVSConCabNO_TIPO.Value := 'm�';
    1: QrVSConCabNO_TIPO.Value := 'ft�';
    else QrVSConCabNO_TIPO.Value := '???';
  end;
  QrVSConCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSConCabDtHrLibOpe.Value, 106, True);
  QrVSConCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSConCabDtHrFimOpe.Value, 106, True);
  QrVSConCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSConCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSConCab.QrVSConDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSConCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSConCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmConBxa)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSConDstMovimTwn.Value),
  '']);
  SQL_Group := '';
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSConBxa, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
*)
  //
end;

procedure TFmVSConCab.QrVSConDstBeforeClose(DataSet: TDataSet);
begin
(*
  QrVSConBxa.Close;
*)
end;

procedure TFmVSConCab.QrVSConOriIMEIAfterScroll(DataSet: TDataSet);
begin
  ReopenVSSubPrdIts(0);
end;

procedure TFmVSConCab.QrVSConOriIMEIBeforeClose(DataSet: TDataSet);
begin
  QrVSSubPrdIts.Close;
end;

//Pesagem!
//Data n�o pode ser menor que a data m�nima: 01/12/2018

end.

