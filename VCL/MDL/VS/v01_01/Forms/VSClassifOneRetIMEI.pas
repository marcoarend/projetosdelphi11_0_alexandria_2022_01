unit VSClassifOneRetIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  mySQLDbTables, UnProjGroup_Consts, UnDmkProcFunc, dmkEditCB,
  dmkDBLookupComboBox, UnAppEnums;

type
  TFmVSClassifOneRetIMEI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnPartida: TPanel;
    DBGEmClasse: TdmkDBGridZTO;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_FICHA: TWideStringField;
    QrVSMovItsCUSTO_M2: TFloatField;
    QrVSMovItsCUSTO_P2: TFloatField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    DsVSMovIts: TDataSource;
    QrVSPaClaCab: TmySQLQuery;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    DsVSGerArtNew: TDataSource;
    DBGAClassificar: TdmkDBGridZTO;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    Label49: TLabel;
    MeLeitura: TMemo;
    Label33: TLabel;
    Label34: TLabel;
    EdDVIMEI: TdmkEdit;
    EdIMEI: TdmkEdit;
    LaVSRibCad: TLabel;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdIMEIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGEmClasseDblClick(Sender: TObject);
    procedure DBGAClassificarDblClick(Sender: TObject);
    procedure MeLeituraChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAbertos();
    procedure SelecionaImei();
    procedure ExecutaPesquisa();
    procedure GeraNovoIMEI();
    procedure NeutralizaAcaoUsu(Neutraliza: Boolean);
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FPallet01, FPallet02, FPallet03, FPallet04, FPallet05,
    FPallet06, FPallet07, FPallet08, FPallet09, FPallet10,
    FPallet11, FPallet12, FPallet13, FPallet14, FPallet15: Integer;
    FMovimID: TEstqMovimID;
    FForm: TForm;
    FBoxMax: Integer;
  end;

  var
  FmVSClassifOneRetIMEI: TFmVSClassifOneRetIMEI;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_CRC_PF, VSClassifOneNew,
  VSClassifOneNw3;

{$R *.DFM}

procedure TFmVSClassifOneRetIMEI.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  FCacCod := 0;
  //FMovimID := 0;
  Close;
end;

procedure TFmVSClassifOneRetIMEI.DBGAClassificarDblClick(Sender: TObject);
begin
  GeraNovoIMEI();
end;

procedure TFmVSClassifOneRetIMEI.DBGEmClasseDblClick(Sender: TObject);
begin
  if (QrVSMovIts.State <> dsInactive) and (QrVSMovIts.RecordCount > 0) then
    SelecionaIMEI();
end;

procedure TFmVSClassifOneRetIMEI.EdIMEIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    ExecutaPesquisa();
end;

procedure TFmVSClassifOneRetIMEI.ExecutaPesquisa();
const
  Controle = 0;
var
  IMEI: Integer;
begin
  IMEI := EdIMEI.ValueVariant;
  ReopenAbertos();
  VS_CRC_PF.ReopenVSGerArtDst_ToClassPorIMEI(QrVSGerArtNew, IMEI);
  //
  if (QrVSMovIts.RecordCount = 1) and (QrVSGerArtNew.RecordCount = 0) then
     SelecionaIMEI()
  else
  if (*(QrVSMovIts.RecordCount = 0) and *)(QrVSGerArtNew.RecordCount = 1) then
    GeraNovoIMEI();
end;

procedure TFmVSClassifOneRetIMEI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClassifOneRetIMEI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  //
  FPallet01 := 0;
  FPallet02 := 0;
  FPallet03 := 0;
  FPallet04 := 0;
  FPallet05 := 0;
  FPallet06 := 0;
  FPallet07 := 0;
  FPallet08 := 0;
  FPallet09 := 0;
  FPallet10 := 0;
  FPallet11 := 0;
  FPallet12 := 0;
  FPallet13 := 0;
  FPallet14 := 0;
  FPallet15 := 0;
  //
  ReopenAbertos();
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSClassifOneRetIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClassifOneRetIMEI.GeraNovoIMEI();
const
  Automatico = True;
var
  StqCenLoc, VSMovIts, VSGerArt: Integer;
begin
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local!') then
    Exit;
  if (QrVSGerArtNew.State <> dsInactive) and (QrVSGerArtNew.RecordCount > 0) then
  begin
    if DmkPF.DigitoVerificardorDmk3CasasInteger(EdIMEI.ValueVariant) = EdDVIMEI.ValueVariant then
    begin
      NeutralizaAcaoUsu(True);
      if FBoxMax <= 6 then
        VS_CRC_PF.MostraFormVSClaArtPrpMDz(FPallet01, FPallet02, FPallet03, FPallet04,
        FPallet05, FPallet06, FForm, QrVSGerArtNewControle.Value,
        EdDVIMEI.ValueVariant, StqCenLoc)
      else
        VS_CRC_PF.MostraFormVSClaArtPrpQnz(
        // ini 2019-09-09
        //[FPallet01, FPallet02, FPallet03, FPallet04, FPallet05,
        [0, FPallet01, FPallet02, FPallet03, FPallet04, FPallet05, // Pallets v�o do um ao 15!!!
        FPallet06, FPallet07, FPallet08, FPallet09, FPallet10,
        FPallet11, FPallet12, FPallet13, FPallet14, FPallet15],
        FForm, QrVSGerArtNewControle.Value,
        EdDVIMEI.ValueVariant, StqCenLoc);
      //
      Close;
    end;
  end;
end;

procedure TFmVSClassifOneRetIMEI.MeLeituraChange(Sender: TObject);
var
  Leitura: String;
begin
  if MeLeitura.Lines.Count > 1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Processando dados da leitura');
      Leitura := MeLeitura.Lines[0];
      while Length(Leitura) < 13 do
      begin
        Leitura := '0' + Leitura;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, Leitura);
      MeLeitura.Text := Leitura;
      //
      EdIMEI.ValueVariant := 0;
      //
      if DmkPF.CheckSumEAN13(Leitura, True) then
      begin
        EdDVIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 1, 3));
        EdIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 4, 9));
        //
        SpeedButton1Click(Self);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVSClassifOneRetIMEI.NeutralizaAcaoUsu(Neutraliza: Boolean);
begin
  if Neutraliza then
    Visible := False
  else
    Visible := True;
  DBGEmClasse.Visible := not Neutraliza;
  DBGAClassificar.Visible := not Neutraliza;
  Application.ProcessMessages;
end;

procedure TFmVSClassifOneRetIMEI.ReopenAbertos();
var
  IMEI: Integer;
begin
  IMEI := EdIMEI.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(wmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(wmi.Ficha=0, "V�rias", wmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(CONVERT(wmi.Controle, CHAR), " ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  'IF(wmi.Ficha=0, "", CONCAT(" - S�rie / Ficha RMP ", wmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.Controle=' + Geral.FF0(IMEI),
  '']);
end;

procedure TFmVSClassifOneRetIMEI.SelecionaImei;
var
  StqCenLoc, VSMovIts, VSGerArt: Integer;
begin
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local!') then
    Exit;
  NeutralizaAcaoUsu(True);
  VSGerArt := QrVSMovItsCodigo.Value;
  VSMovIts := QrVSMovItsControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vspaclacaba ',
  'WHERE VSMovIts=' + Geral.FF0(VSMovIts),
  'AND VSGerArt=' + Geral.FF0(VSGerArt),
  '']);
  //
  FCodigo := 0;
  FCacCod := 0;
  case QrVSPaClaCab.RecordCount of
    0: Geral.MB_Aviso('Configura��o de classifica��o n�o localizada!');
    1:
    begin
      FCodigo := QrVSPaClaCabCodigo.Value;
      FCacCod := QrVSPaClaCabCacCod.Value;
      if VSGerArt > 0 then
        FMovimID := emidClassArtXXUni
      else
        FMovimID := emidReclasXXUni;
        //
      if LowerCase(FForm.Name) = LowerCase(FmVSClassifOneNw3.Name) then
      begin
        TFmVSClassifOneNw3(FForm).FCodigo := FCodigo;
        TFmVSClassifOneNw3(FForm).FCacCod := FCacCod;
        TFmVSClassifOneNw3(FForm).FMovimID := FMovimID;
        //
        TFmVSClassifOneNw3(FForm).ReopenVSPaClaCab();
      end else
      if LowerCase(FForm.Name) = LowerCase(FmVSClassifOneNew.Name) then
      begin
        TFmVSClassifOneNew(FForm).FCodigo := FCodigo;
        TFmVSClassifOneNew(FForm).FCacCod := FCacCod;
        TFmVSClassifOneNew(FForm).FMovimID := FMovimID;
        //
        TFmVSClassifOneNew(FForm).ReopenVSPaClaCab();
      end;
    end;
    else Geral.MB_Aviso('Foram localizadas ' + Geral.FF0(QrVSPaClaCab.RecordCount) +
    ' configura��es de classifica��o!' + sLineBreak +
    'Por seguran�a nenhuma ser� ulilizada' + sLineBreak +
    'Avise a DERMATEK!');
  end;
  if FCodigo <> 0 then
    Close
  else
    NeutralizaAcaoUsu(False);
end;

procedure TFmVSClassifOneRetIMEI.SpeedButton1Click(Sender: TObject);
begin
  ExecutaPesquisa();
end;

end.
