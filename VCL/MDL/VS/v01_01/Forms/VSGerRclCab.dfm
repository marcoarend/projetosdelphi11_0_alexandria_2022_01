object FmVSGerRclCab: TFmVSGerRclCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-044 :: Gerenciamento de Reclassifica'#231#227'o'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 532
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 20
    ExplicitTop = -112
    object GBCntrl: TGroupBox
      Left = 0
      Top = 531
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 226
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 400
        Top = 15
        Width = 606
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 473
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtClassesGeradas: TBitBtn
          Tag = 5
          Left = 8
          Top = 4
          Width = 160
          Height = 40
          Cursor = crHandPoint
          Caption = '&Reclasses Geradas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtClassesGeradasClick
        end
        object BtCorrigeBaix: TBitBtn
          Tag = 253
          Left = 172
          Top = 4
          Width = 160
          Height = 40
          Cursor = crHandPoint
          Caption = 'Corrige &Baixas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCorrigeBaixClick
        end
        object BtAlterarDatas: TBitBtn
          Tag = 10098
          Left = 336
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Alterar datas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlterarDatasClick
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 241
      Align = alTop
      Caption = ' Dados da Configura'#231#227'o de OC: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 82
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label32: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = dmkDBEdit1
        end
        object Label23: TLabel
          Left = 72
          Top = 0
          Width = 24
          Height = 13
          Caption = 'O.C.:'
          FocusControl = dmkDBEdit2
        end
        object Label2: TLabel
          Left = 144
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit1
        end
        object Label22: TLabel
          Left = 548
          Top = 0
          Width = 25
          Height = 13
          Caption = #193'rea:'
          FocusControl = DBEdit14
        end
        object Label11: TLabel
          Left = 584
          Top = 0
          Width = 92
          Height = 13
          Caption = 'Lib. p/ reclassificar:'
          FocusControl = DBEdit3
        end
        object Label33: TLabel
          Left = 700
          Top = 0
          Width = 103
          Height = 13
          Caption = 'Configur. p/ reclassif.:'
          FocusControl = DBEdit25
        end
        object Label34: TLabel
          Left = 816
          Top = 0
          Width = 92
          Height = 13
          Caption = 'Fim reclassifica'#231#227'o:'
          FocusControl = DBEdit26
        end
        object Label1: TLabel
          Left = 932
          Top = 0
          Width = 32
          Height = 13
          Caption = 'IME-C:'
          FocusControl = DBEdit0
        end
        object Label5: TLabel
          Left = 8
          Top = 40
          Width = 172
          Height = 13
          Caption = 'Observa'#231#227'o sobre a reclassifica'#231#227'o:'
          FocusControl = DBEdit4
        end
        object Label37: TLabel
          Left = 584
          Top = 40
          Width = 30
          Height = 13
          Caption = 'Artigo:'
          FocusControl = DBEdit15
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSGerRcl
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 72
          Top = 16
          Width = 69
          Height = 21
          TabStop = False
          DataField = 'CacCod'
          DataSource = DsVSGerRcl
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 144
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSGerRcl
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 200
          Top = 16
          Width = 345
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsVSGerRcl
          TabOrder = 3
        end
        object DBEdit14: TDBEdit
          Left = 548
          Top = 16
          Width = 32
          Height = 21
          DataField = 'NO_TIPO'
          DataSource = DsVSGerRcl
          TabOrder = 4
        end
        object DBEdit3: TDBEdit
          Left = 584
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrLibCla'
          DataSource = DsVSGerRcl
          TabOrder = 5
        end
        object DBEdit25: TDBEdit
          Left = 700
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrCfgCla'
          DataSource = DsVSGerRcl
          TabOrder = 6
        end
        object DBEdit26: TDBEdit
          Left = 816
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrFimCla'
          DataSource = DsVSGerRcl
          TabOrder = 7
        end
        object DBEdit0: TdmkDBEdit
          Left = 932
          Top = 16
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'MovimCod'
          DataSource = DsVSGerRcl
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 8
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 56
          Width = 573
          Height = 21
          DataField = 'Nome'
          DataSource = DsVSGerRcl
          TabOrder = 9
        end
        object DBEdit15: TDBEdit
          Left = 584
          Top = 56
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsVSGerRcl
          TabOrder = 10
        end
        object DBEdit28: TDBEdit
          Left = 644
          Top = 56
          Width = 348
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsVSGerRcl
          TabOrder = 11
        end
      end
      object GroupBox1: TGroupBox
        Left = 2
        Top = 97
        Width = 1004
        Height = 142
        Align = alClient
        Caption = ' Dados do Pallet de Origem: '
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 16
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label4: TLabel
          Left = 76
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label6: TLabel
          Left = 500
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label7: TLabel
          Left = 16
          Top = 56
          Width = 194
          Height = 13
          Caption = 'Artigo de Ribeira Classificado (Reduzido):'
        end
        object Label10: TLabel
          Left = 424
          Top = 56
          Width = 93
          Height = 13
          Caption = 'Cliente preferencial:'
        end
        object Label8: TLabel
          Left = 796
          Top = 56
          Width = 33
          Height = 13
          Caption = 'Status:'
        end
        object Label13: TLabel
          Left = 16
          Top = 96
          Width = 126
          Height = 13
          Caption = 'Data / hora encerramento:'
          FocusControl = DBEdit13
        end
        object Label14: TLabel
          Left = 148
          Top = 96
          Width = 30
          Height = 13
          Caption = 'Artigo:'
          FocusControl = DBEdit16
        end
        object Label15: TLabel
          Left = 420
          Top = 96
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit18
        end
        object Label16: TLabel
          Left = 492
          Top = 96
          Width = 43
          Height = 13
          Caption = 'Peso Kg:'
          FocusControl = DBEdit19
        end
        object Label17: TLabel
          Left = 564
          Top = 96
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit20
        end
        object Label18: TLabel
          Left = 636
          Top = 96
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          FocusControl = DBEdit21
        end
        object Label19: TLabel
          Left = 708
          Top = 96
          Width = 39
          Height = 13
          Caption = '$ Custo:'
          FocusControl = DBEdit22
        end
        object Label20: TLabel
          Left = 780
          Top = 96
          Width = 50
          Height = 13
          Caption = 'Sdo Pe'#231'a:'
          FocusControl = DBEdit23
        end
        object Label21: TLabel
          Left = 852
          Top = 96
          Width = 38
          Height = 13
          Caption = 'Sdo Kg:'
          FocusControl = DBEdit24
        end
        object Label9: TLabel
          Left = 924
          Top = 96
          Width = 36
          Height = 13
          Caption = 'Sdo m'#178':'
          FocusControl = DBEdit27
        end
        object Label12: TLabel
          Left = 424
          Top = 16
          Width = 39
          Height = 13
          Caption = 'Arquivo:'
          FocusControl = DBEdit29
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSPallet
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 76
          Top = 32
          Width = 345
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsVSPallet
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit5: TDBEdit
          Left = 500
          Top = 32
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSPallet
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 560
          Top = 32
          Width = 433
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsVSPallet
          TabOrder = 3
        end
        object DBEdit7: TDBEdit
          Left = 16
          Top = 72
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsVSPallet
          TabOrder = 4
        end
        object DBEdit8: TDBEdit
          Left = 72
          Top = 72
          Width = 349
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsVSPallet
          TabOrder = 5
        end
        object DBEdit9: TDBEdit
          Left = 424
          Top = 72
          Width = 56
          Height = 21
          DataField = 'CliStat'
          DataSource = DsVSPallet
          TabOrder = 6
        end
        object DBEdit10: TDBEdit
          Left = 480
          Top = 72
          Width = 313
          Height = 21
          DataField = 'NO_CLISTAT'
          DataSource = DsVSPallet
          TabOrder = 7
        end
        object DBEdit11: TDBEdit
          Left = 796
          Top = 72
          Width = 56
          Height = 21
          DataField = 'Status'
          DataSource = DsVSPallet
          TabOrder = 8
        end
        object DBEdit12: TDBEdit
          Left = 852
          Top = 72
          Width = 141
          Height = 21
          DataField = 'NO_STATUS'
          DataSource = DsVSPallet
          TabOrder = 9
        end
        object DBEdit13: TDBEdit
          Left = 16
          Top = 112
          Width = 129
          Height = 21
          DataField = 'DtHrEndAdd_TXT'
          DataSource = DsVSPallet
          TabOrder = 10
        end
        object DBEdit16: TDBEdit
          Left = 148
          Top = 112
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsSumPall
          TabOrder = 11
        end
        object DBEdit17: TDBEdit
          Left = 204
          Top = 112
          Width = 213
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsSumPall
          TabOrder = 12
        end
        object DBEdit18: TDBEdit
          Left = 420
          Top = 112
          Width = 68
          Height = 21
          DataField = 'Pecas'
          DataSource = DsSumPall
          TabOrder = 13
        end
        object DBEdit19: TDBEdit
          Left = 492
          Top = 112
          Width = 68
          Height = 21
          DataField = 'PesoKg'
          DataSource = DsSumPall
          TabOrder = 14
        end
        object DBEdit20: TDBEdit
          Left = 564
          Top = 112
          Width = 68
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsSumPall
          TabOrder = 15
        end
        object DBEdit21: TDBEdit
          Left = 636
          Top = 112
          Width = 68
          Height = 21
          DataField = 'AreaP2'
          DataSource = DsSumPall
          TabOrder = 16
        end
        object DBEdit22: TDBEdit
          Left = 708
          Top = 112
          Width = 68
          Height = 21
          DataField = 'ValorT'
          DataSource = DsSumPall
          TabOrder = 17
        end
        object DBEdit23: TDBEdit
          Left = 780
          Top = 112
          Width = 68
          Height = 21
          DataField = 'SdoVrtPeca'
          DataSource = DsSumPall
          TabOrder = 18
        end
        object DBEdit24: TDBEdit
          Left = 852
          Top = 112
          Width = 68
          Height = 21
          DataField = 'SdoVrtPeso'
          DataSource = DsSumPall
          TabOrder = 19
        end
        object DBEdit27: TDBEdit
          Left = 924
          Top = 112
          Width = 68
          Height = 21
          DataField = 'SdoVrtArM2'
          DataSource = DsSumPall
          TabOrder = 20
        end
        object DBEdit29: TDBEdit
          Left = 424
          Top = 32
          Width = 73
          Height = 21
          DataField = 'NO_TTW'
          DataSource = DsSumPall
          TabOrder = 21
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 241
      Width = 1008
      Height = 252
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Dados gerais'
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 90
          Align = alTop
          Caption = ' '#218'ltima (re)configura'#231#227'o de pallets: '
          TabOrder = 0
          object DGVSPaRclCab: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 996
            Height = 73
            Align = alClient
            DataSource = DsVSPaRclCab
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID Reconfig'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CacCod'
                Title.Caption = 'OC'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal01'
                Title.Caption = 'Pallet Box 1'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal02'
                Title.Caption = 'Pallet Box 2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal03'
                Title.Caption = 'Pallet Box 3'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal04'
                Title.Caption = 'Pallet Box4'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal05'
                Title.Caption = 'Pallet Box 5'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LstPal06'
                Title.Caption = 'Pallet Box 6'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFimCla_TXT'
                Title.Caption = 'Fim classifica'#231#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178':'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178':'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatorInt'
                Visible = True
              end>
          end
        end
        object Panel7: TPanel
          Left = 548
          Top = 90
          Width = 452
          Height = 134
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 226
            Height = 134
            Align = alLeft
            Caption = ' IME-Is de origem: '
            TabOrder = 0
            object DGVMI_Sorc: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 222
              Height = 117
              Align = alClient
              DataSource = DsVMI_Sorc
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'VMI_Sorc'
                  Title.Caption = 'IME-I origem'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Visible = True
                end>
            end
          end
          object GroupBox4: TGroupBox
            Left = 226
            Top = 0
            Width = 226
            Height = 134
            Align = alClient
            Caption = ' IME-Is de baixa: '
            TabOrder = 1
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 222
              Height = 117
              Align = alClient
              DataSource = DsVMI_Baix
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'VMI_Baix'
                  Title.Caption = 'IME-I baixa'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VMI_Dest'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VMI_Sorc'
                  Visible = True
                end>
            end
          end
        end
        object DGVSPaRclIts: TdmkDBGridZTO
          Left = 4
          Top = 100
          Width = 549
          Height = 77
          DataSource = DsVSPaRclIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMVSPaRclIts
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tecla'
              Title.Caption = 'Box'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPallet'
              Title.Caption = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Dest'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'IME-I Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -12
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrIni'
              Title.Caption = 'In'#237'cio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFim_TXT'
              Title.Caption = 'Final'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Sorc'
              Title.Caption = 'IME-I origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Baix'
              Title.Caption = 'IME-I baixa'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Couro a Couro'
        ImageIndex = 1
        object DBGCacItsA: TdmkDBGridZTO
          Left = 0
          Top = 48
          Width = 1000
          Height = 176
          Align = alClient
          DataSource = DsVSCacItsA
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtExclCacIts: TBitBtn
            Left = 8
            Top = 4
            Width = 160
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui todos itens'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtExclCacItsClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 421
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 421
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 421
        Height = 32
        Caption = 'Gerenciamento de Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = FmVSGerArtCab.BtIts
    Left = 156
    Top = 65524
  end
  object QrVSGerRcl: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSGerRclBeforeOpen
    AfterOpen = QrVSGerRclAfterOpen
    BeforeClose = QrVSGerRclBeforeClose
    AfterScroll = QrVSGerRclAfterScroll
    OnCalcFields = QrVSGerRclCalcFields
    SQL.Strings = (
      'SELECT vgr.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, pal.Nome NO_PALLET'
      'FROM vsgerrcla vgr'
      'LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet '
      'WHERE vgr.Codigo > 0')
    Left = 16
    Top = 461
    object QrVSGerRclNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSGerRclNO_DtHrFimCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimCla'
      Calculated = True
    end
    object QrVSGerRclNO_DtHrLibCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibCla'
      Calculated = True
    end
    object QrVSGerRclNO_DtHrCfgCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgCla'
      Calculated = True
    end
    object QrVSGerRclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerRclMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerRclCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSGerRclGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerRclVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSGerRclNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSGerRclEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerRclDtHrLibCla: TDateTimeField
      FieldName = 'DtHrLibCla'
    end
    object QrVSGerRclDtHrCfgCla: TDateTimeField
      FieldName = 'DtHrCfgCla'
    end
    object QrVSGerRclDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrVSGerRclTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSGerRclLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerRclDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerRclDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerRclUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerRclUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerRclAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerRclAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerRclNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSGerRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerRclNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerRclTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSGerRclClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsVSGerRcl: TDataSource
    DataSet = QrVSGerRcl
    Left = 16
    Top = 509
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &Gera'#231#227'o (c'#243'digo)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
    object PelaOC1: TMenuItem
      Caption = 'Pela OC'
      OnClick = PelaOC1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object Outrasimpresses1: TMenuItem
      Caption = 'Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
  end
  object QrVSPaRclCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaRclCabBeforeClose
    AfterScroll = QrVSPaRclCabAfterScroll
    OnCalcFields = QrVSPaRclCabCalcFields
    SQL.Strings = (
      'SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  '
      
        '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_T' +
        'XT,'
      'cn1.FatorInt  '
      'FROM vsparclcaba pra'
      'LEFT JOIN vspalleta  let ON let.Codigo=pra.VSPallet'
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 '
      'WHERE pra.VSGerRcl>0'
      'ORDER BY pra.Codigo ')
    Left = 232
    Top = 460
    object QrVSPaRclCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaRclCabVSGerRcl: TIntegerField
      FieldName = 'VSGerRcl'
    end
    object QrVSPaRclCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaRclCabVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaRclCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaRclCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaRclCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaRclCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaRclCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaRclCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaRclCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaRclCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
    end
    object QrVSPaRclCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
    end
    object QrVSPaRclCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
    end
    object QrVSPaRclCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
    end
    object QrVSPaRclCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
    end
    object QrVSPaRclCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
    end
    object QrVSPaRclCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
    end
    object QrVSPaRclCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
    end
    object QrVSPaRclCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
    end
    object QrVSPaRclCabDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaRclCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaRclCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaRclCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaRclCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaRclCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaRclCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaRclCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaRclCabDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
    end
    object QrVSPaRclCabPecas: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Pecas'
      Calculated = True
    end
    object QrVSPaRclCabAreaM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVSPaRclCabAreaP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVSPaRclCabFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object DsVSPaRclCab: TDataSource
    DataSet = QrVSPaRclCab
    Left = 232
    Top = 509
  end
  object QrVSPaRclIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaRclItsBeforeClose
    AfterScroll = QrVSPaRclItsAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM vsparclitsa'
      'WHERE Codigo=5'
      'ORDER BY DtHrIni')
    Left = 316
    Top = 460
    object QrVSPaRclItsFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPaRclItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaRclItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPaRclItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaRclItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaRclItsVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPaRclItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaRclItsTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrVSPaRclItsDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaRclItsDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSPaRclItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaRclItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaRclItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaRclItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaRclItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaRclItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaRclItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaRclItsDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
    end
  end
  object DsVSPaRclIts: TDataSource
    DataSet = QrVSPaRclIts
    Left = 316
    Top = 509
  end
  object PMVSPaRclIts: TPopupMenu
    Left = 40
    Top = 356
    object EncerraPallet1: TMenuItem
      Caption = '&Encerra Pallet'
      OnClick = EncerraPallet1Click
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Aream2) AreaM2, SUM(AreaP2) AreaP2'
      'FROM vscacitsa'
      'WHERE CacCod=65'
      'AND Codigo=5')
    Left = 384
    Top = 460
    object QrSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPalletBeforeClose
    AfterScroll = QrVSPalletAfterScroll
    OnCalcFields = QrVSPalletCalcFields
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspalleta let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'WHERE let.Codigo > 0')
    Left = 88
    Top = 461
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrVSPalletDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrVSPalletQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 88
    Top = 505
  end
  object QrSumPall: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.GraGruX, SUM(wmi.Pecas) Pecas,'
      'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2,'
      'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,'
      'SUM(wmi.SdoVrtPeca) SdoVrtPeca, SUM(wmi.SdoVrtPeso) SdoVrtPeso,'
      'SUM(wmi.SdoVrtArM2) SdoVrtArM2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, MAX(wmi.DataHora) DataHora'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 156
    Top = 460
    object QrSumPallPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrSumPallGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrSumPallPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPallAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumPallAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumPallPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSumPallNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSumPallNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrSumPallSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrSumPallFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrSumPallNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrSumPallID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrSumPallSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrSumPallTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrSumPallMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrSumPallNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrSumPallNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrSumPallValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrSumPallSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSumPallSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSumPallSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSumPall: TDataSource
    DataSet = QrSumPall
    Left = 156
    Top = 508
  end
  object QrVMI_Sorc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Sorc, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Sorc')
    Left = 456
    Top = 460
    object QrVMI_SorcVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVMI_SorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_SorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object DsVMI_Sorc: TDataSource
    DataSet = QrVMI_Sorc
    Left = 456
    Top = 509
  end
  object QrVMI_Baix: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Baix, VMI_Sorc, VMI_Dest,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Baix')
    Left = 536
    Top = 460
    object QrVMI_BaixVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVMI_BaixPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_BaixAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMI_BaixVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVMI_BaixVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsVMI_Baix: TDataSource
    DataSet = QrVMI_Baix
    Left = 536
    Top = 509
  end
  object PMQuery: TPopupMenu
    Left = 188
    Top = 48
    object Peladescriao1: TMenuItem
      Caption = 'Pela descri'#231#227'o'
      OnClick = Peladescriao1Click
    end
    object Poroutrasinformaes1: TMenuItem
      Caption = 'Por outras informa'#231#245'es'
      OnClick = PoroutrasInformaes1Click
    end
  end
  object QrPal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DtHrEndAdd '
      'FROM vspalleta'
      'WHERE Codigo = 221')
    Left = 340
    Top = 144
    object QrPalDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
  end
  object QrVMI_Src: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE Controle=0')
    Left = 404
    Top = 76
    object QrVMI_SrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMI_SrcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMI_SrcMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMI_SrcMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMI_SrcMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVMI_SrcEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMI_SrcTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMI_SrcCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVMI_SrcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMI_SrcLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVMI_SrcLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVMI_SrcDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMI_SrcPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVMI_SrcGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMI_SrcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_SrcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMI_SrcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMI_SrcAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVMI_SrcValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMI_SrcSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVMI_SrcSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVMI_SrcSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVMI_SrcSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVMI_SrcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMI_SrcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMI_SrcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMI_SrcObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVMI_SrcSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMI_SrcFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMI_SrcMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVMI_SrcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVMI_SrcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVMI_SrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVMI_SrcValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMI_SrcDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVMI_SrcDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVMI_SrcDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVMI_SrcDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVMI_SrcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMI_SrcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMI_SrcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMI_SrcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMI_SrcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMI_SrcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMI_SrcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMI_SrcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMI_SrcAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVMI_SrcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMI_SrcMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMI_SrcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVMI_SrcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVMI_SrcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVMI_SrcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVMI_SrcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVMI_SrcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVMI_SrcAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMI_SrcTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
  end
  object QrVMI_Dst: TMySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 124
    object QrVMI_DstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMI_DstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMI_DstMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMI_DstMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMI_DstMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVMI_DstEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMI_DstTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMI_DstCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVMI_DstMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMI_DstLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVMI_DstLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVMI_DstDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMI_DstPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVMI_DstGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMI_DstPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_DstPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMI_DstAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMI_DstAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVMI_DstValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMI_DstSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVMI_DstSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVMI_DstSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVMI_DstSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVMI_DstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMI_DstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMI_DstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMI_DstObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVMI_DstSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMI_DstFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMI_DstMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVMI_DstFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVMI_DstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVMI_DstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVMI_DstValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMI_DstDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVMI_DstDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVMI_DstDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVMI_DstDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVMI_DstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMI_DstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMI_DstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMI_DstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMI_DstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMI_DstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMI_DstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMI_DstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMI_DstAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVMI_DstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMI_DstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMI_DstLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVMI_DstDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVMI_DstDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVMI_DstUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVMI_DstUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVMI_DstAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVMI_DstAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMI_DstTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVMI_DstStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object QrVCI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 168
    object QrVCIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVCIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVCIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVCIPcBxa: TFloatField
      FieldName = 'PcBxa'
    end
  end
  object QrVSCacItsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vscacitsa'
      'WHERE VSPallet=17')
    Left = 636
    Top = 464
    object QrVSCacItsACacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSCacItsACacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrVSCacItsACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCacItsAControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrVSCacItsAClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrVSCacItsARclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrVSCacItsAVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrVSCacItsAVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrVSCacItsAVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSCacItsAVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSCacItsAVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSCacItsABox: TIntegerField
      FieldName = 'Box'
    end
    object QrVSCacItsAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsAAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsARevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrVSCacItsADigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrVSCacItsADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSCacItsAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCacItsAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCacItsASumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrVSCacItsARclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrVSCacItsAVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSCacItsAMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrVSCacItsAFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrVSCacItsASubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
  end
  object DsVSCacItsA: TDataSource
    DataSet = QrVSCacItsA
    Left = 636
    Top = 512
  end
  object PMCorrigeBaix: TPopupMenu
    Left = 672
    Top = 612
    object Antigo1: TMenuItem
      Caption = '&Antigo'
      OnClick = Antigo1Click
    end
    object Novo1: TMenuItem
      Caption = '&Novo'
      OnClick = Novo1Click
    end
  end
  object QrVMIsOri: TMySQLQuery
    Database = Dmod.MyDB
    Left = 736
    Top = 544
    object QrVMIsOriSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 732
    Top = 492
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
end
