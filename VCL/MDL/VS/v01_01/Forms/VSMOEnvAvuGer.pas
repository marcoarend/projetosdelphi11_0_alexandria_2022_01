unit VSMOEnvAvuGer;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBGridZTO, dmkDBLookupComboBox, dmkEditCB,
  dmkEditCalc, UnProjGroup_Vars;

type
  TFmVSMOEnvAvuGer = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrVSMOEnvAvu: TmySQLQuery;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    DsVSMOEnvAVMI: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    QrVSMOEnvAvuNO_Empresa: TWideStringField;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    Panel7: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label34: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label41: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label3: TLabel;
    QrVSMOEnvAvuNome: TWideStringField;
    DBMemo1: TDBMemo;
    QrVSMOEnvAvuNO_Transpor: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSMOEnvAvuAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSMOEnvAvuBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSMOEnvAVMI(SQLType: TSQLType);
    procedure MostraJanelaIMEC();
    procedure Relocaliza();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSMOEnvAVMI(Codigo: Integer);

  end;

var
  FmVSMOEnvAvuGer: TFmVSMOEnvAvuGer;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSMOEnvAVMI, UnVS_PF,
  ModVS_CRC, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSMOEnvAvuGer.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSMOEnvAvuGer.MostraFormVSMOEnvAVMI(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmVSMOEnvAVMI, FmVSMOEnvAVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvAVMI.ImgTipo.SQLType := SQLType;
    FmVSMOEnvAVMI.FQrCab := QrVSMOEnvAvu;
    FmVSMOEnvAVMI.FDsCab := DsVSMOEnvAvu;
    FmVSMOEnvAVMI.FQrIts := QrVSMOEnvAVMI;
    if SQLType = stIns then
      FmVSMOEnvAVMI.EdCPF1.ReadOnly := False
    else
    begin
      FmVSMOEnvAVMI.EdControle.ValueVariant := QrVSMOEnvAVMIControle.Value;
      //
      FmVSMOEnvAVMI.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrVSMOEnvAVMICNPJ_CPF.Value);
      FmVSMOEnvAVMI.EdNomeEmiSac.Text := QrVSMOEnvAVMINome.Value;
      FmVSMOEnvAVMI.EdCPF1.ReadOnly := True;
    end;
    FmVSMOEnvAVMI.ShowModal;
    FmVSMOEnvAVMI.Destroy;
  end;
*)
end;

procedure TFmVSMOEnvAvuGer.MostraJanelaIMEC();
begin
  VS_CRC_PF.MostraFormVS_Do_IMEC(QrVSMOEnvAvuVSVMI_MovimCod.Value,
  'Este item de frete n�o pode ser gerenciado aqui!' + sLineBreak +
  'Deseja visualizar a janela de movimento do IME-C onde o item pode ser gerenciado?');
end;

procedure TFmVSMOEnvAvuGer.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSMOEnvAvu, QrVSMOEnvAVMI);
end;

procedure TFmVSMOEnvAvuGer.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSMOEnvAVMI);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSMOEnvAVMI);
end;

procedure TFmVSMOEnvAvuGer.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSMOEnvAvuCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSMOEnvAvuGer.DefParams;
begin
  VAR_GOTOTABELA := 'vsmoenvavu';
  VAR_GOTOMYSQLTABLE := QrVSMOEnvAvu;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mea.*,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transpor');
  VAR_SQLx.Add('FROM vsmoenvavu mea');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=mea.CFTMA_Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades tra ON tra.Codigo=mea.CFTMA_Terceiro');
  VAR_SQLx.Add('WHERE mea.Codigo > 0');
  //
  VAR_SQL1.Add('AND mea.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSMOEnvAvuGer.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSMOEnvAVMI(stUpd);
end;

procedure TFmVSMOEnvAvuGer.CabExclui1Click(Sender: TObject);
begin
  VAR_VSMOEnvAvu := QrVSMOEnvAvuCodigo.Value;
  //
  if QrVSMOEnvAvuVSVMI_MovimCod.Value = 0 then
    DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI)
  else
    MostraJanelaIMEC();
  //
  Relocaliza();
end;

procedure TFmVSMOEnvAvuGer.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSMOEnvAvuGer.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSMOEnvAvuGer.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSMOEnvAVMI', 'Controle', QrVSMOEnvAVMIControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSMOEnvAVMI,
      QrVSMOEnvAVMIControle, QrVSMOEnvAVMIControle.Value);
    ReopenVSMOEnvAVMI(Controle);
  end;
}
end;

procedure TFmVSMOEnvAvuGer.Relocaliza();
var
  Codigo: Integer;
begin
  if VAR_VSMOEnvAvu <> 0 then
    Codigo := VAR_VSMOEnvAvu
  else
    Codigo := QrVSMOEnvAvuCodigo.Value;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSMOEnvAvuGer.ReopenVSMOEnvAVMI(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvAVMI, Dmod.MyDB, [
  'SELECT mei.* ',
  'FROM vsmoenvavmi mei ',
  'WHERE mei.VSMOEnvAvu=' + Geral.FF0(QrVSMOEnvAvuCodigo.Value),
  '']);
  //
  QrVSMOEnvAVMI.Locate('Codigo', Codigo, []);
end;


procedure TFmVSMOEnvAvuGer.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSMOEnvAvuGer.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSMOEnvAvuGer.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSMOEnvAvuGer.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSMOEnvAvuGer.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSMOEnvAvuGer.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvAvuGer.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSMOEnvAvuCodigo.Value;
  Close;
end;

procedure TFmVSMOEnvAvuGer.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSMOEnvAVMI(stIns);
end;

procedure TFmVSMOEnvAvuGer.CabAltera1Click(Sender: TObject);
begin
(*
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSMOEnvAvu, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmoenvavu');
*)
  VAR_VSMOEnvAvu := QrVSMOEnvAvuCodigo.Value;
  //
  if QrVSMOEnvAvuVSVMI_MovimCod.Value = 0 then
    VS_PF.MostraFormVSMOEnvAvu(stUpd, (*QrIMEIDest*)nil, QrVSMOEnvAvu, QrVSMOEnvAVMI,
    siPositivo, 0, 0, 0)
  else
    MostraJanelaIMEC();
  //
  Relocaliza();
end;

procedure TFmVSMOEnvAvuGer.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('vsmoenvavu', 'Codigo', ImgTipo.SQLType,
    QrVSMOEnvAvuCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('vsmoenvavu', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrVSMOEnvAvuCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'vsmoenvavu',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmVSMOEnvAvuGer.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsmoenvavu', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsmoenvavu', 'Codigo');
end;

procedure TFmVSMOEnvAvuGer.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSMOEnvAvuGer.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSMOEnvAvuGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGVSMOEnvAVMI.Align := alClient;
  CriaOForm;
  FSeq := 0;
  VAR_PC_FRETE_VS_NAME := 'TsFrCompr';
end;

procedure TFmVSMOEnvAvuGer.FormDestroy(Sender: TObject);
begin
  VAR_PC_FRETE_VS_NAME := '';
end;

procedure TFmVSMOEnvAvuGer.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSMOEnvAvuCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMOEnvAvuGer.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSMOEnvAvuGer.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSMOEnvAvuCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMOEnvAvuGer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSMOEnvAvuGer.QrVSMOEnvAvuAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSMOEnvAvuGer.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMOEnvAVMI(0);
end;

procedure TFmVSMOEnvAvuGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSMOEnvAvuCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSMOEnvAvuGer.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSMOEnvAvuCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsmoenvavu', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSMOEnvAvuGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvAvuGer.CabInclui1Click(Sender: TObject);
begin
{
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSMOEnvAvu, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmoenvavu');
}
  //
  VAR_VSMOEnvAvu := QrVSMOEnvAvuCodigo.Value;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, (*QrIMEIDest*)nil, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo, 0, 0, 0);
  //
  Relocaliza();
end;

procedure TFmVSMOEnvAvuGer.QrVSMOEnvAvuBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSMOEnvAvuGer.QrVSMOEnvAvuBeforeOpen(DataSet: TDataSet);
begin
  QrVSMOEnvAvuCodigo.DisplayFormat := FFormatFloat;
end;

end.

