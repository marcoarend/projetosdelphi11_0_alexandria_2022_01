unit VSDvlCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, UnProjGroup_Consts, UnGrl_Consts, UnAppEnums, UnProjGroup_Vars;

type
  TFmVSDvlCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSDvlCab: TmySQLQuery;
    DsVSDvlCab: TDataSource;
    QrVSDvlIts: TmySQLQuery;
    DsVSDvlIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSDvlCabCodigo: TIntegerField;
    QrVSDvlCabMovimCod: TIntegerField;
    QrVSDvlCabEmpresa: TIntegerField;
    QrVSDvlCabDtCompra: TDateTimeField;
    QrVSDvlCabDtViagem: TDateTimeField;
    QrVSDvlCabDtEntrada: TDateTimeField;
    QrVSDvlCabFornecedor: TIntegerField;
    QrVSDvlCabTransporta: TIntegerField;
    QrVSDvlCabPecas: TFloatField;
    QrVSDvlCabPesoKg: TFloatField;
    QrVSDvlCabAreaM2: TFloatField;
    QrVSDvlCabAreaP2: TFloatField;
    QrVSDvlCabLk: TIntegerField;
    QrVSDvlCabDataCad: TDateField;
    QrVSDvlCabDataAlt: TDateField;
    QrVSDvlCabUserCad: TIntegerField;
    QrVSDvlCabUserAlt: TIntegerField;
    QrVSDvlCabAlterWeb: TSmallintField;
    QrVSDvlCabAtivo: TSmallintField;
    QrVSDvlCabNO_EMPRESA: TWideStringField;
    QrVSDvlCabNO_FORNECE: TWideStringField;
    QrVSDvlCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    N2: TMenuItem;
    QrVSDvlCabValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    N1: TMenuItem;
    Atualizaestoque1: TMenuItem;
    frxDsVSDvlCab: TfrxDBDataset;
    frxDsVSDvlIts: TfrxDBDataset;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    QrVSDvlCabClienteMO: TIntegerField;
    QrVSDvlCabProcednc: TIntegerField;
    QrVSDvlCabMotorista: TIntegerField;
    QrVSDvlCabPlaca: TWideStringField;
    QrVSDvlCabNO_CLIENTEMO: TWideStringField;
    QrVSDvlCabNO_PROCEDNC: TWideStringField;
    QrVSDvlCabNO_MOTORISTA: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrVSMovDif: TmySQLQuery;
    QrVSMovDifControle: TIntegerField;
    QrVSMovDifInfPecas: TFloatField;
    QrVSMovDifInfPesoKg: TFloatField;
    QrVSMovDifInfAreaM2: TFloatField;
    QrVSMovDifInfAreaP2: TFloatField;
    QrVSMovDifInfValorT: TFloatField;
    QrVSMovDifLk: TIntegerField;
    QrVSMovDifDataCad: TDateField;
    QrVSMovDifDataAlt: TDateField;
    QrVSMovDifUserCad: TIntegerField;
    QrVSMovDifUserAlt: TIntegerField;
    QrVSMovDifAlterWeb: TSmallintField;
    QrVSMovDifAtivo: TSmallintField;
    QrVSMovDifDifPecas: TFloatField;
    QrVSMovDifDifPesoKg: TFloatField;
    QrVSMovDifDifAreaM2: TFloatField;
    QrVSMovDifDifAreaP2: TFloatField;
    QrVSMovDifDifValorT: TFloatField;
    Histrico1: TMenuItem;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    TabSheet2: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSHisFch: TmySQLQuery;
    DsVSHisFch: TDataSource;
    QrVSHisFchCodigo: TIntegerField;
    QrVSHisFchVSMovIts: TIntegerField;
    QrVSHisFchSerieFch: TIntegerField;
    QrVSHisFchFicha: TIntegerField;
    QrVSHisFchDataHora: TDateTimeField;
    QrVSHisFchNome: TWideStringField;
    QrVSHisFchObserv: TWideStringField;
    QrVSHisFchLk: TIntegerField;
    QrVSHisFchDataCad: TDateField;
    QrVSHisFchDataAlt: TDateField;
    QrVSHisFchUserCad: TIntegerField;
    QrVSHisFchUserAlt: TIntegerField;
    QrVSHisFchAlterWeb: TSmallintField;
    QrVSHisFchAtivo: TSmallintField;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    DBMemo1: TDBMemo;
    Exclui1: TMenuItem;
    Splitter2: TSplitter;
    PMVDvlIts: TPopupMenu;
    IrparajaneladegerenciamentodeFichaRMP1: TMenuItem;
    DGDados: TdmkDBGridZTO;
    FichadePallets1: TMenuItem;
    Fichadetodospalletsdestacompra1: TMenuItem;
    frxWET_CURTI_106_00_A: TfrxReport;
    frxDsPallets: TfrxDBDataset;
    PackingList1: TMenuItem;
    FichaCOMnomedoPallet1: TMenuItem;
    FichaSEMnomedoPallet1: TMenuItem;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSDvlCabTemIMEIMrt: TIntegerField;
    QrVSDvlItsCodigo: TLargeintField;
    QrVSDvlItsControle: TLargeintField;
    QrVSDvlItsMovimCod: TLargeintField;
    QrVSDvlItsMovimNiv: TLargeintField;
    QrVSDvlItsMovimTwn: TLargeintField;
    QrVSDvlItsEmpresa: TLargeintField;
    QrVSDvlItsTerceiro: TLargeintField;
    QrVSDvlItsCliVenda: TLargeintField;
    QrVSDvlItsMovimID: TLargeintField;
    QrVSDvlItsDataHora: TDateTimeField;
    QrVSDvlItsPallet: TLargeintField;
    QrVSDvlItsGraGruX: TLargeintField;
    QrVSDvlItsPecas: TFloatField;
    QrVSDvlItsPesoKg: TFloatField;
    QrVSDvlItsAreaM2: TFloatField;
    QrVSDvlItsAreaP2: TFloatField;
    QrVSDvlItsValorT: TFloatField;
    QrVSDvlItsSrcMovID: TLargeintField;
    QrVSDvlItsSrcNivel1: TLargeintField;
    QrVSDvlItsSrcNivel2: TLargeintField;
    QrVSDvlItsSrcGGX: TLargeintField;
    QrVSDvlItsSdoVrtPeca: TFloatField;
    QrVSDvlItsSdoVrtPeso: TFloatField;
    QrVSDvlItsSdoVrtArM2: TFloatField;
    QrVSDvlItsObserv: TWideStringField;
    QrVSDvlItsSerieFch: TLargeintField;
    QrVSDvlItsFicha: TLargeintField;
    QrVSDvlItsMisturou: TLargeintField;
    QrVSDvlItsFornecMO: TLargeintField;
    QrVSDvlItsCustoMOKg: TFloatField;
    QrVSDvlItsCustoMOM2: TFloatField;
    QrVSDvlItsCustoMOTot: TFloatField;
    QrVSDvlItsValorMP: TFloatField;
    QrVSDvlItsDstMovID: TLargeintField;
    QrVSDvlItsDstNivel1: TLargeintField;
    QrVSDvlItsDstNivel2: TLargeintField;
    QrVSDvlItsDstGGX: TLargeintField;
    QrVSDvlItsQtdGerPeca: TFloatField;
    QrVSDvlItsQtdGerPeso: TFloatField;
    QrVSDvlItsQtdGerArM2: TFloatField;
    QrVSDvlItsQtdGerArP2: TFloatField;
    QrVSDvlItsQtdAntPeca: TFloatField;
    QrVSDvlItsQtdAntPeso: TFloatField;
    QrVSDvlItsQtdAntArM2: TFloatField;
    QrVSDvlItsQtdAntArP2: TFloatField;
    QrVSDvlItsNotaMPAG: TFloatField;
    QrVSDvlItsPedItsFin: TLargeintField;
    QrVSDvlItsMarca: TWideStringField;
    QrVSDvlItsStqCenLoc: TLargeintField;
    QrVSDvlItsNO_PALLET: TWideStringField;
    QrVSDvlItsNO_PRD_TAM_COR: TWideStringField;
    QrVSDvlItsNO_TTW: TWideStringField;
    QrVSDvlItsID_TTW: TLargeintField;
    QrVSDvlItsReqMovEstq: TLargeintField;
    QrVSDvlItsRendKgm2: TFloatField;
    QrVSDvlItsNotaMPAG_TXT: TWideStringField;
    QrVSDvlItsRendKgm2_TXT: TWideStringField;
    QrVSDvlItsMisturou_TXT: TWideStringField;
    QrVSDvlItsNOMEUNIDMED: TWideStringField;
    QrVSDvlItsSIGLAUNIDMED: TWideStringField;
    QrVSDvlItsm2_CouroTXT: TWideStringField;
    QrVSDvlItsKgMedioCouro: TFloatField;
    QrVSDvlItsSEQ: TIntegerField;
    QrVSDvlItsClientMO: TLargeintField;
    Label28: TLabel;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label29: TLabel;
    Label31: TLabel;
    Edemi_serie: TdmkEdit;
    Edemi_nNF: TdmkEdit;
    Label32: TLabel;
    QrVSDvlCabide_serie: TSmallintField;
    QrVSDvlCabide_nNF: TIntegerField;
    QrVSDvlCabemi_serie: TSmallintField;
    QrVSDvlCabemi_nNF: TIntegerField;
    QrVSDvlCabNFeStatus: TIntegerField;
    QrVSDvlItsIxxMovIX: TLargeintField;
    QrVSDvlItsIxxFolha: TLargeintField;
    QrVSDvlItsIxxLinha: TLargeintField;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvAvu: TmySQLQuery;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    DsVSMOEnvAVMI: TDataSource;
    TsFrCompr: TTabSheet;
    TsEnvioMO: TTabSheet;
    PnFrCompr: TPanel;
    DBGVSMOEnvAvu: TdmkDBGridZTO;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVMI: TdmkDBGridZTO;
    N3: TMenuItem;
    AtrelamentoFreteCompra1: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    QrVSDvlItsCusFrtAvuls: TFloatField;
    QrVSDvlItsCusFrtMOEnv: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSDvlCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSDvlCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSDvlCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSDvlCabBeforeClose(DataSet: TDataSet);
    procedure QrVSDvlItsBeforeClose(DataSet: TDataSet);
    procedure QrVSDvlItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure frxWET_CURTI_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichaRMP1Click(Sender: TObject);
    procedure QrVSDvlItsCalcFields(DataSet: TDataSet);
    procedure PackingList1Click(Sender: TObject);
    procedure frxWET_CURTI_106_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure FichaCOMnomedoPallet1Click(Sender: TObject);
    procedure FichaSEMnomedoPallet1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSDvlIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure MostraFormVSHisFchAdd(SQLType: TSQLType);
    procedure ImprimeFichaDePallet(InfoNO_PALLET: Boolean);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenVSDvlIts(Controle: Integer);
    procedure ReopenVSHisFch(Codigo: Integer);

  end;

var
  FmVSDvlCab: TFmVSDvlCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSDvlIts, ModuleGeral,
Principal, VSMovImp, UnVS_PF, UnVS_CRC_PF, VSHisFchAdd, CreateVS,
ModVS, ModVS_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSDvlCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSDvlCab.MostraFormVSHisFchAdd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSHisFchAdd, FmVSHisFchAdd, afmoNegarComAviso) then
  begin
    FmVSHisFchAdd.ImgTipo.SQLType := SQLType;
    FmVSHisFchAdd.FQrIts                  := QrVSHisFch;
    FmVSHisFchAdd.EdVSMovIts.ValueVariant := QrVSDvlItsControle.Value;
    FmVSHisFchAdd.EdSerieFch.ValueVariant := QrVSDvlItsSerieFch.Value;
    FmVSHisFchAdd.EdFicha.ValueVariant    := QrVSDvlItsFicha.Value;
    if SQLType = stUpd then
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := QrVSHisFchCodigo.Value;
      FmVSHisFchAdd.TPDataHora.Date       := QrVSHisFchDataHora.Value;
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(QrVSHisFchDataHora.Value, 100);
      FmVSHisFchAdd.EdNome.Text           := QrVSHisFchNome.Value;
      FmVSHisFchAdd.MeObserv.Text         := QrVSHisFchObserv.Value;
    end else
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := 0;
      FmVSHisFchAdd.TPDataHora.Date       := DModG.ObtemAgora();
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(DModG.ObtemAgora(), 100);
      FmVSHisFchAdd.EdNome.Text           := '';
      FmVSHisFchAdd.MeObserv.Text         := '';
    end;
    FmVSHisFchAdd.ShowModal;
    FmVSHisFchAdd.Destroy;
  end;
end;

procedure TFmVSDvlCab.MostraFormVSDvlIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSDvlIts, FmVSDvlIts, afmoNegarComAviso) then
  begin
    FmVSDvlIts.ImgTipo.SQLType := SQLType;
    FmVSDvlIts.FQrCab := QrVSDvlCab;
    FmVSDvlIts.FDsCab := DsVSDvlCab;
    FmVSDvlIts.FQrIts := QrVSDvlIts;
    FmVSDvlIts.FDataHora := QrVSDvlCabDtEntrada.Value;
    FmVSDvlIts.FEmpresa  := QrVSDvlCabEmpresa.Value;
    FmVSDvlIts.FClientMO := QrVSDvlCabClienteMO.Value;
    //
    FmVSDvlIts.EdFornecedor.ValueVariant := QrVSDvlCabFornecedor.Value;
    FmVSDvlIts.CBFornecedor.KeyValue     := QrVSDvlCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmVSDvlIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSDvlIts.EdControle.ValueVariant := QrVSDvlItsControle.Value;
      //
      FmVSDvlIts.EdGragruX.ValueVariant    := QrVSDvlItsGraGruX.Value;
      FmVSDvlIts.CBGragruX.KeyValue        := QrVSDvlItsGraGruX.Value;
      FmVSDvlIts.EdSerieFch.ValueVariant   := QrVSDvlItsSerieFch.Value;
      FmVSDvlIts.CBSerieFch.KeyValue       := QrVSDvlItsSerieFch.Value;
      FmVSDvlIts.EdFicha.ValueVariant      := QrVSDvlItsFicha.Value;
      FmVSDvlIts.EdPallet.ValueVariant     := QrVSDvlItsPallet.Value;
      FmVSDvlIts.CBPallet.KeyValue         := QrVSDvlItsPallet.Value;
      FmVSDvlIts.EdPecas.ValueVariant      := QrVSDvlItsPecas.Value;
      FmVSDvlIts.EdPesoKg.ValueVariant     := QrVSDvlItsPesoKg.Value;
      FmVSDvlIts.EdAreaM2.ValueVariant     := QrVSDvlItsAreaM2.Value;
      FmVSDvlIts.EdAreaP2.ValueVariant     := QrVSDvlItsAreaP2.Value;
      FmVSDvlIts.EdValorMP.ValueVariant    := QrVSDvlItsValorMP.Value;
      FmVSDvlIts.EdCustoMOKg.ValueVariant  := QrVSDvlItsCustoMOKg.Value;
      FmVSDvlIts.EdFornecMO.ValueVariant   := QrVSDvlItsFornecMO.Value;
      FmVSDvlIts.CBFornecMO.KeyValue       := QrVSDvlItsFornecMO.Value;
      FmVSDvlIts.EdObserv.ValueVariant     := QrVSDvlItsObserv.Value;
      FmVSDvlIts.EdStqCenLoc.ValueVariant  := QrVSDvlItsStqCenLoc.Value;
      FmVSDvlIts.CBStqCenLoc.KeyValue      := QrVSDvlItsStqCenLoc.Value;
      FmVSDvlIts.EdReqMovEstq.ValueVariant := QrVSDvlItsReqMovEstq.Value;
      FmVSDvlIts.RGIxxMovIX.ItemIndex      := QrVSDvlItsIxxMovIX.Value;
      FmVSDvlIts.EdIxxFolha.ValueVariant   := QrVSDvlItsIxxFolha.Value;
      FmVSDvlIts.EdIxxLinha.ValueVariant   := QrVSDvlItsIxxLinha.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSDvlIts.EdCustoMOTot.ValueVariant := QrVSDvlItsCustoMOTot.Value;
      FmVSDvlIts.EdValorT.ValueVariant     := QrVSDvlItsValorT.Value;
    end;
    FmVSDvlIts.ShowModal;
    FmVSDvlIts.Destroy;
  end;
end;

procedure TFmVSDvlCab.PackingList1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_106_00_A, [
  DModG.frxDsDono,
  frxDsPallets
  ]);
  MyObjects.frxMostra(frxWET_CURTI_106_00_A, 'Packing List');
end;

procedure TFmVSDvlCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSDvlCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSDvlCab, QrVSDvlIts);
end;

procedure TFmVSDvlCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSDvlCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSDvlIts);
  //MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrVSDvlIts, QrVSItsBxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSDvlIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSDvlItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrVSDvlIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrVSMOEnvAvu);
  AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSDvlIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
end;

procedure TFmVSDvlCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSDvlCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSDvlCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsdvlcab';
  VAR_GOTOMYSQLTABLE := QrVSDvlCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA');
  VAR_SQLx.Add('FROM vsdvlcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSDvlCab.Estoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSDvlCab.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSHisFchCodigo.Value;
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de hist�rico?',
  'vshisfch', 'Codigo', Codigo, DMod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSHisFch,
      QrVSHisFchCodigo, QrVSHisFchCodigo.Value);
    ReopenVSHisFch(Codigo);
  end;
end;

procedure TFmVSDvlCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.ImprimeFichaDePallet(InfoNO_PALLET: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  if QrVSDvlItsSdoVrtPeca.Value <= 0 then
    Geral.MB_Aviso('O Pallet s� ser� impresso se tiver saldo positivo de pe�as!');
  Empresa  := QrVSDvlItsEmpresa.Value;
  ClientMO := QrVSDvlItsClientMO.Value;
  Pallet   := QrVSDvlItsPallet.Value;
  TempTab  := Self.Name;
  VS_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_PALLET);
end;

procedure TFmVSDvlCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSDvlItsEmpresa.Value;
  N := 0;
  QrVSDvlIts.First;
  while not QrVSDvlIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSDvlItsPallet.Value;
    //
    QrVSDvlIts.Next;
  end;
  if N > 0 then
    VS_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSDvlCab.Inclui1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stIns);
end;

procedure TFmVSDvlCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSDvlIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, QrVSDvlCabide_Serie.Value, QrVSDvlCabide_nNF.Value);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSDvlCabMovimCod.Value, 0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.IncluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, QrVSDvlIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSDvlCabFornecedor.Value, QrVSDvlCabide_serie.Value, QrVSDvlCabide_nNF.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSDvlCabMovimCod.Value, 0);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.IrparajaneladegerenciamentodeFichaRMP1Click(
  Sender: TObject);
var
  SerieFicha, Ficha: Integer;
begin
  SerieFicha := QrVSDvlItsSerieFch.Value;
  Ficha      := QrVSDvlItsFicha.Value;
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
end;

procedure TFmVSDvlCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSDvlIts(stUpd);
end;

procedure TFmVSDvlCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSDvlCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSDvlCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSDvlCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrVSDvlItsCodigo.Value;
  MovimCod := QrVSDvlItsMovimCod.Value;
  Controle := QrVSDvlItsControle.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSDvlIts, TIntegerField(QrVSDvlItsControle),
  Controle, CtrlBaix, QrVSDvlItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti106)) then
  begin
    // Exclui tambem da tabela vsmovdif
    VS_PF.ExcluiVSNaoVMI('', 'vsmovdif', 'Controle', Controle, Dmod.MyDB);
    // Atualiza!
    VS_PF.AtualizaTotaisVSXxxCab('vsdvlcab', MovimCod);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSDvlCab.ReopenVSHisFch(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSHisFch, Dmod.MyDB, [
  'SELECT * ',
  'FROM vshisfch ',
  'WHERE VSMovIts=' + Geral.FF0(QrVSDvlItsControle.Value),
  'OR ( ',
  '  SerieFch=' + Geral.FF0(QrVSDvlItsSerieFch.Value),
  '  AND ',
  '  Ficha=' + Geral.FF0(QrVSDvlItsFicha.Value),
  ') ',
  '']);
  //
  if Codigo <> 0 then
    QrVSHisFch.Locate('Codigo', Codigo, []);
end;

procedure TFmVSDvlCab.ReopenVSDvlIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSDvlIts, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(wmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(wmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro ',
  'FROM v s m o v i t s wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrVSDvlCabMovimCod.Value),
  'ORDER BY NO_Pallet, wmi.Controle ',
  '']);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSDvlCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  //'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  '']);
  //'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSDvlCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSDvlIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSDvlIts);
  //
  QrVSDvlIts.Locate('Controle', Controle, []);
end;


procedure TFmVSDvlCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSDvlCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSDvlCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSDvlCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSDvlCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSDvlCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSDvlCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSDvlCabCodigo.Value;
  Close;
end;

procedure TFmVSDvlCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSDvlIts(stIns);
end;

procedure TFmVSDvlCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSDvlCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'vsdvlcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSDvlCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSDvlCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_serie, ide_nNF, emi_serie, emi_nNF: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_serie      := Edemi_serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  //
  if not VS_PF.ValidaCampoNF(11, Edide_nNF, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsdvlcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsdvlcab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_serie', 'ide_nNF',
  'emi_serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta, (* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)
  ClienteMO, Procednc, Motorista,
  Placa, ide_serie, ide_nNF,
  ide_serie, ide_nNF
  ], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidDevolucao, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'Terceiro', CO_DATA_HORA_VMI,
      'ClientMO'], ['MovimCod'], [
      Empresa, Terceiro, DataHora,
      ClienteMO], [MovimCod], True);
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormVSDvlIts(stIns);
  end;
end;

procedure TFmVSDvlCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsdvlcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsdvlcab', 'Codigo');
end;

procedure TFmVSDvlCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSDvlCab.BtReclasifClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmVSDvlCab.Altera1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stUpd);
end;

procedure TFmVSDvlCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSDvlIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, 0, 0);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSDvlCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stUpd, QrVSDvlIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSDvlCabFornecedor.Value, QrVSDvlCabide_serie.Value, QrVSDvlCabide_nNF.Value);

  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSDvlCabMovimCod.Value, 0);
  //
  LocCod(QrVSDvlCabCodigo.Value,QrVSDvlCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSDvlCab.Atualizaestoque1Click(Sender: TObject);
begin
  VS_PF.AtualizaSaldoIMEI(QrVSDvlItsControle.Value, True);
end;

procedure TFmVSDvlCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSDvlCabMovimCod.Value, TEstqMovimID.emidDevolucao, [(**)], [eminSemNiv]);
end;

procedure TFmVSDvlCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSDvlCab, QrVSDvlCabDtCompra.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSDvlCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PCItens.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSDvlCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSDvlCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSDvlCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDvlCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDvlCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDvlCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSDvlCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDvlCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSDvlCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSDvlCab.QrVSDvlCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSDvlCab.QrVSDvlCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSDvlIts(0);
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSDvlCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSDvlCabMovimCod.Value, 0);
end;

procedure TFmVSDvlCab.FichaCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichaDePallet(True);
end;

procedure TFmVSDvlCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSDvlCab.FichaSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichaDePallet(False);
end;

procedure TFmVSDvlCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSDvlCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSDvlCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSDvlCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidDevolucao, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSDvlIts(Controle);
  end;
end;

procedure TFmVSDvlCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDvlCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSDvlCab.frxWET_CURTI_006_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSDvlCab.frxWET_CURTI_106_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := QrVSDvlCabNO_EMPRESA.Value
  else
  if VarName = 'VARF_CLIENTE' then
    Value := QrVSDvlCabNO_FORNECE.Value
  else
  if VarName = 'VARF_ID' then
    Value := QrVSDvlCabCodigo.Value
  else
  if VarName ='VARF_DATA' then
    Value := Now()
end;

procedure TFmVSDvlCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSDvlCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSDvlCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsdvlcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

procedure TFmVSDvlCab.QrVSDvlCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvEnv.Close;
  QrVSMOEnvAvu.Close;
  QrVSDvlIts.Close;
end;

procedure TFmVSDvlCab.QrVSDvlCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSDvlCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSDvlCab.QrVSDvlItsAfterScroll(DataSet: TDataSet);
begin
  VS_PF.ReopenVSMovDif(QrVSMovDif, QrVSDvlItsControle.Value);
  ReopenVSHisFch(0);
end;

procedure TFmVSDvlCab.QrVSDvlItsBeforeClose(DataSet: TDataSet);
begin
  QrVSMovDif.Close;
  QrVSHisFch.Close;
end;

procedure TFmVSDvlCab.QrVSDvlItsCalcFields(DataSet: TDataSet);
begin
  QrVSDvlItsSEQ.Value := QrVSDvlIts.RecNo;
end;

procedure TFmVSDvlCab.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi, QrVSMOEnvAvuCodigo.VAlue, 0);
end;

procedure TFmVSDvlCab.QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSDvlCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVMI(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.VAlue, 0);
end;

procedure TFmVSDvlCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

end.

