object FmVSImpEstqReduz: TFmVSImpEstqReduz
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-266 :: Estoque de Reduzido'
  ClientHeight = 629
  ClientWidth = 853
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 853
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 805
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 757
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 260
        Height = 32
        Caption = 'Estoque de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 260
        Height = 32
        Caption = 'Estoque de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 260
        Height = 32
        Caption = 'Estoque de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 853
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 849
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 853
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 707
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 705
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 853
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 853
      Height = 121
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 428
        Top = 41
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
      end
      object Label55: TLabel
        Left = 428
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label10: TLabel
        Left = 8
        Top = 40
        Width = 61
        Height = 13
        Caption = 'Cliente M.O.:'
      end
      object Label1: TLabel
        Left = 8
        Top = 80
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 361
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTerceiro: TdmkEditCB
        Left = 428
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdStqCenCad: TdmkEditCB
        Left = 428
        Top = 55
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 484
        Top = 55
        Width = 361
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 4
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 484
        Top = 16
        Width = 361
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 5
        dmkEditCB = EdTerceiro
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClientMO: TdmkEditCB
        Left = 8
        Top = 57
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClientMOChange
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 64
        Top = 57
        Width = 361
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsClientMO
        TabOrder = 7
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 97
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 97
        Width = 781
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 9
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 0
      Top = 121
      Width = 853
      Height = 346
      Align = alClient
      DataSource = DsEstoque
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'IMEI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 161
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdGGX'
          Title.Caption = 'Ordem'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo Pe'#231'as'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo Peso kg'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Width = 43
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do terceiro'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Total kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 65524
    Top = 65523
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 32
    Top = 236
    object QrClientMONOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrClientMOCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 32
    Top = 284
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 124
    Top = 236
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 124
    Top = 284
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 216
    Top = 236
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 216
    Top = 284
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ggx.GraGruY,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 312
    Top = 240
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 312
    Top = 289
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.Empresa, wmi.GraGruX, wmi.SdoVrtPeca,  '
      'wmi.SdoVrtPeso, wmi.SdoVrtArM2,  '
      'FLOOR((wmi.SdoVrtArM2 / 0.09290304)) + '
      'FLOOR(((MOD((wmi.SdoVrtArM2 / 0.09290304), 1)) + '
      '0.12499) * 4) * 0.25 AreaP2, 0.00 ValorT,  '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wmi.Pallet, vsp.Nome NO_Pallet, '
      'wmi.Terceiro, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      '"" NO_STATUS, '
      '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, '
      'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, '
      'wmi.Codigo, wmi.MovimCod IMEC, wmi.Controle IMEI, '
      'wmi.MovimID, wmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, '
      '1 Ativo '
      'FROM bluederm_2_1_cialeather.vsmovits wmi '
      
        'LEFT JOIN bluederm_2_1_cialeather.vspalleta  vsp ON vsp.Codigo=w' +
        'mi.Pallet   '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragrux    ggx ON ggx.Controle' +
        '=IF(wmi.Pallet <> 0, vsp.GraGruX, wmi.GraGruX) '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruy    ggy ON ggy.Codigo=g' +
        'gx.GraGruY '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruc    ggc ON ggc.Controle' +
        '=ggx.GraGruC '
      
        'LEFT JOIN bluederm_2_1_cialeather.gracorcad  gcc ON gcc.Codigo=g' +
        'gc.GraCorCad '
      
        'LEFT JOIN bluederm_2_1_cialeather.gratamits  gti ON gti.Controle' +
        '=ggx.GraTamI '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragru1    gg1 ON gg1.Nivel1=g' +
        'gx.GraGru1 '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  ent ON ent.Codigo=w' +
        'mi.Terceiro '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  emp ON emp.Codigo=w' +
        'mi.Empresa '
      'WHERE wmi.Controle <> 0 '
      'AND wmi.GraGruX<>0 '
      'AND wmi.SdoVrtPeca > 0'
      'AND wmi.Pallet = 0'
      'AND wmi.Empresa=-11')
    Left = 400
    Top = 244
    object QrEstoqueControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstoqueEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstoqueGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstoquePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstoquePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstoqueAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstoqueValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEstoqueGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstoqueNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEstoquePallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstoqueNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrEstoqueTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstoqueNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstoqueNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstoqueNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Required = True
      Size = 0
    end
    object QrEstoqueDataHora: TWideStringField
      FieldName = 'DataHora'
      Required = True
      Size = 19
    end
    object QrEstoqueOrdGGX: TLargeintField
      FieldName = 'OrdGGX'
      Required = True
    end
    object QrEstoqueOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEstoqueCodiGGY: TIntegerField
      FieldName = 'CodiGGY'
      Required = True
    end
    object QrEstoqueNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEstoqueCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstoqueIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrEstoqueIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrEstoqueMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstoqueMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstoqueNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Required = True
      Size = 0
    end
    object QrEstoqueNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Required = True
      Size = 0
    end
    object QrEstoqueAtivo: TLargeintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEstoqueGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstoqueVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrEstoqueClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrEstoqueSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrEstoqueFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrEstoqueMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEstoqueNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrEstoqueSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstoqueSdoVrtArP2: TFloatField
      FieldName = 'SdoVrtArP2'
    end
    object QrEstoqueSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstoqueSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrEstoqueGGXInProc: TIntegerField
      FieldName = 'GGXInProc'
    end
    object QrEstoqueSdoVrtValr: TFloatField
      FieldName = 'SdoVrtValr'
    end
    object QrEstoqueKg_Peca: TFloatField
      FieldName = 'Kg_Peca'
    end
    object QrEstoqueValor_kg: TFloatField
      FieldName = 'Valor_kg'
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 400
    Top = 292
  end
  object frxWET_CURTI_266_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 45104.344349780100000000
    ReportOptions.LastChange = 45104.344349780100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_266_1GetValue
    Left = 520
    Top = 240
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstoque
        DataSetName = 'frxDsEstoque'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047244100000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149606299212600000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472455590000000000
          Top = 83.149606299212600000
          Width = 68.031496060000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 128.354360000000000000
          Top = 83.149606299212600000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ficha')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 189.031540000000000000
          Top = 83.149606299212600000
          Width = 90.708661420000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 279.684951500000000000
          Top = 83.149606299212600000
          Width = 207.874015748031500000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 820.157402200000000000
          Top = 83.149606299212600000
          Width = 75.590556060000000000
          Height = 18.897637800000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo Valor')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1012.914040000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 797.480830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de [VARF_GRAGRUX]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 502.677490000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 41.574830000000000000
          Width = 502.677490000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FORNECEDOR]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 60.472480000000000000
          Width = 502.677490000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_STQCENCAD]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 502.677490000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CLIENTMO]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559199130000000000
          Top = 83.149606299212600000
          Width = 188.976404800000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535650310000000000
          Top = 83.149606299212600000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo Pe'#231'as')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 733.228575910000000000
          Top = 83.149606299212600000
          Width = 86.929133860000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo Peso kg')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748041260000000000
          Top = 83.149606299212600000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/kg')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 952.441560000000000000
          Top = 83.149606299212600000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg/pe'#231'a')
          ParentFont = False
        end
      end
      object MD01: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 181.417440000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsEstoque
        DataSetName = 'frxDsEstoque'
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DataField = 'Controle'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."Controle"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472455590000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataField = 'NO_SerieFch'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstoque."NO_SerieFch"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 128.354360000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DataField = 'Ficha'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."Ficha"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 189.031540000000000000
          Width = 90.708661420000000000
          Height = 18.897637795275600000
          DataField = 'Marca'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstoque."Marca"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 279.684951500000000000
          Width = 37.795275590000000000
          Height = 18.897637795275600000
          DataField = 'Terceiro'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."Terceiro"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 317.480080630000000000
          Width = 170.078715750000000000
          Height = 18.897637800000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstoque."NO_FORNECE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 820.157402200000000000
          Width = 75.590556060000000000
          Height = 18.897637800000000000
          DataField = 'SdoVrtValr'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."SdoVrtValr"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559247950000000000
          Width = 37.795273150000000000
          Height = 18.897637800000000000
          DataField = 'GraGruX'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."GraGruX"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354499130000000000
          Width = 151.181104800000000000
          Height = 18.897637800000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstoque."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535650310000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."SdoVrtPeca"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 733.228575910000000000
          Width = 86.929133860000000000
          Height = 18.897637800000000000
          DataField = 'SdoVrtPeso'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."SdoVrtPeso"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748041260000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DataField = 'Valor_kg'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."Valor_kg"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DataField = 'Kg_Peca'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstoque."Kg_Peca"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 332.598640000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 725.669760000000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstoque."ValorT"]')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133890000000000000
        Top = 260.787570000000000000
        Width = 1009.134510000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 676.535625910000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 820.157621890000000000
          Width = 75.590556060000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstoque."ValorT">,MD01)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 56.692913390000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstoque."SdoVrtPeca">,MD01)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 733.228795600000000000
          Width = 86.929133860000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstoque."SdoVrtPeso">,MD01)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748260950000000000
          Width = 56.692913390000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsEstoque."ValorT">,MD01) / SUM(<frxDsEstoque."PesoKg">' +
              ',MD01) ]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 952.441779690000000000
          Width = 56.692913390000000000
          Height = 37.795275590000000000
          DataSet = frxDsEstoque
          DataSetName = 'frxDsEstoque'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsEstoque."SdoVrtPeso">,MD01) / SUM(<frxDsEstoque."SdoV' +
              'rtPeca">,MD01)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstoque: TfrxDBDataset
    UserName = 'frxDsEstoque'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'ValorT=ValorT'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_Pallet=NO_Pallet'
      'Terceiro=Terceiro'
      'NO_FORNECE=NO_FORNECE'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'Ordem=Ordem'
      'CodiGGY=CodiGGY'
      'Nome=Nome'
      'Codigo=Codigo'
      'IMEC=IMEC'
      'IMEI=IMEI'
      'MovimID=MovimID'
      'MovimNiv=MovimNiv'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'Ativo=Ativo'
      'GraGruY=GraGruY'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Marca=Marca'
      'NO_SerieFch=NO_SerieFch'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtArP2=SdoVrtArP2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'GGXInProc=GGXInProc'
      'SdoVrtValr=SdoVrtValr'
      'Kg_Peca=Kg_Peca'
      'Valor_kg=Valor_kg')
    DataSet = QrEstoque
    BCDToCurrency = False
    DataSetOptions = []
    Left = 400
    Top = 340
  end
end
