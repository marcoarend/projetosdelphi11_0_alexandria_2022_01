unit VSClassifOneNew;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, UnInternalConsts,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkDBLookupComboBox, dmkEditCB, UnDmkWeb, ShellAPI,
  UnProjGroup_Consts, UnGrl_Geral, UnGrl_Vars, UnAppEnums;

type
  TFmVSClassifOneNew = class(TForm)
    PanelOC: TPanel;
    PnBoxesAll: TPanel;
    PnBoxesT01: TPanel;
    PnBoxesT02: TPanel;
    PnBoxT1L1: TPanel;
    Panel2: TPanel;
    QrVSPaClaCab: TmySQLQuery;
    DsVSPaClaCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    DsVSGerArtNew: TDataSource;
    QrVSPaClaCabNO_TIPO: TWideStringField;
    QrVSPaClaCabGraGruX: TIntegerField;
    QrVSPaClaCabNome: TWideStringField;
    QrVSPaClaCabTipoArea: TSmallintField;
    QrVSPaClaCabEmpresa: TIntegerField;
    QrVSPaClaCabMovimCod: TIntegerField;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaClaCabNO_EMPRESA: TWideStringField;
    QrVSPallet1: TmySQLQuery;
    QrVSPallet1Codigo: TIntegerField;
    QrVSPallet1Nome: TWideStringField;
    QrVSPallet1Lk: TIntegerField;
    QrVSPallet1DataCad: TDateField;
    QrVSPallet1DataAlt: TDateField;
    QrVSPallet1UserCad: TIntegerField;
    QrVSPallet1UserAlt: TIntegerField;
    QrVSPallet1AlterWeb: TSmallintField;
    QrVSPallet1Ativo: TSmallintField;
    QrVSPallet1Empresa: TIntegerField;
    QrVSPallet1Status: TIntegerField;
    QrVSPallet1CliStat: TIntegerField;
    QrVSPallet1GraGruX: TIntegerField;
    QrVSPallet1NO_CLISTAT: TWideStringField;
    QrVSPallet1NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet1NO_STATUS: TWideStringField;
    DsVSPallet1: TDataSource;
    QrVSPallet2: TmySQLQuery;
    QrVSPallet2Codigo: TIntegerField;
    QrVSPallet2Nome: TWideStringField;
    QrVSPallet2Lk: TIntegerField;
    QrVSPallet2DataCad: TDateField;
    QrVSPallet2DataAlt: TDateField;
    QrVSPallet2UserCad: TIntegerField;
    QrVSPallet2UserAlt: TIntegerField;
    QrVSPallet2AlterWeb: TSmallintField;
    QrVSPallet2Ativo: TSmallintField;
    QrVSPallet2Empresa: TIntegerField;
    QrVSPallet2Status: TIntegerField;
    QrVSPallet2CliStat: TIntegerField;
    QrVSPallet2GraGruX: TIntegerField;
    QrVSPallet2NO_CLISTAT: TWideStringField;
    QrVSPallet2NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet2NO_STATUS: TWideStringField;
    DsVSPallet2: TDataSource;
    QrVSPallet3: TmySQLQuery;
    QrVSPallet3Codigo: TIntegerField;
    QrVSPallet3Nome: TWideStringField;
    QrVSPallet3Lk: TIntegerField;
    QrVSPallet3DataCad: TDateField;
    QrVSPallet3DataAlt: TDateField;
    QrVSPallet3UserCad: TIntegerField;
    QrVSPallet3UserAlt: TIntegerField;
    QrVSPallet3AlterWeb: TSmallintField;
    QrVSPallet3Ativo: TSmallintField;
    QrVSPallet3Empresa: TIntegerField;
    QrVSPallet3Status: TIntegerField;
    QrVSPallet3CliStat: TIntegerField;
    QrVSPallet3GraGruX: TIntegerField;
    QrVSPallet3NO_CLISTAT: TWideStringField;
    QrVSPallet3NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet3NO_STATUS: TWideStringField;
    DsVSPallet3: TDataSource;
    QrVSPallet4: TmySQLQuery;
    QrVSPallet4Codigo: TIntegerField;
    QrVSPallet4Nome: TWideStringField;
    QrVSPallet4Lk: TIntegerField;
    QrVSPallet4DataCad: TDateField;
    QrVSPallet4DataAlt: TDateField;
    QrVSPallet4UserCad: TIntegerField;
    QrVSPallet4UserAlt: TIntegerField;
    QrVSPallet4AlterWeb: TSmallintField;
    QrVSPallet4Ativo: TSmallintField;
    QrVSPallet4Empresa: TIntegerField;
    QrVSPallet4Status: TIntegerField;
    QrVSPallet4CliStat: TIntegerField;
    QrVSPallet4GraGruX: TIntegerField;
    QrVSPallet4NO_CLISTAT: TWideStringField;
    QrVSPallet4NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet4NO_STATUS: TWideStringField;
    DsVSPallet4: TDataSource;
    QrVSPallet5: TmySQLQuery;
    QrVSPallet5Codigo: TIntegerField;
    QrVSPallet5Nome: TWideStringField;
    QrVSPallet5Lk: TIntegerField;
    QrVSPallet5DataCad: TDateField;
    QrVSPallet5DataAlt: TDateField;
    QrVSPallet5UserCad: TIntegerField;
    QrVSPallet5UserAlt: TIntegerField;
    QrVSPallet5AlterWeb: TSmallintField;
    QrVSPallet5Ativo: TSmallintField;
    QrVSPallet5Empresa: TIntegerField;
    QrVSPallet5Status: TIntegerField;
    QrVSPallet5CliStat: TIntegerField;
    QrVSPallet5GraGruX: TIntegerField;
    QrVSPallet5NO_CLISTAT: TWideStringField;
    QrVSPallet5NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet5NO_STATUS: TWideStringField;
    DsVSPallet5: TDataSource;
    QrVSPallet6: TmySQLQuery;
    QrVSPallet6Codigo: TIntegerField;
    QrVSPallet6Nome: TWideStringField;
    QrVSPallet6Lk: TIntegerField;
    QrVSPallet6DataCad: TDateField;
    QrVSPallet6DataAlt: TDateField;
    QrVSPallet6UserCad: TIntegerField;
    QrVSPallet6UserAlt: TIntegerField;
    QrVSPallet6AlterWeb: TSmallintField;
    QrVSPallet6Ativo: TSmallintField;
    QrVSPallet6Empresa: TIntegerField;
    QrVSPallet6Status: TIntegerField;
    QrVSPallet6CliStat: TIntegerField;
    QrVSPallet6GraGruX: TIntegerField;
    QrVSPallet6NO_CLISTAT: TWideStringField;
    QrVSPallet6NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet6NO_STATUS: TWideStringField;
    DsVSPallet6: TDataSource;
    QrVSPalClaIts1: TmySQLQuery;
    QrVSPalClaIts1Controle: TIntegerField;
    QrVSPalClaIts1DtHrIni: TDateTimeField;
    DsVSPalClaIts1: TDataSource;
    QrVSPalClaIts2: TmySQLQuery;
    QrVSPalClaIts2Controle: TIntegerField;
    QrVSPalClaIts2DtHrIni: TDateTimeField;
    DsVSPalClaIts2: TDataSource;
    QrVSPalClaIts3: TmySQLQuery;
    QrVSPalClaIts3Controle: TIntegerField;
    QrVSPalClaIts3DtHrIni: TDateTimeField;
    DsVSPalClaIts3: TDataSource;
    QrVSPalClaIts4: TmySQLQuery;
    QrVSPalClaIts4Controle: TIntegerField;
    QrVSPalClaIts4DtHrIni: TDateTimeField;
    DsVSPalClaIts4: TDataSource;
    QrVSPalClaIts5: TmySQLQuery;
    QrVSPalClaIts5Controle: TIntegerField;
    QrVSPalClaIts5DtHrIni: TDateTimeField;
    DsVSPalClaIts5: TDataSource;
    QrVSPalClaIts6: TmySQLQuery;
    QrVSPalClaIts6Controle: TIntegerField;
    QrVSPalClaIts6DtHrIni: TDateTimeField;
    DsVSPalClaIts6: TDataSource;
    QrItens1: TmySQLQuery;
    DsItens1: TDataSource;
    QrSum1: TmySQLQuery;
    DsSum1: TDataSource;
    QrSum1Pecas: TFloatField;
    QrSum1AreaM2: TFloatField;
    QrSum1AreaP2: TFloatField;
    QrItens1Controle: TLargeintField;
    QrItens1Pecas: TFloatField;
    QrItens1AreaM2: TFloatField;
    QrItens1AreaP2: TFloatField;
    QrItens2: TmySQLQuery;
    QrItens2Controle: TLargeintField;
    QrItens2Pecas: TFloatField;
    QrItens2AreaM2: TFloatField;
    QrItens2AreaP2: TFloatField;
    DsItens2: TDataSource;
    QrSum2: TmySQLQuery;
    QrSum2Pecas: TFloatField;
    QrSum2AreaM2: TFloatField;
    QrSum2AreaP2: TFloatField;
    DsSum2: TDataSource;
    QrItens3: TmySQLQuery;
    QrItens3Controle: TLargeintField;
    QrItens3Pecas: TFloatField;
    QrItens3AreaM2: TFloatField;
    QrItens3AreaP2: TFloatField;
    DsItens3: TDataSource;
    QrSum3: TmySQLQuery;
    QrSum3Pecas: TFloatField;
    QrSum3AreaM2: TFloatField;
    QrSum3AreaP2: TFloatField;
    DsSum3: TDataSource;
    QrItens4: TmySQLQuery;
    QrItens4Controle: TLargeintField;
    QrItens4Pecas: TFloatField;
    QrItens4AreaM2: TFloatField;
    QrItens4AreaP2: TFloatField;
    DsItens4: TDataSource;
    QrSum4: TmySQLQuery;
    QrSum4Pecas: TFloatField;
    QrSum4AreaM2: TFloatField;
    QrSum4AreaP2: TFloatField;
    DsSum4: TDataSource;
    QrItens5: TmySQLQuery;
    QrItens5Controle: TLargeintField;
    QrItens5Pecas: TFloatField;
    QrItens5AreaM2: TFloatField;
    QrItens5AreaP2: TFloatField;
    DsItens5: TDataSource;
    QrSum5: TmySQLQuery;
    QrSum5Pecas: TFloatField;
    QrSum5AreaM2: TFloatField;
    QrSum5AreaP2: TFloatField;
    DsSum5: TDataSource;
    QrItens6: TmySQLQuery;
    QrItens6Controle: TLargeintField;
    QrItens6Pecas: TFloatField;
    QrItens6AreaM2: TFloatField;
    QrItens6AreaP2: TFloatField;
    DsItens6: TDataSource;
    QrSum6: TmySQLQuery;
    QrSum6Pecas: TFloatField;
    QrSum6AreaM2: TFloatField;
    QrSum6AreaP2: TFloatField;
    DsSum6: TDataSource;
    QrSumT_: TmySQLQuery;
    DsSumT: TDataSource;
    PnBoxT1L2: TPanel;
    DBGAll: TdmkDBGridZTO;
    QrAll: TmySQLQuery;
    QrAllControle: TLargeintField;
    QrAllPecas: TFloatField;
    QrAllAreaM2: TFloatField;
    QrAllAreaP2: TFloatField;
    DsAll: TDataSource;
    QrAllBox: TIntegerField;
    QrAllVSPaClaIts: TIntegerField;
    QrAllVSPallet: TIntegerField;
    PnBoxT1L3: TPanel;
    PnBoxT2L3: TPanel;
    PnBoxT2L2: TPanel;
    PnBoxT2L1: TPanel;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    Panel7: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    Panel12: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEDAreaT: TDBEdit;
    Panel13: TPanel;
    Label19: TLabel;
    Panel14: TPanel;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdit24: TDBEdit;
    PMItens01: TPopupMenu;
    EncerrarPallet01: TMenuItem;
    RemoverPallet01: TMenuItem;
    AdicionarPallet01: TMenuItem;
    PMItens02: TPopupMenu;
    AdicionarPallet02: TMenuItem;
    EncerrarPallet02: TMenuItem;
    RemoverPallet02: TMenuItem;
    QrVSPalClaIts1VMI_Baix: TIntegerField;
    QrVSPalClaIts1VMI_Dest: TIntegerField;
    QrVSPalClaIts2VMI_Dest: TIntegerField;
    QrVSPalClaIts3VMI_Dest: TIntegerField;
    QrVSPalClaIts4VMI_Dest: TIntegerField;
    QrVSPalClaIts5VMI_Dest: TIntegerField;
    QrVSPalClaIts6VMI_Dest: TIntegerField;
    QrAllVMI_Sorc: TIntegerField;
    QrAllVMI_Dest: TIntegerField;
    QrSumPal1: TmySQLQuery;
    DsSumPal1: TDataSource;
    QrSumPal1Pecas: TFloatField;
    QrSumPal1AreaM2: TFloatField;
    QrSumPal1AreaP2: TFloatField;
    QrSumPal2: TmySQLQuery;
    QrSumPal2Pecas: TFloatField;
    QrSumPal2AreaM2: TFloatField;
    QrSumPal2AreaP2: TFloatField;
    DsSumPal2: TDataSource;
    QrSumPal3: TmySQLQuery;
    QrSumPal3Pecas: TFloatField;
    QrSumPal3AreaM2: TFloatField;
    QrSumPal3AreaP2: TFloatField;
    DsSumPal3: TDataSource;
    QrSumPal4: TmySQLQuery;
    QrSumPal4Pecas: TFloatField;
    QrSumPal4AreaM2: TFloatField;
    QrSumPal4AreaP2: TFloatField;
    DsSumPal4: TDataSource;
    QrSumPal5: TmySQLQuery;
    QrSumPal5Pecas: TFloatField;
    QrSumPal5AreaM2: TFloatField;
    QrSumPal5AreaP2: TFloatField;
    DsSumPal5: TDataSource;
    QrSumPal6: TmySQLQuery;
    QrSumPal6Pecas: TFloatField;
    QrSumPal6AreaM2: TFloatField;
    QrSumPal6AreaP2: TFloatField;
    DsSumPal6: TDataSource;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    BtEncerra: TBitBtn;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    PMItens03: TPopupMenu;
    AdicionarPallet03: TMenuItem;
    EncerrarPallet03: TMenuItem;
    RemoverPallet03: TMenuItem;
    PMItens04: TPopupMenu;
    AdicionarPallet04: TMenuItem;
    EncerrarPallet04: TMenuItem;
    RemoverPallet04: TMenuItem;
    PMItens05: TPopupMenu;
    AdicionarPallet05: TMenuItem;
    EncerrarPallet05: TMenuItem;
    RemoverPallet05: TMenuItem;
    PMItens06: TPopupMenu;
    AdicionarPallet06: TMenuItem;
    EncerrarPallet06: TMenuItem;
    RemoverPallet06: TMenuItem;
    BtImprime: TBitBtn;
    BtDigitacao: TButton;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrVMIsDePal: TmySQLQuery;
    QrVMIsDePalVMI_Dest: TIntegerField;
    QrVSPalClaIts2VMI_Baix: TIntegerField;
    QrVSPalClaIts3VMI_Baix: TIntegerField;
    QrVSPalClaIts4VMI_Baix: TIntegerField;
    QrVSPalClaIts5VMI_Baix: TIntegerField;
    QrVSPalClaIts6VMI_Baix: TIntegerField;
    Panel42: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    Label22: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label24: TLabel;
    Label18: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit11: TDBEdit;
    Panel43: TPanel;
    Label71: TLabel;
    Label70: TLabel;
    CBRevisor: TdmkDBLookupComboBox;
    CBDigitador: TdmkDBLookupComboBox;
    EdDigitador: TdmkEditCB;
    EdRevisor: TdmkEditCB;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSMrtCad: TmySQLQuery;
    DsVSMrtCad: TDataSource;
    QrVSMrtCadCodigo: TIntegerField;
    QrVSMrtCadNome: TWideStringField;
    PnMartelo: TPanel;
    Label72: TLabel;
    CBVSMrtCad: TdmkDBLookupComboBox;
    EdVSMrtCad: TdmkEditCB;
    BtReabre: TBitBtn;
    QrVSPallet1QtdPrevPc: TIntegerField;
    QrVSPallet2QtdPrevPc: TIntegerField;
    QrVSPallet3QtdPrevPc: TIntegerField;
    QrVSPallet4QtdPrevPc: TIntegerField;
    QrVSPallet5QtdPrevPc: TIntegerField;
    QrVSPallet6QtdPrevPc: TIntegerField;
    ImprimirNmeroPallet1: TMenuItem;
    DadosPaletsemWordPad1: TMenuItem;
    Mensagemdedados1: TMenuItem;
    Mensagemdedados2: TMenuItem;
    Mensagemdedados3: TMenuItem;
    Mensagemdedados4: TMenuItem;
    Mensagemdedados5: TMenuItem;
    Mensagemdedados6: TMenuItem;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    Label85: TLabel;
    EdTempo: TEdit;
    N3: TMenuItem;
    rocarVichaRMP1: TMenuItem;
    CkMartelo: TCheckBox;
    CkSubClass: TCheckBox;
    PnSubClass: TPanel;
    Label86: TLabel;
    EdSubClass: TdmkEdit;
    Panel1: TPanel;
    PnEqualize: TPanel;
    Panel70: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    CkNota: TCheckBox;
    DBGrid2: TDBGrid;
    PnNota: TPanel;
    DBEdit42: TDBEdit;
    QrAllSubClass: TWideStringField;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnBox01: TPanel;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label54: TLabel;
    DBEdit4: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdArea1: TDBEdit;
    DBEdit50: TDBEdit;
    GroupBox2: TGroupBox;
    Panel16: TPanel;
    Label52: TLabel;
    Label55: TLabel;
    Label53: TLabel;
    Label73: TLabel;
    DBEdSumPcPal01: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit57: TDBEdit;
    EdMedia1: TdmkEdit;
    DBGPallet1: TdmkDBGridZTO;
    Memo1: TMemo;
    Panel4: TPanel;
    Panel8: TPanel;
    Panel11: TPanel;
    DBEdit3: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit1: TDBEdit;
    Panel50: TPanel;
    Label79: TLabel;
    Panel51: TPanel;
    Panel64: TPanel;
    CkSubClass1: TCheckBox;
    PnBox04: TPanel;
    DBGPallet4: TdmkDBGridZTO;
    Panel33: TPanel;
    GroupBox7: TGroupBox;
    Panel36: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label51: TLabel;
    DBEdit28: TDBEdit;
    DBEdSumPcIMEI04: TDBEdit;
    DBEdArea4: TDBEdit;
    DBEdit56: TDBEdit;
    GroupBox8: TGroupBox;
    Panel37: TPanel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label76: TLabel;
    DBEdSumPcPal04: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    EdMedia4: TdmkEdit;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel47: TPanel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit51: TDBEdit;
    Panel48: TPanel;
    Label81: TLabel;
    Panel49: TPanel;
    Panel61: TPanel;
    CkSubClass4: TCheckBox;
    PnBox06: TPanel;
    PnBox03: TPanel;
    DBGPallet3: TdmkDBGridZTO;
    Panel22: TPanel;
    GroupBox5: TGroupBox;
    Panel34: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label36: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdArea3: TDBEdit;
    DBEdit61: TDBEdit;
    GroupBox6: TGroupBox;
    Panel35: TPanel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label75: TLabel;
    DBEdSumPcPal03: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    EdMedia3: TdmkEdit;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel55: TPanel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit30: TDBEdit;
    Panel56: TPanel;
    Label83: TLabel;
    Panel57: TPanel;
    Panel65: TPanel;
    CkSubClass3: TCheckBox;
    DBGPallet6: TdmkDBGridZTO;
    Panel25: TPanel;
    GroupBox11: TGroupBox;
    Panel40: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    DBEdit40: TDBEdit;
    DBEdSumPcIMEI06: TDBEdit;
    DBEdArea6: TDBEdit;
    DBEdit72: TDBEdit;
    GroupBox12: TGroupBox;
    Panel41: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label78: TLabel;
    DBEdSumPcPal06: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    EdMedia6: TdmkEdit;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel58: TPanel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit37: TDBEdit;
    Panel59: TPanel;
    Label84: TLabel;
    Panel60: TPanel;
    Panel66: TPanel;
    CkSubClass6: TCheckBox;
    PnBox02: TPanel;
    PnBox05: TPanel;
    DBGPallet2: TdmkDBGridZTO;
    Panel19: TPanel;
    GroupBox3: TGroupBox;
    Panel26: TPanel;
    Label5: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    DBEdit15: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdArea2: TDBEdit;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel30: TPanel;
    Label31: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label74: TLabel;
    DBEdSumPcPal02: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    EdMedia2: TdmkEdit;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel44: TPanel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit22: TDBEdit;
    Panel45: TPanel;
    Label80: TLabel;
    Panel46: TPanel;
    Panel62: TPanel;
    CkSubClass2: TCheckBox;
    DBGPallet5: TdmkDBGridZTO;
    Panel29: TPanel;
    GroupBox9: TGroupBox;
    Panel38: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label46: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdArea5: TDBEdit;
    DBEdit68: TDBEdit;
    GroupBox10: TGroupBox;
    Panel39: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label77: TLabel;
    DBEdSumPcPal05: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    EdMedia5: TdmkEdit;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel52: TPanel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit44: TDBEdit;
    Panel53: TPanel;
    Label82: TLabel;
    Panel54: TPanel;
    Panel63: TPanel;
    CkSubClass5: TCheckBox;
    rocarIMEI1: TMenuItem;
    QrVSPallet1FatorInt: TFloatField;
    QrVSPallet2FatorInt: TFloatField;
    QrVSPallet3FatorInt: TFloatField;
    QrVSPallet4FatorInt: TFloatField;
    QrVSPallet5FatorInt: TFloatField;
    QrVSPallet6FatorInt: TFloatField;
    QrVSGerArtNewFatorInt: TFloatField;
    PnIntMei1: TPanel;
    PnIntMei4: TPanel;
    PnIntMei2: TPanel;
    PnIntMei5: TPanel;
    PnIntMei6: TPanel;
    PnIntMei3: TPanel;
    QrSumT: TmySQLQuery;
    QrSumTAreaM2: TFloatField;
    QrSumT_Pecas: TFloatField;
    QrSumT_AreaM2: TFloatField;
    QrSumT_AreaP2: TFloatField;
    QrSumT_FALTA_PECA: TFloatField;
    QrSumT_FALTA_AREA: TFloatField;
    QrSumT_IntOuPart: TFloatField;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTPecas: TFloatField;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoIMEI2: TMenuItem;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    QrAllNO_MARTELO: TWideStringField;
    QrVSGerArtNewClientMO: TIntegerField;
    QrVSGerArtNewFornecMO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaClaCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDigitacaoClick(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtReabreClick(Sender: TObject);
    procedure DBEdSumPcPal06Change(Sender: TObject);
    procedure DBEdSumPcPal05Change(Sender: TObject);
    procedure DBEdSumPcPal02Change(Sender: TObject);
    procedure DBEdSumPcPal04Change(Sender: TObject);
    procedure DBEdSumPcPal01Change(Sender: TObject);
    procedure DBEdSumPcPal03Change(Sender: TObject);
    procedure ImprimirNmeroPallet1Click(Sender: TObject);
    procedure DadosPaletsemWordPad1Click(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure rocarVichaRMP1Click(Sender: TObject);
    procedure CkSubClassClick(Sender: TObject);
    procedure CkMarteloClick(Sender: TObject);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    procedure EdSubClassExit(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure rocarIMEI1Click(Sender: TObject);
    procedure QrVSPallet1AfterOpen(DataSet: TDataSet);
    procedure QrVSGerArtNewAfterOpen(DataSet: TDataSet);
    procedure QrVSPallet2AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet3AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet4AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet5AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet6AfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoIMEI1Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoIMEI2Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCriando: Boolean;
    FDifTime: TDateTime;

    //
(*
    FLast_vsgerarta, FLast_vspaclacaba, FLast_LstPal01, FLast_LstPal02,
    FLast_LstPal03, FLast_LstPal04, FLast_LstPal05, FLast_LstPal06: Double;
*)
    //
    function  EncerraOC(Forca: Boolean): Boolean;
    procedure RealinhaBoxes();
    procedure ReopenVSGerArtDst();
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalClaIts:
              TmySQLQuery; Pallet: Integer; QrVSPalletFatorInt: TFloatField);
    procedure ReopenItens(VSPaClaIts, VSPallet: Integer; QrIts, QrSum,
              QrSumPal: TmySQLQuery);
    procedure InsereCouroAtual();
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure AtualizaInfoOC();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    function  BoxDeComp(Compo: TObject): Integer;
    function  ObtemDadosBox(const Box: Integer; var VSPaClaIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet, QrItens, QrSum,
              QrSumPal: TmySQLQuery): Boolean;
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure AdicionaPallet(Box: Integer);
    procedure ReopenSumPal(QrSumPal: TmySQLQuery; VSPallet: Integer);
    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure LiberaDigitacao();
    function  SubstituiOC(): Boolean;
    procedure VerificaBoxes();
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenEqualize(Automatico: Boolean);
    procedure TentarFocarEdArea();
    procedure ConfigPnSubClass();
    procedure ReconfiguraPaineisIntMei(Qry: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoIMEI();
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    //
    procedure ReopenVSPaClaCab();
  end;

var
  FmVSClassifOneNew: TFmVSClassifOneNew;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAdd,
  {$IfDef sAllVS}UnVS_PF,{$EndIf}
  MyDBCheck, UnVS_CRC_PF, VSClaArtPrpMDz, VSPalNumImp, UnDmkProcFunc;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX_06] of Boolean;

procedure TFmVSClassifOneNew.AdicionaPallet(Box: Integer);
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAdd, FmVSClaArtPalAdd, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAdd.FMovimIDGer              := emidClassArtXXUni;
    FmVSClaArtPalAdd.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAdd.FFornecMO                := QrVSGerArtNewFornecMO.Value;
    FmVSClaArtPalAdd.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAdd.FFornecedor              := QrVSGerArtNewTerceiro.Value;
    FmVSClaArtPalAdd.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAdd.FMovimID                 := FMovimID;
    FmVSClaArtPalAdd.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAdd.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAdd.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAdd.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAdd.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAdd.EdCodigo.ValueVariant    := QrVSPaClaCabCodigo.Value;
    FmVSClaArtPalAdd.EdSrcPallet.ValueVariant := 0;
    FmVSClaArtPalAdd.FExigeSrcPallet          := False;
    FmVSClaArtPalAdd.EdMovimCod.ValueVariant  := QrVSPaClaCabMovimCod.Value;
    FmVSClaArtPalAdd.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAdd.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAdd.FBox := Box;
    FmVSClaArtPalAdd.PnBox.Caption := Geral.FF0(Box);
    //
    FmVSClaArtPalAdd.FPal01 := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPalAdd.FPal02 := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPalAdd.FPal03 := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPalAdd.FPal04 := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPalAdd.FPal05 := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPalAdd.FPal06 := QrVSPaClaCabLstPal06.Value;
    //
    VS_CRC_PF.ReopenVSPallet(FmVSClaArtPalAdd.QrVSPallet,
      FmVSClaArtPalAdd.FEmpresa, FmVSClaArtPalAdd.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAdd.ShowModal;
    FmVSClaArtPalAdd.Destroy;
    //
    ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNew.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNew.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSClassifOneNew.AtualizaInfoOC();
var
  AreaT: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ',
  'cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ',
  'cia.SubClass, mrt.Nome NO_MARTELO',
  'FROM vscacitsa cia',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo',
  'WHERE cia.CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY cia.Controle DESC ',
(*
  'SELECT Controle, VSPaClaIts, VSPallet, Pecas, ',
  'AreaM2, AreaP2, VMI_Sorc, VMI_Dest, Box, ',
  'SubClass',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY Controle DESC ',
*)
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
(*
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(Pecas * FatorIntDst / FatorIntSrc) IntOuPart ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
*)
  'SELECT Pecas, AreaM2, SdoVrtPeca, SdoVrtArM2',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(QrVSGerArtNewControle.Value),
  '']);
  //
(*
  if QrSumTAreaM2.Value > 0 then
  begin
    if QrVSPaClaCabTipoArea.Value = 0 then // m2
    begin
      AreaT := QrSumTAreaM2.Value;
      //
      EdPercent1.ValueVariant := QrSum1AreaM2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaM2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaM2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaM2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaM2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaM2.Value / AreaT * 100;
    end else
    begin
      AreaT := Geral.ConverteArea(QrSumTAreaM2.Value, ctM2toP2, cfQuarto);
      //
      EdPercent1.ValueVariant := QrSum1AreaP2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaP2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaP2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaP2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaP2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaP2.Value / AreaT * 100;
    end;
  end else
  begin
    EdPercent1.ValueVariant := 0;
    EdPercent2.ValueVariant := 0;
    EdPercent3.ValueVariant := 0;
    EdPercent4.ValueVariant := 0;
    EdPercent5.ValueVariant := 0;
    EdPercent6.ValueVariant := 0;
  end;
  //
*)
  if QrSumPal1Pecas.Value > 0 then
    EdMedia1.ValueVariant := QrSumPal1AreaM2.Value / QrSumPal1Pecas.Value
  else
    EdMedia1.ValueVariant := 0;
  //
  if QrSumPal2Pecas.Value > 0 then
    EdMedia2.ValueVariant := QrSumPal2AreaM2.Value / QrSumPal2Pecas.Value
  else
    EdMedia2.ValueVariant := 0;
  //
  if QrSumPal3Pecas.Value > 0 then
    EdMedia3.ValueVariant := QrSumPal3AreaM2.Value / QrSumPal3Pecas.Value
  else
    EdMedia3.ValueVariant := 0;
  //
  if QrSumPal4Pecas.Value > 0 then
    EdMedia4.ValueVariant := QrSumPal4AreaM2.Value / QrSumPal4Pecas.Value
  else
    EdMedia4.ValueVariant := 0;
  //
  if QrSumPal5Pecas.Value > 0 then
    EdMedia5.ValueVariant := QrSumPal5AreaM2.Value /  QrSumPal5Pecas.Value
  else
    EdMedia5.ValueVariant := 0;
  //
  if QrSumPal6Pecas.Value > 0 then
    EdMedia6.ValueVariant := QrSumPal6AreaM2.Value /  QrSumPal6Pecas.Value
  else
    EdMedia6.ValueVariant := 0;
end;

function TFmVSClassifOneNew.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  Result := Geral.IMV(CompName[Length(CompName)]);
end;

function TFmVSClassifOneNew.BoxInBoxes(Box: Integer): Boolean;
(*
var
  I: Integer;
*)
begin
(*
  Result := False;
  if Box <> 0 then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if FBoxes[I] then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
*)
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX_06) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSClassifOneNew.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSClassifOneNew.BtImprimeClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, 4);
end;

procedure TFmVSClassifOneNew.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNew.CkMarteloClick(Sender: TObject);
begin
  PnMartelo.Visible := CkMartelo.Checked;
end;

procedure TFmVSClassifOneNew.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSClassifOneNew.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNew.CkSubClassClick(Sender: TObject);
begin
  ConfigPnSubClass();
end;

procedure TFmVSClassifOneNew.ConfigPnSubClass();
begin
  if not CkSubClass.Checked then
    CkSubClass.Checked :=
    (CkSubClass1.Checked and PnBox01.Visible) or
    (CkSubClass2.Checked and PnBox02.Visible)  or
    (CkSubClass3.Checked and PnBox03.Visible)  or
    (CkSubClass4.Checked and PnBox04.Visible)  or
    (CkSubClass5.Checked and PnBox05.Visible)  or
    (CkSubClass6.Checked and PnBox06.Visible) ;
  //
  PnSubClass.Visible := CkSubClass.Checked;
end;

procedure TFmVSClassifOneNew.DadosPaletsemWordPad1Click(Sender: TObject);
(*
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    ReAviso.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
*)
var
  Dir, NomeArq, Arq: String;
begin
  Dir := 'C:\Dermatek\OCsCapt\';
  NomeArq := 'OC' + DBEdCodigo.Text +
    Geral.SoNumero_TT(Geral.FDT(DModG.ObtemAgora(), 109)) + '.jpg';
  try
    Arq := DmkWeb.CapturaTela(Dir, NomeArq);
    ShellExecute(Application.Handle, 'open', pchar(Arq), nil, nil, sw_ShowNormal);
  except
    ForceDirectories(Dir);
    raise;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal01Change(Sender: TObject);
begin
  if (QrSumPal1.State <> dsInactive) and (QrVSPAllet1.State <> dsInactive) then
  begin
    if QrSumPal1Pecas.Value >= QrVSPAllet1QtdPrevPc.Value then
    begin
      DBEdSumPcPal01.Font.Style := DBEdSumPcPal01.Font.Style + [fsBold];
      DBEdSumPcPal01.Font.Color := clRed;
      DBEdSumPcPal01.Color := clYellow;
    end else
    begin
      DBEdSumPcPal01.Font.Style := DBEdSumPcPal01.Font.Style - [fsBold];
      DBEdSumPcPal01.Font.Color := clWindowText;
      DBEdSumPcPal01.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal02Change(Sender: TObject);
begin
  if (QrSumPal2.State <> dsInactive) and (QrVSPAllet2.State <> dsInactive) then
  begin
    if QrSumPal2Pecas.Value >= QrVSPAllet2QtdPrevPc.Value then
    begin
      DBEdSumPcPal02.Font.Style := DBEdSumPcPal02.Font.Style + [fsBold];
      DBEdSumPcPal02.Font.Color := clRed;
      DBEdSumPcPal02.Color := clYellow;
    end else
    begin
      DBEdSumPcPal02.Font.Style := DBEdSumPcPal02.Font.Style - [fsBold];
      DBEdSumPcPal02.Font.Color := clWindowText;
      DBEdSumPcPal02.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal03Change(Sender: TObject);
begin
  if (QrSumPal3.State <> dsInactive) and (QrVSPAllet3.State <> dsInactive) then
  begin
    if QrSumPal3Pecas.Value >= QrVSPAllet3QtdPrevPc.Value then
    begin
      DBEdSumPcPal03.Font.Style := DBEdSumPcPal03.Font.Style + [fsBold];
      DBEdSumPcPal03.Font.Color := clRed;
      DBEdSumPcPal03.Color := clYellow;
    end else
    begin
      DBEdSumPcPal03.Font.Style := DBEdSumPcPal03.Font.Style - [fsBold];
      DBEdSumPcPal03.Font.Color := clWindowText;
      DBEdSumPcPal03.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal04Change(Sender: TObject);
begin
  if (QrSumPal4.State <> dsInactive) and (QrVSPAllet4.State <> dsInactive) then
  begin
    if QrSumPal4Pecas.Value >= QrVSPAllet4QtdPrevPc.Value then
    begin
      DBEdSumPcPal04.Font.Style := DBEdSumPcPal04.Font.Style + [fsBold];
      DBEdSumPcPal04.Font.Color := clRed;
      DBEdSumPcPal04.Color := clYellow;
    end else
    begin
      DBEdSumPcPal04.Font.Style := DBEdSumPcPal04.Font.Style - [fsBold];
      DBEdSumPcPal04.Font.Color := clWindowText;
      DBEdSumPcPal04.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal05Change(Sender: TObject);
begin
  if (QrSumPal5.State <> dsInactive) and (QrVSPAllet5.State <> dsInactive) then
  begin
    if QrSumPal5Pecas.Value >= QrVSPAllet5QtdPrevPc.Value then
    begin
      DBEdSumPcPal05.Font.Style := DBEdSumPcPal05.Font.Style + [fsBold];
      DBEdSumPcPal05.Font.Color := clRed;
      DBEdSumPcPal05.Color := clYellow;
    end else
    begin
      DBEdSumPcPal05.Font.Style := DBEdSumPcPal05.Font.Style - [fsBold];
      DBEdSumPcPal05.Font.Color := clWindowText;
      DBEdSumPcPal05.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.DBEdSumPcPal06Change(Sender: TObject);
begin
  if (QrSumPal6.State <> dsInactive) and (QrVSPAllet6.State <> dsInactive) then
  begin
    if QrSumPal6Pecas.Value >= QrVSPAllet6QtdPrevPc.Value then
    begin
      DBEdSumPcPal06.Font.Style := DBEdSumPcPal06.Font.Style + [fsBold];
      DBEdSumPcPal06.Font.Color := clRed;
      DBEdSumPcPal06.Color := clYellow;
    end else
    begin
      DBEdSumPcPal06.Font.Style := DBEdSumPcPal06.Font.Style - [fsBold];
      DBEdSumPcPal06.Font.Color := clWindowText;
      DBEdSumPcPal06.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneNew.BtDigitacaoClick(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Ant, Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
  Boxes: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdit21.Text;
  //
  if InputQuery('Itens a classificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    Boxes := 0;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    begin
      if FBoxes[I] then
        Boxes := Boxes + 1;
    end;
    //
    Ant := 0;
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Trunc(Random(1000) / 166.666); //  Random(6)
        if BoxInBoxes(Box) then
        begin
          if ((Boxes > 2) and (Ant <> Box))
          or (Boxes <=2) then
          begin
            OK := True;
            EdBox.ValueVariant := Box;
            Ant := Box;
          end;
        end;
      end;
      if QrSumTSdoVrtPeca.Value > 1 then
      begin
        Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
        Randomize;
        Desvio := Random(3000);
        Fator := 1 + ((Desvio - 1500) / 10000);
        Area := Area * Fator;
      end else
        Area := QrSumTSdoVrtArM2.Value;
      EdArea.ValueVariant := Area * 100;
      InsereCouroAtual();
    end;
  end;
end;

procedure TFmVSClassifOneNew.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
  //if (EdBox.ValueVariant > 0) and (EdBox.ValueVariant <= VAR_CLA_ART_RIB_MAX_BOX) then
    if PnSubClass.Visible then
      EdSubClass.SetFocus
    else
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end else
  if (EdBox.Text = '-') then
    UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNew.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNew.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNew.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSClassifOneNew.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSClassifOneNew.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
begin
(*
  if Key = VK_RETURN then
  begin
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
*)
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
    end;
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
end;

procedure TFmVSClassifOneNew.EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereCouroAtual();
end;

function TFmVSClassifOneNew.EncerraOC(Forca: Boolean): Boolean;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
var
  DtHrFimCla: String;
  Codigo, I, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
begin
  Result := False;
  if Forca or (Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES) then
  begin
    { DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaClaCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    }
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    Codigo     := QrVSGerArtNewCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      //'DtHrFimCla'], ['Codigo'], [DtHrFimCla
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06
      ], [Codigo], True);
(*
      then
      begin
        if not SubstituiOC() then
          Close;
      end;
*)
    end;
  end;

end;

function TFmVSClassifOneNew.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  FromBox: Variant;
begin
  Result := False;
  //
  FromBox := Box;
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  Result := VS_CRC_PF.EncerraPalletNew(VSPallet, Pergunta);
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNew.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNew.EstaOCOrdemdeclassificao1Click(Sender: TObject);
begin
  if EncerraOC(False) then
        if not SubstituiOC() then
          Close;
end;

procedure TFmVSClassifOneNew.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNew.UpdDelCouroSelecionado(SQLTipo: TSQLType);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt: String;
  Continua: Boolean;
  //
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaClaIts := QrAllVSPaClaIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      Pecas  := 1;
      //
      if QrVSPaClaCabTipoArea.Value = 0 then
        Txt := FloatToStr(QrAllAreaM2.Value * 100)
      else
        Txt := FloatToStr(QrAllAreaP2.Value * 100);
      //
      if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
      begin
        AreaM2 := Geral.IMV(Txt);
        //
        if QrVSPaClaCabTipoArea.Value = 0 then
        begin
          AreaM2 := AreaM2 / 100;
          AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
        end else
        begin
          AreaP2 := AreaM2 / 100;
          AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
        end;
        Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                      'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                      [Controle], VAR_InsUpd_AWServerID);
      end;
    end;
    stDel:
    begin
      Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle,
      Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_CRC_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
    Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
    //
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    //
    ReopenItens(All_VSPaClaIts, All_VSPallet, QrItens, QrSum, QrSumPal);
    AtualizaInfoOC();
  end;
end;

procedure TFmVSClassifOneNew.VerificaBoxes();
begin
(*
  if not Mostrartodosboxes1.Checked then
  begin
    PnBoxT1L1.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBoxT1L2.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBoxT1L3.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBoxT2L1.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBoxT2L2.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBoxT2L3.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBoxT1L1.Visible := True;
    PnBoxT1L2.Visible := True;
    PnBoxT1L3.Visible := True;
    PnBoxT2L1.Visible := True;
    PnBoxT2L2.Visible := True;
    PnBoxT2L3.Visible := True;
  end;
*)
  if not Mostrartodosboxes1.Checked then
  begin
    PnBox01.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBox02.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBox03.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBox04.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBox05.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBox06.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBox01.Visible := True;
    PnBox02.Visible := True;
    PnBox03.Visible := True;
    PnBox04.Visible := True;
    PnBox05.Visible := True;
    PnBox06.Visible := True;
  end;
  if ((PnBoxT1L3.Visible = False) or (PnBoxT1L1.Visible = False)) and (PnBoxT1L2.Visible = True) then
    PnBoxT1L2.Align := alLeft
  else
    PnBoxT1L2.Align := alClient;
  //
  if ((PnBoxT2L3.Visible = False) or (PnBoxT2L1.Visible = False)) and (PnBoxT2L2.Visible = True) then
    PnBoxT2L2.Align := alLeft
  else
    PnBoxT2L2.Align := alClient;
  //
  RealinhaBoxes();
end;

procedure TFmVSClassifOneNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSClassifOneNew.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriando := True;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    FBoxes[I] := False;
  RealinhaBoxes();
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
  //
  DBGAll.PopupMenu := PMAll;
  //
  BtDigitacao.Enabled := VAR_USUARIO = -1;
end;

procedure TFmVSClassifOneNew.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_MULTIPLY then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
  end;
end;

procedure TFmVSClassifOneNew.FormResize(Sender: TObject);
begin
  RealinhaBoxes();
end;

procedure TFmVSClassifOneNew.ImprimeFluxoDeMovimentoDoIMEI();
begin
  {$IfDef sAllVS}
  VS_PF.MostraRelatoroFluxoIMEI(QrVSGerArtNewControle.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSClassifOneNew.ImprimirfluxodemovimentodoIMEI1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNew.ImprimirfluxodemovimentodoIMEI2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNew.ImprimirNmeroPallet1Click(Sender: TObject);
  procedure ConfigBox(Box: Integer; QrVSPallet: TmySQLQuery; Check: TCheckBox);
  var
    Pallet: Integer;
  begin
    Pallet := QrVSPallet.FieldByName('Codigo').AsInteger;
    if (QrVSPallet1.State = dsInactive) then
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box);
      Check.Enabled := False;
      FmVSPalNumImp.Boxes[Box] := 0;
    end else
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box) + ' - Pallet ' + Geral.FF0(Pallet);
      Check.Enabled := True;
      FmVSPalNumImp.Boxes[Box] := Pallet;
    end;
  end;
begin
  if DBCheck.CriaFm(TFmVSPalNumImp, FmVSPalNumImp, afmoNegarComAviso) then
  begin
    ConfigBox(1, QrVSPallet1, FmVSPalNumImp.Ck1);
    ConfigBox(2, QrVSPallet2, FmVSPalNumImp.Ck2);
    ConfigBox(3, QrVSPallet3, FmVSPalNumImp.Ck3);
    ConfigBox(4, QrVSPallet4, FmVSPalNumImp.Ck4);
    ConfigBox(5, QrVSPallet5, FmVSPalNumImp.Ck5);
    ConfigBox(6, QrVSPallet6, FmVSPalNumImp.Ck6);
    //
    FmVSPalNumImp.ShowModal;
    FmVSPalNumImp.Destroy;
  end;
end;

procedure TFmVSClassifOneNew.InsereCouroAtual;
var
  VSPaClaIts: Integer;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Tempo: TDateTime;
var
  DataHora, SubClass: String;
  CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri, VSPaRclIts, VSPallet, VMI_Sorc,
  VMI_Baix, VMI_Dest, Box, Revisor, Digitador, Martelo: Integer;
  Controle: Int64;
  Pecas, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
begin
  Tempo := Now();
  //
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  //
  if MyObjects.FIC(Revisor = 0, EdRevisor, 'Informe o classificador!') then
    Exit;
  if MyObjects.FIC(Digitador = 0, EdDigitador, 'Informe o digitador!') then
    Exit;
  //
  CacCod         := FCacCod;
  CacID          := Integer(emidClassArtXXUni);
  Codigo         := FCodigo;
  Controle       := 0;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  //
  if PnMartelo.Visible then
  begin
    if CBVSMrtCad.KeyValue = QrVSMrtCadNome.Value then
      Martelo := QrVSMrtCadCodigo.Value
    else
      Martelo := 0;
  end else
    Martelo := 0;
  //
  //VSPaClaIts     := ;   ver abaixo!!!
  VSPaRclIts     := 0;
  //VSPallet       := ;
  //VMI_Sorc       := ;
  //VMI_Dest       := ;
  Box            := EdBox.ValueVariant;
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  DataHora       := Geral.FDT(Now() + FDifTime, 109);
  if PnSubClass.Visible then
    SubClass     := dmkPF.SoTextoLayout(EdSubClass.Text)
  else
    SubClass     := '';
//begin
  if EdArea.ValueVariant < 0.01 then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
    Exit;
  end;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    Pecas          := 1;
    //
    if QrVSPaClaCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
      Exit;
    if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
      Exit;
    //
    //
    FatorIntSrc    := QrVSGerArtNewFatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
   //
    VMI_Sorc := QrVSGerArtNewControle.Value;
    //VMI_Dest :=
    Controle   := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);
    ClaAPalOri := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'Martelo', CO_DATA_HORA_GRL,
    'SubClass', 'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix, VMI_Dest,
    Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, Martelo, DataHora,
    SubClass, FatorIntSrc, FatorIntDst], [
    Controle], VAR_InsUpd_AWServerID) then
    begin
      VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
        QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      //
      EdSubClass.Text     := '';
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
      //
      ReopenItens(VSPaClaIts, VSPallet, QrItens, QrSum, QrSumPal);
      AtualizaInfoOC();
      //
      Tempo := Now() - Tempo;
      EdTempo.Text := FormatDateTime('ss:zzz', Tempo);
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSClassifOneNew.LiberaDigitacao();
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  BtDigitacao.Visible := Libera;
end;

procedure TFmVSClassifOneNew.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  QrSum, QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
begin
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
  case Box of
    1:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      //EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      QrSum     := QrSum2;
      QrSumPal  := QrSumPal2;
      //EdPercent := EdPercent2;
      EdMedia   := EdMedia2;
    end;
    3:
    begin
      QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      //EdPercent := EdPercent3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      //EdPercent := EdPercent4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      //EdPercent := EdPercent5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      //EdPercent := EdPercent6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaClaCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O DA FICHA RMP:  ' + QrVSGerArtNewNO_Ficha.Value);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
end;

procedure TFmVSClassifOneNew.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSClassifOneNew.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSClassifOneNew.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  case Box of
    1: Result := CkSubClass1.Checked;
    2: Result := CkSubClass2.Checked;
    3: Result := CkSubClass3.Checked;
    4: Result := CkSubClass4.Checked;
    5: Result := CkSubClass5.Checked;
    6: Result := CkSubClass6.Checked;
  end;
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSClassifOneNew.ObtemDadosBox(const Box: Integer; var
VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
begin
  VMI_Sorc := QrVSGerArtNewControle.Value;
  //
  case Box of
    1:
    begin
      VSPaClaIts := QrVSPalClaIts1Controle.Value;
      VSPallet   := QrVSPallet1Codigo.Value;
      VMI_Baix   := QrVSPalClaIts1VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts1VMI_Dest.Value;
    end;
    2:
    begin
      VSPaClaIts := QrVSPalClaIts2Controle.Value;
      VSPallet   := QrVSPallet2Codigo.Value;
      VMI_Baix   := QrVSPalClaIts2VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts2VMI_Dest.Value;
    end;
    3:
    begin
      VSPaClaIts := QrVSPalClaIts3Controle.Value;
      VSPallet   := QrVSPallet3Codigo.Value;
      VMI_Baix   := QrVSPalClaIts3VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts3VMI_Dest.Value;
    end;
    4:
    begin
      VSPaClaIts := QrVSPalClaIts4Controle.Value;
      VSPallet   := QrVSPallet4Codigo.Value;
      VMI_Baix   := QrVSPalClaIts4VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts4VMI_Dest.Value;
    end;
    5:
    begin
      VSPaClaIts := QrVSPalClaIts5Controle.Value;
      VSPallet   := QrVSPallet5Codigo.Value;
      VMI_Baix   := QrVSPalClaIts5VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts5VMI_Dest.Value;
    end;
    6:
    begin
      VSPaClaIts := QrVSPalClaIts6Controle.Value;
      VSPallet   := QrVSPallet6Codigo.Value;
      VMI_Baix   := QrVSPalClaIts6VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts6VMI_Dest.Value;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemDadosBox"');
      Exit;
    end;
  end;
  Result := True;
end;

function TFmVSClassifOneNew.ObtemQryesBox(const Box: Integer; var QrVSPallet,
  QrItens, QrSum, QrSumPal: TmySQLQuery): Boolean;
begin
  case Box of
    1:
    begin
      QrVSPallet := QrVSPallet1;
      QrItens    := QrItens1;
      QrSum      := QrSum1;
      QrSumPal   := QrSumPal1;
    end;
    2:
    begin
      QrVSPallet := QrVSPallet2;
      QrItens    := QrItens2;
      QrSum      := QrSum2;
      QrSumPal   := QrSumPal2;
    end;
    3:
    begin
      QrVSPallet := QrVSPallet3;
      QrItens    := QrItens3;
      QrSum      := QrSum3;
      QrSumPal   := QrSumPal3;
    end;
    4:
    begin
      QrVSPallet := QrVSPallet4;
      QrItens    := QrItens4;
      QrSum      := QrSum4;
      QrSumPal   := QrSumPal4;
    end;
    5:
    begin
      QrVSPallet := QrVSPallet5;
      QrItens    := QrItens5;
      QrSum      := QrSum5;
      QrSumPal   := QrSumPal5;
    end;
    6:
    begin
      QrVSPallet := QrVSPallet6;
      QrItens    := QrItens6;
      QrSum      := QrSum6;
      QrSumPal   := QrSumPal6;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemQryesBox"');
      Exit;
    end;
  end;
  Result := True;
end;

procedure TFmVSClassifOneNew.PMEncerraPopup(Sender: TObject);
begin
  Palletdobox01.Enabled :=
    (QrVSPallet1.State <> dsInactive) and (QrVSPallet1.RecordCount > 0);
  Palletdobox02.Enabled :=
    (QrVSPallet2.State <> dsInactive) and (QrVSPallet2.RecordCount > 0);
  Palletdobox03.Enabled :=
    (QrVSPallet3.State <> dsInactive) and (QrVSPallet3.RecordCount > 0);
  Palletdobox04.Enabled :=
    (QrVSPallet4.State <> dsInactive) and (QrVSPallet4.RecordCount > 0);
  Palletdobox05.Enabled :=
    (QrVSPallet5.State <> dsInactive) and (QrVSPallet5.RecordCount > 0);
  Palletdobox06.Enabled :=
    (QrVSPallet6.State <> dsInactive) and (QrVSPallet6.RecordCount > 0);
end;

procedure TFmVSClassifOneNew.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  //AdicionarPallet1.Enabled := (QrVSPallet1.State = dsInactive) or (QrVSPallet1.RecordCount = 0);
  //MyObjects.HabilitaMenuItemCabUpd(EncerrarPallet1, QrVSPallet1);
  //MyObjects.HabilitaMenuItemCabDel(RemoverPallet1, QrVSPallet1, QrItens1);
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemItsDel(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSClassifOneNew.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    //0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    0: QrSumTJaFoi_AREA.Value := QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value;
    //1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    1: QrSumTJaFoi_AREA.Value := Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  //QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
  QrSumTJaFoi_PECA.Value := QrSumTPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSClassifOneNew.QrVSGerArtNewAfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet1, PnIntMei1);
  ReconfiguraPaineisIntMei(QrVSPallet2, PnIntMei2);
  ReconfiguraPaineisIntMei(QrVSPallet3, PnIntMei3);
  ReconfiguraPaineisIntMei(QrVSPallet4, PnIntMei4);
  ReconfiguraPaineisIntMei(QrVSPallet5, PnIntMei5);
  ReconfiguraPaineisIntMei(QrVSPallet6, PnIntMei6);
end;

procedure TFmVSClassifOneNew.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
var
  Campo: String;
  I: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    if QrVSPaClaCab.FieldByName(VS_CRC_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  case QrVSPaClaCabTipoArea.Value of
    0: Campo := 'AreaM2';
    1: Campo := 'AreaP2';
    else Campo := 'Area?2';
  end;
  DBGAll.Columns[2].FieldName := Campo;
  //
  DBGPallet1.Columns[0].FieldName := Campo;
  DBGPallet2.Columns[0].FieldName := Campo;
  DBGPallet3.Columns[0].FieldName := Campo;
  DBGPallet4.Columns[0].FieldName := Campo;
  DBGPallet5.Columns[0].FieldName := Campo;
  DBGPallet6.Columns[0].FieldName := Campo;
  //
  DBEdArea1.DataField := Campo;
  DBEdArea2.DataField := Campo;
  DBEdArea3.DataField := Campo;
  DBEdArea4.DataField := Campo;
  DBEdArea5.DataField := Campo;
  DBEdArea6.DataField := Campo;
  //
  //DBEDAreaT.DataField := Campo;
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  ReopenVSPallet(1, QrVSPallet1, QrVSPalClaIts1, QrVSPaClaCabLstPal01.Value, QrVSPallet1FatorInt);
  ReopenVSPallet(2, QrVSPallet2, QrVSPalClaIts2, QrVSPaClaCabLstPal02.Value, QrVSPallet2FatorInt);
  ReopenVSPallet(3, QrVSPallet3, QrVSPalClaIts3, QrVSPaClaCabLstPal03.Value, QrVSPallet3FatorInt);
  ReopenVSPallet(4, QrVSPallet4, QrVSPalClaIts4, QrVSPaClaCabLstPal04.Value, QrVSPallet4FatorInt);
  ReopenVSPallet(5, QrVSPallet5, QrVSPalClaIts5, QrVSPaClaCabLstPal05.Value, QrVSPallet5FatorInt);
  ReopenVSPallet(6, QrVSPallet6, QrVSPalClaIts6, QrVSPaClaCabLstPal06.Value, QrVSPallet6FatorInt);
  //
  ReopenItens(QrVSPalClaIts1Controle.Value, QrVSPallet1Codigo.Value, QrItens1, QrSum1, QrSumPal1);
  ReopenItens(QrVSPalClaIts2Controle.Value, QrVSPallet2Codigo.Value, QrItens2, QrSum2, QrSumPal2);
  ReopenItens(QrVSPalClaIts3Controle.Value, QrVSPallet3Codigo.Value, QrItens3, QrSum3, QrSumPal3);
  ReopenItens(QrVSPalClaIts4Controle.Value, QrVSPallet4Codigo.Value, QrItens4, QrSum4, QrSumPal4);
  ReopenItens(QrVSPalClaIts5Controle.Value, QrVSPallet5Codigo.Value, QrItens5, QrSum5, QrSumPal5);
  ReopenItens(QrVSPalClaIts6Controle.Value, QrVSPallet6Codigo.Value, QrItens6, QrSum6, QrSumPal6);
  //
  AtualizaInfoOC();
  //
end;

procedure TFmVSClassifOneNew.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
begin
  PnBox01.Visible := True;
  PnBox02.Visible := True;
  PnBox03.Visible := True;
  PnBox04.Visible := True;
  PnBox05.Visible := True;
  PnBox06.Visible := True;
  //
  QrVSGerArtNew.Close;
  //
  QrVSPallet1.Close;
  QrVSPallet2.Close;
  QrVSPallet3.Close;
  QrVSPallet4.Close;
  QrVSPallet5.Close;
  QrVSPallet6.Close;
  //
  QrVSPalClaIts1.Close;
  QrVSPalClaIts2.Close;
  QrVSPalClaIts3.Close;
  QrVSPalClaIts4.Close;
  QrVSPalClaIts5.Close;
  QrVSPalClaIts6.Close;
  //
  QrItens1.Close;
  QrItens2.Close;
  QrItens3.Close;
  QrItens4.Close;
  QrItens5.Close;
  QrItens6.Close;
  //
  QrSum1.Close;
  QrSum2.Close;
  QrSum3.Close;
  QrSum4.Close;
  QrSum5.Close;
  QrSum6.Close;
  //
end;

procedure TFmVSClassifOneNew.QrVSPaClaCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    0: QrVSPaClaCabNO_TIPO.Value := 'm�';
    1: QrVSPaClaCabNO_TIPO.Value := 'ft�';
    else QrVSPaClaCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaClaCabNO_TIPO.Value;
end;

procedure TFmVSClassifOneNew.QrVSPallet1AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet1, PnIntMei1);
end;

procedure TFmVSClassifOneNew.QrVSPallet2AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet2, PnIntMei2);
end;

procedure TFmVSClassifOneNew.QrVSPallet3AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet3, PnIntMei3);
end;

procedure TFmVSClassifOneNew.QrVSPallet4AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet4, PnIntMei4);
end;

procedure TFmVSClassifOneNew.QrVSPallet5AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet5, PnIntMei5);
end;

procedure TFmVSClassifOneNew.QrVSPallet6AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet6, PnIntMei6);
end;

procedure TFmVSClassifOneNew.RealinhaBoxes();
var
  H_T, W_T, H_1, W_1: Integer;
begin
  H_T := PnBoxesAll.Height;
  W_T := PnBoxesAll.Width;
  //
  H_1 := H_T div 2;
  W_1 := W_T div 3;
  //
  PnBoxesT01.Height := H_1;
  //PnBoxesT02.Height := H_1;

  PnBoxT1L1.Width := W_1;
  PnBoxT1L2.Width := W_1;
  PnBoxT1L3.Width := W_1;

  PnBoxT2L1.Width := W_1;
  PnBoxT2L2.Width := W_1;
  PnBoxT2L3.Width := W_1;
end;

procedure TFmVSClassifOneNew.ReconfiguraPaineisIntMei(Qry: TmySQLQuery;
Panel: TPanel);
var
  Habilita: Boolean;
begin
  Habilita := Qry.FieldByName('FatorInt').AsFloat < QrVSGerArtNewFatorInt.Value;
  if Habilita <> Panel.Visible then
    Panel.Visible := Habilita;
end;

procedure TFmVSClassifOneNew.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo: String;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, Codigo: Integer;
  DtHrFim: String;
begin
  Campo := VS_CRC_PF.CampoLstPal(Box);
  Codigo := QrVSPaClaCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  if VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaClaIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_CRC_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaClaCab();
      end;
    end;
  end;
end;

procedure TFmVSClassifOneNew.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNew.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {$IfDef sAllVS}
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSClassifOneNew.ReopenItens(VSPaClaIts, VSPallet: Integer; QrIts,
  QrSum, QrSumPal: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  'ORDER BY Controle DESC ',

  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  '']);
  //
  ReopenSumPal(QrSumPal, VSPallet);
end;

procedure TFmVSClassifOneNew.ReopenSumPal(QrSumPal: TmySQLQuery;
  VSPallet: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPal, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
  '']);
end;

procedure TFmVSClassifOneNew.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, cn1.FatorInt ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND vmi.MovimCod=' + Geral.FF0(QrVSPaClaCabMovimCod.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSClassifOneNew.ReopenVSPaClaCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vspaclacaba pcc',
  'LEFT JOIN vsgerarta  vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
end;

procedure TFmVSClassifOneNew.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalClaIts: TmySQLQuery; Pallet: Integer; QrVSPalletFatorInt: TFloatField);
begin
  if Pallet > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT let.*, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt ',
    'FROM vspalleta let ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE let.Codigo=' + Geral.FF0(Pallet),
    '']);
    if (QrVSPallet.RecordCount > 0) and (QrVSPalletFatorInt.Value <= 0) then
    begin
      if Tecla = 0 then
        Geral.MB_Aviso(
        'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!'
        + sLineBreak + 'Para evitar erros de estoque esta janela ser� fechada!');
        Close;
        Exit;
    end;
    //
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalClaIts, Dmod.MyDB, [
    'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
    'FROM vspaclaitsa ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    'AND Tecla=' + Geral.FF0(Tecla),
    'AND VSPallet=' + Geral.FF0(Pallet),
    'ORDER BY DtHrIni DESC, Controle DESC ',
    'LIMIT 1 ',
    '']);
  end else
  begin
    QrVSPallet.Close;
  end;
  //
end;

procedure TFmVSClassifOneNew.rocarIMEI1Click(Sender: TObject);
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
begin
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
{ Pal07 := QrVSPaClaCabLstPal07.Value;
  Pal08 := QrVSPaClaCabLstPal08.Value;
  Pal09 := QrVSPaClaCabLstPal09.Value;
  Pal10 := QrVSPaClaCabLstPal10.Value;
  Pal11 := QrVSPaClaCabLstPal11.Value;
  Pal12 := QrVSPaClaCabLstPal12.Value;
  Pal13 := QrVSPaClaCabLstPal13.Value;
  Pal14 := QrVSPaClaCabLstPal14.Value;
  Pal15 := QrVSPaClaCabLstPal15.Value;
} //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  VS_CRC_PF.MostraFormVSClassifOneRetIMEI_06(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self,
    6);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
end;

procedure TFmVSClassifOneNew.rocarVichaRMP1Click(Sender: TObject);
(*
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
*)
begin
(* Form removido para eviatr erro de usuario na ecolha do IMEI!
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  //
  VS_CRC_PF.MostraFormVSClassifOneRetFichaRMP(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
*)
end;

procedure TFmVSClassifOneNew.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

function TFmVSClassifOneNew.SubstituiOC(): Boolean;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSClaArtPrpMDz, FmVSClaArtPrpMDz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpMDz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpMDz.EdPallet1.ValueVariant := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.EdPallet2.ValueVariant := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.EdPallet3.ValueVariant := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.EdPallet4.ValueVariant := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.EdPallet5.ValueVariant := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.EdPallet6.ValueVariant := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.CBPallet1.KeyValue := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.CBPallet2.KeyValue := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.CBPallet3.KeyValue := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.CBPallet4.KeyValue := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.CBPallet5.KeyValue := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.CBPallet6.KeyValue := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.ShowModal;
    Codigo := FmVSClaArtPrpMDz.FCodigo;
    CacCod := FmVSClaArtPrpMDz.FCacCod;
    MovimID := FmVSClaArtPrpMDz.FMovimID;
    FmVSClaArtPrpMDz.Destroy;
    //
    if Codigo <> 0 then
    begin
      FCodigo := Codigo;
      FCacCod := CacCod;
      FMovimID := MovimID;
      ReopenVSPaClaCab();
      //
      Result := True;
    end else
      Result := False;
  end;
end;

procedure TFmVSClassifOneNew.TentarFocarEdArea();
begin
  ConfigPnSubClass();
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

end.
