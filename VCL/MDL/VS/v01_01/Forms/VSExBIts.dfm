object FmVSExBIts: TFmVSExBIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-079 :: Item de Baixa Extra de Couro na Ribeira'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 289
    Width = 875
    Height = 26
    BevelOuter = bvNone
    TabOrder = 0
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 487
        Height = 32
        Caption = 'Item de Baixa Extra de Couro na Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 487
        Height = 32
        Caption = 'Item de Baixa Extra de Couro na Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 487
        Height = 32
        Caption = 'Item de Baixa Extra de Couro na Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 547
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 447
        Height = 17
        Caption = 
          'F4 na caixa de pe'#231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de orige' +
          'm!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 447
        Height = 17
        Caption = 
          'F4 na caixa de pe'#231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de orige' +
          'm!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtExcluiClick
      end
      object CkContinuar: TCheckBox
        Left = 140
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 405
      Height = 499
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 64
        Width = 405
        Height = 261
        Align = alTop
        Caption = ' Dados do item: '
        TabOrder = 0
        object Label6: TLabel
          Left = 316
          Top = 56
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 66
          Height = 13
          Caption = 'Mat'#233'ria-prima:'
        end
        object LaPecas: TLabel
          Left = 8
          Top = 177
          Width = 54
          Height = 13
          Caption = 'Pe'#231'as [F4]:'
        end
        object LaAreaM2: TLabel
          Left = 80
          Top = 177
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 156
          Top = 177
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object LaPeso: TLabel
          Left = 240
          Top = 177
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object Label4: TLabel
          Left = 8
          Top = 56
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object SBPallet: TSpeedButton
          Left = 293
          Top = 72
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBPalletClick
        end
        object Label9: TLabel
          Left = 8
          Top = 217
          Width = 184
          Height = 13
          Caption = 'Observa'#231#227'o (Motivo da baixa for'#231'ada):'
        end
        object Label5: TLabel
          Left = 316
          Top = 177
          Width = 71
          Height = 13
          Caption = 'Valor total [F5]:'
        end
        object Label14: TLabel
          Left = 8
          Top = 137
          Width = 53
          Height = 13
          Caption = 'ID Reclas.:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label10: TLabel
          Left = 76
          Top = 137
          Width = 56
          Height = 13
          Caption = 'ID Estoque:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label17: TLabel
          Left = 144
          Top = 137
          Width = 43
          Height = 13
          Caption = 'ID Pallet:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label11: TLabel
          Left = 196
          Top = 97
          Width = 56
          Height = 13
          Caption = 'Ficha RMP:'
        end
        object Label2: TLabel
          Left = 8
          Top = 97
          Width = 83
          Height = 13
          Caption = 'S'#233'rie Ficha RMP:'
        end
        object Label13: TLabel
          Left = 212
          Top = 137
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label15: TLabel
          Left = 280
          Top = 97
          Width = 28
          Height = 13
          Caption = 'IME-I:'
        end
        object EdControle: TdmkEdit
          Left = 316
          Top = 72
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 64
          Top = 32
          Width = 333
          Height = 21
          DropDownRows = 2
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 2
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdGraGruX
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGruX: TdmkEditCB
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdGraGruXKeyDown
          OnRedefinido = EdGraGruXRedefinido
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPecas: TdmkEdit
          Left = 8
          Top = 193
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdPecasKeyDown
          OnRedefinido = EdPecasRedefinido
        end
        object EdAreaM2: TdmkEditCalc
          Left = 80
          Top = 193
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'AreaM2'
          UpdCampo = 'AreaM2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaM2Change
          OnKeyDown = EdPecasKeyDown
          dmkEditCalcA = EdAreaP2
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2: TdmkEditCalc
          Left = 156
          Top = 193
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'AreaP2'
          UpdCampo = 'AreaP2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdAreaM2
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPesoKg: TdmkEdit
          Left = 240
          Top = 193
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'PesoKg'
          UpdCampo = 'PesoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPsqPallet: TdmkEditCB
          Left = 8
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pallet'
          UpdCampo = 'Pallet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdPsqPalletRedefinido
          DBLookupComboBox = CBPsqPallet
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPsqPallet: TdmkDBLookupComboBox
          Left = 64
          Top = 72
          Width = 225
          Height = 21
          DropDownRows = 2
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsVSPallet
          TabOrder = 4
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdPsqPallet
          QryCampo = 'Pallet'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdObserv: TdmkEdit
          Left = 8
          Top = 233
          Width = 389
          Height = 21
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ'
          UpdCampo = 'Observ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValorT: TdmkEdit
          Left = 316
          Top = 193
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 16
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdValorTKeyDown
        end
        object EdSrcMovID: TdmkEdit
          Left = 8
          Top = 153
          Width = 64
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcMovIDChange
        end
        object EdSrcNivel1: TdmkEdit
          Left = 76
          Top = 153
          Width = 64
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'MovimCod'
          UpdCampo = 'MovimCod'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel1Change
        end
        object EdSrcNivel2: TdmkEdit
          Left = 144
          Top = 153
          Width = 64
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel2Change
        end
        object EdPsqFicha: TdmkEdit
          Left = 196
          Top = 113
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdPsqFichaRedefinido
        end
        object EdPsqSerieFch: TdmkEditCB
          Left = 8
          Top = 113
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pallet'
          UpdCampo = 'Pallet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdPsqSerieFchRedefinido
          DBLookupComboBox = CBPsqSerieFch
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPsqSerieFch: TdmkDBLookupComboBox
          Left = 64
          Top = 113
          Width = 129
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsVSPallet
          TabOrder = 6
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdPsqSerieFch
          QryCampo = 'Pallet'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdSrcGGX: TdmkEdit
          Left = 212
          Top = 153
          Width = 64
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel2Change
        end
        object EdIMEI: TdmkEdit
          Left = 280
          Top = 113
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdIMEIRedefinido
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 405
        Height = 64
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 12
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label7: TLabel
          Left = 96
          Top = 20
          Width = 32
          Height = 13
          Caption = 'IME-C:'
          FocusControl = DBEdMovimCod
        end
        object Label8: TLabel
          Left = 180
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdEmpresa
        end
        object Label12: TLabel
          Left = 228
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Data / hora:'
          FocusControl = DBEdDtEntrada
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdMovimCod: TdmkDBEdit
          Left = 96
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'MovimCod'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdEmpresa: TdmkDBEdit
          Left = 180
          Top = 36
          Width = 45
          Height = 21
          TabStop = False
          DataField = 'Empresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdDtEntrada: TdmkDBEdit
          Left = 228
          Top = 36
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'DtBaixa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
      end
      object RGDataHora: TRadioGroup
        Left = 0
        Top = 373
        Width = 405
        Height = 107
        Align = alTop
        Caption = ' Fonte da data / hora de baixa:'
        ItemIndex = 3
        Items.Strings = (
          'N'#227'o definido'
          'Do cabe'#231'alho'
          'Informada aqui (abaixo)'
          'Conforme '#250'ltima movimenta'#231#227'o de cada item')
        TabOrder = 2
      end
      object Panel8: TPanel
        Left = 0
        Top = 325
        Width = 405
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object Label18: TLabel
          Left = 8
          Top = 4
          Width = 140
          Height = 13
          Caption = 'Data / hora "informada aqui":'
        end
        object Label19: TLabel
          Left = 192
          Top = 4
          Width = 123
          Height = 13
          Caption = 'Senha p/ uso IME-I vago:'
        end
        object TPDataSenha: TdmkEditDateTimePicker
          Left = 7
          Top = 20
          Width = 112
          Height = 21
          Date = 42131.000000000000000000
          Time = 0.794453055546910000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraSenha: TdmkEdit
          Left = 122
          Top = 20
          Width = 61
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '20:00:00'
          QryCampo = 'DtCompra'
          UpdCampo = 'DtCompra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.833333333333333400
          ValWarn = False
        end
        object EdSenha: TEdit
          Left = 190
          Top = 21
          Width = 125
          Height = 20
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 2
        end
      end
    end
    object Panel3: TPanel
      Left = 405
      Top = 0
      Width = 603
      Height = 499
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 603
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label16: TLabel
          Left = 176
          Top = 0
          Width = 86
          Height = 13
          Caption = 'M'#225'ximo de pe'#231'as:'
        end
        object EdMaxPecas: TdmkEdit
          Left = 176
          Top = 16
          Width = 89
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '10'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 10
          ValWarn = False
          OnRedefinido = EdMaxPecasRedefinido
        end
        object RGPosiNega: TRadioGroup
          Left = 0
          Top = 0
          Width = 165
          Height = 41
          Align = alLeft
          Caption = ' Estoque: '
          Columns = 2
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Positivo'
            'Negativo')
          TabOrder = 1
          OnClick = RGPosiNegaClick
        end
      end
      object DBGVSMovIts: TDBGrid
        Left = 0
        Top = 41
        Width = 603
        Height = 458
        Align = alClient
        DataSource = DsVSMovIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGVSMovItsDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Title.Caption = 'Ficha RMP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtArM2'
            Title.Caption = 'Sdo '#225'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeso'
            Title.Caption = 'Sdo peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Title.Caption = 'ID Estoque'
            Visible = True
          end>
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 656
    Top = 172
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 656
    Top = 216
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Grandeza'
      'FROM defpecas')
    Left = 720
    Top = 172
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 720
    Top = 216
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 724
    Top = 68
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 724
    Top = 116
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.Nome NO_Pallet, wmi.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  '
      'FROM vsmovits wmi '
      'LEFT JOIN vspalleta pal ON pal.Codigo=wmi.Pallet '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Terceiro')
    Left = 652
    Top = 68
    object QrVSMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TIntegerField
      FieldName = 'Misturou'
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 656
    Top = 116
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 788
    Top = 172
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 788
    Top = 220
  end
  object QrLastMov: TMySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 168
    object QrLastMovDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
end
