object FmVSCfgEstqOthersEFD: TFmVSCfgEstqOthersEFD
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-215 :: Confer'#234'ncia de Baixas x Estoque de Couros '
  ClientHeight = 735
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 534
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 534
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 534
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 562
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 562
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 562
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 352
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = -2
          ExplicitTop = 181
        end
        object Panel5: TPanel
          Left = 2
          Top = 510
          Width = 1004
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtCorrige: TBitBtn
            Tag = 56
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Corrige'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCorrigeClick
          end
        end
        object DGPsq: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 337
          Align = alClient
          DataSource = DsPsq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'IMEIInn'
              Title.Caption = 'IME-I Gerado'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Reduzido'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimID'
              Title.Caption = 'ID Mov.'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimNiv'
              Title.Caption = 'ID N'#237'vel'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'N'#237'vel movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKgInn'
              Title.Caption = 'Kg gerado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PecasInn'
              Title.Caption = 'P'#231' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2Inn'
              Title.Caption = 'm'#178' gerado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKgBxa'
              Title.Caption = 'Kg baixado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PecasBxa'
              Title.Caption = 'P'#231' baixadas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2Bxa'
              Title.Caption = 'm'#178' baixados'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoKg'
              Title.Caption = 'Saldo kg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoPc'
              Title.Caption = 'Saldo P'#231
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoM2'
              Title.Caption = 'Saldo m'#178
              Width = 72
              Visible = True
            end>
        end
        object DGGer: TdmkDBGridZTO
          Left = 2
          Top = 357
          Width = 1004
          Height = 153
          Align = alBottom
          DataSource = DsBxa
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'IMEIGer'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimID'
              Title.Caption = 'ID Mov.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimNiv'
              Title.Caption = 'ID N'#237'vel'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'N'#237'vel movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Width = 72
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 610
    Width = 1008
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReopen: TBitBtn
        Tag = 18
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reopen'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReopenClick
      end
    end
  end
  object QrPsq: TMySQLQuery
   
    BeforeClose = QrPsqBeforeClose
    AfterScroll = QrPsqAfterScroll
    SQL.Strings = (
      'SELECT sdo.*, '
      
        ' ELT(sdo.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento","Desclassifica'#231#227'o","Caleado","Couro PDA","Cou' +
        'ro DTA","Subproduto em proc.","Reprocesso / reparo","Curtido","D' +
        'ilu./Mist. insumos","Entrada sem Cobertura","Sa'#237'da sem Cobertura' +
        '") NO_MovimID,'
      
        ' ELT(sdo.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o' +
        '","PDA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em o' +
        'pera'#231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o"' +
        ',"Destino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de' +
        ' DTA","Origem PSP em processo","PSP processo","Destino PSP em pr' +
        'ocesso","Baixa de PSP em processo","Artigo de PSP","Origem RRM e' +
        'm reprocesso","RRM reprocesso","Destino RRM em reprocesso","Baix' +
        'a de RRM em reprocesso","Artigo de Reprocesso / reparo","Baixa d' +
        'e insumo em mistura","Gera'#231#227'o de insumo em mistura") NO_MovimNiv' +
        ','
      'ggx.Controle Reduzido, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR  '
      'FROM _Baixados_SDO_GER_ sdo '
      
        'LEFT JOIN bluederm_colorado.gragrux    ggx ON ggx.Controle=sdo.G' +
        'GXInn '
      
        'LEFT JOIN bluederm_colorado.gragruy    ggy ON ggy.Codigo=ggx.Con' +
        'trole '
      
        'LEFT JOIN bluederm_colorado.gragruc    ggc ON ggc.Controle=ggx.G' +
        'raGruC  '
      
        'LEFT JOIN bluederm_colorado.gracorcad  gcc ON gcc.Codigo=ggc.Gra' +
        'CorCad  '
      
        'LEFT JOIN bluederm_colorado.gratamits  gti ON gti.Controle=ggx.G' +
        'raTamI  '
      
        'LEFT JOIN bluederm_colorado.gragru1    gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1  '
      'WHERE SdoPc <= 0 '
      'AND ( '
      '  SdoM2 <> 0 '
      '  OR '
      '  SdoKg <> 0 '
      ') '
      'ORDER BY DataHora ')
    Left = 28
    Top = 116
    object QrPsqSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrPsqDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrPsqGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
    end
    object QrPsqJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
    object QrPsqRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
    end
    object QrPsqGGXInn: TIntegerField
      FieldName = 'GGXInn'
    end
    object QrPsqIMEIInn: TIntegerField
      FieldName = 'IMEIInn'
    end
    object QrPsqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPsqMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPsqMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrPsqSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrPsqPecasInn: TFloatField
      FieldName = 'PecasInn'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqAreaM2Inn: TFloatField
      FieldName = 'AreaM2Inn'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqPesoKgInn: TFloatField
      FieldName = 'PesoKgInn'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrPsqPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqAreaM2Bxa: TFloatField
      FieldName = 'AreaM2Bxa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqSdoPc: TFloatField
      FieldName = 'SdoPc'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqSdoM2: TFloatField
      FieldName = 'SdoM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqSdoKg: TFloatField
      FieldName = 'SdoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqReduzido: TIntegerField
      FieldName = 'Reduzido'
      Required = True
    end
    object QrPsqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPsqNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrPsqNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 72
    Top = 116
  end
  object QrBxa: TMySQLQuery
   
    AfterOpen = QrBxaAfterOpen
    BeforeClose = QrBxaBeforeClose
    SQL.Strings = (
      'SELECT '
      'CONCAT(gg1.Nome,   '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR,  '
      
        ' ELT(bxa.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento","Desclassifica'#231#227'o","Caleado","Couro PDA","Cou' +
        'ro DTA","Subproduto em proc.","Reprocesso / reparo","Curtido","D' +
        'ilu./Mist. insumos","Entrada sem Cobertura","Sa'#237'da sem Cobertura' +
        '") NO_MovimID,'
      
        ' ELT(bxa.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o' +
        '","PDA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em o' +
        'pera'#231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o"' +
        ',"Destino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de' +
        ' DTA","Origem PSP em processo","PSP processo","Destino PSP em pr' +
        'ocesso","Baixa de PSP em processo","Artigo de PSP","Origem RRM e' +
        'm reprocesso","RRM reprocesso","Destino RRM em reprocesso","Baix' +
        'a de RRM em reprocesso","Artigo de Reprocesso / reparo","Baixa d' +
        'e insumo em mistura","Gera'#231#227'o de insumo em mistura") NO_MovimNiv' +
        ','
      'bxa.* '
      'FROM _Baixados_lct_bxa_ bxa'
      
        'LEFT JOIN bluederm_colorado.gragrux    ggx ON ggx.Controle=bxa.G' +
        'GXBxa '
      
        'LEFT JOIN bluederm_colorado.gragruy    ggy ON ggy.Codigo=ggx.Con' +
        'trole '
      
        'LEFT JOIN bluederm_colorado.gragruc    ggc ON ggc.Controle=ggx.G' +
        'raGruC '
      
        'LEFT JOIN bluederm_colorado.gracorcad  gcc ON gcc.Codigo=ggc.Gra' +
        'CorCad '
      
        'LEFT JOIN bluederm_colorado.gratamits  gti ON gti.Controle=ggx.G' +
        'raTamI '
      
        'LEFT JOIN bluederm_colorado.gragru1    gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1 '
      'WHERE SrcNivel2=22799'
      'ORDER BY IMEIBxa DESC')
    Left = 28
    Top = 164
    object QrBxaGGXBxa: TIntegerField
      FieldName = 'GGXBxa'
    end
    object QrBxaIMEIBxa: TIntegerField
      FieldName = 'IMEIBxa'
    end
    object QrBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrBxaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrBxaMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrBxaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrBxaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrBxaNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrBxaNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
  end
  object DsBxa: TDataSource
    DataSet = QrBxa
    Left = 72
    Top = 164
  end
  object PMCorrigeID01: TPopupMenu
    OnPopup = PMCorrigeID01Popup
    Left = 28
    Top = 548
    object CorrigePesosBaixaInNatura1: TMenuItem
      Caption = '&1. Corrige pesos baixa In Natura'
      object CorrigePesosBaixaInNatura_Atual1: TMenuItem
        Caption = '&Atual'
        OnClick = CorrigePesosBaixaInNatura_Atual1Click
      end
      object CorrigePesosBaixaInNatura_Selecionadospossveis1: TMenuItem
        Caption = '&Selecionados poss'#237'veis'
        OnClick = CorrigePesosBaixaInNatura_Selecionadospossveis1Click
      end
      object CorrigePesosBaixaInNatura_Todospossveis1: TMenuItem
        Caption = '&Todos poss'#237'veis'
        OnClick = CorrigePesosBaixaInNatura_Todospossveis1Click
      end
    end
    object ID01N2CorrigepeasepesobaizaInNatura1: TMenuItem
      Caption = '2. Corrige pe'#231'as e peso baixa In Natura'
      OnClick = ID01N2CorrigepeasepesobaizaInNatura1Click
    end
    object Atualizasaldodoitemgerado1: TMenuItem
      Caption = '&Atualiza saldo do item gerado'
      OnClick = Atualizasaldodoitemgerado1Click
    end
    object CorrigeIMEIDestinodageraogradeacima1: TMenuItem
      Caption = 'Corrige IME-I &Baixado (grade acima)'
      OnClick = CorrigeIMEIDestinodageraogradeacima1Click
    end
    object CorrigeIMEIdeBaixagradeabaixo1: TMenuItem
      Caption = 'Corrige IME-I &De baixa (grade abaixo)'
      OnClick = CorrigeIMEIdeBaixagradeabaixo1Click
    end
  end
end
