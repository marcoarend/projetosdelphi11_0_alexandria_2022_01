object FmVSGerArtAltQtd: TFmVSGerArtAltQtd
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-103 :: Altera'#231#227'o de Quantidades de Artigo Gerado'
  ClientHeight = 554
  ClientWidth = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 415
    Width = 860
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 860
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 1
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label14: TLabel
      Left = 216
      Top = 20
      Width = 51
      Height = 13
      Caption = 'ID Movim.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label1: TLabel
      Left = 284
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID Gera'#231#227'o:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label17: TLabel
      Left = 352
      Top = 20
      Width = 61
      Height = 13
      Caption = 'ID It.Gerado:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 148
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Red. It.Ger.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label13: TLabel
      Left = 788
      Top = 16
      Width = 62
      Height = 13
      Caption = 'ID Mov Twn:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label8: TLabel
      Left = 420
      Top = 20
      Width = 36
      Height = 13
      Caption = '$ Total:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdSrcNivel2: TdmkEdit
      Left = 352
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcNivel1: TdmkEdit
      Left = 284
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcMovID: TdmkEdit
      Left = 216
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcGGX: TdmkEdit
      Left = 148
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimTwn: TdmkEdit
      Left = 788
      Top = 32
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimTwn'
      UpdCampo = 'MovimTwn'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcValorT: TdmkEdit
      Left = 420
      Top = 36
      Width = 100
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 860
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 812
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 764
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 529
        Height = 32
        Caption = 'Altera'#231#227'o de Quantidades de Artigo Gerado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 529
        Height = 32
        Caption = 'Altera'#231#227'o de Quantidades de Artigo Gerado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 529
        Height = 32
        Caption = 'Altera'#231#227'o de Quantidades de Artigo Gerado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 440
    Width = 860
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 856
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 484
    Width = 860
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 714
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 712
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 112
    Width = 860
    Height = 303
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 860
      Height = 303
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 2
        Top = 15
        Width = 856
        Height = 106
        Align = alTop
        Caption = ' Dados da baixa do couro In Natura: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 852
          Height = 89
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object LaPecasBxa: TLabel
            Left = 96
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object LaPesoKgBxa: TLabel
            Left = 172
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
            Enabled = False
          end
          object SbPesoKgBxa: TSpeedButton
            Left = 244
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbPesoKgBxaClick
          end
          object LaQtdGerArM2Bxa: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            Enabled = False
          end
          object LaQtdGerArP2Bxa: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
            Enabled = False
          end
          object Label9: TLabel
            Left = 12
            Top = 44
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object EdControleBxa: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPecasBxa: TdmkEdit
            Left = 96
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecasBxaChange
          end
          object EdPesoKgBxa: TdmkEdit
            Left = 172
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPesoKgBxaChange
          end
          object EdQtdGerArM2Bxa: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArP2Bxa
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdQtdGerArP2Bxa: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArM2Bxa
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservBxa: TdmkEdit
            Left = 12
            Top = 60
            Width = 421
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 121
        Width = 856
        Height = 180
        Align = alClient
        Caption = ' Dados do artigo gerado: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 852
          Height = 163
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label4: TLabel
            Left = 96
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Pe'#231'as: '
          end
          object Label7: TLabel
            Left = 172
            Top = 4
            Width = 76
            Height = 13
            Caption = 'Peso kg origem:'
            Enabled = False
          end
          object LaQtdGerArM2Src: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object LaQtdGerArP2Src: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label12: TLabel
            Left = 12
            Top = 44
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object Label11: TLabel
            Left = 676
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
          end
          object Label10: TLabel
            Left = 428
            Top = 4
            Width = 82
            Height = 13
            Caption = 'Peso kg destino: '
          end
          object EdControleSrc: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPecasSrc: TdmkEdit
            Left = 96
            Top = 20
            Width = 72
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdPecasSrcKeyDown
          end
          object EdPesoKgSrc: TdmkEdit
            Left = 172
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdQtdGerArM2Src: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArP2Src
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdQtdGerArP2Src: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArM2Src
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservSrc: TdmkEdit
            Left = 12
            Top = 60
            Width = 833
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPData: TdmkEditDateTimePicker
            Left = 676
            Top = 20
            Width = 112
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 6
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtHrAberto'
            UpdCampo = 'DtHrAberto'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdHora: TdmkEdit
            Left = 788
            Top = 20
            Width = 57
            Height = 21
            TabOrder = 7
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            QryName = 'QrVSGerArt'
            QryCampo = 'DtHrAberto'
            UpdCampo = 'DtHrAberto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdQtdGerPeso: TdmkEdit
            Left = 428
            Top = 20
            Width = 105
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdPecasSrcKeyDown
          end
        end
      end
    end
  end
  object QrNiv1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 552
    Top = 232
    object QrNiv1FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrNiv1MediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrNiv1MediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
  end
  object PMPesoKgBxa: TPopupMenu
    Left = 336
    Top = 188
    object Usarpesototaldeorigem1: TMenuItem
      Caption = 'Usar peso &Total de origem'
      OnClick = Usarpesototaldeorigem1Click
    end
    object InformarpesoManualmente1: TMenuItem
      Caption = 'Informar peso &Manualmente'
      OnClick = InformarpesoManualmente1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Voltaraomodoautomtico1: TMenuItem
      Caption = 'Voltar ao modo autom'#225'tico'
      OnClick = Voltaraomodoautomtico1Click
    end
  end
end
