unit VSMovItsAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnDmkProcFunc, AppListas,
  UnVS_CRC_PF, dmkCheckBox, dmkCheckGroup, UnProjGroup_Consts, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmVSMovItsAlt = class(TForm)
    GBQtdOriginal: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label1: TLabel;
    Label6: TLabel;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    QrVSMovSrc: TmySQLQuery;
    DsVSMovSrc: TDataSource;
    QrVSMovDst: TmySQLQuery;
    DsVSMovDst: TDataSource;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsGraGru1: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_EstqMovimID: TWideStringField;
    QrVSMovItsNO_DstMovID: TWideStringField;
    QrVSMovItsNO_SrcMovID: TWideStringField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    DsVSMovIts: TDataSource;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox5: TGroupBox;
    Panel14: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    GroupBox6: TGroupBox;
    Panel16: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    GroupBox7: TGroupBox;
    Panel17: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    Panel7: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Label2: TLabel;
    Label13: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit12: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit11: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox4: TGroupBox;
    Panel12: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    GroupBox8: TGroupBox;
    Panel13: TPanel;
    Label26: TLabel;
    Label27: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    Panel15: TPanel;
    Label40: TLabel;
    Label34: TLabel;
    Label41: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label46: TLabel;
    Label51: TLabel;
    DBEdit40: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit46: TDBEdit;
    Label52: TLabel;
    GBDadosArtigo: TGroupBox;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Panel5: TPanel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    CBSerieFch: TdmkDBLookupComboBox;
    EdSerieFch: TdmkEditCB;
    EdFicha: TdmkEdit;
    EdMarca: TdmkEdit;
    CkMisturou: TdmkCheckBox;
    CGNotFluxo: TdmkCheckGroup;
    CkZerado: TdmkCheckBox;
    QrVSMovItsZerado: TSmallintField;
    QrVSMovItsNotFluxo: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    EdStqCenLoc: TdmkEditCB;
    EdReqMovEstq: TdmkEdit;
    Label56: TLabel;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label57: TLabel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    Label39: TLabel;
    Label38: TLabel;
    DBEdit36: TDBEdit;
    Label37: TLabel;
    DBEdit37: TDBEdit;
    Label36: TLabel;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovSrcCodigo: TLargeintField;
    QrVSMovSrcControle: TLargeintField;
    QrVSMovSrcMovimCod: TLargeintField;
    QrVSMovSrcMovimNiv: TLargeintField;
    QrVSMovSrcMovimTwn: TLargeintField;
    QrVSMovSrcEmpresa: TLargeintField;
    QrVSMovSrcTerceiro: TLargeintField;
    QrVSMovSrcCliVenda: TLargeintField;
    QrVSMovSrcMovimID: TLargeintField;
    QrVSMovSrcDataHora: TDateTimeField;
    QrVSMovSrcPallet: TLargeintField;
    QrVSMovSrcGraGruX: TLargeintField;
    QrVSMovSrcPecas: TFloatField;
    QrVSMovSrcPesoKg: TFloatField;
    QrVSMovSrcAreaM2: TFloatField;
    QrVSMovSrcAreaP2: TFloatField;
    QrVSMovSrcValorT: TFloatField;
    QrVSMovSrcSrcMovID: TLargeintField;
    QrVSMovSrcSrcNivel1: TLargeintField;
    QrVSMovSrcSrcNivel2: TLargeintField;
    QrVSMovSrcSrcGGX: TLargeintField;
    QrVSMovSrcSdoVrtPeca: TFloatField;
    QrVSMovSrcSdoVrtPeso: TFloatField;
    QrVSMovSrcSdoVrtArM2: TFloatField;
    QrVSMovSrcObserv: TWideStringField;
    QrVSMovSrcSerieFch: TLargeintField;
    QrVSMovSrcFicha: TLargeintField;
    QrVSMovSrcMisturou: TLargeintField;
    QrVSMovSrcFornecMO: TLargeintField;
    QrVSMovSrcCustoMOKg: TFloatField;
    QrVSMovSrcCustoMOM2: TFloatField;
    QrVSMovSrcCustoMOTot: TFloatField;
    QrVSMovSrcValorMP: TFloatField;
    QrVSMovSrcDstMovID: TLargeintField;
    QrVSMovSrcDstNivel1: TLargeintField;
    QrVSMovSrcDstNivel2: TLargeintField;
    QrVSMovSrcDstGGX: TLargeintField;
    QrVSMovSrcQtdGerPeca: TFloatField;
    QrVSMovSrcQtdGerPeso: TFloatField;
    QrVSMovSrcQtdGerArM2: TFloatField;
    QrVSMovSrcQtdGerArP2: TFloatField;
    QrVSMovSrcQtdAntPeca: TFloatField;
    QrVSMovSrcQtdAntPeso: TFloatField;
    QrVSMovSrcQtdAntArM2: TFloatField;
    QrVSMovSrcQtdAntArP2: TFloatField;
    QrVSMovSrcNotaMPAG: TFloatField;
    QrVSMovSrcStqCenLoc: TLargeintField;
    QrVSMovSrcReqMovEstq: TLargeintField;
    QrVSMovSrcGraGru1: TLargeintField;
    QrVSMovSrcNO_EstqMovimID: TWideStringField;
    QrVSMovSrcNO_MovimID: TWideStringField;
    QrVSMovSrcNO_MovimNiv: TWideStringField;
    QrVSMovSrcNO_PALLET: TWideStringField;
    QrVSMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrVSMovSrcNO_FORNECE: TWideStringField;
    QrVSMovSrcNO_SerieFch: TWideStringField;
    QrVSMovSrcNO_TTW: TWideStringField;
    QrVSMovSrcID_TTW: TLargeintField;
    QrVSMovSrcNO_LOC_CEN: TWideStringField;
    QrVSMovDstCodigo: TLargeintField;
    QrVSMovDstControle: TLargeintField;
    QrVSMovDstMovimCod: TLargeintField;
    QrVSMovDstMovimNiv: TLargeintField;
    QrVSMovDstMovimTwn: TLargeintField;
    QrVSMovDstEmpresa: TLargeintField;
    QrVSMovDstTerceiro: TLargeintField;
    QrVSMovDstCliVenda: TLargeintField;
    QrVSMovDstMovimID: TLargeintField;
    QrVSMovDstDataHora: TDateTimeField;
    QrVSMovDstPallet: TLargeintField;
    QrVSMovDstGraGruX: TLargeintField;
    QrVSMovDstPecas: TFloatField;
    QrVSMovDstPesoKg: TFloatField;
    QrVSMovDstAreaM2: TFloatField;
    QrVSMovDstAreaP2: TFloatField;
    QrVSMovDstValorT: TFloatField;
    QrVSMovDstSrcMovID: TLargeintField;
    QrVSMovDstSrcNivel1: TLargeintField;
    QrVSMovDstSrcNivel2: TLargeintField;
    QrVSMovDstSrcGGX: TLargeintField;
    QrVSMovDstSdoVrtPeca: TFloatField;
    QrVSMovDstSdoVrtPeso: TFloatField;
    QrVSMovDstSdoVrtArM2: TFloatField;
    QrVSMovDstObserv: TWideStringField;
    QrVSMovDstSerieFch: TLargeintField;
    QrVSMovDstFicha: TLargeintField;
    QrVSMovDstMisturou: TLargeintField;
    QrVSMovDstFornecMO: TLargeintField;
    QrVSMovDstCustoMOKg: TFloatField;
    QrVSMovDstCustoMOM2: TFloatField;
    QrVSMovDstCustoMOTot: TFloatField;
    QrVSMovDstValorMP: TFloatField;
    QrVSMovDstDstMovID: TLargeintField;
    QrVSMovDstDstNivel1: TLargeintField;
    QrVSMovDstDstNivel2: TLargeintField;
    QrVSMovDstDstGGX: TLargeintField;
    QrVSMovDstQtdGerPeca: TFloatField;
    QrVSMovDstQtdGerPeso: TFloatField;
    QrVSMovDstQtdGerArM2: TFloatField;
    QrVSMovDstQtdGerArP2: TFloatField;
    QrVSMovDstQtdAntPeca: TFloatField;
    QrVSMovDstQtdAntPeso: TFloatField;
    QrVSMovDstQtdAntArM2: TFloatField;
    QrVSMovDstQtdAntArP2: TFloatField;
    QrVSMovDstNotaMPAG: TFloatField;
    QrVSMovDstStqCenLoc: TLargeintField;
    QrVSMovDstReqMovEstq: TLargeintField;
    QrVSMovDstGraGru1: TLargeintField;
    QrVSMovDstNO_EstqMovimID: TWideStringField;
    QrVSMovDstNO_MovimID: TWideStringField;
    QrVSMovDstNO_MovimNiv: TWideStringField;
    QrVSMovDstNO_PALLET: TWideStringField;
    QrVSMovDstNO_PRD_TAM_COR: TWideStringField;
    QrVSMovDstNO_FORNECE: TWideStringField;
    QrVSMovDstNO_SerieFch: TWideStringField;
    QrVSMovDstNO_TTW: TWideStringField;
    QrVSMovDstID_TTW: TLargeintField;
    QrVSMovDstNO_LOC_CEN: TWideStringField;
    TPDataHora: TdmkEditDateTimePicker;
    Label58: TLabel;
    EdDataHora: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrVSMovItsAfterOpen(DataSet: TDataSet);
    procedure QrVSMovItsBeforeClose(DataSet: TDataSet);
    procedure QrVSMovItsCalcFields(DataSet: TDataSet);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FatorPeca, FatorPeso, FatorArea, FatorValT: Integer;
    procedure ReopenVSMovIts(Controle: Integer);
  public
    { Public declarations }
    FAtualizaSaldoModoGenerico: Boolean;
    FVSMovIts: Integer;
    (*
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    *)
    //
    procedure DefineImeiAEditar(Controle: Integer);
  end;

  var
  FmVSMovItsAlt: TFmVSMovItsAlt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSMovItsAlt.BtOKClick(Sender: TObject);
var
  //Observ,
  DataHora, Marca, Observ: String;
  //Codigo,
  Controle,(*MovimCod,*) MovimNiv, (*MovimTwn, Empresa, Terceiro, CliVenda,*)
  MovimID(*, LnkNivXtr1, LnkNivXtr2, Pallet, GraGruX, SrcMovID, SrcNivel1,
  SrcNivel2, SrcGGX*), SerieFch, Ficha, Misturou (*FornecMO, DstMovID, DstNivel1,
  DstNivel2, DstGGX, AptoUso, TpCalcAuto*): Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT(*,
  SdoVrtPeca, SdoVrtPeso, SdoVrtArM2, CustoMOKg, CustoMOTot, ValorMP,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2, NotaMPAG*): Double;
  Zerado, (*EmFluxo,*) NotFluxo, StqCenLoc, ReqMovEstq: Integer;
begin
  //Codigo         := ;
  Controle       := QrVSMovItsControle.Value; // >> FVSMovIts
(*  MovimCod       := ;
*)
  MovimNiv       := QrVSMovItsMovimNiv.Value;;
(*  MovimTwn       := ;
  Empresa        := ;
  Terceiro       := ;
  CliVenda       := ;
*)
  MovimID        := QrVSMovItsMovimID.Value;
(*  LnkNivXtr1     := ;
  LnkNivXtr2     := ;
*)
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
(*
  Pallet         := ;
  GraGruX        := ;
*)
  Pecas          := EdPecas.ValueVariant  * FatorPeca;
  PesoKg         := EdPesoKg.ValueVariant * FatorPeso;
  AreaM2         := EdAreaM2.ValueVariant * FatorArea;
  AreaP2         := EdAreaP2.ValueVariant * FatorArea;
  ValorT         := EdValorT.ValueVariant * FatorValT;
(*  SrcMovID       := ;
  SrcNivel1      := ;
  SrcNivel2      := ;
  SrcGGX         := ;
  SdoVrtPeca     := ;
  SdoVrtPeso     := ;
  SdoVrtArM2     := ;
*)
  Observ         := EdObserv.Text;
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;;
  Misturou       := Geral.BoolToInt(CkMisturou.Checked);
(*
  FornecMO       := ;
  CustoMOKg      := ;
  CustoMOTot     := ;
  ValorMP        := ;
  DstMovID       := ;
  DstNivel1      := ;
  DstNivel2      := ;
  DstGGX         := ;
  QtdGerPeca     := ;
  QtdGerPeso     := ;
  QtdGerArM2     := ;
  QtdGerArP2     := ;
  QtdAntPeca     := ;
  QtdAntPeso     := ;
  QtdAntArM2     := ;
  QtdAntArP2     := ;
  AptoUso        := ;
  NotaMPAG       := ;
*)
  Marca          := EdMarca.Text;
(*  TpCalcAuto     := ;
*)
  Zerado   := Geral.BoolToInt(CkZerado.Checked);
  NotFluxo := CGNotFluxo.Value;
  //
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  (*'Codigo', 'MovimCod', 'MovimNiv',
  'MovimTwn', 'Empresa', 'Terceiro',
  'CliVenda', 'MovimID', 'LnkNivXtr1',
  'LnkNivXtr2',*) CO_DATA_HORA_VMI, (*'Pallet',
  'GraGruX',*) 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'(*,
  'SrcMovID', 'SrcNivel1', 'SrcNivel2',
  'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
  'SdoVrtArM2'*), 'Observ', 'SerieFch',
  'Ficha', 'Misturou'(*, 'FornecMO',
  'CustoMOKg', 'CustoMOTot', 'ValorMP',
  'DstMovID', 'DstNivel1', 'DstNivel2',
  'DstGGX', 'QtdGerPeca', 'QtdGerPeso',
  'QtdGerArM2', 'QtdGerArP2', 'QtdAntPeca',
  'QtdAntPeso', 'QtdAntArM2', 'QtdAntArP2',
  'AptoUso', 'NotaMPAG'*), 'Marca',
  (*'TpCalcAuto'*)
  'Zerado', (*'EmFluxo',*) 'NotFluxo',
  'StqCenLoc', 'ReqMovEstq'], [
  'Controle'], [
  (*Codigo, MovimCod, MovimNiv,
  MovimTwn, Empresa, Terceiro,
  CliVenda, MovimID, LnkNivXtr1,
  LnkNivXtr2,*) DataHora, (*Pallet,
  GraGruX,*) Pecas, PesoKg,
  AreaM2, AreaP2, ValorT(*,
  SrcMovID, SrcNivel1, SrcNivel2,
  SrcGGX, SdoVrtPeca, SdoVrtPeso,
  SdoVrtArM2*), Observ, SerieFch,
  Ficha, Misturou(*, FornecMO,
  CustoMOKg, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2,
  DstGGX, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, QtdAntPeca,
  QtdAntPeso, QtdAntArM2, QtdAntArP2,
  AptoUso, NotaMPAG*), Marca,
  (*TpCalcAuto*)
  Zerado, (*EmFluxo,*) NotFluxo,
  StqCenLoc, ReqMovEstq], [
  Controle], True) then
  begin
    if FAtualizaSaldoModoGenerico
    or (Zerado <> QrVSMovItsZerado.Value) then
      VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
    Close;
  end;
end;

procedure TFmVSMovItsAlt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovItsAlt.FormActivate(Sender: TObject);
begin
(*
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
*)
  MyObjects.CorIniComponente();
end;

procedure TFmVSMovItsAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FAtualizaSaldoModoGenerico := False;
  //
  UnDmkDAC_PF.AbreQuery(QrVSSerFch, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSMovItsAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovItsAlt.QrVSMovItsAfterOpen(DataSet: TDataSet);
const
  CtrlSrc = 0;
  CtrlDst = 0;
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  VS_CRC_PF.ReopenVSMovXXX(QrVSMovSrc, 'SrcNivel2', QrVSMovItsControle.Value, TemIMEIMrt, CtrlSrc);
  VS_CRC_PF.ReopenVSMovXXX(QrVSMovDst, 'DstNivel2', QrVSMovItsControle.Value, TemIMEIMrt, CtrlDst);
end;

procedure TFmVSMovItsAlt.QrVSMovItsBeforeClose(DataSet: TDataSet);
begin
  QrVSMovSrc.Close;
  QrVSMovDst.Close;
end;

procedure TFmVSMovItsAlt.QrVSMovItsCalcFields(DataSet: TDataSet);
begin
  QrVSMovItsNO_SrcMovID.Value    := sEstqMovimID[QrVSMovItsSrcMovID.Value];
  QrVSMovItsNO_DstMovID.Value    := sEstqMovimID[QrVSMovItsDstMovID.Value];
end;

procedure TFmVSMovItsAlt.DefineImeiAEditar(Controle: Integer);
begin
  FVSMovIts := Controle;
  ReopenVSMovIts(FVSMovIts);
(* ini 2023-04-10
   // Inutilizado! dando problemas!!!
  if QrVSMovItsPecas.Value < 0 then
    FatorPeca := -1
  else
    FatorPeca := 1;
  if QrVSMovItsAreaM2.Value < 0 then
    FatorArea := -1
  else
    FatorArea := 1;
  if QrVSMovItsPesoKg.Value < 0 then
    FatorPeso := -1
  else
    FatorPeso := 1;
  if QrVSMovItsValorT.Value < 0 then
    FatorValT := -1
  else
    FatorValT := 1;
*)
  FatorPeca := 1;
  FatorArea := 1;
  FatorArea := 1;
  FatorPeso := 1;
  FatorValT := 1;
  // fim 2023-04-10
  EdPecas.ValueVariant  := QrVSMovItsPecas.Value  * FatorPeca;
  EdAreaM2.ValueVariant := QrVSMovItsAreaM2.Value * FatorArea;
  EdAreaP2.ValueVariant := QrVSMovItsAreaP2.Value * FatorArea;
  EdPesoKg.ValueVariant := QrVSMovItsPesoKg.Value * FatorPeso;
  EdValorT.ValueVariant := QrVSMovItsValorT.Value * FatorValT;
  EdObserv.ValueVariant := QrVSMovItsObserv.Value;
  //
  EdSerieFch.ValueVariant := QrVSMovItsSerieFch.Value;
  CBSerieFch.KeyValue     := QrVSMovItsSerieFch.Value;
  EdFicha.ValueVariant    := QrVSMovItsFicha.Value;
  EdMarca.Text            := QrVSMovItsMarca.Value;
  CkMisturou.Checked      := Geral.IntToBool(QrVSMovItsSerieFch.Value);
  //
  CkZerado.Checked        := QrVSMovItsZerado.Value = 1;
  CGNotFluxo.Value        := QrVSMovItsNotFluxo.Value;
  //
  EdStqCenLoc.ValueVariant  := QrVSMovItsStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSMovItsStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSMovItsReqMovEstq.Value;
  //
  EdObserv.Text             := QrVSMovItsObserv.Value;
  //
  TPDataHora.Date           := Trunc(QrVSMovItsDataHora.Value);
  EdDataHora.ValueVariant   := QrVSMovItsDataHora.Value;
  //
end;

procedure TFmVSMovItsAlt.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Area, Peso, Pecs: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Area  := EdAreaM2.ValueVariant;
      Peso  := EdPesoKg.ValueVariant;
      Pecs  := EdPecas.ValueVariant;
      if Area >= 0.01 then
        EdValorT.ValueVariant := Area * Preco
      else
      if Peso >= 0.01 then
        EdValorT.ValueVariant := Peso * Preco
      else
        EdValorT.ValueVariant := Pecs * Preco
    end;
  end;
end;

procedure TFmVSMovItsAlt.ReopenVSMovIts(Controle: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('wmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('wmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*,',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, ',
  'IF(wmi.Terceiro=0, "Desconhecido", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(wmi.Ficha=0, "?????", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?????", vsf.Nome), " ", wmi.Ficha)) NO_FICHA ',
  //
  'FROM ' + CO_SEL_TAB_VMI + ' wmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.Controle=' + Geral.FF0(Controle),
  '']);
end;

end.
