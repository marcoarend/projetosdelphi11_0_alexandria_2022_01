object FmVSInnNatPend: TFmVSInnNatPend
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-241 :: In Natura Pendente'
  ClientHeight = 301
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 757
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 709
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 661
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 235
        Height = 32
        Caption = 'In Natura Pendente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 235
        Height = 32
        Caption = 'In Natura Pendente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 235
        Height = 32
        Caption = 'In Natura Pendente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 187
    Width = 757
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 753
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 231
    Width = 757
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 611
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 609
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 757
    Height = 139
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 757
      Height = 148
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 208
        Top = 80
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object Panel5: TPanel
        Left = 4
        Top = 4
        Width = 745
        Height = 73
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox13: TGroupBox
          Left = 0
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora compra:'
          TabOrder = 0
          object TPCompraIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkCompraIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
          end
          object CkCompraFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
          end
          object TPCompraFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox1: TGroupBox
          Left = 248
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / sa'#237'da:'
          TabOrder = 1
          object TPViagemIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkViagemIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
          end
          object CkViagemFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
          end
          object TPViagemFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox2: TGroupBox
          Left = 496
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora chegada:'
          TabOrder = 2
          object TPEntradaIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEntradaIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object CkEntradaFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object TPEntradaFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
      object CkItensNaoEncer: TCheckBox
        Left = 8
        Top = 100
        Width = 181
        Height = 17
        Caption = 'Somente itens n'#227'o encerrados.'
        TabOrder = 1
      end
      object CkItensComSaldo: TCheckBox
        Left = 8
        Top = 80
        Width = 181
        Height = 17
        Caption = 'Somente itens com saldo.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object EdMarca: TdmkEdit
        Left = 208
        Top = 96
        Width = 113
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPend: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPendAfterScroll
    SQL.Strings = (
      'SELECT  '
      '(vmi.Pecas - vmi.SdoVrtPeca) / vmi.Pecas * 100 PercPc, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,   '
      ' '
      'vmi.Pecas, vmi.SdoVrtPeca, vmi.*   '
      'FROM vsmovits vmi  '
      'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo  '
      ' '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vmi.MovimID=1  ')
    Left = 152
    object QrPendPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrPendSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrPendCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPendControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPendMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrPendMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrPendMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrPendEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPendTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrPendCliVenda: TIntegerField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrPendMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrPendLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrPendLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrPendDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrPendPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPendGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPendPecas_1: TFloatField
      FieldName = 'Pecas_1'
      Required = True
    end
    object QrPendPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrPendAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrPendAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrPendValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrPendSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrPendSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrPendSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrPendSdoVrtPeca_1: TFloatField
      FieldName = 'SdoVrtPeca_1'
      Required = True
    end
    object QrPendSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrPendObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrPendFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrPendMisturou: TSmallintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrPendCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      Required = True
    end
    object QrPendCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
    end
    object QrPendLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPendDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPendDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPendUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPendUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPendAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPendAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPendSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrPendSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrPendSerieFch: TIntegerField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrPendFornecMO: TIntegerField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrPendValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrPendDstMovID: TIntegerField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrPendDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrPendDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrPendDstGGX: TIntegerField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrPendQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      Required = True
    end
    object QrPendQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      Required = True
    end
    object QrPendQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      Required = True
    end
    object QrPendQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      Required = True
    end
    object QrPendQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      Required = True
    end
    object QrPendQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      Required = True
    end
    object QrPendQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      Required = True
    end
    object QrPendQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      Required = True
    end
    object QrPendAptoUso: TSmallintField
      FieldName = 'AptoUso'
      Required = True
    end
    object QrPendNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrPendMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrPendTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
      Required = True
    end
    object QrPendZerado: TSmallintField
      FieldName = 'Zerado'
      Required = True
    end
    object QrPendEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
      Required = True
    end
    object QrPendLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
      Required = True
    end
    object QrPendCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      Required = True
    end
    object QrPendNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
      Required = True
    end
    object QrPendFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
      Required = True
    end
    object QrPendFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
      Required = True
    end
    object QrPendPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrPendPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrPendPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrPendGSPInnNiv2: TIntegerField
      FieldName = 'GSPInnNiv2'
      Required = True
    end
    object QrPendGSPArtNiv2: TIntegerField
      FieldName = 'GSPArtNiv2'
      Required = True
    end
    object QrPendReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrPendStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrPendItemNFe: TIntegerField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrPendVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
      Required = True
    end
    object QrPendVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrPendClientMO: TIntegerField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrPendCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrPendKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
      Required = True
    end
    object QrPendNFeSer: TSmallintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrPendNFeNum: TIntegerField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrPendVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrPendGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrPendJmpMovID: TIntegerField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrPendJmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrPendJmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrPendJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrPendRmsMovID: TIntegerField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrPendRmsNivel1: TIntegerField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrPendRmsNivel2: TIntegerField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrPendRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrPendGSPSrcMovID: TIntegerField
      FieldName = 'GSPSrcMovID'
      Required = True
    end
    object QrPendGSPSrcNiv2: TIntegerField
      FieldName = 'GSPSrcNiv2'
      Required = True
    end
    object QrPendGSPJmpMovID: TIntegerField
      FieldName = 'GSPJmpMovID'
      Required = True
    end
    object QrPendGSPJmpNiv2: TIntegerField
      FieldName = 'GSPJmpNiv2'
      Required = True
    end
    object QrPendDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrPendMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
      Required = True
    end
    object QrPendIxxMovIX: TSmallintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrPendIxxFolha: TIntegerField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrPendIxxLinha: TIntegerField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrPendAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrPendAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrPendIuvpei: TIntegerField
      FieldName = 'Iuvpei'
      Required = True
    end
    object QrPendCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrPendCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      Required = True
    end
    object QrPendCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      Required = True
    end
    object QrPendCustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
      Required = True
    end
    object QrPendCustoComiss: TFloatField
      FieldName = 'CustoComiss'
      Required = True
    end
    object QrPendPerceComiss: TFloatField
      FieldName = 'PerceComiss'
      Required = True
    end
    object QrPendCusKgComiss: TFloatField
      FieldName = 'CusKgComiss'
      Required = True
    end
    object QrPendCredValrImposto: TFloatField
      FieldName = 'CredValrImposto'
      Required = True
    end
    object QrPendCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
      Required = True
    end
    object QrPendRpICMS: TFloatField
      FieldName = 'RpICMS'
      Required = True
    end
    object QrPendRpICMSST: TFloatField
      FieldName = 'RpICMSST'
      Required = True
    end
    object QrPendRpPIS: TFloatField
      FieldName = 'RpPIS'
      Required = True
    end
    object QrPendRpCOFINS: TFloatField
      FieldName = 'RpCOFINS'
      Required = True
    end
    object QrPendRvICMS: TFloatField
      FieldName = 'RvICMS'
      Required = True
    end
    object QrPendRvICMSST: TFloatField
      FieldName = 'RvICMSST'
      Required = True
    end
    object QrPendRvPIS: TFloatField
      FieldName = 'RvPIS'
      Required = True
    end
    object QrPendRvCOFINS: TFloatField
      FieldName = 'RvCOFINS'
      Required = True
    end
    object QrPendRpIPI: TFloatField
      FieldName = 'RpIPI'
      Required = True
    end
    object QrPendRvIPI: TFloatField
      FieldName = 'RvIPI'
      Required = True
    end
    object QrPendGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPendNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPendPercPc: TFloatField
      FieldName = 'PercPc'
    end
  end
  object DsPend: TDataSource
    DataSet = QrPend
    Left = 152
    Top = 48
  end
  object frxWET_CURTI_241_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_241_01GetValue
    Left = 152
    Top = 96
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPend
        DataSetName = 'frxDsPend'
      end
      item
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 98.267780000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'In Natura Pendente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo compra:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoCompra]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 60.472480000000000000
          Width = 298.582821180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Marca]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 79.370130000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#231' entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 79.370130000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo sa'#237'da:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 41.574830000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoViagem]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo entrada:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 60.472480000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PeriodoEntrada]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 79.370130000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#231' saldo')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 79.370130000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'a')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPend
        DataSetName = 'frxDsPend'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPend."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Marca'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPend."Marca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 7.559060000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo gerado')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 26.456710000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 26.456710000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 26.456710000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 26.456710000000000000
          Width = 124.724387480000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'PercPc'
          DataSet = frxDsPend
          DataSetName = 'frxDsPend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPend."PercPc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 26.456710000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% P'#231)
          ParentFont = False
          WordWrap = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
        RowCount = 0
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'Marca'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."Marca"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'AreaM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."AreaM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'MediaM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."MediaM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'kgM2'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."kgM2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 124.724387480000000000
          Height = 18.897650000000000000
          DataField = 'Observ'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."Observ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          DataField = 'PercPc'
          DataSet = frxDsItens
          DataSetName = 'frxDsItens'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PercPc"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsPend: TfrxDBDataset
    UserName = 'frxDsPend'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pecas=Pecas'
      'SdoVrtPeca=SdoVrtPeca'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas_1=Pecas_1'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca_1=SdoVrtPeca_1'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'KgCouPQ=KgCouPQ'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'GGXRcl=GGXRcl'
      'JmpMovID=JmpMovID'
      'JmpNivel1=JmpNivel1'
      'JmpNivel2=JmpNivel2'
      'JmpGGX=JmpGGX'
      'RmsMovID=RmsMovID'
      'RmsNivel1=RmsNivel1'
      'RmsNivel2=RmsNivel2'
      'RmsGGX=RmsGGX'
      'GSPSrcMovID=GSPSrcMovID'
      'GSPSrcNiv2=GSPSrcNiv2'
      'GSPJmpMovID=GSPJmpMovID'
      'GSPJmpNiv2=GSPJmpNiv2'
      'DtCorrApo=DtCorrApo'
      'MovCodPai=MovCodPai'
      'IxxMovIX=IxxMovIX'
      'IxxFolha=IxxFolha'
      'IxxLinha=IxxLinha'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Iuvpei=Iuvpei'
      'CusFrtAvuls=CusFrtAvuls'
      'CusFrtMOEnv=CusFrtMOEnv'
      'CusFrtMORet=CusFrtMORet'
      'CustoMOPc=CustoMOPc'
      'CustoComiss=CustoComiss'
      'PerceComiss=PerceComiss'
      'CusKgComiss=CusKgComiss'
      'CredValrImposto=CredValrImposto'
      'CredPereImposto=CredPereImposto'
      'RpICMS=RpICMS'
      'RpICMSST=RpICMSST'
      'RpPIS=RpPIS'
      'RpCOFINS=RpCOFINS'
      'RvICMS=RvICMS'
      'RvICMSST=RvICMSST'
      'RvPIS=RvPIS'
      'RvCOFINS=RvCOFINS'
      'RpIPI=RpIPI'
      'RvIPI=RvIPI'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercPc=PercPc')
    DataSet = QrPend
    BCDToCurrency = False
    DataSetOptions = []
    Left = 152
    Top = 144
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      'SELECT -vmi.Pecas Pecas, -vmi.PesoKg PesoKg, '
      '-vmi.QtdGerArM2 AreaM2, '
      'vmi.QtdGerArM2 / vmi.Pecas MediaM2, '
      'vmi.PesoKg / vmi.QtdGerArM2 kgM2,   '
      'vmi.Codigo, vmi.MovimCod, vmi.Controle,   '
      'vmi.DstGGX, vmi.SrcGGX, vmi.SrcNivel1, vmi.SrcNivel2,  '
      'ggx.GraGru1, CONCAT(gg1.Nome,   '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),    '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    '
      'NO_PRD_TAM_COR,  '
      'vmi.Marca, vmi.Observ  '
      'FROM vsmovits vmi    '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.DstGGX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'WHERE SrcMovID=1 '
      'AND SrcNivel1=9222 ')
    Left = 316
    Top = 4
    object QrItensPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrItensPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrItensAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrItensMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrItenskgM2: TFloatField
      FieldName = 'kgM2'
    end
    object QrItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItensMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrItensDstGGX: TIntegerField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrItensSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrItensSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrItensSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrItensGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrItensNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrItensObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrItensMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrItensPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
  end
  object frxDsItens: TfrxDBDataset
    UserName = 'frxDsItens'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'MediaM2=MediaM2'
      'kgM2=kgM2'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'DstGGX=DstGGX'
      'SrcGGX=SrcGGX'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Marca=Marca'
      'PercPc=PercPc')
    DataSet = QrItens
    BCDToCurrency = False
    DataSetOptions = []
    Left = 316
    Top = 56
  end
  object QrSumP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(vmi.SdoVrtPeca) / SUM(vmi.Pecas) * 100 PercPc,  '
      'SUM(vmi.Pecas) Pecas, SUM(vmi.SdoVrtPeca) SdoVrtPeca,'
      'SUM(vmi.PesoKg) PesoKg '
      'FROM vsmovits vmi   '
      'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo   '
      'WHERE vmi.MovimID=1   '
      
        'AND vic.DtEntrada  BETWEEN "2023-01-21" AND "2023-02-20 23:59:59' +
        '" '
      'AND vmi.SdoVrtPeca > 0   ')
    Left = 390
    Top = 2
    object QrSumPPercPc: TFloatField
      FieldName = 'PercPc'
    end
    object QrSumPPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumPPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
end
