unit VSGerArtItsCurUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, AppListas, dmkCheckGroup, UnProjGroup_Consts, UnAppEnums,
  BlueDermConsts;

type
  TFmVSGerArtItsCurUni = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosCodigo: TIntegerField;
    QrAptosControle: TIntegerField;
    QrAptosMovimCod: TIntegerField;
    QrAptosMovimNiv: TIntegerField;
    QrAptosMovimTwn: TIntegerField;
    QrAptosEmpresa: TIntegerField;
    QrAptosTerceiro: TIntegerField;
    QrAptosCliVenda: TIntegerField;
    QrAptosMovimID: TIntegerField;
    QrAptosLnkNivXtr1: TIntegerField;
    QrAptosLnkNivXtr2: TIntegerField;
    QrAptosDataHora: TDateTimeField;
    QrAptosPallet: TIntegerField;
    QrAptosGraGruX: TIntegerField;
    QrAptosPecas: TFloatField;
    QrAptosPesoKg: TFloatField;
    QrAptosAreaM2: TFloatField;
    QrAptosAreaP2: TFloatField;
    QrAptosSrcMovID: TIntegerField;
    QrAptosSrcNivel1: TIntegerField;
    QrAptosSrcNivel2: TIntegerField;
    QrAptosObserv: TWideStringField;
    QrAptosLk: TIntegerField;
    QrAptosDataCad: TDateField;
    QrAptosDataAlt: TDateField;
    QrAptosUserCad: TIntegerField;
    QrAptosUserAlt: TIntegerField;
    QrAptosAlterWeb: TSmallintField;
    QrAptosAtivo: TSmallintField;
    QrAptosFicha: TIntegerField;
    QrAptosMisturou: TSmallintField;
    QrAptosCustoMOKg: TFloatField;
    QrAptosCustoMOTot: TFloatField;
    QrAptosNO_PRD_TAM_COR: TWideStringField;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    DBGAptos: TDBGrid;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrAptosSerieFch: TIntegerField;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrAptosNO_SerieFch: TWideStringField;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    QrAptosMarca: TWideStringField;
    QrNiv1: TmySQLQuery;
    QrNiv1FatorInt: TFloatField;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    Label6: TLabel;
    EdControleBxa: TdmkEdit;
    LaPecasBxa: TLabel;
    EdPecasBxa: TdmkEdit;
    LaPesoKgBxa: TLabel;
    EdPesoKgBxa: TdmkEdit;
    SbPesoKgBxa: TSpeedButton;
    LaQtdGerArM2Bxa: TLabel;
    EdAreaM2Bxa: TdmkEditCalc;
    LaQtdGerArP2Bxa: TLabel;
    EdAreaP2Bxa: TdmkEditCalc;
    Label9: TLabel;
    EdObservBxa: TdmkEdit;
    CGTpCalcAutoBxa: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    LaQtdGerArM2Src: TLabel;
    LaQtdGerArP2Src: TLabel;
    Label12: TLabel;
    EdControleSrc: TdmkEdit;
    EdPecasSrc: TdmkEdit;
    EdPesoKgSrc: TdmkEdit;
    EdQtdGerArM2Src: TdmkEditCalc;
    EdQtdGerArP2Src: TdmkEditCalc;
    EdObservSrc: TdmkEdit;
    Label13: TLabel;
    EdMovimTwn: TdmkEdit;
    QrNiv1MediaMinM2: TFloatField;
    QrNiv1MediaMaxM2: TFloatField;
    Label8: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    CkSemArea: TCheckBox;
    QrAptosValorMP: TFloatField;
    QrAptosCustoPQ: TFloatField;
    QrAptosSdoPeca: TFloatField;
    QrAptosSdoPeso: TFloatField;
    QrAptosSdoArM2: TFloatField;
    QrAptosSdoArP2: TFloatField;
    QrAptosNFeSer: TSmallintField;
    QrAptosNFeNum: TIntegerField;
    QrAptosVSMulNFeCab: TIntegerField;
    QrGGXJmp: TmySQLQuery;
    QrGGXJmpGraGru1: TIntegerField;
    QrGGXJmpControle: TIntegerField;
    QrGGXJmpNO_PRD_TAM_COR: TWideStringField;
    QrGGXJmpSIGLAUNIDMED: TWideStringField;
    QrGGXJmpCODUSUUNIDMED: TIntegerField;
    QrGGXJmpNOMEUNIDMED: TWideStringField;
    DsGGXJmp: TDataSource;
    Label20: TLabel;
    EdJmpGGX: TdmkEditCB;
    CBJmpGGX: TdmkDBLookupComboBox;
    QrAptosDstGGX: TIntegerField;
    SbStqCenLoc: TSpeedButton;
    QrAptosValorT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBGAptosDblClick(Sender: TObject);
    procedure EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPecasBxaChange(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure SbPesoKgBxaClick(Sender: TObject);
    procedure EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrGGXJmpAfterOpen(DataSet: TDataSet);
    procedure SbStqCenLocClick(Sender: TObject);
  private
    { Private declarations }
    FFatorIntSrc, FFatorIntDst: Integer;
    FMediaMinM2, FMediaMaxM2: Double;
    //
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure FechaPesquisa();
    procedure ReopenVSGerArtSrc(Controle: Integer);
    function  ValorMPParcial(): Double;
    function  ValorTParcial(): Double;
    function  ValorMOTot(): Double;
    procedure LiberaEdicaoUni(Libera: Boolean);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOrigMovimNiv: TEstqMovimNiv;
    FEmpresa, FClientMO, FFornecMO, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea: Integer;
    FCustoMOKg: Double;
    FDataHora: TDateTime;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSGerArtItsCurUni: TFmVSGerArtItsCurUni;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSGerArtCab, UnVS_CRC_PF, GetValor;

{$R *.DFM}

procedure TFmVSGerArtItsCurUni.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSGerArtItsCurUni.BtOKClick(Sender: TObject);
const
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0; // Nao usar aqui! usar na classificacao???
  Pallet     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  //Misturou   = 0;
  AptoUso    = 1;
  //
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  //
  EdPallet    = nil;
  EdValorT    = nil;
  EdAreaM2    = nil;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda   = 0;
  //
  GSPSrcMovID = emidAjuste;
  GSPSrcNiv2  = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  RmsMovID = emidAjuste;
  RmsNivel1 = 0;
  RmsNivel2= 0;
  GSPJmpMovID = emidAjuste;
  GSPJmpNiv2 = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Fornecedor, SrcNivel1,
  SrcNivel2, DstNivel1, DstNivel2, SerieFch, Ficha, SrcGGX, DstGGX,
  MovimTwn, StqCenLoc, ReqMovEstq: Integer;
  AreaM2, AreaP2, CustoPQ,
  Pecas, PesoKg, ValorMP, ValorT, QtdGerArM2, QtdGerArP2,
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC, CustoMOTot: Double;
  DstMovID, SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  TpCalcAuto, ClientMO: Integer;
  Qry: TmySQLQuery;
  JmpMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX, RmsGGX, MovCodPai, FornecMO: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Controle       := EdControleBxa.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  ClienteMO      := FClientMO;
  FornecMO       := FFornecMO;
  Fornecedor     := QrAptosTerceiro.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidIndsXX;
  GraGruX        := QrAptosGraGruX.Value;
  Pecas          := -EdPecasBxa.ValueVariant;
  PesoKg         := -EdPesoKgBxa.ValueVariant;
  AreaM2         := -EdAreaM2Bxa.ValueVariant;
  AreaP2         := -EdAreaP2Bxa.ValueVariant;
  QtdGerArM2     := -EdQtdGerArM2Src.ValueVariant;
  QtdGerArP2     := -EdQtdGerArP2Src.ValueVariant;
  // ini 2023-04-15
  //ValorMP        := -ValorTParcial();
  //ValorT         := ValorMP;
  ValorMP        := -ValorMPParcial();
  ValorT         := -ValorTParcial();
  CustoPQ        := ValorT - ValorMP;
  // fim 2023-04-15
  CustoMOTot     := 0;
  Observ         := EdObservBxa.Text;
  //
  SerieFch       := QrAptosSerieFch.Value;
  Ficha          := QrAptosFicha.Value;
  Marca          := QrAptosMarca.Value;
  //
  (*DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
  DstNivel1      := EdSrcNivel1.ValueVariant;
  DstNivel2      := EdSrcNivel2.ValueVariant;
  DstGGX         := EdSrcGGX.ValueVariant;*)
  //
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  SrcMovID       := TEstqMovimID(QrAptosMovimID.Value);
  SrcNivel1      := QrAptosCodigo.Value;
  SrcNivel2      := QrAptosControle.Value;
  SrcGGX         := QrAptosGraGruX.Value;
  //
  MovimNiv       := eminBaixCurtiXX;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := 0;
  ClientMO       := FClientMO;
  //
  //2017-11-14
  JmpMovID       := TEstqMovimID.emidCurtido;
  JmpNivel1      := QrAptosCodigo.Value;
  JmpNivel2      := 0;
  JmpGGX         := EdJmpGGX.ValueVariant;
  RmsGGX         := QrAptosDstGGX.Value;
  if MyObjects.FIC(JmpGGX = 0, EdJmpGGX, 'Informe a mat�ria-prima jumped!') then
    Exit;
  MovCodPai      := QrAptosMovimCod.Value;
  //Fim 2017-11-14
  if not CkSemArea.Checked then
    if MyObjects.FIC(-QtdGerArM2 < 0.01, EdQtdGerArM2Src, 'Informe a  �rea!') then
      Exit;
  if not VS_CRC_PF.AreaEstaNaMedia(EdPecasSrc.ValueVariant, -QtdGerArM2, FMediaMinM2, FMediaMaxM2, True) then
      Exit;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, (*EdFicha*) nil, EdPecasBxa,
    EdAreaM2, EdPesoKgBxa, EdValorT, ExigeFornecedor, CO_GraGruY_2048_VSRibCad,
    ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  //
  if -Pecas > QrAptosSdoPeca.Value then
  begin
    if Geral.MB_Pergunta(
    'A quantidade de pe�as a ser baixada � superior ao estoque dispon�vel!' +
    sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  //
  FatorMP  := VS_CRC_PF.FatorNotaCC(QrAptosSrcNivel2.Value, QrAptosMovimID.Value);
  FatorAR  := VS_CRC_PF.FatorNotaAR(FNewGraGruX);
  NotaMPAG := VS_CRC_PF.NotaCouroRibeiraApuca(-Pecas, -PesoKg, -QtdGerArM2, FatorMP, FatorAR);
  FatNotaVRC := FatorAR;
  FatNotaVNC := FatorMP;

  //
  TpCalcAuto := CGTpCalcAutoBxa.Value;
  // Baixa couro In Natura
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  // 2015-05-09 Calcular Nota MPAG
    DstMovID       := MovimID;
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdControleSrc.ValueVariant;
    DstNivel2      := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, DstNivel2);
    DstGGX         := EdSrcGGX.ValueVariant;
    // FIM 2015-05-09 Calcular Nota MPAG
  //
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID,
  DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
 (*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  *)
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei019(*Gera��o de artigo (UNI) de ribeira de curtimento*)) then
  begin
    VS_CRC_PF.AtualizaCustoPQ(Controle, CustoPQ); // 2023-05-10
    VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
    VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrAptosNFeSer.Value,
      QrAptosNFeNum.Value, QrAptosVSMulNFeCab.Value);
    // 2015-05-09 Calcular Nota MPAG
    // Geracao Couro Curtido
    //Controle       := EdControleSrc.ValueVariant;
    Controle       := DstNivel2;
    // FIM 2015-05-09 Calcular Nota MPAG
    //
    MovimNiv       := eminSorcCurtiXX;
    GraGruX        := EdSrcGGX.ValueVariant;
    Pecas          := EdPecasSrc.ValueVariant;
    PesoKg         := EdPesoKgBxa.ValueVariant;
    AreaM2         := 0;
    AreaP2         := 0;
    QtdGerArM2     := EdQtdGerArM2Src.ValueVariant;
    QtdGerArP2     := EdQtdGerArP2Src.ValueVariant;
    // ini 2023-04-15
    //ValorMP        := -ValorTParcial();
    //ValorT         := ValorMP;
    ValorMP        := ValorMPParcial();
    ValorT         := ValorTParcial() + ValorMOTot();
    // fim 2023-04-15
    CustoMOTot     := ValorMOTot();
    Observ         := EdObservSrc.Text;
    //
    DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdSrcNivel2.ValueVariant;
    DstGGX         := EdSrcGGX.ValueVariant;
    //
    SrcMovID       := TEstqMovimID(0);
    SrcNivel1      := 0;
    SrcNivel2      := 0;
    SrcGGX         := 0;
    //
    TpCalcAuto     := 0;
    //
    StqCenLoc      := EdStqCenLoc.ValueVariant;
    ReqMovEstq     := EdReqMovEstq.ValueVariant;
    //
    //
    // 2015-05-09 Calcular Nota MPAG
    //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    // FIM 2015-05-09 Calcular Nota MPAG
    if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID,
    DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
    AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei020(*Baixa de mat�ria prima na gera��o de artigo (UNI) de ribeira de curtimento*)) then
    begin
      VS_CRC_PF.AtualizaCustoPQ(Controle, CustoPQ); // 2023-05-10
      VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
      VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrAptosNFeSer.Value,
        QrAptosNFeNum.Value, QrAptosVSMulNFeCab.Value);
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Atualizar dados de baixa do In Natura selecionado
      //VS_CRC_PF.AtualizaSaldoIMEI(QrAptosControle.Value, True);
      VS_CRC_PF.AtualizaSaldoItmCur(QrAptosControle.Value);
      VS_CRC_PF.AtualizaTotaisVSCurCab(QrAptosMovimCod.Value);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.DistribuiCustoIndsVS(FOrigMovimNiv, FOrigMovimCod, FOrigCodigo,
        EdSrcNivel2.ValueVariant);
      //
      FmVSGerArtCab.AtualizaNFeItens();
      //
      FmVSGerArtCab.LocCod(Codigo, Codigo);
      ReopenVSGerArtSrc(Controle);
      //
      if CkContinuar.Checked then
      begin
        EdMovimTwn.ValueVariant       := 0;
        CGTpCalcAutoBxa.Value         := 2;  // So PesoKg a principio!
        //
        ImgTipo.SQLType               := stIns;
        LiberaEdicaoUni(False);
        ReopenItensAptos();
        //
        EdGraGruX.ValueVariant        := 0;
        CBGraGruX.KeyValue            := Null;
        //
        EdControleBxa.ValueVariant    := 0;
        EdPecasBxa.ValueVariant       := 0;
        EdPesoKgBxa.ValueVariant      := 0;
        EdAreaM2Bxa.ValueVariant      := 0;
        EdAreaP2Bxa.ValueVariant      := 0;
        EdObservBxa.Text              := '';
        //
        EdControleSrc.ValueVariant    := 0;
        EdPecasSrc.ValueVariant       := 0;
        EdPesoKgSrc.ValueVariant      := 0;
        EdQtdGerArM2Src.ValueVariant  := 0;
        EdQtdGerArP2Src.ValueVariant  := 0;
        EdObservSrc.Text              := '';
        //
        //erro ! EdGraGruX.SetFocus;
      end else
        Close;
    end;
  end;
end;

procedure TFmVSGerArtItsCurUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtItsCurUni.DBGAptosDblClick(Sender: TObject);
begin
  LiberaEdicaoUni(True);
end;

procedure TFmVSGerArtItsCurUni.DefineTipoArea();
begin
  LaQtdGerArM2Src.Enabled := FTipoArea = 0;
  EdQtdGerArM2Src.Enabled := FTipoArea = 0;
  //
  LaQtdGerArP2Src.Enabled := FTipoArea = 1;
  EdQtdGerArP2Src.Enabled := FTipoArea = 1;
end;

procedure TFmVSGerArtItsCurUni.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurUni.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurUni.EdPecasBxaChange(Sender: TObject);
  procedure CalculaPesoParcial();
  var
    Pecas, PesoKg, AreaM2: Double;
  begin
    Pecas  := EdPecasBxa.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoPeca.Value * QrAptosSdoPeso.Value;
    EdPesoKgBxa.ValueVariant := PesoKg;
    //
    AreaM2 := 0;
    if QrAptosSdoArM2.Value > 0 then
      AreaM2 := Pecas / QrAptosSdoPeca.Value * QrAptosSdoArM2.Value;
    EdAreaM2Bxa.ValueVariant  := AreaM2;
  end;
var
  Pecas: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdPecasBxa.ValueVariant;
  if (QrAptos.State <> dsInactive) and (QrAptos.RecordCount > 0)
  and (Pecas >= QrAptosSdoPeca.Value) then
  begin
    EdPesoKgBxa.ValueVariant  := QrAptosSdoPeso.Value;
    LaPesoKgBxa.Enabled  := False;
    EdPesoKgBxa.Enabled  := False;
    EdAreaM2Bxa.ValueVariant  := QrAptosSdoArM2.Value;
  end else
  begin
    //LaPesoKg.Enabled  := GBGerar.Visible;
    //EdPesoKg.Enabled  := GBGerar.Visible;
    CalculaPesoParcial();
  end;
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdPecasSrc.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst;
end;

procedure TFmVSGerArtItsCurUni.EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Txt: String;
  Pecas: Double;
begin
  if Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
    CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 1;
  //
  if Key = VK_F4 then
  begin
    EdPecasBxa.ValueVariant := QrAptosSdoPeca.Value;
    EdPesoKgBxa.ValueVariant := QrAptosSdoPeso.Value;
    if not Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value + 1;
  end else
  if Key = VK_F5 then
  begin
    Txt := '0,00';
    if InputQuery('Pe�as', 'Informe as pe�as a gerar:', Txt) then
    begin
      Pecas := Geral.DMV(Txt);
      EdPecasBxa.ValueVariant := Pecas / 2;
      EdPecasSrc.ValueVariant := Pecas;
    end;
  end;
end;

procedure TFmVSGerArtItsCurUni.EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Fator: Integer;
begin
  if Key = VK_F5 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdPecasSrc.ValueVariant := EdPecasBxa.ValueVariant * Fator;
  end;
end;

procedure TFmVSGerArtItsCurUni.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurUni.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurUni.FechaPesquisa();
begin
  QrAptos.Close;
end;

procedure TFmVSGerArtItsCurUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtItsCurUni.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));
  VS_CRC_PF.AbreGraGruXY(QrGGXJmp,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1877_VSCouCur) + //',' +
    ')');
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  CGTpCalcAutoBxa.Value := 2; // so peso a principio
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidCurtido);
end;

procedure TFmVSGerArtItsCurUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtItsCurUni.LiberaEdicaoUni(Libera: Boolean);
var
  Status: Boolean;
begin
  if Libera then
    Status := QrAptos.RecordCount > 0
  else
    Status := False;
  //
  if Status then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if (FFatorIntSrc = 0) or (FFatorIntDst = 0) then
    begin
      Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
      'O Artigo "' + FmVSGerArtCab.QrVSGerArtNewNO_PRD_TAM_COR.Value +
      '" n�o est� configurado completamente!' + sLineBreak +
      'Termine sua configura��o antes de continuar!');
      //
      Close;
      Exit;
    end;
    if FFatorIntSrc <> FFatorIntDst then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end;
  //
  GBAptos.Enabled := not Status;

  GBGerar.Visible := Status;
  //
  LaFicha.Enabled := not Status;
  EdFicha.Enabled := not Status;
  LaGraGruX.Enabled := not Status;
  EdGraGruX.Enabled := not Status;
  CBGraGruX.Enabled := not Status;
  LaTerceiro.Enabled := not Status;
  EdTerceiro.Enabled := not Status;
  CBTerceiro.Enabled := not Status;
  BtReabre.Enabled := not Status;
  //
  if Libera and (EdPecasBxa.Enabled) and (EdPecasBxa.Visible) then
    EdPecasBxa.SetFocus;
  //
end;

procedure TFmVSGerArtItsCurUni.QrGGXJmpAfterOpen(DataSet: TDataSet);
begin
  if QrGGXJmp.RecordCount = 1 then
  begin
    EdJmpGGX.ValueVariant := QrGGXJmpControle.Value;
    CBJmpGGX.KeyValue     := QrGGXJmpControle.Value;
  end;
end;

procedure TFmVSGerArtItsCurUni.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSGerArtItsCurUni.ReopenItensAptos();
var
  SQL_GraGruX, SQL_Terceiro, SQL_SerieFch, SQL_Ficha: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
  SQL_Ficha    := '';
  SQL_SerieFch := '';
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND wmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND wmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
  if EdSerieFch.ValueVariant <> 0 then
    SQL_SerieFch  := 'AND wmi.SerieFch=' + Geral.FF0(EdSerieFch.ValueVariant);
  if EdFicha.ValueVariant <> 0 then
    SQL_Ficha  := 'AND wmi.Ficha=' + Geral.FF0(EdFicha.ValueVariant);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'QtdAntPeca-QtdGerPeca SdoPeca, ',
  'QtdAntPeso-QtdGerPeso SdoPeso, ',
  'QtdAntArM2-QtdGerArM2 SdoArM2, ',
  'QtdAntArP2-QtdGerArP2 SdoArP2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  //'LEFT JOIN vsnatart   vna ON vna.VSNatCad=wmi.GraGruX',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidEmProcCur)),
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),
  'AND QtdAntPeca>QtdGerPeca ',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND ClientMO=' + Geral.FF0(FClientMO),
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  'ORDER BY wmi.Controle ',
  '']);
end;

procedure TFmVSGerArtItsCurUni.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtItsCurUni.SbPesoKgBxaClick(Sender: TObject);
var
  ValVar: Variant;
  PesoKg: Double;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, 0, 3, 0,
  '0,000', EdPesoKgBxa.Text, True,
  'Peso kg', 'Informe o peso (kg): ', 0, ValVar) then
  begin
    EdPesoKgBxa.ValueVariant := Geral.DMV(ValVar);
    //FTpCalcAuto := UnAppListas.DefineTpCalcAutoCouro(False, True, False, False);
    if Geral.IntInConjunto(2, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 2;
  end else;
end;

procedure TFmVSGerArtItsCurUni.SbStqCenLocClick(Sender: TObject);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidCurtido);
end;

function TFmVSGerArtItsCurUni.ValorMOTot(): Double;
begin
  Result := EdPesoKgBxa.ValueVariant * FCustoMOKg;
end;

function TFmVSGerArtItsCurUni.ValorMPParcial(): Double;
var
  ValorMP: Double;
begin
  ValorMP := - QrAptosValorMP.Value;
  //
(* n�o pode! � o peso curtido e n�o o peso VS!!!
  if QrAptosPesoKg.Value < 0 then
    Result := -(EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value) * ValorMP
  else
  if QrAptosAreaM2.Value < 0 then
    Result := -(EdAreaM2Bxa.ValueVariant / QrAptosAreaM2.Value) * ValorMP
  else
*)
  if QrAptosPecas.Value < 0 then
    Result := -(EdPecasBxa.ValueVariant / QrAptosPecas.Value) * ValorMP
  else
    Result := 0;
end;

function TFmVSGerArtItsCurUni.ValorTParcial(): Double;
var
  ValorCurtido: Double;
begin
  // n�o pode!!! duplica custo de curtimento !
  //ValorCurtido := - (QrAptosValorT.Value + QrAptosCustoPQ.Value);
  // ValorT deve ser igual a ValorMP + CustoPQ + CustoMOTot
  //ValorCurtido := - (QrAptosValorMP.Value + QrAptosCustoPQ.Value);
  ValorCurtido := - QrAptosValorT.Value;
  //
(* n�o pode! � o peso curtido e n�o o peso VS!!!
  if QrAptosPesoKg.Value < 0 then
    Result := -(EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value) * ValorCurtido
  else
  if QrAptosAreaM2.Value < 0 then
    Result := -(EdAreaM2Bxa.ValueVariant / QrAptosAreaM2.Value) * ValorCurtido
  else
*)
  if QrAptosPecas.Value < 0 then
    Result := -(EdPecasBxa.ValueVariant / QrAptosPecas.Value) * ValorCurtido
  else
    Result := 0;
end;

end.
