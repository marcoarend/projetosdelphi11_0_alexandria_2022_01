unit VSMOPWEGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables,
  UnProjGroup_Consts, dmkDBGridZTO, UnAppEnums;

type
  TFmVSMOPWEGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Qr00Fornecedor: TmySQLQuery;
    Qr00FornecedorCodigo: TIntegerField;
    Qr00FornecedorNOMEENTIDADE: TWideStringField;
    Ds00Fornecedor: TDataSource;
    Panel5: TPanel;
    Label55: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    DBG19_22: TdmkDBGridZTO;
    Qr19_22: TmySQLQuery;
    Ds19_22: TDataSource;
    BtReabre: TBitBtn;
    QrDstA: TmySQLQuery;
    DsDstA: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    Qr19_22Codigo: TIntegerField;
    Qr19_22MovimCod: TIntegerField;
    Qr19_22Controle: TIntegerField;
    Qr19_22FornecMO: TIntegerField;
    Qr19_22DataHora: TDateTimeField;
    Qr19_22GraGruX: TIntegerField;
    Qr19_22CustoA: TFloatField;
    Qr19_22AreaM2: TFloatField;
    Qr19_22PesoKg: TFloatField;
    Qr19_22ValorT: TFloatField;
    Qr19_22CustoMOTot: TFloatField;
    Qr19_22ValorMP: TFloatField;
    Qr19_22CustoPQ: TFloatField;
    QrDstACodigo: TIntegerField;
    QrDstAMovimCod: TIntegerField;
    QrDstAMovimID: TIntegerField;
    QrDstAMovimNiv: TIntegerField;
    QrDstAControle: TIntegerField;
    QrDstADataHora: TDateTimeField;
    QrDstAGraGruX: TIntegerField;
    QrDstACustoMedio: TFloatField;
    QrDstAAreaM2: TFloatField;
    QrDstAPesoKg: TFloatField;
    QrDstAValorT: TFloatField;
    QrDstACustoMOTot: TFloatField;
    QrDstAValorMP: TFloatField;
    QrDstACustoPQ: TFloatField;
    Splitter1: TSplitter;
    EdMOM2: TdmkEdit;
    Label1: TLabel;
    QrDstACustoMOM2: TFloatField;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrDstAMovimTwn: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Qr19_22BeforeClose(DataSet: TDataSet);
    procedure Qr19_22AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenDst(Query: TmySQLQuery; SrcNivel2: Integer);
    procedure AtualizaSubItens(Query: TmySQLQuery; CustoAllM2: Double);
    procedure ReopenPesquisa();
  public
    { Public declarations }
  end;

  var
  FmVSMOPWEGer: TFmVSMOPWEGer;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnDmkProcFunc, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmVSMOPWEGer.AtualizaSubItens(Query: TmySQLQuery; CustoAllM2: Double);
var
  AreaM2, ValorT: Double;
  Controle: Integer;
  Qry: TmySQLQuery;
begin
  Query.First;
  while not Query.Eof do
  begin
    AreaM2 := Query.FieldByName('AreaM2').AsFloat;
    ValorT := CustoAllM2 * AreaM2;
    //
    Controle := Query.FieldByName('Controle').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'ValorT'], [
    'Controle'], [
    ValorT], [
    Controle], True);
    //
    case TEstqMovimID(Query.FieldByName('MovimID').AsInteger) of
      (*02*)emidVenda: ; // nada
      (*25*)emidTransfLoc:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrTwn, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimTwn <> 0',
        'AND MovimTwn=' + Geral.FF0(Query.FieldByName('MovimTwn').AsInteger),
        'AND MovimNiv=' + Geral.FF0(Integer(eminDestLocal)),
        '']);
        //
        Qry := TmySQLQuery.Create(Dmod);
        try
          QrTwn.First;
          while not QrTwn.Eof do
          begin
            ReopenDst(Qry, QrTwnControle.Value);
            AtualizaSubItens(Qry, CustoAllM2);
          end;
        finally
          Qry.Free;
        end;
      end;
      else Geral.MB_Erro('MovimID n�o implementado em "AtualizaSubItens"');
    end;
    //
    Query.Next;
  end;
end;

procedure TFmVSMOPWEGer.BtOKClick(Sender: TObject);
var
  Txt: String;
  ValorT, AreaM2, CustoM2, CustoMOM2, CustoMOTot, CustoAllM2: Double;
  Controle, I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  Txt := '';

  //////////////////////////////////////////////////////////////////////////////

(*
  Qr19_22.First;
  while not Qr19_22.Eof do
  begin
*)
  with DBG19_22.DataSource.DataSet do
  for I := 0 to DBG19_22.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG19_22.SelectedRows.Items[I]));
    GotoBookmark(DBG19_22.SelectedRows.Items[I]);
    //
    QrDstA.First;
    while not QrDstA.Eof do
    begin
      if ((QrDstAMovimID.Value = 02) and (QrDstAMovimNiv.Value = 00))
      or ((QrDstAMovimID.Value = 25) and (QrDstAMovimNiv.Value = 27)) then
        // nada
      else
      begin
        Txt := Txt + 'IME-I ' + Geral.FF0(QrDstAControle.Value) + ': ' +
        'MovimID = ' + Geral.FF0(QrDstAMovimID.Value) + ' e MovimNiv = ' +
        Geral.FF0(QrDstAMovimNiv.Value) + sLineBreak;
      end;
      QrDstA.Next;
    end;
    //Qr19_22.Next;
  end;
  if Txt <> '' then
  begin
    Geral.MB_Erro('Falta de implementa��o:' + sLinebreak + Txt);
    Exit;
  end;

  //////////////////////////////////////////////////////////////////////////////

  CustoMOM2 := EdMOM2.ValueVariant;
  if Geral.MB_Pergunta(
  'Confirma a altera��o da MO dos itens pesquisados para $ ' + EdMOM2.Text +
  '?') <> ID_YES then
    Exit;
  //
(*
  Qr19_22.First;
  while not Qr19_22.Eof do
  begin
*)
  with DBG19_22.DataSource.DataSet do
  for I := 0 to DBG19_22.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG19_22.SelectedRows.Items[I]));
    GotoBookmark(DBG19_22.SelectedRows.Items[I]);
    //
    AreaM2     := Qr19_22AreaM2.Value;
    CustoMOTot := AreaM2 * CustoMoM2;
    ValorT     := CustoMOTot + Qr19_22ValorMP.Value + Qr19_22CustoPQ.Value;
    Controle := Qr19_22Controle.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'CustoMOM2', 'CustoMOTot', 'ValorT'], [
    'Controle'], [
    CustoMOM2, CustoMOTot, ValorT], [
    Controle], True) then
    begin
      if AreaM2 <> 0 then
      begin
        CustoAllM2 := ValorT / AreaM2;
        AtualizaSubItens(QrDstA, CustoAllM2);
      end;
    end;
    //
    //Qr19_22.Next;
  end;

  //////////////////////////////////////////////////////////////////////////////
  ReopenPesquisa();

  //////////////////////////////////////////////////////////////////////////////
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSMOPWEGer.BtReabreClick(Sender: TObject);
begin
  ReopenPesquisa();
end;

procedure TFmVSMOPWEGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOPWEGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOPWEGer.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(Qr00Fornecedor, Dmod.MyDB);
  ImgTipo.SQLType := stLok;
  TPDataIni.Date := Date - 90;
  TPDataFim.Date := Date;
end;

procedure TFmVSMOPWEGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOPWEGer.Qr19_22AfterScroll(DataSet: TDataSet);
begin
  ReopenDst(QrDstA, Qr19_22Controle.Value);
end;

procedure TFmVSMOPWEGer.Qr19_22BeforeClose(DataSet: TDataSet);
begin
  QrDstA.Close;
end;

procedure TFmVSMOPWEGer.ReopenDst(Query: TmySQLQuery; SrcNivel2: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT vmi.Codigo, vmi.MovimCod, vmi.MovimID,',
  'vmi.MovimNiv, vmi.MovimTwn, vmi.Controle, ',
  'vmi.DataHora, vmi.GraGruX, vmi.CustoMOM2, ',
  'vmi.ValorT/vmi.AreaM2 CustoMedio, ',
  'vmi.AreaM2, vmi.PesoKg, vmi.ValorT, ',
  'vmi.CustoMOTot, vmi.ValorMP, vmi.CustoPQ ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  '']);
end;

procedure TFmVSMOPWEGer.ReopenPesquisa();
var
  Terceiro: Integer;
  SQL_Periodo, SQL_Terceiro: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE vmi.DataHora ',
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  Terceiro := EdTerceiro.ValueVariant;
  if Terceiro = 0 then
    SQL_Terceiro := ''
  else
    SQL_Terceiro := 'WHERE FornecMO=' + Geral.FF0(Terceiro);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr19_22, DModG.MyPId_DB, [
  'DROP TABLE IF EXISTS mo_19_21;',
  'CREATE TABLE MO_19_21',
  'SELECT vmi.Codigo, vmi.MovimCod, ',
  'vmi.Controle, vmi.MovimTwn, vmi.FornecMO,  ',
  'vmi.DataHora, vmi.GraGruX, ',
  'vmi.CustoMOTot/vmi.AreaM2 CustoA, ',
  'vmi.AreaM2, vmi.PesoKg, vmi.ValorT, ',
  'vmi.CustoMOTot, vmi.ValorMP, vmi.CustoPQ, ',
  'vmi.CustoMOM2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
  'WHERE vmi.MovimID=19 ',
  'AND vmi.MovimNiv=21 ',
  ';',
  'DROP TABLE IF EXISTS mo_20_22;',
  'CREATE TABLE mo_20_22',
  'SELECT vmi.Codigo, vmi.MovimCod, ',
  'vmi.Controle, vmi.MovimTwn, vmi.FornecMO,  ',
  'vmi.DataHora, vmi.GraGruX, ',
  'vmi.CustoMOTot/vmi.AreaM2 CustoA, ',
  'vmi.AreaM2, vmi.PesoKg, vmi.ValorT, ',
  'vmi.CustoMOTot, vmi.ValorMP, vmi.CustoPQ  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
  SQL_Periodo,
  'AND vmi.MovimID=20 ',
  'AND vmi.MovimNiv=22 ',
  ';',
  'SELECT src.CustoMOM2, dst.* ',
  'FROM mo_20_22 dst',
  'LEFT JOIN mo_19_21 src ON src.MovimCod=dst.MovimCod',
  'WHERE dst.MovimCod IN (',
  '  SELECT MovimCod',
  '  FROM mo_19_21',
  '  ' + SQL_Terceiro,
  ')',
  ';',
  '']);
end;

end.
