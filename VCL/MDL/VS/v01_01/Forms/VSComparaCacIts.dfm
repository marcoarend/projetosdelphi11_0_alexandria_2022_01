object FmVSComparaCacIts: TFmVSComparaCacIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-162 :: Impress'#227'o de Campara'#231#227'o de Pallets'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Campara'#231#227'o de Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Campara'#231#227'o de Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Campara'#231#227'o de Pallets'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel64: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 53
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label62: TLabel
            Left = 8
            Top = 8
            Width = 39
            Height = 13
            Caption = 'Pallet A:'
          end
          object Label63: TLabel
            Left = 92
            Top = 8
            Width = 39
            Height = 13
            Caption = 'Pallet B:'
          end
          object EdPalletA: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPalletB: TdmkEdit
            Left = 92
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 68
          Width = 808
          Height = 397
          Align = alClient
          DataSource = DsComparaCacIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PalletA'
              Title.Caption = 'Pallet A'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PalletB'
              Title.Caption = 'Pallet B'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkAgregar: TdmkCheckBox
        Left = 148
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Agregar!!!'
        Enabled = False
        TabOrder = 1
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
  end
  object QrSumA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT(AreaM2) AreaM2, '
      'COUNT(Controle) Itens '
      'FROM vscacitsa'
      'WHERE VSPallet=6676'
      'GROUP BY AreaM2'
      'ORDER BY AreaM2')
    Left = 192
    Top = 164
    object QrSumAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumAItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrItensA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Pecas, AreaM2 '
      'FROM vscacitsa'
      'WHERE VSPallet=6676'
      'GROUP BY AreaM2'
      'ORDER BY AreaM2')
    Left = 192
    Top = 212
    object QrItensAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItensAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrSumB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT(AreaM2) AreaM2, '
      'COUNT(Controle) Itens '
      'FROM vscacitsa'
      'WHERE VSPallet=6676'
      'GROUP BY AreaM2'
      'ORDER BY AreaM2')
    Left = 260
    Top = 164
    object QrSumBAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumBItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrItensB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Pecas, AreaM2 '
      'FROM vscacitsa'
      'WHERE VSPallet=6676'
      'GROUP BY AreaM2'
      'ORDER BY AreaM2')
    Left = 260
    Top = 212
    object QrItensBPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItensBAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrComparaCacIts: TmySQLQuery
   
    Left = 192
    Top = 268
    object QrComparaCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,##0.00'
    end
    object QrComparaCacItsPalletA: TIntegerField
      FieldName = 'PalletA'
    end
    object QrComparaCacItsPalletB: TIntegerField
      FieldName = 'PalletB'
    end
  end
  object DsComparaCacIts: TDataSource
    DataSet = QrComparaCacIts
    Left = 192
    Top = 316
  end
end
