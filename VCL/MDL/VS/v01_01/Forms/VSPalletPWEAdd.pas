unit VSPalletPWEAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, UnAppEnums;

type
  TFmVSPalletPWEAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    DsClientes: TDataSource;
    QrVSPalSta: TmySQLQuery;
    QrVSPalStaCodigo: TIntegerField;
    QrVSPalStaNome: TWideStringField;
    DsVSPalSta: TDataSource;
    QrVSFinCla: TmySQLQuery;
    QrVSFinClaGraGruX: TIntegerField;
    QrVSFinClaNO_PRD_TAM_COR: TWideStringField;
    DsVSFinCla: TDataSource;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    SBCliente: TSpeedButton;
    LaVSRibCla: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCliStat: TdmkEditCB;
    CBCliStat: TdmkDBLookupComboBox;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdQtdPrevPc: TdmkEdit;
    QrVSFinClaPrevPcPal: TIntegerField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label20: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMovimIDGer: TEstqMovimID;
    FPallet: Integer;
  end;

  var
  FmVSPalletPWEAdd: TFmVSPalletPWEAdd;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmVSPalletPWEAdd.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Status         := EdStatus.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  CliStat        := EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  MovimIDGer     := Integer(FMovimIDGer);
  QtdPrevPc      := EdQtdPrevPc.ValueVariant;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'MovimIDGer',
  'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, MovimIDGer,
  QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    FPallet := Codigo;
(*
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
*)
      Close;
  end;
end;

procedure TFmVSPalletPWEAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPalletPWEAdd.EdGraGruXRedefinido(Sender: TObject);
begin
  if (QrVSFinClaPrevPcPal.Value <> 0) and (ImgTipo.SQLType = stIns) then
    EdQtdPrevPc.ValueVariant := QrVSFinClaPrevPcPal.Value;
end;

procedure TFmVSPalletPWEAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPalletPWEAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPallet := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSPalSta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSFinCla, Dmod.MyDB);
end;

procedure TFmVSPalletPWEAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPalletPWEAdd.SBClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliStat.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliStat.Text := IntToStr(VAR_ENTIDADE);
    CBCliStat.KeyValue := VAR_ENTIDADE;
  end;
end;

end.
