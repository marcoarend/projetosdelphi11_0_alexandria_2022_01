unit VSEmitCus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, BlueDermConsts;

type
  TFmVSEmitCus = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    DBGrid1: TDBGrid;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    DsVSPallet: TDataSource;
    Panel3: TPanel;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    Panel5: TPanel;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label9: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    Panel6: TPanel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPeso: TdmkEdit;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaPecas: TLabel;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    RGTipoCouro: TRadioGroup;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    GroupBox3: TGroupBox;
    EdPsqPallet: TdmkEdit;
    Label10: TLabel;
    EdPsqFicha: TdmkEdit;
    Label11: TLabel;
    QrVSMovItsSerieFch: TIntegerField;
    Label12: TLabel;
    EdSrcGGX: TdmkEdit;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrVSMovItsMediaM2: TFloatField;
    Label13: TLabel;
    EdPsqIMEI: TdmkEdit;
    BtReabre: TBitBtn;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    Panel9: TPanel;
    EdCodigo: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label2: TLabel;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    Label38: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    Label39: TLabel;
    CBCouNiv1: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure RGTipoCouroClick(Sender: TObject);
    procedure EdPsqPalletChange(Sender: TObject);
    procedure EdPsqFichaChange(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    //function  DefineProximoPallet(): String;
    procedure ReopenVSMovIts((*GraGruX: Integer*));
    procedure HabilitaInclusao();
    procedure ReopenGGXY();
    
  public
    { Public declarations }
    FDataHora, FDataEmis: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FFormula: Integer;
    FBxaEstqVS: TBxaEstqVS;
  end;

  var
  FmVSEmitCus: TFmVSEmitCus;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSEmitCus.BtOKClick(Sender: TObject);
var
  DataEmis: String;
  Codigo, Controle, MPIn, Formula, MPVIts, VSPedIts, VSMovIts, BxaEstqVS,
  GraGruX: Integer;
  Pecas, Peso, Custo, AreaM2, AreaP2, PercTotCus: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  MPIn           := 0; // Deprecado
  Formula        := FFormula; // Depois? QrEmitNumero.Value;
  BxaEstqVS      := Integer(FBxaEstqVS);  // Depois?
  DataEmis       := Geral.FDT(FDataEmis, 1); //'0000-00-00'; // Depois? QrEmitDataEmis.Value;
  Pecas          := EdPecas.ValueVariant;
  Peso           := EdPeso.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  Custo          := 0; // Calcula Depois ?
  MPVIts         := 0; // Deprecado?
  PercTotCus     := 0; // Depois? Peso do Item / Peso Pesagem * 100 >> % do custo da pesagem;
  VSPedIts       := 0; // ????
  VSMovIts       := EdSrcNivel2.ValueVariant;
  GraGruX        := QrVSMovItsGraGruX.Value;

  //
  Controle := UMyMod.BuscaEmLivreY_Def('emitcus', 'Controle', SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitcus', False, [
  'Codigo', 'MPIn', 'Formula',
  'DataEmis', 'Pecas', 'Peso',
  'Custo', 'MPVIts', 'AreaM2',
  'AreaP2', 'PercTotCus', 'VSPedIts',
  CO_FLD_TAB_VMI, 'BxaEstqVS', 'GraGruX'], [
  'Controle'], [
  Codigo, MPIn, Formula,
  DataEmis, Pecas, Peso,
  Custo, MPVIts, AreaM2,
  AreaP2, PercTotCus, VSPedIts,
  VSMovIts, BxaEstqVS, GraGruX], [
  Controle], True) then
  begin
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      //
      EdSrcMovID.ValueVariant    := 0;
      EdSrcNivel1.ValueVariant   := 0;
      EdSrcNivel2.ValueVariant   := 0;
      EdSrcGGX.ValueVariant      := 0;
      //
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPeso.ValueVariant        := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      //
      EdGraGruX.SetFocus;
      //
      ReopenVSMovIts();
    end else
      Close;
  end;
end;

procedure TFmVSEmitCus.BtReabreClick(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSEmitCus.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSEmitCus.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  EdPeso.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  //
  EdPeso.SetFocus;
(*
  if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
    Valor := QrVSMovItsSdoVrtArM2.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
  else
  if QrVSMovItsPecas.Value > 0 then
    Valor := QrVSMovItsSdoVrtPeca.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  EdValorT.ValueVariant := Valor;
*)
end;

procedure TFmVSEmitCus.DBGrid1DblClick(Sender: TObject);
begin
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;
  //
  try
    EdPecas.SetFocus;
  except
    //
  end;
end;

procedure TFmVSEmitCus.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSEmitCus.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSEmitCus.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSEmitCus.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmVSEmitCus.EdPsqFichaChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSEmitCus.EdPsqPalletChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSEmitCus.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSEmitCus.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSEmitCus.FormCreate(Sender: TObject);
const
  Default = 2;
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.Align := alClient;
  FUltGGX := 0;
  //
  ReopenGGXY();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vspalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  //
  VS_CRC_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS, Default);
end;

procedure TFmVSEmitCus.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSEmitCus.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmVSEmitCus.ReopenGGXY();
begin
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
end;

procedure TFmVSEmitCus.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSEmitCus.ReopenVSMovIts();
const
  StqCenCad = 0;
  StqCenLoc = 0;
  ImeiSrc   = 0;
var
  GraGruX, Empresa, Pallet, Ficha, IMEI, CouNiv1, CouNiv2: Integer;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := EdEmpresa.ValueVariant;
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  VS_CRC_PF.ReopenIMEIsPositivos(QrVSMovIts, Empresa, GraGruX, Pallet, Ficha, IMEI,
    RGTipoCouro.ItemIndex, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1, CouNiv2,
    FDataEmis);
end;

procedure TFmVSEmitCus.RGTipoCouroClick(Sender: TObject);
begin
  ReopenGGXY();
end;

end.
