object FmVSCurOriIMEI: TFmVSCurOriIMEI
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-146 :: Origem de Artigo em Processo de Curtimento'
  ClientHeight = 808
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 3
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label14: TLabel
      Left = 608
      Top = 20
      Width = 51
      Height = 13
      Caption = 'ID Movim.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label1: TLabel
      Left = 676
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID Gera'#231#227'o:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label17: TLabel
      Left = 744
      Top = 20
      Width = 61
      Height = 13
      Caption = 'ID It.Gerado:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 540
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Red. It.Ger.:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdSrcNivel2: TdmkEdit
      Left = 744
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcNivel1: TdmkEdit
      Left = 676
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcMovID: TdmkEdit
      Left = 608
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcGGX: TdmkEdit
      Left = 540
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 549
        Height = 32
        Caption = 'Origem de Artigo em Processo de Curtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 549
        Height = 32
        Caption = 'Origem de Artigo em Processo de Curtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 549
        Height = 32
        Caption = 'Origem de Artigo em Processo de Curtimento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 694
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 738
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 6
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 7
        Top = 3
        Width = 119
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 452
    Align = alClient
    Caption = ' Filtros: '
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 723
      Top = 165
      Width = 5
      Height = 285
      Align = alRight
      ExplicitLeft = 725
    end
    object Splitter2: TSplitter
      Left = 533
      Top = 165
      Width = 5
      Height = 285
      Align = alRight
      ExplicitLeft = 537
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 94
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaGraGruX: TLabel
        Left = 396
        Top = 48
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
      end
      object LaTerceiro: TLabel
        Left = 688
        Top = 48
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object LaFicha: TLabel
        Left = 324
        Top = 48
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
      end
      object Label11: TLabel
        Left = 180
        Top = 48
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Ficha RMP:'
      end
      object Label7: TLabel
        Left = 352
        Top = 8
        Width = 187
        Height = 13
        Caption = 'Cliente interno (cliente de m'#227'o-de-obra):'
      end
      object Label8: TLabel
        Left = 732
        Top = 8
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 396
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 452
        Top = 64
        Width = 233
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXInn
        TabOrder = 4
        OnClick = CBGraGruXClick
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTerceiro: TdmkEditCB
        Left = 688
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 744
        Top = 64
        Width = 125
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 6
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFicha: TdmkEdit
        Left = 328
        Top = 64
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFichaChange
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 876
        Top = 50
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object EdSerieFch: TdmkEditCB
        Left = 180
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSerieFchChange
        DBLookupComboBox = CBSerieFch
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSerieFch: TdmkDBLookupComboBox
        Left = 220
        Top = 64
        Width = 101
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSSerFch
        TabOrder = 1
        dmkEditCB = EdSerieFch
        QryCampo = 'SerieFch'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGArtOrigem: TRadioGroup
        Left = 8
        Top = 4
        Width = 341
        Height = 41
        Caption = ' Origem da mat'#233'ria-prima: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Couros em caleiro'
          'Peles In Natura'
          'Peles Conservadas')
        TabOrder = 8
        OnClick = RGArtOrigemClick
      end
      object EdClientMO: TdmkEditCB
        Left = 352
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        OnRedefinido = EdClientMORedefinido
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 408
        Top = 24
        Width = 317
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCliInt
        TabOrder = 10
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 840
        Top = 24
        Width = 156
        Height = 21
        KeyField = 'Marca'
        ListField = 'Martelo'
        ListSource = DsMarcas
        TabOrder = 11
        OnClick = CBMarcaClick
        dmkEditCB = EdMarca
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMarca: TdmkEditCB
        Left = 732
        Top = 24
        Width = 109
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSerieFchChange
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CkSoClientMO: TCheckBox
        Left = 12
        Top = 64
        Width = 157
        Height = 17
        Caption = 'Somente do cliente de MO.'
        Checked = True
        State = cbChecked
        TabOrder = 13
        OnClick = CkSoClientMOClick
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 2
      Top = 165
      Width = 531
      Height = 285
      Align = alClient
      DataSource = DsEstoque
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnAfterMultiselect = DBG04EstqAfterMultiselect
      OnDblClick = DBG04EstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'IMEI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 98
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdGGX'
          Title.Caption = 'Ordem'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SDOPECA'
          Title.Caption = 'P'#231' n'#227'o curt.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SDOPESO'
          Title.Caption = 'kg n'#227'o curtido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do terceiro'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Total kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end>
    end
    object PnMarcaSemContarPecas: TPanel
      Left = 2
      Top = 109
      Width = 1004
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1004
        Height = 56
        Align = alTop
        Caption = 
          ' Dados gerais da baixa por marca sem contar as pe'#231'as mas pelo pe' +
          'so do ful'#227'o: '
        TabOrder = 0
        object Label20: TLabel
          Left = 96
          Top = 16
          Width = 68
          Height = 13
          Caption = 'Peso kg ful'#227'o:'
        end
        object Label21: TLabel
          Left = 12
          Top = 16
          Width = 72
          Height = 13
          Caption = '% Ganho peso:'
        end
        object EdPesoMarcaSemContarPecas: TdmkEdit
          Left = 96
          Top = 31
          Width = 105
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdGerPeso'
          UpdCampo = 'QtdGerPeso'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdQtdAntPesoKeyDown
        end
        object EdPercGanhoPeso: TdmkEdit
          Left = 12
          Top = 31
          Width = 81
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-99'
          ValMax = '99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '5,0000'
          QryCampo = 'QtdGerPeso'
          UpdCampo = 'QtdGerPeso'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 5.000000000000000000
          ValWarn = False
          OnKeyDown = EdQtdAntPesoKeyDown
        end
      end
    end
    object PanelOriCal: TPanel
      Left = 728
      Top = 165
      Width = 278
      Height = 285
      Align = alRight
      Caption = 'Aguarde... Reabrindo...'
      TabOrder = 3
      object DBGOriCal: TdmkDBGridZTO
        Left = 1
        Top = 45
        Width = 276
        Height = 239
        Align = alClient
        DataSource = DsOriCal
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Ficha'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Marca'
            Width = 102
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Visible = True
          end
          item
            Expanded = False
            Visible = True
          end>
      end
      object PnOriCal: TPanel
        Left = 1
        Top = 1
        Width = 276
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label10: TLabel
          Left = 4
          Top = 4
          Width = 90
          Height = 13
          Caption = 'Saldo pe'#231'as IMEI: '
        end
        object Label12: TLabel
          Left = 124
          Top = 4
          Width = 96
          Height = 13
          Caption = 'Saldo peso kg IMEI:'
        end
        object EdEstoqueSDOPECA: TdmkEdit
          Left = 4
          Top = 19
          Width = 109
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPecasChange
          OnKeyDown = EdPecasKeyDown
        end
        object EdEstoqueSDOPESO: TdmkEdit
          Left = 124
          Top = 19
          Width = 145
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdGerPeso'
          UpdCampo = 'QtdGerPeso'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdQtdAntPesoKeyDown
        end
      end
    end
    object PnOriEmi: TPanel
      Left = 538
      Top = 165
      Width = 185
      Height = 285
      Align = alRight
      Caption = 'Aguarde... Reabrindo...'
      TabOrder = 4
      object DBGOriEmi: TDBGrid
        Left = 1
        Top = 1
        Width = 183
        Height = 283
        Align = alClient
        DataSource = DsEmiOri
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Fulao'
            Title.Caption = 'Ful'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataEmis'
            Title.Caption = 'Dt.Emi.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Pesagem'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Obs'
            Title.Caption = 'Observa'#231#245'es'
            Width = 144
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Numero'
            Title.Caption = 'N'#186' f'#243'rmula'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Nome da f'#243'rmula'
            Width = 147
            Visible = True
          end>
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 620
    Width = 1008
    Height = 74
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 48
      Width = 1008
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object CkContinuar: TCheckBox
        Left = 16
        Top = 4
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      TabStop = True
      object Label50: TLabel
        Left = 8
        Top = 4
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 104
        Top = 4
        Width = 103
        Height = 13
        Caption = 'Mat'#233'ria-prima jumped:'
      end
      object Label63: TLabel
        Left = 540
        Top = 4
        Width = 91
        Height = 13
        Caption = 'Mat'#233'ria-prima DTA:'
      end
      object EdReqMovEstq: TdmkEdit
        Left = 8
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdJmpGGX: TdmkEditCB
        Left = 104
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBJmpGGX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBJmpGGX: TdmkDBLookupComboBox
        Left = 160
        Top = 20
        Width = 377
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXJmp
        TabOrder = 2
        dmkEditCB = EdJmpGGX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdRmsGGX: TdmkEditCB
        Left = 540
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRmsGGX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBRmsGGX: TdmkDBLookupComboBox
        Left = 596
        Top = 20
        Width = 409
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsRmsGGX
        TabOrder = 4
        dmkEditCB = EdRmsGGX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 564
    Width = 1008
    Height = 56
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 56
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object LaPesoKg: TLabel
        Left = 168
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Peso kg ful'#227'o [F3]:'
      end
      object SbCalcula: TSpeedButton
        Left = 260
        Top = 30
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbCalculaClick
      end
      object Label6: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object LaPecas: TLabel
        Left = 92
        Top = 16
        Width = 72
        Height = 13
        Caption = 'Pe'#231'as: [F4][F3]'
      end
      object Label9: TLabel
        Left = 284
        Top = 16
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object Label3: TLabel
        Left = 792
        Top = 16
        Width = 63
        Height = 13
        Caption = 'Peso kg Bxa:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 868
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 936
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object EdQtdAntPeso: TdmkEdit
        Left = 168
        Top = 31
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'QtdGerPeso'
        UpdCampo = 'QtdGerPeso'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdQtdAntPesoKeyDown
      end
      object EdControle: TdmkEdit
        Left = 8
        Top = 31
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 92
        Top = 31
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPecasChange
        OnKeyDown = EdPecasKeyDown
      end
      object EdObserv: TdmkEdit
        Left = 284
        Top = 31
        Width = 505
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 792
        Top = 31
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 868
        Top = 31
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 936
        Top = 31
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
    end
  end
  object QrGGXInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 52
    Top = 376
    object QrGGXInnGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXInnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXInnNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXInnSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXInnCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXInnNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXInn: TDataSource
    DataSet = QrGGXInn
    Left = 52
    Top = 424
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 128
    Top = 376
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 128
    Top = 424
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 200
    Top = 376
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 200
    Top = 424
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEstoqueBeforeClose
    AfterScroll = QrEstoqueAfterScroll
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.Empresa, wmi.GraGruX, wmi.SdoVrtPeca,  '
      'wmi.SdoVrtPeso, wmi.SdoVrtArM2,  '
      'FLOOR((wmi.SdoVrtArM2 / 0.09290304)) + '
      'FLOOR(((MOD((wmi.SdoVrtArM2 / 0.09290304), 1)) + '
      '0.12499) * 4) * 0.25 AreaP2, 0.00 ValorT,  '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wmi.Pallet, vsp.Nome NO_Pallet, '
      'wmi.Terceiro, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      '"" NO_STATUS, '
      '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, '
      'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, '
      'wmi.Codigo, wmi.MovimCod IMEC, wmi.Controle IMEI, '
      'wmi.MovimID, wmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, '
      '1 Ativo '
      'FROM bluederm_2_1_cialeather.vsmovits wmi '
      
        'LEFT JOIN bluederm_2_1_cialeather.vspalleta  vsp ON vsp.Codigo=w' +
        'mi.Pallet   '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragrux    ggx ON ggx.Controle' +
        '=IF(wmi.Pallet <> 0, vsp.GraGruX, wmi.GraGruX) '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruy    ggy ON ggy.Codigo=g' +
        'gx.GraGruY '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruc    ggc ON ggc.Controle' +
        '=ggx.GraGruC '
      
        'LEFT JOIN bluederm_2_1_cialeather.gracorcad  gcc ON gcc.Codigo=g' +
        'gc.GraCorCad '
      
        'LEFT JOIN bluederm_2_1_cialeather.gratamits  gti ON gti.Controle' +
        '=ggx.GraTamI '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragru1    gg1 ON gg1.Nivel1=g' +
        'gx.GraGru1 '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  ent ON ent.Codigo=w' +
        'mi.Terceiro '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  emp ON emp.Codigo=w' +
        'mi.Empresa '
      'WHERE wmi.Controle <> 0 '
      'AND wmi.GraGruX<>0 '
      'AND wmi.SdoVrtPeca > 0'
      'AND wmi.Pallet = 0'
      'AND wmi.Empresa=-11')
    Left = 500
    Top = 376
    object QrEstoqueControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstoqueEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstoqueGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstoquePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstoquePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstoqueAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstoqueValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEstoqueGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstoqueNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEstoquePallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstoqueNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrEstoqueTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstoqueNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstoqueNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstoqueNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Required = True
      Size = 0
    end
    object QrEstoqueDataHora: TWideStringField
      FieldName = 'DataHora'
      Required = True
      Size = 19
    end
    object QrEstoqueOrdGGX: TLargeintField
      FieldName = 'OrdGGX'
      Required = True
    end
    object QrEstoqueOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEstoqueCodiGGY: TIntegerField
      FieldName = 'CodiGGY'
      Required = True
    end
    object QrEstoqueNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEstoqueCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstoqueIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrEstoqueIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrEstoqueMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstoqueMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstoqueNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Required = True
      Size = 0
    end
    object QrEstoqueNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Required = True
      Size = 0
    end
    object QrEstoqueAtivo: TLargeintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEstoqueGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstoqueVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrEstoqueClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrEstoqueSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrEstoqueFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrEstoqueMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEstoqueNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrEstoqueMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstoqueSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstoqueSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,##0.000'
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 500
    Top = 420
  end
  object QrVMICal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=26'
      'AND MovimNiv=29'
      'AND MovimCod=0'
      'AND QtdAntPeca > QtdGerPeca'
      '')
    Left = 356
    Top = 376
    object QrVMICalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMICalControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMICalMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMICalMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMICalMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMICalGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMICalPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVMICalTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMICalVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVMICalQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMICalQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMICalQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMICalQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMICalValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMICalMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMICalSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMICalFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMICalClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVMICalQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMICalQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMICalQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMICalQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMICalGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVMICalPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMICalAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMICalPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMICalStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object QrVMISum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 424
    object QrVMISumSdoPeca: TFloatField
      FieldName = 'SdoPeca'
    end
    object QrVMISumSdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrVMISumSdoArM2: TFloatField
      FieldName = 'SdoArM2'
    end
    object QrVMISumSdoArP2: TFloatField
      FieldName = 'SdoArP2'
    end
    object QrVMISumQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
  end
  object QrOriCal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 572
    Top = 376
    object QrOriCalGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrOriCalControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOriCalNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOriCalSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrOriCalCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrOriCalNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrOriCalFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrOriCalMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object DsOriCal: TDataSource
    DataSet = QrOriCal
    Left = 572
    Top = 424
  end
  object QrGGXJmp: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGGXJmpAfterOpen
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 428
    Top = 376
    object QrGGXJmpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXJmpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXJmpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXJmpSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXJmpCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXJmpNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXJmp: TDataSource
    DataSet = QrGGXJmp
    Left = 428
    Top = 424
  end
  object QrRmsGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 592
    Top = 56
    object QrRmsGGXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrRmsGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRmsGGXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrRmsGGXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrRmsGGXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrRmsGGXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsRmsGGX: TDataSource
    DataSet = QrRmsGGX
    Left = 592
    Top = 108
  end
  object QrMedia: TMySQLQuery
    Database = Dmod.MyDB
    Left = 368
    Top = 100
    object QrMediaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMediaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrMediaMedia: TFloatField
      FieldName = 'Media'
    end
  end
  object PMCalcula: TPopupMenu
    OnPopup = PMCalculaPopup
    Left = 292
    Top = 492
    object ASaberaspeasdeumaunicamarcapelopeso1: TMenuItem
      Caption = '&A. Saber as pe'#231'as de uma '#250'nica marca pelo peso'
      OnClick = ASaberaspeasdeumaunicamarcapelopeso1Click
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 764
    Top = 372
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 764
    Top = 416
  end
  object QrMarcas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 852
    Top = 372
    object QrMarcasMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrMarcasMartelo: TWideStringField
      FieldName = 'Martelo'
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 852
    Top = 416
  end
  object QrFichas: TMySQLQuery
    Database = Dmod.MyDB
    Left = 664
    Top = 376
  end
  object TmEstoqueAfterScrool: TTimer
    OnTimer = TmEstoqueAfterScroolTimer
    Left = 232
    Top = 324
  end
  object QrEmiOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emi.DataEmis, emi.Fulao, emi.Codigo, '
      'emi.Numero, emi.Nome, emi.Obs'
      'FROM emit emi '
      'WHERE emi.VSMovCod>0')
    Left = 572
    Top = 476
    object QrEmiOriDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmiOriFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmiOriCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmiOriNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrEmiOriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmiOriObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
  end
  object DsEmiOri: TDataSource
    DataSet = QrEmiOri
    Left = 572
    Top = 524
  end
end
