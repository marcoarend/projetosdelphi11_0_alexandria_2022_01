unit VSGruGGX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnGrade_Jan, UnAppEnums;

type
  TFmVSGruGGX = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBGGXIntegrl: TdmkDBLookupComboBox;
    EdGGXIntegrl: TdmkEditCB;
    Label1: TLabel;
    SbGGXIntegrl: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGGXIntegrl: TmySQLQuery;
    DsGGXIntegrl: TDataSource;
    QrGGXIntegrlControle: TIntegerField;
    QrGGXIntegrlNO_PRD_TAM_COR: TWideStringField;
    QrGGXLaminad: TmySQLQuery;
    QrGGXLaminadControle: TIntegerField;
    QrGGXLaminadNO_PRD_TAM_COR: TWideStringField;
    DsGGXLaminad: TDataSource;
    QrGGXDivTrip: TmySQLQuery;
    QrGGXDivTripControle: TIntegerField;
    QrGGXDivTripNO_PRD_TAM_COR: TWideStringField;
    DsGGXDivTrip: TDataSource;
    QrGGXDivCurt: TmySQLQuery;
    QrGGXDivCurtControle: TIntegerField;
    QrGGXDivCurtNO_PRD_TAM_COR: TWideStringField;
    DsGGXDivCurt: TDataSource;
    Label2: TLabel;
    EdGGXLaminad: TdmkEditCB;
    CBGGXLaminad: TdmkDBLookupComboBox;
    SbGGXLaminad: TSpeedButton;
    Label3: TLabel;
    EdGGXDivTrip: TdmkEditCB;
    CBGGXDivTrip: TdmkDBLookupComboBox;
    SbGGXDivTrip: TSpeedButton;
    Label4: TLabel;
    EdGGXDivCurt: TdmkEditCB;
    CBGGXDivCurt: TdmkDBLookupComboBox;
    SbGGXDivCurt: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGGXIntegrlClick(Sender: TObject);
    procedure SbGGXLaminadClick(Sender: TObject);
    procedure SbGGXDivTripClick(Sender: TObject);
    procedure SbGGXDivCurtClick(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure ReopenArtigo(Query: TmySQLQUery; Bastidao: TVSBastidao);
    procedure CadastraArtigo(EdSel: TdmkEditCB; CBSel: TdmkDBLookupComboBox;
              QrSel: TmySQLQuery);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSGruGGX: TFmVSGruGGX;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSGruGGX.BtOKClick(Sender: TObject);
const
  Tipo = 1;
var
  Nome: String;
  Codigo, GGXIntegrl, GGXLaminad, GGXDivTrip, GGXDivCurt: Integer;
  SQLType: TSQLType;
begin
  SQLType    := ImgTipo.SQLType;
  Codigo     := EdCodigo.ValueVariant;
  Nome       := EdNome.Text;
  GGXIntegrl := EdGGXIntegrl.ValueVariant;
  GGXLaminad := EdGGXLaminad.ValueVariant;
  GGXDivTrip := EdGGXDivTrip.ValueVariant;
  GGXDivCurt := EdGGXDivCurt.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsgruggx', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsgruggx', False, [
  'Nome', 'Tipo', 'GGXIntegrl',
  'GGXLaminad', 'GGXDivTrip', 'GGXDivCurt'], [
  'Codigo'], [
  Nome, Tipo, GGXIntegrl,
  GGXLaminad, GGXDivTrip, GGXDivCurt], [
  Codigo], True) then
  begin
    //ReopenCadastro_Com_Itens_ITS(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdCodigo.ValueVariant     := 0;

      EdGGXIntegrl.ValueVariant := 0;
      EdGGXLaminad.ValueVariant := 0;
      EdGGXDivTrip.ValueVariant := 0;
      EdGGXDivCurt.ValueVariant := 0;

      CBGGXIntegrl.KeyValue     := 0;
      CBGGXLaminad.KeyValue     := 0;
      CBGGXDivTrip.KeyValue     := 0;
      CBGGXDivCurt.KeyValue     := 0;

      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSGruGGX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGruGGX.CadastraArtigo(EdSel: TdmkEditCB; CBSel:
  TdmkDBLookupComboBox; QrSel: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraGruY(EdSel.ValueVariant, 0, '');
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO, 'Controle');
end;

procedure TFmVSGruGGX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGruGGX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenArtigo(QrGGXIntegrl, vsbstdIntegral);
  ReopenArtigo(QrGGXLaminad, vsbstdLaminado);
  ReopenArtigo(QrGGXDivTrip, vsbstdDivTripa);
  ReopenArtigo(QrGGXDivCurt, vsbstdDivCurti);
end;

procedure TFmVSGruGGX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGruGGX.ReopenArtigo(Query: TmySQLQUery; Bastidao: TVSBastidao);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR  ',
  'FROM gragruxcou cou',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE cou.Bastidao=' + Geral.FF0(Integer(Bastidao)),
  'ORDER BY NO_PRD_TAM_COR',
  '']);
end;

procedure TFmVSGruGGX.SbGGXDivCurtClick(Sender: TObject);
begin
  CadastraArtigo(EdGGXDivCurt, CBGGXDivCurt, QrGGXDivCurt);
end;

procedure TFmVSGruGGX.SbGGXDivTripClick(Sender: TObject);
begin
  CadastraArtigo(EdGGXDivTrip, CBGGXDivTrip, QrGGXDivTrip);
end;

procedure TFmVSGruGGX.SbGGXIntegrlClick(Sender: TObject);
begin
  CadastraArtigo(EdGGXIntegrl, CBGGXIntegrl, QrGGXIntegrl);
end;

procedure TFmVSGruGGX.SbGGXLaminadClick(Sender: TObject);
begin
  CadastraArtigo(EdGGXLaminad, CBGGXLaminad, QrGGXLaminad);
end;

(*
procedure TFmVSGruGGX.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;
*)

end.
