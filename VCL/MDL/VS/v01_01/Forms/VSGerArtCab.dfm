object FmVSGerArtCab: TFmVSGerArtCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-008 :: Gera'#231#227'o de Artigo de Ribeira'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 585
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 96
    ExplicitTop = 74
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 85
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 932
        Top = 4
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 72
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 584
        Top = 4
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 8
        Top = 44
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label22: TLabel
        Left = 432
        Top = 4
        Width = 25
        Height = 13
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 468
        Top = 4
        Width = 94
        Height = 13
        Caption = 'Data/hora gera'#231#227'o:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 700
        Top = 4
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 816
        Top = 4
        Width = 83
        Height = 13
        Caption = 'Fim classifica'#231#227'o:'
        FocusControl = DBEdit26
      end
      object DBEdit0: TdmkDBEdit
        Left = 932
        Top = 20
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsVSGerArt
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 72
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSGerArt
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 128
        Top = 20
        Width = 301
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSGerArt
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 584
        Top = 20
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibCla'
        DataSource = DsVSGerArt
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 60
        Width = 985
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSGerArt
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 432
        Top = 20
        Width = 32
        Height = 21
        DataField = 'NO_TIPO'
        DataSource = DsVSGerArt
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 468
        Top = 20
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsVSGerArt
        TabOrder = 6
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 8
        Top = 20
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSGerArt
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 700
        Top = 20
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgCla'
        DataSource = DsVSGerArt
        TabOrder = 8
      end
      object DBEdit26: TDBEdit
        Left = 816
        Top = 20
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimCla'
        DataSource = DsVSGerArt
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 521
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 90
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 264
        Top = 15
        Width = 742
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 628
          Top = 0
          Width = 114
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 104
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 421
          Left = 4
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Artigo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 420
          Left = 108
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&In Natura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtClassesGeradas: TBitBtn
          Left = 212
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Classes Geradas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtClassesGeradasClick
        end
        object BtTribIncIts: TBitBtn
          Tag = 502
          Left = 524
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtTribIncItsClick
        end
        object BtVSOutNFeCab: TBitBtn
          Tag = 456
          Left = 316
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe Cab'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtVSOutNFeCabClick
        end
        object BtVSOutNFeIts: TBitBtn
          Tag = 575
          Left = 420
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = 'N&Fe Itm'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtVSOutNFeItsClick
        end
      end
    end
    object Panel8: TPanel
      Left = 0
      Top = 85
      Width = 1008
      Height = 180
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 180
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1008
          Height = 125
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label13: TLabel
            Left = 8
            Top = 4
            Width = 28
            Height = 13
            Caption = 'IME-I:'
          end
          object Label14: TLabel
            Left = 88
            Top = 4
            Width = 76
            Height = 13
            Caption = 'Artigo de ribeira:'
          end
          object Label15: TLabel
            Left = 520
            Top = 4
            Width = 62
            Height = 13
            Caption = '$/kg M.obra:'
          end
          object Label16: TLabel
            Left = 8
            Top = 44
            Width = 74
            Height = 13
            Caption = 'Pe'#231'as geradas:'
            Enabled = False
          end
          object Label17: TLabel
            Left = 444
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Peso (origem):'
            Enabled = False
          end
          object Label20: TLabel
            Left = 84
            Top = 44
            Width = 75
            Height = 13
            Caption = #193'rea m'#178' gerado:'
            Enabled = False
          end
          object Label21: TLabel
            Left = 164
            Top = 44
            Width = 73
            Height = 13
            Caption = #193'rea ft'#178' gerado:'
            Enabled = False
          end
          object Label24: TLabel
            Left = 8
            Top = 84
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
            FocusControl = DBEdit16
          end
          object Label26: TLabel
            Left = 584
            Top = 44
            Width = 91
            Height = 13
            Caption = 'S'#233'rie / Ficha RMP:'
            FocusControl = DBEdit18
          end
          object Label36: TLabel
            Left = 676
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Nota MPAG:'
            FocusControl = DBEdit27
          end
          object Label37: TLabel
            Left = 340
            Top = 44
            Width = 63
            Height = 13
            Caption = 'Pe'#231'as Saldo:'
            Enabled = False
          end
          object Label38: TLabel
            Left = 416
            Top = 44
            Width = 67
            Height = 13
            Caption = #193'rea m'#178' saldo:'
            Enabled = False
          end
          object Label51: TLabel
            Left = 352
            Top = 84
            Width = 148
            Height = 13
            Caption = 'Local do (e) centro de estoque:'
            FocusControl = DBEdit39
          end
          object Label52: TLabel
            Left = 604
            Top = 4
            Width = 42
            Height = 13
            Caption = 'N'#176' RME:'
            FocusControl = DBEdit41
          end
          object Label47: TLabel
            Left = 252
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Peso kg gerado:'
            Enabled = False
          end
          object Label48: TLabel
            Left = 496
            Top = 44
            Width = 70
            Height = 13
            Caption = 'Peso kg saldo:'
            Enabled = False
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 20
            Width = 77
            Height = 21
            DataField = 'Controle'
            DataSource = DsVSGerArtNew
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 88
            Top = 20
            Width = 40
            Height = 21
            DataField = 'GraGruX'
            DataSource = DsVSGerArtNew
            TabOrder = 1
          end
          object DBEdit7: TDBEdit
            Left = 128
            Top = 20
            Width = 313
            Height = 21
            DataField = 'NO_PRD_TAM_COR'
            DataSource = DsVSGerArtNew
            TabOrder = 2
          end
          object DBEdit8: TDBEdit
            Left = 520
            Top = 20
            Width = 80
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSGerArtNew
            TabOrder = 3
          end
          object DBEdit9: TDBEdit
            Left = 8
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Pecas'
            DataSource = DsVSGerArtNew
            TabOrder = 4
          end
          object DBEdit10: TDBEdit
            Left = 444
            Top = 20
            Width = 72
            Height = 21
            DataField = 'QtdAntPeso'
            DataSource = DsVSGerArtNew
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 84
            Top = 60
            Width = 76
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsVSGerArtNew
            TabOrder = 6
          end
          object DBEdit13: TDBEdit
            Left = 164
            Top = 60
            Width = 84
            Height = 21
            DataField = 'AreaP2'
            DataSource = DsVSGerArtNew
            TabOrder = 7
          end
          object DBEdit16: TDBEdit
            Left = 8
            Top = 100
            Width = 56
            Height = 21
            DataField = 'Terceiro'
            DataSource = DsVSGerArtNew
            TabOrder = 8
          end
          object DBEdit17: TDBEdit
            Left = 64
            Top = 100
            Width = 285
            Height = 21
            DataField = 'NO_FORNECE'
            DataSource = DsVSGerArtNew
            TabOrder = 9
          end
          object DBEdit18: TDBEdit
            Left = 584
            Top = 60
            Width = 161
            Height = 21
            DataField = 'NO_FICHA'
            DataSource = DsVSGerArtNew
            TabOrder = 10
          end
          object DBEdit27: TDBEdit
            Left = 676
            Top = 20
            Width = 68
            Height = 21
            DataField = 'NotaMPAG'
            DataSource = DsVSGerArtNew
            TabOrder = 11
          end
          object DBEdit28: TDBEdit
            Left = 340
            Top = 60
            Width = 72
            Height = 21
            DataField = 'SdoVrtPeca'
            DataSource = DsVSGerArtNew
            TabOrder = 12
          end
          object DBEdit29: TDBEdit
            Left = 416
            Top = 60
            Width = 76
            Height = 21
            DataField = 'SdoVrtArM2'
            DataSource = DsVSGerArtNew
            TabOrder = 13
          end
          object DBEdit40: TDBEdit
            Left = 352
            Top = 100
            Width = 56
            Height = 21
            DataField = 'StqCenLoc'
            DataSource = DsVSGerArtNew
            TabOrder = 14
          end
          object DBEdit39: TDBEdit
            Left = 408
            Top = 100
            Width = 337
            Height = 21
            DataField = 'NO_LOC_CEN'
            DataSource = DsVSGerArtNew
            TabOrder = 15
          end
          object DBEdit41: TDBEdit
            Left = 604
            Top = 20
            Width = 68
            Height = 21
            DataField = 'ReqMovEstq'
            DataSource = DsVSGerArtNew
            TabOrder = 16
          end
          object GroupBox3: TGroupBox
            Left = 784
            Top = 15
            Width = 222
            Height = 108
            Align = alRight
            Caption = ' Impostos do Item: '
            TabOrder = 17
            object DBGrid3: TDBGrid
              Left = 2
              Top = 15
              Width = 218
              Height = 91
              Align = alClient
              DataSource = DsTribIncIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_Tributo'
                  Title.Caption = 'Tributo'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Percent'
                  Title.Caption = '%'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrTrib'
                  Title.Caption = 'Valor '
                  Width = 72
                  Visible = True
                end>
            end
          end
          object DBEdit24: TDBEdit
            Left = 252
            Top = 60
            Width = 84
            Height = 21
            DataField = 'PesoKg'
            DataSource = DsVSGerArtNew
            TabOrder = 18
          end
          object DBEdit33: TDBEdit
            Left = 496
            Top = 60
            Width = 84
            Height = 21
            DataField = 'SdoVrtPeso'
            DataSource = DsVSGerArtNew
            TabOrder = 19
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 125
          Width = 1008
          Height = 55
          Align = alClient
          Caption = ' Valores: '
          TabOrder = 1
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 1004
            Height = 38
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label25: TLabel
              Left = 8
              Top = 0
              Width = 46
              Height = 13
              Caption = '$/kg MO:'
            end
            object Label27: TLabel
              Left = 76
              Top = 0
              Width = 47
              Height = 13
              Caption = 'Total MO:'
              FocusControl = DBEdit20
            end
            object Label28: TLabel
              Left = 454
              Top = 0
              Width = 50
              Height = 13
              Caption = 'Valor total:'
              FocusControl = DBEdit21
            end
            object Label29: TLabel
              Left = 172
              Top = 0
              Width = 46
              Height = 13
              Caption = 'Valor MP:'
              FocusControl = DBEdit22
            end
            object Label30: TLabel
              Left = 550
              Top = 0
              Width = 39
              Height = 13
              Caption = 'R$ / m'#178':'
              FocusControl = DBEdit23
            end
            object Label18: TLabel
              Left = 618
              Top = 0
              Width = 164
              Height = 13
              Caption = 'Observa'#231#227'o sobre o artigo gerado:'
            end
            object Label54: TLabel
              Left = 364
              Top = 0
              Width = 71
              Height = 13
              Caption = 'Frete ret. M.O.:'
              FocusControl = DBEdit34
            end
            object Label58: TLabel
              Left = 268
              Top = 0
              Width = 45
              Height = 13
              Caption = 'Total PQ:'
              FocusControl = DBEdit36
            end
            object DBEdit19: TDBEdit
              Left = 8
              Top = 16
              Width = 64
              Height = 21
              DataField = 'CustoMOKg'
              DataSource = DsVSGerArtNew
              TabOrder = 0
            end
            object DBEdit20: TDBEdit
              Left = 76
              Top = 16
              Width = 92
              Height = 21
              DataField = 'CustoMOTot'
              DataSource = DsVSGerArtNew
              TabOrder = 1
            end
            object DBEdit21: TDBEdit
              Left = 454
              Top = 16
              Width = 92
              Height = 21
              DataField = 'ValorT'
              DataSource = DsVSGerArtNew
              TabOrder = 5
            end
            object DBEdit22: TDBEdit
              Left = 172
              Top = 16
              Width = 92
              Height = 21
              DataField = 'ValorMP'
              DataSource = DsVSGerArtNew
              TabOrder = 2
            end
            object DBEdit23: TDBEdit
              Left = 550
              Top = 16
              Width = 64
              Height = 21
              DataField = 'CUSTO_M2'
              DataSource = DsVSGerArtNew
              TabOrder = 6
            end
            object DBEdit11: TDBEdit
              Left = 618
              Top = 16
              Width = 382
              Height = 21
              DataField = 'Observ'
              DataSource = DsVSGerArtNew
              TabOrder = 7
            end
            object DBEdit34: TDBEdit
              Left = 364
              Top = 16
              Width = 86
              Height = 21
              DataField = 'CusFrtMORet'
              DataSource = DsVSGerArtNew
              TabOrder = 4
            end
            object DBEdit36: TDBEdit
              Left = 268
              Top = 16
              Width = 92
              Height = 21
              DataField = 'CustoPQ'
              DataSource = DsVSGerArtNew
              TabOrder = 3
            end
          end
        end
      end
    end
    object PCItens: TPageControl
      Left = 0
      Top = 265
      Width = 1008
      Height = 204
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = ' Itens '
        object DGDados: TDBGrid
          Left = 0
          Top = 0
          Width = 642
          Height = 176
          Align = alClient
          DataSource = DsVSGerArt015
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 92
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstGGX'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Pele In Natura'
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoKgInn'
              Title.Caption = '$ kg MP'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerPeca'
              Title.Caption = 'p'#231' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerArM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerPeso'
              Title.Caption = 'kg gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoPQ'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor Total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoAreaMP'
              Title.Caption = '$ m'#178' MP'
              Width = 56
              Visible = True
            end>
        end
        object PCSubItens: TPageControl
          Left = 642
          Top = 0
          Width = 358
          Height = 176
          ActivePage = TabSheet4
          Align = alRight
          TabOrder = 1
          object TabSheet4: TTabSheet
            Caption = 'IME-I g'#234'meo'
            ImageIndex = 1
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 350
              Height = 148
              Align = alClient
              TabOrder = 0
              object Label40: TLabel
                Left = 8
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Controle'
                FocusControl = DBEdit30
              end
              object Label41: TLabel
                Left = 8
                Top = 48
                Width = 30
                Height = 13
                Caption = 'Pecas'
                FocusControl = DBEdit31
              end
              object Label42: TLabel
                Left = 8
                Top = 92
                Width = 36
                Height = 13
                Caption = #193'rea m'#178
                FocusControl = DBEdit32
              end
              object Label57: TLabel
                Left = 148
                Top = 92
                Width = 76
                Height = 13
                Caption = 'Custo m'#178'curtido:'
                FocusControl = DBEdit35
              end
              object DBEdit30: TDBEdit
                Left = 8
                Top = 20
                Width = 134
                Height = 21
                DataField = 'Controle'
                DataSource = DsVSGerArt014
                TabOrder = 0
              end
              object DBEdit31: TDBEdit
                Left = 8
                Top = 64
                Width = 134
                Height = 21
                DataField = 'Pecas'
                DataSource = DsVSGerArt014
                TabOrder = 1
              end
              object DBEdit32: TDBEdit
                Left = 8
                Top = 108
                Width = 134
                Height = 21
                DataField = 'QtdGerArM2'
                DataSource = DsVSGerArt014
                TabOrder = 2
              end
              object DBEdit35: TDBEdit
                Left = 148
                Top = 108
                Width = 134
                Height = 21
                DataField = 'CustoAreaCur'
                DataSource = DsVSGerArt014
                TabOrder = 3
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'NFe'
        ImageIndex = 1
        object DBGrid5: TDBGrid
          Left = 0
          Top = 0
          Width = 205
          Height = 176
          Align = alLeft
          DataSource = DsVSOutNFeCab
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'rie'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'N'#186' NFe'
              Visible = True
            end>
        end
        object DBGrid2: TDBGrid
          Left = 205
          Top = 0
          Width = 795
          Height = 176
          Align = alClient
          DataSource = DsVSOutNFeIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ItemNFe'
              Title.Caption = 'NFi'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo / tamanho / cor'
              Width = 254
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorU'
              Title.Caption = 'Valor Unit.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor Total'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
      end
      object TsRetornoMO: TTabSheet
        Caption = ' Retorno da M.O.'
        ImageIndex = 2
        object PnRetornoMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 176
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvRet: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 661
            Height = 176
            Align = alLeft
            DataSource = DsVSMOEnvRet
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_nCT'
                Title.Caption = 'N'#186' CT'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_AreaM2'
                Title.Caption = 'Area m'#178
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_SerNF'
                Title.Caption = 'S'#233'r. Ret'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_nNF'
                Title.Caption = 'NF Retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_SerNF'
                Title.Caption = 'S'#233'r. M.O.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_nNF'
                Title.Caption = 'NF M.O.'
                Visible = True
              end>
          end
          object PnItensRetMO: TPanel
            Left = 661
            Top = 0
            Width = 339
            Height = 176
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 0
              Top = 61
              Width = 339
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitLeft = 585
              ExplicitTop = 1
              ExplicitWidth = 208
            end
            object GroupBox7: TGroupBox
              Left = 0
              Top = 0
              Width = 339
              Height = 61
              Align = alClient
              Caption = ' Itens de NFes enviados para M.O.: '
              TabOrder = 0
              object DBGVSMOEnvRVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 44
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvRVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NFEMP_SerNF'
                    Title.Caption = 'S'#233'r. NF'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFEMP_nNF'
                    Title.Caption = 'NF'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor Total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Title.Caption = 'Valor Frete'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMOEnvEnv'
                    Title.Caption = 'ID Envio'
                    Visible = True
                  end>
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 66
              Width = 339
              Height = 110
              Align = alBottom
              Caption = ' IME-Is de couros prontos retornados: '
              TabOrder = 1
              object DBGVSMOEnvGVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 93
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvGVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMovIts'
                    Title.Caption = 'IME-I'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 585
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
      end
      object Label39: TLabel
        Left = 704
        Top = 56
        Width = 64
        Height = 13
        Caption = 'GGX anterior:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 44659.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 685
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 776
        Top = 16
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 7
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdGGXAnterior: TdmkEdit
        Left = 704
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 522
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 180
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label3: TLabel
        Left = 100
        Top = 16
        Width = 76
        Height = 13
        Caption = 'Artigo de ribeira:'
      end
      object Label10: TLabel
        Left = 508
        Top = 16
        Width = 89
        Height = 13
        Caption = '$/kg M'#227'o-de-obra:'
      end
      object Label12: TLabel
        Left = 16
        Top = 96
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object Label19: TLabel
        Left = 776
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label49: TLabel
        Left = 372
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label50: TLabel
        Left = 740
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaPeso: TLabel
        Left = 700
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label46: TLabel
        Left = 16
        Top = 136
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object SbStqCenLoc: TSpeedButton
        Left = 717
        Top = 71
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbStqCenLocClick
      end
      object Label55: TLabel
        Left = 604
        Top = 16
        Width = 92
        Height = 13
        Caption = '% Ret.Tributos MO:'
      end
      object EdControle: TdmkEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 100
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 156
        Top = 32
        Width = 345
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 2
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOKg: TdmkEdit
        Left = 508
        Top = 32
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdCustoMOKgRedefinido
      end
      object EdObserv: TdmkEdit
        Left = 16
        Top = 112
        Width = 817
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 776
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 8
        dmkEditCB = EdFornecMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 372
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 428
        Top = 72
        Width = 288
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 10
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReqMovEstq: TdmkEdit
        Left = 740
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 700
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdClientMO: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 493
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 14
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCredPereImposto: TdmkEdit
        Left = 604
        Top = 32
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 10
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdCredPereImpostoRedefinido
      end
      object CkMedEClas: TCheckBox
        Left = 572
        Top = 156
        Width = 273
        Height = 17
        Caption = 'A classifica'#231#227'o ser'#225' realizada junto com a medi'#231#227'o.'
        TabOrder = 15
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 277
      Width = 1008
      Height = 56
      Align = alTop
      TabOrder = 3
      object Label31: TLabel
        Left = 168
        Top = 8
        Width = 73
        Height = 13
        Caption = 'Total Valor MP:'
      end
      object Label44: TLabel
        Left = 360
        Top = 8
        Width = 90
        Height = 13
        Caption = 'Total M'#227'o-de-obra:'
      end
      object LaPecas: TLabel
        Left = 16
        Top = 8
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label43: TLabel
        Left = 456
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Valor Total:'
      end
      object Label45: TLabel
        Left = 92
        Top = 8
        Width = 61
        Height = 13
        Caption = 'Peso origem:'
      end
      object Label56: TLabel
        Left = 265
        Top = 8
        Width = 90
        Height = 13
        Caption = '$ Ret.Tributos MO:'
      end
      object EdValorMP: TdmkEdit
        Left = 168
        Top = 24
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdValorMPRedefinido
      end
      object EdCustoMOTot: TdmkEdit
        Left = 360
        Top = 24
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdCustoMOTotRedefinido
      end
      object EdPecas: TdmkEdit
        Left = 16
        Top = 24
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdValorT: TdmkEdit
        Left = 456
        Top = 24
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtdAntPeso: TdmkEdit
        Left = 92
        Top = 24
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdQtdAntPesoRedefinido
      end
      object EdCredValrImposto: TdmkEdit
        Left = 265
        Top = 24
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 352
        Height = 32
        Caption = 'Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 352
        Height = 32
        Caption = 'Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 352
        Height = 32
        Caption = 'Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 490
        Height = 17
        Caption = 
          '$/kg M'#227'o-de-obra: Valor pago quando a empresa curtiu o couro em ' +
          'terceiro.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 490
        Height = 17
        Caption = 
          '$/kg M'#227'o-de-obra: Valor pago quando a empresa curtiu o couro em ' +
          'terceiro.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSGerArt: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSGerArtBeforeOpen
    AfterOpen = QrVSGerArtAfterOpen
    BeforeClose = QrVSGerArtBeforeClose
    AfterScroll = QrVSGerArtAfterScroll
    OnCalcFields = QrVSGerArtCalcFields
    SQL.Strings = (
      'SELECT vga.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsgerarta vga'
      'LEFT JOIN entidades ent ON ent.Codigo=vga.Empresa'
      'WHERE vga.Codigo > 0')
    Left = 120
    Top = 461
    object QrVSGerArtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSGerArtDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrVSGerArtDtHrLibCla: TDateTimeField
      FieldName = 'DtHrLibCla'
    end
    object QrVSGerArtDtHrCfgCla: TDateTimeField
      FieldName = 'DtHrCfgCla'
    end
    object QrVSGerArtDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrVSGerArtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSGerArtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtPecasMan: TFloatField
      FieldName = 'PecasMan'
    end
    object QrVSGerArtAreaManM2: TFloatField
      FieldName = 'AreaManM2'
    end
    object QrVSGerArtAreaManP2: TFloatField
      FieldName = 'AreaManP2'
    end
    object QrVSGerArtTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSGerArtNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSGerArtNO_DtHrFimCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimCla'
      Calculated = True
    end
    object QrVSGerArtNO_DtHrLibCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibCla'
      Calculated = True
    end
    object QrVSGerArtNO_DtHrCfgCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgCla'
      Calculated = True
    end
    object QrVSGerArtCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSGerArtGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
    end
    object QrVSGerArtCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
    end
    object QrVSGerArtValorManMP: TFloatField
      FieldName = 'ValorManMP'
    end
    object QrVSGerArtValorManT: TFloatField
      FieldName = 'ValorManT'
    end
    object QrVSGerArtTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
  end
  object DsVSGerArt: TDataSource
    DataSet = QrVSGerArt
    Left = 120
    Top = 509
  end
  object QrVSGerArtNew: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSGerArtNewBeforeClose
    AfterScroll = QrVSGerArtNewAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_TERCEIRO, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 200
    Top = 461
    object QrVSGerArtNewCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSGerArtNewControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSGerArtNewMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSGerArtNewMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSGerArtNewMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSGerArtNewEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSGerArtNewTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSGerArtNewCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSGerArtNewMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSGerArtNewDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSGerArtNewPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSGerArtNewGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSGerArtNewSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSGerArtNewSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSGerArtNewSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSGerArtNewFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSGerArtNewMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSGerArtNewFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerArtNewDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerArtNewDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSGerArtNewDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '0.0000'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSGerArtNewID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSGerArtNewMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerArtNewPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSGerArtNewStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSGerArtNewClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSGerArtNewDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSGerArtNewCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSGerArtNewGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrVSGerArtNewCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
    end
    object QrVSGerArtNewCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMedEClas: TSmallintField
      FieldName = 'MedEClas'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 200
    Top = 509
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 528
    Top = 576
    object ItsInclui1: TMenuItem
      Caption = 'Adiciona da &Barraca'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object AdicionadoCurimento1: TMenuItem
      Caption = 'Adiciona do Curimento - &IME-I - '#218'nico'
      OnClick = AdicionadoCurimento1Click
    end
    object AdicionadoCurimentoMltiplo1: TMenuItem
      Caption = 'Adiciona do Curimento - &Ficha - '#218'nica'
      OnClick = AdicionadoCurimentoMltiplo1Click
    end
    object AdicionadoCurimentoMltiplodeMltiplo1: TMenuItem
      Caption = 'Adiciona do Curimento - Ficha - &V'#225'rias'
      OnClick = AdicionadoCurimentoMltiplodeMltiplo1Click
    end
    object AdicionadoCurimentoIMECnico1: TMenuItem
      Caption = 'Adiciona do Curimento - IME-&C - '#218'nico'
      OnClick = AdicionadoCurimentoIMECnico1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      OnClick = ItsAltera1Click
      object Quantidades1: TMenuItem
        Caption = '&Quantidades / data'
        OnClick = Quantidades1Click
      end
      object InNaturadeOrigem1: TMenuItem
        Caption = 'In Natura de &Origem'
        OnClick = InNaturadeOrigem1Click
      end
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object Incluiatrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = Incluiatrelamento1Click
      end
      object Alteraatrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = Alteraatrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      Visible = False
      OnClick = ItsExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Removeforado1: TMenuItem
      Caption = 'Remove for'#231'ado'
      OnClick = Removeforado1Click
    end
    object Editaantigo1: TMenuItem
      Caption = 'Edita antigo ???'
      OnClick = Editaantigo1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Atualizacustodescendentesdoitemselecionado1: TMenuItem
      Caption = 'Atualiza custo descendentes do item selecionado'
      OnClick = Atualizacustodescendentesdoitemselecionado1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 420
    Top = 552
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabLibera1: TMenuItem
      Caption = '&Libera classifica'#231#227'o'
      OnClick = CabLibera1Click
    end
    object CabReeditar1: TMenuItem
      Caption = '&Volta a editar'
      Enabled = False
      OnClick = CabReeditar1Click
    end
    object AlteraDatas1: TMenuItem
      Caption = 'Altera &Datas'
      OnClick = AlteraDatas1Click
    end
    object Alterapesokg1: TMenuItem
      Caption = '&Altera peso kg'
      OnClick = Alterapesokg1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Corrigeartigogerado1: TMenuItem
      Caption = 'Corrige artigo gerado'
      OnClick = Corrigeartigogerado1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 100
  end
  object QrVSGerArt015: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSGerArt015BeforeClose
    AfterScroll = QrVSGerArt015AfterScroll
    OnCalcFields = QrVSGerArt015CalcFields
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 288
    Top = 461
    object QrVSGerArt015Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSGerArt015Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSGerArt015MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSGerArt015MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSGerArt015MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSGerArt015Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSGerArt015Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSGerArt015CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSGerArt015MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSGerArt015DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSGerArt015Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSGerArt015GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerArt015Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt015PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt015AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSGerArt015SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSGerArt015SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSGerArt015SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSGerArt015SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGerArt015SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt015SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArt015SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSGerArt015Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSGerArt015Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSGerArt015FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSGerArt015CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerArt015DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerArt015DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSGerArt015DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSGerArt015QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGerArt015QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt015QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGerArt015QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt015QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '0.00'
    end
    object QrVSGerArt015NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArt015NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArt015NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSGerArt015ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSGerArt015NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArt015NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArt015CusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSGerArt015CustoKgInn: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CustoKgInn'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrVSGerArt015Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerArt015CustoAreaMP: TFloatField
      FieldName = 'CustoAreaMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt015CustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSGerArt015: TDataSource
    DataSet = QrVSGerArt015
    Left = 288
    Top = 509
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 420
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 420
    Top = 100
  end
  object PMNumero: TPopupMenu
    Left = 156
    Top = 20
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &Gera'#231#227'o (c'#243'digo)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I Artigo de Ribeira gerado'
      OnClick = IMEIArtigodeRibeiragerado1Click
    end
    object Classesgeradas1: TMenuItem
      Caption = '&Classes geradas'
      object Destagerao1: TMenuItem
        Caption = 'Desta gera'#231#227'o'
        OnClick = Destagerao1Click
      end
      object Devriasgeraes1: TMenuItem
        Caption = 'De v'#225'rias gera'#231#245'es'
        OnClick = Devriasgeraes1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
    object FluxodoIMEIGerado1: TMenuItem
      Caption = 'Fluxo do IME-I Gerado (cabe'#231'alho)'
      OnClick = FluxodoIMEIGerado1Click
    end
    object FluxodaFichaRMPOrigemitemselecionado1: TMenuItem
      Caption = 'Fluxo da Ficha RMP Origem (item selecionado)'
      OnClick = FluxodaFichaRMPOrigemitemselecionado1Click
    end
    object TMenuItem
      Caption = 'Classes geradas de v'#225'rias gera'#231#245'es de artigo'
    end
  end
  object PMQuery: TPopupMenu
    Left = 196
    Top = 20
    object Pelaobservaosobreaaindustrializao1: TMenuItem
      Caption = 'Pela observa'#231#227'o sobrea a &Industrializa'#231#227'o'
      OnClick = Pelaobservaosobreaaindustrializao1Click
    end
    object Pesquisasgeral1: TMenuItem
      Caption = 'Pesquisas &Geral'
      OnClick = Pesquisasgeral1Click
    end
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 724
    Top = 420
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTwnMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTwnSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrTeste: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vsmovits wmi '
      'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE wmi.MovimID=1'
      'AND SdoVrtPeca>0 '
      'AND Empresa=-11'
      'ORDER BY wmi.Controle ')
    Left = 612
    Top = 468
    object QrTesteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTesteControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTesteMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTesteMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTesteMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTesteEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTesteTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTesteCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTesteMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTesteLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrTesteLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrTesteDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTestePallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrTesteGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTestePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrTestePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrTesteAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrTesteAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrTesteValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTesteSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTesteSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTesteSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTesteSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTesteSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrTesteObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTesteLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTesteDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTesteDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTesteUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTesteUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTesteAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTesteAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTesteFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrTesteMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrTesteCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrTesteCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTesteNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTesteSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrTesteSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrTesteNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTesteMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTesteVSRibCad: TIntegerField
      FieldName = 'VSRibCad'
    end
    object QrTesteMeios: TFloatField
      FieldName = 'Meios'
    end
    object QrTesteFatorVNC: TFloatField
      FieldName = 'FatorVNC'
    end
    object QrTesteFatorVRC: TFloatField
      FieldName = 'FatorVRC'
    end
    object QrTesteFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object QrNiv14: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimCod=2121'
      'AND MovimNiv=14')
    Left = 544
    Top = 468
    object QrNiv14CustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNiv14MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrNiv14Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNiv14Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNiv14DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNiv14DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNiv14UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNiv14UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNiv14AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNiv14Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNiv14Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNiv14Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNiv14MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrNiv14MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrNiv14Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNiv14Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrNiv14CliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrNiv14MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrNiv14LnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrNiv14LnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrNiv14DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrNiv14Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrNiv14GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNiv14PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrNiv14AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrNiv14AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrNiv14ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrNiv14SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrNiv14SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrNiv14SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrNiv14SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrNiv14SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrNiv14SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrNiv14SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrNiv14Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrNiv14SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrNiv14Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrNiv14Misturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrNiv14FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrNiv14CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrNiv14CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrNiv14ValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrNiv14DstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrNiv14DstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrNiv14DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrNiv14DstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrNiv14QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrNiv14QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrNiv14QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrNiv14QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrNiv14QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrNiv14QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrNiv14QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrNiv14QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrNiv14AptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrNiv14NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrNiv14Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrNiv14TpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrNiv14Zerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrNiv14EmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrNiv14NotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrNiv14FatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrNiv14FatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrNiv14LnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrNiv14PedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrNiv14PedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrNiv14PedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
  end
  object QrNiv15: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimCod=2121'
      'AND MovimTwn=5064'
      'AND MovimNiv=15'
      ''
      '')
    Left = 544
    Top = 512
    object QrNiv15Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNiv15Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNiv15DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNiv15DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNiv15UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNiv15UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNiv15AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNiv15Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNiv15Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNiv15MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrNiv15MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrNiv15MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrNiv15Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNiv15Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrNiv15CliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrNiv15MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrNiv15LnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrNiv15LnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrNiv15DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrNiv15Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrNiv15GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNiv15Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNiv15PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrNiv15AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrNiv15AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrNiv15ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrNiv15SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrNiv15SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrNiv15SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrNiv15SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrNiv15SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrNiv15SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrNiv15SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrNiv15Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrNiv15SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrNiv15Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrNiv15Misturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrNiv15FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrNiv15CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrNiv15CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrNiv15ValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrNiv15DstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrNiv15DstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrNiv15DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrNiv15DstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrNiv15QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrNiv15QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrNiv15QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrNiv15QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrNiv15QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrNiv15QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrNiv15QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrNiv15QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrNiv15AptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrNiv15NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrNiv15Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrNiv15TpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrNiv15Zerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrNiv15EmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrNiv15NotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrNiv15FatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrNiv15FatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrNiv15LnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrNiv15PedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrNiv15PedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrNiv15PedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
  end
  object QrVSGerArt014: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 376
    Top = 457
    object QrVSGerArt014Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSGerArt014Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSGerArt014MovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSGerArt014MovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSGerArt014MovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSGerArt014Empresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSGerArt014Terceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSGerArt014CliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSGerArt014MovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSGerArt014DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSGerArt014Pallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSGerArt014GraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerArt014Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt014PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt014AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014SrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSGerArt014SrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSGerArt014SrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSGerArt014SrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSGerArt014SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGerArt014SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt014SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArt014SerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSGerArt014Ficha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSGerArt014Misturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSGerArt014FornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSGerArt014CustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014CustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014CustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014ValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014DstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerArt014DstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerArt014DstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSGerArt014DstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSGerArt014QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGerArt014QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt014QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGerArt014QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArt014QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArt014NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSGerArt014NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArt014NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArt014NO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSGerArt014ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSGerArt014NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArt014NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArt014ReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSGerArt014CustoAreaCur: TFloatField
      FieldName = 'CustoAreaCur'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSGerArt014: TDataSource
    DataSet = QrVSGerArt014
    Left = 376
    Top = 505
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object PMTribIncIts: TPopupMenu
    Left = 824
    Top = 572
    object IncluiTributo1: TMenuItem
      Caption = '&Inclui Tributo'
      OnClick = IncluiTributo1Click
    end
    object AlteraTributoAtual1: TMenuItem
      Caption = '&Altera Tributo Atual'
      OnClick = AlteraTributoAtual1Click
    end
    object ExcluiTributoAtual1: TMenuItem
      Caption = '&Exclui Tributo Atual'
      OnClick = ExcluiTributoAtual1Click
    end
  end
  object QrTribIncIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 456
    Top = 460
    object QrTribIncItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTribIncItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrTribIncItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTribIncItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTribIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTribIncItsData: TDateField
      FieldName = 'Data'
    end
    object QrTribIncItsHora: TTimeField
      FieldName = 'Hora'
    end
    object QrTribIncItsValorFat: TFloatField
      FieldName = 'ValorFat'
    end
    object QrTribIncItsBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTribIncItsPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '0.00'
    end
    object QrTribIncItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTribIncItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTribIncItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTribIncItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTribIncItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTribIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTribIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTribIncItsTributo: TIntegerField
      FieldName = 'Tributo'
    end
    object QrTribIncItsVUsoCalc: TFloatField
      FieldName = 'VUsoCalc'
    end
    object QrTribIncItsOperacao: TSmallintField
      FieldName = 'Operacao'
    end
    object QrTribIncItsNO_Tributo: TWideStringField
      FieldName = 'NO_Tributo'
      Size = 60
    end
    object QrTribIncItsFatorDC: TSmallintField
      FieldName = 'FatorDC'
    end
  end
  object DsTribIncIts: TDataSource
    DataSet = QrTribIncIts
    Left = 456
    Top = 508
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object PMVSOutNFeCab: TPopupMenu
    Left = 612
    Top = 572
    object IncluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Inclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = IncluiSerieNumeroDeNFe1Click
    end
    object AlteraSerieNumeroDeNFe1: TMenuItem
      Caption = 'Aterai s'#233'rie/ n'#250'mero de NFe'
      OnClick = AlteraSerieNumeroDeNFe1Click
    end
    object ExcluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Exclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = ExcluiSerieNumeroDeNFe1Click
    end
  end
  object PMVSOutNFeIts: TPopupMenu
    Left = 732
    Top = 572
    object IncluiitemdeNF1: TMenuItem
      Caption = '&Inclui item de NF'
      OnClick = IncluiitemdeNF1Click
    end
    object AlteraitemdeNF1: TMenuItem
      Caption = '&Altera item de NF'
      OnClick = AlteraitemdeNF1Click
    end
    object ExcluiitemdeNF1: TMenuItem
      Caption = '&Exclui item de NF'
      OnClick = ExcluiitemdeNF1Click
    end
  end
  object QrVSOutNFeCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSOutNFeCabBeforeClose
    AfterScroll = QrVSOutNFeCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM vsoutnfecab'
      'WHERE MovimCod=:P0')
    Left = 868
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSOutNFeCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSOutNFeCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSOutNFeCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSOutNFeCabOriCod: TIntegerField
      FieldName = 'OriCod'
    end
    object QrVSOutNFeCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeCab: TDataSource
    DataSet = QrVSOutNFeCab
    Left = 868
    Top = 500
  end
  object QrVSOutNFeIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, voi.* '
      'FROM vsoutnfi voi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE voi.Codigo=0')
    Left = 960
    Top = 456
    object QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSOutNFeItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSOutNFeItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSOutNFeItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSOutNFeItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSOutNFeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSOutNFeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsTpCalcVal: TSmallintField
      FieldName = 'TpCalcVal'
    end
    object QrVSOutNFeItsValorU: TFloatField
      FieldName = 'ValorU'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSOutNFeItsTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
    object QrVSOutNFeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeIts: TDataSource
    DataSet = QrVSOutNFeIts
    Left = 960
    Top = 504
  end
  object QrVSMOEnvRet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvRetBeforeClose
    AfterScroll = QrVSMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMOEnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrVSMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMOEnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrVSMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrVSMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrVSMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrVSMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrVSMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrVSMOEnvRetCFTPA_Pecas: TFloatField
      FieldName = 'CFTPA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetCFTPA_PesoKg: TFloatField
      FieldName = 'CFTPA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetCFTPA_AreaM2: TFloatField
      FieldName = 'CFTPA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_AreaP2: TFloatField
      FieldName = 'CFTPA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRet: TDataSource
    DataSet = QrVSMOEnvRet
    Left = 608
    Top = 48
  end
  object QrVSMOEnvRVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrvmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.VSMOEnvEnv')
    Left = 692
    object QrVSMOEnvRVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvRVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvRVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvRVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvRVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvRVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvRVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvRVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvRVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvRVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvRVmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvRVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRVmi: TDataSource
    DataSet = QrVSMOEnvRVmi
    Left = 692
    Top = 48
  end
  object QrVSMOEnvGVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgvmi')
    Left = 780
    object QrVSMOEnvGVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvGVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvGVmiVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvGVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvGVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvGVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvGVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvGVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvGVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvGVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvGVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvGVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvGVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvGVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvGVmiVSMovimCod: TIntegerField
      FieldName = 'VSMovimCod'
    end
  end
  object DsVSMOEnvGVmi: TDataSource
    DataSet = QrVSMOEnvGVmi
    Left = 780
    Top = 48
  end
  object PMNovo: TPopupMenu
    Left = 96
    Top = 36
    object CorrigesaldosdeorigemenotaMPAG1: TMenuItem
      Caption = 'Corrige saldos de origem e nota MPAG'
      OnClick = CorrigesaldosdeorigemenotaMPAG1Click
    end
    object Corrigevalorderecuperaodeimpostos1: TMenuItem
      Caption = 'Corrige valor de recupera'#231#227'o de impostos'
      OnClick = Corrigevalorderecuperaodeimpostos1Click
    end
  end
end
