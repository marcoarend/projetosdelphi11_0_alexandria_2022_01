object FmVSImpHistorico2: TFmVSImpHistorico2
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-120 :: Impress'#227'o de Historico'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 279
        Height = 32
        Caption = 'Impress'#227'o de Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 279
        Height = 32
        Caption = 'Impress'#227'o de Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 279
        Height = 32
        Caption = 'Impress'#227'o de Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 456
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 456
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 467
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 439
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitLeft = -154
          ExplicitTop = 0
          ExplicitWidth = 996
          ExplicitHeight = 539
          object Label1: TLabel
            Left = 8
            Top = 0
            Width = 66
            Height = 13
            Caption = 'Mat'#233'ria-prima:'
          end
          object Label4: TLabel
            Left = 8
            Top = 40
            Width = 29
            Height = 13
            Caption = 'Pallet:'
          end
          object Label10: TLabel
            Left = 8
            Top = 80
            Width = 144
            Height = 13
            Caption = 'Terceiro (fornecedor / cliente):'
          end
          object Label53: TLabel
            Left = 416
            Top = 80
            Width = 90
            Height = 13
            Caption = 'Centro de estoque:'
          end
          object Ed01GraGruX: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CB01GraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CB01GraGruX: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 469
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = Ds01GraGruX
            TabOrder = 1
            dmkEditCB = Ed01GraGruX
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object Ed01Pallet: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Pallet'
            UpdCampo = 'Pallet'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CB01Pallet
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CB01Pallet: TdmkDBLookupComboBox
            Left = 64
            Top = 56
            Width = 469
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = Ds01VSPallet
            TabOrder = 3
            dmkEditCB = Ed01Pallet
            QryCampo = 'Pallet'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox2: TGroupBox
            Left = 540
            Top = 4
            Width = 245
            Height = 73
            Caption = ' Per'#237'odo: '
            TabOrder = 4
            object TP01DataIni: TdmkEditDateTimePicker
              Left = 8
              Top = 40
              Width = 112
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object Ck01DataIni: TCheckBox
              Left = 8
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data inicial'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object Ck01DataFim: TCheckBox
              Left = 124
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data final'
              TabOrder = 2
            end
            object TP01DataFim: TdmkEditDateTimePicker
              Left = 124
              Top = 40
              Width = 112
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object Ed01Terceiro: TdmkEditCB
            Left = 8
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Fornecedor'
            UpdCampo = 'Fornecedor'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CB01Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CB01Terceiro: TdmkDBLookupComboBox
            Left = 64
            Top = 96
            Width = 349
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = Ds01Fornecedor
            TabOrder = 6
            dmkEditCB = Ed01Terceiro
            QryCampo = 'Fornecedor'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object Ed00StqCenCad: TdmkEditCB
            Left = 416
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CB00StqCenCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CB00StqCenCad: TdmkDBLookupComboBox
            Left = 472
            Top = 96
            Width = 313
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = Ds00StqCenCad
            TabOrder = 8
            dmkEditCB = Ed00StqCenCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
        ExplicitWidth = 1004
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65523
  end
  object frxWET_CURTI_018_01_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412040000000
    ReportOptions.LastChange = 41608.425381412040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure DBCross1ColumnTotal0OnBeforePrint(Sender: TfrxComponen' +
        't);'
      'begin'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_01_AGetValue
    Left = 60
    Top = 368
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 119.055186460000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Movimento de Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 60.472480000000000000
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 332.598640000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 699.213050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 733.228820000000000000
          Top = 64.252010000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PALLET_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 71.811070000000000000
          Top = 83.149660000000000000
          Width = 891.969080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 71.811070000000000000
          Top = 64.252010000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_GRAGRUX_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 287.244280000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 381.732530000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 260.787570000000000000
          Top = 105.826840000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 98.267780000000000000
          Top = 105.826778980000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 566.929500000000000000
          Top = 105.826840000000000000
          Width = 132.283449920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 778.583180000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 733.228820000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 827.717070000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 699.213050000000000000
          Top = 105.826840000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 873.071430000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 427.086890000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 105.826840000000000000
          Width = 60.472440944881890000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 166.299320000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 211.653680000000000000
          Top = 105.826840000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 476.220780000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 922.205320000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 525.354670000000000000
          Top = 105.826840000000000000
          Width = 41.574800710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 971.339210000000000000
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
        RowCount = 0
        object Memo57: TfrxMemoView
          Left = 60.472480000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Pallet'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 332.598640000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 287.244280000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 381.732530000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 260.787570000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 98.267780000000000000
          Top = -0.000061019999999995
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 566.929500000000000000
          Width = 132.283449920000000000
          Height = 13.228346460000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 778.583180000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AcumAreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 733.228820000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AcumAreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 827.717070000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AcumPesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 699.213050000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'AcumPecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 873.071430000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AcumValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 476.220780000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."CustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DataField = 'DataHora'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 166.299320000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 211.653680000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 427.086890000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'ValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 922.205320000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumCustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 525.354670000000000000
          Width = 41.574800710000000000
          Height = 13.228346460000000000
          DataField = 'NFeNum'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."NFeNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 275.905690000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 600.945270000000000000
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrEstqR2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEstqR2CalcFields
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 60
    Top = 412
    object QrEstqR2OrdGrupSeq: TIntegerField
      FieldName = 'OrdGrupSeq'
    end
    object QrEstqR2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqR2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstqR2MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstqR2MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstqR2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR2MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstqR2DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR2Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrEstqR2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstqR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstqR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstqR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEstqR2SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrEstqR2SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrEstqR2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrEstqR2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR2SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstqR2Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrEstqR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrEstqR2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 160
    end
    object QrEstqR2NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR2NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR2AcumPecas: TFloatField
      FieldName = 'AcumPecas'
    end
    object QrEstqR2AcumPesoKg: TFloatField
      FieldName = 'AcumPesoKg'
    end
    object QrEstqR2AcumAreaM2: TFloatField
      FieldName = 'AcumAreaM2'
    end
    object QrEstqR2AcumAreaP2: TFloatField
      FieldName = 'AcumAreaP2'
    end
    object QrEstqR2AcumValorT: TFloatField
      FieldName = 'AcumValorT'
    end
    object QrEstqR2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEstqR2NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
    end
    object QrEstqR2CustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CustoM'
      Calculated = True
    end
    object QrEstqR2AcumCustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AcumCustoM'
      Calculated = True
    end
    object QrEstqR2NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrEstqR2NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrEstqR2VSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
  end
  object frxDsEstqR2: TfrxDBDataset
    UserName = 'frxDsEstqR2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'OrdGrupSeq=OrdGrupSeq'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'ValorT=ValorT'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_PALLET=NO_PALLET'
      'NO_FORNECE=NO_FORNECE'
      'AcumPecas=AcumPecas'
      'AcumPesoKg=AcumPesoKg'
      'AcumAreaM2=AcumAreaM2'
      'AcumAreaP2=AcumAreaP2'
      'AcumValorT=AcumValorT'
      'Ativo=Ativo'
      'NO_MovimID=NO_MovimID'
      'CustoM=CustoM'
      'AcumCustoM=AcumCustoM'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab')
    DataSet = QrEstqR2
    BCDToCurrency = False
    Left = 60
    Top = 456
  end
  object QrSumIR2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 60
    Top = 504
    object QrSumIR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumIR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumIR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumIR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSumIR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object Qr01Fornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 160
    Top = 368
    object Qr01FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr01FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds01Fornecedor: TDataSource
    DataSet = Qr01Fornecedor
    Left = 160
    Top = 412
  end
  object Qr01VSPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 232
    Top = 368
    object Qr01VSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr01VSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds01VSPallet: TDataSource
    DataSet = Qr01VSPallet
    Left = 232
    Top = 412
  end
  object Qr01GraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 304
    Top = 368
    object Qr01GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr01GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr01GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr01GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr01GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr01GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds01GraGruX: TDataSource
    DataSet = Qr01GraGruX
    Left = 304
    Top = 412
  end
  object Qr00StqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 380
    Top = 368
    object Qr00StqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr00StqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object Ds00StqCenCad: TDataSource
    DataSet = Qr00StqCenCad
    Left = 380
    Top = 412
  end
end
