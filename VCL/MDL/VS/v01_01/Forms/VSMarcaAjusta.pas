unit VSMarcaAjusta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmVSMarcaAjusta = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsAjusMarca: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    LaSrc: TLabel;
    LaDst: TLabel;
    EdSrc: TdmkEdit;
    EdDst: TdmkEdit;
    BtSubstituir: TBitBtn;
    QrAjusMarca: TMySQLQuery;
    QrAjusMarcaControle: TIntegerField;
    QrAjusMarcaMarca: TWideStringField;
    QrAjusMarcaAjustado: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSubstituirClick(Sender: TObject);
    procedure QrAjusMarcaAfterOpen(DataSet: TDataSet);
    procedure QrAjusMarcaBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenAjusMarca();
  public
    { Public declarations }
  end;

  var
  FmVSMarcaAjusta: TFmVSMarcaAjusta;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSMarcaAjusta.BtOKClick(Sender: TObject);
var
  Src, Dst: String;
begin
  Src := EdSrc.Text;
  Dst := EdDst.Text;
  //
  if MyObjects.FIC(Src = EmptyStr, EdSrc,
    'Informe os caracteres a serem substituídos!') then Exit;
  //
  ReopenAjusMarca();
end;

procedure TFmVSMarcaAjusta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMarcaAjusta.BtSubstituirClick(Sender: TObject);
var
  Itens: Integer;
  Src, Dst: String;
begin
  Itens := 0;
  Src := EdSrc.Text;
  Dst := EdDst.Text;
  //
  if Geral.MB_Pergunta('Deseja realmente substituir "' + Src + '" por "' + Dst +
  '"?') <> ID_YES then Exit;
  //
  Itens := 0;
  //
  QrAjusMarca.First;
  while not QrAjusMarca.Eof do
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE vsmovits SET Marca="' + QrAjusMarcaAjustado.Value + '"',
    'WHERE Controle=' + Geral.FF0(QrAjusMarcaControle.Value),
    '']));
    //
    Itens := Itens + 1;
    //
    QrAjusMarca.Next;
  end;
  //
  ReopenAjusMarca();
  Geral.MB_Info(Geral.FF0(Itens) + ' Foram alterados!');
end;

procedure TFmVSMarcaAjusta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMarcaAjusta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSMarcaAjusta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMarcaAjusta.QrAjusMarcaAfterOpen(DataSet: TDataSet);
begin
  BtSubstituir.Enabled := QrAjusMarca.RecordCount > 0;
  LaSrc.Enabled := QrAjusMarca.RecordCount = 0;
  EdSrc.Enabled := QrAjusMarca.RecordCount = 0;
  LaDst.Enabled := QrAjusMarca.RecordCount = 0;
  EdDst.Enabled := QrAjusMarca.RecordCount = 0;
end;

procedure TFmVSMarcaAjusta.QrAjusMarcaBeforeClose(DataSet: TDataSet);
begin
  BtSubstituir.Enabled := False;
end;

procedure TFmVSMarcaAjusta.ReopenAjusMarca();
var
  Src, Dst: String;
begin
  Src := EdSrc.Text;
  Dst := EdDst.Text;
  //
  if MyObjects.FIC(Src = EmptyStr, EdSrc,
    'Informe os caracteres a serem substituídos!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAjusMarca, Dmod.MyDB, [
  'SELECT Controle, Marca, ',
  //SUBSTRING_INDEX(Marca, "' + Src + '", 1) P, ',
  'REPLACE(Marca, "' + Src + '", "' + Dst + '") Ajustado ',
  'FROM vsmovits ',
  'WHERE Marca LIKE "%' + Src + '%" ',
  'ORDER BY Controle ',
  '']);
end;

end.
