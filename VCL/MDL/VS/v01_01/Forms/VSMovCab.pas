unit VSMovCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, AppListas, UnGrl_Consts, Vcl.ComCtrls;

type
  TFmVSMovCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrVSMovCab: TmySQLQuery;
    DsVSMovCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrVSMovCabCodigo: TIntegerField;
    QrVSMovCabMovimID: TIntegerField;
    QrVSMovCabCodigoID: TIntegerField;
    QrVSMovCabTemIMEIMrt: TIntegerField;
    QrVSMovCabLk: TIntegerField;
    QrVSMovCabDataCad: TDateField;
    QrVSMovCabDataAlt: TDateField;
    QrVSMovCabUserCad: TIntegerField;
    QrVSMovCabUserAlt: TIntegerField;
    QrVSMovCabAlterWeb: TSmallintField;
    QrVSMovCabAtivo: TSmallintField;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    QrVSMovCabNome: TWideStringField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_EstqMovimID: TWideStringField;
    QrVSMovItsNO_DstMovID: TWideStringField;
    QrVSMovItsNO_SrcMovID: TWideStringField;
    QrVSMovItsNO_LOC_CEN: TWideStringField;
    QrVSMovItsGraGru1: TLargeintField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TLargeintField;
    QrVSMovItsPedItsFin: TLargeintField;
    QrVSMovItsPedItsVda: TLargeintField;
    QrVSMovItsStqCenLoc: TLargeintField;
    QrVSMovItsItemNFe: TLargeintField;
    QrVSMovItsLnkNivXtr1: TLargeintField;
    QrVSMovItsLnkNivXtr2: TLargeintField;
    QrVSMovItsClientMO: TLargeintField;
    QrVSMovItsCustoPQ: TFloatField;
    QrVSMovItsVSMulFrnCab: TLargeintField;
    QrVSMovItsNFeSer: TLargeintField;
    QrVSMovItsNFeNum: TLargeintField;
    QrVSMovItsVSMulNFeCab: TLargeintField;
    QrVSMovItsJmpMovID: TLargeintField;
    QrVSMovItsJmpNivel1: TLargeintField;
    QrVSMovItsJmpNivel2: TLargeintField;
    QrVSMovItsRmsMovID: TLargeintField;
    QrVSMovItsRmsNivel1: TLargeintField;
    QrVSMovItsRmsNivel2: TLargeintField;
    QrVSMovItsGGXRcl: TLargeintField;
    QrVSMovItsRmsGGX: TLargeintField;
    QrVSMovItsJmpGGX: TLargeintField;
    QrVSMovItsDtCorrApo: TDateTimeField;
    DsVSMovIts: TDataSource;
    Irparajaneladomovimento1: TMenuItem;
    QrVSMovItsCustoM2: TFloatField;
    QrVSMovItsCustoKg: TFloatField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    AtualizaquantidadesdosIMEIs1: TMenuItem;
    QrVMIsOri: TMySQLQuery;
    QrVMIsOriSrcNivel2: TIntegerField;
    QrVMI: TMySQLQuery;
    QrVMIPecas: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSMovCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSMovCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSMovCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSMovCabBeforeClose(DataSet: TDataSet);
    procedure QrVSMovItsCalcFields(DataSet: TDataSet);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure AtualizaquantidadesdosIMEIs1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSMovIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenVSMovIts(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmVSMovCab: TFmVSMovCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSMovIts, UnVS_PF, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSMovCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSMovCab.MostraVSMovIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSMovIts, FmVSMovIts, afmoNegarComAviso) then
  begin
    FmVSMovIts.ImgTipo.SQLType := SQLType;
    FmVSMovIts.ShowModal;
    FmVSMovIts.Destroy;
  end;
end;

procedure TFmVSMovCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSMovCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSMovCab, QrVSMovIts);
  MyObjects.HabilitaMenuItemCabUpd(Irparajaneladomovimento1, QrVSMovCab);
end;

procedure TFmVSMovCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSMovCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSMovIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSMovIts);
end;

procedure TFmVSMovCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSMovCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSMovCab.DefParams;
var
  ATT_MovimID, ATT_MovimNiv: String;
  //
begin
  ATT_MovimID := dmkPF.ArrayToTexto('cab.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  VAR_GOTOTABELA := 'vsmovcab';
  VAR_GOTOMYSQLTABLE := QrVSMovCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cab.*, emid.Nome ');
  VAR_SQLx.Add('FROM vsmovcab cab ');
  VAR_SQLx.Add('LEFT JOIN vsmovimid emid ON emid.Codigo=cab.MovimID ');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND emid.Nome Like :P0');
  //
end;

procedure TFmVSMovCab.Irparajaneladomovimento1Click(Sender: TObject);
var
  MovimID, Codigo, Controle: Integer;
begin
  MovimID  := QrVSMovCabMovimID.Value;
  Codigo   := QrVSMovCabCodigoID.Value;
  Controle := QrVSMovItsControle.Value;
  VS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
end;

procedure TFmVSMovCab.ItsAltera1Click(Sender: TObject);
begin
  MostraVSMovIts(stUpd);
end;

procedure TFmVSMovCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSMovCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSMovCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSMovCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSMovIts', 'Controle', QrVSMovItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSMovIts,
      QrVSMovItsControle, QrVSMovItsControle.Value);
    ReopenVSMovIts(Controle);
  end;
}
end;

procedure TFmVSMovCab.ReopenVSMovIts(Controle: Integer);
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  //'CAST(vmi.Marca AS CHAR) Marca,',
  'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,',
  'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
  'CAST(vsp.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN, ',
  'IF(vmi.AreaM2 <> 0, vmi.ValorT/vmi.AreaM2, 0.0000) CustoM2, ',
  'IF(vmi.PesoKg <> 0, vmi.ValorT/vmi.PesoKg, 0.0000) CustoKg ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVsMovCabCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  '']);
  //Geral.MB_SQL(Self, QrVSMovIts);
  //
  QrVSMovIts.Locate('Controle', Controle, []);
end;


procedure TFmVSMovCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSMovCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSMovCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSMovCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSMovCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSMovCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSMovCabCodigo.Value;
  Close;
end;

procedure TFmVSMovCab.ItsInclui1Click(Sender: TObject);
begin
  MostraVSMovIts(stIns);
end;

procedure TFmVSMovCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSMovCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmovcab');
end;

procedure TFmVSMovCab.BtConfirmaClick(Sender: TObject);
{
var
  Codigo: Integer;
  Nome: String;
}
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('vsmovcab', 'Codigo', ImgTipo.SQLType,
    QrVSMovCabCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrVSMovCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'vsmovcab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmVSMovCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsmovcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsmovcab', 'Codigo');
end;

procedure TFmVSMovCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSMovCab.AtualizaquantidadesdosIMEIs1Click(Sender: TObject);
const
  Gera = False;
var
  Controle, VMI_Sorc(*, I, N*): Integer;
  Pecas, AreaM2, AreaP2: Double;
  //ArrSrcs: array of Integer;
  //Achou: Boolean;
begin
  if QrVSMovCabMovimID.Value in ([7,8]) then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMIsOri, Dmod.MyDB, [
      'SELECT DISTINCT(SrcNivel2) SrcNivel2',
      'FROM vsmovits',
      'WHERE MovimCod=' + Geral.FF0(QrVSMovCabCodigo.Value),
      'AND MovimID IN (7,8) ', // Classe e reclasse
      'AND MovimNiv=1 ', // Baixa
      '']);
      PB1.Position := 0;
      PB1.Max := QrVSMovIts.RecordCount + QrVMIsOri.RecordCount;
      //SetLength(ArrSrcs, 0);
      //N := 0;
      QrVSMovIts.First;
      while not QrVSMovIts.Eof do
      begin
        Controle := QrVSMovItsControle.Value;
        case TEstqMovimNiv(QrVSMovItsMovimNiv.Value) of
          eminSorcClass: // Baixa
          begin
            MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
            'Atualizando quantidades do VMI_Baix = ' + Geral.FF0(Controle));
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
            'SELECT SUM(Pecas*FatorIntDst) Pecas, SUM(AreaM2) AreaM2, ',
            'SUM(AreaP2) AreaP2 ',
            'FROM vscacitsa ',
            'WHERE VMI_Baix=' + Geral.FF0(Controle),
            '']);
            Pecas  := -QrVMIPecas.Value;
            AreaM2 := -QrVMIAreaM2.Value;
            AreaP2 := -QrVMIAreaP2.Value;
           //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
            'Pecas', 'AreaM2', 'AreaP2'], [
            'Controle'], [
            Pecas, AreaM2, AreaP2], [
            Controle], True) then
            begin
              (*
              VMI_Sorc := QrVSMovItsSrcNivel2.Value;
              Achou := False;
              for I := Low(ArrSrcs) to High(ArrSrcs) do
              begin
                if ArrSrcs[I] = VMI_Sorc then
                  Achou := True;
              end;
              if Achou = False then
              begin
                N := N + 1;
                SetLength(ArrSrcs, N);
                ArrSrcs[N - 1] := VMI_Sorc;
                PB1.Max := PB1.Max + 1;
              end;
              *)
            end;
          end;
          eminDestClass: // Gera��o
          begin
            MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
            'Atualizando quantidades do VMI_Dest = ' + Geral.FF0(Controle));
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
            'SELECT SUM(Pecas*FatorIntDst) Pecas, SUM(AreaM2) AreaM2, ',
            'SUM(AreaP2) AreaP2 ',
            'FROM vscacitsa ',
            'WHERE VMI_Dest=' + Geral.FF0(Controle),
            '']);
            Pecas  := QrVMIPecas.Value;
            AreaM2 := QrVMIAreaM2.Value;
            AreaP2 := QrVMIAreaP2.Value;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
            'Pecas', 'AreaM2', 'AreaP2'], [
            'Controle'], [
            Pecas, AreaM2, AreaP2], [
            Controle], True) then
              VS_PF.AtualizaSaldoVirtualVSMovIts(Controle, Gera);
          end;
        end;
        //
        QrVSMovIts.Next;
      end;
      //
      //for I := 0 to N - 1 do
      QrVMIsOri.First;
      while not QrVMIsOri.Eof do
      begin
        VS_PF.AtualizaSaldoVirtualVSMovIts(QrVMIsOriSrcNivel2.Value, Gera);
        QrVMIsOri.Next;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    PB1.Position := 0;
    PB1.Max := 0;
  end else
    Geral.MB_Info('A��o implementada apenas para OC de classe e reclasse!' +
    sLineBreak + 'Para outro movimento solicite implementa��o � DERMATEK!');
  //
  LocCod(QrVSMovCabCodigo.Value, QrVSMovCabCodigo.Value);
end;

procedure TFmVSMovCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSMovCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSMovCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSMovCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMovCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSMovCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSMovCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMovCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSMovCab.QrVSMovCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSMovCab.QrVSMovCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMovIts(0);
end;

procedure TFmVSMovCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSMovCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSMovCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSMovCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsmovcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSMovCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSMovCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmovcab');
end;

procedure TFmVSMovCab.QrVSMovCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMovIts.Close;
end;

procedure TFmVSMovCab.QrVSMovCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSMovCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSMovCab.QrVSMovItsCalcFields(DataSet: TDataSet);
begin
  QrVSMovItsNO_EstqMovimID.Value := sEstqMovimID[QrVSMovItsMovimID.Value];
  QrVSMovItsNO_SrcMovID.Value    := sEstqMovimID[QrVSMovItsSrcMovID.Value];
  QrVSMovItsNO_DstMovID.Value    := sEstqMovimID[QrVSMovItsDstMovID.Value];
end;

end.

