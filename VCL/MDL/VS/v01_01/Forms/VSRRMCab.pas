unit VSRRMCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects,
  UnProjGroup_Consts, dmkDBGridZTO, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums,
  UnProjGroup_Vars;

type
  TFmVSRRMCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSRRMCab: TmySQLQuery;
    DsVSRRMCab: TDataSource;
    QrVSRRMAtu: TmySQLQuery;
    DsVSRRMAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSRRMCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSRRMCabEmpresa: TIntegerField;
    QrVSRRMCabDtHrAberto: TDateTimeField;
    QrVSRRMCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSRRMCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSRRMOriIMEI: TmySQLQuery;
    DsVSRRMOriIMEI: TDataSource;
    QrVSRRMCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSRRMCabLk: TIntegerField;
    QrVSRRMCabDataCad: TDateField;
    QrVSRRMCabDataAlt: TDateField;
    QrVSRRMCabUserCad: TIntegerField;
    QrVSRRMCabUserAlt: TIntegerField;
    QrVSRRMCabAlterWeb: TSmallintField;
    QrVSRRMCabAtivo: TSmallintField;
    QrVSRRMCabPecasMan: TFloatField;
    QrVSRRMCabAreaManM2: TFloatField;
    QrVSRRMCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSRRMCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSRRMCabNO_TIPO: TWideStringField;
    QrVSRRMCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSRRMCabNO_DtHrLibOpe: TWideStringField;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSRRMCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSRRMCabCacCod: TIntegerField;
    QrVSRRMCabGraGruX: TIntegerField;
    QrVSRRMCabCustoManMOKg: TFloatField;
    QrVSRRMCabCustoManMOTot: TFloatField;
    QrVSRRMCabValorManMP: TFloatField;
    QrVSRRMCabValorManT: TFloatField;
    QrVSRRMCabDtHrLibOpe: TDateTimeField;
    QrVSRRMCabDtHrCfgOpe: TDateTimeField;
    QrVSRRMCabDtHrFimOpe: TDateTimeField;
    QrVSRRMCabPecasSrc: TFloatField;
    QrVSRRMCabAreaSrcM2: TFloatField;
    QrVSRRMCabAreaSrcP2: TFloatField;
    QrVSRRMCabPecasDst: TFloatField;
    QrVSRRMCabAreaDstM2: TFloatField;
    QrVSRRMCabAreaDstP2: TFloatField;
    QrVSRRMCabPecasSdo: TFloatField;
    QrVSRRMCabAreaSdoM2: TFloatField;
    QrVSRRMCabAreaSdoP2: TFloatField;
    QrVSRRMDst: TmySQLQuery;
    DsVSRRMDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    AtualizaestoqueEmProcesso1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrVSRRMBxa: TmySQLQuery;
    DsVSRRMBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSRRMCabPesoKgSrc: TFloatField;
    QrVSRRMCabPesoKgMan: TFloatField;
    QrVSRRMCabPesoKgDst: TFloatField;
    QrVSRRMCabPesoKgSdo: TFloatField;
    QrVSRRMCabValorTMan: TFloatField;
    QrVSRRMCabValorTSrc: TFloatField;
    QrVSRRMCabValorTSdo: TFloatField;
    QrVSRRMCabPecasINI: TFloatField;
    QrVSRRMCabAreaINIM2: TFloatField;
    QrVSRRMCabAreaINIP2: TFloatField;
    QrVSRRMCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSRRMCabPesoKgBxa: TFloatField;
    QrVSRRMCabPecasBxa: TFloatField;
    QrVSRRMCabAreaBxaM2: TFloatField;
    QrVSRRMCabAreaBxaP2: TFloatField;
    QrVSRRMCabValorTBxa: TFloatField;
    Definiodequantidadesmanualmente1: TMenuItem;
    PCRRMOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSRRMOriPallet: TmySQLQuery;
    DsVSRRMOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    otal2: TMenuItem;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdCustoMOM2: TdmkEdit;
    Label48: TLabel;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label51: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label52: TLabel;
    DBEdit41: TDBEdit;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrVSRRMCabCliente: TIntegerField;
    QrVSRRMCabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrVSRRMCabNFeRem: TIntegerField;
    QrVSRRMCabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrVSRRMCabTemIMEIMrt: TIntegerField;
    QrVSRRMAtuCodigo: TLargeintField;
    QrVSRRMAtuControle: TLargeintField;
    QrVSRRMAtuMovimCod: TLargeintField;
    QrVSRRMAtuMovimNiv: TLargeintField;
    QrVSRRMAtuMovimTwn: TLargeintField;
    QrVSRRMAtuEmpresa: TLargeintField;
    QrVSRRMAtuTerceiro: TLargeintField;
    QrVSRRMAtuCliVenda: TLargeintField;
    QrVSRRMAtuMovimID: TLargeintField;
    QrVSRRMAtuDataHora: TDateTimeField;
    QrVSRRMAtuPallet: TLargeintField;
    QrVSRRMAtuGraGruX: TLargeintField;
    QrVSRRMAtuPecas: TFloatField;
    QrVSRRMAtuPesoKg: TFloatField;
    QrVSRRMAtuAreaM2: TFloatField;
    QrVSRRMAtuAreaP2: TFloatField;
    QrVSRRMAtuValorT: TFloatField;
    QrVSRRMAtuSrcMovID: TLargeintField;
    QrVSRRMAtuSrcNivel1: TLargeintField;
    QrVSRRMAtuSrcNivel2: TLargeintField;
    QrVSRRMAtuSrcGGX: TLargeintField;
    QrVSRRMAtuSdoVrtPeca: TFloatField;
    QrVSRRMAtuSdoVrtPeso: TFloatField;
    QrVSRRMAtuSdoVrtArM2: TFloatField;
    QrVSRRMAtuObserv: TWideStringField;
    QrVSRRMAtuSerieFch: TLargeintField;
    QrVSRRMAtuFicha: TLargeintField;
    QrVSRRMAtuMisturou: TLargeintField;
    QrVSRRMAtuFornecMO: TLargeintField;
    QrVSRRMAtuCustoMOKg: TFloatField;
    QrVSRRMAtuCustoMOM2: TFloatField;
    QrVSRRMAtuCustoMOTot: TFloatField;
    QrVSRRMAtuValorMP: TFloatField;
    QrVSRRMAtuDstMovID: TLargeintField;
    QrVSRRMAtuDstNivel1: TLargeintField;
    QrVSRRMAtuDstNivel2: TLargeintField;
    QrVSRRMAtuDstGGX: TLargeintField;
    QrVSRRMAtuQtdGerPeca: TFloatField;
    QrVSRRMAtuQtdGerPeso: TFloatField;
    QrVSRRMAtuQtdGerArM2: TFloatField;
    QrVSRRMAtuQtdGerArP2: TFloatField;
    QrVSRRMAtuQtdAntPeca: TFloatField;
    QrVSRRMAtuQtdAntPeso: TFloatField;
    QrVSRRMAtuQtdAntArM2: TFloatField;
    QrVSRRMAtuQtdAntArP2: TFloatField;
    QrVSRRMAtuNotaMPAG: TFloatField;
    QrVSRRMAtuNO_PALLET: TWideStringField;
    QrVSRRMAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMAtuNO_TTW: TWideStringField;
    QrVSRRMAtuID_TTW: TLargeintField;
    QrVSRRMAtuNO_FORNECE: TWideStringField;
    QrVSRRMAtuReqMovEstq: TLargeintField;
    QrVSRRMAtuCUSTO_M2: TFloatField;
    QrVSRRMAtuCUSTO_P2: TFloatField;
    QrVSRRMAtuNO_LOC_CEN: TWideStringField;
    QrVSRRMAtuMarca: TWideStringField;
    QrVSRRMAtuPedItsLib: TLargeintField;
    QrVSRRMAtuStqCenLoc: TLargeintField;
    QrVSRRMAtuNO_FICHA: TWideStringField;
    QrVSRRMOriIMEICodigo: TLargeintField;
    QrVSRRMOriIMEIControle: TLargeintField;
    QrVSRRMOriIMEIMovimCod: TLargeintField;
    QrVSRRMOriIMEIMovimNiv: TLargeintField;
    QrVSRRMOriIMEIMovimTwn: TLargeintField;
    QrVSRRMOriIMEIEmpresa: TLargeintField;
    QrVSRRMOriIMEITerceiro: TLargeintField;
    QrVSRRMOriIMEICliVenda: TLargeintField;
    QrVSRRMOriIMEIMovimID: TLargeintField;
    QrVSRRMOriIMEIDataHora: TDateTimeField;
    QrVSRRMOriIMEIPallet: TLargeintField;
    QrVSRRMOriIMEIGraGruX: TLargeintField;
    QrVSRRMOriIMEIPecas: TFloatField;
    QrVSRRMOriIMEIPesoKg: TFloatField;
    QrVSRRMOriIMEIAreaM2: TFloatField;
    QrVSRRMOriIMEIAreaP2: TFloatField;
    QrVSRRMOriIMEIValorT: TFloatField;
    QrVSRRMOriIMEISrcMovID: TLargeintField;
    QrVSRRMOriIMEISrcNivel1: TLargeintField;
    QrVSRRMOriIMEISrcNivel2: TLargeintField;
    QrVSRRMOriIMEISrcGGX: TLargeintField;
    QrVSRRMOriIMEISdoVrtPeca: TFloatField;
    QrVSRRMOriIMEISdoVrtPeso: TFloatField;
    QrVSRRMOriIMEISdoVrtArM2: TFloatField;
    QrVSRRMOriIMEIObserv: TWideStringField;
    QrVSRRMOriIMEISerieFch: TLargeintField;
    QrVSRRMOriIMEIFicha: TLargeintField;
    QrVSRRMOriIMEIMisturou: TLargeintField;
    QrVSRRMOriIMEIFornecMO: TLargeintField;
    QrVSRRMOriIMEICustoMOKg: TFloatField;
    QrVSRRMOriIMEICustoMOM2: TFloatField;
    QrVSRRMOriIMEICustoMOTot: TFloatField;
    QrVSRRMOriIMEIValorMP: TFloatField;
    QrVSRRMOriIMEIDstMovID: TLargeintField;
    QrVSRRMOriIMEIDstNivel1: TLargeintField;
    QrVSRRMOriIMEIDstNivel2: TLargeintField;
    QrVSRRMOriIMEIDstGGX: TLargeintField;
    QrVSRRMOriIMEIQtdGerPeca: TFloatField;
    QrVSRRMOriIMEIQtdGerPeso: TFloatField;
    QrVSRRMOriIMEIQtdGerArM2: TFloatField;
    QrVSRRMOriIMEIQtdGerArP2: TFloatField;
    QrVSRRMOriIMEIQtdAntPeca: TFloatField;
    QrVSRRMOriIMEIQtdAntPeso: TFloatField;
    QrVSRRMOriIMEIQtdAntArM2: TFloatField;
    QrVSRRMOriIMEIQtdAntArP2: TFloatField;
    QrVSRRMOriIMEINotaMPAG: TFloatField;
    QrVSRRMOriIMEINO_PALLET: TWideStringField;
    QrVSRRMOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSRRMOriIMEINO_TTW: TWideStringField;
    QrVSRRMOriIMEIID_TTW: TLargeintField;
    QrVSRRMOriIMEINO_FORNECE: TWideStringField;
    QrVSRRMOriIMEINO_SerieFch: TWideStringField;
    QrVSRRMOriIMEIReqMovEstq: TLargeintField;
    QrVSRRMOriPalletPallet: TLargeintField;
    QrVSRRMOriPalletGraGruX: TLargeintField;
    QrVSRRMOriPalletPecas: TFloatField;
    QrVSRRMOriPalletAreaM2: TFloatField;
    QrVSRRMOriPalletAreaP2: TFloatField;
    QrVSRRMOriPalletPesoKg: TFloatField;
    QrVSRRMOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMOriPalletNO_Pallet: TWideStringField;
    QrVSRRMOriPalletSerieFch: TLargeintField;
    QrVSRRMOriPalletFicha: TLargeintField;
    QrVSRRMOriPalletNO_TTW: TWideStringField;
    QrVSRRMOriPalletID_TTW: TLargeintField;
    QrVSRRMOriPalletSeries_E_Fichas: TWideStringField;
    QrVSRRMOriPalletTerceiro: TLargeintField;
    QrVSRRMOriPalletMarca: TWideStringField;
    QrVSRRMOriPalletNO_FORNECE: TWideStringField;
    QrVSRRMOriPalletNO_SerieFch: TWideStringField;
    QrVSRRMOriPalletValorT: TFloatField;
    QrVSRRMOriPalletSdoVrtPeca: TFloatField;
    QrVSRRMOriPalletSdoVrtPeso: TFloatField;
    QrVSRRMOriPalletSdoVrtArM2: TFloatField;
    QrVSRRMDstCodigo: TLargeintField;
    QrVSRRMDstControle: TLargeintField;
    QrVSRRMDstMovimCod: TLargeintField;
    QrVSRRMDstMovimNiv: TLargeintField;
    QrVSRRMDstMovimTwn: TLargeintField;
    QrVSRRMDstEmpresa: TLargeintField;
    QrVSRRMDstTerceiro: TLargeintField;
    QrVSRRMDstCliVenda: TLargeintField;
    QrVSRRMDstMovimID: TLargeintField;
    QrVSRRMDstDataHora: TDateTimeField;
    QrVSRRMDstPallet: TLargeintField;
    QrVSRRMDstGraGruX: TLargeintField;
    QrVSRRMDstPecas: TFloatField;
    QrVSRRMDstPesoKg: TFloatField;
    QrVSRRMDstAreaM2: TFloatField;
    QrVSRRMDstAreaP2: TFloatField;
    QrVSRRMDstValorT: TFloatField;
    QrVSRRMDstSrcMovID: TLargeintField;
    QrVSRRMDstSrcNivel1: TLargeintField;
    QrVSRRMDstSrcNivel2: TLargeintField;
    QrVSRRMDstSrcGGX: TLargeintField;
    QrVSRRMDstSdoVrtPeca: TFloatField;
    QrVSRRMDstSdoVrtPeso: TFloatField;
    QrVSRRMDstSdoVrtArM2: TFloatField;
    QrVSRRMDstObserv: TWideStringField;
    QrVSRRMDstSerieFch: TLargeintField;
    QrVSRRMDstFicha: TLargeintField;
    QrVSRRMDstMisturou: TLargeintField;
    QrVSRRMDstFornecMO: TLargeintField;
    QrVSRRMDstCustoMOKg: TFloatField;
    QrVSRRMDstCustoMOM2: TFloatField;
    QrVSRRMDstCustoMOTot: TFloatField;
    QrVSRRMDstValorMP: TFloatField;
    QrVSRRMDstDstMovID: TLargeintField;
    QrVSRRMDstDstNivel1: TLargeintField;
    QrVSRRMDstDstNivel2: TLargeintField;
    QrVSRRMDstDstGGX: TLargeintField;
    QrVSRRMDstQtdGerPeca: TFloatField;
    QrVSRRMDstQtdGerPeso: TFloatField;
    QrVSRRMDstQtdGerArM2: TFloatField;
    QrVSRRMDstQtdGerArP2: TFloatField;
    QrVSRRMDstQtdAntPeca: TFloatField;
    QrVSRRMDstQtdAntPeso: TFloatField;
    QrVSRRMDstQtdAntArM2: TFloatField;
    QrVSRRMDstQtdAntArP2: TFloatField;
    QrVSRRMDstNotaMPAG: TFloatField;
    QrVSRRMDstNO_PALLET: TWideStringField;
    QrVSRRMDstNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMDstNO_TTW: TWideStringField;
    QrVSRRMDstID_TTW: TLargeintField;
    QrVSRRMDstNO_FORNECE: TWideStringField;
    QrVSRRMDstNO_SerieFch: TWideStringField;
    QrVSRRMDstReqMovEstq: TLargeintField;
    QrVSRRMDstPedItsFin: TLargeintField;
    QrVSRRMDstMarca: TWideStringField;
    QrVSRRMBxaCodigo: TLargeintField;
    QrVSRRMBxaControle: TLargeintField;
    QrVSRRMBxaMovimCod: TLargeintField;
    QrVSRRMBxaMovimNiv: TLargeintField;
    QrVSRRMBxaMovimTwn: TLargeintField;
    QrVSRRMBxaEmpresa: TLargeintField;
    QrVSRRMBxaTerceiro: TLargeintField;
    QrVSRRMBxaCliVenda: TLargeintField;
    QrVSRRMBxaMovimID: TLargeintField;
    QrVSRRMBxaDataHora: TDateTimeField;
    QrVSRRMBxaPallet: TLargeintField;
    QrVSRRMBxaGraGruX: TLargeintField;
    QrVSRRMBxaPecas: TFloatField;
    QrVSRRMBxaPesoKg: TFloatField;
    QrVSRRMBxaAreaM2: TFloatField;
    QrVSRRMBxaAreaP2: TFloatField;
    QrVSRRMBxaValorT: TFloatField;
    QrVSRRMBxaSrcMovID: TLargeintField;
    QrVSRRMBxaSrcNivel1: TLargeintField;
    QrVSRRMBxaSrcNivel2: TLargeintField;
    QrVSRRMBxaSrcGGX: TLargeintField;
    QrVSRRMBxaSdoVrtPeca: TFloatField;
    QrVSRRMBxaSdoVrtPeso: TFloatField;
    QrVSRRMBxaSdoVrtArM2: TFloatField;
    QrVSRRMBxaObserv: TWideStringField;
    QrVSRRMBxaSerieFch: TLargeintField;
    QrVSRRMBxaFicha: TLargeintField;
    QrVSRRMBxaMisturou: TLargeintField;
    QrVSRRMBxaFornecMO: TLargeintField;
    QrVSRRMBxaCustoMOKg: TFloatField;
    QrVSRRMBxaCustoMOM2: TFloatField;
    QrVSRRMBxaCustoMOTot: TFloatField;
    QrVSRRMBxaValorMP: TFloatField;
    QrVSRRMBxaDstMovID: TLargeintField;
    QrVSRRMBxaDstNivel1: TLargeintField;
    QrVSRRMBxaDstNivel2: TLargeintField;
    QrVSRRMBxaDstGGX: TLargeintField;
    QrVSRRMBxaQtdGerPeca: TFloatField;
    QrVSRRMBxaQtdGerPeso: TFloatField;
    QrVSRRMBxaQtdGerArM2: TFloatField;
    QrVSRRMBxaQtdGerArP2: TFloatField;
    QrVSRRMBxaQtdAntPeca: TFloatField;
    QrVSRRMBxaQtdAntPeso: TFloatField;
    QrVSRRMBxaQtdAntArM2: TFloatField;
    QrVSRRMBxaQtdAntArP2: TFloatField;
    QrVSRRMBxaNotaMPAG: TFloatField;
    QrVSRRMBxaNO_PALLET: TWideStringField;
    QrVSRRMBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMBxaNO_TTW: TWideStringField;
    QrVSRRMBxaID_TTW: TLargeintField;
    QrVSRRMBxaNO_FORNECE: TWideStringField;
    QrVSRRMBxaNO_SerieFch: TWideStringField;
    QrVSRRMBxaReqMovEstq: TLargeintField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    Label60: TLabel;
    DBEdit45: TDBEdit;
    N4: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    QrVSRRMAtuNO_FORNEC_MO: TWideStringField;
    FornecedorMO1: TMenuItem;
    Localdoestoque1: TMenuItem;
    Cliente1: TMenuItem;
    QrVSRRMDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSRRMAtuClientMO: TLargeintField;
    OPpreenchida1: TMenuItem;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    LaGGXDst: TLabel;
    CkCpermitirCrust: TCheckBox;
    EdGGXDst: TdmkEditCB;
    CBGGXDst: TdmkDBLookupComboBox;
    QrVSRRMCabGGXDst: TIntegerField;
    IrparacadastrodoPallet1: TMenuItem;
    dmkDBEdit3: TdmkDBEdit;
    Label59: TLabel;
    QrVSRRMCabSerieRem: TSmallintField;
    EdSerieRem: TdmkEdit;
    Label57: TLabel;
    N5: TMenuItem;
    MudarodestinoparaoutraOP1: TMenuItem;
    QrVSCOPCab: TmySQLQuery;
    QrVSCOPCabCodigo: TIntegerField;
    QrVSCOPCabNome: TWideStringField;
    DsVSCOPCab: TDataSource;
    GBConfig: TGroupBox;
    Label6: TLabel;
    Label64: TLabel;
    EdControle: TdmkEdit;
    EdVSCOPCab: TdmkEditCB;
    CBVSCOPCab: TdmkDBLookupComboBox;
    QrVSRRMCabVSCOPCab: TIntegerField;
    QrVSRRMCabNO_VSCOPCab: TWideStringField;
    SbVSCOPCabCad: TSpeedButton;
    SbVSCOPCabCpy: TSpeedButton;
    Adicionadesclassificado1: TMenuItem;
    QrVSRRMDesclDst: TmySQLQuery;
    QrVSRRMDesclDstCodigo: TLargeintField;
    QrVSRRMDesclDstControle: TLargeintField;
    QrVSRRMDesclDstMovimCod: TLargeintField;
    QrVSRRMDesclDstMovimNiv: TLargeintField;
    QrVSRRMDesclDstMovimTwn: TLargeintField;
    QrVSRRMDesclDstEmpresa: TLargeintField;
    QrVSRRMDesclDstTerceiro: TLargeintField;
    QrVSRRMDesclDstCliVenda: TLargeintField;
    QrVSRRMDesclDstMovimID: TLargeintField;
    QrVSRRMDesclDstDataHora: TDateTimeField;
    QrVSRRMDesclDstPallet: TLargeintField;
    QrVSRRMDesclDstGraGruX: TLargeintField;
    QrVSRRMDesclDstPecas: TFloatField;
    QrVSRRMDesclDstPesoKg: TFloatField;
    QrVSRRMDesclDstAreaM2: TFloatField;
    QrVSRRMDesclDstAreaP2: TFloatField;
    QrVSRRMDesclDstValorT: TFloatField;
    QrVSRRMDesclDstSrcMovID: TLargeintField;
    QrVSRRMDesclDstSrcNivel1: TLargeintField;
    QrVSRRMDesclDstSrcNivel2: TLargeintField;
    QrVSRRMDesclDstSrcGGX: TLargeintField;
    QrVSRRMDesclDstSdoVrtPeca: TFloatField;
    QrVSRRMDesclDstSdoVrtPeso: TFloatField;
    QrVSRRMDesclDstSdoVrtArM2: TFloatField;
    QrVSRRMDesclDstObserv: TWideStringField;
    QrVSRRMDesclDstSerieFch: TLargeintField;
    QrVSRRMDesclDstFicha: TLargeintField;
    QrVSRRMDesclDstMisturou: TLargeintField;
    QrVSRRMDesclDstFornecMO: TLargeintField;
    QrVSRRMDesclDstCustoMOKg: TFloatField;
    QrVSRRMDesclDstCustoMOM2: TFloatField;
    QrVSRRMDesclDstCustoMOTot: TFloatField;
    QrVSRRMDesclDstValorMP: TFloatField;
    QrVSRRMDesclDstDstMovID: TLargeintField;
    QrVSRRMDesclDstDstNivel1: TLargeintField;
    QrVSRRMDesclDstDstNivel2: TLargeintField;
    QrVSRRMDesclDstDstGGX: TLargeintField;
    QrVSRRMDesclDstQtdGerPeca: TFloatField;
    QrVSRRMDesclDstQtdGerPeso: TFloatField;
    QrVSRRMDesclDstQtdGerArM2: TFloatField;
    QrVSRRMDesclDstQtdGerArP2: TFloatField;
    QrVSRRMDesclDstQtdAntPeca: TFloatField;
    QrVSRRMDesclDstQtdAntPeso: TFloatField;
    QrVSRRMDesclDstQtdAntArM2: TFloatField;
    QrVSRRMDesclDstQtdAntArP2: TFloatField;
    QrVSRRMDesclDstNotaMPAG: TFloatField;
    QrVSRRMDesclDstNO_PALLET: TWideStringField;
    QrVSRRMDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMDesclDstNO_TTW: TWideStringField;
    QrVSRRMDesclDstID_TTW: TLargeintField;
    QrVSRRMDesclDstNO_FORNECE: TWideStringField;
    QrVSRRMDesclDstNO_SerieFch: TWideStringField;
    QrVSRRMDesclDstReqMovEstq: TLargeintField;
    QrVSRRMDesclDstPedItsFin: TLargeintField;
    QrVSRRMDesclDstMarca: TWideStringField;
    QrVSRRMDesclDstStqCenLoc: TLargeintField;
    DsVSRRMDesclDst: TDataSource;
    QrVSRRMDesclBxa: TmySQLQuery;
    QrVSRRMDesclBxaCodigo: TLargeintField;
    QrVSRRMDesclBxaControle: TLargeintField;
    QrVSRRMDesclBxaMovimCod: TLargeintField;
    QrVSRRMDesclBxaMovimNiv: TLargeintField;
    QrVSRRMDesclBxaMovimTwn: TLargeintField;
    QrVSRRMDesclBxaEmpresa: TLargeintField;
    QrVSRRMDesclBxaTerceiro: TLargeintField;
    QrVSRRMDesclBxaCliVenda: TLargeintField;
    QrVSRRMDesclBxaMovimID: TLargeintField;
    QrVSRRMDesclBxaDataHora: TDateTimeField;
    QrVSRRMDesclBxaPallet: TLargeintField;
    QrVSRRMDesclBxaGraGruX: TLargeintField;
    QrVSRRMDesclBxaPecas: TFloatField;
    QrVSRRMDesclBxaPesoKg: TFloatField;
    QrVSRRMDesclBxaAreaM2: TFloatField;
    QrVSRRMDesclBxaAreaP2: TFloatField;
    QrVSRRMDesclBxaValorT: TFloatField;
    QrVSRRMDesclBxaSrcMovID: TLargeintField;
    QrVSRRMDesclBxaSrcNivel1: TLargeintField;
    QrVSRRMDesclBxaSrcNivel2: TLargeintField;
    QrVSRRMDesclBxaSrcGGX: TLargeintField;
    QrVSRRMDesclBxaSdoVrtPeca: TFloatField;
    QrVSRRMDesclBxaSdoVrtPeso: TFloatField;
    QrVSRRMDesclBxaSdoVrtArM2: TFloatField;
    QrVSRRMDesclBxaObserv: TWideStringField;
    QrVSRRMDesclBxaSerieFch: TLargeintField;
    QrVSRRMDesclBxaFicha: TLargeintField;
    QrVSRRMDesclBxaMisturou: TLargeintField;
    QrVSRRMDesclBxaFornecMO: TLargeintField;
    QrVSRRMDesclBxaCustoMOKg: TFloatField;
    QrVSRRMDesclBxaCustoMOM2: TFloatField;
    QrVSRRMDesclBxaCustoMOTot: TFloatField;
    QrVSRRMDesclBxaValorMP: TFloatField;
    QrVSRRMDesclBxaDstMovID: TLargeintField;
    QrVSRRMDesclBxaDstNivel1: TLargeintField;
    QrVSRRMDesclBxaDstNivel2: TLargeintField;
    QrVSRRMDesclBxaDstGGX: TLargeintField;
    QrVSRRMDesclBxaQtdGerPeca: TFloatField;
    QrVSRRMDesclBxaQtdGerPeso: TFloatField;
    QrVSRRMDesclBxaQtdGerArM2: TFloatField;
    QrVSRRMDesclBxaQtdGerArP2: TFloatField;
    QrVSRRMDesclBxaQtdAntPeca: TFloatField;
    QrVSRRMDesclBxaQtdAntPeso: TFloatField;
    QrVSRRMDesclBxaQtdAntArM2: TFloatField;
    QrVSRRMDesclBxaQtdAntArP2: TFloatField;
    QrVSRRMDesclBxaNotaMPAG: TFloatField;
    QrVSRRMDesclBxaNO_PALLET: TWideStringField;
    QrVSRRMDesclBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMDesclBxaNO_TTW: TWideStringField;
    QrVSRRMDesclBxaID_TTW: TLargeintField;
    QrVSRRMDesclBxaNO_FORNECE: TWideStringField;
    QrVSRRMDesclBxaNO_SerieFch: TWideStringField;
    QrVSRRMDesclBxaReqMovEstq: TLargeintField;
    DsVSRRMDesclBxa: TDataSource;
    PCRRMDst: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel12: TPanel;
    Panel13: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    N6: TMenuItem;
    Removedesclassificado1: TMenuItem;
    Editaea1: TMenuItem;
    Corrigetodasbaixas1: TMenuItem;
    TabSheet5: TTabSheet;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    DsEmit: TDataSource;
    DBGrid6: TDBGrid;
    PMPesagem: TPopupMenu;
    EmitePesagem2: TMenuItem;
    Reimprimereceita1: TMenuItem;
    ExcluiPesagem1: TMenuItem;
    PB1: TProgressBar;
    BtPesagem: TBitBtn;
    DBEdit48: TDBEdit;
    Label65: TLabel;
    QrVSRRMAtuCustoPQ: TFloatField;
    Recalculacusto1: TMenuItem;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    QrVSRRMCabEmitGru: TIntegerField;
    QrVSRRMCabNO_EmitGru: TWideStringField;
    Label66: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    Label67: TLabel;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    TabSheet6: TTabSheet;
    DBGrid7: TDBGrid;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    N7: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    DBGIts: TDBGrid;
    Emitepesagem1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    AdicionasubprodutoWBRaspa1: TMenuItem;
    TabSheet7: TTabSheet;
    DBGrid8: TDBGrid;
    QrVSRRMSubPrd: TmySQLQuery;
    QrVSRRMSubPrdCodigo: TLargeintField;
    QrVSRRMSubPrdControle: TLargeintField;
    QrVSRRMSubPrdMovimCod: TLargeintField;
    QrVSRRMSubPrdMovimNiv: TLargeintField;
    QrVSRRMSubPrdMovimTwn: TLargeintField;
    QrVSRRMSubPrdEmpresa: TLargeintField;
    QrVSRRMSubPrdTerceiro: TLargeintField;
    QrVSRRMSubPrdCliVenda: TLargeintField;
    QrVSRRMSubPrdMovimID: TLargeintField;
    QrVSRRMSubPrdDataHora: TDateTimeField;
    QrVSRRMSubPrdPallet: TLargeintField;
    QrVSRRMSubPrdGraGruX: TLargeintField;
    QrVSRRMSubPrdPecas: TFloatField;
    QrVSRRMSubPrdPesoKg: TFloatField;
    QrVSRRMSubPrdAreaM2: TFloatField;
    QrVSRRMSubPrdAreaP2: TFloatField;
    QrVSRRMSubPrdValorT: TFloatField;
    QrVSRRMSubPrdSrcMovID: TLargeintField;
    QrVSRRMSubPrdSrcNivel1: TLargeintField;
    QrVSRRMSubPrdSrcNivel2: TLargeintField;
    QrVSRRMSubPrdSrcGGX: TLargeintField;
    QrVSRRMSubPrdSdoVrtPeca: TFloatField;
    QrVSRRMSubPrdSdoVrtPeso: TFloatField;
    QrVSRRMSubPrdSdoVrtArM2: TFloatField;
    QrVSRRMSubPrdObserv: TWideStringField;
    QrVSRRMSubPrdSerieFch: TLargeintField;
    QrVSRRMSubPrdFicha: TLargeintField;
    QrVSRRMSubPrdMisturou: TLargeintField;
    QrVSRRMSubPrdFornecMO: TLargeintField;
    QrVSRRMSubPrdCustoMOKg: TFloatField;
    QrVSRRMSubPrdCustoMOM2: TFloatField;
    QrVSRRMSubPrdCustoMOTot: TFloatField;
    QrVSRRMSubPrdValorMP: TFloatField;
    QrVSRRMSubPrdDstMovID: TLargeintField;
    QrVSRRMSubPrdDstNivel1: TLargeintField;
    QrVSRRMSubPrdDstNivel2: TLargeintField;
    QrVSRRMSubPrdDstGGX: TLargeintField;
    QrVSRRMSubPrdQtdGerPeca: TFloatField;
    QrVSRRMSubPrdQtdGerPeso: TFloatField;
    QrVSRRMSubPrdQtdGerArM2: TFloatField;
    QrVSRRMSubPrdQtdGerArP2: TFloatField;
    QrVSRRMSubPrdQtdAntPeca: TFloatField;
    QrVSRRMSubPrdQtdAntPeso: TFloatField;
    QrVSRRMSubPrdQtdAntArM2: TFloatField;
    QrVSRRMSubPrdQtdAntArP2: TFloatField;
    QrVSRRMSubPrdNotaMPAG: TFloatField;
    QrVSRRMSubPrdNO_PALLET: TWideStringField;
    QrVSRRMSubPrdNO_PRD_TAM_COR: TWideStringField;
    QrVSRRMSubPrdNO_TTW: TWideStringField;
    QrVSRRMSubPrdID_TTW: TLargeintField;
    QrVSRRMSubPrdNO_FORNECE: TWideStringField;
    QrVSRRMSubPrdNO_SerieFch: TWideStringField;
    QrVSRRMSubPrdReqMovEstq: TLargeintField;
    QrVSRRMSubPrdPedItsFin: TLargeintField;
    QrVSRRMSubPrdMarca: TWideStringField;
    QrVSRRMSubPrdStqCenLoc: TLargeintField;
    DsVSRRMSubPrd: TDataSource;
    RemovesubprodutoWBRaspa1: TMenuItem;
    CorrigeMO1: TMenuItem;
    Panel14: TPanel;
    PCRRMDestSub: TPageControl;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet9: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    AtrelamentoNFsdeMO2: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    CobranadeMO1: TMenuItem;
    Recurtimento1: TMenuItem;
    Caleirocurtimento1: TMenuItem;
    Acabamento1: TMenuItem;
    QrVSRRMOriIMEIDtCorrApo: TDateTimeField;
    QrVSRRMDstDtCorrApo: TDateTimeField;
    QrVSRRMDstCusFrtMORet: TFloatField;
    QrVSRRMDstCustoMOPc: TFloatField;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvRVmi: TmySQLQuery;
    QrVSMOEnvRVmiCodigo: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorFrete: TFloatField;
    QrVSMOEnvRVmiLk: TIntegerField;
    QrVSMOEnvRVmiDataCad: TDateField;
    QrVSMOEnvRVmiDataAlt: TDateField;
    QrVSMOEnvRVmiUserCad: TIntegerField;
    QrVSMOEnvRVmiUserAlt: TIntegerField;
    QrVSMOEnvRVmiAlterWeb: TSmallintField;
    QrVSMOEnvRVmiAWServerID: TIntegerField;
    QrVSMOEnvRVmiAWStatSinc: TSmallintField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField;
    QrVSMOEnvRVmiNFEMP_nNF: TIntegerField;
    QrVSMOEnvRVmiValorT: TFloatField;
    DsVSMOEnvRVmi: TDataSource;
    QrVSMOEnvGVmi: TmySQLQuery;
    QrVSMOEnvGVmiCodigo: TIntegerField;
    QrVSMOEnvGVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvGVmiVSMovIts: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiValorFrete: TFloatField;
    QrVSMOEnvGVmiLk: TIntegerField;
    QrVSMOEnvGVmiDataCad: TDateField;
    QrVSMOEnvGVmiDataAlt: TDateField;
    QrVSMOEnvGVmiUserCad: TIntegerField;
    QrVSMOEnvGVmiUserAlt: TIntegerField;
    QrVSMOEnvGVmiAlterWeb: TSmallintField;
    QrVSMOEnvGVmiAWServerID: TIntegerField;
    QrVSMOEnvGVmiAWStatSinc: TSmallintField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiVSMovimCod: TIntegerField;
    DsVSMOEnvGVmi: TDataSource;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_FatID: TIntegerField;
    QrVSMOEnvRetCFTPA_FatNum: TIntegerField;
    QrVSMOEnvRetCFTPA_Empresa: TIntegerField;
    QrVSMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_nItem: TIntegerField;
    QrVSMOEnvRetCFTPA_SerCT: TIntegerField;
    QrVSMOEnvRetCFTPA_nCT: TIntegerField;
    QrVSMOEnvRetCFTPA_Pecas: TFloatField;
    QrVSMOEnvRetCFTPA_PesoKg: TFloatField;
    QrVSMOEnvRetCFTPA_AreaM2: TFloatField;
    QrVSMOEnvRetCFTPA_AreaP2: TFloatField;
    QrVSMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_ValorT: TFloatField;
    DsVSMOEnvRet: TDataSource;
    TsEnvioMO: TTabSheet;
    TsRetornoMO: TTabSheet;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVmi: TdmkDBGridZTO;
    PnRetornoMO: TPanel;
    DBGVSMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    N10: TMenuItem;
    N11: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrVSRRMAtuCusFrtMOEnv: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRRMCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRRMCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSRRMCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSRRMCabBeforeClose(DataSet: TDataSet);
    procedure CabLibera1Click(Sender: TObject);
    procedure QrVSRRMCabCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmProcesso1Click(Sender: TObject);
    procedure QrVSRRMDstBeforeClose(DataSet: TDataSet);
    procedure QrVSRRMDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure otal2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FornecedorMO1Click(Sender: TObject);
    procedure Localdoestoque1Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure OPpreenchida1Click(Sender: TObject);
    procedure CkCpermitirCrustClick(Sender: TObject);
    procedure IrparacadastrodoPallet1Click(Sender: TObject);
    procedure MudarodestinoparaoutraOP1Click(Sender: TObject);
    procedure SbVSCOPCabCadClick(Sender: TObject);
    procedure SbVSCOPCabCpyClick(Sender: TObject);
    procedure Adicionadesclassificado1Click(Sender: TObject);
    procedure QrVSRRMDesclDstAfterScroll(DataSet: TDataSet);
    procedure Removedesclassificado1Click(Sender: TObject);
    procedure Editaea1Click(Sender: TObject);
    procedure Corrigetodasbaixas1Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure ExcluiPesagem1Click(Sender: TObject);
    procedure Reimprimereceita1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure AdicionasubprodutoWBRaspa1Click(Sender: TObject);
    procedure RemovesubprodutoWBRaspa1Click(Sender: TObject);
    procedure CorrigeMO1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure CobranadeMO1Click(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure Recurtimento1Click(Sender: TObject);
    procedure Acabamento1Click(Sender: TObject);
    procedure Caleirocurtimento1Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
  private
    FItsExcluiOriIMEI_Enabled, FItsExcluiOriPallet_Enabled,
    FItsIncluiOri_Enabled, FItsIncluiDst_Enabled, FAtualizando: Boolean;
    //
    function  CalculoCustoOrigemM2(): Double;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSRRMOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSRRMOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSRRMDst(SQLType: TSQLType);
    procedure MostraVSRRMDescl(SQLType: TSQLType);
    procedure MostraVSRRMSubPrd(SQLType: TSQLType; DesabilitaBaixa: Boolean);
    procedure InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX, CliVenda: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSRRMDesclDst(Controle, Pallet: Integer);
    procedure ReopenVSRRMSubPrd(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure PesquisaArtigoJaFeito(Key: Word);
    procedure ReopenEmit();
    procedure RecalculaCusto();
    procedure EmiteReceita(ReceitaSetor: TReceitaTipoSetor);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
    procedure ReabreGGX();
  end;

var
  FmVSRRMCab: TFmVSRRMCab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnVS_PF, AppListas, UnGrade_PF,
  VSRRMOriPall, VSRRMOriIMEI, VSRRMDst,
  //VSRRMDescl, VSRRMSubPrd,
  ModVS, UnPQ_PF, UnEntities, ModVS_CRC, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2Env = True;
  FGerArX2Ret = False;

procedure TFmVSRRMCab.Localdoestoque1Click(Sender: TObject);
begin
  if VS_PF.AlteraVMI_StqCenLoc(QrVSRRMAtuControle.Value,
  QrVSRRMAtuStqCenLoc.Value) then
    LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSRRMCab.MostraVSRRMDescl(SQLType: TSQLType);
begin
//
end;

procedure TFmVSRRMCab.MostraVSRRMDst(SQLType: TSQLType);
begin
  if not FItsIncluiDst_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSRRMDst, FmVSRRMDst, afmoNegarComAviso) then
  begin
    FmVSRRMDst.ImgTipo.SQLType := SQLType;
    FmVSRRMDst.FQrCab := QrVSRRMCab;
    FmVSRRMDst.FDsCab := DsVSRRMCab;
    FmVSRRMDst.FQrIts := QrVSRRMDst;
    //
    //FmVSRRMDst.FDataHora    := QrVSRRMCabDtHrAberto.Value;
    FmVSRRMDst.FEmpresa     := QrVSRRMCabEmpresa.Value;
    FmVSRRMDst.FClientMO    := QrVSRRMAtuClientMO.Value;
    FmVSRRMDst.FValM2       := CalculoCustoOrigemM2();
    //FmVSRRMDst.FFatorIntSrc := QrVSRRMAtuFatorInt.Value;
    FmVSRRMDst.FGraGruXSrc  := QrVSRRMAtuGraGruX.Value;
    FmVSRRMDst.FFornecMO    := QrVSRRMAtuFornecMO.Value;
    //
    FmVSRRMDst.EdCustoMOM2.ValueVariant := QrVSRRMAtuCustoMOM2.Value;
    //
    if SQLType = stIns then
    begin
      FmVSRRMDst.FSdoPecas  := QrVSRRMAtuSdoVrtPeca.Value;
      FmVSRRMDst.FSdoPesoKg := QrVSRRMAtuSdoVrtPeso.Value;
      FmVSRRMDst.FSdoReaM2  := QrVSRRMAtuSdoVrtArM2.Value;
      //
      //FmVSRRMDst.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSRRMDst.QrVSPedIts, QrVSRRMAtuControle.Value,
      QrVSRRMAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSRRMDst.EdPedItsFin.ValueVariant := QrVSRRMAtuPedItsLib.Value;
      FmVSRRMDst.CBPedItsFin.KeyValue     := QrVSRRMAtuPedItsLib.Value;
      //
      //FmVSRRMDst.TPData.Date              := QrVSRRMCabDtHrAberto.Value;
      //FmVSRRMDst.EdHora.ValueVariant      := QrVSRRMCabDtHrAberto.Value;
      //
      FmVSRRMDst.EdGragruX.ValueVariant    := QrVSRRMCabGGXDst.Value;
      FmVSRRMDst.CBGragruX.KeyValue        := QrVSRRMCabGGXDst.Value;
    end else
    begin
      SetLength(FmVSRRMDst.FPallOnEdit, 1);
      FmVSRRMDst.FPallOnEdit[0] := QrVSRRMDstPallet.Value;
      FmVSRRMDst.FSdoPecas  := 0;
      FmVSRRMDst.FSdoPesoKg := 0;
      FmVSRRMDst.FSdoReaM2  := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSRRMDst.QrVSPedIts, QrVSRRMAtuControle.Value,
       QrVSRRMDstControle.Value);
      FmVSRRMDst.EdCtrl1.ValueVariant      := QrVSRRMDstControle.Value;
      //FmVSRRMDst.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSRRMDstMovimCod.Value, QrVSRRMDstMovimTwn.Value, emidEmReprRM,
        //eminEmRRMBxa);
      FmVSRRMDst.EdMovimTwn.ValueVariant     := QrVSRRMDstMovimTwn.Value;
      FmVSRRMDst.EdGragruX.ValueVariant      := QrVSRRMDstGraGruX.Value;
      FmVSRRMDst.CBGragruX.KeyValue          := QrVSRRMDstGraGruX.Value;
      FmVSRRMDst.EdPecas.ValueVariant        := QrVSRRMDstPecas.Value;
      FmVSRRMDst.EdPesoKg.ValueVariant       := QrVSRRMDstPesoKg.Value;
      FmVSRRMDst.EdAreaM2.ValueVariant       := QrVSRRMDstAreaM2.Value;
      FmVSRRMDst.EdAreaP2.ValueVariant       := QrVSRRMDstAreaP2.Value;
      FmVSRRMDst.EdValorT.ValueVariant       := QrVSRRMDstValorT.Value;
      FmVSRRMDst.EdObserv.ValueVariant       := QrVSRRMDstObserv.Value;
      FmVSRRMDst.EdSerieFch.ValueVariant     := QrVSRRMDstSerieFch.Value;
      FmVSRRMDst.CBSerieFch.KeyValue         := QrVSRRMDstSerieFch.Value;
      FmVSRRMDst.EdFicha.ValueVariant        := QrVSRRMDstFicha.Value;
      FmVSRRMDst.EdMarca.ValueVariant        := QrVSRRMDstMarca.Value;
      //FmVSRRMDst.RGMisturou.ItemIndex      := QrVSRRMDstMisturou.Value;
      FmVSRRMDst.EdPedItsFin.ValueVariant    := QrVSRRMDstPedItsFin.Value;
      FmVSRRMDst.CBPedItsFin.KeyValue        := QrVSRRMDstPedItsFin.Value;
      //
      //FmVSRRMDst.TPData.Date               := QrVSRRMDstDataHora.Value;
      //FmVSRRMDst.EdHora.ValueVariant       := QrVSRRMDstDataHora.Value;
      VS_PF.DefineDataHoraOuDtCorrApoCompos(QrVSRRMDstDataHora.Value,
        QrVSRRMDstDtCorrApo.Value,  FmVSRRMDst.TPData, FmVSRRMDst.EdHora);
      //
      FmVSRRMDst.EdPedItsFin.ValueVariant    := QrVSRRMDstPedItsFin.Value;
      FmVSRRMDst.CBPedItsFin.KeyValue        := QrVSRRMDstPedItsFin.Value;
      FmVSRRMDst.EdPallet.ValueVariant       := QrVSRRMDstPallet.Value;
      FmVSRRMDst.CBPallet.KeyValue           := QrVSRRMDstPallet.Value;
      FmVSRRMDst.EdStqCenLoc.ValueVariant    := QrVSRRMDstStqCenLoc.Value;
      FmVSRRMDst.CBStqCenLoc.KeyValue        := QrVSRRMDstStqCenLoc.Value;
      FmVSRRMDst.EdReqMovEstq.ValueVariant   := QrVSRRMDstReqMovEstq.Value;
      FmVSRRMDst.EdCustoMOPc.ValueVariant    := QrVSRRMDstCustoMOPc.Value;
      FmVSRRMDst.EdCustoMOKg.ValueVariant    := QrVSRRMDstCustoMOKg.Value;
      FmVSRRMDst.EdCustoMOM2.ValueVariant    := QrVSRRMDstCustoMOM2.Value;
      FmVSRRMDst.EdCustoMOTot.ValueVariant   := QrVSRRMDstCustoMOTot.Value;
      FmVSRRMDst.EdCusFrtMORet.ValueVariant  := QrVSRRMDstCusFrtMORet.Value;
      FmVSRRMDst.EdValorT.ValueVariant       := QrVSRRMDstValorT.Value;
      //FmVSRRMDst.EdRendimento.ValueVariant := QrVSRRMDstRendimento.Value;
      //
      if QrVSRRMDstSdoVrtPeca.Value < QrVSRRMDstPecas.Value then
      begin
        FmVSRRMDst.EdGraGruX.Enabled        := False;
        FmVSRRMDst.CBGraGruX.Enabled        := False;
      end;
      if QrVSRRMBxa.RecordCount > 0 then
      begin
        FmVSRRMDst.CkBaixa.Checked          := True;
        FmVSRRMDst.EdBxaPecas.ValueVariant  := -QrVSRRMBxaPecas.Value;
        FmVSRRMDst.EdBxaPesoKg.ValueVariant := -QrVSRRMBxaPesoKg.Value;
        FmVSRRMDst.EdBxaAreaM2.ValueVariant := -QrVSRRMBxaAreaM2.Value;
        FmVSRRMDst.EdBxaAreaP2.ValueVariant := -QrVSRRMBxaAreaP2.Value;
        FmVSRRMDst.EdBxaValorT.ValueVariant := -QrVSRRMBxaValorT.Value;
        FmVSRRMDst.EdCtrl2.ValueVariant     := QrVSRRMBxaControle.Value;
        FmVSRRMDst.EdBxaObserv.ValueVariant := QrVSRRMBxaObserv.Value;
      end;
      if QrVSRRMDstSdoVrtPeca.Value < QrVSRRMDstPecas.Value then
      begin
        FmVSRRMDst.EdGraGruX.Enabled  := False;
        FmVSRRMDst.CBGraGruX.Enabled  := False;
        FmVSRRMDst.EdSerieFch.Enabled := False;
        FmVSRRMDst.CBSerieFch.Enabled := False;
        FmVSRRMDst.EdFicha.Enabled    := False;
      end;
      FmVSRRMDst.CalculaCusto();
    end;
    //
    FmVSRRMDst.LaBxaPesoKg.Enabled  := QrVSRRMCabPesoKgINI.Value <> 0;
    FmVSRRMDst.EdBxaPesoKg.Enabled  := QrVSRRMCabPesoKgINI.Value <> 0;
    FmVSRRMDst.LaBxaAreaM2.Enabled  := QrVSRRMCabAreaINIM2.Value <> 0;
    FmVSRRMDst.EdBxaAreaM2.Enabled  := QrVSRRMCabAreaINIM2.Value <> 0;
    FmVSRRMDst.LaBxaAreaP2.Enabled  := QrVSRRMCabAreaINIM2.Value <> 0;
    FmVSRRMDst.EdBxaAreaP2.Enabled  := QrVSRRMCabAreaINIM2.Value <> 0;
    //
    FmVSRRMDst.ShowModal;
    FmVSRRMDst.Destroy;
    //
    VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSRRMCab.MostraVSRRMOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSRRMOriIMEI, FmVSRRMOriIMEI, afmoNegarComAviso) then
  begin
    FmVSRRMOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSRRMOriIMEI.FQrCab          := QrVSRRMCab;
    FmVSRRMOriIMEI.FDsCab          := DsVSRRMCab;
    //FmVSRRMOriIMEI.FVSRRMOriIMEIIMEI   := QrVSRRMOriIMEIIMEI;
    //FmVSRRMOriIMEI.FVSRRMOriIMEIIMEIet := QrVSRRMOriIMEIIMEIet;
    FmVSRRMOriIMEI.FEmpresa        := QrVSRRMCabEmpresa.Value;
    FmVSRRMOriIMEI.FTipoArea       := QrVSRRMCabTipoArea.Value;
    FmVSRRMOriIMEI.FOrigMovimNiv   := QrVSRRMAtuMovimNiv.Value;
    FmVSRRMOriIMEI.FOrigMovimCod   := QrVSRRMAtuMovimCod.Value;
    FmVSRRMOriIMEI.FOrigCodigo     := QrVSRRMAtuCodigo.Value;
    FmVSRRMOriIMEI.FNewGraGruX     := QrVSRRMAtuGraGruX.Value;
    FmVSRRMOriIMEI.FStqCenLoc      := QrVSRRMAtuStqCenLoc.Value;
    FmVSRRMOriIMEI.FFornecMO       := QrVSRRMAtuFornecMO.Value;
    //
    FmVSRRMOriIMEI.EdCodigo.ValueVariant    := QrVSRRMCabCodigo.Value;
    FmVSRRMOriIMEI.EdMovimCod.ValueVariant  := QrVSRRMCabMovimCod.Value;
    FmVSRRMOriIMEI.EdSrcMovID.ValueVariant  := QrVSRRMAtuMovimID.Value;
    FmVSRRMOriIMEI.EdSrcNivel1.ValueVariant := QrVSRRMAtuCodigo.Value;
    FmVSRRMOriIMEI.EdSrcNivel2.ValueVariant := QrVSRRMAtuControle.Value;
    FmVSRRMOriIMEI.EdSrcGGX.ValueVariant    := QrVSRRMAtuGraGruX.Value;
    //
    FmVSRRMOriIMEI.ReopenItensAptos();
    FmVSRRMOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSRRMOriIMEI.FDataHora                := QrVSRRMCabDtHrAberto.Value;
    end else
    begin
      VS_PF.DefineDataHoraOuDtCorrApoCompos(QrVSRRMOriIMEIDataHora.Value,
        QrVSRRMOriIMEIDtCorrApo.Value,  FmVSRRMOriIMEI.FDataHora);
(*
      FmVSRRMOriIMEI.EdControle.ValueVariant := QrVSRRMCabOldControle.Value;
      //
      FmVSRRMOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSRRMCabOldCNPJ_CPF.Value);
      FmVSRRMOriIMEI.EdNomeEmiSac.Text := QrVSRRMCabOldNome.Value;
      FmVSRRMOriIMEI.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSRRMOriIMEI.FParcial := True;
        FmVSRRMOriIMEI.DBG04Estq.Options := FmVSRRMOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSRRMOriIMEI.FParcial := False;
        FmVSRRMOriIMEI.DBG04Estq.Options := FmVSRRMOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSRRMOriIMEI.FParcial := False;
        FmVSRRMOriIMEI.DBG04Estq.Options := FmVSRRMOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    // Deve ser o mesmo
    FmVSRRMOriIMEI.EdGraGruX.ValueVariant := QrVSRRMCabGGXDst.Value; //QrVSRRMAtuGraGruX.Value; 2023-05-19
    FmVSRRMOriIMEI.CBGraGruX.KeyValue     := QrVSRRMCabGGXDst.Value; //QrVSRRMAtuGraGruX.Value; 2023-05-19
    FmVSRRMOriIMEI.ReopenGraGruX();
    FmVSRRMOriIMEI.ReopenItensAptos();
    //
    FmVSRRMOriIMEI.ShowModal;
    FmVSRRMOriIMEI.Destroy;
    RecalculaCusto();
    VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSRRMCab.MostraVSRRMOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if not FItsIncluiOri_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSRRMOriPall, FmVSRRMOriPall, afmoNegarComAviso) then
  begin
    FmVSRRMOriPall.ImgTipo.SQLType := SQLType;
    FmVSRRMOriPall.FQrCab          := QrVSRRMCab;
    FmVSRRMOriPall.FDsCab          := DsVSRRMCab;
    //FmVSRRMOriPall.FVSRRMOriPallIMEI   := QrVSRRMOriPallIMEI;
    //FmVSRRMOriPall.FVSRRMOriPallPallet := QrVSRRMOriPallPallet;
    FmVSRRMOriPall.FEmpresa        := QrVSRRMCabEmpresa.Value;
    FmVSRRMOriPall.FClientMO       := QrVSRRMAtuClientMO.Value;
    FmVSRRMOriPall.FTipoArea       := QrVSRRMCabTipoArea.Value;
    FmVSRRMOriPall.FOrigMovimNiv   := QrVSRRMAtuMovimNiv.Value;
    FmVSRRMOriPall.FOrigMovimCod   := QrVSRRMAtuMovimCod.Value;
    FmVSRRMOriPall.FOrigCodigo     := QrVSRRMAtuCodigo.Value;
    FmVSRRMOriPall.FNewGraGruX     := QrVSRRMAtuGraGruX.Value;
    FmVSRRMOriPall.FStqCenLoc      := QrVSRRMAtuStqCenLoc.Value;
    FmVSRRMOriPall.FFornecMO       := QrVSRRMAtuFornecMO.Value;
    //
    FmVSRRMOriPall.EdCodigo.ValueVariant    := QrVSRRMCabCodigo.Value;
    FmVSRRMOriPall.EdMovimCod.ValueVariant  := QrVSRRMCabMovimCod.Value;
    FmVSRRMOriPall.EdSrcMovID.ValueVariant  := QrVSRRMAtuMovimID.Value;
    FmVSRRMOriPall.EdSrcNivel1.ValueVariant := QrVSRRMAtuCodigo.Value;
    FmVSRRMOriPall.EdSrcNivel2.ValueVariant := QrVSRRMAtuControle.Value;
    FmVSRRMOriPall.EdSrcGGX.ValueVariant    := QrVSRRMAtuGraGruX.Value;
    //
    FmVSRRMOriPall.ReopenItensAptos();
    FmVSRRMOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSRRMOriPall.FDataHora                := QrVSRRMCabDtHrAberto.Value;
    end else
    begin
      VS_PF.DefineDataHoraOuDtCorrApoCompos(QrVSRRMOriIMEIDataHora.Value,
        QrVSRRMOriIMEIDtCorrApo.Value,  FmVSRRMOriPall.FDataHora);
(*
      FmVSRRMOriPall.EdControle.ValueVariant := QrVSRRMCabOldControle.Value;
      //
      FmVSRRMOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSRRMCabOldCNPJ_CPF.Value);
      FmVSRRMOriPall.EdNomeEmiSac.Text := QrVSRRMCabOldNome.Value;
      FmVSRRMOriPall.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSRRMOriPall.FParcial := True;
        FmVSRRMOriPall.DBG04Estq.Options := FmVSRRMOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSRRMOriPall.FParcial := False;
        FmVSRRMOriPall.DBG04Estq.Options := FmVSRRMOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSRRMOriPall.FParcial := False;
        FmVSRRMOriPall.DBG04Estq.Options := FmVSRRMOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    // Deve ser o mesmo!
    FmVSRRMOriPall.EdGraGruX.ValueVariant := QrVSRRMCabGGXDst.Value; //QrVSRRMAtuGraGruX.Value; 2023-05-19
    FmVSRRMOriPall.CBGraGruX.KeyValue     := QrVSRRMCabGGXDst.Value; //QrVSRRMAtuGraGruX.Value; 2023-05-19
    FmVSRRMOriPall.ReopenGraGruX();
    FmVSRRMOriPall.ReopenItensAptos();
    //
    FmVSRRMOriPall.ShowModal;
    FmVSRRMOriPall.Destroy;
    RecalculaCusto();
    VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSRRMCab.MostraVSRRMSubPrd(SQLType: TSQLType; DesabilitaBaixa: Boolean);
begin
//
end;

procedure TFmVSRRMCab.MudarodestinoparaoutraOP1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de OP';
  Prompt = 'Informe o n�mero da OP';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  OP, Codigo, MovimCod, Controle, Ctrl1: Integer;
  Resp: Variant;
  Qry: TmySQLQuery;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT cab.Codigo, cab.Nome Descricao ',
    'FROM vsrrmcab cab ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    OP := Resp;
    if OP <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * FROM vsrrmcab ',
        'WHERE Codigo=' + Geral.FF0(OP),
        '']);
        if Qry.recordCount > 0 then
        begin
          Codigo   := Qry.FieldByName('Codigo').AsInteger;
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          //
          Controle := QrVSRRMBxaControle.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'Codigo', 'MovimCod'], [
          'Controle'], [
          Codigo, MovimCod], [
          Controle], True) then
          begin
            Ctrl1 := Controle;
            Controle := QrVSRRMDstControle.Value;
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Codigo', 'MovimCod'], [
            'Controle'], [
            Codigo, MovimCod], [
            Controle], True) then
            begin
              VS_PF.AtualizaTotaisVSRRMCab(MovimCod);
              VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
              VS_PF.AtualizaNF_IMEI(QrVSRRMCabMovimCod.Value, Ctrl1);
              VS_PF.AtualizaNF_IMEI(QrVSRRMCabMovimCod.Value, Controle);
              //
              LocCod(QrVSRRMCabMovimCod.Value, QrVSRRMCabMovimCod.Value);
            end;
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

procedure TFmVSRRMCab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = True;
var
  Codigo: Integer;
begin
  PCRRMOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSRRMCabMovimCod.Value,
  QrVSRRMCabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSRRMCabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
end;

procedure TFmVSRRMCab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
  PCRRMOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
end;

procedure TFmVSRRMCab.OPpreenchida1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSRRMCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSRRMCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSRRMAtuControle.Value], viikOPPreenchida, QrVSRRMCabLPFMO.Value,
    NFeRem, QrVSRRMCab);
end;

procedure TFmVSRRMCab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
  VS_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmReprRM], True, False, '', 0);
end;

procedure TFmVSRRMCab.otal1Click(Sender: TObject);
begin
  MostraVSRRMOriPall(stIns, ptTotal);
end;

procedure TFmVSRRMCab.otal2Click(Sender: TObject);
begin
  MostraVSRRMOriIMEI(stIns, ptTotal);
end;

procedure TFmVSRRMCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSRRMCab.Parcial1Click(Sender: TObject);
begin
  MostraVSRRMOriPall(stIns, ptParcial);
end;

procedure TFmVSRRMCab.Parcial2Click(Sender: TObject);
begin
  MostraVSRRMOriIMEI(stIns, ptParcial);
end;

procedure TFmVSRRMCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRRMCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSRRMCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEC(emidEmReprRM);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSRRMCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEI(emidEmReprRM);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSRRMCab.PesquisaArtigoJaFeito(Key: Word);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  if Key = VK_F4 then
  begin
    Localizou := VS_PF.MostraFormVSRMPPsq(emidEmReprRM, [eminEmRRMInn], stCpy,
    Codigo, Controle, False);
    //
    if (Controle <> 0) and (DmModVS.QrIMEI.State <> dsInactive) then
    begin
      EdGraGruX.ValueVariant   := DmModVS.QrIMEIGraGruX.Value;
      CBGraGruX.KeyValue       := DmModVS.QrIMEIGraGruX.Value;
      EdFornecMO.ValueVariant  := DmModVS.QrIMEIFornecMO.Value;
      CBFornecMO.KeyValue      := DmModVS.QrIMEIFornecMO.Value;
      EdStqCenLoc.ValueVariant := DmModVS.QrIMEIStqCenLoc.Value;
      CBStqCenLoc.KeyValue     := DmModVS.QrIMEIStqCenLoc.Value;
      EdCustoMOM2.ValueVariant := DmModVS.QrIMEICustoMOM2.Value;
      EdCliente.ValueVariant   := DmModVS.QrIMEICliVenda.Value;
      CBCliente.KeyValue       := DmModVS.QrIMEICliVenda.Value;
    end;
  end;
end;

procedure TFmVSRRMCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSRRMCab);
  MyObjects.HabilitaMenuItemCabDelC1I4(CabExclui1, QrVSRRMCab, QrVSRRMOriIMEI,
    QrVSRRMOriPallet, QrVSRRMDst, QrEmit);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrVSRRMCab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrVSRRMCab);
  if QrVSRRMCabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrVSRRMCabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
  //
  //VS_PF.HabilitaMenuItensVSAberto(QrVSRRMCab, QrVSRRMCabDtHrAberto.Value, [CabAltera1, CabExclui1]);
end;

procedure TFmVSRRMCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSRRMCab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSRRMDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSRRMDst);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSRRMDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSRRMDst);

  MyObjects.HabilitaMenuItemItsIns(Adicionadesclassificado1, QrVSRRMCab);
  MyObjects.HabilitaMenuItemItsDel(Removedesclassificado1, QrVSRRMDesclDst);

(* Movidos para os click dos itens de menu!!!
  if (QrVSRRMCabDtHrLibOpe.Value > 2) or (QrVSRRMCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
*)
  VS_PF.HabilitaComposVSAtivo(QrVSRRMDstID_TTW.Value, [ItsExcluiDst]);
  MyObjects.HabilitaMenuItemItsUpd(Editaea1, QrVSRRMBxa);
  Corrigetodasbaixas1.Enabled :=
    (QrVSRRMCab.State <> dsInactive) and (QrVSRRMCabAreaSdoM2.Value < 0);
 //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSRRMDst);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvRet);
  //
  if (QrVSRRMCabDtHrLibOpe.Value > 2) or (QrVSRRMCabDtHrFimOpe.Value > 2) then
    ItsIncluiDst.Enabled := False;
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
end;

procedure TFmVSRRMCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSRRMCab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSRRMOriPallet);
  if (QrVSRRMCabDtHrLibOpe.Value > 2) or (QrVSRRMCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCRRMOri.ActivePageIndex = 1);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSRRMOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_PF.HabilitaComposVSAtivo(QrVSRRMOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  FItsExcluiOriPallet_Enabled := ItsExcluiOriPallet.Enabled;
  ItsExcluiOriPallet.Enabled  := True;
  case PCRRMOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
end;

procedure TFmVSRRMCab.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSRRMCab);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSRRMCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSRRMCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRRMCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRRMCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsrrmcab';
  VAR_GOTOMYSQLTABLE := QrVSRRMCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vcc.Nome NO_VSCOPCab, ');
  VAR_SQLx.Add('IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcP2) AreaINIP2,');
  VAR_SQLx.Add('IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('egr.Nome NO_EmitGru, ');
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vsrrmcab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  VAR_SQLx.Add('LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab');
  VAR_SQLx.Add('LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSRRMCab.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSRRMCab.EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSRRMCab.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSRRMCab.EdGraGruXRedefinido(Sender: TObject);
begin
  //EdGGXDst.ValueVariant := EdGraGruX.ValueVariant;
  //CBGGXDst.KeyValue := EdGraGruX.ValueVariant;
end;

procedure TFmVSRRMCab.Editaea1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  PCRRMDst.ActivePageIndex := 0;
  PCRRMDestSub.ActivePageIndex := 0;
  //
  if VS_PF.AlteraVMI_AreaM2(QrVSRRMBxaMovimID.Value,
  QrVSRRMBxaMovimNiv.Value, QrVSRRMBxaControle.Value,
  QrVSRRMBxaAreaM2.Value, siNegativo) then
  begin
    VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrVSRRMAtuControle.Value,
    QrVSRRMAtuMovimID.Value, QrVSRRMAtuMovimNiv.Value);
    LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
  end;
end;

procedure TFmVSRRMCab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCRRMOri.ActivePageIndex := 3;
end;

procedure TFmVSRRMCab.EmitePesagem2Click(Sender: TObject);
begin
  PCRRMOri.ActivePageIndex := 2;
end;

procedure TFmVSRRMCab.EmiteReceita(ReceitaSetor: TReceitaTipoSetor);
begin
  case ReceitaSetor of
    rectipsetrRibeira:
      PQ_PF.MostraFormVSFormulasImp_BH(QrVSRRMCabMovimCod.Value,
        QrVSRRMAtuControle.Value, QrVSRRMCabTemIMEIMrt.Value,
        //QrVSRRMCabEmitGru.Value,
        eminEmRRMInn, eminSorcRRM, recribsetNaoAplic);
    rectipsetrRecurtimento:
      PQ_PF.MostraFormVSFormulasImp_WE(QrVSRRMCabMovimCod.Value,
        QrVSRRMAtuControle.Value, QrVSRRMCabTemIMEIMrt.Value,
        QrVSRRMCabEmitGru.Value,
        eminEmRRMInn, eminSorcRRM);
    rectipsetrAcabamento:
      PQ_PF.MostraFormVSFormulasImp_FI(QrVSRRMCabMovimCod.Value,
        QrVSRRMAtuControle.Value, QrVSRRMCabTemIMEIMrt.Value,
        QrVSRRMCabEmitGru.Value,
        eminEmRRMInn, eminSorcRRM);
    else Geral.MB_Aviso('Setor sem tipo de receita definida!');
  end;
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSRRMCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSRRMCabMovimCod.Value);
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
  //
  LocCod(QrVSRRMCabCodigo.Value,QrVSRRMCabCodigo.Value);
  //
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
  DmModVS_CRC.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
  //
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
  //
end;

procedure TFmVSRRMCab.Excluigrupo1Click(Sender: TObject);
begin
  PCRRMOri.ActivePageIndex := 3;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
end;

procedure TFmVSRRMCab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
  PCRRMOri.ActivePageIndex := 3;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
end;

procedure TFmVSRRMCab.ExcluiPesagem1Click(Sender: TObject);
begin
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, PB1) then
    LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSRRMCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.CabLibera1Click(Sender: TObject);
var
  Codigo: Integer;
begin
(*
  Codigo := QrVSRRMCabCodigo.Value;
  //colocar metragem e encerrar!
  if DBCheck.CriaFm(TFmVSGerArtEnc, FmVSGerArtEnc, afmoNegarComAviso) then
  begin
    FmVSGerArtEnc.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtEnc.EdCodigo.ValueVariant := Codigo;
    FmVSGerArtEnc.EdMovimCod.ValueVariant := QrVSRRMAtuMovimCod.Value;
    FmVSGerArtEnc.EdControle.ValueVariant := QrVSRRMAtuControle.Value;
    //
    FmVSGerArtEnc.EdPecas.ValueVariant      := QrVSRRMAtuPecas.Value;
    FmVSGerArtEnc.EdAreaM2.ValueVariant     := QrVSRRMAtuAreaM2.Value;
    FmVSGerArtEnc.EdAreaP2.ValueVariant     := QrVSRRMAtuAreaP2.Value;
    FmVSGerArtEnc.EdCustoMOKg.ValueVariant  := QrVSRRMAtuCustoMOKg.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
    FmVSGerArtEnc.EdCustoMOTot.ValueVariant := QrVSRRMAtuCustoMOTot.Value;
    FmVSGerArtEnc.EdValorMP.ValueVariant    := QrVSRRMAtuValorMP.Value;
    FmVSGerArtEnc.EdValorT.ValueVariant     := QrVSRRMAtuValorT.Value;

    //
    FmVSGerArtEnc.EdPecasMan.ValueVariant      := QrVSRRMCabPecasMan.Value;
    FmVSGerArtEnc.EdAreaManM2.ValueVariant     := QrVSRRMCabAreaManM2.Value;
    FmVSGerArtEnc.EdAreaManP2.ValueVariant     := QrVSRRMCabAreaManP2.Value;
    //
    FmVSGerArtEnc.EdCustoManMOKg.ValueVariant  := QrVSRRMCabCustoManMOKg.Value;
    FmVSGerArtEnc.EdCustoManMOTot.ValueVariant := QrVSRRMCabCustoManMOTot.Value;
    FmVSGerArtEnc.EdValorManMP.ValueVariant    := QrVSRRMCabValorManMP.Value;
    FmVSGerArtEnc.EdValorManT.ValueVariant     := QrVSRRMCabValorManT.Value;
    //
    FmVSGerArtEnc.ShowModal;
    //
    LocCod(Codigo, Codigo);
    FmVSGerArtEnc.Destroy;
  end;
*)
end;

procedure TFmVSRRMCab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSRRMCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsrrmcab', False, [
  'DtHrLibOpe'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

function TFmVSRRMCab.CalculoCustoOrigemM2: Double;
begin
  Result := VS_CRC_PF.CalculoValorOrigemM2(QrVSRRMAtuAreaM2.Value,
    QrVSRRMAtuCustoPQ.Value, QrVSRRMAtuValorMP.Value,
    QrVSRRMAtuCusFrtMOEnv.Value);
end;

procedure TFmVSRRMCab.Caleirocurtimento1Click(Sender: TObject);
begin
  EmiteReceita(TReceitaTipoSetor.rectipsetrRibeira);
end;

procedure TFmVSRRMCab.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSRRMCab.CkCpermitirCrustClick(Sender: TObject);
begin
  ReabreGGX();
end;

procedure TFmVSRRMCab.Cliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSRRMCabCodigo.Value;
  if VS_PF.AlteraVMI_CliVenda(QrVSRRMAtuControle.Value,
  QrVSRRMAtuCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsrrmcab', False, [
    'Cliente'], [
    'Codigo'], [
    QrVSRRMAtuCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSRRMCab.CobranadeMO1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpMOEnvRet();
end;

procedure TFmVSRRMCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSRRMAtuControle.Value;
  MovimCod    := QrVSRRMCabMovimCod.Value;
  Codigo      := QrVSRRMCabCodigo.Value;
  MovimNivSrc := eminSorcRRM;
  MovimNivDst := eminDestRRM;
  VS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSRRMCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSRRMCab.CorrigeMO1Click(Sender: TObject);
begin
  //VS_PF.MostraFormVSMOPWEGer(); Fazer????
end;

procedure TFmVSRRMCab.Corrigetodasbaixas1Click(Sender: TObject);
var
  Controle: Integer;
  Fator, AreaM2, AreaP2, Soma: Double;
begin
  if QrVSRRMCabAreaINIM2.Value > 0 then
  begin
    Soma  := 0;
    Fator := QrVSRRMCabAreaDstM2.Value / QrVSRRMCabAreaINIM2.Value;
    QrVSRRMDst.First;
    while not QrVSRRMDst.Eof do
    begin
      Controle := QrVSRRMBxaControle.Value;
      if QrVSRRMDst.RecNo = QrVSRRMDst.RecordCount then
        AreaM2 := - (QrVSRRMCabAreaINIM2.Value + Soma)
      else
        AreaM2 := QrVSRRMBxaAreaM2.Value * Fator;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'AreaM2', 'AreaP2'], [
      'Controle'], [
      AreaM2, AreaP2], [
      Controle], True) then ;
        //VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
      //
      Soma := Soma + AreaM2;
      //
      QrVSRRMDst.Next;
    end;
    VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrVSRRMAtuControle.Value,
    QrVSRRMAtuMovimID.Value, QrVSRRMAtuMovimNiv.Value);
    LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
  end else
    Geral.MB_Aviso('�rea inicial inv�lida!');
end;

procedure TFmVSRRMCab.CorrigirFornecedor1Click(Sender: TObject);
begin
  VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSRRMCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSRRMCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSRRMAtuControle.Value], viikOrdemOperacao,
    QrVSRRMCabLPFMO.Value, NFeRem, QrVSRRMCab);
end;

procedure TFmVSRRMCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSRRMOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, QrVSRRMCabSerieRem.Value, QrVSRRMCabNFeRem.Value);
  //
  LocCod(QrVSRRMCabCodigo.Value,QrVSRRMCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.IncluiAtrelamento2Click(Sender: TObject);
var
  MovimCod: Integer;
begin
{$IfDef sAllVS}
  //PCRRMOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsRetornoMO;
  //
  MovimCod := QrVSRRMCabMovimCod.Value;
  //
  VS_PF.MostraFormVSMOEnvRet(stIns, QrVSRRMAtu, QrVSRRMDst, QrVSRRMBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, MovimCod, 0, 0, siPositivo,
  FGerArX2Ret);
  //
  LocCod(QrVSRRMCabCodigo.Value,QrVSRRMCabCodigo.Value);
  //
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSRRMOriIMEIControle.Value,
    QrVSRRMOriIMEIReqMovEstq.Value, QrVSRRMOriIMEI);
end;

procedure TFmVSRRMCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSRRMDstControle.Value,
    QrVSRRMDstReqMovEstq.Value, QrVSRRMDst);
end;

procedure TFmVSRRMCab.InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
GraGruX, CliVenda: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOM2, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmReprRM;
  MovimNiv       := eminEmRRMInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei087(*Gera��o de couro que est� em reprocesso/reparo*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
    VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
    if GBVSPedIts.Visible then
      VS_PF.AtualizaVSPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;

procedure TFmVSRRMCab.IrparacadastrodoPallet1Click(Sender: TObject);
begin
  case PCRRMOri.ActivePageIndex of
    0: VS_PF.MostraFormVSPallet(QrVSRRMOriPalletPallet.Value);
    1: VS_PF.MostraFormVSPallet(QrVSRRMOriIMEIPallet.Value);
    else Geral.MB_Erro(
      '"PCRRMOri.ActivePageIndex" n�o implementado em "FmVSRRMCab.IrparacadastrodoPallet"');
  end;
end;

procedure TFmVSRRMCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrVSRRMDstPallet.Value);
end;

procedure TFmVSRRMCab.ItsAlteraDstClick(Sender: TObject);
begin
  if (QrVSRRMCabDtHrLibOpe.Value > 2) or (QrVSRRMCabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
  MostraVSRRMDst(stUpd);
end;

procedure TFmVSRRMCab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraVSGArtOri(stUpd);
end;

procedure TFmVSRRMCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSRRMCabCodigo.Value;
  VS_PF.ExcluiCabEIMEI_OpeCab(QrVSRRMCabMovimCod.Value,
    QrVSRRMAtuControle.Value, emidEmReprRM, TEstqMotivDel.emtdWetCurti161);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSRRMCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSRRMCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRRMCab.ReabreGGX();
begin
  // ni 2023-05-19
  //VS_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY<>0');
  VS_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY=7168');
  // fim 2023-05-19
  VS_PF.AbreGraGruXY(QrGGXDst, 'AND ggx.GragruY<>0');
end;

procedure TFmVSRRMCab.RecalculaCusto();
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSRRMCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSRRMCabMovimCod.Value);
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.Recalculacusto1Click(Sender: TObject);
begin
  RecalculaCusto();
end;

procedure TFmVSRRMCab.Recurtimento1Click(Sender: TObject);
begin
  EmiteReceita(TReceitaTipoSetor.rectipsetrRecurtimento);
end;

procedure TFmVSRRMCab.Reimprimereceita1Click(Sender: TObject);
begin
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TReceitaTipoSetor.rectipsetrRibeira, PB1);
end;

procedure TFmVSRRMCab.Removedesclassificado1Click(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  PCRRMDst.ActivePageIndex := 1;
  if Geral.MB_Pergunta('Confirma a exclus�o do item desclassificado?') = ID_YES then
  begin
    Codigo   := QrVSRRMCabCodigo.Value;
    CtrlDel  := QrVSRRMDesclDstControle.Value;
    CtrlDst  := QrVSRRMDesclDstSrcNivel2.Value;
    MovimNiv := QrVSRRMAtuMovimNiv.Value;
    MovimCod := QrVSRRMAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_PF.AtualizaSaldoIMEI(QrVSRRMAtuControle.Value, True);
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSRRMDesclDstMovimCod.Value, QrVSRRMDesclDstMovimTwn.Value, emidDesclasse,
        eminSorcClass);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSRRMDesclDst,
        TIntegerField(QrVSRRMDesclDstControle), QrVSRRMDesclDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
      VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
      LocCod(Codigo, Codigo);
      ReopenVSRRMDesclDst(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSRRMCab.RemovesubprodutoWBRaspa1Click(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  PCRRMDst.ActivePageIndex := 2;
  if Geral.MB_Pergunta('Confirma a exclus�o do subproduto?') = ID_YES then
  begin
    Codigo   := QrVSRRMCabCodigo.Value;
    CtrlDel  := QrVSRRMSubPrdControle.Value;
    CtrlDst  := QrVSRRMSubPrdSrcNivel2.Value;
    MovimNiv := QrVSRRMAtuMovimNiv.Value;
    MovimCod := QrVSRRMAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_PF.AtualizaSaldoIMEI(QrVSRRMAtuControle.Value, True);
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSRRMSubPrd,
        TIntegerField(QrVSRRMSubPrdControle), QrVSRRMSubPrdControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
      VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
      LocCod(Codigo, Codigo);
      ReopenVSRRMSubPrd(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSRRMCab.ReopenEmit();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT * ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(QrVSRRMCabMovimCod.Value),
  '']);
end;

procedure TFmVSRRMCab.ReopenVSRRMDesclDst(Controle, Pallet: Integer);
begin
  VS_PF.ReopenVSXxxExtra(QrVSRRMDesclDst, QrVSRRMCabCodigo.Value,
    (*QrVSRRMAtuControle.Value,*) QrVSRRMCabTemIMEIMrt.Value,
    emidDesclasse, emidEmReprRM, eminDestClass);
end;

procedure TFmVSRRMCab.ReopenVSRRMSubPrd(Controle, Pallet: Integer);
begin
  VS_PF.ReopenVSXxxExtra(QrVSRRMSubPrd, QrVSRRMCabCodigo.Value,
    (*QrVSRRMAtuControle.Value,*) QrVSRRMCabTemIMEIMrt.Value,
    emidGeraSubProd, emidEmReprRM, eminSemNiv);
end;

procedure TFmVSRRMCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if (QrVSRRMCabDtHrLibOpe.Value > 2) or (QrVSRRMCabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
(*
  if QrVSRRMAtuSdoVrtPeca.Value < QrVSRRMAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSRRMCabCodigo.Value;
    CtrlDel  := QrVSRRMDstControle.Value;
    CtrlDst  := QrVSRRMDstSrcNivel2.Value;
    MovimNiv := QrVSRRMAtuMovimNiv.Value;
    MovimCod := QrVSRRMAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSRRMDstMovimCod.Value, QrVSRRMDstMovimTwn.Value, emidEmReprRM,
        eminEmRRMBxa);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSRRMDst,
        TIntegerField(QrVSRRMDstControle), QrVSRRMDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
      VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_PF.ReopenVSOpePrcDst(QrVSRRMDst, QrVSRRMCabMovimCod.Value, CtrlNxt,
      QrVSRRMCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestRRM);
    end;
  end;
end;

procedure TFmVSRRMCab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
(*
  if QrVSRRMAtuSdoVrtPeca.Value < QrVSRRMAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de origem cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrVSRRMCabCodigo.Value;
    CtrlDel   := QrVSRRMOriIMEIControle.Value;
    CtrlOri   := QrVSRRMOriIMEISrcNivel2.Value;
    MovimNiv  := QrVSRRMAtuMovimNiv.Value;
    MovimCod  := QrVSRRMAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
      VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
     //
     CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSRRMOriIMEI,
        TIntegerField(QrVSRRMOriIMEIControle), QrVSRRMOriIMEIControle.Value);
      //
      RecalculaCusto();
      VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_PF.ReopenVSXxxOris(QrVSRRMCab, QrVSRRMOriIMEI, QrVSRRMOriPallet,
      CtrlNxt, 0, eminSorcRRM);
    end;
  end;
end;

procedure TFmVSRRMCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if not FItsExcluiOriPallet_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if MyObjects.FIC(QrVSRRMOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrVSRRMAtuSdoVrtPeca.Value < QrVSRRMAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSRRMOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSRRMOriIMEI.First;
    while not QrVSRRMOriIMEI.Eof do
    begin
      if QrVSRRMOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSRRMOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSRRMOriIMEI.First;
        while not QrVSRRMOriIMEI.Eof do
        begin
          if QrVSRRMOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSRRMCabCodigo.Value;
            CtrlDel   := QrVSRRMOriIMEIControle.Value;
            CtrlOri   := QrVSRRMOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSRRMAtuMovimNiv.Value;
            MovimCod  := QrVSRRMAtuMovimCod.Value;
            //
            if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti161),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_PF.AtualizaTotaisVSXxxCab('vsrrmcab', MovimCod);
              VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
            end;
            //
          end;
          QrVSRRMOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSRRMOriIMEI,
      TIntegerField(QrVSRRMOriIMEIControle), QrVSRRMOriIMEIControle.Value);
      //
      RecalculaCusto();
      VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_PF.ReopenVSXxxOris(QrVSRRMCab, QrVSRRMOriIMEI, QrVSRRMOriPallet,
      CtrlNxt, 0, eminSorcRRM);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSRRMCab.DefineONomeDoForm;
begin
end;

procedure TFmVSRRMCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRRMCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRRMCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRRMCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRRMCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRRMCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRRMCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRRMCabCodigo.Value;
  Close;
end;

procedure TFmVSRRMCab.ItsIncluiDstClick(Sender: TObject);
begin
  MostraVSRRMDst(stIns);
end;

procedure TFmVSRRMCab.CabAltera1Click(Sender: TObject);
var
  Empresa(*, VSPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSRRMCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsrrmcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSRRMCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrVSRRMAtuClientMO.Value;
  CBClientMO.KeyValue        := QrVSRRMAtuClientMO.Value;
  //
  EdCliente.ValueVariant    := QrVSRRMCabCliente.Value;
  CBCliente.KeyValue        := QrVSRRMCabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrVSRRMCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSRRMCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSRRMCabLPFMO.Value;
  //
  EdControle.ValueVariant   := QrVSRRMAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSRRMAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSRRMAtuGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrVSRRMAtuCustoMOTot.Value;
  EdCustoMOM2.ValueVariant  := QrVSRRMAtuCustoMOM2.Value;
  EdPecas.ValueVariant      := QrVSRRMAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSRRMAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSRRMAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSRRMAtuObserv.Value;
  //
  VS_PF.ReopenPedItsXXX(QrVSPedIts, QrVSRRMAtuControle.Value, QrVSRRMAtuControle.Value);
  Habilita := QrVSRRMAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSRRMAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSRRMAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSRRMAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSRRMAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSRRMAtuReqMovEstq.Value;
  //
  EdFornecMO.ValueVariant   := QrVSRRMAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSRRMAtuFornecMO.Value;
  //
  EdClientMO.ValueVariant   := QrVSRRMAtuClientMO.Value;
  CBClientMO.KeyValue       := QrVSRRMAtuClientMO.Value;
  //
  EdVSCOPCab.ValueVariant   := QrVSRRMCabVSCOPCab.Value;
  CBVSCOPCab.KeyValue       := QrVSRRMCabVSCOPCab.Value;
  //
  EdEmitGru.ValueVariant    := QrVSRRMCabEmitGru.Value;
  CBEmitGru.KeyValue        := QrVSRRMCabEmitGru.Value;
  //
  EdVSCOPCab.SetFocus;
  //
  Habilita := (QrVSRRMOriIMEI.RecordCount = 0) and (QrVSRRMDst.RecordCount = 0);
  begin
    CkCpermitirCrust.Enabled := Habilita;
    LaGGXDst.Enabled := Habilita;
    EdGGXDst.Enabled := Habilita;
    CBGGXDst.Enabled := Habilita;
  end;
end;

procedure TFmVSRRMCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, MovimNiv,
  Forma, Quant, Cliente, CliVenda, StqCenLoc, NFeRem, GGXDst, SerieRem,
  EmitGru: Integer;
  Nome, DtHrAberto, DataHora, LPFMO: String;
  SQLType: TSQLType;
  ExigeNFe: Boolean;
  CustoMOM2: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  EmitGru        := EdEmitGru.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst, 'Informe o Artigo a ser produzido!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  ExigeNFe := (FornecMO <> 0) and (FornecMO <> ClientMO) and (NFeRem = 0);
  if MyObjects.FIC(ExigeNFe, EdNFeRem, 'Defina a NFe de remessa!') then
    Exit;
  //
  if not VS_PF.ValidaCampoNF(9, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsrrmcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsrrmcab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', 'GGXDst', 'EmitGru'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, GGXDst, EmitGru], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidEmReprRM, Codigo)
    else
    begin
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
      //
      CustoMOM2 := EdCustoMOM2.ValueVariant;
      MovimNiv  := Integer(eminEmRRMInn);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'CustoMOM2'], ['MovimCod', 'MovimNiv'], [
      CustoMOM2], [MovimCod, MovimNiv], True);
    end;
    InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO, GraGruX, Cliente, DtHrAberto);

    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSRRMOriIMEI.RecordCount = 0) then
    begin
      //MostraVSRRMOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      'Pallet Total', 'Pallet Parcial',
      'IME-I Total', 'IME-I Parcial'], -1);
(*
      case Forma of
        0:
        begin
          Quant := MyObjects.SelRadioGroup('Quantidade na sele��o',
          'Escolha a quantidade de sele��o', ['Parcial', 'Total'], -1);
          case Quant of
            0: MostraVSRRMOriPall(stIns, ptParcial);
            1: MostraVSRRMOriPall(stIns, ptTotal);
          end;
        end;
        1:
        begin
          Quant := MyObjects.SelRadioGroup('Quantidade na sele��o',
          'Escolha a quantidade de sele��o', ['Parcial', 'Total'], -1);
          case Forma of
            0: MostraVSRRMOriIMEI(stIns, ptParcial);
            1: MostraVSRRMOriIMEI(stIns, ptTotal);
          end;
        end;
      end;
*)
      FItsIncluiOri_Enabled := True;
      case Forma of
        0: MostraVSRRMOriPall(stIns, ptTotal);
        1: MostraVSRRMOriPall(stIns, ptParcial);
        2: MostraVSRRMOriIMEI(stIns, ptTotal);
        3: MostraVSRRMOriIMEI(stIns, ptParcial);
      end;
    end;
    VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
    VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
  end;
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSRRMCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSRRMCabMovimCod.Value);
  LocCod(Codigo, Codigo);
end;

procedure TFmVSRRMCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsrrmcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsrrmcab', 'Codigo');
end;

procedure TFmVSRRMCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSRRMCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSRRMCab.BtPesagemClick(Sender: TObject);
begin
  if not PCRRMOri.ActivePageIndex in [2,3] then
    PCRRMOri.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSRRMCab.Acabamento1Click(Sender: TObject);
begin
  EmiteReceita(TReceitaTipoSetor.rectipsetrAcabamento);
end;

procedure TFmVSRRMCab.Adicionadesclassificado1Click(Sender: TObject);
begin
  MostraVSRRMDescl(stIns);
end;

procedure TFmVSRRMCab.AdicionasubprodutoWBRaspa1Click(Sender: TObject);
const
  DesabilitaBaixa = True;
begin
  MostraVSRRMSubPrd(stIns, DesabilitaBaixa);
end;

procedure TFmVSRRMCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCRRMOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSRRMOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, 0, 0);
  //
  LocCod(QrVSRRMCabCodigo.Value,QrVSRRMCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCRRMOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsRetornoMO;
  //
  VS_PF.MostraFormVSMOEnvRet(stUpd, QrVSRRMAtu, QrVSRRMDst, QrVSRRMBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, 0, 0, 0, siPositivo, FGerArX2Env);
  //
  LocCod(QrVSRRMCabCodigo.Value,QrVSRRMCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.AtualizaestoqueEmProcesso1Click(Sender: TObject);
begin
  VS_PF.AtualizaTotaisVSRRMCab(QrVSRRMCabMovimCod.Value);
// Parei Aqui 2014-12-14
  LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.AtualizaFornecedoresDeDestino;
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsrrmcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSRRMCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_PF.AtualizaFornecedorRRM(QrVSRRMCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSRRMCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSRRMCabMovimCod.Value, TEstqMovimID.emidEmReprRM, [eminSorcRRM],
  [eminEmRRMInn, eminDestRRM, eminEmRRMBxa]);
  //
  if QrVSRRMDesclDst.RecordCount > 0 then
    DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
    QrVSRRMDesclDstMovimCod.Value, TEstqMovimID.emidDesclasse, [eminSorcClass],
    [eminDestClass]);
end;

procedure TFmVSRRMCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSRRMCab, QrVSRRMCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSRRMCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  //PnDst.Align := alClient;
  PCRRMDst.Align := alClient;
  PCRRMDestSub.ActivePageIndex := 1;
  CriaOForm;
  FSeq := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  VS_PF.ReopenVSCOPCab(QrVSCOPCab, emidEmReprRM);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  ReabreGGX();
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCRRMOri, VAR_PC_FRETE_VS_NAME, 0);
  MyObjects.DefinePageControlActivePageByName(PCRRMDst, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSRRMCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSRRMCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSRRMCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSRRMCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSRRMCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSRRMCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRRMCab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSRRMCab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSRRMCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVmi(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.Value, 0);
end;

procedure TFmVSRRMCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVmi.Close;
end;

procedure TFmVSRRMCab.QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi, QrVSMOEnvRetCodigo.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi, QrVSMOEnvRetCodigo.Value, 0);
end;

procedure TFmVSRRMCab.QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvGVmi.Close;
  QrVSMOEnvRVmi.Close;
end;

procedure TFmVSRRMCab.QrVSRRMCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRRMCab.QrVSRRMCabAfterScroll(DataSet: TDataSet);
begin
  // Em processo
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSRRMAtu, QrVSRRMCabMovimCod.Value, 0,
  QrVSRRMCabTemIMEIMrt.Value, eminEmRRMInn);
  // materia-prima
  VS_PF.ReopenVSXxxOris(QrVSRRMCab, QrVSRRMOriIMEI, QrVSRRMOriPallet, 0, 0,
  eminSorcRRM);
  ReopenVSRRMDesclDst(0, 0);
  // Semi acabado
  VS_PF.ReopenVSOpePrcDst(QrVSRRMDst, QrVSRRMCabMovimCod.Value, 0,
  QrVSRRMCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestRRM);
  // Raspa WB
  ReopenVSRRMSubPrd(0,0);
  // Baixa forcada
  VS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(QrForcados, 0, QrVSRRMCabCodigo.Value,
    QrVSRRMAtuControle.Value, QrVSRRMCabTemIMEIMrt.Value, emidEmReprRM);
  // Insumos
  ReopenEmit();
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSRRMCabMovimCod.Value, 0);
{$IfDef sAllVS}
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSRRMCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet, QrVSRRMCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRRMCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSRRMCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSRRMCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidEmReprRM, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //ReopenVSRRMIts(Controle);
  end;
end;

procedure TFmVSRRMCab.SbVSCOPCabCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSCOPCab(0);
  UMyMod.SetaCodigoPesquisado(EdVSCOPCab, CBVSCOPCab, QrVSCOPCab, VAR_CADASTRO);
end;

procedure TFmVSRRMCab.SbVSCOPCabCpyClick(Sender: TObject);
const
  EdOperacoes = nil;
  CBOperacoes = nil;
  EdCustoMOKg = nil;
begin
  VS_PF.PreencheDadosDeVSCOPCab(EdVSCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoArea,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOM2,
  nil, nil, nil, nil, nil, nil, nil, nil, nil, nil);
end;

procedure TFmVSRRMCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRRMCab.FornecedorMO1Click(Sender: TObject);
begin
  if VS_PF.AlteraVMI_FornecMO(QrVSRRMAtuControle.Value,
  QrVSRRMAtuFornecMO.Value) then
    LocCod(QrVSRRMCabCodigo.Value, QrVSRRMCabCodigo.Value);
end;

procedure TFmVSRRMCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSRRMCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  CkCpermitirCrust.Enabled := True;
  LaGGXDst.Enabled := True;
  EdGGXDst.Enabled := True;
  CBGGXDst.Enabled := True;
  //
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSRRMCab, [PnDados],
  [PnEdita], EdVSCOPCab, ImgTipo, 'vsrrmcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
end;

procedure TFmVSRRMCab.QrVSRRMCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSRRMAtu.Close;
  QrVSRRMOriIMEI.Close;
  QrVSRRMOriPallet.Close;
  QrVSRRMDst.Close;
  QrVSRRMSubPrd.Close;
  QrForcados.Close;
  QrEmit.Close;
  QrPQO.Close;
  QrVSMOEnvEnv.Close;
  QrVSMOEnvRet.Close;
end;

procedure TFmVSRRMCab.QrVSRRMCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSRRMCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSRRMCab.QrVSRRMCabCalcFields(DataSet: TDataSet);
begin
  case QrVSRRMCabTipoArea.Value of
    0: QrVSRRMCabNO_TIPO.Value := 'm�';
    1: QrVSRRMCabNO_TIPO.Value := 'ft�';
    else QrVSRRMCabNO_TIPO.Value := '???';
  end;
  QrVSRRMCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSRRMCabDtHrLibOpe.Value, 106, True);
  QrVSRRMCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSRRMCabDtHrFimOpe.Value, 106, True);
  QrVSRRMCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSRRMCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSRRMCab.QrVSRRMDesclDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSRRMCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
  'AND vmi.SrcMovID=' + Geral.FF0(Integer(emidEmReprRM)),
  'AND vmi.SrcNivel1=' + Geral.FF0(QrVSRRMCabCodigo.Value),
  'AND vmi.SrcNivel2=' + Geral.FF0(QrVSRRMAtuControle.Value),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSRRMDesclDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRRMDesclBxa, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
end;

procedure TFmVSRRMCab.QrVSRRMDstAfterScroll(DataSet: TDataSet);
const
  SQL_Limit = '';
  Controle = 0;
var
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSRRMCabTemIMEIMrt.Value;
  VS_PF.ReopenVSOpePrcBxa(QrVSRRMBxa, QrVSRRMCabMovimCod.Value,
  QrVSRRMDstMovimTwn.Value, Controle, TemIMEIMrt, eminEmRRMBxa, SQL_Limit);
end;

procedure TFmVSRRMCab.QrVSRRMDstBeforeClose(DataSet: TDataSet);
begin
  QrVSRRMBxa.Close;
  QrVSMOEnvRet.Close;
end;

end.

