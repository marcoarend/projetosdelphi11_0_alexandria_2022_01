object FmVSCPMRSBER: TFmVSCPMRSBER
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-193 :: Encerramento de Rendimento de Lote'
  ClientHeight = 267
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 523
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 475
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 461
        Height = 32
        Caption = 'Encerramento de Rendimento de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 461
        Height = 32
        Caption = 'Encerramento de Rendimento de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 461
        Height = 32
        Caption = 'Encerramento de Rendimento de Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 571
    Height = 105
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 571
      Height = 105
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 571
        Height = 105
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 72
          Top = 28
          Width = 109
          Height = 13
          Caption = 'Data do encerramento:'
        end
        object TPData: TdmkEditDateTimePicker
          Left = 72
          Top = 44
          Width = 186
          Height = 21
          Date = 43069.633499293980000000
          Time = 43069.633499293980000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 153
    Width = 571
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 567
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 197
    Width = 571
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 425
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 12
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 423
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrAbertos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 104
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.MovimCod, vmi.SerieFch, vmi.Ficha'
      'FROM vsinncab cab'
      'LEFT JOIN vsmovits vmi ON vmi.MovimCod=cab.MovimCod'
      'WHERE cab.TpEnceRend=0  '
      'AND ('
      '  vmi.SdoVrtPeca=0 '
      '  OR '
      '  (vmi.SdoVrtPeso=0 AND vmi.Pecas=0) '
      ') ')
    Left = 216
    Top = 148
    object QrListaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrListaSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrListaFicha: TIntegerField
      FieldName = 'Ficha'
    end
  end
  object QrFechou: TmySQLQuery
    Database = Dmod.MyDB
    Left = 288
    Top = 48
  end
end
