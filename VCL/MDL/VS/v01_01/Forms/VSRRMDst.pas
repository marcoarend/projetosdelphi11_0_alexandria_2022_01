unit VSRRMDst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Menus, UnProjGroup_Consts, dmkCheckBox, UnAppEnums;

type
  TFmVSRRMDst = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdCtrl1: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    EdFicha: TdmkEdit;
    Label4: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    GroupBox3: TGroupBox;
    CkBaixa: TCheckBox;
    PnBaixa: TPanel;
    Label8: TLabel;
    EdBxaPecas: TdmkEdit;
    LaBxaPesoKg: TLabel;
    EdBxaPesoKg: TdmkEdit;
    LaBxaAreaM2: TLabel;
    EdBxaAreaM2: TdmkEditCalc;
    LaBxaAreaP2: TLabel;
    EdBxaAreaP2: TdmkEditCalc;
    Label16: TLabel;
    EdBxaValorT: TdmkEdit;
    EdCtrl2: TdmkEdit;
    Label17: TLabel;
    EdBxaObserv: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    Label20: TLabel;
    LaVSRibCla: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet1: TSpeedButton;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdPedItsFin: TdmkEditCB;
    Label48: TLabel;
    CBPedItsFin: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Panel3: TPanel;
    EdPecas: TdmkEdit;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    LaPecas: TLabel;
    EdObserv: TdmkEdit;
    Label9: TLabel;
    EdRendimento: TdmkEdit;
    Label13: TLabel;
    EdCustoMOM2: TdmkEdit;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdCustoMOTot: TdmkEdit;
    Label15: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    MeAviso: TMemo;
    BtCalculo: TButton;
    PMCalculo: TPopupMenu;
    Peasapartirdareadesemi1: TMenuItem;
    Pesoderaspa1: TMenuItem;
    SBNewPallet: TSpeedButton;
    CkEncerraPallet: TdmkCheckBox;
    Label22: TLabel;
    EdCustoMOPc: TdmkEdit;
    Label23: TLabel;
    EdCustoMOKg: TdmkEdit;
    Label21: TLabel;
    EdCusFrtMORet: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Change(Sender: TObject);
    procedure CkBaixaClick(Sender: TObject);
    procedure EdBxaAreaM2Change(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure EdBxaPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaAreaM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaValorTChange(Sender: TObject);
    procedure EdCustoMOM2Change(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdBxaPecasChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure Peasapartirdareadesemi1Click(Sender: TObject);
    procedure Pesoderaspa1Click(Sender: TObject);
    procedure BtCalculoClick(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOPcChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure EdCusFrtMORetChange(Sender: TObject);
  private
    { Private declarations }
    FUltGGX, FGraGruXDst, FFatorIntDst, FFatorIntSrc: Integer;
    //
    procedure ReopenVSInnIts(Controle: Integer);
    procedure CalculaRendimento();
    procedure CalculaParcial();
  public
    { Public declarations }
    //FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FGraGruXSrc, FFornecMO: Integer;
    FValM2, FSdoPecas, FSdoPesoKg, FSdoReaM2: Double;
    FPallOnEdit: array of Integer;
    FAreaTotal: Double;
    //
    procedure CalculaCusto();
  end;

  var
  FmVSRRMDst: TFmVSRRMDst;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSRRMCab, Principal, UnVS_PF, AppListas, UnGrade_PF, CalcParc4Val;

{$R *.DFM}

procedure TFmVSRRMDst.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0;
  //Pallet     = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 1;
  NotaMPAG   = 0;
  //
  Fornecedor = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  //
  TpCalcAuto = -1;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  //PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  //Misturou,
  Codigo, Controle, MovimCod, Empresa, GraGruX, SerieFch, Ficha, Ctrl1, Ctrl2,
  MovimTwn, Pallet, PedItsFin, StqCenLoc, ReqMovEstq, ClientMO, PalToClose,
  FornecMO, VmiPai: Integer;
  Pecas, PesoKg, ValorT, AreaM2, AreaP2, CustoMOPc, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := 0;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Ctrl1          := EdCtrl1.ValueVariant;
  Ctrl2          := EdCtrl2.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  //DataHora       := Geral.FDT(FDataHora, 109);
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidEmReprRM;
  MovimNiv       := eminDestRRM;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  CustoMOPc      := EdCustoMOPc.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  ValorMP        := EdBxaValorT.ValueVariant;
  Observ         := EdObserv.Text;
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;
  Marca          := EdMarca.Text;
  Pallet         := EdPallet.ValueVariant;
  PalToClose     := Pallet;
  //Misturou       := RGMisturou.ItemIndex;
  PedItsFin      := EdPedItsFin.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  FornecMO       := FFornecMO;
  //
  if VS_PF.FichaErro(EdSerieFch, Empresa, Ctrl1, Ficha) then
    Exit;
  // Pode ser raspa por peso sem custo!
  if VS_PF.VSFic(GraGruX, Empresa, ClientMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, (*EdFicha*)nil, EdPecas,
    EdAreaM2, EdPesoKg, (*EdValorT*)nil, ExigeFornecedor, CO_GraGruY_6144_VSFinCla,
    ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  //
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl1);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei090(*Gera��o de couro resultante de reprocesso/reparo*)) then
  begin
    Ctrl1  := Controle;
    Pallet := 0;
    VS_PF.AtualizaSaldoIMEI(Ctrl1, True);
    VS_PF.AtualizaVSPedIts_Fin(PedItsFin);
    //
    if CkBaixa.Checked then
    begin
      GraGruX        := FmVSRRMCab.QrVSRRMAtuGraGruX.Value;
      SerieFch       := FmVSRRMCab.QrVSRRMAtuSerieFch.Value;
      Ficha          := FmVSRRMCab.QrVSRRMAtuFicha.Value;
      Marca          := FmVSRRMCab.QrVSRRMAtuMarca.Value;
      MovimID        := emidEmReprRM;
      MovimNiv       := eminEmRRMBxa;
      AreaM2         := -EdBxaAreaM2.ValueVariant;
      AreaP2         := -EdBxaAreaP2.ValueVariant;
      Pecas          := -EdBxaPecas.ValueVariant;
      PesoKg         := -EdBxaPesoKg.ValueVariant;
      ValorT         := -EdBxaValorT.ValueVariant;
      ValorMP        := 0;
      CustoMOPc      := 0;
      CustoMOKg      := 0;
      CustoMOM2      := 0;
      CustoMOTot     := 0;
      Observ         := EdBxaObserv.Text;
      //
      //PedItsLib      := 0;
      PedItsFin      := 0;
      //PedItsVda      := 0;
      //GSPSrcMovID     := 0;
      //GSPSrcNiv2     := 0;
      ReqMovEstq     := 0;
      VmiPai := FmVSRRMCab.QrVSRRMAtuControle.Value;
      //
      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl2);
      if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
      MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
      CliVenda, Controle, Ficha, (*Misturou,*) CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
      SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
      AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei091(*Baixa de couro em reprocesso/reparo na gera��o de couro resultante de reprocesso/reparo*)) then
      begin
        Ctrl2 := Controle;
        //VS_PF.AtualizaSaldoIMEI(Ctrl2, True);
        //VS_PF.AtualizaSaldoIMEI(FmVSRRMCab.QrVSRRMAtuControle.Value, True);
      end;
    end;
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSRRMcab', MovimCod);
    VS_PF.AtualizaTotaisVSRRMCab(FmVSRRMCab.QrVSRRMCabMovimCod.Value);
    VS_PF.AtualizaNF_IMEI(FmVSRRMCab.QrVSRRMCabMovimCod.Value, Ctrl1);
    FmVSRRMCab.AtualizaNFeItens();
    //
    if CkEncerraPallet.Checked then
      VS_PF.EncerraPalletSimples(PalToClose, FEmpresa, FClientMO, QrVSPallet, FPallOnEdit);
    VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
    FmVSRRMCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Ctrl1);
    if FmVSRRMCab.QrVSRRMAtuFornecMO.Value <>
    FmVSRRMCab.QrVSRRMAtuEmpresa.Value then
    begin
      if ImgTipo.SQLType = stIns then
        FmVSRRMCab.IncluiAtrelamento1Click(FmVSRRMCab.IncluiAtrelamento1)
      else
        FmVSRRMCab.AlteraAtrelamento1Click(FmVSRRMCab.AlteraAtrelamento1);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdPecas.ReadOnly           := True;
      EdCtrl1.ValueVariant       := 0;
      EdCtrl2.ValueVariant       := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdObserv.Text              := '';
      EdFicha.ValueVariant       := 0;
      EdMarca.ValueVariant       := '';
      //
      //CkBaixa.Checked            := False;
      EdBxaPecas.ValueVariant    := 0;
      EdBxaPesoKg.ValueVariant   := 0;
      EdBxaAreaM2.ValueVariant   := 0;
      EdBxaAreaP2.ValueVariant   := 0;
      EdBxaValorT.ValueVariant   := 0;
      EdBxaObserv.ValueVariant   := '';
      //
      EdGraGruX.Enabled          := True;
      CBGraGruX.Enabled          := True;
      //EdSerieFch.Enabled         := True;
      //CBSerieFch.Enabled         := True;
      //EdFicha.Enabled            := True;
      //EdMarca.Enabled            := True;
      //EdValorT.Enabled           := True;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      SBNewPallet.Enabled        := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSRRMDst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRRMDst.BtCalculoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCalculo, BtCalculo);
end;

procedure TFmVSRRMDst.CalculaCusto();
{
var
  BxaValorT, AreaM2, CustoMOM2, CustoMOTot, ValorT: Double;
begin
  BxaValorT  := EdBxaValorT.ValueVariant;
  AreaM2     := EdAreaM2.ValueVariant;
  CustoMOM2  := EdCustoMOM2.ValueVariant;
  CustoMOTot := CustoMOM2 * AreaM2;
  ValorT     := BxaValorT + CustoMOTot;
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdValorT.ValueVariant     := ValorT;
}
var
  // AreaM2, CustoMOM2, CustoMOTot, CusFrtMORet,
  BxaValorT, ValorT, Pecas, PesoKg, AreaM2, MOPc, MOKg, MOM2, ValorMP, ValorT2,
  CustoMOTot, CusFrtMORet: Double;
begin
  BxaValorT   := EdBxaValorT.ValueVariant;
  //AreaM2      := EdAreaM2.ValueVariant;
  //CustoMOM2   := EdCustoMOM2.ValueVariant;
  //CustoMOTot  := CustoMOM2 * AreaM2;
  //////////////////////////////////////////////////////////////////////////////
  Pecas       := EdPecas.ValueVariant;
  PesoKg      := EdPesoKg.ValueVariant;
  AreaM2      := EdAreaM2.ValueVariant;
  //
  MOPC        := EdCustoMOPc.ValueVariant;
  MOKg        := EdCustoMOKg.ValueVariant;
  MOM2        := EdCustoMOM2.ValueVariant;
  //
  CusFrtMORet := EdCusFrtMORet.ValueVariant;
  CustoMOTot  := (Pecas * MOPc) + (PesoKg * MOKg) + (AreaM2 * MOM2);
  //////////////////////////////////////////////////////////////////////////////
  ValorT      := BxaValorT + CustoMOTot + CusFrtMORet;
  //////////////////////////////////////////////////////////////////////////////
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdValorT.ValueVariant     := ValorT;
end;

procedure TFmVSRRMDst.CalculaParcial();
var
  AreaTotal: String;
  Area: Double;
begin
  Area := EdAreaM2.ValueVariant;
  if Area = 0 then
  begin
    Geral.MB_Aviso('Informe a �rea!');
    Exit;
  end;
  if FAreaTotal = 0 then
  begin
    AreaTotal := '0,00';
    if InputQuery('Area total', 'Informe a area total:', AreaTotal) then
      FAreaTotal := Geral.DMV(AreaTotal);
  end;
  if FAreaTotal <> 0 then
  begin
    Application.CreateForm(TFmCalcParc4Val, FmCalcParc4Val);
    FmCalcParc4Val.FCalcExec := calcexecPecasPorRendArea;
    FmCalcParc4Val.EdA1.ValueVariant := FmVSRRMCab.QrVSRRMCabPecasINI.Value;
    FmCalcParc4Val.EdA2.ValueVariant := FmVSRRMCab.QrVSRRMCabAreaINIM2.Value;
    FmCalcParc4Val.EdB1.ValueVariant := FmVSRRMCab.QrVSRRMCabPecasINI.Value;
    FmCalcParc4Val.EdB2.Text := AreaTotal;
    //FmCalcParc4Val.Ed03Peca.ValueVariant := ???;
    FmCalcParc4Val.Ed03ArM2.ValueVariant := Area;
    FmCalcParc4Val.ShowModal;
    if FmCalcParc4Val.FUsaDados then
    begin
      EdBxaPecas.ValueVariant := Geral.RoundC(FmCalcParc4Val.Ed03Peca.ValueVariant, 0);
      EdBxaAreaM2.ValueVariant := FmCalcParc4Val.Ed04ArM2.ValueVariant;
      //
      EdAreaM2.ValueVariant := FmCalcParc4Val.Ed03ArM2.ValueVariant;
    end;
    FmCalcParc4Val.Destroy;
  end;
end;

procedure TFmVSRRMDst.CalculaRendimento();
var
  Antes, Depois, Rendi: Double;
begin
  Antes  := EdBxaAreaM2.ValueVariant;
  Depois := EdAreaM2.ValueVariant;
  if Antes > 0 then
  begin
    Rendi := ((Depois - Antes) * 100) / Antes;
  end else
    Rendi := 0;
  EdRendimento.ValueVariant := Rendi;
end;

procedure TFmVSRRMDst.CkBaixaClick(Sender: TObject);
begin
  PnBaixa.Visible := CkBaixa.Checked;
end;

procedure TFmVSRRMDst.Criar1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  VS_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrVSPallet, emidEmReprRM, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
end;

procedure TFmVSRRMDst.EdAreaM2Change(Sender: TObject);
begin
  CalculaRendimento();
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdBxaAreaM2Change(Sender: TObject);
begin
  EdBxaValorT.ValueVariant := FValM2 * EdBxaAreaM2.ValueVariant;
  CalculaRendimento();
end;

procedure TFmVSRRMDst.EdBxaAreaM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdBxaAreaM2.ValueVariant := EdAreaM2.ValueVariant;
end;

procedure TFmVSRRMDst.EdBxaPecasChange(Sender: TObject);
var
  Pecas, FatorPecas: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto da �rea
  Pecas := EdBxaPecas.ValueVariant;
  if (FSdoPecas > 0) and (ImgTipo.SQLType = stIns) then
  begin
    if Pecas >= FSdoPecas then
      FatorPecas := 1
    else
      FatorPecas := Pecas / FSdoPecas;
    EdBxaPesoKg.ValueVariant := FatorPecas * FSdoPesoKg;
    EdBxaAreaM2.ValueVariant := FatorPecas * FSdoReaM2;
  end;
  //
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdPecas.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst;
end;

procedure TFmVSRRMDst.EdBxaPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdBxaPecas.ValueVariant  := FmVSRRMCab.QrVSRRMCabPecasSdo.Value;
    EdBxaPesoKg.ValueVariant := FmVSRRMCab.QrVSRRMCabPesoKgSdo.Value;
    EdBxaAreaM2.ValueVariant := FmVSRRMCab.QrVSRRMCabAreaSdoM2.Value;
  end
end;

procedure TFmVSRRMDst.EdBxaPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdBxaPesoKg.ValueVariant := EdPesoKg.ValueVariant;
end;

procedure TFmVSRRMDst.EdBxaValorTChange(Sender: TObject);
begin
  EdValorT.ValueVariant := EdBxaValorT.ValueVariant;
end;

procedure TFmVSRRMDst.EdCusFrtMORetChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdCustoMOM2Change(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdCustoMOPcChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ficha: Integer;
begin
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
end;

procedure TFmVSRRMDst.EdGraGruXChange(Sender: TObject);
begin
  FGraGruXDst := EdGraGruX.ValueVariant;
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue     := Null;
  if FGraGruXDst = 0 then
    QrVSPallet.Close
  else
    VS_PF.ReopenVSPallet(QrVSPallet, FEmpresa, FClientMO, FGraGruXDst, '', FPallOnEdit);
end;

procedure TFmVSRRMDst.EdGraGruXRedefinido(Sender: TObject);
begin
  if (FGraGruXDst <> null) and (FGraGruXDst <> 0) then
  begin
    VS_PF.ObtemFatorInteiro(FGraGruXSrc, FFatorIntSrc, False, 1, MeAviso);
    VS_PF.ObtemFatorInteiro(FGraGruXDst, FFatorIntDst, True, FFatorIntSrc, MeAviso);
  end else
  begin
    FFatorIntSrc := 0;
    FFatorIntDst := 0;
  end;
end;

procedure TFmVSRRMDst.EdPecasChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdPecas.ReadOnly := False;
    Geral.MB_Info('Informe as pe�as direto na caixa de edi��o!')
  end;
end;

procedure TFmVSRRMDst.EdPesoKgChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmVSRRMDst.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSRRMDst.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAreaTotal := 0;
  FUltGGX  := 0;
  FGraGruXDst := 0;
  TPData.Date := Now();
  EdHora.ValueVariant := Now();
  //
  VS_PF.AbreGraGruXY(QrGraGruX, '');
    //'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_VSFinCla));
  //
  VS_PF.AbreVSSerFch(QrVSSerFch);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSRRMDst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRRMDst.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(EdPallet.ValueVariant);
end;

procedure TFmVSRRMDst.Peasapartirdareadesemi1Click(Sender: TObject);
begin
  CalculaParcial();
end;

procedure TFmVSRRMDst.Pesoderaspa1Click(Sender: TObject);
var
  KgM2, ArM2: String;
  Area, Peso, Pecas: Double;
begin
  if FSdoPesoKg > 0 then
  begin
    KgM2 := '3,500';
    if InputQuery('Calculo de pe�as de raspa', 'Kg por m�:', KgM2) then
    begin
      Area := EdAreaM2.ValueVariant;
      if Area = 0 then
      begin
        ArM2 := '0,00';
        if InputQuery('Calculo de pe�as de raspa', '�rea m�:', ArM2) then
        begin
          Area := Geral.DMV(ArM2);
          EdAreaM2.ValueVariant := Area;
        end;
      end;
      Peso := Geral.DMV(KgM2);
      Peso := Round(Area * Peso);
      //
      Pecas := Round(Peso / FSdoPesoKg * FSdoPecas);
      //
      EdBxaPecas.ValueVariant := Pecas;
    end;
  end;
end;

procedure TFmVSRRMDst.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSRRMDst.SBNewPalletClick(Sender: TObject);
begin
  VS_PF.GeraNovoPallet(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  EdGraGruX, EdPallet, CBPallet, SBNewPallet, QrVSPallet, FPallOnEdit);
end;

procedure TFmVSRRMDst.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

end.
