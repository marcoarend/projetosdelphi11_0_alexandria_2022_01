object FmVSGerArtDatas: TFmVSGerArtDatas
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-113 :: Datas de Gera'#231#227'o de Artigo'
  ClientHeight = 297
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 454
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 465
    object GB_R: TGroupBox
      Left = 406
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 417
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 358
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 369
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 338
        Height = 32
        Caption = 'Datas de Gera'#231#227'o de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 338
        Height = 32
        Caption = 'Datas de Gera'#231#227'o de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 338
        Height = 32
        Caption = 'Datas de Gera'#231#227'o de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 454
    Height = 124
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 465
    ExplicitHeight = 132
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 454
      Height = 124
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 465
      ExplicitHeight = 132
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 454
        Height = 124
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 465
        ExplicitHeight = 132
        object Label23: TLabel
          Left = 32
          Top = 16
          Width = 94
          Height = 13
          Caption = 'Data/hora gera'#231#227'o:'
        end
        object Label11: TLabel
          Left = 208
          Top = 16
          Width = 107
          Height = 13
          Caption = 'Liberado p/ classificar:'
        end
        object Label33: TLabel
          Left = 32
          Top = 60
          Width = 94
          Height = 13
          Caption = 'Configur. p/ classif.:'
        end
        object Label34: TLabel
          Left = 208
          Top = 60
          Width = 83
          Height = 13
          Caption = 'Fim classifica'#231#227'o:'
        end
        object TPDtHrAberto: TdmkEditDateTimePicker
          Left = 32
          Top = 32
          Width = 112
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdDtHrAberto: TdmkEdit
          Left = 144
          Top = 32
          Width = 57
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryName = 'QrVSGerArt'
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDtHrLibCla: TdmkEditDateTimePicker
          Left = 208
          Top = 32
          Width = 112
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdDtHrLibCla: TdmkEdit
          Left = 320
          Top = 32
          Width = 57
          Height = 21
          TabOrder = 3
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryName = 'QrVSGerArt'
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDtHrCfgCla: TdmkEditDateTimePicker
          Left = 32
          Top = 76
          Width = 112
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdDtHrCfgCla: TdmkEdit
          Left = 144
          Top = 76
          Width = 57
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryName = 'QrVSGerArt'
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDtHrFimCla: TdmkEditDateTimePicker
          Left = 208
          Top = 76
          Width = 112
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 6
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdDtHrFimCla: TdmkEdit
          Left = 320
          Top = 76
          Width = 57
          Height = 21
          TabOrder = 7
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryName = 'QrVSGerArt'
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 172
    Width = 454
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 450
      Height = 21
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 461
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 36
      Width = 450
      Height = 17
      Align = alBottom
      TabOrder = 1
      ExplicitLeft = 84
      ExplicitTop = 12
      ExplicitWidth = 150
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 227
    Width = 454
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 224
    ExplicitWidth = 465
    object PnSaiDesis: TPanel
      Left = 308
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 319
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 306
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 317
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrVSPaClaCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    SQL.Strings = (
      'SELECT prc.* '
      'FROM vspaclacaba prc '
      'WHERE VSGerArt=5 ')
    Left = 28
    Top = 148
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaClaCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
    end
    object QrVSPaClaCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
    end
    object QrVSPaClaCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
    end
    object QrVSPaClaCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
    end
    object QrVSPaClaCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
    end
    object QrVSPaClaCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
    end
    object QrVSPaClaCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
    end
    object QrVSPaClaCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
    end
    object QrVSPaClaCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
    end
    object QrVSPaClaCabDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaCabDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
    end
    object QrVSPaClaCabFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object QrVSPaClaIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsparclitsa'
      'WHERE Codigo=5'
      'ORDER BY DtHrIni')
    Left = 108
    Top = 148
    object QrVSPaClaItsFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPaClaItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPaClaItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaClaItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaClaItsVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPaClaItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaClaItsTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrVSPaClaItsDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaClaItsDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSPaClaItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaItsDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
    end
  end
  object QrVSPaMulCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaMulCabBeforeClose
    AfterScroll = QrVSPaMulCabAfterScroll
    SQL.Strings = (
      'SELECT pra.*,'
      'cn1.FatorInt  '
      'FROM vspamulcaba pra'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pra.VMI_Sorc'
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 '
      'WHERE pra.VSGerArt=2'
      'ORDER BY pra.Codigo ')
    Left = 388
    Top = 76
    object QrVSPaMulCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaMulCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaMulCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaMulCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaMulCabVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaMulCabDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSPaMulCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPaMulCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSPaMulCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSPaMulCabAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSPaMulCabValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSPaMulCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaMulCabPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSPaMulCabTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPaMulCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPaMulCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSPaMulCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaMulCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaMulCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaMulCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaMulCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaMulCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaMulCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaMulCabFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object QrVSPaMulIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 388
    Top = 121
    object QrVSPaMulItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPaMulItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPaMulItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPaMulItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPaMulItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPaMulItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPaMulItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPaMulItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPaMulItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPaMulItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrVSPaMulItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPaMulItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPaMulItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPaMulItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPaMulItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPaMulItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPaMulItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPaMulItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPaMulItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPaMulItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPaMulItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPaMulItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPaMulItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPaMulItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPaMulItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPaMulItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPaMulItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPaMulItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPaMulItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPaMulItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaMulItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPaMulItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPaMulItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPaMulItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPaMulItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPaMulItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPaMulItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object QrVMIs: TmySQLQuery
    Database = Dmod.MyDB
    Left = 200
    Top = 212
    object QrVMIsVMI: TIntegerField
      FieldName = 'VMI'
    end
    object QrVMIsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
end
