unit VSPosNegSameReg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmVSPosNegSameReg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    PnPesquisa: TPanel;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSPosNegSameReg: TFmVSPosNegSameReg;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSPosNegSameReg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPosNegSameReg.BtTeste2Click(Sender: TObject);
const
  Avisa = True;
  ForcaMostrarForm = False;
  SelfCall = True;
var
  TemIMEiMrt: Integer;
begin
  TemIMEiMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  DmModVS.IMEIsComCamposPositivosENegativosNoMesmoRegistro(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2);
end;

procedure TFmVSPosNegSameReg.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrFldPosNegMesmoRegControle.Value)
end;

procedure TFmVSPosNegSameReg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPosNegSameReg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DGDados.Datasource := DmModVS.DsFldPosNegMesmoReg;
end;

procedure TFmVSPosNegSameReg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
