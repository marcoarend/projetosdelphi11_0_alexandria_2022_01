object FmVSImpRecla: TFmVSImpRecla
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-048 :: Reclasses Geradas'
  ClientHeight = 242
  ClientWidth = 718
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 718
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 452
    object GB_R: TGroupBox
      Left = 670
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 404
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 622
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 356
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 239
        Height = 32
        Caption = 'Reclasses Geradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 239
        Height = 32
        Caption = 'Reclasses Geradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 239
        Height = 32
        Caption = 'Reclasses Geradas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 718
    Height = 80
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 452
    ExplicitHeight = 118
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 718
      Height = 80
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 452
      ExplicitHeight = 118
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 718
        Height = 80
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 452
        ExplicitHeight = 118
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 714
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel5'
          TabOrder = 0
          ExplicitLeft = 364
          ExplicitTop = 200
          ExplicitWidth = 185
          ExplicitHeight = 41
          object Label1: TLabel
            Left = 48
            Top = 0
            Width = 109
            Height = 13
            Caption = 'Artigo (Re)Classificado:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 48
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 104
            Top = 16
            Width = 597
            Height = 21
            KeyField = 'GraGruX'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsVSCacIts
            TabOrder = 1
            dmkEditCB = EdGraGruX
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 128
    Width = 718
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 166
    ExplicitWidth = 452
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 714
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 448
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 172
    Width = 718
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 210
    ExplicitWidth = 452
    object PnSaiDesis: TPanel
      Left = 572
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 306
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 570
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 304
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrVSCacIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSCacItsBeforeClose
    AfterScroll = QrVSCacItsAfterScroll
    OnCalcFields = QrVSCacItsCalcFields
    SQL.Strings = (
      'SELECT pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE cia.vmi_Sorc=5'
      'GROUP BY pla.GraGruX ')
    Left = 56
    Top = 84
    object QrVSCacItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSCacItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSCacItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCacItsPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      Calculated = True
    end
    object QrVSCacItsPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrVSCacItsMediaM2PC: TFloatField
      FieldName = 'MediaM2PC'
    end
    object QrVSCacItsAgrupaTudo: TFloatField
      FieldName = 'AgrupaTudo'
    end
  end
  object frxWET_CURTI_048_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41979.425569537040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_048_001GetValue
    Left = 408
    Top = 84
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929175350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000000000
          Top = 64.252010000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 427.086890000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 332.598640000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 574.488560000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 627.401980000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677165350000000000
        ParentFont = False
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSCacIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 480.000310000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 427.086890000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 332.598640000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'MediaM2PC'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."MediaM2PC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 805.039890000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSCacIts."AgrupaTudo"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Width = 385.511864720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 532.913730000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 464.881892200000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object ChartPie0: TfrxChartView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 461.102362200000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C65080B4178697356697369626C65080D4672616D652E5669736962
            6C6508175669657733444F7074696F6E732E456C65766174696F6E033B011856
            69657733444F7074696F6E732E4F7274686F676F6E616C08195669657733444F
            7074696F6E732E50657273706563746976650200165669657733444F7074696F
            6E732E526F746174696F6E0368010B56696577334457616C6C73080A42657665
            6C4F75746572070662764E6F6E6505436F6C6F720707636C576869746511436F
            6C6F7250616C65747465496E646578020D000A54506965536572696573075365
            7269657331134D61726B732E4172726F772E56697369626C6509194D61726B73
            2E43616C6C6F75742E42727573682E436F6C6F720707636C426C61636B1B4D61
            726B732E43616C6C6F75742E4172726F772E56697369626C65090D4D61726B73
            2E56697369626C65090D5856616C7565732E4F72646572070B6C6F417363656E
            64696E670C5956616C7565732E4E616D6506035069650D5956616C7565732E4F
            7264657207066C6F4E6F6E651A4672616D652E496E6E657242727573682E4261
            636B436F6C6F720705636C526564224672616D652E496E6E657242727573682E
            4772616469656E742E456E64436F6C6F720706636C47726179224672616D652E
            496E6E657242727573682E4772616469656E742E4D6964436F6C6F720707636C
            5768697465244672616D652E496E6E657242727573682E4772616469656E742E
            5374617274436F6C6F720440404000214672616D652E496E6E65724272757368
            2E4772616469656E742E56697369626C65091B4672616D652E4D6964646C6542
            727573682E4261636B436F6C6F720708636C59656C6C6F77234672616D652E4D
            6964646C6542727573682E4772616469656E742E456E64436F6C6F7204828282
            00234672616D652E4D6964646C6542727573682E4772616469656E742E4D6964
            436F6C6F720707636C5768697465254672616D652E4D6964646C654272757368
            2E4772616469656E742E5374617274436F6C6F720706636C4772617922467261
            6D652E4D6964646C6542727573682E4772616469656E742E56697369626C6509
            1A4672616D652E4F7574657242727573682E4261636B436F6C6F720707636C47
            7265656E224672616D652E4F7574657242727573682E4772616469656E742E45
            6E64436F6C6F720440404000224672616D652E4F7574657242727573682E4772
            616469656E742E4D6964436F6C6F720707636C5768697465244672616D652E4F
            7574657242727573682E4772616469656E742E5374617274436F6C6F72070863
            6C53696C766572214672616D652E4F7574657242727573682E4772616469656E
            742E56697369626C65090D4672616D652E56697369626C65080B4672616D652E
            57696474680204194F74686572536C6963652E4C6567656E642E56697369626C
            6508000000}
          ChartElevation = 315
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtDBData
              DataSet = frxDsVSCacIts
              DataSetName = 'frxDsVSCacIts'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsVSCacIts."NO_PRD_TAM_COR"'
              Source2 = 'frxDsVSCacIts."PercM2"'
              XSource = 'frxDsVSCacIts."NO_PRD_TAM_COR"'
              YSource = 'frxDsVSCacIts."PercM2"'
            end>
        end
      end
    end
  end
  object frxDsVSCacIts: TfrxDBDataset
    UserName = 'frxDsVSCacIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercM2=PercM2'
      'PercPc=PercPc'
      'MediaM2PC=MediaM2PC'
      'AgrupaTudo=AgrupaTudo')
    DataSet = QrVSCacIts
    BCDToCurrency = False
    Left = 56
    Top = 132
  end
  object QrVSCacSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2'
      'FROM vscacitsa cia '
      'WHERE cia.vmi_Sorc=5')
    Left = 140
    Top = 84
    object QrVSCacSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrVSGerRcl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vgr.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, pal.Nome NO_PALLET'
      'FROM vsgerrcla vgr'
      'LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet '
      'WHERE vgr.Codigo > 0')
    Left = 12
    Top = 13
    object QrVSGerRclNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSGerRclNO_DtHrFimCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimCla'
      Calculated = True
    end
    object QrVSGerRclNO_DtHrLibCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibCla'
      Calculated = True
    end
    object QrVSGerRclNO_DtHrCfgCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgCla'
      Calculated = True
    end
    object QrVSGerRclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerRclMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerRclCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSGerRclGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerRclVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSGerRclNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSGerRclEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerRclDtHrLibCla: TDateTimeField
      FieldName = 'DtHrLibCla'
    end
    object QrVSGerRclDtHrCfgCla: TDateTimeField
      FieldName = 'DtHrCfgCla'
    end
    object QrVSGerRclDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrVSGerRclTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSGerRclLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerRclDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerRclDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerRclUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerRclUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerRclAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerRclAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerRclNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSGerRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerRclNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
  end
  object QrSubClas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSubClasCalcFields
    SQL.Strings = (
      'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      
        'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) Medi' +
        'aM2PC, '
      'ggx.GraGru1, cia.SubClass, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta  pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN vsdsnsub   vds ON vds.SubClass=cia.SubClass'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vds.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod'
      'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo'
      'WHERE vdc.Codigo=7'
      'AND pla.GraGruX=7'
      'GROUP BY pla.GraGruX, cia.SubClass ')
    Left = 224
    Top = 84
    object QrSubClasAgrupaTudo: TFloatField
      FieldName = 'AgrupaTudo'
      Required = True
    end
    object QrSubClasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSubClasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSubClasAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSubClasAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSubClasMediaM2PC: TFloatField
      FieldName = 'MediaM2PC'
    end
    object QrSubClasGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSubClasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrSubClasNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSubClasPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrSubClasPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      Calculated = True
    end
  end
  object frxDsSubClas: TfrxDBDataset
    UserName = 'frxDsSubClas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'AgrupaTudo=AgrupaTudo'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'MediaM2PC=MediaM2PC'
      'GraGru1=GraGru1'
      'SubClass=SubClass'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercPc=PercPc'
      'PercM2=PercM2')
    DataSet = QrSubClas
    BCDToCurrency = False
    Left = 224
    Top = 132
  end
  object frxWET_CURTI_048_002: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41979.425569537040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_048_001GetValue
    Left = 408
    Top = 132
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSubAll
        DataSetName = 'frxDsSubAll'
      end
      item
        DataSet = frxDsSubClas
        DataSetName = 'frxDsSubClas'
      end
      item
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929175350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000000000
          Top = 64.252010000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 427.086890000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 332.598640000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 574.488560000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 627.401980000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 49.133875350000000000
        ParentFont = False
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
        KeepTogether = True
        RowCount = 0
        StartNewPage = True
        object MeValNome: TfrxMemoView
          Left = 68.031540000000000000
          Top = 3.779530000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSCacIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 480.000310000000000000
          Top = 3.779530000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 427.086890000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 332.598640000000000000
          Top = 3.779530000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 574.488560000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 627.401980000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'MediaM2PC'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."MediaM2PC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 68.031540000000000000
          Top = 26.456710000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 37.795300000000000000
          Top = 26.456710000000000000
          Width = 30.236196060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 480.000310000000000000
          Top = 26.456710000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 427.086890000000000000
          Top = 26.456710000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 332.598640000000000000
          Top = 26.456710000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 574.488560000000000000
          Top = 26.456710000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 627.401980000000000000
          Top = 26.456710000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 876.850960000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSCacIts."AgrupaTudo"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 793.701300000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Width = 385.511864720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 532.913730000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSubClas
        DataSetName = 'frxDsSubClas'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 68.031540000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSubClas."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 37.795300000000000000
          Width = 30.236196060000000000
          Height = 22.677165350000000000
          DataField = 'SubClass'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSubClas."SubClass"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 480.000310000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubClas."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 427.086890000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubClas."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 332.598640000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'AreaM2'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubClas."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubClas."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'MediaM2PC'
          DataSet = frxDsSubClas
          DataSetName = 'frxDsSubClas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubClas."MediaM2PC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 464.881892200000000000
        ParentFont = False
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object ChartPie0: TfrxChartView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 461.102362200000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C65080B4178697356697369626C65080D4672616D652E5669736962
            6C6508175669657733444F7074696F6E732E456C65766174696F6E033B011856
            69657733444F7074696F6E732E4F7274686F676F6E616C08195669657733444F
            7074696F6E732E50657273706563746976650200165669657733444F7074696F
            6E732E526F746174696F6E0368010B56696577334457616C6C73080A42657665
            6C4F75746572070662764E6F6E6505436F6C6F720707636C576869746511436F
            6C6F7250616C65747465496E646578020D000A54506965536572696573075365
            7269657331134D61726B732E4172726F772E56697369626C6509194D61726B73
            2E43616C6C6F75742E42727573682E436F6C6F720707636C426C61636B1B4D61
            726B732E43616C6C6F75742E4172726F772E56697369626C65090D4D61726B73
            2E56697369626C65090D5856616C7565732E4F72646572070B6C6F417363656E
            64696E670C5956616C7565732E4E616D6506035069650D5956616C7565732E4F
            7264657207066C6F4E6F6E651A4672616D652E496E6E657242727573682E4261
            636B436F6C6F720705636C526564224672616D652E496E6E657242727573682E
            4772616469656E742E456E64436F6C6F720706636C47726179224672616D652E
            496E6E657242727573682E4772616469656E742E4D6964436F6C6F720707636C
            5768697465244672616D652E496E6E657242727573682E4772616469656E742E
            5374617274436F6C6F720440404000214672616D652E496E6E65724272757368
            2E4772616469656E742E56697369626C65091B4672616D652E4D6964646C6542
            727573682E4261636B436F6C6F720708636C59656C6C6F77234672616D652E4D
            6964646C6542727573682E4772616469656E742E456E64436F6C6F7204828282
            00234672616D652E4D6964646C6542727573682E4772616469656E742E4D6964
            436F6C6F720707636C5768697465254672616D652E4D6964646C654272757368
            2E4772616469656E742E5374617274436F6C6F720706636C4772617922467261
            6D652E4D6964646C6542727573682E4772616469656E742E56697369626C6509
            1A4672616D652E4F7574657242727573682E4261636B436F6C6F720707636C47
            7265656E224672616D652E4F7574657242727573682E4772616469656E742E45
            6E64436F6C6F720440404000224672616D652E4F7574657242727573682E4772
            616469656E742E4D6964436F6C6F720707636C5768697465244672616D652E4F
            7574657242727573682E4772616469656E742E5374617274436F6C6F72070863
            6C53696C766572214672616D652E4F7574657242727573682E4772616469656E
            742E56697369626C65090D4672616D652E56697369626C65080B4672616D652E
            57696474680204194F74686572536C6963652E4C6567656E642E56697369626C
            6508000000}
          ChartElevation = 315
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtDBData
              DataSet = frxDsSubClas
              DataSetName = 'frxDsSubClas'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsSubClas."SubClass"'
              Source2 = 'frxDsSubClas."PercM2"'
              XSource = 'frxDsSubClas."SubClass"'
              YSource = 'frxDsSubClas."PercM2"'
            end>
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929175350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo20: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 68.031540000000000000
          Top = 64.252010000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 427.086890000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 332.598640000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 574.488560000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 627.401980000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677165350000000000
        ParentFont = False
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSubAll
        DataSetName = 'frxDsSubAll'
        RowCount = 0
        object Memo32: TfrxMemoView
          Left = 68.031540000000000000
          Width = 264.566929130000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSubAll."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          DataField = 'SubClass'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSubAll."SubClass"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 480.000310000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubAll."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 427.086890000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubAll."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 332.598640000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'AreaM2'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubAll."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubAll."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'MediaM2PC'
          DataSet = frxDsSubAll
          DataSetName = 'frxDsSubAll'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSubAll."MediaM2PC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSCacIts."AgrupaTudo"'
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        object Memo41: TfrxMemoView
          Width = 385.511864720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 532.913730000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsSubAll."Pecas">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 385.512060000000000000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsSubAll."AreaM2">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 464.881892200000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object ChartPie1: TfrxChartView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 461.102362200000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C65080B4178697356697369626C65080D4672616D652E5669736962
            6C6508175669657733444F7074696F6E732E456C65766174696F6E033B011856
            69657733444F7074696F6E732E4F7274686F676F6E616C08195669657733444F
            7074696F6E732E50657273706563746976650200165669657733444F7074696F
            6E732E526F746174696F6E0368010B56696577334457616C6C73080A42657665
            6C4F75746572070662764E6F6E6505436F6C6F720707636C576869746511436F
            6C6F7250616C65747465496E646578020D000A54506965536572696573075365
            7269657331134D61726B732E4172726F772E56697369626C6509194D61726B73
            2E43616C6C6F75742E42727573682E436F6C6F720707636C426C61636B1B4D61
            726B732E43616C6C6F75742E4172726F772E56697369626C65090D4D61726B73
            2E56697369626C65090D5856616C7565732E4F72646572070B6C6F417363656E
            64696E670C5956616C7565732E4E616D6506035069650D5956616C7565732E4F
            7264657207066C6F4E6F6E651A4672616D652E496E6E657242727573682E4261
            636B436F6C6F720705636C526564224672616D652E496E6E657242727573682E
            4772616469656E742E456E64436F6C6F720706636C47726179224672616D652E
            496E6E657242727573682E4772616469656E742E4D6964436F6C6F720707636C
            5768697465244672616D652E496E6E657242727573682E4772616469656E742E
            5374617274436F6C6F720440404000214672616D652E496E6E65724272757368
            2E4772616469656E742E56697369626C65091B4672616D652E4D6964646C6542
            727573682E4261636B436F6C6F720708636C59656C6C6F77234672616D652E4D
            6964646C6542727573682E4772616469656E742E456E64436F6C6F7204828282
            00234672616D652E4D6964646C6542727573682E4772616469656E742E4D6964
            436F6C6F720707636C5768697465254672616D652E4D6964646C654272757368
            2E4772616469656E742E5374617274436F6C6F720706636C4772617922467261
            6D652E4D6964646C6542727573682E4772616469656E742E56697369626C6509
            1A4672616D652E4F7574657242727573682E4261636B436F6C6F720707636C47
            7265656E224672616D652E4F7574657242727573682E4772616469656E742E45
            6E64436F6C6F720440404000224672616D652E4F7574657242727573682E4772
            616469656E742E4D6964436F6C6F720707636C5768697465244672616D652E4F
            7574657242727573682E4772616469656E742E5374617274436F6C6F72070863
            6C53696C766572214672616D652E4F7574657242727573682E4772616469656E
            742E56697369626C65090D4672616D652E56697369626C65080B4672616D652E
            57696474680204194F74686572536C6963652E4C6567656E642E56697369626C
            6508000000}
          ChartElevation = 315
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtDBData
              DataSet = frxDsSubAll
              DataSetName = 'frxDsSubAll'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsSubAll."SubClass"'
              Source2 = 'frxDsSubAll."PercM2"'
              XSource = 'frxDsSubAll."SubClass"'
              YSource = 'frxDsSubAll."PercM2"'
            end>
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 805.039890000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrSubAll: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSubAllCalcFields
    SQL.Strings = (
      'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      
        'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) Medi' +
        'aM2PC, '
      'ggx.GraGru1, cia.SubClass, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta  pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN vsdsnsub   vds ON vds.SubClass=cia.SubClass'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vds.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod'
      'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo'
      'WHERE vdc.Codigo=7'
      'AND pla.GraGruX=7'
      'GROUP BY pla.GraGruX, cia.SubClass ')
    Left = 304
    Top = 84
    object QrSubAllAgrupaTudo: TFloatField
      FieldName = 'AgrupaTudo'
      Required = True
    end
    object QrSubAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSubAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSubAllAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSubAllAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSubAllMediaM2PC: TFloatField
      FieldName = 'MediaM2PC'
    end
    object QrSubAllGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSubAllSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrSubAllNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSubAllPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrSubAllPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      Calculated = True
    end
  end
  object frxDsSubAll: TfrxDBDataset
    UserName = 'frxDsSubAll'
    CloseDataSource = False
    FieldAliases.Strings = (
      'AgrupaTudo=AgrupaTudo'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'MediaM2PC=MediaM2PC'
      'GraGru1=GraGru1'
      'SubClass=SubClass'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercPc=PercPc'
      'PercM2=PercM2')
    DataSet = QrSubAll
    BCDToCurrency = False
    Left = 304
    Top = 132
  end
  object DsVSCacIts: TDataSource
    DataSet = QrVSCacIts
    Left = 140
    Top = 132
  end
end
