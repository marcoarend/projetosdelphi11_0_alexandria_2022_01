object FmVSMOEnvAvuGer: TFmVSMOEnvAvuGer
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-225 :: Gerenciamento de Frete Simples'
  ClientHeight = 529
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 433
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 370
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 433
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 209
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 76
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdNome
        end
        object Label34: TLabel
          Left = 709
          Top = 1
          Width = 32
          Height = 13
          Caption = 'IME-C:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSMOEnvAvu
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 136
          Top = 16
          Width = 569
          Height = 21
          Color = clWhite
          DataField = 'NO_Empresa'
          DataSource = DsVSMOEnvAvu
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 76
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'CFTMA_Empresa'
          DataSource = DsVSMOEnvAvu
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 708
          Top = 16
          Width = 64
          Height = 21
          DataField = 'VSVMI_MovimCod'
          DataSource = DsVSMOEnvAvu
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 2
        Top = 55
        Width = 780
        Height = 152
        Align = alClient
        Caption = ' CTe Cobran'#231'a de frete: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 776
          Height = 135
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label32: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            Enabled = False
          end
          object Label33: TLabel
            Left = 44
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label35: TLabel
            Left = 120
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label36: TLabel
            Left = 156
            Top = 4
            Width = 49
            Height = 13
            Caption = 'Num CF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label37: TLabel
            Left = 232
            Top = 4
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label38: TLabel
            Left = 12
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label39: TLabel
            Left = 220
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label40: TLabel
            Left = 324
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label42: TLabel
            Left = 672
            Top = 44
            Width = 45
            Height = 13
            Caption = '$ Total:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label43: TLabel
            Left = 472
            Top = 44
            Width = 81
            Height = 13
            Caption = 'Peso kg frete:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 572
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Cus. frete kg:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label45: TLabel
            Left = 268
            Top = 4
            Width = 84
            Height = 13
            Caption = 'Transportador:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label41: TLabel
            Left = 116
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label3: TLabel
            Left = 12
            Top = 84
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object DBEdit2: TDBEdit
            Left = 12
            Top = 20
            Width = 28
            Height = 21
            DataField = 'CFTMA_FatID'
            DataSource = DsVSMOEnvAvu
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 44
            Top = 20
            Width = 72
            Height = 21
            DataField = 'CFTMA_FatNum'
            DataSource = DsVSMOEnvAvu
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 120
            Top = 20
            Width = 32
            Height = 21
            DataField = 'CFTMA_SerCT'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 156
            Top = 20
            Width = 72
            Height = 21
            DataField = 'CFTMA_nCT'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object DBEdit6: TDBEdit
            Left = 232
            Top = 20
            Width = 32
            Height = 21
            DataField = 'CFTMA_nItem'
            DataSource = DsVSMOEnvAvu
            TabOrder = 4
          end
          object DBEdit7: TDBEdit
            Left = 268
            Top = 20
            Width = 56
            Height = 21
            DataField = 'CFTMA_Terceiro'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit8: TDBEdit
            Left = 328
            Top = 20
            Width = 442
            Height = 21
            DataField = 'NO_Transpor'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object DBEdit9: TDBEdit
            Left = 12
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMA_Pecas'
            DataSource = DsVSMOEnvAvu
            TabOrder = 7
          end
          object DBEdit10: TDBEdit
            Left = 116
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMA_PesoKg'
            DataSource = DsVSMOEnvAvu
            TabOrder = 8
          end
          object DBEdit11: TDBEdit
            Left = 220
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMA_AreaM2'
            DataSource = DsVSMOEnvAvu
            TabOrder = 9
          end
          object DBEdit12: TDBEdit
            Left = 324
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMA_AreaP2'
            DataSource = DsVSMOEnvAvu
            TabOrder = 10
          end
          object DBEdit13: TDBEdit
            Left = 472
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMA_PesTrKg'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 11
          end
          object DBEdit14: TDBEdit
            Left = 572
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMA_CusTrKg'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object DBEdit15: TDBEdit
            Left = 672
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMA_ValorT'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
          end
          object DBMemo1: TDBMemo
            Left = 12
            Top = 100
            Width = 645
            Height = 33
            DataField = 'Nome'
            DataSource = DsVSMOEnvAvu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 369
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10134
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dados CTe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 223
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-Is'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DBGVSMOEnvAVMI: TdmkDBGridZTO
      Left = 0
      Top = 209
      Width = 784
      Height = 136
      Align = alTop
      DataSource = DsVSMOEnvAVMI
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovIts'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorFrete'
          Title.Caption = '$ Frete'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 72
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 399
        Height = 32
        Caption = 'Gerenciamento de Frete Simples'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 399
        Height = 32
        Caption = 'Gerenciamento de Frete Simples'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 399
        Height = 32
        Caption = 'Gerenciamento de Frete Simples'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSMOEnvAvu: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSMOEnvAvuBeforeOpen
    AfterOpen = QrVSMOEnvAvuAfterOpen
    BeforeClose = QrVSMOEnvAvuBeforeClose
    AfterScroll = QrVSMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT mea.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,'
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_Terceiro'
      'FROM vsmoenvavu mea'
      'LEFT JOIN entidades emp ON emp.Codigo=mea.CFTMA_Empresa'
      'LEFT JOIN entidades ter ON ter.Codigo=mea.CFTMA_Terceiro'
      'WHERE mea.Codigo>0')
    Left = 180
    Top = 285
    object QrVSMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrVSMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrVSMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrVSMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrVSMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrVSMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrVSMOEnvAvuCFTMA_Pecas: TFloatField
      FieldName = 'CFTMA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvAvuCFTMA_PesoKg: TFloatField
      FieldName = 'CFTMA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvAvuCFTMA_AreaM2: TFloatField
      FieldName = 'CFTMA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAvuCFTMA_AreaP2: TFloatField
      FieldName = 'CFTMA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrVSMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvAvuNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrVSMOEnvAvuNO_Transpor: TWideStringField
      FieldName = 'NO_Transpor'
      Size = 100
    end
    object QrVSMOEnvAvuNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsVSMOEnvAvu: TDataSource
    DataSet = QrVSMOEnvAvu
    Left = 180
    Top = 333
  end
  object QrVSMOEnvAVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mei.*'
      'FROM vsmoenvavmi mei'
      'WHERE mei.VSMOEnvAvu>0'
      '')
    Left = 272
    Top = 285
    object QrVSMOEnvAVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField
      FieldName = 'VSMOEnvAvu'
    end
    object QrVSMOEnvAVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvAVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvAVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvAVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvAVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvAVMI: TDataSource
    DataSet = QrVSMOEnvAVMI
    Left = 272
    Top = 329
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 308
    Top = 428
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
