unit VSHisFchAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkMemo, Vcl.ComCtrls, UnProjGroup_Consts,
  dmkEditDateTimePicker;

type
  TFmVSHisFchAdd = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdVSMovIts: TdmkEdit;
    EdSerieFch: TdmkEdit;
    EdFicha: TdmkEdit;
    MeObserv: TdmkMemo;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSHisFch(Codigo: Integer);
  public
    { Public declarations }
    //FQrCab,
    FQrIts: TmySQLQuery;
    //FDsCab: TDataSource;
  end;

  var
  FmVSHisFchAdd: TFmVSHisFchAdd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmVSHisFchAdd.BtOKClick(Sender: TObject);
var
  DataHora, Nome, Observ: String;
  Codigo, VSMovIts, SerieFch, Ficha: Integer;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma TAG!') then
    Exit;
  if MyObjects.FIC(Trim(MeObserv.Text) = '', MeObserv, 'Informe um histórico!') then
    Exit;
  Codigo         := EdCodigo.ValueVariant;
  VSMovIts       := EdVSMovIts.ValueVariant;
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPDataHora.Date, EdDataHora.Text);
  Nome           := EdNome.Text;
  Observ         := MeObserv.Text;
  //
  Codigo := UMyMod.BPGS1I32('vshisfch', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vshisfch', False, [
  CO_FLD_TAB_VMI, 'SerieFch', 'Ficha',
  CO_DATA_HORA_GRL, 'Nome', 'Observ'], [
  'Codigo'], [
  VSMovIts, SerieFch, Ficha,
  DataHora, Nome, Observ], [
  Codigo], True) then
  begin
    ReopenVSHisFch(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      TPDataHora.Date := DModG.ObtemAgora();
      EdDataHora.Text := Geral.FDT(DModG.ObtemAgora(), 100);
      EdNome.Text     := '';
      MeObserv.Text   := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSHisFchAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSHisFchAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSHisFchAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmVSHisFchAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSHisFchAdd.ReopenVSHisFch(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
