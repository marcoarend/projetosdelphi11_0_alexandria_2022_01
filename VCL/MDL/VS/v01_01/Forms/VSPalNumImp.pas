unit VSPalNumImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums;

type
  TFmVSPalNumImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Ck1: TCheckBox;
    Ck2: TCheckBox;
    Ck3: TCheckBox;
    Ck4: TCheckBox;
    Ck5: TCheckBox;
    Ck6: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Boxes: array[1..6] of Integer;
    Pallets: array of Integer;
  end;

  var
  FmVSPalNumImp: TFmVSPalNumImp;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmVSPalNumImp.BtOKClick(Sender: TObject);
  procedure Adiciona();
  var
    I: Integer;
  begin
    I := Length(Pallets);
    SetLength(Pallets, I + 1);

  end;
begin
  SetLength(Pallets, 0);
  if Ck1.Checked then Adiciona()
end;

procedure TFmVSPalNumImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPalNumImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPalNumImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSPalNumImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
