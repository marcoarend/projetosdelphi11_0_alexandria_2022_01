unit VSCurCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects,
  UnProjGroup_Consts, UnVS_PF, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSCurCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSCurCab: TmySQLQuery;
    DsVSCurCab: TDataSource;
    QrVSCurAtu: TmySQLQuery;
    DsVSCurAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSCurCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSCurCabEmpresa: TIntegerField;
    QrVSCurCabDtHrAberto: TDateTimeField;
    QrVSCurCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSCurCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSCurOriIMEI: TmySQLQuery;
    DsVSCurOriIMEI: TDataSource;
    QrVSCurCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSCurCabLk: TIntegerField;
    QrVSCurCabDataCad: TDateField;
    QrVSCurCabDataAlt: TDateField;
    QrVSCurCabUserCad: TIntegerField;
    QrVSCurCabUserAlt: TIntegerField;
    QrVSCurCabAlterWeb: TSmallintField;
    QrVSCurCabAtivo: TSmallintField;
    QrVSCurCabPecasMan: TFloatField;
    QrVSCurCabAreaManM2: TFloatField;
    QrVSCurCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSCurCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSCurCabNO_TIPO: TWideStringField;
    QrVSCurCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSCurCabNO_DtHrLibOpe: TWideStringField;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSCurCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSCurCabCacCod: TIntegerField;
    QrVSCurCabGraGruX: TIntegerField;
    QrVSCurCabCustoManMOKg: TFloatField;
    QrVSCurCabCustoManMOTot: TFloatField;
    QrVSCurCabValorManMP: TFloatField;
    QrVSCurCabValorManT: TFloatField;
    QrVSCurCabDtHrLibOpe: TDateTimeField;
    QrVSCurCabDtHrCfgOpe: TDateTimeField;
    QrVSCurCabDtHrFimOpe: TDateTimeField;
    QrVSCurCabPecasSrc: TFloatField;
    QrVSCurCabAreaSrcM2: TFloatField;
    QrVSCurCabAreaSrcP2: TFloatField;
    QrVSCurCabPecasDst: TFloatField;
    QrVSCurCabAreaDstM2: TFloatField;
    QrVSCurCabAreaDstP2: TFloatField;
    QrVSCurCabPecasSdo: TFloatField;
    QrVSCurCabAreaSdoM2: TFloatField;
    QrVSCurCabAreaSdoP2: TFloatField;
    QrVSCurDst: TmySQLQuery;
    DsVSCurDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    AtualizaestoqueEmProcesso1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid1: TDBGrid;
    QrVSCurBxa: TmySQLQuery;
    DsVSCurBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSCurCabPesoKgSrc: TFloatField;
    QrVSCurCabPesoKgMan: TFloatField;
    QrVSCurCabPesoKgDst: TFloatField;
    QrVSCurCabPesoKgSdo: TFloatField;
    QrVSCurCabValorTMan: TFloatField;
    QrVSCurCabValorTSrc: TFloatField;
    QrVSCurCabValorTSdo: TFloatField;
    QrVSCurCabPecasINI: TFloatField;
    QrVSCurCabAreaINIM2: TFloatField;
    QrVSCurCabAreaINIP2: TFloatField;
    QrVSCurCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSCurCabPesoKgBxa: TFloatField;
    QrVSCurCabPecasBxa: TFloatField;
    QrVSCurCabAreaBxaM2: TFloatField;
    QrVSCurCabAreaBxaP2: TFloatField;
    QrVSCurCabValorTBxa: TFloatField;
    PCCurOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSCurOriPallet: TmySQLQuery;
    DsVSCurOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    otal2: TMenuItem;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdCustoMOM2: TdmkEdit;
    Label48: TLabel;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label51: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label52: TLabel;
    DBEdit41: TDBEdit;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrVSCurCabCliente: TIntegerField;
    QrVSCurCabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrVSCurCabNFeRem: TIntegerField;
    QrVSCurCabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrVSCurCabTemIMEIMrt: TIntegerField;
    QrVSCurAtuCodigo: TLargeintField;
    QrVSCurAtuControle: TLargeintField;
    QrVSCurAtuMovimCod: TLargeintField;
    QrVSCurAtuMovimNiv: TLargeintField;
    QrVSCurAtuMovimTwn: TLargeintField;
    QrVSCurAtuEmpresa: TLargeintField;
    QrVSCurAtuTerceiro: TLargeintField;
    QrVSCurAtuCliVenda: TLargeintField;
    QrVSCurAtuMovimID: TLargeintField;
    QrVSCurAtuDataHora: TDateTimeField;
    QrVSCurAtuPallet: TLargeintField;
    QrVSCurAtuGraGruX: TLargeintField;
    QrVSCurAtuPecas: TFloatField;
    QrVSCurAtuPesoKg: TFloatField;
    QrVSCurAtuAreaM2: TFloatField;
    QrVSCurAtuAreaP2: TFloatField;
    QrVSCurAtuValorT: TFloatField;
    QrVSCurAtuSrcMovID: TLargeintField;
    QrVSCurAtuSrcNivel1: TLargeintField;
    QrVSCurAtuSrcNivel2: TLargeintField;
    QrVSCurAtuSrcGGX: TLargeintField;
    QrVSCurAtuSdoVrtPeca: TFloatField;
    QrVSCurAtuSdoVrtPeso: TFloatField;
    QrVSCurAtuSdoVrtArM2: TFloatField;
    QrVSCurAtuObserv: TWideStringField;
    QrVSCurAtuSerieFch: TLargeintField;
    QrVSCurAtuFicha: TLargeintField;
    QrVSCurAtuMisturou: TLargeintField;
    QrVSCurAtuFornecMO: TLargeintField;
    QrVSCurAtuCustoMOKg: TFloatField;
    QrVSCurAtuCustoMOM2: TFloatField;
    QrVSCurAtuCustoMOTot: TFloatField;
    QrVSCurAtuValorMP: TFloatField;
    QrVSCurAtuDstMovID: TLargeintField;
    QrVSCurAtuDstNivel1: TLargeintField;
    QrVSCurAtuDstNivel2: TLargeintField;
    QrVSCurAtuDstGGX: TLargeintField;
    QrVSCurAtuQtdGerPeca: TFloatField;
    QrVSCurAtuQtdGerPeso: TFloatField;
    QrVSCurAtuQtdGerArM2: TFloatField;
    QrVSCurAtuQtdGerArP2: TFloatField;
    QrVSCurAtuQtdAntPeca: TFloatField;
    QrVSCurAtuQtdAntPeso: TFloatField;
    QrVSCurAtuQtdAntArM2: TFloatField;
    QrVSCurAtuQtdAntArP2: TFloatField;
    QrVSCurAtuNotaMPAG: TFloatField;
    QrVSCurAtuNO_PALLET: TWideStringField;
    QrVSCurAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSCurAtuNO_TTW: TWideStringField;
    QrVSCurAtuID_TTW: TLargeintField;
    QrVSCurAtuNO_FORNECE: TWideStringField;
    QrVSCurAtuReqMovEstq: TLargeintField;
    QrVSCurAtuCUSTO_M2: TFloatField;
    QrVSCurAtuCUSTO_P2: TFloatField;
    QrVSCurAtuNO_LOC_CEN: TWideStringField;
    QrVSCurAtuMarca: TWideStringField;
    QrVSCurAtuPedItsLib: TLargeintField;
    QrVSCurAtuStqCenLoc: TLargeintField;
    QrVSCurAtuNO_FICHA: TWideStringField;
    QrVSCurOriIMEICodigo: TLargeintField;
    QrVSCurOriIMEIControle: TLargeintField;
    QrVSCurOriIMEIMovimCod: TLargeintField;
    QrVSCurOriIMEIMovimNiv: TLargeintField;
    QrVSCurOriIMEIMovimTwn: TLargeintField;
    QrVSCurOriIMEIEmpresa: TLargeintField;
    QrVSCurOriIMEITerceiro: TLargeintField;
    QrVSCurOriIMEICliVenda: TLargeintField;
    QrVSCurOriIMEIMovimID: TLargeintField;
    QrVSCurOriIMEIDataHora: TDateTimeField;
    QrVSCurOriIMEIPallet: TLargeintField;
    QrVSCurOriIMEIGraGruX: TLargeintField;
    QrVSCurOriIMEIPecas: TFloatField;
    QrVSCurOriIMEIPesoKg: TFloatField;
    QrVSCurOriIMEIAreaM2: TFloatField;
    QrVSCurOriIMEIAreaP2: TFloatField;
    QrVSCurOriIMEIValorT: TFloatField;
    QrVSCurOriIMEISrcMovID: TLargeintField;
    QrVSCurOriIMEISrcNivel1: TLargeintField;
    QrVSCurOriIMEISrcNivel2: TLargeintField;
    QrVSCurOriIMEISrcGGX: TLargeintField;
    QrVSCurOriIMEISdoVrtPeca: TFloatField;
    QrVSCurOriIMEISdoVrtPeso: TFloatField;
    QrVSCurOriIMEISdoVrtArM2: TFloatField;
    QrVSCurOriIMEIObserv: TWideStringField;
    QrVSCurOriIMEISerieFch: TLargeintField;
    QrVSCurOriIMEIFicha: TLargeintField;
    QrVSCurOriIMEIMisturou: TLargeintField;
    QrVSCurOriIMEIFornecMO: TLargeintField;
    QrVSCurOriIMEICustoMOKg: TFloatField;
    QrVSCurOriIMEICustoMOM2: TFloatField;
    QrVSCurOriIMEICustoMOTot: TFloatField;
    QrVSCurOriIMEIValorMP: TFloatField;
    QrVSCurOriIMEIDstMovID: TLargeintField;
    QrVSCurOriIMEIDstNivel1: TLargeintField;
    QrVSCurOriIMEIDstNivel2: TLargeintField;
    QrVSCurOriIMEIDstGGX: TLargeintField;
    QrVSCurOriIMEIQtdGerPeca: TFloatField;
    QrVSCurOriIMEIQtdGerPeso: TFloatField;
    QrVSCurOriIMEIQtdGerArM2: TFloatField;
    QrVSCurOriIMEIQtdGerArP2: TFloatField;
    QrVSCurOriIMEIQtdAntPeca: TFloatField;
    QrVSCurOriIMEIQtdAntPeso: TFloatField;
    QrVSCurOriIMEIQtdAntArM2: TFloatField;
    QrVSCurOriIMEIQtdAntArP2: TFloatField;
    QrVSCurOriIMEINotaMPAG: TFloatField;
    QrVSCurOriIMEINO_PALLET: TWideStringField;
    QrVSCurOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSCurOriIMEINO_TTW: TWideStringField;
    QrVSCurOriIMEIID_TTW: TLargeintField;
    QrVSCurOriIMEINO_FORNECE: TWideStringField;
    QrVSCurOriIMEINO_SerieFch: TWideStringField;
    QrVSCurOriIMEIReqMovEstq: TLargeintField;
    QrVSCurOriPalletPallet: TLargeintField;
    QrVSCurOriPalletGraGruX: TLargeintField;
    QrVSCurOriPalletPecas: TFloatField;
    QrVSCurOriPalletAreaM2: TFloatField;
    QrVSCurOriPalletAreaP2: TFloatField;
    QrVSCurOriPalletPesoKg: TFloatField;
    QrVSCurOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSCurOriPalletNO_Pallet: TWideStringField;
    QrVSCurOriPalletSerieFch: TLargeintField;
    QrVSCurOriPalletFicha: TLargeintField;
    QrVSCurOriPalletNO_TTW: TWideStringField;
    QrVSCurOriPalletID_TTW: TLargeintField;
    QrVSCurOriPalletSeries_E_Fichas: TWideStringField;
    QrVSCurOriPalletTerceiro: TLargeintField;
    QrVSCurOriPalletMarca: TWideStringField;
    QrVSCurOriPalletNO_FORNECE: TWideStringField;
    QrVSCurOriPalletNO_SerieFch: TWideStringField;
    QrVSCurOriPalletValorT: TFloatField;
    QrVSCurOriPalletSdoVrtPeca: TFloatField;
    QrVSCurOriPalletSdoVrtPeso: TFloatField;
    QrVSCurOriPalletSdoVrtArM2: TFloatField;
    QrVSCurDstCodigo: TLargeintField;
    QrVSCurDstControle: TLargeintField;
    QrVSCurDstMovimCod: TLargeintField;
    QrVSCurDstMovimNiv: TLargeintField;
    QrVSCurDstMovimTwn: TLargeintField;
    QrVSCurDstEmpresa: TLargeintField;
    QrVSCurDstTerceiro: TLargeintField;
    QrVSCurDstCliVenda: TLargeintField;
    QrVSCurDstMovimID: TLargeintField;
    QrVSCurDstDataHora: TDateTimeField;
    QrVSCurDstPallet: TLargeintField;
    QrVSCurDstGraGruX: TLargeintField;
    QrVSCurDstPecas: TFloatField;
    QrVSCurDstPesoKg: TFloatField;
    QrVSCurDstAreaM2: TFloatField;
    QrVSCurDstAreaP2: TFloatField;
    QrVSCurDstValorT: TFloatField;
    QrVSCurDstSrcMovID: TLargeintField;
    QrVSCurDstSrcNivel1: TLargeintField;
    QrVSCurDstSrcNivel2: TLargeintField;
    QrVSCurDstSrcGGX: TLargeintField;
    QrVSCurDstSdoVrtPeca: TFloatField;
    QrVSCurDstSdoVrtPeso: TFloatField;
    QrVSCurDstSdoVrtArM2: TFloatField;
    QrVSCurDstObserv: TWideStringField;
    QrVSCurDstSerieFch: TLargeintField;
    QrVSCurDstFicha: TLargeintField;
    QrVSCurDstMisturou: TLargeintField;
    QrVSCurDstFornecMO: TLargeintField;
    QrVSCurDstCustoMOKg: TFloatField;
    QrVSCurDstCustoMOM2: TFloatField;
    QrVSCurDstCustoMOTot: TFloatField;
    QrVSCurDstValorMP: TFloatField;
    QrVSCurDstDstMovID: TLargeintField;
    QrVSCurDstDstNivel1: TLargeintField;
    QrVSCurDstDstNivel2: TLargeintField;
    QrVSCurDstDstGGX: TLargeintField;
    QrVSCurDstQtdGerPeca: TFloatField;
    QrVSCurDstQtdGerPeso: TFloatField;
    QrVSCurDstQtdGerArM2: TFloatField;
    QrVSCurDstQtdGerArP2: TFloatField;
    QrVSCurDstQtdAntPeca: TFloatField;
    QrVSCurDstQtdAntPeso: TFloatField;
    QrVSCurDstQtdAntArM2: TFloatField;
    QrVSCurDstQtdAntArP2: TFloatField;
    QrVSCurDstNotaMPAG: TFloatField;
    QrVSCurDstNO_PALLET: TWideStringField;
    QrVSCurDstNO_PRD_TAM_COR: TWideStringField;
    QrVSCurDstNO_TTW: TWideStringField;
    QrVSCurDstID_TTW: TLargeintField;
    QrVSCurDstNO_FORNECE: TWideStringField;
    QrVSCurDstNO_SerieFch: TWideStringField;
    QrVSCurDstReqMovEstq: TLargeintField;
    QrVSCurDstPedItsFin: TLargeintField;
    QrVSCurDstMarca: TWideStringField;
    QrVSCurBxaCodigo: TLargeintField;
    QrVSCurBxaControle: TLargeintField;
    QrVSCurBxaMovimCod: TLargeintField;
    QrVSCurBxaMovimNiv: TLargeintField;
    QrVSCurBxaMovimTwn: TLargeintField;
    QrVSCurBxaEmpresa: TLargeintField;
    QrVSCurBxaTerceiro: TLargeintField;
    QrVSCurBxaCliVenda: TLargeintField;
    QrVSCurBxaMovimID: TLargeintField;
    QrVSCurBxaDataHora: TDateTimeField;
    QrVSCurBxaPallet: TLargeintField;
    QrVSCurBxaGraGruX: TLargeintField;
    QrVSCurBxaPecas: TFloatField;
    QrVSCurBxaPesoKg: TFloatField;
    QrVSCurBxaAreaM2: TFloatField;
    QrVSCurBxaAreaP2: TFloatField;
    QrVSCurBxaValorT: TFloatField;
    QrVSCurBxaSrcMovID: TLargeintField;
    QrVSCurBxaSrcNivel1: TLargeintField;
    QrVSCurBxaSrcNivel2: TLargeintField;
    QrVSCurBxaSrcGGX: TLargeintField;
    QrVSCurBxaSdoVrtPeca: TFloatField;
    QrVSCurBxaSdoVrtPeso: TFloatField;
    QrVSCurBxaSdoVrtArM2: TFloatField;
    QrVSCurBxaObserv: TWideStringField;
    QrVSCurBxaSerieFch: TLargeintField;
    QrVSCurBxaFicha: TLargeintField;
    QrVSCurBxaMisturou: TLargeintField;
    QrVSCurBxaFornecMO: TLargeintField;
    QrVSCurBxaCustoMOKg: TFloatField;
    QrVSCurBxaCustoMOM2: TFloatField;
    QrVSCurBxaCustoMOTot: TFloatField;
    QrVSCurBxaValorMP: TFloatField;
    QrVSCurBxaDstMovID: TLargeintField;
    QrVSCurBxaDstNivel1: TLargeintField;
    QrVSCurBxaDstNivel2: TLargeintField;
    QrVSCurBxaDstGGX: TLargeintField;
    QrVSCurBxaQtdGerPeca: TFloatField;
    QrVSCurBxaQtdGerPeso: TFloatField;
    QrVSCurBxaQtdGerArM2: TFloatField;
    QrVSCurBxaQtdGerArP2: TFloatField;
    QrVSCurBxaQtdAntPeca: TFloatField;
    QrVSCurBxaQtdAntPeso: TFloatField;
    QrVSCurBxaQtdAntArM2: TFloatField;
    QrVSCurBxaQtdAntArP2: TFloatField;
    QrVSCurBxaNotaMPAG: TFloatField;
    QrVSCurBxaNO_PALLET: TWideStringField;
    QrVSCurBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSCurBxaNO_TTW: TWideStringField;
    QrVSCurBxaID_TTW: TLargeintField;
    QrVSCurBxaNO_FORNECE: TWideStringField;
    QrVSCurBxaNO_SerieFch: TWideStringField;
    QrVSCurBxaReqMovEstq: TLargeintField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    Label60: TLabel;
    DBEdit45: TDBEdit;
    N4: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    QrVSCurAtuNO_FORNEC_MO: TWideStringField;
    Localdoestoque1: TMenuItem;
    Cliente1: TMenuItem;
    QrVSCurDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSCurAtuClientMO: TLargeintField;
    EmitePesagem1: TMenuItem;
    N5: TMenuItem;
    QrVSCurOriIMEICustoPQ: TFloatField;
    TabSheet3: TTabSheet;
    QrEmit: TmySQLQuery;
    DsEmit: TDataSource;
    DBGrid4: TDBGrid;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    PB1: TProgressBar;
    QrVSCurAtuCUSTO_KG: TFloatField;
    Label31: TLabel;
    DBEdit24: TDBEdit;
    QrVSCurAtuCustoPQ: TFloatField;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    BtDst: TBitBtn;
    BtPesagem: TBitBtn;
    teste1: TMenuItem;
    BtReclasif: TBitBtn;
    CourosemprocessoBH1: TMenuItem;
    Especifico1: TMenuItem;
    Padrao1: TMenuItem;
    QrVSCurCabSerieRem: TSmallintField;
    Label57: TLabel;
    EdSerieRem: TdmkEdit;
    Label59: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Irparajaneladomovimento1: TMenuItem;
    QrVSCurOriIMEIJmpMovID: TLargeintField;
    QrVSCurOriIMEIJmpNivel1: TLargeintField;
    QrVSCurOriIMEIJmpNivel2: TLargeintField;
    QrVSCurCabGGXSrc: TIntegerField;
    Corrigepesodebaixa1: TMenuItem;
    PMPesagem: TPopupMenu;
    EmitePesagem2: TMenuItem;
    MenuItem1: TMenuItem;
    Reimprimereceita1: TMenuItem;
    N8: TMenuItem;
    ExcluiPesagem1: TMenuItem;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    N7: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    Recalculacusto1: TMenuItem;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    QrVSCurCabEmitGru: TIntegerField;
    TabSheet4: TTabSheet;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    QrVSCurOriIMEIRmsMovID: TLargeintField;
    QrVSCurOriIMEIRmsNivel1: TLargeintField;
    QrVSCurOriIMEIRmsNivel2: TLargeintField;
    AdicionaporMarcasemcontagemdepeas1: TMenuItem;
    ItsExcluiOriIMEI2: TMenuItem;
    QrEmitDtCorrApo_TXT: TWideStringField;
    QrEmitDtCorrApo: TDateTimeField;
    QrEmitDtaBaixa_TXT: TWideStringField;
    QrEmitDtaBaixa: TDateField;
    ReopenitensTeste1: TMenuItem;
    DBGrid5: TDBGrid;
    Splitter2: TSplitter;
    QrVSCurInd: TmySQLQuery;
    QrVSCurIndCodigo: TLargeintField;
    QrVSCurIndControle: TLargeintField;
    QrVSCurIndMovimCod: TLargeintField;
    QrVSCurIndMovimNiv: TLargeintField;
    QrVSCurIndMovimTwn: TLargeintField;
    QrVSCurIndEmpresa: TLargeintField;
    QrVSCurIndTerceiro: TLargeintField;
    QrVSCurIndCliVenda: TLargeintField;
    QrVSCurIndMovimID: TLargeintField;
    QrVSCurIndDataHora: TDateTimeField;
    QrVSCurIndPallet: TLargeintField;
    QrVSCurIndGraGruX: TLargeintField;
    QrVSCurIndPecas: TFloatField;
    QrVSCurIndPesoKg: TFloatField;
    QrVSCurIndAreaM2: TFloatField;
    QrVSCurIndAreaP2: TFloatField;
    QrVSCurIndValorT: TFloatField;
    QrVSCurIndSrcMovID: TLargeintField;
    QrVSCurIndSrcNivel1: TLargeintField;
    QrVSCurIndSrcNivel2: TLargeintField;
    QrVSCurIndSrcGGX: TLargeintField;
    QrVSCurIndSdoVrtPeca: TFloatField;
    QrVSCurIndSdoVrtPeso: TFloatField;
    QrVSCurIndSdoVrtArM2: TFloatField;
    QrVSCurIndObserv: TWideStringField;
    QrVSCurIndSerieFch: TLargeintField;
    QrVSCurIndFicha: TLargeintField;
    QrVSCurIndMisturou: TLargeintField;
    QrVSCurIndFornecMO: TLargeintField;
    QrVSCurIndCustoMOKg: TFloatField;
    QrVSCurIndCustoMOM2: TFloatField;
    QrVSCurIndCustoMOTot: TFloatField;
    QrVSCurIndValorMP: TFloatField;
    QrVSCurIndDstMovID: TLargeintField;
    QrVSCurIndDstNivel1: TLargeintField;
    QrVSCurIndDstNivel2: TLargeintField;
    QrVSCurIndDstGGX: TLargeintField;
    QrVSCurIndQtdGerPeca: TFloatField;
    QrVSCurIndQtdGerPeso: TFloatField;
    QrVSCurIndQtdGerArM2: TFloatField;
    QrVSCurIndQtdGerArP2: TFloatField;
    QrVSCurIndQtdAntPeca: TFloatField;
    QrVSCurIndQtdAntPeso: TFloatField;
    QrVSCurIndQtdAntArM2: TFloatField;
    QrVSCurIndQtdAntArP2: TFloatField;
    QrVSCurIndNotaMPAG: TFloatField;
    QrVSCurIndNO_PALLET: TWideStringField;
    QrVSCurIndNO_PRD_TAM_COR: TWideStringField;
    QrVSCurIndNO_TTW: TWideStringField;
    QrVSCurIndID_TTW: TLargeintField;
    QrVSCurIndNO_FORNECE: TWideStringField;
    QrVSCurIndNO_SerieFch: TWideStringField;
    QrVSCurIndReqMovEstq: TLargeintField;
    QrVSCurIndPedItsFin: TLargeintField;
    QrVSCurIndMarca: TWideStringField;
    QrVSCurIndStqCenLoc: TLargeintField;
    QrVSCurIndNO_MovimNiv: TWideStringField;
    QrVSCurIndNO_MovimID: TWideStringField;
    DsVSCurInd: TDataSource;
    SbStqCenLoc: TSpeedButton;
    Datadeabertura1: TMenuItem;
    TabSheet5: TTabSheet;
    DBGrid6: TDBGrid;
    QrCaleados: TmySQLQuery;
    QrCaleadosCodigo: TLargeintField;
    QrCaleadosControle: TLargeintField;
    QrCaleadosMovimCod: TLargeintField;
    QrCaleadosMovimNiv: TLargeintField;
    QrCaleadosMovimTwn: TLargeintField;
    QrCaleadosEmpresa: TLargeintField;
    QrCaleadosTerceiro: TLargeintField;
    QrCaleadosCliVenda: TLargeintField;
    QrCaleadosMovimID: TLargeintField;
    QrCaleadosDataHora: TDateTimeField;
    QrCaleadosPallet: TLargeintField;
    QrCaleadosGraGruX: TLargeintField;
    QrCaleadosPecas: TFloatField;
    QrCaleadosPesoKg: TFloatField;
    QrCaleadosAreaM2: TFloatField;
    QrCaleadosAreaP2: TFloatField;
    QrCaleadosValorT: TFloatField;
    QrCaleadosSrcMovID: TLargeintField;
    QrCaleadosSrcNivel1: TLargeintField;
    QrCaleadosSrcNivel2: TLargeintField;
    QrCaleadosSrcGGX: TLargeintField;
    QrCaleadosSdoVrtPeca: TFloatField;
    QrCaleadosSdoVrtPeso: TFloatField;
    QrCaleadosSdoVrtArM2: TFloatField;
    QrCaleadosObserv: TWideStringField;
    QrCaleadosSerieFch: TLargeintField;
    QrCaleadosFicha: TLargeintField;
    QrCaleadosMisturou: TLargeintField;
    QrCaleadosFornecMO: TLargeintField;
    QrCaleadosCustoMOKg: TFloatField;
    QrCaleadosCustoMOM2: TFloatField;
    QrCaleadosCustoMOTot: TFloatField;
    QrCaleadosValorMP: TFloatField;
    QrCaleadosDstMovID: TLargeintField;
    QrCaleadosDstNivel1: TLargeintField;
    QrCaleadosDstNivel2: TLargeintField;
    QrCaleadosDstGGX: TLargeintField;
    QrCaleadosQtdGerPeca: TFloatField;
    QrCaleadosQtdGerPeso: TFloatField;
    QrCaleadosQtdGerArM2: TFloatField;
    QrCaleadosQtdGerArP2: TFloatField;
    QrCaleadosQtdAntPeca: TFloatField;
    QrCaleadosQtdAntPeso: TFloatField;
    QrCaleadosQtdAntArM2: TFloatField;
    QrCaleadosQtdAntArP2: TFloatField;
    QrCaleadosNotaMPAG: TFloatField;
    QrCaleadosNO_PRD_TAM_COR: TWideStringField;
    QrCaleadosNO_TTW: TWideStringField;
    QrCaleadosID_TTW: TLargeintField;
    QrCaleadosNO_FORNECE: TWideStringField;
    QrCaleadosReqMovEstq: TLargeintField;
    QrCaleadosCustoPQ: TFloatField;
    QrCaleadosJmpMovID: TLargeintField;
    QrCaleadosJmpNivel1: TLargeintField;
    QrCaleadosJmpNivel2: TLargeintField;
    QrCaleadosRmsMovID: TLargeintField;
    QrCaleadosRmsNivel1: TLargeintField;
    QrCaleadosRmsNivel2: TLargeintField;
    DsCaleados: TDataSource;
    QrVSCurOriIMEIMarca: TWideStringField;
    Atualizacustos1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCurCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCurCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCurCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSCurCabBeforeClose(DataSet: TDataSet);
    procedure QrVSCurCabCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmProcesso1Click(Sender: TObject);
    procedure QrVSCurDstBeforeClose(DataSet: TDataSet);
    procedure QrVSCurDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure otal2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Localdoestoque1Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure EmitePesagem1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure Reimprimereceita1Click(Sender: TObject);
    procedure ExcluiPesagem1Click(Sender: TObject);
    procedure teste1Click(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure CourosemprocessoBH1Click(Sender: TObject);
    procedure Padrao1Click(Sender: TObject);
    procedure Especifico1Click(Sender: TObject);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure Corrigepesodebaixa1Click(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure AdicionaporMarcasemcontagemdepeas1Click(Sender: TObject);
    procedure ItsExcluiOriIMEI2Click(Sender: TObject);
    procedure ReopenitensTeste1Click(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure Datadeabertura1Click(Sender: TObject);
    procedure Atualizacustos1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSCurOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo;
              MarcaSemContarPeca: Boolean);
    procedure MostraVSCurOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSCurDst(SQLType: TSQLType);
    procedure InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX, CliVenda: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSCurOris(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
    procedure PesquisaArtigoJaFeito(Key: Word);
    procedure RecalculaCusto();
    procedure ReopenCaleados();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEmit();
    procedure ImprimeIMEI(Tipo: TXXImpImeiKind);
  end;

var
  FmVSCurCab: TFmVSCurCab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, ModuleGeral, VSCurDst, VSCurOriIMEI,
  AppListas, UnGrade_PF, ModVS, UnPQ_PF, UnEntities, ModVS_CRC, UnVS_CRC_PF,
  UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCurCab.Localdoestoque1Click(Sender: TObject);
begin
  if VS_PF.AlteraVMI_StqCenLoc(QrVSCurAtuControle.Value,
  QrVSCurAtuStqCenLoc.Value) then
    LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCurCab.MostraVSCurDst(SQLType: TSQLType);
  function CalculoValorM2(): Double;
  begin
    if QrVSCurAtuAreaM2.Value > 0 then
      Result := QrVSCurAtuValorT.Value / QrVSCurAtuAreaM2.Value
    else
      Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmVSCurDst, FmVSCurDst, afmoNegarComAviso) then
  begin
    FmVSCurDst.ImgTipo.SQLType := SQLType;
    FmVSCurDst.FQrCab := QrVSCurCab;
    FmVSCurDst.FDsCab := DsVSCurCab;
    FmVSCurDst.FQrIts := QrVSCurDst;
    //
    //FmVSCurDst.FDataHora    := QrVSCurCabDtHrAberto.Value;
    FmVSCurDst.FEmpresa     := QrVSCurCabEmpresa.Value;
    FmVSCurDst.FClientMO    := QrVSCurAtuClientMO.Value;
    FmVSCurDst.FValM2       := CalculoValorM2();
    //FmVSCurDst.FFatorIntSrc := QrVSCurAtuFatorInt.Value;
    FmVSCurDst.FGraGruXSrc  := QrVSCurAtuGraGruX.Value;
    //
    FmVSCurDst.FVmiPai := QrVSCurAtuControle.Value;
    //
    FmVSCurDst.EdCustoMOM2.ValueVariant := QrVSCurAtuCustoMOM2.Value;
    if SQLType = stIns then
    begin
      FmVSCurDst.FSdoPecas  := QrVSCurAtuSdoVrtPeca.Value;
      FmVSCurDst.FSdoPesoKg := QrVSCurAtuSdoVrtPeso.Value;
      FmVSCurDst.FSdoReaM2  := QrVSCurAtuSdoVrtArM2.Value;
      //
      //FmVSCurDst.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSCurDst.QrVSPedIts, QrVSCurAtuControle.Value,
      QrVSCurAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSCurDst.EdPedItsFin.ValueVariant := QrVSCurAtuPedItsLib.Value;
      FmVSCurDst.CBPedItsFin.KeyValue     := QrVSCurAtuPedItsLib.Value;
      //
      //FmVSCurDst.TPData.Date              := QrVSCurCabDtHrAberto.Value;
      //FmVSCurDst.EdHora.ValueVariant      := QrVSCurCabDtHrAberto.Value;
    end else
    begin
      FmVSCurDst.FSdoPecas  := 0;
      FmVSCurDst.FSdoPesoKg := 0;
      FmVSCurDst.FSdoReaM2  := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSCurDst.QrVSPedIts, QrVSCurAtuControle.Value,
       QrVSCurDstControle.Value);
      FmVSCurDst.EdCtrl1.ValueVariant      := QrVSCurDstControle.Value;
      //FmVSCurDst.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSCurDstMovimCod.Value, QrVSCurDstMovimTwn.Value, emidEmProcCur,
        //eminEmCurBxa);
      FmVSCurDst.EdMovimTwn.ValueVariant   := QrVSCurDstMovimTwn.Value;
      FmVSCurDst.EdGragruX.ValueVariant    := QrVSCurDstGraGruX.Value;
      FmVSCurDst.CBGragruX.KeyValue        := QrVSCurDstGraGruX.Value;
      FmVSCurDst.EdPecas.ValueVariant      := QrVSCurDstPecas.Value;
      FmVSCurDst.EdPesoKg.ValueVariant     := QrVSCurDstPesoKg.Value;
      FmVSCurDst.EdAreaM2.ValueVariant     := QrVSCurDstAreaM2.Value;
      FmVSCurDst.EdAreaP2.ValueVariant     := QrVSCurDstAreaP2.Value;
      FmVSCurDst.EdValorT.ValueVariant     := QrVSCurDstValorT.Value;
      FmVSCurDst.EdObserv.ValueVariant     := QrVSCurDstObserv.Value;
      FmVSCurDst.EdSerieFch.ValueVariant   := QrVSCurDstSerieFch.Value;
      FmVSCurDst.CBSerieFch.KeyValue       := QrVSCurDstSerieFch.Value;
      FmVSCurDst.EdFicha.ValueVariant      := QrVSCurDstFicha.Value;
      FmVSCurDst.EdMarca.ValueVariant      := QrVSCurDstMarca.Value;
      //FmVSCurDst.RGMisturou.ItemIndex    := QrVSCurDstMisturou.Value;
      FmVSCurDst.EdPedItsFin.ValueVariant  := QrVSCurDstPedItsFin.Value;
      FmVSCurDst.CBPedItsFin.KeyValue      := QrVSCurDstPedItsFin.Value;
      //
      FmVSCurDst.TPData.Date               := QrVSCurDstDataHora.Value;
      FmVSCurDst.EdHora.ValueVariant       := QrVSCurDstDataHora.Value;
      FmVSCurDst.EdPedItsFin.ValueVariant  := QrVSCurDstPedItsFin.Value;
      FmVSCurDst.CBPedItsFin.KeyValue      := QrVSCurDstPedItsFin.Value;
      FmVSCurDst.EdPallet.ValueVariant     := QrVSCurDstPallet.Value;
      FmVSCurDst.CBPallet.KeyValue         := QrVSCurDstPallet.Value;
      FmVSCurDst.EdStqCenLoc.ValueVariant  := QrVSCurDstStqCenLoc.Value;
      FmVSCurDst.CBStqCenLoc.KeyValue      := QrVSCurDstStqCenLoc.Value;
      FmVSCurDst.EdReqMovEstq.ValueVariant := QrVSCurDstReqMovEstq.Value;
      FmVSCurDst.EdCustoMOM2.ValueVariant  := QrVSCurDstCustoMOM2.Value;
      FmVSCurDst.EdCustoMOTot.ValueVariant := QrVSCurDstCustoMOTot.Value;
      FmVSCurDst.EdValorT.ValueVariant     := QrVSCurDstValorT.Value;
      //FmVSCurDst.EdRendimento.ValueVariant := QrVSCurDstRendimento.Value;
      //
      if QrVSCurDstSdoVrtPeca.Value < QrVSCurDstPecas.Value then
      begin
        FmVSCurDst.EdGraGruX.Enabled        := False;
        FmVSCurDst.CBGraGruX.Enabled        := False;
      end;
      if QrVSCurBxa.RecordCount > 0 then
      begin
        FmVSCurDst.CkBaixa.Checked          := True;
        FmVSCurDst.EdBxaPecas.ValueVariant  := -QrVSCurBxaPecas.Value;
        FmVSCurDst.EdBxaPesoKg.ValueVariant := -QrVSCurBxaPesoKg.Value;
        FmVSCurDst.EdBxaAreaM2.ValueVariant := -QrVSCurBxaAreaM2.Value;
        FmVSCurDst.EdBxaAreaP2.ValueVariant := -QrVSCurBxaAreaP2.Value;
        FmVSCurDst.EdBxaValorT.ValueVariant := -QrVSCurBxaValorT.Value;
        FmVSCurDst.EdCtrl2.ValueVariant     := QrVSCurBxaControle.Value;
        FmVSCurDst.EdBxaObserv.ValueVariant := QrVSCurBxaObserv.Value;
      end;
      if QrVSCurDstSdoVrtPeca.Value < QrVSCurDstPecas.Value then
      begin
        FmVSCurDst.EdGraGruX.Enabled  := False;
        FmVSCurDst.CBGraGruX.Enabled  := False;
        FmVSCurDst.EdSerieFch.Enabled := False;
        FmVSCurDst.CBSerieFch.Enabled := False;
        FmVSCurDst.EdFicha.Enabled    := False;
      end;
    end;
    //
    FmVSCurDst.LaBxaPesoKg.Enabled  := QrVSCurCabPesoKgINI.Value <> 0;
    FmVSCurDst.EdBxaPesoKg.Enabled  := QrVSCurCabPesoKgINI.Value <> 0;
    FmVSCurDst.LaBxaAreaM2.Enabled  := QrVSCurCabAreaINIM2.Value <> 0;
    FmVSCurDst.EdBxaAreaM2.Enabled  := QrVSCurCabAreaINIM2.Value <> 0;
    FmVSCurDst.LaBxaAreaP2.Enabled  := QrVSCurCabAreaINIM2.Value <> 0;
    FmVSCurDst.EdBxaAreaP2.Enabled  := QrVSCurCabAreaINIM2.Value <> 0;
    //
    FmVSCurDst.ShowModal;
    FmVSCurDst.Destroy;
    //
    VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCurCab.MostraVSCurOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo;
  MarcaSemContarPeca: Boolean);
begin
  if DBCheck.CriaFm(TFmVSCurOriIMEI, FmVSCurOriIMEI, afmoNegarComAviso) then
  begin
    FmVSCurOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSCurOriIMEI.FFornecMO := QrVSCurAtuFornecMO.Value;
    //FmVSCurOriIMEI.FClientMO := QrVSCurAtuClientMO.Value;
    FmVSCurOriIMEI.FQrCab          := QrVSCurCab;
    FmVSCurOriIMEI.FDsCab          := DsVSCurCab;
    //FmVSCurOriIMEI.FVSCurOriIMEIIMEI   := QrVSCurOriIMEIIMEI;
    //FmVSCurOriIMEI.FVSCurOriIMEIIMEIet := QrVSCurOriIMEIIMEIet;
    FmVSCurOriIMEI.FEmpresa        := QrVSCurCabEmpresa.Value;
    FmVSCurOriIMEI.FTipoArea       := QrVSCurCabTipoArea.Value;
    FmVSCurOriIMEI.FOrigMovimNiv   := QrVSCurAtuMovimNiv.Value;
    FmVSCurOriIMEI.FOrigMovimCod   := QrVSCurAtuMovimCod.Value;
    FmVSCurOriIMEI.FOrigCodigo     := QrVSCurAtuCodigo.Value;
    FmVSCurOriIMEI.FNewGraGruX     := QrVSCurAtuGraGruX.Value;
    //
    FmVSCurOriIMEI.EdCodigo.ValueVariant    := QrVSCurCabCodigo.Value;
    FmVSCurOriIMEI.EdMovimCod.ValueVariant  := QrVSCurCabMovimCod.Value;
    FmVSCurOriIMEI.FDataHora                := QrVSCurCabDtHrAberto.Value;
    //
    FmVSCurOriIMEI.EdSrcMovID.ValueVariant  := QrVSCurAtuMovimID.Value;
    FmVSCurOriIMEI.EdSrcNivel1.ValueVariant := QrVSCurAtuCodigo.Value;
    FmVSCurOriIMEI.EdSrcNivel2.ValueVariant := QrVSCurAtuControle.Value;
    FmVSCurOriIMEI.EdSrcGGX.ValueVariant    := QrVSCurAtuGraGruX.Value;
    //
    FmVSCurOriIMEI.FVmiPai := QrVSCurAtuControle.Value;
    FmVSCurOriIMEI.ReopenItensAptos();
    FmVSCurOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSCurOriIMEI.EdControle.ValueVariant := QrVSCurCabOldControle.Value;
      //
      FmVSCurOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSCurCabOldCNPJ_CPF.Value);
      FmVSCurOriIMEI.EdNomeEmiSac.Text := QrVSCurCabOldNome.Value;
      FmVSCurOriIMEI.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        //FmVSCurOriIMEI.FParcial := True;
        FmVSCurOriIMEI.FFormaBaixa       := frmabxaParcial;
        FmVSCurOriIMEI.DBG04Estq.Options := FmVSCurOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        if MarcaSemContarPeca then
        begin
          FmVSCurOriIMEI.PnMarcaSemContarPecas.Visible := True;
          FmVSCurOriIMEI.FFormaBaixa       := frmabxaMarcaSemContar;
        end else
          FmVSCurOriIMEI.FFormaBaixa       := frmabxaTotal;
        //
        FmVSCurOriIMEI.DBG04Estq.Options := FmVSCurOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSCurOriIMEI.FFormaBaixa       := frmabxaIndef;
        FmVSCurOriIMEI.DBG04Estq.Options := FmVSCurOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSCurOriIMEI.ShowModal;
    FmVSCurOriIMEI.Destroy;
    VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCurCab.MostraVSCurOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
{
  if DBCheck.CriaFm(TFmVSCurOriPall, FmVSCurOriPall, afmoNegarComAviso) then
  begin
    FmVSCurOriPall.ImgTipo.SQLType := SQLType;
    FmVSCurOriPall.FQrCab          := QrVSCurCab;
    FmVSCurOriPall.FDsCab          := DsVSCurCab;
    //FmVSCurOriPall.FVSCurOriPallIMEI   := QrVSCurOriPallIMEI;
    //FmVSCurOriPall.FVSCurOriPallPallet := QrVSCurOriPallPallet;
    FmVSCurOriPall.FEmpresa        := QrVSCurCabEmpresa.Value;
    FmVSCurOriPall.FClientMO       := QrVSCurAtuClientMO.Value;
    FmVSCurOriPall.FTipoArea       := QrVSCurCabTipoArea.Value;
    FmVSCurOriPall.FOrigMovimNiv   := QrVSCurAtuMovimNiv.Value;
    FmVSCurOriPall.FOrigMovimCod   := QrVSCurAtuMovimCod.Value;
    FmVSCurOriPall.FOrigCodigo     := QrVSCurAtuCodigo.Value;
    FmVSCurOriPall.FNewGraGruX     := QrVSCurAtuGraGruX.Value;
    //
    FmVSCurOriPall.EdCodigo.ValueVariant    := QrVSCurCabCodigo.Value;
    FmVSCurOriPall.EdMovimCod.ValueVariant  := QrVSCurCabMovimCod.Value;
    FmVSCurOriPall.FDataHora                := QrVSCurCabDtHrAberto.Value;
    //
    FmVSCurOriPall.EdSrcMovID.ValueVariant  := QrVSCurAtuMovimID.Value;
    FmVSCurOriPall.EdSrcNivel1.ValueVariant := QrVSCurAtuCodigo.Value;
    FmVSCurOriPall.EdSrcNivel2.ValueVariant := QrVSCurAtuControle.Value;
    FmVSCurOriPall.EdSrcGGX.ValueVariant    := QrVSCurAtuGraGruX.Value;
    //
    FmVSCurOriPall.ReopenItensAptos();
    FmVSCurOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
(*
      FmVSCurOriPall.EdControle.ValueVariant := QrVSCurCabOldControle.Value;
      //
      FmVSCurOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSCurCabOldCNPJ_CPF.Value);
      FmVSCurOriPall.EdNomeEmiSac.Text := QrVSCurCabOldNome.Value;
      FmVSCurOriPall.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSCurOriPall.FParcial := True;
        FmVSCurOriPall.DBG04Estq.Options := FmVSCurOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSCurOriPall.FParcial := False;
        FmVSCurOriPall.DBG04Estq.Options := FmVSCurOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSCurOriPall.FParcial := False;
        FmVSCurOriPall.DBG04Estq.Options := FmVSCurOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSCurOriPall.ShowModal;
    FmVSCurOriPall.Destroy;
    VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmVSCurCab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = False;
var
  Codigo: Integer;
begin
  PCCurOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSCurCabMovimCod.Value,
  QrVSCurCabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSCurCabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
end;

procedure TFmVSCurCab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
  PCCurOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
end;

procedure TFmVSCurCab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
  VS_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmProcCur], True, False, '', 0);
end;

procedure TFmVSCurCab.otal1Click(Sender: TObject);
begin
  MostraVSCurOriPall(stIns, ptTotal);
end;

procedure TFmVSCurCab.otal2Click(Sender: TObject);
const
  bMarcaSemContarPeca = False;
begin
  MostraVSCurOriIMEI(stIns, ptTotal, bMarcaSemContarPeca);
end;

procedure TFmVSCurCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSCurCab.Padrao1Click(Sender: TObject);
begin
  ImprimeIMEI(viikOrdemOperacao);
end;

procedure TFmVSCurCab.Parcial1Click(Sender: TObject);
begin
  MostraVSCurOriPall(stIns, ptParcial);
end;

procedure TFmVSCurCab.Parcial2Click(Sender: TObject);
const
  bMarcaSemContarPeca = False;
begin
  MostraVSCurOriIMEI(stIns, ptParcial, bMarcaSemContarPeca);
end;

procedure TFmVSCurCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCurCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCurCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEC(emidEmProcCur);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCurCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEI(emidEmProcCur);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCurCab.PesquisaArtigoJaFeito(Key: Word);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  if Key = VK_F4 then
  begin
    Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCur, [eminEmCurInn], stCpy,
    Codigo, Controle, False);
    //
    if (Controle <> 0) and (DmModVS.QrIMEI.State <> dsInactive) then
    begin
      EdGraGruX.ValueVariant   := DmModVS.QrIMEIGraGruX.Value;
      CBGraGruX.KeyValue       := DmModVS.QrIMEIGraGruX.Value;
      EdFornecMO.ValueVariant  := DmModVS.QrIMEIFornecMO.Value;
      CBFornecMO.KeyValue      := DmModVS.QrIMEIFornecMO.Value;
      EdStqCenLoc.ValueVariant := DmModVS.QrIMEIStqCenLoc.Value;
      CBStqCenLoc.KeyValue     := DmModVS.QrIMEIStqCenLoc.Value;
      EdCustoMOM2.ValueVariant := DmModVS.QrIMEICustoMOM2.Value;
      EdCliente.ValueVariant   := DmModVS.QrIMEICliVenda.Value;
      CBCliente.KeyValue       := DmModVS.QrIMEICliVenda.Value;
    end;
  end;
end;

procedure TFmVSCurCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCurCab);
  MyObjects.HabilitaMenuItemCabDelC1I3(CabExclui1, QrVSCurCab, QrVSCurOriIMEI,
    QrEmit, QrVSCurDst);
  DataDeAbertura1.Enabled := (QrVSCurCabDtHrFimOpe.Value > 0) and
    (QrVSCurCabDtHrAberto.Value > QrVSCurCabDtHrFimOpe.Value);
end;

procedure TFmVSCurCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSCurCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSCurDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSCurDst);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSCurDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSCurDst);
  if (QrVSCurCabDtHrLibOpe.Value > 2) or (QrVSCurCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
  VS_PF.HabilitaComposVSAtivo(QrVSCurDstID_TTW.Value, [ItsExcluiDst]);
end;

procedure TFmVSCurCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSCurCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSCurOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSCurOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSCurOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI2, QrVSCurOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSCurOriPallet);
  MyObjects.HabilitaMenuItemItsDel(Corrigepesodebaixa1, QrVSCurOriIMEI);
  if (QrVSCurCabDtHrLibOpe.Value > 2) or (QrVSCurCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  case PCCurOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCCurOri.ActivePageIndex = 1);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSCurOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_PF.HabilitaComposVSAtivo(QrVSCurOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  ItsExcluiOriIMEI2.Enabled := ItsExcluiOriIMEI2.Enabled and (ItsExcluiOriIMEI.Enabled = False);
end;

procedure TFmVSCurCab.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSCurCab);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSCurCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSCurCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCurCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCurCab.DefParams;
begin
  VAR_GOTOTABELA := 'vscurcab';
  VAR_GOTOMYSQLTABLE := QrVSCurCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(PecasMan<>0, PecasMan, -PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(AreaManM2<>0, AreaManM2, -AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(AreaManP2<>0, AreaManP2, -AreaSrcM2) AreaINIP2,');
  VAR_SQLx.Add('IF(PesoKgMan<>0, PesoKgMan, -PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  //VAR_SQLx.Add('IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_ClienteMO, ');
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vscurcab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  //VAR_SQLx.Add('LEFT JOIN entidades cmo ON cmo.Codigo=voc.ClienteMO');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSCurCab.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCurCab.EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCurCab.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCurCab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCCurOri.ActivePageIndex := 3;
end;

procedure TFmVSCurCab.EmitePesagem1Click(Sender: TObject);
begin
  PQ_PF.MostraFormVSFormulasImp_BH(QrVSCurCabMovimCod.Value,
    QrVSCurAtuControle.Value, QrVSCurCabTemIMEIMrt.Value,
    eminEmCurInn, eminSorcCur, recribsetCurtimento);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
  LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.EmitePesagem2Click(Sender: TObject);
begin
  PCCurOri.ActivePageIndex := 2;
end;

procedure TFmVSCurCab.Especifico1Click(Sender: TObject);
begin
  ImprimeIMEI(viikProcCurt);
end;

procedure TFmVSCurCab.Excluigrupo1Click(Sender: TObject);
begin
  PCCurOri.ActivePageIndex := 3;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
end;

procedure TFmVSCurCab.ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, MovimID: Integer;
  SrcNivel2: Integer;
  Qry: TmySQLQuery;
begin
  if ExigeSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  //
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrVSCurCabCodigo.Value;
    CtrlDel   := QrVSCurOriIMEIControle.Value;
    CtrlOri   := QrVSCurOriIMEISrcNivel2.Value;
    MovimID   := QrVSCurAtuMovimID.Value;
    MovimNiv  := QrVSCurAtuMovimNiv.Value;
    MovimCod  := QrVSCurAtuMovimCod.Value;
    SrcNivel2 := QrVSCurOriIMEISrcNivel2.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti145), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vscurcab', MovimCod);
      VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
      // Atualiza estoque do couro em caleiro
      VS_PF.AtualizaSaldoItmCal(SrcNivel2);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        if QrVSCurOriIMEIJmpNivel2.Value <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE JmpMovID=' + Geral.FF0(MovimID),
          'AND JmpNivel1=' + Geral.FF0(Codigo),
          'AND JmpNivel2=' + Geral.FF0(CtrlDel),
          '']);
          Qry.First;
          while not Qry.Eof do
          begin
            CtrlDel := Qry.FieldByName('Controle').AsInteger;
            VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti145), Dmod.QrUpd, Dmod.MyDB,
            CO_MASTER);
            //
            Qry.Next;
          end;
          //
          CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCurOriIMEI,
            TIntegerField(QrVSCurOriIMEIControle), QrVSCurOriIMEIControle.Value);
        end;
        //
        if QrVSCurOriIMEIRmsNivel2.Value <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE RmsMovID=' + Geral.FF0(MovimID),
          'AND RmsNivel1=' + Geral.FF0(Codigo),
          'AND RmsNivel2=' + Geral.FF0(CtrlDel),
          '']);
          Qry.First;
          while not Qry.Eof do
          begin
            CtrlDel := Qry.FieldByName('Controle').AsInteger;
            VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti145), Dmod.QrUpd, Dmod.MyDB,
            CO_MASTER);
            //
            Qry.Next;
          end;
          //
        end;
      finally
        Qry.Free;
      end;
      //
      VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSCurOris(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSCurCab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
  PCCurOri.ActivePageIndex := 3;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
end;

procedure TFmVSCurCab.ExcluiPesagem1Click(Sender: TObject);
begin
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, PB1) then
  begin
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
    VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
    LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
  end;
end;

procedure TFmVSCurCab.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCurCab.Cliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSCurCabCodigo.Value;
  if VS_PF.AlteraVMI_CliVenda(QrVSCurAtuControle.Value,
  QrVSCurAtuCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurcab', False, [
    'Cliente'], [
    'Codigo'], [
    QrVSCurAtuCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSCurCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSCurAtuControle.Value;
  MovimCod    := QrVSCurCabMovimCod.Value;
  Codigo      := QrVSCurCabCodigo.Value;
  MovimNivSrc := eminSorcCur;
  MovimNivDst := eminDestCur;
  VS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCurCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSCurCab.Corrigepesodebaixa1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  //
(*
  if VS_PF.AlteraVMI_PesoKg('QtdGerPeso', QrVSCurDstMovimID.Value,
  QrVSCurDstMovimNiv.Value, QrVSCurDstControle.Value,
  QrVSCurDstQtdGerPeso.Value, siNegativo) then
    LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
*)
  if VS_PF.AlteraVMI_PesoKg('PesoKg', QrVSCurDstMovimID.Value,
  QrVSCurDstMovimNiv.Value, QrVSCurDstControle.Value,
  QrVSCurDstPesoKg.Value, siNegativo) then
    LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.CorrigirFornecedor1Click(Sender: TObject);
begin
  VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
  LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.CourosemprocessoBH1Click(Sender: TObject);
begin
  // ini 2023-12-31
  //VS_PF.ImprimeVSEmProcBH();
  VS_Jan.MostraFormVSImpEmProcBH();
  // fim 2023-12-31
end;

procedure TFmVSCurCab.ImprimeIMEI(Tipo: TXXImpImeiKind);
var
  NFeRem: String;
begin
  if QrVSCurCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSCurCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSCurAtuControle.Value], Tipo, QrVSCurCabLPFMO.Value,
    NFeRem, QrVSCurCab);
end;

procedure TFmVSCurCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSCurOriIMEIControle.Value,
    QrVSCurOriIMEIReqMovEstq.Value, QrVSCurOriIMEI);
end;

procedure TFmVSCurCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSCurDstControle.Value,
    QrVSCurDstReqMovEstq.Value, QrVSCurDst);
end;

procedure TFmVSCurCab.InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
GraGruX, CliVenda: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOM2, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmProcCur;
  MovimNiv       := eminEmCurInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei010(*Cabe�alho de processo de curtimento*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('vscurcab', MovimCod);
    VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
    if GBVSPedIts.Visible then
      VS_PF.AtualizaVSPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;

procedure TFmVSCurCab.Irparajaneladomovimento1Click(Sender: TObject);
begin
  if (QrVSCurDst.State <> dsInactive) and (QrVSCurDst.RecordCount > 0) then
    VS_PF.MostraFormVS_Do_IMEI(QrVSCurDstControle.Value);
end;

procedure TFmVSCurCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrVSCurDstPallet.Value);
end;

procedure TFmVSCurCab.ItsAlteraDstClick(Sender: TObject);
begin
  MostraVSCurDst(stUpd);
end;

procedure TFmVSCurCab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraVSGArtOri(stUpd);
end;

procedure TFmVSCurCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSCurCabCodigo.Value;
  VS_PF.ExcluiCabEIMEI_OpeCab(QrVSCurCabMovimCod.Value,
    QrVSCurAtuControle.Value, emidEmProcCur, TEstqMotivDel.emtdWetCurti145);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSCurCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSCurCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCurCab.RecalculaCusto;
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
  LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.Recalculacusto1Click(Sender: TObject);
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
  LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.Reimprimereceita1Click(Sender: TObject);
begin
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TReceitaTipoSetor.rectipsetrRibeira, PB1);
end;

procedure TFmVSCurCab.ReopenCaleados();
const
  TemIMEIMrt = 1;
(*
procedure TUnProjGroup_PF.ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_CMO(),
  '-1.000 FatorImp ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_CMO(),
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE JmpNivel1=' + Geral.FF0(QrVSCurCabCodigo.Value),
  'AND JmpMovID=' + Geral.FF0(Integer(emidEmProcCur)), // 27
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestCal)), // 31
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaleados, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
end;

procedure TFmVSCurCab.ReopenEmit();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0(QrVSCurCabMovimCod.Value),
  '']);
end;

procedure TFmVSCurCab.ReopenitensTeste1Click(Sender: TObject);
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSCurAtu, QrVSCurOriIMEI, QrVSCurOriPallet,
  QrVSCurDst, QrVSCurInd, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  QrEmit, QrPQO, QrVSCurCabCodigo.Value,
  QrVSCurCabMovimCod.Value, QrVSCurCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmProcCur, emidIndsXX, emidEmProcCur,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmCurInn, eminSorcCur, eminDestCur);
end;

procedure TFmVSCurCab.ReopenVSCurOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //VS_PF.ReopenVSOpeOriIMEI(QrVSCurOriIMEI, QrVSCurCabMovimCod.Value, Controle);
  TemIMEIMrt := QrVSCurCabTemIMEIMrt.Value;
(*
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCurCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCurOriIMEI, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  QrVSCurOriIMEI.Locate('Controle', Controle, []);
*)
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVSCurOriIMEI, QrVSCurCabMovimCod.Value, Controle,
  TemIMEIMrt, eminSorcCur, SQL_Limit);
  //
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCurCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCurOriPallet, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSCurOriPallet.Locate('Pallet', Pallet, []);
end;

procedure TFmVSCurCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
(*
  if QrVSCurAtuSdoVrtPeca.Value < QrVSCurAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSCurCabCodigo.Value;
    CtrlDel  := QrVSCurDstControle.Value;
    CtrlDst  := QrVSCurDstSrcNivel2.Value;
    MovimNiv := QrVSCurAtuMovimNiv.Value;
    MovimCod := QrVSCurAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti145), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSCurDstMovimCod.Value, QrVSCurDstMovimTwn.Value, emidEmProcCur,
        eminEmCurBxa);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti145), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCurDst,
        TIntegerField(QrVSCurDstControle), QrVSCurDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vscurcab', MovimCod);
      VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      //
      VS_PF.ReopenVSOpePrcDst(QrVSCurDst, QrVSCurCabMovimCod.Value, CtrlNxt,
      QrVSCurCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestCur);
    end;
  end;
end;

procedure TFmVSCurCab.ItsExcluiOriIMEI2Click(Sender: TObject);
begin
  ExcluiImeiDeOrigem(True);
end;

procedure TFmVSCurCab.ItsExcluiOriIMEIClick(Sender: TObject);
begin
  ExcluiImeiDeOrigem(False);
end;

procedure TFmVSCurCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if MyObjects.FIC(QrVSCurOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrVSCurAtuSdoVrtPeca.Value < QrVSCurAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSCurOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSCurOriIMEI.First;
    while not QrVSCurOriIMEI.Eof do
    begin
      if QrVSCurOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSCurOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSCurOriIMEI.First;
        while not QrVSCurOriIMEI.Eof do
        begin
          if QrVSCurOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSCurCabCodigo.Value;
            CtrlDel   := QrVSCurOriIMEIControle.Value;
            CtrlOri   := QrVSCurOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSCurAtuMovimNiv.Value;
            MovimCod  := QrVSCurAtuMovimCod.Value;
            //
            if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti145),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_PF.AtualizaTotaisVSXxxCab('vscurcab', MovimCod);
              VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
            end;
            //
          end;
          QrVSCurOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCurOriIMEI,
      TIntegerField(QrVSCurOriIMEIControle), QrVSCurOriIMEIControle.Value);
      //
      VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSCurOris(CtrlNxt, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSCurCab.Datadeabertura1Click(Sender: TObject);
var
  Data, Hora: TDateTime;
  Codigo: Integer;
  DtHrAberto: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  if not DBCheck.ObtemData(Date, Data, Date, Hora, True) then Exit;
  DtHrAberto := Geral.FDT(Data, 109);
  //
  Codigo := QrVSCurCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurcab', False, [
  'DtHrAberto'], ['Codigo'], [DtHrAberto], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCurCab.DefineONomeDoForm;
begin
end;

procedure TFmVSCurCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCurCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCurCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCurCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCurCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCurCab.teste1Click(Sender: TObject);
var
  SrcNivel2: Integer;
begin
  SrcNivel2 := QrVSCurOriIMEISrcNivel2.Value;
  VS_PF.AtualizaSaldoItmCal(SrcNivel2);
end;

procedure TFmVSCurCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCurCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCurCabCodigo.Value;
  Close;
end;

procedure TFmVSCurCab.ItsIncluiDstClick(Sender: TObject);
begin
  MostraVSCurDst(stIns);
end;

procedure TFmVSCurCab.CabAltera1Click(Sender: TObject);
var
  Empresa(*, VSPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCurCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vscurcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSCurCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrVSCurAtuClientMO.Value;
  CBClientMO.KeyValue        := QrVSCurAtuClientMO.Value;
  //
  EdCliente.ValueVariant    := QrVSCurCabCliente.Value;
  CBCliente.KeyValue        := QrVSCurCabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrVSCurCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSCurCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSCurCabLPFMO.Value;
  //
  EdControle.ValueVariant   := QrVSCurAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSCurAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSCurAtuGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrVSCurAtuCustoMOTot.Value;
  EdCustoMOM2.ValueVariant  := QrVSCurAtuCustoMOM2.Value;
  EdPecas.ValueVariant      := QrVSCurAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSCurAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSCurAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSCurAtuObserv.Value;
  //
  VS_PF.ReopenPedItsXXX(QrVSPedIts, QrVSCurAtuControle.Value, QrVSCurAtuControle.Value);
  Habilita := QrVSCurAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSCurAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSCurAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSCurAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSCurAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSCurAtuReqMovEstq.Value;
  //
  EdFornecMO.ValueVariant   := QrVSCurAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSCurAtuFornecMO.Value;
end;

procedure TFmVSCurCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, GGXDst,
  Forma, Quant, Cliente, CliVenda, StqCenLoc, SerieRem, NFeRem: Integer;
  Nome, DtHrAberto, DataHora, LPFMO: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  //
  if not VS_PF.ValidaCampoNF(8, EdNFeRem, True) then Exit;
  //
  GGXDst := GraGruX;
  Codigo := UMyMod.BPGS1I32('vscurcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscurcab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', 'GGXDst'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, GGXDst], [
  Codigo], True) then
  begin
    if SQLType = stIns then
    begin
      VS_PF.InsereVSMovCab(MovimCod, emidEmProcCur, Codigo);
      VS_PF.CriaVMIJmpCur(Codigo, Empresa, DtHrAberto);
    end else
    begin
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO, GraGruX, Cliente, DtHrAberto);

    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSCurOriIMEI.RecordCount = 0) then
    begin
      //MostraVSCurOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      //'Pallet Total', 'Pallet Parcial',
      'IME-I Total', 'IME-I Parcial', 'Por Marca sem contagem de pe�as'], -1);
      case Forma of
        //0: MostraVSCurOriPall(stIns, ptTotal);
        //1: MostraVSCurOriPall(stIns, ptParcial);
        0: MostraVSCurOriIMEI(stIns, ptTotal, False);
        1: MostraVSCurOriIMEI(stIns, ptParcial, False);
        2: MostraVSCurOriIMEI(stIns, ptTotal, True);
      end;
    end;
    VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
  end;
end;

procedure TFmVSCurCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscurcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscurcab', 'Codigo');
end;

procedure TFmVSCurCab.BtDstClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMDst, BtDst);

end;

procedure TFmVSCurCab.BtOriClick(Sender: TObject);
begin
  PCCurOri.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSCurCab.BtPesagemClick(Sender: TObject);
begin
  if not PCCurOri.ActivePageIndex in [2,3] then
    PCCurOri.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSCurCab.BtReclasifClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmVSCurCab.AdicionaporMarcasemcontagemdepeas1Click(Sender: TObject);
const
  bMarcaSemContarPeca = True;
begin
  MostraVSCurOriIMEI(stIns, ptTotal, bMarcaSemContarPeca);
end;

procedure TFmVSCurCab.Atualizacustos1Click(Sender: TObject);
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
end;

procedure TFmVSCurCab.AtualizaestoqueEmProcesso1Click(Sender: TObject);
begin
  VS_PF.AtualizaTotaisVSCurCab(QrVSCurCabMovimCod.Value);
  LocCod(QrVSCurCabCodigo.Value, QrVSCurCabCodigo.Value);
end;

procedure TFmVSCurCab.AtualizaFornecedoresDeDestino;
var
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vscurcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSCurCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      if not FAtualizando then
        Qry1.Last
      else
      begin
        Codigo := Qry1.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_PF.AtualizaFornecedorCur(QrVSCurCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCurCabMovimCod.Value);
        VS_PF.AtualizaVSCurCabGGxSrc(QrVSCurCabMovimCod.Value);
        //
      end;
      Qry1.Next;
    end;
  finally
    Qry1.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSCurCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSCurCab, QrVSCurCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCurCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  QrVSCurCab.Database  := Dmod.MyDB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PCCurOri.ActivePageIndex := 1;
  //GBEdita.Align := alClient;
  PnDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1707_VSProCur));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidEmProcCur);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
end;

procedure TFmVSCurCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSCurCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCurCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSCurCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSCurCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSCurCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCurCab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSCurCab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSCurCab.QrVSCurCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCurCab.QrVSCurCabAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSCurAtu, QrVSCurCabMovimCod.Value, 0,
  QrVSCurCabTemIMEIMrt.Value, eminEmCurInn);
  //VS_PF.ReopenVSCurOri(QrVSCurOri, QrVSCurCabMovimCod.Value, 0);
  ReopenVSCurOris(0, 0);
  VS_PF.ReopenVSOpePrcDst(QrVSCurDst, QrVSCurCabMovimCod.Value, 0,
  QrVSCurCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestCur);
  //ReopenForcados(0);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(QrForcados, 0, QrVSCurCabCodigo.Value,
    QrVSCurAtuControle.Value, QrVSCurCabTemIMEIMrt.Value, emidEmProcCur);
  ReopenEmit();
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSCurCabMovimCod.Value, 0);
  //
  ReopenCaleados();
end;

procedure TFmVSCurCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCurCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCurCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCur, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //ReopenVSCurIts(Controle);
  end;
end;

procedure TFmVSCurCab.SbStqCenLocClick(Sender: TObject);
begin
  VS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidEmProcCur);
end;

procedure TFmVSCurCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCurCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSCurCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  if QrGraGruX.RecordCount > 1 then
  begin
    EdGraGruX.ValueVariant    := 0;
    CBGraGruX.KeyValue        := 0;
  end;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  if QrPrestador.RecordCount > 1 then
  begin
    EdFornecMO.ValueVariant   := 0;
    CBFornecMO.KeyValue       := 0;
  end;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCurCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vscurcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  //TPData.Date := Agora;
  //EdHora.ValueVariant := Agora;
  VS_PF.DefineDataHoraOuDtMinima(Agora, TPData, EdHora);
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSCurCab.QrVSCurCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCurAtu.Close;
  QrVSCurOriIMEI.Close;
  QrVSCurOriPallet.Close;
  QrVSCurDst.Close;
  QrForcados.Close;
  QrEmit.Close;
  QrPQO.Close;
  QrCaleados.Close;
end;

procedure TFmVSCurCab.QrVSCurCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSCurCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSCurCab.QrVSCurCabCalcFields(DataSet: TDataSet);
begin
  case QrVSCurCabTipoArea.Value of
    0: QrVSCurCabNO_TIPO.Value := 'm�';
    1: QrVSCurCabNO_TIPO.Value := 'ft�';
    else QrVSCurCabNO_TIPO.Value := '???';
  end;
  QrVSCurCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSCurCabDtHrLibOpe.Value, 106, True);
  QrVSCurCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSCurCabDtHrFimOpe.Value, 106, True);
  QrVSCurCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSCurCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSCurCab.QrVSCurDstAfterScroll(DataSet: TDataSet);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCurBxa, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCurAtuMovimCod.Value),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSCurDstMovimTwn.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmCurBxa)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  '']);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSCurCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCurCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmCurBxa)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSCurDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCurBxa, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
(*
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
*)
end;

procedure TFmVSCurCab.QrVSCurDstBeforeClose(DataSet: TDataSet);
begin
  QrVSCurBxa.Close;
end;

end.

