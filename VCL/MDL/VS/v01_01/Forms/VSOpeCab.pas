unit VSOpeCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnProjGroup_Consts,
  UnMyObjects, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums, dmkDBGridZTO,
  UnProjGroup_Vars;

type
  TFmVSOpeCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSOpeCab: TmySQLQuery;
    DsVSOpeCab: TDataSource;
    QrVSOpeAtu: TmySQLQuery;
    DsVSOpeAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSOpeCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSOpeCabEmpresa: TIntegerField;
    QrVSOpeCabDtHrAberto: TDateTimeField;
    QrVSOpeCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSOpeCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSOpeOriIMEI: TmySQLQuery;
    DsVSOpeOriIMEI: TDataSource;
    QrVSOpeCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSOpeCabLk: TIntegerField;
    QrVSOpeCabDataCad: TDateField;
    QrVSOpeCabDataAlt: TDateField;
    QrVSOpeCabUserCad: TIntegerField;
    QrVSOpeCabUserAlt: TIntegerField;
    QrVSOpeCabAlterWeb: TSmallintField;
    QrVSOpeCabAtivo: TSmallintField;
    QrVSOpeCabPecasMan: TFloatField;
    QrVSOpeCabAreaManM2: TFloatField;
    QrVSOpeCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSOpeCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSOpeCabNO_TIPO: TWideStringField;
    QrVSOpeCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSOpeCabNO_DtHrLibOpe: TWideStringField;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSOpeCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSOpeCabCacCod: TIntegerField;
    QrVSOpeCabGraGruX: TIntegerField;
    QrVSOpeCabCustoManMOKg: TFloatField;
    QrVSOpeCabCustoManMOTot: TFloatField;
    QrVSOpeCabValorManMP: TFloatField;
    QrVSOpeCabValorManT: TFloatField;
    QrVSOpeCabDtHrLibOpe: TDateTimeField;
    QrVSOpeCabDtHrCfgOpe: TDateTimeField;
    QrVSOpeCabDtHrFimOpe: TDateTimeField;
    QrVSOpeCabPecasSrc: TFloatField;
    QrVSOpeCabAreaSrcM2: TFloatField;
    QrVSOpeCabAreaSrcP2: TFloatField;
    QrVSOpeCabPecasDst: TFloatField;
    QrVSOpeCabAreaDstM2: TFloatField;
    QrVSOpeCabAreaDstP2: TFloatField;
    QrVSOpeCabPecasSdo: TFloatField;
    QrVSOpeCabAreaSdoM2: TFloatField;
    QrVSOpeCabAreaSdoP2: TFloatField;
    DsVSOpeDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    AtualizaestoqueEmOperao1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid1: TDBGrid;
    QrVSOpeBxa: TmySQLQuery;
    DsVSOpeBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSOpeCabPesoKgSrc: TFloatField;
    QrVSOpeCabPesoKgMan: TFloatField;
    QrVSOpeCabPesoKgDst: TFloatField;
    QrVSOpeCabPesoKgSdo: TFloatField;
    QrVSOpeCabValorTMan: TFloatField;
    QrVSOpeCabValorTSrc: TFloatField;
    QrVSOpeCabValorTSdo: TFloatField;
    QrVSOpeCabPecasINI: TFloatField;
    QrVSOpeCabAreaINIM2: TFloatField;
    QrVSOpeCabAreaINIP2: TFloatField;
    QrVSOpeCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSOpeCabPesoKgBxa: TFloatField;
    QrVSOpeCabPecasBxa: TFloatField;
    QrVSOpeCabAreaBxaM2: TFloatField;
    QrVSOpeCabAreaBxaP2: TFloatField;
    QrVSOpeCabValorTBxa: TFloatField;
    Definiodequantidadesmanualmente1: TMenuItem;
    PCOpeOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSOpeOriPallet: TmySQLQuery;
    DsVSOpeOriPallet: TDataSource;
    QrVSOpeOriPalletPecas: TFloatField;
    QrVSOpeOriPalletAreaM2: TFloatField;
    QrVSOpeOriPalletAreaP2: TFloatField;
    QrVSOpeOriPalletPesoKg: TFloatField;
    QrVSOpeOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSOpeOriPalletNO_Pallet: TWideStringField;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    Label48: TLabel;
    DBEdit45: TDBEdit;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label51: TLabel;
    DBEdit40: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit41: TDBEdit;
    Label52: TLabel;
    Corrigirfornecedor1: TMenuItem;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    QrVSOpeCabNFeRem: TIntegerField;
    QrVSOpeCabLPFMO: TWideStringField;
    DBEdit44: TDBEdit;
    Label58: TLabel;
    Label59: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label56: TLabel;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label57: TLabel;
    Ordensdeoperaoemaberto1: TMenuItem;
    porPalletparcial1: TMenuItem;
    N4: TMenuItem;
    porIMEIParcial1: TMenuItem;
    QrVSOpeCabTemIMEIMrt: TIntegerField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    QrVSOpeOriIMEICodigo: TLargeintField;
    QrVSOpeOriIMEIControle: TLargeintField;
    QrVSOpeOriIMEIMovimCod: TLargeintField;
    QrVSOpeOriIMEIMovimNiv: TLargeintField;
    QrVSOpeOriIMEIMovimTwn: TLargeintField;
    QrVSOpeOriIMEIEmpresa: TLargeintField;
    QrVSOpeOriIMEITerceiro: TLargeintField;
    QrVSOpeOriIMEICliVenda: TLargeintField;
    QrVSOpeOriIMEIMovimID: TLargeintField;
    QrVSOpeOriIMEIDataHora: TDateTimeField;
    QrVSOpeOriIMEIPallet: TLargeintField;
    QrVSOpeOriIMEIGraGruX: TLargeintField;
    QrVSOpeOriIMEIPecas: TFloatField;
    QrVSOpeOriIMEIPesoKg: TFloatField;
    QrVSOpeOriIMEIAreaM2: TFloatField;
    QrVSOpeOriIMEIAreaP2: TFloatField;
    QrVSOpeOriIMEIValorT: TFloatField;
    QrVSOpeOriIMEISrcMovID: TLargeintField;
    QrVSOpeOriIMEISrcNivel1: TLargeintField;
    QrVSOpeOriIMEISrcNivel2: TLargeintField;
    QrVSOpeOriIMEISrcGGX: TLargeintField;
    QrVSOpeOriIMEISdoVrtPeca: TFloatField;
    QrVSOpeOriIMEISdoVrtPeso: TFloatField;
    QrVSOpeOriIMEISdoVrtArM2: TFloatField;
    QrVSOpeOriIMEIObserv: TWideStringField;
    QrVSOpeOriIMEISerieFch: TLargeintField;
    QrVSOpeOriIMEIFicha: TLargeintField;
    QrVSOpeOriIMEIMisturou: TLargeintField;
    QrVSOpeOriIMEIFornecMO: TLargeintField;
    QrVSOpeOriIMEICustoMOKg: TFloatField;
    QrVSOpeOriIMEICustoMOM2: TFloatField;
    QrVSOpeOriIMEICustoMOTot: TFloatField;
    QrVSOpeOriIMEIValorMP: TFloatField;
    QrVSOpeOriIMEIDstMovID: TLargeintField;
    QrVSOpeOriIMEIDstNivel1: TLargeintField;
    QrVSOpeOriIMEIDstNivel2: TLargeintField;
    QrVSOpeOriIMEIDstGGX: TLargeintField;
    QrVSOpeOriIMEIQtdGerPeca: TFloatField;
    QrVSOpeOriIMEIQtdGerPeso: TFloatField;
    QrVSOpeOriIMEIQtdGerArM2: TFloatField;
    QrVSOpeOriIMEIQtdGerArP2: TFloatField;
    QrVSOpeOriIMEIQtdAntPeca: TFloatField;
    QrVSOpeOriIMEIQtdAntPeso: TFloatField;
    QrVSOpeOriIMEIQtdAntArM2: TFloatField;
    QrVSOpeOriIMEIQtdAntArP2: TFloatField;
    QrVSOpeOriIMEINotaMPAG: TFloatField;
    QrVSOpeOriIMEINO_PALLET: TWideStringField;
    QrVSOpeOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSOpeOriIMEINO_TTW: TWideStringField;
    QrVSOpeOriIMEIID_TTW: TLargeintField;
    QrVSOpeOriIMEINO_FORNECE: TWideStringField;
    QrVSOpeOriIMEINO_SerieFch: TWideStringField;
    QrVSOpeOriIMEIReqMovEstq: TLargeintField;
    QrVSOpeOriPalletPallet: TLargeintField;
    QrVSOpeOriPalletGraGruX: TLargeintField;
    QrVSOpeOriPalletSerieFch: TLargeintField;
    QrVSOpeOriPalletFicha: TLargeintField;
    QrVSOpeOriPalletNO_TTW: TWideStringField;
    QrVSOpeOriPalletID_TTW: TLargeintField;
    QrVSOpeOriPalletSeries_E_Fichas: TWideStringField;
    QrVSOpeOriPalletTerceiro: TLargeintField;
    QrVSOpeOriPalletMarca: TWideStringField;
    QrVSOpeOriPalletNO_FORNECE: TWideStringField;
    QrVSOpeOriPalletNO_SerieFch: TWideStringField;
    QrVSOpeOriPalletValorT: TFloatField;
    QrVSOpeOriPalletSdoVrtPeca: TFloatField;
    QrVSOpeOriPalletSdoVrtPeso: TFloatField;
    QrVSOpeOriPalletSdoVrtArM2: TFloatField;
    QrVSOpeDst: TmySQLQuery;
    QrVSOpeDstCodigo: TLargeintField;
    QrVSOpeDstControle: TLargeintField;
    QrVSOpeDstMovimCod: TLargeintField;
    QrVSOpeDstMovimNiv: TLargeintField;
    QrVSOpeDstMovimTwn: TLargeintField;
    QrVSOpeDstEmpresa: TLargeintField;
    QrVSOpeDstTerceiro: TLargeintField;
    QrVSOpeDstCliVenda: TLargeintField;
    QrVSOpeDstMovimID: TLargeintField;
    QrVSOpeDstDataHora: TDateTimeField;
    QrVSOpeDstPallet: TLargeintField;
    QrVSOpeDstGraGruX: TLargeintField;
    QrVSOpeDstPecas: TFloatField;
    QrVSOpeDstPesoKg: TFloatField;
    QrVSOpeDstAreaM2: TFloatField;
    QrVSOpeDstAreaP2: TFloatField;
    QrVSOpeDstValorT: TFloatField;
    QrVSOpeDstSrcMovID: TLargeintField;
    QrVSOpeDstSrcNivel1: TLargeintField;
    QrVSOpeDstSrcNivel2: TLargeintField;
    QrVSOpeDstSrcGGX: TLargeintField;
    QrVSOpeDstSdoVrtPeca: TFloatField;
    QrVSOpeDstSdoVrtPeso: TFloatField;
    QrVSOpeDstSdoVrtArM2: TFloatField;
    QrVSOpeDstObserv: TWideStringField;
    QrVSOpeDstSerieFch: TLargeintField;
    QrVSOpeDstFicha: TLargeintField;
    QrVSOpeDstMisturou: TLargeintField;
    QrVSOpeDstFornecMO: TLargeintField;
    QrVSOpeDstCustoMOKg: TFloatField;
    QrVSOpeDstCustoMOM2: TFloatField;
    QrVSOpeDstCustoMOTot: TFloatField;
    QrVSOpeDstValorMP: TFloatField;
    QrVSOpeDstDstMovID: TLargeintField;
    QrVSOpeDstDstNivel1: TLargeintField;
    QrVSOpeDstDstNivel2: TLargeintField;
    QrVSOpeDstDstGGX: TLargeintField;
    QrVSOpeDstQtdGerPeca: TFloatField;
    QrVSOpeDstQtdGerPeso: TFloatField;
    QrVSOpeDstQtdGerArM2: TFloatField;
    QrVSOpeDstQtdGerArP2: TFloatField;
    QrVSOpeDstQtdAntPeca: TFloatField;
    QrVSOpeDstQtdAntPeso: TFloatField;
    QrVSOpeDstQtdAntArM2: TFloatField;
    QrVSOpeDstQtdAntArP2: TFloatField;
    QrVSOpeDstNotaMPAG: TFloatField;
    QrVSOpeDstNO_PALLET: TWideStringField;
    QrVSOpeDstNO_PRD_TAM_COR: TWideStringField;
    QrVSOpeDstNO_TTW: TWideStringField;
    QrVSOpeDstID_TTW: TLargeintField;
    QrVSOpeDstNO_FORNECE: TWideStringField;
    QrVSOpeDstNO_SerieFch: TWideStringField;
    QrVSOpeDstReqMovEstq: TLargeintField;
    QrVSOpeDstMarca: TWideStringField;
    QrVSOpeDstPedItsFin: TLargeintField;
    QrVSOpeBxaCodigo: TLargeintField;
    QrVSOpeBxaControle: TLargeintField;
    QrVSOpeBxaMovimCod: TLargeintField;
    QrVSOpeBxaMovimNiv: TLargeintField;
    QrVSOpeBxaMovimTwn: TLargeintField;
    QrVSOpeBxaEmpresa: TLargeintField;
    QrVSOpeBxaTerceiro: TLargeintField;
    QrVSOpeBxaCliVenda: TLargeintField;
    QrVSOpeBxaMovimID: TLargeintField;
    QrVSOpeBxaDataHora: TDateTimeField;
    QrVSOpeBxaPallet: TLargeintField;
    QrVSOpeBxaGraGruX: TLargeintField;
    QrVSOpeBxaPecas: TFloatField;
    QrVSOpeBxaPesoKg: TFloatField;
    QrVSOpeBxaAreaM2: TFloatField;
    QrVSOpeBxaAreaP2: TFloatField;
    QrVSOpeBxaValorT: TFloatField;
    QrVSOpeBxaSrcMovID: TLargeintField;
    QrVSOpeBxaSrcNivel1: TLargeintField;
    QrVSOpeBxaSrcNivel2: TLargeintField;
    QrVSOpeBxaSrcGGX: TLargeintField;
    QrVSOpeBxaSdoVrtPeca: TFloatField;
    QrVSOpeBxaSdoVrtPeso: TFloatField;
    QrVSOpeBxaSdoVrtArM2: TFloatField;
    QrVSOpeBxaObserv: TWideStringField;
    QrVSOpeBxaSerieFch: TLargeintField;
    QrVSOpeBxaFicha: TLargeintField;
    QrVSOpeBxaMisturou: TLargeintField;
    QrVSOpeBxaFornecMO: TLargeintField;
    QrVSOpeBxaCustoMOKg: TFloatField;
    QrVSOpeBxaCustoMOM2: TFloatField;
    QrVSOpeBxaCustoMOTot: TFloatField;
    QrVSOpeBxaValorMP: TFloatField;
    QrVSOpeBxaDstMovID: TLargeintField;
    QrVSOpeBxaDstNivel1: TLargeintField;
    QrVSOpeBxaDstNivel2: TLargeintField;
    QrVSOpeBxaDstGGX: TLargeintField;
    QrVSOpeBxaQtdGerPeca: TFloatField;
    QrVSOpeBxaQtdGerPeso: TFloatField;
    QrVSOpeBxaQtdGerArM2: TFloatField;
    QrVSOpeBxaQtdGerArP2: TFloatField;
    QrVSOpeBxaQtdAntPeca: TFloatField;
    QrVSOpeBxaQtdAntPeso: TFloatField;
    QrVSOpeBxaQtdAntArM2: TFloatField;
    QrVSOpeBxaQtdAntArP2: TFloatField;
    QrVSOpeBxaNotaMPAG: TFloatField;
    QrVSOpeBxaNO_PALLET: TWideStringField;
    QrVSOpeBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSOpeBxaNO_TTW: TWideStringField;
    QrVSOpeBxaID_TTW: TLargeintField;
    QrVSOpeBxaNO_FORNECE: TWideStringField;
    QrVSOpeBxaNO_SerieFch: TWideStringField;
    QrVSOpeBxaReqMovEstq: TLargeintField;
    QrVSOpeAtuCodigo: TLargeintField;
    QrVSOpeAtuControle: TLargeintField;
    QrVSOpeAtuMovimCod: TLargeintField;
    QrVSOpeAtuMovimNiv: TLargeintField;
    QrVSOpeAtuMovimTwn: TLargeintField;
    QrVSOpeAtuEmpresa: TLargeintField;
    QrVSOpeAtuTerceiro: TLargeintField;
    QrVSOpeAtuCliVenda: TLargeintField;
    QrVSOpeAtuMovimID: TLargeintField;
    QrVSOpeAtuDataHora: TDateTimeField;
    QrVSOpeAtuPallet: TLargeintField;
    QrVSOpeAtuGraGruX: TLargeintField;
    QrVSOpeAtuPecas: TFloatField;
    QrVSOpeAtuPesoKg: TFloatField;
    QrVSOpeAtuAreaM2: TFloatField;
    QrVSOpeAtuAreaP2: TFloatField;
    QrVSOpeAtuValorT: TFloatField;
    QrVSOpeAtuSrcMovID: TLargeintField;
    QrVSOpeAtuSrcNivel1: TLargeintField;
    QrVSOpeAtuSrcNivel2: TLargeintField;
    QrVSOpeAtuSrcGGX: TLargeintField;
    QrVSOpeAtuSdoVrtPeca: TFloatField;
    QrVSOpeAtuSdoVrtPeso: TFloatField;
    QrVSOpeAtuSdoVrtArM2: TFloatField;
    QrVSOpeAtuObserv: TWideStringField;
    QrVSOpeAtuSerieFch: TLargeintField;
    QrVSOpeAtuFicha: TLargeintField;
    QrVSOpeAtuMisturou: TLargeintField;
    QrVSOpeAtuFornecMO: TLargeintField;
    QrVSOpeAtuCustoMOKg: TFloatField;
    QrVSOpeAtuCustoMOM2: TFloatField;
    QrVSOpeAtuCustoMOTot: TFloatField;
    QrVSOpeAtuValorMP: TFloatField;
    QrVSOpeAtuDstMovID: TLargeintField;
    QrVSOpeAtuDstNivel1: TLargeintField;
    QrVSOpeAtuDstNivel2: TLargeintField;
    QrVSOpeAtuDstGGX: TLargeintField;
    QrVSOpeAtuQtdGerPeca: TFloatField;
    QrVSOpeAtuQtdGerPeso: TFloatField;
    QrVSOpeAtuQtdGerArM2: TFloatField;
    QrVSOpeAtuQtdGerArP2: TFloatField;
    QrVSOpeAtuQtdAntPeca: TFloatField;
    QrVSOpeAtuQtdAntPeso: TFloatField;
    QrVSOpeAtuQtdAntArM2: TFloatField;
    QrVSOpeAtuQtdAntArP2: TFloatField;
    QrVSOpeAtuNotaMPAG: TFloatField;
    QrVSOpeAtuNO_PALLET: TWideStringField;
    QrVSOpeAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSOpeAtuNO_TTW: TWideStringField;
    QrVSOpeAtuID_TTW: TLargeintField;
    QrVSOpeAtuNO_FORNECE: TWideStringField;
    QrVSOpeAtuReqMovEstq: TLargeintField;
    QrVSOpeAtuCUSTO_M2: TFloatField;
    QrVSOpeAtuCUSTO_P2: TFloatField;
    QrVSOpeAtuNO_LOC_CEN: TWideStringField;
    QrVSOpeAtuMarca: TWideStringField;
    QrVSOpeAtuPedItsLib: TLargeintField;
    QrVSOpeAtuStqCenLoc: TLargeintField;
    QrVSOpeAtuNO_FICHA: TWideStringField;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    N5: TMenuItem;
    AlteraPallet1: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    QrVSOpeOriIMEIVSMulFrnCab: TLargeintField;
    AlteraLocaldoestoque1: TMenuItem;
    N6: TMenuItem;
    QrVSOpeDstStqCenLoc: TLargeintField;
    AlteraLocaldoestoque2: TMenuItem;
    QrVSOpeDstNO_LOC_CEN: TWideStringField;
    Alteraartigodedestino1: TMenuItem;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSOpeAtuClientMO: TLargeintField;
    Somenteinfo1: TMenuItem;
    Apreencher1: TMenuItem;
    EdGGXDst: TdmkEditCB;
    CBGGXDst: TdmkDBLookupComboBox;
    Label54: TLabel;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    CkCpermitirCrust: TCheckBox;
    QrVSOpeCabGGXDst: TIntegerField;
    Incluiraspa1: TMenuItem;
    N7: TMenuItem;
    Corrigeartigodedestino1: TMenuItem;
    QrOperacoes: TmySQLQuery;
    DsOperacoes: TDataSource;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    EdOperacoes: TdmkEditCB;
    Label55: TLabel;
    CBOperacoes: TdmkDBLookupComboBox;
    QrVSOpeCabOperacoes: TIntegerField;
    QrVSOpeCabNO_Operacoes: TWideStringField;
    Label60: TLabel;
    DBEdit42: TDBEdit;
    SpeedButton5: TSpeedButton;
    Oitemselecionado1: TMenuItem;
    odosapartirdoselecionado1: TMenuItem;
    EdSerieRem: TdmkEdit;
    QrVSOpeCabSerieRem: TSmallintField;
    dmkDBEdit3: TdmkDBEdit;
    GBConfig: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdVSCOPCab: TdmkEditCB;
    Label61: TLabel;
    CBVSCOPCab: TdmkDBLookupComboBox;
    SbVSCOPCabCad: TSpeedButton;
    QrVSOpeCabVSCOPCab: TIntegerField;
    QrVSOpeCabNO_VSCOPCab: TWideStringField;
    SbVSCOPCabCpy: TSpeedButton;
    QrVSCOPCab: TmySQLQuery;
    DsVSCOPCab: TDataSource;
    QrVSCOPCabCodigo: TIntegerField;
    QrVSCOPCabNome: TWideStringField;
    Desfazencerramento1: TMenuItem;
    N8: TMenuItem;
    TransfereTodaOPparaOutra1: TMenuItem;
    Comorigemedestino1: TMenuItem;
    QrVSOpeOriIMEIDtCorrApo: TDateTimeField;
    QrVSOpeDstDtCorrApo: TDateTimeField;
    QrForcadosDtCorrApo: TDateTimeField;
    Label63: TLabel;
    DBEdit43: TDBEdit;
    DBEdit46: TDBEdit;
    QrVSOpeAtuNO_FORNEC_MO: TWideStringField;
    este1: TMenuItem;
    QrVSOpeOriIMEIIxxMovIX: TLargeintField;
    QrVSOpeOriIMEIIxxFolha: TLargeintField;
    QrVSOpeOriIMEIIxxLinha: TLargeintField;
    InformaPginaelinhadeIEC1: TMenuItem;
    ReceberDadosDaOPdeOrigem1: TMenuItem;
    ransfenciadecourosorigementreOPs1: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    TsEnvioMO: TTabSheet;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    TsRetornoMO: TTabSheet;
    AtrelamentoNFsdeMO2: TMenuItem;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_FatID: TIntegerField;
    QrVSMOEnvRetCFTPA_FatNum: TIntegerField;
    QrVSMOEnvRetCFTPA_Empresa: TIntegerField;
    QrVSMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_nItem: TIntegerField;
    QrVSMOEnvRetCFTPA_SerCT: TIntegerField;
    QrVSMOEnvRetCFTPA_nCT: TIntegerField;
    QrVSMOEnvRetCFTPA_Pecas: TFloatField;
    QrVSMOEnvRetCFTPA_PesoKg: TFloatField;
    QrVSMOEnvRetCFTPA_AreaM2: TFloatField;
    QrVSMOEnvRetCFTPA_AreaP2: TFloatField;
    QrVSMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_ValorT: TFloatField;
    DsVSMOEnvRet: TDataSource;
    QrVSMOEnvRVmi: TmySQLQuery;
    QrVSMOEnvRVmiCodigo: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorFrete: TFloatField;
    QrVSMOEnvRVmiLk: TIntegerField;
    QrVSMOEnvRVmiDataCad: TDateField;
    QrVSMOEnvRVmiDataAlt: TDateField;
    QrVSMOEnvRVmiUserCad: TIntegerField;
    QrVSMOEnvRVmiUserAlt: TIntegerField;
    QrVSMOEnvRVmiAlterWeb: TSmallintField;
    QrVSMOEnvRVmiAWServerID: TIntegerField;
    QrVSMOEnvRVmiAWStatSinc: TSmallintField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField;
    QrVSMOEnvRVmiNFEMP_nNF: TIntegerField;
    QrVSMOEnvRVmiValorT: TFloatField;
    DsVSMOEnvRVmi: TDataSource;
    QrVSMOEnvGVmi: TmySQLQuery;
    QrVSMOEnvGVmiCodigo: TIntegerField;
    QrVSMOEnvGVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvGVmiVSMovIts: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiValorFrete: TFloatField;
    QrVSMOEnvGVmiLk: TIntegerField;
    QrVSMOEnvGVmiDataCad: TDateField;
    QrVSMOEnvGVmiDataAlt: TDateField;
    QrVSMOEnvGVmiUserCad: TIntegerField;
    QrVSMOEnvGVmiUserAlt: TIntegerField;
    QrVSMOEnvGVmiAlterWeb: TSmallintField;
    QrVSMOEnvGVmiAWServerID: TIntegerField;
    QrVSMOEnvGVmiAWStatSinc: TSmallintField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiVSMovimCod: TIntegerField;
    DsVSMOEnvGVmi: TDataSource;
    PnRetornoMO: TPanel;
    DBGVSMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVmi: TdmkDBGridZTO;
    DBEdit47: TDBEdit;
    Label64: TLabel;
    DBEdit49: TDBEdit;
    Label66: TLabel;
    QrVSOpeAtuCusFrtMOEnv: TFloatField;
    QrVSOpeAtuCusFrtMORet: TFloatField;
    QrVSOpeDstCustoMOPc: TFloatField;
    QrVSOpeDstCusFrtMORet: TFloatField;
    QrVSOpeAtuCustoPQ: TFloatField;
    QrVSOpeOriIMEICustoM2: TFloatField;
    QrVSOpeOriIMEICustoKg: TFloatField;
    QrVSOpeDstCustoM2: TFloatField;
    QrVSOpeDstCustoKg: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSOpeCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSOpeCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSOpeCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSOpeCabBeforeClose(DataSet: TDataSet);
    procedure CabLibera1Click(Sender: TObject);
    procedure QrVSOpeCabCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmOperao1Click(Sender: TObject);
    procedure QrVSOpeDstBeforeClose(DataSet: TDataSet);
    procedure QrVSOpeDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure porPallet1Click(Sender: TObject);
    procedure porIMEI1Click(Sender: TObject);
    procedure InformaNmerodaRME11Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure Corrigirfornecedor1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure Ordensdeoperaoemaberto1Click(Sender: TObject);
    procedure porPalletparcial1Click(Sender: TObject);
    procedure porIMEIParcial1Click(Sender: TObject);
    procedure AlteraPallet1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure AlteraLocaldoestoque1Click(Sender: TObject);
    procedure AlteraLocaldoestoque2Click(Sender: TObject);
    procedure Alteraartigodedestino1Click(Sender: TObject);
    procedure Somenteinfo1Click(Sender: TObject);
    procedure Apreencher1Click(Sender: TObject);
    procedure CkCpermitirCrustClick(Sender: TObject);
    procedure Incluiraspa1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Oitemselecionado1Click(Sender: TObject);
    procedure odosapartirdoselecionado1Click(Sender: TObject);
    procedure SbVSCOPCabCpyClick(Sender: TObject);
    procedure SbVSCOPCabCadClick(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure Comorigemedestino1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure InformaPginaelinhadeIEC1Click(Sender: TObject);
    procedure TransfereTodaOPparaOutra1Click(Sender: TObject);
    procedure ReceberDadosDaOPdeOrigem1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
    procedure QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
  private
    FItsExcluiOriIMEI_Enabled, FItsExcluiOriPallet_Enabled,
    FItsIncluiOri_Enabled, FItsIncluiDst_Enabled, FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //procedure MostraVSOpeOriIMEI(SQLType: TSQLType);
    //procedure MostraVSOpeOriPall(SQLType: TSQLType);
    procedure MostraVSOpeOriIMEI2(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSOpeOriPall2(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSOpeDst(SQLType: TSQLType; DesabilitaBaixa: Boolean);
    procedure InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSOpeOris(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ImprimeIMEI(Formato: TXXImpImeiKind);
    procedure CorrigeArtigos(Todos: Boolean);
    function  CalculoCustoOrigemM2(): Double;
  public
    { Public declarations }
    FSeq, FCabIni, FOOOrigemCodigo, FOOOrigemMovimCod, FOOOrigemAtuControle: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
  end;

var
  FmVSOpeCab: TFmVSOpeCab;
const
  FFormatFloat = '00000';

implementation

uses
  //VSOpeOriPall, VSOpeOriIMEI, VSOpeEnc,
  Module, MyDBCheck, DmkDAC_PF, ModuleGeral, VSOpeDst, AppListas,
{$IfDef sAllVS} UnVS_PF, {$EndIf}
  VSOpeOriPall2, VSOpeOriIMEI2, UnVS_CRC_PF, ModVS_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2Env = True;
  FGerArX2Ret = False;

procedure TFmVSOpeCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSOpeCab.MostraVSOpeDst(SQLType: TSQLType; DesabilitaBaixa: Boolean);
begin
  if (not DesabilitaBaixa) then
    if (not FItsIncluiDst_Enabled) then
      if not DBCheck.LiberaPelaSenhaAdmin then
        Exit;
  if DBCheck.CriaFm(TFmVSOpeDst, FmVSOpeDst, afmoNegarComAviso) then
  begin
    FmVSOpeDst.ImgTipo.SQLType := SQLType;
    FmVSOpeDst.FQrCab := QrVSOpeCab;
    FmVSOpeDst.FDsCab := DsVSOpeCab;
    FmVSOpeDst.FQrIts := QrVSOpeDst;
    FmVSOpeDst.FEmpresa  := QrVSOpeCabEmpresa.Value;
    FmVSOpeDst.FClientMO := QrVSOpeAtuClientMO.Value;
    FmVSOpeDst.FFornecMO := QrVSOpeAtuFornecMO.Value;
    FmVSOpeDst.FValM2   := CalculoCustoOrigemM2(); // 2023-05-13
    FmVSOpeDst.FVmiPai := QrVSOpeAtuControle.Value;
    if SQLType = stIns then
    begin
      //FmVSOpeDst.EdCPF1.ReadOnly := False
      VS_CRC_PF.ReopenPedItsXXX(FmVSOpeDst.QrVSPedIts, QrVSOpeAtuControle.Value,
      QrVSOpeAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSOpeDst.EdPedItsFin.ValueVariant  := QrVSOpeAtuPedItsLib.Value;
      FmVSOpeDst.CBPedItsFin.KeyValue      := QrVSOpeAtuPedItsLib.Value;
      //
      FmVSOpeDst.EdGragruX.ValueVariant    := QrVSOpeCabGGXDst.Value;
      FmVSOpeDst.CBGragruX.KeyValue        := QrVSOpeCabGGXDst.Value;
      //
      FmVSOpeDst.TPData.Date               := QrVSOpeCabDtHrAberto.Value;
      FmVSOpeDst.EdHora.ValueVariant       := QrVSOpeCabDtHrAberto.Value;
    end else
    begin
      VS_CRC_PF.ReopenPedItsXXX(FmVSOpeDst.QrVSPedIts, QrVSOpeAtuControle.Value,
       QrVSOpeDstControle.Value);
      FmVSOpeDst.EdCtrl1.ValueVariant      := QrVSOpeDstControle.Value;
      //FmVSOpeDst.EdCtrl2.ValueVariant     := VS_CRC_PF.ObtemControleMovimTwin(
        //QrVSOpeDstMovimCod.Value, QrVSOpeDstMovimTwn.Value, emidEmOperacao,
        //eminEmOperBxa);
      FmVSOpeDst.EdMovimTwn.ValueVariant   := QrVSOpeDstMovimTwn.Value;
      FmVSOpeDst.EdGragruX.ValueVariant    := QrVSOpeDstGraGruX.Value;
      FmVSOpeDst.CBGragruX.KeyValue        := QrVSOpeDstGraGruX.Value;
      FmVSOpeDst.EdPecas.ValueVariant      := QrVSOpeDstPecas.Value;
      FmVSOpeDst.EdPesoKg.ValueVariant     := QrVSOpeDstPesoKg.Value;
      FmVSOpeDst.EdAreaM2.ValueVariant     := QrVSOpeDstAreaM2.Value;
      FmVSOpeDst.EdAreaP2.ValueVariant     := QrVSOpeDstAreaP2.Value;
      // Cuidado!!!
      FmVSOpeDst.EdValorT.ValueVariant     := QrVSOpeDstValorMP.Value;
      //
      FmVSOpeDst.EdObserv.ValueVariant     := QrVSOpeDstObserv.Value;
      FmVSOpeDst.EdSerieFch.ValueVariant   := QrVSOpeDstSerieFch.Value;
      FmVSOpeDst.CBSerieFch.KeyValue       := QrVSOpeDstSerieFch.Value;
      FmVSOpeDst.EdFicha.ValueVariant      := QrVSOpeDstFicha.Value;
      FmVSOpeDst.EdMarca.ValueVariant      := QrVSOpeDstMarca.Value;
      //FmVSOpeDst.RGMisturou.ItemIndex    := QrVSOpeDstMisturou.Value;
      FmVSOpeDst.EdPedItsFin.ValueVariant  := QrVSOpeDstPedItsFin.Value;
      FmVSOpeDst.CBPedItsFin.KeyValue      := QrVSOpeDstPedItsFin.Value;
      FmVSOpeDst.EdPallet.ValueVariant     := QrVSOpeDstPallet.Value;
      FmVSOpeDst.CBPallet.KeyValue         := QrVSOpeDstPallet.Value;
      FmVSOpeDst.EdStqCenLoc.ValueVariant  := QrVSOpeDstStqCenLoc.Value;
      FmVSOpeDst.CBStqCenLoc.KeyValue      := QrVSOpeDstStqCenLoc.Value;
      FmVSOpeDst.EdReqMovEstq.ValueVariant := QrVSOpeDstReqMovEstq.Value;
      //
      FmVSOpeDst.EdCustoMOPc.ValueVariant  := QrVSOpeDstCustoMOPc.Value;
      FmVSOpeDst.EdCustoMOKg.ValueVariant  := QrVSOpeDstCustoMOKg.Value;
      FmVSOpeDst.EdCustoMOM2.ValueVariant  := QrVSOpeDstCustoMOM2.Value;
      FmVSOpeDst.EdCustoMOTot.ValueVariant := QrVSOpeDstCustoMOTot.Value;
      FmVSOpeDst.EdCusFrtMORet.ValueVariant := QrVSOpeDstCusFrtMORet.Value;
      // Cuidado!!!
      FmVSOpeDst.EdValorT2.ValueVariant    := QrVSOpeDstValorT.Value;
      //
      //FmVSOpeDst.TPData.Date               := QrVSOpeDstDataHora.Value;
      //FmVSOpeDst.EdHora.ValueVariant       := QrVSOpeDstDataHora.Value;
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSOpeDstDataHora.Value,
        QrVSOpeDstDtCorrApo.Value,  FmVSOpeDst.TPData, FmVSOpeDst.EdHora);
      //
      if QrVSOpeDstSdoVrtPeca.Value < QrVSOpeDstPecas.Value then
      begin
        FmVSOpeDst.EdGraGruX.Enabled        := False;
        FmVSOpeDst.CBGraGruX.Enabled        := False;
      end;
      if QrVSOpeBxa.RecordCount > 0 then
      begin
        FmVSOpeDst.CkBaixa.Checked          := True;
        FmVSOpeDst.EdBxaPecas.ValueVariant  := -QrVSOpeBxaPecas.Value;
        FmVSOpeDst.EdBxaPesoKg.ValueVariant := -QrVSOpeBxaPesoKg.Value;
        FmVSOpeDst.EdBxaAreaM2.ValueVariant := -QrVSOpeBxaAreaM2.Value;
        FmVSOpeDst.EdBxaAreaP2.ValueVariant := -QrVSOpeBxaAreaP2.Value;
        FmVSOpeDst.EdBxaValorT.ValueVariant := -QrVSOpeBxaValorT.Value;
        FmVSOpeDst.EdCtrl2.ValueVariant     := QrVSOpeBxaControle.Value;
        FmVSOpeDst.EdBxaObserv.ValueVariant := QrVSOpeBxaObserv.Value;
      end;
    end;
    //
    FmVSOpeDst.LaBxaPesoKg.Enabled  := QrVSOpeCabPesoKgINI.Value <> 0;
    FmVSOpeDst.EdBxaPesoKg.Enabled  := QrVSOpeCabPesoKgINI.Value <> 0;
    FmVSOpeDst.LaBxaAreaM2.Enabled  := QrVSOpeCabAreaINIM2.Value <> 0;
    FmVSOpeDst.EdBxaAreaM2.Enabled  := QrVSOpeCabAreaINIM2.Value <> 0;
    FmVSOpeDst.LaBxaAreaP2.Enabled  := QrVSOpeCabAreaINIM2.Value <> 0;
    FmVSOpeDst.EdBxaAreaP2.Enabled  := QrVSOpeCabAreaINIM2.Value <> 0;
    //
    if DesabilitaBaixa then
      FmVSOpeDst.CkBaixa.Enabled := False;
    FmVSOpeDst.ShowModal;
    FmVSOpeDst.Destroy;
    VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

(*
procedure TFmVSOpeCab.MostraVSOpeOriIMEI(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSOpeOriIMEI, FmVSOpeOriIMEI, afmoNegarComAviso) then
  begin
    FmVSOpeOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSOpeOriIMEI.FQrCab          := QrVSOpeCab;
    FmVSOpeOriIMEI.FDsCab          := DsVSOpeCab;
    //FmVSOpeOriIMEI.FVSOpeOriIMEIIMEI   := QrVSOpeOriIMEIIMEI;
    //FmVSOpeOriIMEI.FVSOpeOriIMEIIMEIet := QrVSOpeOriIMEIIMEIet;
    FmVSOpeOriIMEI.FEmpresa        := QrVSOpeCabEmpresa.Value;
    FmVSOpeOriIMEI.FTipoArea       := QrVSOpeCabTipoArea.Value;
    FmVSOpeOriIMEI.FOrigMovimNiv   := QrVSOpeAtuMovimNiv.Value;
    FmVSOpeOriIMEI.FOrigMovimCod   := QrVSOpeAtuMovimCod.Value;
    FmVSOpeOriIMEI.FOrigCodigo     := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriIMEI.FNewGraGruX     := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriIMEI.EdCodigo.ValueVariant    := QrVSOpeCabCodigo.Value;
    FmVSOpeOriIMEI.EdMovimCod.ValueVariant  := QrVSOpeCabMovimCod.Value;
    FmVSOpeOriIMEI.FDataHora                := QrVSOpeCabDtHrAberto.Value;
    //
    FmVSOpeOriIMEI.EdSrcMovID.ValueVariant  := QrVSOpeAtuMovimID.Value;
    FmVSOpeOriIMEI.EdSrcNivel1.ValueVariant := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriIMEI.EdSrcNivel2.ValueVariant := QrVSOpeAtuControle.Value;
    FmVSOpeOriIMEI.EdSrcGGX.ValueVariant    := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriIMEI.ReopenItensAptos();
    FmVSOpeOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSOpeOriIMEI.EdControle.ValueVariant := QrVSOpeCabOldControle.Value;
      //
      FmVSOpeOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSOpeCabOldCNPJ_CPF.Value);
      FmVSOpeOriIMEI.EdNomeEmiSac.Text := QrVSOpeCabOldNome.Value;
      FmVSOpeOriIMEI.EdCPF1.ReadOnly := True;
}
    end;
    FmVSOpeOriIMEI.ShowModal;
    FmVSOpeOriIMEI.Destroy;
    //
    VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;
*)

(*
procedure TFmVSOpeCab.MostraVSOpeOriPall(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSOpeOriPall, FmVSOpeOriPall, afmoNegarComAviso) then
  begin
    FmVSOpeOriPall.ImgTipo.SQLType := SQLType;
    FmVSOpeOriPall.FQrCab          := QrVSOpeCab;
    FmVSOpeOriPall.FDsCab          := DsVSOpeCab;
    //FmVSOpeOriPall.FVSOpeOriPallIMEI   := QrVSOpeOriPallIMEI;
    //FmVSOpeOriPall.FVSOpeOriPallPallet := QrVSOpeOriPallPallet;
    FmVSOpeOriPall.FEmpresa        := QrVSOpeCabEmpresa.Value;
    FmVSOpeOriPall.FTipoArea       := QrVSOpeCabTipoArea.Value;
    FmVSOpeOriPall.FOrigMovimNiv   := QrVSOpeAtuMovimNiv.Value;
    FmVSOpeOriPall.FOrigMovimCod   := QrVSOpeAtuMovimCod.Value;
    FmVSOpeOriPall.FOrigCodigo     := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriPall.FNewGraGruX     := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriPall.EdCodigo.ValueVariant    := QrVSOpeCabCodigo.Value;
    FmVSOpeOriPall.EdMovimCod.ValueVariant  := QrVSOpeCabMovimCod.Value;
    FmVSOpeOriPall.FDataHora                := QrVSOpeCabDtHrAberto.Value;
    //
    FmVSOpeOriPall.EdSrcMovID.ValueVariant  := QrVSOpeAtuMovimID.Value;
    FmVSOpeOriPall.EdSrcNivel1.ValueVariant := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriPall.EdSrcNivel2.ValueVariant := QrVSOpeAtuControle.Value;
    FmVSOpeOriPall.EdSrcGGX.ValueVariant    := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriPall.ReopenItensAptos();
    FmVSOpeOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSOpeOriPall.EdControle.ValueVariant := QrVSOpeCabOldControle.Value;
      //
      FmVSOpeOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSOpeCabOldCNPJ_CPF.Value);
      FmVSOpeOriPall.EdNomeEmiSac.Text := QrVSOpeCabOldNome.Value;
      FmVSOpeOriPall.EdCPF1.ReadOnly := True;
}
    end;
    FmVSOpeOriPall.ShowModal;
    FmVSOpeOriPall.Destroy;
    //
    VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;
*)

procedure TFmVSOpeCab.MostraVSOpeOriPall2(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
  if not FItsIncluiOri_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSOpeOriPall2, FmVSOpeOriPall2, afmoNegarComAviso) then
  begin
    FmVSOpeOriPall2.ImgTipo.SQLType := SQLType;
    FmVSOpeOriPall2.FQrCab          := QrVSOpeCab;
    FmVSOpeOriPall2.FDsCab          := DsVSOpeCab;
    //FmVSOpeOriPall2.FVSOpeOriPallIMEI   := QrVSOpeOriPallIMEI;
    //FmVSOpeOriPall2.FVSOpeOriPallPallet := QrVSOpeOriPallPallet;
    FmVSOpeOriPall2.FEmpresa        := QrVSOpeCabEmpresa.Value;
    FmVSOpeOriPall2.FTipoArea       := QrVSOpeCabTipoArea.Value;
    FmVSOpeOriPall2.FOrigMovimNiv   := QrVSOpeAtuMovimNiv.Value;
    FmVSOpeOriPall2.FOrigMovimCod   := QrVSOpeAtuMovimCod.Value;
    FmVSOpeOriPall2.FOrigCodigo     := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriPall2.FNewGraGruX     := QrVSOpeAtuGraGruX.Value;
    FmVSOpeOriPall2.FClientMO       := QrVSOpeAtuClientMO.Value;
    FmVSOpeOriPall2.FFornecMO       := QrVSOpeAtuFornecMO.Value;
    FmVSOpeOriPall2.FStqCenLoc      := QrVSOpeAtuStqCenLoc.Value;
    //
    FmVSOpeOriPall2.EdCodigo.ValueVariant    := QrVSOpeCabCodigo.Value;
    FmVSOpeOriPall2.EdMovimCod.ValueVariant  := QrVSOpeCabMovimCod.Value;
    //
    FmVSOpeOriPall2.EdSrcMovID.ValueVariant  := QrVSOpeAtuMovimID.Value;
    FmVSOpeOriPall2.EdSrcNivel1.ValueVariant := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriPall2.EdSrcNivel2.ValueVariant := QrVSOpeAtuControle.Value;
    FmVSOpeOriPall2.EdSrcGGX.ValueVariant    := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriPall2.ReopenItensAptos();
    FmVSOpeOriPall2.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSOpeOriPall2.FDataHora                := QrVSOpeCabDtHrAberto.Value;
    end else
    begin
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSOpeOriIMEIDataHora.Value,
        QrVSOpeOriIMEIDtCorrApo.Value,  FmVSOpeOriPall2.FDataHora);
{
      FmVSOpeOriPall2.EdControle.ValueVariant := QrVSOpeCabOldControle.Value;
      //
      FmVSOpeOriPall2.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSOpeCabOldCNPJ_CPF.Value);
      FmVSOpeOriPall2.EdNomeEmiSac.Text := QrVSOpeCabOldNome.Value;
      FmVSOpeOriPall2.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSOpeOriPall2.FParcial := True;
        FmVSOpeOriPall2.DBG04Estq.Options := FmVSOpeOriPall2.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSOpeOriPall2.FParcial := False;
        FmVSOpeOriPall2.DBG04Estq.Options := FmVSOpeOriPall2.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSOpeOriPall2.FParcial := False;
        FmVSOpeOriPall2.DBG04Estq.Options := FmVSOpeOriPall2.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSOpeOriPall2.ShowModal;
    FmVSOpeOriPall2.Destroy;
    VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSOpeCab.odosapartirdoselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(True);
end;

procedure TFmVSOpeCab.MostraVSOpeOriIMEI2(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSOpeOriIMEI2, FmVSOpeOriIMEI2, afmoNegarComAviso) then
  begin
    FmVSOpeOriIMEI2.ImgTipo.SQLType := SQLType;
    FmVSOpeOriIMEI2.FQrCab          := QrVSOpeCab;
    FmVSOpeOriIMEI2.FDsCab          := DsVSOpeCab;
    //FmVSOpeOriIMEI2.FVSOpeOriIMEIIMEI   := QrVSOpeOriIMEIIMEI;
    //FmVSOpeOriIMEI2.FVSOpeOriIMEIIMEIet := QrVSOpeOriIMEIIMEIet;
    FmVSOpeOriIMEI2.FEmpresa        := QrVSOpeCabEmpresa.Value;
    FmVSOpeOriIMEI2.FClientMO       := QrVSOpeAtuClientMO.Value;
    FmVSOpeOriIMEI2.FFornecMO       := QrVSOpeAtuFornecMO.Value;
    FmVSOpeOriIMEI2.FStqCenLoc      := QrVSOpeAtuStqCenLoc.Value;
    FmVSOpeOriIMEI2.FTipoArea       := QrVSOpeCabTipoArea.Value;
    FmVSOpeOriIMEI2.FOrigMovimNiv   := QrVSOpeAtuMovimNiv.Value;
    FmVSOpeOriIMEI2.FOrigMovimCod   := QrVSOpeAtuMovimCod.Value;
    FmVSOpeOriIMEI2.FOrigCodigo     := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriIMEI2.FNewGraGruX     := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriIMEI2.EdCodigo.ValueVariant    := QrVSOpeCabCodigo.Value;
    FmVSOpeOriIMEI2.EdMovimCod.ValueVariant  := QrVSOpeCabMovimCod.Value;
    //
    FmVSOpeOriIMEI2.EdSrcMovID.ValueVariant  := QrVSOpeAtuMovimID.Value;
    FmVSOpeOriIMEI2.EdSrcNivel1.ValueVariant := QrVSOpeAtuCodigo.Value;
    FmVSOpeOriIMEI2.EdSrcNivel2.ValueVariant := QrVSOpeAtuControle.Value;
    FmVSOpeOriIMEI2.EdSrcGGX.ValueVariant    := QrVSOpeAtuGraGruX.Value;
    //
    FmVSOpeOriIMEI2.ReopenItensAptos();
    FmVSOpeOriIMEI2.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSOpeOriIMEI2.FDataHora                := QrVSOpeCabDtHrAberto.Value;
    end else
    begin
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSOpeOriIMEIDataHora.Value,
        QrVSOpeOriIMEIDtCorrApo.Value,  FmVSOpeOriIMEI2.FDataHora);
{
      FmVSOpeOriIMEI2.EdControle.ValueVariant := QrVSOpeCabOldControle.Value;
      //
      FmVSOpeOriIMEI2.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSOpeCabOldCNPJ_CPF.Value);
      FmVSOpeOriIMEI2.EdNomeEmiSac.Text := QrVSOpeCabOldNome.Value;
      FmVSOpeOriIMEI2.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSOpeOriIMEI2.FParcial := True;
        FmVSOpeOriIMEI2.DBG04Estq.Options := FmVSOpeOriIMEI2.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSOpeOriIMEI2.FParcial := False;
        FmVSOpeOriIMEI2.DBG04Estq.Options := FmVSOpeOriIMEI2.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSOpeOriIMEI2.FParcial := False;
        FmVSOpeOriIMEI2.DBG04Estq.Options := FmVSOpeOriIMEI2.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSOpeOriIMEI2.ShowModal;
    FmVSOpeOriIMEI2.Destroy;
    VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSOpeCab.Oitemselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(False);
end;

procedure TFmVSOpeCab.Ordensdeoperaoemaberto1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeOXsAbertas(1, 2, 0, 1, [TEstqMovimID.emidEmOperacao], True, False, '', 0);
end;

procedure TFmVSOpeCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSOpeCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSOpeCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSOpeCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEC(emidEmOperacao);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSOpeCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEI(emidEmOperacao);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSOpeCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSOpeCab);
  MyObjects.HabilitaMenuItemCabDelC1I3(CabExclui1, QrVSOpeCab,
  QrVSOpeOriIMEI, QrVSOpeOriPallet, QrVSOpeDst);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrVSOpeCab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrVSOpeCab);
  if QrVSOpeCabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrVSOpeCabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
  //
  TransfereTodaOPparaOutra1.Enabled := FOOOrigemMovimCod = 0;
  ReceberDadosDaOPdeOrigem1.Enabled := FOOOrigemMovimCod <> 0;
end;

procedure TFmVSOpeCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSOpeCab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  //MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSOpeDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSOpeDst);
  if (QrVSOpeCabDtHrLibOpe.Value > 2) or (QrVSOpeCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    //ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSOpeDst);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSOpeDstID_TTW.Value, [ItsExcluiDst]);
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
  //
  AtrelamentoNFsdeMO2.Enabled := PCOpeOri.ActivePage = TsRetornoMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrVSOpeCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrVSMOEnvRet);
  //
end;

procedure TFmVSOpeCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSOpeCab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  //MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSOpeOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSOpeOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaPginaelinhadeIEC1, QrVSOpeOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSOpeOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSOpeOriPallet);
  if (QrVSOpeCabDtHrLibOpe.Value > 2) or (QrVSOpeCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    //ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  //
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCOpeOri.ActivePageIndex = 1);
  InformaPginaelinhadeIEC1.Enabled := InformaNmerodaRME1.Enabled;
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSOpeOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSOpeOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  FItsExcluiOriPallet_Enabled := ItsExcluiOriPallet.Enabled;
  ItsExcluiOriPallet.Enabled  := True;
  case PCOpeOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
  //
  AtrelamentoNFsdeMO1.Enabled := PCOpeOri.ActivePage = TsEnvioMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSOpeOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  //
end;

procedure TFmVSOpeCab.porIMEI1Click(Sender: TObject);
begin
  MostraVSOpeOriIMEI2(stIns, ptTotal);
end;

procedure TFmVSOpeCab.porIMEIParcial1Click(Sender: TObject);
begin
  MostraVSOpeOriIMEI2(stIns, ptParcial);
end;

procedure TFmVSOpeCab.porPallet1Click(Sender: TObject);
begin
  //MostraVSOpeOriPall(stIns);
  MostraVSOpeOriPall2(stIns, ptTotal);
end;

procedure TFmVSOpeCab.porPalletparcial1Click(Sender: TObject);
begin
  MostraVSOpeOriPall2(stIns, ptParcial);
end;

procedure TFmVSOpeCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSOpeCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSOpeCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsopecab';
  VAR_GOTOMYSQLTABLE := QrVSOpeCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vcc.Nome NO_VSCOPCab, ');
  VAR_SQLx.Add('IF(PecasMan<>0, PecasMan, -PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(AreaManM2<>0, AreaManM2, -AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(AreaManP2<>0, AreaManP2, -AreaSrcP2) AreaINIP2,');
  VAR_SQLx.Add('IF(PesoKgMan<>0, PesoKgMan, -PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('ope.Nome NO_Operacoes, voc.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA');
  VAR_SQLx.Add('FROM vsopecab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN operacoes ope ON ope.Codigo=voc.Operacoes');
  VAR_SQLx.Add('LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSOpeCab.Desfazencerramento1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSOpeCabCodigo.Value;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE vsopecab ',
  'SET DtHrFimOpe="1899-12-30" ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  LocCod(Codigo, Codigo);
end;

procedure TFmVSOpeCab.este1Click(Sender: TObject);
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSOpeAtu, QrVSOpeOriIMEI, QrVSOpeOriPallet,
  QrVSOpeDst, (*QrVSOpeInd*)nil, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  (*QrEmit*)nil, (*QrPQO*)nil, QrVSOpeCabCodigo.Value,
  QrVSOpeCabMovimCod.Value, QrVSOpeCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmOperacao, emidEmOperacao, emidEmOperacao,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmOperInn, eminSorcOper, eminDestOper);
end;

procedure TFmVSOpeCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCOpeOri.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
  //
  LocCod(QrVSOpeCabCodigo.Value, QrVSOpeCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
  DmModVS_CRC.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
  //
  LocCod(QrVSOpeCabCodigo.Value,QrVSOpeCabCodigo.Value);
end;

procedure TFmVSOpeCab.CabLibera1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{
  Codigo := QrVSOpeCabCodigo.Value;
  //colocar metragem e encerrar!
  if DBCheck.CriaFm(TFmVSGerArtEnc, FmVSGerArtEnc, afmoNegarComAviso) then
  begin
    FmVSGerArtEnc.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtEnc.EdCodigo.ValueVariant := Codigo;
    FmVSGerArtEnc.EdMovimCod.ValueVariant := QrVSOpeAtuMovimCod.Value;
    FmVSGerArtEnc.EdControle.ValueVariant := QrVSOpeAtuControle.Value;
    //
    FmVSGerArtEnc.EdPecas.ValueVariant      := QrVSOpeAtuPecas.Value;
    FmVSGerArtEnc.EdAreaM2.ValueVariant     := QrVSOpeAtuAreaM2.Value;
    FmVSGerArtEnc.EdAreaP2.ValueVariant     := QrVSOpeAtuAreaP2.Value;
    FmVSGerArtEnc.EdCustoMOKg.ValueVariant  := QrVSOpeAtuCustoMOKg.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
    FmVSGerArtEnc.EdCustoMOTot.ValueVariant := QrVSOpeAtuCustoMOTot.Value;
    FmVSGerArtEnc.EdValorMP.ValueVariant    := QrVSOpeAtuValorMP.Value;
    FmVSGerArtEnc.EdValorT.ValueVariant     := QrVSOpeAtuValorT.Value;

    //
    FmVSGerArtEnc.EdPecasMan.ValueVariant      := QrVSOpeCabPecasMan.Value;
    FmVSGerArtEnc.EdAreaManM2.ValueVariant     := QrVSOpeCabAreaManM2.Value;
    FmVSGerArtEnc.EdAreaManP2.ValueVariant     := QrVSOpeCabAreaManP2.Value;
    //
    FmVSGerArtEnc.EdCustoManMOKg.ValueVariant  := QrVSOpeCabCustoManMOKg.Value;
    FmVSGerArtEnc.EdCustoManMOTot.ValueVariant := QrVSOpeCabCustoManMOTot.Value;
    FmVSGerArtEnc.EdValorManMP.ValueVariant    := QrVSOpeCabValorManMP.Value;
    FmVSGerArtEnc.EdValorManT.ValueVariant     := QrVSOpeCabValorManT.Value;
    //
    FmVSGerArtEnc.ShowModal;
    //
    LocCod(Codigo, Codigo);
    FmVSGerArtEnc.Destroy;
  end;
}
end;

procedure TFmVSOpeCab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSOpeCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsopecab', False, [
  'DtHrLibOpe'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

function TFmVSOpeCab.CalculoCustoOrigemM2(): Double;
begin
  Result := VS_CRC_PF.CalculoValorOrigemM2(QrVSOpeAtuAreaM2.Value,
    QrVSOpeAtuCustoPQ.Value, QrVSOpeAtuValorMP.Value,
    QrVSOpeAtuCusFrtMOEnv.Value);
end;

procedure TFmVSOpeCab.CkCpermitirCrustClick(Sender: TObject);
begin
  if CkCpermitirCrust.Checked then
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_VSFinCla))
  else
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_VSRibCla));
end;

procedure TFmVSOpeCab.Comorigemedestino1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeOrdem(emidEmOperacao, QrVSOpeCabEmpresa.Value,
  QrVSOpeCabCodigo.Value, QrVSOpeCabMovimCod.Value);
end;

procedure TFmVSOpeCab.CorrigeArtigos(Todos: Boolean);
begin
  if VS_CRC_PF.VerificaOpeSemOperacao() then
    Exit;
  VS_CRC_PF.CorrigeArtigoOpeEmDiante(QrVSOpeDstControle.Value,
    QrVSOpeDstGraGruX.Value);
  if Todos then
  begin
    QrVSOpeDst.Next;
    while not QrVSOpeDst.Eof do
    begin
      VS_CRC_PF.CorrigeArtigoOpeEmDiante(QrVSOpeDstControle.Value,
        QrVSOpeDstGraGruX.Value);
      //
      QrVSOpeDst.Next;
    end;
  end;
  //
  VS_CRC_PF.ReopenVSOpePrcDst(QrVSOpeDst, QrVSOpeCabMovimCod.Value, 0,
    QrVSOpeCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestOper);
end;

procedure TFmVSOpeCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSOpeAtuControle.Value;
  MovimCod    := QrVSOpeCabMovimCod.Value;
  Codigo      := QrVSOpeCabCodigo.Value;
  MovimNivSrc := eminSorcOper;
  MovimNivDst := eminDestOper;
  VS_CRC_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSOpeCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSOpeCab.Corrigirfornecedor1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrVSOpeCabCodigo.Value, QrVSOpeCabCodigo.Value);
end;

procedure TFmVSOpeCab.ImprimeIMEI(Formato: TXXImpImeiKind);
var
  NFeRem: String;
begin
  if QrVSOpeCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSOpeCabNFeRem.Value)
  else
    NFeRem := '';
  VS_CRC_PF.ImprimeIMEI([QrVSOpeAtuControle.Value], Formato, QrVSOpeCabLPFMO.Value,
    NFeRem, QrVSOpeCab);
end;

procedure TFmVSOpeCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCOpeOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSOpeOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, QrVSOpeCabSerieRem.Value, QrVSOpeCabNFeRem.Value);
  //
  LocCod(QrVSOpeCabCodigo.Value,QrVSOpeCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.IncluiAtrelamento2Click(Sender: TObject);
var
  MovimCod: Integer;
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCOpeOri.ActivePage := TsRetornoMO;
  //
  MovimCod := QrVSOpeCabMovimCod.Value;
  //
  VS_PF.MostraFormVSMOEnvRet(stIns, QrVSOpeAtu, QrVSOpeDst, QrVSOpeBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, MovimCod, 0, 0, siPositivo,
  FGerArX2Ret);
  //
  LocCod(QrVSOpeCabCodigo.Value,QrVSOpeCabCodigo.Value);
  //
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.Incluiraspa1Click(Sender: TObject);
const
  DesabilitaBaixa = True;
begin
  MostraVSOpeDst(stIns, DesabilitaBaixa);
end;

procedure TFmVSOpeCab.InformaNmerodaRME11Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrVSOpeOriIMEIControle.Value,
    QrVSOpeOriIMEIReqMovEstq.Value, QrVSOpeOriIMEI);
end;

procedure TFmVSOpeCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrVSOpeOriIMEIControle.Value,
    QrVSOpeOriIMEIReqMovEstq.Value, QrVSOpeOriIMEI);
end;

procedure TFmVSOpeCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrVSOpeDstControle.Value,
    QrVSOpeDstReqMovEstq.Value, QrVSOpeDst);
end;

procedure TFmVSOpeCab.InformaPginaelinhadeIEC1Click(Sender: TObject);
begin
  if VS_CRC_PF.InformaIxx(DGDadosOri, QrVSOpeOriIMEI, 'Controle') then
    PCOpeOri.ActivePageIndex := 1;
end;

procedure TFmVSOpeCab.InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO,
GraGruX: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmOperacao;
  MovimNiv       := eminEmOperInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei062(*Gera��o de couro que est� em opera��o*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('vsopecab', MovimCod);
    VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
  end;
end;

procedure TFmVSOpeCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPallet(QrVSOpeDstPallet.Value);
end;

procedure TFmVSOpeCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSOpeCabCodigo.Value;
  ///Permitir excluir IMEC 337
  ///  Excluir tambem QrVSOpeAtu!!!
  ///  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  ///  Caption + sLineBreak + TMenuItem(Sender).Name);
  VS_CRC_PF.ExcluiCabEIMEI_OpeCab(QrVSOpeCabMovimCod.Value,
    QrVSOpeAtuControle.Value, emidEmOperacao, TEstqMotivDel.emtdWetCurti038);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSOpeCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSOpeCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSOpeCab.TransfereTodaOPparaOutra1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  FOOOrigemCodigo := 0;
  FOOOrigemMovimCod := 0;
  FOOOrigemAtuControle := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM ' + CO_TAB_TAB_VMI,
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
    'AND NOT (MovimNiv IN (' + Geral.FF0(Integer(eminSorcOper)) +
    ',' + Geral.FF0(Integer(eminEmOperInn)) + '))',
    'AND MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Esta OO n�o pode ser movida pois j� posui descendentes!');
      Exit;
    end;
    //
    FOOOrigemMovimCod := QrVSOpeCabMovimCod.Value;
    FOOOrigemCodigo   := QrVSOpeCabCodigo.Value;
    FOOOrigemAtuControle := QrVSOpeAtuControle.Value;
    if FOOOrigemMovimCod <> 0 then
    begin
      Geral.MB_Info('O n�mero da OP foi salvo em mem�ria!' + sLineBreak +
      'V� para a OP de destino e clique no item de menu "Transfer�ncia de couros origem entre OPs" > "Receber dados da OP de origem" do bot�o "Em Opera��o"!');
    end;
  except
    Qry.Free;
  end;
end;

procedure TFmVSOpeCab.ReceberDadosDaOPdeOrigem1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSOpeCabCodigo.Value;
  MovimCod := QrVSOpeCabMovimCod.Value;
  if (Codigo = 0) or (MovimCod = 0) then
  begin
    Geral.MB_Aviso('Esta OP n�o pode receber dados de outra OP! Selecione uma OP v�lida!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirme a tranfer�ncia dos couros de origem da OP ' +
  Geral.FF0(FOOOrigemCodigo) + ' (IME-C ' + Geral.FF0(FOOOrigemMovimCod) +
  ') para esta OP?') = ID_Yes then
  begin
    UnDmkDAC_PF.ExecutaMySQLQUery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI +
    ' SET Codigo=' + Geral.FF0(Codigo),
    ', MovimCod=' + Geral.FF0(MovimCod),
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
    'AND Codigo=' + Geral.FF0(FOOOrigemCodigo),
    'AND MovimCod=' + Geral.FF0(FOOOrigemMovimCod),
    '']);
    //
    VS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod);
    //
    LocCod(Codigo, Codigo);
(*
    VS_CRC_PF.ExcluiCabEIMEI_OpeCab(FOOOrigemMovimCod, FOOOrigemAtuControle,
    emidEmOperacao, TEstqMotivDel.emtdWetCurti038);
*)
    //
  end;
end;

procedure TFmVSOpeCab.ReopenVSOpeOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //VS_CRC_PF.ReopenVSOpeOriIMEI(QrVSOpeOriIMEI, QrVSOpeCabMovimCod.Value, Controle);
  TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
(*
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOpeOriIMEI, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  QrVSOpeOriIMEI.Locate('Controle', Controle, []);
*)
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVSOpeOriIMEI, QrVSOpeCabMovimCod.Value, Controle,
  TemIMEIMrt, eminSorcOper, SQL_Limit);
  //
  //VS_CRC_PF.ReopenVSOpeOriPallet(QrVSOpeOriPallet, QrVSOpeCabMovimCod.Value, Pallet);
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  '']);
  SQL_Group :=   'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOpeOriPallet, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  //Geral.MB_SQL(self, QrVSOpeOriPallet);
  QrVSOpeOriPallet.Locate('Pallet', Pallet, []);
end;

procedure TFmVSOpeCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod, VSPedIts: Integer;
begin
(*
  if QrVSOpeAtuSdoVrtPeca.Value < QrVSOpeAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Opera��o que foi gerado!');
    Exit;
  end else
*)
//
  if Geral.MB_Pergunta('Confirme a exclus�o do item de destino?') = ID_YES then
//
  begin
    Codigo   := QrVSOpeCabCodigo.Value;
    CtrlDel  := QrVSOpeDstControle.Value;
    CtrlDst  := QrVSOpeDstSrcNivel2.Value;
    MovimNiv := QrVSOpeAtuMovimNiv.Value;
    MovimCod := QrVSOpeAtuMovimCod.Value;
    VSPedIts := QrVSOpeDstPedItsFin.Value;
    //
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    //CO_:::_TAB_VMI, 'Controle', CtrlDel, Dmod.MyDB) = ID_YES then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_CRC_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_CRC_PF.ObtemControleMovimTwin(
        QrVSOpeDstMovimCod.Value, QrVSOpeDstMovimTwn.Value, emidEmOperacao,
        eminEmOperBxa);
      //
      VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      VS_CRC_PF.AtualizaVSPedIts_Fin(VSPedIts);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSOpeDst,
        TIntegerField(QrVSOpeDstControle), QrVSOpeDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_CRC_PF.AtualizaTotaisVSXxxCab('vsopecab', MovimCod);
      VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
      //
      VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
      VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmOperacao, MovimCod);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_CRC_PF.ReopenVSOpePrcDst(QrVSOpeDst, QrVSOpeCabMovimCod.Value, CtrlNxt,
        QrVSOpeCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestOper);
    end;
  end;
end;

procedure TFmVSOpeCab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
(*
  if QrVSOpeAtuSdoVrtPeca.Value < QrVSOpeAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de origem cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Opera��o que foi gerado!');
    Exit;
  end else
*)
  begin
    Codigo    := QrVSOpeCabCodigo.Value;
    CtrlDel   := QrVSOpeOriIMEIControle.Value;
    CtrlOri   := QrVSOpeOriIMEISrcNivel2.Value;
    MovimNiv  := QrVSOpeAtuMovimNiv.Value;
    MovimCod  := QrVSOpeAtuMovimCod.Value;
    //
    if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
    begin
      if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      begin
        VS_CRC_PF.AtualizaSaldoIMEI(CtrlOri, True);
        //
        // Nao se aplica. Calcula com function propria a seguir.
        //VS_CRC_PF.AtualizaTotaisVSXxxCab('vsopecab', MovimCod);
        VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
        //
        CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSOpeOriIMEI,
          TIntegerField(QrVSOpeOriIMEIControle), QrVSOpeOriIMEIControle.Value);
        //
        VS_CRC_PF.AtualizaFornecedorOpe(MovimCod);
        CorrigeFornecedorePalletsDestino(False);
        LocCod(Codigo, Codigo);
        ReopenVSOpeOris(CtrlNxt, 0);
      end;
    end;
  end;
end;

procedure TFmVSOpeCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if not FItsExcluiOriPallet_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  //
  if (QrVSOpeAtuSdoVrtPeca.Value < QrVSOpeAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSOpeOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSOpeOriIMEI.First;
    while not QrVSOpeOriIMEI.Eof do
    begin
      if QrVSOpeOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSOpeOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSOpeOriIMEI.First;
        while not QrVSOpeOriIMEI.Eof do
        begin
          if QrVSOpeOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSOpeCabCodigo.Value;
            CtrlDel   := QrVSOpeOriIMEIControle.Value;
            CtrlOri   := QrVSOpeOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSOpeAtuMovimNiv.Value;
            MovimCod  := QrVSOpeAtuMovimCod.Value;
            //
            if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti038),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_CRC_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_CRC_PF.AtualizaTotaisVSXxxCab('vsopecab', MovimCod);
              VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
            end;
            //
          end;
          QrVSOpeOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSOpeOriIMEI,
      TIntegerField(QrVSOpeOriIMEIControle), QrVSOpeOriIMEIControle.Value);
      //
      VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSOpeOris(CtrlNxt, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSOpeCab.DefineONomeDoForm;
begin
end;

procedure TFmVSOpeCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSOpeCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSOpeCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSOpeCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSOpeCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSOpeCab.SpeedButton5Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VAR_CADASTRO := 0;
  VS_PF.MostraFormOperacoes(EdOperacoes.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdOperacoes, CBOperacoes, QrOperacoes, VAR_CADASTRO);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOpeCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSOpeCabCodigo.Value;
  Close;
end;

procedure TFmVSOpeCab.ItsIncluiDstClick(Sender: TObject);
const
  DesabilitaBaixa = False;
begin
  MostraVSOpeDst(stIns, DesabilitaBaixa);
end;

procedure TFmVSOpeCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSOpeCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsopecab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSOpeCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdSerieRem.ValueVariant   := QrVSOpeCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSOpeCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSOpeCabLPFMO.Value;
  //
  EdOperacoes.ValueVariant   := QrVSOpeCabOperacoes.Value;
  CBOperacoes.KeyValue       := QrVSOpeCabOperacoes.Value;
  //
  //
  EdControle.ValueVariant   := QrVSOpeAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSOpeAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSOpeAtuGraGruX.Value;
  EdCustoMOTot.ValueVariant := QrVSOpeAtuCustoMOTot.Value;
  EdPecas.ValueVariant      := QrVSOpeAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSOpeAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSOpeAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSOpeAtuObserv.Value;
  //
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, QrVSOpeAtuControle.Value, QrVSOpeAtuControle.Value);
  Habilita := QrVSOpeAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSOpeAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSOpeAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSOpeAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSOpeAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSOpeAtuReqMovEstq.Value;
  //
  EdClientMO.ValueVariant   := QrVSOpeAtuClientMO.Value;
  CBClientMO.KeyValue       := QrVSOpeAtuClientMO.Value;
  //
  EdFornecMO.ValueVariant   := QrVSOpeAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSOpeAtuFornecMO.Value;
  //
  EdVSCOPCab.ValueVariant   := QrVSOpeCabVSCOPCab.Value;
  CBVSCOPCab.KeyValue       := QrVSOpeCabVSCOPCab.Value;
  //
end;

procedure TFmVSOpeCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, MovimCod, TipoArea, FornecMO, Operacoes, Forma,
  Controle, PedItsLib, StqCenLoc, SerieRem, NFeRem, ClientMO, GGXDst,
  VSCOPCab: Integer;
  Nome, DtHrAberto, LPFMO: String;
  SQLType: TSQLType;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  Operacoes      := EdOperacoes.ValueVariant;
  VSCOPCab       := EdVSCOPCab.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdCLientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst, 'Informe o Artigo a ser produzido!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque!') then
    Exit;
  if MyObjects.FIC(Operacoes = 0, EdOperacoes, 'Informe o c�digo de cadastro da opera��o!') then
    Exit;
  //
  if not VS_CRC_PF.ValidaCampoNF(2, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsopecab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsopecab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'NFeRem', 'SerieRem', 'LPFMO',
  'GGXDst', 'Operacoes', 'VSCOPCab'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  NFeRem, SerieRem, LPFMO,
  GGXDst, Operacoes, VSCOPCab], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidEmOperacao, Codigo);
    InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO, GraGruX, DtHrAberto);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSOpeOriIMEI.RecordCount = 0) then
    begin
      //MostraVSOpeOriPall(stIns);
      ItsIncluiDst.Enabled  := True;
      //
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      'Pallet Total', 'Pallet Parcial',
      'IME-I Total',  'IME-I Parcial'], -1);
      case Forma of
        0: MostraVSOpeOriPall2(stIns, ptTotal);
        1: MostraVSOpeOriPall2(stIns, ptParcial);
        2: MostraVSOpeOriIMEI2(stIns, ptTotal);
        3: MostraVSOpeOriIMEI2(stIns, ptParcial);
      end;
      ItsIncluiDst.Enabled  := False;
    end else begin
      VS_CRC_PF.AtualizaFornecedorOpe(MovimCod);
      //
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmOperacao, QrVSOpeCabMovimCod.Value);
    end;
  end;
end;

procedure TFmVSOpeCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsopecab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsopecab', 'Codigo');
end;

procedure TFmVSOpeCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSOpeCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSOpeCab.Alteraartigodedestino1Click(Sender: TObject);
var
  DesabilitaBaixa: Boolean;
begin
  DesabilitaBaixa := QrVSOpeBxa.RecordCount = 0;
  MostraVSOpeDst(stUpd, DesabilitaBaixa);
end;

procedure TFmVSOpeCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCOpeOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSOpeOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, 0, 0);
  //
  LocCod(QrVSOpeCabCodigo.Value,QrVSOpeCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCOpeOri.ActivePage := TsRetornoMO;
  //
  VS_PF.MostraFormVSMOEnvRet(stUpd, QrVSOpeAtu, QrVSOpeDst, QrVSOpeBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, 0, 0, 0, siPositivo, FGerArX2Env);
  //
  LocCod(QrVSOpeCabCodigo.Value,QrVSOpeCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.AlteraLocaldoestoque1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoStqCenLoc(QrVSOpeDstControle.Value,
    QrVSOpeDstStqCenLoc.Value, QrVSOpeDst);
end;

procedure TFmVSOpeCab.AlteraLocaldoestoque2Click(Sender: TObject);
begin
  VS_CRC_PF.InfoStqCenLoc(QrVSOpeAtuControle.Value,
    QrVSOpeAtuStqCenLoc.Value, QrVSOpeAtu);
end;

procedure TFmVSOpeCab.AlteraPallet1Click(Sender: TObject);
begin
  if QrVSOpeDstPecas.Value <> QrVSOpeDstSdoVrtPeca.Value   then
  begin
    Geral.MB_Info('Altera��o abortada! Saldo difere do esperado!');
    Exit;
  end;
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  VS_CRC_PF.InfoPalletVMI(QrVSOpeDstControle.Value,
    QrVSOpeDstPallet.Value, QrVSOpeDst);
end;

procedure TFmVSOpeCab.Apreencher1Click(Sender: TObject);
begin
  ImprimeIMEI(viikOPaPreencher);
end;

procedure TFmVSOpeCab.AtualizaestoqueEmOperao1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSOpeCabMovimCod.Value);
// Parei Aqui 2014-12-14
  LocCod(QrVSOpeCabCodigo.Value, QrVSOpeCabCodigo.Value);
end;

procedure TFmVSOpeCab.AtualizaFornecedoresDeDestino();
var
  Qry: TmySQLQuery;
  Codigo, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, MovimCod',
    'FROM vsopecab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSOpeCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo   := Qry.FieldByName('Codigo').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando a OO ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_CRC_PF.AtualizaFornecedorOpe(QrVSOpeCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSOpeCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSOpeCabMovimCod.Value, TEstqMovimID.emidEmOperacao, [eminSorcOper],
  [eminEmOperInn, eminDestOper, eminEmOperBxa]);
end;

procedure TFmVSOpeCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSOpeCab, QrVSOpeCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, CabAltera1, Corrigirfornecedor1,
  Desfazencerramento1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSOpeCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  FOOOrigemCodigo := 0;
  FOOOrigemMovimCod := 0;
  FOOOrigemAtuControle := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  PnDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_4096_VSRibOpe));
  VS_CRC_PF.AbreGraGruXY(QrGGXDst,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_VSRibCla));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
  VS_CRC_PF.ReopenVSCOPCab(QrVSCOPCab, emidEmOperacao);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCOpeOri, VAR_PC_FRETE_VS_NAME, 0);
  //MyObjects.DefinePageControlActivePageByName(PCOpeDst, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSOpeCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSOpeCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSOpeCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSOpeCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSOpeCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSOpeCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSOpeCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVmi(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.Value, 0);
end;

procedure TFmVSOpeCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

procedure TFmVSOpeCab.QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi, QrVSMOEnvRetCodigo.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi, QrVSMOEnvRetCodigo.Value, 0);
end;

procedure TFmVSOpeCab.QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvGVmi.Close;
  QrVSMOEnvRVmi.Close;
end;

procedure TFmVSOpeCab.QrVSOpeCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSOpeCab.QrVSOpeCabAfterScroll(DataSet: TDataSet);
begin
{
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSOpeAtu, QrVSOpeCabMovimCod.Value, 0,
  QrVSOpeCabTemIMEIMrt.Value, eminEmOperInn);
  //VS_CRC_PF.ReopenVSOpeOri(QrVSOpeOri, QrVSOpeCabMovimCod.Value, 0);
  ReopenVSOpeOris(0, 0);
  VS_CRC_PF.ReopenVSOpePrcDst(QrVSOpeDst, QrVSOpeCabMovimCod.Value, 0,
    QrVSOpeCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestOper);
  //ReopenForcados(0);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(QrForcados, 0, QrVSOpeCabCodigo.Value,
    QrVSOpeAtuControle.Value, QrVSOpeCabTemIMEIMrt.Value,  emidEmOperacao);
}
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSOpeAtu, QrVSOpeOriIMEI, QrVSOpeOriPallet,
  QrVSOpeDst, (*QrVSOpeInd*)nil, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  (*QrEmit*)nil, (*QrPQO*)nil, QrVSOpeCabCodigo.Value,
  QrVSOpeCabMovimCod.Value, QrVSOpeCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmOperacao, emidEmOperacao, emidEmOperacao,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmOperInn, eminSorcOper, eminDestOper);
  //
{$IfDef sAllVS}
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSOpeCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet, QrVSOpeCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOpeCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSOpeCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSOpeCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSOpeCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsopecab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSOpeCab.SbVSCOPCabCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_CRC_PF.MostraFormVSCOPCab(0);
  UMyMod.SetaCodigoPesquisado(EdVSCOPCab, CBVSCOPCab, QrVSCOPCab, VAR_CADASTRO);
end;

procedure TFmVSOpeCab.SbVSCOPCabCpyClick(Sender: TObject);
const
  EdCliente = nil;
  CBCliente = nil;
  EdCustoMOKg = nil;
begin
  VS_CRC_PF.PreencheDadosDeVSCOPCab(EdVSCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoArea,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOKg,
  nil, nil, nil, nil, nil, nil, nil, nil, nil, nil);
end;

procedure TFmVSOpeCab.Somenteinfo1Click(Sender: TObject);
begin
  ImprimeIMEI(viikArtigoGerado);
end;

procedure TFmVSOpeCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOpeCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSOpeCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSOpeCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsopecab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  LaPedItsLib.Enabled       := True;
  EdPedItsLib.Enabled       := True;
  CBPedItsLib.Enabled       := True;
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSOpeCab.QrVSOpeCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSOpeAtu.Close;
  QrVSOpeOriIMEI.Close;
  QrVSOpeOriPallet.Close;
  QrVSOpeDst.Close;
  QrForcados.Close;
  QrVSMOEnvEnv.Close;
  QrVSMOEnvRet.Close;
end;

procedure TFmVSOpeCab.QrVSOpeCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSOpeCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSOpeCab.QrVSOpeCabCalcFields(DataSet: TDataSet);
begin
  case QrVSOpeCabTipoArea.Value of
    0: QrVSOpeCabNO_TIPO.Value := 'm�';
    1: QrVSOpeCabNO_TIPO.Value := 'ft�';
    else QrVSOpeCabNO_TIPO.Value := '???';
  end;
  QrVSOpeCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSOpeCabDtHrLibOpe.Value, 106, True);
  QrVSOpeCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSOpeCabDtHrFimOpe.Value, 106, True);
  QrVSOpeCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSOpeCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSOpeCab.QrVSOpeDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmOperBxa)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSOpeDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOpeBxa, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  //QrVSOpeBxa.Locate('Controle', Controle, []);
  //
end;

procedure TFmVSOpeCab.QrVSOpeDstBeforeClose(DataSet: TDataSet);
begin
  QrVSOpeBxa.Close;
end;

end.

