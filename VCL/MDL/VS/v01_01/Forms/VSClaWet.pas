unit VSClaWet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmVSClaWet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSRibCla: TmySQLQuery;
    DsVSRibCla: TDataSource;
    QrVSWetEnd: TmySQLQuery;
    DsVSWetEnd: TDataSource;
    QrVSWetEndGraGruX: TIntegerField;
    QrVSWetEndNO_PRD_TAM_COR: TWideStringField;
    QrVSRibClaGraGruX: TIntegerField;
    QrVSRibClaNO_PRD_TAM_COR: TWideStringField;
    LaVSRibCla: TLabel;
    EdVSRibCla: TdmkEditCB;
    CBVSRibCla: TdmkDBLookupComboBox;
    LaVSWetEnd: TLabel;
    EdVSWetEnd: TdmkEditCB;
    CBVSWetEnd: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTabLink(Codigo: Integer);
    function  LinkExiste(VSRibCla, VSWetEnd: Integer): Boolean;
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FCodigo: Integer;
  end;

  var
  FmVSClaWet: TFmVSClaWet;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmVSClaWet.BtOKClick(Sender: TObject);
var
  Codigo, VSRibCla, VSWetENd: Integer;
begin
  Codigo         := FCodigo;
  VSRibCla       := EdVSRibCla.ValueVariant;
  VSWetEnd       := EdVSWetEnd.ValueVariant;
  //
  if MyObjects.FIC(VSRibCla = 0, EdVSRibCla,
  'Informe o reduzido do artigo de ribeira!') then
    Exit;
  if MyObjects.FIC(VSWetEnd = 0, EdVSWetEnd,
  'Informe o reduzido do semi acabado!') then
    Exit;
  //
  if LinkExiste(VSRibCla, VSWetEnd) then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsclawet', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsclawet', False, [
  'VSRibCla', 'VSWetEnd'], [
  'Codigo'], [
  VSRibCla, VSWetEnd], [
  Codigo], True) then
  begin
    ReopenTabLink(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      if LaVSRibCla.Enabled then
      begin
        EdVSRibCla.ValueVariant := 0;
        CBVSRibCla.KeyValue     := 0;
        //
        EdVSRibCla.SetFocus;
      end;
      //
      if LaVSRibCla.Enabled then
      begin
        EdVSRibCla.ValueVariant := 0;
        CBVSRibCla.KeyValue     := 0;
        //
        EdVSRibCla.SetFocus;
      end;
    end else Close;
  end;
end;

procedure TFmVSClaWet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSClaWet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClaWet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVSRibCla, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSWetEnd, Dmod.MyDB);
end;

procedure TFmVSClaWet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSClaWet.LinkExiste(VSRibCla, VSWetEnd: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsclawet ',
    'WHERE VSRibCla=' + Geral.FF0(VSRibCla),
    'AND VSWetEnd=' + Geral.FF0(VSWetEnd),
    '']);
    Result := Qry.RecordCount > 0;
  finally
    Qry.Free;
  end;
  if Result then
    Geral.MB_Aviso('Link de interclasses configurado j� existe!');
end;

procedure TFmVSClaWet.ReopenTabLink(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
