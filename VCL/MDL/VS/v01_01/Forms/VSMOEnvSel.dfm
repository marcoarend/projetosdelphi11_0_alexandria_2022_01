object FmVSMOEnvSel: TFmVSMOEnvSel
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-219 :: Sele'#231#227'o de Enviado(s) para MO'
  ClientHeight = 629
  ClientWidth = 847
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 847
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 799
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 751
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 387
        Height = 32
        Caption = 'Sele'#231#227'o de Enviado(s) para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 387
        Height = 32
        Caption = 'Sele'#231#227'o de Enviado(s) para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 387
        Height = 32
        Caption = 'Sele'#231#227'o de Enviado(s) para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 847
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 847
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 847
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 843
          Height = 62
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label9: TLabel
            Left = 12
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label12: TLabel
            Left = 48
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Num NF:'
          end
          object Label31: TLabel
            Left = 124
            Top = 4
            Width = 141
            Height = 13
            Caption = 'Prestador do servi'#231'o da M.O.:'
          end
          object Label11: TLabel
            Left = 584
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label1: TLabel
            Left = 508
            Top = 4
            Width = 66
            Height = 13
            Caption = 'IME-C origem:'
          end
          object EdNFEMP_SerNF: TdmkEdit
            Left = 12
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFEMP_nNF: TdmkEdit
            Left = 47
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_Terceiro: TdmkEditCB
            Left = 124
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNFEMP_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFEMP_Terceiro: TdmkDBLookupComboBox
            Left = 180
            Top = 20
            Width = 325
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador
            TabOrder = 3
            dmkEditCB = EdNFEMP_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdNFEMP_Empresa: TdmkEdit
            Left = 584
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdVSVMI_MovimCod: TdmkEdit
            Left = 507
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdVSVMI_MovimCodRedefinido
          end
          object CkComSaldo: TCheckBox
            Left = 12
            Top = 44
            Width = 653
            Height = 17
            Caption = 'Somente itens com saldo de pe'#231'as para retorno.'
            TabOrder = 6
          end
          object BtPesquisa: TBitBtn
            Tag = 14
            Left = 716
            Top = 16
            Width = 120
            Height = 40
            Caption = '&Reabrir'
            NumGlyphs = 2
            TabOrder = 7
            OnClick = BtPesquisaClick
          end
        end
        object DBGVSMOEnvEnv: TdmkDBGridZTO
          Left = 2
          Top = 77
          Width = 843
          Height = 388
          Align = alClient
          DataSource = DsVSMOEnvEnv
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGVSMOEnvEnvDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID Envio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_SerNF'
              Title.Caption = 'S'#233'rie NF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_nNF'
              Title.Caption = 'n'#186' NF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_SdoPeca'
              Title.Caption = 'Saldo P'#231
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_SdoPeso'
              Title.Caption = 'Saldo Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_SdoArM2'
              Title.Caption = 'Saldo m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_ValorT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CFTMP_ValorT'
              Title.Caption = 'Frete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFEMP_Terceiro'
              Title.Caption = 'Terceiro'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSVMI_MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 847
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 843
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 847
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 701
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 699
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 7
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 136
    Top = 172
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 136
    Top = 220
  end
  object QrVSMOEnvEnv: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSMOEnvEnvAfterOpen
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo')
    Left = 48
    Top = 172
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvNFEMP_SdoPeca: TFloatField
      FieldName = 'NFEMP_SdoPeca'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvNFEMP_SdoPeso: TFloatField
      FieldName = 'NFEMP_SdoPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvNFEMP_SdoArM2: TFloatField
      FieldName = 'NFEMP_SdoArM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 48
    Top = 220
  end
end
