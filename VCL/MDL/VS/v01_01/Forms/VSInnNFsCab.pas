unit VSInnNFsCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkCheckBox, dmkEditCalc;

type
  TFmVSInnNFsCab = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBMotorista: TdmkDBLookupComboBox;
    EdMotorista: TdmkEditCB;
    Label1: TLabel;
    SBMotorista: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdCodigo: TdmkEdit;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    LaPecas: TLabel;
    EdPesoKg: TdmkEdit;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    CkContinuar_: TCheckBox;
    QrFornecedores: TMySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOME: TWideStringField;
    QrFornecedoresTipo: TSmallintField;
    QrFornecedoresCNPJ: TWideStringField;
    QrFornecedoresCPF: TWideStringField;
    DsFornecedores: TDataSource;
    QrTransportadores: TMySQLQuery;
    QrTransportadoresTipo: TSmallintField;
    QrTransportadoresCNPJ: TWideStringField;
    QrTransportadoresCPF: TWideStringField;
    QrTransportadoresCodigo: TIntegerField;
    QrTransportadoresNOME: TWideStringField;
    DsTransportadores: TDataSource;
    QrCI: TMySQLQuery;
    QrCITipo: TSmallintField;
    QrCICNPJ: TWideStringField;
    QrCICPF: TWideStringField;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    DsCI: TDataSource;
    Panel3: TPanel;
    PainelDados: TPanel;
    Label17: TLabel;
    Label11: TLabel;
    Label3: TLabel;
    Label19: TLabel;
    LaCI: TLabel;
    Label9: TLabel;
    LaSitPedido: TLabel;
    Label108: TLabel;
    Label16: TLabel;
    TPDT_E_S: TdmkEditDateTimePicker;
    TPDT_DOC: TdmkEditDateTimePicker;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    CBCI: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBTransportador: TdmkDBLookupComboBox;
    EdTransportador: TdmkEditCB;
    EdPedido: TdmkEdit;
    EdIND_FRT: TdmkEdit;
    EdIND_FRT_TXT: TdmkEdit;
    EdIND_PGTO: TdmkEdit;
    EdIND_PGTO_TXT: TdmkEdit;
    Panel5: TPanel;
    LaCHV_NFE: TLabel;
    EdCHV_NFE: TdmkEdit;
    LaCOD_MOD: TLabel;
    EdCOD_MOD: TdmkEdit;
    EdSER: TdmkEdit;
    LaSER: TLabel;
    LaNUM_DOC: TLabel;
    EdNUM_DOC: TdmkEdit;
    SbNF_VP: TSpeedButton;
    EdStaLnk: TdmkEdit;
    Label12: TLabel;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdMovimCod: TdmkEdit;
    Label13: TLabel;
    Label25: TLabel;
    EdEmpresa: TdmkEdit;
    EdCOD_SIT: TdmkEdit;
    Label26: TLabel;
    EdCOD_SIT_TXT: TdmkEdit;
    CkTipoNF: TdmkCheckBox;
    Label18: TLabel;
    EdVL_MERC: TdmkEdit;
    Label20: TLabel;
    EdVL_FRT: TdmkEdit;
    EdVL_SEG: TdmkEdit;
    Label22: TLabel;
    Label28: TLabel;
    EdVL_OUT_DA: TdmkEdit;
    Label29: TLabel;
    EdVL_DESC: TdmkEdit;
    EdVL_DOC: TdmkEdit;
    Label4: TLabel;
    EdVL_BC_ICMS: TdmkEdit;
    Label14: TLabel;
    EdVL_ICMS: TdmkEdit;
    Label15: TLabel;
    EdVL_BC_ICMS_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    Label24: TLabel;
    Label27: TLabel;
    EdVL_IPI: TdmkEdit;
    Label32: TLabel;
    EdVL_PIS: TdmkEdit;
    Label35: TLabel;
    EdVL_COFINS: TdmkEdit;
    Label8: TLabel;
    EdVL_PIS_ST: TdmkEdit;
    EdVL_COFINS_ST: TdmkEdit;
    Label21: TLabel;
    Label7: TLabel;
    Qr00_NFeCabA: TMySQLQuery;
    Qr00_NFeCabAFatID: TIntegerField;
    Qr00_NFeCabAFatNum: TIntegerField;
    Qr00_NFeCabAEmpresa: TIntegerField;
    Qr00_NFeCabAIDCtrl: TIntegerField;
    Qr00_NFeCabAAtrelaFatID: TIntegerField;
    Qr00_NFeCabAAtrelaFatNum: TSmallintField;
    Qr00_NFeCabAAtrelaStaLnk: TSmallintField;
    Qr00_NFeCabAide_mod: TSmallintField;
    Qr00_NFeCabAide_serie: TIntegerField;
    Qr00_NFeCabAide_nNF: TIntegerField;
    Qr00_NFeCabAId: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBMotoristaClick(Sender: TObject);
    procedure EdCOD_SITChange(Sender: TObject);
    procedure EdCOD_SITKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkTipoNFClick(Sender: TObject);
    procedure EdIND_FRTChange(Sender: TObject);
    procedure EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    // Meu
    procedure CalculaValorTotalDocumento(Sender: TObject);
    procedure SbNF_VPClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
  private
    { Private declarations }
    F_IND_PGTO_EFD, F_IND_FRT_EFD, F_COD_SIT_EFD: MyArrayLista;
    //
    function ItemDeListaSpedNaoDefinido(Codigo: Integer; Texto, Mensagem:
             String; Lista: MyArrayLista): Boolean;
  public
    { Public declarations }
    FControle: Integer;
    //
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSInnNFsCab: TFmVSInnNFsCab;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, (*VSInnCab,*) NFeXMLGerencia, NFe_PF;

{$R *.DFM}

procedure TFmVSInnNFsCab.BtOKClick(Sender: TObject);
var
  Placa: String;
  Codigo, Controle, Motorista: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SQLType: TSQLType;
var
  CHV_NFE, DT_DOC, DT_E_S, IND_PGTO, IND_FRT(*, VSVmcObs, VSVmcSeq*): String;
  MovimCod, Empresa, COD_MOD, COD_SIT, SER, NUM_DOC, NFeStatus, FatID, FatNum,
  StaLnk(*, VSVmcWrn, VSVmcSta*), Terceiro: Integer;
  (*
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  *)
  //
  DataChegadaInvalida: Boolean;

  //
  function ChaveDocNotOK(): Boolean;
  var
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
  begin
    Result := Length(CHV_NFE) <> 44;
    if MyObjects.FIC(Result = True, EdCHV_NFE,
      'Defina a chave de acesso do documento!') then Exit;
    //
    NFeXMLGeren.DesmontaChaveDeAcesso(CHV_NFE, 2.00,
      UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    //
    Result := ModNFe <> '55';
    if MyObjects.FIC(Result = True, EdCHV_NFE,
      'Chave de acesso do documento n�o implementada!: '+ EdCHV_NFE.Text) then Exit;
    //
    Result := COD_MOD <> Geral.IMV(ModNFe);
    if MyObjects.FIC(Result, EdCOD_MOD,
      'O modelo do documento n�o confere com a chave!') then Exit;
    //
    Result := SER <> Geral.IMV(SerNFe);
    if MyObjects.FIC(Result, EdSER,
      'A s�ire do documento n�o confere com a chave!') then Exit;
    //
    Result := NUM_DOC <> Geral.IMV(NumNFe);
    if MyObjects.FIC(Result, EdNUM_DOC,
      'O n�mero da NF do documento n�o confere com a chave!') then Exit;
  end;
  //
begin
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  FControle      := EdControle.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  COD_MOD        := EdCOD_MOD.ValueVariant;
  COD_SIT        := EdCOD_SIT.ValueVariant;
  SER            := EdSER.ValueVariant;
  NUM_DOC        := EdNUM_DOC.ValueVariant;
  CHV_NFE        := EdCHV_NFE      .ValueVariant;
  NFeStatus      := 100;
  DT_DOC         := Geral.FDT(TPDT_DOC.Date, 1);
  DT_E_S         := Geral.FDT(TPDT_E_S.Date, 1);
  IND_PGTO       := EdIND_PGTO     .ValueVariant;
  IND_FRT        := EdIND_FRT      .ValueVariant;
(*
  VL_DOC         := EdVL_DOC       .ValueVariant;
  VL_DESC        := EdVL_DESC      .ValueVariant;
  VL_ABAT_NT     := 0.00;  // Abatimento n�o comercial (Ex.: Desconto ICMS nas remessas para ZFM)
  VL_MERC        := EdVL_MERC      .ValueVariant;
  VL_FRT         := EdVL_FRT       .ValueVariant;
  VL_SEG         := EdVL_SEG       .ValueVariant;
  VL_OUT_DA      := EdVL_OUT_DA    .ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS   .ValueVariant;
  VL_ICMS        := EdVL_ICMS      .ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST   .ValueVariant;
  VL_IPI         := EdVL_IPI       .ValueVariant;
  VL_PIS         := EdVL_PIS       .ValueVariant;
  VL_COFINS      := EdVL_COFINS    .ValueVariant;
  VL_PIS_ST      := EdVL_PIS_ST    .ValueVariant;
  VL_COFINS_ST   := EdVL_COFINS_ST .ValueVariant;
*)
  FatID          := EdFatID        .ValueVariant;
  FatNum         := EdFatNum       .ValueVariant;
  StaLnk         := EdStaLnk       .ValueVariant;
(*
  VSVmcWrn       := EdVSVmcWrn     .ValueVariant;
  VSVmcObs       := EdVSVmcObs     .ValueVariant;
  VSVmcSeq       := EdVSVmcSeq     .ValueVariant;
  VSVmcSta       := EdVSVmcSta     .ValueVariant;
*)
  Terceiro := EdFornecedor.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  //
  if MyObjects.FIC(TPDT_E_S.Date < 2, TPDT_E_S, 'Defina a data de entrada!') then Exit;
  if MyObjects.FIC(TPDT_DOC.Date < 2, TPDT_DOC, 'Defina a data da emiss�o do documento!') then Exit;
  DataChegadaInvalida := (Trunc(TPDT_DOC.Date) > Trunc(TPDT_E_S.Date));
  if MyObjects.FIC(DataChegadaInvalida, TPDT_DOC,
    'A data de chegada n�o pode ser menor que a data da emiss�o do documento!') then Exit;
  //
  if ChaveDocNotOK() then Exit;
  //
  if MyObjects.FIC(Pecas = 0, EdPecas, 'Informe a quantidade de pe�as!') then
    Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Informe o peso (kg)!') then
    Exit;
  //
  if MyObjects.FIC(Terceiro = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  //
  if ItemDeListaSpedNaoDefinido(EdIND_PGTO.ValueVariant, EdIND_PGTO_TXT.Text,
    'Informe o Indicador do tipo de pagamento', F_IND_PGTO_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdIND_FRT.ValueVariant, EdIND_FRT_TXT.Text,
    'Informe o Indicador do tipo de frete', F_IND_FRT_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdCOD_SIT.ValueVariant, EdCOD_SIT_TXT.Text,
    'Informe o C�digo da situa��o do documento fiscal', F_COD_SIT_EFD) then Exit;
  //
  //if not VS_CRC_PF.ValidaCampoNF(5, Edide_nNF, True) then Exit;
  //
  FControle := UMyMod.BPGS1I32('vsinnnfscab', 'Controle', '', '', tsPos, SQLTYpe, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinnnfscab', False, [
  'Codigo', 'MovimCod', 'Empresa',
  'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',
  'Motorista', 'Placa', 'COD_MOD',
  'COD_SIT', 'SER', 'NUM_DOC',
  'CHV_NFE', 'NFeStatus', 'DT_DOC',
  'DT_E_S', (*'VL_DOC',*) 'IND_PGTO',
  (*'VL_DESC', 'VL_ABAT_NT', 'VL_MERC',*)
  'IND_FRT', (*'VL_FRT', 'VL_SEG',
  'VL_OUT_DA', 'VL_BC_ICMS', 'VL_ICMS',
  'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_IPI',
  'VL_PIS', 'VL_COFINS', 'VL_PIS_ST',
  'VL_COFINS_ST',*) 'FatID', 'FatNum',
  'StaLnk'(*, 'VSVmcWrn', 'VSVmcObs',
  'VSVmcSeq', 'VSVmcSta'*), 'Terceiro'], [
  'Controle'], [
  Codigo, MovimCod, Empresa,
  Pecas, PesoKg,
  AreaM2, AreaP2, ValorT,
  Motorista, Placa, COD_MOD,
  COD_SIT, SER, NUM_DOC,
  CHV_NFE, NFeStatus, DT_DOC,
  DT_E_S, (*VL_DOC,*) IND_PGTO,
  (*VL_DESC, VL_ABAT_NT, VL_MERC,*)
  IND_FRT, (*VL_FRT, VL_SEG,
  VL_OUT_DA, VL_BC_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI,
  VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST,*) FatID, FatNum,
  StaLnk(*, VSVmcWrn, VSVmcObs,
  VSVmcSeq, VSVmcSta*), Terceiro], [
  FControle], True) then
  begin
    Close;
  end;
end;

procedure TFmVSInnNFsCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnNFsCab.CalculaValorTotalDocumento(Sender: TObject);
var
  VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_DESC, ValorNF: Double;
begin
  VL_MERC  := EdVL_MERC.ValueVariant;
  VL_FRT := EdVL_FRT.ValueVariant;
  VL_SEG   := EdVL_SEG.ValueVariant;
  VL_OUT_DA  := EdVL_OUT_DA.ValueVariant;
  VL_DESC := EdVL_DESC.ValueVariant;
  //
  ValorNF := VL_MERC + VL_FRT + VL_SEG - VL_DESC + VL_OUT_DA;
  EdVL_DOC.ValueVariant := ValorNF;
end;

procedure TFmVSInnNFsCab.CkTipoNFClick(Sender: TObject);
var
  EhNFe: Boolean;
begin
  EhNFe := CkTipoNF.Checked;
  //
  LaCHV_NFE.Enabled := EhNFe;
  EdCHV_NFE.Enabled := EhNFe;
  //
  LaCOD_MOD.Enabled := not EhNFe;
  EdCOD_MOD.Enabled := not EhNFe;
  LaSer.Enabled     := not EhNFe;
  EdSER.Enabled     := not EhNFe;
  LaNUM_DOC.Enabled := not EhNFe;
  EdNUM_DOC.Enabled := not EhNFe;
end;

procedure TFmVSInnNFsCab.EdCOD_SITChange(Sender: TObject);
begin
  EdCOD_SIT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCOD_SIT_EFD,  EdCOD_SIT.ValueVariant);
end;

procedure TFmVSInnNFsCab.EdCOD_SITKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCOD_SIT.Text := Geral.SelecionaItem(F_COD_SIT_EFD, 0,
      'SEL-LISTA-000 :: Situa��o do Documento Fiscal',
      TitCols, Screen.Width)
  end;
end;

procedure TFmVSInnNFsCab.EdEmpresaChange(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCI, Dmod.MyDB, [
  'SELECT Codigo, Tipo, CNPJ, CPF,',
  'IF(Tipo=0, RazaoSocial, Nome) NOME',
  'FROM entidades',
  'WHERE Codigo=' + Geral.FF0(EdEmpresa.ValueVariant),
  'ORDER BY Nome',
  '']);
end;

procedure TFmVSInnNFsCab.EdFornecedorChange(Sender: TObject);
var
  Fornecedor: Integer;
begin
  Fornecedor := EdFornecedor.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedores, Dmod.MyDB, [
  'SELECT Codigo, Tipo, CNPJ, CPF,',
  'IF(Tipo=0, RazaoSocial, Nome) NOME',
  'FROM entidades',
  'WHERE Codigo=' + Geral.FF0(Fornecedor),
  'ORDER BY Nome',
  '']);
  if EdFornecedor.ValueVariant <> Fornecedor then
    EdFornecedor.ValueVariant := Fornecedor;
  if CBFornecedor.KeyValue <> Fornecedor then
    CBFornecedor.KeyValue := Fornecedor;
end;

procedure TFmVSInnNFsCab.EdIND_FRTChange(Sender: TObject);
begin
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
end;

procedure TFmVSInnNFsCab.EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_FRT.Text := Geral.SelecionaItem(F_IND_FRT_EFD, 0,
      'SEL-LISTA-000 :: Indicador do Tipo de Frete',
      TitCols, Screen.Width)
  end;
end;

procedure TFmVSInnNFsCab.EdIND_PGTOChange(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
end;

procedure TFmVSInnNFsCab.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmVSInnNFsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnNFsCab.FormCreate(Sender: TObject);
begin
  FControle := 0;
  ImgTipo.SQLType := stLok;
  //
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
  F_IND_FRT_EFD  := UnNFe_PF.ListaIND_FRT_EFD();
  F_COD_SIT_EFD  := UnNFe_PF.ListaCOD_SIT_EFD();
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
end;

procedure TFmVSInnNFsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSInnNFsCab.ItemDeListaSpedNaoDefinido(Codigo: Integer;
  Texto, Mensagem: String; Lista: MyArrayLista): Boolean;
begin
  Result := (Codigo < 0) or (Texto = EmptyStr);
  if Result = False then
    Geral.MB_Aviso('Informe o campo: "' + Mensagem + '"')
  else
  if UnNFe_PF.ListaContemplaCodigoSelecionado(Codigo, Lista) = False then
    Geral.MB_Aviso('Aten��o! O valor informado para o campo "' + Mensagem +
    ' n�o foi reconhecido pelo ERP como v�lido!');
end;

procedure TFmVSInnNFsCab.SBMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnNFsCab.SbNF_VPClick(Sender: TObject);
var
  Empresa, CI, Emitente, ide_mod, ide_Serie, ide_nNF: Integer;
  SQL_CNPJ_CPF_Emit, SQL_CNPJ_CPF_Dest: String;
  Continua: Boolean;
begin
  Continua := True;
  //
  if not DModG.ObtemEmpresaSelecionada(TdmkEditCB(EdEmpresa), Empresa) then Exit;
  Emitente := EdFornecedor.ValueVariant;
{
  CI := EdCI.ValueVariant;
  case TipoNFeEntrada of
    TTipoNFeEntrada.tneVP:
      if EMpresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser o mesmo que a empresa!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneCC:
      if Empresa = CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
    TTipoNFeEntrada.tneRP:
      if Empresa <> CI then
        Continua := Geral.MB_Pergunta(
        'Para esta pesquisa o cliente interno deve ser diferente da emprea!'
        + sLineBreak + 'Mas deseja pesquisar assim mesmo?') = ID_YES;
  end;
}
  //
  if Continua then
  begin
    case QrFornecedoresTipo.Value of
      0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
      1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
      else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
    end;
    //
    case QrCITipo.Value of
      0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(QrCICNPJ.Value) + '"';
      1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(QrCICPF.Value) + '"';
      else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
    end;
    //
    if MyObjects.FIC(Emitente = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
    //if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
    //
(*
    case TipoNFeEntrada of
      //tneND: //=0,
      tneVP:
      begin
        // NF de venda
        ide_mod   := EdmodNF.ValueVariant;
        ide_Serie := EdSerie.ValueVariant;
        ide_nNF   := EdNF.ValueVariant;
      end;
       //=1,
      tneRP: //=2,
      begin
        // NF de remessa produ��o
        ide_mod   := EdmodRP.ValueVariant;
        ide_Serie := EdSerRP.ValueVariant;
        ide_nNF   := EdNF_RP.ValueVariant;
      end;
      tneCC: //=3)
      begin
        // NF de remessa cobertura cliente
        ide_mod   := EdmodCC.ValueVariant;
        ide_Serie := EdSerCC.ValueVariant;
        ide_nNF   := EdNF_CC.ValueVariant;
      end;
    end;
    //
*)
    // NF de venda
    ide_mod   := EdCOD_MOD.ValueVariant;
    ide_Serie := EdSER.ValueVariant;
    ide_nNF   := EdNUM_DOC.ValueVariant;

    UnDmkDAC_PF.AbreMySQLQuery0(Qr00_NFeCabA, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, IDCtrl,',
    'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,',
    'ide_mod, ide_serie, ide_nNF, Id  ',
    'FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_CNPJ_CPF_Emit,
    SQL_CNPJ_CPF_Dest,
    'AND ide_mod IN (0,' + Geral.FF0(ide_mod) + ')',
    'AND ide_serie >= 0 ', // + Geral.FF0(ide_Serie),
    'AND ide_nNF=' + Geral.FF0(ide_nNF),
    'ORDER BY ide_mod DESC ',
    '']);
    if Qr00_NFeCabA.RecordCount > 0 then
    begin
      if not Qr00_NFeCabA.Locate('ide_mod;ide_serie', VarArrayOf([ide_mod, ide_serie]), []) then
      begin
        if not Qr00_NFeCabA.Locate('ide_serie', ide_serie, []) then
        begin
          if not Qr00_NFeCabA.Locate('ide_mod', ide_mod, []) then
          begin

          end;
        end;
      end;
      //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
      //
(*
      if Qr00_NFeCabA.RecordCount > 0 then
      begin
        if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        begin
          case TipoNFeEntrada of
            tneVP: EdSerie.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneRP: EdSerRP.ValueVariant := Qr00_NFeCabAide_serie.Value;
            tneCC: EdSerCC.ValueVariant := Qr00_NFeCabAide_serie.Value;
          end;
        end;
        //
        case TipoNFeEntrada of
          tneVP:
          begin
            EdrefNFe.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFVP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFVP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFVP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneRP:
          begin
            EdNFe_RP.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFRP_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFRP_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFRP_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
          tneCC:
          begin
            EdNFe_CC.ValueVariant       := Qr00_NFeCabAId.Value;
            EdNFCC_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
            EdNFCC_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
            EdNFCC_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
          end;
        end;
      end;
*)
      if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        EdSER.ValueVariant := Qr00_NFeCabAide_serie.Value;
      //
      EdCHV_NFE.ValueVariant := Qr00_NFeCabAId.Value;
      EdStaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
      EdFatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
      EdFatNum.ValueVariant   := Qr00_NFeCabAFatNum.Value;
    end;
  end;
end;

end.
