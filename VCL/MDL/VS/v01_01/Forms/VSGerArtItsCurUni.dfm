object FmVSGerArtItsCurUni: TFmVSGerArtItsCurUni
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-149 :: Item de Gera'#231#227'o de Artigo de Ribeira do Curtime' +
    'nto - IME-I - '#218'nico'
  ClientHeight = 701
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 562
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 137
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
    end
    object CkSemArea: TCheckBox
      Left = 168
      Top = 4
      Width = 249
      Height = 17
      Caption = 'Ainda n'#227'o sei a '#225'rea total do lote!'
      TabOrder = 1
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label14: TLabel
      Left = 216
      Top = 20
      Width = 51
      Height = 13
      Caption = 'ID Movim.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label1: TLabel
      Left = 284
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID Gera'#231#227'o:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label17: TLabel
      Left = 352
      Top = 20
      Width = 61
      Height = 13
      Caption = 'ID It.Gerado:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 148
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Red. It.Ger.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label13: TLabel
      Left = 788
      Top = 16
      Width = 62
      Height = 13
      Caption = 'ID Mov Twn:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdSrcNivel2: TdmkEdit
      Left = 352
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcNivel1: TdmkEdit
      Left = 284
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcMovID: TdmkEdit
      Left = 216
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcGGX: TdmkEdit
      Left = 148
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimTwn: TdmkEdit
      Left = 788
      Top = 32
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimTwn'
      UpdCampo = 'MovimTwn'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 815
        Height = 32
        Caption = 
          'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - IME-I - '#218'ni' +
          'co'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 815
        Height = 32
        Caption = 
          'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - IME-I - '#218'ni' +
          'co'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 815
        Height = 32
        Caption = 
          'Item de Gera'#231#227'o de Artigo de Ribeira do Curtimento - IME-I - '#218'ni' +
          'co'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 587
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 631
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 224
    Align = alClient
    Caption = ' Filtros: '
    TabOrder = 5
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaGraGruX: TLabel
        Left = 224
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
      end
      object LaTerceiro: TLabel
        Left = 516
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object LaFicha: TLabel
        Left = 152
        Top = 0
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
      end
      object Label11: TLabel
        Left = 8
        Top = 0
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Ficha RMP:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 224
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 280
        Top = 16
        Width = 233
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 4
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTerceiro: TdmkEditCB
        Left = 516
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 572
        Top = 16
        Width = 153
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 6
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFicha: TdmkEdit
        Left = 152
        Top = 16
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFichaChange
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 732
        Top = 2
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object EdSerieFch: TdmkEditCB
        Left = 8
        Top = 16
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSerieFchChange
        DBLookupComboBox = CBSerieFch
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSerieFch: TdmkDBLookupComboBox
        Left = 48
        Top = 16
        Width = 101
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSSerFch
        TabOrder = 1
        dmkEditCB = EdSerieFch
        QryCampo = 'SerieFch'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object DBGAptos: TDBGrid
      Left = 2
      Top = 57
      Width = 1004
      Height = 165
      Align = alClient
      DataSource = DsAptos
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGAptosDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie RMP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Title.Caption = 'Ficha RMP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 111
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Pele In Natura'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeca'
          Title.Caption = 'Sdo. Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoPeso'
          Title.Caption = 'Sdo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Codi. entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorMP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Visible = True
        end>
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 336
    Width = 1008
    Height = 226
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 226
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object GroupBox2: TGroupBox
        Left = 2
        Top = 15
        Width = 1004
        Height = 146
        Align = alTop
        Caption = ' Dados da baixa do couro In Natura: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 129
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object LaPecasBxa: TLabel
            Left = 96
            Top = 4
            Width = 72
            Height = 13
            Caption = 'Pe'#231'as: [F4][F5]'
          end
          object LaPesoKgBxa: TLabel
            Left = 172
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
            Enabled = False
          end
          object SbPesoKgBxa: TSpeedButton
            Left = 244
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbPesoKgBxaClick
          end
          object LaQtdGerArM2Bxa: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            Enabled = False
          end
          object LaQtdGerArP2Bxa: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
            Enabled = False
          end
          object Label9: TLabel
            Left = 12
            Top = 44
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object Label49: TLabel
            Left = 428
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object Label50: TLabel
            Left = 754
            Top = 4
            Width = 74
            Height = 13
            Caption = 'Req.Mov.Estq.:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label20: TLabel
            Left = 416
            Top = 44
            Width = 103
            Height = 13
            Caption = 'Mat'#233'ria-prima jumped:'
          end
          object SbStqCenLoc: TSpeedButton
            Left = 731
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbStqCenLocClick
          end
          object EdControleBxa: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPecasBxa: TdmkEdit
            Left = 96
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPecasBxaChange
            OnKeyDown = EdPecasBxaKeyDown
          end
          object EdPesoKgBxa: TdmkEdit
            Left = 172
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAreaM2Bxa: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdAreaP2Bxa
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdAreaP2Bxa: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdAreaM2Bxa
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservBxa: TdmkEdit
            Left = 12
            Top = 60
            Width = 401
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CGTpCalcAutoBxa: TdmkCheckGroup
            Left = 12
            Top = 84
            Width = 837
            Height = 41
            Caption = ' C'#225'lculos autom'#225'ticos: '
            Columns = 4
            Enabled = False
            Items.Strings = (
              'Pe'#231'as'
              'Peso kg'
              'Area m'#178
              'Area ft'#178)
            TabOrder = 9
            QryCampo = 'TpCalcAuto'
            UpdCampo = 'TpCalcAuto'
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 428
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'StqCenLoc'
            UpdCampo = 'StqCenLoc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 483
            Top = 20
            Width = 246
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_LOC_CEN'
            ListSource = DsStqCenLoc
            TabOrder = 6
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdReqMovEstq: TdmkEdit
            Left = 755
            Top = 20
            Width = 93
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdJmpGGX: TdmkEditCB
            Left = 416
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBJmpGGX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnFormActivate
          end
          object CBJmpGGX: TdmkDBLookupComboBox
            Left = 472
            Top = 60
            Width = 377
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGGXJmp
            TabOrder = 11
            dmkEditCB = EdJmpGGX
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 161
        Width = 1004
        Height = 63
        Align = alClient
        Caption = ' Dados do artigo gerado: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 46
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label4: TLabel
            Left = 96
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label7: TLabel
            Left = 172
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
            Enabled = False
          end
          object LaQtdGerArM2Src: TLabel
            Left = 268
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object LaQtdGerArP2Src: TLabel
            Left = 344
            Top = 4
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label12: TLabel
            Left = 428
            Top = 4
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object Label8: TLabel
            Left = 132
            Top = 4
            Width = 21
            Height = 13
            Caption = ' [F5]'
            Enabled = False
          end
          object EdControleSrc: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPecasSrc: TdmkEdit
            Left = 96
            Top = 20
            Width = 72
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdPecasSrcKeyDown
          end
          object EdPesoKgSrc: TdmkEdit
            Left = 172
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdQtdGerArM2Src: TdmkEditCalc
            Left = 268
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArP2Src
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdQtdGerArP2Src: TdmkEditCalc
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdQtdGerArM2Src
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdObservSrc: TdmkEdit
            Left = 428
            Top = 20
            Width = 421
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrAptos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vsmovits wmi '
      'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE wmi.MovimID=1'
      'AND SdoVrtPeca>0 '
      'AND Empresa=-11'
      'ORDER BY wmi.Controle ')
    Left = 36
    Top = 192
    object QrAptosNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrAptosNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrAptosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAptosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAptosMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrAptosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrAptosMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrAptosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAptosTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrAptosCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrAptosMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrAptosLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrAptosLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrAptosDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrAptosPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrAptosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrAptosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAptosPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrAptosAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrAptosAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrAptosSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrAptosSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrAptosSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrAptosObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrAptosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAptosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAptosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAptosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAptosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAptosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAptosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAptosFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrAptosMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrAptosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrAptosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrAptosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrAptosSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrAptosNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrAptosMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrAptosValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrAptosCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrAptosSdoPeca: TFloatField
      FieldName = 'SdoPeca'
    end
    object QrAptosSdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrAptosSdoArM2: TFloatField
      FieldName = 'SdoArM2'
    end
    object QrAptosSdoArP2: TFloatField
      FieldName = 'SdoArP2'
    end
    object QrAptosVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrAptosDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrAptosValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 36
    Top = 240
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 192
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 188
    Top = 240
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 260
    Top = 192
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 260
    Top = 240
  end
  object QrNiv1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 552
    Top = 232
    object QrNiv1FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrNiv1MediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrNiv1MediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 336
    Top = 196
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 336
    Top = 244
  end
  object QrGGXJmp: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGGXJmpAfterOpen
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 656
    Top = 216
    object QrGGXJmpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXJmpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXJmpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXJmpSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXJmpCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXJmpNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXJmp: TDataSource
    DataSet = QrGGXJmp
    Left = 656
    Top = 264
  end
end
