object FmVSCorreCustosIMECs: TFmVSCorreCustosIMECs
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-259 :: Corre'#231#227'o de custos em Sequ'#234'ncia de IME-C'
  ClientHeight = 573
  ClientWidth = 868
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 868
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 820
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 772
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 543
        Height = 32
        Caption = 'Corre'#231#227'o de custos em Sequ'#234'ncia de IME-C'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 543
        Height = 32
        Caption = 'Corre'#231#227'o de custos em Sequ'#234'ncia de IME-C'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 543
        Height = 32
        Caption = 'Corre'#231#227'o de custos em Sequ'#234'ncia de IME-C'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 438
    Width = 868
    Height = 65
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 864
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 487
        Height = 17
        Caption = 
          'Esta corre'#231#227'o se aplica ao grupo de artigos 3072 e somente  para' +
          ' '#225'rea > 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 487
        Height = 17
        Caption = 
          'Esta corre'#231#227'o se aplica ao grupo de artigos 3072 e somente  para' +
          ' '#225'rea > 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 864
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 503
    Width = 868
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 722
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 720
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Corrigir'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 868
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 137
      Width = 868
      Height = 253
      Align = alClient
      DataSource = DmModVS.DsCorreCustosIMECs
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DGDadosDblClick
    end
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 868
      Height = 137
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label2: TLabel
        Left = 8
        Top = 40
        Width = 30
        Height = 13
        Caption = 'Artigo:'
        Enabled = False
      end
      object Label8: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label1: TLabel
        Left = 8
        Top = 84
        Width = 78
        Height = 13
        Caption = 'Custo m m'#237'nimo:'
      end
      object Label3: TLabel
        Left = 108
        Top = 84
        Width = 61
        Height = 13
        Caption = 'IME-C inicial:'
      end
      object Label4: TLabel
        Left = 552
        Top = 0
        Width = 53
        Height = 13
        Caption = 'Movim IDs:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 56
        Width = 481
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 16
        Width = 477
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 0
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object BtTeste2: TBitBtn
        Tag = 22
        Left = 552
        Top = 40
        Width = 128
        Height = 45
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtTeste2Click
      end
      object CkTemIMEiMrt: TCheckBox
        Left = 196
        Top = 104
        Width = 197
        Height = 17
        Caption = 'Inclui o arquivo morto na pesquisa.'
        TabOrder = 5
      end
      object EdCustoMinM2: TdmkEdit
        Left = 8
        Top = 100
        Width = 97
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '13,340000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 13.340000000000000000
        ValWarn = False
      end
      object CkJaCorrigidos: TCheckBox
        Left = 404
        Top = 104
        Width = 153
        Height = 17
        Caption = 'Incluir itens j'#225' corrigidos.'
        Checked = True
        State = cbChecked
        TabOrder = 7
      end
      object EdIMECIni: TdmkEdit
        Left = 108
        Top = 100
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimIDs: TdmkEdit
        Left = 552
        Top = 16
        Width = 277
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65531
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, ggy.Nome NO_GraGruY, '
      'ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'unm.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 212
    Top = 60
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object QrGraGruXNO_GraGruY: TWideStringField
      FieldName = 'NO_GraGruY'
      Size = 255
    end
    object QrGraGruXGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 212
    Top = 112
  end
  object QrVmi2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, MovimCod, Controle, '
      'Pecas, AreaM2, ValorT, '
      'SrcMovID, SrcNivel1,'
      'SrcNivel2, SrcGGX'
      'FROM vsmovits'
      'WHERE MovimCod=606'
      'AND MovimNiv=11')
    Left = 116
    Top = 200
    object QrVmi2DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVmi2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVmi2MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVmi2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVmi2Pecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVmi2AreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVmi2ValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVmi2SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVmi2SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVmi2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVmi2SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 12
    Top = 200
  end
  object QrVmi1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, MovimCod, Controle, '
      'Pecas, AreaM2, ValorT, '
      'SrcMovID, SrcNivel1,'
      'SrcNivel2, SrcGGX'
      'FROM vsmovits'
      'WHERE MovimCod=606'
      'AND MovimNiv=11')
    Left = 116
    Top = 148
    object QrVmi1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVmi1MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVmi1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVmi1Pecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVmi1AreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVmi1ValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVmi1SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVmi1SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVmi1SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVmi1SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVmi1DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
  end
  object QrVmi3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, MovimCod, Controle, '
      'Pecas, AreaM2, ValorT, '
      'SrcMovID, SrcNivel1,'
      'SrcNivel2, SrcGGX'
      'FROM vsmovits'
      'WHERE MovimCod=606'
      'AND MovimNiv=11')
    Left = 116
    Top = 256
    object QrVmi3DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVmi3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVmi3MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVmi3Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVmi3Pecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVmi3AreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVmi3ValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVmi3SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVmi3SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVmi3SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVmi3SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
  end
  object QrVmi4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, MovimCod, Controle, '
      'Pecas, AreaM2, ValorT, '
      'SrcMovID, SrcNivel1,'
      'SrcNivel2, SrcGGX'
      'FROM vsmovits'
      'WHERE MovimCod=606'
      'AND MovimNiv=11')
    Left = 116
    Top = 312
    object QrVmi4DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVmi4Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVmi4MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVmi4Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVmi4Pecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVmi4AreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVmi4ValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVmi4SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVmi4SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVmi4SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVmi4SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
  end
  object Qr0615: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, MovimTwn, MovCodPai, JmpMovID '
      'FROM vsmovits'
      'WHERE MovimID=6'
      'AND MovimNiv=15'
      'AND MovimCod=17'
      'ORDER BY Controle')
    Left = 336
    Top = 268
    object Qr0615Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object Qr0615MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object Qr0615MovCodPai: TIntegerField
      FieldName = 'MovCodPai'
      Required = True
    end
    object Qr0615JmpMovID: TIntegerField
      FieldName = 'JmpMovID'
      Required = True
    end
    object Qr0615SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr0615Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr0615PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr0615ValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object Qr0615ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = DModG.RV_CEP_DB
    Left = 328
    Top = 356
  end
end
