unit VSPedIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditCalc, dmkRadioGroup,
  dmkEditDateTimePicker, dmkMemo, UnDmkListas, dmkCheckBox, TypInfo;

type
  TFmVSPedIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrReceiRecu: TmySQLQuery;
    QrReceiRecuNumero: TIntegerField;
    QrReceiRecuNome: TWideStringField;
    DsReceiRecu: TDataSource;
    QrReceirefu: TmySQLQuery;
    QrReceirefuNumero: TIntegerField;
    QrReceirefuNome: TWideStringField;
    DsReceiRefu: TDataSource;
    QrFluxos: TmySQLQuery;
    QrFluxosCodigo: TIntegerField;
    QrFluxosNome: TWideStringField;
    DsFluxos: TDataSource;
    QrReceiAcab: TmySQLQuery;
    QrReceiAcabNumero: TIntegerField;
    QrReceiAcabNome: TWideStringField;
    DsReceiAcab: TDataSource;
    PainelIts: TPanel;
    GroupBox3: TGroupBox;
    BtConfirma2: TBitBtn;
    Panel3: TPanel;
    BtDesiste2: TBitBtn;
    GBItem: TGroupBox;
    PnItem_: TPanel;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    Label32: TLabel;
    LaPrecoVal: TLabel;
    TPEntrega: TdmkEditDateTimePicker;
    EdPrecoVal: TdmkEdit;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    MeObserv: TdmkMemo;
    RGTipoProd: TRadioGroup;
    GroupBox6: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdReceiRecu: TdmkEditCB;
    CBReceiRecu: TdmkDBLookupComboBox;
    EdReceiAcab: TdmkEditCB;
    CBReceiAcab: TdmkDBLookupComboBox;
    EdFluxo: TdmkEditCB;
    CBFluxo: TdmkDBLookupComboBox;
    EdReceiRefu: TdmkEditCB;
    CBReceiRefu: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    Label13: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    DsGraGruX: TDataSource;
    EdControle: TdmkEdit;
    Label2: TLabel;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    RGPrecoTipo: TdmkRadioGroup;
    Label22: TLabel;
    EdPrecoMoeda: TdmkEditCB;
    CBPrecoMoeda: TdmkDBLookupComboBox;
    SpeedButton9: TSpeedButton;
    VuCambioMda: TdmkValUsu;
    EdPercDesco: TdmkEdit;
    Label1: TLabel;
    QrCambioMda: TmySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    EdStatus: TdmkEdit;
    Label6: TLabel;
    EdStatus_TXT: TdmkEdit;
    Label7: TLabel;
    EdPercTrib: TdmkEdit;
    CkNaoFinaliza: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure RGPrecoTipoClick(Sender: TObject);
    procedure EdStatusChange(Sender: TObject);
    procedure EdStatusKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenVSPedIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSPedIts: TFmVSPedIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
{$IfNDef SemCotacoes}
  CambioMda,
{$EndIf}
UnVS_PF, ModuleGeral, AppListas;

{$R *.DFM}

procedure TFmVSPedIts.BtOKClick(Sender: TObject);
const
  ValTot = 0;
var
  Texto, Entrega, Observ: String;
  Codigo, Controle, PrecoTipo, PrecoMoeda, Fluxo,
  TipoProd, GraGruX, ReceiRecu, ReceiRefu, ReceiAcab, Status, NaoFinaliza: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, PrecoVal,
  PercDesco, Perctrib, Valor, Descn: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  PrecoTipo      := RGPrecoTipo.ItemIndex;
  PrecoVal       := EdPrecoVal.ValueVariant;
  PercDesco      := EdPercDesco.ValueVariant;
  Perctrib       := EdPerctrib.ValueVariant;
  Valor          := VS_PF.CalculaValorCouros(TTipoCalcCouro(PrecoTipo),
                    PrecoVal, Pecas, PesoKg, AreaM2, AreaP2, ValTot);
  Descn          := Geral.RoundC(Valor * (1-(PercDesco/100)), 2);
  Texto          := EdTexto.Text;
  Entrega        := Geral.FDT(TPEntrega.Date, 1);
  Fluxo          := EdFluxo.ValueVariant;
  Observ         := MeObserv.Text;
  TipoProd       := RGTipoProd.ItemIndex;
  GraGruX        := EdGraGruX.ValueVariant;
  ReceiRecu      := EdReceiRecu.ValueVariant;
  ReceiRefu      := EdReceiRefu.ValueVariant;
  ReceiAcab      := EdReceiAcab.ValueVariant;
  Status         := EdStatus.ValueVariant;
  NaoFinaliza    := Geral.BoolToInt(CkNaoFinaliza.Checked);
  //
  if MyObjects.FIC(PrecoTipo < 1, RGPrecoTipo, 'Informe a " Aplica��o do pre�o"') then
    Exit;
  if not UMyMod.ObtemCodigoDeCodUsu(EdPrecoMoeda, PrecoMoeda,
    'Informe a moeda!', 'Codigo', 'CodUsu') then Exit;
  if MyObjects.FIC(Status < 1, EdStatus, 'Informe o Status! (F4)') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o Artigo') then
    Exit;
  if MyObjects.FIC((NaoFinaliza = 1)
  and (Status <> Integer(TStausPedVda.spvLiberado)), EdStatus,
  'Para manter o pedido sempre aberto (nunca finalizar) � necess�rio definir o Status como Liberado!') then
      Exit;
  //
  Controle := UMyMod.BPGS1I32('vspedits', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vspedits', False, [
  'Codigo', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'PrecoTipo', 'PrecoMoeda', 'PrecoVal',
  'PercDesco', 'Valor', 'Descn',
  'Texto', 'Entrega',
  'Fluxo', 'Observ', 'TipoProd',
  'GraGruX', 'ReceiRecu', 'ReceiRefu',
  'ReceiAcab', 'Status', 'Perctrib',
  'NaoFinaliza'], [
  'Controle'], [
  Codigo, Pecas,
  PesoKg, AreaM2, AreaP2,
  PrecoTipo, PrecoMoeda, PrecoVal,
  PercDesco, Valor, Descn,
  Texto, Entrega,
  Fluxo, Observ, TipoProd,
  GraGruX, ReceiRecu, ReceiRefu,
  ReceiAcab, Status, Perctrib,
  NaoFinaliza], [
  Controle], True) then
  begin
    VS_PF.AtualizaTotaisVSPedCab(Codigo);
    ReopenVSPedIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //
      EdPecas.ValueVariant     := 0;
      EdAreaM2.ValueVariant    := 0;
      EdAreaP2.ValueVariant    := 0;
      EdPesoKg.ValueVariant    := 0;
(*
      RGPrecoTipo.ItemIndex    := 0;
      EdPrecoMoeda.ValueVariant     := 0;
      CBPrecoMoeda.KeyValue         := Null;
      TPEntrega.Date           := ;
*)
      EdPrecoVal.ValueVariant  := 0;
      EdPercDesco.ValueVariant := 0;

      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := Null;

      EdFluxo.ValueVariant     := 0;
      CBFluxo.KeyValue         := Null;

      EdReceiRecu.ValueVariant := 0;
      CBReceiRecu.KeyValue     := Null;

      EdReceiRefu.ValueVariant := 0;
      CBReceiRefu.KeyValue     := Null;

      EdReceiAcab.ValueVariant := 0;
      CBReceiAcab.KeyValue     := Null;

      EdTexto.ValueVariant     := '';
      MeObserv.Text            := '';
      //
      EdPecas.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSPedIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPedIts.EdStatusChange(Sender: TObject);
(*var
  s: String;
  Status: Integer;
begin
  Status := EdStatus.ValueVariant;
  case Status of
    0: s := 'N�O DEFINIDO';
    1: s := 'Cancelado';
    2: s := 'Suspenso';
    3: s := 'Liberado';
    9: s := 'Finalizado';
    else s := 'N�O IMPLEMENTADO';
  end;
  EdStatus_TXT.ValueVariant := s;
*)
var
  Status: Integer;
begin
  Status := EdStatus.ValueVariant;
  EdStatus_TXT.ValueVariant := sStausPedVda[Status];
end;

procedure TFmVSPedIts.EdStatusKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    MyObjects.CriaSelListaArray(sStausPedVda,
    'SEL-LISTA-000 :: Status do Pedido', TitCols, Screen.Width, EdStatus);
    //EdStatus_TXT.Text := VAR_TEXTO_SEL;
  end;
end;

procedure TFmVSPedIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
  EdStatusChange(Self);
end;

procedure TFmVSPedIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_Ped_VS + ') ');
  //
  CBGraGruX.LocF7SQLMasc := '';
  CBGraGruX.LocF7SQLText.Text := '';
(*  CBGraGruX.LocF7SQLMasc := CO_JOKE_SQL;
  CBGraGruX.LocF7SQLText.Text := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM wbartcab wac ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=wac.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE "%' + CO_JOKE_SQL + '%" ',
  'ORDER BY _Nome ',
  '']);
*)
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRecu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRefu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiAcab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFluxos, Dmod.MyDB);
  //
  TPEntrega.Date := DModG.ObtemAgora();
end;

procedure TFmVSPedIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPedIts.ReopenVSPedIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSPedIts.RGPrecoTipoClick(Sender: TObject);
begin
  LaPrecoVal.Caption := 'Pre�o ' + RGPrecoTipo.Items[RGPrecoTipo.ItemIndex];
end;

procedure TFmVSPedIts.SpeedButton9Click(Sender: TObject);
{$IfNDef SemCotacoes}
var
  Moeda: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdPrecoMoeda, Moeda,
    'Informe a moeda!', 'Codigo', 'CodUsu') then Exit;
  //
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    if Moeda <> 0 then
      FmCambioMda.LocCod(Moeda, Moeda);
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrCambioMda, Dmod.MyDB);
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdPrecoMoeda.ValueVariant := DModG.QrCambioMdaCodUsu.Value;
        CBPrecoMoeda.KeyValue     := DModG.QrCambioMdaCodUsu.Value;
        EdPrecoMoeda.SetFocus;
      end;
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModuloTDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

end.
