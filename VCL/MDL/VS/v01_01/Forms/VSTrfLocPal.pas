unit VSTrfLocPal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Mask, dmkDBEdit, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSTrfLocPal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBG04Estq: TdmkDBGridZTO;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    DsEstqR4: TDataSource;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrEstqR4MediaM2: TFloatField;
    QrEstqR4DataHora: TDateTimeField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    LaFicha: TLabel;
    Label11: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel10: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    Label16: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    EdObserv: TdmkEdit;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    EdItemNFe: TdmkEdit;
    LaItemNFe: TLabel;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdStqCenLocSrc: TdmkEditCB;
    CBStqCenLocSrc: TdmkDBLookupComboBox;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrEstqR4NFeSer: TSmallintField;
    QrEstqR4NFeNum: TIntegerField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    Label38: TLabel;
    CBCouNiv2: TdmkDBLookupComboBox;
    Label39: TLabel;
    CBCouNiv1: TdmkDBLookupComboBox;
    EdCouNiv2: TdmkEditCB;
    EdCouNiv1: TdmkEditCB;
    EdTerceiro: TdmkEditCB;
    LaTerceiro: TLabel;
    CBTerceiro: TdmkDBLookupComboBox;
    QrEstqR4NO_LOC_CEN: TWideStringField;
    QrStqCenLocEntiSitio: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    procedure AdicionaPallet();
    procedure InsereIMEI_Atual();
    procedure ReopenVSMovIts();

  public
    { Public declarations }
    FEmpresa, FClientMO: Integer;
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure PesquisaPallets();
  end;

  var
  FmVSTrfLocPal: TFmVSTrfLocPal;

implementation

uses UnMyObjects, Module, ModuleGeral, VSTrfLocCab, UMySQLModule,
  DmkDAC_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSTrfLocPal.AdicionaPallet();
begin
  ReopenVSMovIts();
  QrVSMovIts.First;
  while not QrVSMovIts.Eof do
  begin
    InsereIMEI_Atual();
    QrVSMovIts.Next;
  end;
end;

procedure TFmVSTrfLocPal.BtOKClick(Sender: TObject);
var
  N, I: Integer;
begin
  if MyObjects.FIC(EdStqCenLoc.ValueVariant = 0, EdStqCenLoc,
  'Informe o local/centro de estoque') then
    Exit;
  //
  if MyObjects.FIC(EdItemNFe.ValueVariant = 0, EdItemNFe,
  'Informe o item da NFe') then
    Exit;
  //
  QrEstqR4.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      AdicionaPallet();
    end;
    if N > 0 then
      Close;
  finally
    QrEstqR4.EnableControls;
  end;
  FmVSTrfLocCab.AtualizaNFeItens();
end;

procedure TFmVSTrfLocPal.BtReabreClick(Sender: TObject);
begin
  PesquisaPallets();
end;

procedure TFmVSTrfLocPal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSTrfLocPal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
end;

procedure TFmVSTrfLocPal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GraGruY <> 0');
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, ''); //'AND ggx.GraGruY <> 0');
  //
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
end;

procedure TFmVSTrfLocPal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSTrfLocPal.InsereIMEI_Atual();
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2, Pallet, Codigo, CtrlSorc, CtrlDest, MovimCod,
  Empresa, GraGruX, Terceiro, CliVenda, SerieFch, Ficha, (*Misturou,*)
  DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX, ItemNFe, VSMulFrnCab,
  ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
  //
  NotaMPAG: Double;
  FornecMO, ReqMovEstq, StqCenLoc, MovimTwn, GGXRcl: Integer;
  Observ: String;
begin
  //FornecMO       := QrVSMovItsFornecMO.Value;
  FornecMO       := QrStqCenLocEntiSitio.Value;
  NotaMPAG       := QrVSMovItsNotaMPAG.Value;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  Observ         := EdObserv.Text;
  //
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SrcGGX         := 0;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  CtrlSorc       := 0; //EdControle.ValueVariant;
  CtrlDest       := 0; //EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVSMovItsClientMO.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidTransfLoc;
  MovimNiv       := eminDestLocal;
  Pallet         := QrVSMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Pecas          := QrVSMovItsSdoVrtPeca.Value;
  AreaM2         := QrVSMovItsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  PesoKg         := 0;
  if (AreaM2 = 0) and (QrVSMovItsSdoVrtPeso.Value > 0) then
      PesoKg     := QrVSMovItsSdoVrtPeso.Value;
  if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
    Valor := QrVSMovItsSdoVrtArM2.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
  else
  if QrVSMovItsPesoKg.Value > 0 then
    Valor := QrVSMovItsSdoVrtPeso.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value)
  else
  if QrVSMovItsPecas.Value > 0 then
    Valor := QrVSMovItsSdoVrtPeca.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  ValorMP        := Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := 0; //QrVSMovItsSerieFch.Value;
  Ficha          := 0;  //QrVSMovItsFicha.Value;
  Marca          := ''; //QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
  ItemNFe        := EdItemNFe.ValueVariant;
  //
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  CtrlSorc := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlSorc);
  CtrlDest := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlDest);
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  //
  // Geração!!!
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, (*Controle*) CtrlDest, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss,
  CO_0_CusKgComiss,
  CO_0_CustoComiss,
  CO_0_CredPereImposto,
  CO_0_CredValrImposto,
  CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei079(*Geração de couro no destino em transferência de estoque por pallet*)) then
  begin
    // Baixa!!!
    //FornecMO       := QrVSMovItsFornecMO.Value;
    NotaMPAG       := 0;
    ReqMovEstq     := 0;
    StqCenLoc      := QrVSMovItsStqCenLoc.Value;
    Observ         := '';
    SrcMovID       := QrVSMovItsMovimID.Value;
    SrcNivel1      := QrVSMovItsCodigo.Value;
    SrcNivel2      := QrVSMovItsControle.Value;
    SrcGGX         := QrVSMovItsGraGruX.Value;
    //
    MovimNiv       := eminSorcLocal;
    Pecas          := -Pecas;
    PesoKg         := -PesoKg;
    AreaM2         := -AreaM2;
    AreaP2         := -AreaP2;
    ValorT         := -ValorT;
    //
    DstMovID       := TEstqMovimID(0);
    DstNivel1      := 0;
    DstNivel2      := 0;
    DstGGX         := 0;
    //
    if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, (*Controle*) CtrlSorc, Ficha, (*Misturou,*)
    CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
    QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
    GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss,
  CO_0_CusKgComiss,
  CO_0_CustoComiss,
  CO_0_CredPereImposto,
  CO_0_CredValrImposto,
  CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei080(*Baixa de couro da origem em transferência de estoque por pallet*)) then
    begin
      VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
      VS_CRC_PF.AtualizaSaldoIMEI(CtrlDest, False);
      VS_CRC_PF.AtualizaTotaisVSXxxCab('vstrfloccab', MovimCod);
      FmVSTrfLocCab.LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmVSTrfLocPal.PesquisaPallets();
const
  SQL_Especificos = '';
  Pallet    = 0;
  Terceiro  = 0;
  StqCenLoc = 0;
  GraGruYs  = '';
  MovimID   = 0;
  MovimCod  = 0;
var
  GraGruX: TPallArr;
  StqCenCad, CouNiv1, CouNiv2: Integer;
begin
  StqCenCad := EdStqCenLocSrc.ValueVariant;
  VS_CRC_PF.SetaGGXUnicoEmLista(EdGraGruX.ValueVariant, GraGruX);
  CouNiv2 := EdCouNiv2.ValueVariant;
  CouNiv1 := EdCouNiv1.ValueVariant;
  //
  if VS_CRC_PF.PesquisaPallets(FEmpresa, FClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqCenLoc, Self.Name, GraGruYs, GraGruX, [],   SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FVSMovImp4)
  then
    VS_CRC_PF.ReopenVSListaPallets(QrEstqR4, FVSMovImp4,
    FEmpresa, EdGraGruX.ValueVariant, Pallet, Terceiro, '');
end;

procedure TFmVSTrfLocPal.ReopenVSMovIts();
begin
  VS_CRC_PF.ReopenVSMovIts_Pallet2(QrVSMovIts, QrEstqR4);
end;

end.
