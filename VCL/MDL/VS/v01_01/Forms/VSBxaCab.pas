unit VSBxaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker, AppListas,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnProjGroup_Consts, frxClass,
  frxDBSet, UnGrl_Consts, UnAppEnums;

type
  TFmVSBxaCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSBxaCab: TmySQLQuery;
    DsVSBxaCab: TDataSource;
    QrVSBxaIts: TmySQLQuery;
    DsVSBxaIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtBaixa: TdmkEditDateTimePicker;
    EdDtBaixa: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSBxaCabCodigo: TIntegerField;
    QrVSBxaCabMovimCod: TIntegerField;
    QrVSBxaCabEmpresa: TIntegerField;
    QrVSBxaCabDtBaixa: TDateTimeField;
    QrVSBxaCabPecas: TFloatField;
    QrVSBxaCabPesoKg: TFloatField;
    QrVSBxaCabAreaM2: TFloatField;
    QrVSBxaCabAreaP2: TFloatField;
    QrVSBxaCabLk: TIntegerField;
    QrVSBxaCabDataCad: TDateField;
    QrVSBxaCabDataAlt: TDateField;
    QrVSBxaCabUserCad: TIntegerField;
    QrVSBxaCabUserAlt: TIntegerField;
    QrVSBxaCabAlterWeb: TSmallintField;
    QrVSBxaCabAtivo: TSmallintField;
    QrVSBxaCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    EdNome: TdmkEdit;
    Label3: TLabel;
    QrVSBxaCabNome: TWideStringField;
    DBEdit4: TDBEdit;
    N1: TMenuItem;
    AdicionaIMEIscompletos1: TMenuItem;
    QrVSBxaCabTemIMEIMrt: TIntegerField;
    QrVSBxaItsCodigo: TLargeintField;
    QrVSBxaItsControle: TLargeintField;
    QrVSBxaItsMovimCod: TLargeintField;
    QrVSBxaItsMovimNiv: TLargeintField;
    QrVSBxaItsMovimTwn: TLargeintField;
    QrVSBxaItsEmpresa: TLargeintField;
    QrVSBxaItsTerceiro: TLargeintField;
    QrVSBxaItsCliVenda: TLargeintField;
    QrVSBxaItsMovimID: TLargeintField;
    QrVSBxaItsDataHora: TDateTimeField;
    QrVSBxaItsPallet: TLargeintField;
    QrVSBxaItsGraGruX: TLargeintField;
    QrVSBxaItsPecas: TFloatField;
    QrVSBxaItsPesoKg: TFloatField;
    QrVSBxaItsAreaM2: TFloatField;
    QrVSBxaItsAreaP2: TFloatField;
    QrVSBxaItsValorT: TFloatField;
    QrVSBxaItsSrcMovID: TLargeintField;
    QrVSBxaItsSrcNivel1: TLargeintField;
    QrVSBxaItsSrcNivel2: TLargeintField;
    QrVSBxaItsSrcGGX: TLargeintField;
    QrVSBxaItsSdoVrtPeca: TFloatField;
    QrVSBxaItsSdoVrtPeso: TFloatField;
    QrVSBxaItsSdoVrtArM2: TFloatField;
    QrVSBxaItsObserv: TWideStringField;
    QrVSBxaItsSerieFch: TLargeintField;
    QrVSBxaItsFicha: TLargeintField;
    QrVSBxaItsMisturou: TLargeintField;
    QrVSBxaItsFornecMO: TLargeintField;
    QrVSBxaItsCustoMOKg: TFloatField;
    QrVSBxaItsCustoMOM2: TFloatField;
    QrVSBxaItsCustoMOTot: TFloatField;
    QrVSBxaItsValorMP: TFloatField;
    QrVSBxaItsDstMovID: TLargeintField;
    QrVSBxaItsDstNivel1: TLargeintField;
    QrVSBxaItsDstNivel2: TLargeintField;
    QrVSBxaItsDstGGX: TLargeintField;
    QrVSBxaItsQtdGerPeca: TFloatField;
    QrVSBxaItsQtdGerPeso: TFloatField;
    QrVSBxaItsQtdGerArM2: TFloatField;
    QrVSBxaItsQtdGerArP2: TFloatField;
    QrVSBxaItsQtdAntPeca: TFloatField;
    QrVSBxaItsQtdAntPeso: TFloatField;
    QrVSBxaItsQtdAntArM2: TFloatField;
    QrVSBxaItsQtdAntArP2: TFloatField;
    QrVSBxaItsNotaMPAG: TFloatField;
    QrVSBxaItsNO_PALLET: TWideStringField;
    QrVSBxaItsNO_PRD_TAM_COR: TWideStringField;
    QrVSBxaItsNO_TTW: TWideStringField;
    QrVSBxaItsID_TTW: TLargeintField;
    QrOrigem: TmySQLQuery;
    QrOrigemNO_MovimID: TWideStringField;
    QrOrigemNO_CEN_LOC: TWideStringField;
    QrOrigemNO_FMO: TWideStringField;
    QrOrigemNO_CLI: TWideStringField;
    QrOrigemNO_PRD_TAM_COR: TWideStringField;
    QrOrigemGraGruX: TIntegerField;
    QrOrigemPecas: TFloatField;
    QrOrigemPesoKg: TFloatField;
    QrOrigemAreaM2: TFloatField;
    QrOrigemAreaP2: TFloatField;
    QrOrigemValorT: TFloatField;
    frxDsOrigem: TfrxDBDataset;
    frxWET_CURTI_027_1: TfrxReport;
    frxDsVSBxaCab: TfrxDBDataset;
    QrOrigemStqCenLoc: TIntegerField;
    QrOrigemDataHora: TDateTimeField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSBxaCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSBxaCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSBxaCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSBxaCabBeforeClose(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AdicionaIMEIscompletos1Click(Sender: TObject);
    procedure frxWET_CURTI_027_1GetValue(const VarName: string;
      var Value: Variant);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSBxaIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSBxaIts(Controle: Integer);

  end;

var
  FmVSBxaCab: TFmVSBxaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
VSBxaIts, UnVS_PF, VSBxaItsIMEIs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSBxaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSBxaCab.MostraFormVSBxaIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSBxaIts, FmVSBxaIts, afmoNegarComAviso) then
  begin
    FmVSBxaIts.ImgTipo.SQLType := SQLType;
    FmVSBxaIts.FQrCab    := QrVSBxaCab;
    FmVSBxaIts.FDsCab    := DsVSBxaCab;
    FmVSBxaIts.FQrIts    := QrVSBxaIts;
    FmVSBxaIts.FDataHora := QrVSBxaCabDtBaixa.Value;
    FmVSBxaIts.FEmpresa  := QrVSBxaCabEmpresa.Value;
    if SQLType = stIns then
    begin
      //FmVSBxaIts.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmVSBxaIts.FDataHora := QrVSBxaCabDtVenda.Value;
      FmVSBxaIts.EdControle.ValueVariant := QrVSBxaItsControle.Value;
      //
      FmVSBxaIts.EdGragruX.ValueVariant  := QrVSBxaItsGraGruX.Value;
      FmVSBxaIts.CBGragruX.KeyValue      := QrVSBxaItsGraGruX.Value;
      FmVSBxaIts.EdPecas.ValueVariant    := -QrVSBxaItsPecas.Value;
      FmVSBxaIts.EdPesoKg.ValueVariant   := -QrVSBxaItsPesoKg.Value;
      FmVSBxaIts.EdAreaM2.ValueVariant   := -QrVSBxaItsAreaM2.Value;
      FmVSBxaIts.EdAreaP2.ValueVariant   := -QrVSBxaItsAreaP2.Value;
      //FmVSBxaIts.EdValorT.ValueVariant   := -QrVSBxaItsValorT.Value;
      FmVSBxaIts.EdPallet.ValueVariant   := QrVSBxaItsPallet.Value;
      //
    end;
}
    FmVSBxaIts.ShowModal;
    FmVSBxaIts.Destroy;
    end;
  end;
end;

procedure TFmVSBxaCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSBxaCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSBxaCab, QrVSBxaIts);
end;

procedure TFmVSBxaCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSBxaCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSBxaIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSBxaIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSBxaItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSBxaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSBxaCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSBxaCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsbxacab';
  VAR_GOTOMYSQLTABLE := QrVSBxaCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM vsbxacab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSBxaCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSBxaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSBxaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSBxaCab.ItsAltera1Click(Sender: TObject);
begin
// Nao fazer!
end;

procedure TFmVSBxaCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSBxaItsCodigo.Value;
  MovimCod := QrVSBxaItsMovimCod.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSBxaIts, TIntegerField(QrVSBxaItsControle),
  QrVSBxaItsControle.Value, CtrlBaix, QrVSBxaItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti027)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsbxacab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSBxaCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSBxaIts(stIns);
end;

procedure TFmVSBxaCab.ReopenVSBxaIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSBxaIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSBxaCabMovimCod.Value),
  'ORDER BY vmi.Controle ',
  '']);
  //
  QrVSBxaIts.Locate('Controle', Controle, []);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSBxaCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSBxaCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSBxaIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrVSBxaIts.Locate('Controle', Controle, []);
end;


procedure TFmVSBxaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSBxaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSBxaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSBxaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSBxaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSBxaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSBxaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSBxaCabCodigo.Value;
  Close;
end;

procedure TFmVSBxaCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSBxaCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsbxacab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSBxaCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSBxaCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtBaixa, DataHora: String;
  Codigo, MovimCod, Empresa: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtBaixa        := Geral.FDT_TP_Ed(TPDtBaixa.Date, EdDtBaixa.Text);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Nome = '', EdNome, 'Defina o motivo da baixa!') then Exit;
  if MyObjects.FIC(TPDtBaixa.DateTime < 2, TPDtBaixa,
    'Defina uma data de baixa!') then Exit;

  Codigo := UMyMod.BPGS1I32('vsbxacab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsbxacab', False, [
  'Nome', 'MovimCod', 'Empresa',
  'DtBaixa', 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'], [
  'Codigo'], [
  Nome, MovimCod, Empresa,
  DtBaixa, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidForcado, Codigo)
    else
    begin
      DataHora := DtBaixa;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSBxaCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsbxacab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsbxacab', 'Codigo');
end;

procedure TFmVSBxaCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSBxaCab.AdicionaIMEIscompletos1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmVSBxaItsIMEIs, FmVSBxaItsIMEIs, afmoNegarComAviso) then
  begin
    FmVSBxaItsIMEIs.ImgTipo.SQLType := stIns;
    //
    FmVSBxaItsIMEIs.FCodigo   := QrVSBxaCabCodigo.Value;
    FmVSBxaItsIMEIs.FMovimCod := QrVSBxaCabMovimCod.Value;
    FmVSBxaItsIMEIs.FEmpresa  := QrVSBxaCabEmpresa.Value;
    //FmVSBxaItsIMEIs.FClientMO := QrVSBxaCabClientMO.Value;
    //
    FmVSBxaItsIMEIs.ShowModal;
    Controle := FmVSBxaItsIMEIs.FControle;
    FmVSBxaItsIMEIs.Destroy;
    ReopenVSBxaIts(Controle);
  end;
end;

procedure TFmVSBxaCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSBxaCab, QrVSBxaCabDtBaixa.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSBxaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSBxaCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSBxaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSBxaCab.SbImprimeClick(Sender: TObject);
var
  SQL_Periodo, ATT_MovimID, ATT_MovimNiv: String;
begin
  //
  ATT_MovimID := dmkPF.ArrayToTexto('vmd.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmd.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  UnDMkDAC_PF.AbreMySQLQuery0(QrOrigem, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_O_;',
  'CREATE TABLE _BXA_ITENS_ORIG_O_',
  'SELECT * FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
  'WHERE MovimCod=' + Geral.FF0(QrVSBxaCabMovimCod.Value),
  ';',
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_X_;',
  'CREATE TABLE _BXA_ITENS_ORIG_X_',
  'SELECT SrcNivel2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
  'WHERE MovimCod=' + Geral.FF0(QrVSBxaCabMovimCod.Value),
  ';',
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_D_;',
  'CREATE TABLE _BXA_ITENS_ORIG_D_',
  'SELECT vmi.* ',
  'FROM _BXA_ITENS_ORIG_X_  vmx ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=vmx.SrcNivel2',
  ';',
  'SELECT ' + ATT_MovimID,
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_CEN_LOC, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FMO, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
  'CONCAT(gg1.Nome,   ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  '  NO_PRD_TAM_COR, vmd.GraGruX, vmd.StqCenLoc, vmd.DataHora, ',
  '-vmo.Pecas Pecas, -vmo.PesoKg PesoKg, -vmo.AreaM2 AreaM2, ',
  '-vmo.AreaP2 AreaP2, -vmo.ValorT ValorT',
  '',
  'FROM _BXA_ITENS_ORIG_D_ vmd',
  'LEFT JOIN _BXA_ITENS_ORIG_O_ vmo ON vmo.SrcNivel2=vmd.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmd.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.entidades  cli ON cli.Codigo=vmd.ClientMO ',
  'LEFT JOIN ' + TMeuDB + '.entidades  fmo ON fmo.Codigo=vmd.FornecMO ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmd.StqCenLoc  ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo  ',
  ' ',
  'ORDER BY vmd.StqcenLoc, vmd.DataHora',
  '']);
  //Geral.MB_SQL(Self, QrOrigem);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_027_1, [
  DModG.frxDsDono,
  frxDsOrigem,
  frxDsVSBxaCab
  ]);
  MyObjects.frxMostra(frxWET_CURTI_027_1, 'Itens de Baixa for�ada');
end;

procedure TFmVSBxaCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSBxaCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSBxaCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSBxaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSBxaCab.QrVSBxaCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
end;

procedure TFmVSBxaCab.QrVSBxaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSBxaIts(0);
end;

procedure TFmVSBxaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSBxaCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSBxaCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSBxaCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsbxacab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSBxaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSBxaCab.frxWET_CURTI_027_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
end;

procedure TFmVSBxaCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSBxaCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsbxacab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDtBaixa.Date := DModG.ObtemAgora();
  EdDtBaixa.ValueVariant := DModG.ObtemAgora();
  if EdEmpresa.ValueVariant > 0 then
    TPDtBaixa.SetFocus;
end;

procedure TFmVSBxaCab.QrVSBxaCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSBxaIts.Close;
end;

procedure TFmVSBxaCab.QrVSBxaCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSBxaCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

