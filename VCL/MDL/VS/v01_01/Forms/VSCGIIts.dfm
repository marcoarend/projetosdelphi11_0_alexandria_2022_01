object FmVSCGIIts: TFmVSCGIIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-088 :: Sele'#231#227'o de IME-Is VS'
  ClientHeight = 691
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 554
    Width = 757
    Height = 23
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 220
    ExplicitWidth = 617
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 757
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 617
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 673
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 757
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 709
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 661
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 267
        Height = 32
        Caption = 'Sele'#231#227'o de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 267
        Height = 32
        Caption = 'Sele'#231#227'o de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 267
        Height = 32
        Caption = 'Sele'#231#227'o de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 757
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 243
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 753
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 757
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 287
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 611
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 609
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DBGIMEIs: TdmkDBGridZTO
    Left = 0
    Top = 112
    Width = 757
    Height = 442
    Align = alClient
    DataSource = DsIMEIs
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    Columns = <
      item
        Expanded = False
        FieldName = 'VMI_Sorc'
        Title.Caption = 'IME-I'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_SerieFch'
        Title.Caption = 'S'#233'rie'
        Width = 104
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ficha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_PRD_TAM_COR'
        Title.Caption = 'Artigo'
        Width = 228
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORNECE'
        Title.Caption = 'Fornecedor'
        Width = 246
        Visible = True
      end>
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT VMI_Sorc, vsf.Nome NO_SerieFch, vmi.Ficha,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))'
      ')'
      'NO_PRD_TAM_COR'
      'FROM vspaclaitsa  pci'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pci.VMI_Sorc'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'WHERE NOT ( '
      '  pci.VMI_Sorc IN ( '
      '    SELECT DISTINCT VSMovIts '
      '    FROM vscgiits '
      '  ) '
      ') '
      'ORDER BY VMI_Sorc DESC '
      '')
    Left = 240
    Top = 60
    object QrIMEIsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrIMEIsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrIMEIsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrIMEIsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 240
    Top = 108
  end
end
