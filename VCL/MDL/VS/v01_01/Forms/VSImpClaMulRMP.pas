unit VSImpClaMulRMP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables;

type
  TFmVSImpClaMulRMP = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Panel5: TPanel;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    MeListaFichas: TMemo;
    PB1: TProgressBar;
    RGModelo: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSImpClaMulRMP: TFmVSImpClaMulRMP;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF;

{$R *.DFM}

procedure TFmVSImpClaMulRMP.BtOKClick(Sender: TObject);
const
  MostraJanela = False;
var
  SerieFch: Integer;
  NO_Serie: String;
  Txt: String;
  Fichas: array of Integer;
  I, N, K, IMEI: Integer;
begin
  SerieFch := EdSerieFch.ValueVariant;
  if MyObjects.FIC(SerieFch = 0, EdSerieFch, 'Informe a s�rie!') then Exit;
  //
  NO_Serie := CBSerieFch.Text;
  //
  K := 0;
  N := MeListaFichas.Lines.Count;
  //SetLength(Fichas, N);
  for I := 0 to N - 1 do
  begin
    Txt := Trim(MeListaFichas.Lines[I]);
    IMEI := Geral.IMV(Txt);
    if IMEI > 0 then
    begin
      K := K + 1;
      SetLength(Fichas, K);
      Fichas[K - 1] := IMEI;
    end else
      if Txt <> '' then
        Geral.MB_Aviso('O texto "' + MeListaFichas.Lines[I] +
        '" n�o � um n�mero de IME-I v�lido e n�o ser� pesquisado!');
  end;
  if K = 0 then
  begin
    Geral.MB_Aviso('Nenhuma ficha foi informada!');
    Exit;
  end;
  case RGModelo.ItemIndex of
    0: VS_PF.ImprimeClassFichaRMP_Mul_Old(SerieFch, NO_Serie, Fichas, MostraJanela,
       PB1, LaAviso1, LaAviso2);
    1: VS_PF.ImprimeClassFichaRMP_Mul_New(SerieFch, NO_Serie, Fichas, MostraJanela,
       PB1, LaAviso1, LaAviso2);
  end;
end;

procedure TFmVSImpClaMulRMP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpClaMulRMP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpClaMulRMP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVSSerFch, Dmod.MyDB);
end;

procedure TFmVSImpClaMulRMP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
