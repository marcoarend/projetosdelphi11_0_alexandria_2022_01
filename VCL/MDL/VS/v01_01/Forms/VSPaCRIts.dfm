object FmVSPaCRIts: TFmVSPaCRIts
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-052 :: Gerenciamento de Boxes de Classifica'#231#227'o / Recla' +
    'ssifica'#231#227'o'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 724
        Height = 32
        Caption = 'Gerenciamento de Boxes de Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 724
        Height = 32
        Caption = 'Gerenciamento de Boxes de Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 724
        Height = 32
        Caption = 'Gerenciamento de Boxes de Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGVSPaCRIts: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 450
          Align = alClient
          DataSource = DsVSPaCRIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGVSPaCRItsDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_ClaOuRcl'
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CacCod'
              Title.Caption = 'OC'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Adi'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPallet'
              Title.Caption = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Sorc'
              Title.Caption = 'IME-I origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Dest'
              Title.Caption = 'IME-I destino'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Baix'
              Title.Caption = 'IME-I Baixa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tecla'
              Title.Caption = 'Box'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrIni'
              Title.Caption = 'Data/hora adi'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFim'
              Title.Caption = 'Data/hora encerrou'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QIt_Sorc'
              Title.Caption = 'Qtd itens orig'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QIt_Baix'
              Title.Caption = 'Qtd itens bxa'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtEncerra: TBitBtn
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Encerra'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEncerraClick
      end
    end
  end
  object QrVSPaCRIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.DtHrEndAdd, cab.DtHrFimCla, ('
      'IF(pal.DtHrEndAdd < "100-01-01", 1, 0) + '
      
        'IF(cab.LstPal01=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '2, 0) +'
      
        'IF(cab.LstPal02=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '4, 0) +'
      
        'IF(cab.LstPal03=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '8, 0) +'
      
        'IF(cab.LstPal04=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '16, 0) +'
      
        'IF(cab.LstPal05=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '32, 0) +'
      
        'IF(cab.LstPal06=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", ' +
        '64, 0) ) '
      '+ 0.000 EstahNaOC,'
      '1.000 ClaOuRcl,"Classifica'#231#227'o" NO_ClaOuRcl, '
      'cab.CacCod, its.Codigo, its.Controle, its.VSPallet,  '
      'its.VMI_Sorc, its.VMI_Baix, its.VMI_Dest, its.Tecla,  '
      'its.DtHrIni, its.DtHrFim, its.QIt_Sorc, its.QIt_Baix '
      'FROM vspaclaitsa its '
      'LEFT JOIN vspaclacaba cab ON cab.Codigo=its.Codigo '
      'LEFT JOIN vspalleta pal ON pal.Codigo=its.VSPallet'
      'WHERE its.DtHrFim < "1900-01-01" '
      ' '
      'UNION '
      ' '
      'SELECT pal.DtHrEndAdd, cab.DtHrFimCla, ('
      'IF(pal.DtHrEndAdd < "100-01-01", 1, 0) + '
      'IF(cab.LstPal01=its.VSPallet, 2, 0) +'
      'IF(cab.LstPal02=its.VSPallet, 4, 0) +'
      'IF(cab.LstPal03=its.VSPallet, 8, 0) +'
      'IF(cab.LstPal04=its.VSPallet, 16, 0) +'
      'IF(cab.LstPal05=its.VSPallet, 32, 0) +'
      'IF(cab.LstPal06=its.VSPallet, 64, 0) ) + 0.000 EstahNaOC,'
      '2.000 ClaOuRcl,"Reclassifica'#231#227'o" NO_ClaOuRcl, '
      'cab.CacCod, its.Codigo, its.Controle, its.VSPallet,  '
      'its.VMI_Sorc, its.VMI_Baix, its.VMI_Dest, its.Tecla,  '
      'its.DtHrIni, its.DtHrFim, its.QIt_Sorc, its.QIt_Baix '
      'FROM vsparclitsa its '
      'LEFT JOIN vsparclcaba cab ON cab.Codigo=its.Codigo '
      'LEFT JOIN vspalleta pal ON pal.Codigo=its.VSPallet'
      'WHERE its.DtHrFim < "1900-01-01" '
      'ORDER BY VSPallet, Controle ')
    Left = 268
    Top = 196
    object QrVSPaCRItsNO_ClaOuRcl: TWideStringField
      FieldName = 'NO_ClaOuRcl'
      Origin = 'NO_ClaOuRcl'
      Required = True
      Size = 15
    end
    object QrVSPaCRItsClaOuRcl: TFloatField
      FieldName = 'ClaOuRcl'
      Origin = 'ClaOuRcl'
      Required = True
    end
    object QrVSPaCRItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object QrVSPaCRItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'Controle'
      Required = True
    end
    object QrVSPaCRItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
      Origin = 'VSPallet'
      Required = True
    end
    object QrVSPaCRItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
      Origin = 'VMI_Sorc'
      Required = True
    end
    object QrVSPaCRItsVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
      Origin = 'VMI_Baix'
      Required = True
    end
    object QrVSPaCRItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
      Origin = 'VMI_Dest'
      Required = True
    end
    object QrVSPaCRItsTecla: TIntegerField
      FieldName = 'Tecla'
      Origin = 'Tecla'
      Required = True
    end
    object QrVSPaCRItsDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      Origin = 'DtHrIni'
      Required = True
    end
    object QrVSPaCRItsDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
      Origin = 'DtHrFim'
      Required = True
    end
    object QrVSPaCRItsQIt_Sorc: TIntegerField
      FieldName = 'QIt_Sorc'
      Origin = 'QIt_Sorc'
      Required = True
    end
    object QrVSPaCRItsQIt_Baix: TIntegerField
      FieldName = 'QIt_Baix'
      Origin = 'QIt_Baix'
      Required = True
    end
    object QrVSPaCRItsEstahNaOC: TFloatField
      FieldName = 'EstahNaOC'
    end
    object QrVSPaCRItsCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object DsVSPaCRIts: TDataSource
    DataSet = QrVSPaCRIts
    Left = 264
    Top = 244
  end
  object QrRcl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 520
    Top = 224
    object QrRclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRclCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrRclVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrRclMontPalt: TIntegerField
      FieldName = 'MontPalt'
    end
  end
  object QrCla: TmySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 324
    object QrClaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClaCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrClaVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrClaMontPalt: TIntegerField
      FieldName = 'MontPalt'
    end
  end
  object PMEncerra: TPopupMenu
    Left = 192
    Top = 528
    object Encerraitematual1: TMenuItem
      Caption = 'Encerra item &Atual'
      OnClick = Encerraitematual1Click
    end
    object EncerraTodositenspossiveis1: TMenuItem
      Caption = 'Encerra &Todos itens poss'#237'veis'
      Enabled = False
      OnClick = EncerraTodositenspossiveis1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizatodosboxes1: TMenuItem
      Caption = 'Atualiza todos boxes'
      OnClick = Atualizatodosboxes1Click
    end
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 788
    Top = 312
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 792
    Top = 360
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 872
    Top = 312
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 872
    Top = 360
  end
end
