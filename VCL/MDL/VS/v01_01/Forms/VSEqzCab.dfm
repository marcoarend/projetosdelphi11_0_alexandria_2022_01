object FmVSEqzCab: TFmVSEqzCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-067 :: Gerenciamento de Equ'#225'lize'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 788
      Top = 197
      Height = 304
      ExplicitLeft = 689
      ExplicitTop = 209
    end
    object Splitter2: TSplitter
      Left = 397
      Top = 197
      Height = 304
      Visible = False
      ExplicitLeft = 260
      ExplicitTop = 201
      ExplicitHeight = 191
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 352
        Top = 15
        Width = 654
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 521
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtArt: TBitBtn
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Artigo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtArtClick
        end
        object BtIts: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItsClick
        end
        object BtSub: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sub classe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSubClick
        end
      end
    end
    object GBNotas: TGroupBox
      Left = 895
      Top = 197
      Width = 113
      Height = 304
      Align = alRight
      Caption = ' Notas por sub classe: '
      TabOrder = 1
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 2
        Top = 15
        Width = 109
        Height = 287
        Align = alClient
        DataSource = DsNotas
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = '% '#225'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BasNota'
            Title.Caption = 'Base nota'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaM2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Soma notas m'#178
            Width = 112
            Visible = True
          end>
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 197
      Width = 397
      Height = 304
      Align = alLeft
      Caption = 'Pallets de destino: '
      TabOrder = 2
      object DBGrid1: TDBGrid
        Left = 2
        Top = 15
        Width = 393
        Height = 287
        Align = alClient
        DataSource = DsVSEqzIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Pallet'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Nome artigo'
            Width = 221
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 56
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 400
      Top = 197
      Width = 388
      Height = 304
      Align = alLeft
      Caption = ' Sub classes definidas: '
      TabOrder = 3
      object DBGrid2: TDBGrid
        Left = 2
        Top = 15
        Width = 384
        Height = 287
        Align = alClient
        DataSource = DsVSEqzSub
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubClass'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Sigla'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BasNota'
            Visible = True
          end>
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 197
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 137
        Align = alTop
        Caption = ' Dados de cadastro do cabe;alho: '
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 16
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label2: TLabel
          Left = 76
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 16
          Top = 56
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label6: TLabel
          Left = 16
          Top = 96
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label10: TLabel
          Left = 584
          Top = 56
          Width = 62
          Height = 13
          Caption = 'Classificador:'
        end
        object Label11: TLabel
          Left = 692
          Top = 16
          Width = 87
          Height = 13
          Caption = 'Data / hora in'#237'cio:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label12: TLabel
          Left = 844
          Top = 16
          Width = 95
          Height = 13
          Caption = 'Data / hora t'#233'rmino:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label13: TLabel
          Left = 892
          Top = 96
          Width = 96
          Height = 13
          Caption = 'Base para nota 100:'
        end
        object Label14: TLabel
          Left = 388
          Top = 76
          Width = 33
          Height = 13
          Caption = 'DtHrIni'
          FocusControl = DBEdit3
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSEqzCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 76
          Top = 32
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSEqzCab
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 132
          Top = 32
          Width = 557
          Height = 21
          DataField = 'NO_EMP'
          DataSource = DsVSEqzCab
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 692
          Top = 32
          Width = 148
          Height = 21
          DataField = 'DtHrIni'
          DataSource = DsVSEqzCab
          TabOrder = 3
        end
        object DBEdit4: TDBEdit
          Left = 844
          Top = 32
          Width = 148
          Height = 21
          DataField = 'DtHrFim'
          DataSource = DsVSEqzCab
          TabOrder = 4
        end
        object DBEdit5: TDBEdit
          Left = 72
          Top = 72
          Width = 505
          Height = 21
          DataField = 'NO_CLI'
          DataSource = DsVSEqzCab
          TabOrder = 5
        end
        object DBEdit6: TDBEdit
          Left = 16
          Top = 72
          Width = 56
          Height = 21
          DataField = 'Cliente'
          DataSource = DsVSEqzCab
          TabOrder = 6
        end
        object DBEdit7: TDBEdit
          Left = 584
          Top = 72
          Width = 56
          Height = 21
          DataField = 'Classificador'
          DataSource = DsVSEqzCab
          TabOrder = 7
        end
        object DBEdit8: TDBEdit
          Left = 640
          Top = 72
          Width = 353
          Height = 21
          DataField = 'NO_REV'
          DataSource = DsVSEqzCab
          TabOrder = 8
        end
        object DBEdit9: TDBEdit
          Left = 16
          Top = 112
          Width = 873
          Height = 21
          DataField = 'Nome'
          DataSource = DsVSEqzCab
          TabOrder = 9
        end
        object DBEdit10: TDBEdit
          Left = 892
          Top = 112
          Width = 100
          Height = 21
          DataField = 'BasNotZero'
          DataSource = DsVSEqzCab
          TabOrder = 10
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 137
        Width = 1008
        Height = 60
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 480
          Height = 60
          Align = alLeft
          Caption = ' Dados de nota de todos couros: '
          TabOrder = 0
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 476
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label15: TLabel
              Left = 8
              Top = 0
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
              FocusControl = DBEdit11
            end
            object Label16: TLabel
              Left = 68
              Top = 0
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
              FocusControl = DBEdit12
            end
            object Label17: TLabel
              Left = 152
              Top = 0
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
              FocusControl = DBEdit13
            end
            object Label18: TLabel
              Left = 236
              Top = 0
              Width = 59
              Height = 13
              Caption = 'Soma notas:'
              FocusControl = DBEdit14
            end
            object Label19: TLabel
              Left = 320
              Top = 0
              Width = 67
              Height = 13
              Caption = 'Nota bruta m'#178':'
              FocusControl = DBEdit15
            end
            object Label21: TLabel
              Left = 396
              Top = 0
              Width = 72
              Height = 13
              Caption = 'Nota corrig. m'#178':'
              FocusControl = DBEdit16
            end
            object DBEdit11: TDBEdit
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Pecas'
              DataSource = DsNotaAll
              TabOrder = 0
            end
            object DBEdit12: TDBEdit
              Left = 68
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsNotaAll
              TabOrder = 1
            end
            object DBEdit13: TDBEdit
              Left = 152
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaP2'
              DataSource = DsNotaAll
              TabOrder = 2
            end
            object DBEdit14: TDBEdit
              Left = 236
              Top = 16
              Width = 80
              Height = 21
              DataField = 'NotaAll'
              DataSource = DsNotaAll
              TabOrder = 3
            end
            object DBEdit15: TDBEdit
              Left = 320
              Top = 16
              Width = 72
              Height = 21
              DataField = 'NotaBrutaM2'
              DataSource = DsNotaAll
              TabOrder = 4
            end
            object DBEdit16: TDBEdit
              Left = 396
              Top = 16
              Width = 72
              Height = 21
              DataField = 'NotaEqzM2'
              DataSource = DsNotaAll
              TabOrder = 5
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 480
          Top = 0
          Width = 528
          Height = 60
          Align = alClient
          Caption = ' Dados de nota dos couros com sub classe definida: '
          TabOrder = 1
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 524
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label22: TLabel
              Left = 8
              Top = 0
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
              FocusControl = DBEdit17
            end
            object Label23: TLabel
              Left = 68
              Top = 0
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
              FocusControl = DBEdit18
            end
            object Label24: TLabel
              Left = 152
              Top = 0
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
              FocusControl = DBEdit19
            end
            object Label25: TLabel
              Left = 236
              Top = 0
              Width = 59
              Height = 13
              Caption = 'Soma notas:'
              FocusControl = DBEdit20
            end
            object Label26: TLabel
              Left = 320
              Top = 0
              Width = 67
              Height = 13
              Caption = 'Nota bruta m'#178':'
              FocusControl = DBEdit21
            end
            object Label27: TLabel
              Left = 396
              Top = 0
              Width = 72
              Height = 13
              Caption = 'Nota corrig. m'#178':'
              FocusControl = DBEdit22
            end
            object DBEdit17: TDBEdit
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Pecas'
              DataSource = DsNotaCrr
              TabOrder = 0
            end
            object DBEdit18: TDBEdit
              Left = 68
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsNotaCrr
              TabOrder = 1
            end
            object DBEdit19: TDBEdit
              Left = 152
              Top = 16
              Width = 80
              Height = 21
              DataField = 'AreaP2'
              DataSource = DsNotaCrr
              TabOrder = 2
            end
            object DBEdit20: TDBEdit
              Left = 236
              Top = 16
              Width = 80
              Height = 21
              DataField = 'NotaAll'
              DataSource = DsNotaCrr
              TabOrder = 3
            end
            object DBEdit21: TDBEdit
              Left = 320
              Top = 16
              Width = 72
              Height = 21
              DataField = 'NotaBrutaM2'
              DataSource = DsNotaCrr
              TabOrder = 4
            end
            object DBEdit22: TDBEdit
              Left = 396
              Top = 16
              Width = 72
              Height = 21
              DataField = 'NotaEqzM2'
              DataSource = DsNotaCrr
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 5
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label52: TLabel
        Left = 692
        Top = 16
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 844
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Data / hora t'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 96
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object SbCliente: TSpeedButton
        Left = 560
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteClick
      end
      object Label3: TLabel
        Left = 584
        Top = 56
        Width = 62
        Height = 13
        Caption = 'Classificador:'
      end
      object SBClassificador: TSpeedButton
        Left = 972
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBClassificadorClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 557
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 692
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 800
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 844
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 952
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 112
        Width = 977
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 8
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClassificador: TdmkEditCB
        Left = 584
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Classificador'
        UpdCampo = 'Classificador'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClassificador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClassificador: TdmkDBLookupComboBox
        Left = 640
        Top = 72
        Width = 333
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClassificadores
        TabOrder = 10
        dmkEditCB = EdClassificador
        QryCampo = 'Classificador'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PnSohUpdate: TPanel
      Left = 0
      Top = 137
      Width = 1008
      Height = 44
      Align = alTop
      TabOrder = 2
      Visible = False
      object Label4: TLabel
        Left = 16
        Top = 0
        Width = 96
        Height = 13
        Caption = 'Base para nota 100:'
      end
      object EdBasNotZero: TdmkEdit
        Left = 16
        Top = 16
        Width = 97
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'BasNotZero'
        UpdCampo = 'BasNotZero'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object PnSohInsert: TPanel
      Left = 0
      Top = 181
      Width = 1008
      Height = 48
      Align = alTop
      TabOrder = 3
      Visible = False
      object Label28: TLabel
        Left = 16
        Top = 4
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de equ'#225'lize:'
      end
      object SbVSCfgEqzCb: TSpeedButton
        Left = 560
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbVSCfgEqzCbClick
      end
      object EdVSCfgEqzCb: TdmkEditCB
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'VSCfgEqzCb'
        UpdCampo = 'VSCfgEqzCb'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSCfgEqzCb
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVSCfgEqzCb: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSCfgEqzCb
        TabOrder = 1
        dmkEditCB = EdVSCfgEqzCb
        QryCampo = 'Classificador'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 334
        Height = 32
        Caption = 'Gerenciamento de Equ'#225'lize'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Caption = 'Gerenciamento de Equ'#225'lize'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 334
        Height = 32
        Caption = 'Gerenciamento de Equ'#225'lize'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSEqzCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSEqzCabBeforeOpen
    AfterOpen = QrVSEqzCabAfterOpen
    BeforeClose = QrVSEqzCabBeforeClose
    AfterScroll = QrVSEqzCabAfterScroll
    SQL.Strings = (
      'SELECT vdc.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, '
      'IF(rev.Tipo=0, rev.RazaoSocial, rev.Nome) NO_REV'
      'FROM vsdsncab vdc'
      'LEFT JOIN entidades emp ON emp.Codigo=vdc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=vdc.Cliente'
      'LEFT JOIN entidades rev ON rev.Codigo=vdc.Desnatador'
      '')
    Left = 224
    Top = 45
    object QrVSEqzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSEqzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSEqzCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSEqzCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSEqzCabClassificador: TIntegerField
      FieldName = 'Classificador'
    end
    object QrVSEqzCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSEqzCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSEqzCabBasNotZero: TFloatField
      FieldName = 'BasNotZero'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSEqzCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSEqzCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSEqzCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSEqzCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSEqzCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSEqzCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSEqzCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSEqzCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrVSEqzCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrVSEqzCabNO_REV: TWideStringField
      FieldName = 'NO_REV'
      Size = 100
    end
  end
  object DsVSEqzCab: TDataSource
    DataSet = QrVSEqzCab
    Left = 224
    Top = 93
  end
  object QrVSEqzArt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dsa.*, '
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR '
      'FROM vsdsnart dsa '
      'LEFT JOIN gragrux ggx ON ggx.Controle=dsa.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ')
    Left = 300
    Top = 45
    object QrVSEqzArtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSEqzArtControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSEqzArtGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSEqzArtBasNota: TFloatField
      FieldName = 'BasNota'
    end
    object QrVSEqzArtInteresse: TSmallintField
      FieldName = 'Interesse'
    end
    object QrVSEqzArtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSEqzArtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSEqzArtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSEqzArtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSEqzArtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSEqzArtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSEqzArtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSEqzArtNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSEqzArtNO_Interesse: TWideStringField
      FieldName = 'NO_Interesse'
      Size = 3
    end
  end
  object DsVSEqzArt: TDataSource
    DataSet = QrVSEqzArt
    Left = 300
    Top = 93
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object Palletsdedestino1: TMenuItem
        Caption = '&Pallets de destino'
        OnClick = Palletsdedestino1Click
      end
      object Palletdeorigem1: TMenuItem
        Caption = 'Pallet de origem'
        Enabled = False
      end
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrVSEqzIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dsi.*, ger.GraGruX, ger.MovimCod, ger.VSPallet,'
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR  '
      'FROM vsdsnits dsi'
      'LEFT JOIN vsgerrcla ger ON ger.CacCod=dsi.VSCacCod'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ger.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ')
    Left = 376
    Top = 45
    object QrVSEqzItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSEqzItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSEqzItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSEqzItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSEqzItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSEqzItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSEqzItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSEqzItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSEqzItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSEqzItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSEqzItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSEqzItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
  end
  object DsVSEqzIts: TDataSource
    DataSet = QrVSEqzIts
    Left = 376
    Top = 93
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 752
    Top = 44
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 752
    Top = 92
  end
  object QrClassificadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'OR ent.Terceiro="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 828
    Top = 44
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClassificadores: TDataSource
    DataSet = QrClassificadores
    Left = 828
    Top = 92
  end
  object PMArt: TPopupMenu
    OnPopup = PMArtPopup
    Left = 364
    Top = 376
    object AdicionaArtigo1: TMenuItem
      Caption = '&Adiciona Artigo'
      Enabled = False
      OnClick = AdicionaArtigo1Click
    end
    object EditaArtigo1: TMenuItem
      Caption = '&Edita Artigo'
      Enabled = False
      OnClick = EditaArtigo1Click
    end
    object RemoveArtigo1: TMenuItem
      Caption = '&Remove Artigo'
      Enabled = False
      OnClick = RemoveArtigo1Click
    end
  end
  object QrVSEqzSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vds.*,'
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR '
      'FROM vsdsnsub vds'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vds.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      '')
    Left = 452
    Top = 44
    object QrVSEqzSubCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSEqzSubControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSEqzSubSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrVSEqzSubGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSEqzSubBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSEqzSubInteresse: TSmallintField
      FieldName = 'Interesse'
    end
    object QrVSEqzSubLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSEqzSubDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSEqzSubDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSEqzSubUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSEqzSubUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSEqzSubAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSEqzSubAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSEqzSubNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsVSEqzSub: TDataSource
    DataSet = QrVSEqzSub
    Left = 452
    Top = 92
  end
  object PMSub: TPopupMenu
    OnPopup = PMSubPopup
    Left = 476
    Top = 376
    object AdicionaSigla1: TMenuItem
      Caption = '&Adiciona info de Sigla'
      OnClick = AdicionaSigla1Click
    end
    object EditaSigla1: TMenuItem
      Caption = '&Edita info de Sigla'
      OnClick = EditaSigla1Click
    end
    object RemoveSigla1: TMenuItem
      Caption = '&Remove info Sigla'
      OnClick = RemoveSigla1Click
    end
  end
  object QrNotaAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 524
    Top = 44
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 524
    Top = 88
  end
  object QrNotaCrr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 600
    Top = 44
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 600
    Top = 88
  end
  object QrNotas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 676
    Top = 44
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 676
    Top = 88
  end
  object QrVSCfgEqzCb: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vscfgeqzcb'
      'ORDER BY Nome')
    Left = 908
    Top = 25
    object QrVSCfgEqzCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCfgEqzCbNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSCfgEqzCbBasNotZero: TFloatField
      FieldName = 'BasNotZero'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSCfgEqzCb: TDataSource
    DataSet = QrVSCfgEqzCb
    Left = 912
    Top = 73
  end
  object QrVSCfgEqzIt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vscfgeqzit'
      'WHERE Codigo > 0')
    Left = 556
    Top = 381
    object QrVSCfgEqzItCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCfgEqzItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSCfgEqzItSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrVSCfgEqzItGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCfgEqzItBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSCfgEqzItFatorEqz: TFloatField
      FieldName = 'FatorEqz'
      DisplayFormat = '#,###,##0.00'
    end
  end
end
