unit VSInnCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, AppListas, UnGrl_Consts, UnComps_Vars, UnGrl_Geral, UnAppEnums,
  UnProjGroup_Consts, UnProjGroup_Vars, UnInternalConsts3;

type
  THackDBGrid = class(TDBGrid);
  TFmVSInnCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSInnCab: TMySQLQuery;
    DsVSInnCab: TDataSource;
    QrVSInnIts: TmySQLQuery;
    DsVSInnIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSInnCabCodigo: TIntegerField;
    QrVSInnCabMovimCod: TIntegerField;
    QrVSInnCabEmpresa: TIntegerField;
    QrVSInnCabDtCompra: TDateTimeField;
    QrVSInnCabDtViagem: TDateTimeField;
    QrVSInnCabDtEntrada: TDateTimeField;
    QrVSInnCabFornecedor: TIntegerField;
    QrVSInnCabTransporta: TIntegerField;
    QrVSInnCabPecas: TFloatField;
    QrVSInnCabPesoKg: TFloatField;
    QrVSInnCabAreaM2: TFloatField;
    QrVSInnCabAreaP2: TFloatField;
    QrVSInnCabLk: TIntegerField;
    QrVSInnCabDataCad: TDateField;
    QrVSInnCabDataAlt: TDateField;
    QrVSInnCabUserCad: TIntegerField;
    QrVSInnCabUserAlt: TIntegerField;
    QrVSInnCabAlterWeb: TSmallintField;
    QrVSInnCabAtivo: TSmallintField;
    QrVSInnCabNO_EMPRESA: TWideStringField;
    QrVSInnCabNO_FORNECE: TWideStringField;
    QrVSInnCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    QrVSItsBxa: TmySQLQuery;
    DsVSItsBxa: TDataSource;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    N2: TMenuItem;
    QrVSInnCabValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    N1: TMenuItem;
    Atualizaestoque1: TMenuItem;
    QrVSItsGer: TmySQLQuery;
    DsVSItsGer: TDataSource;
    QrVSInnItsRendKgm2: TFloatField;
    QrVSInnItsNotaMPAG_TXT: TWideStringField;
    QrVSInnItsRendKgm2_TXT: TWideStringField;
    QrVSItsGerMisturou_TXT: TWideStringField;
    QrVSInnItsMisturou_TXT: TWideStringField;
    AnliseMPAG1: TMenuItem;
    frxWET_CURTI_006_01: TfrxReport;
    frxDsVSInnCab: TfrxDBDataset;
    frxDsVSInnIts: TfrxDBDataset;
    frxDsVSItsBxa: TfrxDBDataset;
    frxDsVSItsGer: TfrxDBDataset;
    QrVSItsGerNO_FORNECMO: TWideStringField;
    QrVSInnItsSIGLAUNIDMED: TWideStringField;
    QrVSInnItsNOMEUNIDMED: TWideStringField;
    QrVSItsBxaKgMedioCouro: TFloatField;
    QrVSItsGerCusto_m2: TFloatField;
    QrVSInnItsm2_CouroTXT: TWideStringField;
    QrVSInnItsKgMedioCouro: TFloatField;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    QrVSInnCabClienteMO: TIntegerField;
    QrVSInnCabProcednc: TIntegerField;
    QrVSInnCabMotorista: TIntegerField;
    QrVSInnCabPlaca: TWideStringField;
    QrVSInnCabNO_CLIENTEMO: TWideStringField;
    QrVSInnCabNO_PROCEDNC: TWideStringField;
    QrVSInnCabNO_MOTORISTA: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrVSMovDif: TmySQLQuery;
    QrVSMovDifControle: TIntegerField;
    QrVSMovDifInfPecas: TFloatField;
    QrVSMovDifInfPesoKg: TFloatField;
    QrVSMovDifInfAreaM2: TFloatField;
    QrVSMovDifInfAreaP2: TFloatField;
    QrVSMovDifInfValorT: TFloatField;
    QrVSMovDifLk: TIntegerField;
    QrVSMovDifDataCad: TDateField;
    QrVSMovDifDataAlt: TDateField;
    QrVSMovDifUserCad: TIntegerField;
    QrVSMovDifUserAlt: TIntegerField;
    QrVSMovDifAlterWeb: TSmallintField;
    QrVSMovDifAtivo: TSmallintField;
    QrVSMovDifDifPecas: TFloatField;
    QrVSMovDifDifPesoKg: TFloatField;
    QrVSMovDifDifAreaM2: TFloatField;
    QrVSMovDifDifAreaP2: TFloatField;
    QrVSMovDifDifValorT: TFloatField;
    Histrico1: TMenuItem;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    TabSheet2: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSHisFch: TmySQLQuery;
    DsVSHisFch: TDataSource;
    QrVSHisFchCodigo: TIntegerField;
    QrVSHisFchVSMovIts: TIntegerField;
    QrVSHisFchSerieFch: TIntegerField;
    QrVSHisFchFicha: TIntegerField;
    QrVSHisFchDataHora: TDateTimeField;
    QrVSHisFchNome: TWideStringField;
    QrVSHisFchObserv: TWideStringField;
    QrVSHisFchLk: TIntegerField;
    QrVSHisFchDataCad: TDateField;
    QrVSHisFchDataAlt: TDateField;
    QrVSHisFchUserCad: TIntegerField;
    QrVSHisFchUserAlt: TIntegerField;
    QrVSHisFchAlterWeb: TSmallintField;
    QrVSHisFchAtivo: TSmallintField;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    DBMemo1: TDBMemo;
    Exclui1: TMenuItem;
    Splitter2: TSplitter;
    PMVInnIts: TPopupMenu;
    IrparajaneladegerenciamentodeFichaRMP1: TMenuItem;
    PB1: TProgressBar;
    BtSubProduto: TBitBtn;
    PMSubProduto: TPopupMenu;
    IncluiSubProduto1: TMenuItem;
    AlteraSubProduto1: TMenuItem;
    ExcluiSubProduto1: TMenuItem;
    QrVSSubPrdIts: TmySQLQuery;
    DsVSSubPrdIts: TDataSource;
    Splitter3: TSplitter;
    QrVSMovDifPerQbrViag: TFloatField;
    QrVSMovDifPerQbrSal: TFloatField;
    QrVSMovDifRstCouPc: TFloatField;
    QrVSMovDifRstCouKg: TFloatField;
    QrVSMovDifRstCouVl: TFloatField;
    QrVSMovDifRstSalKg: TFloatField;
    QrVSMovDifRstSalVl: TFloatField;
    QrVSMovDifRstTotVl: TFloatField;
    QrVSMovDifTribDefSel: TIntegerField;
    QrVSMovDifPesoSalKg: TFloatField;
    QrTribIncIts: TmySQLQuery;
    DsTribIncIts: TDataSource;
    QrTribIncItsFatID: TIntegerField;
    QrTribIncItsFatNum: TIntegerField;
    QrTribIncItsFatParcela: TIntegerField;
    QrTribIncItsEmpresa: TIntegerField;
    QrTribIncItsControle: TIntegerField;
    QrTribIncItsData: TDateField;
    QrTribIncItsHora: TTimeField;
    QrTribIncItsValorFat: TFloatField;
    QrTribIncItsBaseCalc: TFloatField;
    QrTribIncItsValrTrib: TFloatField;
    QrTribIncItsPercent: TFloatField;
    QrTribIncItsLk: TIntegerField;
    QrTribIncItsDataCad: TDateField;
    QrTribIncItsDataAlt: TDateField;
    QrTribIncItsUserCad: TIntegerField;
    QrTribIncItsUserAlt: TIntegerField;
    QrTribIncItsAlterWeb: TSmallintField;
    QrTribIncItsAtivo: TSmallintField;
    QrTribIncItsTributo: TIntegerField;
    QrTribIncItsVUsoCalc: TFloatField;
    QrTribIncItsOperacao: TSmallintField;
    QrTribIncItsNO_Tributo: TWideStringField;
    PMTribIncIts: TPopupMenu;
    IncluiTributo1: TMenuItem;
    AlteraTributoAtual1: TMenuItem;
    ExcluiTributoAtual1: TMenuItem;
    BtTribIncIts: TBitBtn;
    QrTribIncItsFatorDC: TSmallintField;
    QrVSInnCabTemIMEIMrt: TIntegerField;
    QrVSInnItsCodigo: TLargeintField;
    QrVSInnItsControle: TLargeintField;
    QrVSInnItsMovimCod: TLargeintField;
    QrVSInnItsMovimNiv: TLargeintField;
    QrVSInnItsMovimTwn: TLargeintField;
    QrVSInnItsEmpresa: TLargeintField;
    QrVSInnItsTerceiro: TLargeintField;
    QrVSInnItsCliVenda: TLargeintField;
    QrVSInnItsMovimID: TLargeintField;
    QrVSInnItsDataHora: TDateTimeField;
    QrVSInnItsPallet: TLargeintField;
    QrVSInnItsGraGruX: TLargeintField;
    QrVSInnItsPecas: TFloatField;
    QrVSInnItsPesoKg: TFloatField;
    QrVSInnItsAreaM2: TFloatField;
    QrVSInnItsAreaP2: TFloatField;
    QrVSInnItsValorT: TFloatField;
    QrVSInnItsSrcMovID: TLargeintField;
    QrVSInnItsSrcNivel1: TLargeintField;
    QrVSInnItsSrcNivel2: TLargeintField;
    QrVSInnItsSrcGGX: TLargeintField;
    QrVSInnItsSdoVrtPeca: TFloatField;
    QrVSInnItsSdoVrtPeso: TFloatField;
    QrVSInnItsSdoVrtArM2: TFloatField;
    QrVSInnItsObserv: TWideStringField;
    QrVSInnItsSerieFch: TLargeintField;
    QrVSInnItsFicha: TLargeintField;
    QrVSInnItsMisturou: TLargeintField;
    QrVSInnItsFornecMO: TLargeintField;
    QrVSInnItsCustoMOKg: TFloatField;
    QrVSInnItsCustoMOM2: TFloatField;
    QrVSInnItsCustoMOTot: TFloatField;
    QrVSInnItsValorMP: TFloatField;
    QrVSInnItsDstMovID: TLargeintField;
    QrVSInnItsDstNivel1: TLargeintField;
    QrVSInnItsDstNivel2: TLargeintField;
    QrVSInnItsDstGGX: TLargeintField;
    QrVSInnItsQtdGerPeca: TFloatField;
    QrVSInnItsQtdGerPeso: TFloatField;
    QrVSInnItsQtdGerArM2: TFloatField;
    QrVSInnItsQtdGerArP2: TFloatField;
    QrVSInnItsQtdAntPeca: TFloatField;
    QrVSInnItsQtdAntPeso: TFloatField;
    QrVSInnItsQtdAntArM2: TFloatField;
    QrVSInnItsQtdAntArP2: TFloatField;
    QrVSInnItsNotaMPAG: TFloatField;
    QrVSInnItsNO_PALLET: TWideStringField;
    QrVSInnItsNO_PRD_TAM_COR: TWideStringField;
    QrVSInnItsNO_TTW: TWideStringField;
    QrVSInnItsID_TTW: TLargeintField;
    QrVSInnItsReqMovEstq: TLargeintField;
    QrVSInnItsPedItsFin: TLargeintField;
    QrVSInnItsMarca: TWideStringField;
    QrVSInnItsStqCenLoc: TLargeintField;
    QrVSSubPrdItsCodigo: TLargeintField;
    QrVSSubPrdItsControle: TLargeintField;
    QrVSSubPrdItsMovimCod: TLargeintField;
    QrVSSubPrdItsMovimNiv: TLargeintField;
    QrVSSubPrdItsMovimTwn: TLargeintField;
    QrVSSubPrdItsEmpresa: TLargeintField;
    QrVSSubPrdItsTerceiro: TLargeintField;
    QrVSSubPrdItsCliVenda: TLargeintField;
    QrVSSubPrdItsMovimID: TLargeintField;
    QrVSSubPrdItsDataHora: TDateTimeField;
    QrVSSubPrdItsPallet: TLargeintField;
    QrVSSubPrdItsGraGruX: TLargeintField;
    QrVSSubPrdItsPecas: TFloatField;
    QrVSSubPrdItsPesoKg: TFloatField;
    QrVSSubPrdItsAreaM2: TFloatField;
    QrVSSubPrdItsAreaP2: TFloatField;
    QrVSSubPrdItsValorT: TFloatField;
    QrVSSubPrdItsSrcMovID: TLargeintField;
    QrVSSubPrdItsSrcNivel1: TLargeintField;
    QrVSSubPrdItsSrcNivel2: TLargeintField;
    QrVSSubPrdItsSrcGGX: TLargeintField;
    QrVSSubPrdItsSdoVrtPeca: TFloatField;
    QrVSSubPrdItsSdoVrtPeso: TFloatField;
    QrVSSubPrdItsSdoVrtArM2: TFloatField;
    QrVSSubPrdItsObserv: TWideStringField;
    QrVSSubPrdItsSerieFch: TLargeintField;
    QrVSSubPrdItsFicha: TLargeintField;
    QrVSSubPrdItsMisturou: TLargeintField;
    QrVSSubPrdItsFornecMO: TLargeintField;
    QrVSSubPrdItsCustoMOKg: TFloatField;
    QrVSSubPrdItsCustoMOM2: TFloatField;
    QrVSSubPrdItsCustoMOTot: TFloatField;
    QrVSSubPrdItsValorMP: TFloatField;
    QrVSSubPrdItsDstMovID: TLargeintField;
    QrVSSubPrdItsDstNivel1: TLargeintField;
    QrVSSubPrdItsDstNivel2: TLargeintField;
    QrVSSubPrdItsDstGGX: TLargeintField;
    QrVSSubPrdItsQtdGerPeca: TFloatField;
    QrVSSubPrdItsQtdGerPeso: TFloatField;
    QrVSSubPrdItsQtdGerArM2: TFloatField;
    QrVSSubPrdItsQtdGerArP2: TFloatField;
    QrVSSubPrdItsQtdAntPeca: TFloatField;
    QrVSSubPrdItsQtdAntPeso: TFloatField;
    QrVSSubPrdItsQtdAntArM2: TFloatField;
    QrVSSubPrdItsQtdAntArP2: TFloatField;
    QrVSSubPrdItsNotaMPAG: TFloatField;
    QrVSSubPrdItsNO_FORNECE: TWideStringField;
    QrVSSubPrdItsNO_PRD_TAM_COR: TWideStringField;
    QrVSSubPrdItsID_TTW: TLargeintField;
    QrVSSubPrdItsNO_TTW: TWideStringField;
    QrVSItsGerCodigo: TLargeintField;
    QrVSItsGerControle: TLargeintField;
    QrVSItsGerMovimCod: TLargeintField;
    QrVSItsGerMovimNiv: TLargeintField;
    QrVSItsGerMovimTwn: TLargeintField;
    QrVSItsGerEmpresa: TLargeintField;
    QrVSItsGerTerceiro: TLargeintField;
    QrVSItsGerCliVenda: TLargeintField;
    QrVSItsGerMovimID: TLargeintField;
    QrVSItsGerDataHora: TDateTimeField;
    QrVSItsGerPallet: TLargeintField;
    QrVSItsGerGraGruX: TLargeintField;
    QrVSItsGerPecas: TFloatField;
    QrVSItsGerPesoKg: TFloatField;
    QrVSItsGerAreaM2: TFloatField;
    QrVSItsGerAreaP2: TFloatField;
    QrVSItsGerValorT: TFloatField;
    QrVSItsGerSrcMovID: TLargeintField;
    QrVSItsGerSrcNivel1: TLargeintField;
    QrVSItsGerSrcNivel2: TLargeintField;
    QrVSItsGerSrcGGX: TLargeintField;
    QrVSItsGerSdoVrtPeca: TFloatField;
    QrVSItsGerSdoVrtPeso: TFloatField;
    QrVSItsGerSdoVrtArM2: TFloatField;
    QrVSItsGerObserv: TWideStringField;
    QrVSItsGerSerieFch: TLargeintField;
    QrVSItsGerFicha: TLargeintField;
    QrVSItsGerMisturou: TLargeintField;
    QrVSItsGerFornecMO: TLargeintField;
    QrVSItsGerCustoMOKg: TFloatField;
    QrVSItsGerCustoMOM2: TFloatField;
    QrVSItsGerCustoMOTot: TFloatField;
    QrVSItsGerValorMP: TFloatField;
    QrVSItsGerDstMovID: TLargeintField;
    QrVSItsGerDstNivel1: TLargeintField;
    QrVSItsGerDstNivel2: TLargeintField;
    QrVSItsGerDstGGX: TLargeintField;
    QrVSItsGerQtdGerPeca: TFloatField;
    QrVSItsGerQtdGerPeso: TFloatField;
    QrVSItsGerQtdGerArM2: TFloatField;
    QrVSItsGerQtdGerArP2: TFloatField;
    QrVSItsGerQtdAntPeca: TFloatField;
    QrVSItsGerQtdAntPeso: TFloatField;
    QrVSItsGerQtdAntArM2: TFloatField;
    QrVSItsGerQtdAntArP2: TFloatField;
    QrVSItsGerNotaMPAG: TFloatField;
    QrVSItsGerPedItsFin: TLargeintField;
    QrVSItsGerMarca: TWideStringField;
    QrVSItsGerStqCenLoc: TLargeintField;
    QrVSItsGerNO_PALLET: TWideStringField;
    QrVSItsGerNO_PRD_TAM_COR: TWideStringField;
    QrVSItsGerNO_TTW: TWideStringField;
    QrVSItsGerID_TTW: TLargeintField;
    QrVSItsGerReqMovEstq: TLargeintField;
    QrVSItsBxaCodigo: TLargeintField;
    QrVSItsBxaControle: TLargeintField;
    QrVSItsBxaMovimCod: TLargeintField;
    QrVSItsBxaMovimNiv: TLargeintField;
    QrVSItsBxaMovimTwn: TLargeintField;
    QrVSItsBxaEmpresa: TLargeintField;
    QrVSItsBxaTerceiro: TLargeintField;
    QrVSItsBxaCliVenda: TLargeintField;
    QrVSItsBxaMovimID: TLargeintField;
    QrVSItsBxaDataHora: TDateTimeField;
    QrVSItsBxaPallet: TLargeintField;
    QrVSItsBxaGraGruX: TLargeintField;
    QrVSItsBxaPecas: TFloatField;
    QrVSItsBxaPesoKg: TFloatField;
    QrVSItsBxaAreaM2: TFloatField;
    QrVSItsBxaAreaP2: TFloatField;
    QrVSItsBxaValorT: TFloatField;
    QrVSItsBxaSrcMovID: TLargeintField;
    QrVSItsBxaSrcNivel1: TLargeintField;
    QrVSItsBxaSrcNivel2: TLargeintField;
    QrVSItsBxaSrcGGX: TLargeintField;
    QrVSItsBxaSdoVrtPeca: TFloatField;
    QrVSItsBxaSdoVrtPeso: TFloatField;
    QrVSItsBxaSdoVrtArM2: TFloatField;
    QrVSItsBxaObserv: TWideStringField;
    QrVSItsBxaSerieFch: TLargeintField;
    QrVSItsBxaFicha: TLargeintField;
    QrVSItsBxaMisturou: TLargeintField;
    QrVSItsBxaFornecMO: TLargeintField;
    QrVSItsBxaCustoMOKg: TFloatField;
    QrVSItsBxaCustoMOM2: TFloatField;
    QrVSItsBxaCustoMOTot: TFloatField;
    QrVSItsBxaValorMP: TFloatField;
    QrVSItsBxaDstMovID: TLargeintField;
    QrVSItsBxaDstNivel1: TLargeintField;
    QrVSItsBxaDstNivel2: TLargeintField;
    QrVSItsBxaDstGGX: TLargeintField;
    QrVSItsBxaQtdGerPeca: TFloatField;
    QrVSItsBxaQtdGerPeso: TFloatField;
    QrVSItsBxaQtdGerArM2: TFloatField;
    QrVSItsBxaQtdGerArP2: TFloatField;
    QrVSItsBxaQtdAntPeca: TFloatField;
    QrVSItsBxaQtdAntPeso: TFloatField;
    QrVSItsBxaQtdAntArM2: TFloatField;
    QrVSItsBxaQtdAntArP2: TFloatField;
    QrVSItsBxaNotaMPAG: TFloatField;
    QrVSItsBxaPedItsFin: TLargeintField;
    QrVSItsBxaMarca: TWideStringField;
    QrVSItsBxaStqCenLoc: TLargeintField;
    QrVSItsBxaNO_PALLET: TWideStringField;
    QrVSItsBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSItsBxaNO_TTW: TWideStringField;
    QrVSItsBxaID_TTW: TLargeintField;
    QrVSItsBxaReqMovEstq: TLargeintField;
    QrVSInnItsVSMulFrnCab: TLargeintField;
    QrVSInnItsClientMO: TLargeintField;
    QrVSInnItsNO_SerieFch: TWideStringField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsZerado: TSmallintField;
    QrVSMovItsEmFluxo: TSmallintField;
    QrVSMovItsLnkIDXtr: TIntegerField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsNotFluxo: TIntegerField;
    QrVSMovItsFatNotaVNC: TFloatField;
    QrVSMovItsFatNotaVRC: TFloatField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsItemNFe: TIntegerField;
    QrVSMovItsVSMorCab: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrVSMovItsCustoPQ: TFloatField;
    frxDsVSMovIts: TfrxDBDataset;
    QrVSMovItsNO_MovimID: TWideStringField;
    QrVSMovItsNO_MovimNiv: TWideStringField;
    QrVSMovItsGraGru1: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsSIGLAUNIDMED: TWideStringField;
    QrVSMovItsCODUSUUNIDMED: TIntegerField;
    QrVSMovItsNOMEUNIDMED: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsNO_GGY: TWideStringField;
    QrVSMovItsKgCouPQ: TFloatField;
    QrVSMovItsNO_GRUPO: TWideStringField;
    QrVSMovItsID_GRUPO: TFloatField;
    QrVSMovItsORDEM2: TFloatField;
    Label28: TLabel;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label29: TLabel;
    QrVSInnCabide_nNF: TIntegerField;
    QrVSInnCabide_serie: TSmallintField;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    QrVSGerados: TmySQLQuery;
    QrVSGeradosCodigo: TIntegerField;
    QrVSGeradosControle: TIntegerField;
    QrVSGeradosMovimCod: TIntegerField;
    QrVSGeradosMovimNiv: TIntegerField;
    QrVSGeradosMovimTwn: TIntegerField;
    QrVSGeradosEmpresa: TIntegerField;
    QrVSGeradosTerceiro: TIntegerField;
    QrVSGeradosCliVenda: TIntegerField;
    QrVSGeradosMovimID: TIntegerField;
    QrVSGeradosLnkNivXtr1: TIntegerField;
    QrVSGeradosLnkNivXtr2: TIntegerField;
    QrVSGeradosDataHora: TDateTimeField;
    QrVSGeradosPallet: TIntegerField;
    QrVSGeradosGraGruX: TIntegerField;
    QrVSGeradosPecas: TFloatField;
    QrVSGeradosPesoKg: TFloatField;
    QrVSGeradosAreaM2: TFloatField;
    QrVSGeradosAreaP2: TFloatField;
    QrVSGeradosValorT: TFloatField;
    QrVSGeradosSrcMovID: TIntegerField;
    QrVSGeradosSrcNivel1: TIntegerField;
    QrVSGeradosSrcNivel2: TIntegerField;
    QrVSGeradosSdoVrtPeca: TFloatField;
    QrVSGeradosSdoVrtArM2: TFloatField;
    QrVSGeradosObserv: TWideStringField;
    QrVSGeradosFicha: TIntegerField;
    QrVSGeradosMisturou: TSmallintField;
    QrVSGeradosCustoMOKg: TFloatField;
    QrVSGeradosCustoMOTot: TFloatField;
    QrVSGeradosLk: TIntegerField;
    QrVSGeradosDataCad: TDateField;
    QrVSGeradosDataAlt: TDateField;
    QrVSGeradosUserCad: TIntegerField;
    QrVSGeradosUserAlt: TIntegerField;
    QrVSGeradosAlterWeb: TSmallintField;
    QrVSGeradosAtivo: TSmallintField;
    QrVSGeradosSrcGGX: TIntegerField;
    QrVSGeradosSdoVrtPeso: TFloatField;
    QrVSGeradosSerieFch: TIntegerField;
    QrVSGeradosFornecMO: TIntegerField;
    QrVSGeradosValorMP: TFloatField;
    QrVSGeradosDstMovID: TIntegerField;
    QrVSGeradosDstNivel1: TIntegerField;
    QrVSGeradosDstNivel2: TIntegerField;
    QrVSGeradosDstGGX: TIntegerField;
    QrVSGeradosQtdGerPeca: TFloatField;
    QrVSGeradosQtdGerPeso: TFloatField;
    QrVSGeradosQtdGerArM2: TFloatField;
    QrVSGeradosQtdGerArP2: TFloatField;
    QrVSGeradosQtdAntPeca: TFloatField;
    QrVSGeradosQtdAntPeso: TFloatField;
    QrVSGeradosQtdAntArM2: TFloatField;
    QrVSGeradosQtdAntArP2: TFloatField;
    QrVSGeradosAptoUso: TSmallintField;
    QrVSGeradosNotaMPAG: TFloatField;
    QrVSGeradosMarca: TWideStringField;
    QrVSGeradosTpCalcAuto: TIntegerField;
    QrVSGeradosZerado: TSmallintField;
    QrVSGeradosEmFluxo: TSmallintField;
    QrVSGeradosLnkIDXtr: TIntegerField;
    QrVSGeradosCustoMOM2: TFloatField;
    QrVSGeradosNotFluxo: TIntegerField;
    QrVSGeradosFatNotaVNC: TFloatField;
    QrVSGeradosFatNotaVRC: TFloatField;
    QrVSGeradosPedItsLib: TIntegerField;
    QrVSGeradosPedItsFin: TIntegerField;
    QrVSGeradosPedItsVda: TIntegerField;
    QrVSGeradosReqMovEstq: TIntegerField;
    QrVSGeradosStqCenLoc: TIntegerField;
    QrVSGeradosItemNFe: TIntegerField;
    QrVSGeradosVSMorCab: TIntegerField;
    QrVSGeradosVSMulFrnCab: TIntegerField;
    QrVSGeradosClientMO: TIntegerField;
    QrVSGeradosCustoPQ: TFloatField;
    QrVSGeradosNO_MovimID: TWideStringField;
    QrVSGeradosNO_MovimNiv: TWideStringField;
    QrVSGeradosGraGru1: TIntegerField;
    QrVSGeradosNO_PRD_TAM_COR: TWideStringField;
    QrVSGeradosSIGLAUNIDMED: TWideStringField;
    QrVSGeradosCODUSUUNIDMED: TIntegerField;
    QrVSGeradosNOMEUNIDMED: TWideStringField;
    QrVSGeradosGraGruY: TIntegerField;
    QrVSGeradosNO_GGY: TWideStringField;
    QrVSGeradosKgCouPQ: TFloatField;
    QrVSGeradosNO_GRUPO: TWideStringField;
    QrVSGeradosID_GRUPO: TFloatField;
    QrVSGeradosORDEM2: TFloatField;
    frxDsVSGerados: TfrxDBDataset;
    Edemi_serie: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    Edemi_nNF: TdmkEdit;
    QrVSInnCabemi_serie: TIntegerField;
    QrVSInnCabemi_nNF: TIntegerField;
    QrVSInnCabNFeStatus: TIntegerField;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Resultados1: TMenuItem;
    NFes1: TMenuItem;
    N3: TMenuItem;
    IncluiNFe1: TMenuItem;
    AlteraNFe1: TMenuItem;
    ExcluiNFe1: TMenuItem;
    QrVSInnNFs: TmySQLQuery;
    DsVSInnNFs: TDataSource;
    QrVSInnNFsCodigo: TIntegerField;
    QrVSInnNFsControle: TIntegerField;
    QrVSInnNFsConta: TIntegerField;
    QrVSInnNFsPecas: TFloatField;
    QrVSInnNFsPesoKg: TFloatField;
    QrVSInnNFsAreaM2: TFloatField;
    QrVSInnNFsAreaP2: TFloatField;
    QrVSInnNFsValorT: TFloatField;
    QrVSInnNFsMotorista: TIntegerField;
    QrVSInnNFsPlaca: TWideStringField;
    QrVSInnNFside_serie: TIntegerField;
    QrVSInnNFside_nNF: TIntegerField;
    QrVSInnNFsemi_serie: TIntegerField;
    QrVSInnNFsemi_nNF: TIntegerField;
    QrVSInnNFsNFeStatus: TIntegerField;
    QrVSInnNFsVSVmcWrn: TSmallintField;
    QrVSInnNFsVSVmcObs: TWideStringField;
    QrVSInnNFsVSVmcSeq: TWideStringField;
    QrVSInnNFsLk: TIntegerField;
    QrVSInnNFsDataCad: TDateField;
    QrVSInnNFsDataAlt: TDateField;
    QrVSInnNFsUserCad: TIntegerField;
    QrVSInnNFsUserAlt: TIntegerField;
    QrVSInnNFsAlterWeb: TSmallintField;
    QrVSInnNFsAtivo: TSmallintField;
    QrVSInnNFsNo_Motorista: TWideStringField;
    QrVSInnItsDtCorrApo: TDateTimeField;
    AnliseMPAG2: TMenuItem;
    frxWET_CURTI_006_02: TfrxReport;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    ProgressBar1: TProgressBar;
    frxDsReceitas: TfrxDBDataset;
    frxDsDespesas: TfrxDBDataset;
    QrReceitas: TmySQLQuery;
    QrReceitasCodigo: TIntegerField;
    QrReceitasControle: TIntegerField;
    QrReceitasMovimCod: TIntegerField;
    QrReceitasMovimNiv: TIntegerField;
    QrReceitasMovimTwn: TIntegerField;
    QrReceitasEmpresa: TIntegerField;
    QrReceitasTerceiro: TIntegerField;
    QrReceitasCliVenda: TIntegerField;
    QrReceitasMovimID: TIntegerField;
    QrReceitasLnkNivXtr1: TIntegerField;
    QrReceitasLnkNivXtr2: TIntegerField;
    QrReceitasDataHora: TDateTimeField;
    QrReceitasPallet: TIntegerField;
    QrReceitasGraGruX: TIntegerField;
    QrReceitasPecas: TFloatField;
    QrReceitasPesoKg: TFloatField;
    QrReceitasAreaM2: TFloatField;
    QrReceitasAreaP2: TFloatField;
    QrReceitasValorT: TFloatField;
    QrReceitasSrcMovID: TIntegerField;
    QrReceitasSrcNivel1: TIntegerField;
    QrReceitasSrcNivel2: TIntegerField;
    QrReceitasSdoVrtPeca: TFloatField;
    QrReceitasSdoVrtArM2: TFloatField;
    QrReceitasObserv: TWideStringField;
    QrReceitasFicha: TIntegerField;
    QrReceitasMisturou: TSmallintField;
    QrReceitasCustoMOKg: TFloatField;
    QrReceitasCustoMOTot: TFloatField;
    QrReceitasLk: TIntegerField;
    QrReceitasDataCad: TDateField;
    QrReceitasDataAlt: TDateField;
    QrReceitasUserCad: TIntegerField;
    QrReceitasUserAlt: TIntegerField;
    QrReceitasAlterWeb: TSmallintField;
    QrReceitasAtivo: TSmallintField;
    QrReceitasSrcGGX: TIntegerField;
    QrReceitasSdoVrtPeso: TFloatField;
    QrReceitasSerieFch: TIntegerField;
    QrReceitasFornecMO: TIntegerField;
    QrReceitasValorMP: TFloatField;
    QrReceitasDstMovID: TIntegerField;
    QrReceitasDstNivel1: TIntegerField;
    QrReceitasDstNivel2: TIntegerField;
    QrReceitasDstGGX: TIntegerField;
    QrReceitasQtdGerPeca: TFloatField;
    QrReceitasQtdGerPeso: TFloatField;
    QrReceitasQtdGerArM2: TFloatField;
    QrReceitasQtdGerArP2: TFloatField;
    QrReceitasQtdAntPeca: TFloatField;
    QrReceitasQtdAntPeso: TFloatField;
    QrReceitasQtdAntArM2: TFloatField;
    QrReceitasQtdAntArP2: TFloatField;
    QrReceitasAptoUso: TSmallintField;
    QrReceitasNotaMPAG: TFloatField;
    QrReceitasMarca: TWideStringField;
    QrReceitasTpCalcAuto: TIntegerField;
    QrReceitasZerado: TSmallintField;
    QrReceitasEmFluxo: TSmallintField;
    QrReceitasLnkIDXtr: TIntegerField;
    QrReceitasCustoMOM2: TFloatField;
    QrReceitasNotFluxo: TIntegerField;
    QrReceitasFatNotaVNC: TFloatField;
    QrReceitasFatNotaVRC: TFloatField;
    QrReceitasPedItsLib: TIntegerField;
    QrReceitasPedItsFin: TIntegerField;
    QrReceitasPedItsVda: TIntegerField;
    QrReceitasReqMovEstq: TIntegerField;
    QrReceitasStqCenLoc: TIntegerField;
    QrReceitasItemNFe: TIntegerField;
    QrReceitasVSMorCab: TIntegerField;
    QrReceitasVSMulFrnCab: TIntegerField;
    QrReceitasClientMO: TIntegerField;
    QrReceitasCustoPQ: TFloatField;
    QrReceitasNO_MovimID: TWideStringField;
    QrReceitasNO_MovimNiv: TWideStringField;
    QrReceitasGraGru1: TIntegerField;
    QrReceitasNO_PRD_TAM_COR: TWideStringField;
    QrReceitasSIGLAUNIDMED: TWideStringField;
    QrReceitasCODUSUUNIDMED: TIntegerField;
    QrReceitasNOMEUNIDMED: TWideStringField;
    QrReceitasGraGruY: TIntegerField;
    QrReceitasNO_GGY: TWideStringField;
    QrReceitasKgCouPQ: TFloatField;
    QrReceitasNO_GRUPO: TWideStringField;
    QrReceitasID_GRUPO: TFloatField;
    QrReceitasORDEM2: TFloatField;
    QrDespesas: TmySQLQuery;
    QrDespesasCodigo: TIntegerField;
    QrDespesasControle: TIntegerField;
    QrDespesasMovimCod: TIntegerField;
    QrDespesasMovimNiv: TIntegerField;
    QrDespesasMovimTwn: TIntegerField;
    QrDespesasEmpresa: TIntegerField;
    QrDespesasTerceiro: TIntegerField;
    QrDespesasCliVenda: TIntegerField;
    QrDespesasMovimID: TIntegerField;
    QrDespesasLnkNivXtr1: TIntegerField;
    QrDespesasLnkNivXtr2: TIntegerField;
    QrDespesasDataHora: TDateTimeField;
    QrDespesasPallet: TIntegerField;
    QrDespesasGraGruX: TIntegerField;
    QrDespesasPecas: TFloatField;
    QrDespesasPesoKg: TFloatField;
    QrDespesasAreaM2: TFloatField;
    QrDespesasAreaP2: TFloatField;
    QrDespesasValorT: TFloatField;
    QrDespesasSrcMovID: TIntegerField;
    QrDespesasSrcNivel1: TIntegerField;
    QrDespesasSrcNivel2: TIntegerField;
    QrDespesasSdoVrtPeca: TFloatField;
    QrDespesasSdoVrtArM2: TFloatField;
    QrDespesasObserv: TWideStringField;
    QrDespesasFicha: TIntegerField;
    QrDespesasMisturou: TSmallintField;
    QrDespesasCustoMOKg: TFloatField;
    QrDespesasCustoMOTot: TFloatField;
    QrDespesasLk: TIntegerField;
    QrDespesasDataCad: TDateField;
    QrDespesasDataAlt: TDateField;
    QrDespesasUserCad: TIntegerField;
    QrDespesasUserAlt: TIntegerField;
    QrDespesasAlterWeb: TSmallintField;
    QrDespesasAtivo: TSmallintField;
    QrDespesasSrcGGX: TIntegerField;
    QrDespesasSdoVrtPeso: TFloatField;
    QrDespesasSerieFch: TIntegerField;
    QrDespesasFornecMO: TIntegerField;
    QrDespesasValorMP: TFloatField;
    QrDespesasDstMovID: TIntegerField;
    QrDespesasDstNivel1: TIntegerField;
    QrDespesasDstNivel2: TIntegerField;
    QrDespesasDstGGX: TIntegerField;
    QrDespesasQtdGerPeca: TFloatField;
    QrDespesasQtdGerPeso: TFloatField;
    QrDespesasQtdGerArM2: TFloatField;
    QrDespesasQtdGerArP2: TFloatField;
    QrDespesasQtdAntPeca: TFloatField;
    QrDespesasQtdAntPeso: TFloatField;
    QrDespesasQtdAntArM2: TFloatField;
    QrDespesasQtdAntArP2: TFloatField;
    QrDespesasAptoUso: TSmallintField;
    QrDespesasNotaMPAG: TFloatField;
    QrDespesasMarca: TWideStringField;
    QrDespesasTpCalcAuto: TIntegerField;
    QrDespesasZerado: TSmallintField;
    QrDespesasEmFluxo: TSmallintField;
    QrDespesasLnkIDXtr: TIntegerField;
    QrDespesasCustoMOM2: TFloatField;
    QrDespesasNotFluxo: TIntegerField;
    QrDespesasFatNotaVNC: TFloatField;
    QrDespesasFatNotaVRC: TFloatField;
    QrDespesasPedItsLib: TIntegerField;
    QrDespesasPedItsFin: TIntegerField;
    QrDespesasPedItsVda: TIntegerField;
    QrDespesasReqMovEstq: TIntegerField;
    QrDespesasStqCenLoc: TIntegerField;
    QrDespesasItemNFe: TIntegerField;
    QrDespesasVSMorCab: TIntegerField;
    QrDespesasVSMulFrnCab: TIntegerField;
    QrDespesasClientMO: TIntegerField;
    QrDespesasCustoPQ: TFloatField;
    QrDespesasNO_MovimID: TWideStringField;
    QrDespesasNO_MovimNiv: TWideStringField;
    QrDespesasGraGru1: TIntegerField;
    QrDespesasNO_PRD_TAM_COR: TWideStringField;
    QrDespesasSIGLAUNIDMED: TWideStringField;
    QrDespesasCODUSUUNIDMED: TIntegerField;
    QrDespesasNOMEUNIDMED: TWideStringField;
    QrDespesasGraGruY: TIntegerField;
    QrDespesasNO_GGY: TWideStringField;
    QrDespesasKgCouPQ: TFloatField;
    QrDespesasNO_GRUPO: TWideStringField;
    QrDespesasID_GRUPO: TFloatField;
    QrDespesasORDEM2: TFloatField;
    DBEdit27: TDBEdit;
    Label38: TLabel;
    QrVSInnCabVSVmcWrn: TSmallintField;
    QrVSInnCabVSVmcObs: TWideStringField;
    QrVSInnCabVSVmcSeq: TWideStringField;
    QrVSInnCabVSVmcSta: TSmallintField;
    QrVSInnCabDtEnceRend: TDateTimeField;
    QrVSInnCabTpEnceRend: TSmallintField;
    QrVSInnCabDtEnceRend_TXT: TWideStringField;
    Encerrarendimento1: TMenuItem;
    QrVSCPMRSBCb: TmySQLQuery;
    QrVSCPMRSBIt: TmySQLQuery;
    QrVSCPMRSBCbCodigo: TIntegerField;
    QrVSCPMRSBCbNome: TWideStringField;
    QrVSCPMRSBCbDataIni: TDateField;
    QrVSCPMRSBCbDataFim: TDateField;
    QrVSCPMRSBItControle: TIntegerField;
    QrVSCPMRSBItGraGruX: TIntegerField;
    QrVSCPMRSBItDivKgSPpcCou: TFloatField;
    QrVSCPMRSBItPercKgSPKgCou: TFloatField;
    QrReceitasMetaDivKgPc: TFloatField;
    QrReceitasMetaPerKgKg: TFloatField;
    QrReceitasResDivKgPc: TFloatField;
    QrReceitasResPerKgKg: TFloatField;
    Corrigepesodebaixas1: TMenuItem;
    PCSubItens: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBGrid5: TDBGrid;
    DBGrid4: TDBGrid;
    QrVSMOEnvEnv: TmySQLQuery;
    DsVSMOEnvEnv: TDataSource;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    Splitter1: TSplitter;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    TsEnvioMO: TTabSheet;
    QrVSMOEnvEVMI: TmySQLQuery;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    QrVSInnItsCusFrtAvuls: TFloatField;
    QrVSInnItsCusFrtMOEnv: TFloatField;
    TsFrCompr: TTabSheet;
    AtrelamentoFreteCompra1: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    QrVSMOEnvAvu: TmySQLQuery;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    DsVSMOEnvAVMI: TDataSource;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    PnFrCompr: TPanel;
    DBGVSMOEnvAvu: TdmkDBGridZTO;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVMI: TdmkDBGridZTO;
    MapadeDefeitos1: TMenuItem;
    QrMedias: TMySQLQuery;
    QrMediasPecas: TFloatField;
    QrMediasQtdGerArM2: TFloatField;
    QrMediasM2_PC: TFloatField;
    QrMediasPesoKg: TFloatField;
    QrMediasKg_PC: TFloatField;
    QrMediasKg_M2: TFloatField;
    frxWET_CURTI_006_01_SemGelatina: TfrxReport;
    AnliseMPAGdoIMEIselecionadoModelo1A1: TMenuItem;
    ClassesGeradas1: TMenuItem;
    QrVSInnCabAWServerID: TIntegerField;
    QrVSInnCabAWStatSinc: TSmallintField;
    QrVSInnItsCustoComiss: TFloatField;
    QrVSInnItsPerceComiss: TFloatField;
    QrVSInnItsCusKgComiss: TFloatField;
    QrVSInnItsCredValrImposto: TFloatField;
    QrVSInnItsCredPereImposto: TFloatField;
    TabSheet3: TTabSheet;
    QrEmissT: TMySQLQuery;
    QrEmissTCarteira: TIntegerField;
    QrEmissTDescricao: TWideStringField;
    QrEmissTNotaFiscal: TIntegerField;
    QrEmissTDebito: TFloatField;
    QrEmissTCompensado: TDateField;
    QrEmissTDocumento: TFloatField;
    QrEmissTSit: TIntegerField;
    QrEmissTVencimento: TDateField;
    QrEmissTFatID: TIntegerField;
    QrEmissTFatParcela: TIntegerField;
    QrEmissTData: TDateField;
    QrEmissTTipo: TSmallintField;
    QrEmissTAutorizacao: TIntegerField;
    QrEmissTGenero: TIntegerField;
    QrEmissTCredito: TFloatField;
    QrEmissTLk: TIntegerField;
    QrEmissTNOMESIT: TWideStringField;
    QrEmissTNOMETIPO: TWideStringField;
    QrEmissTNOMECARTEIRA: TWideStringField;
    QrEmissTID_Sub: TSmallintField;
    QrEmissTSub: TIntegerField;
    QrEmissTFatura: TWideStringField;
    QrEmissTBanco: TIntegerField;
    QrEmissTLocal: TIntegerField;
    QrEmissTCartao: TIntegerField;
    QrEmissTLinha: TIntegerField;
    QrEmissTPago: TFloatField;
    QrEmissTMez: TIntegerField;
    QrEmissTFornecedor: TIntegerField;
    QrEmissTCliente: TIntegerField;
    QrEmissTControle: TIntegerField;
    QrEmissTID_Pgto: TIntegerField;
    QrEmissTCliInt: TIntegerField;
    QrEmissTQtde: TFloatField;
    QrEmissTFatID_Sub: TIntegerField;
    QrEmissTOperCount: TIntegerField;
    QrEmissTLancto: TIntegerField;
    QrEmissTForneceI: TIntegerField;
    QrEmissTMoraDia: TFloatField;
    QrEmissTMulta: TFloatField;
    QrEmissTProtesto: TDateField;
    QrEmissTDataDoc: TDateField;
    QrEmissTCtrlIni: TIntegerField;
    QrEmissTNivel: TIntegerField;
    QrEmissTVendedor: TIntegerField;
    QrEmissTAccount: TIntegerField;
    QrEmissTICMS_P: TFloatField;
    QrEmissTICMS_V: TFloatField;
    QrEmissTDuplicata: TWideStringField;
    QrEmissTDepto: TIntegerField;
    QrEmissTDescoPor: TIntegerField;
    QrEmissTDataCad: TDateField;
    QrEmissTDataAlt: TDateField;
    QrEmissTUserCad: TIntegerField;
    QrEmissTUserAlt: TIntegerField;
    QrEmissTEmitente: TWideStringField;
    QrEmissTContaCorrente: TWideStringField;
    QrEmissTCNPJCPF: TWideStringField;
    QrEmissTFatNum: TFloatField;
    QrEmissTAgencia: TIntegerField;
    DsEmissT: TDataSource;
    QrSumT: TMySQLQuery;
    QrSumTDebito: TFloatField;
    QrEmissM: TMySQLQuery;
    QrEmissMData: TDateField;
    QrEmissMTipo: TSmallintField;
    QrEmissMCarteira: TIntegerField;
    QrEmissMAutorizacao: TIntegerField;
    QrEmissMGenero: TIntegerField;
    QrEmissMDescricao: TWideStringField;
    QrEmissMNotaFiscal: TIntegerField;
    QrEmissMDebito: TFloatField;
    QrEmissMCredito: TFloatField;
    QrEmissMCompensado: TDateField;
    QrEmissMDocumento: TFloatField;
    QrEmissMSit: TIntegerField;
    QrEmissMVencimento: TDateField;
    QrEmissMLk: TIntegerField;
    QrEmissMFatID: TIntegerField;
    QrEmissMFatParcela: TIntegerField;
    QrEmissMNOMESIT: TWideStringField;
    QrEmissMNOMETIPO: TWideStringField;
    QrEmissMNOMECARTEIRA: TWideStringField;
    QrEmissMID_Sub: TSmallintField;
    QrEmissMSub: TIntegerField;
    QrEmissMFatura: TWideStringField;
    QrEmissMBanco: TIntegerField;
    QrEmissMLocal: TIntegerField;
    QrEmissMCartao: TIntegerField;
    QrEmissMLinha: TIntegerField;
    QrEmissMPago: TFloatField;
    QrEmissMMez: TIntegerField;
    QrEmissMFornecedor: TIntegerField;
    QrEmissMCliente: TIntegerField;
    QrEmissMControle: TIntegerField;
    QrEmissMID_Pgto: TIntegerField;
    QrEmissMCliInt: TIntegerField;
    QrEmissMQtde: TFloatField;
    QrEmissMFatID_Sub: TIntegerField;
    QrEmissMOperCount: TIntegerField;
    QrEmissMLancto: TIntegerField;
    QrEmissMForneceI: TIntegerField;
    QrEmissMMoraDia: TFloatField;
    QrEmissMMulta: TFloatField;
    QrEmissMProtesto: TDateField;
    QrEmissMDataDoc: TDateField;
    QrEmissMCtrlIni: TIntegerField;
    QrEmissMNivel: TIntegerField;
    QrEmissMVendedor: TIntegerField;
    QrEmissMAccount: TIntegerField;
    QrEmissMICMS_P: TFloatField;
    QrEmissMICMS_V: TFloatField;
    QrEmissMDuplicata: TWideStringField;
    QrEmissMDepto: TIntegerField;
    QrEmissMDescoPor: TIntegerField;
    QrEmissMDataCad: TDateField;
    QrEmissMDataAlt: TDateField;
    QrEmissMUserCad: TIntegerField;
    QrEmissMUserAlt: TIntegerField;
    QrEmissMEmitente: TWideStringField;
    QrEmissMContaCorrente: TWideStringField;
    QrEmissMCNPJCPF: TWideStringField;
    QrEmissMFatNum: TFloatField;
    QrEmissMAgencia: TIntegerField;
    DsEmissM: TDataSource;
    QrSumM: TMySQLQuery;
    QrSumMDebito: TFloatField;
    BtFinanceiro: TBitBtn;
    PMFinanceiro: TPopupMenu;
    MatriaPrima1: TMenuItem;
    Inclui2: TMenuItem;
    QrVSInnCabValorMP: TFloatField;
    QrVSInnCabCustoComiss: TFloatField;
    QrVSInnCabCusFrtAvMoER: TFloatField;
    QrEmissC: TMySQLQuery;
    QrEmissCData: TDateField;
    QrEmissCTipo: TSmallintField;
    QrEmissCCarteira: TIntegerField;
    QrEmissCAutorizacao: TIntegerField;
    QrEmissCGenero: TIntegerField;
    QrEmissCDescricao: TWideStringField;
    QrEmissCNotaFiscal: TIntegerField;
    QrEmissCDebito: TFloatField;
    QrEmissCCredito: TFloatField;
    QrEmissCCompensado: TDateField;
    QrEmissCDocumento: TFloatField;
    QrEmissCSit: TIntegerField;
    QrEmissCVencimento: TDateField;
    QrEmissCLk: TIntegerField;
    QrEmissCFatID: TIntegerField;
    QrEmissCFatParcela: TIntegerField;
    QrEmissCNOMESIT: TWideStringField;
    QrEmissCNOMETIPO: TWideStringField;
    QrEmissCNOMECARTEIRA: TWideStringField;
    QrEmissCID_Sub: TSmallintField;
    QrEmissCSub: TIntegerField;
    QrEmissCFatura: TWideStringField;
    QrEmissCBanco: TIntegerField;
    QrEmissCLocal: TIntegerField;
    QrEmissCCartao: TIntegerField;
    QrEmissCLinha: TIntegerField;
    QrEmissCPago: TFloatField;
    QrEmissCMez: TIntegerField;
    QrEmissCFornecedor: TIntegerField;
    QrEmissCCliente: TIntegerField;
    QrEmissCControle: TIntegerField;
    QrEmissCID_Pgto: TIntegerField;
    QrEmissCCliInt: TIntegerField;
    QrEmissCQtde: TFloatField;
    QrEmissCFatID_Sub: TIntegerField;
    QrEmissCOperCount: TIntegerField;
    QrEmissCLancto: TIntegerField;
    QrEmissCForneceI: TIntegerField;
    QrEmissCMoraDia: TFloatField;
    QrEmissCMulta: TFloatField;
    QrEmissCProtesto: TDateField;
    QrEmissCDataDoc: TDateField;
    QrEmissCCtrlIni: TIntegerField;
    QrEmissCNivel: TIntegerField;
    QrEmissCVendedor: TIntegerField;
    QrEmissCAccount: TIntegerField;
    QrEmissCICMS_P: TFloatField;
    QrEmissCICMS_V: TFloatField;
    QrEmissCDuplicata: TWideStringField;
    QrEmissCDepto: TIntegerField;
    QrEmissCDescoPor: TIntegerField;
    QrEmissCDataCad: TDateField;
    QrEmissCDataAlt: TDateField;
    QrEmissCUserCad: TIntegerField;
    QrEmissCUserAlt: TIntegerField;
    QrEmissCEmitente: TWideStringField;
    QrEmissCContaCorrente: TWideStringField;
    QrEmissCCNPJCPF: TWideStringField;
    QrEmissCFatNum: TFloatField;
    QrEmissCAgencia: TIntegerField;
    DsEmissC: TDataSource;
    QrSumC: TMySQLQuery;
    QrSumCDebito: TFloatField;
    GroupBox3: TGroupBox;
    Label39: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EdPagM: TdmkEdit;
    EdPagT: TdmkEdit;
    EdPagC: TdmkEdit;
    GroupBox7: TGroupBox;
    GridF: TDBGrid;
    GroupBox6: TGroupBox;
    GridT: TDBGrid;
    GroupBox5: TGroupBox;
    DBGrid6: TDBGrid;
    QrVSInnCabCodCliInt: TIntegerField;
    Altera2: TMenuItem;
    Exclui2: TMenuItem;
    QrEmissMQtd2: TFloatField;
    QrEmissMSerieNF: TWideStringField;
    Frete1: TMenuItem;
    Inclui3: TMenuItem;
    Altera3: TMenuItem;
    Exclui3: TMenuItem;
    DBEdit28: TDBEdit;
    Label40: TLabel;
    Comisso1: TMenuItem;
    Inclui4: TMenuItem;
    Altera4: TMenuItem;
    Exclui4: TMenuItem;
    QrEmissTQtd2: TFloatField;
    QrEmissTSerieNF: TWideStringField;
    QrEmissCQtd2: TFloatField;
    QrEmissCSerieNF: TWideStringField;
    QrEmissMGenCtb: TIntegerField;
    QrEmissCGenCtb: TIntegerField;
    QrEmissTGenCtb: TIntegerField;
    frxWET_CURTI_006_02_A: TfrxReport;
    AnliseMPAGdoIMEIselecionadoModelo2A1: TMenuItem;
    BtFiscal: TBitBtn;
    PMFiscal: TPopupMenu;
    IncluiDocumento1: TMenuItem;
    AlteraDocumento1: TMenuItem;
    ExcluiDocumento1: TMenuItem;
    N4: TMenuItem;
    IncluiItemdodocumento1: TMenuItem;
    AlteraoItemselecionadododocumento1: TMenuItem;
    ExcluioItemselecionadododocumento1: TMenuItem;
    PCBottom: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel7: TPanel;
    Splitter4: TSplitter;
    DBGrid3: TDBGrid;
    GroupBox2: TGroupBox;
    Splitter5: TSplitter;
    DBGBaixa: TDBGrid;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    QrVSInnItsGrandeza: TSmallintField;
    QrGraGru1: TMySQLQuery;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrVSInnCabUF_EMPRESA: TSmallintField;
    QrVSInnCabUF_FORNECE: TSmallintField;
    N5: TMenuItem;
    otalizarfisicopeloescritural1: TMenuItem;
    QrSumNFsInn: TMySQLQuery;
    QrSumNFsInnPecas: TFloatField;
    QrSumNFsInnPesoKg: TFloatField;
    QrSumNFsInnValorT: TFloatField;
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabNO_TER: TWideStringField;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    QrEfdInnNFsCabCliInt: TIntegerField;
    DsEfdInnNFsCab: TDataSource;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField;
    QrEfdInnNFsItsGerBxaEstq: TSmallintField;
    QrEfdInnNFsItsNCM: TWideStringField;
    QrEfdInnNFsItsUnidMed: TIntegerField;
    QrEfdInnNFsItsEx_TIPI: TWideStringField;
    QrEfdInnNFsItsGrandeza: TSmallintField;
    QrEfdInnNFsItsTipo_Item: TSmallintField;
    DsEfdInnNFsIts: TDataSource;
    DBGInnNFsIts: TDBGrid;
    QrEfdInnCTsCab: TMySQLQuery;
    QrEfdInnCTsCabMovFatID: TIntegerField;
    QrEfdInnCTsCabMovFatNum: TIntegerField;
    QrEfdInnCTsCabMovimCod: TIntegerField;
    QrEfdInnCTsCabEmpresa: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrEfdInnCTsCabIsLinked: TSmallintField;
    QrEfdInnCTsCabSqLinked: TIntegerField;
    QrEfdInnCTsCabCliInt: TIntegerField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabPecas: TFloatField;
    QrEfdInnCTsCabPesoKg: TFloatField;
    QrEfdInnCTsCabAreaM2: TFloatField;
    QrEfdInnCTsCabAreaP2: TFloatField;
    QrEfdInnCTsCabValorT: TFloatField;
    QrEfdInnCTsCabMotorista: TIntegerField;
    QrEfdInnCTsCabPlaca: TWideStringField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabNFe_FatID: TIntegerField;
    QrEfdInnCTsCabNFe_FatNum: TIntegerField;
    QrEfdInnCTsCabNFe_StaLnk: TSmallintField;
    QrEfdInnCTsCabVSVmcWrn: TSmallintField;
    QrEfdInnCTsCabVSVmcObs: TWideStringField;
    QrEfdInnCTsCabVSVmcSeq: TWideStringField;
    QrEfdInnCTsCabVSVmcSta: TSmallintField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEfdInnCTsCabNO_TER: TWideStringField;
    DsEfdInnCTsCab: TDataSource;
    TabSheet9: TTabSheet;
    DBGrid7: TDBGrid;
    DBGInnNFsCab: TDBGrid;
    BtCTe: TBitBtn;
    PMCTe: TPopupMenu;
    IncluiConhecimentodefrete1: TMenuItem;
    AlteraConhecimentodefrete1: TMenuItem;
    ExcluiConhecimentodefrete1: TMenuItem;
    QrEmissCCentroCusto: TIntegerField;
    QrEmissMCentroCusto: TIntegerField;
    QrEmissTCentroCusto: TIntegerField;
    QrEmissCGenCtbD: TIntegerField;
    QrEmissCGenCtbC: TIntegerField;
    QrEmissMGenCtbD: TIntegerField;
    QrEmissMGenCtbC: TIntegerField;
    QrEmissTGenCtbD: TIntegerField;
    QrEmissTGenCtbC: TIntegerField;
    Splitter6: TSplitter;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCST_PIS: TWideStringField;
    QrEfdInnNFsItsCST_COFINS: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TWideStringField;
    QrEfdInnNFsCabRegrFiscal: TIntegerField;
    QrEfdInnNFsCabTpEntrd: TIntegerField;
    QrFornecedorNOME_E_DOC_ENTIDADE: TWideStringField;
    QrNFeItens: TMySQLQuery;
    QrNFeItensFatID: TIntegerField;
    QrNFeItensFatNum: TIntegerField;
    QrNFeItensEmpresa: TIntegerField;
    QrNFeItensnItem: TIntegerField;
    QrNFeItensprod_cProd: TWideStringField;
    QrNFeItensprod_cEAN: TWideStringField;
    QrNFeItensprod_cBarra: TWideStringField;
    QrNFeItensprod_xProd: TWideStringField;
    QrNFeItensprod_NCM: TWideStringField;
    QrNFeItensprod_CEST: TIntegerField;
    QrNFeItensprod_indEscala: TWideStringField;
    QrNFeItensprod_CNPJFab: TWideStringField;
    QrNFeItensprod_cBenef: TWideStringField;
    QrNFeItensprod_EXTIPI: TWideStringField;
    QrNFeItensprod_genero: TSmallintField;
    QrNFeItensprod_CFOP: TIntegerField;
    QrNFeItensprod_uCom: TWideStringField;
    QrNFeItensprod_qCom: TFloatField;
    QrNFeItensprod_vUnCom: TFloatField;
    QrNFeItensprod_vProd: TFloatField;
    QrNFeItensprod_cEANTrib: TWideStringField;
    QrNFeItensprod_cBarraTrib: TWideStringField;
    QrNFeItensprod_uTrib: TWideStringField;
    QrNFeItensprod_qTrib: TFloatField;
    QrNFeItensprod_vUnTrib: TFloatField;
    QrNFeItensprod_vFrete: TFloatField;
    QrNFeItensprod_vSeg: TFloatField;
    QrNFeItensprod_vDesc: TFloatField;
    QrNFeItensprod_vOutro: TFloatField;
    QrNFeItensprod_indTot: TSmallintField;
    QrNFeItensprod_xPed: TWideStringField;
    QrNFeItensprod_nItemPed: TIntegerField;
    QrNFeItensTem_IPI: TSmallintField;
    QrNFeItens_Ativo_: TSmallintField;
    QrNFeItensInfAdCuztm: TIntegerField;
    QrNFeItensEhServico: TIntegerField;
    QrNFeItensUsaSubsTrib: TSmallintField;
    QrNFeItensICMSRec_pRedBC: TFloatField;
    QrNFeItensICMSRec_vBC: TFloatField;
    QrNFeItensICMSRec_pAliq: TFloatField;
    QrNFeItensICMSRec_vICMS: TFloatField;
    QrNFeItensIPIRec_pRedBC: TFloatField;
    QrNFeItensIPIRec_vBC: TFloatField;
    QrNFeItensIPIRec_pAliq: TFloatField;
    QrNFeItensIPIRec_vIPI: TFloatField;
    QrNFeItensPISRec_pRedBC: TFloatField;
    QrNFeItensPISRec_vBC: TFloatField;
    QrNFeItensPISRec_pAliq: TFloatField;
    QrNFeItensPISRec_vPIS: TFloatField;
    QrNFeItensCOFINSRec_pRedBC: TFloatField;
    QrNFeItensCOFINSRec_vBC: TFloatField;
    QrNFeItensCOFINSRec_pAliq: TFloatField;
    QrNFeItensCOFINSRec_vCOFINS: TFloatField;
    QrNFeItensMeuID: TIntegerField;
    QrNFeItensNivel1: TIntegerField;
    QrNFeItensGraGruX: TIntegerField;
    QrNFeItensUnidMedCom: TIntegerField;
    QrNFeItensUnidMedTrib: TIntegerField;
    QrNFeItensICMSRec_vBCST: TFloatField;
    QrNFeItensICMSRec_vICMSST: TFloatField;
    QrNFeItensICMSRec_pAliqST: TFloatField;
    QrNFeItensTem_II: TSmallintField;
    QrNFeItensprod_nFCI: TWideStringField;
    QrNFeItensStqMovValA: TIntegerField;
    QrNFeItensAtrelaID: TIntegerField;
    QrNFeItensICMS_Orig: TSmallintField;
    QrNFeItensICMS_CST: TSmallintField;
    QrNFeItensICMS_vBC: TFloatField;
    QrNFeItensICMS_pICMS: TFloatField;
    QrNFeItensICMS_vICMS: TFloatField;
    QrNFeItensICMS_vBCST: TFloatField;
    QrNFeItensICMS_pICMSST: TFloatField;
    QrNFeItensICMS_vICMSST: TFloatField;
    QrNFeItensIPI_cEnq: TWideStringField;
    QrNFeItensIND_APUR: TWideStringField;
    QrNFeItensIPI_CST: TSmallintField;
    QrNFeItensIPI_vBC: TFloatField;
    QrNFeItensIPI_pIPI: TFloatField;
    QrNFeItensIPI_vIPI: TFloatField;
    QrNFeItensPIS_CST: TSmallintField;
    QrNFeItensPIS_vBC: TFloatField;
    QrNFeItensPIS_pPIS: TFloatField;
    QrNFeItensPIS_vPIS: TFloatField;
    QrNFeItensPISST_vBC: TFloatField;
    QrNFeItensPISST_pPIS: TFloatField;
    QrNFeItensPISST_vPIS: TFloatField;
    QrNFeItensCOFINS_CST: TSmallintField;
    QrNFeItensCOFINS_vBC: TFloatField;
    QrNFeItensCOFINS_pCOFINS: TFloatField;
    QrNFeItensCOFINS_vCOFINS: TFloatField;
    QrNFeItensCOFINSST_vBC: TFloatField;
    QrNFeItensCOFINSST_pCOFINS: TFloatField;
    QrNFeItensCOFINSST_vCOFINS: TFloatField;
    QrVSInnItsRpICMS: TFloatField;
    QrVSInnItsRpICMSST: TFloatField;
    QrVSInnItsRpPIS: TFloatField;
    QrVSInnItsRpCOFINS: TFloatField;
    QrVSInnItsRvICMS: TFloatField;
    QrVSInnItsRvICMSST: TFloatField;
    QrVSInnItsRvPIS: TFloatField;
    QrVSInnItsRvCOFINS: TFloatField;
    QrVSInnItsRpIPI: TFloatField;
    QrVSInnItsRvIPI: TFloatField;
    QrEfdInnCTsCabRegrFiscal: TIntegerField;
    QrEfdInnCTsCabIND_NAT_FRT: TWideStringField;
    QrEfdInnCTsCabVL_ITEM: TFloatField;
    QrEfdInnCTsCabCST_PIS: TWideStringField;
    QrEfdInnCTsCabNAT_BC_CRED: TWideStringField;
    QrEfdInnCTsCabVL_BC_PIS: TFloatField;
    QrEfdInnCTsCabALIQ_PIS: TFloatField;
    QrEfdInnCTsCabVL_PIS: TFloatField;
    QrEfdInnCTsCabCST_COFINS: TWideStringField;
    QrEfdInnCTsCabVL_BC_COFINS: TFloatField;
    QrEfdInnCTsCabALIQ_COFINS: TFloatField;
    QrEfdInnCTsCabVL_COFINS: TFloatField;
    QrNFeItensIND_MOV: TSmallintField;
    QrVSItsBxaCustoKg: TFloatField;
    QrVSInnItsCustoKg: TFloatField;
    QrVSSubPrdItsSeqSifDipoa: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSInnCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSInnCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSInnCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSInnCabBeforeClose(DataSet: TDataSet);
    procedure QrVSInnItsBeforeClose(DataSet: TDataSet);
    procedure QrVSInnItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure QrVSItsBxaBeforeClose(DataSet: TDataSet);
    procedure QrVSItsBxaAfterScroll(DataSet: TDataSet);
    procedure AnliseMPAG1Click(Sender: TObject);
    procedure frxWET_CURTI_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichaRMP1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure IncluiSubProduto1Click(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure BtSubProdutoClick(Sender: TObject);
    procedure PMSubProdutoPopup(Sender: TObject);
    procedure AlteraSubProduto1Click(Sender: TObject);
    procedure ExcluiSubProduto1Click(Sender: TObject);
    procedure IncluiTributo1Click(Sender: TObject);
    procedure AlteraTributoAtual1Click(Sender: TObject);
    procedure ExcluiTributoAtual1Click(Sender: TObject);
    procedure PMTribIncItsPopup(Sender: TObject);
    procedure BtTribIncItsClick(Sender: TObject);
    procedure Resultados1Click(Sender: TObject);
    procedure Edide_nNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edemi_nNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure IncluiNFe1Click(Sender: TObject);
    procedure AlteraNFe1Click(Sender: TObject);
    procedure ExcluiNFe1Click(Sender: TObject);
    procedure AnliseMPAG2Click(Sender: TObject);
    procedure Encerrarendimento1Click(Sender: TObject);
    procedure QrReceitasCalcFields(DataSet: TDataSet);
    procedure Corrigepesodebaixas1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure AtrelamentoFreteCompra1Click(Sender: TObject);
    procedure AtrelamentoNFsdeMO1Click(Sender: TObject);
    procedure MapadeDefeitos1Click(Sender: TObject);
    procedure AnliseMPAGdoIMEIselecionadoModelo1A1Click(Sender: TObject);
    procedure ClassesGeradas1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure BtFinanceiroClick(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure TPDtCompraRedefInPlace(Sender: TObject);
    procedure Inclui3Click(Sender: TObject);
    procedure Altera3Click(Sender: TObject);
    procedure Exclui3Click(Sender: TObject);
    procedure QrEmissTCalcFields(DataSet: TDataSet);
    procedure QrEmissMCalcFields(DataSet: TDataSet);
    procedure QrEmissCCalcFields(DataSet: TDataSet);
    procedure Inclui4Click(Sender: TObject);
    procedure Altera4Click(Sender: TObject);
    procedure Exclui4Click(Sender: TObject);
    procedure AnliseMPAGdoIMEIselecionadoModelo2A1Click(Sender: TObject);
    procedure BtFiscalClick(Sender: TObject);
    procedure IncluiDocumento1Click(Sender: TObject);
    procedure AlteraDocumento1Click(Sender: TObject);
    procedure ExcluiDocumento1Click(Sender: TObject);
    procedure PMFiscalPopup(Sender: TObject);
    procedure IncluiItemdodocumento1Click(Sender: TObject);
    procedure AlteraoItemselecionadododocumento1Click(Sender: TObject);
    procedure ExcluioItemselecionadododocumento1Click(Sender: TObject);
    procedure QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
    procedure QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
    procedure DBGInnNFsCabEnter(Sender: TObject);
    procedure otalizarfisicopeloescritural1Click(Sender: TObject);
    procedure BtCTeClick(Sender: TObject);
    procedure IncluiConhecimentodefrete1Click(Sender: TObject);
    procedure AlteraConhecimentodefrete1Click(Sender: TObject);
    procedure ExcluiConhecimentodefrete1Click(Sender: TObject);
    procedure PMCTePopup(Sender: TObject);
    procedure DBGBaixaDblClick(Sender: TObject);
  private
    { Private declarations }
    FMPAG_Artigos: String;
    FTabLctA: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSInnIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure MostraFormVSHisFchAdd(SQLType: TSQLType);
    procedure ReopenTribIncIts(Controle: Integer);
    procedure MostraFormVSInnNFs(SQLType: TSQLType);
    // ini EfdInnNFs
    procedure MostraFormEfdInnNFsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnNFsIts(SQLType: TSQLType; UsaNFeItens: Boolean = False);
    procedure ReopenEFdInnNFsCab(Controle: Integer);
    procedure ReopenEfdInnNFsIts(Conta: Integer);
    // fim EfdInnNFs
    // ini EfdInnCTs
    procedure MostraFormEfdInnCTsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnCTsIts(SQLType: TSQLType);
    procedure ReopenEFdInnCTsCab(Controle: Integer);
    // fim EfdInnCTs


    //Em desenvolvimento procedure ImportaDadosNFeCab(EdnNF: TDmkEdit);
    procedure Encerrarendimento();
    procedure ImprimeAmaliseMPAGModelo1(SemGelatina: Boolean);
    procedure DefineVarDup();
    function  CalculaDiferencas(tPag: TAquemPag): Double;
    procedure SubQuery1Reopen();
    procedure ReopenEmissM(Controle: Integer);
    procedure ReopenEmissT(Controle: Integer);
    procedure ReopenEmissC(Controle: Integer);
    //
    procedure PreparaModelo2X();

  public
    { Public declarations }
    FSeq, FCabIni, FEmpresa: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaSaldoIts(Controle: Integer);
    procedure AtualizaNFeItens();
    procedure ReopenLookupRend();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure ReopenVSItsBxa(Controle: Integer);
    procedure ReopenVSHisFch(Codigo: Integer);
    procedure ReopenVSSubPrdIts(Controle: Integer);
    procedure ReopenVSInnNFs(Conta: Integer);

  end;

var
  FmVSInnCab: TFmVSInnCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSInnIts, ModuleGeral,
{$IfDef sAllVS} VSMovImp, VSHisFchAdd , UnTributos_PF, VSInnNFs, UnVS_PF,{$EndIf}
  ModVS_CRC, Principal, UnVS_CRC_PF, UnVS_Jan, UnFinanceiro, UnPagtos,
  UnEmpresas, EfdInnNFsCab, EfdInnNFsIts, ModProd, ModuleNFe_0000, EfdInnCTsCab,
  UnSPED_PF;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_1003;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSInnCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSInnCab.MapadeDefeitos1Click(Sender: TObject);
begin
  VS_Jan.ImprimeVSImpMapaDefei(QrVSInnItsSerieFch.Value, QrVSInnItsFicha.Value);
end;

procedure TFmVSInnCab.MostraFormVSHisFchAdd(SQLType: TSQLType);
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSHisFchAdd, FmVSHisFchAdd, afmoNegarComAviso) then
  begin
    FmVSHisFchAdd.ImgTipo.SQLType := SQLType;
    FmVSHisFchAdd.FQrIts                  := QrVSHisFch;
    FmVSHisFchAdd.EdVSMovIts.ValueVariant := QrVSInnItsControle.Value;
    FmVSHisFchAdd.EdSerieFch.ValueVariant := QrVSInnItsSerieFch.Value;
    FmVSHisFchAdd.EdFicha.ValueVariant    := QrVSInnItsFicha.Value;
    if SQLType = stUpd then
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := QrVSHisFchCodigo.Value;
      FmVSHisFchAdd.TPDataHora.Date       := QrVSHisFchDataHora.Value;
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(QrVSHisFchDataHora.Value, 100);
      FmVSHisFchAdd.EdNome.Text           := QrVSHisFchNome.Value;
      FmVSHisFchAdd.MeObserv.Text         := QrVSHisFchObserv.Value;
    end else
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := 0;
      FmVSHisFchAdd.TPDataHora.Date       := DModG.ObtemAgora();
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(DModG.ObtemAgora(), 100);
      FmVSHisFchAdd.EdNome.Text           := '';
      FmVSHisFchAdd.MeObserv.Text         := '';
    end;
    FmVSHisFchAdd.ShowModal;
    FmVSHisFchAdd.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.MostraFormVSInnIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSInnIts, FmVSInnIts, afmoNegarComAviso) then
  begin
    FmVSInnIts.ImgTipo.SQLType := SQLType;
    FmVSInnIts.FQrCab          := QrVSInnCab;
    FmVSInnIts.FDsCab          := DsVSInnCab;
    FmVSInnIts.FQrIts          := QrVSInnIts;
    FmVSInnIts.FQrTribIncIts   := QrTribIncIts;
    FmVSInnIts.FEmpresa        := QrVSInnCabEmpresa.Value;
    FmVSInnIts.FFornece        := QrVSInnCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmVSInnIts.EdCPF1.ReadOnly := False
      FmVSInnIts.FDataHora       := QrVSInnCabDtEntrada.Value;
      //
      if QrVSInnCabClienteMO.Value <> 0 then
        FmVSInnIts.FClientMO := QrVSInnCabClienteMO.Value
      else
        FmVSInnIts.FClientMO := QrVSInnCabEmpresa.Value;
      //
      FmVSInnIts.EdRpICMS.ValueVariant   := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
      FmVSInnIts.EdRpPIS.ValueVariant    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
      FmVSInnIts.EdRpCOFINS.ValueVariant := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
      FmVSInnIts.EdRpIPI.ValueVariant    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
    end else
    begin
      FmVSInnIts.EdControle.ValueVariant := QrVSInnItsControle.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSInnItsDataHora.Value,
        QrVSInnItsDtCorrApo.Value,  FmVSInnIts.FDataHora);
      //
      FmVSInnIts.FClientMO               := QrVSInnItsClientMO.Value;
      FmVSInnIts.EdGragruX.ValueVariant  := QrVSInnItsGraGruX.Value;
      FmVSInnIts.CBGragruX.KeyValue      := QrVSInnItsGraGruX.Value;
      FmVSInnIts.EdPecas.ValueVariant    := QrVSInnItsPecas.Value;
      FmVSInnIts.EdPesoKg.ValueVariant   := QrVSInnItsPesoKg.Value;
      FmVSInnIts.EdValorMP.ValueVariant  := QrVSInnItsValorMP.Value;
      //
      FmVSInnIts.EdCusKgComiss.ValueVariant     := QrVSInnItsCusKgComiss.Value;
      FmVSInnIts.EdCustoComiss.ValueVariant     := QrVSInnItsCustoComiss.Value;
      FmVSInnIts.EdCredPereImposto.ValueVariant := QrVSInnItsCredPereImposto.Value;
      FmVSInnIts.EdCredValrImposto.ValueVariant := QrVSInnItsCredValrImposto.Value;
      FmVSInnIts.EdCusFrtAvuls.ValueVariant     := QrVSInnItsCusFrtAvuls.Value;
      //
      FmVSInnIts.EdValorT.ValueVariant          := QrVSInnItsValorT.Value;
      //
      FmVSInnIts.EdObserv.ValueVariant   := QrVSInnItsObserv.Value;
      FmVSInnIts.EdSerieFch.ValueVariant := QrVSInnItsSerieFch.Value;
      FmVSInnIts.CBSerieFch.KeyValue     := QrVSInnItsSerieFch.Value;
      FmVSInnIts.EdFicha.ValueVariant    := QrVSInnItsFicha.Value;
      FmVSInnIts.EdMarca.ValueVariant    := QrVSInnItsMarca.Value;
      //FmVSInnIts.RGMisturou.ItemIndex    := QrVSInnItsMisturou.Value;
      //
      if QrVSInnItsSdoVrtPeca.Value < QrVSInnItsPecas.Value then
      begin
        FmVSInnIts.EdGraGruX.Enabled  := False;
        FmVSInnIts.CBGraGruX.Enabled  := False;
        FmVSInnIts.EdSerieFch.Enabled := False;
        FmVSInnIts.CBSerieFch.Enabled := False;
        FmVSInnIts.EdFicha.Enabled    := False;
      end;
      //
      FmVSInnIts.EdInfPecas.ValueVariant    := QrVSMovDifInfPecas.Value;
      FmVSInnIts.EdInfPesoKg.ValueVariant   := QrVSMovDifInfPesoKg.Value;
      FmVSInnIts.EdInfValorT.ValueVariant   := QrVSMovDifInfValorT.Value;
      //
      FmVSInnIts.EdPerQbrViag.ValueVariant  := QrVSMovDifPerQbrViag.Value;
      FmVSInnIts.EdPerQbrSal.ValueVariant   := QrVSMovDifPerQbrSal.Value;
      FmVSInnIts.EdPesoSalKg.ValueVariant   := QrVSMovDifPesoSalKg.Value;
      //
      FmVSInnIts.EdStqCenLoc.ValueVariant   := QrVSInnItsStqCenLoc.Value;
      FmVSInnIts.EdReqMovEstq.ValueVariant  := QrVSInnItsReqMovEstq.Value;

      FmVSInnIts.EdRstCouPc.ValueVariant    := QrVSMovDifRstCouPc.Value;
      FmVSInnIts.EdRstCouKg.ValueVariant    := QrVSMovDifRstCouKg.Value;
      FmVSInnIts.EdRstCouVl.ValueVariant    := QrVSMovDifRstCouVl.Value;
      FmVSInnIts.EdRstSalKg.ValueVariant    := QrVSMovDifRstSalKg.Value;
      FmVSInnIts.EdRstSalVl.ValueVariant    := QrVSMovDifRstSalVl.Value;
      FmVSInnIts.EdRstTotVl.ValueVariant    := QrVSMovDifRstTotVl.Value;

      FmVSInnIts.EdTribDefSel.ValueVariant  := QrVSMovDifTribDefSel.Value;
      FmVSInnIts.CBTribDefSel.KeyValue      := QrVSMovDifTribDefSel.Value;
      FmVSInnIts.LaTribDefSel.Enabled       := False;
      FmVSInnIts.EdTribDefSel.Enabled       := False;
      FmVSInnIts.CBTribDefSel.Enabled       := False;
      //
      FmVSInnIts.EdCustoMOKg.ValueVariant   := QrVSInnItsCustoMOKg.Value;
      FmVSInnIts.EdCustoMOTot.ValueVariant  := QrVSInnItsCustoMOTot.Value;
      //
      FmVSInnIts.EdRpICMS.ValueVariant    := QrVSInnItsRpICMS.Value;
      FmVSInnIts.EdRvICMS.ValueVariant    := QrVSInnItsRvICMS.Value;
      FmVSInnIts.EdRpPIS.ValueVariant     := QrVSInnItsRpPIS.Value;
      FmVSInnIts.EdRvPIS.ValueVariant     := QrVSInnItsRvPIS.Value;
      FmVSInnIts.EdRpCOFINS.ValueVariant  := QrVSInnItsRpCOFINS.Value;
      FmVSInnIts.EdRvCOFINS.ValueVariant  := QrVSInnItsRvCOFINS.Value;
      FmVSInnIts.EdRpIPI.ValueVariant     := QrVSInnItsRpIPI.Value;
      FmVSInnIts.EdRvIPI.ValueVariant     := QrVSInnItsRvIPI.Value;

    end;
    FmVSInnIts.ShowModal;
    FmVSInnIts.Destroy;
  end;
end;

procedure TFmVSInnCab.MostraFormVSInnNFs(SQLType: TSQLType);
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSInnNFs, FmVSInnNFs, afmoNegarComAviso) then
  begin
    FmVSInnNFs.ImgTipo.SQLType := SQLType;
    FmVSInnNFs.FQrIts                  := QrVSInnNFs;
    FmVSInnNFs.EdCodigo.ValueVariant   := QrVSInnCabCodigo.Value;
    FmVSInnNFs.EdControle.ValueVariant := QrVSInnItsControle.Value;
    //FmVSInnNFs.EdSerieFch.ValueVariant := QrVSInnItsSerieFch.Value;
    //FmVSInnNFs.EdFicha.ValueVariant    := QrVSInnItsFicha.Value;
    if SQLType = stUpd then
    begin
      FmVSInnNFs.EdConta.ValueVariant     := QrVSInnNFsConta.Value;
      FmVSInnNFs.EdPecas.ValueVariant     := QrVSInnNFsPecas.Value;
      FmVSInnNFs.EdPesoKg.ValueVariant    := QrVSInnNFsPesoKg.Value;
      FmVSInnNFs.EdValorT.ValueVariant    := QrVSInnNFsValorT.Value;
      FmVSInnNFs.EdMotorista.ValueVariant := QrVSInnNFsMotorista.Value;
      FmVSInnNFs.CBMotorista.KeyValue     := QrVSInnNFsMotorista.Value;
      FmVSInnNFs.EdPlaca.ValueVariant     := QrVSInnNFsPlaca.Value;
      FmVSInnNFs.Edide_serie.ValueVariant := QrVSInnNFside_serie.Value;
      FmVSInnNFs.Edide_nNF.ValueVariant   := QrVSInnNFside_nNF.Value;
      FmVSInnNFs.Edemi_serie.ValueVariant := QrVSInnNFsemi_serie.Value;
      FmVSInnNFs.Edemi_nNF.ValueVariant   := QrVSInnNFsemi_nNF.Value;
    end else
    begin
      //
    end;
    FmVSInnNFs.ShowModal;
    FmVSInnNFs.Destroy;
    //
    MostraFormVSInnIts(stUpd);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.otalizarfisicopeloescritural1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumNFsInn, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(ValorT) ValorT ',
  'FROM efdinnnfscab ',
  'WHERE MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND MovFatNum=' + Geral.FF0(QrVSInnCabCodigo.Value),
  '']);
  //

end;

procedure TFmVSInnCab.MostraFormEfdInnCTsCab(SQLType: TSQLType);
var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnCTsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnCTsCab, FmEfdInnCTsCab, afmoNegarComAviso) then
  begin
    FmEfdInnCTsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnCTsCab.FQrIts                         := QrEfdInnCTsCab;
    FmEfdInnCTsCab.EdMovFatID.ValueVariant        := FEFDInnNFsMainFatID;
    FmEfdInnCTsCab.EdMovFatNum.ValueVariant       := QrVSInnCabCodigo.Value;
    FmEfdInnCTsCab.EdMovimCod.ValueVariant        := QrVSInnCabMovimCod.Value;
    FmEfdInnCTsCab.EdEmpresa.ValueVariant         := QrVSInnCabEmpresa.Value;
    //FmEfdInnCTsCab.EdSerieFch.ValueVariant        := QrPQEItsSerieFch.Value;
    //FmEfdInnCTsCab.EdFicha.ValueVariant           := QrPQEItsFicha.Value;
    FmEfdInnCTsCab.EdControle.ValueVariant           := 0;
    if (SQLType = stIns) and (QrVSInnCab.RecordCount > 0) then
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := 0;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := QrVSInnCabPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrVSInnCabPesoKg.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdValorT.ValueVariant        := QrVSInnCabValorT.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := 0;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := 0;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := '';
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := '57';
      (*if QrPQECI.Value = QrPQEEmpresa.Value then
      begin*)
        FmEfdInnCTsCab.EdSER.ValueVariant           := 0; //QrVSInnCabCTe_serie.Value;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := 0; //QrVSInnCabConhecimento.Value;
        FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := ''; //QrVSInnCabCte_Id.Value;
      (*end else
      begin
        FmEfdInnCTsCab.EdSER.ValueVariant           := 0;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := ?;
        FmEfdInnCTsCab.EdCHV_NFE.ValueVariant       := '';
      end;*)
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrVSInnCabDtCompra.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrVSInnCabDtEntrada.Value;

      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrVSInnCabClienteMO.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrVSInnCabClienteMO.Value;

      FmEfdInnCTsCab.EdTransportador.ValueVariant    := QrVSInnCabTransporta.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue        := QrVSInnCabTransporta.Value;

      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := QrVSInnCabCusFrtAvMoER.Value;
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnCTsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := QrVSInnCabCusFrtAvMoER.Value;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant      := QrVSInnCabCusFrtAvMoER.Value;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := 0.00;
      //FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnCTsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnCTsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnCTsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnCTsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnCTsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnCTsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := QrEfdInnCTsCabControle.Value;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := QrEfdInnCTsCabPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrEfdInnCTsCabPesoKg.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := QrEfdInnCTsCabAreaM2.Value;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := QrEfdInnCTsCabAreaP2.Value;
      FmEfdInnCTsCab.EdValorT.ValueVariant        := QrEfdInnCTsCabValorT.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := QrEfdInnCTsCabPlaca.Value;
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant     := QrVSInnCTsCEdNFeStaab.Value;
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrEfdInnCTsCabDT_DOC.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrEfdInnCTsCabDT_A_P.Value;
      //
      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrEfdInnCTsCabCliInt.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrEfdInnCTsCabCliInt.Value;
      //
      FmEfdInnCTsCab.EdTransportador.ValueVariant := QrEfdInnCTsCabTerceiro.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue     := QrEfdInnCTsCabTerceiro.Value;
      //
      FmEfdInnCTsCab.EdCFOP.ValueVariant          := QrEfdInnCTsCabCFOP.Value;
      FmEfdInnCTsCab.CBCFOP.KeyValue              := QrEfdInnCTsCabCFOP.Value;
      //
      FmEfdInnCTsCab.EdICMS_CST.ValueVariant      := QrEfdInnCTsCabCST_ICMS.Value;
      //
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := QrEfdInnCTsCabIND_PGTO.Value;
      FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := QrEfdInnCTsCabIND_FRT.Value;
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := QrEfdInnCTsCabVL_SERV.Value;
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := QrEfdInnCTsCabVL_DESC.Value;
      FmEfdInnCTsCab.EdVL_NT.ValueVariant         := QrEfdInnCTsCabVL_NT.Value;  // Abatimento n�o comercia


      FmEfdInnCTsCab.EdVL_RED_BC.ValueVariant     := QrEfdInnCTsCabVL_RED_BC.Value;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnCTsCabVL_BC_ICMS.Value;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := QrEfdInnCTsCabALIQ_ICMS.Value;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := QrEfdInnCTsCabVL_ICMS.Value;
      FmEfdInnCTsCab.EdVL_OPR.ValueVariant        := QrEfdInnCTsCabVL_OPR.Value;
      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := QrEfdInnCTsCabVL_DOC.Value;

      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := QrEfdInnCTsCabCOD_MOD.Value;
      FmEfdInnCTsCab.EdSER.ValueVariant           := QrEfdInnCTsCabSER.Value;
      FmEfdInnCTsCab.EdSUB.ValueVariant           := QrEfdInnCTsCabSUB.Value;
      FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := QrEfdInnCTsCabNUM_DOC.Value;
      FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := QrEfdInnCTsCabCHV_CTE.Value;

      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;

      FmEfdInnCTsCab.EdTP_CT_e.ValueVariant       := QrEfdInnCTsCabTP_CT_e.Value;


      FmEfdInnCTsCab.EdCOD_MUN_ORIG.ValueVariant  := QrEfdInnCTsCabCOD_MUN_ORIG.Value;
      FmEfdInnCTsCab.CBCOD_MUN_ORIG.KeyValue      := QrEfdInnCTsCabCOD_MUN_ORIG.Value;

      FmEfdInnCTsCab.EdCOD_MUN_DEST.ValueVariant  := QrEfdInnCTsCabCOD_MUN_DEST.Value;
      FmEfdInnCTsCab.CBCOD_MUN_DEST.KeyValue      := QrEfdInnCTsCabCOD_MUN_DEST.Value;

      FmEfdInnCTsCab.EdRegrFiscal.ValueVariant  := QrEfdInnCTsCabRegrFiscal.Value;
      FmEfdInnCTsCab.CBRegrFiscal.KeyValue      := QrEfdInnCTsCabRegrFiscal.Value;


      FmEfdInnCTsCab.EdIND_NAT_FRT.ValueVariant     := QrEfdInnCTsCabIND_NAT_FRT.Value;
      //FmEfdInnCTsCab.EdVL_ITEM.ValueVariant         := QrEfdInnCTsCabVL_ITEM.Value;
      FmEfdInnCTsCab.EdCST_PIS.ValueVariant         := QrEfdInnCTsCabCST_PIS.Value;
      FmEfdInnCTsCab.EdNAT_BC_CRED.ValueVariant     := QrEfdInnCTsCabNAT_BC_CRED.Value;
      FmEfdInnCTsCab.EdVL_BC_PIS.ValueVariant       := QrEfdInnCTsCabVL_BC_PIS.Value;
      FmEfdInnCTsCab.EdALIQ_PIS.ValueVariant        := QrEfdInnCTsCabALIQ_PIS.Value;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant          := QrEfdInnCTsCabVL_PIS.Value;
      FmEfdInnCTsCab.EdCST_COFINS.ValueVariant      := QrEfdInnCTsCabCST_COFINS.Value;
      FmEfdInnCTsCab.EdVL_BC_COFINS.ValueVariant    := QrEfdInnCTsCabVL_BC_COFINS.Value;
      FmEfdInnCTsCab.EdALIQ_COFINS.ValueVariant     := QrEfdInnCTsCabALIQ_COFINS.Value;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant       := QrEfdInnCTsCabVL_COFINS.Value;

   end;
    FmEfdInnCTsCab.ShowModal;
    NewControle := FmEfdInnCTsCab.FControle;
    FmEfdInnCTsCab.Destroy;
    //
    ReopenEfdInnCTsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnCTsCabControle.Value = NewControle then
        MostraFormEfdInnCTsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.MostraFormEfdInnCTsIts(SQLType: TSQLType);
begin
// Fazer no futuro?
end;

procedure TFmVSInnCab.MostraFormEfdInnNFsCab(SQLType: TSQLType);
var
  NewControle, Controle: Integer;
  InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT: Double;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnNFsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsCab, FmEfdInnNFsCab, afmoNegarComAviso) then
  begin
    FmEfdInnNFsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsCab.FQrIts                         := QrEfdInnNFsCab;
    FmEfdInnNFsCab.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsCab.EdMovFatNum.ValueVariant       := QrVSInnCabCodigo.Value;
    FmEfdInnNFsCab.EdMovimCod.ValueVariant        := QrVSInnCabMovimCod.Value;
    FmEfdInnNFsCab.EdEmpresa.ValueVariant         := QrVSInnCabEmpresa.Value;
    FmEfdInnNFsCab.FFornecedor                    := QrVSInnCabFornecedor.Value;
    //FmEfdInnNFsCab.EdSerieFch.ValueVariant        := QrVSInnItsSerieFch.Value;
    //FmEfdInnNFsCab.EdFicha.ValueVariant           := QrVSInnItsFicha.Value;
    FmEfdInnNFsCab.EdControle.ValueVariant           := 0;
    if (SQLType = stIns) and (QrVSInnIts.RecordCount > 0) then
    begin
      FmEfdInnNFsCab.EdControle.ValueVariant      := 0;
      InfPecas  := 0;
      InfPesoKg := 0;
      InfAreaM2 := 0;
      InfAreaP2 := 0;
      InfValorT := 0;
      if QrEfdInnNFsCab.State <> dsInactive then
      begin
        QrEfdInnNFsCab.First;
        while not QrEfdInnNFsCab.Eof do
        begin
          InfPecas  := InfPecas  + QrEfdInnNFsCabPecas.Value;
          InfPesoKg := InfPesoKg + QrEfdInnNFsCabPesoKg.Value;
          InfAreaM2 := InfAreaM2 + QrEfdInnNFsCabAreaM2.Value;
          InfAreaP2 := InfAreaP2 + QrEfdInnNFsCabAreaP2.Value;
          InfValorT := InfValorT + QrEfdInnNFsCabValorT.Value;
          //
          QrEfdInnNFsCab.Next;
        end;
      end;

      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrVSMovDifInfPecas.Value  - InfPecas;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrVSMovDifInfPesoKg.Value - InfPesoKg;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrVSMovDifInfAreaM2.Value - InfAreaM2;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrVSMovDifInfAreaP2.Value - InfAreaP2;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrVSMovDifInfValorT.Value - InfValorT;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrVSInnCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrVSInnCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrVSInnCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := 55;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrVSInnCabemi_serie.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrVSInnCabemi_nNF.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := '';
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrVSInnCabDtCompra.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrVSInnCabDtEntrada.Value;

      FmEfdInnNFsCab.EdCliInt.ValueVariant        := QrVSInnCabClienteMO.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue            := QrVSInnCabClienteMO.Value;

      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrVSInnCabFornecedor.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrVSInnCabFornecedor.Value;

      FmEfdInnNFsCab.EdTransportador.ValueVariant    := QrVSInnCabTransporta.Value;
      FmEfdInnNFsCab.CBTransportador.KeyValue        := QrVSInnCabTransporta.Value;

      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrVSMovDifInfValorT.Value - InfValorT;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrVSMovDifInfValorT.Value - InfValorT;
      //FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnNFsCab.RGTpEntrd.ItemIndex          := QrEfdInnNFsCabTpEntrd.Value;
      FmEfdInnNFsCab.EdControle.ValueVariant      := QrEfdInnNFsCabControle.Value;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrEfdInnNFsCabPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrEfdInnNFsCabPesoKg.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrEfdInnNFsCabAreaM2.Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrEfdInnNFsCabAreaP2.Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrEfdInnNFsCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := QrEfdInnNFsCabCOD_MOD.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := QrEfdInnNFsCabCOD_SIT.Value;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrEfdInnNFsCabSER.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrEfdInnNFsCabNUM_DOC.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := QrEfdInnNFsCabCHV_NFE.Value;
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant     := QrVSInnNFsCEdNFeStaab.Value;
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrEfdInnNFsCabDT_DOC.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrEfdInnNFsCabDT_E_S.Value;
      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrEfdInnNFsCabVL_DOC.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := QrEfdInnNFsCabIND_PGTO.Value;
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := QrEfdInnNFsCabVL_DESC.Value;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrEfdInnNFsCabVL_MERC.Value;
      FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := QrEfdInnNFsCabIND_FRT.Value;
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := QrEfdInnNFsCabVL_FRT.Value;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := QrEfdInnNFsCabVL_SEG.Value;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := QrEfdInnNFsCabVL_OUT_DA.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnNFsCabVL_BC_ICMS.Value;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := QrEfdInnNFsCabVL_ICMS.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := QrEfdInnNFsCabVL_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := QrEfdInnNFsCabVL_IPI.Value;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := QrEfdInnNFsCabVL_PIS.Value;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := QrEfdInnNFsCabVL_PIS_ST.Value;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := QrEfdInnNFsCabNFe_FatID.Value;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := QrEfdInnNFsCabNFe_FatNum.Value;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := QrEfdInnNFsCabNFe_StaLnk.Value;
      //

      FmEfdInnNFsCab.EdCliInt.ValueVariant        := QrEfdInnNFsCabCliInt.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue            := QrEfdInnNFsCabCliInt.Value;

      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrEfdInnNFsCabTerceiro.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrEfdInnNFsCabTerceiro.Value;

      //
      FmEfdInnNFsCab.EdRegrFiscal.ValueVariant    := QrEfdInnNFsCabRegrFiscal.Value;
      FmEfdInnNFsCab.CBRegrFiscal.KeyValue        := QrEfdInnNFsCabRegrFiscal.Value;

    end;
    FmEfdInnNFsCab.ShowModal;
    NewControle := FmEfdInnNFsCab.FControle;
    FmEfdInnNFsCab.Destroy;
    //
    ReopenEfdInnNFsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnNFsCabControle.Value = NewControle then
      begin
        SPED_PF.ReopenNFeItens_EFD(QrNFeItens, QrEfdInnNFsCabNFe_FatID.Value,
        QrEfdInnNFsCabNFe_FatNum.Value, QrEfdInnNFsCabEmpresa.Value);
        QrNFeItens.First;
        while not QrNFeItens.Eof do
        begin
          MostraFormEfdInnNFsIts(stIns, True);
          //
          QrNFeItens.Next;
        end;
      end;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.MostraFormEfdInnNFsIts(SQLType: TSQLType; UsaNFeItens: Boolean);
const
  sProcName = 'TFmVSInnCab.MostraFormEfdInnNFsIts()';
var
  NewConta, Conta, GraGru1: Integer;
  CST_B: Integer;
  IPI_CST, PIS_CST, COFINS_CST: String;
  //ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
  _ICMS_vBC, _IPI_vBC, _PIS_vBC, _COFINS_vBC,
  ICMSAliq, PISAliq, COFINSAliq,
  RpICMS, RpIPI, RpPIS, RpCOFINS,
  RvICMS, RvIPI, RvPIS, RvCOFINS: Double;

begin
{$IfDef sAllVS}
  //NewConta := 0;
  //Conta := QrEfdInnNFsItsConta.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsIts, FmEfdInnNFsIts, afmoNegarComAviso) then
  begin
    FmEfdInnNFsIts.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsIts.FData           := QrEfdInnNFsCabDT_E_S.Value;
    FmEfdInnNFsIts.FRegrFiscal     := QrEfdInnNFsCabRegrFiscal.Value;

    FmEfdInnNFsIts.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnNFsIts.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnNFsIts.EdEmpresa.ValueVariant         := QrVSInnCabEmpresa.Value;
    FmEfdInnNFsIts.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsIts.EdMovFatNum.ValueVariant       := QrVSInnCabCodigo.Value;
    FmEfdInnNFsIts.EdMovimCod.ValueVariant        := QrVSInnCabMovimCod.Value;
    FmEfdInnNFsIts.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnNFsIts.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnNFsIts.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnNFsIts.FQrIts                         := QrVSInnNFs;
    if SQLType = stIns then
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := 0;

      GraGru1 := DmProd.ObtemGraGru1DeGraGruX(QrVSInnItsGraGruX.Value);

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrVSInnItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrVSInnItsGraGruX.Value;
      case QrVSInnItsGrandeza.Value of
        0: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrEfdInnNFsCabPecas.Value;
        2: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrEfdInnNFsCabPesoKg.Value;
        else Geral.MB_Aviso('Grandeza no cadastro grade da mat�ria-prima deve ser Pe�a ou Peso(kg)!');
      end;
      //FmEfdInnNFsIts.EdCFOP.Text                                  := '';

      case QrEfdInnNFsCabTpEntrd.Value of
        0: // compra
        begin
          if QrVSInnCabUF_Empresa.Value = QrVSInnCabUF_Fornece.Value then
            FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_DE.Value
          else
            FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_FE.Value;
        end;
        1: // remessa
        begin
          if QrVSInnCabUF_Empresa.Value = QrVSInnCabUF_Fornece.Value then
            FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleRemTrib_MP_CFOP_DE.Value
          else
            FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleRemTrib_MP_CFOP_FE.Value;
        end;
        else Geral.MB_Aviso('Tipo de entrada n�o implementada em ' + sProcName);
      end;
      //
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := 0.00;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := '';
      {
      if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
        'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, ',
        'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  ',
        'COFINSRec_pAliq  ',
        'FROM gragru1 ',
        'WHERE Nivel1=' + Geral.FF0(GraGru1),
        '']);
        //
        CST_B           := QrGraGru1CST_B.Value;
        IPI_CST         := QrGraGru1IPI_CST.Value;
        PIS_CST         := QrGraGru1PIS_CST.Value;
        COFINS_CST      := QrGraGru1COFINS_CST.Value;
        ICMSRec_pAliq   := QrGraGru1ICMSRec_pAliq.Value;
        ICMSSTRec_pAliq := 0.00;
        IPIRec_pAliq    := QrGraGru1IPIRec_pAliq.Value;
        PISRec_pAliq    := QrGraGru1PISRec_pAliq.Value;
        COFINSRec_pAliq := QrGraGru1COFINSRec_pAliq.Value;
      end else
      begin
      }
      case QrEfdInnNFsCabTpEntrd.Value of
        0: // compra
        begin
          CST_B           := Geral.IMV(Dmod.QrControleCreTrib_MP_ICMS_CST.Value);
          IPI_CST         := Dmod.QrControleCreTrib_MP_IPI_CST.Value;
          PIS_CST         := Dmod.QrControleCreTrib_MP_PIS_CST.Value;
          COFINS_CST      := Dmod.QrControleCreTrib_MP_COFINS_CST.Value;
         //
          FmEfdInnNFsIts.EdIND_MOV.ValueVariant := 0; // Movimenta
          FmEfdInnNFsIts.EdAjusteVL_OUTROS.ValueVariant := 0.00;
          // Parei aqui 2022-05-07
          case Dmod.QrControleCreTrib_MP_TES_ICMS.Value of
            0: // N�o tributa
            begin
              _ICMS_vBC                 := 0.00;
              RpICMS                    := 0.00;
              RvICMS                    := 0.00;
            end;
            1: // do XML da NFe
            begin
              if UsaNFeItens then
              begin
                _ICMS_vBC                 := QrNFeItensICMSRec_vBC.Value;
                RpICMS                    := QrNFeItensICMS_pICMS.Value;
                RvICMS                    := QrNFeItensICMS_vICMS.Value;
              end else
              begin
                _ICMS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpICMS                    := 0.00;
                RvICMS                    := 0.00;
              end;
            end;
            2: // da Regra Fiscal
            begin
              (*if UsaNFeItens then
              begin
                _ICMS_vBC                 := QrNFeItensICMSRec_vBC.Value;
                RpICMS                    := QrNFeItensICMS_pICMS.Value;
                RvICMS                    := QrNFeItensICMS_vICMS.Value;
              end else*)
              begin
                _ICMS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpICMS                    := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
                RvICMS                    := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value / 100 * QrEfdInnNFsCabValorT.Value;
              end;
            end;
            else
              Geral.MB_Erro('TES ICMS n�o implementado em ' + sProcName);
          end;
          //
          case Dmod.QrControleCreTrib_MP_TES_IPI.Value of
            0: // N�o tributa
            begin
              _IPI_vBC                 := 0.00;
              RpIPI                    := 0.00;
              RvIPI                    := 0.00;
            end;
            1: // do XML da NFe
            begin
              if UsaNFeItens then
              begin
                _IPI_vBC                 := QrNFeItensIPIRec_vBC.Value;
                RpIPI                    := QrNFeItensIPI_pIPI.Value;
                RvIPI                    := QrNFeItensIPI_vIPI.Value;
              end else
              begin
                _IPI_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpIPI                    := 0.00;
                RvIPI                    := 0.00;
              end;
            end;
            2: // da Regra Fiscal
            begin
              (*if UsaNFeItens then
              begin
                _IPI_vBC                 := QrNFeItensIPIRec_vBC.Value;
                RpIPI                    := QrNFeItensIPI_pIPI.Value;
                RvIPI                    := QrNFeItensIPI_vIPI.Value;
              end else*)
              begin
                _IPI_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpIPI                    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
                RvIPI                    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value / 100 * QrEfdInnNFsCabValorT.Value;
              end;
            end;
            else
              Geral.MB_Erro('TES IPI n�o implementado em ' + sProcName);
          end;
          //
          case Dmod.QrControleCreTrib_MP_TES_PIS.Value of
            0: // N�o tributa
            begin
              _PIS_vBC                 := 0.00;
              RpPIS                    := 0.00;
              RvPIS                    := 0.00;
            end;
            1: // do XML da NFe
            begin
              if UsaNFeItens then
              begin
                _PIS_vBC                 := QrNFeItensPISRec_vBC.Value;
                RpPIS                    := QrNFeItensPIS_pPIS.Value;
                RvPIS                    := QrNFeItensPIS_vPIS.Value;
              end else
              begin
                _PIS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpPIS                    := 0.00;
                RvPIS                    := 0.00;
              end;
            end;
            2: // da Regra Fiscal
            begin
              (*if UsaNFeItens then
              begin
                _PIS_vBC                 := QrNFeItensPISRec_vBC.Value;
                RpPIS                    := QrNFeItensPIS_pPIS.Value;
                RvPIS                    := QrNFeItensPIS_vPIS.Value;
              end else*)
              begin
                _PIS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpPIS                    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
                RvPIS                    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value / 100 * QrEfdInnNFsCabValorT.Value;
              end;
            end;
            else
              Geral.MB_Erro('TES PIS n�o implementado em ' + sProcName);
          end;
          //
          case Dmod.QrControleCreTrib_MP_TES_COFINS.Value of
            0: // N�o tributa
            begin
              _COFINS_vBC                 := 0.00;
              RpCOFINS                    := 0.00;
              RvCOFINS                    := 0.00;
            end;
            1: // do XML da NFe
            begin
              if UsaNFeItens then
              begin
                _COFINS_vBC                 := QrNFeItensCOFINSRec_vBC.Value;
                RpCOFINS                    := QrNFeItensCOFINS_pCOFINS.Value;
                RvCOFINS                    := QrNFeItensCOFINS_vCOFINS.Value;
              end else
              begin
                _COFINS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpCOFINS                    := 0.00;
                RvCOFINS                    := 0.00;
              end;
            end;
            2: // da Regra Fiscal
            begin
              (*if UsaNFeItens then
              begin
                _COFINS_vBC                 := QrNFeItensCOFINSRec_vBC.Value;
                RpCOFINS                    := QrNFeItensCOFINS_pCOFINS.Value;
                RvCOFINS                    := QrNFeItensCOFINS_vCOFINS.Value;
              end else*)
              begin
                _COFINS_vBC                 := QrEfdInnNFsCabValorT.Value;
                RpCOFINS                    := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
                RvCOFINS                    := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value / 100 * QrEfdInnNFsCabValorT.Value;
              end;
            end;
            else
              Geral.MB_Erro('TES COFINS n�o implementado em ' + sProcName);
          end;
          //
        end;
        1: // remessa
        begin
          CST_B           := Geral.IMV(Dmod.QrControleRemTrib_MP_ICMS_CST.Value);
          IPI_CST         := Dmod.QrControleRemTrib_MP_IPI_CST.Value;
          PIS_CST         := Dmod.QrControleRemTrib_MP_PIS_CST.Value;
          COFINS_CST      := Dmod.QrControleRemTrib_MP_COFINS_CST.Value;

          _ICMS_vBC                 := 0.00;
          RpICMS                    := 0.00;
          RvICMS                    := 0.00;

          _IPI_vBC                 := 0.00;
          RpIPI                    := 0.00;
          RvIPI                    := 0.00;

          _PIS_vBC                 := 0.00;
          RpPIS                    := 0.00;
          RvPIS                    := 0.00;

          _COFINS_vBC                 := 0.00;
          RpCOFINS                    := 0.00;
          RvCOFINS                    := 0.00;

          //
          FmEfdInnNFsIts.EdIND_MOV.ValueVariant := 1; // N�o movimenta
          FmEfdInnNFsIts.EdAjusteVL_OUTROS.ValueVariant :=
            QrVSMovDifInfValorT.Value - FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant;

        end; //j� avisa acima!
        //else Geral.MB_Aviso('Tipo de entrada n�o implementada em ' + sProcName);
      end;
      //
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant   := _ICMS_vBC;
      FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant    := _IPI_vBC;
      FmEfdInnNFsIts.EdVL_BC_PIS.ValueVariant    := _PIS_vBC;
      FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant := _COFINS_vBC;
      //
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant    := RpICMS;
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant     := RpIPI;
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant     := RpPIS;
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant  := RpCOFINS;
      //
      FmEfdInnNFsIts.EdVL_ICMS.ValueVariant      := RvICMS;
      FmEfdInnNFsIts.EdVL_IPI.ValueVariant       := RvIPI;
      FmEfdInnNFsIts.EdVL_PIS.ValueVariant       := RvPIS;
      FmEfdInnNFsIts.EdVL_COFINS.ValueVariant    := RvCOFINS;

      {end;}
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := CST_B;
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       := COD_NAT
      //FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := ;
      //FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := ICMSRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := VL_ICMS
      //FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := VL_BC_ICMS_ST
      //FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := ICMSSTRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := VL_ICMS_ST
      //FmEfdInnNFsIts.EdIND_APUR  .ValueVariant                    := IND_APUR
      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := IPI_CST;
      //FmEfdInnNFsIts.EdCOD_ENQ   .ValueVariant                    := COD_ENQ
      //FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := VL_BC_IPI
      //FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := IPIRec_pAliq;
      //FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := VL_IPI
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := PIS_CST;
      //FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := VL_BC_PIS
      //FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := PISRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QUANT_BC_PIS
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := ALIQ_PIS_r
      //FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := VL_PIS
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := COFINS_CST;
      //FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := VL_BC_COFINS
      //FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := COFINSRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QUANT_BC_COFINS
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := ALIQ_COFINS_r
      //FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := VL_COFINS
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := COD_CTA
      //FmEfdInnNFsIts.EdVL_ABAT_NT     .ValueVariant               := VL_ABAT_NT

    end else
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := QrEfdInnNFsItsConta.Value;
      FmEfdInnNFsIts.FTpEntrd                       := QrEfdInnNFsCabTpEntrd.Value;

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnNFsItsCFOP.Value));
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       :=
      FmEfdInnNFsIts.EdQTD.ValueVariant                           := QrEfdInnNFsItsQTD.Value;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := QrEfdInnNFsItsIND_MOV.Value;
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrEfdInnNFsItsprod_vProd.Value;
      FmEfdInnNFsIts.Edprod_vFrete.ValueVariant                   := QrEfdInnNFsItsprod_vFrete.Value;
      FmEfdInnNFsIts.Edprod_vSeg.ValueVariant                     := QrEfdInnNFsItsprod_vSeg.Value;
      FmEfdInnNFsIts.Edprod_vOutro.ValueVariant                   := QrEfdInnNFsItsprod_vOutro.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrEfdInnNFsItsVL_ITEM.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := QrEfdInnNFsItsVL_DESC.Value;
      FmEfdInnNFsIts.EdOri_IPIpIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIpIPI.Value;
      FmEfdInnNFsIts.EdOri_IPIvIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIvIPI.Value;
      FmEfdInnNFsIts.EdxLote.ValueVariant                         := QrEfdInnNFsItsxLote.Value;
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := QrEfdInnNFsItsCST_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnNFsItsVL_BC_ICMS.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := QrEfdInnNFsItsALIQ_ICMS.Value;
      FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := QrEfdInnNFsItsVL_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnNFsItsVL_BC_ICMS_ST.Value;
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := QrEfdInnNFsItsALIQ_ST.Value;
      FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnNFsItsVL_ICMS_ST.Value;
      //FmEfdInnNFsIts.EdIND_APUR.ValueVariant                      := IND_APUR
      //FmEfdInnNFsIts.EdCOD_ENQ.ValueVariant                       := COD_ENQ


      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := QrEfdInnNFsItsCST_IPI.Value;;
      FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := QrEfdInnNFsItsVL_BC_IPI.Value;
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := QrEfdInnNFsItsALIQ_IPI.Value;
      FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := QrEfdInnNFsItsVL_IPI.Value;
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := QrEfdInnNFsItsCST_PIS.Value;
      FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := QrEfdInnNFsItsVL_BC_PIS.Value;
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := QrEfdInnNFsItsALIQ_PIS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QrEfdInnNFsItsQUANT_BC_PIS.Value;
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := QrEfdInnNFsItsALIQ_PIS_r.Value;
      FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := QrEfdInnNFsItsVL_PIS.Value;
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := QrEfdInnNFsItsCST_COFINS.Value;
      FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnNFsItsVL_BC_COFINS.Value;
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := QrEfdInnNFsItsALIQ_COFINS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QrEfdInnNFsItsQUANT_BC_COFINS.Value;
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := QrEfdInnNFsItsALIQ_COFINS_r.Value;
      FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := QrEfdInnNFsItsVL_COFINS.Value;
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := QrEfdInnNFsItsCOD_CTA.Value;
      //FmEfdInnNFsIts.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnNFsItsVL_ABAT_NT.Value;
    end;
    FmEfdInnNFsIts.ShowModal;
    NewConta := FmEfdInnNFsIts.FConta;
    FmEfdInnNFsIts.Destroy;
        //
    ReopenEfdInnNFsIts(NewConta);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSInnCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSInnCab, QrVSInnIts);
end;

procedure TFmVSInnCab.PMCTePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrVsInnCab.State <> dsInactive) and (QrVSInnCab.RecordCount > 0);
  IncluiConhecimentodefrete1.Enabled := Habilita;
  Habilita := Habilita and (QrEfdInnCTsCab.RecordCount > 0);
  AlteraConhecimentodefrete1.Enabled := Habilita;
  ExcluiConhecimentodefrete1.Enabled := Habilita;
end;

procedure TFmVSInnCab.PMFiscalPopup(Sender: TObject);
begin
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiDocumento1, QrVSInnCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiDocumento1, QrEfdInnNFsCab, QrEfdInnNFsIts);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiItemdodocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraoItemselecionadododocumento1, QrEfdInnNFsIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluioItemselecionadododocumento1, QrEfdInnNFsIts);
  //
end;

procedure TFmVSInnCab.PMImprimePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(ClassesGeradas1, QrVSInnIts);
end;

procedure TFmVSInnCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSInnCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSInnIts);
  //MyObjects.HabilitaMenuItemCabDelC1I3(ItsExclui1, QrVSInnIts, QrVSItsBxa, QrTribIncIts, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui1, QrVSInnIts, QrVSItsBxa, QrTribIncIts);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSInnItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiNFe1, QrVSInnIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraNFe1, QrVSInnNFs);
  MyObjects.HabilitaMenuItemItsDel(ExcluiNFe1, QrVSInnNFs);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrVSInnIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrVSMOEnvAvu);
  //AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSInnIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  //AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
end;

procedure TFmVSInnCab.PMSubProdutoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSubProduto1, QrVSInnIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSubProduto1, QrVSSubPrdIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSubProduto1, QrVSSubPrdIts);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSSubPrdItsID_TTW.Value, [AlteraSubProduto1, ExcluiSubProduto1]);
end;

procedure TFmVSInnCab.PMTribIncItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiTributo1, QrVSInnIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraTributoAtual1, QrTribIncIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiTributoAtual1, QrTribIncIts);
end;

procedure TFmVSInnCab.PreparaModelo2X();
  procedure AbreQry(Qry: TmySQLQuery; SQL_EXTRA: String);
  var
    ATT_MovimID, ATT_MovimNiv: String;
  begin
    ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 1 ',
    '  WHEN 23 THEN 1 ',
    '  WHEN 26 THEN 2 ',
    '  WHEN 27 THEN 2 ',
    '  WHEN 29 THEN 1 ',
    'ELSE 9 END) + 0.000 AS DECIMAL) ID_GRUPO,   ',
    'CASE vmi.MovimID  ',
    '  WHEN 01 THEN "RECEITAS" ',
    '  WHEN 23 THEN "RECEITAS" ',
    '  WHEN 26 THEN "DESPESAS" ',
    '  WHEN 27 THEN "DESPESAS" ',
    '  WHEN 29 THEN "RECEITAS" ',
    'ELSE "OUTROS" END NO_GRUPO,   ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 101 ',
    '  WHEN 29 THEN 102 ',
    '  WHEN 23 THEN 103 ',
    '  WHEN 26 THEN 201 ',
    '  WHEN 27 THEN 202 ',
    'ELSE 999 END) + 0.000 AS DECIMAL) ORDEM2,   ',
    'vmi.*,  ',

(*
    'SELECT  ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 1 ',
    '  WHEN 23 THEN 1 ',
    '  WHEN 26 THEN 2 ',
    '  WHEN 27 THEN 2 ',
    'ELSE 9 END) + 0.000 AS DECIMAL) ID_GRUPO,   ',
    'CASE vmi.MovimID  ',
    '  WHEN 01 THEN "RECEITAS" ',
    '  WHEN 23 THEN "RECEITAS" ',
    '  WHEN 26 THEN "DESPESAS" ',
    '  WHEN 27 THEN "DESPESAS" ',
    'ELSE "OUTROS" END NO_GRUPO,   ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 101 ',
    '  WHEN 23 THEN 102 ',
    '  WHEN 26 THEN 201 ',
    '  WHEN 27 THEN 202 ',
    'ELSE 999 END) + 0.000 AS DECIMAL) ORDEM2,   ',
    'vmi.*,  ',
*)
    ATT_MovimID,
    ATT_MovimNiv,
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
    'ggx.GraGruY, ggy.Nome NO_GGY',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'WHERE vmi.SerieFch=' + Geral.FF0(QrVSInnItsSerieFch.Value),
    'AND vmi.Ficha=' + Geral.FF0(QrVSInnItsFicha.Value),
    'AND (NOT MovimNiv IN (14,15,30,35)) ',
    SQL_EXTRA,
    'ORDER BY ORDEM2, vmi.Controle  ',
    '']);
    //Geral.MB_SQL(FmVSInnCab, Qry);
  end;
begin
  if QrVSInnCabDtEnceRend.Value < 2 then
    EncerraRendimento();
  //
  ReopenLookupRend();
  //
  // ini 2020-09-12
{
  AbreQry(QrReceitas, 'AND vmi.MovimID IN (' +
    (*01*)Geral.FF0(Integer(emidCompra)) + ',' +
    (*23*)Geral.FF0(Integer(emidGeraSubProd)) + ')'
}

  AbreQry(QrReceitas, 'AND (vmi.MovimID IN (' +
    (*01*)Geral.FF0(Integer(emidCompra)) + ',' +
    (*23*)Geral.FF0(Integer(emidGeraSubProd)) + ')'

    // Couro caleado para gelatina!
    //CO_TXT_iuvpei006 = 'Gera��o de artigo durante o processo de caleiro';
    + ' OR iuvpei=' + Geral.FF0(Integer(TInsUpdVMIPrcExecID.iuvpei006))
    + ')'
// fim 2020-09-12

  );
  //Geral.MB_SQL(Self, QrReceitas);
  AbreQry(QrDespesas, 'AND vmi.MovimID IN (' +
    (*26*)Geral.FF0(Integer(emidEmProcCal)) + ',' +
    (*27*)Geral.FF0(Integer(emidEmProcCur)) + ')'
  );
  //Geral.MB_SQL(Self, QrDespesas);
  AbreQry(QrVSGerados, 'AND vmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)));
  //Geral.MB_Teste(QrVSGerados.SQL.Text);
  //
  QrReceitas.First;
  QrDespesas.First;
end;

procedure TFmVSInnCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSInnCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSInnCab.DefParams;
begin
  VAR_GOTOTABELA := 'VSInnCab';
  VAR_GOTOMYSQLTABLE := QrVSInnCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(wic.DtEnceRend < "1900-01-01", "", DATE_FORMAT(wic.DtEnceRend, "%d/%m/%y %H:%i")) DtEnceRend_TXT,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA,');
  VAR_SQLx.Add('ei.CodCliInt, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.EUF, ent.PUF) UF_EMPRESA, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.EUF, frn.PUF) UF_FORNECE ');
  VAR_SQLx.Add('FROM vsinncab wic');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSInnCab.Edemi_nNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    //ImportaDadosNFeCab(Edemi_nNF);
end;

procedure TFmVSInnCab.Edide_nNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    //ImportaDadosNFeCab(Edide_nNF);
end;

procedure TFmVSInnCab.Encerrarendimento();
const
  GGYIni = CO_GraGruY_1024_VSNatCad; // 1024;
  GGYFim = CO_GraGruY_1877_VSCouCur; // 1877;
  GGYPos = CO_GraGruY_2048_VSRibCad; // 2048;
(*
  GGYFim = CO_GraGruY_2048_VSRibCad; // 2048;
  GGYPos = CO_GraGruY_3072_VSRibCla; // 3072;
*)
var
  EnceRendInn, Codigo, MovimCod, Serie, Ficha: Integer;
begin
  Codigo      := QrVSInnItsCodigo.Value;
  MovimCod    := QrVSInnCabMovimCod.Value;
  Serie       := QrVSInnItsSerieFch.Value;
  Ficha       := QrVSInnItsFicha.Value;
  EnceRendInn := MyObjects.SelRadioGroup('Encerramento de Rendimento',
  'Selecione a forma de encerramento', ['Automatico para todos poss�veis',
  'Automatico para este', 'Manual para este', 'Nenhum'], 1);
  case EnceRendInn of
    0: VS_CRC_PF.EncerraRendimentoInn(TdmkModoExec.dmodexAutomatico, 0, 0, 0,
       PB1, LaAviso1, LaAviso2, GGYIni, GGYFim, GGYPos);
    1: VS_CRC_PF.EncerraRendimentoInn(TdmkModoExec.dmodexAutomatico, MovimCod,
      Serie, Ficha, PB1, LaAviso1, LaAviso2, GGYIni, GGYFim, GGYPos);
    2: VS_CRC_PF.EncerraRendimentoInn(TdmkModoExec.dmodexManual, MovimCod, Serie,
      Ficha, PB1, LaAviso1, LaAviso2, GGYIni, GGYFim, GGYPos);
  end;
  LocCod(Codigo, Codigo);
end;

procedure TFmVSInnCab.Encerrarendimento1Click(Sender: TObject);
begin
  Encerrarendimento();
end;

(*Em desenvolvimento
procedure TFmVSInnCab.ImportaDadosNFeCab(EdnNF: TDmkEdit);
var
  Continua: Boolean;
  Empresa, NF: Integer;
begin
  Empresa  := EdEmpresa.ValueVariant;
  NF       := EdnNF.ValueVariant;
  Continua := True;
  //
  if NF <> 0 then
  begin
    if EdnNF = Edide_nNF then
      Continua := Geral.MB_Pergunta('A NF-e de entrada j� foi definida!' +
                    sLineBreak + 'Realmente deseja alter�-la?') = ID_YES
    else if EdnNF = Edemi_nNF then
      Continua := Geral.MB_Pergunta('A NF-e fornecedor j� foi definida!' +
                    sLineBreak + 'Realmente deseja alter�-la?') = ID_YES
    else
      Continua := False;
  end;
  //
  if Continua then
  begin
    if EdnNF = Edide_nNF then
      VS_CRC_PF.ImportaDadosNFeCab(0, nil, nil, nil, nil, nil,
        nil, TPDtEntrada, EdDtEntrada, Edide_serie, Edide_nNF)
    else if EdnNF = Edemi_nNF then
      VS_CRC_PF.ImportaDadosNFeCab(1, EdFornecedor, CBFornecedor, EdClienteMO,
        CBClienteMO, TPDtCompra, EdDtCompra, TPDtViagem, EdDtViagem,
        Edemi_serie, Edemi_nNF);
    //
    EdEmpresa.ValueVariant := Empresa;
    CBEmpresa.KeyValue     := Empresa;
  end;
end;
*)

procedure TFmVSInnCab.Estoque1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSInnCab.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSHisFchCodigo.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de hist�rico?',
  'vshisfch', 'Codigo', Codigo, DMod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSHisFch,
      QrVSHisFchCodigo, QrVSHisFchCodigo.Value);
    ReopenVSHisFch(Codigo);
  end;
end;

procedure TFmVSInnCab.Exclui2Click(Sender: TObject);
begin
  //if NaoPermiteFinanca(QrEmissM, True) then
    //Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissM, QrEmissMFatID.Value,
    QrEmissMFatNum.Value, QrEmissMFatParcela.Value, QrEmissMCarteira.Value,
    QrEmissMSit.Value, QrEmissMTipo.Value, dmkPF.MotivDel_ValidaCodigo(316),
    FTabLctA)
  then
    ReopenEmissM(QrEmissMFatParcela.Value);
end;

procedure TFmVSInnCab.Exclui3Click(Sender: TObject);
begin
  //if NaoPermiteFinanca(QrEmissT, True) then
    //Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissT, QrEmissTFatID.Value,
    QrEmissTFatNum.Value, QrEmissTFatParcela.Value, QrEmissTCarteira.Value,
    QrEmissTSit.Value, QrEmissTTipo.Value, dmkPF.MotivDel_ValidaCodigo(317),
    FTabLctA)
  then
    ReopenEmissT(QrEmissTFatParcela.Value);
end;

procedure TFmVSInnCab.Exclui4Click(Sender: TObject);
begin
  //if NaoPermiteFinanca(QrEmissC, True) then
    //Exit;
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissC, QrEmissCFatID.Value,
    QrEmissCFatNum.Value, QrEmissCFatParcela.Value, QrEmissCCarteira.Value,
    QrEmissCSit.Value, QrEmissCTipo.Value, dmkPF.MotivDel_ValidaCodigo(319),
    FTabLctA)
  then
    ReopenEmissC(QrEmissCFatParcela.Value);
end;

procedure TFmVSInnCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.ExcluiConhecimentodefrete1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
*)
    if QrEfdInnCTsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do CT-e selecionado?',
      'EfdInnCTsCab', 'Controle', QrEfdInnCTsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnCTsCab,
          QrEfdInnCTsCabControle, QrEfdInnCTsCabControle.Value);
        ReopenEfdInnCTsCab(Controle);
      end;
    end;
  //end;
end;

procedure TFmVSInnCab.ExcluiDocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
    if QrEfdInnNFsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do DF-e selecionado?',
      'EfdInnNFsCab', 'Controle', QrEfdInnNFsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnNFsCab,
          QrEfdInnNFsCabControle, QrEfdInnNFsCabControle.Value);
        ReopenEfdInnNFsCab(Controle);
      end;
    end;
  end;
(*
begin
  DmEFDII.ExcluiDocumento(FEFDInnNFSMainFatID, QrVsInnCabCodigo.Value, QrEfdInnNFsCab,
    QrEfdInnNFsIts, QrEfdInnNFsCabControle);
*)
end;

procedure TFmVSInnCab.ExcluiNFe1Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := QrVSInnNFsConta.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de NF-e?',
  'vsinnnfs', 'Controle', Conta, DMod.MyDB) = ID_YES then
  begin
    AtualizaSaldoIts(QrVSInnItsControle.Value);
    Conta := GOTOy.LocalizaPriorNextIntQr(QrVSInnNFs,
      QrVSInnNFsConta, QrVSInnNFsConta.Value);
    ReopenVSInnNFs(Conta);
    //
    MostraFormVSInnIts(stUpd);
  end;
end;

procedure TFmVSInnCab.ExcluioItemselecionadododocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Controle := QrEfdInnNFsCabControle.Value;
    //
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEfdInnNFsIts, TDBGrid(DBGInnNFsIts),
    'EfdInnNFsIts', ['Conta'], ['Conta'], istPergunta, '');
    //
    DmNFe_0000.AtualizaTotaisEfdInnNFs(Controle);
  end;
end;

procedure TFmVSInnCab.ExcluiSubProduto1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  Controle := QrVSSubPrdItsControle.Value;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '',
    'UNION',
    '',
    'SELECT Controle ',
    'FROM ' + CO_TAB_VMB,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '']);
    //
    if MyObjects.FIC(Qry.RecordCount > 0, nil,
    'O item n�o pode ser exclu�do pois j� possui baixas!') then
      Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do sub produto?') <> ID_YES then
      Exit;
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti111), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSInnCab.ExcluiTributoAtual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrTribIncItsControle.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de tributo?',
  'tribincits', 'Controle', Controle, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribIncIts,
      QrTribIncItsControle, QrTribIncItsControle.Value);
    ReopenTribIncIts(Controle);
  end;
end;

procedure TFmVSInnCab.ImprimeAmaliseMPAGModelo1(SemGelatina: Boolean);
  procedure AbreQry(Qry: TmySQLQuery; SQL_EXTRA: String);
  var
    ATT_MovimID, ATT_MovimNiv: String;
  begin
    ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 1 ',
    '  WHEN 23 THEN 1 ',
    '  WHEN 26 THEN 2 ',
    '  WHEN 27 THEN 2 ',
    '  WHEN 29 THEN 1 ',
    'ELSE 9 END) + 0.000 AS DECIMAL) ID_GRUPO,   ',
    'CASE vmi.MovimID  ',
    '  WHEN 01 THEN "RECEITAS" ',
    '  WHEN 23 THEN "RECEITAS" ',
    '  WHEN 26 THEN "DESPESAS" ',
    '  WHEN 27 THEN "DESPESAS" ',
    '  WHEN 29 THEN "RECEITAS" ',
    'ELSE "OUTROS" END NO_GRUPO,   ',
    'CAST((CASE vmi.MovimID  ',
    '  WHEN 01 THEN 101 ',
    '  WHEN 29 THEN 102 ',
    '  WHEN 23 THEN 103 ',
    '  WHEN 26 THEN 201 ',
    '  WHEN 27 THEN 202 ',
    'ELSE 999 END) + 0.000 AS DECIMAL) ORDEM2,   ',
    'vmi.*,  ',
    ATT_MovimID,
    ATT_MovimNiv,
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
    'ggx.GraGruY, ggy.Nome NO_GGY',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'WHERE vmi.SerieFch=' + Geral.FF0(QrVSInnItsSerieFch.Value),
    'AND vmi.Ficha=' + Geral.FF0(QrVSInnItsFicha.Value),
    'AND (NOT MovimNiv IN (14,15,30,35)) ',
    SQL_EXTRA,
    'ORDER BY ORDEM2, vmi.Controle  ',
    '']);
    //if Mostra then
      //Geral.MB_SQL(Self, Qry);
  end;
  procedure ReopenVSItsBxa_Medias();
  const
    SrcMovID = TEstqMovimID.emidCompra;
    MovimNiv15 = TEstqMovimNiv.eminBaixCurtiXX;
    MovimNiv29 = TEstqMovimNiv.eminSorcCal;
    //corrigir baixa
  var
    SrcNivel1, SrcNivel2, LocCtrl, TemIMEIMrt: Integer;
  begin
    SrcNivel1  := QrVSInnItsCodigo.Value;
    SrcNivel2  := QrVSInnItsControle.Value;
    TemIMEIMrt := QrVSInnCabTemIMEIMrt.Value;
    //
    VS_CRC_PF.ReopenVSItsBxa_Medias(
      QrMedias, SrcMovID, SrcNivel1, SrcNivel2, TemIMEIMrt,
      [MovimNiv15, MovimNiv29]);
  end;
var
  Report: TfrxReport;
begin
// ini 2020-09-12
{
  AbreQry(QrVSMovIts, 'AND NOT vmi.MovimID IN (' +
    (*06*)Geral.FF0(Integer(emidIndsXX)) + ',' +
    (*29*)Geral.FF0(Integer(emidCaleado)) + ',' +
    (*30*)Geral.FF0(Integer(emidEmRibPDA)) + ',' +
    (*31*)Geral.FF0(Integer(emidEmRibDTA)) + ')'
    + ' OR iuvpei=6'
}
  //AbreQry(QrVSMovIts, 'AND NOT vmi.MovimID IN (' +
  AbreQry(QrVSMovIts, 'AND (NOT vmi.MovimID IN (' +
    (*06*)Geral.FF0(Integer(emidIndsXX)) + ',' +
    (*29*)Geral.FF0(Integer(emidCaleado)) + ',' +
    (*30*)Geral.FF0(Integer(emidEmRibPDA)) + ',' +
    (*31*)Geral.FF0(Integer(emidEmRibDTA)) + ')'
    // Couro caleado para gelatina!
    //CO_TXT_iuvpei006 = 'Gera��o de artigo durante o processo de caleiro';
    + ' OR iuvpei=' + Geral.FF0(Integer(TInsUpdVMIPrcExecID.iuvpei006))
    + ')'

// fim 2020-09-12

  );
  AbreQry(QrVSGerados, 'AND vmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)));
(*
  FMPAG_Artigos := '';
  QrVSMovIts.First;
  while not QrVSMovIts.Eof do
  begin
    if QrVSMovItsMovimID.Value = 6 then
    begin
      if QrVSMovIts.RecNo > 1 then
        FMPAG_Artigos := FMPAG_Artigos + ' | ';
      FMPAG_Artigos := FMPAG_Artigos + QrVSMovItsNO_PRD_TAM_COR.Value;
    end;
    //
    QrVSMovIts.Next;
  end;
*)
  //
  // Ini 2019-01-09
  if SemGelatina then
  begin
    ReopenVSItsBxa_Medias();
    Report := frxWET_CURTI_006_01_SemGelatina;
  end else
    Report := frxWET_CURTI_006_01;
  // Fim 2019-01-09
  QrVSMovIts.First;
  //Geral.MB_SQL(Self, QrVSMovIts);
  MyObjects.frxDefineDataSets(Report, [
    DModG.frxDsDono,
    frxDsVSInnCab,
    frxDsVSInnIts,
    frxDsVSItsBxa,
    frxDsVSItsGer,
    //
    frxDsVSMovIts,
    frxDsVSGerados
  ]);
  MyObjects.frxMostra(Report, 'An�lise MPAG');
end;

procedure TFmVSInnCab.Inclui1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stIns);
end;

procedure TFmVSInnCab.Inclui2Click(Sender: TObject);
const
  Titulo = 'Compra de Mat�ria-Prima';
  Reconfig = True; // >> Setar Carteira!
var
  Terceiro, Cod, Genero, GenCtb, Empresa, NF, Carteira: Integer;
  SerieNF, Descricao: String;
  Valor, Pecas, PesoKg: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if FmPrincipal.AvisaFaltaDeContaDoPlano(FEFDInnNFSMainFatID) then
    Exit;
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaMPCompr.Value;
  GenCtb   := 0; // Fazer?
  Cod      := QrVSInnCabCodigo.Value;
  Terceiro := QrVSInnCabFornecedor.Value;
  Valor    := CalculaDiferencas(apFornece);
  Empresa  := QrVSInnCabEmpresa.Value;
  SerieNF  := Geral.FF0(QrVSInnCabide_serie.Value);
  NF       := QrVSInnCabide_nNF.Value;
  Pecas    := QrVSInnCabPecas.Value;
  PesoKg   := QrVSInnCabPesoKg.Value;
  //
{
  if Valor = 0 then
  begin
    QrVSInnIts.First;
    while not QrVSInnIts.Eof do
    begin
      Valor := Valor + QrVSInnItsValorMP.Value;
      QrVSInnIts.Next;
    end;
  end;
}
  //
  if (QrEmissM.State <> dsInactive) and (QrEmissM.RecordCount > 0) then
  begin
    QrEmissM.First;
    while not QrEmissM.Eof do
    begin
      Pecas  := Pecas  - QrEmissMQtde.Value;
      PesoKg := PesoKg - QrEmissMQtd2.Value;
      QrEmissM.Next;
    end;
  end;
  //
  if (QrVSInnIts.State <> dsInactive) and (QrVSInnIts.RecordCount > 0) then
    Descricao := QrVSInnItsNO_PRD_TAM_COR.Value
  else
    Descricao := Titulo;
  //
  try
    Carteira := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo  ',
    'FROM carteiras ',
    'WHERE FornecePadrao=' + Geral.FF0(QrVSInnCabFornecedor.Value),
    'ORDER BY Tipo, Codigo',
    '']);
    if Dmod.QrAux.RecordCount > 0 then
      Carteira := Dmod.QrAux.FieldByName('Codigo').AsInteger;
  finally
    UPagtos.Pagto(QrEmissM, tpDeb, Cod, Terceiro, FEFDInnNFSMainFatID, Genero, GenCtb, stIns,
      Titulo, Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
      0, True, Reconfig, Carteira, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF, Pecas, PesoKg);
    //SubQuery1Reopen;
    ReopenEmissM(0);
  end;
  //
end;

procedure TFmVSInnCab.Inclui3Click(Sender: TObject);
const
  Titulo = 'Frete de Mat�ria-Prima';
  Reconfig = False; // >> Setar Carteira!
var
  Terceiro, Cod, Genero, Empresa, NF, Carteira: Integer;
  Descricao, SerieNF: String;
  Valor: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1004) then
    Exit;
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaMPFrete.Value;
  Cod      := QrVSInnCabCodigo.Value;
  Terceiro := QrVSInnCabFornecedor.Value;
  Valor    := CalculaDiferencas(apTranspo);
  Empresa  := QrVSInnCabEmpresa.Value;
  SerieNF  := '';//Geral.FF0(QrVSInnCabide_serie.Value);
  NF       := 0;//QrVSInnCabide_nNF.Value;
  //
(*
  if Valor = 0 then
  begin
    QrVSInnIts.First;
    while not QrVSInnIts.Eof do
    begin
      Valor := Valor + QrVSInnItsValorMP.Value;
      QrVSInnIts.Next;
    end;
  end;
  //
*)

(*
  if (QrEmissT.State <> dsInactive) and (QrEmissT.RecordCount > 0) then
  begin
    QrEmissT.First;
    while not QrEmissT.Eof do
    begin
      Pecas  := Pecas  - QrEmissTQtde.Value;
      PesoKg := PesoKg - QrEmissTQtd2.Value;
      QrEmissT.Next;
    end;
  end;
*)
  //
  if (QrVSInnIts.State <> dsInactive) and (QrVSInnIts.RecordCount > 0) then
    Descricao := QrVSInnItsNO_PRD_TAM_COR.Value
  else
    Descricao := Titulo;
  //
  try
    Carteira := 0;
(*
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo  ',
    'FROM carteiras ',
    'WHERE FornecePadrao=' + Geral.FF0(QrVSInnCabFornecedor.Value),
    'ORDER BY Tipo, Codigo',
    '']);
    if Dmod.QrAux.RecordCount > 0 then
      Carteira := Dmod.QrAux.FieldByName('Codigo').AsInteger;
*)
  finally
    UPagtos.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_1004, Genero, 0(*GenCtb*), stIns,
      Titulo, Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
      0, True, Reconfig, Carteira, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF); //, Pecas, PesoKg);
    //SubQuery1Reopen;
    ReopenEmissT(0);
  end;
  //
end;

procedure TFmVSInnCab.Inclui4Click(Sender: TObject);
const
  Titulo = 'Comiss�o Compra MP';
  Reconfig = False; // >> Setar Carteira!
var
  Terceiro, Cod, Genero, Empresa, NF, Carteira: Integer;
  Descricao, SerieNF: String;
  Valor: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  if FmPrincipal.AvisaFaltaDeContaDoPlano(VAR_FATID_1010) then
    Exit;
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaComisCMP.Value;
  Cod      := QrVSInnCabCodigo.Value;
  Terceiro := QrVSInnCabFornecedor.Value;
  Valor    := CalculaDiferencas(apComisao);
  Empresa  := QrVSInnCabEmpresa.Value;
  SerieNF  := '';//Geral.FF0(QrVSInnCabide_serie.Value);
  NF       := 0;//QrVSInnCabide_nNF.Value;
  //
(*
  if Valor = 0 then
  begin
    QrVSInnIts.First;
    while not QrVSInnIts.Eof do
    begin
      Valor := Valor + QrVSInnItsValorMP.Value;
      QrVSInnIts.Next;
    end;
  end;
  //
*)

(*
  if (QrEmissC.State <> dsInactive) and (QrEmissC.RecordCount > 0) then
  begin
    QrEmissC.First;
    while not QrEmissC.Eof do
    begin
      Pecas  := Pecas  - QrEmissCQtde.Value;
      PesoKg := PesoKg - QrEmissCQtd2.Value;
      QrEmissC.Next;
    end;
  end;
*)
  //
  if (QrVSInnIts.State <> dsInactive) and (QrVSInnIts.RecordCount > 0) then
    Descricao := QrVSInnItsNO_PRD_TAM_COR.Value
  else
    Descricao := Titulo;
  //
  try
    Carteira := 0;
(*
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo  ',
    'FROM carteiras ',
    'WHERE FornecePadrao=' + Geral.FF0(QrVSInnCabFornecedor.Value),
    'ORDER BY Tipo, Codigo',
    '']);
    if Dmod.QrAux.RecordCount > 0 then
      Carteira := Dmod.QrAux.FieldByName('Codigo').AsInteger;
*)
  finally
    UPagtos.Pagto(QrEmissC, tpDeb, Cod, Terceiro, VAR_FATID_1010, Genero, 0(*GenCtb*), stIns,
      Titulo, Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
      0, True, Reconfig, Carteira, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF); //, Pecas, PesoKg);
    //SubQuery1Reopen;
    ReopenEmissC(0);
  end;
  //
end;

procedure TFmVSInnCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSInnIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, QrVSInnCabide_Serie.Value, QrVSInnCabide_nNF.Value);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSInnCabMovimCod.Value, 0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.IncluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, QrVSInnIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo, (*Terceiro*)0, (*CabSerieRem*)0, (*CabNFeRem*)0);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSInnCabMovimCod.Value, 0);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.IncluiConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stIns);
end;

procedure TFmVSInnCab.IncluiNFe1Click(Sender: TObject);
begin
  MostraFormVSInnNFs(stIns);
end;

procedure TFmVSInnCab.IncluiDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stIns);
end;

procedure TFmVSInnCab.IncluiItemdodocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stIns, False);
end;

procedure TFmVSInnCab.IncluiTributo1Click(Sender: TObject);
const
  Controle = 0;
  Operacao = 0;
  Tributo  = 0;
  ValorFat = 0.00;
  BaseCalc = 0.00;
  Percent  = 0.00;
  VUsoCalc = 0.00;
  ValrTrib = 0.00;
var
  FatNum, FatParcela, Empresa: Integer;
  Data, Hora: TDateTime;
begin
{$IfDef sAllVS}
  FatNum     := QrVSInnItsCodigo.Value;
  FatParcela := QrVSInnItsControle.Value;
  Empresa    := QrVSInnCabEmpresa.Value;
  Data       := QrVSInnCabDtCompra.Value;
  Hora       := QrVSInnCabDtCompra.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stIns, QrTribIncIts, FEFDInnNFSMainFatID,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siPositivo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.IncluiSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  Controle   = 0;
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  VSMulFrnCab, OriGGX, OriSerieFch, OriFicha: Integer;
  OriMarca: String;
begin
{$IfDef sAllVS}
  DataHora    := DModG.ObtemAgora();
  GSPSrcMovID := TEstqMovimID(QrVSInnItsMovimID.Value);
  GSPSrcNiv2  := QrVSInnItsControle.Value;
  Codigo      := 0;
  MovimCod    := 0;
  Empresa     := QrVSInnItsEmpresa.Value;
  ClientMO    := QrVSInnItsClientMO.Value;
  Terceiro    := QrVSInnItsTerceiro.Value;
  VSMulFrnCab := QrVSInnItsVSMulFrnCab.Value;
  Ficha       := QrVSInnItsFicha.Value;
  SerieFch    := QrVSInnItsSerieFch.Value;
  //
  OriGGX      := QrVSInnItsGraGruX.Value;
  OriSerieFch := QrVSInnItsSerieFch.Value;
  OriFicha    := QrVSInnItsFicha.Value;
  OriMarca    := QrVSInnItsMarca.Value;
  //
  VS_PF.MostraFormVSInnSubPrdIts_Uni(stIns, DataHora, QrCab, QrVSSubPrdIts, DsCab,
  GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
  VSMulFrnCab, Ficha, SerieFch, Controle,
  (*OriGGX, OriSerieFch, OriFicha,*) OriMarca);
  //ReopenVSSubPrdIts(0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.IrparajaneladegerenciamentodeFichaRMP1Click(
  Sender: TObject);
var
  SerieFicha, Ficha: Integer;
begin
{$IfDef sAllVS}
  SerieFicha := QrVSInnItsSerieFch.Value;
  Ficha      := QrVSInnItsFicha.Value;
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSInnIts(stUpd);
end;

procedure TFmVSInnCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSInnCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSInnCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSInnCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrVSInnItsCodigo.Value;
  MovimCod := QrVSInnItsMovimCod.Value;
  Controle := QrVSInnItsControle.Value;
  //
  if VS_CRC_PF.ExcluiControleVSMovIts(QrVSInnIts, TIntegerField(QrVSInnItsControle),
  Controle, CtrlBaix, QrVSInnItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti006)) then
  begin
    // Exclui tambem da tabela vsmovdif
    VS_CRC_PF.ExcluiVSNaoVMI('', 'vsmovdif', 'Controle', Controle, Dmod.MyDB);
    // Exclui tambem da tabela vsproqui
    VS_CRC_PF.ExcluiVSNaoVMI('', 'vsproqui', 'Controle', Controle, Dmod.MyDB);
    // Atualiza!
    VS_CRC_PF.AtualizaTotaisVSXxxCab('VSInnCab', MovimCod);
    //
    FmVSInnCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSInnCab.ReopenEmissC(Controle: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissC, Dmod.MyDB, [
  'SELECT la.*, ca.Nome NOMECARTEIRA ',
  'FROM ' + FTabLctA + ' la  ',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
  'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1010),
  'AND la.FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
  'AND la.ID_Pgto = 0 ',
  'ORDER BY FatParcela ',
  '']);
  //
  if Controle <> 0 then
    QrEmissC.Locate('Controle', Controle, []);
end;

procedure TFmVSInnCab.ReopenEmissM(Controle: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissM, Dmod.MyDB, [
  'SELECT la.*, ca.Nome NOMECARTEIRA ',
  'FROM ' + FTabLctA + ' la  ',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
  'WHERE la.FatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND la.FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
  'AND la.ID_Pgto = 0 ',
  'ORDER BY FatParcela ',
  '']);
  //
  if Controle <> 0 then
    QrEmissM.Locate('Controle', Controle, []);
end;

procedure TFmVSInnCab.ReopenEmissT(Controle: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissT, Dmod.MyDB, [
  'SELECT la.*, ca.Nome NOMECARTEIRA ',
  'FROM ' + FTabLctA + ' la  ',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
  'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1004),
  'AND la.FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
  'AND la.ID_Pgto = 0 ',
  'ORDER BY FatParcela ',
  '']);
  //
  if Controle <> 0 then
    QrEmissT.Locate('Controle', Controle, []);

end;

procedure TFmVSInnCab.ReopenLookupRend();
var
  DtEnceRend: String;
  VSCPMRSBCb: Integer;
begin
  VSCPMRSBCb := -999999999;
  if QrVSInnCabDtEnceRend.Value > 2 then
  begin
    DtEnceRend := Geral.FDT(QrVSInnCabDtEnceRend.Value, 109);
    //
    UnDmkDAC_PF.AbreMySQLQUery0(QrVSCPMRSBCb, Dmod.MyDB, [
    'SELECT Codigo, DataIni, DataFim, Nome',
    'FROM vscpmrsbcb',
    'WHERE (Empresa=0 OR Empresa=' + Geral.FF0(QrVSInnCabEmpresa.Value) + ')',
    'AND (',
    '  DataIni < "1900-01-01"',
    '  OR',
    '  DataIni < "' + DtEnceRend + '"',
    ') ',
    'AND (',
    '  DataFim < "1900-01-01"',
    '  OR',
    '  DATE_ADD(DataFim, Interval 1 DAY) > "' + DtEnceRend + '"',
    ')',
    'ORDER BY Codigo DESC',
    '']);
    if QrVSCPMRSBCb.RecordCount > 0 then
      VSCPMRSBCb := QrVSCPMRSBCbCodigo.Value;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCPMRSBIt, Dmod.MyDB, [
  'SELECT Controle, GraGruX,  ',
  'DivKgSPpcCou, PercKgSPKgCou  ',
  'FROM vscpmrsbit ',
  'WHERE Codigo=' + Geral.FF0(VSCPMRSBCb),
  'ORDER BY GraGruX, Controle ',
  '']);
end;

procedure TFmVSInnCab.ReopenTribIncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT tii.*, tdd.Nome NO_Tributo  ',
  'FROM tribincits tii ',
  'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo',
  'WHERE tii.FatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND tii.FatNum=' + Geral.FF0(QrVSInnItsCodigo.Value),
  'AND tii.FatParcela=' + Geral.FF0(QrVSInnItsControle.Value),
  'ORDER BY tii.Controle DESC ',
  '']);
end;

procedure TFmVSInnCab.ReopenVSHisFch(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSHisFch, Dmod.MyDB, [
  'SELECT * ',
  'FROM vshisfch ',
  'WHERE VSMovIts=' + Geral.FF0(QrVSInnItsControle.Value),
  'OR ( ',
  '  SerieFch=' + Geral.FF0(QrVSInnItsSerieFch.Value),
  '  AND ',
  '  Ficha=' + Geral.FF0(QrVSInnItsFicha.Value),
  ') ',
  '']);
  //
  if Codigo <> 0 then
    QrVSHisFch.Locate('Codigo', Codigo, []);
end;

procedure TFmVSInnCab.ReopenVSInnIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnIts, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSInnCabMovimCod.Value),
  'ORDER BY NO_Pallet, vmi.Controle ',
  '']);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVsInnCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  //'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, unm.Grandeza, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'vmi.ClientMO, fch.Nome NO_SerieFch, vmi.CustoComiss, ',
  'vmi.PerceComiss, vmi.CusKgComiss, vmi.CredValrImposto, ',
  'vmi.CredPereImposto, vmi.RpICMS, vmi.RpICMSST, ',
  'vmi.RpPIS, vmi.RpCOFINS, vmi.RvICMS, ',
  'vmi.RvICMSST, vmi.RvPIS, vmi.RvCOFINS, ',
  'vmi.RpIPI, RvIPI, ',
  'IF(vmi.PesoKg <> 0.00, vmi.ValorT / vmi.PesoKg, 0.00) CustoKg ',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN vsserfch   fch ON fch.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSInnCabMovimCod.Value),
  '']);
  SQL_Group := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVsInnIts, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVsInnIts);
  //
  QrVSInnIts.Locate('Controle', Controle, []);
end;


procedure TFmVSInnCab.ReopenVSInnNFs(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrVSInnNFs, Dmod.MyDB, [
  'SELECT nnn.*,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) No_Motorista ',
  'FROM vsinnnfs nnn ',
  'LEFT JOIN entidades ent ON ent.Codigo=nnn.Motorista ',
  'WHERE nnn.Controle=' + Geral.FF0(QrVSInnItsControle.Value),
  ' ']);
  //
  QrVSInnNFs.Locate('Conta', Conta, []);
end;

procedure TFmVSInnCab.ReopenEFdInnCTsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnctscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFsMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrVSInnCabCodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(QrVSInnCabMovimCod.Value), // N�o precisa
  ' ']);
  //
  QrEfdInnCTsCab.Locate('Controle', Controle, []);
end;

procedure TFmVSInnCab.ReopenEfdInnNFsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnnfscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrVSInnCabCodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(QrVSInnCabMovimCod.Value), // N�o precisa
  ' ']);
  //
  QrEfdInnNFsCab.Locate('Controle', Controle, []);
end;

procedure TFmVSInnCab.ReopenEfdInnNFsIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdinnnfsits     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
  '']);
  //
  QrEfdInnNFsIts.Locate('Conta', Conta, []);
end;

procedure TFmVSInnCab.ReopenVSItsBxa(Controle: Integer);
const
  SrcMovID = TEstqMovimID.emidCompra;
  MovimNiv15 = TEstqMovimNiv.eminBaixCurtiXX;
  MovimNiv29 = TEstqMovimNiv.eminSorcCal;
  //corrigir baixa
var
  SrcNivel1, SrcNivel2, LocCtrl, TemIMEIMrt: Integer;
begin
  SrcNivel1  := QrVSInnItsCodigo.Value;
  SrcNivel2  := QrVSInnItsControle.Value;
  TemIMEIMrt := QrVSInnCabTemIMEIMrt.Value;
  //
  VS_CRC_PF.ReopenVSItsBxa(
    QrVSItsBxa, SrcMovID, SrcNivel1, SrcNivel2, TemIMEIMrt,
    [MovimNiv15, MovimNiv29], Controle);
end;

procedure TFmVSInnCab.ReopenVSSubPrdIts(Controle: Integer);
var
  GSPInnNiv2, TemIMEIMrt: Integer;
begin
{$IfDef sAllVS}
  GSPInnNiv2 := QrVSInnItsControle.Value;
  TemIMEIMrt := QrVSInnCabTemIMEIMrt.Value;
  VS_PF.ReopenVSSubPrdIts(QrVSSubPrdIts, GSPInnNiv2, TemIMEIMrt, Controle);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.Resultados1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.MostraFormVSImpResultVS();
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.DBGInnNFsCabEnter(Sender: TObject);
begin
  PCBottom.ActivePageIndex := 1;
end;

procedure TFmVSInnCab.DBGBaixaDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DBGBaixa.Columns[THackDBGrid(DBGBaixa).Col-1].FieldName;
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(QrVSItsBxaControle.Value);
end;

procedure TFmVSInnCab.DefineONomeDoForm;
begin
end;

procedure TFmVSInnCab.DefineVarDup();
begin
  IC3_ED_FatNum := QrVSInnCabMovimCod.Value;
  IC3_ED_NF     := QrVSInnCabemi_nNF.Value;
  IC3_ED_Data   := QrVSInnCabDtViagem.Value;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSInnCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSInnCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSInnCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSInnCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSInnCab.SubQuery1Reopen();
begin
  // Compatibilidade deprecada?
end;

procedure TFmVSInnCab.TPDtCompraRedefInPlace(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    TPDtViagem.Date  := TPDtCompra.Date;
    TPDtEntrada.Date := TPDtCompra.Date;
  end;
end;

procedure TFmVSInnCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnCab.BtSubProdutoClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSubProduto, BtSubProduto);
end;

procedure TFmVSInnCab.BtTribIncItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTribIncIts, BtTribIncIts);
end;

procedure TFmVSInnCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSInnCabCodigo.Value;
  Close;
end;

procedure TFmVSInnCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSInnIts(stIns);
end;

procedure TFmVSInnCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSInnCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'VSInnCab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSInnCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSInnCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_Serie, ide_nNF, emi_Serie, emi_nNF: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_Serie      := Edide_Serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_Serie      := Edemi_Serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if not VS_CRC_PF.ValidaCampoNF(5, Edide_nNF, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsinncab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinncab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_Serie', 'ide_nNF',
  'emi_Serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta, (* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)
  ClienteMO, Procednc, Motorista,
  Placa, ide_Serie, ide_nNF,
  emi_Serie, emi_nNF], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'ClientMO', 'Terceiro',
      CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, ClienteMO, Terceiro,
      DataHora], [MovimCod], True);
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormVSInnIts(stIns)
    else
    begin
      QrVSInnIts.First;
      while not QrVSInnIts.Eof do
      begin
        VS_CRC_PF.AtualizaDescendentes(QrVSInnItsControle.Value, ['ClientMO'], [ClienteMO]);
        //
        QrVSInnIts.Next;
      end;
    end;
    if QrVSInnCabClienteMO.Value <> 0 then
    begin
      QrVSInnIts.First;
      while not QrVSInnIts.Eof do
      begin
        DmModVS_CRC.AtualizaVSMovIts_CusEmit(QrVSInnItsControle.Value);
        QrVSInnIts.Next;
      end;
    end;
  end;
end;

procedure TFmVSInnCab.BtCTeClick(Sender: TObject);
begin
  PCBottom.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMCTe, BtCTe);
end;

procedure TFmVSInnCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'VSInnCab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'VSInnCab', 'Codigo');
end;

procedure TFmVSInnCab.BtFinanceiroClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 4;
  MyObjects.MostraPopUpDeBotao(PMFinanceiro, BtFinanceiro);
end;

procedure TFmVSInnCab.BtFiscalClick(Sender: TObject);
begin
  PCBottom.ActivePageIndex := 1;
  MyObjects.MostraPopupDeBotao(PMFiscal, BtFiscal);
end;

procedure TFmVSInnCab.BtItsClick(Sender: TObject);
begin
  if PCItens.ActivePageIndex = 4 then
    PCItens.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSInnCab.BtReclasifClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmVSInnCab.Altera1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stUpd);
end;

procedure TFmVSInnCab.Altera2Click(Sender: TObject);
var
  CliInt, Terceiro, Cod, Genero, GenCtb: Integer;
  Valor: Double;
  Descricao, SerieNF: String;
  Qtde, Qtd2: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissMControle.Value;
  //
  DefineVarDup;
  //
  Genero    := Dmod.QrControleCtaMPCompr.Value;
  GenCtb    := QrEmissMGenCtb.Value;
  Cod       := QrVSInnCabCodigo.Value;
  Terceiro  := QrVSInnCabFornecedor.Value;
  Valor     := QrEmissMDebito.Value;
  CliInt    := QrEmissMCliInt.Value;
  Descricao := QrEmissMDescricao.Value;
  SerieNF   := QrEmissMSerieNF.Value;
  Qtde      := QrEmissMQtde.Value;
  Qtd2      := QrEmissMQtd2.Value;
  //
  UPagtos.Pagto(QrEmissM, tpDeb, Cod, Terceiro, FEFDInnNFSMainFatID, Genero, GenCtb, stUpd,
    'Compra de Mat�ria-Prima', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
    True, False, 0, 0, 0, 0, 0, FTabLctA,
    Descricao, SerieNF, Qtde, Qtd2);
  SubQuery1Reopen;
end;

procedure TFmVSInnCab.Altera3Click(Sender: TObject);
var
  CliInt, Terceiro, Cod, Genero, GenCtb: Integer;
  Valor: Double;
  Descricao, SerieNF: String;
  //Qtde, Qtd2: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissTControle.Value;
  //
  DefineVarDup;
  //
  Genero    := Dmod.QrControleCtaMPFrete.Value;
  GenCtb    := QrEmissTGenCtb.Value;
  Cod       := QrVSInnCabCodigo.Value;
  Terceiro  := QrVSInnCabFornecedor.Value;
  Valor     := QrEmissTDebito.Value;
  CliInt    := QrEmissTCliInt.Value;
  Descricao := QrEmissTDescricao.Value;
  SerieNF   := ''; //QrEmissTSerieNF.Value;
  //
  UPagtos.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_1004, Genero, GenCtb, stUpd,
    'Frete de Mat�ria-Prima', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
    True, False, 0, 0, 0, 0, 0, FTabLctA,
    Descricao, SerieNF); //, Qtde, Qtd2);
  SubQuery1Reopen;
end;

procedure TFmVSInnCab.Altera4Click(Sender: TObject);
var
  CliInt, Terceiro, Cod, Genero, GenCtb: Integer;
  Valor: Double;
  Descricao, SerieNF: String;
  //Qtde, Qtd2: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  IC3_ED_Controle := QrEmissCControle.Value;
  //
  DefineVarDup;
  //
  Genero    := Dmod.QrControleCtaComisCMP.Value;
  GenCtb    := QrEmissCGenCtb.Value;
  Cod       := QrVSInnCabCodigo.Value;
  Terceiro  := QrVSInnCabFornecedor.Value;
  Valor     := QrEmissCDebito.Value;
  CliInt    := QrEmissCCliInt.Value;
  Descricao := QrEmissCDescricao.Value;
  SerieNF   := ''; //QrEmissCSerieNF.Value;
  //
  UPagtos.Pagto(QrEmissC, tpDeb, Cod, Terceiro, VAR_FATID_1010, Genero, GenCtb, stUpd,
    'Comiss�o Compra MP ', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
    True, False, 0, 0, 0, 0, 0, FTabLctA,
    Descricao, SerieNF); //, Qtde, Qtd2);
  SubQuery1Reopen;
end;


procedure TFmVSInnCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSInnIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, 0, 0);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSInnCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stUpd, QrVSInnIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo, (*Terceiro*)0, (*CabSerieRem*)0, (*CabNFeRem*)0);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSInnCabMovimCod.Value, 0);
  //
  LocCod(QrVSInnCabCodigo.Value,QrVSInnCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.AlteraConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stIns);
end;

procedure TFmVSInnCab.AlteraDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stUpd);
end;

procedure TFmVSInnCab.AlteraNFe1Click(Sender: TObject);
begin
  MostraFormVSInnNFs(stUpd);
end;

procedure TFmVSInnCab.AlteraoItemselecionadododocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stUpd, False);
end;

procedure TFmVSInnCab.AlteraSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  Controle, VSMulFrnCab: Integer;
  OriMarca: String;
begin
{$IfDef sAllVS}
  DataHora    := 0;
  GSPSrcMovID := TEstqMovimID(0);
  GSPSrcNiv2  := 0;
  Codigo      := 0;
  MovimCod    := 0;
  Empresa     := 0;
  ClientMO    := 0;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  Ficha       := 0;
  SerieFch    := 0;
  Controle    := QrVSSubPrdItsControle.Value;
  OriMarca    := '';
  //
  VS_PF.MostraFormVSInnSubPrdIts_Uni(stUpd, DataHora, QrCab, QrVSSubPrdIts, DsCab,
  GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
  VSMulFrnCab, Ficha, SerieFch, Controle,
  (*OriGGX, OriSerieFch, OriFicha,*) OriMarca);
  //ReopenVSSubPrdIts(0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.AlteraTributoAtual1Click(Sender: TObject);
var
  Operacao, Tributo, FatID, FatNum, FatParcela, Empresa, Controle: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib: Double;
begin
{$IfDef sAllVS}
  FatNum     := QrTribIncItsFatNum.Value;
  FatParcela := QrTribIncItsFatParcela.Value;
  Empresa    := QrTribIncItsEmpresa.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Controle   := QrTribIncItsControle.Value;
  Operacao   := QrTribIncItsOperacao.Value;
  Tributo    := QrTribIncItsTributo.Value;
  ValorFat   := QrTribIncItsValorFat.Value;
  BaseCalc   := QrTribIncItsBaseCalc.Value;
  Percent    := QrTribIncItsPercent.Value;
  VUsoCalc   := QrTribIncItsVUsoCalc.Value;
  ValrTrib   := QrTribIncItsValrTrib.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stUpd, QrTribIncIts, FEFDInnNFSMainFatID,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siPositivo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.AnliseMPAG1Click(Sender: TObject);
begin
  ImprimeAmaliseMPAGModelo1(False);
end;

procedure TFmVSInnCab.AnliseMPAG2Click(Sender: TObject);
begin
  PreparaModelo2X();
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_006_02, [
    DModG.frxDsDono,
    frxDsVSInnCab,
    frxDsVSInnIts,
    frxDsVSItsBxa,
    frxDsVSItsGer,
    //
    frxDsReceitas,
    frxDsDespesas,
    frxDsVSGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_006_02, 'An�lise MPAG Modelo 2');
end;

procedure TFmVSInnCab.AnliseMPAGdoIMEIselecionadoModelo1A1Click(
  Sender: TObject);
begin
  ImprimeAmaliseMPAGModelo1(True);
end;

procedure TFmVSInnCab.AnliseMPAGdoIMEIselecionadoModelo2A1Click(
  Sender: TObject);
begin
  PreparaModelo2X();
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_006_02_A, [
    DModG.frxDsDono,
    frxDsVSInnCab,
    frxDsVSInnIts,
    frxDsVSItsBxa,
    frxDsVSItsGer,
    //
    frxDsReceitas,
    frxDsDespesas,
    frxDsVSGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_006_02_A, 'An�lise MPAG Modelo 2-A');
end;

procedure TFmVSInnCab.AtrelamentoFreteCompra1Click(Sender: TObject);
begin
  //AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  if PCItens.ActivePage <> TsFrCompr then
    Geral.MB_Aviso('Selecione a guia "' + TsFrCompr.Caption + '"')
end;

procedure TFmVSInnCab.AtrelamentoNFsdeMO1Click(Sender: TObject);
begin
  //AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
  if PCItens.ActivePage <> TsEnvioMO then
    Geral.MB_Aviso('Selecione a guia "' + TsEnvioMO.Caption + '"')
end;

procedure TFmVSInnCab.Atualizaestoque1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaSaldoIMEI(QrVSInnItsControle.Value, True);
end;

procedure TFmVSInnCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSInnCabMovimCod.Value, TEstqMovimID.emidCompra, [(**)], [eminSemNiv]);
end;

procedure TFmVSInnCab.AtualizaSaldoIts(Controle: Integer);
var
  Qry: TmySQLQuery;
  Pecas, PesoKg, ValorT: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(ValorT) ValorT ',
    'FROM vsinnnfs ',
    'WHERE Controle=' + Geral.FF0(Controle),
    ' ']);
    Pecas  := Qry.FieldByName('Pecas').AsFloat;
    PesoKg := Qry.FieldByName('PesoKg').AsFloat;
    ValorT := Qry.FieldByName('ValorT').AsFloat;
    // N�o zerar Pecas e Peso!
    if (Pecas > 0) or (PesoKg > 0) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      //'InfPecas', 'InfPesoKg', 'InfValorT',
      'ValorMP', 'ValorT'], ['Controle'], [
      //Pecas, PesoKg, ValorT,
      ValorT, ValorT], [Controle], True) then
      begin
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'vsmovdif', False, [
        'InfPecas', 'InfPesoKg', (*'InfAreaM2',
        'InfAreaP2',*) 'InfValorT'(*, 'DifPecas',
        'DifPesoKg', 'DifAreaM2', 'DifAreaP2',
        'DifValorT', 'PerQbrViag', 'PerQbrSal',
        'RstCouVl', 'RstSalKg', 'RstSalVl',
        'RstTotVl', 'TribDefSel', 'PesoSalKg',
        'RstCouPc', 'RstCouKg' *)], [
        'Controle'], [
        'InfPecas', 'InfPesoKg', (*'InfAreaM2',
        'InfAreaP2',*) 'InfValorT'(*, 'DifPecas',
        'DifPesoKg', 'DifAreaM2', 'DifAreaP2',
        'DifValorT', 'PerQbrViag', 'PerQbrSal',
        'RstCouVl', 'RstSalKg', 'RstSalVl',
        'RstTotVl', 'TribDefSel', 'PesoSalKg',
        'RstCouPc', 'RstCouKg'*)], [
        Pecas, PesoKg, (*InfAreaM2,
        InfAreaP2,*) ValorT(*, DifPecas,
        DifPesoKg, DifAreaM2, DifAreaP2,
        DifValorT, PerQbrViag, PerQbrSal,
        RstCouVl, RstSalKg, RstSalVl,
        RstTotVl, TribDefSel, PesoSalKg,
        RstCouPc, RstCouKg*)], [
        Controle], [
        Pecas, PesoKg, (*InfAreaM2,
        InfAreaP2,*) ValorT(*, DifPecas,
        DifPesoKg, DifAreaM2, DifAreaP2,
        DifValorT, PerQbrViag, PerQbrSal,
        RstCouVl, RstSalKg, RstSalVl,
        RstTotVl, TribDefSel, PesoSalKg,
        RstCouPc, RstCouKg*)], True);
       //
       VS_CRC_PF.AtualizaTotaisVSXxxCab('vsinncab', QrVSInnCabMovimCod.Value);
       VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
       //
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSInnCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSInnCab, QrVSInnCabDtEntrada.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSInnCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PCItens.Align := alClient;
  //PCItens.ActivePageIndex := 0;
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_VS_NAME, 0);
  PCSubItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  QrVSInnCab.Database  := Dmod.MyDB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
end;

procedure TFmVSInnCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInnCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSInnCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSInnCab.SbNovoClick(Sender: TObject);
{  Teste de v�rias inclus�es. Para Nota MPAG?
var
  SQLType: TSQLType;
  Qry: TmySQLQuery;
  //
  function ObtemFornecedorRand(): Integer;
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, RAND() Randomico ',
    'FROM Entidades ',
    'WHERE Codigo>0 ',
    'AND Fornece1="V" ',
    'ORDER BY Randomico DESC ',
    'LIMIT 0, 1 ',
    '']);
    //
    Result := Qry.FieldByName('Codigo').AsInteger;
  end;
  function ObtemVSNatCadRand(): Integer;
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGruX, RAND() Randomico ',
    'FROM vsnatcad ',
    'WHERE GraGruX>0  ',
    'ORDER BY Randomico DESC ',
    'LIMIT 0, 1 ',
    '']);
    //
    Result := Qry.FieldByName('GraGruX').AsInteger;
  end;
  function ObtemSVSerFchRand(): Integer;
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, RAND() Randomico',
    'FROM vsserfch',
    'WHERE Codigo>0 ',
    'ORDER BY Randomico DESC',
    'LIMIT 0, 1',
    '']);
    //
    Result := Qry.FieldByName('Codigo').AsInteger;
  end;

  procedure InsereItem(DataHora: String; Codigo, MovimCod, Empresa, ClientMO, Fornecedor: Integer);
  const
    SrcMovID   = TEstqMovimID(0);
    SrcNivel1  = 0;
    SrcNivel2  = 0;
    SrcGGX     = 0;
    DstMovID   = TEstqMovimID(0);
    DstNivel1  = 0;
    DstNivel2  = 0;
    DstGGX     = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    MovimTwn   = 0;
    Pallet     = 0;
    AreaM2     = 0;
    AreaP2     = 0;
    CustoMOKg  = 0;
    CustoMOM2  = 0;
    CustoMOTot = 0;
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    QtdGerArM2 = 0;
    QtdGerArP2 = 0;
    AptoUso    = 1;
    FornecMO   = 0;
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    ExigeAreaouPeca = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdAreaM2 = nil;
    //
    InfAreaM2 = 0;
    InfAreaP2 = 0;
    PerQbrViag = 2;
    PerQbrSal  = 0;
    //
    PedItsLib  = 0;
    PedItsFin  = 0;
    PedItsVda  = 0;
    //
    GSPSrcMovID = TEstqMovimID(0);
    GSPSrcNiv2 = 0;
    //
    ReqMovEstq = 0;
    Stq CenLoc  = 0;
    //
    RstCouPc = 0;
    RstCouKg = 0;
    RstCouVl = 0;
    RstSalKg = 0;
    RstSalVl = 0;
    RstTotVl = 0;
    TribDefSel  = 0;
    PesoSalKg   = 0;
    ItemNFe     = 0;
    VSMulFrnCab = 0;
    //
    QtdAntPeca = 0;
    QtdAntPeso = 0;
    QtdAntArM2 = 0;
    QtdAntArP2 = 0;
  var
    Observ, Marca: String;
    Controle, GraGruX, SerieFch, Ficha(*,
    Misturou*): Integer;
    Pecas, PesoKg, ValorT: Double;
    MovimID: TEstqMovimID;
    MovimNiv: TEstqMovimNiv;
    //
    InfPecas, InfPesoKg, InfValorT: Double;
    Atenua: Integer;
  begin
    //Codigo         :=
    Controle       := 0;
    //MovimCod       :=
    //Empresa        := FEmpresa;
    //Fornecedor     := ;
    //DataHora       := Geral.FDT(FDataHora, 109);
    MovimID        := emidCompra;
    MovimNiv       := eminSemNiv;
    GraGruX        := ObtemVSNatCadRand();
    Randomize();
    Pecas          := Trunc(100 + Random(450));
    PesoKg         := Pecas * (18 + Random(40));
    ValorT         := PesoKg * (2.80 + Random(1));
    Observ         := '';
    SerieFch       := ObtemSVSerFchRand();
    VS_CRC_PF.ObtemProximaFichaRMP(Empresa, nil, Ficha, SerieFch);
    Marca          := '';
    //Misturou       := RGMisturou.ItemIndex;
    //
    Atenua := Random(5);
    if Atenua = 5 then
    begin
      InfPecas  := Pecas - Round(10);
      InfPesoKg := (PesoKg / Pecas) * InfPecas;
      InfValorT := (ValorT / Pecas) * InfPecas;
    end else
    begin
      InfPecas  := Pecas;
      InfPesoKg := PesoKg;
      InfValorT := ValorT;
    end;

(*
    if VS_CRC_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha) then
      Exit;
    //
    if VS_CRC_PF.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
    PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
    EdPesoKg, EdValorT, ExigeFornecedor, CO_GraGruY_1024_VSNatCad,
    ExigeAreaouPeca) then
      Exit;
    //
*)
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
    if VS_CRC_PF.InsUpdVSMovIts_(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
    SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
    AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl) then
    begin
      VS_CRC_PF.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
      VS_CRC_PF.InsereVSMovDif(Controle, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
      InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT, PerQbrViag,
      PerQbrSal,
      RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl, RstTotVl, TribDefSel,
      PesoSalKg);
    end;
  end;
var
  Data: TDateTime;
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, Itens, I: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
}
begin
{
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSGerArtCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    //
    Data := EncodeDate(2013, 12, 25);
    while Data < EncodeDate(2014, 12, 31) do
    begin
      Randomize;
      Itens := Random(3);
      Data := Data + 1;
      DtCompra       := Geral.FDT(Data, 1);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, DtCompra);
      for I := 1 to Itens do
      begin
        SQLType        := stIns;
        Codigo         := 0;
        MovimCod       := EdMovimCod.ValueVariant;
        Empresa        := -11;
        DtViagem       := DtCompra;
        DtEntrada      := DtCompra;
        Fornecedor     := ObtemFornecedorRand();
        Transporta     := 0;
        ClienteMO      := 0;
        Procednc       := 0;
        Motorista      := 0;
        Placa          := '';
        //
        Codigo := UMyMod.BPGS1I32('vsinncab', 'Codigo', '', '', tsPos, SQLType, Codigo);
        MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinncab', False, [
        'MovimCod', 'Empresa', 'DtCompra',
        'DtViagem', 'DtEntrada', 'Fornecedor',
        'Transporta',(*, 'Pecas', 'PesoKg',
        'AreaM2', 'AreaP2', 'ValorT'*)
        'ClienteMO', 'Procednc', 'Motorista',
        'Placa'
        ], [
        'Codigo'], [
        MovimCod, Empresa, DtCompra,
        DtViagem, DtEntrada, Fornecedor,
        Transporta, (* (*Pecas, PesoKg,
        AreaM2, AreaP2, ValorT*)
        ClienteMO, Procednc, Motorista,
        Placa
        ], [
        Codigo], True) then
        begin
          VS_CRC_PF.InsereVSMovCab(MovimCod, emidCompra, Codigo);
          DataHora := DtEntrada;
          Terceiro := Fornecedor;
          //
          InsereItem(DtEntrada, Codigo, MovimCod, Empresa, ClienteMO, Fornecedor);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
}
end;

procedure TFmVSInnCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSInnCab.QrEmissCCalcFields(DataSet: TDataSet);
begin
  QrEmissCNOMESIT.Value := UFinanceiro.NomeSitLancto(QrEmissCSit.Value,
    QrEmissCTipo.Value, 0(*QrEmissCPrazo.Value*), QrEmissCVencimento.Value,
    0(*QrEmissCReparcel.Value*));
end;

procedure TFmVSInnCab.QrEmissMCalcFields(DataSet: TDataSet);
begin
  QrEmissMNOMESIT.Value := UFinanceiro.NomeSitLancto(QrEmissMSit.Value,
    QrEmissMTipo.Value, 0(*QrEmissMPrazo.Value*), QrEmissMVencimento.Value,
    0(*QrEmissMReparcel.Value*));
end;

procedure TFmVSInnCab.QrEmissTCalcFields(DataSet: TDataSet);
begin
  QrEmissTNOMESIT.Value := UFinanceiro.NomeSitLancto(QrEmissTSit.Value,
    QrEmissTTipo.Value, 0(*QrEmissTPrazo.Value*), QrEmissTVencimento.Value,
    0(*QrEmissTReparcel.Value*));
end;

procedure TFmVSInnCab.QrReceitasCalcFields(DataSet: TDataSet);
var
  Base: Double;
begin
  if QrVSInnItsPecas.Value > 0 then
    Base := QrReceitasPesoKg.Value / QrVSInnItsPecas.Value
  else
    Base := 0;
  if QrReceitasMetaDivKgPc.Value <> 0 then
    QrReceitasResDivKgPc.Value := Base / QrReceitasMetaDivKgPc.Value * 100
  else
    QrReceitasResDivKgPc.Value := 0;
  //
  if QrVSInnItsPesoKg.Value > 0 then
    Base := QrReceitasPesoKg.Value / QrVSInnItsPesoKg.Value * 100
  else
    Base := 0;
  if QrReceitasMetaPerKgKg.Value <> 0 then
    QrReceitasResPerKgKg.Value := Base / QrReceitasMetaPerKgKg.Value * 100
  else
    QrReceitasResPerKgKg.Value := 0;
end;

procedure TFmVSInnCab.QrVSInnCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtFinanceiro.Enabled := QrVSInnCab.RecordCount > 0;
end;

procedure TFmVSInnCab.QrVSInnCabAfterScroll(DataSet: TDataSet);
begin
  if QrVSInnCabCodCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrVSInnCabCodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenVSInnIts(0);
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSInnCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSInnCabMovimCod.Value, 0);
  ReopenEmissM(0);
  ReopenEmissT(0);
  ReopenEmissC(0);
  ReopenEfdInnNFsCab(0);
  ReopenEFdInnCTsCab(0);
end;

procedure TFmVSInnCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSInnCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSInnCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidCompra, [], stPsq, Codigo, Controle,
  False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSInnIts(Controle);
  end;
end;

procedure TFmVSInnCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSInnCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInnCab.frxWET_CURTI_006_01GetValue(const VarName: string;
  var Value: Variant);
var
  Valor, AreaM2, ValM2: Double;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_REGISTROS_GERADOS' then
    Value := QrVSGerados.RecordCount
  else
  if VarName ='VARF_KG_ITEM_ORI' then
  begin
    case TEstqMovimNiv(QrVSMovItsMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrVSMovItsKgCouPQ.Value;
      else Value := 0;
    end;
  end else
  if VarName ='VARF_KG_ITEM_ORI_D' then
  begin
    case TEstqMovimNiv(QrDespesasMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrDespesasKgCouPQ.Value;
      else Value := 0;
    end;
  end else
  if VarName ='VARF_KG_ITEM_DST' then
  begin
    case TEstqMovimNiv(QrVSMovItsMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrVSMovItsQtdAntPeso.Value;
      else Value := 0;
    end;
  end else
  if VarName ='VARF_KG_ITEM_DST_D' then
  begin
    case TEstqMovimNiv(QrDespesasMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrDespesasQtdAntPeso.Value;
      else Value := 0;
    end;
  end else
  if VarName ='VARF_VAL_ITEM' then
  begin
    case TEstqMovimNiv(QrVSMovItsMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrVSMovItsCustoPQ.Value;
      eminDestCurtiXX,
      eminSorcCurtiXX,
      eminBaixCurtiXX: Value := 0;
      else Value := QrVSMovItsCustoMOTot.Value;
    end;
  end else
  if VarName ='VARF_VAL_ITEM_C' then
  begin
    case TEstqMovimNiv(QrReceitasMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrReceitasCustoPQ.Value;
      eminDestCurtiXX,
      eminSorcCurtiXX,
      eminBaixCurtiXX: Value := 0;
      else Value := QrReceitasCustoMOTot.Value;
    end;
  end else
  if VarName ='VARF_VAL_ITEM_D' then
  begin
    case TEstqMovimNiv(QrDespesasMovimNiv.Value) of
      eminSorcCal,
      eminSorcCur: Value := QrDespesasCustoPQ.Value;
      eminDestCurtiXX,
      eminSorcCurtiXX,
      eminBaixCurtiXX: Value := 0;
      else Value := QrDespesasCustoMOTot.Value;
    end;
  end else
  if VarName ='VARF_SemGelatina_KG_PC' then
    Value := Geral.FFT(QrMediasKg_PC.Value, 3, siPositivo)
  else
  if VarName = 'VARF_SemGelatina_KG_M2' then
    Value := Geral.FFT(QrMediasKg_M2.Value, 3, siPositivo)
  else
  if VarName = 'VARF_SemGelatina_M2_PC' then
    Value := Geral.FFT(QrMediasM2_PC.Value, 2, siPositivo)
{
  else
  if VarName = 'VARF_REAIS_M2_UNI' then
  begin
    Valor := 0.0000;
    ValM2 := 0.00;
    AreaM2 := 0.00;
    //QrVSGerados.First;
    //while not QrVSGerados.Eof do
    //begin
      AreaM2 := AreaM2 + QrVSGeradosAreaM2.Value;
      //
      //QrVSGerados.Next;
    //end;
    //QrVSGerados.First;
    if AreaM2 > 0 then
    begin
      case TEstqMovimNiv(QrDespesasMovimNiv.Value) of
        eminSorcCal,
        eminSorcCur:
        begin
          //QrDespesas.First;
          //while not QrDespesas.Eof do
          //begin
           Valor := Valor + QrDespesasCustoPQ.Value;
           //
           //QrDespesas.Next;
          //end;
          //QrDespesas.First;
        end;
        eminDestCurtiXX,
        eminSorcCurtiXX,
        eminBaixCurtiXX: Valor := 0;
        //else Valor := QrDespesasCustoMOTot.Value; Fazer?
        else Valor := 0;
      end;
      Valor := (-Valor) + QrVSInnItsValorT.Value;
      ValM2 := Valor / AreaM2;
    end;
    Value := Geral.FFT(ValM2, 2, siNegativo)
  end
}
  else
  if VarName = 'VARF_REAIS_M2_TOT' then
  begin
    Valor := 0.0000;
    ValM2 := 0.00;
    AreaM2 := 0.00;
    QrVSGerados.First;
    while not QrVSGerados.Eof do
    begin
      AreaM2 := AreaM2 + QrVSGeradosAreaM2.Value;
      //
      QrVSGerados.Next;
    end;
    QrVSGerados.First;
    if AreaM2 > 0 then
    begin
      case TEstqMovimNiv(QrDespesasMovimNiv.Value) of
        eminSorcCal,
        eminSorcCur:
        begin
          QrDespesas.First;
          while not QrDespesas.Eof do
          begin
           Valor := Valor + QrDespesasCustoPQ.Value;
           //
           QrDespesas.Next;
          end;
          QrDespesas.First;
        end;
        eminDestCurtiXX,
        eminSorcCurtiXX,
        eminBaixCurtiXX: Valor := 0;
        //else Valor := QrDespesasCustoMOTot.Value; Fazer?
        else Valor := 0;
      end;
      Valor := (-Valor) + QrVSInnItsValorT.Value;
      ValM2 := Valor / AreaM2;
    end;
    Value := Geral.FFT(ValM2, 2, siNegativo)
  end
  else

end;

procedure TFmVSInnCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSInnCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSInnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'VSInnCab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  EdDtEntrada.ValueVariant := Agora;
  TPDtCompra.Date := Agora;
  EdDtCompra.ValueVariant := Agora;
  TPDtViagem.Date := Agora;
  EdDtViagem.ValueVariant := Agora;
  //EdDtCompra.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

function TFmVSInnCab.CalculaDiferencas(tPag: TAquemPag): Double;
var
  //N, B, L,
  M, T, C: Double;
begin
  //DistribuiCustoDoFrete; > j� � por item.
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrConfT, Dmod.MyDB, [
  'SELECT SUM(TotalPeso)  PesoL, ',
  'SUM(Volumes*PesoVB) PesoB, ',
  'SUM(TotalCusto) TotalCusto, ',
  'SUM(ValorItem + IPI_vIPI) ValorItem ',
  'FROM pqeits ',
  'WHERE Codigo=' + Geral.FF0(QrPQECodigo.Value),
  '']);
}
  //
  M := 0;
  T := 0;
  C := 0;
  //
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumM, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(FEFDInnNFSMainFatID),
      'AND FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1004),
      'AND FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumC, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_1010),
      'AND FatNum=' + Geral.FF0(QrVSInnCabMovimCod.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    if QrSumMDebito.Value <> 0 then
      M := QrSumMDebito.Value - QrVSInnCabValorMP.Value;
    if QrSumTDebito.Value <> 0 then
      T := QrSumTDebito.Value - QrVSInnCabCusFrtAvMoER.Value;
    if QrSumCDebito.Value <> 0 then
      C := QrSumCDebito.Value - QrVSInnCabCustoComiss.Value;
  end;
  //
  EdPagM.Text   := Geral.TFT(FloatToStr(M), 2, siNegativo);
  EdPagT.Text   := Geral.TFT(FloatToStr(T), 2, siNegativo);
  EdPagC.Text   := Geral.TFT(FloatToStr(C), 2, siNegativo);
  //
  M := QrVSInnCabValorMP.Value      - QrSumMDebito.Value;
  T := QrVSInnCabCusFrtAvMoER.Value - QrSumTDebito.Value;
  C := QrVSInnCabCustoComiss.Value  - QrSumCDebito.Value;
  if M < 0 then M := 0;
  if T < 0 then T := 0;
  if C < 0 then C := 0;
  //
  if tPag = apCliente then Result := M else
  if tPag = apFornece then Result := M else
  if tPag = apTranspo then Result := T else
  if tPag = apComisao then Result := C else
  if tPag = apNinguem then Result := 0 else
  //
  Result := 0;
end;

procedure TFmVSInnCab.ClassesGeradas1Click(Sender: TObject);
const
  MostraJanela = True;
var
  Fichas: array of Integer;
begin
  SetLength(Fichas, QrVSInnIts.RecordCount);
  try
    QrVSInnIts.DisableControls;
    QrVSInnIts.First;
    while not QrVSInnIts.Eof do
    begin
      Fichas[QrVSInnIts.RecNo -1] := QrVSInnItsFicha.Value;
      //
      QrVSInnIts.Next;
    end;
  finally
    QrVSInnIts.EnableControls;
  end;
  VS_PF.ImprimeClassFichaRMP_Mul_New(QrVSInnItsSerieFch.Value,
    QrVSInnItsNO_SerieFCH.Value, Fichas, MostraJanela,
    PB1, LaAviso1, LaAviso2);
end;

procedure TFmVSInnCab.Corrigepesodebaixas1Click(Sender: TObject);
var
  PecasOri, PesoKgOri, PecasBxa, PesoKgBxa: Double;
  Controle, DstNivel2: Integer;
  //Qry: TmySQLQuery;
begin
  Screen.Cursor := crHourGlass;
  try
    //Qry := TmySQLQuery.Create(Dmod);
    //try
      PecasOri  := QrVSInnCabPecas.Value;
      PesoKgOri := QrVSInnCabPesoKg.Value;
      //
      QrVSItsBxa.First;
      while not QrVSItsBxa.Eof do
      begin
        PecasBxa  := QrVSItsBxaPecas.Value;
        if PecasOri = 0 then
          PesoKgBxa := 0
        else
          PesoKgBxa := PecasBxa / PecasOri * PesoKgOri;
        //
        Controle  := QrVSItsBxaControle.Value;
        DstNivel2 := QrVSItsBxaDstNivel2.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'PesoKg'], ['Controle'], [-PesoKgBxa], [DstNivel2], True)
        then
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'PesoKg'], ['Controle'], [PesoKgBxa], [Controle], True);
        //
        QrVSItsBxa.Next;
      end;
      VS_CRC_PF.AtualizaSaldoIMEI(QrVSInnItsControle.Value, True);
      ReopenVSItsBxa(0);
    //finally
      //Qry.Free;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSInnCab.QrVSInnCabBeforeClose(
  DataSet: TDataSet);
begin
  BtFinanceiro.Enabled := False;
  //
  QrVSMOEnvEnv.Close;
  QrVSMOEnvAvu.Close;
  QrVSInnIts.Close;
  QrEmissM.Close;
  QrEmissT.Close;
  QrEmissC.Close;
  QrEfdInnNFsCab.Close;
  QrEfdInnCTsCab.Close;
end;

procedure TFmVSInnCab.QrVSInnCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSInnCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSInnCab.QrVSInnItsAfterScroll(DataSet: TDataSet);
begin
{$IfDef sAllVS}
  ReopenVSItsBxa(0);
  VS_PF.ReopenVSMovDif(QrVSMovDif, QrVSInnItsControle.Value);
  ReopenVSHisFch(0);
  ReopenVSSubPrdIts(0);
  ReopenTribIncIts(0);
  ReopenVSInnNFs(0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSInnCab.QrVSInnItsBeforeClose(DataSet: TDataSet);
begin
  QrVSItsBxa.Close;
  QrVSMovDif.Close;
  QrVSHisFch.Close;
  QrVSSubPrdIts.Close;
  QrTribIncIts.Close;
  QrVSInnNFs.Close;
end;

procedure TFmVSInnCab.QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenEfdInnNFsIts(0);
end;

procedure TFmVSInnCab.QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
begin
  QrEfdInnNFsIts.Close;
end;

procedure TFmVSInnCab.QrVSItsBxaAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSIts_Controle_If(QrVSItsGer, QrVSItsBxaDstNivel2.Value,
    QrVSInnCabTemIMEIMrt.Value);
end;

procedure TFmVSInnCab.QrVSItsBxaBeforeClose(DataSet: TDataSet);
begin
  QrVSItsGer.Close;
end;

procedure TFmVSInnCab.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi, QrVSMOEnvAvuCodigo.Value, 0);
end;

procedure TFmVSInnCab.QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSInnCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVMI(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.Value, 0);
end;

procedure TFmVSInnCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

{
object QrEfdInnNFsCab: TMySQLQuery
  Database = Dmod.MyDB
  BeforeClose = QrEfdInnNFsCabBeforeClose
  AfterScroll = QrEfdInnNFsCabAfterScroll
  SQL.Strings = (
    'SELECT vin.*, '
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
    'FROM efdinnnfscab vin '
    'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
    ' ')
  Left = 964
  Top = 286
  object QrEfdInnNFsCabNO_TER: TWideStringField
    FieldName = 'NO_TER'
    Size = 100
  end
  object QrEfdInnNFsCabMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsCabMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsCabMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsCabEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsCabControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsCabTerceiro: TIntegerField
    FieldName = 'Terceiro'
    Required = True
  end
  object QrEfdInnNFsCabPecas: TFloatField
    FieldName = 'Pecas'
    Required = True
  end
  object QrEfdInnNFsCabPesoKg: TFloatField
    FieldName = 'PesoKg'
    Required = True
  end
  object QrEfdInnNFsCabAreaM2: TFloatField
    FieldName = 'AreaM2'
    Required = True
  end
  object QrEfdInnNFsCabAreaP2: TFloatField
    FieldName = 'AreaP2'
    Required = True
  end
  object QrEfdInnNFsCabValorT: TFloatField
    FieldName = 'ValorT'
    Required = True
  end
  object QrEfdInnNFsCabMotorista: TIntegerField
    FieldName = 'Motorista'
    Required = True
  end
  object QrEfdInnNFsCabPlaca: TWideStringField
    FieldName = 'Placa'
  end
  object QrEfdInnNFsCabCOD_MOD: TSmallintField
    FieldName = 'COD_MOD'
    Required = True
  end
  object QrEfdInnNFsCabCOD_SIT: TSmallintField
    FieldName = 'COD_SIT'
    Required = True
  end
  object QrEfdInnNFsCabSER: TIntegerField
    FieldName = 'SER'
    Required = True
  end
  object QrEfdInnNFsCabNUM_DOC: TIntegerField
    FieldName = 'NUM_DOC'
    Required = True
  end
  object QrEfdInnNFsCabCHV_NFE: TWideStringField
    FieldName = 'CHV_NFE'
    Size = 44
  end
  object QrEfdInnNFsCabNFeStatus: TIntegerField
    FieldName = 'NFeStatus'
    Required = True
  end
  object QrEfdInnNFsCabDT_DOC: TDateField
    FieldName = 'DT_DOC'
    Required = True
  end
  object QrEfdInnNFsCabDT_E_S: TDateField
    FieldName = 'DT_E_S'
    Required = True
  end
  object QrEfdInnNFsCabVL_DOC: TFloatField
    FieldName = 'VL_DOC'
    Required = True
  end
  object QrEfdInnNFsCabIND_PGTO: TWideStringField
    FieldName = 'IND_PGTO'
    Size = 1
  end
  object QrEfdInnNFsCabVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsCabVL_MERC: TFloatField
    FieldName = 'VL_MERC'
    Required = True
  end
  object QrEfdInnNFsCabIND_FRT: TWideStringField
    FieldName = 'IND_FRT'
    Size = 1
  end
  object QrEfdInnNFsCabVL_FRT: TFloatField
    FieldName = 'VL_FRT'
    Required = True
  end
  object QrEfdInnNFsCabVL_SEG: TFloatField
    FieldName = 'VL_SEG'
    Required = True
  end
  object QrEfdInnNFsCabVL_OUT_DA: TFloatField
    FieldName = 'VL_OUT_DA'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS_ST: TFloatField
    FieldName = 'VL_PIS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
    FieldName = 'VL_COFINS_ST'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatID: TIntegerField
    FieldName = 'NFe_FatID'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatNum: TIntegerField
    FieldName = 'NFe_FatNum'
    Required = True
  end
  object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
    FieldName = 'NFe_StaLnk'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcWrn: TSmallintField
    FieldName = 'VSVmcWrn'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcObs: TWideStringField
    FieldName = 'VSVmcObs'
    Size = 60
  end
  object QrEfdInnNFsCabVSVmcSeq: TWideStringField
    FieldName = 'VSVmcSeq'
    Size = 25
  end
  object QrEfdInnNFsCabVSVmcSta: TSmallintField
    FieldName = 'VSVmcSta'
    Required = True
  end
  object QrEfdInnNFsCabCliInt: TIntegerField
    FieldName = 'CliInt'
    Required = True
  end
end
object DsEfdInnNFsCab: TDataSource
  DataSet = QrEfdInnNFsCab
  Left = 964
  Top = 336
end
object QrEfdInnNFsIts: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '

      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
      'e)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
    'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
    'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
    'pgt.Tipo_Item) Tipo_Item '
    'FROM efdinnnfsits     vin'
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
  Left = 1052
  Top = 286
  object QrEfdInnNFsItsMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsItsMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsItsMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsItsEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsItsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
    Required = True
  end
  object QrEfdInnNFsItsMovimTwn: TIntegerField
    FieldName = 'MovimTwn'
    Required = True
  end
  object QrEfdInnNFsItsControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsItsConta: TIntegerField
    FieldName = 'Conta'
    Required = True
  end
  object QrEfdInnNFsItsGraGru1: TIntegerField
    FieldName = 'GraGru1'
    Required = True
  end
  object QrEfdInnNFsItsGraGruX: TIntegerField
    FieldName = 'GraGruX'
    Required = True
  end
  object QrEfdInnNFsItsprod_vProd: TFloatField
    FieldName = 'prod_vProd'
    Required = True
  end
  object QrEfdInnNFsItsprod_vFrete: TFloatField
    FieldName = 'prod_vFrete'
    Required = True
  end
  object QrEfdInnNFsItsprod_vSeg: TFloatField
    FieldName = 'prod_vSeg'
    Required = True
  end
  object QrEfdInnNFsItsprod_vOutro: TFloatField
    FieldName = 'prod_vOutro'
    Required = True
  end
  object QrEfdInnNFsItsQTD: TFloatField
    FieldName = 'QTD'
    Required = True
  end
  object QrEfdInnNFsItsUNID: TWideStringField
    FieldName = 'UNID'
    Size = 6
  end
  object QrEfdInnNFsItsVL_ITEM: TFloatField
    FieldName = 'VL_ITEM'
    Required = True
  end
  object QrEfdInnNFsItsVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsItsIND_MOV: TWideStringField
    FieldName = 'IND_MOV'
    Size = 1
  end
  object QrEfdInnNFsItsCST_ICMS: TIntegerField
    FieldName = 'CST_ICMS'
  end
  object QrEfdInnNFsItsCFOP: TIntegerField
    FieldName = 'CFOP'
  end
  object QrEfdInnNFsItsCOD_NAT: TWideStringField
    FieldName = 'COD_NAT'
    Size = 10
  end
  object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ICMS: TFloatField
    FieldName = 'ALIQ_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ST: TFloatField
    FieldName = 'ALIQ_ST'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsIND_APUR: TWideStringField
    FieldName = 'IND_APUR'
    Size = 1
  end
  object QrEfdInnNFsItsCST_IPI: TWideStringField
    FieldName = 'CST_IPI'
    Size = 2
  end
  object QrEfdInnNFsItsCOD_ENQ: TWideStringField
    FieldName = 'COD_ENQ'
    Size = 3
  end
  object QrEfdInnNFsItsVL_BC_IPI: TFloatField
    FieldName = 'VL_BC_IPI'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_IPI: TFloatField
    FieldName = 'ALIQ_IPI'
    Required = True
  end
  object QrEfdInnNFsItsVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsItsCST_PIS: TSmallintField
    FieldName = 'CST_PIS'
  end
  object QrEfdInnNFsItsVL_BC_PIS: TFloatField
    FieldName = 'VL_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
    FieldName = 'ALIQ_PIS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
    FieldName = 'QUANT_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
    FieldName = 'ALIQ_PIS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsItsCST_COFINS: TSmallintField
    FieldName = 'CST_COFINS'
  end
  object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
    FieldName = 'VL_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
    FieldName = 'ALIQ_COFINS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
    FieldName = 'QUANT_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
    FieldName = 'ALIQ_COFINS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsCOD_CTA: TWideStringField
    FieldName = 'COD_CTA'
    Size = 255
  end
  object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsItsDtCorrApo: TDateTimeField
    FieldName = 'DtCorrApo'
    Required = True
  end
  object QrEfdInnNFsItsxLote: TWideStringField
    FieldName = 'xLote'
  end
  object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
    FieldName = 'Ori_IPIpIPI'
    Required = True
  end
  object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
    FieldName = 'Ori_IPIvIPI'
    Required = True
  end
  object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrEfdInnNFsItsGerBxaEstq: TSmallintField
    FieldName = 'GerBxaEstq'
  end
  object QrEfdInnNFsItsNCM: TWideStringField
    FieldName = 'NCM'
    Size = 10
  end
  object QrEfdInnNFsItsUnidMed: TIntegerField
    FieldName = 'UnidMed'
  end
  object QrEfdInnNFsItsEx_TIPI: TWideStringField
    FieldName = 'Ex_TIPI'
    Size = 3
  end
  object QrEfdInnNFsItsGrandeza: TSmallintField
    FieldName = 'Grandeza'
  end
  object QrEfdInnNFsItsTipo_Item: TSmallintField
    FieldName = 'Tipo_Item'
  end
end
object DsEfdInnNFsIts: TDataSource
  DataSet = QrEfdInnNFsIts
  Left = 1052
  Top = 336
end
}
{
object DBGInnNFsCab: TDBGrid
  Left = 0
  Top = 0
  Width = 404
  Height = 14
  Align = alClient
  TabOrder = 0
  TitleFont.Charset = DEFAULT_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -12
  TitleFont.Name = 'MS Sans Serif'
  TitleFont.Style = []
  OnEnter = DBGInnNFsCabEnter
end
object DBGInnNFsIts: TDBGrid
  Left = 0
  Top = 0
  Width = 988
  Height = 165
  Align = alClient
  TabOrder = 0
  TitleFont.Charset = DEFAULT_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -12
  TitleFont.Name = 'MS Sans Serif'
  TitleFont.Style = []
end
}
end.

