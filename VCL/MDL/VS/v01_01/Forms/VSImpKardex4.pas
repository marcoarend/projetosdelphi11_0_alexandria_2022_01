unit VSImpKardex4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, CreateVS, frxClass, frxDBSet;

type
  THackDBGrid = class(TDBGrid);
  TFmVSImpKardex4 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSMovIts: TMySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_MovimID: TWideStringField;
    QrVSMovItsMediaArM2: TFloatField;
    QrVSMovItsMediaPeso: TFloatField;
    DsVSMovIts: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrVSAnt: TMySQLQuery;
    QrVSAntPecas: TFloatField;
    QrVSAntPesoKg: TFloatField;
    QrVSAntAreaM2: TFloatField;
    QrVSAntAreaP2: TFloatField;
    QrVSAntVAlorT: TFloatField;
    QrVSAntSdoVrtPeca: TFloatField;
    QrVSAntSdoVrtPeso: TFloatField;
    QrVSAntSdoVrtArM2: TFloatField;
    QrVSAntSdoVrtArP2: TFloatField;
    DsVSAnt: TDataSource;
    DBGAnt: TdmkDBGridZTO;
    QrVSAntCusM2: TFloatField;
    DsVSAtu: TDataSource;
    QrVSAtu: TMySQLQuery;
    PB1: TProgressBar;
    frxWET_CURTI_245_A_1: TfrxReport;
    frxDsVSAtu: TfrxDBDataset;
    frxDsVSAnt: TfrxDBDataset;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    TP02DataIni: TdmkEditDateTimePicker;
    Ck02DataIni: TCheckBox;
    Ck02DataFim: TCheckBox;
    TP02DataFim: TdmkEditDateTimePicker;
    CkSomComPc: TCheckBox;
    QrVSAtuControle: TLargeintField;
    QrVSAtuDataHora: TDateTimeField;
    QrVSAtuPecasInn: TFloatField;
    QrVSAtuAreaM2Inn: TFloatField;
    QrVSAtuValorTInn: TFloatField;
    QrVSAtuCustoM2Inn: TFloatField;
    QrVSAtuPecasOut: TFloatField;
    QrVSAtuAreaM2Out: TFloatField;
    QrVSAtuValorTOut: TFloatField;
    QrVSAtuCustoM2Out: TFloatField;
    QrVSAtuAcumPeca: TFloatField;
    QrVSAtuAcumPeso: TFloatField;
    QrVSAtuAcumArM2: TFloatField;
    QrVSAtuAcumArP2: TFloatField;
    QrVSAtuAcumValorT: TFloatField;
    QrVSAtuAcumCusPc: TFloatField;
    QrVSAtuAcumCusKg: TFloatField;
    QrVSAtuAcumCusM2: TFloatField;
    QrVSAtuAcumCusP2: TFloatField;
    frxWET_CURTI_245_B_1: TfrxReport;
    QrVSAtuPesoKgInn: TFloatField;
    QrVSAtuPesoKgOut: TFloatField;
    QrVSAtuCustoKgInn: TFloatField;
    QrVSAtuCustoKgOut: TFloatField;
    QrVSAntCusKg: TFloatField;
    QrVSAtuNO_MovimID: TWideStringField;
    frxWET_CURTI_245_B_2: TfrxReport;
    CkProcesso: TCheckBox;
    frxWET_CURTI_245_A_2: TfrxReport;
    QrGraGruXGrandeza: TSmallintField;
    QrVSAtuCustoMP: TFloatField;
    QrVSAntCustoMP: TFloatField;
    QrVSMovItsCustoMP: TFloatField;
    QrCorda: TMySQLQuery;
    QrVSAtuOrdem: TLargeintField;
    QrVSAtuPalOri: TLargeintField;
    QrVSAtuIMEIOri: TLargeintField;
    QrVSAtuAcImeiPeca: TFloatField;
    QrVSAtuAcImeiPeso: TFloatField;
    QrVSAtuAcImeiArM2: TFloatField;
    QrVSAtuAcImeiArP2: TFloatField;
    QrVSAtuAcImeiValorT: TFloatField;
    QrVSAtuAcImeiCusPc: TFloatField;
    QrVSAtuAcImeiCusKg: TFloatField;
    QrVSAtuAcImeiCusM2: TFloatField;
    QrVSAtuAcImeiCusP2: TFloatField;
    QrVSMovItsIMEIOri: TLargeintField;
    QrVSMovItsPalOri: TLargeintField;
    Qry: TMySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGAtu: TdmkDBGridZTO;
    TabSheet2: TTabSheet;
    CkDivergencias: TCheckBox;
    QrVSAtuPallet: TLargeintField;
    Panel3: TPanel;
    Panel6: TPanel;
    DBGItsSdo: TDBGrid;
    EdValMin: TdmkEdit;
    Label1: TLabel;
    Label3: TLabel;
    EdValMax: TdmkEdit;
    BtItsSdoo: TBitBtn;
    QrItsSdo: TMySQLQuery;
    QrItsSdoControle: TLargeintField;
    QrItsSdoCodigo: TLargeintField;
    QrItsSdoMovimCod: TLargeintField;
    QrItsSdoPallet: TLargeintField;
    QrItsSdoGraGruX: TLargeintField;
    QrItsSdoPecas: TFloatField;
    QrItsSdoPesoKg: TFloatField;
    QrItsSdoAreaM2: TFloatField;
    QrItsSdoValorT: TFloatField;
    QrItsSdoSrcNivel2: TLargeintField;
    QrItsSdoAcImeiPeca: TFloatField;
    QrItsSdoAcImeiPeso: TFloatField;
    QrItsSdoAcImeiArM2: TFloatField;
    QrItsSdoAcImeiValorT: TFloatField;
    DsItsSdo: TDataSource;
    BtImprime: TBitBtn;
    QrInfo: TMySQLQuery;
    QrInfoGraGruX: TLargeintField;
    QrInfoGraGruY: TIntegerField;
    QrInfoGrandeza: TSmallintField;
    QrInfoITENS: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_245_A_1GetValue(const VarName: string;
      var Value: Variant);
    procedure BtItsSdooClick(Sender: TObject);
    procedure DBGItsSdoDblClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    //FVSKardexAnt,
    FVSKardexMov, FDataHora: String;
    FEmpresa, FGraGruX, FGraGruY, FGrandeza, FAtuIMEIOri,
    FLastAntPall, FLastIMEIOri(*FLastMovimCod*): Integer;
    FDataI_Dta, FDtaAnt_Dta: TDateTime;
    FDataI_Txt: String;
    FCalculou: Boolean;
    //
    function  DefineSQLMovimID(const Obrigatorio: Boolean; const EdMovimID:
              TdmkEdit; var SQL: String): Boolean;
    procedure PesquisaLancamentosA();
    procedure PesquisaLancamentosB();
    procedure AtualizaEvolucaoA();
    procedure AtualizaEvolucaoB();
    //
    function SQLWhereAnt(GraGruY: Integer): String;
    function SQLWhereAtu(GraGruY: Integer): String;
    //
    procedure MostraRelatorio(Grandeza, GraGruY: Integer);
  public
    { Public declarations }
  end;

  var
  FmVSImpKardex4: TFmVSImpKardex4;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UnGrl_Consts,
  UnDmkProcFunc, UMySQLModule, ModVS, UnVS_CRC_PF, UMySQLDB;

{$R *.DFM}

procedure TFmVSImpKardex4.AtualizaEvolucaoA();
var
  AcumPeca, AcumPeso, AcumArM2, AcumArP2, AcumValorT, AcumCusPc, AcumCusKg,
  AcumCusM2, AcumCusP2, AcumSdoVrtPeca, AcumSDoVrtPeso, AcumSdoVrtArM2,
  AcumSdoVrtArP2, AcumSdoVrtValr,
  AcImeiPeca, AcImeiPeso, AcImeiArM2, AcImeiArP2, AcImeiValorT, AcImeiCusPc, AcImeiCusKg,
  AcImeiCusM2, AcImeiCusP2: Double;
  SQL, SQL_WHERE: String;
  Controle, ControleAnt: Integer;
begin
  SQL_WHERE := SQLWhereAnt(FGraGruY);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(CustoMP) CustoMP, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_kardex_mov_4 ',
  SQL_WHERE,
  ' ']);
  //Geral.MB_Teste(QrVSAnt.SQL.Text);
  //

  AcumPeca     := QrVSAntPecas.Value;
  AcumPeso     := QrVSAntPesoKg.Value;
  AcumArM2     := QrVSAntAreaM2.Value;
  AcumArP2     := QrVSAntAreaP2.Value;
  //AcumValorT   := QrVSAntValorT.Value;
  AcumValorT   := QrVSAntCustoMP.Value;

  //
  AcumSdoVrtPeca := QrVSAntSdoVrtPeca.Value;
  AcumSDoVrtPeso := QrVSAntSdoVrtPeso.Value;
  AcumSdoVrtArM2 := QrVSAntSdoVrtArM2.Value;
  AcumSdoVrtArP2 := QrVSAntSdoVrtArP2.Value;

  //
  SQL_WHERE := SQLWhereAtu(FGRaGruY);

  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
  'SELECT * FROM ' + FVSKardexMov,
  //,
  //'ORDER BY DataHora, Controle ',
  SQL_WHERE,
  'ORDER BY PalOri, IMEIOri, Ordem, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrVSMovIts.RecordCount;
  QrVSMovIts.DisableControls;
  try
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      Controle := QrVSMovItsControle.Value;
      PB1.Position := PB1.Position + 1;
      Application.ProcessMessages;
      // ///////////////////////////////////////////////////////////////////////
      AcumPeca     := AcumPeca   + QrVSMovItsPecas.Value;
      AcumPeso     := AcumPeso   + QrVSMovItsPesoKg.Value;
      AcumArM2     := AcumArM2   + QrVSMovItsAreaM2.Value;
      AcumArP2     := AcumArP2   + QrVSMovItsAreaP2.Value;
      //AcumValorT   := AcumValorT + QrVSMovItsValorT.Value;
      AcumValorT   := AcumValorT + QrVSMovItsCustoMP.Value;
      //
      if AcumPeca <> 0 then
        AcumCusPc    := AcumValorT / AcumPeca
      else
        AcumCusPc    := 0.00;

      if AcumPeso <> 0 then
        AcumCusKg    := AcumValorT / AcumPeso
      else
        AcumCusKg    := 0.00;

      if (ABS(AcumValorT) >= 0.0001) AND (ABS(AcumArM2) >= 0.01) then
        AcumCusM2    := AcumValorT / AcumArM2
      else
        AcumCusM2    := 0.00;

      if AcumArP2 <> 0 then
        AcumCusP2    := AcumValorT / AcumArP2
      else
        AcumCusP2    := 0.00;
      //
      // ///////////////////////////////////////////////////////////////////////
      if FAtuIMEIOri <> QrVSMovItsIMEIOri.Value then
      begin
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
        'UPDATE _vs_kardex_mov_4 SET ' +
        '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 3, siNegativo) +
        ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 3, siNegativo) +
        ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
        ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
        ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 4, siNegativo) +
        ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 3, siNegativo) +
        ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 3, siNegativo) +
        ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 2, siNegativo) +
        ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 2, siNegativo) +
        '  WHERE Controle=' + IntToStr(ControleAnt) +
        '');
        //
        AcImeiPeca   := 0.000;
        AcImeiPeso   := 0.000;
        AcImeiArM2   := 0.00;
        AcImeiArP2   := 0.00;
        AcImeiValorT := 0.0000;
        FAtuIMEIOri := QrVSMovItsControle.Value;
      end;
      AcImeiPeca     := AcImeiPeca   + QrVSMovItsPecas.Value;
      AcImeiPeso     := AcImeiPeso   + QrVSMovItsPesoKg.Value;
      AcImeiArM2     := AcImeiArM2   + QrVSMovItsAreaM2.Value;
      AcImeiArP2     := AcImeiArP2   + QrVSMovItsAreaP2.Value;
      //AcImeiValorT   := AcImeiValorT + QrVSMovItsValorT.Value;
      AcImeiValorT   := AcImeiValorT + QrVSMovItsCustoMP.Value;
      //
      if AcImeiPeca <> 0 then
        AcImeiCusPc    := AcImeiValorT / AcImeiPeca
      else
        AcImeiCusPc    := 0.00;

      if AcImeiPeso <> 0 then
        AcImeiCusKg    := AcImeiValorT / AcImeiPeso
      else
        AcImeiCusKg    := 0.00;

      if (ABS(AcImeiValorT) >= 0.0001) AND (ABS(AcImeiArM2) >= 0.01) then
        AcImeiCusM2    := AcImeiValorT / AcImeiArM2
      else
        AcImeiCusM2    := 0.00;

      if AcImeiArP2 <> 0 then
        AcImeiCusP2    := AcImeiValorT / AcImeiArP2
      else
        AcImeiCusP2    := 0.00;
      //
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
      'UPDATE _vs_kardex_mov_4 SET ' +
      '  AcumPeca=' + Geral.FFT_Dot(AcumPeca, 3, siNegativo) +
      ', AcumPeso=' + Geral.FFT_Dot(AcumPeso, 3, siNegativo) +
      ', AcumArM2=' + Geral.FFT_Dot(AcumArM2, 2, siNegativo) +
      ', AcumArP2=' + Geral.FFT_Dot(AcumArP2, 2, siNegativo) +
      ', AcumValorT=' + Geral.FFT_Dot(AcumValorT, 4, siNegativo) +
      ', AcumCusPc=' + Geral.FFT_Dot(AcumCusPc, 3, siNegativo) +
      ', AcumCusKg=' + Geral.FFT_Dot(AcumCusKg, 3, siNegativo) +
      ', AcumCusM2=' + Geral.FFT_Dot(AcumCusM2, 2, siNegativo) +
      ', AcumCusP2=' + Geral.FFT_Dot(AcumCusP2, 2, siNegativo) +
(*
      ', AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 2, siNegativo) +
      ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 2, siNegativo) +
      ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
      ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
      ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 2, siNegativo) +
      ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 2, siNegativo) +
      ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 2, siNegativo) +
      ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 4, siNegativo) +
      ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 4, siNegativo) +
*)
      '  WHERE Controle=' + IntToStr(Controle) +
      '');
      //
      ControleAnt := Controle;
      QrVSMovIts.Next;
    end;
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    'UPDATE _vs_kardex_mov_4 SET ' +
    '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 3, siNegativo) +
    ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 3, siNegativo) +
    ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
    ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
    ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 4, siNegativo) +
    ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 3, siNegativo) +
    ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 3, siNegativo) +
    ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 2, siNegativo) +
    ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 2, siNegativo) +
    '  WHERE Controle=' + IntToStr(ControleAnt) +
    '');
    QrVSMovIts.First;
  finally
    QrVSMovIts.EnableControls;
  end;
  //
end;

procedure TFmVSImpKardex4.AtualizaEvolucaoB();
var
  AcumPeca, AcumPeso, AcumArM2, AcumArP2, AcumValorT, AcumCusPc, AcumCusKg,
  AcumCusM2, AcumCusP2, AcumSdoVrtPeca, AcumSDoVrtPeso, AcumSdoVrtArM2,
  AcumSdoVrtArP2, AcumSdoVrtValr,
  AcImeiPeca, AcImeiPeso, AcImeiArM2, AcImeiArP2, AcImeiValorT, AcImeiCusPc, AcImeiCusKg,
  AcImeiCusM2, AcImeiCusP2: Double;
  SQL, SQL_WHERE: String;
  Controle, ControleAnt: Integer;
begin
  SQL_WHERE := SQLWhereAnt(FGRaGruY);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(CustoMP) CustoMP, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_kardex_mov_4 ',
  SQL_WHERE,
  ' ']);
  //Geral.MB_Teste(QrVSAnt.SQL.Text);
  //

  AcumPeca     := QrVSAntPecas.Value;
  AcumPeso     := QrVSAntPesoKg.Value;
  AcumArM2     := QrVSAntAreaM2.Value;
  AcumArP2     := QrVSAntAreaP2.Value;
  //AcumValorT   := QrVSAntValorT.Value;
  AcumValorT   := QrVSAntCustoMP.Value;

  //
  AcumSdoVrtPeca := QrVSAntSdoVrtPeca.Value;
  AcumSDoVrtPeso := QrVSAntSdoVrtPeso.Value;
  AcumSdoVrtArM2 := QrVSAntSdoVrtArM2.Value;
  AcumSdoVrtArP2 := QrVSAntSdoVrtArP2.Value;

  //
  SQL_WHERE := SQLWhereAtu(FGraGruY);

  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
  'SELECT * FROM ' + FVSKardexMov,
  //,
  SQL_WHERE,
  //'ORDER BY DataHora, Controle ',
  //'ORDER BY MovimCod, IMEIOri, Ordem, Controle ',
  'ORDER BY IMEIOri, Ordem, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrVSMovIts.RecordCount;
  QrVSMovIts.DisableControls;
  try
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      Controle := QrVSMovItsControle.Value;
      PB1.Position := PB1.Position + 1;
      Application.ProcessMessages;
      // ///////////////////////////////////////////////////////////////////////
      AcumPeca     := AcumPeca   + QrVSMovItsPecas.Value;
      AcumPeso     := AcumPeso   + QrVSMovItsPesoKg.Value;
      AcumArM2     := AcumArM2   + QrVSMovItsAreaM2.Value;
      AcumArP2     := AcumArP2   + QrVSMovItsAreaP2.Value;
      //AcumValorT   := AcumValorT + QrVSMovItsValorT.Value;
      AcumValorT   := AcumValorT + QrVSMovItsCustoMP.Value;
      //
      if AcumPeca <> 0 then
        AcumCusPc    := AcumValorT / AcumPeca
      else
        AcumCusPc    := 0.00;

      if AcumPeso <> 0 then
        AcumCusKg    := AcumValorT / AcumPeso
      else
        AcumCusKg    := 0.00;

      if (ABS(AcumValorT) >= 0.0001) AND (ABS(AcumArM2) >= 0.01) then
        AcumCusM2    := AcumValorT / AcumArM2
      else
        AcumCusM2    := 0.00;

      if AcumArP2 <> 0 then
        AcumCusP2    := AcumValorT / AcumArP2
      else
        AcumCusP2    := 0.00;
      //
      // ///////////////////////////////////////////////////////////////////////
      if FAtuIMEIOri <> QrVSMovItsIMEIOri.Value then
      begin
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
        'UPDATE _vs_kardex_mov_4 SET ' +
        '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 3, siNegativo) +
        ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 3, siNegativo) +
        ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
        ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
        ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 4, siNegativo) +
        ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 3, siNegativo) +
        ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 3, siNegativo) +
        ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 2, siNegativo) +
        ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 2, siNegativo) +
        '  WHERE Controle=' + IntToStr(ControleAnt) +
        '');
        //
        AcImeiPeca   := 0.000;
        AcImeiPeso   := 0.000;
        AcImeiArM2   := 0.00;
        AcImeiArP2   := 0.00;
        AcImeiValorT := 0.0000;
        FAtuIMEIOri := QrVSMovItsControle.Value;
      end;
      AcImeiPeca     := AcImeiPeca   + QrVSMovItsPecas.Value;
      AcImeiPeso     := AcImeiPeso   + QrVSMovItsPesoKg.Value;
      AcImeiArM2     := AcImeiArM2   + QrVSMovItsAreaM2.Value;
      AcImeiArP2     := AcImeiArP2   + QrVSMovItsAreaP2.Value;
      //AcImeiValorT   := AcImeiValorT + QrVSMovItsValorT.Value;
      AcImeiValorT   := AcImeiValorT + QrVSMovItsCustoMP.Value;
      //
      if AcImeiPeca <> 0 then
        AcImeiCusPc    := AcImeiValorT / AcImeiPeca
      else
        AcImeiCusPc    := 0.00;

      if AcImeiPeso <> 0 then
        AcImeiCusKg    := AcImeiValorT / AcImeiPeso
      else
        AcImeiCusKg    := 0.00;

      if (ABS(AcImeiValorT) >= 0.0001) AND (ABS(AcImeiArM2) >= 0.01) then
        AcImeiCusM2    := AcImeiValorT / AcImeiArM2
      else
        AcImeiCusM2    := 0.00;

      if AcImeiArP2 <> 0 then
        AcImeiCusP2    := AcImeiValorT / AcImeiArP2
      else
        AcImeiCusP2    := 0.00;
      //
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
      'UPDATE _vs_kardex_mov_4 SET ' +
      '  AcumPeca=' + Geral.FFT_Dot(AcumPeca, 3, siNegativo) +
      ', AcumPeso=' + Geral.FFT_Dot(AcumPeso, 3, siNegativo) +
      ', AcumArM2=' + Geral.FFT_Dot(AcumArM2, 2, siNegativo) +
      ', AcumArP2=' + Geral.FFT_Dot(AcumArP2, 2, siNegativo) +
      ', AcumValorT=' + Geral.FFT_Dot(AcumValorT, 4, siNegativo) +
      ', AcumCusPc=' + Geral.FFT_Dot(AcumCusPc, 3, siNegativo) +
      ', AcumCusKg=' + Geral.FFT_Dot(AcumCusKg, 3, siNegativo) +
      ', AcumCusM2=' + Geral.FFT_Dot(AcumCusM2, 2, siNegativo) +
      ', AcumCusP2=' + Geral.FFT_Dot(AcumCusP2, 2, siNegativo) +
(*
      ', AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 2, siNegativo) +
      ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 2, siNegativo) +
      ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
      ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
      ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 2, siNegativo) +
      ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 2, siNegativo) +
      ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 2, siNegativo) +
      ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 4, siNegativo) +
      ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 4, siNegativo) +
*)
      '  WHERE Controle=' + IntToStr(Controle) +
      '');
      //
      ControleAnt := Controle;
      QrVSMovIts.Next;
    end;
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    'UPDATE _vs_kardex_mov_4 SET ' +
    '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 3, siNegativo) +
    ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 3, siNegativo) +
    ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
    ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
    ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 4, siNegativo) +
    ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 3, siNegativo) +
    ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 3, siNegativo) +
    ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 2, siNegativo) +
    ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 2, siNegativo) +
    '  WHERE Controle=' + IntToStr(ControleAnt) +
    '');
    QrVSMovIts.First;
  finally
    QrVSMovIts.EnableControls;
  end;
  //
end;

procedure TFmVSImpKardex4.BtImprimeClick(Sender: TObject);
const
  CaptionFm = 'Sele��o de grandeza';
  CaptionRG = 'Selecione a grandeza';
  SelOnClick = True;
var
  Grandeza, GraGruY: Integer;
begin
  FVSKardexMov := '_vs_kardex_mov_4';
  if USQLDB.TabelaExiste(FVSKardexMov, DModG.MyPID_DB) then
  begin
    if FCalculou then
    begin
      Grandeza := FGrandeza;
      GraGruY := FGraGruY;
    end else
    begin
(*
      Grandeza := MyObjects.SelRadioGroup(CaptionFm, CaptionRG, ['Kg', 'm�'], 2,
      -1, SelOnClick);
*)
      UnDmkDAC_PF.AbreMySQLQuery0(QrInfo, DModG.MyPID_DB, [
      'SELECT km4.GraGruX, ggx.GraGruY,  ',
      'med.Grandeza, COUNT(km4.GraGruX) ITENS ',
      'FROM _vs_kardex_mov_4 km4 ',
      'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=km4.GraGruX ',
      'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed  ',
      'GROUP BY km4.GraGruX ',
      'ORDER BY ITENS ',
      '']);
      Grandeza := QrInfoGrandeza.Value;
      GraGruY := QrInfoGraGruY.Value;
      EdGraGruX.ValueVariant := QrInfoGraGruX.Value;
      CBGraGruX.KeyValue := QrInfoGraGruX.Value;
    end;
    if Grandeza > -1 then
      MostraRelatorio(Grandeza, GraGruY);
  end;
end;

procedure TFmVSImpKardex4.BtItsSdooClick(Sender: TObject);
var
  ValMin, ValMax: Double;
begin
  ValMin := EdValMin.ValueVariant;
  ValMax := EdValMax.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsSdo, DModG.MyPID_DB, [
  'SELECT Controle, Codigo, MovimCod, Pallet, ',
  'GraGruX, Pecas, PesoKg, AreaM2, ValorT, ',
  'SrcNivel2, AcImeiPeca, AcImeiPeso,  ',
  'AcImeiArM2, AcImeiValorT ',
  'FROM _vs_kardex_mov_4 ',
  'WHERE AcImeiValorT >= ' + Geral.FFT_Dot(ValMin, 6, siNegativo),
  'AND AcImeiValorT <= ' + Geral.FFT_Dot(ValMax, 6, siNegativo),
  'AND AcImeiValorT <> 0 ',
  '']);
end;

procedure TFmVSImpKardex4.BtOKClick(Sender: TObject);
const
  TemIMEiMrt = 0;
  MargemErro = 0.01;
  Avisa = True;
  ForcaMostrarForm = False;
  SelfCall = False;
begin
  Screen.Cursor := crHourGlass;
  try
  FAtuIMEIOri := 0;
  FEmpresa := EdEmpresa.ValueVariant;
  if Ck02DataIni.Checked then
    FDataI_Dta  := Int(TP02DataIni.Date)
  else
    FDataI_Dta  := 0;
  //
  FDataHora := Geral.FDT(FDataI_Dta - 1, 1) + ' 23:59:59';
  FDtaAnt_Dta := FDataI_Dta - 1;
  FDataI_Txt  := Geral.FDT(FDataI_Dta, 1);
  FGraGruX     := EdGraGruX.ValueVariant;
  FGraGruY     := QrGraGruXGraGruY.Value;
  FGrandeza    := QrGraGruXGrandeza.Value;
  //
  if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then
    Exit;
    //
  case FGraGruY of
    1024,
    1088,
    1195,
    2048,
    3072,
    1072,
    1365,
    1536,
    1621,
    1707,
    4096,
    5120,
    6144,
    7168: (* nada *);
    else
    begin
      Geral.MB_Info('Este artigo n�o est� habilitado para este relat�rio.' +
      'Solicite � DERMATEK!  GGY: ' + Geral.FF0(FGraGruY));
      Exit;
    end;
  end;
  //
  if CkDivergencias.Checked then
  begin
    DmModVS.ZeraValoMP_de_MovimID_2();
    if DmModVS.QtdPosNegSameReg(TemIMEIMrt, Avisa,
      ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
    if DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2) then Exit;
    if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.CustosDiferentesOrigemParaDestino2(FEmpresa, FGraGruX, MargemErro,
      False, False, False, TemIMEiMrt, LaAviso1, LaAviso2) then Exit;
    if VS_PF.RegistrosComProblema(FEmpresa, FGraGruX, LaAviso1, LaAviso2) then Exit;
    if DmModVS.CustosDifPreRecl(FEmpresa, FGraGruX, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
    if DmModVS.CustosDifReclasse(FEmpresa, FGraGruX, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
    if DmModVS.VSPcPosNegXValTNegPos(FEmpresa, FGraGruX, TemIMEIMrt, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
   if DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
   if DmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall, TemIMEIMrt, nil, nil) then Exit;
    if DmModVS.CustosDiferentesSaldosVirtuais3(FEmpresa, FGraGruX, TemIMEIMrt,
       0.0005, Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
 end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados');
  case FGraGruY of
    1024,
    1088,
    1195,
    1536,
    1621,
    2048,
    3072,
    6144:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(Pallet) Pallet ',
      'FROM vsmovits ',
      'WHERE (Pecas + PesoKg) > 0  ',
      'AND (NOT (MovimID IN (9,17,41)))  ',
      'AND GraGruX=' + Geral.FF0(FGraGruX),
      'AND DataHora < "' + FDataI_Txt + '" ',
      '']);
      FLastAntPall := Qry.Fields[0].AsInteger;
    end;
    1072,
    1365,
    1707,
    4096,
    5120,
    7168:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(Controle) IMEIOri ',
      'FROM vsmovits ',
      'WHERE (Pecas + PesoKg) > 0  ',
      'AND (NOT (MovimID IN (9,17,41)))  ',
      'AND GraGruX=' + Geral.FF0(FGraGruX),
      'AND DataHora < "' + FDataI_Txt + '" ',
      '']);
      FLastIMEIOri := Qry.Fields[0].AsInteger;
    end;
    else
      Geral.MB_Info('Grupo de estoque selecionado n�o implemetado: ' + Geral.FF0(FGraGruY));
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa');
  //FVSKardexAnt := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex, DModG.QrUpdPID1, False, 1, '_vs_Kardex_ant');
  FVSKardexMov := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex3, DModG.QrUpdPID1, False, 1, '_vs_kardex_mov_4');

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados');
  case FGraGruY of
    1024,
    1088,
    1195,
    1536,
    1621,
    2048,
    3072,
    6144: PesquisaLancamentosA();
    1072,
    1365,
    1707,
    4096,
    5120,
    7168: PesquisaLancamentosB();
    else
      Geral.MB_Info('Grupo de estoque selecionado n�o implemetado: ' + Geral.FF0(FGraGruY));
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando acumulados');
  case FGraGruY of
    1024,
    1088,
    1195,
    1536,
    1621,
    2048,
    3072,
    6144: AtualizaEvolucaoA();
    1072,
    1365,
    1707,
    4096,
    5120,
    7168: AtualizaEvolucaoB();
    else
      Geral.MB_Info('Grupo de estoque selecionado n�o implemetado: ' + Geral.FF0(FGraGruY));
  end;
  FCalculou := True;
  //
  MostraRelatorio(FGrandeza, FGraGruY);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmVSImpKardex4.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpKardex4.DBGItsSdoDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DBGItsSdo.Columns[THackDBGrid(DBGItsSdo).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(QrItsSdoControle.Value);
  if Campo = 'SrcNivel2' then
      VS_PF.MostraFormVSMovIts(QrItsSdoSrcNivel2.Value);
end;

function TFmVSImpKardex4.DefineSQLMovimID(const Obrigatorio: Boolean;
  const EdMovimID: TdmkEdit; var SQL: String): Boolean;
var
  MovimID: Integer;
begin
  Result := False;
  SQL := '';
  MovimID := EdMovimID.ValueVariant;
  if MyObjects.FIC((MovimID < 0) and Obrigatorio, EdMovimID,
  'Informe o ID do movimento!') then
    Exit;
  if MovimID > -1 then
    SQL := 'AND vmi.MovimID=' + Geral.FF0(MovimID);
  Result := True;
end;

procedure TFmVSImpKardex4.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpKardex4.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TP02DataIni.Date := Trunc(DModG.ObtemAgora()) - 30;
  TP02DataFim.Date := DModG.ObtemAgora();

  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  FCalculou := False;
end;

procedure TFmVSImpKardex4.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpKardex4.frxWET_CURTI_245_A_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_ARTIGO' then
    Value := dmkPF.ParValueCodTxt(
      'Artigo: ', CBGraGruX.Text, EdGraGruX.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_ANT' then
    Value := Geral.FDT(FDtaAnt_Dta + (1439/1440), 107)
end;

procedure TFmVSImpKardex4.MostraRelatorio(Grandeza, GraGruY: Integer);
var
  Report: TfrxReport;
  SQL_SomComPc, SQL_WHERE, ORDER_BY, ATT_MovimID: String;
begin
  if CkSomComPc.Checked then
    SQL_SomComPc := 'AND Pecas <> 0'
  else
    SQL_SomComPc := '';
  //
  SQL_WHERE := SQLWhereAnt(GraGruY);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(CustoMP) CustoMP, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_kardex_mov_4 ',
  SQL_WHERE,
  ' ']);
  //
  //
  SQL_WHERE := SQLWhereAtu(GraGruY);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando acumulados');
  case GraGruY of
    1024,
    1088,
    1195,
    1536,
    1621,
    2048,
    3072,
    //6144: ORDER_BY := 'ORDER BY Pallet, IMEIOri, Ordem, Controle ';
    6144: ORDER_BY := 'ORDER BY PalOri, IMEIOri, Ordem, Controle ';
    1072,
    1365,
    1707,
    4096,
    //5120: ORDER_BY := 'ORDER BY MovimCod, IMEIOri, Ordem, Controle ';
    5120,
    7168: ORDER_BY := 'ORDER BY IMEIOri, Ordem, Controle ';
    else
      Geral.MB_Info('Grupo de estoque selecionado n�o implemetado: ' + Geral.FF0(GraGruY));
  end;

  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID_FRENDLY);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAtu, DModG.MyPID_DB, [
  'SELECT Controle, DataHora,',
  ATT_MovimID,
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, Pecas) PecasInn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, PesoKg) PesoKgInn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, AreaM2) AreaM2Inn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, AreaP2) AreaP2Inn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, ValorT) ValorTInn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, ValorT / PesoKg) CustoKgInn,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), 0, ValorT / AreaM2) CustoM2Inn,',
  //
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), Pecas, 0) PecasOut,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), PesoKg, 0) PesoKgOut,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), AreaM2, 0) AreaM2Out,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), AreaP2, 0) AreaP2Out,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), ValorT, 0) ValorTOut,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), ValorT / PesoKg, 0) CustoKgOut,',
  'IF((MovimID IN (' + CO_ALL_CODS_IDS_GEN_NEGAT_VS + ')) AND (MovimNiv IN (' + CO_ALL_CODS_NIV_GEN_NEGAT_VS + ')), ValorT / AreaM2, 0) CustoM2Out,',
  //
  'CustoMP, ',
  'AcumPeca, AcumPeso,  AcumArM2, ',
  'AcumArP2, AcumValorT, AcumCusPc, ',
  'AcumCusKg, AcumCusM2, AcumCusP2,',
  'AcImeiPeca, AcImeiPeso,  AcImeiArM2, ',
  'AcImeiArP2, AcImeiValorT, AcImeiCusPc, ',
  'AcImeiCusKg, AcImeiCusM2, AcImeiCusP2,',
  'PalOri, IMEIOri, Ordem, Pallet ',
  'FROM ' + FVSKardexMov,
  SQL_WHERE,
  SQL_SomComPc,
  //'ORDER BY DataHora, Controle ',
  ORDER_BY,
  '']);
  //Geral.MB_Teste(QrVSAtu.SQL.Text);
  //
  if Grandeza = 1 then // m2
  begin
    if CkProcesso.Checked then
      Report := frxWET_CURTI_245_B_2
    else
      Report := frxWET_CURTI_245_B_1;
  end else
  begin
    if CkProcesso.Checked then
      Report := frxWET_CURTI_245_A_2
    else
      Report := frxWET_CURTI_245_A_1;
  end;
  //
  DBGAtu.DataSource := nil;
  MyObjects.frxDefineDataSets(Report, [
    DModG.frxDsDono,
    frxDsVSAnt,
    frxDsVSAtu
    ]);
  MyObjects.frxMostra(Report, 'Ficha Kardex');
  DBGAtu.DataSource := DsVSAtu;
end;

procedure TFmVSImpKardex4.PesquisaLancamentosA();
  function SQL_LJ_GGX: String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Periodo, SQL_GraGruX, ATT_MovimID, SQL_MovimID: String;
  GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  procedure GeraVSKardex();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Artigo, SQL, Corda: String;
  begin
    SQL_Artigo := '';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
   '1 Ordem, Pallet PalOri, Controle IMEIOri, Controle ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE (Pecas + PesoKg) > 0 ',
    'AND (NOT (MovimID IN (9,17,41)) ',
    'AND (NOT (MovimNiv IN (14))) ',
    ') ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    SQL_GraGruX,
    SQL_Periodo,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_1; ',
    'CREATE TABLE _vs_cardex_imeis_1 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ';',
    'SELECT DISTINCT Controle FROM _vs_cardex_imeis_1',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', '-999999999');
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de baixa');
    SQL_Flds := Geral.ATS([
    '2 Ordem, ime.PalOri, vmi.SrcNivel2 IMEIOri, vmi.Controle ',
    '']);
    SQL_Left := 'LEFT JOIN _vs_cardex_imeis_1 ime ON ime.Controle=vmi.SrcNivel2';
    SQL_Wher := Geral.ATS([
    'WHERE  ',
    '  ( ',
    '    (SrcNivel2 IN ( ' + Corda + '    ))  ',
    (*'    OR  ',
    '    (MovimID IN (9,17,41)) ',*)
    '  ) ',
    'AND (NOT (MovimNiv IN (14))) ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    SQL_GraGruX,
    SQL_Periodo,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_2; ',
    'CREATE TABLE _vs_cardex_imeis_2 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Unindo dados de entrada e baixa');
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_3; ',
    'CREATE TABLE _vs_cardex_imeis_3 ',
    'SELECT Ordem, PalOri, IMEIOri, Controle FROM _vs_cardex_imeis_1 ',
    'UNION ',
    'SELECT Ordem, PalOri, IMEIOri, Controle FROM _vs_cardex_imeis_2 ',
    '; ',
    //'SELECT Ordem, IMEIOri, Controle  ',
    'SELECT DISTINCT Controle  ',
    'FROM _vs_cardex_imeis_3 ',
    //'ORDER BY IMEIOri, Ordem, Controle; ',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', '-999999999');
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados');
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    'IF(ValorT<>0.00, ValorT, IF(MovimID IN (2), 0.00, IF(ValorMP <> 0, ValorMP, ValorT - CustoPQ))) CustoMP,',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    'IF(Pecas>0, AreaM2/Pecas, 0) MediaArM2, ',
    'IF(Pecas>0, PesoKg/Pecas, 0) MediaPeso, ',
    '0.00 AcumPeca, 0.00 AcumPeso, 0.00 AcumArM2, ',
    '0.00 AcumArP2, 0.00 AcumValorT, 0.00 AcumCusPc, ',
    '0.00 AcumCusKg, 0.00 AcumCusM2, 0.00 AcumCusP2, ',
    '0.00 AcImeiPeca, 0.00 AcImeiPeso, 0.00 AcImeiArM2, ',
    '0.00 AcImeiArP2, 0.00 AcImeiValorT, 0.00 AcImeiCusPc, ',
    '0.00 AcImeiCusKg, 0.00 AcImeiCusM2, 0.00 AcImeiCusP2, ',
    'ci3.Ordem, ci3.PalOri, ci3.IMEIOri ', // diferenye aqui!!!
    '']);
    SQL_Left := Geral.ATS([
    SQL_LJ_GGX(),
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vmi.Terceiro',
    'LEFT JOIN _vs_cardex_imeis_3 ci3 ON ci3.Controle=vmi.Controle',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Controle IN (' + Corda + ')',
    '']);
    SQL_Group := '';
    //UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    SQL := Geral.ATS([
    'INSERT INTO ' + FVSKardexMov,
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY NO_Pallet, Controle ',
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    //
    //Geral.MB_Teste(SQL);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora', 0,
      TP02DataFim.Date, True, Ck02DataFim.Checked);

  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, FEmpresa);
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  GeraVSKardex();
  //
end;

procedure TFmVSImpKardex4.PesquisaLancamentosB();
  function SQL_LJ_GGX: String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Periodo, SQL_GraGruX, ATT_MovimID, SQL_MovimID: String;
  GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  procedure GeraVSKardex();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Artigo, SQL, Corda: String;
    MovimIDInn: TEstqMovimID;
    MovimNivInn, MovimNivBxa: TEstqMovimNiv;
  begin
    MovimIDInn := VS_PF.ObtemMovimIDDeGraGruY(FGraGruY);
    MovimNivInn := VS_CRC_PF.ObtemMovimNivInnDeMovimID(MovimIDInn);
    SQL_Artigo := '';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
   '1 Ordem, MovimCod, Controle IMEIOri, Controle ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimNiv=' + Geral.FF0(Integer(MovimNivInn)),
    'AND Empresa=' + Geral.FF0(FEmpresa),
    SQL_GraGruX,
    SQL_Periodo,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_1; ',
    'CREATE TABLE _vs_cardex_imeis_1 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ';',
    //'SELECT DISTINCT MovimCod FROM _vs_cardex_imeis_1',
    'SELECT DISTINCT IMEIOri FROM _vs_cardex_imeis_1',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    //Corda := MyObjects.CordaDeQuery(QrCorda, 'MovimCod', '-999999999');
    Corda := MyObjects.CordaDeQuery(QrCorda, 'IMEIOri', '-999999999');
    MovimNivBxa := VS_CRC_PF.ObtemMovimNivInnDeMovimID(MovimIDInn);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de baixa');
    SQL_Flds := Geral.ATS([
    '2 Ordem, vmi.MovimCod, IF(vmi.VmiPai>0, vmi.VmiPai, vmi.SrcNivel2) IMEIOri, vmi.Controle ',
    '']);
    SQL_Left := 'LEFT JOIN _vs_cardex_imeis_1 ime ON ime.Controle=vmi.SrcNivel2';
    SQL_Wher := Geral.ATS([
    'WHERE  ',
    '  ( ',
    '    (SrcNivel2 IN ( ' + Corda + '    ))  ',
    '    OR ',
    '    (VmiPai IN ( ' + Corda + '    ))  ',
    (*'    OR  ',
    '    (MovimID IN (9,17,41)) ',*)
    '  ) ',
    'AND (NOT (MovimNiv IN (14))) ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    //SQL_GraGruX, n�o pode, muitos imeis tem outro artigo!
    SQL_Periodo,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_2; ',
    'CREATE TABLE _vs_cardex_imeis_2 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Unindo dados de entrada e baixa');
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_3; ',
    'CREATE TABLE _vs_cardex_imeis_3 ',
    'SELECT Ordem, MovimCod, IMEIOri, Controle FROM _vs_cardex_imeis_1 ',
    'UNION ',
    'SELECT Ordem, MovimCod, IMEIOri, Controle FROM _vs_cardex_imeis_2 ',
    '; ',
    //'SELECT Ordem, IMEIOri, Controle  ',
    'SELECT DISTINCT Controle  ',
    'FROM _vs_cardex_imeis_3 ',
    //'ORDER BY IMEIOri, Ordem, Controle; ',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', '-999999999');
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados');
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    'IF(ValorT<>0.00, ValorT, IF(MovimID IN (2), 0.00, IF(ValorMP <> 0, ValorMP, ValorT - CustoPQ))) CustoMP,',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    'IF(Pecas>0, AreaM2/Pecas, 0) MediaArM2, ',
    'IF(Pecas>0, PesoKg/Pecas, 0) MediaPeso, ',
    '0.00 AcumPeca, 0.00 AcumPeso, 0.00 AcumArM2, ',
    '0.00 AcumArP2, 0.00 AcumValorT, 0.00 AcumCusPc, ',
    '0.00 AcumCusKg, 0.00 AcumCusM2, 0.00 AcumCusP2, ',
    '0.00 AcImeiPeca, 0.00 AcImeiPeso, 0.00 AcImeiArM2, ',
    '0.00 AcImeiArP2, 0.00 AcImeiValorT, 0.00 AcImeiCusPc, ',
    '0.00 AcImeiCusKg, 0.00 AcImeiCusM2, 0.00 AcImeiCusP2, ',
    'ci3.Ordem, 0 PalOri, ci3.IMEIOri ', // diferenye aqui!!!
    '']);
    SQL_Left := Geral.ATS([
    SQL_LJ_GGX(),
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vmi.Terceiro',
    'LEFT JOIN _vs_cardex_imeis_3 ci3 ON ci3.Controle=vmi.Controle',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Controle IN (' + Corda + ')',
    '']);
    SQL_Group := '';
    //UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    SQL := Geral.ATS([
    'INSERT INTO ' + FVSKardexMov,
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY MovimCod, IMEIOri, Controle ',
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    //
    //Geral.MB_Teste(SQL);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora', 0,
      TP02DataFim.Date, True, Ck02DataFim.Checked);

  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, FEmpresa);
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  GeraVSKardex();
  //
end;

function TFmVSImpKardex4.SQLWhereAnt(GraGruY: Integer): String;
begin
  case GraGruY of
    1024,
    1195,
    1536,
    1621,
    2048: Result := 'WHERE DataHora <= "' + FDataHora + '"  ';
    1088,
    3072,
    //6144: Result := 'WHERE Pallet <= ' + Geral.FF0(FLastAntPall);
    6144: Result := 'WHERE PalOri <= ' + Geral.FF0(FLastAntPall);
    1072,
    1365,
    1707,
    4096,
    //5120: Result := 'WHERE MovimCod <= ' + Geral.FF0(FLastMovimCod);
    5120,
    7168: Result := 'WHERE IMEIOri <= ' + Geral.FF0(FLastIMEIOri);
    else Result := '??? SQLWhereAnt() ???';
  end;
end;

function TFmVSImpKardex4.SQLWhereAtu(GraGruY: Integer): String;
begin
  case GraGruY of
    1024,
    1195,
    1536, 1621,
    2048: Result := 'WHERE DataHora > "' + FDataHora + '"  ';
    1088,
    3072,
    //6144: Result := 'WHERE Pallet > ' + Geral.FF0(FLastAntPall);
    6144: Result := 'WHERE PalOri > ' + Geral.FF0(FLastAntPall);
    1072,
    1365,
    1707,
    4096,
    //5120: Result := 'WHERE MovimCod > ' + Geral.FF0(FLastMovimCod);
    5120,
    7168: Result := 'WHERE IMEIOri > ' + Geral.FF0(FLastIMEIOri);
    else Result := '??? SQLWhereAtu() ???';
  end;
end;

end.
