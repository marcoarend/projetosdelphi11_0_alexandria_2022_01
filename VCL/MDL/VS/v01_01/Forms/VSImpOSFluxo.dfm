object FmVSImpOSFluxo: TFmVSImpOSFluxo
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-233 :: Impress'#227'o de OS de Fluxo de Produ'#231#227'o'
  ClientHeight = 397
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 13
  object QrFluxosIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFluxosItsBeforeClose
    AfterScroll = QrFluxosItsAfterScroll
    SQL.Strings = (
      'SELECT ope.Nome NOMEOPERACAO, fli.*'
      'FROM fluxosits fli'
      'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao'
      'WHERE fli.Codigo=:P0'
      'ORDER BY fli.Ordem, fli.Controle DESC')
    Left = 236
    Top = 61
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFluxosItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFluxosItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrFluxosItsAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 30
    end
    object QrFluxosItsAcao2: TWideStringField
      FieldName = 'Acao2'
      Size = 30
    end
    object QrFluxosItsAcao3: TWideStringField
      FieldName = 'Acao3'
      Size = 30
    end
    object QrFluxosItsAcao4: TWideStringField
      FieldName = 'Acao4'
      Size = 30
    end
    object QrFluxosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxosItsNOMEOPERACAO: TWideStringField
      FieldName = 'NOMEOPERACAO'
      Size = 30
    end
    object QrFluxosItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object QrVSPWECab: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSPWECabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 64
    Top = 61
    object QrVSPWECabNO_PRD_TAM_COR_DST: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_DST'
      Size = 255
    end
    object QrVSPWECabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrVSPWECabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrVSPWECabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrVSPWECabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPWECabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrVSPWECabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrVSPWECabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrVSPWECabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrVSPWECabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrVSPWECabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
    object QrVSPWECabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrVSPWECabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrVSPWECabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrVSPWECabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrVSPWECabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrVSPWECabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrVSPWECabPecasMan: TFloatField
      FieldName = 'PecasMan'
      Origin = 'vsopecab.PecasMan'
    end
    object QrVSPWECabAreaManM2: TFloatField
      FieldName = 'AreaManM2'
      Origin = 'vsopecab.AreaManM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaManP2: TFloatField
      FieldName = 'AreaManP2'
      Origin = 'vsopecab.AreaManP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsopecab.TipoArea'
    end
    object QrVSPWECabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPWECabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrVSPWECabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrVSPWECabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrVSPWECabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrVSPWECabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsopecab.GraGruX'
    end
    object QrVSPWECabCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
      Origin = 'vsopecab.CustoManMOKg'
    end
    object QrVSPWECabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrVSPWECabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasSrc: TFloatField
      FieldName = 'PecasSrc'
      Origin = 'vsopecab.PecasSrc'
    end
    object QrVSPWECabAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
      Origin = 'vsopecab.AreaSrcM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
      Origin = 'vsopecab.AreaSrcP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasDst: TFloatField
      FieldName = 'PecasDst'
      Origin = 'vsopecab.PecasDst'
    end
    object QrVSPWECabAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
      Origin = 'vsopecab.AreaDstM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
      Origin = 'vsopecab.AreaDstP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasSdo: TFloatField
      FieldName = 'PecasSdo'
      Origin = 'vsopecab.PecasSdo'
    end
    object QrVSPWECabAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
      Origin = 'vsopecab.AreaSdoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
      Origin = 'vsopecab.AreaSdoP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
      Origin = 'vsopecab.PesoKgSrc'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
      Origin = 'vsopecab.PesoKgMan'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
      Origin = 'vsopecab.PesoKgDst'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
      Origin = 'vsopecab.PesoKgSdo'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSPWECabAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      Origin = 'vsopecab.PesoKgBxa'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrVSPWECabAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
      Origin = 'vsopecab.AreaBxaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
      Origin = 'vsopecab.AreaBxaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSPWECabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVSPWECabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSPWECabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSPWECabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPWECabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSPWECabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPWECabVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
    object QrVSPWECabNO_VSCOPCab: TWideStringField
      FieldName = 'NO_VSCOPCab'
      Size = 60
    end
    object QrVSPWECabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSPWECabNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrVSPWECabVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
    end
    object QrVSPWECabNO_VSArtCab: TWideStringField
      FieldName = 'NO_VSArtCab'
      Size = 255
    end
    object QrVSPWECabEmitGrLote: TWideStringField
      FieldName = 'EmitGrLote'
      Size = 60
    end
    object QrVSPWECabValorTDst: TFloatField
      FieldName = 'ValorTDst'
      Required = True
    end
    object QrVSPWECabKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
      Required = True
    end
    object QrVSPWECabGGXSrc: TIntegerField
      FieldName = 'GGXSrc'
      Required = True
    end
    object QrVSPWECabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrVSPWECabOperacoes: TIntegerField
      FieldName = 'Operacoes'
      Required = True
    end
    object QrVSPWECabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrVSPWECabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrVSPWECabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrVSPWECabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrVSPWECabLinCulReb: TIntegerField
      FieldName = 'LinCulReb'
      Required = True
    end
    object QrVSPWECabLinCabReb: TIntegerField
      FieldName = 'LinCabReb'
      Required = True
    end
    object QrVSPWECabLinCulSem: TIntegerField
      FieldName = 'LinCulSem'
      Required = True
    end
    object QrVSPWECabLinCabSem: TIntegerField
      FieldName = 'LinCabSem'
      Required = True
    end
    object QrVSPWECabDataPed: TDateTimeField
      FieldName = 'DataPed'
      Required = True
    end
    object QrVSPWECabDataStart: TDateTimeField
      FieldName = 'DataStart'
      Required = True
    end
    object QrVSPWECabDataCrust: TDateTimeField
      FieldName = 'DataCrust'
      Required = True
    end
    object QrVSPWECabDataExped: TDateTimeField
      FieldName = 'DataExped'
      Required = True
    end
    object QrVSPWECabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
      Required = True
    end
    object QrVSPWECabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
      Required = True
    end
    object QrVSPWECabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSPWECabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVSPWECabClasses: TWideStringField
      FieldName = 'Classes'
      Size = 30
    end
    object QrVSPWECabAREA_SEL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AREA_SEL'
      Calculated = True
    end
    object QrVSPWECabNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrVSPWECabNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrVSPWECabNO_FLUXO: TWideStringField
      FieldName = 'NO_FLUXO'
      Size = 100
    end
    object QrVSPWECabMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
    object QrVSPWECabObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
    end
  end
  object frxDsVSPWECab: TfrxDBDataset
    UserName = 'frxDsVSPWECab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR_DST=NO_PRD_TAM_COR_DST'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'NO_EMPRESA=NO_EMPRESA'
      'DtHrAberto=DtHrAberto'
      'DtHrLibOpe=DtHrLibOpe'
      'DtHrCfgOpe=DtHrCfgOpe'
      'DtHrFimOpe=DtHrFimOpe'
      'Nome=Nome'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'PecasMan=PecasMan'
      'AreaManM2=AreaManM2'
      'AreaManP2=AreaManP2'
      'TipoArea=TipoArea'
      'NO_TIPO=NO_TIPO'
      'NO_DtHrFimOpe=NO_DtHrFimOpe'
      'NO_DtHrLibOpe=NO_DtHrLibOpe'
      'NO_DtHrCfgOpe=NO_DtHrCfgOpe'
      'CacCod=CacCod'
      'GraGruX=GraGruX'
      'CustoManMOKg=CustoManMOKg'
      'CustoManMOTot=CustoManMOTot'
      'ValorManMP=ValorManMP'
      'ValorManT=ValorManT'
      'PecasSrc=PecasSrc'
      'AreaSrcM2=AreaSrcM2'
      'AreaSrcP2=AreaSrcP2'
      'PecasDst=PecasDst'
      'AreaDstM2=AreaDstM2'
      'AreaDstP2=AreaDstP2'
      'PecasSdo=PecasSdo'
      'AreaSdoM2=AreaSdoM2'
      'AreaSdoP2=AreaSdoP2'
      'PesoKgSrc=PesoKgSrc'
      'PesoKgMan=PesoKgMan'
      'PesoKgDst=PesoKgDst'
      'PesoKgSdo=PesoKgSdo'
      'ValorTMan=ValorTMan'
      'ValorTSrc=ValorTSrc'
      'ValorTSdo=ValorTSdo'
      'PecasINI=PecasINI'
      'AreaINIM2=AreaINIM2'
      'AreaINIP2=AreaINIP2'
      'PesoKgINI=PesoKgINI'
      'PesoKgBxa=PesoKgBxa'
      'PecasBxa=PecasBxa'
      'AreaBxaM2=AreaBxaM2'
      'AreaBxaP2=AreaBxaP2'
      'ValorTBxa=ValorTBxa'
      'Cliente=Cliente'
      'NO_Cliente=NO_Cliente'
      'NFeRem=NFeRem'
      'LPFMO=LPFMO'
      'TemIMEIMrt=TemIMEIMrt'
      'GGXDst=GGXDst'
      'SerieRem=SerieRem'
      'VSCOPCab=VSCOPCab'
      'NO_VSCOPCab=NO_VSCOPCab'
      'EmitGru=EmitGru'
      'NO_EmitGru=NO_EmitGru'
      'VSArtCab=VSArtCab'
      'NO_VSArtCab=NO_VSArtCab'
      'EmitGrLote=EmitGrLote'
      'ValorTDst=ValorTDst'
      'KgCouPQ=KgCouPQ'
      'GGXSrc=GGXSrc'
      'NFeStatus=NFeStatus'
      'Operacoes=Operacoes'
      'VSVmcWrn=VSVmcWrn'
      'VSVmcObs=VSVmcObs'
      'VSVmcSeq=VSVmcSeq'
      'VSVmcSta=VSVmcSta'
      'LinCulReb=LinCulReb'
      'LinCabReb=LinCabReb'
      'LinCulSem=LinCulSem'
      'LinCabSem=LinCabSem'
      'DataPed=DataPed'
      'DataStart=DataStart'
      'DataCrust=DataCrust'
      'DataExped=DataExped'
      'ReceiRecu=ReceiRecu'
      'ReceiRefu=ReceiRefu'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Classes=Classes'
      'AREA_SEL=AREA_SEL'
      'NO_ReceiRecu=NO_ReceiRecu'
      'NO_ReceiRefu=NO_ReceiRefu'
      'NO_FLUXO=NO_FLUXO'
      'MPVIts=MPVIts'
      'Observ=Observ')
    DataSet = QrVSPWECab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 64
    Top = 112
  end
  object frxDsVSArtCab: TfrxDBDataset
    UserName = 'frxDsVSArtCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'NO_ReceiRecu=NO_ReceiRecu'
      'NO_ReceiRefu=NO_ReceiRefu'
      'NO_ReceiAcab=NO_ReceiAcab'
      'ReceiRecu=ReceiRecu'
      'ReceiRefu=ReceiRefu'
      'ReceiAcab=ReceiAcab'
      'TxtMPs=TxtMPs'
      'Observa=Observa'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'FluxProCab=FluxProCab'
      'FluxPcpCab=FluxPcpCab'
      'NO_FluxProCab=NO_FluxProCab'
      'NO_FluxPcpCab=NO_FluxPcpCab')
    DataSet = QrVSArtCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 152
    Top = 112
  end
  object frxDsFluxosIts: TfrxDBDataset
    UserName = 'frxDsFluxosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Ordem=Ordem'
      'Operacao=Operacao'
      'Acao1=Acao1'
      'Acao2=Acao2'
      'Acao3=Acao3'
      'Acao4=Acao4'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NOMEOPERACAO=NOMEOPERACAO'
      'SEQ=SEQ')
    DataSet = QrFluxosIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 236
    Top = 112
  end
  object QrVSArtCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fo1.Nome NO_ReceiRecu, '
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab, '
      'fro.Nome NO_FluxProCab, fpc.Nome NO_FluxPcpCab, vac.* '
      'FROM vsartcab vac '
      'LEFT JOIN fluxos       fro ON fro.Codigo=vac.FluxProCab '
      'LEFT JOIN fluxpcpcab   fpc ON fpc.Codigo=vac.FluxPcpCab '
      'LEFT JOIN formulas fo1 ON fo1.Numero=vac.ReceiRecu '
      'LEFT JOIN formulas fo2 ON fo2.Numero=vac.ReceiRefu '
      'LEFT JOIN formulas ti1 ON ti1.Numero=vac.ReceiAcab '
      'WHERE vac.Codigo > 0')
    Left = 148
    Top = 64
    object QrVSArtCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSArtCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSArtCabNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrVSArtCabNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrVSArtCabNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 30
    end
    object QrVSArtCabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrVSArtCabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrVSArtCabReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrVSArtCabTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrVSArtCabObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
    end
    object QrVSArtCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSArtCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSArtCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSArtCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSArtCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSArtCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSArtCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSArtCabFluxProCab: TIntegerField
      FieldName = 'FluxProCab'
    end
    object QrVSArtCabFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
    end
    object QrVSArtCabNO_FluxProCab: TWideStringField
      FieldName = 'NO_FluxProCab'
      Size = 100
    end
    object QrVSArtCabNO_FluxPcpCab: TWideStringField
      FieldName = 'NO_FluxPcpCab'
      Size = 100
    end
  end
  object QrVSPWEAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 324
    Top = 61
    object QrVSPWEAtuCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSPWEAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSPWEAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWEAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPWEAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPWEAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSPWEAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSPWEAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPWEAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSPWEAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSPWEAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSPWEAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuNO_CLIENT_MO: TWideStringField
      FieldName = 'NO_CLIENT_MO'
      Size = 100
    end
  end
  object frxDsVSPWEAtu: TfrxDBDataset
    UserName = 'frxDsVSPWEAtu'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CusFrtMORet=CusFrtMORet'
      'CusFrtMOEnv=CusFrtMOEnv'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'ReqMovEstq=ReqMovEstq'
      'CUSTO_M2=CUSTO_M2'
      'CUSTO_P2=CUSTO_P2'
      'NO_LOC_CEN=NO_LOC_CEN'
      'Marca=Marca'
      'PedItsLib=PedItsLib'
      'StqCenLoc=StqCenLoc'
      'NO_FICHA=NO_FICHA'
      'NO_FORNEC_MO=NO_FORNEC_MO'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_CLIENT_MO=NO_CLIENT_MO')
    DataSet = QrVSPWEAtu
    BCDToCurrency = False
    DataSetOptions = []
    Left = 324
    Top = 112
  end
  object QrVSPWEOriPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 412
    Top = 61
    object QrVSPWEOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrVSPWEOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSPWEOriPalletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPWEOriPalletAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriPalletAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSPWEOriPalletSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSPWEOriPalletFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSPWEOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVSPWEOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVSPWEOriPalletSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrVSPWEOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrVSPWEOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEOriPalletNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSPWEOriPalletSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEOriPalletSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriPalletSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object frxDsVSPWEOriPallet: TfrxDBDataset
    UserName = 'frxDsVSPWEOriPallet'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_PALLET'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'Series_E_Fichas=Series_E_Fichas'
      'Terceiro=Terceiro'
      'Marca=Marca'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2')
    DataSet = QrVSPWEOriPallet
    BCDToCurrency = False
    DataSetOptions = []
    Left = 412
    Top = 112
  end
  object frxWET_CURTI_233_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '                                                 '
      'end.')
    Left = 85
    Top = 237
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsVSPWECab
        DataSetName = 'frxDsVSPWECab'
      end
      item
        DataSet = frxDsVSArtCab
        DataSetName = 'frxDsVSArtCab'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsVSPWEAtu
        DataSetName = 'frxDsVSPWEAtu'
      end
      item
        DataSet = frxDsVSPWEOriPallet
        DataSetName = 'frxDsVSPWEOriPallet'
      end
      item
        DataSet = frxDsFluxosICt
        DataSetName = 'frxDsFluxosICt'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 359.055320710000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921215910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 370.393940000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OS: [frxDsVSPWECab."Codigo"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 71.811070000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classes:')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 71.811070000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Rebaixe')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 71.811070000000000000
          Width = 86.929160710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Espess.Semi')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 71.811070000000000000
          Width = 117.165368980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea [frxDsVSPWECab."NO_TIPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 90.708720000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_TIPO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 90.708720000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."Classes"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 90.708720000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."LinCulReb"] / [frxDsVSPWECab."LinCabReb"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 90.708720000000000000
          Width = 86.929160710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."LinCulSem"] / [frxDsVSPWECab."LinCabSem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 90.708720000000000000
          Width = 117.165368980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWECab."AREA_SEL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 109.606370000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."Nome"]'
            '[frxDsVSPWEAtu."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 181.417440000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data semi: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 181.417440000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataCrust"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 162.519790000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 162.519790000000000000
          Width = 457.323081180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."EmitGrLote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 30.236240000000000000
          Width = 400.629926140000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."ClientMO"] - [frxDsVSPWEAtu."NO_CLIENT_MO"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 109.606370000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 162.519790000000000000
          Width = 90.708666300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataPed"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 49.133890000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 49.133890000000000000
          Width = 185.196970000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 257.008040000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 257.008040000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 257.008040000000000000
          Width = 521.575022830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Top = 313.700990000000000000
          Width = 680.314960630000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 275.905690000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 275.905690000000000000
          Width = 196.535560000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 275.905690000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 275.905690000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Top = 275.905690000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada Wet Blue')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 49.133890000000000000
          Width = 400.629926140000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."GGXDst"] - [frxDsVSPWECab."NO_PRD_TAM_COR_DST"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 181.417440000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 181.417440000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataExped"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Top = 219.212740000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Configura'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 219.212740000000000000
          Width = 593.386026930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_VSArtCab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236240000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Contrato:')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 30.236240000000000000
          Width = 185.196970000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_EmitGru"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Top = 238.110390000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 238.110390000000000000
          Width = 593.386026930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_FLUXO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 578.268090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 75.590600000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 49.133890000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 75.590600000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 49.133890000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 173.858380000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSPWEOriPallet
        DataSetName = 'frxDsVSPWEOriPallet'
        RowCount = 0
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."Pallet"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."NO_PALLET"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[frxDsVSPWEOriPallet."NO_SerieFch"] [frxDsVSPWEOriPallet."Ficha"' +
              ']')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 34.015701650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."GraGruX"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."Pecas">]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."AreaM2">]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."Marca"]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Width = 143.622071650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."AreaP2">]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."PesoKg">]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S'#233'rie/Ficha')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 177.637841650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 514.016080000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        ReprintOnNewPage = True
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data e hora de entrada')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 3.779530000000000000
          Width = 173.858380000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Respons'#225'vel')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 3.779530000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data e hora do t'#233'rmino')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 661.417750000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosICt
        DataSetName = 'frxDsFluxosICt'
        RowCount = 0
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472431180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Par'#226'metro:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 325.039580000000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosICt
          DataSetName = 'frxDsFluxosICt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosICt."Acao1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 102.047310000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Width = 136.063080000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Obs.:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 56.692901180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Realizado:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrFluxosICt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fluxosict flc'
      'WHERE flc.Controle=:P0'
      'ORDER BY flc.Ordem, flc.Conta DESC')
    Left = 304
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosICtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFluxosICtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFluxosICtConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFluxosICtAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 60
    end
    object QrFluxosICtOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrFluxosICtSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object frxDsFluxosICt: TfrxDBDataset
    UserName = 'frxDsFluxosICt'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Acao1=Acao1'
      'Ordem=Ordem'
      'SEQ=SEQ')
    DataSet = QrFluxosICt
    BCDToCurrency = False
    DataSetOptions = []
    Left = 304
    Top = 236
  end
  object frxDsMPPIts: TfrxDBDataset
    UserName = 'frxDsMPPIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEMP=NOMEMP'
      'Codigo=Codigo'
      'Controle=Controle'
      'MP=MP'
      'Qtde=Qtde'
      'Preco=Preco'
      'Valor=Valor'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Texto=Texto'
      'Desco=Desco'
      'SubTo=SubTo'
      'Entrega=Entrega'
      'Pronto=Pronto'
      'PRONTO_TXT=PRONTO_TXT'
      'Status=Status'
      'Fluxo=Fluxo'
      'Classe=Classe'
      'M2Pedido=M2Pedido'
      'Pecas=Pecas'
      'Unidade=Unidade'
      'Observ=Observ'
      'NOMEUNIDADE=NOMEUNIDADE'
      'EspesTxt=EspesTxt'
      'CorTxt=CorTxt'
      'Pedido=Pedido'
      'AlterWeb=AlterWeb'
      'PrecoPed=PrecoPed'
      'ValorPed=ValorPed'
      'TipoProd=TipoProd'
      'NOMETIPOPROD=NOMETIPOPROD'
      'NOMEFLUXO=NOMEFLUXO'
      'Descricao=Descricao'
      'Complementacao=Complementacao'
      'Ativo=Ativo'
      'CustoPQ=CustoPQ'
      'GraGruX=GraGruX'
      'ReceiRecu=ReceiRecu'
      'ReceiRefu=ReceiRefu'
      'ReceiAcab=ReceiAcab'
      'TxtMPs=TxtMPs'
      'NO_Fluxo=NO_Fluxo'
      'NO_ReceiRecu=NO_ReceiRecu'
      'NO_ReceiRefu=NO_ReceiRefu'
      'NO_ReceiAcab=NO_ReceiAcab'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'CustoWB=CustoWB'
      'DtaCrust=DtaCrust'
      'VSArtCab=VSArtCab'
      'VSArtGGX=VSArtGGX'
      'VSArtCab_TXT=VSArtCab_TXT'
      'FluxPcpCab=FluxPcpCab')
    DataSet = QrMPPIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 492
    Top = 236
  end
  object frxWET_CURTI_233_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '                                                 '
      'end.')
    Left = 65
    Top = 13
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsVSPWECab
        DataSetName = 'frxDsVSPWECab'
      end
      item
        DataSet = frxDsVSArtCab
        DataSetName = 'frxDsVSArtCab'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsVSPWEAtu
        DataSetName = 'frxDsVSPWEAtu'
      end
      item
        DataSet = frxDsVSPWEOriPallet
        DataSetName = 'frxDsVSPWEOriPallet'
      end
      item
        DataSet = frxDsMPPIts
        DataSetName = 'frxDsMPPIts'
      end
      item
        DataSet = frxDsMPP
        DataSetName = 'frxDsMPP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 487.559370000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 309.921215910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'OS: [frxDsMPPIts."Controle"]    [frxDsMPPIts."NOMETIPOPROD"]    ' +
              'OP: [frxDsVSPWECab."Codigo"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 449.764070000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 449.764070000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 449.764070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classes:')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 449.764070000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Rebaixe')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 449.764070000000000000
          Width = 86.929160710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Espess.Semi')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 449.764070000000000000
          Width = 117.165368980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea [frxDsVSPWECab."NO_TIPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 468.661720000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 468.661720000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_TIPO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 468.661720000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."Classes"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 468.661720000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."LinCulReb"] / [frxDsVSPWECab."LinCabReb"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 468.661720000000000000
          Width = 86.929160710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."LinCulSem"] / [frxDsVSPWECab."LinCabSem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 468.661720000000000000
          Width = 117.165368980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWECab."AREA_SEL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 136.063080000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."Observ"]'
            '[frxDsVSPWEAtu."Observ"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data semi: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 188.976500000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataCrust"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 30.236240000000000000
          Width = 400.629926140000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."ClientMO"] - [frxDsVSPWEAtu."NO_CLIENT_MO"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 136.063080000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 226.771800000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta Pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 226.771800000000000000
          Width = 68.031486300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataPed"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 49.133890000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 49.133890000000000000
          Width = 185.196970000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEAtu."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 264.567100000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 264.567100000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 264.567100000000000000
          Width = 521.575022830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Top = 321.260050000000000000
          Width = 680.314960630000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 283.464750000000000000
          Width = 196.535560000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 283.464750000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Top = 283.464750000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada Wet Blue')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 404.409710000000000000
          Width = 680.314960630000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 366.614410000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 366.614410000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 366.614410000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Top = 366.614410000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada no Acabamento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 49.133890000000000000
          Width = 400.629926140000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."GGXDst"] - [frxDsVSPWECab."NO_PRD_TAM_COR_DST"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 188.976500000000000000
          Width = 226.771653540000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPWECab."DataExped"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Top = 226.771800000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Configura'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 226.771800000000000000
          Width = 453.543416930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_VSArtCab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236240000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Contrato:')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 30.236240000000000000
          Width = 185.196970000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_EmitGru"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Top = 245.669450000000000000
          Width = 86.929006930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 245.669450000000000000
          Width = 453.543416930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWECab."NO_FLUXO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 68.031540000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 68.031540000000000000
          Width = 347.716506140000000000
          Height = 15.118110240000000000
          DataSet = FmMPP.frxDsMPP
          DataSetName = 'frxDsMPP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NOMETRANSP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 83.149660000000000000
          Width = 347.716760000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."NO_CONDICAOPG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 83.149660000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'OC:')
          ParentFont = False
          WordWrap = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 83.149660000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPP."PedidCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Top = 68.031540000000000000
          Width = 98.267780000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 98.267780000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Condi'#231#227'o pagto.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 245.669450000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N'#176' Pedido')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 245.669450000000000000
          Width = 68.031486300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."Pedido"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 98.267780000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 98.267780000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 98.267780000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 98.267780000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 98.267780000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsMPPIts."Peca' +
              's">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 117.165430000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 117.165430000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 117.165430000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 117.165430000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMPPIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 117.165430000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMPPIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 117.165430000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPPIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.803174020000000000
        Top = 706.772110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao1: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao1"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao2: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao2'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao2"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao3: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao3'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao3"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao4'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao4"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 17.007874020000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 17.007874020000000000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 17.007874020000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 17.007874020000000000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 17.007874020000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 17.007874020000000000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000300000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 34.015770000000300000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000300000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000300000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 34.015770000000300000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000300000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 604.724800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSPWEOriPallet
        DataSetName = 'frxDsVSPWEOriPallet'
        RowCount = 0
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."Pallet"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."NO_PALLET"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[frxDsVSPWEOriPallet."NO_SerieFch"] [frxDsVSPWEOriPallet."Ficha"' +
              ']')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 34.015701650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."GraGruX"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."Pecas">]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."AreaM2">]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."Marca"]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Width = 143.622071650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPWEOriPallet."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."AreaP2">]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSPWEOriPallet."PesoKg">]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S'#233'rie/Ficha')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 177.637841650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 79.370061650000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSPWEOriPallet
          DataSetName = 'frxDsVSPWEOriPallet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 642.520100000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 665.197280000000000000
        Width = 680.315400000000000000
        ReprintOnNewPage = True
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 1')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 3.779530000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 2')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 3')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 3.779530000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 4')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 786.142240000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrMPPIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMPPItsCalcFields
    SQL.Strings = (
      'SELECT pec.Nome NOMEUNIDADE, '
      'ag.Nome NOMEMP, flu.Nome NOMEFLUXO, mvi.*,'
      'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,'
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM mpvits mvi'
      'LEFT JOIN defpecas pec ON pec.Codigo=mvi.Unidade'
      'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP'
      'LEFT JOIN fluxos flu ON flu.Codigo=mvi.Fluxo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=mvi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN formulas fo1 ON fo1.Numero=mvi.ReceiRecu'
      'LEFT JOIN formulas fo2 ON fo2.Numero=mvi.ReceiRefu'
      'LEFT JOIN tintascab ti1 ON ti1.Numero=mvi.ReceiAcab'
      'WHERE mvi.Pedido<>0'
      'AND mvi.Pedido=:P0')
    Left = 492
    Top = 185
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPPItsNOMEMP: TWideStringField
      FieldName = 'NOMEMP'
      Origin = 'artigosgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrMPPItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'MPVIts.Codigo'
      Required = True
    end
    object QrMPPItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'MPVIts.Controle'
      Required = True
    end
    object QrMPPItsMP: TIntegerField
      FieldName = 'MP'
      Origin = 'MPVIts.MP'
      Required = True
    end
    object QrMPPItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'MPVIts.Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'MPVIts.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'MPVIts.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'MPVIts.Lk'
    end
    object QrMPPItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'MPVIts.DataCad'
    end
    object QrMPPItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'MPVIts.DataAlt'
    end
    object QrMPPItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'MPVIts.UserCad'
    end
    object QrMPPItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'MPVIts.UserAlt'
    end
    object QrMPPItsTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'MPVIts.Texto'
      Required = True
      Size = 50
    end
    object QrMPPItsDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'MPVIts.Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsSubTo: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SubTo'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMPPItsEntrega: TDateField
      FieldName = 'Entrega'
      Origin = 'MPVIts.Entrega'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPPItsPronto: TDateField
      FieldName = 'Pronto'
      Origin = 'MPVIts.Pronto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPPItsPRONTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRONTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrMPPItsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'MPVIts.Status'
      Required = True
    end
    object QrMPPItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Origin = 'MPVIts.Fluxo'
      Required = True
    end
    object QrMPPItsClasse: TWideStringField
      FieldName = 'Classe'
      Origin = 'MPVIts.Classe'
      Size = 15
    end
    object QrMPPItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Origin = 'MPVIts.M2Pedido'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'MPVIts.Pecas'
      Required = True
    end
    object QrMPPItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'MPVIts.Unidade'
      Required = True
    end
    object QrMPPItsObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'MPVIts.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsNOMEUNIDADE: TWideStringField
      FieldName = 'NOMEUNIDADE'
      Origin = 'defpecas.Nome'
      Size = 30
    end
    object QrMPPItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Origin = 'MPVIts.EspesTxt'
      Size = 10
    end
    object QrMPPItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Origin = 'MPVIts.CorTxt'
      Size = 30
    end
    object QrMPPItsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrMPPItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMPPItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPPItsTipoProd: TSmallintField
      FieldName = 'TipoProd'
      Required = True
    end
    object QrMPPItsNOMETIPOPROD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOPROD'
      Size = 15
      Calculated = True
    end
    object QrMPPItsNOMEFLUXO: TWideStringField
      FieldName = 'NOMEFLUXO'
      Size = 100
    end
    object QrMPPItsDescricao: TWideMemoField
      FieldName = 'Descricao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPPItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMPPItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMPPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrMPPItsReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrMPPItsReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrMPPItsReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrMPPItsTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrMPPItsNO_Fluxo: TWideStringField
      FieldName = 'NO_Fluxo'
      Size = 100
    end
    object QrMPPItsNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrMPPItsNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrMPPItsNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 30
    end
    object QrMPPItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrMPPItsCustoWB: TFloatField
      FieldName = 'CustoWB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMPPItsDtaCrust: TDateField
      FieldName = 'DtaCrust'
    end
    object QrMPPItsVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
    end
    object QrMPPItsVSArtGGX: TIntegerField
      FieldName = 'VSArtGGX'
    end
    object QrMPPItsVSArtCab_TXT: TWideStringField
      FieldName = 'VSArtCab_TXT'
      Size = 255
    end
    object QrMPPItsFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
    end
  end
  object QrMPP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial'
      'ELSE ve.Nome END NOMEVENDEDOR,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSP,'
      'pc.Nome NO_CONDICAOPG,'
      'MPP.*'
      'FROM mpp MPP'
      'LEFT JOIN entidades cl ON cl.Codigo=MPP.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=MPP.Vendedor'
      'LEFT JOIN entidades tr ON tr.Codigo=MPP.Transp'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=mpp.CondicaoPg'
      'WHERE MPP.Codigo > 0')
    Left = 440
    Top = 185
    object QrMPPNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrMPPNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMPPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpp.Codigo'
    end
    object QrMPPCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'mpp.Cliente'
    end
    object QrMPPVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'mpp.Vendedor'
    end
    object QrMPPDataF: TDateField
      FieldName = 'DataF'
      Origin = 'mpp.DataF'
    end
    object QrMPPQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'mpp.Qtde'
    end
    object QrMPPValor: TFloatField
      FieldName = 'Valor'
      Origin = 'mpp.Valor'
    end
    object QrMPPObz: TWideStringField
      FieldName = 'Obz'
      Origin = 'mpp.Obz'
      Size = 255
    end
    object QrMPPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpp.Lk'
    end
    object QrMPPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpp.DataCad'
    end
    object QrMPPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpp.DataAlt'
    end
    object QrMPPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpp.UserCad'
    end
    object QrMPPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpp.UserAlt'
    end
    object QrMPPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpp.AlterWeb'
    end
    object QrMPPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMPPTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrMPPCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrMPPNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrMPPNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrMPPPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
  object frxDsMPP: TfrxDBDataset
    UserName = 'frxDsMPP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEVENDEDOR=NOMEVENDEDOR'
      'NOMECLIENTE=NOMECLIENTE'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'Vendedor=Vendedor'
      'DataF=DataF'
      'Qtde=Qtde'
      'Valor=Valor'
      'Obz=Obz'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Transp=Transp'
      'CondicaoPg=CondicaoPg'
      'NOMETRANSP=NOMETRANSP'
      'NO_CONDICAOPG=NO_CONDICAOPG'
      'PedidCli=PedidCli')
    DataSet = QrMPP
    BCDToCurrency = False
    DataSetOptions = []
    Left = 440
    Top = 236
  end
end
