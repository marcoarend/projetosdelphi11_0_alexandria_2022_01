unit VSReclassPrePal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSReclassPrePal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPallets: TmySQLQuery;
    QrPalletsEmpresa: TIntegerField;
    QrPalletsMovimID: TIntegerField;
    QrPalletsGraGruX: TIntegerField;
    QrPalletsPallet: TIntegerField;
    QrPalletsVMI_Codi: TIntegerField;
    QrPalletsVMI_Ctrl: TIntegerField;
    QrPalletsVMI_MovimNiv: TIntegerField;
    QrPalletsTerceiro: TIntegerField;
    QrPalletsFicha: TIntegerField;
    QrPalletsDataHora: TDateTimeField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsAreaP2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsNO_EMPRESA: TWideStringField;
    QrPalletsNO_FORNECE: TWideStringField;
    DsPallets: TDataSource;
    Panel5: TPanel;
    LaVSRibCad: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet: TSpeedButton;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrPalletsGraGruY: TIntegerField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrPalletsClientMO: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel6: TPanel;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdVSPwdDdClas: TEdit;
    Label35: TLabel;
    CkReclasse: TCheckBox;
    QrStqCenLocEntiSitio: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkReclasseClick(Sender: TObject);
  private
    { Private declarations }
    FCodigo, FMovimCod, FControle: Integer;
    //
    procedure InsereBaixaAtual(Codigo, MovimCod: Integer; DataHora: String;
              NewVMI, StqCenLoc: Integer);
    procedure InsereNovoVMI_DoPallet(Pallet, Controle, Codigo, MovimCod:
              Integer; DataHora: String; Pecas, PesoKg, AreaM2, AreaP2: Double;
              SerieFch, Ficha, Terceiro, VSMulFrnCab, FornecMO: Integer; Marca:
              String; CustoMOKg, CustoMOTot, ValorMP, ValorT: Double; StqCenLoc:
              Integer);
    procedure ReopenPallets();
  public
    { Public declarations }
    FPallet, FStqCenLoc, FFornecMO: Integer;
  end;

  var
  FmVSReclassPrePal: TFmVSReclassPrePal;

implementation

uses UnMyObjects, Module, DmkDAC_PF, AppListas, ModuleGeral, UMySQLModule,
  ModVS_CRC, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSReclassPrePal.BtOKClick(Sender: TObject);
var
  NewVMI, Codigo, MovimCod: Integer;
  DataHora: String;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SerieFch, Ficha, Terceiro, FornecMO, VSMulFrnCab, StqCenLoc: Integer;
  Marca: String;
  CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
var
  SQL_Rcl, SQL_15: String;
begin
  if CkReclasse.Checked then
  begin
    SQL_Rcl := ' ';
    SQL_15  := ', 15';
  end else
  begin
    SQL_Rcl := 'AND vsp.GerRclCab = 0 ';
    SQL_15   := '';
  end;
  //
  Codigo   := FCodigo;
  MovimCod := FMovimCod;
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local! (7)') then
    Exit;
  //
  if VS_CRC_PF.SenhaVSPwdDdNaoConfere(EdVSPwdDdClas.Text) then
    Exit;
  //
  FPallet    := EdPallet.ValueVariant;
  FStqCenLoc := StqCenLoc;
  FFornecMO  := QrStqCenLocEntiSitio.Value;
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT vmi.*, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ggx.GraGruY',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet  ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=vmi.Terceiro ',
  'WHERE vmi.Pecas > 0 ',
  'AND vmi.Empresa=' + Geral.FF0(QrPalletsEmpresa.Value),
  'AND vmi.Pallet > 0 ',
  'AND vmi.Pallet=' + Geral.FF0(FPallet),
  'AND vmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND MovimID IN (' + CO_ALL_CODS_CLASS_VS + SQL_15 + ') ',
  'ORDER BY Controle ',
  '']);
  if MyObjects.FIC(QrVSMovIts.RecordCount = 0, nil,
  'Pallet sem IME-Is com saldo!!') then
    Exit;
  //
  Pecas       := 0;
  PesoKg      := 0;
  AreaM2      := 0;
  AreaP2      := 0;
  SerieFch    := QrVSMovItsSerieFch.Value;
  Ficha       := QrVSMovItsFicha.Value;
  Terceiro    := QrVSMovItsTerceiro.Value;
  //FornecMO    := QrVSMovItsFornecMO.Value;
  FornecMO    := QrVSMovItsEmpresa.Value;
  Marca       := QrVSMovItsMarca.Value;
  VSMulFrnCab := QrVSMovItsVSMulFrnCab.Value;
  CustoMOKg   := 0;
  CustoMOTot  := 0;
  ValorMP     := 0;
  ValorT      := 0;

  DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
  //Controle :=
  NewVMI := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, FControle);
  Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  QrVSMovIts.First;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsprepalcab', False, [
  'Pallet'], ['Codigo'], [FPallet], [Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidPreReclasse, Codigo);
    while not QrVSMovIts.Eof do
    begin
      Pecas  := Pecas + QrVSMovItsSdoVrtPeca.Value;
      PesoKg := PesoKg + QrVSMovItsSdoVrtPeso.Value;
      AreaM2 := AreaM2 + QrVSMovItsSdoVrtArM2.Value;
/////////////////////////////////////////////////////////////////////////////////
      // ini 2023-04-04
(*
      // ini 2022-06-17
      if QrVSMovItsAreaM2.Value <> 0 then
        ValorT := QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value * QrVSMovItsSdoVrtArM2.Value
      else
      if QrVSMovItsPesoKg.Value <> 0 then
        ValorT := QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value * QrVSMovItsSdoVrtPeso.Value
      else
      if QrVSMovItsPecas.Value <> 0 then
        ValorT := QrVSMovItsValorT.Value / QrVSMovItsPecas.Value * QrVSMovItsSdoVrtPeca.Value;
      // fim 2022-06-17
*)
      if QrVSMovItsAreaM2.Value <> 0 then
        ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value * QrVSMovItsSdoVrtArM2.Value)
      else
      if QrVSMovItsPesoKg.Value <> 0 then
        ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value * QrVSMovItsSdoVrtPeso.Value)
      else
      if QrVSMovItsPecas.Value <> 0 then
        ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value * QrVSMovItsSdoVrtPeca.Value);
      // fim 2023-04-04
/////////////////////////////////////////////////////////////////////////////////


      //AreaP2 := 0;
      if SerieFch <> QrVSMovItsSerieFch.Value then
        SerieFch := 0;
      if Ficha <> QrVSMovItsFicha.Value then
        Ficha := 0;
      if Terceiro <> QrVSMovItsTerceiro.Value then
        Terceiro := 0;
      //if FornecMO <> QrVSMovItsFornecMO.Value then
      //  FornecMO := 0;
      if Marca <> QrVSMovItsMarca.Value then
        Marca := '';
      if VSMulFrnCab <> QrVSMovItsVSMulFrnCab.Value then
        VSMulFrnCab := 0;
      //
      if (*((Terceiro = 0) and (VSMulFrnCab = 0) )
      or*) ((Terceiro <> 0) and (VSMulFrnCab <> 0) ) then
      begin
        Terceiro    := 0;
        VSMulFrnCab := 0;
      end;
      InsereBaixaAtual(Codigo, MovimCod, DataHora, NewVMI, StqCenLoc);
      //QrVSMovIts.First;
      //QrVSMovIts.First;
      //
      QrVSMovIts.Next;
    end;
    //
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    InsereNovoVMI_DoPallet(FPallet, NewVMI, Codigo, MovimCod, DataHora, Pecas,
      PesoKg, AreaM2, AreaP2, SerieFch, Ficha, Terceiro, VSMulFrnCab, FornecMO,
      Marca, CustoMOKg, CustoMOTot, ValorMP, ValorT, StqCenLoc);
    //
    //DmModVS.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
    //MovimCod, TEstqMovimID.emidPreReclasse, [eminSorcPreReclas], [eminDestPreReclas]);
    //
    Close;
  end;
end;

procedure TFmVSReclassPrePal.BtSaidaClick(Sender: TObject);
begin
  FPallet    := 0;
  FStqCenLoc := 0;
  FFornecMO  := 0;
  //
  Close;
end;

procedure TFmVSReclassPrePal.CkReclasseClick(Sender: TObject);
begin
  ReopenPallets();
end;

procedure TFmVSReclassPrePal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSReclassPrePal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FPallet    := 0;
  FStqCenLoc := 0;
  FFornecMO  := 0;
  FCodigo    := 0;
  FMovimCod  := 0;
  FControle  := 0;
  //
  ReopenPallets();
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSReclassPrePal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSReclassPrePal.InsereBaixaAtual(Codigo, MovimCod: Integer;
  DataHora: String; NewVMI, StqcenLoc: Integer);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora,
  Observ, Marca: String;
  //Codigo, MovimCod,
  Pallet, Controle, Empresa, GraGruX, Terceiro, VSMulFrnCab, Ficha,
  GraGruY, FornecMO, SerieFch, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  SrcMovID, DstMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, DstGGX: Integer;
begin
  //Codigo         := Codigo;
  //Controle       := ??;
  //MovimCod       := 0;
  Empresa        := QrVSMovItsEmpresa.Value;
  ClientMO       := QrVSMovItsClientMO.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminSorcPreReclas;
  GraGruX        := QrVSMovItsGraGruX.Value;
  Pallet         := QrVSMovItsPallet.Value;
  //DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := QrVSMovItsSdoVrtPeca.Value;
  PesoKg         := QrVSMovItsSdoVrtPeso.Value;
  AreaM2         := QrVSMovItsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(QrVSMovItsSdoVrtArM2.Value, ctM2toP2, cfQuarto);
  // Ver depois?
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  //FornecMO       := QrVSMovItsFornecMO.Value;
  FornecMO       := QrVSMovItsEmpresa.Value;
  Marca          := QrVSMovItsMarca.Value;
  CustoMOKg      := 0;
  CustoMOM2      := 0;
  CustoMOTot     := 0;
  ValorMP        := 0;
  // ini 2022-06-17
  //ValorT         := 0;
  // ini 2023-04-04
  //ValorT          := QrVSMovItsValorT.Value;
  if QrVSMovItsAreaM2.Value <> 0 then
    ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value * QrVSMovItsSdoVrtArM2.Value)
  else
  if QrVSMovItsPesoKg.Value <> 0 then
    ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value * QrVSMovItsSdoVrtPeso.Value)
  else
  if QrVSMovItsPecas.Value <> 0 then
    ValorT := ValorT + (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value * QrVSMovItsSdoVrtPeca.Value);
  // fim 2023-04-04
  // ini 2022-06-17
  Observ         := '';
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  SrcMovID   := TEstqMovimID(QrVSMovItsMovimID.Value);
  SrcNivel1  := QrVSMovItsCodigo.Value;
  SrcNivel2  := QrVSMovItsControle.Value;
  SrcGGX     := QrVSMovItsGraGruX.Value;
  //
  DstMovID   := emidPreReclasse;
  DstNivel1  := Codigo;
  DstNivel2  := NewVMI;
  DstGGX     := QrVSMovItsGraGruX.Value;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, FControle);
  //
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, -Pecas, -PesoKg, -AreaM2, -AreaP2,
  -ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei039(*Baixa na preparação de pallet para reclassificação 1/2*)) then
  begin
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, True);

    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
  end;
end;

procedure TFmVSReclassPrePal.InsereNovoVMI_DoPallet(Pallet, Controle, Codigo,
MovimCod: Integer; DataHora: String; Pecas, PesoKg, AreaM2, AreaP2: Double;
SerieFch, Ficha, Terceiro, VSMulFrnCab, FornecMO: Integer; Marca: String;
CustoMOKg, CustoMOTot, ValorMP, ValorT: Double; StqCenLoc: Integer);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  CustoMOM2  = 0;
  ItemNFe    = 0;
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora, Marca,
  Observ: String;
  //Pallet, Codigo, MovimCod, Controle, Terceiro, Ficha, FornecMO, SerieFch,
  Empresa, ClientMO, GraGruX, GraGruY, TpCalcAuto, PedItsLib, PedItsFin,
  GSPSrcNiv2, ReqMovEstq, PedItsVda: Integer;
  GSPSrcMovID: TEstqMovimID;
  //Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
begin
  //Codigo         := FCodigo;
  //Controle       := NewVMI;
  //MovimCod       := FMovimCod;
  Empresa        := QrPalletsEmpresa.Value;
  ClientMO       := QrPalletsClientMO.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminDestPreReclas;
  GraGruX        := QrPalletsGraGruX.Value;
(*
  Pallet         := EdPallet.ValueVariant;
  DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  // Ver depois?
  SerieFch       := 0;
  Ficha          := 0;
  Terceiro       := 0;
  FornecMO       := 0;
  Marca          := '';
  CustoMOKg      := ;
  CustoMOTot     := ;
  ValorMP        := ;
  ValorT         := ;
*)
  Observ         := '';
  GraGruY        := QrPalletsGraGruY.Value;
  //
  // ver o que fazer!!!
  PedItsLib     := 0; // QrVSMovItsPedItsLib.Value;
  PedItsFin     := 0; // QrVSMovItsPedItsFin.Value;
  PedItsVda     := 0; // QrVSMovItsPedItsVda.Value;
  GSPSrcMovID    := TEstqMovimID(0);
  GSPSrcNiv2    := 0; //
  ReqMovEstq    := QrVSMovItsReqMovEstq.Value;
  //StqCenLoc     := QrVSMovItsStqCenLoc.Value;
  //
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  //
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei038(*Preparação de pallet para reclassificação 1/2*)) then
  begin
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
    if (Terceiro = 0) and (VSMulFrnCab = 0) then
      VS_CRC_PF.AtualizaFornecedorPreRcl(MovimCod);
  end;
end;

procedure TFmVSReclassPrePal.ReopenPallets();
var
  SQL_Rcl, SQL_15: String;
begin
  if CkReclasse.Checked then
  begin
    SQL_Rcl := ' ';
    SQL_15  := ', 15';
  end else
  begin
    SQL_Rcl  := 'AND vsp.GerRclCab = 0 ';
    SQL_15   := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  'SELECT vmi.Empresa, vmi.ClientMO, vmi.MovimID, vmi.GraGruX, ',
  'vmi.Pallet, vmi.Codigo VMI_Codi, vmi.Controle VMI_Ctrl, ',
  'vmi.MovimNiv VMI_MovimNiv, vmi.Terceiro, vmi.Ficha, ',
  'MAX(DataHora) DataHora, SUM(vmi.Pecas) Pecas,   ',
  'IF(MIN(AreaM2) > 0.01, 0, SUM(vmi.AreaM2)) AreaM2,   ',
  'IF(MIN(AreaP2) > 0.01, 0, SUM(vmi.AreaP2)) AreaP2,   ',
  'IF(MIN(PesoKg) > 0.001, 0, SUM(vmi.PesoKg)) PesoKg,   ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'ggx.GraGruY ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet    ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=vmi.Terceiro   ',
  'WHERE vmi.Pecas > 0 ',
  //'AND vmi.Empresa=-11  ',
  'AND vmi.Pallet > 0 ',
  'AND vmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND MovimID IN (' + CO_ALL_CODS_CLASS_VS + SQL_15 + ') ',
  'GROUP BY vmi.Pallet, vmi.GraGruX, vmi.Empresa ',
  'ORDER BY NO_PRD_TAM_COR ',
  ' ']);
end;

end.
