unit VSPSPCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnProjGroup_Consts,
  UnMyObjects, UnVS_PF, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSPSPCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSPSPCab: TmySQLQuery;
    DsVSPSPCab: TDataSource;
    QrVSPSPAtu: TmySQLQuery;
    DsVSPSPAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSPSPCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSPSPCabEmpresa: TIntegerField;
    QrVSPSPCabDtHrAberto: TDateTimeField;
    QrVSPSPCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSPSPCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSPSPOriIMEI: TmySQLQuery;
    DsVSPSPOriIMEI: TDataSource;
    QrVSPSPCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSPSPCabLk: TIntegerField;
    QrVSPSPCabDataCad: TDateField;
    QrVSPSPCabDataAlt: TDateField;
    QrVSPSPCabUserCad: TIntegerField;
    QrVSPSPCabUserAlt: TIntegerField;
    QrVSPSPCabAlterWeb: TSmallintField;
    QrVSPSPCabAtivo: TSmallintField;
    QrVSPSPCabPecasMan: TFloatField;
    QrVSPSPCabAreaManM2: TFloatField;
    QrVSPSPCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSPSPCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSPSPCabNO_TIPO: TWideStringField;
    QrVSPSPCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSPSPCabNO_DtHrLibOpe: TWideStringField;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSPSPCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSPSPCabCacCod: TIntegerField;
    QrVSPSPCabGraGruX: TIntegerField;
    QrVSPSPCabCustoManMOKg: TFloatField;
    QrVSPSPCabCustoManMOTot: TFloatField;
    QrVSPSPCabValorManMP: TFloatField;
    QrVSPSPCabValorManT: TFloatField;
    QrVSPSPCabDtHrLibOpe: TDateTimeField;
    QrVSPSPCabDtHrCfgOpe: TDateTimeField;
    QrVSPSPCabDtHrFimOpe: TDateTimeField;
    QrVSPSPCabPecasSrc: TFloatField;
    QrVSPSPCabAreaSrcM2: TFloatField;
    QrVSPSPCabAreaSrcP2: TFloatField;
    QrVSPSPCabPecasDst: TFloatField;
    QrVSPSPCabAreaDstM2: TFloatField;
    QrVSPSPCabAreaDstP2: TFloatField;
    QrVSPSPCabPecasSdo: TFloatField;
    QrVSPSPCabAreaSdoM2: TFloatField;
    QrVSPSPCabAreaSdoP2: TFloatField;
    DsVSPSPDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    AtualizaestoqueEmOperao1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid1: TDBGrid;
    QrVSPSPBxa: TmySQLQuery;
    DsVSPSPBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSPSPCabPesoKgSrc: TFloatField;
    QrVSPSPCabPesoKgMan: TFloatField;
    QrVSPSPCabPesoKgDst: TFloatField;
    QrVSPSPCabPesoKgSdo: TFloatField;
    QrVSPSPCabValorTMan: TFloatField;
    QrVSPSPCabValorTSrc: TFloatField;
    QrVSPSPCabValorTSdo: TFloatField;
    QrVSPSPCabPecasINI: TFloatField;
    QrVSPSPCabAreaINIM2: TFloatField;
    QrVSPSPCabAreaINIP2: TFloatField;
    QrVSPSPCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSPSPCabPesoKgBxa: TFloatField;
    QrVSPSPCabPecasBxa: TFloatField;
    QrVSPSPCabAreaBxaM2: TFloatField;
    QrVSPSPCabAreaBxaP2: TFloatField;
    QrVSPSPCabValorTBxa: TFloatField;
    Definiodequantidadesmanualmente1: TMenuItem;
    QrVSPSPOriPallet: TmySQLQuery;
    DsVSPSPOriPallet: TDataSource;
    QrVSPSPOriPalletPecas: TFloatField;
    QrVSPSPOriPalletAreaM2: TFloatField;
    QrVSPSPOriPalletAreaP2: TFloatField;
    QrVSPSPOriPalletPesoKg: TFloatField;
    QrVSPSPOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPSPOriPalletNO_Pallet: TWideStringField;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    porIMEI1: TMenuItem;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    Label48: TLabel;
    DBEdit45: TDBEdit;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label51: TLabel;
    DBEdit40: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit41: TDBEdit;
    Label52: TLabel;
    Corrigirfornecedor1: TMenuItem;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    QrVSPSPCabNFeRem: TIntegerField;
    QrVSPSPCabLPFMO: TWideStringField;
    DBEdit44: TDBEdit;
    Label58: TLabel;
    Label59: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label56: TLabel;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label57: TLabel;
    Ordensdeoperaoemaberto1: TMenuItem;
    porIMEIParcial1: TMenuItem;
    QrVSPSPCabTemIMEIMrt: TIntegerField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    QrVSPSPOriIMEICodigo: TLargeintField;
    QrVSPSPOriIMEIControle: TLargeintField;
    QrVSPSPOriIMEIMovimCod: TLargeintField;
    QrVSPSPOriIMEIMovimNiv: TLargeintField;
    QrVSPSPOriIMEIMovimTwn: TLargeintField;
    QrVSPSPOriIMEIEmpresa: TLargeintField;
    QrVSPSPOriIMEITerceiro: TLargeintField;
    QrVSPSPOriIMEICliVenda: TLargeintField;
    QrVSPSPOriIMEIMovimID: TLargeintField;
    QrVSPSPOriIMEIDataHora: TDateTimeField;
    QrVSPSPOriIMEIPallet: TLargeintField;
    QrVSPSPOriIMEIGraGruX: TLargeintField;
    QrVSPSPOriIMEIPecas: TFloatField;
    QrVSPSPOriIMEIPesoKg: TFloatField;
    QrVSPSPOriIMEIAreaM2: TFloatField;
    QrVSPSPOriIMEIAreaP2: TFloatField;
    QrVSPSPOriIMEIValorT: TFloatField;
    QrVSPSPOriIMEISrcMovID: TLargeintField;
    QrVSPSPOriIMEISrcNivel1: TLargeintField;
    QrVSPSPOriIMEISrcNivel2: TLargeintField;
    QrVSPSPOriIMEISrcGGX: TLargeintField;
    QrVSPSPOriIMEISdoVrtPeca: TFloatField;
    QrVSPSPOriIMEISdoVrtPeso: TFloatField;
    QrVSPSPOriIMEISdoVrtArM2: TFloatField;
    QrVSPSPOriIMEIObserv: TWideStringField;
    QrVSPSPOriIMEISerieFch: TLargeintField;
    QrVSPSPOriIMEIFicha: TLargeintField;
    QrVSPSPOriIMEIMisturou: TLargeintField;
    QrVSPSPOriIMEIFornecMO: TLargeintField;
    QrVSPSPOriIMEICustoMOKg: TFloatField;
    QrVSPSPOriIMEICustoMOM2: TFloatField;
    QrVSPSPOriIMEICustoMOTot: TFloatField;
    QrVSPSPOriIMEIValorMP: TFloatField;
    QrVSPSPOriIMEIDstMovID: TLargeintField;
    QrVSPSPOriIMEIDstNivel1: TLargeintField;
    QrVSPSPOriIMEIDstNivel2: TLargeintField;
    QrVSPSPOriIMEIDstGGX: TLargeintField;
    QrVSPSPOriIMEIQtdGerPeca: TFloatField;
    QrVSPSPOriIMEIQtdGerPeso: TFloatField;
    QrVSPSPOriIMEIQtdGerArM2: TFloatField;
    QrVSPSPOriIMEIQtdGerArP2: TFloatField;
    QrVSPSPOriIMEIQtdAntPeca: TFloatField;
    QrVSPSPOriIMEIQtdAntPeso: TFloatField;
    QrVSPSPOriIMEIQtdAntArM2: TFloatField;
    QrVSPSPOriIMEIQtdAntArP2: TFloatField;
    QrVSPSPOriIMEINotaMPAG: TFloatField;
    QrVSPSPOriIMEINO_PALLET: TWideStringField;
    QrVSPSPOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSPSPOriIMEINO_TTW: TWideStringField;
    QrVSPSPOriIMEIID_TTW: TLargeintField;
    QrVSPSPOriIMEINO_FORNECE: TWideStringField;
    QrVSPSPOriIMEINO_SerieFch: TWideStringField;
    QrVSPSPOriIMEIReqMovEstq: TLargeintField;
    QrVSPSPOriPalletPallet: TLargeintField;
    QrVSPSPOriPalletGraGruX: TLargeintField;
    QrVSPSPOriPalletSerieFch: TLargeintField;
    QrVSPSPOriPalletFicha: TLargeintField;
    QrVSPSPOriPalletNO_TTW: TWideStringField;
    QrVSPSPOriPalletID_TTW: TLargeintField;
    QrVSPSPOriPalletSeries_E_Fichas: TWideStringField;
    QrVSPSPOriPalletTerceiro: TLargeintField;
    QrVSPSPOriPalletMarca: TWideStringField;
    QrVSPSPOriPalletNO_FORNECE: TWideStringField;
    QrVSPSPOriPalletNO_SerieFch: TWideStringField;
    QrVSPSPOriPalletValorT: TFloatField;
    QrVSPSPOriPalletSdoVrtPeca: TFloatField;
    QrVSPSPOriPalletSdoVrtPeso: TFloatField;
    QrVSPSPOriPalletSdoVrtArM2: TFloatField;
    QrVSPSPDst: TmySQLQuery;
    QrVSPSPDstCodigo: TLargeintField;
    QrVSPSPDstControle: TLargeintField;
    QrVSPSPDstMovimCod: TLargeintField;
    QrVSPSPDstMovimNiv: TLargeintField;
    QrVSPSPDstMovimTwn: TLargeintField;
    QrVSPSPDstEmpresa: TLargeintField;
    QrVSPSPDstTerceiro: TLargeintField;
    QrVSPSPDstCliVenda: TLargeintField;
    QrVSPSPDstMovimID: TLargeintField;
    QrVSPSPDstDataHora: TDateTimeField;
    QrVSPSPDstPallet: TLargeintField;
    QrVSPSPDstGraGruX: TLargeintField;
    QrVSPSPDstPecas: TFloatField;
    QrVSPSPDstPesoKg: TFloatField;
    QrVSPSPDstAreaM2: TFloatField;
    QrVSPSPDstAreaP2: TFloatField;
    QrVSPSPDstValorT: TFloatField;
    QrVSPSPDstSrcMovID: TLargeintField;
    QrVSPSPDstSrcNivel1: TLargeintField;
    QrVSPSPDstSrcNivel2: TLargeintField;
    QrVSPSPDstSrcGGX: TLargeintField;
    QrVSPSPDstSdoVrtPeca: TFloatField;
    QrVSPSPDstSdoVrtPeso: TFloatField;
    QrVSPSPDstSdoVrtArM2: TFloatField;
    QrVSPSPDstObserv: TWideStringField;
    QrVSPSPDstSerieFch: TLargeintField;
    QrVSPSPDstFicha: TLargeintField;
    QrVSPSPDstMisturou: TLargeintField;
    QrVSPSPDstFornecMO: TLargeintField;
    QrVSPSPDstCustoMOKg: TFloatField;
    QrVSPSPDstCustoMOM2: TFloatField;
    QrVSPSPDstCustoMOTot: TFloatField;
    QrVSPSPDstValorMP: TFloatField;
    QrVSPSPDstDstMovID: TLargeintField;
    QrVSPSPDstDstNivel1: TLargeintField;
    QrVSPSPDstDstNivel2: TLargeintField;
    QrVSPSPDstDstGGX: TLargeintField;
    QrVSPSPDstQtdGerPeca: TFloatField;
    QrVSPSPDstQtdGerPeso: TFloatField;
    QrVSPSPDstQtdGerArM2: TFloatField;
    QrVSPSPDstQtdGerArP2: TFloatField;
    QrVSPSPDstQtdAntPeca: TFloatField;
    QrVSPSPDstQtdAntPeso: TFloatField;
    QrVSPSPDstQtdAntArM2: TFloatField;
    QrVSPSPDstQtdAntArP2: TFloatField;
    QrVSPSPDstNotaMPAG: TFloatField;
    QrVSPSPDstNO_PALLET: TWideStringField;
    QrVSPSPDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPSPDstNO_TTW: TWideStringField;
    QrVSPSPDstID_TTW: TLargeintField;
    QrVSPSPDstNO_FORNECE: TWideStringField;
    QrVSPSPDstNO_SerieFch: TWideStringField;
    QrVSPSPDstReqMovEstq: TLargeintField;
    QrVSPSPDstMarca: TWideStringField;
    QrVSPSPDstPedItsFin: TLargeintField;
    QrVSPSPBxaCodigo: TLargeintField;
    QrVSPSPBxaControle: TLargeintField;
    QrVSPSPBxaMovimCod: TLargeintField;
    QrVSPSPBxaMovimNiv: TLargeintField;
    QrVSPSPBxaMovimTwn: TLargeintField;
    QrVSPSPBxaEmpresa: TLargeintField;
    QrVSPSPBxaTerceiro: TLargeintField;
    QrVSPSPBxaCliVenda: TLargeintField;
    QrVSPSPBxaMovimID: TLargeintField;
    QrVSPSPBxaDataHora: TDateTimeField;
    QrVSPSPBxaPallet: TLargeintField;
    QrVSPSPBxaGraGruX: TLargeintField;
    QrVSPSPBxaPecas: TFloatField;
    QrVSPSPBxaPesoKg: TFloatField;
    QrVSPSPBxaAreaM2: TFloatField;
    QrVSPSPBxaAreaP2: TFloatField;
    QrVSPSPBxaValorT: TFloatField;
    QrVSPSPBxaSrcMovID: TLargeintField;
    QrVSPSPBxaSrcNivel1: TLargeintField;
    QrVSPSPBxaSrcNivel2: TLargeintField;
    QrVSPSPBxaSrcGGX: TLargeintField;
    QrVSPSPBxaSdoVrtPeca: TFloatField;
    QrVSPSPBxaSdoVrtPeso: TFloatField;
    QrVSPSPBxaSdoVrtArM2: TFloatField;
    QrVSPSPBxaObserv: TWideStringField;
    QrVSPSPBxaSerieFch: TLargeintField;
    QrVSPSPBxaFicha: TLargeintField;
    QrVSPSPBxaMisturou: TLargeintField;
    QrVSPSPBxaFornecMO: TLargeintField;
    QrVSPSPBxaCustoMOKg: TFloatField;
    QrVSPSPBxaCustoMOM2: TFloatField;
    QrVSPSPBxaCustoMOTot: TFloatField;
    QrVSPSPBxaValorMP: TFloatField;
    QrVSPSPBxaDstMovID: TLargeintField;
    QrVSPSPBxaDstNivel1: TLargeintField;
    QrVSPSPBxaDstNivel2: TLargeintField;
    QrVSPSPBxaDstGGX: TLargeintField;
    QrVSPSPBxaQtdGerPeca: TFloatField;
    QrVSPSPBxaQtdGerPeso: TFloatField;
    QrVSPSPBxaQtdGerArM2: TFloatField;
    QrVSPSPBxaQtdGerArP2: TFloatField;
    QrVSPSPBxaQtdAntPeca: TFloatField;
    QrVSPSPBxaQtdAntPeso: TFloatField;
    QrVSPSPBxaQtdAntArM2: TFloatField;
    QrVSPSPBxaQtdAntArP2: TFloatField;
    QrVSPSPBxaNotaMPAG: TFloatField;
    QrVSPSPBxaNO_PALLET: TWideStringField;
    QrVSPSPBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPSPBxaNO_TTW: TWideStringField;
    QrVSPSPBxaID_TTW: TLargeintField;
    QrVSPSPBxaNO_FORNECE: TWideStringField;
    QrVSPSPBxaNO_SerieFch: TWideStringField;
    QrVSPSPBxaReqMovEstq: TLargeintField;
    QrVSPSPAtuCodigo: TLargeintField;
    QrVSPSPAtuControle: TLargeintField;
    QrVSPSPAtuMovimCod: TLargeintField;
    QrVSPSPAtuMovimNiv: TLargeintField;
    QrVSPSPAtuMovimTwn: TLargeintField;
    QrVSPSPAtuEmpresa: TLargeintField;
    QrVSPSPAtuTerceiro: TLargeintField;
    QrVSPSPAtuCliVenda: TLargeintField;
    QrVSPSPAtuMovimID: TLargeintField;
    QrVSPSPAtuDataHora: TDateTimeField;
    QrVSPSPAtuPallet: TLargeintField;
    QrVSPSPAtuGraGruX: TLargeintField;
    QrVSPSPAtuPecas: TFloatField;
    QrVSPSPAtuPesoKg: TFloatField;
    QrVSPSPAtuAreaM2: TFloatField;
    QrVSPSPAtuAreaP2: TFloatField;
    QrVSPSPAtuValorT: TFloatField;
    QrVSPSPAtuSrcMovID: TLargeintField;
    QrVSPSPAtuSrcNivel1: TLargeintField;
    QrVSPSPAtuSrcNivel2: TLargeintField;
    QrVSPSPAtuSrcGGX: TLargeintField;
    QrVSPSPAtuSdoVrtPeca: TFloatField;
    QrVSPSPAtuSdoVrtPeso: TFloatField;
    QrVSPSPAtuSdoVrtArM2: TFloatField;
    QrVSPSPAtuObserv: TWideStringField;
    QrVSPSPAtuSerieFch: TLargeintField;
    QrVSPSPAtuFicha: TLargeintField;
    QrVSPSPAtuMisturou: TLargeintField;
    QrVSPSPAtuFornecMO: TLargeintField;
    QrVSPSPAtuCustoMOKg: TFloatField;
    QrVSPSPAtuCustoMOM2: TFloatField;
    QrVSPSPAtuCustoMOTot: TFloatField;
    QrVSPSPAtuValorMP: TFloatField;
    QrVSPSPAtuDstMovID: TLargeintField;
    QrVSPSPAtuDstNivel1: TLargeintField;
    QrVSPSPAtuDstNivel2: TLargeintField;
    QrVSPSPAtuDstGGX: TLargeintField;
    QrVSPSPAtuQtdGerPeca: TFloatField;
    QrVSPSPAtuQtdGerPeso: TFloatField;
    QrVSPSPAtuQtdGerArM2: TFloatField;
    QrVSPSPAtuQtdGerArP2: TFloatField;
    QrVSPSPAtuQtdAntPeca: TFloatField;
    QrVSPSPAtuQtdAntPeso: TFloatField;
    QrVSPSPAtuQtdAntArM2: TFloatField;
    QrVSPSPAtuQtdAntArP2: TFloatField;
    QrVSPSPAtuNotaMPAG: TFloatField;
    QrVSPSPAtuNO_PALLET: TWideStringField;
    QrVSPSPAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSPSPAtuNO_TTW: TWideStringField;
    QrVSPSPAtuID_TTW: TLargeintField;
    QrVSPSPAtuNO_FORNECE: TWideStringField;
    QrVSPSPAtuReqMovEstq: TLargeintField;
    QrVSPSPAtuCUSTO_M2: TFloatField;
    QrVSPSPAtuCUSTO_P2: TFloatField;
    QrVSPSPAtuNO_LOC_CEN: TWideStringField;
    QrVSPSPAtuMarca: TWideStringField;
    QrVSPSPAtuPedItsLib: TLargeintField;
    QrVSPSPAtuStqCenLoc: TLargeintField;
    QrVSPSPAtuNO_FICHA: TWideStringField;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    N5: TMenuItem;
    QrVSPSPOriIMEIVSMulFrnCab: TLargeintField;
    AlteraLocaldoestoque1: TMenuItem;
    QrVSPSPDstStqCenLoc: TLargeintField;
    AlteraLocaldoestoque2: TMenuItem;
    QrVSPSPDstNO_LOC_CEN: TWideStringField;
    Alteraartigodedestino1: TMenuItem;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSPSPAtuClientMO: TLargeintField;
    Somenteinfo1: TMenuItem;
    Apreencher1: TMenuItem;
    EdGGXDst: TdmkEditCB;
    CBGGXDst: TdmkDBLookupComboBox;
    Label54: TLabel;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    QrVSPSPCabGGXDst: TIntegerField;
    N7: TMenuItem;
    Corrigeartigodedestino1: TMenuItem;
    QrOperacoes: TmySQLQuery;
    DsOperacoes: TDataSource;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    EdOperacoes: TdmkEditCB;
    Label55: TLabel;
    CBOperacoes: TdmkDBLookupComboBox;
    QrVSPSPCabOperacoes: TIntegerField;
    QrVSPSPCabNO_Operacoes: TWideStringField;
    Label60: TLabel;
    DBEdit42: TDBEdit;
    SpeedButton5: TSpeedButton;
    Oitemselecionado1: TMenuItem;
    odosapartirdoselecionado1: TMenuItem;
    EdSerieRem: TdmkEdit;
    QrVSPSPCabSerieRem: TSmallintField;
    dmkDBEdit3: TdmkDBEdit;
    GBConfig: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdVSCOPCab: TdmkEditCB;
    Label61: TLabel;
    CBVSCOPCab: TdmkDBLookupComboBox;
    SbVSCOPCabCad: TSpeedButton;
    QrVSPSPCabVSCOPCab: TIntegerField;
    QrVSPSPCabNO_VSCOPCab: TWideStringField;
    SbVSCOPCabCpy: TSpeedButton;
    QrVSCOPCab: TmySQLQuery;
    DsVSCOPCab: TDataSource;
    QrVSCOPCabCodigo: TIntegerField;
    QrVSCOPCabNome: TWideStringField;
    BtPesagem: TBitBtn;
    PMPesagem: TPopupMenu;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    MenuItem1: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    Recalculacusto1: TMenuItem;
    PCPSPOri: TPageControl;
    TabSheet1: TTabSheet;
    DGDadosOri: TDBGrid;
    TabSheet2: TTabSheet;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    QrVSPSPCabEmitGru: TIntegerField;
    QrVSPSPCabNO_EmitGru: TWideStringField;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    Label66: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    Label63: TLabel;
    DBEdit43: TDBEdit;
    DBEdit46: TDBEdit;
    Reabreitensteste1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPSPCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPSPCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPSPCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSPSPCabBeforeClose(DataSet: TDataSet);
    procedure CabLibera1Click(Sender: TObject);
    procedure QrVSPSPCabCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmOperao1Click(Sender: TObject);
    procedure QrVSPSPDstBeforeClose(DataSet: TDataSet);
    procedure QrVSPSPDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure porIMEI1Click(Sender: TObject);
    procedure InformaNmerodaRME11Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure Corrigirfornecedor1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure Ordensdeoperaoemaberto1Click(Sender: TObject);
    procedure porIMEIParcial1Click(Sender: TObject);
    procedure AlteraLocaldoestoque1Click(Sender: TObject);
    procedure AlteraLocaldoestoque2Click(Sender: TObject);
    procedure Alteraartigodedestino1Click(Sender: TObject);
    procedure Somenteinfo1Click(Sender: TObject);
    procedure Apreencher1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Oitemselecionado1Click(Sender: TObject);
    procedure odosapartirdoselecionado1Click(Sender: TObject);
    procedure SbVSCOPCabCpyClick(Sender: TObject);
    procedure SbVSCOPCabCadClick(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure PMPesagemPopup(Sender: TObject);
    procedure Reabreitensteste1Click(Sender: TObject);
  private
    FItsExcluiOriIMEI_Enabled,
    FItsIncluiOri_Enabled, FItsIncluiDst_Enabled, FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSPSPOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSPSPDst(SQLType: TSQLType; DesabilitaBaixa: Boolean);
    procedure InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSPSPOris(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ImprimeIMEI(Formato: TXXImpImeiKind);
    procedure CorrigeArtigos(Todos: Boolean);
    procedure RecalculaCusto();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
  end;

var
  FmVSPSPCab: TFmVSPSPCab;
const
  FFormatFloat = '00000';

implementation

uses
  Module, MyDBCheck, DmkDAC_PF, ModuleGeral, VSPSPDst, AppListas, UnGrade_PF,
  VSPSPOriIMEI, ModVS, UnPQ_PF, ModVS_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPSPCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPSPCab.MostraVSPSPDst(SQLType: TSQLType; DesabilitaBaixa: Boolean);
  function CalculoValorKg(): Double;
  begin
    if QrVSPSPAtuPesoKg.Value > 0 then
      Result := QrVSPSPAtuValorT.Value / QrVSPSPAtuPesoKg.Value
    else
      Result := 0;
  end;
begin
  if not FItsIncluiDst_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSPSPDst, FmVSPSPDst, afmoNegarComAviso) then
  begin
    FmVSPSPDst.ImgTipo.SQLType := SQLType;
    FmVSPSPDst.FQrCab := QrVSPSPCab;
    FmVSPSPDst.FDsCab := DsVSPSPCab;
    FmVSPSPDst.FQrIts := QrVSPSPDst;
    FmVSPSPDst.FEmpresa            := QrVSPSPCabEmpresa.Value;
    FmVSPSPDst.FClientMO           := QrVSPSPAtuClientMO.Value;
    FmVSPSPDst.FFornecMO           := QrVSPSPAtuFornecMO.Value;
    FmVSPSPDst.FValKg              := CalculoValorKg();
    if SQLType = stIns then
    begin
      //FmVSPSPDst.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSPSPDst.QrVSPedIts, QrVSPSPAtuControle.Value,
      QrVSPSPAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSPSPDst.EdPedItsFin.ValueVariant  := QrVSPSPAtuPedItsLib.Value;
      FmVSPSPDst.CBPedItsFin.KeyValue      := QrVSPSPAtuPedItsLib.Value;
      //
      FmVSPSPDst.EdGragruX.ValueVariant    := QrVSPSPCabGGXDst.Value;
      FmVSPSPDst.CBGragruX.KeyValue        := QrVSPSPCabGGXDst.Value;
      //
      FmVSPSPDst.TPData.Date               := QrVSPSPCabDtHrAberto.Value;
      FmVSPSPDst.EdHora.ValueVariant       := QrVSPSPCabDtHrAberto.Value;
    end else
    begin
      VS_PF.ReopenPedItsXXX(FmVSPSPDst.QrVSPedIts, QrVSPSPAtuControle.Value,
       QrVSPSPDstControle.Value);
      FmVSPSPDst.EdCtrl1.ValueVariant      := QrVSPSPDstControle.Value;
      //FmVSPSPDst.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSPSPDstMovimCod.Value, QrVSPSPDstMovimTwn.Value, emidEmProcSP,
        //eminEmPSPBxa);
      FmVSPSPDst.EdMovimTwn.ValueVariant   := QrVSPSPDstMovimTwn.Value;
      FmVSPSPDst.EdGragruX.ValueVariant    := QrVSPSPDstGraGruX.Value;
      FmVSPSPDst.CBGragruX.KeyValue        := QrVSPSPDstGraGruX.Value;
      FmVSPSPDst.EdPecas.ValueVariant      := QrVSPSPDstPecas.Value;
      FmVSPSPDst.EdPesoKg.ValueVariant     := QrVSPSPDstPesoKg.Value;
      FmVSPSPDst.EdAreaM2.ValueVariant     := QrVSPSPDstAreaM2.Value;
      FmVSPSPDst.EdAreaP2.ValueVariant     := QrVSPSPDstAreaP2.Value;
      FmVSPSPDst.EdValorT.ValueVariant     := QrVSPSPDstValorT.Value;
      FmVSPSPDst.EdObserv.ValueVariant     := QrVSPSPDstObserv.Value;
      FmVSPSPDst.EdSerieFch.ValueVariant   := QrVSPSPDstSerieFch.Value;
      FmVSPSPDst.CBSerieFch.KeyValue       := QrVSPSPDstSerieFch.Value;
      FmVSPSPDst.EdFicha.ValueVariant      := QrVSPSPDstFicha.Value;
      FmVSPSPDst.EdMarca.ValueVariant      := QrVSPSPDstMarca.Value;
      //FmVSPSPDst.RGMisturou.ItemIndex    := QrVSPSPDstMisturou.Value;
      FmVSPSPDst.EdPedItsFin.ValueVariant  := QrVSPSPDstPedItsFin.Value;
      FmVSPSPDst.CBPedItsFin.KeyValue      := QrVSPSPDstPedItsFin.Value;
      FmVSPSPDst.EdPallet.ValueVariant     := QrVSPSPDstPallet.Value;
      FmVSPSPDst.CBPallet.KeyValue         := QrVSPSPDstPallet.Value;
      FmVSPSPDst.EdStqCenLoc.ValueVariant  := QrVSPSPDstStqCenLoc.Value;
      FmVSPSPDst.CBStqCenLoc.KeyValue      := QrVSPSPDstStqCenLoc.Value;
      FmVSPSPDst.EdReqMovEstq.ValueVariant := QrVSPSPDstReqMovEstq.Value;
      //
      FmVSPSPDst.TPData.Date               := QrVSPSPDstDataHora.Value;
      FmVSPSPDst.EdHora.ValueVariant       := QrVSPSPDstDataHora.Value;
      //
      if QrVSPSPDstSdoVrtPeca.Value < QrVSPSPDstPecas.Value then
      begin
        FmVSPSPDst.EdGraGruX.Enabled        := False;
        FmVSPSPDst.CBGraGruX.Enabled        := False;
      end;
      if QrVSPSPBxa.RecordCount > 0 then
      begin
        FmVSPSPDst.CkBaixa.Checked          := True;
        FmVSPSPDst.EdBxaPecas.ValueVariant  := -QrVSPSPBxaPecas.Value;
        FmVSPSPDst.EdBxaPesoKg.ValueVariant := -QrVSPSPBxaPesoKg.Value;
        FmVSPSPDst.EdBxaAreaM2.ValueVariant := -QrVSPSPBxaAreaM2.Value;
        FmVSPSPDst.EdBxaAreaP2.ValueVariant := -QrVSPSPBxaAreaP2.Value;
        FmVSPSPDst.EdBxaValorT.ValueVariant := -QrVSPSPBxaValorT.Value;
        FmVSPSPDst.EdCtrl2.ValueVariant     := QrVSPSPBxaControle.Value;
        FmVSPSPDst.EdBxaObserv.ValueVariant := QrVSPSPBxaObserv.Value;
      end;
    end;
    //
    FmVSPSPDst.LaBxaPesoKg.Enabled  := QrVSPSPCabPesoKgINI.Value <> 0;
    FmVSPSPDst.EdBxaPesoKg.Enabled  := QrVSPSPCabPesoKgINI.Value <> 0;
    FmVSPSPDst.LaBxaAreaM2.Enabled  := QrVSPSPCabAreaINIM2.Value <> 0;
    FmVSPSPDst.EdBxaAreaM2.Enabled  := QrVSPSPCabAreaINIM2.Value <> 0;
    FmVSPSPDst.LaBxaAreaP2.Enabled  := QrVSPSPCabAreaINIM2.Value <> 0;
    FmVSPSPDst.EdBxaAreaP2.Enabled  := QrVSPSPCabAreaINIM2.Value <> 0;
    //
    if DesabilitaBaixa then
      FmVSPSPDst.CkBaixa.Enabled := False;

    FmVSPSPDst.ShowModal;
    FmVSPSPDst.Destroy;
    VS_PF.AtualizaFornecedorOpe(QrVSPSPCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSPSPCab.odosapartirdoselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(True);
end;

procedure TFmVSPSPCab.MostraVSPSPOriIMEI(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSPSPOriIMEI, FmVSPSPOriIMEI, afmoNegarComAviso) then
  begin
    FmVSPSPOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSPSPOriIMEI.FQrCab          := QrVSPSPCab;
    FmVSPSPOriIMEI.FDsCab          := DsVSPSPCab;
    //FmVSPSPOriIMEI.FVSPSPOriIMEIIMEI   := QrVSPSPOriIMEIIMEI;
    //FmVSPSPOriIMEI.FVSPSPOriIMEIIMEIet := QrVSPSPOriIMEIIMEIet;
    FmVSPSPOriIMEI.FEmpresa        := QrVSPSPCabEmpresa.Value;
    FmVSPSPOriIMEI.FClientMO       := QrVSPSPAtuClientMO.Value;
    FmVSPSPOriIMEI.FFornecMO       := QrVSPSPAtuFornecMO.Value;
    FmVSPSPOriIMEI.FStqCenLoc      := QrVSPSPAtuStqCenLoc.Value;
    FmVSPSPOriIMEI.FTipoArea       := QrVSPSPCabTipoArea.Value;
    FmVSPSPOriIMEI.FOrigMovimNiv   := QrVSPSPAtuMovimNiv.Value;
    FmVSPSPOriIMEI.FOrigMovimCod   := QrVSPSPAtuMovimCod.Value;
    FmVSPSPOriIMEI.FOrigCodigo     := QrVSPSPAtuCodigo.Value;
    FmVSPSPOriIMEI.FNewGraGruX     := QrVSPSPAtuGraGruX.Value;
    //
    FmVSPSPOriIMEI.EdCodigo.ValueVariant    := QrVSPSPCabCodigo.Value;
    FmVSPSPOriIMEI.EdMovimCod.ValueVariant  := QrVSPSPCabMovimCod.Value;
    FmVSPSPOriIMEI.FDataHora                := QrVSPSPCabDtHrAberto.Value;
    //
    FmVSPSPOriIMEI.EdSrcMovID.ValueVariant  := QrVSPSPAtuMovimID.Value;
    FmVSPSPOriIMEI.EdSrcNivel1.ValueVariant := QrVSPSPAtuCodigo.Value;
    FmVSPSPOriIMEI.EdSrcNivel2.ValueVariant := QrVSPSPAtuControle.Value;
    FmVSPSPOriIMEI.EdSrcGGX.ValueVariant    := QrVSPSPAtuGraGruX.Value;
    //
    FmVSPSPOriIMEI.ReopenItensAptos();
    FmVSPSPOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSPSPOriIMEI.EdControle.ValueVariant := QrVSPSPCabOldControle.Value;
      //
      FmVSPSPOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSPSPCabOldCNPJ_CPF.Value);
      FmVSPSPOriIMEI.EdNomeEmiSac.Text := QrVSPSPCabOldNome.Value;
      FmVSPSPOriIMEI.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSPSPOriIMEI.FParcial := True;
        FmVSPSPOriIMEI.DBG04Estq.Options := FmVSPSPOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSPSPOriIMEI.FParcial := False;
        FmVSPSPOriIMEI.DBG04Estq.Options := FmVSPSPOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSPSPOriIMEI.FParcial := False;
        FmVSPSPOriIMEI.DBG04Estq.Options := FmVSPSPOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSPSPOriIMEI.ShowModal;
    FmVSPSPOriIMEI.Destroy;
    VS_PF.AtualizaFornecedorPSP(QrVSPSPCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSPSPCab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = True;
var
  Codigo: Integer;
begin
  PCPSPOri.ActivePageIndex := 1;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSPSPCabMovimCod.Value,
  QrVSPSPCabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSPSPCabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
end;

procedure TFmVSPSPCab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
  PCPSPOri.ActivePageIndex := 1;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
end;

procedure TFmVSPSPCab.Oitemselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(False);
end;

procedure TFmVSPSPCab.Ordensdeoperaoemaberto1Click(Sender: TObject);
begin
  VS_PF.ImprimeOXsAbertas(1, 2, 0, 1, [TEstqMovimID.emidEmProcSP], True, False, '', 0);
end;

procedure TFmVSPSPCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSPSPCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPSPCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPSPCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEC(emidEmProcSP);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPSPCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEI(emidEmProcSP);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPSPCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPSPCab);
  MyObjects.HabilitaMenuItemCabDelC1I3(CabExclui1, QrVSPSPCab,
  QrVSPSPOriIMEI, QrVSPSPOriPallet, QrVSPSPDst);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrVSPSPCab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrVSPSPCab);
  if QrVSPSPCabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrVSPSPCabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
end;

procedure TFmVSPSPCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSPSPCab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  //MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSPSPDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSPSPDst);
  if (QrVSPSPCabDtHrLibOpe.Value > 2) or (QrVSPSPCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    //ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSPSPDst);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSPSPDstID_TTW.Value, [ItsExcluiDst]);
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
end;

procedure TFmVSPSPCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSPSPCab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  //MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSPSPOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSPSPOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSPSPOriIMEI);
  if (QrVSPSPCabDtHrLibOpe.Value > 2) or (QrVSPSPCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    //ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
  end;
  //
  VS_PF.HabilitaComposVSAtivo(QrVSPSPOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_PF.HabilitaComposVSAtivo(QrVSPSPOriPalletID_TTW.Value, [ItsIncluiOri]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
end;

procedure TFmVSPSPCab.PMPesagemPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSPSPCab);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSPSPCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  //MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  //MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSPSPCab.porIMEI1Click(Sender: TObject);
begin
  MostraVSPSPOriIMEI(stIns, ptTotal);
end;

procedure TFmVSPSPCab.porIMEIParcial1Click(Sender: TObject);
begin
  MostraVSPSPOriIMEI(stIns, ptParcial);
end;

procedure TFmVSPSPCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPSPCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPSPCab.DefParams;
begin
  VAR_GOTOTABELA := 'vspspcab';
  VAR_GOTOMYSQLTABLE := QrVSPSPCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vcc.Nome NO_VSCOPCab, ');
  VAR_SQLx.Add('IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcP2) AreaINIP2,');
  VAR_SQLx.Add('IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('ope.Nome NO_Operacoes, ');
  VAR_SQLx.Add('egr.Nome NO_EmitGru, ');
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vspspcab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  VAR_SQLx.Add('LEFT JOIN operacoes ope ON ope.Codigo=voc.Operacoes');
  VAR_SQLx.Add('LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab');
  VAR_SQLx.Add('LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSPSPCab.Excluigrupo1Click(Sender: TObject);
begin
  PCPSPOri.ActivePageIndex := 1;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
end;

procedure TFmVSPSPCab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
  PCPSPOri.ActivePageIndex := 1;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
end;

procedure TFmVSPSPCab.CabLibera1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{
  Codigo := QrVSPSPCabCodigo.Value;
  //colocar metragem e encerrar!
  if DBCheck.CriaFm(TFmVSGerArtEnc, FmVSGerArtEnc, afmoNegarComAviso) then
  begin
    FmVSGerArtEnc.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtEnc.EdCodigo.ValueVariant := Codigo;
    FmVSGerArtEnc.EdMovimCod.ValueVariant := QrVSPSPAtuMovimCod.Value;
    FmVSGerArtEnc.EdControle.ValueVariant := QrVSPSPAtuControle.Value;
    //
    FmVSGerArtEnc.EdPecas.ValueVariant      := QrVSPSPAtuPecas.Value;
    FmVSGerArtEnc.EdAreaM2.ValueVariant     := QrVSPSPAtuAreaM2.Value;
    FmVSGerArtEnc.EdAreaP2.ValueVariant     := QrVSPSPAtuAreaP2.Value;
    FmVSGerArtEnc.EdCustoMOKg.ValueVariant  := QrVSPSPAtuCustoMOKg.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
    FmVSGerArtEnc.EdCustoMOTot.ValueVariant := QrVSPSPAtuCustoMOTot.Value;
    FmVSGerArtEnc.EdValorMP.ValueVariant    := QrVSPSPAtuValorMP.Value;
    FmVSGerArtEnc.EdValorT.ValueVariant     := QrVSPSPAtuValorT.Value;

    //
    FmVSGerArtEnc.EdPecasMan.ValueVariant      := QrVSPSPCabPecasMan.Value;
    FmVSGerArtEnc.EdAreaManM2.ValueVariant     := QrVSPSPCabAreaManM2.Value;
    FmVSGerArtEnc.EdAreaManP2.ValueVariant     := QrVSPSPCabAreaManP2.Value;
    //
    FmVSGerArtEnc.EdCustoManMOKg.ValueVariant  := QrVSPSPCabCustoManMOKg.Value;
    FmVSGerArtEnc.EdCustoManMOTot.ValueVariant := QrVSPSPCabCustoManMOTot.Value;
    FmVSGerArtEnc.EdValorManMP.ValueVariant    := QrVSPSPCabValorManMP.Value;
    FmVSGerArtEnc.EdValorManT.ValueVariant     := QrVSPSPCabValorManT.Value;
    //
    FmVSGerArtEnc.ShowModal;
    //
    LocCod(Codigo, Codigo);
    FmVSGerArtEnc.Destroy;
  end;
}
end;

procedure TFmVSPSPCab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSPSPCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspspcab', False, [
  'DtHrLibOpe'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPSPCab.CorrigeArtigos(Todos: Boolean);
begin
  VS_PF.CorrigeArtigoOpeEmDiante(QrVSPSPDstControle.Value,
    QrVSPSPDstGraGruX.Value);
  if Todos then
  begin
    QrVSPSPDst.Next;
    while not QrVSPSPDst.Eof do
    begin
      VS_PF.CorrigeArtigoOpeEmDiante(QrVSPSPDstControle.Value,
        QrVSPSPDstGraGruX.Value);
      //
      QrVSPSPDst.Next;
    end;
  end;
  //
  VS_PF.ReopenVSOpePrcDst(QrVSPSPDst, QrVSPSPCabMovimCod.Value, 0,
    QrVSPSPCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestPSP);
end;

procedure TFmVSPSPCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSPSPAtuControle.Value;
  MovimCod    := QrVSPSPCabMovimCod.Value;
  Codigo      := QrVSPSPCabCodigo.Value;
  MovimNivSrc := eminSorcPSP;
  MovimNivDst := eminDestPSP;
  VS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPSPCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSPSPCab.Corrigirfornecedor1Click(Sender: TObject);
begin
  VS_PF.AtualizaFornecedorOpe(QrVSPSPCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrVSPSPCabCodigo.Value, QrVSPSPCabCodigo.Value);
end;

procedure TFmVSPSPCab.ImprimeIMEI(Formato: TXXImpImeiKind);
var
  NFeRem: String;
begin
  if QrVSPSPCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSPSPCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSPSPAtuControle.Value], Formato, QrVSPSPCabLPFMO.Value,
    NFeRem, QrVSPSPCab);
end;

procedure TFmVSPSPCab.InformaNmerodaRME11Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSPSPOriIMEIControle.Value,
    QrVSPSPOriIMEIReqMovEstq.Value, QrVSPSPOriIMEI);
end;

procedure TFmVSPSPCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSPSPOriIMEIControle.Value,
    QrVSPSPOriIMEIReqMovEstq.Value, QrVSPSPOriIMEI);
end;

procedure TFmVSPSPCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSPSPDstControle.Value,
    QrVSPSPDstReqMovEstq.Value, QrVSPSPDst);
end;

procedure TFmVSPSPCab.InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO,
GraGruX: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmProcSP;
  MovimNiv       := eminEmPSPInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei081(*Gera��o de subproduto em processo em processamento de subproduto*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('vspspcab', MovimCod);
    VS_PF.AtualizaTotaisvspspcab(QrVSPSPCabMovimCod.Value);
  end;
end;

procedure TFmVSPSPCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSPSPCabCodigo.Value;
  ///Permitir excluir IMEC 337
  ///  Excluir tambem QrVSPSPAtu!!!
  ///  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  ///  Caption + sLineBreak + TMenuItem(Sender).Name);
  VS_PF.ExcluiCabEIMEI_OpeCab(QrVSPSPCabMovimCod.Value,
    QrVSPSPAtuControle.Value, emidEmProcSP, TEstqMotivDel.emtdWetCurti173);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSPSPCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSPSPCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPSPCab.Reabreitensteste1Click(Sender: TObject);
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSPSPAtu, QrVSPSPOriIMEI, QrVSPSPOriPallet,
  QrVSPSPDst, (*QrVSPSPInd*)nil, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  (*QrEmit*)nil, (*QrPQO*)nil, QrVSPSPCabCodigo.Value,
  QrVSPSPCabMovimCod.Value, QrVSPSPCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmProcSP, emidEmProcSP, emidEmProcSP,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmPSPInn, eminSorcPSP, eminDestPSP);
end;

procedure TFmVSPSPCab.RecalculaCusto;
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSPSPCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCurCab(QrVSPSPCabMovimCod.Value);
  LocCod(QrVSPSPCabCodigo.Value, QrVSPSPCabCodigo.Value);
end;

procedure TFmVSPSPCab.Recalculacusto1Click(Sender: TObject);
begin
  RecalculaCusto();
end;

procedure TFmVSPSPCab.ReopenVSPSPOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSPSPCabTemIMEIMrt.Value;
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVSPSPOriIMEI, QrVSPSPCabMovimCod.Value, Controle,
  TemIMEIMrt, eminSorcPSP, SQL_Limit);
  //
{
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPSPCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcPSP)),
  '']);
  SQL_Group :=   'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPSPOriPallet, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSPSPOriPallet.Locate('Pallet', Pallet, []);
}
end;

procedure TFmVSPSPCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod, VSPedIts: Integer;
begin
  if Geral.MB_Pergunta('Confirme a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSPSPCabCodigo.Value;
    CtrlDel  := QrVSPSPDstControle.Value;
    CtrlDst  := QrVSPSPDstSrcNivel2.Value;
    MovimNiv := QrVSPSPAtuMovimNiv.Value;
    MovimCod := QrVSPSPAtuMovimCod.Value;
    VSPedIts := QrVSPSPDstPedItsFin.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti173), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    //CO_���+TAB_VMI, 'Controle', CtrlDel, Dmod.MyDB) = ID_YES then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSPSPDstMovimCod.Value, QrVSPSPDstMovimTwn.Value, emidEmProcSP,
        eminEmPSPBxa);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti173), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      VS_PF.AtualizaVSPedIts_Fin(VSPedIts);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPSPDst,
        TIntegerField(QrVSPSPDstControle), QrVSPSPDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vspspcab', MovimCod);
      VS_PF.AtualizaTotaisvspspcab(QrVSPSPCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorOpe(QrVSPSPCabMovimCod.Value);
      VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcSP, MovimCod);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_PF.ReopenVSOpePrcDst(QrVSPSPDst, QrVSPSPCabMovimCod.Value, CtrlNxt,
        QrVSPSPCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestPSP);
    end;
  end;
end;

procedure TFmVSPSPCab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
(*
  if QrVSPSPAtuSdoVrtPeca.Value < QrVSPSPAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de origem cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Opera��o que foi gerado!');
    Exit;
  end else
*)
  begin
    Codigo    := QrVSPSPCabCodigo.Value;
    CtrlDel   := QrVSPSPOriIMEIControle.Value;
    CtrlOri   := QrVSPSPOriIMEISrcNivel2.Value;
    MovimNiv  := QrVSPSPAtuMovimNiv.Value;
    MovimCod  := QrVSPSPAtuMovimCod.Value;
    //
    if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
    begin
      if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti173), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      begin
        VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
        //
        // Nao se aplica. Calcula com function propria a seguir.
        //VS_PF.AtualizaTotaisVSXxxCab('vspspcab', MovimCod);
        VS_PF.AtualizaTotaisvspspcab(QrVSPSPCabMovimCod.Value);
        //
        CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPSPOriIMEI,
          TIntegerField(QrVSPSPOriIMEIControle), QrVSPSPOriIMEIControle.Value);
        //
        VS_PF.AtualizaFornecedorOpe(MovimCod);
        CorrigeFornecedorePalletsDestino(False);
        LocCod(Codigo, Codigo);
        ReopenVSPSPOris(CtrlNxt, 0);
      end;
    end;
  end;
end;

procedure TFmVSPSPCab.DefineONomeDoForm;
begin
end;

procedure TFmVSPSPCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPSPCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPSPCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPSPCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPSPCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPSPCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormOperacoes(EdOperacoes.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdOperacoes, CBOperacoes, QrOperacoes, VAR_CADASTRO);
end;

procedure TFmVSPSPCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPSPCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPSPCabCodigo.Value;
  Close;
end;

procedure TFmVSPSPCab.ItsIncluiDstClick(Sender: TObject);
const
  DesabilitaBaixa = False;
begin
  MostraVSPSPDst(stIns, DesabilitaBaixa);
end;

procedure TFmVSPSPCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPSPCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vspspcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPSPCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdSerieRem.ValueVariant   := QrVSPSPCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSPSPCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSPSPCabLPFMO.Value;
  //
  EdOperacoes.ValueVariant   := QrVSPSPCabOperacoes.Value;
  CBOperacoes.KeyValue       := QrVSPSPCabOperacoes.Value;
  //
  //
  EdControle.ValueVariant   := QrVSPSPAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSPSPAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSPSPAtuGraGruX.Value;
  EdCustoMOTot.ValueVariant := QrVSPSPAtuCustoMOTot.Value;
  EdPecas.ValueVariant      := QrVSPSPAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSPSPAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSPSPAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSPSPAtuObserv.Value;
  //
  VS_PF.ReopenPedItsXXX(QrVSPedIts, QrVSPSPAtuControle.Value, QrVSPSPAtuControle.Value);
  Habilita := QrVSPSPAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSPSPAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSPSPAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSPSPAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSPSPAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSPSPAtuReqMovEstq.Value;
  //
  EdClientMO.ValueVariant   := QrVSPSPAtuClientMO.Value;
  CBClientMO.KeyValue       := QrVSPSPAtuClientMO.Value;
  //
  EdFornecMO.ValueVariant   := QrVSPSPAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSPSPAtuFornecMO.Value;
  //
  EdVSCOPCab.ValueVariant   := QrVSPSPCabVSCOPCab.Value;
  CBVSCOPCab.KeyValue       := QrVSPSPCabVSCOPCab.Value;
  //
  EdEmitGru.ValueVariant    := QrVSPSPCabEmitGru.Value;
  CBEmitGru.KeyValue        := QrVSPSPCabEmitGru.Value;
  //
end;

procedure TFmVSPSPCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, MovimCod, TipoArea, FornecMO, Operacoes, Forma,
  Controle, PedItsLib, StqCenLoc, SerieRem, NFeRem, ClientMO, GGXDst,
  VSCOPCab, EmitGru: Integer;
  Nome, DtHrAberto, LPFMO: String;
  SQLType: TSQLType;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  Operacoes      := EdOperacoes.ValueVariant;
  VSCOPCab       := EdVSCOPCab.ValueVariant;
  EmitGru        := EdEmitGru.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdCLientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst, 'Informe o Artigo a ser produzido!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque!') then
    Exit;
  //if MyObjects.FIC(Operacoes = 0, EdOperacoes, 'Informe o c�digo de cadastro da opera��o!') then
    //Exit;
  //
  if not VS_PF.ValidaCampoNF(6, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vspspcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspspcab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'NFeRem', 'SerieRem', 'LPFMO',
  'GGXDst', 'Operacoes', 'VSCOPCab',
  'EmitGru'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  NFeRem, SerieRem, LPFMO,
  GGXDst, Operacoes, VSCOPCab,
  EmitGru], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidEmProcSP, Codigo);
    InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO, GraGruX, DtHrAberto);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSPSPOriIMEI.RecordCount = 0) then
    begin
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      'IME-I Total',  'IME-I Parcial'], -1);
      case Forma of
        0: MostraVSPSPOriIMEI(stIns, ptTotal);
        1: MostraVSPSPOriIMEI(stIns, ptParcial);
      end;
    end else begin
      VS_PF.AtualizaFornecedorOpe(MovimCod);
      //
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcSP, QrVSPSPCabMovimCod.Value);
    end;
  end;
end;

procedure TFmVSPSPCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vspspcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vspspcab', 'Codigo');
end;

procedure TFmVSPSPCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSPSPCab.BtOriClick(Sender: TObject);
begin
  PCPSPOri.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSPSPCab.BtPesagemClick(Sender: TObject);
begin
  PCPSPOri.ActivePageIndex := 1;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSPSPCab.Alteraartigodedestino1Click(Sender: TObject);
var
  DesabilitaBaixa: Boolean;
begin
  DesabilitaBaixa := QrVSPSPBxa.RecordCount = 0;
  MostraVSPSPDst(stUpd, DesabilitaBaixa);
end;

procedure TFmVSPSPCab.AlteraLocaldoestoque1Click(Sender: TObject);
begin
  VS_PF.InfoStqCenLoc(QrVSPSPDstControle.Value,
    QrVSPSPDstStqCenLoc.Value, QrVSPSPDst);
end;

procedure TFmVSPSPCab.AlteraLocaldoestoque2Click(Sender: TObject);
begin
  VS_PF.InfoStqCenLoc(QrVSPSPAtuControle.Value,
    QrVSPSPAtuStqCenLoc.Value, QrVSPSPAtu);
end;

procedure TFmVSPSPCab.Apreencher1Click(Sender: TObject);
begin
  ImprimeIMEI(viikOPaPreencher);
end;

procedure TFmVSPSPCab.AtualizaestoqueEmOperao1Click(Sender: TObject);
begin
  VS_PF.AtualizaTotaisvspspcab(QrVSPSPCabMovimCod.Value);
// Parei Aqui 2014-12-14
  LocCod(QrVSPSPCabCodigo.Value, QrVSPSPCabCodigo.Value);
end;

procedure TFmVSPSPCab.AtualizaFornecedoresDeDestino();
var
  Qry: TmySQLQuery;
  Codigo, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, MovimCod',
    'FROM vspspcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSPSPCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo   := Qry.FieldByName('Codigo').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando a OO ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_PF.AtualizaFornecedorOpe(QrVSPSPCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSPSPCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSPSPCabMovimCod.Value, TEstqMovimID.emidEmProcSP, [eminSorcPSP],
  [eminEmPSPInn, eminDestPSP, eminEmPSPBxa]);
end;

procedure TFmVSPSPCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSPSPCab, QrVSPSPCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, CabAltera1, Corrigirfornecedor1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPSPCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PCPSPOri.Align := alClient;
  PCPSPOri.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0683_VSPSPPro));
  VS_PF.AbreGraGruXY(QrGGXDst,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0853_VSPSPEnd));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
  VS_PF.ReopenVSCOPCab(QrVSCOPCab, emidEmProcSP);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
end;

procedure TFmVSPSPCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPSPCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSPSPCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSPSPCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSPSPCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSPSPCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPSPCab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSPSPCab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSPSPCab.QrVSPSPCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPSPCab.QrVSPSPCabAfterScroll(DataSet: TDataSet);
begin
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSPSPAtu, QrVSPSPCabMovimCod.Value, 0,
  QrVSPSPCabTemIMEIMrt.Value, eminEmPSPInn);
  ReopenVSPSPOris(0, 0);
  VS_PF.ReopenVSOpePrcDst(QrVSPSPDst, QrVSPSPCabMovimCod.Value, 0,
    QrVSPSPCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestPSP);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(QrForcados, 0, QrVSPSPCabCodigo.Value,
    QrVSPSPAtuControle.Value, QrVSPSPCabTemIMEIMrt.Value,  emidEmProcSP);
  // Insumos
  //ReopenEmit();
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSPSPCabMovimCod.Value, 0);
end;

procedure TFmVSPSPCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPSPCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPSPCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSPSPCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vspspcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSPSPCab.SbVSCOPCabCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSCOPCab(0);
  UMyMod.SetaCodigoPesquisado(EdVSCOPCab, CBVSCOPCab, QrVSCOPCab, VAR_CADASTRO);
end;

procedure TFmVSPSPCab.SbVSCOPCabCpyClick(Sender: TObject);
const
  EdCliente = nil;
  CBCliente = nil;
  EdCustoMOKg = nil;
begin
  VS_PF.PreencheDadosDeVSCOPCab(EdVSCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoArea,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOKg,
  nil, nil, nil, nil, nil, nil, nil, nil, nil, nil);
end;

procedure TFmVSPSPCab.Somenteinfo1Click(Sender: TObject);
begin
  ImprimeIMEI(viikArtigoGerado);
end;

procedure TFmVSPSPCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPSPCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSPSPCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPSPCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspspcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  LaPedItsLib.Enabled       := True;
  EdPedItsLib.Enabled       := True;
  CBPedItsLib.Enabled       := True;
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSPSPCab.QrVSPSPCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPSPAtu.Close;
  QrVSPSPOriIMEI.Close;
  QrVSPSPOriPallet.Close;
  QrVSPSPDst.Close;
  QrForcados.Close;
end;

procedure TFmVSPSPCab.QrVSPSPCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPSPCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSPSPCab.QrVSPSPCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPSPCabTipoArea.Value of
    0: QrVSPSPCabNO_TIPO.Value := 'm�';
    1: QrVSPSPCabNO_TIPO.Value := 'ft�';
    else QrVSPSPCabNO_TIPO.Value := '???';
  end;
  QrVSPSPCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSPSPCabDtHrLibOpe.Value, 106, True);
  QrVSPSPCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSPSPCabDtHrFimOpe.Value, 106, True);
  QrVSPSPCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSPSPCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSPSPCab.QrVSPSPDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSPSPCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPSPCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmPSPBxa)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSPSPDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPSPBxa, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  //QrVSPSPBxa.Locate('Controle', Controle, []);
  //
end;

procedure TFmVSPSPCab.QrVSPSPDstBeforeClose(DataSet: TDataSet);
begin
  QrVSPSPBxa.Close;
end;

end.

