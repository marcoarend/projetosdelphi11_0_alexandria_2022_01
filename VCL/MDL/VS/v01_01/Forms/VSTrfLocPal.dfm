object FmVSTrfLocPal: TFmVSTrfLocPal
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-133 :: Transfer'#234'ncia de Local de Estoque VS - por Pall' +
    'et'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 603
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS - por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 603
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS - por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 603
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS - por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 65
        Width = 1008
        Height = 402
        Align = alClient
        Caption = 'Filtors: '
        TabOrder = 0
        object DBG04Estq: TdmkDBGridZTO
          Left = 2
          Top = 97
          Width = 1004
          Height = 215
          Align = alClient
          DataSource = DsEstqR4
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NFeSer'
              Title.Caption = 'S'#233'rie'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFeNum'
              Title.Caption = 'N'#176' NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LOC_CEN'
              Title.Caption = 'Local (Centro)'
              Width = 155
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Title.Caption = 'ID Pallet'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PALLET'
              Title.Caption = 'Pallet'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdGGX'
              Title.Caption = 'Ordem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtPeca'
              Title.Caption = 'Pe'#231'as'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtArM2'
              Title.Caption = #193'rea m'#178
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtPeso'
              Title.Caption = 'Peso kg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MediaM2'
              Title.Caption = 'm'#178' / pe'#231'a'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Nome do terceiro'
              Width = 220
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LaGraGruX: TLabel
            Left = 224
            Top = 0
            Width = 66
            Height = 13
            Caption = 'Mat'#233'ria-prima:'
          end
          object LaFicha: TLabel
            Left = 152
            Top = 0
            Width = 56
            Height = 13
            Caption = 'Ficha RMP:'
            Enabled = False
          end
          object Label11: TLabel
            Left = 8
            Top = 0
            Width = 83
            Height = 13
            Caption = 'S'#233'rie Ficha RMP:'
            Enabled = False
          end
          object Label1: TLabel
            Left = 8
            Top = 39
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object Label38: TLabel
            Left = 428
            Top = 39
            Width = 78
            Height = 13
            Caption = 'Tipo de material:'
          end
          object Label39: TLabel
            Left = 696
            Top = 39
            Width = 82
            Height = 13
            Caption = 'Parte do material:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 224
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 280
            Top = 16
            Width = 589
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 4
            dmkEditCB = EdGraGruX
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdFicha: TdmkEdit
            Left = 152
            Top = 16
            Width = 68
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object BtReabre: TBitBtn
            Tag = 18
            Left = 880
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 5
            OnClick = BtReabreClick
          end
          object EdSerieFch: TdmkEditCB
            Left = 8
            Top = 16
            Width = 40
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SerieFch'
            UpdCampo = 'SerieFch'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSerieFch
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBSerieFch: TdmkDBLookupComboBox
            Left = 48
            Top = 16
            Width = 101
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsVSSerFch
            TabOrder = 1
            dmkEditCB = EdSerieFch
            QryCampo = 'SerieFch'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLocSrc: TdmkEditCB
            Left = 8
            Top = 55
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'StqCenLoc'
            UpdCampo = 'StqCenLoc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLocSrc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLocSrc: TdmkDBLookupComboBox
            Left = 64
            Top = 55
            Width = 361
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCad
            TabOrder = 7
            dmkEditCB = EdStqCenLocSrc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCouNiv2: TdmkDBLookupComboBox
            Left = 484
            Top = 55
            Width = 208
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCouNiv2
            TabOrder = 8
            dmkEditCB = EdCouNiv2
            QryName = 'QrGrGruXCou'
            QryCampo = 'CouNiv2'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCouNiv1: TdmkDBLookupComboBox
            Left = 752
            Top = 55
            Width = 208
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCouNiv1
            TabOrder = 9
            dmkEditCB = EdCouNiv1
            QryName = 'QrGrGruXCou'
            QryCampo = 'CouNiv1'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCouNiv2: TdmkEditCB
            Left = 428
            Top = 55
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrGrGruXCou'
            QryCampo = 'CouNiv2'
            UpdCampo = 'CouNiv2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCouNiv2
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCouNiv1: TdmkEditCB
            Left = 696
            Top = 55
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrGrGruXCou'
            QryCampo = 'CouNiv1'
            UpdCampo = 'CouNiv1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCouNiv1
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 312
          Width = 1004
          Height = 88
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Label49: TLabel
            Left = 88
            Top = 3
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object Label50: TLabel
            Left = 510
            Top = 3
            Width = 74
            Height = 13
            Caption = 'Req.Mov.Estq.:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label16: TLabel
            Left = 608
            Top = 3
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object LaItemNFe: TLabel
            Left = 4
            Top = 3
            Width = 46
            Height = 13
            Caption = 'Item NFe:'
          end
          object Label19: TLabel
            Left = 4
            Top = 44
            Width = 149
            Height = 13
            Caption = 'Material usado para emitir NF-e:'
            Enabled = False
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 88
            Top = 19
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'StqCenLoc'
            UpdCampo = 'StqCenLoc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 144
            Top = 19
            Width = 361
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_LOC_CEN'
            ListSource = DsStqCenLoc
            TabOrder = 2
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdReqMovEstq: TdmkEdit
            Left = 511
            Top = 19
            Width = 93
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdObserv: TdmkEdit
            Left = 608
            Top = 19
            Width = 389
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Observ'
            UpdCampo = 'Observ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdItemNFe: TdmkEdit
            Left = 4
            Top = 19
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ItemNFe'
            UpdCampo = 'ItemNFe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdGGXRcl: TdmkEditCB
            Left = 4
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GGXRcl'
            UpdCampo = 'GGXRcl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGGXRcl
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGGXRcl: TdmkDBLookupComboBox
            Left = 60
            Top = 60
            Width = 457
            Height = 21
            Enabled = False
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGGXRcl
            TabOrder = 6
            dmkEditCB = EdGGXRcl
            QryCampo = 'GGXRcl'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 65
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 1008
          Height = 65
          Align = alClient
          Caption = ' Dados do cabe'#231'alho:'
          Enabled = False
          TabOrder = 0
          object Label5: TLabel
            Left = 12
            Top = 20
            Width = 53
            Height = 13
            Caption = 'ID entrada:'
            FocusControl = DBEdCodigo
          end
          object Label2: TLabel
            Left = 96
            Top = 20
            Width = 55
            Height = 13
            Caption = 'ID estoque:'
            FocusControl = DBEdMovimCod
          end
          object Label3: TLabel
            Left = 180
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            FocusControl = DBEdEmpresa
          end
          object Label7: TLabel
            Left = 228
            Top = 20
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
            FocusControl = DBEdDtEntrada
          end
          object Label8: TLabel
            Left = 344
            Top = 20
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdCliVenda
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 12
            Top = 36
            Width = 80
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            UpdCampo = 'Codigo'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdMovimCod: TdmkDBEdit
            Left = 96
            Top = 36
            Width = 80
            Height = 21
            TabStop = False
            DataField = 'MovimCod'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            UpdCampo = 'Codigo'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdEmpresa: TdmkDBEdit
            Left = 180
            Top = 36
            Width = 45
            Height = 21
            TabStop = False
            DataField = 'Empresa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            UpdCampo = 'Codigo'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdDtEntrada: TdmkDBEdit
            Left = 228
            Top = 36
            Width = 112
            Height = 21
            TabStop = False
            DataField = 'DtVenda'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            UpdCampo = 'Codigo'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdCliVenda: TdmkDBEdit
            Left = 344
            Top = 36
            Width = 61
            Height = 21
            TabStop = False
            DataField = 'Cliente'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 4
            UpdCampo = 'Codigo'
            UpdType = utYes
            Alignment = taRightJustify
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaTerceiro: TLabel
        Left = 304
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdTerceiro: TdmkEditCB
        Left = 304
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 360
        Top = 20
        Width = 69
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 2
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object QrEstqR4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 432
    Top = 384
    object QrEstqR4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR4Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrEstqR4PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR4NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR4Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR4Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR4ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR4SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR4SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEstqR4SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrEstqR4MediaM2: TFloatField
      FieldName = 'MediaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrEstqR4NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrEstqR4NO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 100
    end
  end
  object DsEstqR4: TDataSource
    DataSet = QrEstqR4
    Left = 432
    Top = 428
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.Nome NO_Pallet, wmi.*  '
      'FROM vsmovits wmi '
      'LEFT JOIN vspallet pal ON pal.Codigo=wmi.Pallet '
      'WHERE wmi.Empresa=-11 '
      'AND wmi.GraGruX=3328 '
      'AND ( '
      '  wmi.MovimID=1 '
      '  OR  '
      '  wmi.SrcMovID<>0 '
      ') '
      'AND wmi.SdoVrtPeca > 0 '
      'ORDER BY DataHora, Pallet ')
    Left = 484
    Top = 284
    object QrVSMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TIntegerField
      FieldName = 'Misturou'
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 288
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 336
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 288
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 188
    Top = 336
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 260
    Top = 288
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 260
    Top = 336
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 336
    Top = 288
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
    object QrStqCenLocEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 336
    Top = 336
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 36
    Top = 288
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 36
    Top = 340
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 416
    Top = 288
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 416
    Top = 336
  end
  object QrCouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 560
    Top = 284
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 560
    Top = 332
  end
  object QrCouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 636
    Top = 284
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 636
    Top = 332
  end
end
