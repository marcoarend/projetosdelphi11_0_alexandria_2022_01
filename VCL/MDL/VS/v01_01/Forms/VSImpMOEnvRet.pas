unit VSImpMOEnvRet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc, mySQLDbTables, frxClass, frxDBSet, BluedermConsts, dmkEditCB,
  dmkDBLookupComboBox, UnDmkProcFunc, dmkDBGridZTO, dmkEditDateTimePicker,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSImpMOEnvRet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetVSVMI_SerNF: TIntegerField;
    QrVSMOEnvRetVSVMI_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetVSVMI_Controle: TIntegerField;
    QrVSMOEnvRetVSVMI_Codigo: TIntegerField;
    QrVSMOEnvRetVSVMI_MovimID: TIntegerField;
    QrVSMOEnvRetVSVMI_MovimNiv: TIntegerField;
    QrVSMOEnvRetVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvRetVSVMI_Empresa: TIntegerField;
    QrVSMOEnvRetLk: TIntegerField;
    QrVSMOEnvRetDataCad: TDateField;
    QrVSMOEnvRetDataAlt: TDateField;
    QrVSMOEnvRetUserCad: TIntegerField;
    QrVSMOEnvRetUserAlt: TIntegerField;
    QrVSMOEnvRetAlterWeb: TSmallintField;
    QrVSMOEnvRetAtivo: TSmallintField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_EstqMovimID: TWideStringField;
    QrVSMovItsNO_DstMovID: TWideStringField;
    QrVSMovItsNO_SrcMovID: TWideStringField;
    QrVSMovItsNO_LOC_CEN: TWideStringField;
    QrVSMovItsGraGru1: TLargeintField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TLargeintField;
    QrVSMovItsPedItsFin: TLargeintField;
    QrVSMovItsPedItsVda: TLargeintField;
    QrVSMovItsStqCenLoc: TLargeintField;
    QrVSMovItsItemNFe: TLargeintField;
    QrVSMovItsMarca_1: TWideStringField;
    QrVSMovItsLnkNivXtr1: TLargeintField;
    QrVSMovItsLnkNivXtr2: TLargeintField;
    QrVSMovItsNFeNum: TLargeintField;
    QrVSPWEBxa: TmySQLQuery;
    QrVSPWEBxaCodigo: TLargeintField;
    QrVSPWEBxaControle: TLargeintField;
    QrVSPWEBxaMovimCod: TLargeintField;
    QrVSPWEBxaMovimNiv: TLargeintField;
    QrVSPWEBxaMovimTwn: TLargeintField;
    QrVSPWEBxaEmpresa: TLargeintField;
    QrVSPWEBxaTerceiro: TLargeintField;
    QrVSPWEBxaCliVenda: TLargeintField;
    QrVSPWEBxaMovimID: TLargeintField;
    QrVSPWEBxaDataHora: TDateTimeField;
    QrVSPWEBxaPallet: TLargeintField;
    QrVSPWEBxaGraGruX: TLargeintField;
    QrVSPWEBxaPecas: TFloatField;
    QrVSPWEBxaPesoKg: TFloatField;
    QrVSPWEBxaAreaM2: TFloatField;
    QrVSPWEBxaAreaP2: TFloatField;
    QrVSPWEBxaValorT: TFloatField;
    QrVSPWEBxaSrcMovID: TLargeintField;
    QrVSPWEBxaSrcNivel1: TLargeintField;
    QrVSPWEBxaSrcNivel2: TLargeintField;
    QrVSPWEBxaSrcGGX: TLargeintField;
    QrVSPWEBxaSdoVrtPeca: TFloatField;
    QrVSPWEBxaSdoVrtPeso: TFloatField;
    QrVSPWEBxaSdoVrtArM2: TFloatField;
    QrVSPWEBxaObserv: TWideStringField;
    QrVSPWEBxaSerieFch: TLargeintField;
    QrVSPWEBxaFicha: TLargeintField;
    QrVSPWEBxaMisturou: TLargeintField;
    QrVSPWEBxaFornecMO: TLargeintField;
    QrVSPWEBxaCustoMOKg: TFloatField;
    QrVSPWEBxaCustoMOM2: TFloatField;
    QrVSPWEBxaCustoMOTot: TFloatField;
    QrVSPWEBxaValorMP: TFloatField;
    QrVSPWEBxaDstMovID: TLargeintField;
    QrVSPWEBxaDstNivel1: TLargeintField;
    QrVSPWEBxaDstNivel2: TLargeintField;
    QrVSPWEBxaDstGGX: TLargeintField;
    QrVSPWEBxaQtdGerPeca: TFloatField;
    QrVSPWEBxaQtdGerPeso: TFloatField;
    QrVSPWEBxaQtdGerArM2: TFloatField;
    QrVSPWEBxaQtdGerArP2: TFloatField;
    QrVSPWEBxaQtdAntPeca: TFloatField;
    QrVSPWEBxaQtdAntPeso: TFloatField;
    QrVSPWEBxaQtdAntArM2: TFloatField;
    QrVSPWEBxaQtdAntArP2: TFloatField;
    QrVSPWEBxaNotaMPAG: TFloatField;
    QrVSPWEBxaNO_PALLET: TWideStringField;
    QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEBxaNO_TTW: TWideStringField;
    QrVSPWEBxaID_TTW: TLargeintField;
    QrVSPWEBxaNO_FORNECE: TWideStringField;
    QrVSPWEBxaNO_SerieFch: TWideStringField;
    QrVSPWEBxaReqMovEstq: TLargeintField;
    QrVSMO: TmySQLQuery;
    QrVSMOCodigo: TIntegerField;
    QrVSMONFCMO_FatID: TIntegerField;
    QrVSMONFCMO_FatNum: TIntegerField;
    QrVSMONFCMO_Empresa: TIntegerField;
    QrVSMONFCMO_nItem: TIntegerField;
    QrVSMONFCMO_SerNF: TIntegerField;
    QrVSMONFCMO_nNF: TIntegerField;
    QrVSMONFCMO_Pecas: TFloatField;
    QrVSMONFCMO_PesoKg: TFloatField;
    QrVSMONFCMO_AreaM2: TFloatField;
    QrVSMONFCMO_AreaP2: TFloatField;
    QrVSMONFCMO_CusMOM2: TFloatField;
    QrVSMONFCMO_CusMOKG: TFloatField;
    QrVSMONFCMO_ValorT: TFloatField;
    QrVSMONFRMP_FatID: TIntegerField;
    QrVSMONFRMP_FatNum: TIntegerField;
    QrVSMONFRMP_Empresa: TIntegerField;
    QrVSMONFRMP_nItem: TIntegerField;
    QrVSMONFRMP_SerNF: TIntegerField;
    QrVSMONFRMP_nNF: TIntegerField;
    QrVSMONFRMP_Pecas: TFloatField;
    QrVSMONFRMP_PesoKg: TFloatField;
    QrVSMONFRMP_AreaM2: TFloatField;
    QrVSMONFRMP_AreaP2: TFloatField;
    QrVSMONFRMP_ValorT: TFloatField;
    QrVSMOVSVMI_Controle: TIntegerField;
    QrVSMOVSVMI_Codigo: TIntegerField;
    QrVSMOVSVMI_MovimID: TIntegerField;
    QrVSMOVSVMI_MovimNiv: TIntegerField;
    QrVSMOVSVMI_MovimCod: TIntegerField;
    QrVSMOVSVMI_Empresa: TIntegerField;
    QrVSMOVSVMI_SerNF: TIntegerField;
    QrVSMOVSVMI_nNF: TIntegerField;
    QrVSMOVSVMI_Pecas: TFloatField;
    QrVSMOVSVMI_PesoKg: TFloatField;
    QrVSMOVSVMI_AreaM2: TFloatField;
    QrVSMOVSVMI_AreaM2WB: TFloatField;
    QrVSMOVSVMI_PercRendim: TFloatField;
    QrVSMOGraGru1: TIntegerField;
    QrVSMONO_PRD_TAM_COR: TWideStringField;
    QrVSMOPallet: TIntegerField;
    QrVSMODataHora: TDateTimeField;
    QrVSMOAtivo: TSmallintField;
    frxWET_CURTI_168_01: TfrxReport;
    frxDsVSMO: TfrxDBDataset;
    QrVSMOVSVMI_PesoKgWB: TFloatField;
    QrVSMOGrandeza: TSmallintField;
    Panel6: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrVSMOVSVMI_M2Virtual: TFloatField;
    QrSaldo: TmySQLQuery;
    QrSaldoPecasSdo: TFloatField;
    QrSaldoAreaSdoM2: TFloatField;
    QrSaldoPesoKgSdo: TFloatField;
    RGRelatorio: TRadioGroup;
    QrVSMOSerieNF_VMI: TWideStringField;
    QrVSMOSerieNF_CMO: TWideStringField;
    QrVSMOSerieNF_RMP: TWideStringField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrOrfaos: TmySQLQuery;
    DsOrfaos: TDataSource;
    RGNFSorc: TRadioGroup;
    Panel55: TPanel;
    GroupBox23: TGroupBox;
    TP24DataIni: TdmkEditDateTimePicker;
    Ck24DataIni: TCheckBox;
    Ck24DataFim: TCheckBox;
    TP24DataFim: TdmkEditDateTimePicker;
    GroupBox24: TGroupBox;
    Panel57: TPanel;
    Label57: TLabel;
    Label58: TLabel;
    Ed24NFeIni: TdmkEdit;
    Ed24NFeFim: TdmkEdit;
    Ck24Serie: TCheckBox;
    Ed24Serie: TdmkEdit;
    SpeedButton1: TSpeedButton;
    QrVSMovItsNFeSer: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_168_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FVSMO: String;
    //
    //procedure InsereRegistroPesquisa();
    procedure PesquisaIMEIsOrfaos();
    procedure ImprimeRelatorioNF();
  public
    { Public declarations }
  end;

  var
  FmVSImpMOEnvRet: TFmVSImpMOEnvRet;

implementation

uses Module, UnMyObjects, DmkDAC_PF, CreateVS, ModuleGeral, UnVS_PF,
  UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmVSImpMOEnvRet.BtOKClick(Sender: TObject);
begin
  if MyObjects.FIC(RGRelatorio.ItemIndex < 1,  RGRelatorio,
  'Selecione o relat�rio!') then Exit;
  if MyObjects.FIC(RGNFSorc.ItemIndex < 1,  RGNFSorc,
  'Selecione o "' + RGNFSorc.Caption + '"!') then Exit;
  //
  case RGRelatorio.ItemIndex of
    1,2: ImprimeRelatorioNF();
    3: PesquisaIMEIsOrfaos();
    else Geral.MB_Erro('Relat�rio n�o implementado!');
  end;
end;

procedure TFmVSImpMOEnvRet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpMOEnvRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpMOEnvRet.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  //
  Data := DMOdG.ObtemAgora();
  TP24DataIni.Date := Data - 90;
  TP24DataFim.Date := Data - 60;
end;

procedure TFmVSImpMOEnvRet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpMOEnvRet.frxWET_CURTI_168_01GetValue(const VarName: string;
  var Value: Variant);
var
  SQL_Serie: String;
  SerieRem: Integer;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt('', CBEmpresa.Text, EdEmpresa.ValueVariant)
  else
  if VarName = 'VARF_FORNEC_MO' then
    Value := dmkPF.ParValueCodTxt('', CBFornecMO.Text, EdFornecMO.ValueVariant)
  else
  if VarName = 'VARF_SALDO_ABERTO_NF' then
  begin
    SQL_Serie := '';
    SerieRem := Ed24Serie.ValueVariant;
    //if SerieRem <> -1 then
    if Ck24Serie.Checked then
      SQL_Serie := 'AND SerieRem=' + Geral.FF0(SerieRem);
    //
    UnDmkDAC_PF.AbremySQLQuery0(QrSaldo, Dmod.MyDB, [
    'SELECT SUM(PecasSdo) PecasSdo,  ',
    'SUM(AreaSdoM2) AreaSdoM2, SUM(PesoKgSdo) PesoKgSdo  ',
    'FROM vspwecab  ',
    'WHERE PecasSdo<>0 ',
    'AND NFeRem=' + Geral.FF0(QrVSMOVSVMI_nNF.Value),
    SQL_Serie,
    '']);
    if QrSaldoPecasSdo.Value <> 0 then
    begin
      Value := ' (Saldo: ' + FloatToStr(QrSaldoPecasSdo.Value) + ' Pe�as ';
      if QrSaldoPesoKgSdo.Value <> 0 then
        Value := Value + FloatToStr(QrSaldoPesoKgSdo.Value) + ' kg) '
      else
        Value := Value + Geral.FFT(QrSaldoAreaSdoM2.Value, 2, siNegativo) + ' m�) ';
    end else
      Value := ' - Encerrado';
  end;
end;

procedure TFmVSImpMOEnvRet.ImprimeRelatorioNF();
  procedure AreaM2WB(var AreaM2WB, PesoKgWB, PercRendim, M2Virtual: Double;
    var Grandeza: Integer);
  const
    Controle = 0;
    SQL_Limit = '';
    TemIMEIMrt = 1;
  var
    Fator: Double;
  begin
    VS_PF.ReopenVSOpePrcBxa(QrVSPWEBxa, QrVSMovItsMovimCod.Value,
    QrVSMovItsMovimTwn.Value, Controle, TemIMEIMrt, eminEmWEndBxa, SQL_Limit);
    // Area WB
    AreaM2WB := -QrVSPWEBxaAreaM2.Value;
    PesoKgWB := -QrVSPWEBxaPesoKg.Value;
    // Rendimento FI sobre WB
    if AreaM2WB = 0 then
    begin
      Fator := PesoKgWB / CO_FatorKgWB_M2CRUST;
      if Fator = 0 then
        PercRendim := 0
      else
        PercRendim := (QrVSMovItsAreaM2.Value - Fator) / Fator * 100;
      Grandeza := Integer(gumPesoKG);
      M2Virtual := Fator;
    end else
    begin
      Grandeza := Integer(gumAreaM2);
      PercRendim := (QrVSMovItsAreaM2.Value - AreaM2WB) / AreaM2WB * 100;
      M2Virtual := AreaM2WB;
    end;
  end;
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
var
  SerieNF_VMI, SerieNF_CMO, SerieNF_RMP, NO_PRD_TAM_COR, DataHora: String;
  Codigo, NFCMO_FatID, NFCMO_FatNum, NFCMO_Empresa, NFCMO_nItem, NFCMO_SerNF,
  NFCMO_nNF, NFRMP_FatID, NFRMP_FatNum, NFRMP_Empresa, NFRMP_nItem, NFRMP_SerNF,
  NFRMP_nNF, VSVMI_Controle, VSVMI_Codigo, VSVMI_MovimID, VSVMI_MovimNiv,
  VSVMI_MovimCod, VSVMI_Empresa, VSVMI_SerNF, VSVMI_nNF, GraGru1,
  Pallet, Grandeza: Integer;
  NFCMO_Pecas, NFCMO_PesoKg, NFCMO_AreaM2, NFCMO_AreaP2, NFCMO_CusMOM2,
  NFCMO_CusMOKG, NFCMO_ValorT, NFRMP_Pecas, NFRMP_PesoKg, NFRMP_AreaM2,
  NFRMP_AreaP2, NFRMP_ValorT, VSVMI_Pecas, VSVMI_PesoKg, VSVMI_AreaM2,
  VSVMI_AreaM2WB, VSVMI_PesoKgWB, VSVMI_PercRendim, VSVMI_M2Virtual: Double;
  //
  procedure InsereRegistroPesquisa();
  begin
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FVSMO, False, [
    'Codigo', 'NFCMO_FatID', 'NFCMO_FatNum',
    'NFCMO_Empresa', 'NFCMO_nItem', 'NFCMO_SerNF',
    'NFCMO_nNF', 'NFCMO_Pecas', 'NFCMO_PesoKg',
    'NFCMO_AreaM2', 'NFCMO_AreaP2', 'NFCMO_CusMOM2',
    'NFCMO_CusMOKG', 'NFCMO_ValorT', 'NFRMP_FatID',
    'NFRMP_FatNum', 'NFRMP_Empresa', 'NFRMP_nItem',
    'NFRMP_SerNF', 'NFRMP_nNF', 'NFRMP_Pecas',
    'NFRMP_PesoKg', 'NFRMP_AreaM2', 'NFRMP_AreaP2',
    'NFRMP_ValorT', 'VSVMI_Controle', 'VSVMI_Codigo',
    'VSVMI_MovimID', 'VSVMI_MovimNiv', 'VSVMI_MovimCod',
    'VSVMI_Empresa', 'VSVMI_SerNF', 'VSVMI_nNF',
    'VSVMI_Pecas', 'VSVMI_PesoKg', 'VSVMI_AreaM2',
    'VSVMI_AreaM2WB', 'VSVMI_PesoKgWB', 'Grandeza',
    'VSVMI_PercRendim', 'VSVMI_M2Virtual', 'GraGru1',
    'NO_PRD_TAM_COR', 'Pallet', CO_DATA_HORA_GRL,
    'SerieNF_VMI', 'SerieNF_CMO', 'SerieNF_RMP'], [
    ], [
    Codigo, NFCMO_FatID, NFCMO_FatNum,
    NFCMO_Empresa, NFCMO_nItem, NFCMO_SerNF,
    NFCMO_nNF, NFCMO_Pecas, NFCMO_PesoKg,
    NFCMO_AreaM2, NFCMO_AreaP2, NFCMO_CusMOM2,
    NFCMO_CusMOKG, NFCMO_ValorT, NFRMP_FatID,
    NFRMP_FatNum, NFRMP_Empresa, NFRMP_nItem,
    NFRMP_SerNF, NFRMP_nNF, NFRMP_Pecas,
    NFRMP_PesoKg, NFRMP_AreaM2, NFRMP_AreaP2,
    NFRMP_ValorT, VSVMI_Controle, VSVMI_Codigo,
    VSVMI_MovimID, VSVMI_MovimNiv, VSVMI_MovimCod,
    VSVMI_Empresa, VSVMI_SerNF, VSVMI_nNF,
    VSVMI_Pecas, VSVMI_PesoKg, VSVMI_AreaM2,
    VSVMI_AreaM2WB, VSVMI_PesoKgWB, Grandeza,
    VSVMI_PercRendim, VSVMI_M2Virtual, GraGru1,
    NO_PRD_TAM_COR, Pallet, DataHora,
    SerieNF_VMI, SerieNF_CMO, SerieNF_RMP], [
    ], False) then
    begin
      //
    end;
  end;
  //
var
  SQL_Empresa, SQL_FornecMO, Ordem: String;
  Empresa, FornecMO: Integer;
  //
  Grupo1: TfrxGroupHeader;
  Futer1: TfrxGroupFooter;
  Me_GH1, Me_FT1, MeTitNF, MeNumNF: TfrxMemoView;
  SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora',
    TP24DataIni.Date, TP24DataFim.Date, Ck24DataIni.Checked, Ck24DataFim.Checked);
    //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registros de pesquisa!');
  FVSMO := UnCreateVS.RecriaTempTableNovo(ntrttVSMO, DModG.QrUpdPID1, False);
  //
  SQL_Empresa  := '';
  SQL_FornecMO := '';
  Empresa      := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
    Empresa := DModG.QrEmpresasCodigo.Value;
  FornecMO     := EdFornecMO.ValueVariant;
  if Empresa <> 0 then
    SQL_Empresa  := 'AND cab.Empresa=' + Geral.FF0(Empresa);
  if FornecMO <> 0 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'DROP TABLE IF EXISTS _MO_FORNECMO_; ',
    'CREATE TABLE _MO_FORNECMO_ ',
    'SELECT DISTINCT MovimCod  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
    'WHERE MovimID IN (11,19,26,27)  ',
    'AND FornecMO=' + Geral.FF0(FornecMO),
    '; ',
    '']);
    SQL_FornecMO := 'AND vmi.MovimCod IN (SELECT MovimCod FROM ' +
    VAR_MyPID_DB_NOME + '._MO_FORNECMO_)';
  end;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'CAST(vmi.Marca AS CHAR) Marca,',
  'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,',
  'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
  'CAST(vsp.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN vspwecab   cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN vsmoenvret mer ON mer.VSVMI_Controle=vmi.Controle ',
  '']);
  case RGNFSorc.ItemIndex of
    1:
    SQL_Wher := Geral.ATS([
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidFinished)),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)),
    'AND cab.NFeRem>=' + Geral.FF0(Ed24NFeIni.ValueVariant),
    Geral.ATS_IF(Ed24NFeFim.ValueVariant > Ed24NFeIni.ValueVariant,
      ['AND cab.NFeRem<=' + Geral.FF0(Ed24NFeFim.ValueVariant)]),
    SQL_Empresa,
    SQL_FornecMO,
    SQL_Periodo,
    '']);
    2:
    SQL_Wher := Geral.ATS([
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidFinished)),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)),
    Geral.ATS_IF(Ck24Serie.Checked,
      ['AND mer.NFCMO_SerNF=' + Geral.FF0(Ed24Serie.ValueVariant)]),
    'AND mer.NFCMO_nNF>=' + Geral.FF0(Ed24NFeIni.ValueVariant),
    Geral.ATS_IF(Ed24NFeFim.ValueVariant > Ed24NFeIni.ValueVariant, [
    'AND mer.NFCMO_nNF<=' + Geral.FF0(Ed24NFeFim.ValueVariant)]),
    SQL_Empresa,
    SQL_FornecMO,
    SQL_Periodo,
    '']);
  end;
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI('SELECT DISTINCT ', SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI('SELECT DISTINCT ', SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSMovIts);
  QrVSMovIts.First;
  while not QrVSMovIts.Eof do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvRet, Dmod.MyDB, [
    'SELECT cmo.*  ',
    'FROM vsmoenvret cmo ',
    'WHERE cmo.VSVMI_Controle=' + Geral.FF0(QrVSMovItsControle.Value),
    '']);
    Codigo         := 0;
    NFCMO_FatID    := 0;
    NFCMO_FatNum   := 0;
    NFCMO_Empresa  := 0;
    NFCMO_nItem    := 0;
    NFCMO_SerNF    := 0;
    NFCMO_nNF      := 0;
    NFCMO_Pecas    := 0;
    NFCMO_PesoKg   := 0;
    NFCMO_AreaM2   := 0;
    NFCMO_AreaP2   := 0;
    NFCMO_CusMOM2  := 0;
    NFCMO_CusMOKG  := 0;
    NFCMO_ValorT   := 0;
    NFRMP_FatID    := 0;
    NFRMP_FatNum   := 0;
    NFRMP_Empresa  := 0;
    NFRMP_nItem    := 0;
    NFRMP_SerNF    := 0;
    NFRMP_nNF      := 0;
    NFRMP_Pecas    := 0;
    NFRMP_PesoKg   := 0;
    NFRMP_AreaM2   := 0;
    NFRMP_AreaP2   := 0;
    NFRMP_ValorT   := 0;
    VSVMI_Controle := QrVSMovItsControle.Value;
    VSVMI_Codigo   := QrVSMovItsCodigo.Value;
    VSVMI_MovimID  := QrVSMovItsMovimID.Value;
    VSVMI_MovimNiv := QrVSMovItsMovimNiv.Value;
    VSVMI_MovimCod := QrVSMovItsMovimCod.Value;
    VSVMI_Empresa  := QrVSMovItsEmpresa.Value;
    VSVMI_SerNF    := QrVSMovItsNFeSer.Value;
    VSVMI_nNF      := QrVSMovItsNFeNum.Value;
    VSVMI_Pecas    := QrVSMovItsPecas.Value;
    VSVMI_PesoKg   := QrVSMovItsPesoKg.Value;
    VSVMI_AreaM2   := QrVSMovItsAreaM2.Value;
    AreaM2WB(VSVMI_AreaM2WB, VSVMI_PesoKgWB, VSVMI_PercRendim, VSVMI_M2Virtual, Grandeza);
    //VSVMI_AreaM2WB :=
    //VSVMI_PercRendim :=
    GraGru1        := QrVSMovItsGraGru1.Value;
    NO_PRD_TAM_COR := QrVSMovItsNO_PRD_TAM_COR.Value;
    Pallet         := QrVSMovItsPallet.Value;
    DataHora       := Geral.FDT(QrVSMovItsDataHora.Value, 1);
    //
    if QrVSMOEnvRet.RecordCount = 0 then
      InsereRegistroPesquisa()
    else
    begin
      while not QrVSMOEnvRet.Eof do
      begin
        Codigo         := QrVSMOEnvRetCodigo.Value;
        NFCMO_FatID    := QrVSMOEnvRetNFCMO_FatID.Value;
        NFCMO_FatNum   := QrVSMOEnvRetNFCMO_FatNum.Value;
        NFCMO_Empresa  := QrVSMOEnvRetNFCMO_Empresa.Value;
        NFCMO_nItem    := QrVSMOEnvRetNFCMO_nItem.Value;
        NFCMO_SerNF    := QrVSMOEnvRetNFCMO_SerNF.Value;
        NFCMO_nNF      := QrVSMOEnvRetNFCMO_nNF.Value;
        NFCMO_Pecas    := QrVSMOEnvRetNFCMO_Pecas.Value;
        NFCMO_PesoKg   := QrVSMOEnvRetNFCMO_PesoKg.Value;
        NFCMO_AreaM2   := QrVSMOEnvRetNFCMO_AreaM2.Value;
        NFCMO_AreaP2   := QrVSMOEnvRetNFCMO_AreaP2.Value;
        NFCMO_CusMOM2  := QrVSMOEnvRetNFCMO_CusMOM2.Value;
        NFCMO_CusMOKG  := QrVSMOEnvRetNFCMO_CusMOKg.Value;
        NFCMO_ValorT   := QrVSMOEnvRetNFCMO_ValorT.Value;
        NFRMP_FatID    := QrVSMOEnvRetNFRMP_FatID.Value;
        NFRMP_FatNum   := QrVSMOEnvRetNFRMP_FatNum.Value;
        NFRMP_Empresa  := QrVSMOEnvRetNFRMP_Empresa.Value;
        NFRMP_nItem    := QrVSMOEnvRetNFRMP_nItem.Value;
        NFRMP_SerNF    := QrVSMOEnvRetNFRMP_SerNF.Value;
        NFRMP_nNF      := QrVSMOEnvRetNFRMP_nNF.Value;
        NFRMP_Pecas    := QrVSMOEnvRetNFRMP_Pecas.Value;
        NFRMP_PesoKg   := QrVSMOEnvRetNFRMP_PesoKg.Value;
        NFRMP_AreaM2   := QrVSMOEnvRetNFRMP_AreaM2.Value;
        NFRMP_AreaP2   := QrVSMOEnvRetNFRMP_AreaP2.Value;
        NFRMP_ValorT   := QrVSMOEnvRetNFRMP_ValorT.Value;
        //
        SerieNF_VMI := 'S�rie ' + Geral.FF0(VSVMI_SerNF) + ' N� ' + Geral.FF0(VSVMI_nNF);
        SerieNF_CMO := 'S�rie ' + Geral.FF0(NFCMO_SerNF) + ' N� ' + Geral.FF0(NFCMO_nNF);
        SerieNF_RMP := 'S�rie ' + Geral.FF0(NFRMP_SerNF) + ' N� ' + Geral.FF0(NFRMP_nNF);
        if QrVSMOEnvRet.RecNo > 1 then
        begin
          VSVMI_Pecas     := 0;
          VSVMI_PesoKg    := 0;
          VSVMI_AreaM2    := 0;
          VSVMI_AreaM2WB  := 0;
          VSVMI_M2Virtual := 0;
        end;
        InsereRegistroPesquisa();
        QrVSMOEnvRet.Next;
      end;
    end;
    //
    QrVSMovIts.Next;
  end;
  //
(*
  case RGRelatorio.ItemIndex of
    1: Ordem :=   'SerieNF_VMI, SerieNF_CMO ';
    2: Ordem :=   'SerieNF_CMO, SerieNF_VMI';
    else Ordem := '***ERRO***';
  end;
*)
  case RGRelatorio.ItemIndex of
    1: Ordem :=   'VSVMI_SerNF, VSVMI_nNF ';
    2: Ordem :=   'NFCMO_SerNF, NFCMO_nNF ';
    else Ordem := '***ERRO***';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMO, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FVSMO,
  'ORDER BY ',
  Ordem,
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_168_01, [
    DModG.frxDsDono,
    frxDsVSMO
  ]);
  Grupo1 := frxWET_CURTI_168_01.FindObject('GH001') as TfrxGroupHeader;
  Grupo1.Visible := True;
  Me_GH1  := frxWET_CURTI_168_01.FindObject('MeGrupo1Head') as TfrxMemoView;
  Me_FT1  := frxWET_CURTI_168_01.FindObject('MeGrupo1Foot') as TfrxMemoView;
  MeTitNF := frxWET_CURTI_168_01.FindObject('MeTitNF') as TfrxMemoView;
  MeNumNF := frxWET_CURTI_168_01.FindObject('MeNumNF') as TfrxMemoView;
  case RGRelatorio.ItemIndex of
    1:
    begin
      Grupo1.Condition := 'frxDsVSMO."SerieNF_VMI"';
      Me_GH1.Memo.Text := 'NF Remessa: [frxDsVSMO."SerieNF_VMI"] [VARF_SALDO_ABERTO_NF]';
      Me_FT1.Memo.Text := 'Total da NF [frxDsVSMO."SerieNF_VMI"]: ';
      MeTitNF.Memo.Text := 'NF MO';
      MeNumNF.Memo.Text := '[frxDsVSMO."NFCMO_nNF"]';
    end;
    2:
    begin
      Grupo1.Condition := 'frxDsVSMO."SerieNF_CMO"';
      Me_GH1.Memo.Text := 'NF M�o de obra: [frxDsVSMO."SerieNF_CMO"]';
      Me_FT1.Memo.Text := 'Total da NF [frxDsVSMO."SerieNF_CMO"]: ';
      MeTitNF.Memo.Text := 'NF Rem';
      MeNumNF.Memo.Text := '[frxDsVSMO."VSVMI_nNF"]';
    end;
    else Grupo1.Condition := '***ERRO***';
  end;
  //
  MyObjects.frxMostra(frxWET_CURTI_168_01, 'Cobran�a de MO');
end;

procedure TFmVSImpMOEnvRet.PesquisaIMEIsOrfaos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _CMO_ORFAO_A_;',
  'CREATE TABLE _CMO_ORFAO_A_',
  'SELECT * ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
  'WHERE MovimID=20',
  'AND MovimNiv=22',
  'AND NFeNum>=' + Geral.FF0(Ed24NFeIni.ValueVariant),
   Geral.ATS_IF(Ed24NFeFim.ValueVariant > Ed24NFeIni.ValueVariant, [
  'AND NFeNum<=' + Geral.FF0(Ed24NFeFim.ValueVariant)]),
  ';',
  'SELECT mer.VSVMI_Controle, coa.* ',
  'FROM _CMO_ORFAO_A_ coa',
  'LEFT JOIN ' + TMeuDB + '.vsmoenvret mer ON mer.VSVMI_Controle=coa.Controle',
  'WHERE mer.VSVMI_Controle IS NULL',
  ';',
  '']);
end;

procedure TFmVSImpMOEnvRet.SpeedButton1Click(Sender: TObject);
const
  MinData = 0;
  HrDefault = 0;
  HabilitaHora = False;
  //
  UsaSerie = False;
  NFeIni = 0;
  NFeFim = 0;
var
  Data: TDateTime;
  Qry: TmySQLQuery;
  Serie: Integer;
begin
  Data := DModG.ObtemAgora() - 90;
  case RGNFSorc.ItemIndex of
    0: Geral.MB_Aviso('Selecione o "' + RGNFSorc.Caption + '"');
    1:
    begin
      DBCheck.ObtemData(Data, Data, MinData, HrDefault, HabilitaHora,
        'Data da NFe Inicial');
      //
      try
        Qry := TmySQLQuery.Create(Dmod);
        VS_PF.AbreSQLListaNFesEmitidasVS(Qry, UsaSerie, Serie, NFeIni, NFeFim,
        //VS_PF.AbreSQLListaNFesEmitidasVS(Qry, Ck24Serie.Checked,
        //Ed24Serie.ValueVariant, Ed24NFeIni.ValueVariant, Ed24NFeFim.ValueVariant,
        Geral.ATS([
        'SELECT Serie, NFe',
        'FROM ' + CO_NFes_XX,
        'WHERE Data>="' + Geral.FDT(data, 1) + '"',
        'ORDER BY Serie, NFe ',
        '']));
        //Geral.MB_SQL(nil, Qry);
        Serie := Qry.FieldByName('Serie').AsInteger;
        Ed24Serie.ValueVariant := Serie;
        Ed24NFeIni.ValueVariant := Qry.FieldByName('NFe').AsInteger;
        Qry.Last;
        Ck24Serie.Checked := Serie = Qry.FieldByName('Serie').AsInteger;
        Ed24NFeFim.ValueVariant := Qry.FieldByName('NFe').AsInteger;
      finally
        Qry.Free;
      end;
    end;
    (*
    2:
    begin
      /
    end;
    *)
    else Geral.MB_Aviso('"' + RGNFSorc.Caption +
    '" sem implementa��o para pesquisa de NFe inicial!');
  end;
end;

end.
