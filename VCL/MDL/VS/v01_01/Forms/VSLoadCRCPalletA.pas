unit VSLoadCRCPalletA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Vcl.Mask, dmkDBEdit, mySQLDbTables, Vcl.Menus;

type
  TFmVSLoadCRCPalletA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSPalletDst: TmySQLQuery;
    QrVSPalletDstCodigo: TIntegerField;
    QrVSPalletDstNome: TWideStringField;
    QrVSPalletDstLk: TIntegerField;
    QrVSPalletDstDataCad: TDateField;
    QrVSPalletDstDataAlt: TDateField;
    QrVSPalletDstUserCad: TIntegerField;
    QrVSPalletDstUserAlt: TIntegerField;
    QrVSPalletDstAlterWeb: TSmallintField;
    QrVSPalletDstAtivo: TSmallintField;
    QrVSPalletDstEmpresa: TIntegerField;
    QrVSPalletDstNO_EMPRESA: TWideStringField;
    QrVSPalletDstStatus: TIntegerField;
    QrVSPalletDstCliStat: TIntegerField;
    QrVSPalletDstGraGruX: TIntegerField;
    QrVSPalletDstNO_CLISTAT: TWideStringField;
    QrVSPalletDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletDstNO_STATUS: TWideStringField;
    QrVSPalletDstDtHrEndAdd: TDateTimeField;
    QrVSPalletDstDtHrEndAdd_TXT: TWideStringField;
    QrVSPalletDstQtdPrevPc: TIntegerField;
    QrVSPalletDstGerRclCab: TIntegerField;
    QrVSPalletDstDtHrFimRcl: TDateTimeField;
    QrVSPalletDstMovimIDGer: TIntegerField;
    QrVSPalletDstClientMO: TIntegerField;
    DsVSPalletDst: TDataSource;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrVSPalletOri: TmySQLQuery;
    QrVSPalletOriCodigo: TIntegerField;
    QrVSPalletOriNome: TWideStringField;
    QrVSPalletOriLk: TIntegerField;
    QrVSPalletOriDataCad: TDateField;
    QrVSPalletOriDataAlt: TDateField;
    QrVSPalletOriUserCad: TIntegerField;
    QrVSPalletOriUserAlt: TIntegerField;
    QrVSPalletOriAlterWeb: TSmallintField;
    QrVSPalletOriAtivo: TSmallintField;
    QrVSPalletOriEmpresa: TIntegerField;
    QrVSPalletOriNO_EMPRESA: TWideStringField;
    QrVSPalletOriStatus: TIntegerField;
    QrVSPalletOriCliStat: TIntegerField;
    QrVSPalletOriGraGruX: TIntegerField;
    QrVSPalletOriNO_CLISTAT: TWideStringField;
    QrVSPalletOriNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletOriNO_STATUS: TWideStringField;
    QrVSPalletOriDtHrEndAdd: TDateTimeField;
    QrVSPalletOriDtHrEndAdd_TXT: TWideStringField;
    QrVSPalletOriQtdPrevPc: TIntegerField;
    QrVSPalletOriGerRclCab: TIntegerField;
    QrVSPalletOriDtHrFimRcl: TDateTimeField;
    QrVSPalletOriMovimIDGer: TIntegerField;
    QrVSPalletOriClientMO: TIntegerField;
    DsVSPalletOri: TDataSource;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    ApenasAtrelar1: TMenuItem;
    QrVSPalletOriAWServerID: TIntegerField;
    QrVSPalletOriAWStatSinc: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure ApenasAtrelar1Click(Sender: TObject);
  private
    { Private declarations }
    function  NeutralizaOrigem(): Boolean;
  public
    { Public declarations }
    procedure ReopenPallets(Pallet: Integer);
  end;

  var
  FmVSLoadCRCPalletA: TFmVSLoadCRCPalletA;

implementation

uses UnMyObjects, ModuleGeral, Module, DmkDAC_PF, UMySQLModule, VSLoadCRCCab1;

{$R *.DFM}

procedure TFmVSLoadCRCPalletA.ApenasAtrelar1Click(Sender: TObject);
var
  Dst_Codigo, Ori_Codigo, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
  AtrelIns: TERPAtrelIns;
begin
  if FmVSLoadCRCCab1.AtrelaTabelas_Codigo('vspalleta',
  QrVSPalletOriCodigo.Value, QrVSPalletDstCodigo.Value,
  QrVSPalletOriAWServerID.Value, QrVSPalletOriAWStatSinc.Value,
  TERPAtrelIns.atrinsNoAtrelButIns) then
  begin
    //
    if FmVSLoadCRCCab1.NeutralizaOrigem('vspalleta', ['Codigo'],
    [QrVSPalletOriCodigo.Value]) then
      Close;
  end;
end;

procedure TFmVSLoadCRCPalletA.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmVSLoadCRCPalletA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSLoadCRCPalletA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSLoadCRCPalletA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSLoadCRCPalletA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSLoadCRCPalletA.NeutralizaOrigem(): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stUpd;
  Codigo         := QrVSPalletOriCodigo.Value;
  AWStatSinc     := Integer(stDwnSinc);
  //
  //Geral.MB_Info(TmySQLDataBase(DModG.QrUpdSync.Database).DatabaseName);
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, 'vspalleta', False, [
  'AWStatSinc'], [
  'Codigo'], [
  AWStatSinc], [
  Codigo], False);
end;

procedure TFmVSLoadCRCPalletA.ReopenPallets(Pallet: Integer);
  procedure ReopenPallet(QrPallet: TmySQLQuery; DB: TmySQLDatabase);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPallet, DB, [
    'SELECT let.*,   ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS    ',
    'FROM vspalleta let   ',
    'LEFT JOIN entidades  emp ON emp.Codigo=let.Empresa   ',
    'LEFT JOIN entidades  cli ON cli.Codigo=let.CliStat   ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'WHERE let.Codigo=' + Geral.FF0(Pallet),
    '']);
  end;
begin
  ReopenPallet(QrVSPalletOri, DModG.MySyncDB);
  ReopenPallet(QrVSPalletDst, Dmod.MyDB);
end;

end.
