unit VSBalCabNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, UnGOTOy, UnInternalConsts, UnMsgInt, StdCtrls,
  Db, (*DBTables,*) Buttons, UMySQLModule, mySQLDbTables, dmkGeral, dmkImage,
  UnDmkEnums, unDmkProcFunc, DmkDAC_PF;

type
  TFmVSBalCabNew = class(TForm)
    QrLocPeriodo: TmySQLQuery;
    QrLocPeriodoPeriodo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel1: TPanel;
    CBAno: TComboBox;
    Label3: TLabel;
    Label2: TLabel;
    CBMes: TComboBox;
    GBRodape: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmVSBalCabNew: TFmVSBalCabNew;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UnVS_PF, VSBalCab, ModVS;

{$R *.DFM}

procedure TFmVSBalCabNew.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
begin
  ImgTipo.SQLType := stLok;
  //
(*
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    Add(IntToStr(Ano-2));
    Add(IntToStr(Ano-1));
    Add(IntToStr(Ano));
    Add(IntToStr(Ano+1));
    Add(IntToStr(Ano+2));
  end;
  CBAno.ItemIndex := 2;
  CBMes.ItemIndex := (Mes - 1);
*)
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0, False);
end;

procedure TFmVSBalCabNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSBalCabNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSBalCabNew.BtConfirmaClick(Sender: TObject);
var
  Ano, Mes, Dia : word;
  Periodo, Atual, GrupoBal: Integer;
  //
  Codigo, CodUsu, Empresa, PrdGrupTip, StqCenCad, CasasProd: Integer;
  Nome, Abertura, Encerrou: String;
begin
  Ano := Geral.IMV(CBAno.Text);
  if MyObjects.FIC(Ano = 0, CBAno, 'Informe o ano!') then
    Exit;
  Mes := CBMes.ItemIndex + 1;
  Periodo := DmkPF.PeriodoEncode(Ano, Mes);
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPeriodo, Dmod.MyDB, [
  'SELECT Periodo  ',
  'FROM vsbalcab ',
  'WHERE Periodo=' + Geral.FF0(Periodo),
  '']);
  if QrLocPeriodo.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este per�odo de balan�o j� existe!');
    Exit;
  end;
  // A - Impede criar do m�s futuro (pela data do sistema)
  DecodeDate(Date, Ano, Mes, Dia);
  Atual := DmkPF.PeriodoEncode(Ano, Mes);
  if Periodo > Atual then
  begin
    Geral.MB_Aviso('M�s inv�lido. [Futuro]');
    Exit;
  end;
  // fim A
  // B - Impede criar m�s anterior ao m�ximo
  if Periodo < VS_PF.VerificaBalanco() then
  begin
    Geral.MB_Aviso('M�s inv�lido. J� existe m�s posterior ao desejado.');
    Exit;
  end;
  //Fim B
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsbalcab', False, [
  ], [
  'Periodo'], [
  ], [
  Periodo], True) then
  begin
    DmModVS.DefinePeriodoBalVSEmpresaLogada();
    FmVSBalCab.DefParams;
    FmVSBalCab.LocCod(FmVSBalCab.QrVSBalCabPeriodo.Value, Periodo);
    FmVSBalCab.Incluinovaempresa1Click(Self);
    //
    Close;
  end;
end;

procedure TFmVSBalCabNew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

end.
