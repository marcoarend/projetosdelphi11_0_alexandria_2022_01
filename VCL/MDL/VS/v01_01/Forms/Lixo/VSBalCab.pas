unit VSBalCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, Grids, DBGrids, Menus, mySQLDbTables,
  ComCtrls, Variants, dmkGeral, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums,
  dmkDBGridZTO, frxClass, frxDBSet, dmkDBGrid;

type
  TFmVSBalCab = class(TForm)
    PainelDados: TPanel;
    DsVSBalCab: TDataSource;
    QrVSBalCab: TmySQLQuery;
    QrVSBalCabPeriodo: TIntegerField;
    QrVSBalCabDataCad: TDateField;
    QrVSBalCabDataAlt: TDateField;
    QrVSBalCabUserCad: TIntegerField;
    QrVSBalCabUserAlt: TIntegerField;
    QrVSBalCabLk: TIntegerField;
    QrVSBalCabPeriodo2: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DBGCI: TdmkDBGridZTO;
    QrVSBalEmp: TmySQLQuery;
    DsVSBalEmp: TDataSource;
    QrVSBalEmpNO_ENT: TWideStringField;
    PB2: TProgressBar;
    PMCab: TPopupMenu;
    Incluinovoinventrio1: TMenuItem;
    Excluiinventrioatual1: TMenuItem;
    PMIts: TPopupMenu;
    Incluinovaempresa1: TMenuItem;
    Removeempresaatual1: TMenuItem;
    QrVSBalEmpPeriodo: TIntegerField;
    QrVSBalEmpEmpresa: TIntegerField;
    QrVSBalEmpLk: TIntegerField;
    QrVSBalEmpDataCad: TDateField;
    QrVSBalEmpDataAlt: TDateField;
    QrVSBalEmpUserCad: TIntegerField;
    QrVSBalEmpUserAlt: TIntegerField;
    QrVSBalEmpAlterWeb: TSmallintField;
    QrVSBalEmpAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSBalCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure QrVSBalCabCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure QrVSBalCabAfterScroll(DataSet: TDataSet);
    procedure QrVSBalCabBeforeClose(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure QrVSBalEmpAfterScroll(DataSet: TDataSet);
    procedure Excluiinventrioatual1Click(Sender: TObject);
    procedure Incluinovoinventrio1Click(Sender: TObject);
    procedure Incluinovaempresa1Click(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
   ////Procedures do form
    function  JaTemBalancoPosterior(): Boolean;
  public
    { Public declarations }

    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReindexaTabela(Locate: Boolean);
    procedure ReEntitula();
    procedure ReopenVSBalEmp(Empresa: Integer);
  end;

var
  FmVSBalCab: TFmVSBalCab;

implementation
  uses UnMyObjects, Module, Principal, ModuleGeral, VSBalCabNew, UnVS_PF,
  UCreate, MyDBCheck, DmkDAC_PF, CreateBlueDerm;

{$R *.DFM}

var
  TimerIni: TDateTime;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSBalCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSBalCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabDel(Excluiinventrioatual1, QrVSBalCab, QrVSBalEmp);
end;

procedure TFmVSBalCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluinovaempresa1, QrVSBalCab);
  MyObjects.HabilitaMenuItemItsDel(Removeempresaatual1, QrVSBalEmp);
end;

procedure TFmVSBalCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSBalCabPeriodo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSBalCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsbalcab';
  VAR_GOTOMYSQLTABLE := QrVSBalCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_PERIODO;
  VAR_GOTONOME := '';//CO_NOME;
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsbalcab');
  VAR_SQLx.Add('WHERE Periodo > 0');
  //
  VAR_SQL1.Add('AND Periodo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmVSBalCab.Excluiinventrioatual1Click(Sender: TObject);
var
  Periodo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Periodo := QrVSBalCabPeriodo.Value;
  if VS_PF.VerificaBalanco()+ 1 <> Periodo then
    Geral.MB_Aviso('Exclus�o da balan�o abortada! Balan�o n�o � o vigente!')
  else begin
    if Geral.MB_Pergunta('Deseja excluir este balan�o e TODOS itens dele?') = ID_YES then
    begin
      (*
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdU, Dmod.MyDB, [
      'DELETE FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID_0000),
      'AND OrigemCodi=' + Geral.FF0(Periodo),
      '']) then
      *)
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdU, Dmod.MyDB, [
      'DELETE FROM vsbalcab ',
      'WHERE Periodo=' + Geral.FF0(Periodo),
      '']);
      //
      Dmod.DesfazBalancoGrade();
      //
      Periodo := VS_PF.VerificaBalanco()+ 1;
      LocCod(Periodo, Periodo);
    end;
  end;
end;

procedure TFmVSBalCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSBalCab.Incluinovaempresa1Click(Sender: TObject);
var
  CliInt: Integer;
  Qry: TmySQLQuery;
var
  Periodo, Empresa: Integer;
begin
  Periodo := QrVSBalCabPeriodo.Value;
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsbalemp ',
    'WHERE Periodo=' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(CliInt),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Empresa j� est� definida neste invent�rio!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
  //
  Empresa := CliInt;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsbalemp', False, [], [
  'Periodo', 'Empresa'], [], [
  Periodo, Empresa], True) then
  begin
    LocCod(Periodo, Periodo);
    QrVSBalEmp.Locate('Empresa', Empresa, []);
  end;
end;

procedure TFmVSBalCab.Incluinovoinventrio1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  IncluiRegistro;
end;

procedure TFmVSBalCab.IncluiRegistro;
begin
  Application.CreateForm(TFmVSBalCabNew, FmVSBalCabNew);
  FmVSBalCabNew.ShowModal;
  FmVSBalCabNew.Destroy;
end;

function TFmVSBalCab.JaTemBalancoPosterior(): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsbalcab ',
    'WHERE Periodo > ' + Geral.FF0(QrVSBalCabPeriodo.Value),
    '']);
    //
    Result := Qry.RecordCount > 0;
    if Result then
      Geral.MB_Aviso('J� existe um balan�o posterior!');
  finally
    Qry.Free;
  end;
end;

procedure TFmVSBalCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSBalCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSBalCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSBalCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSBalCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSBalCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSBalCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSBalCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CAIXA := QrVSBalCabPeriodo.Value;
  Close;
end;

procedure TFmVSBalCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  CriaOForm;
  //
  //MostraEdicao(False, stLok, 0);
end;

procedure TFmVSBalCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSBalCabPeriodo.Value, LaRegistro.Caption);
end;

procedure TFmVSBalCab.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmVSBalCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSBalCab.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmVSBalCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSBalCab.QrVSBalCabAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrVSBalCabPeriodo.Value, False);
  QueryPrincipalAfterOpen;
  ReEntitula();
end;

procedure TFmVSBalCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSBalCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSBalCab.QrVSBalCabCalcFields(DataSet: TDataSet);
begin
  QrVSBalCabPeriodo2.Value := ' Balan�o de '+
  Geral.Maiusculas(dmkPF.PrimeiroDiaDoPeriodo(QrVSBalCabPeriodo.Value, dtTexto), False);
end;

procedure TFmVSBalCab.ReEntitula();
var
  Texto: String;
begin
  if QrVSBalCab.State <> dsInactive then
  begin
    //Texto := 'Balan�o de Insumos: ' + QrVSBalCabPeriodo2.Value;
    Texto := QrVSBalCabPeriodo2.Value;
    MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Texto, False, taCenter, 2, 10, 20);
  end;
end;

procedure TFmVSBalCab.ReindexaTabela(Locate: Boolean);
begin
end;

procedure TFmVSBalCab.ReopenVSBalEmp(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSBalEmp, Dmod.MyDB, [
  'SELECT vbe.*,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM vsbalemp vbe ',
  'LEFT JOIN entidades ent ON ent.Codigo=vbe.Empresa ',
  'WHERE vbe.Periodo=' + Geral.FF0(QrVSBalCabPeriodo.Value),
  'ORDER BY NO_ENT ',
  '']);
  //
  QrVSBalEmp.Locate('Empresa', Empresa, []);
end;

procedure TFmVSBalCab.QrVSBalEmpAfterScroll(DataSet: TDataSet);
begin
  ReindexaTabela(False);
end;

procedure TFmVSBalCab.FormResize(Sender: TObject);
begin
  ReEntitula();
end;

procedure TFmVSBalCab.QrVSBalCabAfterScroll(DataSet: TDataSet);
begin
  ReEntitula();
  ReopenVSBalEmp(0);
end;

procedure TFmVSBalCab.QrVSBalCabBeforeClose(DataSet: TDataSet);
begin
  QrVSBalEmp.Close;
end;

end.

