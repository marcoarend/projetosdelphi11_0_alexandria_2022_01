object FmVSBalCab: TFmVSBalCab
  Left = 340
  Top = 174
  Caption = 'WET-CURTI-083 :: Invent'#225'rio de Couros VS'
  ClientHeight = 624
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 276
    ExplicitTop = 293
    ExplicitWidth = 1008
    ExplicitHeight = 471
    object DBGCI: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 784
      Height = 455
      Align = alClient
      DataSource = DsVSBalEmp
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Empresa'
          Title.Caption = 'C'#243'digo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ENT'
          Title.Caption = 'Nome da empresa'
          Width = 464
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1008
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 960
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 744
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 295
        Height = 32
        Caption = 'Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 295
        Height = 32
        Caption = 'Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 295
        Height = 32
        Caption = 'Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 1008
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1004
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB2: TProgressBar
        Left = 0
        Top = 19
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
        ExplicitWidth = 1004
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 560
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 576
    ExplicitWidth = 1008
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 216
      Height = 47
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
      ExplicitWidth = 122
    end
    object Panel3: TPanel
      Left = 390
      Top = 15
      Width = 392
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      ExplicitLeft = 604
      object Panel7: TPanel
        Left = 259
        Top = 0
        Width = 133
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 577
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtCab: TBitBtn
        Tag = 191
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Invent'#225'rio'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtCabClick
      end
      object BtIts: TBitBtn
        Tag = 393
        Left = 128
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Empresa'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtItsClick
      end
    end
  end
  object DsVSBalCab: TDataSource
    DataSet = QrVSBalCab
    Left = 56
    Top = 209
  end
  object QrVSBalCab: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSBalCabAfterOpen
    BeforeClose = QrVSBalCabBeforeClose
    AfterScroll = QrVSBalCabAfterScroll
    OnCalcFields = QrVSBalCabCalcFields
    SQL.Strings = (
      'SELECT * FROM balancos')
    Left = 56
    Top = 161
    object QrVSBalCabPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrVSBalCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSBalCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSBalCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSBalCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSBalCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSBalCabPeriodo2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo2'
      Size = 50
      Calculated = True
    end
  end
  object QrVSBalEmp: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrVSBalEmpAfterScroll
    SQL.Strings = (
      'SELECT vbe.*,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM vsbalemp vbe '
      'LEFT JOIN entidades ent ON ent.Codigo=vbe.Empresa '
      'WHERE vbe.Periodo <> 0 '
      'ORDER BY NO_ENT ')
    Left = 120
    Top = 160
    object QrVSBalEmpNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrVSBalEmpPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrVSBalEmpEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSBalEmpLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSBalEmpDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSBalEmpDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSBalEmpUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSBalEmpUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSBalEmpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSBalEmpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSBalEmp: TDataSource
    DataSet = QrVSBalEmp
    Left = 120
    Top = 208
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 336
    Top = 532
    object Incluinovoinventrio1: TMenuItem
      Caption = '&Inclui novo invent'#225'rio'
      OnClick = Incluinovoinventrio1Click
    end
    object Excluiinventrioatual1: TMenuItem
      Caption = '&Exclui invent'#225'rio atual'
      OnClick = Excluiinventrioatual1Click
    end
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 464
    Top = 532
    object Incluinovaempresa1: TMenuItem
      Caption = 'Inclui nova empresa'
      OnClick = Incluinovaempresa1Click
    end
    object Removeempresaatual1: TMenuItem
      Caption = '&Remove empresa atual'
    end
  end
end
