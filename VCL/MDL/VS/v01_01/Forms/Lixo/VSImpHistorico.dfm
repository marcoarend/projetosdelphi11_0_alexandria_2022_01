object FmVSImpHistorico: TFmVSImpHistorico
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-120 :: Impress'#227'o de Historico'
  ClientHeight = 545
  ClientWidth = 842
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxWET_CURTI_018_01_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412040000000
    ReportOptions.LastChange = 41608.425381412040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure DBCross1ColumnTotal0OnBeforePrint(Sender: TfrxComponen' +
        't);'
      'begin'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_01_AGetValue
    Left = 68
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 119.055186460000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Movimento de Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 60.472480000000000000
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 332.598640000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 699.213050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 733.228820000000000000
          Top = 64.252010000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PALLET_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 71.811070000000000000
          Top = 83.149660000000000000
          Width = 891.969080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 71.811070000000000000
          Top = 64.252010000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_GRAGRUX_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 287.244280000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 381.732530000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 260.787570000000000000
          Top = 105.826840000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 98.267780000000000000
          Top = 105.826778980000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 566.929500000000000000
          Top = 105.826840000000000000
          Width = 132.283449920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 778.583180000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 733.228820000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 827.717070000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 699.213050000000000000
          Top = 105.826840000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acum. p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 873.071430000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 427.086890000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 105.826840000000000000
          Width = 60.472440944881890000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 166.299320000000000000
          Top = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 211.653680000000000000
          Top = 105.826840000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 476.220780000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 922.205320000000000000
          Top = 105.826840000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 525.354670000000000000
          Top = 105.826840000000000000
          Width = 41.574800710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 971.339210000000000000
        DataSet = frxDsEstqR2
        DataSetName = 'frxDsEstqR2'
        RowCount = 0
        object Memo57: TfrxMemoView
          Left = 60.472480000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Pallet'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 332.598640000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 287.244280000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 381.732530000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 260.787570000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 98.267780000000000000
          Top = -0.000061019999999995
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 566.929500000000000000
          Width = 132.283449920000000000
          Height = 13.228346460000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 778.583180000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AcumAreaP2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 733.228820000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AcumAreaM2'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumAreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 827.717070000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'AcumPesoKg'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 699.213050000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'AcumPecas'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 873.071430000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AcumValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 476.220780000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."CustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DataField = 'DataHora'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 166.299320000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 211.653680000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR2."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 427.086890000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'ValorT'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 922.205320000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR2."AcumCustoM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 525.354670000000000000
          Width = 41.574800710000000000
          Height = 13.228346460000000000
          DataField = 'NFeNum'
          DataSet = frxDsEstqR2
          DataSetName = 'frxDsEstqR2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR2."NFeNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 275.905690000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 600.945270000000000000
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrEstqR2: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrEstqR2CalcFields
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 64
    Top = 48
    object QrEstqR2OrdGrupSeq: TIntegerField
      FieldName = 'OrdGrupSeq'
    end
    object QrEstqR2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqR2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstqR2MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstqR2MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstqR2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR2MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstqR2DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR2Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrEstqR2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstqR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstqR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstqR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEstqR2SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrEstqR2SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrEstqR2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrEstqR2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR2SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstqR2Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrEstqR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrEstqR2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 160
    end
    object QrEstqR2NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR2NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR2AcumPecas: TFloatField
      FieldName = 'AcumPecas'
    end
    object QrEstqR2AcumPesoKg: TFloatField
      FieldName = 'AcumPesoKg'
    end
    object QrEstqR2AcumAreaM2: TFloatField
      FieldName = 'AcumAreaM2'
    end
    object QrEstqR2AcumAreaP2: TFloatField
      FieldName = 'AcumAreaP2'
    end
    object QrEstqR2AcumValorT: TFloatField
      FieldName = 'AcumValorT'
    end
    object QrEstqR2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEstqR2NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
    end
    object QrEstqR2CustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CustoM'
      Calculated = True
    end
    object QrEstqR2AcumCustoM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AcumCustoM'
      Calculated = True
    end
    object QrEstqR2NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrEstqR2NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrEstqR2VSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
  end
  object frxDsEstqR2: TfrxDBDataset
    UserName = 'frxDsEstqR2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'OrdGrupSeq=OrdGrupSeq'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'ValorT=ValorT'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_PALLET=NO_PALLET'
      'NO_FORNECE=NO_FORNECE'
      'AcumPecas=AcumPecas'
      'AcumPesoKg=AcumPesoKg'
      'AcumAreaM2=AcumAreaM2'
      'AcumAreaP2=AcumAreaP2'
      'AcumValorT=AcumValorT'
      'Ativo=Ativo'
      'NO_MovimID=NO_MovimID'
      'CustoM=CustoM'
      'AcumCustoM=AcumCustoM'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab')
    DataSet = QrEstqR2
    BCDToCurrency = False
    Left = 64
    Top = 92
  end
  object QrSumIR2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 64
    Top = 140
    object QrSumIR2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumIR2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumIR2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumIR2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSumIR2ValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
end
