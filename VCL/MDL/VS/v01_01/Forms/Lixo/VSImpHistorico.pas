unit VSImpHistorico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.StdCtrls,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UnDmkProcFunc, DmkGeral, UnDmkEnums, AppListas, UnMyObjects, UnInternalConsts,
  dmkEditCB, frxClass, frxDBSet, Data.DB, mySQLDbTables, Vcl.ComCtrls,
  UnProjGroup_Consts;

type
  TFmVSImpHistorico = class(TForm)
    frxWET_CURTI_018_01_A: TfrxReport;
    QrEstqR2: TmySQLQuery;
    QrEstqR2OrdGrupSeq: TIntegerField;
    QrEstqR2Codigo: TIntegerField;
    QrEstqR2Controle: TIntegerField;
    QrEstqR2MovimCod: TIntegerField;
    QrEstqR2MovimNiv: TIntegerField;
    QrEstqR2Empresa: TIntegerField;
    QrEstqR2Terceiro: TIntegerField;
    QrEstqR2MovimID: TIntegerField;
    QrEstqR2DataHora: TDateTimeField;
    QrEstqR2Pallet: TIntegerField;
    QrEstqR2GraGruX: TIntegerField;
    QrEstqR2Pecas: TFloatField;
    QrEstqR2PesoKg: TFloatField;
    QrEstqR2AreaM2: TFloatField;
    QrEstqR2AreaP2: TFloatField;
    QrEstqR2SrcMovID: TIntegerField;
    QrEstqR2SrcNivel1: TIntegerField;
    QrEstqR2SrcNivel2: TIntegerField;
    QrEstqR2SdoVrtPeca: TFloatField;
    QrEstqR2SdoVrtArM2: TFloatField;
    QrEstqR2Observ: TWideStringField;
    QrEstqR2ValorT: TFloatField;
    QrEstqR2GraGru1: TIntegerField;
    QrEstqR2NO_PRD_TAM_COR: TWideStringField;
    QrEstqR2NO_PALLET: TWideStringField;
    QrEstqR2NO_FORNECE: TWideStringField;
    QrEstqR2AcumPecas: TFloatField;
    QrEstqR2AcumPesoKg: TFloatField;
    QrEstqR2AcumAreaM2: TFloatField;
    QrEstqR2AcumAreaP2: TFloatField;
    QrEstqR2AcumValorT: TFloatField;
    QrEstqR2Ativo: TSmallintField;
    QrEstqR2NO_MovimID: TWideStringField;
    QrEstqR2CustoM: TFloatField;
    QrEstqR2AcumCustoM: TFloatField;
    frxDsEstqR2: TfrxDBDataset;
    QrSumIR2: TmySQLQuery;
    QrSumIR2Pecas: TFloatField;
    QrSumIR2PesoKg: TFloatField;
    QrSumIR2AreaM2: TFloatField;
    QrSumIR2AreaP2: TFloatField;
    QrSumIR2ValorT: TFloatField;
    QrEstqR2NFeSer: TSmallintField;
    QrEstqR2NFeNum: TIntegerField;
    QrEstqR2VSMulNFeCab: TIntegerField;
    procedure QrEstqR2CalcFields(DataSet: TDataSet);
    procedure frxWET_CURTI_018_01_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FVSMovImp2: String;
    //
    procedure InsereVSMovImp2(DataHora: String; AcumPecas, AcumPesoKg,
              AcumAreaM2, AcumAreaP2, AcumValorT: Double);
  public
    { Public declarations }
(*
    Ed01GraGruX, Ed01Pallet, Ed01Terceiro, EdEmpresa: TdmkEditCB;
    TP01DataIni_Date, TP01DataFim_Date, TPDataRelativa_Date: TDateTime;
    Ck01DataIni_Checked, Ck01DataFim_Checked: Boolean;
    CBEmpresa_Text,
    CB01GraGruX_Text, CB01Pallet_Text, CB01Terceiro_Text: String;
    PB1: TProgressBar;
    LaAviso1, LaAviso2: TLabel;
*)
    //
    procedure ImprimeHistorico();
  end;

var
  FmVSImpHistorico: TFmVSImpHistorico;

implementation

{$R *.dfm}

uses
  ModuleGeral, CreateVS, UnVS_PF, DmkDAC_PF, Module, UMySQLModule;

{ TFmVSImpHistorico }

procedure TFmVSImpHistorico.frxWET_CURTI_018_01_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa_Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa_Date
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro_Text, Ed01Terceiro.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PALLET_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Pallet_Text, Ed01Pallet.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_GRAGRUX_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01GraGruX_Text, Ed01GraGruX.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni_Date, TP01DataFim_Date,
    Ck01DataIni_Checked, Ck01DataFim_Checked, '', 'at�', '')
  else
end;

procedure TFmVSImpHistorico.ImprimeHistorico();
const
  TxtCalc = 'Calculando evolu��o do estoque. ';
var
  DataI, SQL_Periodo, SQL_Terceiro, SQL_Pallet, ATT_MovimID: String;
  Empresa, GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  function ParteSQL(Tabela: String): String;
  begin
    Result := Geral.ATS([
    'INSERT INTO ' + FVSMovImp2,
    'SELECT 1 OrdGrupSeq, ',
    'vmi.Codigo, vmi.Controle, vmi.MovimCod, ',
    'vmi.MovimNiv, vmi.Empresa, vmi.Terceiro, ',
    'vmi.MovimID, vmi.DataHora, vmi.Pallet, ',
    'vmi.GraGruX, vmi.Pecas, vmi.PesoKg, ',
    'vmi.AreaM2, vmi.AreaP2, vmi.SrcMovID, ',
    'vmi.SrcNivel1, vmi.SrcNivel2, ',
    'vmi.SdoVrtPeca, vmi.SdoVrtArM2, ',
    'vmi.Observ, vmi.ValorT, ggx.GraGru1, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
    ATT_MovimID,
    '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, ',
    '0 AcumAreaP2, 0 AcumValorT, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    'NFeSer, NFeNum, VSMulNFeCab, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vmi.Terceiro ',
    'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
    'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
    SQL_Periodo,
    SQL_Terceiro,
    SQL_Pallet,
    '; ']);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GragruX := Ed01GraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, Ed01GraGruX, 'Informe a mat�ria-prima') then
    Exit;
  Empresa := DmodG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  FVSMovImp2 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp2, DModG.QrUpdPID1, False);
  //
  if Ck01DataIni_Checked then
    DataI := Geral.FDT(TP01DataIni_Date, 1)
  else
    DataI := Geral.FDT(2, 1);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
    TP01DataIni_Date, TP01DataFim_Date, Ck01DataIni_Checked, Ck01DataFim_Checked);
  //
  VS_PF.DefineSQLTerceiro(False, Ed01Terceiro, SQL_Terceiro);
  VS_PF.DefineSQLPallet(False, Ed01Pallet, SQL_Pallet);

  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR2, DModG.MyPID_DB, [
  'DELETE FROM ' + FVSMovImp2,
  '; ',
  ParteSQL(CO_TAB_VMB),
  ParteSQL(CO_SEL_TAB_VMI),
  'SELECT * FROM ' + FVSMovImp2,
  'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; ',
  ' ']);
  //
  //Geral.MB_SQL(self, QrEstqR2);
  TxtItem := TxtCalc + 'Item %d de ' + Geral.FF0(QrEstqR2.RecordCount) + '.';
  PB1.Position := 0;
  PB1.Max := QrEstqR2.RecordCount;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIR2, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, SUM(vmi.AreaP2) AreaP2, ',
  'SUM(vmi.ValorT) ValorT ',
  'FROM ' + CO_TAB_VMB + ' vmi ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND vmi.DataHora  < "' + DataI + '" ',
  '',
  'UNION',
  '',
  'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, SUM(vmi.AreaP2) AreaP2, ',
  'SUM(vmi.ValorT) ValorT ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND vmi.DataHora  < "' + DataI + '" ',
  '']);
  //Geral.MB_SQL(Self, QrSumIR2);
  //DataHora   := Geral.FDT(TP01DataIni_Date, 1);
  DataHora := DataI;

  AcumPecas  := 0;
  AcumPesoKg := 0;
  AcumAreaM2 := 0;
  AcumAreaP2 := 0;
  AcumValorT := 0;

  QrSumIR2.First;
  while not QrSumIR2.Eof do
  begin
    AcumPecas  := AcumPecas  + QrSumIR2Pecas.Value;
    AcumPesoKg := AcumPesoKg + QrSumIR2PesoKg.Value;
    AcumAreaM2 := AcumAreaM2 + QrSumIR2AreaM2.Value;
    AcumAreaP2 := AcumAreaP2 + QrSumIR2AreaP2.Value;
    AcumValorT := AcumValorT + QrSumIR2ValorT.Value;
    //
    QrSumIR2.Next;
  end;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, TxtCalc);
  InsereVSMovImp2(
    DataHora, AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT);
  QrEstqR2.First;
  while not QrEstqR2.Eof do
  begin
    //PB1.Position := PB1.Position + 1;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      Format(TxtItem, [QrEstqR2.RecNo]));
    //
    Controle := QrEstqR2Controle.Value;
    //
    AcumPecas  := AcumPecas  + QrEstqR2Pecas.Value;
    AcumPesoKg := AcumPesoKg + QrEstqR2PesoKg.Value;
    AcumAreaM2 := AcumAreaM2 + QrEstqR2AreaM2.Value;
    AcumAreaP2 := AcumAreaP2 + QrEstqR2AreaP2.Value;
    AcumValorT := AcumValorT + QrEstqR2ValorT.Value;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVSMovImp2, False, [
    'AcumPecas',
    'AcumPesoKg', 'AcumAreaM2', 'AcumAreaP2',
    'AcumValorT'], [
    'Controle'], [
    AcumPecas,
    AcumPesoKg, AcumAreaM2, AcumAreaP2,
    AcumValorT], [
    Controle], False);
    //
    QrEstqR2.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR2, DModG.MyPID_DB, [
  'SELECT * FROM ' + FVSMovImp2,
  'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; ',
  ' ']);
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_01_A, [
    DModG.frxDsDono,
    frxDsEstqR2
  ]);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  MyObjects.frxMostra(frxWET_CURTI_018_01_A, 'Movimento de MP para semi/acab.');
  PB1.Position := 0;
end;

procedure TFmVSImpHistorico.InsereVSMovImp2(DataHora: String; AcumPecas,
  AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double);
var
  //DataHora,
  Observ, NO_PRD_TAM_COR, NO_PALLET, NO_FORNECE: String;
  OrdGrupSeq, Codigo, Controle, MovimCod, MovimNiv, Empresa, Terceiro, MovimID,
  Pallet, GraGruX, SrcMovID, SrcNivel1, SrcNivel2, GraGru1: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, SdoVrtPeca, SdoVrtArM2, ValorT(*, AcumPecas,
  AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT*): Double;
begin
  OrdGrupSeq     := 0;
  Codigo         := 0;
  Controle       := 0;
  MovimCod       := 0;
  MovimNiv       := 0;
  Empresa        := 0;
  Terceiro       := 0;
  MovimID        := 0;
  //DataHora       := ;
  Pallet         := 0;
  GraGruX        := 0;
  Pecas          := 0;
  PesoKg         := 0;
  AreaM2         := 0;
  AreaP2         := 0;
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SdoVrtPeca     := 0;
  SdoVrtArM2     := 0;
  Observ         := '';
  ValorT         := 0;
  GraGru1        := 0;
  NO_PRD_TAM_COR := '';
  NO_PALLET      := '';
  NO_FORNECE    := '';
{
  AcumPecas      := ;
  AcumPesoKg     := ;
  AcumAreaM2     := ;
  AcumAreaP2     := ;
  AcumValorT     := ;
}
  //
  //if
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FVSMovImp2, False, [
  'OrdGrupSeq', 'Codigo', 'Controle',
  'MovimCod', 'MovimNiv', 'Empresa',
  'Terceiro', 'MovimID', CO_DATA_HORA_GRL,
  'Pallet', 'GraGruX', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'SrcMovID', 'SrcNivel1', 'SrcNivel2',
  'SdoVrtPeca', 'SdoVrtArM2', 'Observ',
  'ValorT', 'GraGru1', 'NO_PRD_TAM_COR',
  'NO_PALLET', 'NO_FORNECE', 'AcumPecas',
  'AcumPesoKg', 'AcumAreaM2', 'AcumAreaP2',
  'AcumValorT'], [
  ], [
  OrdGrupSeq, Codigo, Controle,
  MovimCod, MovimNiv, Empresa,
  Terceiro, MovimID, DataHora,
  Pallet, GraGruX, Pecas,
  PesoKg, AreaM2, AreaP2,
  SrcMovID, SrcNivel1, SrcNivel2,
  SdoVrtPeca, SdoVrtArM2, Observ,
  ValorT, GraGru1, NO_PRD_TAM_COR,
  NO_PALLET, NO_FORNECE, AcumPecas,
  AcumPesoKg, AcumAreaM2, AcumAreaP2,
  AcumValorT], [
  ], False);
end;

procedure TFmVSImpHistorico.QrEstqR2CalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  Valor := 0;
(*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
*)
      if (QrEstqR2AreaM2.Value >=  0.01)
      or (QrEstqR2AreaM2.Value <= -0.01) then
        Valor := QrEstqR2ValorT.Value / QrEstqR2AreaM2.Value
      else
      if QrEstqR2Pecas.Value > 0 then
        Valor := QrEstqR2ValorT.Value / QrEstqR2Pecas.Value;
(*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [2]');
    end;
  end;
*)
  QrEstqR2CustoM.Value := Valor;
  //
  Valor := 0;
(*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
*)
      if (QrEstqR2AcumAreaM2.Value >=  0.01)
      or (QrEstqR2AcumAreaM2.Value <= -0.01) then
        Valor := QrEstqR2AcumValorT.Value / QrEstqR2AcumAreaM2.Value
      else
      if QrEstqR2AcumPecas.Value > 0 then
        Valor := QrEstqR2AcumValorT.Value / QrEstqR2AcumPecas.Value;
(*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [3]');
    end;
  end;
*)
  QrEstqR2AcumCustoM.Value := Valor;
end;

end.
