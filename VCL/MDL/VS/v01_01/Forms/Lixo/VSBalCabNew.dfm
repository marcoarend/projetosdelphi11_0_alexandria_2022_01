object FmVSBalCabNew: TFmVSBalCabNew
  Left = 569
  Top = 308
  Caption = 'WET-CURTI-085 :: Novo Invent'#225'rio de Couros VS'
  ClientHeight = 232
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 398
    object GB_R: TGroupBox
      Left = 431
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 350
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 383
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 302
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 365
        Height = 32
        Caption = 'Novo Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 365
        Height = 32
        Caption = 'Novo Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 365
        Height = 32
        Caption = 'Novo Invent'#225'rio de Couros VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 124
    Width = 479
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
    ExplicitTop = 132
    ExplicitWidth = 398
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 394
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 294
        Height = 16
        Caption = 'INFORME O M'#202'S ATUAL E N'#195'O O ANTERIOR!!!!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 294
        Height = 16
        Caption = 'INFORME O M'#202'S ATUAL E N'#195'O O ANTERIOR!!!!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 76
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 398
    ExplicitHeight = 84
    object Label3: TLabel
      Left = 216
      Top = 28
      Width = 91
      Height = 13
      Caption = 'Ano do movimento:'
    end
    object Label2: TLabel
      Left = 24
      Top = 28
      Width = 92
      Height = 13
      Caption = 'M'#234's do movimento:'
    end
    object CBAno: TComboBox
      Left = 215
      Top = 45
      Width = 95
      Height = 21
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Text = 'CBAno'
    end
    object CBMes: TComboBox
      Left = 25
      Top = 45
      Width = 182
      Height = 21
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'CBMes'
    end
  end
  object GBRodape: TGroupBox
    Left = 0
    Top = 168
    Width = 479
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 176
    ExplicitWidth = 398
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 394
      object PnSaiDesis: TPanel
        Left = 331
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 250
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrLocPeriodo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Periodo '
      'FROM vsbalcab'
      'WHERE Periodo=:P0'
      '')
    Left = 328
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPeriodoPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
end
