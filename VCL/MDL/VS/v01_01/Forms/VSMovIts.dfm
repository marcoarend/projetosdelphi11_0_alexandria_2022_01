object FmVSMovIts: TFmVSMovIts
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-045 :: Gerenciamento de IME-Is'
  ClientHeight = 731
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 625
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 562
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 625
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 474
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 478
      ExplicitWidth = 30
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 369
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 310
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel8: TPanel
          Left = 597
          Top = 0
          Width = 407
          Height = 310
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Gera'#231#227'o: '
            TabOrder = 0
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label15: TLabel
                Left = 4
                Top = 0
                Width = 33
                Height = 13
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit15
              end
              object Label16: TLabel
                Left = 64
                Top = 0
                Width = 43
                Height = 13
                Caption = 'Peso Kg:'
                FocusControl = DBEdit16
              end
              object Label17: TLabel
                Left = 148
                Top = 0
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
                FocusControl = DBEdit17
              end
              object Label18: TLabel
                Left = 232
                Top = 0
                Width = 37
                Height = 13
                Caption = #193'rea ft'#178':'
                FocusControl = DBEdit18
              end
              object Label19: TLabel
                Left = 316
                Top = 0
                Width = 50
                Height = 13
                Caption = 'Valor total:'
                FocusControl = DBEdit19
              end
              object DBEdit15: TDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                DataField = 'Pecas'
                DataSource = DsVSMovIts
                TabOrder = 0
              end
              object DBEdit16: TDBEdit
                Left = 64
                Top = 16
                Width = 80
                Height = 21
                DataField = 'PesoKg'
                DataSource = DsVSMovIts
                TabOrder = 1
              end
              object DBEdit17: TDBEdit
                Left = 148
                Top = 16
                Width = 80
                Height = 21
                DataField = 'AreaM2'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit18: TDBEdit
                Left = 232
                Top = 16
                Width = 80
                Height = 21
                DataField = 'AreaP2'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
              object DBEdit19: TDBEdit
                Left = 316
                Top = 16
                Width = 80
                Height = 21
                DataField = 'ValorT'
                DataSource = DsVSMovIts
                TabOrder = 4
              end
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 57
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Saldo atual: '
            TabOrder = 1
            object Panel14: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label28: TLabel
                Left = 4
                Top = 0
                Width = 33
                Height = 13
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit15
              end
              object Label29: TLabel
                Left = 64
                Top = 0
                Width = 43
                Height = 13
                Caption = 'Peso Kg:'
                FocusControl = DBEdit16
              end
              object Label30: TLabel
                Left = 148
                Top = 0
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
                FocusControl = DBEdit17
              end
              object DBEdit28: TDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                DataField = 'SdoVrtPeca'
                DataSource = DsVSMovIts
                TabOrder = 0
              end
              object DBEdit29: TDBEdit
                Left = 64
                Top = 16
                Width = 80
                Height = 21
                DataField = 'SdoVrtPeso'
                DataSource = DsVSMovIts
                TabOrder = 1
              end
              object DBEdit30: TDBEdit
                Left = 148
                Top = 16
                Width = 80
                Height = 21
                DataField = 'SdoVrtArM2'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
            end
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 114
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Gera'#231#227'o a partir deste IME-I: '
            TabOrder = 2
            object Panel16: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label42: TLabel
                Left = 4
                Top = 0
                Width = 33
                Height = 13
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit15
              end
              object Label43: TLabel
                Left = 64
                Top = 0
                Width = 43
                Height = 13
                Caption = 'Peso Kg:'
                FocusControl = DBEdit16
              end
              object Label44: TLabel
                Left = 148
                Top = 0
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
                FocusControl = DBEdit17
              end
              object Label45: TLabel
                Left = 232
                Top = 0
                Width = 37
                Height = 13
                Caption = #193'rea ft'#178':'
                FocusControl = DBEdit18
              end
              object DBEdit42: TDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                DataField = 'QtdGerPeca'
                DataSource = DsVSMovIts
                TabOrder = 0
              end
              object DBEdit43: TDBEdit
                Left = 64
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdGerPeso'
                DataSource = DsVSMovIts
                TabOrder = 1
              end
              object DBEdit44: TDBEdit
                Left = 148
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdGerArM2'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit45: TDBEdit
                Left = 232
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdGerArP2'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
            end
          end
          object GroupBox7: TGroupBox
            Left = 0
            Top = 171
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Estoque Anterior ???: '
            TabOrder = 3
            object Panel17: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label47: TLabel
                Left = 4
                Top = 0
                Width = 33
                Height = 13
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit15
              end
              object Label48: TLabel
                Left = 64
                Top = 0
                Width = 43
                Height = 13
                Caption = 'Peso Kg:'
                FocusControl = DBEdit16
              end
              object Label49: TLabel
                Left = 148
                Top = 0
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
                FocusControl = DBEdit17
              end
              object Label50: TLabel
                Left = 232
                Top = 0
                Width = 37
                Height = 13
                Caption = #193'rea ft'#178':'
                FocusControl = DBEdit18
              end
              object DBEdit47: TDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                DataField = 'QtdAntPeca'
                DataSource = DsVSMovIts
                TabOrder = 0
              end
              object DBEdit48: TDBEdit
                Left = 64
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdAntPeso'
                DataSource = DsVSMovIts
                TabOrder = 1
              end
              object DBEdit49: TDBEdit
                Left = 148
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdAntArM2'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit50: TDBEdit
                Left = 232
                Top = 16
                Width = 80
                Height = 21
                DataField = 'QtdAntArP2'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 228
            Width = 407
            Height = 82
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 4
            object Label7: TLabel
              Left = 8
              Top = 4
              Width = 47
              Height = 13
              Caption = 'MovimNiv'
              FocusControl = DBEdit6
            end
            object Label8: TLabel
              Left = 68
              Top = 4
              Width = 52
              Height = 13
              Caption = 'MovimTwn'
              FocusControl = DBEdit7
            end
            object Label9: TLabel
              Left = 128
              Top = 4
              Width = 42
              Height = 13
              Caption = 'MovimID'
              FocusControl = DBEdit8
            end
            object Label10: TLabel
              Left = 188
              Top = 4
              Width = 53
              Height = 13
              Caption = 'LnkNivXtr1'
              FocusControl = DBEdit9
            end
            object Label11: TLabel
              Left = 248
              Top = 4
              Width = 53
              Height = 13
              Caption = 'LnkNivXtr2'
              FocusControl = DBEdit10
            end
            object Label39: TLabel
              Left = 308
              Top = 4
              Width = 60
              Height = 13
              Caption = 'Nota MPAG:'
              FocusControl = DBEdit39
            end
            object Label38: TLabel
              Left = 236
              Top = 44
              Width = 40
              Height = 13
              Caption = 'ValorMP'
              FocusControl = DBEdit38
            end
            object Label37: TLabel
              Left = 152
              Top = 44
              Width = 60
              Height = 13
              Caption = 'CustoMOTot'
              FocusControl = DBEdit37
            end
            object Label36: TLabel
              Left = 68
              Top = 44
              Width = 57
              Height = 13
              Caption = 'CustoMOKg'
              FocusControl = DBEdit36
            end
            object Label35: TLabel
              Left = 8
              Top = 44
              Width = 50
              Height = 13
              Caption = 'FornecMO'
              FocusControl = DBEdit35
            end
            object Label58: TLabel
              Left = 320
              Top = 44
              Width = 42
              Height = 13
              Caption = 'CustoPQ'
              FocusControl = DBEdit58
            end
            object DBEdit6: TDBEdit
              Left = 8
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimNiv'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit7: TDBEdit
              Left = 68
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimTwn'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit8: TDBEdit
              Left = 128
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimID'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit9: TDBEdit
              Left = 188
              Top = 20
              Width = 56
              Height = 21
              DataField = 'LnkNivXtr1'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit10: TDBEdit
              Left = 248
              Top = 20
              Width = 56
              Height = 21
              DataField = 'LnkNivXtr2'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
            object DBEdit39: TDBEdit
              Left = 308
              Top = 20
              Width = 92
              Height = 21
              DataField = 'NotaMPAG'
              DataSource = DsVSMovIts
              TabOrder = 5
            end
            object DBEdit37: TDBEdit
              Left = 152
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoMOTot'
              DataSource = DsVSMovIts
              TabOrder = 6
            end
            object DBEdit38: TDBEdit
              Left = 236
              Top = 60
              Width = 80
              Height = 21
              DataField = 'ValorMP'
              DataSource = DsVSMovIts
              TabOrder = 7
            end
            object DBEdit36: TDBEdit
              Left = 68
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoMOKg'
              DataSource = DsVSMovIts
              TabOrder = 8
            end
            object DBEdit35: TDBEdit
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              DataField = 'FornecMO'
              DataSource = DsVSMovIts
              TabOrder = 9
            end
            object DBEdit58: TDBEdit
              Left = 320
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoPQ'
              DataSource = DsVSMovIts
              TabOrder = 10
            end
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 597
          Height = 310
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 597
            Height = 105
            Align = alTop
            Caption = ' Dados Gerais: '
            TabOrder = 0
            object Panel11: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 88
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 0
                Width = 28
                Height = 13
                Caption = 'IME-I:'
                FocusControl = DBEdCodigo
              end
              object Label13: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                FocusControl = DBEdit12
              end
              object Label2: TLabel
                Left = 180
                Top = 0
                Width = 167
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de gera'#231#227'o:'
              end
              object Label12: TLabel
                Left = 428
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Data / hora:'
                FocusControl = DBEdit11
              end
              object Label3: TLabel
                Left = 124
                Top = 0
                Width = 32
                Height = 13
                Caption = 'IME-C:'
                FocusControl = dmkDBEdit1
              end
              object Label4: TLabel
                Left = 64
                Top = 40
                Width = 44
                Height = 13
                Caption = 'Empresa:'
                FocusControl = DBEdit3
              end
              object Label5: TLabel
                Left = 124
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Terceiro:'
                FocusControl = DBEdit4
              end
              object Label6: TLabel
                Left = 184
                Top = 40
                Width = 41
                Height = 13
                Caption = 'Cli. pref.:'
                FocusControl = DBEdit5
              end
              object Label14: TLabel
                Left = 304
                Top = 40
                Width = 33
                Height = 13
                Caption = 'Artigo: '
                FocusControl = DBEdit13
              end
              object Label51: TLabel
                Left = 4
                Top = 40
                Width = 36
                Height = 13
                Caption = 'Codigo:'
                FocusControl = DBEdit3
              end
              object Label54: TLabel
                Left = 544
                Top = 0
                Width = 39
                Height = 13
                Caption = 'Arquivo:'
                FocusControl = DBEdit53
              end
              object Label57: TLabel
                Left = 244
                Top = 40
                Width = 43
                Height = 13
                Caption = 'ClientMO'
                FocusControl = DBEdit57
              end
              object DBEdCodigo: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'Controle'
                DataSource = DsVSMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit12: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                DataField = 'Pallet'
                DataSource = DsVSMovIts
                TabOrder = 1
              end
              object DBEdit1: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'MovimCod'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit2: TDBEdit
                Left = 204
                Top = 16
                Width = 221
                Height = 21
                DataField = 'NO_EstqMovimID'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
              object DBEdit11: TDBEdit
                Left = 428
                Top = 16
                Width = 112
                Height = 21
                DataField = 'DataHora'
                DataSource = DsVSMovIts
                TabOrder = 4
              end
              object dmkDBEdit1: TdmkDBEdit
                Left = 4
                Top = 56
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsVSMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 5
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit3: TDBEdit
                Left = 64
                Top = 56
                Width = 56
                Height = 21
                DataField = 'Empresa'
                DataSource = DsVSMovIts
                TabOrder = 6
              end
              object DBEdit4: TDBEdit
                Left = 124
                Top = 56
                Width = 56
                Height = 21
                DataField = 'Terceiro'
                DataSource = DsVSMovIts
                TabOrder = 7
              end
              object DBEdit5: TDBEdit
                Left = 184
                Top = 56
                Width = 56
                Height = 21
                DataField = 'CliVenda'
                DataSource = DsVSMovIts
                TabOrder = 8
              end
              object DBEdit13: TDBEdit
                Left = 304
                Top = 56
                Width = 56
                Height = 21
                DataField = 'GraGruX'
                DataSource = DsVSMovIts
                TabOrder = 9
              end
              object DBEdit14: TDBEdit
                Left = 360
                Top = 56
                Width = 225
                Height = 21
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSMovIts
                TabOrder = 10
              end
              object DBEdit46: TDBEdit
                Left = 180
                Top = 16
                Width = 25
                Height = 21
                DataField = 'MovimID'
                DataSource = DsVSMovIts
                TabOrder = 11
              end
              object DBEdit53: TDBEdit
                Left = 544
                Top = 16
                Width = 41
                Height = 21
                DataField = 'NO_TTW'
                DataSource = DsVSMovIts
                TabOrder = 12
              end
              object DBEdit57: TDBEdit
                Left = 244
                Top = 56
                Width = 56
                Height = 21
                DataField = 'ClientMO'
                DataSource = DsVSMovIts
                TabOrder = 13
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 105
            Width = 597
            Height = 60
            Align = alTop
            Caption = ' Dados de Origem: '
            TabOrder = 1
            object Panel12: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label20: TLabel
                Left = 4
                Top = 0
                Width = 51
                Height = 13
                Caption = 'IME-I orig.:'
                FocusControl = DBEdSrcNivel2
              end
              object Label21: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                Enabled = False
                FocusControl = DBEdit20
              end
              object Label22: TLabel
                Left = 124
                Top = 0
                Width = 159
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de origem:'
              end
              object Label23: TLabel
                Left = 528
                Top = 0
                Width = 30
                Height = 13
                Caption = 'Artigo:'
                FocusControl = DBEdit23
              end
              object DBEdSrcNivel2: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'SrcNivel2'
                DataSource = DsVSMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                PopupMenu = PMIMIEOrig
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                OnDblClick = DBEdSrcNivel2DblClick
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit20: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                Visible = False
              end
              object DBEdit21: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'SrcNivel1'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit22: TDBEdit
                Left = 180
                Top = 16
                Width = 345
                Height = 21
                DataField = 'NO_SrcMovID'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
              object DBEdit23: TDBEdit
                Left = 528
                Top = 16
                Width = 56
                Height = 21
                DataField = 'SrcGGX'
                DataSource = DsVSMovIts
                TabOrder = 4
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 165
            Width = 597
            Height = 60
            Align = alTop
            Caption = ' Dados de Destino '
            TabOrder = 2
            object Panel13: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label24: TLabel
                Left = 4
                Top = 0
                Width = 54
                Height = 13
                Caption = 'IME-I dest.:'
                FocusControl = DBEdDstNivel2
              end
              object Label25: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                Enabled = False
                FocusControl = DBEdit24
              end
              object Label26: TLabel
                Left = 124
                Top = 0
                Width = 162
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de destino:'
              end
              object Label27: TLabel
                Left = 528
                Top = 0
                Width = 30
                Height = 13
                Caption = 'Artigo:'
                FocusControl = DBEdit27
              end
              object DBEdDstNivel2: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'DstNivel2'
                DataSource = DsVSMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                PopupMenu = PMIMEIDest
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                OnDblClick = DBEdDstNivel2DblClick
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit24: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                Visible = False
              end
              object DBEdit25: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'DstNivel1'
                DataSource = DsVSMovIts
                TabOrder = 2
              end
              object DBEdit26: TDBEdit
                Left = 180
                Top = 16
                Width = 345
                Height = 21
                DataField = 'NO_DstMovID'
                DataSource = DsVSMovIts
                TabOrder = 3
              end
              object DBEdit27: TDBEdit
                Left = 528
                Top = 16
                Width = 56
                Height = 21
                DataField = 'DstGGX'
                DataSource = DsVSMovIts
                TabOrder = 4
              end
            end
          end
          object Panel15: TPanel
            Left = 0
            Top = 225
            Width = 597
            Height = 85
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 3
            object Label40: TLabel
              Left = 240
              Top = 44
              Width = 30
              Height = 13
              Caption = 'Marca'
              FocusControl = DBEdit40
            end
            object Label34: TLabel
              Left = 188
              Top = 44
              Width = 43
              Height = 13
              Caption = 'Misturou:'
              FocusControl = DBEdit34
            end
            object Label41: TLabel
              Left = 128
              Top = 44
              Width = 40
              Height = 13
              Caption = 'Produto:'
              FocusControl = DBEdit41
            end
            object Label33: TLabel
              Left = 68
              Top = 44
              Width = 26
              Height = 13
              Caption = 'Ficha'
              FocusControl = DBEdit33
            end
            object Label32: TLabel
              Left = 8
              Top = 44
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
              FocusControl = DBEdit32
            end
            object Label31: TLabel
              Left = 8
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Observ'
              FocusControl = DBEdit31
            end
            object Label52: TLabel
              Left = 300
              Top = 4
              Width = 134
              Height = 13
              Caption = 'Local (e Centro) de estoque:'
              FocusControl = DBEdit51
            end
            object Label53: TLabel
              Left = 404
              Top = 44
              Width = 27
              Height = 13
              Caption = 'RME:'
              FocusControl = DBEdit52
            end
            object Label55: TLabel
              Left = 476
              Top = 44
              Width = 106
              Height = 13
              Caption = 'Data/hora cor. apont.:'
              FocusControl = DBEdit54
            end
            object Label56: TLabel
              Left = 484
              Top = 4
              Width = 95
              Height = 13
              Caption = 'NF-e S'#233'rie/N'#250'mero:'
              FocusControl = DBEdit55
            end
            object DBEdit40: TDBEdit
              Left = 239
              Top = 60
              Width = 162
              Height = 21
              DataField = 'Marca'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit34: TDBEdit
              Left = 188
              Top = 60
              Width = 48
              Height = 21
              DataField = 'Misturou'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit33: TDBEdit
              Left = 68
              Top = 60
              Width = 56
              Height = 21
              DataField = 'Ficha'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit41: TDBEdit
              Left = 128
              Top = 60
              Width = 56
              Height = 21
              DataField = 'GraGru1'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit32: TDBEdit
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              DataField = 'SerieFch'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
            object DBEdit31: TDBEdit
              Left = 8
              Top = 20
              Width = 289
              Height = 21
              DataField = 'Observ'
              DataSource = DsVSMovIts
              TabOrder = 5
            end
            object DBEdit51: TDBEdit
              Left = 300
              Top = 20
              Width = 181
              Height = 21
              DataField = 'NO_LOC_CEN'
              DataSource = DsVSMovIts
              TabOrder = 6
            end
            object DBEdit52: TDBEdit
              Left = 404
              Top = 60
              Width = 68
              Height = 21
              DataField = 'ReqMovEstq'
              DataSource = DsVSMovIts
              TabOrder = 7
            end
            object DBEdit54: TDBEdit
              Left = 476
              Top = 60
              Width = 112
              Height = 21
              DataField = 'DtCorrApo'
              DataSource = DsVSMovIts
              TabOrder = 8
            end
            object DBEdit55: TDBEdit
              Left = 484
              Top = 20
              Width = 32
              Height = 21
              DataField = 'NFeSer'
              DataSource = DsVSMovIts
              TabOrder = 9
            end
            object DBEdit56: TDBEdit
              Left = 516
              Top = 20
              Width = 72
              Height = 21
              DataField = 'NFeNum'
              DataSource = DsVSMovIts
              TabOrder = 10
            end
          end
        end
      end
      object Panel18: TPanel
        Left = 2
        Top = 325
        Width = 1004
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label59: TLabel
          Left = 8
          Top = 4
          Width = 65
          Height = 13
          Caption = 'VSMulFrnCab'
          FocusControl = DBEdit59
        end
        object Label60: TLabel
          Left = 96
          Top = 4
          Width = 70
          Height = 13
          Caption = 'VSMulNFeCab'
          FocusControl = DBEdit60
        end
        object Label61: TLabel
          Left = 196
          Top = 4
          Width = 51
          Height = 13
          Caption = 'JmpMovID'
          FocusControl = DBEdit61
        end
        object Label62: TLabel
          Left = 256
          Top = 4
          Width = 49
          Height = 13
          Caption = 'JmpNivel1'
          FocusControl = DBEdit62
        end
        object Label63: TLabel
          Left = 316
          Top = 4
          Width = 49
          Height = 13
          Caption = 'JmpNivel2'
          FocusControl = DBEdit63
        end
        object Label65: TLabel
          Left = 376
          Top = 4
          Width = 42
          Height = 13
          Caption = 'JmpGGX'
          FocusControl = DBEdit65
        end
        object Label64: TLabel
          Left = 436
          Top = 4
          Width = 53
          Height = 13
          Caption = 'RmsMovID'
          FocusControl = DBEdit64
        end
        object Label66: TLabel
          Left = 496
          Top = 4
          Width = 51
          Height = 13
          Caption = 'RmsNivel1'
          FocusControl = DBEdit66
        end
        object Label67: TLabel
          Left = 556
          Top = 4
          Width = 51
          Height = 13
          Caption = 'RmsNivel2'
          FocusControl = DBEdit67
        end
        object Label68: TLabel
          Left = 616
          Top = 4
          Width = 42
          Height = 13
          Caption = 'JmpGGX'
          FocusControl = DBEdit68
        end
        object Label69: TLabel
          Left = 676
          Top = 4
          Width = 39
          Height = 13
          Caption = 'GGXRcl'
          FocusControl = DBEdit69
        end
        object Label70: TLabel
          Left = 736
          Top = 4
          Width = 60
          Height = 13
          Caption = 'GSPSrcNiv2'
          FocusControl = DBEdit70
        end
        object Label71: TLabel
          Left = 800
          Top = 4
          Width = 70
          Height = 13
          Caption = 'GSPSrcMovID'
          FocusControl = DBEdit71
        end
        object Label72: TLabel
          Left = 876
          Top = 4
          Width = 63
          Height = 13
          Caption = 'GSPJmpNiv2'
          FocusControl = DBEdit72
        end
        object Label73: TLabel
          Left = 944
          Top = 4
          Width = 73
          Height = 13
          Caption = 'GSPJmpMovID'
          FocusControl = DBEdit73
        end
        object DBEdit59: TDBEdit
          Left = 8
          Top = 20
          Width = 85
          Height = 21
          DataField = 'VSMulFrnCab'
          DataSource = DsVSMovIts
          TabOrder = 0
        end
        object DBEdit60: TDBEdit
          Left = 96
          Top = 20
          Width = 97
          Height = 21
          DataField = 'VSMulNFeCab'
          DataSource = DsVSMovIts
          TabOrder = 1
        end
        object DBEdit61: TDBEdit
          Left = 196
          Top = 20
          Width = 56
          Height = 21
          DataField = 'JmpMovID'
          DataSource = DsVSMovIts
          TabOrder = 2
        end
        object DBEdit62: TDBEdit
          Left = 256
          Top = 20
          Width = 56
          Height = 21
          DataField = 'JmpNivel1'
          DataSource = DsVSMovIts
          TabOrder = 3
        end
        object DBEdit63: TDBEdit
          Left = 316
          Top = 20
          Width = 56
          Height = 21
          DataField = 'JmpNivel2'
          DataSource = DsVSMovIts
          TabOrder = 4
        end
        object DBEdit65: TDBEdit
          Left = 376
          Top = 20
          Width = 56
          Height = 21
          DataField = 'JmpGGX'
          DataSource = DsVSMovIts
          TabOrder = 5
        end
        object DBEdit64: TDBEdit
          Left = 436
          Top = 20
          Width = 56
          Height = 21
          DataField = 'RmsMovID'
          DataSource = DsVSMovIts
          TabOrder = 6
        end
        object DBEdit66: TDBEdit
          Left = 496
          Top = 20
          Width = 56
          Height = 21
          DataField = 'RmsNivel1'
          DataSource = DsVSMovIts
          TabOrder = 7
        end
        object DBEdit67: TDBEdit
          Left = 556
          Top = 20
          Width = 56
          Height = 21
          DataField = 'RmsNivel2'
          DataSource = DsVSMovIts
          TabOrder = 8
        end
        object DBEdit68: TDBEdit
          Left = 616
          Top = 20
          Width = 56
          Height = 21
          DataField = 'JmpGGX'
          DataSource = DsVSMovIts
          TabOrder = 9
        end
        object DBEdit69: TDBEdit
          Left = 676
          Top = 20
          Width = 56
          Height = 21
          DataField = 'GGXRcl'
          DataSource = DsVSMovIts
          TabOrder = 10
        end
        object DBEdit70: TDBEdit
          Left = 736
          Top = 20
          Width = 61
          Height = 21
          DataField = 'GSPSrcNiv2'
          DataSource = DsVSMovIts
          TabOrder = 11
        end
        object DBEdit71: TDBEdit
          Left = 800
          Top = 20
          Width = 73
          Height = 21
          DataField = 'GSPSrcMovID'
          DataSource = DsVSMovIts
          TabOrder = 12
        end
        object DBEdit72: TDBEdit
          Left = 876
          Top = 20
          Width = 65
          Height = 21
          DataField = 'GSPJmpNiv2'
          DataSource = DsVSMovIts
          TabOrder = 13
        end
        object DBEdit73: TDBEdit
          Left = 944
          Top = 20
          Width = 56
          Height = 21
          DataField = 'GSPJmpMovID'
          DataSource = DsVSMovIts
          TabOrder = 14
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 561
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 150
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 324
        Top = 15
        Width = 682
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Label46: TLabel
          Left = 132
          Top = 4
          Width = 146
          Height = 13
          Caption = 'Senha para exclus'#227'o de IME-I:'
        end
        object Panel2: TPanel
          Left = 549
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtIMEI: TBitBtn
          Tag = 611
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-I'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIMEIClick
        end
        object EdSenha: TEdit
          Left = 132
          Top = 21
          Width = 149
          Height = 20
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 2
        end
      end
    end
    object GBMovSrc: TGroupBox
      Left = 0
      Top = 369
      Width = 1008
      Height = 105
      Align = alTop
      Caption = ' IME-Is de Origem: '
      TabOrder = 2
      object DGMovSrc: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 88
        Align = alClient
        DataSource = DsVSMovSrc
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DGMovSrcDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel do movimento'
            Width = 134
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 264
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoM2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoPQ'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeso'
            Title.Caption = 'Sdo Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtArM2'
            Title.Caption = 'Sdo m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = #218'ltima data/hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Visible = True
          end>
      end
    end
    object GBMovDst: TGroupBox
      Left = 0
      Top = 498
      Width = 1008
      Height = 63
      Align = alBottom
      Caption = ' IME-Is de destino:'
      TabOrder = 3
      object DGMovDst: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 46
        Align = alClient
        DataSource = DsVSMovDst
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DGMovDstDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel do movimento'
            Width = 132
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 264
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoM2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoPQ'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeso'
            Title.Caption = 'Sdo Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtArM2'
            Title.Caption = 'Sdo m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = #218'ltima data/hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 120
    Top = 64
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSMovItsBeforeOpen
    AfterOpen = QrVSMovItsAfterOpen
    BeforeClose = QrVSMovItsBeforeClose
    AfterScroll = QrVSMovItsAfterScroll
    OnCalcFields = QrVSMovItsCalcFields
    SQL.Strings = (
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW,'
      'CAST(vmi.Codigo AS SIGNED) Codigo,'
      'CAST(vmi.Controle AS SIGNED) Controle,'
      'CAST(vmi.MovimCod AS SIGNED) MovimCod,'
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv,'
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn,'
      'CAST(vmi.Empresa AS SIGNED) Empresa,'
      'CAST(vmi.ClientMO AS SIGNED) ClientMO,'
      'CAST(vmi.Terceiro AS SIGNED) Terceiro,'
      'CAST(vmi.CliVenda AS SIGNED) CliVenda,'
      'CAST(vmi.MovimID AS SIGNED) MovimID,'
      'CAST(vmi.DataHora AS DATETIME) DataHora,'
      'CAST(vmi.Pallet AS SIGNED) Pallet,'
      'CAST(vmi.GraGruX AS SIGNED) GraGruX,'
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas,'
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg,'
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2,'
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2,'
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT,'
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID,'
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1,'
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2,'
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX,'
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca,'
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso,'
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2,'
      'CAST(vmi.Observ AS CHAR) Observ,'
      'CAST(vmi.SerieFch AS SIGNED) SerieFch,'
      'CAST(vmi.Ficha AS SIGNED) Ficha,'
      'CAST(vmi.Misturou AS UNSIGNED) Misturou,'
      'CAST(vmi.FornecMO AS SIGNED) FornecMO,'
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg,'
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot,'
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP,'
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ,'
      'CAST(vmi.DstMovID AS SIGNED) DstMovID,'
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1,'
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2,'
      'CAST(vmi.DstGGX AS SIGNED) DstGGX,'
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca,'
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso,'
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2,'
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2,'
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca,'
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso,'
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2,'
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2,'
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG,'
      'CAST(vmi.Marca AS CHAR) Marca,'
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib,'
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin,'
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda,'
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2,'
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq,'
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc,'
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe,'
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab,'
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer,'
      'CAST(vmi.NFeNum AS SIGNED) NFeNum,'
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab,'
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID,'
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1,'
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2,'
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID,'
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1,'
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2,'
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl,'
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX,'
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX,'
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo,'
      'CAST(vmi.IxxMovIX AS UNSIGNED) IxxMovIX,'
      'CAST(vmi.IxxFolha AS SIGNED) IxxFolha,'
      'CAST(vmi.IxxLinha AS SIGNED) IxxLinha,'
      'CAST(vmi.CusFrtAvuls AS DECIMAL (15,4)) CusFrtAvuls,'
      'CAST(vmi.CusFrtMOEnv AS DECIMAL (15,4)) CusFrtMOEnv,'
      'CAST(vmi.CusFrtMORet AS DECIMAL (15,4)) CusFrtMORet,'
      'CAST(vmi.CustoMOPc AS DECIMAL (15,6)) CustoMOPc,'
      'CONCAT(gg1.Nome,'
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),'
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome' +
        ')))'
      'NO_PRD_TAM_COR,'
      ''
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1,'
      'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,'
      'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2,'
      'CAST(vmi.GSPInnNiv2 AS SIGNED) GSPInnNiv2,'
      'CAST(vmi.GSPArtNiv2 AS SIGNED) GSPArtNiv2,'
      'CAST(vmi.GSPSrcMovID AS SIGNED) GSPSrcMovID,'
      'CAST(vmi.GSPSrcNiv2 AS SIGNED) GSPSrcNiv2,'
      'CAST(vmi.GSPJmpMovID AS SIGNED) GSPJmpMovID,'
      'CAST(vmi.GSPJmpNiv2 AS SIGNED) GSPJmpNiv2,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'CAST(vsf.Nome AS CHAR) NO_SerieFch,'
      'CAST(vsp.Nome AS CHAR) NO_Pallet,'
      'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN'
      ''
      ''
      'FROM bluederm_panorama.vsmovits vmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch'
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc'
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo'
      ''
      ''
      'WHERE vmi.Controle > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'AND vmi.Controle=:P0'
      '')
    Left = 32
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = 'vsserfch.Nome'
      Size = 60
    end
    object QrVSMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrVSMovItsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVSMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVSMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVSMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVSMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVSMovItsLnkNivXtr1: TLargeintField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrVSMovItsLnkNivXtr2: TLargeintField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrVSMovItsClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrVSMovItsVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrVSMovItsNFeSer: TLargeintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrVSMovItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVSMovItsVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrVSMovItsJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrVSMovItsJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrVSMovItsJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrVSMovItsRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrVSMovItsRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrVSMovItsRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrVSMovItsGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrVSMovItsRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrVSMovItsJmpGGX: TLargeintField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrVSMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSMovItsIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrVSMovItsIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrVSMovItsIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrVSMovItsCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrVSMovItsCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      Required = True
    end
    object QrVSMovItsCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      Required = True
    end
    object QrVSMovItsCustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
      Required = True
    end
    object QrVSMovItsGSPInnNiv2: TLargeintField
      FieldName = 'GSPInnNiv2'
      Required = True
    end
    object QrVSMovItsGSPArtNiv2: TLargeintField
      FieldName = 'GSPArtNiv2'
      Required = True
    end
    object QrVSMovItsGSPSrcMovID: TLargeintField
      FieldName = 'GSPSrcMovID'
      Required = True
    end
    object QrVSMovItsGSPSrcNiv2: TLargeintField
      FieldName = 'GSPSrcNiv2'
      Required = True
    end
    object QrVSMovItsGSPJmpMovID: TLargeintField
      FieldName = 'GSPJmpMovID'
      Required = True
    end
    object QrVSMovItsGSPJmpNiv2: TLargeintField
      FieldName = 'GSPJmpNiv2'
      Required = True
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 32
    Top = 565
  end
  object QrVSMovSrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 108
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovSrcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovSrcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovSrcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovSrcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovSrcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovSrcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovSrcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovSrcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovSrcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovSrcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovSrcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovSrcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovSrcPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovSrcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovSrcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovSrcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovSrcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovSrcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovSrcSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovSrcFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovSrcMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovSrcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovSrcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovSrcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovSrcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovSrcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovSrcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovSrcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovSrcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovSrcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovSrcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovSrcGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovSrcNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovSrcNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrVSMovSrcNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrVSMovSrcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovSrcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovSrcNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovSrcNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovSrcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovSrcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovSrcNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSMovSrcCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMovSrcCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
  end
  object DsVSMovSrc: TDataSource
    DataSet = QrVSMovSrc
    Left = 108
    Top = 565
  end
  object QrVSMovDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 184
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovDstGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovDstNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrVSMovDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrVSMovDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovDstNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSMovDstCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMovDstCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
  end
  object DsVSMovDst: TDataSource
    DataSet = QrVSMovDst
    Left = 184
    Top = 565
  end
  object PMIMEI: TPopupMenu
    OnPopup = PMIMEIPopup
    Left = 528
    Top = 624
    object Recalcula1: TMenuItem
      Caption = '&Recalcula'
      object Recalculasaldos1: TMenuItem
        Caption = '&Saldos'
        OnClick = Recalculasaldos1Click
      end
      object RecalculaReduzidodeorigem1: TMenuItem
        Caption = 'Reduzido de &Origem'
        OnClick = RecalculaReduzidodeorigem1Click
      end
    end
    object AlteradadosdoartigodoIMEI1: TMenuItem
      Caption = '&Altera dados do artigo do IME-I'
      OnClick = AlteradadosdoartigodoIMEI1Click
    end
    object AlteradadosdoitemdeIMEI1: TMenuItem
      Caption = 'Altera dados do item de IM&E-I'
      OnClick = AlteradadosdoitemdeIMEI1Click
    end
    object IrparagerenciamentodoMovimento1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = IrparagerenciamentodoMovimento1Click
    end
    object Irparajaneladopallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = Irparajaneladopallet1Click
    end
    object IrparajaneladaFichaRMP1: TMenuItem
      Caption = 'Ir para janela da Ficha RMP'
      OnClick = IrparajaneladaFichaRMP1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEI1: TMenuItem
      Caption = '&Exclui IME-I'
      OnClick = ExcluiIMEI1Click
    end
    object rvoredoMovimento1: TMenuItem
      Caption = #193'rvore do Movimento'
      OnClick = rvoredoMovimento1Click
    end
    object Recriarbaixa1: TMenuItem
      Caption = '&Recriar baixa'
      OnClick = Recriarbaixa1Click
    end
  end
  object QrInacab: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 260
    Top = 520
    object QrInacabVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrInacabVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrInacabBxaPecas: TFloatField
      FieldName = 'BxaPecas'
    end
    object QrInacabVMI_Pecas: TFloatField
      FieldName = 'VMI_Pecas'
    end
    object QrInacabSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrInacabBxaAreaM2: TFloatField
      FieldName = 'BxaAreaM2'
    end
    object QrInacabBxaAreaP2: TFloatField
      FieldName = 'BxaAreaP2'
    end
    object QrInacabVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
  end
  object PMIMIEOrig: TPopupMenu
    Left = 376
    Top = 120
    object IrparaajaneladoMovimento1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = IrparaajaneladoMovimento1Click
    end
  end
  object PMIMEIDest: TPopupMenu
    Left = 880
    Top = 192
    object MenuItem1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = MenuItem1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 20
    Top = 4
    object PackingListVertical1: TMenuItem
      Caption = 'Packing List Vertical'
      OnClick = PackingListVertical1Click
    end
    object PackingListHorizontal1: TMenuItem
      Caption = 'Packing List Horizontal'
      OnClick = PackingListHorizontal1Click
    end
    object Classesgeradas1: TMenuItem
      Caption = 'Classes geradas'
      OnClick = Classesgeradas1Click
    end
  end
end
