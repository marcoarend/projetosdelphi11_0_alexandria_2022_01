unit VSImpOSFluxo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  frxDBSet, Data.DB, mySQLDbTables,
  dmkGeral, UnDmkEnums;

type
  TFmVSImpOSFluxo = class(TForm)
    QrFluxosIts: TMySQLQuery;
    QrFluxosItsCodigo: TIntegerField;
    QrFluxosItsControle: TIntegerField;
    QrFluxosItsOrdem: TIntegerField;
    QrFluxosItsOperacao: TIntegerField;
    QrFluxosItsAcao1: TWideStringField;
    QrFluxosItsAcao2: TWideStringField;
    QrFluxosItsAcao3: TWideStringField;
    QrFluxosItsAcao4: TWideStringField;
    QrFluxosItsLk: TIntegerField;
    QrFluxosItsDataCad: TDateField;
    QrFluxosItsDataAlt: TDateField;
    QrFluxosItsUserCad: TIntegerField;
    QrFluxosItsUserAlt: TIntegerField;
    QrFluxosItsNOMEOPERACAO: TWideStringField;
    QrFluxosItsSEQ: TIntegerField;
    QrVSPWECab: TMySQLQuery;
    QrVSPWECabCodigo: TIntegerField;
    QrVSPWECabMovimCod: TIntegerField;
    QrVSPWECabEmpresa: TIntegerField;
    QrVSPWECabNO_EMPRESA: TWideStringField;
    QrVSPWECabDtHrAberto: TDateTimeField;
    QrVSPWECabDtHrLibOpe: TDateTimeField;
    QrVSPWECabDtHrCfgOpe: TDateTimeField;
    QrVSPWECabDtHrFimOpe: TDateTimeField;
    QrVSPWECabNome: TWideStringField;
    QrVSPWECabLk: TIntegerField;
    QrVSPWECabDataCad: TDateField;
    QrVSPWECabDataAlt: TDateField;
    QrVSPWECabUserCad: TIntegerField;
    QrVSPWECabUserAlt: TIntegerField;
    QrVSPWECabAlterWeb: TSmallintField;
    QrVSPWECabAtivo: TSmallintField;
    QrVSPWECabPecasMan: TFloatField;
    QrVSPWECabAreaManM2: TFloatField;
    QrVSPWECabAreaManP2: TFloatField;
    QrVSPWECabTipoArea: TSmallintField;
    QrVSPWECabNO_TIPO: TWideStringField;
    QrVSPWECabNO_DtHrFimOpe: TWideStringField;
    QrVSPWECabNO_DtHrLibOpe: TWideStringField;
    QrVSPWECabNO_DtHrCfgOpe: TWideStringField;
    QrVSPWECabCacCod: TIntegerField;
    QrVSPWECabGraGruX: TIntegerField;
    QrVSPWECabCustoManMOKg: TFloatField;
    QrVSPWECabCustoManMOTot: TFloatField;
    QrVSPWECabValorManMP: TFloatField;
    QrVSPWECabValorManT: TFloatField;
    QrVSPWECabPecasSrc: TFloatField;
    QrVSPWECabAreaSrcM2: TFloatField;
    QrVSPWECabAreaSrcP2: TFloatField;
    QrVSPWECabPecasDst: TFloatField;
    QrVSPWECabAreaDstM2: TFloatField;
    QrVSPWECabAreaDstP2: TFloatField;
    QrVSPWECabPecasSdo: TFloatField;
    QrVSPWECabAreaSdoM2: TFloatField;
    QrVSPWECabAreaSdoP2: TFloatField;
    QrVSPWECabPesoKgSrc: TFloatField;
    QrVSPWECabPesoKgMan: TFloatField;
    QrVSPWECabPesoKgDst: TFloatField;
    QrVSPWECabPesoKgSdo: TFloatField;
    QrVSPWECabValorTMan: TFloatField;
    QrVSPWECabValorTSrc: TFloatField;
    QrVSPWECabValorTSdo: TFloatField;
    QrVSPWECabPecasINI: TFloatField;
    QrVSPWECabAreaINIM2: TFloatField;
    QrVSPWECabAreaINIP2: TFloatField;
    QrVSPWECabPesoKgINI: TFloatField;
    QrVSPWECabPesoKgBxa: TFloatField;
    QrVSPWECabPecasBxa: TFloatField;
    QrVSPWECabAreaBxaM2: TFloatField;
    QrVSPWECabAreaBxaP2: TFloatField;
    QrVSPWECabValorTBxa: TFloatField;
    QrVSPWECabCliente: TIntegerField;
    QrVSPWECabNO_Cliente: TWideStringField;
    QrVSPWECabNFeRem: TIntegerField;
    QrVSPWECabLPFMO: TWideStringField;
    QrVSPWECabTemIMEIMrt: TIntegerField;
    QrVSPWECabGGXDst: TIntegerField;
    QrVSPWECabSerieRem: TSmallintField;
    QrVSPWECabVSCOPCab: TIntegerField;
    QrVSPWECabNO_VSCOPCab: TWideStringField;
    QrVSPWECabEmitGru: TIntegerField;
    QrVSPWECabNO_EmitGru: TWideStringField;
    QrVSPWECabVSArtCab: TIntegerField;
    QrVSPWECabNO_VSArtCab: TWideStringField;
    QrVSPWECabEmitGrLote: TWideStringField;
    frxDsVSPWECab: TfrxDBDataset;
    frxDsVSArtCab: TfrxDBDataset;
    frxDsFluxosIts: TfrxDBDataset;
    QrVSArtCab: TMySQLQuery;
    QrVSArtCabCodigo: TIntegerField;
    QrVSArtCabNome: TWideStringField;
    QrVSArtCabNO_ReceiRecu: TWideStringField;
    QrVSArtCabNO_ReceiRefu: TWideStringField;
    QrVSArtCabNO_ReceiAcab: TWideStringField;
    QrVSArtCabReceiRecu: TIntegerField;
    QrVSArtCabReceiRefu: TIntegerField;
    QrVSArtCabReceiAcab: TIntegerField;
    QrVSArtCabTxtMPs: TWideStringField;
    QrVSArtCabObserva: TWideMemoField;
    QrVSArtCabLk: TIntegerField;
    QrVSArtCabDataCad: TDateField;
    QrVSArtCabDataAlt: TDateField;
    QrVSArtCabUserCad: TIntegerField;
    QrVSArtCabUserAlt: TIntegerField;
    QrVSArtCabAlterWeb: TSmallintField;
    QrVSArtCabAtivo: TSmallintField;
    QrVSArtCabFluxProCab: TIntegerField;
    QrVSArtCabFluxPcpCab: TIntegerField;
    QrVSArtCabNO_FluxProCab: TWideStringField;
    QrVSArtCabNO_FluxPcpCab: TWideStringField;
    QrVSPWEAtu: TMySQLQuery;
    QrVSPWEAtuCusFrtMORet: TFloatField;
    QrVSPWEAtuCusFrtMOEnv: TFloatField;
    QrVSPWEAtuCodigo: TLargeintField;
    QrVSPWEAtuControle: TLargeintField;
    QrVSPWEAtuMovimCod: TLargeintField;
    QrVSPWEAtuMovimNiv: TLargeintField;
    QrVSPWEAtuMovimTwn: TLargeintField;
    QrVSPWEAtuEmpresa: TLargeintField;
    QrVSPWEAtuTerceiro: TLargeintField;
    QrVSPWEAtuCliVenda: TLargeintField;
    QrVSPWEAtuMovimID: TLargeintField;
    QrVSPWEAtuDataHora: TDateTimeField;
    QrVSPWEAtuPallet: TLargeintField;
    QrVSPWEAtuGraGruX: TLargeintField;
    QrVSPWEAtuPecas: TFloatField;
    QrVSPWEAtuPesoKg: TFloatField;
    QrVSPWEAtuAreaM2: TFloatField;
    QrVSPWEAtuAreaP2: TFloatField;
    QrVSPWEAtuValorT: TFloatField;
    QrVSPWEAtuSrcMovID: TLargeintField;
    QrVSPWEAtuSrcNivel1: TLargeintField;
    QrVSPWEAtuSrcNivel2: TLargeintField;
    QrVSPWEAtuSrcGGX: TLargeintField;
    QrVSPWEAtuSdoVrtPeca: TFloatField;
    QrVSPWEAtuSdoVrtPeso: TFloatField;
    QrVSPWEAtuSdoVrtArM2: TFloatField;
    QrVSPWEAtuObserv: TWideStringField;
    QrVSPWEAtuSerieFch: TLargeintField;
    QrVSPWEAtuFicha: TLargeintField;
    QrVSPWEAtuMisturou: TLargeintField;
    QrVSPWEAtuFornecMO: TLargeintField;
    QrVSPWEAtuCustoMOKg: TFloatField;
    QrVSPWEAtuCustoMOM2: TFloatField;
    QrVSPWEAtuCustoMOTot: TFloatField;
    QrVSPWEAtuValorMP: TFloatField;
    QrVSPWEAtuDstMovID: TLargeintField;
    QrVSPWEAtuDstNivel1: TLargeintField;
    QrVSPWEAtuDstNivel2: TLargeintField;
    QrVSPWEAtuDstGGX: TLargeintField;
    QrVSPWEAtuQtdGerPeca: TFloatField;
    QrVSPWEAtuQtdGerPeso: TFloatField;
    QrVSPWEAtuQtdGerArM2: TFloatField;
    QrVSPWEAtuQtdGerArP2: TFloatField;
    QrVSPWEAtuQtdAntPeca: TFloatField;
    QrVSPWEAtuQtdAntPeso: TFloatField;
    QrVSPWEAtuQtdAntArM2: TFloatField;
    QrVSPWEAtuQtdAntArP2: TFloatField;
    QrVSPWEAtuNotaMPAG: TFloatField;
    QrVSPWEAtuNO_PALLET: TWideStringField;
    QrVSPWEAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEAtuNO_TTW: TWideStringField;
    QrVSPWEAtuID_TTW: TLargeintField;
    QrVSPWEAtuNO_FORNECE: TWideStringField;
    QrVSPWEAtuReqMovEstq: TLargeintField;
    QrVSPWEAtuCUSTO_M2: TFloatField;
    QrVSPWEAtuCUSTO_P2: TFloatField;
    QrVSPWEAtuNO_LOC_CEN: TWideStringField;
    QrVSPWEAtuMarca: TWideStringField;
    QrVSPWEAtuPedItsLib: TLargeintField;
    QrVSPWEAtuStqCenLoc: TLargeintField;
    QrVSPWEAtuNO_FICHA: TWideStringField;
    QrVSPWEAtuNO_FORNEC_MO: TWideStringField;
    QrVSPWEAtuClientMO: TLargeintField;
    QrVSPWEAtuCustoPQ: TFloatField;
    frxDsVSPWEAtu: TfrxDBDataset;
    QrVSPWEAtuNO_CLIENT_MO: TWideStringField;
    QrVSPWECabNO_PRD_TAM_COR_DST: TWideStringField;
    QrVSPWECabValorTDst: TFloatField;
    QrVSPWECabKgCouPQ: TFloatField;
    QrVSPWECabGGXSrc: TIntegerField;
    QrVSPWECabNFeStatus: TIntegerField;
    QrVSPWECabOperacoes: TIntegerField;
    QrVSPWECabVSVmcWrn: TSmallintField;
    QrVSPWECabVSVmcObs: TWideStringField;
    QrVSPWECabVSVmcSeq: TWideStringField;
    QrVSPWECabVSVmcSta: TSmallintField;
    QrVSPWECabLinCulReb: TIntegerField;
    QrVSPWECabLinCabReb: TIntegerField;
    QrVSPWECabLinCulSem: TIntegerField;
    QrVSPWECabLinCabSem: TIntegerField;
    QrVSPWECabDataPed: TDateTimeField;
    QrVSPWECabDataStart: TDateTimeField;
    QrVSPWECabDataCrust: TDateTimeField;
    QrVSPWECabDataExped: TDateTimeField;
    QrVSPWECabReceiRecu: TIntegerField;
    QrVSPWECabReceiRefu: TIntegerField;
    QrVSPWECabAWServerID: TIntegerField;
    QrVSPWECabAWStatSinc: TSmallintField;
    QrVSPWECabClasses: TWideStringField;
    QrVSPWECabAREA_SEL: TFloatField;
    QrVSPWECabNO_FLUXO: TWideStringField;
    QrVSPWECabNO_ReceiRecu: TWideStringField;
    QrVSPWECabNO_ReceiRefu: TWideStringField;
    QrVSPWEOriPallet: TMySQLQuery;
    QrVSPWEOriPalletPallet: TLargeintField;
    QrVSPWEOriPalletGraGruX: TLargeintField;
    QrVSPWEOriPalletPecas: TFloatField;
    QrVSPWEOriPalletAreaM2: TFloatField;
    QrVSPWEOriPalletAreaP2: TFloatField;
    QrVSPWEOriPalletPesoKg: TFloatField;
    QrVSPWEOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEOriPalletNO_Pallet: TWideStringField;
    QrVSPWEOriPalletSerieFch: TLargeintField;
    QrVSPWEOriPalletFicha: TLargeintField;
    QrVSPWEOriPalletNO_TTW: TWideStringField;
    QrVSPWEOriPalletID_TTW: TLargeintField;
    QrVSPWEOriPalletSeries_E_Fichas: TWideStringField;
    QrVSPWEOriPalletTerceiro: TLargeintField;
    QrVSPWEOriPalletMarca: TWideStringField;
    QrVSPWEOriPalletNO_FORNECE: TWideStringField;
    QrVSPWEOriPalletNO_SerieFch: TWideStringField;
    QrVSPWEOriPalletValorT: TFloatField;
    QrVSPWEOriPalletSdoVrtPeca: TFloatField;
    QrVSPWEOriPalletSdoVrtPeso: TFloatField;
    QrVSPWEOriPalletSdoVrtArM2: TFloatField;
    frxDsVSPWEOriPallet: TfrxDBDataset;
    frxWET_CURTI_233_A: TfrxReport;
    QrFluxosICt: TMySQLQuery;
    QrFluxosICtCodigo: TIntegerField;
    QrFluxosICtControle: TIntegerField;
    QrFluxosICtConta: TIntegerField;
    QrFluxosICtAcao1: TWideStringField;
    QrFluxosICtOrdem: TIntegerField;
    QrFluxosICtSEQ: TIntegerField;
    frxDsFluxosICt: TfrxDBDataset;
    frxDsMPPIts: TfrxDBDataset;
    QrVSPWECabMPVIts: TIntegerField;
    frxWET_CURTI_233_B: TfrxReport;
    QrMPPIts: TMySQLQuery;
    QrMPPItsNOMEMP: TWideStringField;
    QrMPPItsCodigo: TIntegerField;
    QrMPPItsControle: TIntegerField;
    QrMPPItsMP: TIntegerField;
    QrMPPItsQtde: TFloatField;
    QrMPPItsPreco: TFloatField;
    QrMPPItsValor: TFloatField;
    QrMPPItsLk: TIntegerField;
    QrMPPItsDataCad: TDateField;
    QrMPPItsDataAlt: TDateField;
    QrMPPItsUserCad: TIntegerField;
    QrMPPItsUserAlt: TIntegerField;
    QrMPPItsTexto: TWideStringField;
    QrMPPItsDesco: TFloatField;
    QrMPPItsSubTo: TFloatField;
    QrMPPItsEntrega: TDateField;
    QrMPPItsPronto: TDateField;
    QrMPPItsPRONTO_TXT: TWideStringField;
    QrMPPItsStatus: TIntegerField;
    QrMPPItsFluxo: TIntegerField;
    QrMPPItsClasse: TWideStringField;
    QrMPPItsM2Pedido: TFloatField;
    QrMPPItsPecas: TFloatField;
    QrMPPItsUnidade: TIntegerField;
    QrMPPItsObserv: TWideMemoField;
    QrMPPItsNOMEUNIDADE: TWideStringField;
    QrMPPItsEspesTxt: TWideStringField;
    QrMPPItsCorTxt: TWideStringField;
    QrMPPItsPedido: TIntegerField;
    QrMPPItsAlterWeb: TSmallintField;
    QrMPPItsPrecoPed: TFloatField;
    QrMPPItsValorPed: TFloatField;
    QrMPPItsTipoProd: TSmallintField;
    QrMPPItsNOMETIPOPROD: TWideStringField;
    QrMPPItsNOMEFLUXO: TWideStringField;
    QrMPPItsDescricao: TWideMemoField;
    QrMPPItsComplementacao: TWideMemoField;
    QrMPPItsAtivo: TSmallintField;
    QrMPPItsCustoPQ: TFloatField;
    QrMPPItsGraGruX: TIntegerField;
    QrMPPItsReceiRecu: TIntegerField;
    QrMPPItsReceiRefu: TIntegerField;
    QrMPPItsReceiAcab: TIntegerField;
    QrMPPItsTxtMPs: TWideStringField;
    QrMPPItsNO_Fluxo: TWideStringField;
    QrMPPItsNO_ReceiRecu: TWideStringField;
    QrMPPItsNO_ReceiRefu: TWideStringField;
    QrMPPItsNO_ReceiAcab: TWideStringField;
    QrMPPItsNO_PRD_TAM_COR: TWideStringField;
    QrMPPItsCustoWB: TFloatField;
    QrMPPItsDtaCrust: TDateField;
    QrMPPItsVSArtCab: TIntegerField;
    QrMPPItsVSArtGGX: TIntegerField;
    QrMPPItsVSArtCab_TXT: TWideStringField;
    QrMPPItsFluxPcpCab: TIntegerField;
    QrMPP: TMySQLQuery;
    QrMPPNOMEVENDEDOR: TWideStringField;
    QrMPPNOMECLIENTE: TWideStringField;
    QrMPPCodigo: TIntegerField;
    QrMPPCliente: TIntegerField;
    QrMPPVendedor: TIntegerField;
    QrMPPDataF: TDateField;
    QrMPPQtde: TFloatField;
    QrMPPValor: TFloatField;
    QrMPPObz: TWideStringField;
    QrMPPLk: TIntegerField;
    QrMPPDataCad: TDateField;
    QrMPPDataAlt: TDateField;
    QrMPPUserCad: TIntegerField;
    QrMPPUserAlt: TIntegerField;
    QrMPPAlterWeb: TSmallintField;
    QrMPPAtivo: TSmallintField;
    QrMPPTransp: TIntegerField;
    QrMPPCondicaoPg: TIntegerField;
    QrMPPNOMETRANSP: TWideStringField;
    QrMPPNO_CONDICAOPG: TWideStringField;
    QrMPPPedidCli: TWideStringField;
    frxDsMPP: TfrxDBDataset;
    QrVSPWECabObserv: TWideMemoField;
    procedure QrVSPWECabCalcFields(DataSet: TDataSet);
    procedure QrFluxosItsAfterScroll(DataSet: TDataSet);
    procedure QrFluxosItsBeforeClose(DataSet: TDataSet);
    procedure QrMPPItsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FModelo: Integer;
    procedure ReopenFluxosICt();
    procedure ReopenMPP();
  public
    { Public declarations }
    procedure ImprimeOS(Codigo, Modelo: Integer);
  end;

var
  FmVSImpOSFluxo: TFmVSImpOSFluxo;

implementation

uses Module, DmkDAC_PF, UnMyObjects, UnVS_EFD_ICMS_IPI, UnVS_CRC_PF;

{$R *.dfm}

{ TFmVSImpOSFluxo }

procedure TFmVSImpOSFluxo.ImprimeOS(Codigo, Modelo: Integer);
var
 Report: TfrxReport;
begin
  FModelo := Modelo;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPWECab, Dmod.MyDB, [
  'SELECT  fo1.Nome NO_ReceiRecu, ',
  'fo2.Nome NO_ReceiRefu, ', //ti1.Nome NO_ReceiAcab, ',
  'vcc.Nome NO_VSCOPCab,   ',
  'IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI,  ',
  'IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2,  ',
  'IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcP2) AreaINIP2,  ',
  'IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,   ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente,   ',
  'egr.Nome NO_EmitGru, vac.Nome NO_VSArtCab, fpc.Nome NO_FLUXO, ',
  VS_CRC_PF.SQL_NO_GGX_DST(),
  'voc.*  ',
  'FROM vspwecab voc  ',
  'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa  ',
  'LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente  ',
  'LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab  ',
  'LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru  ',
  'LEFT JOIN vsartcab  vac ON vac.Codigo=voc.VSArtCab  ',
  'LEFT JOIN fluxos   fpc ON fpc.Codigo=vac.FluxProCab ',
  'LEFT JOIN formulas fo1 ON fo1.Numero=voc.ReceiRecu  ',
  'LEFT JOIN formulas fo2 ON fo2.Numero=voc.ReceiRefu  ',
  //'LEFT JOIN formulas ti1 ON ti1.Numero=voc.ReceiAcab  ',
  VS_CRC_PF.SQL_LJ_GGX_DST('voc'),
  'WHERE voc.Codigo=' + Geral.FF0(Codigo),
  '']);
  //Geral.MB_Teste(QrVSPWECab.SQL.Text);
  //
  // Em processo
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSPWEAtu, QrVSPWECabMovimCod.Value, 0,
  QrVSPWECabTemIMEIMrt.Value, eminEmWEndInn);
  //Geral.MB_Info(QrVSPWEAtu.SQL.Text);

  UnDmkDAC_PF.AbreMySQLQuery0(QrVSArtCab, Dmod.MyDB, [
  'SELECT fo1.Nome NO_ReceiRecu,  ',
  'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,  ',
  'fro.Nome NO_FluxProCab, fpc.Nome NO_FluxPcpCab, vac.*  ',
  'FROM vsartcab vac  ',
  'LEFT JOIN fluxos       fro ON fro.Codigo=vac.FluxProCab  ',
  'LEFT JOIN fluxpcpcab   fpc ON fpc.Codigo=vac.FluxPcpCab  ',
  'LEFT JOIN formulas fo1 ON fo1.Numero=vac.ReceiRecu  ',
  'LEFT JOIN formulas fo2 ON fo2.Numero=vac.ReceiRefu  ',
  'LEFT JOIN formulas ti1 ON ti1.Numero=vac.ReceiAcab  ',
  'WHERE vac.Codigo=' + Geral.FF0(QrVSPWECabVSArtCab.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosIts, Dmod.MyDB, [
  'SELECT ope.Nome NOMEOPERACAO, fli.*',
  'FROM fluxosits fli',
  'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao',
  'WHERE fli.Codigo=' + Geral.FF0(QrVSArtCabFluxProCab.Value),
  'ORDER BY fli.Ordem, fli.Controle DESC',
  '']);
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriPallet(QrVSPWEOriPallet, (*MovimCod,*)
  QrVSPWECabMovimCod.Value, (*MovNivSrc->eminSorc*)eminSorcWEnd,
  (*TemIMEIMrt*)QrVSPWECabTemIMEIMrt.Value, (*Pallet*)0);
  //
  Report := nil;
  case Modelo of
    1:
    begin
      Report := frxWET_CURTI_233_A;
      MyObjects.frxDefineDataSets(Report, [
        Dmod.frxDsMaster,
        frxDsVSPWECab,
        frxDsVSPWEAtu,
        frxDsFluxosIts,
        frxDsFluxosICt,
        frxDsVSArtCab,
        frxDsVSPWEOriPallet]);
    end;
    2:
    begin
      ReopenMPP();
      //
      Report := frxWET_CURTI_233_B;
      MyObjects.frxDefineDataSets(Report, [
        Dmod.frxDsMaster,
        frxDsVSPWECab,
        frxDsVSPWEAtu,
        frxDsFluxosIts,
        frxDsVSArtCab,
        frxDsVSPWEOriPallet,
        frxDsMPP,
        frxDsMPPIts
        ]);
    end else
      Geral.MB_Erro('Tipo de OS n�o implementado! Comunique a DERMATEK!');
  end;
  if Report <> nil then
  begin
    MyObjects.frxMostra(Report, 'OS Semi Acabado');
  end;
end;

procedure TFmVSImpOSFluxo.QrFluxosItsAfterScroll(DataSet: TDataSet);
begin
  if FModelo = 1 then
    ReopenFluxosICt();
end;

procedure TFmVSImpOSFluxo.QrFluxosItsBeforeClose(DataSet: TDataSet);
begin
  QrFluxosICt.Close;
end;

procedure TFmVSImpOSFluxo.QrMPPItsCalcFields(DataSet: TDataSet);
begin
  QrMPPItsSubTo.Value := QrMPPItsValor.Value+QrMPPItsDesco.Value;
  if QrMPPItsPronto.Value < 2 then QrMPPItsPRONTO_TXT.Value := ' * N�O * '
  else QrMPPItsPRONTO_TXT.Value := Geral.FDT(QrMPPItsPronto.Value, 2);
  //
  if QrMPPItsTipoProd.Value = 0 then
    QrMPPItsNOMETIPOPROD.Value := 'VENDA'
  else
    QrMPPItsNOMETIPOPROD.Value := 'M�O DE OBRA';
end;

procedure TFmVSImpOSFluxo.QrVSPWECabCalcFields(DataSet: TDataSet);
begin
  case QrVSPWECabTipoArea.Value of
    0:
    begin
      QrVSPWECabNO_TIPO.Value := 'm�';
      QrVSPWECabAREA_SEL.Value := QrVSPWECabAreaINIM2.Value;
    end;
    1:
    begin
      QrVSPWECabNO_TIPO.Value := 'ft�';
      QrVSPWECabAREA_SEL.Value := QrVSPWECabAreaINIP2.Value;
    end;
    else
    begin
      QrVSPWECabNO_TIPO.Value := '???';
      QrVSPWECabAREA_SEL.Value := 0.00;
    end;
  end;
  QrVSPWECabNO_DtHrLibOpe.Value := Geral.FDT(QrVSPWECabDtHrLibOpe.Value, 106, True);
  QrVSPWECabNO_DtHrFimOpe.Value := Geral.FDT(QrVSPWECabDtHrFimOpe.Value, 106, True);
  QrVSPWECabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSPWECabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSImpOSFluxo.ReopenFluxosICt();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosIct, Dmod.MyDB, [
  'SELECT * ',
  'FROM fluxosict flc ',
  'WHERE flc.Controle=' + Geral.FF0(QrFluxosItsControle.Value),
  'ORDER BY flc.Ordem, flc.Conta DESC ',  '']);
  //
end;

procedure TFmVSImpOSFluxo.ReopenMPP();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPPIts, Dmod.MyDB, [
  'SELECT pec.Nome NOMEUNIDADE,  ',
  'ag.Nome NOMEMP, flu.Nome NOMEFLUXO, mvi.*, ',
  'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu, ',
  'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,  ',
  'CONCAT(gg1.Nome, ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsa.Nome VSArtCab_TXT ',
  'FROM mpvits mvi ',
  'LEFT JOIN defpecas pec ON pec.Codigo=mvi.Unidade ',
  'LEFT JOIN artigosgrupos ag ON ag.Codigo=mvi.MP ',
  'LEFT JOIN fluxos flu ON flu.Codigo=mvi.Fluxo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=mvi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN formulas fo1 ON fo1.Numero=mvi.ReceiRecu ',
  'LEFT JOIN formulas fo2 ON fo2.Numero=mvi.ReceiRefu ',
  'LEFT JOIN tintascab ti1 ON ti1.Numero=mvi.ReceiAcab ',
  'LEFT JOIN vsartcab vsa ON vsa.Codigo=mvi.VSArtCab ',
  'WHERE mvi.Controle=' + Geral.FF0(QrVSPWECabMPVIts.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPP, Dmod.MyDB, [
  'SELECT ',
  'CASE WHEN ve.Tipo=0 THEN ve.RazaoSocial ',
  'ELSE ve.Nome END NOMEVENDEDOR, ',
  'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
  'ELSE cl.Nome END NOMECLIENTE, ',
  'CASE WHEN tr.Tipo=0 THEN tr.RazaoSocial ',
  'ELSE tr.Nome END NOMETRANSP, ',
  'pc.Nome NO_CONDICAOPG, ',
  'MPP.* ',
  'FROM mpp MPP ',
  'LEFT JOIN entidades cl ON cl.Codigo=MPP.Cliente ',
  'LEFT JOIN entidades ve ON ve.Codigo=MPP.Vendedor ',
  'LEFT JOIN entidades tr ON tr.Codigo=MPP.Transp ',
  'LEFT JOIN pediprzcab pc ON pc.Codigo=mpp.CondicaoPg ',
  'WHERE MPP.Codigo=' + Geral.FF0(QrMPPItsPedido.Value),
  '']);
  //
end;

end.
