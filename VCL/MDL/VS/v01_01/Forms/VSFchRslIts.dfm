object FmVSFchRslIts: TFmVSFchRslIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-059 :: Item de Resultado de Ficha'
  ClientHeight = 368
  ClientWidth = 469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 469
    Height = 142
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 469
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 2
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 29
      Height = 13
      Caption = 'Ficha:'
    end
    object Label2: TLabel
      Left = 156
      Top = 20
      Width = 48
      Height = 13
      Caption = 'Reduzido:'
    end
    object EdSerieFch: TdmkEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFicha: TdmkEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdGraGruX: TdmkEdit
      Left = 156
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNO_PRD_TAM_COR: TdmkEdit
      Left = 240
      Top = 36
      Width = 217
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 469
    Height = 142
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 0
    object GroupBox5: TGroupBox
      Left = 2
      Top = 15
      Width = 465
      Height = 62
      Align = alTop
      Caption = ' Pele In Natura: '
      TabOrder = 0
      object Panel10: TPanel
        Left = 2
        Top = 15
        Width = 461
        Height = 45
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 252
          Top = 4
          Width = 50
          Height = 13
          Caption = '$ M.O. kg:'
        end
        object Label22: TLabel
          Left = 320
          Top = 4
          Width = 51
          Height = 13
          Caption = '$ Frete kg:'
        end
        object Label14: TLabel
          Left = 176
          Top = 4
          Width = 24
          Height = 13
          Caption = '$ kg:'
        end
        object Label1: TLabel
          Left = 88
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label6: TLabel
          Left = 16
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label7: TLabel
          Left = 388
          Top = 4
          Width = 61
          Height = 13
          Caption = '$ Cr'#233'd. Trib.:'
        end
        object EdCustoMOKg: TdmkEdit
          Left = 252
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'CustoMOKg'
          UpdCampo = 'CustoMOKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCusFretKg: TdmkEdit
          Left = 320
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'CusFretKg'
          UpdCampo = 'CusFretKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCustoKg: TdmkEdit
          Left = 175
          Top = 20
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'CustoKg'
          UpdCampo = 'CustoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPesoKg: TdmkEdit
          Left = 87
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'PesoKg'
          UpdCampo = 'PesoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPecas: TdmkEdit
          Left = 15
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdImpostCred: TdmkEdit
          Left = 388
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ImpostCred'
          UpdCampo = 'ImpostCred'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 2
      Top = 77
      Width = 465
      Height = 63
      Align = alClient
      Caption = ' Artigo classificado: '
      TabOrder = 1
      object Panel13: TPanel
        Left = 2
        Top = 15
        Width = 461
        Height = 46
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 104
          Top = 4
          Width = 40
          Height = 13
          Caption = '$ Pre'#231'o:'
        end
        object Label24: TLabel
          Left = 192
          Top = 4
          Width = 50
          Height = 13
          Caption = '$ Frete m'#178':'
        end
        object Label25: TLabel
          Left = 280
          Top = 4
          Width = 59
          Height = 13
          Caption = '% Comiss'#227'o:'
        end
        object Label26: TLabel
          Left = 368
          Top = 4
          Width = 56
          Height = 13
          Caption = '% Impostos:'
        end
        object Label4: TLabel
          Left = 16
          Top = 4
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object EdPreco: TdmkEdit
          Left = 104
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Preco'
          UpdCampo = 'Preco'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdFreteM2: TdmkEdit
          Left = 192
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'FreteM2'
          UpdCampo = 'FreteM2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdComissP: TdmkEdit
          Left = 280
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'ComissP'
          UpdCampo = 'ComissP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdImpostP: TdmkEdit
          Left = 368
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'ImpostP'
          UpdCampo = 'ImpostP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdAreaM2: TdmkEdit
          Left = 16
          Top = 20
          Width = 84
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'AreaM2'
          UpdCampo = 'AreaM2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 469
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 421
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 373
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Item de Resultado de Ficha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Item de Resultado de Ficha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Item de Resultado de Ficha'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 254
    Width = 469
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 465
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 298
    Width = 469
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 323
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 321
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Qr_Sel_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 8
    Top = 12
    object Qr_Sel_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr_Sel_CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object Qr_Sel_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object Ds_Sel_: TDataSource
    DataSet = Qr_Sel_
    Left = 36
    Top = 12
  end
  object VU_Sel_: TdmkValUsu
    dmkEditCB = EdPreco
    QryCampo = '_Sel_'
    UpdCampo = '_Sel_'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 12
  end
end
