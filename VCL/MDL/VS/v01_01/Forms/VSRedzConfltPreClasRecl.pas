unit VSRedzConfltPreClasRecl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmVSRedzConfltPreClasRecl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    PnPesquisa: TPanel;
    BtTeste2: TBitBtn;
    CkIMERT: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSRedzConfltPreClasRecl: TFmVSRedzConfltPreClasRecl;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSRedzConfltPreClasRecl.BtOKClick(Sender: TObject);
var
  sControle, sGraGruX: String;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrRedzConfltPreClsRcl.First;
  while not DmModVS.QrRedzConfltPreClsRcl.Eof do
  begin
    sGraGruX  := Geral.FF0(DmModVS.QrRedzConfltPreClsRclGraGruX.Value);
    sControle := Geral.FF0(DmModVS.QrRedzConfltPreClsRclSrcCtrl.Value);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE vsmovits SET ' +
      ' GraGruX=' + sGraGruX + ', ' +
      ' SrcGGX=' + sGraGruX +
      ' WHERE Controle=' + sControle);
    //
    DmModVS.QrRedzConfltPreClsRcl.Next;
  end;
  DmModVS.QrRedzConfltPreClsRcl.Close;
  DmModVS.QrRedzConfltPreClsRcl.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRedzConfltPreClasRecl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRedzConfltPreClasRecl.BtTeste2Click(Sender: TObject);
const
  ForcaMostrarForm = False;
  SelfCall = True;
var
  TemIMEIMrt: Boolean;
begin
  TemIMEIMrt := CkIMERT.Checked;
  DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(ForcaMostrarForm,
    SelfCall, TemIMEiMrt, LaAviso1, LaAviso2);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foram encontrados ' +
  Geral.FF0(DmModVS.QrRedzConfltPreClsRcl.RecordCount) +
  ' IMEIS com pr� classe/reclasse em conflito!');
end;

procedure TFmVSRedzConfltPreClasRecl.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrRedzConfltPreClsRclControle.Value)
  else
  if Campo = 'SrcCtrl' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrRedzConfltPreClsRclSrcCtrl.Value);
end;

procedure TFmVSRedzConfltPreClasRecl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRedzConfltPreClasRecl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSRedzConfltPreClasRecl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
