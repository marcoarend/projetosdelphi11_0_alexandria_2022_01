object FmOpcoesVSSifDipoa: TFmOpcoesVSSifDipoa
  Left = 339
  Top = 185
  Caption = 'SIF-DIPOA-001 :: Op'#231#245'es SIF/DIPOA'
  ClientHeight = 474
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 237
        Height = 32
        Caption = 'Op'#231#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 237
        Height = 32
        Caption = 'Op'#231#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 237
        Height = 32
        Caption = 'Op'#231#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 360
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 404
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 312
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 185
      Top = 0
      Width = 627
      Height = 312
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 16
        Width = 254
        Height = 13
        Caption = 'N'#186' do registro no Minist'#233'rio da Agricultura SIF/DIPOA:'
      end
      object Label3: TLabel
        Left = 8
        Top = 44
        Width = 146
        Height = 13
        Caption = 'Dias de validade dos produtos:'
      end
      object Label4: TLabel
        Left = 8
        Top = 120
        Width = 135
        Height = 13
        Caption = 'Arquivo do logo da empresa:'
      end
      object SbArqLogoFilial: TSpeedButton
        Left = 588
        Top = 135
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbArqLogoFilialClick
      end
      object Label5: TLabel
        Left = 8
        Top = 160
        Width = 172
        Height = 13
        Caption = 'Arquivo do logo da inspe'#231#227'o do SIF:'
      end
      object SbArqLogoSIF: TSpeedButton
        Left = 588
        Top = 175
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbArqLogoSIFClick
      end
      object EdRegistro: TdmkEdit
        Left = 272
        Top = 12
        Width = 117
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdRegistroRedefinido
      end
      object EdDdValid: TdmkEdit
        Left = 272
        Top = 40
        Width = 117
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '30'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 30
        ValWarn = False
        OnRedefinido = EdDdValidRedefinido
      end
      object EdArqLogoFilial: TdmkEdit
        Left = 8
        Top = 136
        Width = 577
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdArqLogoFilialRedefinido
      end
      object EdArqLogoSIF: TdmkEdit
        Left = 8
        Top = 176
        Width = 577
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdArqLogoSIFRedefinido
      end
      object RGQualDtFab: TdmkRadioGroup
        Left = 8
        Top = 64
        Width = 601
        Height = 53
        Caption = ' Data da produ'#231#227'o para efeitos de validade: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Deixar em branco'
          'Primeira data dos IME-Is'
          #218'ltima data dos IME-Is'
          'Data do pallet (bag)')
        TabOrder = 4
        UpdType = utYes
        OldValor = 0
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 312
      Align = alLeft
      Caption = 'Panel5'
      TabOrder = 0
      object Label1: TLabel
        Left = 1
        Top = 1
        Width = 183
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Empresa'
        ExplicitWidth = 41
      end
      object DBGParamsEmp: TDBGrid
        Left = 1
        Top = 14
        Width = 183
        Height = 297
        Align = alClient
        DataSource = DsParamsEmp
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_EMP'
            Title.Caption = 'Nome'
            Width = 106
            Visible = True
          end>
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrVSSifDipoa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vssifdipoa')
    Left = 124
    Top = 140
    object QrVSSifDipoaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSSifDipoaRegistro: TWideStringField
      FieldName = 'Registro'
    end
    object QrVSSifDipoaDdValid: TIntegerField
      FieldName = 'DdValid'
      Required = True
    end
    object QrVSSifDipoaArqLogoFilial: TWideStringField
      FieldName = 'ArqLogoFilial'
      Size = 511
    end
    object QrVSSifDipoaArqLogoSIF: TWideStringField
      FieldName = 'ArqLogoSIF'
      Size = 511
    end
    object QrVSSifDipoaQualDtFab: TSmallintField
      FieldName = 'QualDtFab'
    end
  end
  object DsVSSifDipoa: TDataSource
    DataSet = QrVSSifDipoa
    Left = 124
    Top = 196
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrParamsEmpBeforeClose
    AfterScroll = QrParamsEmpAfterScroll
    SQL.Strings = (
      'SELECT emp.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP '
      'FROM paramsemp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo')
    Left = 36
    Top = 140
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrParamsEmpNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
  end
  object DsParamsEmp: TDataSource
    DataSet = QrParamsEmp
    Left = 36
    Top = 196
  end
end
