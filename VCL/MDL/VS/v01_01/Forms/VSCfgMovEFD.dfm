object FmVSCfgMovEFD: TFmVSCfgMovEFD
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-163 :: Confer'#234'ncia de Movimento de Couro'
  ClientHeight = 735
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 444
        Height = 32
        Caption = 'Confer'#234'ncia de Movimento de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 444
        Height = 32
        Caption = 'Confer'#234'ncia de Movimento de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 444
        Height = 32
        Caption = 'Confer'#234'ncia de Movimento de Couro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 562
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 562
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 562
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 174
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 10
          ExplicitTop = 253
          ExplicitWidth = 808
        end
        object DBGLista3: TdmkDBGridZTO
          Left = 2
          Top = 101
          Width = 1004
          Height = 73
          Align = alClient
          DataSource = DsLista3
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGLista3DrawColumnCell
          OnDblClick = DBGLista3DblClick
        end
        object dmkDBGridZTO2: TdmkDBGridZTO
          Left = 2
          Top = 179
          Width = 1004
          Height = 242
          Align = alBottom
          DataSource = DsFonte
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = dmkDBGridZTO2DblClick
        end
        object Panel5: TPanel
          Left = 2
          Top = 510
          Width = 1004
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object Label1: TLabel
            Left = 20
            Top = 8
            Width = 30
            Height = 13
            Caption = 'Pecas'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 160
            Top = 8
            Width = 37
            Height = 13
            Caption = 'AreaM2'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 300
            Top = 8
            Width = 37
            Height = 13
            Caption = 'PesoKg'
            FocusControl = DBEdit3
          end
          object DBEdit1: TDBEdit
            Left = 20
            Top = 24
            Width = 134
            Height = 21
            DataField = 'Pecas'
            DataSource = DsSoma
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 160
            Top = 24
            Width = 134
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsSoma
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 300
            Top = 24
            Width = 134
            Height = 21
            DataField = 'PesoKg'
            DataSource = DsSoma
            TabOrder = 2
          end
          object BtCorrige: TBitBtn
            Tag = 18
            Left = 440
            Top = 8
            Width = 120
            Height = 40
            Caption = '&Corrige'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtCorrigeClick
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 86
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 297
            Height = 86
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object LaAno: TLabel
              Left = 8
              Top = 4
              Width = 22
              Height = 13
              Caption = 'Ano:'
            end
            object LaMes: TLabel
              Left = 84
              Top = 4
              Width = 23
              Height = 13
              Caption = 'M'#234's:'
            end
            object CBAno: TComboBox
              Left = 8
              Top = 21
              Width = 73
              Height = 21
              Color = clWhite
              DropDownCount = 3
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Text = 'CBAno'
            end
            object CBMes: TComboBox
              Left = 84
              Top = 21
              Width = 133
              Height = 21
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Text = 'CBMes'
            end
            object EdMovimID: TdmkEditCB
              Left = 8
              Top = 61
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Procednc'
              UpdCampo = 'Procednc'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMovimID
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMovimID: TdmkDBLookupComboBox
              Left = 44
              Top = 61
              Width = 249
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMovimID
              TabOrder = 3
              dmkEditCB = EdMovimID
              QryCampo = 'Procednc'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CkIDMovimento: TCheckBox
              Left = 8
              Top = 44
              Width = 97
              Height = 17
              Caption = 'ID do Movimento'
              TabOrder = 4
            end
          end
          object CGPesquisas: TdmkCheckGroup
            Left = 425
            Top = 0
            Width = 511
            Height = 86
            Align = alClient
            Caption = ' Pesquisas: '
            Columns = 2
            Items.Strings = (
              '1. Saldos (demorado)'
              '2. Datas (demorado)'
              '3. Reduzidos linkados'
              '4. Baixas de semi/acabado'
              '5. Classes e reclasses'
              '6. Caleados sem baixa instant'#226'nea')
            TabOrder = 1
            UpdType = utYes
            Value = 0
            OldValor = 0
            ExplicitWidth = 388
          end
          object Panel8: TPanel
            Left = 297
            Top = 0
            Width = 128
            Height = 86
            Align = alLeft
            TabOrder = 2
            object BtTudo: TBitBtn
              Tag = 127
              Left = 4
              Top = 44
              Width = 120
              Height = 40
              Caption = '&Todos'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtTudoClick
            end
            object BtNenhum: TBitBtn
              Tag = 128
              Left = 4
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Nenhum'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtNenhumClick
            end
          end
          object RGFiltro: TRadioGroup
            Left = 936
            Top = 0
            Width = 68
            Height = 86
            Align = alRight
            Caption = 'Filtro'
            ItemIndex = 0
            Items.Strings = (
              'Todos'
              'Erros')
            TabOrder = 3
            OnClick = RGFiltroClick
          end
        end
        object MeAvisos: TMemo
          Left = 2
          Top = 421
          Width = 1004
          Height = 89
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'MeAvisos')
          ParentFont = False
          TabOrder = 4
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 610
    Width = 1008
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLista3: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLista3AfterScroll
    SQL.Strings = (
      'DROP TABLE IF EXISTS _Lista_GGX_1; '
      'CREATE TABLE _Lista_GGX_1 '
      ' '
      'SELECT SrcNivel2 Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND SrcNivel2 IN ( '
      '  SELECT Controle '
      '  FROM bluederm_colorado.vsmovits '
      '  WHERE MovimID=1 '
      ') '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND MovimID=1 '
      ' '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora < "2016-11-01" '
      'AND MovimID=1 '
      'AND SdoVrtPeca > 0 '
      ' '
      'ORDER BY Controle '
      ' '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_2; '
      'CREATE TABLE _Lista_GGX_2 '
      ' '
      'SELECT DISTINCT Controle'
      'FROM _Lista_GGX_1 '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_3; '
      'CREATE TABLE _Lista_GGX_3 '
      ' '
      'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas, '
      'vmi.AreaM2, vmi.PesoKg, vmi.SdoVrtPeca, '
      'vmi.SdoVrtArM2, vmi.SdoVrtPeso, vmi.Pecas BxaPeca, '
      'vmi.AreaM2 BxaAreaM2, vmi.PesoKg BxaPesoKg,'
      'vmi.Pecas DifPeca, vmi.AreaM2 DifAreaM2, '
      'vmi.PesoKg DifPesoKg'
      'FROM _Lista_GGX_2 lg2 '
      'LEFT JOIN bluederm_colorado.vsmovits vmi  '
      '  ON vmi.Controle=lg2.Controle '
      ';'
      ''
      'UPDATE _Lista_GGX_3 SET'
      '  BxaPeca        = 0,'
      '  BxaAreaM2      = 0,'
      '  BxaPesoKg      = 0,'
      '  DifPeca        = 0,'
      '  DifAreaM2      = 0,'
      '  DifPesoKg      = 0; '
      ' '
      'SELECT *'
      'FROM _Lista_GGX_3'
      '')
    Left = 180
    Top = 176
    object QrLista3MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrLista3MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrLista3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLista3MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrLista3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLista3DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrLista3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLista3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLista3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrLista3PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrLista3SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrLista3SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrLista3SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrLista3BxaPeca: TFloatField
      FieldName = 'BxaPeca'
    end
    object QrLista3BxaAreaM2: TFloatField
      FieldName = 'BxaAreaM2'
    end
    object QrLista3BxaPesoKg: TFloatField
      FieldName = 'BxaPesoKg'
    end
    object QrLista3DifPeca: TFloatField
      FieldName = 'DifPeca'
    end
    object QrLista3DifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrLista3DifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrLista3ErrData: TWideStringField
      FieldName = 'ErrData'
      Size = 3
    end
    object QrLista3ErrSdo0: TWideStringField
      FieldName = 'ErrSdo0'
      Size = 3
    end
    object QrLista3ErrGGXs: TWideStringField
      FieldName = 'ErrGGXs'
      Size = 3
    end
    object QrLista3ErrBxa: TWideStringField
      FieldName = 'ErrBxa'
      Size = 3
    end
    object QrLista3ErrClRc: TWideStringField
      FieldName = 'ErrClRc'
      Size = 3
    end
    object QrLista3ErrIMEI: TIntegerField
      FieldName = 'ErrIMEI'
    end
    object QrLista3QtdErros: TIntegerField
      FieldName = 'QtdErros'
    end
  end
  object DsLista3: TDataSource
    DataSet = QrLista3
    Left = 180
    Top = 224
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    Left = 176
    Top = 272
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSomaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSomaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrLista0: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _Lista_GGX_1; '
      'CREATE TABLE _Lista_GGX_1 '
      ' '
      'SELECT SrcNivel2 Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND SrcNivel2 IN ( '
      '  SELECT Controle '
      '  FROM bluederm_colorado.vsmovits '
      '  WHERE MovimID=1 '
      ') '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND MovimID=1 '
      ' '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora < "2016-11-01" '
      'AND MovimID=1 '
      'AND SdoVrtPeca > 0 '
      ' '
      'ORDER BY Controle '
      ' '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_2; '
      'CREATE TABLE _Lista_GGX_2 '
      ' '
      'SELECT DISTINCT Controle'
      'FROM _Lista_GGX_1 '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_3; '
      'CREATE TABLE _Lista_GGX_3 '
      ' '
      'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas, '
      'vmi.AreaM2, vmi.PesoKg, vmi.SdoVrtPeca, '
      'vmi.SdoVrtArM2, vmi.SdoVrtPeso, vmi.Pecas BxaPeca, '
      'vmi.AreaM2 BxaAreaM2, vmi.PesoKg BxaPesoKg,'
      'vmi.Pecas DifPeca, vmi.AreaM2 DifAreaM2, '
      'vmi.PesoKg DifPesoKg'
      'FROM _Lista_GGX_2 lg2 '
      'LEFT JOIN bluederm_colorado.vsmovits vmi  '
      '  ON vmi.Controle=lg2.Controle '
      ';'
      ''
      'UPDATE _Lista_GGX_3 SET'
      '  BxaPeca        = 0,'
      '  BxaAreaM2      = 0,'
      '  BxaPesoKg      = 0,'
      '  DifPeca        = 0,'
      '  DifAreaM2      = 0,'
      '  DifPesoKg      = 0; '
      ' '
      'SELECT *'
      'FROM _Lista_GGX_3'
      '')
    Left = 180
    Top = 124
    object QrLista0Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLista0DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrLista0GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLista0Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLista0AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrLista0PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrLista0SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrLista0SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrLista0SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrLista0BxaPeca: TFloatField
      FieldName = 'BxaPeca'
    end
    object QrLista0BxaAreaM2: TFloatField
      FieldName = 'BxaAreaM2'
    end
    object QrLista0BxaPesoKg: TFloatField
      FieldName = 'BxaPesoKg'
    end
    object QrLista0DifPeca: TFloatField
      FieldName = 'DifPeca'
    end
    object QrLista0DifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrLista0DifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrLista0MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrLista0MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
  end
  object QrFonte: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataHora, Controle, MovimID, Codigo,  '
      'MovimCod, GraGruX, Pecas, AreaM2, PesoKg,  '
      'IF(Pecas <> 0, PesoKg / Pecas, 0) KgPeca  '
      'FROM vsmovits vmi  '
      'WHERE vmi.SrcNivel2=3037 '
      'AND (vmi.Pecas + vmi.PesoKg < 0  '
      'OR vmi.MovimID IN (9,17,28)  '
      ')  '
      'UNION '
      'SELECT DataHora, Controle, MovimID, Codigo,  '
      'MovimCod, GraGruX, Pecas, AreaM2, PesoKg,  '
      'IF(Pecas <> 0, PesoKg / Pecas, 0) KgPeca  '
      'FROM vsmovitb vmi  '
      'WHERE vmi.SrcNivel2=3037 '
      'AND (vmi.Pecas + vmi.PesoKg < 0  '
      'OR vmi.MovimID IN (9,17,28)  '
      ')  ')
    Left = 388
    Top = 168
    object QrFonteMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrFonteMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrFonteDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrFonteControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFonteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFonteMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrFonteGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrFontePecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrFonteAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00;- '
    end
    object QrFontePesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000;- '
    end
    object QrFonteKgPeca: TFloatField
      FieldName = 'KgPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000;- '
    end
  end
  object DsFonte: TDataSource
    DataSet = QrFonte
    Left = 388
    Top = 212
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 176
    Top = 320
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 264
  end
  object QrMovimID: TMySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 388
    object QrMovimIDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMovimIDNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMovimID: TDataSource
    DataSet = QrMovimID
    Left = 216
    Top = 436
  end
  object QrGGXs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 636
    Top = 172
    object QrGGXsDstCtrl: TIntegerField
      FieldName = 'DstCtrl'
    end
    object QrGGXsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrGGXsSrcCtrl: TIntegerField
      FieldName = 'SrcCtrl'
    end
    object QrGGXsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
  end
  object QrListaPre: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _Lista_GGX_1; '
      'CREATE TABLE _Lista_GGX_1 '
      ' '
      'SELECT SrcNivel2 Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND SrcNivel2 IN ( '
      '  SELECT Controle '
      '  FROM bluederm_colorado.vsmovits '
      '  WHERE MovimID=1 '
      ') '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora BETWEEN "2016-11-01" AND "2016-11-30 23:59:59" '
      'AND MovimID=1 '
      ' '
      ' '
      'UNION '
      ' '
      'SELECT Controle '
      'FROM bluederm_colorado.vsmovits '
      'WHERE DataHora < "2016-11-01" '
      'AND MovimID=1 '
      'AND SdoVrtPeca > 0 '
      ' '
      'ORDER BY Controle '
      ' '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_2; '
      'CREATE TABLE _Lista_GGX_2 '
      ' '
      'SELECT DISTINCT Controle'
      'FROM _Lista_GGX_1 '
      '; '
      ' '
      'DROP TABLE IF EXISTS _Lista_GGX_3; '
      'CREATE TABLE _Lista_GGX_3 '
      ' '
      'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas, '
      'vmi.AreaM2, vmi.PesoKg, vmi.SdoVrtPeca, '
      'vmi.SdoVrtArM2, vmi.SdoVrtPeso, vmi.Pecas BxaPeca, '
      'vmi.AreaM2 BxaAreaM2, vmi.PesoKg BxaPesoKg,'
      'vmi.Pecas DifPeca, vmi.AreaM2 DifAreaM2, '
      'vmi.PesoKg DifPesoKg'
      'FROM _Lista_GGX_2 lg2 '
      'LEFT JOIN bluederm_colorado.vsmovits vmi  '
      '  ON vmi.Controle=lg2.Controle '
      ';'
      ''
      'UPDATE _Lista_GGX_3 SET'
      '  BxaPeca        = 0,'
      '  BxaAreaM2      = 0,'
      '  BxaPesoKg      = 0,'
      '  DifPeca        = 0,'
      '  DifAreaM2      = 0,'
      '  DifPesoKg      = 0; '
      ' '
      'SELECT *'
      'FROM _Lista_GGX_3'
      '')
    Left = 84
    Top = 196
    object QrListaPreMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrListaPreMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrListaPreCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaPreMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrListaPreControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaPreDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrListaPreGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrListaPrePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrListaPreAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrListaPrePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrListaPreSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrListaPreSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrListaPreSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrListaPreBxaPeca: TFloatField
      FieldName = 'BxaPeca'
    end
    object QrListaPreBxaAreaM2: TFloatField
      FieldName = 'BxaAreaM2'
    end
    object QrListaPreBxaPesoKg: TFloatField
      FieldName = 'BxaPesoKg'
    end
    object QrListaPreDifPeca: TFloatField
      FieldName = 'DifPeca'
    end
    object QrListaPreDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrListaPreDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrListaPreErrData: TWideStringField
      FieldName = 'ErrData'
      Size = 3
    end
    object QrListaPreErrSdo0: TWideStringField
      FieldName = 'ErrSdo0'
      Size = 3
    end
    object QrListaPreQtdErros: TIntegerField
      FieldName = 'QtdErros'
    end
  end
  object QrBaixas: TMySQLQuery
    Database = Dmod.MyDB
    Left = 708
    Top = 428
    object QrBaixasCtrlInn: TIntegerField
      FieldName = 'CtrlInn'
    end
    object QrBaixasCtrlBxa: TIntegerField
      FieldName = 'CtrlBxa'
    end
    object QrBaixasGGXInn: TIntegerField
      FieldName = 'GGXInn'
    end
    object QrBaixasGGXBxa: TIntegerField
      FieldName = 'GGXBxa'
    end
  end
  object QrClaRcl: TMySQLQuery
   
    SQL.Strings = (
      'DROP TABLE IF EXISTS _IMV_CLA_RCL_1'
      ';'
      'CREATE TABLE _IMV_CLA_RCL_1'
      'SELECT Codigo, Controle, MovimCod, '
      'MovimID, MovimNiv, MovimTwn, '
      'GraGruX, SrcGGX, DstGGX'
      'FROM bluederm_cialeather.vsmovits'
      'WHERE MovimID IN (7,8)'
      'AND MovimNiv=1'
      'AND DataHora BETWEEN "2018-11-01" AND "2018-11-30 23:59:59"'
      ';'
      'DROP TABLE IF EXISTS _IMV_CLA_RCL_2'
      ';'
      'CREATE TABLE _IMV_CLA_RCL_2'
      'SELECT Codigo, Controle, MovimCod, '
      'MovimID, MovimNiv, MovimTwn, '
      'GraGruX, SrcGGX, DstGGX'
      'FROM bluederm_cialeather.vsmovits'
      'WHERE MovimID IN (7,8)'
      'AND MovimNiv=2'
      'AND DataHora BETWEEN "2018-11-01" AND "2018-11-30 23:59:59"'
      ';'
      'SELECT vm2.Controle, vm2.GraGruX, '
      'vm2.SrcGGX, vm1.Controle Ctrl1, vm1.DstGGX '
      'FROM _IMV_CLA_RCL_1 vm1'
      'LEFT JOIN _IMV_CLA_RCL_2 vm2 ON vm1.MovimTwn=vm2.MovimTwn'
      'WHERE vm2.GraGruX<>vm1.DstGGX'
      'OR vm2.SrcGGX<>vm1.DstGGX'
      ';'
      '')
    Left = 436
    Top = 408
    object QrClaRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrClaRclGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrClaRclSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrClaRclCtrl1: TIntegerField
      FieldName = 'Ctrl1'
    end
    object QrClaRclDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
  end
  object DqErrCaleado: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 616
    Top = 292
  end
end
