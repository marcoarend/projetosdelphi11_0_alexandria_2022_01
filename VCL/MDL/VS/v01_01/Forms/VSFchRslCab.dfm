object FmVSFchRslCab: TFmVSFchRslCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-058 :: Resultado de Fichas'
  ClientHeight = 743
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 647
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 465
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 4
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label43: TLabel
          Left = 276
          Top = 4
          Width = 29
          Height = 13
          Caption = 'Ficha:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdSerieFch: TdmkEdit
          Left = 16
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SerieFch'
          UpdCampo = 'SerieFch'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNO_SerieFch: TdmkEdit
          Left = 72
          Top = 20
          Width = 200
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NO_SerieFch'
          UpdCampo = 'NO_SerieFch'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdFicha: TdmkEdit
          Left = 276
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Ficha'
          UpdCampo = 'Ficha'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object BtAtualiza: TBitBtn
          Tag = 18
          Left = 876
          Top = 6
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Atualiza'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAtualizaClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 2
        Top = 61
        Width = 1004
        Height = 116
        Align = alTop
        Caption = ' Mat'#233'ria-prima:'
        TabOrder = 1
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 99
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object LaPecas: TLabel
            Left = 16
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object LaPeso: TLabel
            Left = 104
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Peso:'
          end
          object Label14: TLabel
            Left = 16
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Custo unit'#225'rio:'
          end
          object Label15: TLabel
            Left = 104
            Top = 48
            Width = 53
            Height = 13
            Caption = 'Custo total:'
          end
          object Label40: TLabel
            Left = 368
            Top = 48
            Width = 50
            Height = 13
            Caption = 'Tributos $:'
            Enabled = False
          end
          object Label41: TLabel
            Left = 456
            Top = 48
            Width = 65
            Height = 13
            Caption = 'Custo l'#237'quido:'
          end
          object Label42: TLabel
            Left = 192
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Tributos MP:'
          end
          object Label44: TLabel
            Left = 280
            Top = 48
            Width = 61
            Height = 13
            Caption = 'Tributos MO:'
          end
          object EdPecas: TdmkEdit
            Left = 16
            Top = 20
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdPecasRedefinido
          end
          object EdPesoKg: TdmkEdit
            Left = 104
            Top = 20
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdPesoKgRedefinido
          end
          object EdCustoUni: TdmkEdit
            Left = 16
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'CustoUni'
            UpdCampo = 'CustoUni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdCustoUniEnter
            OnExit = EdCustoUniExit
          end
          object EdCustoAll: TdmkEdit
            Left = 104
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoAll'
            UpdCampo = 'CustoAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdCustoAllEnter
            OnExit = EdCustoAllExit
          end
          object EdTribtAll: TdmkEdit
            Left = 368
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'TribtAll'
            UpdCampo = 'TribtAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCustoLiq: TdmkEdit
            Left = 456
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoLiq'
            UpdCampo = 'CustoLiq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGTpCalcVal: TdmkRadioGroup
            Left = 192
            Top = 6
            Width = 445
            Height = 37
            Caption = ' Unidade de faturamento:'
            Columns = 6
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Pe'#231'as'
              'Peso kg'
              'Area m'#178
              'Area ft'#178
              'Total')
            TabOrder = 8
            TabStop = True
            OnClick = RGTpCalcValClick
            UpdType = utYes
            OldValor = 0
          end
          object EdTribtMP: TdmkEdit
            Left = 192
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'TribtMP'
            UpdCampo = 'TribtMP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdTribtMPRedefinido
          end
          object EdTribtMO: TdmkEdit
            Left = 280
            Top = 64
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'TribtMO'
            UpdCampo = 'TribtMO'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdTribtMORedefinido
          end
        end
      end
      object GBBaseItens: TGroupBox
        Left = 2
        Top = 177
        Width = 1004
        Height = 80
        Align = alTop
        Caption = ' Dados base para os itens de artigos classificados: '
        TabOrder = 2
        object GroupBox5: TGroupBox
          Left = 2
          Top = 15
          Width = 199
          Height = 63
          Align = alLeft
          Caption = ' Pele In Natura: '
          TabOrder = 0
          object Panel10: TPanel
            Left = 2
            Top = 15
            Width = 195
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label9: TLabel
              Left = 16
              Top = 4
              Width = 50
              Height = 13
              Caption = '$ M.O. kg:'
            end
            object Label22: TLabel
              Left = 104
              Top = 4
              Width = 51
              Height = 13
              Caption = '$ Frete kg:'
            end
            object EdCustoMOKg: TdmkEdit
              Left = 16
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPecasRedefinido
            end
            object EdCusFretKg: TdmkEdit
              Left = 104
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPesoKgRedefinido
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 201
          Top = 15
          Width = 801
          Height = 63
          Align = alClient
          Caption = ' Artigo classificado: '
          TabOrder = 1
          object Panel13: TPanel
            Left = 2
            Top = 15
            Width = 797
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label23: TLabel
              Left = 16
              Top = 4
              Width = 40
              Height = 13
              Caption = '$ Pre'#231'o:'
            end
            object Label24: TLabel
              Left = 104
              Top = 4
              Width = 50
              Height = 13
              Caption = '$ Frete m'#178':'
            end
            object Label25: TLabel
              Left = 192
              Top = 4
              Width = 58
              Height = 13
              Caption = '% comiss'#227'o:'
            end
            object Label26: TLabel
              Left = 280
              Top = 4
              Width = 56
              Height = 13
              Caption = '% Impostos:'
            end
            object EdPreco: TdmkEdit
              Left = 16
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPecasRedefinido
            end
            object EdFreteM2: TdmkEdit
              Left = 104
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPesoKgRedefinido
            end
            object EdComissP: TdmkEdit
              Left = 192
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPecasRedefinido
            end
            object EdImpostP: TdmkEdit
              Left = 280
              Top = 20
              Width = 84
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnRedefinido = EdPesoKgRedefinido
            end
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 584
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 647
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 583
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ratificar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object EdNotaMPAG: TdmkEdit
          Left = 308
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 57
      Width = 1008
      Height = 120
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object DBGIMEIs: TDBGrid
        Left = 0
        Top = 0
        Width = 1008
        Height = 120
        Align = alClient
        DataSource = DsMPrima
        PopupMenu = PMIMEIs
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 264
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatorNota'
            Title.Caption = 'Fator MPAG'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeca'
            Title.Caption = 'Sdo Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtPeso'
            Title.Caption = 'Sdo Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtArM2'
            Title.Caption = 'Sdo m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = #218'ltima data/hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SrcMovID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SrcNivel1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SrcNivel2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DstMovID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DstNivel1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DstNivel2'
            Visible = True
          end>
      end
    end
    object PnItens: TPanel
      Left = 0
      Top = 177
      Width = 945
      Height = 406
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object Panel8: TPanel
        Left = 0
        Top = 204
        Width = 945
        Height = 202
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox3: TGroupBox
          Left = 424
          Top = 45
          Width = 521
          Height = 157
          Align = alRight
          Caption = ' Artigos classificados gerados das fichas de entrada:'
          TabOrder = 0
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 517
            Height = 45
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label11: TLabel
              Left = 4
              Top = 4
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
              FocusControl = DBEdit10
            end
            object Label12: TLabel
              Left = 88
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Peso kg:'
              FocusControl = DBEdit11
            end
            object Label13: TLabel
              Left = 172
              Top = 4
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
              FocusControl = DBEdit12
            end
            object DBEdit10: TDBEdit
              Left = 4
              Top = 20
              Width = 80
              Height = 21
              DataField = 'Pecas'
              DataSource = DsSumArt
              TabOrder = 0
            end
            object DBEdit11: TDBEdit
              Left = 88
              Top = 20
              Width = 80
              Height = 21
              DataField = 'PesoKg'
              DataSource = DsSumArt
              TabOrder = 1
            end
            object DBEdit12: TDBEdit
              Left = 172
              Top = 20
              Width = 80
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsSumArt
              TabOrder = 2
            end
          end
          object DBGrid1: TDBGrid
            Left = 2
            Top = 60
            Width = 517
            Height = 95
            Align = alClient
            DataSource = DsArtigos
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Nome do artigo'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end>
          end
        end
        object DBGrid2: TDBGrid
          Left = 0
          Top = 45
          Width = 424
          Height = 157
          Align = alClient
          DataSource = DsVSFchRslIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome do artigo'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoKg'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOKg'
              Title.Caption = '$ M.O. kg'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Title.Caption = '$ M.O. total'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CusFretKg'
              Title.Caption = '$ Frete kg'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CusFretTot'
              Title.Caption = '$ Frete tot.'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoINTot'
              Title.Caption = '$ Compra'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = '$ Custo'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Preco'
              Title.Caption = 'Pre'#231'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FreteM2'
              Title.Caption = 'Frete m'#178
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FretM2Tot'
              Title.Caption = '$ Frete'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ComissP'
              Title.Caption = '% Comiss'#227'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ComissTotV'
              Title.Caption = '$ Comiss'#227'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ImpostP'
              Title.Caption = '% Impostos'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ImpostTotV'
              Title.Caption = '$ Impostos'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BrutoV'
              Title.Caption = '$ Bruto'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MargemV'
              Title.Caption = '$ Margem'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MargemP'
              Title.Caption = '% Margem'
              Width = 52
              Visible = True
            end>
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 945
          Height = 45
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label27: TLabel
            Left = 872
            Top = 4
            Width = 52
            Height = 13
            Caption = '% Margem:'
            FocusControl = DBEdit19
          end
          object Label28: TLabel
            Left = 8
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
            FocusControl = DBEdit20
          end
          object Label29: TLabel
            Left = 68
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            FocusControl = DBEdit21
          end
          object Label30: TLabel
            Left = 152
            Top = 4
            Width = 43
            Height = 13
            Caption = 'Peso Kg:'
            FocusControl = DBEdit22
          end
          object Label31: TLabel
            Left = 224
            Top = 4
            Width = 54
            Height = 13
            Caption = '$ M.O. Tot.'
            FocusControl = DBEdit23
          end
          object Label32: TLabel
            Left = 296
            Top = 4
            Width = 33
            Height = 13
            Caption = '$ frete:'
            FocusControl = DBEdit24
          end
          object Label33: TLabel
            Left = 368
            Top = 4
            Width = 49
            Height = 13
            Caption = '$ Entrada:'
            FocusControl = DBEdit25
          end
          object Label34: TLabel
            Left = 440
            Top = 4
            Width = 36
            Height = 13
            Caption = '$ Total:'
            FocusControl = DBEdit26
          end
          object Label35: TLabel
            Left = 512
            Top = 4
            Width = 48
            Height = 13
            Caption = '$ imposto:'
            FocusControl = DBEdit27
          end
          object Label36: TLabel
            Left = 584
            Top = 4
            Width = 57
            Height = 13
            Caption = '$ Comiss'#227'o:'
            FocusControl = DBEdit28
          end
          object Label37: TLabel
            Left = 656
            Top = 4
            Width = 50
            Height = 13
            Caption = '$ Frete m'#178':'
            FocusControl = DBEdit29
          end
          object Label38: TLabel
            Left = 728
            Top = 4
            Width = 37
            Height = 13
            Caption = '$ Bruto:'
            FocusControl = DBEdit30
          end
          object Label39: TLabel
            Left = 800
            Top = 4
            Width = 50
            Height = 13
            Caption = '$ Margem:'
            FocusControl = DBEdit31
          end
          object DBEdit19: TDBEdit
            Left = 872
            Top = 20
            Width = 52
            Height = 21
            DataField = 'MargemP'
            DataSource = DsVSFchRslSum
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'Pecas'
            DataSource = DsVSFchRslSum
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 68
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsVSFchRslSum
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 152
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PesoKg'
            DataSource = DsVSFchRslSum
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 224
            Top = 20
            Width = 68
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSFchRslSum
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 296
            Top = 20
            Width = 68
            Height = 21
            DataField = 'CusFretTot'
            DataSource = DsVSFchRslSum
            TabOrder = 5
          end
          object DBEdit25: TDBEdit
            Left = 368
            Top = 20
            Width = 68
            Height = 21
            DataField = 'CustoINTot'
            DataSource = DsVSFchRslSum
            TabOrder = 6
          end
          object DBEdit26: TDBEdit
            Left = 440
            Top = 20
            Width = 68
            Height = 21
            DataField = 'CustoTotal'
            DataSource = DsVSFchRslSum
            TabOrder = 7
          end
          object DBEdit27: TDBEdit
            Left = 512
            Top = 20
            Width = 68
            Height = 21
            DataField = 'ImpostTotV'
            DataSource = DsVSFchRslSum
            TabOrder = 8
          end
          object DBEdit28: TDBEdit
            Left = 584
            Top = 20
            Width = 68
            Height = 21
            DataField = 'ComissTotV'
            DataSource = DsVSFchRslSum
            TabOrder = 9
          end
          object DBEdit29: TDBEdit
            Left = 656
            Top = 20
            Width = 68
            Height = 21
            DataField = 'FretM2Tot'
            DataSource = DsVSFchRslSum
            TabOrder = 10
          end
          object DBEdit30: TDBEdit
            Left = 728
            Top = 20
            Width = 68
            Height = 21
            DataField = 'BrutoV'
            DataSource = DsVSFchRslSum
            TabOrder = 11
          end
          object DBEdit31: TDBEdit
            Left = 800
            Top = 20
            Width = 68
            Height = 21
            DataField = 'MargemV'
            DataSource = DsVSFchRslSum
            TabOrder = 12
          end
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 945
        Height = 204
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object DBGIDs: TDBGrid
          Left = 0
          Top = 0
          Width = 753
          Height = 204
          Align = alLeft
          DataSource = DsFichas
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Gerenciamento'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome do artigo'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end>
        end
        object Panel16: TPanel
          Left = 753
          Top = 0
          Width = 192
          Height = 204
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 192
            Height = 102
            Align = alClient
            Caption = 'Classifica'#231#227'o couro a couro: '
            TabOrder = 0
            object DGVSPaClaIts: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 188
              Height = 85
              Align = alClient
              DataSource = DsVSPaClaIts
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tecla'
                  Title.Caption = 'Box'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSPallet'
                  Title.Caption = 'Pallet'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VMI_Dest'
                  Title.Caption = 'IME-I Destino'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtHrIni'
                  Title.Caption = 'In'#237'cio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtHrFim_TXT'
                  Title.Caption = 'Final'
                  Visible = True
                end>
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 102
            Width = 192
            Height = 102
            Align = alBottom
            Caption = ' Classifica'#231#227'o massiva:'
            TabOrder = 1
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 188
              Height = 85
              Align = alClient
              DataSource = DsVSPaMulCab
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DataHora_TXT'
                  Title.Caption = 'Data / hora'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CacCod'
                  Title.Caption = 'OC'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaP2'
                  Title.Caption = #193'rea ft'#178
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VMI_Sorc'
                  Title.Caption = 'IME-I origem'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovimCod'
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GBDados: TGroupBox
        Left = 469
        Top = 0
        Width = 539
        Height = 57
        Align = alClient
        Caption = ' Fichas de entrada: '
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label2: TLabel
          Left = 144
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Ficha RMP:'
          FocusControl = DBEdit3
        end
        object Label3: TLabel
          Left = 480
          Top = 16
          Width = 59
          Height = 13
          Caption = 'Qtde IME-Is:'
          FocusControl = DBEdit4
        end
        object Label4: TLabel
          Left = 548
          Top = 16
          Width = 93
          Height = 13
          Caption = 'Primeira ocorr'#234'ncia:'
          FocusControl = DBEdit5
        end
        object Label5: TLabel
          Left = 664
          Top = 16
          Width = 85
          Height = 13
          Caption = #218'ltima ocorr'#234'ncia:'
          FocusControl = DBEdit6
        end
        object Label6: TLabel
          Left = 228
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit7
        end
        object Label8: TLabel
          Left = 312
          Top = 16
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
          FocusControl = DBEdit8
        end
        object Label10: TLabel
          Left = 396
          Top = 16
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit9
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 32
          Width = 32
          Height = 21
          DataField = 'SerieFch'
          DataSource = DsLote
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 40
          Top = 32
          Width = 100
          Height = 21
          DataField = 'NO_SERIEFCH'
          DataSource = DsLote
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 144
          Top = 32
          Width = 80
          Height = 21
          DataField = 'Ficha'
          DataSource = DsLote
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 480
          Top = 32
          Width = 64
          Height = 21
          DataField = 'ITENS'
          DataSource = DsLote
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 548
          Top = 32
          Width = 112
          Height = 21
          DataField = 'RefFirst'
          DataSource = DsLote
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 664
          Top = 32
          Width = 112
          Height = 21
          DataField = 'RefLast'
          DataSource = DsLote
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 228
          Top = 32
          Width = 80
          Height = 21
          DataField = 'Pecas'
          DataSource = DsSumMP
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 312
          Top = 32
          Width = 80
          Height = 21
          DataField = 'PesoKg'
          DataSource = DsSumMP
          TabOrder = 7
        end
        object DBEdit9: TDBEdit
          Left = 396
          Top = 32
          Width = 80
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsSumMP
          TabOrder = 8
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 469
        Height = 57
        Align = alLeft
        Caption = ' Lote ratificado: '
        TabOrder = 1
        object Label16: TLabel
          Left = 176
          Top = 16
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit13
        end
        object Label17: TLabel
          Left = 92
          Top = 16
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
          FocusControl = DBEdit14
        end
        object Label18: TLabel
          Left = 8
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit15
        end
        object Label19: TLabel
          Left = 260
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Forma pgto:'
          FocusControl = DBEdit16
        end
        object Label20: TLabel
          Left = 324
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Custo un.:'
          FocusControl = DBEdit17
        end
        object Label21: TLabel
          Left = 384
          Top = 16
          Width = 53
          Height = 13
          Caption = 'Custo total:'
          FocusControl = DBEdit18
        end
        object DBEdit13: TDBEdit
          Left = 176
          Top = 32
          Width = 80
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsVSFchRslCab
          TabOrder = 0
        end
        object DBEdit14: TDBEdit
          Left = 92
          Top = 32
          Width = 80
          Height = 21
          DataField = 'PesoKg'
          DataSource = DsVSFchRslCab
          TabOrder = 1
        end
        object DBEdit15: TDBEdit
          Left = 8
          Top = 32
          Width = 80
          Height = 21
          DataField = 'Pecas'
          DataSource = DsVSFchRslCab
          TabOrder = 2
        end
        object DBEdit16: TDBEdit
          Left = 260
          Top = 32
          Width = 60
          Height = 21
          DataField = 'NO_FrmPago'
          DataSource = DsVSFchRslCab
          TabOrder = 3
        end
        object DBEdit17: TDBEdit
          Left = 324
          Top = 32
          Width = 56
          Height = 21
          DataField = 'CustoUni'
          DataSource = DsVSFchRslCab
          TabOrder = 4
        end
        object DBEdit18: TDBEdit
          Left = 384
          Top = 32
          Width = 80
          Height = 21
          DataField = 'CustoAll'
          DataSource = DsVSFchRslCab
          TabOrder = 5
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 250
        Height = 32
        Caption = 'Resultado de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 250
        Height = 32
        Caption = 'Resultado de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 250
        Height = 32
        Caption = 'Resultado de Fichas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrLote: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLoteAfterOpen
    BeforeClose = QrLoteBeforeClose
    AfterScroll = QrLoteAfterScroll
    SQL.Strings = (
      'SELECT vmi.SerieFch, vmi.Ficha,   '
      '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   '
      'COUNT(vmi.Codigo) ITENS,   '
      'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  '
      'vsf.Nome NO_SERIEFCH   '
      'FROM vsmovits vmi  '
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=6440 '
      'GROUP BY vmi.SerieFch, vmi.Ficha')
    Left = 32
    Top = 173
    object QrLoteSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrLoteFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLoteFCH_SRE: TFloatField
      FieldName = 'FCH_SRE'
    end
    object QrLoteITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrLoteRefFirst: TDateTimeField
      FieldName = 'RefFirst'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrLoteRefLast: TDateTimeField
      FieldName = 'RefLast'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrLoteNO_SERIEFCH: TWideStringField
      FieldName = 'NO_SERIEFCH'
      Size = 60
    end
  end
  object DsLote: TDataSource
    DataSet = QrLote
    Left = 32
    Top = 221
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 656
    Top = 644
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 516
    Top = 636
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrIMEIs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.GraGruX, wmi.Pecas,'
      'wmi.PesoKg, wmi.AreaM2, wmi.AreaP2, wmi.ValorT,'
      'wmi.SdoVrtPeca, wmi.SdoVrtPeso, wmi.SdoVrtArM2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, wmi.DataHora,'
      'wmi.SrcNivel1, wmi.SrcNivel2,'
      'wmi.DstNivel1, wmi.DstNivel2'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 452
    Top = 172
    object QrIMEIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrIMEIsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrIMEIsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIMEIsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIMEIsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrIMEIsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIMEIsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrIMEIsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrIMEIsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEIsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEIsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEIsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrIMEIsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEIsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIsPallet: TIntegerField
      FieldName = 'Pallet'
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 452
    Top = 220
  end
  object PMIMEIs: TPopupMenu
    Left = 624
    Top = 344
    object Irparajaneladomovimento1: TMenuItem
      Caption = 'Ir para janela do movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object IrparajaneladedadosdoIMEI1: TMenuItem
      Caption = 'Ir para janela de dados do IME-I '
      OnClick = IrparajaneladedadosdoIMEI1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEIselecionado1: TMenuItem
      Caption = 'Exclui IME-I selecionado'
      Enabled = False
      Visible = False
      OnClick = ExcluiIMEIselecionado1Click
    end
    object AlteraIMEIorigemdoIMEISelecionado1: TMenuItem
      Caption = 'Altera IME-I origem do IME-I Selecionado'
      Enabled = False
      Visible = False
      OnClick = AlteraIMEIorigemdoIMEISelecionado1Click
    end
    object teste1: TMenuItem
      Caption = '&teste'
      OnClick = teste1Click
    end
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLotesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT SerieFch, Ficha '
      'FROM vsmovits'
      'WHERE Ficha <> 0'
      'ORDER BY Ficha, SerieFch')
    Left = 500
    Top = 376
    object QrLotesSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
  end
  object QrFichas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFichasBeforeClose
    AfterScroll = QrFichasAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet'
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'WHERE wmi.MovimCod IN'
      '  ('
      'SELECT DISTINCT(MovimCod) MovimCod'
      'FROM vsmovits'
      'WHERE SerieFch=1'
      'AND Ficha=6436'
      'AND MovimID=6'
      '  )'
      'AND wmi.MovimNiv=3'
      'ORDER BY NO_Pallet, wmi.Controle')
    Left = 232
    Top = 172
    object QrFichasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFichasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFichasMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrFichasMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrFichasMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrFichasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFichasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrFichasCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrFichasMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrFichasLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrFichasLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrFichasDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrFichasPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrFichasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFichasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrFichasPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrFichasAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrFichasAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrFichasValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrFichasSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrFichasSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrFichasSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrFichasSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrFichasSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrFichasSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrFichasSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrFichasObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrFichasSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrFichasFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrFichasMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrFichasFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrFichasCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrFichasCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrFichasValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrFichasDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrFichasDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrFichasDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrFichasDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrFichasQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrFichasQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrFichasQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrFichasQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrFichasQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrFichasQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrFichasQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrFichasQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrFichasAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrFichasNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrFichasMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrFichasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFichasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFichasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFichasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFichasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFichasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFichasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFichasTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrFichasNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrFichasNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFichasNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrFichasNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrFichasMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
  end
  object DsFichas: TDataSource
    DataSet = QrFichas
    Left = 232
    Top = 220
  end
  object QrVSPaClaIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsparclitsa'
      'WHERE Codigo=5'
      'ORDER BY DtHrIni')
    Left = 232
    Top = 316
    object QrVSPaClaItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPaClaItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaClaItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaClaItsVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPaClaItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaClaItsTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrVSPaClaItsDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaClaItsDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSPaClaItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaItsDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
    end
  end
  object DsVSPaClaIts: TDataSource
    DataSet = QrVSPaClaIts
    Left = 232
    Top = 365
  end
  object QrArtigos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrArtigosAfterScroll
    SQL.Strings = (
      
        'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2' +
        ', '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vspaclaitsa pri '
      'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo '
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pri.VMI_Dest '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pra.VSGerArt IN ( '
      '  SELECT DISTINCT wmi.Codigo '
      '  FROM vsmovits wmi '
      '  WHERE wmi.MovimCod IN '
      '    ( '
      '    SELECT DISTINCT(MovimCod) MovimCod '
      '    FROM vsmovits '
      '    WHERE SerieFch=2 '
      '    AND Ficha=6451 '
      '    AND MovimID=6 '
      '    ) '
      '  AND wmi.MovimNiv=3 '
      '  ) '
      'GROUP BY GraGruX '
      '')
    Left = 224
    Top = 544
    object QrArtigosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrArtigosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrArtigosAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrArtigosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsArtigos: TDataSource
    DataSet = QrArtigos
    Left = 224
    Top = 592
  end
  object QrMPrima: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMPrimaBeforeClose
    AfterScroll = QrMPrimaAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovits'
      'WHERE SerieFch=2'
      'AND Ficha=6440'
      'AND MovimID=1')
    Left = 336
    Top = 172
    object QrMPrimaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMPrimaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrMPrimaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMPrimaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrMPrimaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPrimaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPrimaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPrimaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrMPrimaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrMPrimaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMPrimaGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrMPrimaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrMPrimaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrMPrimaNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrMPrimaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrMPrimaSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrMPrimaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrMPrimaDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrMPrimaDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrMPrimaMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrMPrimaNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrMPrimaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrMPrimaDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrMPrimaPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrMPrimaFatorNota: TFloatField
      FieldName = 'FatorNota'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrMPrimaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrMPrimaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrMPrimaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrMPrimaMediaKg: TFloatField
      FieldName = 'MediaKg'
    end
  end
  object DsMPrima: TDataSource
    DataSet = QrMPrima
    Left = 336
    Top = 220
  end
  object QrSumMP: TmySQLQuery
    Database = Dmod.MyDB
    Left = 792
    Top = 292
    object QrSumMPPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumMPPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumMPAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumMP: TDataSource
    DataSet = QrSumMP
    Left = 792
    Top = 340
  end
  object QrSumArt: TmySQLQuery
    Database = Dmod.MyDB
    Left = 852
    Top = 292
    object FloatField1: TFloatField
      FieldName = 'Pecas'
    end
    object FloatField2: TFloatField
      FieldName = 'PesoKg'
    end
    object FloatField3: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumArt: TDataSource
    DataSet = QrSumArt
    Left = 852
    Top = 340
  end
  object QrVSFchRslCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSFchRslCabBeforeClose
    AfterScroll = QrVSFchRslCabAfterScroll
    SQL.Strings = (
      'SELECT rsl.*, ELT(FrmPago+1, "N'#227'o info", "Peso kg", '
      '"Pe'#231'a (Couro)", "? ? ? ? ?") NO_FrmPago '
      'FROM vsfchrslcab rsl'
      'WHERE rsl.SerieFch=2'
      'AND rsl.Ficha=6451')
    Left = 120
    Top = 173
    object QrVSFchRslCabSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSFchRslCabFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSFchRslCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSFchRslCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSFchRslCabFrmPago: TSmallintField
      FieldName = 'FrmPago'
    end
    object QrVSFchRslCabCustoUni: TFloatField
      FieldName = 'CustoUni'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslCabCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSFchRslCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSFchRslCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSFchRslCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSFchRslCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSFchRslCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSFchRslCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSFchRslCabNO_FrmPago: TWideStringField
      FieldName = 'NO_FrmPago'
      Size = 12
    end
    object QrVSFchRslCabNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSFchRslCabTribtAll: TFloatField
      FieldName = 'TribtAll'
    end
    object QrVSFchRslCabCustoLiq: TFloatField
      FieldName = 'CustoLiq'
    end
    object QrVSFchRslCabTribtMO: TFloatField
      FieldName = 'TribtMO'
    end
    object QrVSFchRslCabTribtMP: TFloatField
      FieldName = 'TribtMP'
    end
  end
  object DsVSFchRslCab: TDataSource
    DataSet = QrVSFchRslCab
    Left = 120
    Top = 217
  end
  object QrVSFchRslIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSFchRslItsCalcFields
    SQL.Strings = (
      'SELECT fsi.*, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR  '
      'FROM vsfchrslits fsi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=fsi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ')
    Left = 28
    Top = 313
    object QrVSFchRslItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSFchRslItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSFchRslItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSFchRslItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSFchRslItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSFchRslItsCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsCusFretKg: TFloatField
      FieldName = 'CusFretKg'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsImpostP: TFloatField
      FieldName = 'ImpostP'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsComissP: TFloatField
      FieldName = 'ComissP'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsFreteM2: TFloatField
      FieldName = 'FreteM2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSFchRslItsSrcMPAG: TFloatField
      FieldName = 'SrcMPAG'
    end
    object QrVSFchRslItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSFchRslItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSFchRslItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSFchRslItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSFchRslItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSFchRslItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSFchRslItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSFchRslItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSFchRslItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsCusFretTot: TFloatField
      FieldName = 'CusFretTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsImpostTotV: TFloatField
      FieldName = 'ImpostTotV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsComissTotV: TFloatField
      FieldName = 'ComissTotV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsFretM2Tot: TFloatField
      FieldName = 'FretM2Tot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsBrutoV: TFloatField
      FieldName = 'BrutoV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsMargemV: TFloatField
      FieldName = 'MargemV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsCustoINTot: TFloatField
      FieldName = 'CustoINTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsMargemP: TFloatField
      FieldName = 'MargemP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSFchRslItsPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      Calculated = True
    end
    object QrVSFchRslItsPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrVSFchRslItsImpostCred: TFloatField
      FieldName = 'ImpostCred'
    end
  end
  object DsVSFchRslIts: TDataSource
    DataSet = QrVSFchRslIts
    Left = 28
    Top = 361
  end
  object QrVSFchRslSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fsi.Pecas) Pecas, SUM(fsi.AreaM2) AreaM2, '
      'SUM(fsi.PesoKg) PesoKg, SUM(fsi.CustoMOTot) CustoMOTot,'
      'SUM(fsi.CusFretTot) CusFretTot, SUM(fsi.CustoINTot) CustoINTot,'
      'SUM(fsi.CustoTotal) CustoTotal, SUM(fsi.ImpostTotV) ImpostTotV,'
      'SUM(fsi.ComissTotV) ComissTotV, SUM(fsi.FretM2Tot) FretM2Tot,'
      'SUM(fsi.BrutoV) BrutoV, SUM(fsi.MargemV) MargemV,'
      '(SUM(fsi.MargemV) / SUM(CustoTotal)) * 100 MargemP'
      'FROM vsfchrslits fsi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=fsi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'WHERE fsi.SerieFch=2'
      'AND fsi.Ficha=6485')
    Left = 128
    Top = 561
    object QrVSFchRslSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSFchRslSumAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSFchRslSumCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumCusFretTot: TFloatField
      FieldName = 'CusFretTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumCustoINTot: TFloatField
      FieldName = 'CustoINTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumImpostTotV: TFloatField
      FieldName = 'ImpostTotV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumComissTotV: TFloatField
      FieldName = 'ComissTotV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumFretM2Tot: TFloatField
      FieldName = 'FretM2Tot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumBrutoV: TFloatField
      FieldName = 'BrutoV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumMargemV: TFloatField
      FieldName = 'MargemV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSFchRslSumMargemP: TFloatField
      FieldName = 'MargemP'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsVSFchRslSum: TDataSource
    DataSet = QrVSFchRslSum
    Left = 128
    Top = 609
  end
  object QrMul: TmySQLQuery
    Database = Dmod.MyDB
    Left = 576
    Top = 176
  end
  object QrVSInnSum: TmySQLQuery
    Database = Dmod.MyDB
    Left = 324
    Top = 328
    object QrVSInnSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSInnSumPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSInnSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSInnSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSInnSumValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrTribIncIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 320
    Top = 372
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrVSPaMulCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pma.*,'
      'IF(DataHora < "1900-01-01", "",'
      '  DATE_FORMAT(DataHora, "%d/%m/%y %h:%i:%s")) DataHora_TXT'
      'FROM vspamulcaba pma'
      'WHERE pma.VSGerArt=14')
    Left = 32
    Top = 557
    object QrVSPaMulCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaMulCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaMulCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaMulCabDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSPaMulCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSPaMulCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSPaMulCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPaMulCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPaMulCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaMulCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaMulCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaMulCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaMulCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaMulCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaMulCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaMulCabVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaMulCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaMulCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaMulCabDataHora_TXT: TWideStringField
      FieldName = 'DataHora_TXT'
      Size = 19
    end
  end
  object DsVSPaMulCab: TDataSource
    DataSet = QrVSPaMulCab
    Left = 32
    Top = 605
  end
  object frxWET_CURTI_058_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41979.425569537040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_058_001GetValue
    Left = 688
    Top = 212
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFichas
        DataSetName = 'frxDsFichas'
      end
      item
        DataSet = frxDsLote
        DataSetName = 'frxDsLote'
      end
      item
        DataSet = frxDsMPrima
        DataSetName = 'frxDsMPrima'
      end
      item
        DataSet = frxDsVSFchRslCab
        DataSetName = 'frxDsVSFchRslCab'
      end
      item
        DataSet = frxDsVSFchRslIts
        DataSetName = 'frxDsVSFchRslIts'
      end
      item
        DataSet = frxDsVSMovDif
        DataSetName = 'frxDsVSMovDif'
      end
      item
        DataSet = frxDsVSPaClaIts
        DataSetName = 'frxDsVSPaClaIts'
      end
      item
        DataSet = frxDsVSPaMulCab
        DataSetName = 'frxDsVSPaMulCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 45.354345350000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape3: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 835.276130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Resultado de Classifica'#231#227'o da Ficha RMP [frxDsLote."NO_SERIEFCH"' +
              '] [frxDsLote."Ficha"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 941.102970000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 612.283860000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsVSFchRslIts
        DataSetName = 'frxDsVSFchRslIts'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 30.236240000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 181.417440000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 487.559370000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 559.370440000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Preco'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."Preco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Left = 207.874150000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 253.228510000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'CustoKg'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CustoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          Left = 283.464750000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOKg'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CustoMOKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOTot'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CustoMOTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 359.055350000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'CusFretKg'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CusFretKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          Left = 393.071120000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'CusFretTot'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CusFretTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          Left = 438.425480000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'CustoINTot'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CustoINTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          Left = 918.425790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."CustoTotal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo133: TfrxMemoView
          Left = 589.606680000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'FreteM2'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."FreteM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          Left = 627.401980000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'FretM2Tot'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."FretM2Tot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 665.197280000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'ComissP'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."ComissP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          Left = 699.213050000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."ComissTotV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          Left = 793.701300000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'ImpostP'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."ImpostP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo142: TfrxMemoView
          Left = 820.158010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'ImpostTotV'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."ImpostTotV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          Left = 740.787880000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'BrutoV'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."BrutoV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 971.339210000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'MargemV'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."MargemV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          Left = 1020.473100000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'MargemP'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."MargemP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          Left = 532.913730000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'PercM2'
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 869.291900000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSFchRslIts."ImpostCred"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 755.906000000000000000
        Width = 1046.929810000000000000
        object Memo93: TfrxMemoView
          Width = 585.827150000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 551.811380000000000000
          Width = 495.118430000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 7.559050240000000000
        Top = 725.669760000000000000
        Width = 1046.929810000000000000
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 177.637910000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsMPrima
        DataSetName = 'frxDsMPrima'
        RowCount = 0
        object Memo16: TfrxMemoView
          Width = 26.456688030000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."GraGruX"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 26.456710000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPrima."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 102.047310000000000000
          Width = 37.795270710000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."NotaMPAG"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 139.842610000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."Pecas"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 170.078850000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."PesoKg"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 306.141930000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPrima."NO_FORNECE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 823.937540000000000000
          Width = 222.992177240000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMPrima."Observ"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 222.992270000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."ValorT"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 396.850650000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."InfPecas"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 427.086890000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."InfPesoKg"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 472.441250000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."InfValorT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 600.945270000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."PesoSalKg"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 525.354670000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."PerQbrViag"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 563.149970000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."PerQbrSal"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 638.740570000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."RstSalVl"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 684.094930000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."RstCouKg"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 721.890230000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."RstCouVl"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 771.024120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovDif."RstTotVl"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Left = 275.905690000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataField = 'MediaKg'
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."MediaKg"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 124.724490000000000000
        Width = 1046.929810000000000000
        object Memo17: TfrxMemoView
          Width = 26.456688030000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduz.')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 26.456710000000000000
          Width = 75.590551180000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria Prima')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 102.047310000000000000
          Top = 15.118120000000000000
          Width = 37.795270710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'N. MPAG')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 139.842610000000000000
          Top = 15.118120000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 170.078850000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 306.141930000000000000
          Top = 15.118120000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 823.937540000000000000
          Width = 222.992177240000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 222.992270000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor $')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 102.047310000000000000
          Width = 204.094546770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados de Entrada')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 306.141930000000000000
          Width = 219.212666770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados do Fornecedor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 396.850650000000000000
          Top = 15.118120000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 427.086890000000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 472.441250000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor $')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 600.945270000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Sal')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 525.354670000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%M'#225'xQV')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 563.149970000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%M'#225'xSal')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 638.740570000000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor sal')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 525.354670000000000000
          Width = 298.582796770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Restitui'#231#227'o do Fornecedor')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 684.094930000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg RQV')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 721.890230000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor RQV')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 771.024120000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total Rest.')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Left = 275.905690000000000000
          Top = 15.118120000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 215.433210000000000000
        Width = 1046.929810000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 18.897640240000000000
        Top = 570.709030000000000000
        Width = 1046.929810000000000000
        object MeTitNome: TfrxMemoView
          Left = 30.236240000000000000
          Top = 3.779530000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduz.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPecas: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 487.559370000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo116: TfrxMemoView
          Left = 559.370440000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo118: TfrxMemoView
          Left = 207.874150000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo121: TfrxMemoView
          Left = 253.228510000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo122: TfrxMemoView
          Left = 283.464750000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$MO/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo124: TfrxMemoView
          Left = 313.700990000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo127: TfrxMemoView
          Left = 359.055350000000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$Frete/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo128: TfrxMemoView
          Left = 393.071120000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Frete')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo131: TfrxMemoView
          Left = 438.425480000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo132: TfrxMemoView
          Left = 918.425790000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Custo tot.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo135: TfrxMemoView
          Left = 589.606680000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$Frete/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo136: TfrxMemoView
          Left = 627.401980000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Frete')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo139: TfrxMemoView
          Left = 665.197280000000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% comis.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo140: TfrxMemoView
          Left = 699.213050000000000000
          Top = 3.779530000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ comis.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo143: TfrxMemoView
          Left = 793.701300000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% trib.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo144: TfrxMemoView
          Left = 820.158010000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ trib. (-)')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo146: TfrxMemoView
          Left = 740.787880000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Bruto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Left = 971.339210000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Margem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo150: TfrxMemoView
          Left = 1020.473100000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Mrg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo152: TfrxMemoView
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo171: TfrxMemoView
          Left = 869.291900000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ trib. (+)')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 650.079160000000000000
        Width = 1046.929810000000000000
        object Memo3: TfrxMemoView
          Width = 181.417342360000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 181.417440000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."Pecas">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 487.559370000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."AreaM2">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 559.370440000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsVSFchRslIts."AreaM2">,MD003) > 0, SUM(<frxDsVSFch' +
              'RslIts."BrutoV">,MD003) / SUM(<frxDsVSFchRslIts."AreaM2">,MD003)' +
              ', 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          Left = 207.874150000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."PesoKg">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          Left = 253.228510000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          Left = 283.464750000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo156: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."CustoMOTot">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo157: TfrxMemoView
          Left = 359.055350000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo158: TfrxMemoView
          Left = 393.071120000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."CusFretTot">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo159: TfrxMemoView
          Left = 438.425480000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."CustoINTot">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo160: TfrxMemoView
          Left = 918.425790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."CustoTotal">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo161: TfrxMemoView
          Left = 589.606680000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.4n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo162: TfrxMemoView
          Left = 627.401980000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."FretM2Tot">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo163: TfrxMemoView
          Left = 665.197280000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo164: TfrxMemoView
          Left = 699.213050000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."ComissTotV">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo165: TfrxMemoView
          Left = 793.701300000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo166: TfrxMemoView
          Left = 820.158010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."ImpostTotV">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo167: TfrxMemoView
          Left = 740.787880000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."BrutoV">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo168: TfrxMemoView
          Left = 971.339210000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."MargemV">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo169: TfrxMemoView
          Left = 1020.473100000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsVSFchRslIts."BrutoV">,MD003) > 0, SUM(<frxDsVSFch' +
              'RslIts."MargemV">,MD003) / SUM(<frxDsVSFchRslIts."BrutoV">,MD003' +
              ') * 100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo170: TfrxMemoView
          Left = 532.913730000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."PercM2">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
              FormatStr = '%2.2n'
              Kind = fkNumeric
            end
            item
            end>
        end
        object Memo172: TfrxMemoView
          Left = 869.291900000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSFchRslIts
          DataSetName = 'frxDsVSFchRslIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSFchRslIts."ImpostCred">,MD003)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 253.228510000000000000
        Width = 1046.929810000000000000
        object Memo50: TfrxMemoView
          Left = 196.535560000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduz.')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 230.551330000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria Prima')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Gera'#231#227'o')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 321.260050000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 464.882190000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 517.795610000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor $')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 136.063080000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 676.535870000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor $')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 570.709030000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MO/kg')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 623.622450000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Left = 729.449290000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Left = 774.803650000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 812.598950000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        object Memo75: TfrxMemoView
          Width = 321.259991420000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total da Gera'#231#227'o de artigos ')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 321.260050000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."Pecas">,MD002)]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMPrima."PesoKg">,MD002)]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."AreaM2">,MD002)]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 464.882190000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."AreaP2">,MD002)]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 517.795610000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."ValorMP">,MD002)]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 676.535870000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."ValorT">,MD002)]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 570.709030000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."CustoMOKg">,MD002)]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 623.622450000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFichas."CustoMOTot">,MD002)]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 291.023810000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsFichas
        DataSetName = 'frxDsFichas'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 196.535560000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."GraGruX"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 230.551330000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFichas."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          DataField = 'Codigo'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."Codigo"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          DataField = 'MovimCod'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."MovimCod"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 321.260050000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."Pecas"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMPrima."PesoKg"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."AreaM2"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 464.882190000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."AreaP2"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 517.795610000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValorMP'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."ValorMP"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 136.063080000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFichas."DataHora"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 676.535870000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."ValorT"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 570.709030000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOKg'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."CustoMOKg"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 623.622450000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOTot'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."CustoMOTot"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."Controle"]')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Left = 729.449290000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsMPrima."Pecas"> = 0, 0,   <frxDsMPrima."PesoKg"> / <f' +
              'rxDsMPrima."Pecas">)]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Left = 774.803650000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."MediaM2"]')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Left = 812.598950000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsMPrima
          DataSetName = 'frxDsMPrima'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsFichas."AreaM2"> = 0, 0,   <frxDsMPrima."PesoKg"> / <' +
              'frxDsFichas."AreaM2">)]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 374.173470000000000000
        Width = 1046.929810000000000000
        object Memo86: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Gera'#231#227'o')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Box')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 188.976500000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 136.063080000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I dest.')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 249.448980000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Final')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        Top = 449.764070000000000000
        Width = 1046.929810000000000000
      end
      object DT002_001: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 411.968770000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsVSPaClaIts
        DataSetName = 'frxDsVSPaClaIts'
        RowCount = 0
        object Memo90: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsFichas
          DataSetName = 'frxDsFichas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFichas."GraGruX"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSPaClaIts
          DataSetName = 'frxDsVSPaClaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaClaIts."Controle"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Tecla'
          DataSet = frxDsVSPaClaIts
          DataSetName = 'frxDsVSPaClaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaClaIts."Tecla"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 188.976500000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DtHrIni'
          DataSet = frxDsVSPaClaIts
          DataSetName = 'frxDsVSPaClaIts'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSPaClaIts."DtHrIni"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 136.063080000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'VMI_Dest'
          DataSet = frxDsVSPaClaIts
          DataSetName = 'frxDsVSPaClaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaClaIts."VMI_Dest"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 249.448980000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrFim_TXT'
          DataSet = frxDsVSPaClaIts
          DataSetName = 'frxDsVSPaClaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSPaClaIts."DtHrFim_TXT"]')
          ParentFont = False
        end
      end
      object Header5: TfrxHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 472.441250000000000000
        Width = 1046.929810000000000000
        object Memo101: TfrxMemoView
          Left = 49.133890000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 79.370130000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 132.283550000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OC')
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Left = 185.196970000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 234.330860000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
        end
      end
      object DT002_002: TfrxDetailData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 510.236550000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsVSPaMulCab
        DataSetName = 'frxDsVSPaMulCab'
        RowCount = 0
        object Memo105: TfrxMemoView
          Left = 49.133890000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."Pecas"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 79.370130000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."AreaM2"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 132.283550000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."AreaP2"]')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'CacCod'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."CacCod"]')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 185.196970000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."Codigo"]')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 234.330860000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'MovimCod'
          DataSet = frxDsVSPaMulCab
          DataSetName = 'frxDsVSPaMulCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPaMulCab."MovimCod"]')
          ParentFont = False
        end
      end
      object Footer5: TfrxFooter
        FillType = ftBrush
        Top = 548.031850000000000000
        Width = 1046.929810000000000000
      end
    end
  end
  object frxDsVSFchRslCab: TfrxDBDataset
    UserName = 'frxDsVSFchRslCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'FrmPago=FrmPago'
      'CustoUni=CustoUni'
      'CustoAll=CustoAll'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_FrmPago=NO_FrmPago')
    DataSet = QrVSFchRslCab
    BCDToCurrency = False
    Left = 116
    Top = 264
  end
  object frxDsMPrima: TfrxDBDataset
    UserName = 'frxDsMPrima'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DataHora=DataHora'
      'NO_MovimID=NO_MovimID'
      'MovimID=MovimID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'MovimNiv=MovimNiv'
      'NO_MovimNiv=NO_MovimNiv'
      'SrcMovID=SrcMovID'
      'DstMovID=DstMovID'
      'Pallet=Pallet'
      'FatorNota=FatorNota'
      'NotaMPAG=NotaMPAG'
      'NO_FORNECE=NO_FORNECE'
      'Observ=Observ'
      'MediaKg=MediaKg')
    DataSet = QrMPrima
    BCDToCurrency = False
    Left = 336
    Top = 268
  end
  object frxDsVSFchRslIts: TfrxDBDataset
    UserName = 'frxDsVSFchRslIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'CustoKg=CustoKg'
      'CustoMOKg=CustoMOKg'
      'CusFretKg=CusFretKg'
      'Preco=Preco'
      'ImpostP=ImpostP'
      'ComissP=ComissP'
      'FreteM2=FreteM2'
      'SrcMPAG=SrcMPAG'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'CustoMOTot=CustoMOTot'
      'CusFretTot=CusFretTot'
      'ImpostTotV=ImpostTotV'
      'ComissTotV=ComissTotV'
      'FretM2Tot=FretM2Tot'
      'BrutoV=BrutoV'
      'MargemV=MargemV'
      'CustoINTot=CustoINTot'
      'CustoTotal=CustoTotal'
      'MargemP=MargemP'
      'PercM2=PercM2'
      'PercPc=PercPc'
      'ImpostCred=ImpostCred')
    DataSet = QrVSFchRslIts
    BCDToCurrency = False
    Left = 28
    Top = 408
  end
  object frxDsLote: TfrxDBDataset
    UserName = 'frxDsLote'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'FCH_SRE=FCH_SRE'
      'ITENS=ITENS'
      'RefFirst=RefFirst'
      'RefLast=RefLast'
      'NO_SERIEFCH=NO_SERIEFCH')
    DataSet = QrLote
    BCDToCurrency = False
    Left = 28
    Top = 268
  end
  object QrVSMovDif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovdif'
      'WHERE Controle=:P0')
    Left = 324
    Top = 540
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovDifControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovDifInfPecas: TFloatField
      FieldName = 'InfPecas'
    end
    object QrVSMovDifInfPesoKg: TFloatField
      FieldName = 'InfPesoKg'
    end
    object QrVSMovDifInfAreaM2: TFloatField
      FieldName = 'InfAreaM2'
    end
    object QrVSMovDifInfAreaP2: TFloatField
      FieldName = 'InfAreaP2'
    end
    object QrVSMovDifInfValorT: TFloatField
      FieldName = 'InfValorT'
    end
    object QrVSMovDifDifPecas: TFloatField
      FieldName = 'DifPecas'
    end
    object QrVSMovDifDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrVSMovDifDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrVSMovDifDifAreaP2: TFloatField
      FieldName = 'DifAreaP2'
    end
    object QrVSMovDifDifValorT: TFloatField
      FieldName = 'DifValorT'
    end
    object QrVSMovDifPerQbrViag: TFloatField
      FieldName = 'PerQbrViag'
    end
    object QrVSMovDifPerQbrSal: TFloatField
      FieldName = 'PerQbrSal'
    end
    object QrVSMovDifPesoSalKg: TFloatField
      FieldName = 'PesoSalKg'
    end
    object QrVSMovDifRstCouPc: TFloatField
      FieldName = 'RstCouPc'
    end
    object QrVSMovDifRstCouKg: TFloatField
      FieldName = 'RstCouKg'
    end
    object QrVSMovDifRstCouVl: TFloatField
      FieldName = 'RstCouVl'
    end
    object QrVSMovDifRstSalKg: TFloatField
      FieldName = 'RstSalKg'
    end
    object QrVSMovDifRstSalVl: TFloatField
      FieldName = 'RstSalVl'
    end
    object QrVSMovDifRstTotVl: TFloatField
      FieldName = 'RstTotVl'
    end
    object QrVSMovDifTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
  end
  object DsVSMovDif: TDataSource
    DataSet = QrVSMovDif
    Left = 324
    Top = 588
  end
  object frxDsVSMovDif: TfrxDBDataset
    UserName = 'frxDsVSMovDif'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'InfPecas=InfPecas'
      'InfPesoKg=InfPesoKg'
      'InfAreaM2=InfAreaM2'
      'InfAreaP2=InfAreaP2'
      'InfValorT=InfValorT'
      'DifPecas=DifPecas'
      'DifPesoKg=DifPesoKg'
      'DifAreaM2=DifAreaM2'
      'DifAreaP2=DifAreaP2'
      'DifValorT=DifValorT'
      'PerQbrViag=PerQbrViag'
      'PerQbrSal=PerQbrSal'
      'PesoSalKg=PesoSalKg'
      'RstCouPc=RstCouPc'
      'RstCouKg=RstCouKg'
      'RstCouVl=RstCouVl'
      'RstSalKg=RstSalKg'
      'RstSalVl=RstSalVl'
      'RstTotVl=RstTotVl'
      'TribDefSel=TribDefSel')
    DataSet = QrVSMovDif
    BCDToCurrency = False
    Left = 324
    Top = 636
  end
  object frxDsFichas: TfrxDBDataset
    UserName = 'frxDsFichas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'TpCalcAuto=TpCalcAuto'
      'NO_SerieFch=NO_SerieFch'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'NO_FORNECE=NO_FORNECE'
      'MediaM2=MediaM2')
    DataSet = QrFichas
    BCDToCurrency = False
    Left = 232
    Top = 268
  end
  object frxDsVSPaClaIts: TfrxDBDataset
    UserName = 'frxDsVSPaClaIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'VSPallet=VSPallet'
      'VMI_Sorc=VMI_Sorc'
      'VMI_Baix=VMI_Baix'
      'VMI_Dest=VMI_Dest'
      'Tecla=Tecla'
      'DtHrIni=DtHrIni'
      'DtHrFim=DtHrFim'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'DtHrFim_TXT=DtHrFim_TXT')
    DataSet = QrVSPaClaIts
    BCDToCurrency = False
    Left = 232
    Top = 412
  end
  object frxDsVSPaMulCab: TfrxDBDataset
    UserName = 'frxDsVSPaMulCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DataHora=DataHora'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'VMI_Sorc=VMI_Sorc'
      'CacCod=CacCod'
      'VSGerArt=VSGerArt'
      'DataHora_TXT=DataHora_TXT')
    DataSet = QrVSPaMulCab
    BCDToCurrency = False
    Left = 32
    Top = 652
  end
end
