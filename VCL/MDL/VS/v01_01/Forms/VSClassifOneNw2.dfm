object FmVSClassifOneNw2: TFmVSClassifOneNw2
  Left = 0
  Top = 0
  Caption = 
    'WET-CURTI-015 :: Classifica'#231#227'o de Artigo de Ribeira Couro a Cour' +
    'o'
  ClientHeight = 807
  ClientWidth = 1904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PanelOC: TPanel
    Left = 0
    Top = 706
    Width = 1904
    Height = 101
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 1471
    object Panel42: TPanel
      Left = 0
      Top = 0
      Width = 1436
      Height = 101
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      ExplicitWidth = 1003
      object Label3: TLabel
        Left = 4
        Top = 8
        Width = 30
        Height = 21
        Caption = 'OC:'
        FocusControl = DBEdCodigo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 172
        Top = 8
        Width = 71
        Height = 21
        Caption = 'Empresa:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 520
        Top = 8
        Width = 41
        Height = 21
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label13: TLabel
        Left = 612
        Top = 8
        Width = 47
        Height = 21
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 792
        Top = 8
        Width = 126
        Height = 21
        Caption = 'Artigo de ribeira:'
      end
      object Label16: TLabel
        Left = 4
        Top = 40
        Width = 49
        Height = 21
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 192
        Top = 40
        Width = 111
        Height = 21
        Caption = 'Peso (origem):'
      end
      object Label20: TLabel
        Left = 444
        Top = 40
        Width = 68
        Height = 21
        Caption = #193'rea m'#178':'
      end
      object Label21: TLabel
        Left = 680
        Top = 40
        Width = 65
        Height = 21
        Caption = #193'rea ft'#178':'
      end
      object Label26: TLabel
        Left = 920
        Top = 40
        Width = 83
        Height = 21
        Caption = 'Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label24: TLabel
        Left = 1216
        Top = 40
        Width = 89
        Height = 21
        Caption = 'Fornecedor:'
        FocusControl = DBEdit16
      end
      object Label18: TLabel
        Left = 4
        Top = 72
        Width = 257
        Height = 21
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 44
        Top = 4
        Width = 120
        Height = 28
        TabStop = False
        DataField = 'CacCod'
        DataSource = DsVSPaClaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit19: TDBEdit
        Left = 252
        Top = 4
        Width = 60
        Height = 29
        DataField = 'Empresa'
        DataSource = DsVSPaClaCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 312
        Top = 4
        Width = 205
        Height = 29
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPaClaCab
        TabOrder = 2
      end
      object DBEdit14: TDBEdit
        Left = 568
        Top = 4
        Width = 40
        Height = 29
        DataField = 'NO_TIPO'
        DataSource = DsVSPaClaCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 668
        Top = 4
        Width = 120
        Height = 29
        DataField = 'Controle'
        DataSource = DsVSGerArtNew
        PopupMenu = PMIMEI
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 940
        Top = 4
        Width = 60
        Height = 29
        DataField = 'GraGruX'
        DataSource = DsVSGerArtNew
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 1000
        Top = 4
        Width = 429
        Height = 29
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSGerArtNew
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 64
        Top = 36
        Width = 120
        Height = 29
        DataField = 'Pecas'
        DataSource = DsVSGerArtNew
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 320
        Top = 36
        Width = 120
        Height = 29
        DataField = 'QtdAntPeso'
        DataSource = DsVSGerArtNew
        TabOrder = 8
      end
      object DBEdit12: TDBEdit
        Left = 524
        Top = 36
        Width = 150
        Height = 29
        DataField = 'AreaM2'
        DataSource = DsVSGerArtNew
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 760
        Top = 36
        Width = 150
        Height = 29
        DataField = 'AreaP2'
        DataSource = DsVSGerArtNew
        TabOrder = 10
      end
      object DBEdit18: TDBEdit
        Left = 1020
        Top = 36
        Width = 190
        Height = 29
        DataField = 'NO_FICHA'
        DataSource = DsVSGerArtNew
        TabOrder = 11
      end
      object DBEdit16: TDBEdit
        Left = 1324
        Top = 36
        Width = 105
        Height = 29
        DataField = 'Terceiro'
        DataSource = DsVSGerArtNew
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 300
        Top = 68
        Width = 1129
        Height = 29
        DataField = 'Observ'
        DataSource = DsVSGerArtNew
        TabOrder = 13
      end
    end
    object Panel43: TPanel
      Left = 1436
      Top = 0
      Width = 468
      Height = 101
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      ExplicitLeft = 1003
      object Label71: TLabel
        Left = 4
        Top = 8
        Width = 74
        Height = 21
        Caption = 'Digitador:'
      end
      object Label70: TLabel
        Left = 4
        Top = 40
        Width = 99
        Height = 21
        Caption = 'Classificador:'
      end
      object Label85: TLabel
        Left = 292
        Top = 72
        Width = 57
        Height = 21
        Caption = 'Tempo:'
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 180
        Top = 36
        Width = 285
        Height = 29
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        TabOrder = 0
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 180
        Top = 4
        Width = 285
        Height = 29
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        TabOrder = 1
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 4
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 36
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdTempo: TEdit
        Left = 368
        Top = 68
        Width = 96
        Height = 29
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
      end
    end
  end
  object PnBoxesAll: TPanel
    Left = 233
    Top = 97
    Width = 1351
    Height = 609
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 918
    object PnBoxesT01: TPanel
      Left = 0
      Top = 0
      Width = 1351
      Height = 422
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 918
      object PnBoxT1L1: TPanel
        Left = 0
        Top = 0
        Width = 477
        Height = 422
        Align = alLeft
        TabOrder = 0
        object PnBox01: TPanel
          Left = 1
          Top = 1
          Width = 475
          Height = 420
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 328
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel6: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label4: TLabel
                  Left = 4
                  Top = 4
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                  FocusControl = DBEdControle01
                end
                object Label54: TLabel
                  Left = 6
                  Top = 32
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                  FocusControl = DBEdControle01
                end
                object DBEdControle01: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI01: TDBEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 248
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel16: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 223
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label52: TLabel
                  Left = 4
                  Top = 34
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label55: TLabel
                  Left = 4
                  Top = 8
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label53: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label73: TLabel
                  Left = 4
                  Top = 87
                  Width = 80
                  Height = 21
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object EdMedia01: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPecas01: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea01: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet01: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 475
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel8: TPanel
              Left = 41
              Top = 0
              Width = 312
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel11: TPanel
                Left = 0
                Top = 29
                Width = 312
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit3: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 212
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit8: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit1: TDBEdit
                Left = 0
                Top = 0
                Width = 312
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel50: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label79: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel51: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '1'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel64: TPanel
              Left = 353
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass1: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass1Click
              end
            end
          end
          object PnIntMei01: TPanel
            Left = 0
            Top = 392
            Width = 475
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
          end
          object SGItens01: TStringGrid
            Left = 298
            Top = 64
            Width = 177
            Height = 328
            Align = alClient
            TabOrder = 3
          end
        end
      end
      object PnBoxT1L2: TPanel
        Left = 477
        Top = 0
        Width = 463
        Height = 422
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 30
        object PnBox02: TPanel
          Left = 1
          Top = 1
          Width = 461
          Height = 420
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 28
          object Panel19: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 328
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel26: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label5: TLabel
                  Left = 4
                  Top = 4
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                end
                object Label30: TLabel
                  Left = 6
                  Top = 32
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                end
                object DBEdControle02: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI02: TDBEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 248
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel30: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 223
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label31: TLabel
                  Left = 4
                  Top = 34
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label56: TLabel
                  Left = 4
                  Top = 4
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label57: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label74: TLabel
                  Left = 4
                  Top = 87
                  Width = 80
                  Height = 21
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object EdPecas02: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea02: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia02: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet02: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 461
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 28
            object Panel18: TPanel
              Left = 41
              Top = 0
              Width = 298
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel44: TPanel
                Left = 0
                Top = 29
                Width = 298
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit25: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 198
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit26: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit22: TDBEdit
                Left = 0
                Top = 0
                Width = 298
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel45: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label80: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel46: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '2'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel62: TPanel
              Left = 339
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitLeft = -94
              object CkSubClass2: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass2Click
              end
              object EdItens: TdmkEdit
                Left = 9
                Top = 32
                Width = 109
                Height = 29
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'EdItens'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'EdItens'
                ValWarn = False
              end
            end
          end
          object PnIntMei02: TPanel
            Left = 0
            Top = 392
            Width = 461
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
            ExplicitWidth = 28
          end
          object SGItens02: TStringGrid
            Left = 298
            Top = 64
            Width = 163
            Height = 328
            Align = alClient
            TabOrder = 3
          end
        end
      end
      object PnBoxT1L3: TPanel
        Left = 940
        Top = 0
        Width = 411
        Height = 422
        Align = alRight
        TabOrder = 2
        ExplicitLeft = 507
        object PnBox03: TPanel
          Left = 1
          Top = 1
          Width = 409
          Height = 420
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel22: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 328
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel34: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label32: TLabel
                  Left = 4
                  Top = 0
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                end
                object Label36: TLabel
                  Left = 6
                  Top = 28
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                end
                object DBEdControle03: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI03: TDBEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 248
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel35: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 223
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label58: TLabel
                  Left = 4
                  Top = 32
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label59: TLabel
                  Left = 4
                  Top = 4
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label60: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label75: TLabel
                  Left = 4
                  Top = 87
                  Width = 79
                  Height = 21
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object EdPecas03: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea03: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia03: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet03: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 409
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel21: TPanel
              Left = 41
              Top = 0
              Width = 246
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel55: TPanel
                Left = 0
                Top = 29
                Width = 246
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit31: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 146
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit32: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit30: TDBEdit
                Left = 0
                Top = 0
                Width = 246
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel56: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label83: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel57: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '3'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel65: TPanel
              Left = 287
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass3: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass3Click
              end
            end
          end
          object PnIntMei03: TPanel
            Left = 0
            Top = 392
            Width = 409
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
          end
          object SGItens03: TStringGrid
            Left = 298
            Top = 64
            Width = 111
            Height = 328
            Align = alClient
            TabOrder = 3
          end
        end
      end
    end
    object PnBoxesT02: TPanel
      Left = 0
      Top = 422
      Width = 1351
      Height = 187
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 918
      object PnBoxT2L3: TPanel
        Left = 936
        Top = 0
        Width = 415
        Height = 187
        Align = alRight
        TabOrder = 2
        ExplicitLeft = 503
        object PnBox06: TPanel
          Left = 1
          Top = 1
          Width = 413
          Height = 185
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel25: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 93
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox11: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel40: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label37: TLabel
                  Left = 4
                  Top = 4
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                end
                object Label41: TLabel
                  Left = 6
                  Top = 32
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                end
                object DBEdControle06: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI06: TDBEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox12: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 13
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel41: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 224
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label67: TLabel
                  Left = 4
                  Top = 32
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label68: TLabel
                  Left = 4
                  Top = 4
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label69: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label78: TLabel
                  Left = 4
                  Top = 87
                  Width = 79
                  Height = 21
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object EdPecas06: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea06: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia06: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet06: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 413
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel24: TPanel
              Left = 41
              Top = 0
              Width = 250
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel58: TPanel
                Left = 0
                Top = 29
                Width = 250
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit38: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 150
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit39: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit37: TDBEdit
                Left = 0
                Top = 0
                Width = 250
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel59: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label84: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel60: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '6'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel66: TPanel
              Left = 291
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass6: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object PnIntMei06: TPanel
            Left = 0
            Top = 157
            Width = 413
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
          end
          object SGItens06: TStringGrid
            Left = 298
            Top = 64
            Width = 115
            Height = 93
            Align = alClient
            TabOrder = 3
          end
        end
      end
      object PnBoxT2L2: TPanel
        Left = 477
        Top = 0
        Width = 459
        Height = 187
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 26
        object PnBox05: TPanel
          Left = 1
          Top = 1
          Width = 457
          Height = 185
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 24
          object Panel29: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 93
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox9: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel38: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label42: TLabel
                  Left = 4
                  Top = 4
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                end
                object Label46: TLabel
                  Left = 6
                  Top = 32
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                end
                object DBEdControle05: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI05: TDBEdit
                  Left = 111
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox10: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 13
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel39: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 224
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label64: TLabel
                  Left = 4
                  Top = 32
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label65: TLabel
                  Left = 4
                  Top = 4
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label66: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label77: TLabel
                  Left = 4
                  Top = 87
                  Width = 80
                  Height = 21
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object EdPecas05: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea05: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia05: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet05: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 457
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 24
            object Panel28: TPanel
              Left = 41
              Top = 0
              Width = 294
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel52: TPanel
                Left = 0
                Top = 29
                Width = 294
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit45: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 194
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit46: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit44: TDBEdit
                Left = 0
                Top = 0
                Width = 294
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel53: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label82: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel54: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '5'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel63: TPanel
              Left = 335
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitLeft = -98
              object CkSubClass5: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass5Click
              end
            end
          end
          object PnIntMei05: TPanel
            Left = 0
            Top = 157
            Width = 457
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
            ExplicitWidth = 24
          end
          object SGItens05: TStringGrid
            Left = 298
            Top = 64
            Width = 159
            Height = 93
            Align = alClient
            TabOrder = 3
          end
        end
      end
      object PnBoxT2L1: TPanel
        Left = 0
        Top = 0
        Width = 477
        Height = 187
        Align = alLeft
        TabOrder = 0
        object PnBox04: TPanel
          Left = 1
          Top = 1
          Width = 475
          Height = 185
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel33: TPanel
            Left = 0
            Top = 64
            Width = 298
            Height = 93
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox7: TGroupBox
              Left = 0
              Top = 0
              Width = 298
              Height = 80
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel36: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 55
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label47: TLabel
                  Left = 4
                  Top = 4
                  Width = 68
                  Height = 21
                  Caption = 'Controle:'
                end
                object Label51: TLabel
                  Left = 6
                  Top = 32
                  Width = 47
                  Height = 21
                  Caption = 'IME-I:'
                end
                object DBEdControle04: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Controle'
                  TabOrder = 0
                end
                object DBEdIMEI04: TDBEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 29
                  DataField = 'VMI_Dest'
                  TabOrder = 1
                end
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 80
              Width = 298
              Height = 13
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel37: TPanel
                Left = 2
                Top = 23
                Width = 294
                Height = 224
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label61: TLabel
                  Left = 4
                  Top = 32
                  Width = 49
                  Height = 21
                  Caption = 'Pe'#231'as:'
                end
                object Label62: TLabel
                  Left = 4
                  Top = 4
                  Width = 24
                  Height = 21
                  Caption = 'ID:'
                end
                object Label63: TLabel
                  Left = 4
                  Top = 60
                  Width = 41
                  Height = 21
                  Caption = #193'rea:'
                end
                object Label76: TLabel
                  Left = 4
                  Top = 87
                  Width = 80
                  Height = 21
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object EdPecas04: TdmkEdit
                  Left = 112
                  Top = 28
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdPecasXXChange
                end
                object EdArea04: TdmkEdit
                  Left = 112
                  Top = 56
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMedia04: TdmkEdit
                  Left = 112
                  Top = 84
                  Width = 180
                  Height = 27
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdPallet04: TDBEdit
                  Left = 112
                  Top = 0
                  Width = 180
                  Height = 29
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clPurple
                  Font.Height = -17
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                end
              end
            end
          end
          object Panel31: TPanel
            Left = 0
            Top = 0
            Width = 475
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel32: TPanel
              Left = 41
              Top = 0
              Width = 312
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel47: TPanel
                Left = 0
                Top = 29
                Width = 312
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 31
                object DBEdit52: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 212
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  TabOrder = 0
                  ExplicitHeight = 29
                end
                object DBEdit53: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  TabOrder = 1
                  ExplicitHeight = 29
                end
              end
              object DBEdit51: TDBEdit
                Left = 0
                Top = 0
                Width = 312
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label81: TLabel
                Left = 0
                Top = 0
                Width = 27
                Height = 21
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel49: TPanel
                Left = 0
                Top = 21
                Width = 41
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                Caption = '4'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -37
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel61: TPanel
              Left = 353
              Top = 0
              Width = 122
              Height = 64
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass4: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass4Click
              end
            end
          end
          object PnIntMei04: TPanel
            Left = 0
            Top = 157
            Width = 475
            Height = 28
            Align = alBottom
            Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
            UseDockManager = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Visible = False
          end
          object SGItens04: TStringGrid
            Left = 298
            Top = 64
            Width = 177
            Height = 93
            Align = alClient
            TabOrder = 3
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 1584
    Top = 97
    Width = 320
    Height = 609
    Align = alRight
    Caption = 'Panel2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    ExplicitLeft = 1151
    object DBGAll: TdmkDBGridZTO
      Left = 1
      Top = 508
      Width = 318
      Height = 100
      Align = alBottom
      DataSource = DsAll
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      PopupMenu = PMAll
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -17
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Box'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubClass'
          Title.Caption = 'Sub classe'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MARTELO'
          Title.Caption = 'Martelo'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I'
          Width = 100
          Visible = True
        end>
    end
    object SGAll: TStringGrid
      Left = 1
      Top = 65
      Width = 318
      Height = 269
      Align = alClient
      ColCount = 13
      DefaultRowHeight = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyDown = SGAllKeyDown
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 318
      Height = 64
      Align = alTop
      Caption = 'Panel3'
      TabOrder = 2
      object BitBtn1: TBitBtn
        Left = 8
        Top = 8
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object EdAll: TdmkEdit
        Left = 88
        Top = 32
        Width = 109
        Height = 29
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdAll'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdAll'
        ValWarn = False
      end
      object BitBtn2: TBitBtn
        Left = 8
        Top = 36
        Width = 75
        Height = 25
        Caption = 'BitBtn2'
        TabOrder = 2
        OnClick = BitBtn2Click
      end
      object EdArrAll: TdmkEdit
        Left = 204
        Top = 31
        Width = 109
        Height = 29
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdAll'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdAll'
        ValWarn = False
      end
    end
    object Memo2: TMemo
      Left = 1
      Top = 334
      Width = 318
      Height = 174
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1904
    Height = 97
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 1471
    object PnDigitacao: TPanel
      Left = 1371
      Top = 1
      Width = 532
      Height = 95
      Align = alRight
      Enabled = False
      TabOrder = 0
      ExplicitLeft = 938
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 530
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = ' Digita'#231#227'o'
        ExplicitWidth = 75
      end
      object Panel7: TPanel
        Left = 141
        Top = 22
        Width = 52
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 33
          Height = 21
          Align = alTop
          Caption = 'Box:'
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 21
          Width = 52
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
        end
      end
      object PnArea: TPanel
        Left = 1
        Top = 22
        Width = 140
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 41
          Height = 21
          Align = alTop
          Caption = #193'rea:'
        end
        object LaTipoArea: TLabel
          Left = 105
          Top = 21
          Width = 35
          Height = 39
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 21
          Width = 105
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object PnMartelo: TPanel
        Left = 276
        Top = 22
        Width = 255
        Height = 72
        Align = alClient
        TabOrder = 3
        Visible = False
        object Label72: TLabel
          Left = 1
          Top = 1
          Width = 253
          Height = 21
          Align = alTop
          Caption = 'Martelo:'
          ExplicitWidth = 62
        end
        object CBVSMrtCad: TdmkDBLookupComboBox
          Left = 85
          Top = 22
          Width = 169
          Height = 47
          Align = alClient
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          KeyField = 'Nome'
          ListField = 'Nome'
          ListSource = DsVSMrtCad
          ParentFont = False
          TabOrder = 1
          TabStop = False
          dmkEditCB = EdVSMrtCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdVSMrtCad: TdmkEditCB
          Left = 1
          Top = 22
          Width = 84
          Height = 49
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnKeyDown = EdVSMrtCadKeyDown
          DBLookupComboBox = CBVSMrtCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
      end
      object PnSubClass: TPanel
        Left = 193
        Top = 22
        Width = 83
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Label86: TLabel
          Left = 0
          Top = 0
          Width = 64
          Height = 21
          Align = alTop
          Caption = 'SubClas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdSubClass: TdmkEdit
          Left = 0
          Top = 21
          Width = 83
          Height = 51
          Align = alClient
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSubClassChange
          OnExit = EdSubClassExit
          OnKeyDown = EdSubClassKeyDown
        end
      end
    end
    object Panel12: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 168
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
      end
      object Panel10: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 90
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdit23
        end
        object DBEdit23: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 46
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEDAreaT
        end
        object DBEDAreaT: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object Panel13: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 213
        Height = 21
        Align = alTop
        Caption = 'Couros que faltam classificar'
      end
      object Panel14: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 90
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdit21
        end
        object DBEdit21: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtPeca'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 46
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdit24
        end
        object DBEdit24: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtArM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object BtEncerra: TBitBtn
      Tag = 10134
      Left = 736
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Menu'
      TabOrder = 3
      TabStop = False
      OnClick = BtEncerraClick
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 956
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Imprimir'
      TabOrder = 4
      TabStop = False
      OnClick = BtImprimeClick
    end
    object BtDigitacao: TButton
      Left = 644
      Top = 28
      Width = 89
      Height = 45
      Caption = 'Teste'
      Enabled = False
      TabOrder = 5
      Visible = False
      OnClick = BtDigitacaoClick
    end
    object BtReabre: TBitBtn
      Tag = 18
      Left = 1177
      Top = 8
      Width = 72
      Height = 72
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = BtReabreClick
    end
    object CkMartelo: TCheckBox
      Left = 1256
      Top = 12
      Width = 97
      Height = 20
      Caption = 'Martelo'
      TabOrder = 7
      OnClick = CkMarteloClick
    end
    object CkSubClass: TCheckBox
      Left = 1256
      Top = 36
      Width = 113
      Height = 20
      Caption = 'Sub Classe'
      TabOrder = 8
      OnClick = CkSubClassClick
    end
    object CkUsaMemory: TdmkCheckBox
      Left = 644
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Mem'#243'ria'
      Checked = True
      State = cbChecked
      TabOrder = 9
      OnClick = CkUsaMemoryClick
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 233
    Height = 609
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 4
    object PnEqualize: TPanel
      Left = 0
      Top = 0
      Width = 233
      Height = 609
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel70: TPanel
        Left = 0
        Top = 0
        Width = 233
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbEqualize: TSpeedButton
          Left = 136
          Top = 8
          Width = 23
          Height = 22
          OnClick = SbEqualizeClick
        end
        object Label88: TLabel
          Left = 8
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Equ'#225'lize:'
        end
        object EdEqualize: TdmkEdit
          Left = 52
          Top = 8
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkEqualize: TCheckBox
          Left = 164
          Top = 0
          Width = 50
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
        object CkNota: TCheckBox
          Left = 164
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Nota.'
          TabOrder = 2
          OnClick = CkNotaClick
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 74
        Width = 233
        Height = 535
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
      object PnNota: TPanel
        Left = 0
        Top = 33
        Width = 233
        Height = 41
        Align = alTop
        TabOrder = 2
        Visible = False
        object DBEdit42: TDBEdit
          Left = 1
          Top = 1
          Width = 231
          Height = 39
          Align = alClient
          DataField = 'NotaEqzM2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Font.Quality = fqAntialiased
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Memo1: TMemo
        Left = 20
        Top = 348
        Width = 185
        Height = 89
        Lines.Strings = (
          'Memo1')
        TabOrder = 3
        Visible = False
      end
    end
  end
  object QrVSPaClaCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    OnCalcFields = QrVSPaClaCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome,'
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP'
      'FROM vspaclacab pcc'
      'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa'
      'WHERE pcc.Codigo=5'
      ''
      ''
      '')
    Left = 1380
    Top = 140
    object QrVSPaClaCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaClaCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPaClaCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSPaClaCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSPaClaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaClaCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaClaCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object DsVSPaClaCab: TDataSource
    DataSet = QrVSPaClaCab
    Left = 1380
    Top = 188
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSGerArtNewAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 1472
    Top = 141
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtNewFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSGerArtNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSGerArtNewClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSGerArtNewFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 1472
    Top = 189
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 1380
    Top = 284
  end
  object DsAll: TDataSource
    Left = 1380
    Top = 376
  end
  object PMItens01: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 220
    object AdicionarPallet01: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet01: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet01: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados1: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens02: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 264
    object AdicionarPallet02: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet02: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet02: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados2: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object QrSumVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1552
    Top = 612
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMItens03: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 308
    object AdicionarPallet03: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet03: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet03: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados3: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens04: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 352
    object AdicionarPallet04: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet04: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet04: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados4: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens05: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 396
    object AdicionarPallet05: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet05: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet05: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados5: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens06: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 440
    object AdicionarPallet06: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet06: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet06: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados6: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 792
    Top = 224
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox01: TMenuItem
      Tag = 1
      Caption = 'Encerra o pallet do box &1'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox02: TMenuItem
      Tag = 2
      Caption = 'Encerra o pallet do box &2'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox03: TMenuItem
      Tag = 3
      Caption = 'Encerra o pallet do box &3'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox04: TMenuItem
      Tag = 4
      Caption = 'Encerra o pallet do box &4'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox05: TMenuItem
      Tag = 5
      Caption = 'Encerra o pallet do box &5'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox06: TMenuItem
      Tag = 6
      Caption = 'Encerra o pallet do box &6'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
    object ImprimirNmeroPallet1: TMenuItem
      Caption = 'Imprimir N'#250'mero Pallet'
      Enabled = False
      OnClick = ImprimirNmeroPallet1Click
    end
    object ImprimirfluxodemovimentodoIMEI1: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI1Click
    end
    object DadosPaletsemWordPad1: TMenuItem
      Caption = 'Dados Palets no Paint'
      OnClick = DadosPaletsemWordPad1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object rocarVichaRMP1: TMenuItem
      Caption = 'Trocar Ficha RMP'
      Enabled = False
      OnClick = rocarVichaRMP1Click
    end
    object rocarIMEI1: TMenuItem
      Caption = 'Trocar IME-I'
      OnClick = rocarIMEI1Click
    end
  end
  object QrRevisores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1384
    Top = 428
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 1384
    Top = 472
  end
  object QrDigitadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1464
    Top = 432
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 1464
    Top = 476
  end
  object QrVMIsDePal: TmySQLQuery
    Database = Dmod.MyDB
    Left = 1472
    Top = 240
    object QrVMIsDePalVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object PMAll: TPopupMenu
    Left = 1600
    Top = 728
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = Alteraitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrVSMrtCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmrtcad'
      'ORDER BY Nome')
    Left = 332
    Top = 672
    object QrVSMrtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMrtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMrtCad: TDataSource
    DataSet = QrVSMrtCad
    Left = 332
    Top = 716
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 852
    Top = 696
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 856
    Top = 744
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 696
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 744
  end
  object QrNotaAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 12
    Top = 216
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 12
    Top = 260
  end
  object QrNotaCrr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 72
    Top = 216
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 72
    Top = 260
  end
  object QrNotas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 132
    Top = 216
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 132
    Top = 260
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1380
    Top = 240
    object QrSumTPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumTSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTJaFoi_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_PECA'
      Calculated = True
    end
    object QrSumTJaFoi_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object PMIMEI: TPopupMenu
    Left = 584
    Top = 836
    object ImprimirfluxodemovimentodoIMEI2: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI2Click
    end
  end
  object DqAll: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 464
  end
  object DqItens: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 72
    Top = 464
  end
  object DqSumPall: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 124
    Top = 464
  end
end
