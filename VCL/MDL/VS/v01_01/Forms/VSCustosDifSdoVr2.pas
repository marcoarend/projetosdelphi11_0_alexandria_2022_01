unit VSCustosDifSdoVr2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, Vcl.Menus, UnGrl_Consts, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSCustosDifSdoVr2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrGraGruXGrandeza: TSmallintField;
    DsGraGruX: TDataSource;
    PnPesquisa: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    BtEntrada: TBitBtn;
    BtTeste2: TBitBtn;
    DBGItens: TdmkDBGridZTO;
    Splitter1: TSplitter;
    QrOrigens: TMySQLQuery;
    DsOrigens: TDataSource;
    Label1: TLabel;
    Label3: TLabel;
    QrOrigensCodigo: TIntegerField;
    QrOrigensMovimCod: TIntegerField;
    QrOrigensControle: TIntegerField;
    QrOrigensMovimID: TIntegerField;
    QrOrigensMovimNiv: TIntegerField;
    QrOrigensGraGruX: TIntegerField;
    QrOrigensPecas: TFloatField;
    QrOrigensPesoKg: TFloatField;
    QrOrigensAreaM2: TFloatField;
    QrOrigensValorT: TFloatField;
    QrOrigensCustoUnit: TFloatField;
    BtBaixa: TBitBtn;
    PMEntrada: TPopupMenu;
    Recalculaselecionado1: TMenuItem;
    RecalculaTodos1: TMenuItem;
    PMBaixa: TPopupMenu;
    RecalculaValorpelaentrada1: TMenuItem;
    CorrigeovalordetodositensdeBaixa1: TMenuItem;
    N1: TMenuItem;
    Distribuidiferenaentreositens1: TMenuItem;
    CkTemIMEiMrt: TCheckBox;
    QrOrigensNO_TTW: TWideStringField;
    QrOrigensID_TTW: TLargeintField;
    Label4: TLabel;
    EdMargemErro: TdmkEdit;
    DaEntradaselecionada1: TMenuItem;
    Detodasentradas1: TMenuItem;
    PB1: TProgressBar;
    Label5: TLabel;
    EdFatorLimite: TdmkEdit;
    SbFatorLimite: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEntradaClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
    procedure DGDadosCellClick(Column: TColumn);
    procedure DBGItensDblClick(Sender: TObject);
    procedure RecalculaTodos1Click(Sender: TObject);
    procedure Recalculaselecionado1Click(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure RecalculaValorpelaentrada1Click(Sender: TObject);
    procedure CorrigeovalordetodositensdeBaixa1Click(Sender: TObject);
    procedure PMEntradaPopup(Sender: TObject);
    procedure PMBaixaPopup(Sender: TObject);
    procedure DaEntradaselecionada1Click(Sender: TObject);
    procedure Detodasentradas1Click(Sender: TObject);
    procedure SbFatorLimiteClick(Sender: TObject);
  private
    { Private declarations }
    FSelControle: Integer;
    procedure ReopenOrigens();
    procedure CorrigeBaixaSelecionada();
  public
    { Public declarations }
    FVersao: Integer;
  end;

  var
  FmVSCustosDifSdoVr2: TFmVSCustosDifSdoVr2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF, ModuleGeral, UnVS_CRC_PF,
  UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmVSCustosDifSdoVr2.BtBaixaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBaixa, BtBaixa);
end;

procedure TFmVSCustosDifSdoVr2.BtEntradaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrada, BtEntrada);
end;

procedure TFmVSCustosDifSdoVr2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCustosDifSdoVr2.BtTeste2Click(Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = False;
  SelfCall = True;
  TemIMEIMrt = 0;
var
  Filial, Empresa, GraGruX: Integer;
  MargemErro: Double;
  sItens: String;
begin
  QrOrigens.Close;
  //
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  GraGruX := EdGraGruX.ValueVariant;
  //
  if DmModVS.QtdPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2) then Exit;
  if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False,
    LaAviso1, LaAviso2) then Exit;
  if DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if VS_PF.RegistrosComProblema(Empresa, GraGruX, LaAviso1, LaAviso2) then Exit;
  //
  //if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
  if GraGruX = 0 then
    if Geral.MB_Pergunta(
    'A pesquisa sem definir o artigo poder� demorar um longo tempo.' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  MargemErro := EdMargemErro.ValueVariant;
  //
  DmModVS.CustosDiferentesSaldosVirtuais2(Empresa, GraGruX,
  Geral.BoolToInt(CkTemIMEiMrt.Checked), MargemErro, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2);
  case FVersao of
    2: sItens := Geral.FF0(DmModVS.QrCustosDifSdoVr2.RecordCount);
    3: sItens := Geral.FF0(DmModVS.QrCustosDifSdoVr3.RecordCount);
    else
      sItens := '????????';
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foram encontrados ' +
  sItens + ' IMEIS com saldo de valor divergente!');
end;

procedure TFmVSCustosDifSdoVr2.CorrigeovalordetodositensdeBaixa1Click(
  Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  DmModVS.QrCustosDifSdoVr2.DisableControls;
  QrOrigens.DisableControls;
  try
    DmModVS.QrCustosDifSdoVr2.First;
    PB1.Max := DmModVS.QrCustosDifSdoVr2.RecordCount;
    PB1.Position := 0;
    while not DmModVS.QrCustosDifSdoVr2.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      if DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0 then
      begin
        ReopenOrigens();
        QrOrigens.First;
        while not QrOrigens.Eof do
        begin
          CorrigeBaixaSelecionada();
          //
          QrOrigens.Next;
        end;
      end;
      //
      DmModVS.QrCustosDifSdoVr2.Next;
    end;
    QrOrigens.Close;
    BtTeste2Click(Self);
  finally
    DmModVS.QrCustosDifSdoVr2.EnableControls;
    QrOrigens.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSdoVr2.DaEntradaselecionada1Click(Sender: TObject);
var
  FatorLimitante, Diferenca, ValorT, Limite: Double;
  Controle: Integer;
  Executa: Boolean;
begin
(*  FatorLimitante := DmModVS.QrCustosDifSdoVr2DifVal.Value /
   DmModVS.QrCustosDifSdoVr2ValorT_A.Value * 100000;
  if FatorLimitante >= 1.00 then
*)
  FatorLimitante := EdFatorLimite.ValueVariant;
  if DmModVS.QrCustosDifSdoVr2FatorLimitante.Value >= FatorLimitante then
  begin
    Geral.MB_Aviso(
    'O valor de diferen�a � proporcionalmente muito alto para rateio.' +
    sLineBreak + '1. Corrija o valor de todos itens de Baixa ' + sLineBreak +
    '2. Caso continue aumente o fator limitante para 2,000000 e refa�a a distribui��o da diferen�a.'
    + sLineBreak +
    '3. Caso a diferen�a continue muito alta informe a Dermatek ou edite o IMEI de baixa apropriado!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
  ReopenOrigens();
  Diferenca := DmModVS.QrCustosDifSdoVr2DifVal.Value;
  Limite := 0.01;
  while ((Diferenca >= Limite) or (Diferenca <= -Limite))  do
  begin
    QrOrigens.first;
    while not QrOrigens.Eof do
    begin
      if ((Diferenca >= Limite) or (Diferenca <= -Limite)) then
      begin
        Executa := False;
        if (Diferenca < Limite) and (Diferenca > -Limite) then
        begin
          QrOrigens.Last;
        end;
        if Diferenca >= Limite then
        begin
          ValorT := QrOrigensValorT.Value - Limite;
          Diferenca := Diferenca - Limite;
          Executa   := True;
        end else
        if Diferenca <= -Limite then
        begin
          ValorT := QrOrigensValorT.Value + Limite;
          Diferenca := Diferenca + Limite;
          Executa   := True;
        end;
        if Executa then
        begin
          Controle := QrOrigensControle.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
          'ValorT'], ['Controle'], [
          ValorT], [Controle], True) then ;
        end;
      end;
      //
      QrOrigens.Next;
    end;
  end;
  VS_CRC_PF.AtualizaSaldoIMEI(FSelControle, False);
  QrOrigens.Close;
  //
  //
  BtTeste2Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSdoVr2.DBGItensDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DBGItens.Columns[THackDBGrid(DBGItens).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(QrOrigensControle.Value);
end;

procedure TFmVSCustosDifSdoVr2.Detodasentradas1Click(Sender: TObject);
var
  ItensForaDoLimite: Integer;
  //
  procedure DistrbuiDiferencaDeCustoNoItemAtual();
  var
    FatorLimitante, Diferenca, ValorT, Limite: Double;
    Controle: Integer;
    Executa: Boolean;
    Tabela: String;
  begin
    FatorLimitante := DmModVS.QrCustosDifSdoVr2DifVal.Value /
      DmModVS.QrCustosDifSdoVr2ValorT_A.Value * 100000;
    if FatorLimitante < 1.00 then
    begin
      ReopenOrigens();
      Diferenca := DmModVS.QrCustosDifSdoVr2DifVal.Value;
      Limite := 0.01;
      while ((Diferenca >= Limite) or (Diferenca <= -Limite))  do
      begin
        QrOrigens.first;
        while not QrOrigens.Eof do
        begin
          if ((Diferenca >= Limite) or (Diferenca <= -Limite)) then
          begin
            Executa := False;
            if (Diferenca < Limite) and (Diferenca > -Limite) then
            begin
              QrOrigens.Last;
            end;
            if Diferenca >= Limite then
            begin
              ValorT := QrOrigensValorT.Value - Limite;
              Diferenca := Diferenca - Limite;
              Executa   := True;
            end else
            if Diferenca <= -Limite then
            begin
              ValorT := QrOrigensValorT.Value + Limite;
              Diferenca := Diferenca + Limite;
              Executa   := True;
            end;
            if Executa then
            begin
              case QrOrigensID_TTW.Value of
                0: Tabela := CO_TAB_VMI;
                1: Tabela := CO_TAB_VMB;
                else Tabela := '???';
              end;
              Controle := QrOrigensControle.Value;
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
              'ValorT'], ['Controle'], [
              ValorT], [Controle], True) then ;
            end;
          end;
          //
          QrOrigens.Next;
        end;
      end;
      VS_CRC_PF.AtualizaSaldoIMEI(FSelControle, False);
      QrOrigens.Close;
    end else
      ItensForaDoLimite := ItensForaDoLimite + 1;
    //
  end;
begin
  Screen.Cursor := crHourGlass;
  ItensForaDoLimite := 0;
  PB1.Position := 0;
  PB1.Max := DmModVS.QrCustosDifSdoVr2.RecordCount;
  DmModVS.QrCustosDifSdoVr2.DisableControls;
  QrOrigens.DisableControls;
  try
    DmModVS.QrCustosDifSdoVr2.First;
    while not DmModVS.QrCustosDifSdoVr2.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //if DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0 then
      DistrbuiDiferencaDeCustoNoItemAtual();
      //
      DmModVS.QrCustosDifSdoVr2.Next;
    end;
    if ItensForaDoLimite > 0 then
    begin
      Geral.MB_Aviso(Geral.FF0(ItensForaDoLimite) +
      ' itens tem o valor de diferen�a proporcionalmente muito alto para rateio.' +
      sLineBreak + '1. Corrija o valor de todos itens de Baixa ' + sLineBreak +
      'Caso a diferen�a continue muito alta informe a Dermatek ou edite o IMEI de baixa apropriado!');
      Exit;
    end;
    BtTeste2Click(Self);
  finally
    DmModVS.QrCustosDifSdoVr2.EnableControls;
    QrOrigens.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSdoVr2.DGDadosCellClick(Column: TColumn);
begin
  if (DmModVS.QrCustosDifSdoVr2Controle.Value <> FSelControle) then
    QrOrigens.Close;
end;

procedure TFmVSCustosDifSdoVr2.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  ReopenOrigens();
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCustosDifSdoVr2Controle.Value);
end;

procedure TFmVSCustosDifSdoVr2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCustosDifSdoVr2.FormCreate(Sender: TObject);
begin
  FVersao := 0;
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSCustosDifSdoVr2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCustosDifSdoVr2.PMBaixaPopup(Sender: TObject);
begin
  RecalculaValorpelaentrada1.Enabled :=
    (DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0)
    and
    (QrOrigensID_TTW.Value = 0);
end;

procedure TFmVSCustosDifSdoVr2.PMEntradaPopup(Sender: TObject);
begin
  Recalculaselecionado1.Enabled := DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0;
  Distribuidiferenaentreositens1.Enabled := DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0;
end;

procedure TFmVSCustosDifSdoVr2.CorrigeBaixaSelecionada();
var
  Controle: Integer;
  AreaM2, PesoKg, Preco, ValorT, Qtde: Double;
begin
  AreaM2 := QrOrigensAreaM2.Value;
  PesoKg := QrOrigensPesoKg.Value;
  Preco  := DmModVS.QrCustosDifSdoVr2CustoUnit_A.Value;
  if AreaM2 <> 0 then
    Qtde := AreaM2
  else
  if PesoKg <> 0 then
    Qtde := PesoKg
  else
    Qtde := 0.00;
  //
  ValorT  := Qtde * Preco;
  //
  Controle := QrOrigensControle.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
  'ValorT'], ['Controle'], [
  ValorT], [Controle], True) then
  begin
    //VS_CRC_PF.AtualizaSaldoIMEI(DmModVS.QrCustosDifSdoVr2Controle.Value, False);
    VS_CRC_PF.AtualizaSaldoIMEI(FSelControle, False);
  end;
end;

procedure TFmVSCustosDifSdoVr2.Recalculaselecionado1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaSaldoIMEI(DmModVS.QrCustosDifSdoVr2Controle.Value, False);
end;

procedure TFmVSCustosDifSdoVr2.RecalculaTodos1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrCustosDifSdoVr2.First;
  while not DmModVS.QrCustosDifSdoVr2.Eof do
  begin
    if DmModVS.QrCustosDifSdoVr2ID_TTW.Value = 0 then
      VS_CRC_PF.AtualizaSaldoIMEI(DmModVS.QrCustosDifSdoVr2Controle.Value, False);
    //
    DmModVS.QrCustosDifSdoVr2.Next;
  end;
  DmModVS.QrCustosDifSdoVr2.Close;
  DmModVS.QrCustosDifSdoVr2.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSdoVr2.RecalculaValorpelaentrada1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    CorrigeBaixaSelecionada();
    QrOrigens.Close;
    BtTeste2Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifSdoVr2.ReopenOrigens();
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  FSelControle := DmModVS.QrCustosDifSdoVr2Controle.Value;
  //
  SQL_Flds := Geral.ATS([
  'Codigo, MovimCod, Controle, ',
  'MovimID, MovimNiv, GraGruX, Pecas, PesoKg, ',
  'AreaM2, ValorT,',
  'IF(AreaM2 <> 0, ValorT / AreaM2, ValorT / PesoKg) CustoUnit ',
  '']);
  SQL_Left := '';
  SQL_Wher := Geral.ATS([
  'WHERE SrcNivel2=' + Geral.FF0(FSelControle),
  '']);
  SQL_Group := '';
  SQL := Geral.ATS([
  VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY ValorT',
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrigens, DModG.MyPID_DB, [SQL]);
end;

procedure TFmVSCustosDifSdoVr2.SbFatorLimiteClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  EdFatorLimite.Enabled := True;
  EdFatorLimite.SetFocus;
end;

end.
