unit VSRclArtPrpNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, Vcl.Mask, UnProjGroup_Vars, dmkRadioGroup,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSRclArtPrpNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    PnPartida: TPanel;
    QrVSPallet1: TmySQLQuery;
    QrVSPallet1Codigo: TIntegerField;
    QrVSPallet1Nome: TWideStringField;
    QrVSPallet1Lk: TIntegerField;
    QrVSPallet1DataCad: TDateField;
    QrVSPallet1DataAlt: TDateField;
    QrVSPallet1UserCad: TIntegerField;
    QrVSPallet1UserAlt: TIntegerField;
    QrVSPallet1AlterWeb: TSmallintField;
    QrVSPallet1Ativo: TSmallintField;
    QrVSPallet1Empresa: TIntegerField;
    QrVSPallet1NO_EMPRESA: TWideStringField;
    QrVSPallet1Status: TIntegerField;
    QrVSPallet1CliStat: TIntegerField;
    QrVSPallet1GraGruX: TIntegerField;
    QrVSPallet1NO_CLISTAT: TWideStringField;
    QrVSPallet1NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet1NO_STATUS: TWideStringField;
    DsVSPallet1: TDataSource;
    PnPallets: TPanel;
    GBTecla: TGroupBox;
    PnTecla: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPallet1: TdmkEditCB;
    CBPallet1: TdmkDBLookupComboBox;
    LaVSRibCla: TLabel;
    SbPallet1: TSpeedButton;
    PnTecla1: TPanel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    SBPallet2: TSpeedButton;
    EdPallet2: TdmkEditCB;
    CBPallet2: TdmkDBLookupComboBox;
    PnTecla2: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrVSPallet2: TmySQLQuery;
    DsVSPallet2: TDataSource;
    QrVSPallet2Codigo: TIntegerField;
    QrVSPallet2Nome: TWideStringField;
    QrVSPallet2Lk: TIntegerField;
    QrVSPallet2DataCad: TDateField;
    QrVSPallet2DataAlt: TDateField;
    QrVSPallet2UserCad: TIntegerField;
    QrVSPallet2UserAlt: TIntegerField;
    QrVSPallet2AlterWeb: TSmallintField;
    QrVSPallet2Ativo: TSmallintField;
    QrVSPallet2Empresa: TIntegerField;
    QrVSPallet2NO_EMPRESA: TWideStringField;
    QrVSPallet2Status: TIntegerField;
    QrVSPallet2CliStat: TIntegerField;
    QrVSPallet2GraGruX: TIntegerField;
    QrVSPallet2NO_CLISTAT: TWideStringField;
    QrVSPallet2NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet2NO_STATUS: TWideStringField;
    Panel9: TPanel;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Panel12: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    SBPallet3: TSpeedButton;
    EdPallet3: TdmkEditCB;
    CBPallet3: TdmkDBLookupComboBox;
    PnTecla3: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Panel14: TPanel;
    GroupBox4: TGroupBox;
    Panel15: TPanel;
    Panel16: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    SBPallet4: TSpeedButton;
    EdPallet4: TdmkEditCB;
    CBPallet4: TdmkDBLookupComboBox;
    PnTecla4: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel18: TPanel;
    GroupBox5: TGroupBox;
    Panel19: TPanel;
    Panel20: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    SBPallet5: TSpeedButton;
    EdPallet5: TdmkEditCB;
    CBPallet5: TdmkDBLookupComboBox;
    PnTecla5: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Panel22: TPanel;
    GroupBox6: TGroupBox;
    Panel23: TPanel;
    Panel24: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    SBPallet6: TSpeedButton;
    EdPallet6: TdmkEditCB;
    CBPallet6: TdmkDBLookupComboBox;
    PnTecla6: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Panel26: TPanel;
    QrVSPallet3: TmySQLQuery;
    DsVSPallet3: TDataSource;
    QrVSPallet4: TmySQLQuery;
    DsVSPallet4: TDataSource;
    QrVSPallet5: TmySQLQuery;
    DsVSPallet5: TDataSource;
    QrVSPallet6: TmySQLQuery;
    DsVSPallet6: TDataSource;
    QrVSPallet3Codigo: TIntegerField;
    QrVSPallet3Nome: TWideStringField;
    QrVSPallet3Lk: TIntegerField;
    QrVSPallet3DataCad: TDateField;
    QrVSPallet3DataAlt: TDateField;
    QrVSPallet3UserCad: TIntegerField;
    QrVSPallet3UserAlt: TIntegerField;
    QrVSPallet3AlterWeb: TSmallintField;
    QrVSPallet3Ativo: TSmallintField;
    QrVSPallet3Empresa: TIntegerField;
    QrVSPallet3NO_EMPRESA: TWideStringField;
    QrVSPallet3Status: TIntegerField;
    QrVSPallet3CliStat: TIntegerField;
    QrVSPallet3GraGruX: TIntegerField;
    QrVSPallet3NO_CLISTAT: TWideStringField;
    QrVSPallet3NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet3NO_STATUS: TWideStringField;
    QrVSPallet4Codigo: TIntegerField;
    QrVSPallet4Nome: TWideStringField;
    QrVSPallet4Lk: TIntegerField;
    QrVSPallet4DataCad: TDateField;
    QrVSPallet4DataAlt: TDateField;
    QrVSPallet4UserCad: TIntegerField;
    QrVSPallet4UserAlt: TIntegerField;
    QrVSPallet4AlterWeb: TSmallintField;
    QrVSPallet4Ativo: TSmallintField;
    QrVSPallet4Empresa: TIntegerField;
    QrVSPallet4NO_EMPRESA: TWideStringField;
    QrVSPallet4Status: TIntegerField;
    QrVSPallet4CliStat: TIntegerField;
    QrVSPallet4GraGruX: TIntegerField;
    QrVSPallet4NO_CLISTAT: TWideStringField;
    QrVSPallet4NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet4NO_STATUS: TWideStringField;
    QrVSPallet5Codigo: TIntegerField;
    QrVSPallet5Nome: TWideStringField;
    QrVSPallet5Lk: TIntegerField;
    QrVSPallet5DataCad: TDateField;
    QrVSPallet5DataAlt: TDateField;
    QrVSPallet5UserCad: TIntegerField;
    QrVSPallet5UserAlt: TIntegerField;
    QrVSPallet5AlterWeb: TSmallintField;
    QrVSPallet5Ativo: TSmallintField;
    QrVSPallet5Empresa: TIntegerField;
    QrVSPallet5NO_EMPRESA: TWideStringField;
    QrVSPallet5Status: TIntegerField;
    QrVSPallet5CliStat: TIntegerField;
    QrVSPallet5GraGruX: TIntegerField;
    QrVSPallet5NO_CLISTAT: TWideStringField;
    QrVSPallet5NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet5NO_STATUS: TWideStringField;
    QrVSPallet6Codigo: TIntegerField;
    QrVSPallet6Nome: TWideStringField;
    QrVSPallet6Lk: TIntegerField;
    QrVSPallet6DataCad: TDateField;
    QrVSPallet6DataAlt: TDateField;
    QrVSPallet6UserCad: TIntegerField;
    QrVSPallet6UserAlt: TIntegerField;
    QrVSPallet6AlterWeb: TSmallintField;
    QrVSPallet6Ativo: TSmallintField;
    QrVSPallet6Empresa: TIntegerField;
    QrVSPallet6NO_EMPRESA: TWideStringField;
    QrVSPallet6Status: TIntegerField;
    QrVSPallet6CliStat: TIntegerField;
    QrVSPallet6GraGruX: TIntegerField;
    QrVSPallet6NO_CLISTAT: TWideStringField;
    QrVSPallet6NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet6NO_STATUS: TWideStringField;
    Panel13: TPanel;
    LaVSRibCad: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet: TSpeedButton;
    Panel17: TPanel;
    EdCodigo: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    EdMovimCod: TdmkEdit;
    EdCacCod: TdmkEdit;
    Label32: TLabel;
    BtReclasif: TBitBtn;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    EdVSGerRcl: TdmkEdit;
    Label33: TLabel;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsVMI_Codi: TIntegerField;
    QrVSMovItsVMI_Ctrl: TIntegerField;
    QrVSMovItsVMI_MovimNiv: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsNO_EMPRESA: TWideStringField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsCodigo: TIntegerField;
    CkReclasse: TCheckBox;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel21: TPanel;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    RGTipoArea: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure SbPalletClick(Sender: TObject);
    procedure EdPallet1Change(Sender: TObject);
    procedure EdPallet2Change(Sender: TObject);
    procedure SBPallet2Click(Sender: TObject);
    procedure SBPallet3Click(Sender: TObject);
    procedure SBPallet4Click(Sender: TObject);
    procedure SBPallet5Click(Sender: TObject);
    procedure SBPallet6Click(Sender: TObject);
    procedure EdPallet3Change(Sender: TObject);
    procedure EdPallet4Change(Sender: TObject);
    procedure EdPallet5Change(Sender: TObject);
    procedure EdPallet6Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure CkReclasseClick(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenVSGerArtDst(Controle: Integer);
    procedure ReopenPallets();
    procedure CadastraPallet(EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery);
  public
    { Public declarations }
    FCodigo, FCacCod, FStqCenLoc, FFornecMO: Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSRclArtPrpNew: TFmVSRclArtPrpNew;

implementation

uses
  UnMyObjects, Module, DmkDAC_PF, VSPalletAdd, MyDBCheck, ModuleGeral, MyListas,
  UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSRclArtPrpNew.BtOKClick(Sender: TObject);
var
  Empresa, FornecMO, ClientMO, Fornecedor, Codigo, MovimCod, VSGerRcl, VsMovIts,
  I, Controle,
  LstPal01, LstPal02, LstPal03, LstPal04, LstPal05, LstPal06, BxaGraGruX,
  GraGruX1, CtrlSorc1, CtrlDest1, GraGruX2, CtrlSorc2, CtrlDest2,
  GraGruX3, CtrlSorc3, CtrlDest3, GraGruX4, CtrlSorc4, CtrlDest4,
  GraGruX5, CtrlSorc5, CtrlDest5, GraGruX6, CtrlSorc6, CtrlDest6,
  BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, TipoArea, VSPallet,
  GraGruX0, VSPaRclCab, CacCod, VMI_XXX, BxaMovimID, VSMulFrnCab: Integer;
  DtHrIni, DtHrCfgCla, DtHrLibCla: String;
  SrcPallet, StqCenLoc: Integer;
  IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID;
  //
begin
  SrcPallet := EdPallet.ValueVariant;
  if MyObjects.FIC(SrcPallet = 0, EdPallet, 'Pallet inválido!') then
    Exit;
  Codigo         := EdCodigo.ValueVariant;
  CacCod         := EdCacCod.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  GraGruX0       := QrVSMovItsGraGruX.Value;
  VSPallet       := QrVSMovItsPallet.Value;
  VSGerRcl       := EdVSGerRcl.ValueVariant;
  VSMovIts       := QrVSMovItsControle.Value;
  Empresa        := QrVSMovItsEmpresa.Value;
  FornecMO       := FFornecMO;
  ClientMO       := QrVSMovItsClientMO.Value;
  Fornecedor     := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  FMovimID       := emidReclasXXUni;
  TipoArea       := RGTipoArea.ItemIndex;

  // Nao permitir duas vezes o mesmo pallet
  LstPal01 := EdPallet1.ValueVariant;
  LstPal02 := EdPallet2.ValueVariant;
  LstPal03 := EdPallet3.ValueVariant;
  LstPal04 := EdPallet4.ValueVariant;
  LstPal05 := EdPallet5.ValueVariant;
  LstPal06 := EdPallet6.ValueVariant;
  //
  GraGruX1 := QrVSPallet1GraGruX.Value;
  GraGruX2 := QrVSPallet2GraGruX.Value;
  GraGruX3 := QrVSPallet3GraGruX.Value;
  GraGruX4 := QrVSPallet4GraGruX.Value;
  GraGruX5 := QrVSPallet5GraGruX.Value;
  GraGruX6 := QrVSPallet6GraGruX.Value;
  //
  if VS_CRC_PF.PalletDuplicado(VAR_CLA_ART_RIB_MAX_BOX_06,
    LstPal01, LstPal02, LstPal03, LstPal04, LstPal05, LstPal06) then
      Exit;
  //
  // Ver se selecionou pelo menos um pallet!
  if MyObjects.FIC((LstPal01 = 0) and (LstPal02 = 0) and (LstPal03 = 0)
  and (LstPal04 = 0) and (LstPal05 = 0) and (LstPal06 = 0), nil,
  'Informe pelo menos um pallet!') then
     Exit;
  //
  //Veificar se a empresa dos pallets selecionados eh a mesma do pallet a reclassificar!
  if MyObjects.FIC((LstPal01 <> 0) and (QrVSPallet1Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 1!') then Exit;
  if MyObjects.FIC((LstPal02 <> 0) and (QrVSPallet2Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 2!') then Exit;
  if MyObjects.FIC((LstPal03 <> 0) and (QrVSPallet3Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 3!') then Exit;
  if MyObjects.FIC((LstPal04 <> 0) and (QrVSPallet4Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 4!') then Exit;
  if MyObjects.FIC((LstPal05 <> 0) and (QrVSPallet5Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 5!') then Exit;
  if MyObjects.FIC((LstPal06 <> 0) and (QrVSPallet6Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 6!') then Exit;
  //
  StqCenLoc := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local (1)!') then
    Exit;
  DtHrCfgCla     := Geral.FDT(DModG.ObtemAgora(), 109);
  DtHrLibCla     := DtHrCfgCla;
  CacCod :=
    UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, CacCod);
  VSGerRcl :=
    UMyMod.BPGS1I32('vsgerrcla', 'Codigo', '', '', tsPos, ImgTipo.SQLType, VSGerRcl);
  MovimCod :=
    UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Codigo :=
    UMyMod.BPGS1I32('vsparclcaba', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  if ImgTipo.SQLType = stIns then
    VS_CRC_PF.InsereVSCacCab(CacCod, emidReclasXXUni, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsparclcaba', False, [
  'VSPallet', CO_FLD_TAB_VMI, 'LstPal01',
  'LstPal02', 'LstPal03', 'LstPal04',
  'LstPal05', 'LstPal06', 'VSGerRcl',
  'CacCod'], [
  'Codigo'], [
  VSPallet, VSMovIts, LstPal01,
  LstPal02, LstPal03, LstPal04,
  LstPal05, LstPal06, VSGerRcl,
  CacCod], [
  Codigo], True) then
  begin
    VSPaRclCab := Codigo;
    if ImgTipo.SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidReclasXXUni, Codigo);
    //
    Codigo := VSGerRcl;
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsgerrcla', False, [
    'MovimCod', 'GraGruX', 'VSPallet',
    (*'Nome',*) 'Empresa', 'DtHrLibCla',
    'DtHrCfgCla', (*'DtHrFimCla',*) 'TipoArea',
    'CacCod'], [
    'Codigo'], [
    MovimCod, GraGruX0, VSPallet,
    (*Nome,*) Empresa, DtHrLibCla,
    DtHrCfgCla, (*DtHrFimCla,*) TipoArea,
    CacCod], [
    Codigo], True);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
    'GerRclCab'], ['Codigo'], [
    VSGerRcl], [VSPallet], True);
    //
    BxaGraGruX   := QrVSMovItsGraGruX.Value;
    BxaMovimNiv  := QrVSMovItsMovimNiv.Value;
    BxaSrcNivel1 := QrVSMovItsCodigo.Value;
    BxaSrcNivel2 := QrVSMovItsControle.Value;
    BxaMovimID   := QrVSMovItsMovimID.Value;
    //
    VMI_XXX     := QrVSMovItsControle.Value;
    //
    IuvpeiInn := iuvpei042(*Reclassificação de artigo de ribeira (UNI) 1/4*);
    IuvpeiBxa := iuvpei043(*Baixa de artigo de ribeira em reclassificação (UNI) 1/4*);
    //
    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      1, LstPal01, GraGruX1, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc1, CtrlDest1);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      2, LstPal02, GraGruX2, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc2, CtrlDest2);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      3, LstPal03, GraGruX3, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc3, CtrlDest3);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      4, LstPal04, GraGruX4, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc4, CtrlDest4);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      5, LstPal05, GraGruX5, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc5, CtrlDest5);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMo, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      6, LstPal06, GraGruX6, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc6, CtrlDest6);

    //
    FCodigo    := VSPaRclCab;
    FCacCod    := CacCod;
    FStqCenLoc := StqCenLoc;
    Close;
  end;
end;

procedure TFmVSRclArtPrpNew.BtReclasifClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
  //ReopenVSGerArtDst(0);
  ReopenPallets();
end;

procedure TFmVSRclArtPrpNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRclArtPrpNew.CadastraPallet(EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
const
  GraGruX = 0;
begin
  VS_CRC_PF.CadastraPalletRibCla(QrVSMovItsEmpresa.Value,
    QrVSMovItsClientMO.Value, EdPallet, CBPallet, QrVSPallet, emidClassArtXXUni,
    GraGruX);
end;

procedure TFmVSRclArtPrpNew.CkReclasseClick(Sender: TObject);
begin
  ReopenPallets();
end;

procedure TFmVSRclArtPrpNew.EdPalletRedefinido(Sender: TObject);
begin
  SbPallet.Enabled := EdPallet.ValueVariant > 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet1Change(Sender: TObject);
begin
  PnTecla1.Visible := EdPallet1.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet2Change(Sender: TObject);
begin
  PnTecla2.Visible := EdPallet2.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet3Change(Sender: TObject);
begin
  PnTecla3.Visible := EdPallet3.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet4Change(Sender: TObject);
begin
  PnTecla4.Visible := EdPallet4.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet5Change(Sender: TObject);
begin
  PnTecla5.Visible := EdPallet5.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.EdPallet6Change(Sender: TObject);
begin
  PnTecla6.Visible := EdPallet6.ValueVariant <> 0;
end;

procedure TFmVSRclArtPrpNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRclArtPrpNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo    := 0;
  FCacCod    := 0;
  FStqCenLoc := 0;
  ReopenPallets();
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSRclArtPrpNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRclArtPrpNew.ReopenPallets();
var
  SQL_Rcl: String;
begin
  if CkReclasse.Checked then
    SQL_Rcl := ' '
  else
    SQL_Rcl := 'AND vsp.GerRclCab = 0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.MovimNiv, vmi.Codigo, ',
  'vmi.Empresa, vmi.ClientMO, vmi.MovimID, vmi.GraGruX, ',
  'vmi.Pallet, vmi.Codigo VMI_Codi, vmi.Controle VMI_Ctrl, ',
  'vmi.MovimNiv VMI_MovimNiv, vmi.Terceiro, vmi.Ficha, ',
  'MAX(DataHora) DataHora, SUM(vmi.Pecas) Pecas,   ',
  'IF(MIN(AreaM2) > 0.01, 0, SUM(vmi.AreaM2)) AreaM2,   ',
  'IF(MIN(AreaP2) > 0.01, 0, SUM(vmi.AreaP2)) AreaP2,   ',
  'IF(MIN(PesoKg) > 0.001, 0, SUM(vmi.PesoKg)) PesoKg,   ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,   ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,   ',
  'vmi.VSMulFrnCab ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet    ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=vmi.Terceiro   ',
  'WHERE vmi.Pecas > 0 ',
  //'AND vmi.Empresa=-11  ',
  'AND vmi.Pallet > 0 ',
  'AND vmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidPreReclasse)),
  'GROUP BY vmi.Pallet, vmi.GraGruX, vmi.Empresa ',
  'ORDER BY NO_PRD_TAM_COR ',
  ' ']);
end;

procedure TFmVSRclArtPrpNew.ReopenVSPallet(QrVSPallet: TmySQLQuery);
begin
  VS_CRC_PF.ReopenVSPallet(QrVSPallet, QrVSMovItsEmpresa.Value,
    QrVSMovItsClientMO.Value, 0, '', []);
end;

procedure TFmVSRclArtPrpNew.SbPallet1Click(Sender: TObject);
begin
  CadastraPallet(EdPallet1, CBPallet1, QrVSPallet1);
end;

procedure TFmVSRclArtPrpNew.SBPallet2Click(Sender: TObject);
begin
  CadastraPallet(EdPallet2, CBPallet2, QrVSPallet2);
end;

procedure TFmVSRclArtPrpNew.SBPallet3Click(Sender: TObject);
begin
  CadastraPallet(EdPallet3, CBPallet3, QrVSPallet3);
end;

procedure TFmVSRclArtPrpNew.SBPallet4Click(Sender: TObject);
begin
  CadastraPallet(EdPallet4, CBPallet4, QrVSPallet4);
end;

procedure TFmVSRclArtPrpNew.SBPallet5Click(Sender: TObject);
begin
  CadastraPallet(EdPallet5, CBPallet5, QrVSPallet5);
end;

procedure TFmVSRclArtPrpNew.SBPallet6Click(Sender: TObject);
begin
  CadastraPallet(EdPallet6, CBPallet6, QrVSPallet6);
end;

procedure TFmVSRclArtPrpNew.SbPalletClick(Sender: TObject);
begin
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet,
  'Informe o pallet de artigo de ribeira!') then
    Exit
  else
  begin
    //
    PnPartida.Enabled := False;
    PnPallets.Visible := True;
    BtOK.Enabled      := True;
    //
    ReopenVSPallet(QrVSPallet1);
    ReopenVSPallet(QrVSPallet2);
    ReopenVSPallet(QrVSPallet3);
    ReopenVSPallet(QrVSPallet4);
    ReopenVSPallet(QrVSPallet5);
    ReopenVSPallet(QrVSPallet6);
  end;
end;

end.
