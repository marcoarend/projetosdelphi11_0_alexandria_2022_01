�
 TFMVSIMPPALLET 0
} TPF0TFmVSImpPalletFmVSImpPalletLeft Top Caption(   WET-CURTI-026 :: Impressão de Pallet VSClientHeightPClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style OnCreate
FormCreatePixelsPerInch`
TextHeight TPanelPnCabecaLeft Top Width�Height0AlignalTop
BevelOuterbvNoneTabOrder  	TGroupBoxGB_RLeft�Top Width0Height0AlignalRightTabOrder  	TdmkImageImgTipoLeftTopWidth Height Transparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width0Height0AlignalLeftTabOrder  	TGroupBoxGB_MLeft0Top Width�Height0AlignalClientTabOrder TLabel
LaTitulo1ALeftTop	WidthHeight Caption   Impressão de Pallet VSColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeft	TopWidthHeight Caption   Impressão de Pallet VSColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeftTop
WidthHeight Caption   Impressão de Pallet VSColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBoxGBRodaPeLeft Top
Width�HeightFAlignalBottomTabOrder TPanel
PnSaiDesisLeft^TopWidth� Height5AlignalRight
BevelOuterbvNoneTabOrder TBitBtnBtSaidaTagLeftTopWidthxHeight(CursorcrHandPointCaption   &Saída	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick   TPanelPanel1LeftTopWidth\Height5AlignalClient
BevelOuterbvNoneTabOrder  TBitBtnBtOKTagLeftTopWidthxHeight(Caption&OK	NumGlyphsTabOrder OnClick	BtOKClick    	TGroupBox	GBAvisos1Left Top�Width�Height,AlignalBottomCaption	 Avisos: TabOrder TPanelPanel4LeftTopWidth�HeightAlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	    	TGroupBox	GroupBox1Left Top0Width�Height�AlignalClientTabOrder TDBGridDBGrid1LeftTopWidth�Height�AlignalClient
DataSourceDsVSMovImp4TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style ColumnsExpanded	FieldNamePalletWidth>Visible	 Expanded	FieldNameGraGruXTitle.CaptionReduzidoVisible	 Expanded	FieldName	NO_ARTIGOTitle.CaptionArtigoWidth,Visible	 Expanded	FieldNamePecasTitle.Caption   PeçasWidthHVisible	 Expanded	FieldNameAreaM2Title.Caption	   Área m²WidthHVisible	 Expanded	FieldNameAreaP2Title.Caption
   Área ft²WidthHVisible	 Expanded	FieldNamePesoKgTitle.CaptionPeso KgWidthHVisible	 Expanded	FieldNameSerieFchTitle.Caption   Série FichaWidth>Visible	 Expanded	FieldNameFichaTitle.Caption   Número FichaWidth>Visible	     
TfrxReportfrxWET_CURTI_026_01_AVersion2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate ����l��@ReportOptions.LastChange ����l��@ScriptLanguagePascalScriptScriptText.Stringsvar  Imprime: Boolean;    4procedure Line3OnBeforePrint(Sender: TfrxComponent);begin4  Line3.Visible := Imprime;                         �  Imprime := not Imprime;                                                                                                                         end;    begin4  Imprime := True;                                  end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft4Top� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight      ��@	PaperSize	
LeftMargin       �@RightMargin       �@	Frame.Typ 
MirrorMode PrintOnPreviousPage	 TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ HeightD�O�^�׋@Top�	�c.�@Width:#J{�/�@DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxShapeViewShape1AllowVectorExport	Topo�ŏ1��@Width:#J{�/�@Height��� v6�@	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	LeftA}˜.���@Top{�G�z��@WidthӇ.�o���@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	LeftUގpZ�r�@TopA��4F�2�@Width/�j]0�j�@Height�	�c.�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaRight
Memo.UTF8W[VARF_DATA] 
ParentFontVAlignvaCenter  TfrxMemoViewMePaginaAllowVectorExport	TopA��4F�2�@Width��*-�I��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ 
Memo.UTF8WItem [Line#] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo1AllowVectorExport	Left�j���@Top��|\*�@Width-En]�@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ 
Memo.UTF8WPALLET [frxDsEstqR5."Pallet"] 
ParentFont  TfrxLineViewLine1AllowVectorExport	Left�Ws�`���?Top�K���H��@Width&6׆�ũ@ColorclBlack	Frame.TypftTop   TfrxLineViewLine5AllowVectorExport	Left�Ws�`���?Top@e�����@Width&6׆�ũ@ColorclBlack	Frame.TypftTop   TfrxMemoViewMemo2AllowVectorExport	Top�4���u��@Width������@Height�]�V��j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WData 
ParentFont  TfrxMemoViewMemo13AllowVectorExport	Left������@Top�J�+�b2�@Width���`�V��@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WCliente 
ParentFontVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	Top��~�T�R�@Width@�C ����@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Matéria-prima: 
ParentFontVAlignvaCenter  TfrxMemoViewMemo29AllowVectorExport	Leftrb��P(�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@VisibleFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Leftۥ�x<�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	LeftT��P(�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontVAlignvaCenter  TfrxMemoViewMemo8AllowVectorExport	TopHL�����@Width������@Height�]�V��j�@DisplayFormat.FormatStrdd/mm/yy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left������@Top�v���@Width���`�V��@Height�e?��r6�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo22AllowVectorExport	Left@�C ����@Top��~�T�R�@Width;�O�^�׋@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Leftrb��P(�@Top���e��@WidthT��P(�@Height׬mi�\.�@VisibleDisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Top���e��@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Leftۥ�x<�@Top���e��@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	LeftT��P(�@Top���e��@WidthT��P(�@Height׬mi�\.�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left@�C ����@Top��|\*�@Width��6��@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W[VARF_EMPNOME] 
ParentFontWordWrapVAlignvaCenterFormats     TfrxMemoViewMemo31AllowVectorExport	Top��|\*�@Width@�C ����@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[VARF_EMPDESC] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo46AllowVectorExport	Left����N��@TopA��4F�2�@Width���QI��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_CODI_FRX] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo5AllowVectorExport	Left����W��@TopE*�-9��@Widthtn�D��˝@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_NO_PALLET] 
ParentFont  TfrxLineViewLine3AllowVectorExport	Top�aYů׋@Width:#J{�/�@OnBeforePrintLine3OnBeforePrintColorclBlackFrame.StylefsDot	Frame.TypftTop   TfrxLineViewLine2AllowVectorExport	TopӇ.�o���@Width:#J{�/�@ColorclBlackFrame.StylefsDot	Frame.TypftTop   TfrxMemoViewMemo10AllowVectorExport	Left�lV}�R�@Top��	K<�L�@Width�Kx�%�@Height��yS�J�@	DataField
NO_PalStatDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaRight
Memo.UTF8W[frxDsEstqR5."NO_PalStat"] 
ParentFont  TfrxMemoViewMemo11AllowVectorExport	Left�j���� @Top��K���P�@Width����N��@Height���)�D��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFont  TfrxMemoViewMemo12AllowVectorExport	Left�lV}�R�@TopM�O����@Width��ǘ��H�@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaRight
Memo.UTF8WDV : [VARF_DV_PALLET] 
ParentFont  TfrxBarCodeViewBarCode2AllowVectorExport	Left��	K<�L�@Top.�&�@Width       �@Height'�l���@BarType
bcCode128C	DataFieldPAL_BARCODEDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5	Frame.Typ Rotation ShowTextTestLineText
0000000000WideBarRatio       � @Zoom       ��?Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style ColorBarclBlack  TfrxBarCodeViewBarCode1AllowVectorExport	Left\���4��@TopM�O����@Width       �@Height/$�ʌ�j�@BarType
bcCode128C
Expression<VARF_DV_PALLET_BARCODE>	Frame.Typ Rotation ShowTextTestLineText0000WideBarRatio       � @Zoom       ��?Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style ColorBarclBlack     TMySQLQueryQrEstqR5DatabaseDModG.RV_CEP_DBFilter(PalVrtPeca > 0) OR (Ativo<>1)Filtered	OnCalcFieldsQrEstqR5CalcFieldsSQL.Strings?SELECT Empresa, GraGruX, SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, =Sum(AreaM2) AreaM2, SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,  9SUM(SdoVrtPeca) SdoVrtPeca, SUM(SdoVrtPeso) SdoVrtPeso,  Sum(SdoVrtArM2) SdoVrtArM2,     (SUM(SdoVrtPeca+LmbVrtPeca) PalVrtPeca,  'SUM(SdoVrtPeso+LmbVrtPeso) PalVrtPeso, (SUM(LmbVrtArM2+SdoVrtArM2) PalVrtArM2,      SUM(LmbVrtPeca) LmbVrtPeca,  9SUM(LmbVrtPeso) LmbVrtPeso, SUM(LmbVrtArM2) LmbVrtArM2,          @GraGru1, NO_PRD_TAM_COR, Pallet, NO_PALLET, Terceiro, CliStat,  9Status, NO_FORNECE,  NO_CLISTAT, NO_EMPRESA, NO_STATUS,  ADataHora, OrdGGX, OrdGGY, GraGruY, NO_GGY, SUM(PalStat) PalStat, >ELT(SUM(PalStat) + 1, "Encerrado", "Montando", "Desmontando", "Multi???") NO_PalStat, Ativo,%IF(SUM(SdoVrtPeca+LmbVrtPeca)  <> 0, ASUM(LmbVrtArM2+SdoVrtArM2) / SUM(SdoVrtPeca+LmbVrtPeca), 0) MediaFROM  _vsmovimp4_fmvsmovimp +WHERE (Pecas >= 0.001 OR Pecas <= -0.001)  &AND SdoVrtPeca > 0 OR LmbVrtPeca <> 0 GROUP BY Pallet ORDER BY Pallet DESC, PalStat  Left8Top8 TIntegerFieldQrEstqR5Empresa	FieldNameEmpresa  TIntegerFieldQrEstqR5GraGruX	FieldNameGraGruX  TFloatFieldQrEstqR5Pecas	FieldNamePecasDisplayFormat&#,###,###,###,###;-#,###,###,###,###;   TFloatFieldQrEstqR5PesoKg	FieldNamePesoKgDisplayFormat$#,###,###,##0.00;-#,###,###,##0.00;   TFloatFieldQrEstqR5AreaM2	FieldNameAreaM2DisplayFormat$#,###,###,##0.00;-#,###,###,##0.00;   TFloatFieldQrEstqR5AreaP2	FieldNameAreaP2DisplayFormat$#,###,###,##0.00;-#,###,###,##0.00;   TIntegerFieldQrEstqR5GraGru1	FieldNameGraGru1  TWideStringFieldQrEstqR5NO_PRD_TAM_CORDisplayWidth� 	FieldNameNO_PRD_TAM_CORSize�   TIntegerFieldQrEstqR5Terceiro	FieldNameTerceiro  TWideStringFieldQrEstqR5NO_FORNECE	FieldName
NO_FORNECESized  TIntegerFieldQrEstqR5Pallet	FieldNamePallet  TWideStringFieldQrEstqR5NO_PALLET	FieldName	NO_PALLETSize<  TFloatFieldQrEstqR5ValorT	FieldNameValorTDisplayFormat$#,###,###,##0.00;-#,###,###,##0.00;   TSmallintFieldQrEstqR5Ativo	FieldNameAtivoMaxValue  TIntegerFieldQrEstqR5OrdGGX	FieldNameOrdGGX  TWideStringFieldQrEstqR5NO_STATUS	FieldName	NO_STATUS  TWideStringFieldQrEstqR5NO_EMPRESA	FieldName
NO_EMPRESASized  TDateTimeFieldQrEstqR5DataHora	FieldNameDataHora  TWideStringFieldQrEstqR5NO_CliStat	FieldName
NO_CliStatSized  TIntegerFieldQrEstqR5CliStat	FieldNameCliStat  TFloatFieldQrEstqR5SdoVrtPeca	FieldName
SdoVrtPeca  TFloatFieldQrEstqR5SdoVrtPeso	FieldName
SdoVrtPeso  TFloatFieldQrEstqR5SdoVrtArM2	FieldName
SdoVrtArM2  TFloatFieldQrEstqR5PalVrtPeca	FieldName
PalVrtPeca  TFloatFieldQrEstqR5PalVrtPeso	FieldName
PalVrtPeso  TFloatFieldQrEstqR5PalVrtArM2	FieldName
PalVrtArM2  TFloatFieldQrEstqR5LmbVrtPeca	FieldName
LmbVrtPeca  TFloatFieldQrEstqR5LmbVrtPeso	FieldName
LmbVrtPeso  TFloatFieldQrEstqR5LmbVrtArM2	FieldName
LmbVrtArM2  TIntegerFieldQrEstqR5Status	FieldNameStatus  TIntegerFieldQrEstqR5OrdGGY	FieldNameOrdGGY  TIntegerFieldQrEstqR5GraGruY	FieldNameGraGruY  TWideStringFieldQrEstqR5NO_GGY	FieldNameNO_GGYSize�   TWideStringFieldQrEstqR5NO_PalStat	FieldName
NO_PalStatSize  TFloatFieldQrEstqR5Media	FieldNameMedia  TIntegerFieldQrEstqR5PalStat	FieldNamePalStat  TWideStringFieldQrEstqR5TEXTO_QRCODEDisplayWidth�	FieldKindfkCalculated	FieldNameTEXTO_QRCODESize�
Calculated	  TWideStringFieldQrEstqR5PAL_TXT	FieldNamePAL_TXTSize2  TIntegerFieldQrEstqR5SerieFch	FieldNameSerieFch  TIntegerFieldQrEstqR5Ficha	FieldNameFicha  TFloatFieldQrEstqR5SdoVrtArP2	FieldKindfkCalculated	FieldName
SdoVrtArP2
Calculated	  TIntegerFieldQrEstqR5IMEI	FieldNameIMEI  TWideStringFieldQrEstqR5PAL_BARCODE	FieldNamePAL_BARCODE   TfrxDBDatasetfrxDsEstqR5UserNamefrxDsEstqR5CloseDataSourceFieldAliases.StringsEmpresa=EmpresaGraGruX=GraGruXPecas=PecasPesoKg=PesoKgAreaM2=AreaM2AreaP2=AreaP2GraGru1=GraGru1NO_PRD_TAM_COR=NO_PRD_TAM_CORTerceiro=TerceiroNO_FORNECE=NO_FORNECEPallet=PalletNO_PALLET=NO_PALLETValorT=ValorTAtivo=AtivoOrdGGX=OrdGGXNO_STATUS=NO_STATUSNO_EMPRESA=NO_EMPRESADataHora=DataHoraNO_CliStat=NO_CliStatCliStat=CliStatSdoVrtPeca=SdoVrtPecaSdoVrtPeso=SdoVrtPesoSdoVrtArM2=SdoVrtArM2PalVrtPeca=PalVrtPecaPalVrtPeso=PalVrtPesoPalVrtArM2=PalVrtArM2LmbVrtPeca=LmbVrtPecaLmbVrtPeso=LmbVrtPesoLmbVrtArM2=LmbVrtArM2Status=StatusOrdGGY=OrdGGYGraGruY=GraGruYNO_GGY=NO_GGYNO_PalStat=NO_PalStatMedia=MediaPalStat=PalStatTEXTO_QRCODE=TEXTO_QRCODEPAL_TXT=PAL_TXTSerieFch=SerieFchFicha=FichaSdoVrtArP2=SdoVrtArP2	IMEI=IMEIPAL_BARCODE=PAL_BARCODE DataSetQrEstqR5BCDToCurrencyDataSetOptions Left8Toph  TMySQLQuery
QrVSCacItsDatabaseDModG.RV_CEP_DBLeft8Top�  TWideStringFieldQrVSCacItsNO_REVISOR	FieldName
NO_REVISORSized  TFloatFieldQrVSCacItsPecas	FieldNamePecas   
TfrxReportfrxWET_CURTI_026_02Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate 0�m���@ReportOptions.LastChange 0�Y�@ScriptLanguagePascalScriptScriptText.Stringsbegin(                                        end. 
OnGetValuefrxWET_CURTI_026_02GetValueLeft<TopDatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTer  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1OrientationpoLandscape
PaperWidth      ��@PaperHeight       �@	PaperSize	
LeftMargin       �@RightMargin       �@	TopMargin       �@BottomMargin       �@	Frame.Typ LargeDesignHeight	
MirrorMode  TfrxPageHeaderPageHeader4FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�|�lf���@Top�	�c.�@Width�nض(��@ TfrxShapeViewShape3AllowVectorExport	Width�nض(��@Height�	�c.�@	Frame.Typ Frame.Width���������?ShapeskRoundRectangle  TfrxMemoViewMemo46AllowVectorExport	Left�j����@WidthӇ.�o���@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsDono."NOMEDONO"] 
ParentFontVAlignvaCenter  TfrxLineViewLine3AllowVectorExport	Top�	�c.�@Width�nض(��@ColorclBlack	Frame.TypftTop Frame.Width���������?  TfrxMemoViewMemo47AllowVectorExport	Left����W��@Top�	�c.�@Width��4�R��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W!IME-Is de Pallets X Fornecedores  
ParentFontVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�j����@Top�	�c.�@Width���o$��@Height�	�c.�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStr
dd/mm/yyyyDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ 
Memo.UTF8W[VARF_DATA] 
ParentFontVAlignvaCenter  TfrxMemoViewMePaginaAllowVectorExport	Left�-���=�@Top�	�c.�@Width���o$��@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaRight
Memo.UTF8W    Página [Page#] de [TotalPages#] 
ParentFontVAlignvaCenter  TfrxMemoView	MeTitNomeAllowVectorExport	Left���u��@Top֨�ht��@Width�DՖ�j�@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W   Nome da matéria-prima 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo62AllowVectorExport	Left@j'���@Top֨�ht��@Width���o$��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8W   Código 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo50AllowVectorExport	Top��	K<�L�@Width��N@a��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_EMPRESA] 
ParentFont  TfrxMemoView
MeTitPecasAllowVectorExport	Left���QI��@Top֨�ht��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W   Peças 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMeTitAreaM2AllowVectorExport	Left����N��@Top֨�ht��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W	   Área m² 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMeTitPesoKgAllowVectorExport	Left���)�D��@Top֨�ht��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8WPeso kg 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo1AllowVectorExport	LeftM�O����@Top֨�ht��@Width�X,���@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8WNome do Pallet 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo2AllowVectorExport	Top֨�ht��@Width��X,���@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo5AllowVectorExport	Left����W��@Top֨�ht��@Width��D"�H�@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8WNome do Fornecedor 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left:#J{�/�@Top֨�ht��@Width��k2�L�@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8W   Código 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo12AllowVectorExport	Left��4�R��@Top֨�ht��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W   Média m²/pç 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����W��@Top֨�ht��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8WInteiros 
ParentFontWordWrapVAlignvaCenter   TfrxMasterDataMD002FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Heightڟ�!����@Top�j����@Width�nض(��@DataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerRowCount  TfrxMemoView	MeValNomeAllowVectorExport	Left���u��@Width�DՖ�j�@Heightڟ�!����@	DataFieldNO_PRD_TAM_CORDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W![frxDsGGXPalTer."NO_PRD_TAM_COR"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoView	MeValCodiAllowVectorExport	Left@j'���@Width���o$��@Heightڟ�!����@	DataFieldGraGruXDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8W[frxDsGGXPalTer."GraGruX"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoView
MeValPecasAllowVectorExport	Left���QI��@Width�BNg@ �@Heightڟ�!����@	DataField
SdoVrtPecaDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W[frxDsGGXPalTer."SdoVrtPeca"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMeValAreaM2AllowVectorExport	Left����N��@Width�BNg@ �@Heightڟ�!����@	DataField
SdoVrtArM2DataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerDisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W[frxDsGGXPalTer."SdoVrtArM2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMeValPesoKgAllowVectorExport	Left���)�D��@Width�BNg@ �@Heightڟ�!����@DataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerDisplayFormat.FormatStr%2.3nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W[frxDsGGXPalTer."SdoVrtPeso"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	LeftM�O����@Width�X,���@Heightڟ�!����@	DataField	NO_PalletDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W[frxDsGGXPalTer."NO_Pallet"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Width��X,���@Heightڟ�!����@	DataFieldPalletDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8W[frxDsGGXPalTer."Pallet"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left����W��@Width��D"�H�@Heightڟ�!����@	DataField
NO_FORNECEDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W[frxDsGGXPalTer."NO_FORNECE"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo8AllowVectorExport	Left:#J{�/�@Width��k2�L�@Heightڟ�!����@	DataFieldTerceiroDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaCenter
Memo.UTF8W[frxDsGGXPalTer."Terceiro"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo15AllowVectorExport	Left��4�R��@Width�BNg@ �@Heightڟ�!����@	DataFieldMediaDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerDisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W[frxDsGGXPalTer."Media"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left����W��@Width�BNg@ �@Heightڟ�!����@	DataFieldInteirosDataSetfrxDsGGXPalTerDataSetNamefrxDsGGXPalTerDisplayFormat.FormatStr%2.1nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W[frxDsGGXPalTer."Inteiros"] 
ParentFontWordWrapVAlignvaCenter   TfrxPageFooterPageFooter2FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Heightڟ�!����@TopM�O����@Width�nض(��@ TfrxMemoViewMemo93AllowVectorExport	Widthd*��g0�@Heightڟ�!����@DataSetDModFin.frxDsSdoCtasDataSetNamefrxDsSdoCtasDisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftTop Frame.Width���������?
Memo.UTF8W*www.dermatek.com.br - Software Customizado 
ParentFont  TfrxMemoViewMeVARF_CODI_FRXAllowVectorExport	Left�����@Width���QI��@Height�˷�N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftTop Frame.Width���������?HAlignhaRight
Memo.UTF8W[VARF_CODI_FRX] 
ParentFontVAlignvaCenter   TfrxReportSummaryReportSummary1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height>�
Y�j�@TopE*�-9�@Width�nض(��@ TfrxMemoViewMe_FTTAllowVectorExport	Top������ @Width��:^A��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8WTOTAL  
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left���QI��@Top������ @Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left����N��@Top������ @Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo18AllowVectorExport	Left���)�D��@Top������ @Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.3nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeso">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo21AllowVectorExport	Left��4�R��@Top������ @Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W�[IIF(SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1) = 0, 0, SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1))] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo23AllowVectorExport	Left����W��@Top������ @Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.1nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W*[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter   TfrxGroupHeaderGH001FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�	�c.�@Top6v�ꭁ��@Width�nض(��@	ConditionfrxDsGGXPalTer."NO_PRD_TAM_COR" TfrxMemoViewMeGrupo1HeadAllowVectorExport	Widthhyܝ���@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W![frxDsGGXPalTer."NO_PRD_TAM_COR"] 
ParentFont   TfrxGroupFooterGF001FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height>�
Y�j�@Top�����@Width�nض(��@ TfrxMemoViewMeGrupo1FootAllowVectorExport	Width��:^A��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8WTotal de ??? 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left���QI��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left����N��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo13AllowVectorExport	Left���)�D��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.3nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeso">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo14AllowVectorExport	Left��4�R��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W�[IIF(SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1) = 0, 0, SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1))] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo22AllowVectorExport	Left����W��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.1nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W*[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter   TfrxGroupHeaderGH002FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�	�c.�@Top��K���P�@Width�nض(��@	ConditionfrxDsGGXPalTer."NO_PRD_TAM_COR" TfrxMemoViewMeGrupo2HeadAllowVectorExport	Widthhyܝ���@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W![frxDsGGXPalTer."NO_PRD_TAM_COR"] 
ParentFont   TfrxGroupFooterGF002FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height>�
Y�j�@Top�UH�I�׋@Width�nض(��@ TfrxMemoViewMeGrupo2FootAllowVectorExport	Width��:^A��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8WTotal de ??? 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo17AllowVectorExport	Left���QI��@Width�BNg@ �@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo19AllowVectorExport	Left����N��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo20AllowVectorExport	Left���)�D��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.3nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W,[SUM(<frxDsGGXPalTer."SdoVrtPeso">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left��4�R��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W�[IIF(SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1) = 0, 0, SUM(<frxDsGGXPalTer."SdoVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1))]     
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo29AllowVectorExport	Left����W��@Width�BNg@ �@Heightڟ�!����@DisplayFormat.FormatStr%2.1nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?HAlignhaRight
Memo.UTF8W*[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)] 
ParentFontWordWrapVAlignvaCenter     TMySQLQueryQrGGXPalTerDatabaseDModG.RV_CEP_DBSQL.Strings(SELECT SUM(vmi.SdoVrtPeca) SdoVrtPeca,  ASUM(vmi.SdoVrtPeso) SdoVrtPeso, SUM(vmi.SdoVrtArM2) SdoVrtArM2,  (vmi.Terceiro, vmi.GraGruX, vmi.Pallet,  vmi.SerieFch, vmi.Ficha, (vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,  0IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  /IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  $NO_PRD_TAM_COR, wbp.Nome NO_Pallet, 2SUM(vmi.SdoVrtArM2) / SUM(vmi.SdoVrtPeca) Media,  KSUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) Inteiros,  6IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  FROM vsmovits vmi  6LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  6LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  6LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  6LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  4LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  3LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet  4LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro 5LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch  7LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   3LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 WHERE vmi.SdoVrtPeca > 0 AND vmi.Pallet IN ( 1126, 1125, 1124, 1123, 1122, 1121, 1120, 1119, 1118, 1117, 1116, 1115, 1114, 1113, 1112, 1111, 1109, 1108, 1106, 1105, 1104, 1103, 1102, 1101, 1100, 1099, 1098, 1097, 1096, 1095, 1094, 1093, 1092, 1091, 1090, 1089, 1088, 1087, 1086, 1085, 1084, 1083, 1082, 1081, 1080, 1079, 1078, 1077, 1076, 1075, 1074, 1073, 1072, 1071, 1069, 1068, 1067, 1066, 1065, 1064, 1063, 1062, 1061, 1060, 1058, 1057, 1056, 1055, 1054, 1053, 1052, 1051, 1050, 1049, 1047, 1046, 1045, 1044, 1042, 1041, 1039, 1038, 1037, 1036, 1029, 1028, 1027, 1026, 1025, 1024, 1023, 1022, 1020, 1019, 1018, 1017, 1016, 1015, 1013, 1012, 1011, 1010, 1004, 1003, 1002, 991, 988, 980, 978, 976, 975, 970, 969, 968, 967, 965, 963, 960, 958, 957, 955, 953, 949, 946, 945, 944, 943, 939, 932, 893, 891, 887, 884, 882, 876, 875, 857, 848, 827, 823, 813, 810, 804, 802, 797, 792, 791, 766, 754, 739, 683, 595, 594, 529, 519, 505, 458, 394, 369, 364, 363, 362) GROUP BY Terceiro, Pallet *ORDER BY Terceiro, Pallet, NO_PRD_TAM_COR      Left� Topd TFloatFieldQrGGXPalTerSdoVrtPeca	FieldName
SdoVrtPeca  TFloatFieldQrGGXPalTerSdoVrtPeso	FieldName
SdoVrtPeso  TFloatFieldQrGGXPalTerSdoVrtArM2	FieldName
SdoVrtArM2  TIntegerFieldQrGGXPalTerTerceiro	FieldNameTerceiro  TIntegerFieldQrGGXPalTerGraGruX	FieldNameGraGruX  TIntegerFieldQrGGXPalTerPallet	FieldNamePallet  TIntegerFieldQrGGXPalTerSerieFch	FieldNameSerieFch  TIntegerFieldQrGGXPalTerFicha	FieldNameFicha  TWideStringFieldQrGGXPalTerNO_SerieFch	FieldNameNO_SerieFchSize<  TWideStringFieldQrGGXPalTerNO_PRD_TAM_COR	FieldNameNO_PRD_TAM_CORSize�   TWideStringFieldQrGGXPalTerNO_Pallet	FieldName	NO_PalletSize<  TFloatFieldQrGGXPalTerMedia	FieldNameMedia  TFloatFieldQrGGXPalTerInteiros	FieldNameInteiros  TWideStringFieldQrGGXPalTerNO_FORNECE	FieldName
NO_FORNECESized   TfrxDBDatasetfrxDsGGXPalTerUserNamefrxDsGGXPalTerCloseDataSourceFieldAliases.StringsSdoVrtPeca=SdoVrtPecaSdoVrtPeso=SdoVrtPesoSdoVrtArM2=SdoVrtArM2Terceiro=TerceiroGraGruX=GraGruXPallet=PalletSerieFch=SerieFchFicha=FichaNO_SerieFch=NO_SerieFchNO_PRD_TAM_COR=NO_PRD_TAM_CORNO_Pallet=NO_PalletMedia=MediaInteiros=InteirosNO_FORNECE=NO_FORNECE DataSetQrGGXPalTerBCDToCurrencyDataSetOptions Left� Top�   TMySQLQueryQrMulFrnDatabaseDModG.RV_CEP_DBLeft� Top TIntegerFieldQrMulFrnFornece	FieldNameFornece  TFloatFieldQrMulFrnPecas	FieldNamePecas  TWideStringFieldQrMulFrnSiglaVS	FieldNameSiglaVSSize
   TMySQLQueryQrTotFrnDatabaseDModG.RV_CEP_DBLeft� Top4 TFloatFieldQrTotFrnPecas	FieldNamePecas   
TfrxReportfrxWET_CURTI_026_03_C_100x150Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft`Top� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�MC;�@Top�	�c.�@Width�C�l����@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left@j'���@Top���a���@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left@j'���@Top�2���V��@Width�s��[�j�@Height�,�n�r�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left��wח���@Top�#:�\.�@WidthO����j�@Height$��L��N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�*���N�@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left9(a���@Top�	�c.�@Width.�&�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left9(a���@Top
�r߉ɖ@Width3�B���@Height��g*@k6�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontRotationWordWrapVAlignvaCenterFormats     TfrxMemoViewMemo14AllowVectorExport	Lefts��5��@Top>�
Y�j�@Width.�&�@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthE*�-9�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@TopI�2��v�@Width�_�]m��@Height�����t:�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotation  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@WidthE*�-9�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top����N��@Width x����j�@Height)�<Ov��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@TopX��0_^,�@Width x����j�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top����N��@Width pч6�@Height)�<Ov��@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@TopX��0_^,�@Width pч6�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Height�5������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@TopM�O����@Width x����j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@TopM�O����@Width pч6�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	Left�	�c.�@TopK̒�mY��@Width�j����@Height�C�l����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftBottom 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@HeightS��/�N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�/qܐA�@Width�j����@Height����N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Topq'�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack     
TfrxReport
frxReport1Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Stringsbegin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft� Top8DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height���F"�H�@Top�	�c.�@Width�C�l����@DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo47AllowVectorExport	Left�	�c.�@Top���)�D�@Width�	�c.�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Tope �u����?Width�P;����@Height�	�c.�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo1AllowVectorExport	Left\8�L �@Top�����@Width�0f�!�@Height��s�x�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W	898989899 
ParentFont  TfrxMemoViewMemo2AllowVectorExport	Left�	�c.�@Top׬mi�\.�@Width�T � C.�@Height�	����j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8WData 
ParentFont  TfrxMemoViewMemo29AllowVectorExport	Left�-�����@Top��k2�L�@Width׬mi�\.�@HeightY��r�\.�@VisibleFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Left�	�c.�@Top��k2�L�@Width׬mi�\.�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Leftǈ~L�H�@Top��k2�L�@Width׬mi�\.�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left� ��I���@Top��k2�L�@Width׬mi�\.�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontVAlignvaCenter  TfrxMemoViewMemo8AllowVectorExport	Left�	�c.�@Top׬mi�\.�@Width��fĻ�H�@Height�	����j�@DisplayFormat.FormatStrdd/mm/yy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left�T � C.�@Top���~�n�@Width�%���X.�@Height��:^A��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo22AllowVectorExport	Leftdt��N.�@TopE��b�݇@Width��IW.�@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left�-�����@Top07�\�e2�@Width׬mi�\.�@Height׬mi�\.�@VisibleDisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left�	�c.�@Top07�\�e2�@Width׬mi�\.�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Leftǈ~L�H�@Top07�\�e2�@Width׬mi�\.�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left� ��I���@Top07�\�e2�@Width׬mi�\.�@Height׬mi�\.�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Leftdt��N.�@Top�#:�\.�@Width��IW.�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W[frxDsEstqR5."NO_EMPRESA"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�y�Cn���@Width�	�c.�@Height���)�D��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_REVISORES] 
ParentFont  TfrxMemoViewMemo10AllowVectorExport	Left�	�c.�@Top�a����@Width>�
Y�j�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontVAlignvaCenter  TfrxMemoViewMemo12AllowVectorExport	Left�	�c.�@Top�j����@Width>�
Y�j�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ Frame.Width       ��?
Memo.UTF8WPallet: 
ParentFontVAlignvaCenter  TfrxMemoViewMemo14AllowVectorExport	Left�	�c.�@TopUގpZ�r�@Width�UH�I�׋@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ Frame.Width       ��?
Memo.UTF8WArtigo / espessura / cor: 
ParentFontVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	Left�	�c.�@Top�J�h��@Width�UH�I�׋@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontVAlignvaCenter     
TfrxReportfrxWET_CURTI_026_04Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate ����l��@ReportOptions.LastChange ����l��@ScriptLanguagePascalScriptScriptText.Stringsvar  Visivel: Boolean;    :procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginH  Visivel := (<frxDsEstqR5."Ativo"> = 1) OR (<frxDsEstqR5."Ativo"> = 3);  //M  Memo1.Visible := Visivel;                                                  M  Memo2.Visible := Visivel;                                                  M  Memo3.Visible := Visivel;                                                  M  Memo4.Visible := Visivel;                                                  M  Memo5.Visible := Visivel;                                                  M  Memo6.Visible := Visivel;                                                  M  Memo7.Visible := Visivel;                                                  M  Memo8.Visible := Visivel;                                                  M  Memo9.Visible := Visivel;                                                  N  Memo10.Visible := Visivel;                                                  N  Memo11.Visible := Visivel;                                                  N  Memo12.Visible := Visivel;                                                  N  Memo13.Visible := Visivel;                                                  N  Memo14.Visible := Visivel;                                                  N  Memo15.Visible := Visivel;                                                    Memo16.Visible := Visivel;  Memo17.Visible := Visivel;  Memo18.Visible := Visivel;  //        N  Shape1.Visible := Visivel;                                                  end;    begin6  Visivel := True;                                    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft� ToptDatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight      ��@	PaperSize	
LeftMargin       �@RightMargin       �@ColumnsColumnWidth       �@ColumnPositions.Strings0102 	Frame.Typ 
MirrorMode PrintOnPreviousPage	 TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�|���t:�@Top��ǘ��H�@Width+5{��@OnBeforePrintMasterData1OnBeforePrint	ColumnGapY��r�\.�@DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5PrintChildIfInvisible	PrintIfDetailEmpty	RowCount  TfrxShapeViewShape1AllowVectorExport	Leftڟ�!���� @Top���!���� @WidthP����x�@Height���`�V��@	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�Ws�`���?Top���Y�킙@WidthZ�V���j�@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WPALLET [frxDsEstqR5."Pallet"] 
ParentFont  TfrxMemoViewMemo3AllowVectorExport	Left�j���� @Topdb�qm�X�@Width}�����@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo: 
ParentFontRotationZVAlignvaCenter  TfrxMemoViewMemo2AllowVectorExport	Left���Y8�� @Top"��f�@Width��hM��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Série - Ficha: 
ParentFontVAlignvaBottom  TfrxMemoViewMemo4AllowVectorExport	Left�j���� @Topx��N���@Width��p8��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontVAlignvaBottom  TfrxMemoViewMemo7AllowVectorExport	Left�mme2�@Topx��N���@Width��p8��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontVAlignvaBottom  TfrxMemoViewMemo8AllowVectorExport	Left�	K<�l2�@Top຃Ta�P�@Width	k3F��@Heighta<�?q[.�@DisplayFormat.FormatStrdd/mm/yy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left���+:.�@Topdb�qm�X�@Width%�HW~��@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left���Y8�� @TopB�l���@Width��hM��@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W0[frxDsEstqR5."SerieFch"] - [frxDsEstqR5."Ficha"] 
ParentFontWordWrapVAlignvaCenterFormats     TfrxMemoViewMemo11AllowVectorExport	Left�j���� @Topڃ���,��@Width��p8��@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo12AllowVectorExport	Left�mme2�@Topڃ���,��@Width��p8��@Height׬mi�\.�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo13AllowVectorExport	Left�	�c.�@Top,��5���@Width���QI��@Height�˷�N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_CODI_FRX] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo5AllowVectorExport	Top�	�c.�@Widthe�F�N�@Height�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_NO_PALLET] 
ParentFont  TfrxMemoViewMemo6AllowVectorExport	LeftsV��W2�@Top"��f�@Width��p8��@Heightڟ�!����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontVAlignvaBottom  TfrxMemoViewMemo14AllowVectorExport	LeftsV��W2�@TopB�l���@Width��hM��@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo15AllowVectorExport	Left)߿���ۭ@Top�
<���� @WidthC�� �H�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8W[VARF_EMPNOME] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left>�
Y�j�@Top�
<���� @Width!�>��X.�@HeightY��r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[VARF_EMPDESC] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo17AllowVectorExport	Left�j����@Top��� v6�@Width��	K<�L�@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W
   Area ft²: 
ParentFont  TfrxMemoViewMemo18AllowVectorExport	Left���u��@Top��� v6�@Width.�&�@Height�	�c.�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."SdoVrtArP2"] 
ParentFont   TfrxPageHeaderPageHeader1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height���5�`0�@Top�	�c.�@Widthl`���ݾ@    TMySQLQuery	QrGraGruXDatabaseDModG.RV_CEP_DBSQL.Strings3SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, /IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), .IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) (NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, .unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMEDFROM gragrux ggx 5LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC 5LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad 5LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI 3LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 1LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMedWHERE ggx.Controle > 0&ORDER BY NO_PRD_TAM_COR, ggx.Controle  Left,Top8 TIntegerFieldQrGraGruXGraGru1	FieldNameGraGru1  TIntegerFieldQrGraGruXControle	FieldNameControle  TWideStringFieldQrGraGruXNO_PRD_TAM_COR	FieldNameNO_PRD_TAM_CORSize�   TWideStringFieldQrGraGruXSIGLAUNIDMED	FieldNameSIGLAUNIDMEDSize  TIntegerFieldQrGraGruXCODUSUUNIDMED	FieldNameCODUSUUNIDMED  TWideStringFieldQrGraGruXNOMEUNIDMED	FieldNameNOMEUNIDMEDSize   TDataSource	DsGraGruXDataSet	QrGraGruXLeft,Toph  TMySQLTableTbVSMovImp4DatabaseDModG.RV_CEP_DBFilterAtivo IN (2,3)Filtered	
BeforePostTbVSMovImp4BeforePost	TableName_vsmovimp4_fmvsmovimpLeft� Top�  TIntegerFieldTbVSMovImp4Empresa	FieldNameEmpresaRequired	  TIntegerFieldTbVSMovImp4GraGruX	FieldNameGraGruXRequired	  TFloatFieldTbVSMovImp4Pecas	FieldNamePecasRequired	  TFloatFieldTbVSMovImp4PesoKg	FieldNamePesoKgRequired	  TFloatFieldTbVSMovImp4AreaM2	FieldNameAreaM2Required	  TFloatFieldTbVSMovImp4AreaP2	FieldNameAreaP2Required	  TFloatFieldTbVSMovImp4ValorT	FieldNameValorTRequired	  TFloatFieldTbVSMovImp4SdoVrtPeca	FieldName
SdoVrtPecaRequired	  TFloatFieldTbVSMovImp4SdoVrtPeso	FieldName
SdoVrtPesoRequired	  TFloatFieldTbVSMovImp4SdoVrtArM2	FieldName
SdoVrtArM2Required	  TFloatFieldTbVSMovImp4LmbVrtPeca	FieldName
LmbVrtPecaRequired	  TFloatFieldTbVSMovImp4LmbVrtPeso	FieldName
LmbVrtPesoRequired	  TFloatFieldTbVSMovImp4LmbVrtArM2	FieldName
LmbVrtArM2Required	  TIntegerFieldTbVSMovImp4GraGru1	FieldNameGraGru1Required	  TWideStringFieldTbVSMovImp4NO_PRD_TAM_COR	FieldNameNO_PRD_TAM_CORSize�   TIntegerFieldTbVSMovImp4Pallet	FieldNamePalletRequired	  TWideStringFieldTbVSMovImp4NO_PALLET	FieldName	NO_PALLETSize<  TIntegerFieldTbVSMovImp4Terceiro	FieldNameTerceiroRequired	  TIntegerFieldTbVSMovImp4CliStat	FieldNameCliStatRequired	  TIntegerFieldTbVSMovImp4Status	FieldNameStatusRequired	  TWideStringFieldTbVSMovImp4NO_FORNECE	FieldName
NO_FORNECESized  TWideStringFieldTbVSMovImp4NO_CLISTAT	FieldName
NO_CLISTATSized  TWideStringFieldTbVSMovImp4NO_EMPRESA	FieldName
NO_EMPRESASized  TWideStringFieldTbVSMovImp4NO_STATUS	FieldName	NO_STATUS  TDateTimeFieldTbVSMovImp4DataHora	FieldNameDataHoraRequired	  TIntegerFieldTbVSMovImp4OrdGGX	FieldNameOrdGGXRequired	  TIntegerFieldTbVSMovImp4OrdGGY	FieldNameOrdGGYRequired	  TIntegerFieldTbVSMovImp4GraGruY	FieldNameGraGruYRequired	  TWideStringFieldTbVSMovImp4NO_GGY	FieldNameNO_GGYSize�   TIntegerFieldTbVSMovImp4PalStat	FieldNamePalStatRequired	  TIntegerFieldTbVSMovImp4CouNiv2	FieldNameCouNiv2Required	  TIntegerFieldTbVSMovImp4CouNiv1	FieldNameCouNiv1Required	  TWideStringFieldTbVSMovImp4NO_CouNiv2	FieldName
NO_CouNiv2Size<  TWideStringFieldTbVSMovImp4NO_CouNiv1	FieldName
NO_CouNiv1Size<  TFloatFieldTbVSMovImp4FatorInt	FieldNameFatorIntRequired	  TFloatFieldTbVSMovImp4SdoInteiros	FieldNameSdoInteirosRequired	  TFloatFieldTbVSMovImp4LmbInteiros	FieldNameLmbInteirosRequired	  TIntegerFieldTbVSMovImp4Codigo	FieldNameCodigoRequired	  TIntegerFieldTbVSMovImp4IMEC	FieldNameIMECRequired	  TIntegerFieldTbVSMovImp4IMEI	FieldNameIMEIRequired	  TIntegerFieldTbVSMovImp4MovimID	FieldNameMovimIDRequired	  TIntegerFieldTbVSMovImp4MovimNiv	FieldNameMovimNivRequired	  TWideStringFieldTbVSMovImp4NO_MovimID	FieldName
NO_MovimIDSize  TWideStringFieldTbVSMovImp4NO_MovimNiv	FieldNameNO_MovimNivSize(  TIntegerFieldTbVSMovImp4SerieFch	FieldNameSerieFchRequired	  TWideStringFieldTbVSMovImp4NO_SerieFch	FieldNameNO_SerieFchSize<  TIntegerFieldTbVSMovImp4Ficha	FieldNameFichaRequired	  TIntegerFieldTbVSMovImp4StatPall	FieldNameStatPallRequired	  TWideStringFieldTbVSMovImp4NO_StatPall	FieldNameNO_StatPallSize<  TSmallintFieldTbVSMovImp4NFeSer	FieldNameNFeSerRequired	  TIntegerFieldTbVSMovImp4NFeNum	FieldNameNFeNumRequired	  TIntegerFieldTbVSMovImp4VSMulNFeCab	FieldNameVSMulNFeCabRequired	  TIntegerFieldTbVSMovImp4StqCenCad	FieldName	StqCenCadRequired	  TWideStringFieldTbVSMovImp4NO_StqCenCad	FieldNameNO_StqCenCadSize2  TIntegerFieldTbVSMovImp4StqCenLoc	FieldName	StqCenLocRequired	  TWideStringFieldTbVSMovImp4NO_StqCenLoc	FieldNameNO_StqCenLocSize2  TSmallintFieldTbVSMovImp4Ativo	FieldNameAtivoRequired	  TWideStringFieldTbVSMovImp4NO_ARTIGO	FieldKindfkLookup	FieldName	NO_ARTIGOLookupDataSet	QrGraGruXLookupKeyFieldsControleLookupResultFieldNO_PRD_TAM_COR	KeyFieldsGraGruXSize� Lookup	   TDataSourceDsVSMovImp4DataSetTbVSMovImp4Left� Top�   
TfrxReportfrxWET_CURTI_026_01_BVersion2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate ����l��@ReportOptions.LastChange ����l��@ScriptLanguagePascalScriptScriptText.Stringsvar  Imprime: Boolean;    4procedure Line3OnBeforePrint(Sender: TfrxComponent);begin4  Line3.Visible := Imprime;                         �  Imprime := not Imprime;                                                                                                                         end;    begin4  Imprime := True;                                  end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft4Top� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight      ��@	PaperSize	
LeftMargin       �@RightMargin       �@	Frame.Typ 
MirrorMode PrintOnPreviousPage	 TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ HeightD�O�^�׋@Top�	�c.�@Width:#J{�/�@DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxShapeViewShape1AllowVectorExport	Topo�ŏ1��@Width:#J{�/�@Height��� v6�@	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	LeftA}˜.���@Top{�G�z��@WidthӇ.�o���@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	LeftUގpZ�r�@TopA��4F�2�@Width/�j]0�j�@Height�	�c.�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaRight
Memo.UTF8W[VARF_DATA] 
ParentFontVAlignvaCenter  TfrxMemoViewMePaginaAllowVectorExport	TopA��4F�2�@Width��*-�I��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ 
Memo.UTF8WItem [Line#] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo1AllowVectorExport	Left\8�L �@Top��|\*�@WidthmT�B���@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ 
Memo.UTF8WPALLET [frxDsEstqR5."Pallet"] 
ParentFontFormats     TfrxLineViewLine1AllowVectorExport	Left�Ws�`���?Top�K���H��@Width&6׆�ũ@ColorclBlack	Frame.TypftTop   TfrxLineViewLine5AllowVectorExport	Left�Ws�`���?TopAe�����@Width&6׆�ũ@ColorclBlack	Frame.TypftTop   TfrxMemoViewMemo2AllowVectorExport	Top�4���u��@Width������@Height�]�V��j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WData 
ParentFont  TfrxMemoViewMemo13AllowVectorExport	Left������@Top�J�+�b2�@Width���`�V��@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WCliente 
ParentFontVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	Top��~�T�R�@Width@�C ����@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Matéria-prima: 
ParentFontVAlignvaCenter  TfrxMemoViewMemo29AllowVectorExport	Leftt@T@�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[VARF_MEDIA_TIT] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left� /�$�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	LeftT��P(�@Top��n*̯֙@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo8AllowVectorExport	TopHL�����@Width������@Height�]�V��j�@DisplayFormat.FormatStrdd/mm/yy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left������@Top�v���@Width���`�V��@Height�e?��r6�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo22AllowVectorExport	Left@�C ����@Top��~�T�R�@Width;�O�^�׋@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Leftt@T@�@Top���e��@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[VARF_MEDIA_VAL] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Top���e��@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left� /�$�@Top���e��@WidthT��P(�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	LeftT��P(�@Top���e��@WidthT��P(�@Height׬mi�\.�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left@�C ����@Top��|\*�@WidthD�O�^�׋@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?
Memo.UTF8W[VARF_EMPNOME] 
ParentFontWordWrapVAlignvaCenter  TfrxMemoViewMemo31AllowVectorExport	Top��|\*�@Width@�C ����@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[VARF_EMPDESC] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo46AllowVectorExport	Left����N��@TopA��4F�2�@Width���QI��@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_CODI_FRX] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo5AllowVectorExport	Left����W��@TopE*�-9��@Widthtn�D��˝@Height       �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_NO_PALLET] 
ParentFont  TfrxLineViewLine3AllowVectorExport	Top�aYů׋@Width:#J{�/�@OnBeforePrintLine3OnBeforePrintColorclBlackFrame.StylefsDot	Frame.TypftTop   TfrxLineViewLine2AllowVectorExport	TopӇ.�o���@Width:#J{�/�@ColorclBlackFrame.StylefsDot	Frame.TypftTop   TfrxMemoViewMemo10AllowVectorExport	Left֨�ht��@Top��	K<�L�@Width�����|��@Height       �@	DataField
NO_PalStatDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaRight
Memo.UTF8W[frxDsEstqR5."NO_PalStat"] 
ParentFont  TfrxMemoViewMemo11AllowVectorExport	Left�j���� @Top��K���P�@Width����N��@Height���)�D��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFont  TfrxMemoViewMemo12AllowVectorExport	Left�lV}�R�@TopyO����@Width�c�]K���@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaRight
Memo.UTF8WDV : [VARF_DV_PALLET] 
ParentFont     
TfrxReportfrxWET_CURTI_026_03_D_100x150Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft`Top� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�MC;�@Top�	�c.�@Width�C�l����@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left@j'���@Top ��a���@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left@j'���@Top 0���V��@Width�s��[�j�@Height�,�n�r�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left��wח���@Top  :�\.�@WidthO����j�@Height$��L��N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�*���N�@Top  	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left ���W��@Top  	�c.�@Width �ǘ��H�@Height �ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left����W��@Top���Y�킥@Width�u%�U�@Height�q�D�V�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ 
Memo.UTF8WR[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] [frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats      TfrxMemoViewMemo14AllowVectorExport	Left ���\��@Top �
Y�j�@Width �ǘ��H�@Height  	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left XH�I�׋@Top  	�c.�@Width ��)�D��@Height �ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left XH�I�׋@Top H�2��v�@Width Px�k��@Height ����t:�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotationWordWrap  TfrxMemoViewMemo12AllowVectorExport	Left S�N��@Top  	�c.�@Width  �&�@Height �
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left �	K<�L�@Top `��ʾۭ@Width �
t���@Height (k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape4AllowVectorExport	Left ��u��@Top `��ʾۭ@Width  �&�@Height @j'���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left �
Y�j�@Top `��ʾۭ@Width  	�c.�@Height @j'���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	Left �
Y�j�@Top В�mY��@Width  	�c.�@Height @�l����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftBottom 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left �
Y�j�@Top �/qܐA�@Width  	�c.�@Height ��Y��f�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo48AllowVectorExport	Left XH�I�׋@Top ��4F�r�@Width �H�?<��@Height ���W��@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotation� VAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left �
Y�j�@Top ���W��@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo29AllowVectorExport	Left P=�T�@Top x�Cn���@Width x����j�@Height  ��1�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Left P͓�T�@Top  �0_^,�@Width x����j�@Height �:�<�L�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left �b���T�@Top `��ʾۭ@Width �;���j�@Height 8������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left @��;�L�@Top x�Cn���@Width pч6�@Height  ��1�@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left @I/�L�@Top  �0_^,�@Width pч6�@Height �:�<�L�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left �ސ��L�@Top `��ʾۭ@Width �l���@Height 8������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left P=�T�@Top �O����@Width x����j�@Height 8������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[TitPeso_kg] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left @��;�L�@Top �O����@Width pч6�@Height 8������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8W[ValPeso_kg] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left ���W��@Top ���N��@Width ��L�J��@Height �g0�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	Left �{�T��@Top ���N��@Width h����@Height h����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter     
TfrxReportfrxWET_CURTI_026_03_A_100x150Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft`TopXDatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�MC;�@Top�	�c.�@Width�C�l����@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left@j'���@Top���a���@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left@j'���@Top�2���V��@Width�s��[�j�@Height�,�n�r�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left��wח���@Top�#:�\.�@WidthO����j�@Height$��L��N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�*���N�@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left9(a���@Top�	�c.�@Width.�&�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left9(a���@Topj�t��@Width�B�I7��@HeightL�ɱL��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."GraGruX"] 
ParentFontRotationWordWrapVAlignvaCenterFormats     TfrxMemoViewMemo14AllowVectorExport	Lefts��5��@Top>�
Y�j�@Width.�&�@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthE*�-9�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@TopI�2��v�@Width�_�]m��@Height�����t:�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotation  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@WidthE*�-9�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top����N��@Width x����j�@Height)�<Ov��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@TopX��0_^,�@Width x����j�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top����N��@Width pч6�@Height)�<Ov��@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@TopX��0_^,�@Width pч6�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Height�5������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@TopM�O����@Width x����j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@TopM�O����@Width pч6�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@HeightS��/�N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�/qܐA�@Width�j����@Height����N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotationFormats     TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Topq'�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo15AllowVectorExport	Left�j����@Width�	�c.�@Height�\��ʾۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WMATERIAL DE ORIGEM LWG 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left�	�c.�@Top����N��@Width�j����@Height�	K<�l2�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats        
TfrxReportfrxWET_CURTI_026_03_B_100x150Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeft`Top� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�MC;�@Top�	�c.�@Width�C�l����@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left@j'���@Top���a���@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left@j'���@Top�2���V��@Width�s��[�j�@Height�,�n�r�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Left��wח���@Top�#:�\.�@WidthO����j�@Height$��L��N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�*���N�@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left9(a���@Top�	�c.�@Width.�&�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left9(a���@Top
�r߉ɖ@Width3�B���@Height��g*@k6�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo14AllowVectorExport	Lefts��5��@Top>�
Y�j�@Width.�&�@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthE*�-9�@Height��ǘ��H�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@TopI�2��v�@Width�_�]m��@Height�����t:�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotation  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@WidthE*�-9�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top����N��@Width x����j�@Height)�<Ov��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@TopX��0_^,�@Width x����j�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top����N��@Width pч6�@Height)�<Ov��@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@TopX��0_^,�@Width pч6�@Height!O�-˶ۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Height�5������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@TopM�O����@Width x����j�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@TopM�O����@Width pч6�@Height�5������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�&k�C4�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@HeightS��/�N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�/qܐA�@Width�j����@Height����N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Topq'�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo15AllowVectorExport	Left�	�c.�@Width�	�c.�@Height�\��ʾۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WMATERIAL DE ORIGEM LWG 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left�	�c.�@Top����N��@Width�j����@Height�	K<�l2�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats        
TfrxReportfrxWET_CURTI_026_03_A_095x220Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeftLTopXDatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Heightj��5�L&�@Top�	�c.�@Widthh��s���@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left�����@Top���a���@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left�����@Top�2���V��@Width�s��[�j�@Height�L&��d2�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Lefts��e��@Top�#:�\.�@WidthO����j�@Height$��L��N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	LeftX��0_^,�@Top�	�c.�@Width˾
t���@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	LeftX��0_^,�@Topj�t��@Width˾
t���@HeightL�ɱL��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."GraGruX"] 
ParentFontRotationWordWrapFormats     TfrxMemoViewMemo14AllowVectorExport	LeftȞ�l�`,�@Top>�
Y�j�@Width˾
t���@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width�5������@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@Top�/��"��@Width�5������@HeightAE8�/g2�@DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@Width}>������@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top'N�w(
 �@Width x����j�@HeightV�������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@Top��鲘�h�@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Heightz�^�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top'N�w(
 �@Width pч6�@HeightV�������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@Top��鲘�h�@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Heightz�^�����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@Top6v�ꭁ��@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@Top6v�ꭁ��@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@HeightS��/�N�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�/qܐA�@Width�j����@Height����N��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotationFormats     TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�����@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Top.�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo15AllowVectorExport	Left�j����@Width�	�c.�@Height�\��ʾۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WMATERIAL DE ORIGEM LWG 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left�	�c.�@Top����N��@Width�j����@Height�	K<�l2�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats        
TfrxReportfrxWET_CURTI_026_03_B_095x220Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeftLTop� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height���O&�@Top�	�c.�@Widthh��s���@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left�����@Top�ƺ�A�@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left�����@Topv�}��@Width�s��[�j�@Height��z�^�׋@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Lefts��e��@Top�#:�\.�@WidthO����j�@Height4����[��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left��ǘ��H�@Top�	�c.�@Width˾
t���@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left��ǘ��H�@Top
�r߉ɖ@Width˾
t���@HeightT��Es,��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo14AllowVectorExport	Left��J�H�@Top>�
Y�j�@Width˾
t���@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width�˜�n�ۭ@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@Top�/��"��@Width!O�-˶ۭ@Height�SH�K$�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotation  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@Width�v��n�ۭ@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top'N�w(
 �@Width x����j�@Heightz�^�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@Top��鲘�h�@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Heightz�^�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top'N�w(
 �@Width pч6�@Heightz�^�����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@Top��鲘�h�@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Heightz�^�����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@Top6v�ꭁ��@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@Top6v�ꭁ��@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@Height�[�F �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top�/qܐA�@Width�j����@Heightǀ�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�����@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Topq'�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo15AllowVectorExport	Left�	�c.�@Width�	�c.�@Height�\��ʾۭ@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WMATERIAL DE ORIGEM LWG 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left�	�c.�@Top����N��@Width�j����@Height�	K<�l2�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats        
TfrxReportfrxWET_CURTI_026_03_C_095x220Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeftLTop� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height���M��σ	@Top�	�c.�@Widthh��s���@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left�����@Top�Nc��Ɍ@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left�����@Top?�[�~<�@Width�s��[�j�@Heightr$��8���@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Lefts��e��@Top�#:�\.�@WidthO����j�@Height7�a��%�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	Left��ǘ��H�@Top�	�c.�@Width�j����@Height��/��f��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	Left��ǘ��H�@Top
�r߉ɖ@WidthуPs��@Height��g*@k6�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W8[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] 
ParentFontRotationWordWrapVAlignvaCenterFormats     TfrxMemoViewMemo14AllowVectorExport	Left��J�H�@Top>�
Y�j�@Width�j����@Height����W��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width>�
Y�j�@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�\��ʾۭ@Top�/��"��@Width�{<£j�@HeightAE8�/g2�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo12AllowVectorExport	Left��T��ۭ@Top�	�c.�@Width>�
Y�j�@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left����=�T�@Top�\��ʾۭ@Width˾
t���@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo29AllowVectorExport	Left�	�c.�@Top�j����@Width x����j�@Height^����z��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Leftn�fS�q.�@Top��	K<�L�@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�R1�Rj.�@Top�\��ʾۭ@WidthH�;���j�@Height^����z��@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left����=�T�@Top�j����@Width pч6�@Height^����z��@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left��HB	�T�@Top��	K<�L�@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ޣ��T�@Top�\��ʾۭ@Width'�l���@Height^����z��@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�	�c.�@Top���QI��@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8WPeso kg 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left����=�T�@Top���QI��@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeso">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxShapeViewShape4AllowVectorExport	Left6v�ꭁ��@Top�\��ʾۭ@Width�	�c.�@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�	�c.�@Top�\��ʾۭ@Width�j����@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	Left�	�c.�@TopK̒�mY��@Width�j����@Height��鲘�h�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftBottom 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left$��q��@Top����N��@Width׬mi�\.�@Height�[�F �@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�	�c.�@Top
��m�k-�@Width�j����@Height�UH�I�׋@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo3AllowVectorExport	LeftE*�-9�@Top����N��@Width�j����@Height�j����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo48AllowVectorExport	Left�����@Top�	�c.�@WidthOo"Κj�@HeightUގpZ�r�@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotationVAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left��	K<�L�@Topq'�&�@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack     
TfrxReportfrxWET_CURTI_026_03_D_095x220Version2022.1DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate x�K˅�@ReportOptions.LastChange x�K˅�@ScriptLanguagePascalScriptScriptText.Strings:procedure MasterData1OnBeforePrint(Sender: TfrxComponent);beginZ  Barcode2D1.Text := <VARF_QRCODE_1>;                                                     end;    begin    end. 
OnGetValuefrxWET_CURTI_026_01_AGetValueLeftLTop� DatasetsDataSetDModG.frxDsDonoDataSetName	frxDsDono DataSetfrxDsEstqR5DataSetNamefrxDsEstqR5  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight       �@	PaperSize 	Frame.Typ 
MirrorMode  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height���O&�@Top�	�c.�@Widthh��s���@OnBeforePrintMasterData1OnBeforePrintDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5RowCount  TfrxMemoViewMemo2AllowVectorExport	Left�����@TopcX��A�@Width�s��[�j�@Height���dH_.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8WData 
ParentFontRotation  TfrxMemoViewMemo8AllowVectorExport	Left�����@Top�莓�'��@Width�s��[�j�@Heightz~���n�@DisplayFormat.FormatStrdd/mm/yyyy hh:nnDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[frxDsEstqR5."DataHora"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo30AllowVectorExport	Lefts��e��@Top�#:�\.�@WidthO����j�@Height�N���d�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftLeftftRightftTopftBottom 
Memo.UTF8W[VARF_EMPNOME] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo10AllowVectorExport	Left�\��ʾۭ@Top�	�c.�@Width>�
Y�j�@HeightM�O����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WFabricado por: 
ParentFontRotationVAlignvaCenter  TfrxShapeViewShape1AllowVectorExport	LeftE*�-9�@Top�	�c.�@Width�j����@Height�l6��f��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo22AllowVectorExport	LeftE*�-9�@Top����*��@Width�����T�@Height�SH�K$�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8WR[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"] [frxDsEstqR5."NO_PALLET"] 
ParentFontRotationFormats      TfrxMemoViewMemo14AllowVectorExport	Left$iq�">�@Top>�
Y�j�@Width�j����@Height�	�c.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WArtigo  espessura  cor 
ParentFontVAlignvaCenter  TfrxShapeViewShape2AllowVectorExport	Left�UH�I�׋@Top�	�c.�@Width֨�ht��@Height���h o��@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo1AllowVectorExport	Left�UH�I�׋@TopI�2��v�@Width����̀�@HeightbXr�.�@	DataFieldPalletDataSetfrxDsEstqR5DataSetNamefrxDsEstqR5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."Pallet"] 
ParentFontRotationWordWrap  TfrxMemoViewMemo12AllowVectorExport	Left��醿׋@Top�	�c.�@Width֨�ht��@Height>�
Y�j�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8WPallet 
ParentFontVAlignvaCenter  TfrxShapeViewShape3AllowVectorExport	Left��	K<�L�@Top�\��ʾۭ@Width˾
t���@Height�<I�f���@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape4AllowVectorExport	Left���u��@Top�\��ʾۭ@Width.�&�@Height2��Y��f�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxShapeViewShape5AllowVectorExport	Left�j����@Top�\��ʾۭ@Width�	�c.�@Height2��Y��f�@Curve	Frame.Typ ShapeskRoundRectangle  TfrxMemoViewMemo47AllowVectorExport	Left�j����@TopK̒�mY��@Width�	�c.�@Height��	K<�L�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftBottom 
Memo.UTF8W[VARF_FORNECEDORES] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo11AllowVectorExport	Left�j����@Top���%3�@Width�	�c.�@HeightUގpZ�r�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ 
Memo.UTF8W[VARF_REVISORES] 
ParentFontRotation  TfrxMemoViewMemo48AllowVectorExport	Left>�
Y�j�@Top�n��\�Ͽ@Widthh�H�?<��@Height����W��@DisplayFormat.DecimalSeparator,DisplayFormat.FormatStrdd/mm/yyyy hh:nn:ssDisplayFormat.Kind
fkDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[VARF_DATA] 
ParentFontRotation� VAlignvaCenter  TfrxBarcode2DView
Barcode2D1AllowVectorExport	Left �
Y�j�@Top ���W��@Width       �@Height       �@StretchModesmActualHeightBarTypebcCodeQRBarProperties.EncodingqrAutoBarProperties.QuietZone BarProperties.ErrorLevelsecLBarProperties.PixelSizeBarProperties.CodePage 	Frame.Typ Rotation ShowTextHexData 31003200330034003500360037003800Zoom       ��?
FontScaled		QuietZone ColorBarclBlack  TfrxMemoViewMemo29AllowVectorExport	Left�N=�T�@Top'N�w(
 �@Width x����j�@HeightV�������@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W
   Área ft² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo4AllowVectorExport	Left�Q͓�T�@Top��鲘�h�@Width x����j�@Height���r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftLeftftRightftTopftBottom HAlignhaCenter
Memo.UTF8W   Peças 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo7AllowVectorExport	Left�b���T�@Top�\��ʾۭ@WidthH�;���j�@Heightz�^�����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.TypftBottom HAlignhaCenter
Memo.UTF8W	   Área m² 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo24AllowVectorExport	Left�@��;�L�@Top'N�w(
 �@Width pч6�@HeightV�������@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."AreaP2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo26AllowVectorExport	Left�CI/�L�@Top��鲘�h�@Width pч6�@Height���r�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8WK[FormatFloat('#,###,###.###;-#,###,###.###; ', <frxDsEstqR5."PalVrtPeca">)] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo28AllowVectorExport	Left��ސ��L�@Top�\��ʾۭ@Width'�l���@Heightz�^�����@DisplayFormat.FormatStr%2.2nDisplayFormat.Kind	fkNumericFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRightftBottom Frame.Width       ��?HAlignhaCenter
Memo.UTF8W[frxDsEstqR5."PalVrtArM2"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo6AllowVectorExport	Left�N=�T�@Top���QI��@Width x����j�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaCenter
Memo.UTF8W[TitPeso_kg] 
ParentFontRotationVAlignvaCenter  TfrxMemoViewMemo27AllowVectorExport	Left�@��;�L�@Top����e��@Width pч6�@Height׬mi�\.�@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.TypftRight HAlignhaCenter
Memo.UTF8W[ValPeso_kg] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo9AllowVectorExport	Left����W��@Top����N��@Width���L�J��@Height���_���@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?HAlignhaCenter
Memo.UTF8W4[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"] 
ParentFontRotationWordWrapVAlignvaCenter  TfrxMemoViewMemo3AllowVectorExport	Left �{�T��@Top ���N��@Width h����@Height h����@Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.StylefsBold 	Frame.Typ Frame.Width       ��?
Memo.UTF8WCliente: 
ParentFontRotationVAlignvaCenter      