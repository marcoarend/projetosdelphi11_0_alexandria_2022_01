unit VSClassifOneNw2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, UnInternalConsts,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkDBLookupComboBox, dmkEditCB, UnDmkWeb, ShellAPI,
  UnProjGroup_Consts, mySQLDirectQuery, dmkCheckBox, UnAppEnums;

type
  TFLsItens = array of array[0..4] of String;
  //TFLsSumPall = array of array[0..3] of String;
  //
  TFmVSClassifOneNw2 = class(TForm)
    PanelOC: TPanel;
    PnBoxesAll: TPanel;
    PnBoxesT01: TPanel;
    PnBoxesT02: TPanel;
    PnBoxT1L1: TPanel;
    Panel2: TPanel;
    QrVSPaClaCab: TmySQLQuery;
    DsVSPaClaCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    DsVSGerArtNew: TDataSource;
    QrVSPaClaCabNO_TIPO: TWideStringField;
    QrVSPaClaCabGraGruX: TIntegerField;
    QrVSPaClaCabNome: TWideStringField;
    QrVSPaClaCabTipoArea: TSmallintField;
    QrVSPaClaCabEmpresa: TIntegerField;
    QrVSPaClaCabMovimCod: TIntegerField;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaClaCabNO_EMPRESA: TWideStringField;
    DsSumT: TDataSource;
    PnBoxT1L2: TPanel;
    DBGAll: TdmkDBGridZTO;
    DsAll: TDataSource;
    PnBoxT1L3: TPanel;
    PnBoxT2L3: TPanel;
    PnBoxT2L2: TPanel;
    PnBoxT2L1: TPanel;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    Panel7: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    Panel12: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEDAreaT: TDBEdit;
    Panel13: TPanel;
    Label19: TLabel;
    Panel14: TPanel;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdit24: TDBEdit;
    PMItens01: TPopupMenu;
    EncerrarPallet01: TMenuItem;
    RemoverPallet01: TMenuItem;
    AdicionarPallet01: TMenuItem;
    PMItens02: TPopupMenu;
    AdicionarPallet02: TMenuItem;
    EncerrarPallet02: TMenuItem;
    RemoverPallet02: TMenuItem;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    BtEncerra: TBitBtn;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    PMItens03: TPopupMenu;
    AdicionarPallet03: TMenuItem;
    EncerrarPallet03: TMenuItem;
    RemoverPallet03: TMenuItem;
    PMItens04: TPopupMenu;
    AdicionarPallet04: TMenuItem;
    EncerrarPallet04: TMenuItem;
    RemoverPallet04: TMenuItem;
    PMItens05: TPopupMenu;
    AdicionarPallet05: TMenuItem;
    EncerrarPallet05: TMenuItem;
    RemoverPallet05: TMenuItem;
    PMItens06: TPopupMenu;
    AdicionarPallet06: TMenuItem;
    EncerrarPallet06: TMenuItem;
    RemoverPallet06: TMenuItem;
    BtImprime: TBitBtn;
    BtDigitacao: TButton;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrVMIsDePal: TmySQLQuery;
    QrVMIsDePalVMI_Dest: TIntegerField;
    Panel42: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    Label22: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label24: TLabel;
    Label18: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit11: TDBEdit;
    Panel43: TPanel;
    Label71: TLabel;
    Label70: TLabel;
    CBRevisor: TdmkDBLookupComboBox;
    CBDigitador: TdmkDBLookupComboBox;
    EdDigitador: TdmkEditCB;
    EdRevisor: TdmkEditCB;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSMrtCad: TmySQLQuery;
    DsVSMrtCad: TDataSource;
    QrVSMrtCadCodigo: TIntegerField;
    QrVSMrtCadNome: TWideStringField;
    PnMartelo: TPanel;
    Label72: TLabel;
    CBVSMrtCad: TdmkDBLookupComboBox;
    EdVSMrtCad: TdmkEditCB;
    BtReabre: TBitBtn;
    ImprimirNmeroPallet1: TMenuItem;
    DadosPaletsemWordPad1: TMenuItem;
    Mensagemdedados1: TMenuItem;
    Mensagemdedados2: TMenuItem;
    Mensagemdedados3: TMenuItem;
    Mensagemdedados4: TMenuItem;
    Mensagemdedados5: TMenuItem;
    Mensagemdedados6: TMenuItem;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    Label85: TLabel;
    EdTempo: TEdit;
    N3: TMenuItem;
    rocarVichaRMP1: TMenuItem;
    CkMartelo: TCheckBox;
    CkSubClass: TCheckBox;
    PnSubClass: TPanel;
    Label86: TLabel;
    EdSubClass: TdmkEdit;
    Panel1: TPanel;
    PnEqualize: TPanel;
    Panel70: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    CkNota: TCheckBox;
    DBGrid2: TDBGrid;
    PnNota: TPanel;
    DBEdit42: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnBox01: TPanel;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label4: TLabel;
    Label54: TLabel;
    DBEdControle01: TDBEdit;
    DBEdIMEI01: TDBEdit;
    GroupBox2: TGroupBox;
    Panel16: TPanel;
    Label52: TLabel;
    Label55: TLabel;
    Label53: TLabel;
    Label73: TLabel;
    EdMedia01: TdmkEdit;
    Memo1: TMemo;
    Panel4: TPanel;
    Panel8: TPanel;
    Panel11: TPanel;
    DBEdit3: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit1: TDBEdit;
    Panel50: TPanel;
    Label79: TLabel;
    Panel51: TPanel;
    Panel64: TPanel;
    CkSubClass1: TCheckBox;
    PnBox04: TPanel;
    Panel33: TPanel;
    GroupBox7: TGroupBox;
    Panel36: TPanel;
    Label47: TLabel;
    Label51: TLabel;
    GroupBox8: TGroupBox;
    Panel37: TPanel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label76: TLabel;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel47: TPanel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit51: TDBEdit;
    Panel48: TPanel;
    Label81: TLabel;
    Panel49: TPanel;
    Panel61: TPanel;
    CkSubClass4: TCheckBox;
    PnBox06: TPanel;
    PnBox03: TPanel;
    Panel22: TPanel;
    GroupBox5: TGroupBox;
    Panel34: TPanel;
    Label32: TLabel;
    Label36: TLabel;
    GroupBox6: TGroupBox;
    Panel35: TPanel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label75: TLabel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel55: TPanel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit30: TDBEdit;
    Panel56: TPanel;
    Label83: TLabel;
    Panel57: TPanel;
    Panel65: TPanel;
    CkSubClass3: TCheckBox;
    Panel25: TPanel;
    GroupBox11: TGroupBox;
    Panel40: TPanel;
    Label37: TLabel;
    Label41: TLabel;
    GroupBox12: TGroupBox;
    Panel41: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label78: TLabel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel58: TPanel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit37: TDBEdit;
    Panel59: TPanel;
    Label84: TLabel;
    Panel60: TPanel;
    Panel66: TPanel;
    CkSubClass6: TCheckBox;
    PnBox02: TPanel;
    PnBox05: TPanel;
    Panel19: TPanel;
    GroupBox3: TGroupBox;
    Panel26: TPanel;
    Label5: TLabel;
    Label30: TLabel;
    GroupBox4: TGroupBox;
    Panel30: TPanel;
    Label31: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label74: TLabel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel44: TPanel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit22: TDBEdit;
    Panel45: TPanel;
    Label80: TLabel;
    Panel46: TPanel;
    Panel62: TPanel;
    CkSubClass2: TCheckBox;
    Panel29: TPanel;
    GroupBox9: TGroupBox;
    Panel38: TPanel;
    Label42: TLabel;
    Label46: TLabel;
    GroupBox10: TGroupBox;
    Panel39: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label77: TLabel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel52: TPanel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit44: TDBEdit;
    Panel53: TPanel;
    Label82: TLabel;
    Panel54: TPanel;
    Panel63: TPanel;
    CkSubClass5: TCheckBox;
    rocarIMEI1: TMenuItem;
    QrVSGerArtNewFatorInt: TFloatField;
    PnIntMei01: TPanel;
    PnIntMei04: TPanel;
    PnIntMei02: TPanel;
    PnIntMei05: TPanel;
    PnIntMei06: TPanel;
    PnIntMei03: TPanel;
    QrSumT: TmySQLQuery;
    QrSumTAreaM2: TFloatField;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTPecas: TFloatField;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoIMEI2: TMenuItem;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    SGAll: TStringGrid;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    DqAll: TmySQLDirectQuery;
    EdAll: TdmkEdit;
    Memo2: TMemo;
    BitBtn2: TBitBtn;
    EdArrAll: TdmkEdit;
    CkUsaMemory: TdmkCheckBox;
    DqItens: TmySQLDirectQuery;
    SGItens01: TStringGrid;
    SGItens02: TStringGrid;
    SGItens03: TStringGrid;
    SGItens04: TStringGrid;
    SGItens05: TStringGrid;
    SGItens06: TStringGrid;
    EdItens: TdmkEdit;
    DqSumPall: TmySQLDirectQuery;
    EdPecas01: TdmkEdit;
    EdArea01: TdmkEdit;
    EdPecas02: TdmkEdit;
    EdArea02: TdmkEdit;
    EdMedia02: TdmkEdit;
    EdPecas03: TdmkEdit;
    EdArea03: TdmkEdit;
    EdMedia03: TdmkEdit;
    EdPecas04: TdmkEdit;
    EdArea04: TdmkEdit;
    EdMedia04: TdmkEdit;
    EdPecas05: TdmkEdit;
    EdArea05: TdmkEdit;
    EdMedia05: TdmkEdit;
    EdPecas06: TdmkEdit;
    EdArea06: TdmkEdit;
    EdMedia06: TdmkEdit;
    DBEdPallet01: TDBEdit;
    DBEdPallet02: TDBEdit;
    DBEdPallet03: TDBEdit;
    DBEdPallet04: TDBEdit;
    DBEdPallet05: TDBEdit;
    DBEdPallet06: TDBEdit;
    DBEdControle02: TDBEdit;
    DBEdIMEI02: TDBEdit;
    DBEdControle03: TDBEdit;
    DBEdIMEI03: TDBEdit;
    DBEdControle04: TDBEdit;
    DBEdIMEI04: TDBEdit;
    DBEdControle05: TDBEdit;
    DBEdIMEI05: TDBEdit;
    DBEdControle06: TDBEdit;
    DBEdIMEI06: TDBEdit;
    QrVSGerArtNewClientMO: TIntegerField;
    QrVSGerArtNewFornecMO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaClaCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDigitacaoClick(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtReabreClick(Sender: TObject);
    procedure ImprimirNmeroPallet1Click(Sender: TObject);
    procedure DadosPaletsemWordPad1Click(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure rocarVichaRMP1Click(Sender: TObject);
    procedure CkSubClassClick(Sender: TObject);
    procedure CkMarteloClick(Sender: TObject);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    procedure EdSubClassExit(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure rocarIMEI1Click(Sender: TObject);
    procedure QrVSGerArtNewAfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoIMEI1Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoIMEI2Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkUsaMemoryClick(Sender: TObject);
    procedure SGAllKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdPecasXXChange(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    FDifTime: TDateTime;
    FLsAll: array of array[0..12] of String;
    FMartelosCod: array of Integer;
    FMartelosNom: array of String;
    //
    //FLsSumPall: array [0..VAR_CLA_ART_RIB_MAX_BOX_06] of TFLsSumPall;
    FMaxPecas: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of Double;
    FSGItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_06] of TStringGrid;
    FLsItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_06] of TFLsItens;
    FEdPecas, FEdArea, FEdMedia: array [0..VAR_CLA_ART_RIB_MAX_BOX_06] of TdmkEdit;
    FQrVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TmySQLQuery;
    FDsVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TDataSource;
    FQrVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TmySQLQuery;
    FDsVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TDataSource;
    FDBEdPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TDBEdit;
    FPalletDoBox: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TMenuItem;
    FPnBox, FPnIntMei: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TPanel;
    FDBEdControle: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TDBEdit;
    FDBEdIMEI: array[0..VAR_CLA_ART_RIB_MAX_BOX_06] of TDBEdit;
    //
(*
    FLast_vsgerarta, FLast_vspaclacaba, FLast_LstPal01, FLast_LstPal02,
    FLast_LstPal03, FLast_LstPal04, FLast_LstPal05, FLast_LstPal06: Double;
*)
    //
    function  EncerraOC(Forca: Boolean): Boolean;
    procedure RealinhaBoxes();
    procedure ReopenVSGerArtDst();
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalClaIts:
              TmySQLQuery; Pallet: Integer);
    procedure ReopenItens(Box, VSPaClaIts, VSPallet: Integer);
    procedure InsereCouroAtual();
    procedure InsereCouroArrAll(CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri,
              VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
              Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
              Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
              FatorIntDst: Double; Controle: Int64);
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure AtualizaInfoOC();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    function  BoxDeComp(Compo: TObject): Integer;
    function  ObtemDadosBox(const Box: Integer; var VSPaClaIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet: TmySQLQuery): Boolean;
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure AdicionaPallet(Box: Integer);
    procedure ReopenSumPal((*QrSumPal: TmySQLQuery;*) Box, VSPallet: Integer);
    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure LiberaDigitacao();
    function  SubstituiOC(): Boolean;
    procedure VerificaBoxes();
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenEqualize(Automatico: Boolean);
    procedure TentarFocarEdArea();
    procedure ConfigPnSubClass();
    procedure ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoIMEI();
    //
    procedure ReopenVSMrtCad();
    procedure ReopenAll();
    procedure CarregaSGAll();
    procedure CarregaSGItens(Box: Integer);
    procedure CarregaSGSumPall(Box: Integer; Acao: TAcaoMath; Pecas,
              AreaM2, AreaP2: Double);
    function  ObtemNO_MARTELO(Martelo: Integer): String;

  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    //
    procedure ReopenVSPaClaCab();
  end;

var
  FmVSClassifOneNw2: TFmVSClassifOneNw2;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAdd,
  MyDBCheck, UnVS_PF, VSClaArtPrpMDz, VSPalNumImp, UnDmkProcFunc;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX_06] of Boolean;
  FUsaMemory: Boolean = True;

procedure TFmVSClassifOneNw2.AdicionaPallet(Box: Integer);
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAdd, FmVSClaArtPalAdd, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAdd.FMovimIDGer              := emidClassArtXXUni;
    FmVSClaArtPalAdd.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAdd.FFornecMO                := QrVSGerArtNewFornecMO.Value;
    FmVSClaArtPalAdd.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAdd.FFornecedor              := QrVSGerArtNewTerceiro.Value;
    FmVSClaArtPalAdd.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAdd.FMovimID                 := FMovimID;
    FmVSClaArtPalAdd.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAdd.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAdd.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAdd.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAdd.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAdd.EdCodigo.ValueVariant    := QrVSPaClaCabCodigo.Value;
    FmVSClaArtPalAdd.EdSrcPallet.ValueVariant := 0;
    FmVSClaArtPalAdd.FExigeSrcPallet          := False;
    FmVSClaArtPalAdd.EdMovimCod.ValueVariant  := QrVSPaClaCabMovimCod.Value;
    FmVSClaArtPalAdd.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAdd.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAdd.FBox := Box;
    FmVSClaArtPalAdd.PnBox.Caption := Geral.FF0(Box);
    //
    FmVSClaArtPalAdd.FPal01 := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPalAdd.FPal02 := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPalAdd.FPal03 := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPalAdd.FPal04 := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPalAdd.FPal05 := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPalAdd.FPal06 := QrVSPaClaCabLstPal06.Value;
    //
    VS_PF.ReopenVSPallet(FmVSClaArtPalAdd.QrVSPallet,
      FmVSClaArtPalAdd.FEmpresa, FmVSClaArtPalAdd.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAdd.ShowModal;
    FmVSClaArtPalAdd.Destroy;
    //
    ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNw2.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNw2.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSClassifOneNw2.AtualizaInfoOC();
var
  AreaT: Double;
begin
  if not FUsaMemory then
    ReopenAll();
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT Pecas, AreaM2, SdoVrtPeca, SdoVrtArM2',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(QrVSGerArtNewControle.Value),
  '']);
  //
end;

procedure TFmVSClassifOneNw2.BitBtn1Click(Sender: TObject);
begin
  ReopenAll();
end;

procedure TFmVSClassifOneNw2.BitBtn2Click(Sender: TObject);
var
  I, J: Integer;
begin
  I := SGAll.RowCount;
  SGAll.RowCount := I + 1;
  for I := 1 to SGAll.RowCount do
  begin
    for J := 1 to SGAll.ColCount do
    begin
      SGAll.Cells[J, I + 1] := SGAll.Cells[J, I];
    end;
  end;
end;

function TFmVSClassifOneNw2.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  Result := Geral.IMV(CompName[Length(CompName)]);
end;

function TFmVSClassifOneNw2.BoxInBoxes(Box: Integer): Boolean;
(*
var
  I: Integer;
*)
begin
(*
  Result := False;
  if Box <> 0 then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if FBoxes[I] then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
*)
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX_06) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSClassifOneNw2.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSClassifOneNw2.BtImprimeClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, 4);
end;

procedure TFmVSClassifOneNw2.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNw2.CarregaSGAll();
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsAll);
  SGAll.RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    SGAll.Cells[00, J] := IntToStr(K - J + 1);
    SGAll.Cells[01, J] := FLsAll[I, 01];   // Box
    SGAll.Cells[02, J] := FLsAll[I, 02];   // SubClass
    SGAll.Cells[03, J] := FLsAll[I, 03];   // AreaM2
    SGAll.Cells[04, J] := FLsAll[I, 04];   // NO_MARTELO
    SGAll.Cells[05, J] := FLsAll[I, 05];   // VMI_Dest
    SGAll.Cells[06, J] := FLsAll[I, 06];   // Controle
    SGAll.Cells[07, J] := FLsAll[I, 07];   // VSPaClaIts
    SGAll.Cells[08, J] := FLsAll[I, 08];   // VSPallet
    SGAll.Cells[09, J] := FLsAll[I, 09];   // Pecas
    SGAll.Cells[10, J] := FLsAll[I, 10];   // AreaP2
    SGAll.Cells[11, J] := FLsAll[I, 11];   // VMI_Sorc
    SGAll.Cells[12, J] := FLsAll[I, 12];   // VMI_Dest
  end;
end;

procedure TFmVSClassifOneNw2.CarregaSGItens(Box: Integer);
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsItens[Box]);
  FSGItens[Box].RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    FSGItens[Box].Cells[00, J] := IntToStr(K - J + 1);
    case QrVSPaClaCabTipoArea.Value of
      0: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 03];
      1: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 04];
      else FSGItens[Box].Cells[01, J] := '?.??';
    end;
(*
    SGItens[Box].Cells[01, J] := FLsItens[Box][I, 01];   // Controle
    SGItens[Box].Cells[02, J] := FLsItens[Box][I, 02];   // Pecas
    SGItens[Box].Cells[03, J] := FLsItens[Box][I, 03];   // AreaM2
    SGItens[Box].Cells[04, J] := FLsItens[Box][I, 04];   // AreaP2
*)
  end;
end;

procedure TFmVSClassifOneNw2.CarregaSGSumPall(Box: Integer; Acao: TAcaoMath;
  Pecas, AreaM2, AreaP2: Double);
var
  Area, P, A, M: Double;
begin
  if QrVSPaClaCabTipoArea.Value = 0 then
    Area := AreaM2
  else
    Area := AreaP2;
  //
  case Acao of
    amathZera:
    begin
      P := 0;
      A := 0;
    end;
    amathSubtrai:
    begin
      P := FEdPecas[Box].ValueVariant - Pecas;
      A := FEdArea[Box].ValueVariant - Area;
    end;
    amathSoma:
    begin
      P := FEdPecas[Box].ValueVariant + Pecas;
      A := FEdArea[Box].ValueVariant + Area;
    end;
    amathSubstitui:
    begin
      P := Pecas;
      A := Area;
    end;
  end;
  if P = 0 then
    M := 0
  else
    M := A / P;
  //
  FEdPecas[Box].ValueVariant := P;
  FEdArea[Box].ValueVariant  := A;
  FEdMedia[Box].ValueVariant := M;
(*
    SGSumPall[Box].Cells[02, J] := FLsSumPall[Box][I, 01];   // Pecas
    SGSumPall[Box].Cells[03, J] := FLsSumPall[Box][I, 02];   // AreaM2
    SGSumPall[Box].Cells[04, J] := FLsSumPall[Box][I, 03];   // AreaP2
*)
end;

procedure TFmVSClassifOneNw2.CkMarteloClick(Sender: TObject);
begin
  PnMartelo.Visible := CkMartelo.Checked;
end;

procedure TFmVSClassifOneNw2.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSClassifOneNw2.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw2.CkSubClassClick(Sender: TObject);
begin
  ConfigPnSubClass();
end;

procedure TFmVSClassifOneNw2.CkUsaMemoryClick(Sender: TObject);
begin
  FUsaMemory := CkUsaMemory.Checked;
end;

procedure TFmVSClassifOneNw2.ConfigPnSubClass();
begin
  if not CkSubClass.Checked then
    CkSubClass.Checked :=
    (CkSubClass1.Checked and PnBox01.Visible) or
    (CkSubClass2.Checked and PnBox02.Visible)  or
    (CkSubClass3.Checked and PnBox03.Visible)  or
    (CkSubClass4.Checked and PnBox04.Visible)  or
    (CkSubClass5.Checked and PnBox05.Visible)  or
    (CkSubClass6.Checked and PnBox06.Visible) ;
  //
  PnSubClass.Visible := CkSubClass.Checked;
end;

procedure TFmVSClassifOneNw2.DadosPaletsemWordPad1Click(Sender: TObject);
(*
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    ReAviso.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
*)
var
  Dir, NomeArq, Arq: String;
begin
  Dir := 'C:\Dermatek\OCsCapt\';
  NomeArq := 'OC' + DBEdCodigo.Text +
    Geral.SoNumero_TT(Geral.FDT(DModG.ObtemAgora(), 109)) + '.jpg';
  try
    Arq := DmkWeb.CapturaTela(Dir, NomeArq);
    ShellExecute(Application.Handle, 'open', pchar(Arq), nil, nil, sw_ShowNormal);
  except
    ForceDirectories(Dir);
    raise;
  end;
end;

procedure TFmVSClassifOneNw2.BtDigitacaoClick(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Ant, Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
  Boxes: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdit21.Text;
  //
  if InputQuery('Itens a classificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    Boxes := 0;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    begin
      if FBoxes[I] then
        Boxes := Boxes + 1;
    end;
    //
    Ant := 0;
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Trunc(Random(1000) / 166.666); //  Random(6)
        if BoxInBoxes(Box) then
        begin
          if ((Boxes > 2) and (Ant <> Box))
          or (Boxes <=2) then
          begin
            OK := True;
            EdBox.ValueVariant := Box;
            Ant := Box;
          end;
        end;
      end;
      if QrSumTSdoVrtPeca.Value > 1 then
      begin
        Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
        Randomize;
        Desvio := Random(3000);
        Fator := 1 + ((Desvio - 1500) / 10000);
        Area := Area * Fator;
      end else
        Area := QrSumTSdoVrtArM2.Value;
      EdArea.ValueVariant := Area * 100;
      InsereCouroAtual();
    end;
  end;
end;

procedure TFmVSClassifOneNw2.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
  //if (EdBox.ValueVariant > 0) and (EdBox.ValueVariant <= VAR_CLA_ART_RIB_MAX_BOX) then
    if PnSubClass.Visible then
      EdSubClass.SetFocus
    else
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end else
  if (EdBox.Text = '-') then
    UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNw2.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNw2.EdPecasXXChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := Geral.IMV(Copy(TdmkEdit(Sender).Name, 8));
  if TdmkEdit(Sender).ValueVariant >= FMaxPecas[Box] then
  begin
    TdmkEdit(Sender).Font.Color := clRed;
    TdmkEdit(Sender).Color := clYellow;
  end else
  begin
    TdmkEdit(Sender).Font.Color := clBlue;
    TdmkEdit(Sender).Color := clWindow;
  end;
end;

procedure TFmVSClassifOneNw2.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNw2.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSClassifOneNw2.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSClassifOneNw2.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
begin
(*
  if Key = VK_RETURN then
  begin
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
*)
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
    end;
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
end;

procedure TFmVSClassifOneNw2.EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereCouroAtual();
end;

function TFmVSClassifOneNw2.EncerraOC(Forca: Boolean): Boolean;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
var
  DtHrFimCla: String;
  Codigo, I, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
begin
  Result := False;
  if Forca or (Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES) then
  begin
    { DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaClaCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    }
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    Codigo     := QrVSGerArtNewCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      //'DtHrFimCla'], ['Codigo'], [DtHrFimCla
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06
      ], [Codigo], True);
(*
      then
      begin
        if not SubstituiOC() then
          Close;
      end;
*)
    end;
  end;

end;

function TFmVSClassifOneNw2.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  FromBox: Variant;
begin
  Result := False;
  //
  FromBox := Box;
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  Result := VS_PF.EncerraPalletNew(VSPallet, Pergunta);
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNw2.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNw2.EstaOCOrdemdeclassificao1Click(Sender: TObject);
begin
  if EncerraOC(False) then
        if not SubstituiOC() then
          Close;
end;

procedure TFmVSClassifOneNw2.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNw2.UpdDelCouroSelecionado(SQLTipo: TSQLType);
(*
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt: String;
  Continua: Boolean;
  //
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaClaIts := QrAllVSPaClaIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      Pecas  := 1;
      //
      if QrVSPaClaCabTipoArea.Value = 0 then
        Txt := FloatToStr(QrAllAreaM2.Value * 100)
      else
        Txt := FloatToStr(QrAllAreaP2.Value * 100);
      //
      if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
      begin
        AreaM2 := Geral.IMV(Txt);
        //
        if QrVSPaClaCabTipoArea.Value = 0 then
        begin
          AreaM2 := AreaM2 / 100;
          AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
        end else
        begin
          AreaP2 := AreaM2 / 100;
          AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
        end;
        Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                      'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                      [Controle], False);
      end;
    end;
    stDel:
    begin
      Continua := VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle,
      Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
    Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
    //
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    //
    ReopenItens(Box, All_VSPaClaIts, All_VSPallet);
    AtualizaInfoOC();
  end;
*)
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
  Pecas, AreaM2, AreaP2, AntM2, AntP2: Double;
  Txt: String;
  Continua: Boolean;
  //
  SelTop, SelBot, cIni, cFim, Count, I: Integer;
begin
  SelTop := SGAll.Selection.Top;
  SelBot := SGAll.Selection.Bottom;
  Count  := Length(FLsAll);
  cIni   := Count - SelTop + 1;
  cFim   := Count - SelBot + 1;
  Geral.MB_Info(IntToStr(cIni) + ' ate ' + IntToStr(cFim));
  //
(*
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaClaIts);                   // VSPaClaIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
*)
  //cIni := Count - cIni + 1;
  //cFim := Count - cFim + 1;
  //
  for I := cIni -1 downto cFim - 1 do
  begin
    //Box := QrAllBox.Value;
    Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
    if not ObtemQryesBox(Box, QrVSPallet) then
      Exit;
    if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
    Box_VMI_Baix, Box_VMI_Dest) then
      Exit;
    //Controle       := QrAllControle.Value;
    Controle       := Geral.I64(FLsAll[I, 06]);
    //All_VSPaClaIts := QrAllVSPaClaIts.Value;
    All_VSPaClaIts := Geral.IMV(FLsAll[I, 07]);
    //All_VSPallet   := QrAllVSPallet.Value;
    All_VSPallet   := Geral.IMV(FLsAll[I, 08]);
    //
    Continua := False;
    //
    if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
    begin
      Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do ou alterado!');
      Exit;
    end;
    case SQLTipo of
      stUpd:
      begin
        Pecas  := 1;
        //
        if QrVSPaClaCabTipoArea.Value = 0 then
          //Txt := FloatToStr(QrAllAreaM2.Value * 100)
          Txt := Geral.SoNumero_TT(FLsAll[I, 03])
        else
          //Txt := FloatToStr(QrAllAreaP2.Value * 100);
          Txt := Geral.SoNumero_TT(FLsAll[I, 10]);
        //
        if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
        begin
          AreaM2 := Geral.IMV(Txt);
          //
          if QrVSPaClaCabTipoArea.Value = 0 then
          begin
            AreaM2 := AreaM2 / 100;
            AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
          end else
          begin
            AreaP2 := AreaM2 / 100;
            AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
          end;
          AntM2  := Geral.DMV_Dot(FLsAll[I, 03]);
          AntP2  := Geral.DMV_Dot(FLsAll[I, 03]);
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                        'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                        [Controle], False);
          FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);
          FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);
          //
          CarregaSGSumPall(Box, amathSubtrai, Pecas, AntM2, AntP2);
          CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
          //
        end;
      end;
      stDel:
      begin
        Continua := VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle,
        Dmod.MyDB) = ID_YES;
        CarregaSGSumPall(Box, amathSubtrai, Pecas, AreaM2, AreaP2);
      end;
      else
        Geral.MB_Erro('N�o implementado!');
    end;
    if Continua then
    begin
      VS_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
      Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      //
      if PnDigitacao.Enabled then
      begin
        EdBox.ValueVariant  := 0;
        EdArea.ValueVariant := 0;
        EdVSMrtCad.Text     := '';
        CBVSMrtCad.KeyValue := Null;
        EdArea.SetFocus;
        EdArea.SelectAll;
      end;
      //
      ReopenItens(Box, All_VSPaClaIts, All_VSPallet);
      AtualizaInfoOC();
    end;
  end;
  ReopenAll();
end;

procedure TFmVSClassifOneNw2.VerificaBoxes();
begin
(*
  if not Mostrartodosboxes1.Checked then
  begin
    PnBoxT1L1.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBoxT1L2.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBoxT1L3.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBoxT2L1.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBoxT2L2.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBoxT2L3.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBoxT1L1.Visible := True;
    PnBoxT1L2.Visible := True;
    PnBoxT1L3.Visible := True;
    PnBoxT2L1.Visible := True;
    PnBoxT2L2.Visible := True;
    PnBoxT2L3.Visible := True;
  end;
*)
  if not Mostrartodosboxes1.Checked then
  begin
    PnBox01.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBox02.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBox03.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBox04.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBox05.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBox06.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBox01.Visible := True;
    PnBox02.Visible := True;
    PnBox03.Visible := True;
    PnBox04.Visible := True;
    PnBox05.Visible := True;
    PnBox06.Visible := True;
  end;
  if ((PnBoxT1L3.Visible = False) or (PnBoxT1L1.Visible = False)) and (PnBoxT1L2.Visible = True) then
    PnBoxT1L2.Align := alLeft
  else
    PnBoxT1L2.Align := alClient;
  //
  if ((PnBoxT2L3.Visible = False) or (PnBoxT2L1.Visible = False)) and (PnBoxT2L2.Visible = True) then
    PnBoxT2L2.Align := alLeft
  else
    PnBoxT2L2.Align := alClient;
  //
  RealinhaBoxes();
end;

procedure TFmVSClassifOneNw2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSClassifOneNw2.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriando := True;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  FDBEdPallet[01] := DBEdPallet01;
  FDBEdPallet[02] := DBEdPallet02;
  FDBEdPallet[03] := DBEdPallet03;
  FDBEdPallet[04] := DBEdPallet04;
  FDBEdPallet[05] := DBEdPallet05;
  FDBEdPallet[06] := DBEdPallet06;
  //
  FDBEdControle[01] := DBEdControle01;
  FDBEdControle[02] := DBEdControle02;
  FDBEdControle[03] := DBEdControle03;
  FDBEdControle[04] := DBEdControle04;
  FDBEdControle[05] := DBEdControle05;
  FDBEdControle[06] := DBEdControle06;
  //
  FDBEdIMEI[01] := DBEdIMEI01;
  FDBEdIMEI[02] := DBEdIMEI02;
  FDBEdIMEI[03] := DBEdIMEI03;
  FDBEdIMEI[04] := DBEdIMEI04;
  FDBEdIMEI[05] := DBEdIMEI05;
  FDBEdIMEI[06] := DBEdIMEI06;
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    FBoxes[I] := False;
    //
    FQrVSPallet[I] := TmySQLQuery.Create(Dmod);
    FDsVSPallet[I] := TDataSource.Create(Self);
    FDsVSPallet[I].DataSet := FQrVSPallet[I];
    FDBEdPallet[I].DataSource := FDsVSPallet[I];
    //
    FQrVSPalClaIts[I] := TmySQLQuery.Create(Dmod);
    FDsVSPalClaIts[I] := TDataSource.Create(Self);
    FDsVSPalClaIts[I].DataSet := FQrVSPalClaIts[I];
    FDBEdControle[I].DataSource := FDsVSPalClaIts[I];
    FDBEdIMEI[I].DataSource := FDsVSPalClaIts[I];
  end;
  FPalletDoBox[01] := PalletDoBox01;
  FPalletDoBox[02] := PalletDoBox02;
  FPalletDoBox[03] := PalletDoBox03;
  FPalletDoBox[04] := PalletDoBox04;
  FPalletDoBox[05] := PalletDoBox05;
  FPalletDoBox[06] := PalletDoBox06;
  //
  FPnIntMei[01] := PnIntMei01;
  FPnIntMei[02] := PnIntMei02;
  FPnIntMei[03] := PnIntMei03;
  FPnIntMei[04] := PnIntMei04;
  FPnIntMei[05] := PnIntMei05;
  FPnIntMei[06] := PnIntMei06;
  //
  FPnBox[01] := PnBox01;
  FPnBox[02] := PnBox02;
  FPnBox[03] := PnBox03;
  FPnBox[04] := PnBox04;
  FPnBox[05] := PnBox05;
  FPnBox[06] := PnBox06;
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  ReopenVSMrtCad();
  //
  DBGAll.PopupMenu := PMAll;
  //
  BtDigitacao.Enabled := VAR_USUARIO = -1;
  //
  SGAll.Cells[01, 0] := 'Box';
  SGAll.Cells[02, 0] := 'SubClass';
  SGAll.Cells[03, 0] := 'AreaM2';
  SGAll.Cells[04, 0] := 'NO_MARTELO';
  SGAll.Cells[05, 0] := 'VMI_Dest';
  SGAll.Cells[06, 0] := 'Controle';
  SGAll.Cells[07, 0] := 'VSPaClaIts';
  SGAll.Cells[08, 0] := 'VSPallet';
  SGAll.Cells[09, 0] := 'Pecas';
  SGAll.Cells[10, 0] := 'AreaP2';
  SGAll.Cells[11, 0] := 'VMI_Sorc';
  SGAll.Cells[12, 0] := 'VMI_Dest';
  //
  SGAll.ColWidths[00] := 40;
  SGAll.ColWidths[01] := 24; //Box
  SGAll.ColWidths[02] := 64; //SubClass
  SGAll.ColWidths[03] := 40; //AreaM2
  SGAll.ColWidths[04] := 60; //NO_MARTELO
  SGAll.ColWidths[05] := 90; //VMI_Dest
  SGAll.ColWidths[06] := 90; //Controle
  SGAll.ColWidths[07] := 90; //VSPaClaIts
  SGAll.ColWidths[08] := 90; //VSPallet
  SGAll.ColWidths[09] := 40; //Pecas
  SGAll.ColWidths[10] := 40; //AreaP2
  SGAll.ColWidths[11] := 90; //VMI_Sorc
  SGAll.ColWidths[12] := 90; //VMI_Dest
  //
  FSGItens[01] := SGItens01;
  FSGItens[02] := SGItens02;
  FSGItens[03] := SGItens03;
  FSGItens[04] := SGItens04;
  FSGItens[05] := SGItens05;
  FSGItens[06] := SGItens06;
  //
  FEdPecas[01] := EdPecas01;
  FEdPecas[02] := EdPecas02;
  FEdPecas[03] := EdPecas03;
  FEdPecas[04] := EdPecas04;
  FEdPecas[05] := EdPecas05;
  FEdPecas[06] := EdPecas06;
  //
  FEdArea[01] := EdArea01;
  FEdArea[02] := EdArea02;
  FEdArea[03] := EdArea03;
  FEdArea[04] := EdArea04;
  FEdArea[05] := EdArea05;
  FEdArea[06] := EdArea06;
  //
  FEdMedia[01] := EdMedia01;
  FEdMedia[02] := EdMedia02;
  FEdMedia[03] := EdMedia03;
  FEdMedia[04] := EdMedia04;
  FEdMedia[05] := EdMedia05;
  FEdMedia[06] := EdMedia06;
  //
end;

procedure TFmVSClassifOneNw2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_MULTIPLY then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
  end;
end;

procedure TFmVSClassifOneNw2.FormResize(Sender: TObject);
begin
  RealinhaBoxes();
end;

procedure TFmVSClassifOneNw2.ImprimeFluxoDeMovimentoDoIMEI();
begin
  VS_PF.MostraRelatoroFluxoIMEI(QrVSGerArtNewControle.Value);
end;

procedure TFmVSClassifOneNw2.ImprimirfluxodemovimentodoIMEI1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNw2.ImprimirfluxodemovimentodoIMEI2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNw2.ImprimirNmeroPallet1Click(Sender: TObject);
  procedure ConfigBox(Box: Integer; QrVSPallet: TmySQLQuery; Check: TCheckBox);
  var
    Pallet: Integer;
  begin
    Pallet := QrVSPallet.FieldByName('Codigo').AsInteger;
    if (FQrVSPallet[01].State = dsInactive) then
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box);
      Check.Enabled := False;
      FmVSPalNumImp.Boxes[Box] := 0;
    end else
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box) + ' - Pallet ' + Geral.FF0(Pallet);
      Check.Enabled := True;
      FmVSPalNumImp.Boxes[Box] := Pallet;
    end;
  end;
begin
  if DBCheck.CriaFm(TFmVSPalNumImp, FmVSPalNumImp, afmoNegarComAviso) then
  begin
    ConfigBox(1, FQrVSPallet[01], FmVSPalNumImp.Ck1);
    ConfigBox(2, FQrVSPallet[02], FmVSPalNumImp.Ck2);
    ConfigBox(3, FQrVSPallet[03], FmVSPalNumImp.Ck3);
    ConfigBox(4, FQrVSPallet[04], FmVSPalNumImp.Ck4);
    ConfigBox(5, FQrVSPallet[05], FmVSPalNumImp.Ck5);
    ConfigBox(6, FQrVSPallet[06], FmVSPalNumImp.Ck6);
    //
    FmVSPalNumImp.ShowModal;
    FmVSPalNumImp.Destroy;
  end;
end;

procedure TFmVSClassifOneNw2.InsereCouroArrAll(CacCod, CacID, Codigo,
  ClaAPalOri, RclAPalOri, VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix,
  VMI_Dest, Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
  Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
  FatorIntDst: Double; Controle: Int64);
var
  I: Integer;
  a, b, t: Cardinal;
begin
  EdAll.Text := '';
  EdArrAll.Text := '';
  EdItens.Text := '';
  a := GetTickCount();
  //
  I := Length(FLsAll);
  SetLength(FLsAll, I + 1);
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaClaIts);                   // VSPaClaIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
  //
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdArrAll.Text := FloatToStr(t / 1000) + 's';

  //////////////////////////////////////////////////////////////////////////////

  a := GetTickCount();
  //
  I := Length(FLsItens[Box]);
  SetLength(FLsItens[Box], I + 1);
  FLsItens[Box][I, 00] := IntToStr(I + 1);
  FLsItens[Box][I, 01] := IntToStr(Controle);                     // Conrole
  FLsItens[Box][I, 02] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsItens[Box][I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsItens[Box][I, 04] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  //
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSClassifOneNw2.InsereCouroAtual;
var
  VSPaClaIts: Integer;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
var
  DataHora, SubClass: String;
  CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri, VSPaRclIts, VSPallet, VMI_Sorc,
  VMI_Baix, VMI_Dest, Box, Revisor, Digitador, Martelo: Integer;
  Controle: Int64;
  Pecas, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
  //
  Ta, Tb, Tt: Cardinal;
  Pecas_TXT, AreaM2_TXT, AreaP2_TXT, ValorT_TXT: String;
begin
  Ta := GetTickCount();
  //
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  //
  if MyObjects.FIC(Revisor = 0, EdRevisor, 'Informe o classificador!') then
    Exit;
  if MyObjects.FIC(Digitador = 0, EdDigitador, 'Informe o digitador!') then
    Exit;
  //
  CacCod         := FCacCod;
  CacID          := Integer(emidClassArtXXUni);
  Codigo         := FCodigo;
  Controle       := 0;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  //
  if PnMartelo.Visible then
  begin
    if CBVSMrtCad.KeyValue = QrVSMrtCadNome.Value then
      Martelo := QrVSMrtCadCodigo.Value
    else
      Martelo := 0;
  end else
    Martelo := 0;
  //
  //VSPaClaIts     := ;   ver abaixo!!!
  VSPaRclIts     := 0;
  //VSPallet       := ;
  //VMI_Sorc       := ;
  //VMI_Dest       := ;
  Box            := EdBox.ValueVariant;
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  DataHora       := Geral.FDT(Now() + FDifTime, 109);
  if PnSubClass.Visible then
    SubClass     := dmkPF.SoTextoLayout(EdSubClass.Text)
  else
    SubClass     := '';
//begin
  if EdArea.ValueVariant < 0.01 then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
    Exit;
  end;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    Pecas          := 1;
    //
    if QrVSPaClaCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet) then
      Exit;
    if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
      Exit;
    //
    FatorIntSrc    := QrVSGerArtNewFatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
   //
    VMI_Sorc := QrVSGerArtNewControle.Value;
    //VMI_Dest :=
    Controle   := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);

    ClaAPalOri := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'Martelo', CO_DATA_HORA_GRL,
    'SubClass', 'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix, VMI_Dest,
    Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, Martelo, DataHora,
    SubClass, FatorIntSrc, FatorIntDst], [
    Controle], False) then
    begin

      if FUsaMemory then
      begin
        InsereCouroArrAll(CacCod, CacID, Codigo,
        ClaAPalOri, RclAPalOri, VSPaClaIts,
        VSPaRclIts, VSPallet,
        VMI_Sorc, VMI_Baix, VMI_Dest,
        Box, Pecas,
        AreaM2, AreaP2, Revisor,
        Digitador, Martelo, DataHora,
        SubClass, FatorIntSrc, FatorIntDst,
        Controle);
        //
        Pecas_TXT  := Geral.FFT_Dot(Pecas, 3, siNegativo);
        AreaM2_TXT := Geral.FFT_Dot(AreaM2, 2, siNegativo);
        AreaP2_TXT := Geral.FFT_Dot(AreaP2, 2, siNegativo);
        ValorT_TXT := Geral.FFT_Dot(QrVSGerArtNewCUSTO_M2.Value * AreaM2, 2, siNegativo);
        //
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca-' + Pecas_TXT +
        ', SdoVrtArM2=SdoVrtArM2-' + AreaM2_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Sorc) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET Pecas=Pecas-' + Pecas_TXT +
        ', AreaM2=AreaM2-' + AreaM2_TXT +
        ', AreaP2=AreaP2-' + AreaP2_TXT +
        ', ValorT=ValorT-' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Baix) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca+' + Pecas_TXT +
        ', SdoVrtArM2=SdoVrtArM2+' + AreaM2_TXT +
        ', Pecas=Pecas+' + Pecas_TXT +
        ', AreaM2=AreaM2+' + AreaM2_TXT +
        ', AreaP2=AreaP2+' + AreaP2_TXT +
        ', ValorT=ValorT+' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Dest) +
        '');
        //
        CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
      end else
      begin
        ReopenItens(Box, VSPaClaIts, VSPallet);
        //
        VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      end;
      //
      EdSubClass.Text     := '';
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
      //
      AtualizaInfoOC();
      //
      Tb := GetTickCount();
      Tt := Tb - Ta;
      EdTempo.Text := FloatToStr(Tt / 1000) + 's';
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSClassifOneNw2.LiberaDigitacao();
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  BtDigitacao.Visible := Libera;
end;

procedure TFmVSClassifOneNw2.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  //QrSum,
  QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
begin
(* ver o que fazer!
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
  case Box of
    1:
    begin
      //QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      //QrSum     := QrSum2;
      QrSumPal  := QrSumPal2;
      EdMedia   := EdMedia2;
    end;
    3:
    begin
      //QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      //QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      //QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      //QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaClaCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O DA FICHA RMP:  ' + QrVSGerArtNewNO_Ficha.Value);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  //Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  //Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
*)
end;

procedure TFmVSClassifOneNw2.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSClassifOneNw2.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSClassifOneNw2.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  case Box of
    1: Result := CkSubClass1.Checked;
    2: Result := CkSubClass2.Checked;
    3: Result := CkSubClass3.Checked;
    4: Result := CkSubClass4.Checked;
    5: Result := CkSubClass5.Checked;
    6: Result := CkSubClass6.Checked;
  end;
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSClassifOneNw2.ObtemDadosBox(const Box: Integer; var
VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
begin
  VMI_Sorc := QrVSGerArtNewControle.Value;
  VSPallet   := FQrVSPallet[Box].FieldByName('Codigo').AsInteger;
  VSPaClaIts := FQrVSPalClaIts[Box].FieldByName('Controle').AsInteger;
  VMI_Baix   := FQrVSPalClaIts[Box].FieldByName('VMI_Baix').AsInteger;
  VMI_Dest   := FQrVSPalClaIts[Box].FieldByName('VMI_Dest').AsInteger;
  Result := True;
end;

function TFmVSClassifOneNw2.ObtemNO_MARTELO(Martelo: Integer): String;
var
  I, N: Integer;
begin
  Result := '' ;
  if Martelo <> 0 then
  begin
    Result := '?????' ;
    for I := 0 to Length(FMartelosCod) - 1 do
    begin
      if Martelo = FMartelosCod[I] then
      begin
        Result := FMartelosNom[I];
        Exit;
      end;
    end;
  end;
end;

function TFmVSClassifOneNw2.ObtemQryesBox(const Box: Integer; var QrVSPallet:
TmySQLQuery): Boolean;
begin
  QrVSPallet := FQrVSPallet[Box];
  Result := True;
end;

procedure TFmVSClassifOneNw2.PMEncerraPopup(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    FPalletdobox[I].Enabled := FQrVSPallet[I].RecordCount > 0;
end;

procedure TFmVSClassifOneNw2.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemItsDel(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSClassifOneNw2.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    //0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    0: QrSumTJaFoi_AREA.Value := QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value;
    //1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    1: QrSumTJaFoi_AREA.Value := Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  //QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
  QrSumTJaFoi_PECA.Value := QrSumTPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSClassifOneNw2.QrVSGerArtNewAfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    ReconfiguraPaineisIntMei(FQrVSPallet[I], FPnIntMei[I]);
end;

procedure TFmVSClassifOneNw2.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
var
  Campo: String;
  I, LstPal, Controle, Codigo: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    if QrVSPaClaCab.FieldByName(VS_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  case QrVSPaClaCabTipoArea.Value of
    0: Campo := 'AreaM2';
    1: Campo := 'AreaP2';
    else Campo := 'Area?2';
  end;
  DBGAll.Columns[2].FieldName := Campo;
  //
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    LstPal := QrVSPaClaCab.FieldByName('LstPal' + Geral.FFN(I, 2)).AsInteger;
    ReopenVSPallet(I, FQrVSPallet[I], FQrVSPalClaIts[I], LstPal);
    if (FQrVSPalClaIts[I].State <> dsInactive) and
    (FQrVSPallet[I].State <> dsInactive) then
    begin
      Controle := FQrVSPalClaIts[I].FieldByName('Controle').AsInteger;
      Codigo   := FQrVSPallet[I].FieldByName('Codigo').Value;
      ReopenItens(I, Controle, Codigo);
    end;
  end;
  //
  if FUsaMemory then
    ReopenAll();
  AtualizaInfoOC();
  //
end;

procedure TFmVSClassifOneNw2.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
var
  I: Integer;
begin
  QrVSGerArtNew.Close;
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    FPnBox[I].Visible := True;
    FQrVSPallet[I].Close;
    FQrVSPalClaIts[I].Close;
  end;
end;

procedure TFmVSClassifOneNw2.QrVSPaClaCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    0: QrVSPaClaCabNO_TIPO.Value := 'm�';
    1: QrVSPaClaCabNO_TIPO.Value := 'ft�';
    else QrVSPaClaCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaClaCabNO_TIPO.Value;
end;

procedure TFmVSClassifOneNw2.RealinhaBoxes();
var
  H_T, W_T, H_1, W_1: Integer;
begin
  H_T := PnBoxesAll.Height;
  W_T := PnBoxesAll.Width;
  //
  H_1 := H_T div 2;
  W_1 := W_T div 3;
  //
  PnBoxesT01.Height := H_1;
  //PnBoxesT02.Height := H_1;

  PnBoxT1L1.Width := W_1;
  PnBoxT1L2.Width := W_1;
  PnBoxT1L3.Width := W_1;

  PnBoxT2L1.Width := W_1;
  PnBoxT2L2.Width := W_1;
  PnBoxT2L3.Width := W_1;
end;

procedure TFmVSClassifOneNw2.ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery;
Panel: TPanel);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrVSPallet.State <> dsInactive) and
    (QrVSGerArtNew.State <> dsInactive) and
    (QrVSPallet.FieldByName('FatorInt').AsFloat < QrVSGerArtNewFatorInt.Value);
  if Habilita <> Panel.Visible then
    Panel.Visible := Habilita;
end;

procedure TFmVSClassifOneNw2.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo: String;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, Codigo: Integer;
  DtHrFim: String;
begin
  Campo := VS_PF.CampoLstPal(Box);
  Codigo := QrVSPaClaCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  if VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaClaIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaClaCab();
      end;
    end;
  end;
end;

procedure TFmVSClassifOneNw2.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNw2.ReopenAll();
var
  I, J, Linhas: Integer;
  a, b, t: Cardinal;
begin
  Linhas := 20;
  a := GetTickCount();
  DqAll.SQL.Text :=
  ' SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ' + // 0 a 3
  ' cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ' +  // 4 a 8
  ' cia.SubClass, mrt.Nome NO_MARTELO ' +                             // 9 a 10
  ' FROM vscacitsa cia ' +
  ' LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo' +
  ' WHERE cia.CacCod=' + Geral.FF0(FCacCod) +
  //' ORDER BY cia.Controle DESC ';
  ' ORDER BY cia.Controle ';
  //
  DqAll.Open();
  SetLength(FLsAll, DqAll.RecordCount);
  DqAll.First();
  while not DqAll.Eof do
  begin
    J := DqAll.RecNo;
    //
    FLsAll[J, 01] := DqAll.FieldValues[08];   // Box
    FLsAll[J, 02] := DqAll.FieldValues[09];   // SubClass
    FLsAll[J, 03] := DqAll.FieldValues[04];   // AreaM2
    FLsAll[J, 04] := DqAll.FieldValues[10];   // NO_MARTELO
    FLsAll[J, 05] := DqAll.FieldValues[07];   // VMI_Dest
    FLsAll[J, 06] := DqAll.FieldValues[00];   // Controle
    FLsAll[J, 07] := DqAll.FieldValues[01];   // VSPaClaIts
    FLsAll[J, 08] := DqAll.FieldValues[02];   // VSPallet
    FLsAll[J, 09] := DqAll.FieldValues[03];   // Pecas
    FLsAll[J, 10] := DqAll.FieldValues[05];   // AreaP2
    FLsAll[J, 11] := DqAll.FieldValues[06];   // VMI_Sorc
    FLsAll[J, 12] := DqAll.FieldValues[07];   // VMI_Dest
    //
    DqAll.Next();
  end;
  DqAll.Close();
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdAll.Text := FloatToStr(t / 1000) + 's';
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ',
  'cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ',
  'cia.SubClass, mrt.Nome NO_MARTELO',
  'FROM vscacitsa cia',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo',
  'WHERE cia.CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY cia.Controle DESC ',
  '']);
*)
end;

procedure TFmVSClassifOneNw2.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
end;

procedure TFmVSClassifOneNw2.ReopenItens(Box, VSPaClaIts, VSPallet: Integer);
var
  I, J: Integer;
  a, b, t: Cardinal;
  //
begin
  a := GetTickCount();
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  'ORDER BY Controle DESC ',
  '']);
*)
  DqItens.SQL.Text :=
  ' SELECT Controle, Pecas, AreaM2, AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE CacCod=' + Geral.FF0(FCacCod) +
  ' AND VSPaClaIts=' + Geral.FF0(VSPaClaIts) +
  ' AND VSPallet=' + Geral.FF0(VSPallet) +
  //' ORDER BY Controle DESC ' +
  ' ORDER BY Controle ' +
  '';

  //
  DqItens.Open();
  SetLength(FLsItens[Box], DqItens.RecordCount);
  DqItens.First();
  while not DqItens.Eof do
  begin
    J := DqItens.RecNo;
    //
    FLsItens[Box][J, 01] := DqItens.FieldValues[00];   // Conrole
    FLsItens[Box][J, 02] := DqItens.FieldValues[01];   // Pecas
    FLsItens[Box][J, 03] := DqItens.FieldValues[02];   // AreaM2
    FLsItens[Box][J, 04] := DqItens.FieldValues[03];   // AreaP2
    //
    DqItens.Next();
  end;
  DqItens.Close();
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ',
  'cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ',
  'cia.SubClass, mrt.Nome NO_MARTELO',
  'FROM vscacitsa cia',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo',
  'WHERE cia.CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY cia.Controle DESC ',
  '']);
*)
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  '']);
*)
  //
  ReopenSumPal((*QrSumPal,*) Box, VSPallet);
end;

procedure TFmVSClassifOneNw2.ReopenSumPal((*QrSumPal: TmySQLQuery;*) Box,
  VSPallet: Integer);
var
  J: Integer;
  Pecas, AreaM2, AreaP2: Double;
begin
  DqSumPall.SQL.Text :=
  ' SELECT ' +
  ' SUM(vmi.SdoVrtPeca) SdoVrtPeca, ' +
  ' SUM(vmi.SdoVrtArM2) SdoVrtArM2' +
  ' FROM ' + CO_SEL_TAB_VMI + ' vmi' +
  ' WHERE vmi.Pallet=' + Geral.FF0(VSPallet) +
  ' AND vmi.Pecas>0 ' +
(*
  ' SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ' +
  ' SUM(AreaP2) AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE VSPallet=' + Geral.FF0(VSPallet) +
*)
  '';
  //
  DqSumPall.Open();
  Pecas  := Geral.DMV_Dot(DqSumPall.FieldValues[00]);   // Pecas
  AreaM2 := Geral.DMV_Dot(DqSumPall.FieldValues[01]);   // AreaM2
  AreaP2 := Geral.ConverteArea(AreaM2, TCalcType.ctM2toP2, TCalcFrac.cfQuarto);
  DqSumPall.Close();
  CarregaSGSumPall(Box, amathSubstitui, Pecas, AreaM2, AreaP2);
end;

procedure TFmVSClassifOneNw2.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, cn1.FatorInt ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND vmi.MovimCod=' + Geral.FF0(QrVSPaClaCabMovimCod.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSClassifOneNw2.ReopenVSMrtCad();
var
  I, N: Integer;
begin
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
  N := QrVSMrtCad.RecordCount;
  SetLength(FMartelosCod, N);
  SetLength(FMartelosNom, N);
  I := 0;
  while not QrVSMrtCad.Eof do
  begin
    FMartelosCod[I] := QrVSMrtCadCodigo.Value;
    FMartelosNom[I] := QrVSMrtCadNome.Value;
    //
    I := I + 1;
    QrVSMrtCad.Next;
  end;
end;

procedure TFmVSClassifOneNw2.ReopenVSPaClaCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vspaclacaba pcc',
  'LEFT JOIN vsgerarta  vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
end;

procedure TFmVSClassifOneNw2.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalClaIts: TmySQLQuery; Pallet: Integer);
begin
  if Pallet > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT let.*, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt ',
    'FROM vspalleta let ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE let.Codigo=' + Geral.FF0(Pallet),
    '']);
    ReconfiguraPaineisIntMei(QrVSPallet, FPnIntMei[Tecla]);
    //
    FMaxPecas[Tecla] := QrVSPallet.FieldByName('QtdPrevPc').AsFloat;
    if (QrVSPallet.State <> dsInactive) and (QrVSPallet.RecordCount > 0) and
    (QrVSPallet.FieldByName('FatorInt').AsFloat <= 0.01) then
    begin
      if Tecla = 0 then
        Geral.MB_Aviso(
        'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!'
        + sLineBreak + 'Para evitar erros de estoque esta janela ser� fechada!');
        Close;
        Exit;
    end;
    //
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalClaIts, Dmod.MyDB, [
    'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
    'FROM vspaclaitsa ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    'AND Tecla=' + Geral.FF0(Tecla),
    'AND VSPallet=' + Geral.FF0(Pallet),
    'ORDER BY DtHrIni DESC, Controle DESC ',
    'LIMIT 1 ',
    '']);
  end else
  begin
    QrVSPallet.Close;
  end;
  //
end;

procedure TFmVSClassifOneNw2.rocarIMEI1Click(Sender: TObject);
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
begin
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  VS_PF.MostraFormVSClassifOneRetIMEI_06(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
end;

procedure TFmVSClassifOneNw2.rocarVichaRMP1Click(Sender: TObject);
(*
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
*)
begin
(* Form removido para eviatr erro de usuario na ecolha do IMEI!
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  //
  VS_PF.MostraFormVSClassifOneRetFichaRMP(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
*)
end;

procedure TFmVSClassifOneNw2.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

procedure TFmVSClassifOneNw2.SGAllKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
    UpdDelCouroSelecionado(stDel)
  else
  if Key = VK_RETURN then
    UpdDelCouroSelecionado(stUpd);
end;

function TFmVSClassifOneNw2.SubstituiOC(): Boolean;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSClaArtPrpMDz, FmVSClaArtPrpMDz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpMDz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpMDz.EdPallet1.ValueVariant := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.EdPallet2.ValueVariant := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.EdPallet3.ValueVariant := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.EdPallet4.ValueVariant := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.EdPallet5.ValueVariant := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.EdPallet6.ValueVariant := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.CBPallet1.KeyValue := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.CBPallet2.KeyValue := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.CBPallet3.KeyValue := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.CBPallet4.KeyValue := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.CBPallet5.KeyValue := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.CBPallet6.KeyValue := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.ShowModal;
    Codigo := FmVSClaArtPrpMDz.FCodigo;
    CacCod := FmVSClaArtPrpMDz.FCacCod;
    MovimID := FmVSClaArtPrpMDz.FMovimID;
    FmVSClaArtPrpMDz.Destroy;
    //
    if Codigo <> 0 then
    begin
      FCodigo := Codigo;
      FCacCod := CacCod;
      FMovimID := MovimID;
      ReopenVSPaClaCab();
      //
      Result := True;
    end else
      Result := False;
  end;
end;

procedure TFmVSClassifOneNw2.TentarFocarEdArea();
begin
  ConfigPnSubClass();
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

(*
  Completo 1,450         >>
  Sem QrSumX 1,350       >> - 0,100
  Sem QrItensX 1,050     >> - 0,300
  Sem QrAll 0,750        >> - 0,300
*)

{
Buscar total de pecas e area de pallet pelo Saldo!
calcular imei ao:
-retirar pallet ou
-encerrar OC ou
-fechar janela!
Exclusao em massa
}
end.
