unit VSEqzCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO;

type
  TFmVSEqzCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    QrVSEqzCab: TmySQLQuery;
    DsVSEqzCab: TDataSource;
    QrVSEqzArt: TmySQLQuery;
    DsVSEqzArt: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtArt: TBitBtn;
    QrVSEqzIts: TmySQLQuery;
    DsVSEqzIts: TDataSource;
    QrVSEqzCabCodigo: TIntegerField;
    QrVSEqzCabNome: TWideStringField;
    QrVSEqzCabEmpresa: TIntegerField;
    QrVSEqzCabCliente: TIntegerField;
    QrVSEqzCabClassificador: TIntegerField;
    QrVSEqzCabDtHrIni: TDateTimeField;
    QrVSEqzCabDtHrFim: TDateTimeField;
    QrVSEqzCabBasNotZero: TFloatField;
    QrVSEqzCabLk: TIntegerField;
    QrVSEqzCabDataCad: TDateField;
    QrVSEqzCabDataAlt: TDateField;
    QrVSEqzCabUserCad: TIntegerField;
    QrVSEqzCabUserAlt: TIntegerField;
    QrVSEqzCabAlterWeb: TSmallintField;
    QrVSEqzCabAtivo: TSmallintField;
    QrVSEqzCabNO_EMP: TWideStringField;
    QrVSEqzCabNO_CLI: TWideStringField;
    QrVSEqzCabNO_REV: TWideStringField;
    QrVSEqzArtCodigo: TIntegerField;
    QrVSEqzArtControle: TIntegerField;
    QrVSEqzArtGraGruX: TIntegerField;
    QrVSEqzArtBasNota: TFloatField;
    QrVSEqzArtInteresse: TSmallintField;
    QrVSEqzArtLk: TIntegerField;
    QrVSEqzArtDataCad: TDateField;
    QrVSEqzArtDataAlt: TDateField;
    QrVSEqzArtUserCad: TIntegerField;
    QrVSEqzArtUserAlt: TIntegerField;
    QrVSEqzArtAlterWeb: TSmallintField;
    QrVSEqzArtAtivo: TSmallintField;
    QrVSEqzArtNO_PRD_TAM_COR: TWideStringField;
    QrVSEqzItsCodigo: TIntegerField;
    QrVSEqzItsControle: TIntegerField;
    QrVSEqzItsLk: TIntegerField;
    QrVSEqzItsDataCad: TDateField;
    QrVSEqzItsDataAlt: TDateField;
    QrVSEqzItsUserCad: TIntegerField;
    QrVSEqzItsUserAlt: TIntegerField;
    QrVSEqzItsAlterWeb: TSmallintField;
    QrVSEqzItsAtivo: TSmallintField;
    QrVSEqzItsGraGruX: TIntegerField;
    QrVSEqzItsNO_PRD_TAM_COR: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label52: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label53: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    EdNome: TdmkEdit;
    Label9: TLabel;
    QrClientes: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientes: TDataSource;
    Label20: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SbCliente: TSpeedButton;
    EdClassificador: TdmkEditCB;
    Label3: TLabel;
    CBClassificador: TdmkDBLookupComboBox;
    SBClassificador: TSpeedButton;
    QrClassificadores: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsClassificadores: TDataSource;
    GBNotas: TGroupBox;
    GBIts: TGroupBox;
    DBGrid1: TDBGrid;
    PMArt: TPopupMenu;
    AdicionaArtigo1: TMenuItem;
    EditaArtigo1: TMenuItem;
    RemoveArtigo1: TMenuItem;
    BtIts: TBitBtn;
    QrVSEqzArtNO_Interesse: TWideStringField;
    Palletdeorigem1: TMenuItem;
    Palletsdedestino1: TMenuItem;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    QrVSEqzSub: TmySQLQuery;
    DsVSEqzSub: TDataSource;
    QrVSEqzSubCodigo: TIntegerField;
    QrVSEqzSubControle: TIntegerField;
    QrVSEqzSubSubClass: TWideStringField;
    QrVSEqzSubGraGruX: TIntegerField;
    QrVSEqzSubBasNota: TFloatField;
    QrVSEqzSubLk: TIntegerField;
    QrVSEqzSubDataCad: TDateField;
    QrVSEqzSubDataAlt: TDateField;
    QrVSEqzSubUserCad: TIntegerField;
    QrVSEqzSubUserAlt: TIntegerField;
    QrVSEqzSubAlterWeb: TSmallintField;
    QrVSEqzSubAtivo: TSmallintField;
    QrVSEqzSubNO_PRD_TAM_COR: TWideStringField;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    BtSub: TBitBtn;
    PMSub: TPopupMenu;
    AdicionaSigla1: TMenuItem;
    EditaSigla1: TMenuItem;
    RemoveSigla1: TMenuItem;
    QrVSEqzSubInteresse: TSmallintField;
    QrVSEqzItsPallet: TIntegerField;
    Panel6: TPanel;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    QrNotaCrr: TmySQLQuery;
    DsNotaCrr: TDataSource;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    QrNotas: TmySQLQuery;
    DsNotas: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    PnSohUpdate: TPanel;
    EdBasNotZero: TdmkEdit;
    Label4: TLabel;
    PnSohInsert: TPanel;
    EdVSCfgEqzCb: TdmkEditCB;
    Label28: TLabel;
    CBVSCfgEqzCb: TdmkDBLookupComboBox;
    SbVSCfgEqzCb: TSpeedButton;
    QrVSCfgEqzCb: TmySQLQuery;
    QrVSCfgEqzCbCodigo: TIntegerField;
    QrVSCfgEqzCbNome: TWideStringField;
    QrVSCfgEqzCbBasNotZero: TFloatField;
    DsVSCfgEqzCb: TDataSource;
    QrVSCfgEqzIt: TmySQLQuery;
    QrVSCfgEqzItCodigo: TIntegerField;
    QrVSCfgEqzItControle: TIntegerField;
    QrVSCfgEqzItSubClass: TWideStringField;
    QrVSCfgEqzItGraGruX: TIntegerField;
    QrVSCfgEqzItBasNota: TFloatField;
    QrVSCfgEqzItFatorEqz: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSEqzCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSEqzCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSEqzCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSEqzCabBeforeClose(DataSet: TDataSet);
    procedure SbClienteClick(Sender: TObject);
    procedure SBClassificadorClick(Sender: TObject);
    procedure AdicionaArtigo1Click(Sender: TObject);
    procedure EditaArtigo1Click(Sender: TObject);
    procedure RemoveArtigo1Click(Sender: TObject);
    procedure PMArtPopup(Sender: TObject);
    procedure BtArtClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Palletsdedestino1Click(Sender: TObject);
    procedure BtSubClick(Sender: TObject);
    procedure PMSubPopup(Sender: TObject);
    procedure AdicionaSigla1Click(Sender: TObject);
    procedure EditaSigla1Click(Sender: TObject);
    procedure RemoveSigla1Click(Sender: TObject);
    procedure SbVSCfgEqzCbClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSEqzArt(SQLType: TSQLType);
    procedure MostraFormVSEqzIts(SQLType: TSQLType);
    procedure MostraFormVSEqzSub(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenVSEqzArt(Controle: Integer);
    procedure ReopenVSEqzIts(Controle: Integer);
    procedure ReopenVSEqzSub(Controle: Integer);
    procedure ReopenNotas();

  end;

var
  FmVSEqzCab: TFmVSEqzCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, (*VSEqzArt,*) VSEqzIts, VSEqzSub,
  ModuleGeral, UnVS_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSEqzCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSEqzCab.MostraFormVSEqzArt(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmVSEqzArt, FmVSEqzArt, afmoNegarComAviso) then
  begin
    FmVSEqzArt.ImgTipo.SQLType := SQLType;
    FmVSEqzArt.FQrCab := QrVSEqzCab;
    FmVSEqzArt.FDsCab := DsVSEqzCab;
    FmVSEqzArt.FQrIts := QrVSEqzArt;
    if SQLType = stIns then
      //
    else
    begin
      FmVSEqzArt.EdControle.ValueVariant := QrVSEqzArtControle.Value;
      //
      FmVSEqzArt.EdBasNota.ValueVariant := QrVSEqzArtBasNota.Value;
      FmVSEqzArt.RGInteresse.ItemIndex  := QrVSEqzArtInteresse.Value;
      FmVSEqzArt.EdGraGruX.ValueVariant := QrVSEqzArtGraGruX.Value;
      FmVSEqzArt.CBGraGruX.KeyValue     := QrVSEqzArtGraGruX.Value;
      //
    end;
    FmVSEqzArt.ShowModal;
    FmVSEqzArt.Destroy;
  end;
}
end;

procedure TFmVSEqzCab.MostraFormVSEqzIts(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrVSEqzCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSEqzIts, FmVSEqzIts, afmoNegarComAviso) then
  begin
    FmVSEqzIts.ImgTipo.SQLType := SQLType;
    FmVSEqzIts.FQrCab := QrVSEqzCab;
    FmVSEqzIts.FDsCab := DsVSEqzCab;
    FmVSEqzIts.FQrIts := QrVSEqzIts;
    if SQLType = stIns then
      //
    else
    begin
      FmVSEqzIts.EdControle.ValueVariant := QrVSEqzItsControle.Value;
      //
      FmVSEqzIts.EdPallet.ValueVariant := QrVSEqzItsPallet.Value;
      FmVSEqzIts.CBPallet.KeyValue     := QrVSEqzItsPallet.Value;
    end;
    FmVSEqzIts.ShowModal;
    LocCod(Codigo, Codigo);
    FmVSEqzIts.Destroy;
  end;
end;

procedure TFmVSEqzCab.MostraFormVSEqzSub(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrVSEqzCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmVSEqzSub, FmVSEqzSub, afmoNegarComAviso) then
  begin
    FmVSEqzSub.ImgTipo.SQLType := SQLType;
    FmVSEqzSub.FQrCab := QrVSEqzCab;
    FmVSEqzSub.FDsCab := DsVSEqzCab;
    FmVSEqzSub.FQrIts := QrVSEqzSub;
    if SQLType = stIns then
      //
    else
    begin
      FmVSEqzSub.EdControle.ValueVariant := QrVSEqzSubControle.Value;
      //
      FmVSEqzSub.EdBasNota.ValueVariant := QrVSEqzSubBasNota.Value;
      FmVSEqzSub.RGInteresse.ItemIndex  := QrVSEqzSubInteresse.Value;
      FmVSEqzSub.EdGraGruX.ValueVariant := QrVSEqzSubGraGruX.Value;
      FmVSEqzSub.CBGraGruX.KeyValue     := QrVSEqzSubGraGruX.Value;
      //
    end;
    FmVSEqzSub.ShowModal;
    LocCod(Codigo, Codigo);
    FmVSEqzSub.Destroy;
  end;
end;

procedure TFmVSEqzCab.Palletsdedestino1Click(Sender: TObject);
begin
  MostraFormVSEqzIts(stIns);
end;

procedure TFmVSEqzCab.PMArtPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaArtigo1, QrVSEqzCab);
  MyObjects.HabilitaMenuItemItsUpd(EditaArtigo1,    QrVSEqzArt);
  MyObjects.HabilitaMenuItemItsDel(RemoveArtigo1,   QrVSEqzArt);
end;

procedure TFmVSEqzCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSEqzCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSEqzCab, QrVSEqzIts);
end;

procedure TFmVSEqzCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSEqzCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSEqzIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSEqzIts);
end;

procedure TFmVSEqzCab.PMSubPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaSigla1, QrVSEqzCab);
  MyObjects.HabilitaMenuItemItsUpd(EditaSigla1,    QrVSEqzSub);
  MyObjects.HabilitaMenuItemItsDel(RemoveSigla1,   QrVSEqzSub);
end;

procedure TFmVSEqzCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSEqzCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSEqzCab.DefParams;
begin
  VAR_GOTOTABELA := 'vseqzcab';
  VAR_GOTOMYSQLTABLE := QrVSEqzCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vdc.*,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ');
  VAR_SQLx.Add('IF(rev.Tipo=0, rev.RazaoSocial, rev.Nome) NO_REV');
  VAR_SQLx.Add('FROM vseqzcab vdc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vdc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=vdc.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades rev ON rev.Codigo=vdc.Classificador');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE vdc.Codigo > 0');
  //
  VAR_SQL1.Add('AND vdc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND vdc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vdc.Nome Like :P0');
  //
end;

procedure TFmVSEqzCab.EdClienteChange(Sender: TObject);
var
  Qry: TmySQLQuery;
  Cliente, VSCfgEqzCb: Integer;
begin
  if ImgTipo.SQLType = stIns then
  begin
    Cliente := EdCliente.ValueVariant;
    if Cliente <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT MAX(VSCfgEqzCb) VSCfgEqzCb ',
        'FROM VSEqzCab ',
        'WHERE Cliente=' + Geral.FF0(Cliente),
        '']);
        //
        VSCfgEqzCb := Qry.FieldByName('VSCfgEqzCb').AsInteger;
        //
        if VSCfgEqzCb <> 0 then
        begin
          EdVSCfgEqzCb.ValueVariant := VSCfgEqzCb;
          CBVSCfgEqzCb.KeyValue     := VSCfgEqzCb;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

procedure TFmVSEqzCab.EditaArtigo1Click(Sender: TObject);
begin
  MostraFormVSEqzArt(stIns);
end;

procedure TFmVSEqzCab.EditaSigla1Click(Sender: TObject);
begin
  MostraFormVSEqzSub(stUpd);
end;

procedure TFmVSEqzCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSEqzIts(stUpd);
end;

procedure TFmVSEqzCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSEqzCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSEqzCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSEqzCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a retirada do pallet selecionado?',
  'vseqzits', 'Controle', QrVSEqzItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSEqzIts,
      QrVSEqzItsControle, QrVSEqzItsControle.Value);
    ReopenVSEqzIts(Controle);
  end;
end;

procedure TFmVSEqzCab.RemoveArtigo1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vseqzart', 'Controle', QrVSEqzArtControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSEqzArt,
      QrVSEqzArtControle, QrVSEqzArtControle.Value);
    ReopenVSEqzArt(Controle);
  end;
end;

procedure TFmVSEqzCab.RemoveSigla1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vseqzsub', 'Controle', QrVSEqzsubControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSEqzsub,
      QrVSEqzsubControle, QrVSEqzsubControle.Value);
    ReopenVSEqzSub(Controle);
  end;
end;

procedure TFmVSEqzCab.ReopenNotas();
begin
  VS_PF.ReopenNotasEqualize(QrVSEqzCabCodigo.Value, QrNotaAll, QrNotaCrr, QrNotas);
end;

procedure TFmVSEqzCab.ReopenVSEqzArt(Controle: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEqzArt, Dmod.MyDB, [
  'SELECT dsa.*, ELT(dsa.Interesse+1, "Sim", "N�o", "???") NO_Interesse, ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR ',
  'FROM vseqzart dsa ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=dsa.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE dsa.Codigo=' + Geral.FF0(QrVSEqzCabCodigo.Value),
  '']);
  //
  QrVSEqzArt.Locate('Controle', Controle, []);
*)
end;


procedure TFmVSEqzCab.ReopenVSEqzIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEqzIts, Dmod.MyDB, [
(*
  'SELECT dsi.*, ger.GraGruX, ger.MovimCod, ger.VSPallet,  ',
  'CONCAT(gg1.Nome,      ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR    ',
  'FROM vseqzits dsi  ',
  'LEFT JOIN vsgerrcla ger ON ger.CacCod=dsi.VSCacCod  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ger.GraGruX      ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC      ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad      ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI      ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE dsi.Codigo=' + Geral.FF0(QrVSEqzCabCodigo.Value),
*)
  'SELECT dsi.*, ger.GraGruX, CONCAT(gg1.Nome,       ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR     ',
  'FROM vseqzits dsi   ',
  'LEFT JOIN vspalleta ger ON ger.Codigo=dsi.Pallet   ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ger.GraGruX       ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC       ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad       ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI       ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1     ',
  'WHERE dsi.Codigo=' + Geral.FF0(QrVSEqzCabCodigo.Value),
  '']);
  //
  QrVSEqzIts.Locate('Controle', Controle, []);
end;

procedure TFmVSEqzCab.ReopenVSEqzSub(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSEqzSub, Dmod.MyDB, [
  'SELECT vds.*, ',
  'CONCAT(gg1.Nome,     ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR  ',
  'FROM vseqzsub vds ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vds.GraGruX     ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC     ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad     ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI     ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE vds.Codigo=' + Geral.FF0(QrVSEqzCabCodigo.Value),
  ' ']);
end;

procedure TFmVSEqzCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSEqzCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSEqzCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSEqzCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSEqzCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSEqzCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSEqzCab.BtSubClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSub, BtSub);
end;

procedure TFmVSEqzCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSEqzCabCodigo.Value;
  Close;
end;

procedure TFmVSEqzCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  PnSohInsert.Visible := False;
  PnSohUpdate.Visible := True;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSEqzCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vseqzcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSEqzCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSEqzCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrIni, DtHrFim: String;
  Codigo, Empresa, Cliente, Classificador, VSCfgEqzCb: Integer;
  BasNotZero: Double;
  //
  procedure InsereItemAtual();
  var
    SubClass: String;
    Controle, GraGruX, Interesse: Integer;
    BasNota, FatorEqz: Double;
  begin
    //Codigo         := ;
    Controle       := 0;
    SubClass       := QrVSCfgEqzItSubClass.Value;
    GraGruX        := QrVSCfgEqzItGraGruX.Value;
    BasNota        := QrVSCfgEqzItBasNota.Value;
    Interesse      := 0;//QrVSCfgEqzItInteresse.Value;
    FatorEqz       := QrVSCfgEqzItFatorEqz.Value;
    //
    Controle := UMyMod.BPGS1I32('vseqzsub', 'Controle', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vseqzsub', False, [
    'Codigo', 'SubClass', 'GraGruX',
    'BasNota', 'Interesse', 'FatorEqz'], [
    'Controle'], [
    Codigo, SubClass, GraGruX,
    BasNota, Interesse, FatorEqz], [
    Controle], True);
  end;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Cliente        := EdCliente.ValueVariant;
  Classificador     := EdClassificador.ValueVariant;
  DtHrIni        := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim        := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
  VSCfgEqzCb     := EdVSCfgEqzCb.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then
    Exit;
  //
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina um cliente!') then
    Exit;
  //if MyObjects.FIC(BasNotZero = 0, EdBasNotZero, 'Defina uma nota base zero!') then
    //Exit;
  //
  if ImgTipo.SQLType = stUpd then
    BasNotZero := EdBasNotZero.ValueVariant
  else
  if VSCfgEqzCb <> 0 then
    BasNotZero := QrVSCfgEqzCbBasNotZero.Value
  else
    BasNotZero := 0;
  //
  Codigo := UMyMod.BPGS1I32('vseqzcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vseqzcab', False, [
  'Nome', 'Empresa', 'Cliente',
  'Classificador', 'DtHrIni', 'DtHrFim',
  'BasNotZero', 'VSCfgEqzCb'], [
  'Codigo'], [
  Nome, Empresa, Cliente,
  Classificador, DtHrIni, DtHrFim,
  BasNotZero, VSCfgEqzCb], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if VSCfgEqzCb <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrVSCfgEqzIt, Dmod.MyDB, [
        'SELECT * ',
        'FROM vscfgeqzit ',
        'WHERE Codigo=' + Geral.FF0(VSCfgEqzCb),
        '']);
        QrVSCfgEqzIt.First;
        while not QrVSCfgEqzIt.Eof do
        begin
          InsereItemAtual();
          //
          QrVSCfgEqzIt.Next;
        end;
      end;
    end;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSEqzCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vseqzcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vseqzcab', 'Codigo');
end;

procedure TFmVSEqzCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSEqzCab.AdicionaArtigo1Click(Sender: TObject);
begin
  MostraFormVSEqzArt(stIns);
end;

procedure TFmVSEqzCab.AdicionaSigla1Click(Sender: TObject);
begin
  MostraFormVSEqzSub(stIns);
end;

procedure TFmVSEqzCab.BtArtClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArt, BtArt);
end;

procedure TFmVSEqzCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSEqzCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBNotas.Align := alClient;
  PnSohInsert.Align := alClient;
  PnSohUpdate.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClassificadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSCfgEqzCb, Dmod.MyDB);
end;

procedure TFmVSEqzCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSEqzCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSEqzCab.SbClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliente.Text := IntToStr(VAR_ENTIDADE);
    CBCliente.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSEqzCab.SBClassificadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClassificadores, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClassificador.Text := IntToStr(VAR_ENTIDADE);
    CBClassificador.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSEqzCab.SbImprimeClick(Sender: TObject);
begin
  VS_PF.ImprimeReclassEqualize(QrVSEqzCabCodigo.Value);
end;

procedure TFmVSEqzCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSEqzCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSEqzCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSEqzCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSEqzCab.QrVSEqzCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSEqzCab.QrVSEqzCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSEqzArt(0);
  ReopenVSEqzIts(0);
  ReopenVSEqzSub(0);
  ReopenNotas();
end;

procedure TFmVSEqzCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSEqzCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSEqzCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSEqzCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vseqzcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSEqzCab.SbVSCfgEqzCbClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSCfgEqzCb(0);
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrVSCfgEqzCb, Dmod.MyDB);
    EdVSCfgEqzCb.Text := IntToStr(VAR_CADASTRO);
    CBVSCfgEqzCb.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmVSEqzCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSEqzCab.CabInclui1Click(Sender: TObject);
begin
  EdBasNotZero.ValueVariant := 0;
  PnSohInsert.Visible := True;
  PnSohUpdate.Visible := False;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSEqzCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vseqzcab');
end;

procedure TFmVSEqzCab.QrVSEqzCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSEqzArt.Close;
  QrVSEqzIts.Close;
  QrVSEqzSub.Close;
  QrNotas.Close;
  QrNotaAll.Close;
  QrNotaCrr.Close;
end;

procedure TFmVSEqzCab.QrVSEqzCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSEqzCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

