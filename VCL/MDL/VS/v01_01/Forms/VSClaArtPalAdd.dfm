object FmVSClaArtPalAdd: TFmVSClaArtPalAdd
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-017 :: Adi'#231#227'o de Pallet em Classifica'#231#227'o de Artigo de ' +
    'Ribeira'
  ClientHeight = 365
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 658
        Height = 32
        Caption = 'Adi'#231#227'o de Pallet em Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 658
        Height = 32
        Caption = 'Adi'#231#227'o de Pallet em Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 658
        Height = 32
        Caption = 'Adi'#231#227'o de Pallet em Classifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 203
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 203
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 203
        Align = alClient
        TabOrder = 0
        object Label35: TLabel
          Left = 20
          Top = 154
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object PnPartida: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 0
          object LaVSRibCad: TLabel
            Left = 48
            Top = 0
            Width = 28
            Height = 13
            Caption = 'IME-I:'
          end
          object Label30: TLabel
            Left = 832
            Top = 0
            Width = 47
            Height = 13
            Caption = 'ID Config:'
            Enabled = False
          end
          object Label31: TLabel
            Left = 916
            Top = 0
            Width = 32
            Height = 13
            Caption = 'IME-C:'
            Enabled = False
          end
          object Label5: TLabel
            Left = 752
            Top = 0
            Width = 65
            Height = 13
            Caption = 'Pallet Origem:'
            Enabled = False
          end
          object EdIMEI: TdmkEdit
            Left = 48
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCodigo: TdmkEdit
            Left = 832
            Top = 16
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_PRD_TAM_COR: TdmkEdit
            Left = 104
            Top = 16
            Width = 645
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdMovimCod: TdmkEdit
            Left = 916
            Top = 16
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSrcPallet: TdmkEdit
            Left = 752
            Top = 16
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GBTecla: TGroupBox
          Left = 2
          Top = 59
          Width = 1004
          Height = 94
          Align = alTop
          Caption = ' Configura'#231#227'o da tecla "1": '
          TabOrder = 1
          object Painel: TPanel
            Left = 2
            Top = 15
            Width = 1000
            Height = 77
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel6: TPanel
              Left = 43
              Top = 0
              Width = 486
              Height = 77
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
              end
              object LaVSRibCla: TLabel
                Left = 60
                Top = 0
                Width = 186
                Height = 13
                Caption = 'Nome do Artigo de Ribeira Classificado:'
              end
              object SbPallet1: TSpeedButton
                Left = 460
                Top = 16
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SbPallet1Click
              end
              object Label49: TLabel
                Left = 4
                Top = 40
                Width = 60
                Height = 13
                Caption = 'Localiza'#231#227'o:'
              end
              object EdPallet: TdmkEditCB
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdPalletChange
                DBLookupComboBox = CBPallet
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPallet: TdmkDBLookupComboBox
                Left = 59
                Top = 16
                Width = 398
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = DsVSPallet
                TabOrder = 1
                dmkEditCB = EdPallet
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdStqCenLoc: TdmkEditCB
                Left = 4
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'StqCenLoc'
                UpdCampo = 'StqCenLoc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStqCenLoc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBStqCenLoc: TdmkDBLookupComboBox
                Left = 60
                Top = 56
                Width = 421
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_LOC_CEN'
                ListSource = DsStqCenLoc
                TabOrder = 3
                dmkEditCB = EdStqCenLoc
                UpdType = utYes
                LocF7NameFldName = 'Nome'
                LocF7SQLMasc = '$#'
              end
            end
            object PnTecla: TPanel
              Left = 529
              Top = 0
              Width = 471
              Height = 77
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              Visible = False
              object Label3: TLabel
                Left = 4
                Top = 0
                Width = 33
                Height = 13
                Caption = 'Status:'
                FocusControl = DBEdit2
              end
              object Label4: TLabel
                Left = 108
                Top = 0
                Width = 93
                Height = 13
                Caption = 'Cliente preferencial:'
                FocusControl = DBEdit3
              end
              object Label2: TLabel
                Left = 4
                Top = 40
                Width = 66
                Height = 13
                Caption = 'Observa'#231#245'es:'
                FocusControl = DBEdit1
              end
              object DBEdit2: TDBEdit
                Left = 4
                Top = 16
                Width = 100
                Height = 21
                TabStop = False
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet
                TabOrder = 0
              end
              object DBEdit3: TDBEdit
                Left = 108
                Top = 16
                Width = 353
                Height = 21
                TabStop = False
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet
                TabOrder = 1
              end
              object DBEdit1: TDBEdit
                Left = 4
                Top = 56
                Width = 457
                Height = 21
                TabStop = False
                DataField = 'Nome'
                DataSource = DsVSPallet
                TabOrder = 2
              end
            end
            object PnBox: TPanel
              Left = 0
              Top = 0
              Width = 43
              Height = 77
              Align = alLeft
              BevelOuter = bvNone
              Caption = '1'
              Font.Charset = ANSI_CHARSET
              Font.Color = 10485760
              Font.Height = -37
              Font.Name = 'Arial Black'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
          end
        end
        object EdVSPwdDdClas: TEdit
          Left = 20
          Top = 170
          Width = 421
          Height = 21
          TabOrder = 2
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 321
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 251
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrVSPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'ORDER BY NO_PRD_TAM_COR'
      '')
    Left = 328
    Top = 37
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 328
    Top = 81
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 880
    Top = 112
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 880
    Top = 160
  end
end
