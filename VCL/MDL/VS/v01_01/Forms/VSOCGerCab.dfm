object FmVSOCGerCab: TFmVSOCGerCab
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-066 :: Gerenciamento de OCs'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de OCs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de OCs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de OCs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGVSPaCRIts: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 450
          Align = alClient
          DataSource = DsPaXxxCab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMOC
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGVSPaCRItsDrawColumnCell
          OnDblClick = DBGVSPaCRItsDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_ClaRcl'
              Title.Caption = 'Tipo'
              Width = 108
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CacCod'
              Title.Caption = 'OC'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPallet'
              Title.Caption = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSMovIts'
              Title.Caption = 'IME-I origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal01'
              Title.Caption = 'Pallet 1'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal02'
              Title.Caption = 'Pallet 2'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal03'
              Title.Caption = 'Pallet 3'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal04'
              Title.Caption = 'Pallet 4'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal05'
              Title.Caption = 'Pallet 5'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LstPal06'
              Title.Caption = 'Pallet 6'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFimCla_TXT'
              Title.Caption = 'Data/hora final'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSGerXxxCab'
              Title.Caption = 'Config.'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtOC: TBitBtn
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Menu'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOCClick
      end
    end
  end
  object QrVSPaXxxCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cla.CacCod, 1.000 ClaRcl, "Classifica'#231#227'o" NO_ClaRcl, '
      'cla.Codigo, cla.VSGerArt VSGerXxxCab, cla.VSMovIts, '
      '0.000 VSPallet, cla.LstPal01, cla.LstPal02, cla.LstPal03,  '
      'cla.LstPal04, cla.LstPal05, cla.LstPal06, cla.DtHrFimCla,  '
      'IF(cla.DtHrFimCla < "1900-01-01", "", DATE_FORMAT(  '
      'cla.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT '
      'FROM vspaclacaba cla '
      'WHERE cla.DtHrFimCla < "1900-01-01" '
      'OR cla.LstPal01 <> 0 OR cla.LstPal02 <> 0  '
      'OR cla.LstPal03 <> 0 OR cla.LstPal04 <> 0 '
      'OR cla.LstPal05 <> 0 OR cla.LstPal06 <> 0 '
      ' '
      'UNION '
      ' '
      'SELECT rcl.CacCod, 2.000 ClaRcl, "Reclassifica'#231#227'o" NO_ClaRcl, '
      
        'rcl.Codigo, rcl.VSGerRcl VSGerXxxCab, rcl.VSMovIts, rcl.VSPallet' +
        ' + '
      '0.000 VSPallet, rcl.LstPal01, rcl.LstPal02, rcl.LstPal03, '
      'rcl.LstPal04, rcl.LstPal05, rcl.LstPal06, rcl.DtHrFimCla, '
      'IF(rcl.DtHrFimCla < "1900-01-01", "", DATE_FORMAT(  '
      'rcl.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT '
      'FROM vsparclcaba rcl '
      'WHERE rcl.DtHrFimCla < "1900-01-01" '
      'OR rcl.LstPal01 <> 0 OR rcl.LstPal02 <> 0  '
      'OR rcl.LstPal03 <> 0 OR rcl.LstPal04 <> 0 '
      'OR rcl.LstPal05 <> 0 OR rcl.LstPal06 <> 0 '
      ' '
      'ORDER BY CacCod '
      ' '
      '')
    Left = 268
    Top = 196
    object QrVSPaXxxCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Required = True
    end
    object QrVSPaXxxCabClaRcl: TFloatField
      FieldName = 'ClaRcl'
      Required = True
    end
    object QrVSPaXxxCabNO_ClaRcl: TWideStringField
      FieldName = 'NO_ClaRcl'
      Required = True
      Size = 15
    end
    object QrVSPaXxxCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPaXxxCabVSGerXxxCab: TIntegerField
      FieldName = 'VSGerXxxCab'
      Required = True
    end
    object QrVSPaXxxCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
      Required = True
    end
    object QrVSPaXxxCabVSPallet: TFloatField
      FieldName = 'VSPallet'
      Required = True
    end
    object QrVSPaXxxCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrVSPaXxxCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
    end
    object QrVSPaXxxCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
    end
    object QrVSPaXxxCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
    end
    object QrVSPaXxxCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
    end
    object QrVSPaXxxCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
    end
    object QrVSPaXxxCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
    end
    object QrVSPaXxxCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
    end
    object QrVSPaXxxCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
    end
    object QrVSPaXxxCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
    end
    object QrVSPaXxxCabDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
      Required = True
    end
    object QrVSPaXxxCabDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
      Size = 24
    end
  end
  object DsPaXxxCab: TDataSource
    DataSet = QrVSPaXxxCab
    Left = 264
    Top = 244
  end
  object PMOC: TPopupMenu
    Left = 128
    Top = 324
    object ContinuarOCconfigurada1: TMenuItem
      Caption = '&Continuar OC configurada'
      OnClick = ContinuarOCconfigurada1Click
    end
    object GerenciamentodaOC1: TMenuItem
      Caption = '&Gerenciamento da OC '
      OnClick = GerenciamentodaOC1Click
    end
  end
end
