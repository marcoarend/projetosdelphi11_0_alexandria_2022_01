unit VSClaArtSelA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSClaArtSelA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_FICHA: TWideStringField;
    QrVSMovItsCUSTO_M2: TFloatField;
    QrVSMovItsCUSTO_P2: TFloatField;
    PnPartida: TPanel;
    LaVSRibCad: TLabel;
    EdIMEI: TdmkEditCB;
    CBIMEI: TdmkDBLookupComboBox;
    QrVSPaClaCab: TmySQLQuery;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrVSMovItsIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    Label35: TLabel;
    EdVSPwdDdClas: TEdit;
    Qry: TMySQLQuery;
    CkSomentePositivos: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkSomentePositivosClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAbertos();
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSClaArtSelA: TFmVSClaArtSelA;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSClaArtSelA.BtOKClick(Sender: TObject);
var
  VSMovIts, VSGerArt: Integer;
begin
  if MyObjects.FIC(EdIMEI.ValueVariant = 0, EdIMEI, 'Informe o IME-I!') then
    Exit;
  if VS_CRC_PF.SenhaVSPwdDdNaoConfere(EdVSPwdDdClas.Text) then
    Exit;
  VSGerArt := QrVSMovItsCodigo.Value;
  VSMovIts := QrVSMovItsControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vspaclacaba ',
  'WHERE VSMovIts=' + Geral.FF0(VSMovIts),
  'AND VSGerArt=' + Geral.FF0(VSGerArt),
  '']);
  //
  FCodigo := 0;
  FCacCod := 0;
  case QrVSPaClaCab.RecordCount of
    0: Geral.MB_Aviso('Configura��o de classifica��o n�o localizada!');
    1:
    begin
      FCodigo := QrVSPaClaCabCodigo.Value;
      FCacCod := QrVSPaClaCabCacCod.Value;
      if VSGerArt > 0 then
        FMovimID := emidClassArtXXUni
      else
        FMovimID := emidReclasXXUni;
      //
    end;
    else Geral.MB_Aviso('Foram localizadas ' + Geral.FF0(QrVSPaClaCab.RecordCount) +
    ' configura��es de classifica��o!' + sLineBreak +
    'Por seguran�a nenhuma ser� ulilizada' + sLineBreak +
    'Avise a DERMATEK!');
  end;
  if FCodigo <> 0 then
    Close;
end;

procedure TFmVSClaArtSelA.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  FCacCod := 0;
  //FMovimID := 0;
  Close;
end;

procedure TFmVSClaArtSelA.CkSomentePositivosClick(Sender: TObject);
begin
  ReopenAbertos();
end;

procedure TFmVSClaArtSelA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClaArtSelA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  //
  ReopenAbertos();
end;

procedure TFmVSClaArtSelA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClaArtSelA.ReopenAbertos();
var
  Corda, SQL_Positivos: String;
begin
  // ini 2023-05-06
  Screen.Cursor := crHourGlass;
  try
  if CkSomentePositivos.Checked then
    SQL_Positivos := 'AND wmi.Pecas > 0 AND wmi.AreaM2 > 0'
  else
    SQL_Positivos := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrCfgCla > "1900-01-01" ',
  '  AND DtHrFimCla < "1900-01-01" ',
  '']);
  Corda := MyObjects.CordaDeQuery(Qry, 'MovimCod', '-999999999');
  // fim 2023-05-06
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(wmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(wmi.Ficha=0, "V�rias", wmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(CONVERT(wmi.Controle, CHAR), " ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  'IF(wmi.Ficha=0, "", CONCAT(" - S�rie / Ficha RMP ", wmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND wmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)),
  // ini 2023-05-06
(*
  'AND wmi.MovimCod IN ( ',
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrCfgCla > "1900-01-01" ',
  '  AND DtHrFimCla < "1900-01-01" ',
  ') ',
*)
  'AND wmi.MovimCod IN (' + Corda + ')',
  SQL_Positivos,
  '']);
  finally
    Screen.Cursor := crDefault;
  end;
  // fim 2023-05-06
end;

end.
