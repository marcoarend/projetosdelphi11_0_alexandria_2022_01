unit VSInnNatPend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet;

type
  TFmVSInnNatPend = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPend: TMySQLQuery;
    DsPend: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    QrPendPecas: TFloatField;
    QrPendSdoVrtPeca: TFloatField;
    QrPendCodigo: TIntegerField;
    QrPendControle: TIntegerField;
    QrPendMovimCod: TIntegerField;
    QrPendMovimNiv: TIntegerField;
    QrPendMovimTwn: TIntegerField;
    QrPendEmpresa: TIntegerField;
    QrPendTerceiro: TIntegerField;
    QrPendCliVenda: TIntegerField;
    QrPendMovimID: TIntegerField;
    QrPendLnkNivXtr1: TIntegerField;
    QrPendLnkNivXtr2: TIntegerField;
    QrPendDataHora: TDateTimeField;
    QrPendPallet: TIntegerField;
    QrPendGraGruX: TIntegerField;
    QrPendPecas_1: TFloatField;
    QrPendPesoKg: TFloatField;
    QrPendAreaM2: TFloatField;
    QrPendAreaP2: TFloatField;
    QrPendValorT: TFloatField;
    QrPendSrcMovID: TIntegerField;
    QrPendSrcNivel1: TIntegerField;
    QrPendSrcNivel2: TIntegerField;
    QrPendSdoVrtPeca_1: TFloatField;
    QrPendSdoVrtArM2: TFloatField;
    QrPendObserv: TWideStringField;
    QrPendFicha: TIntegerField;
    QrPendMisturou: TSmallintField;
    QrPendCustoMOKg: TFloatField;
    QrPendCustoMOTot: TFloatField;
    QrPendLk: TIntegerField;
    QrPendDataCad: TDateField;
    QrPendDataAlt: TDateField;
    QrPendUserCad: TIntegerField;
    QrPendUserAlt: TIntegerField;
    QrPendAlterWeb: TSmallintField;
    QrPendAtivo: TSmallintField;
    QrPendSrcGGX: TIntegerField;
    QrPendSdoVrtPeso: TFloatField;
    QrPendSerieFch: TIntegerField;
    QrPendFornecMO: TIntegerField;
    QrPendValorMP: TFloatField;
    QrPendDstMovID: TIntegerField;
    QrPendDstNivel1: TIntegerField;
    QrPendDstNivel2: TIntegerField;
    QrPendDstGGX: TIntegerField;
    QrPendQtdGerPeca: TFloatField;
    QrPendQtdGerPeso: TFloatField;
    QrPendQtdGerArM2: TFloatField;
    QrPendQtdGerArP2: TFloatField;
    QrPendQtdAntPeca: TFloatField;
    QrPendQtdAntPeso: TFloatField;
    QrPendQtdAntArM2: TFloatField;
    QrPendQtdAntArP2: TFloatField;
    QrPendAptoUso: TSmallintField;
    QrPendNotaMPAG: TFloatField;
    QrPendMarca: TWideStringField;
    QrPendTpCalcAuto: TIntegerField;
    QrPendZerado: TSmallintField;
    QrPendEmFluxo: TSmallintField;
    QrPendLnkIDXtr: TIntegerField;
    QrPendCustoMOM2: TFloatField;
    QrPendNotFluxo: TIntegerField;
    QrPendFatNotaVNC: TFloatField;
    QrPendFatNotaVRC: TFloatField;
    QrPendPedItsLib: TIntegerField;
    QrPendPedItsFin: TIntegerField;
    QrPendPedItsVda: TIntegerField;
    QrPendGSPInnNiv2: TIntegerField;
    QrPendGSPArtNiv2: TIntegerField;
    QrPendReqMovEstq: TIntegerField;
    QrPendStqCenLoc: TIntegerField;
    QrPendItemNFe: TIntegerField;
    QrPendVSMorCab: TIntegerField;
    QrPendVSMulFrnCab: TIntegerField;
    QrPendClientMO: TIntegerField;
    QrPendCustoPQ: TFloatField;
    QrPendKgCouPQ: TFloatField;
    QrPendNFeSer: TSmallintField;
    QrPendNFeNum: TIntegerField;
    QrPendVSMulNFeCab: TIntegerField;
    QrPendGGXRcl: TIntegerField;
    QrPendJmpMovID: TIntegerField;
    QrPendJmpNivel1: TIntegerField;
    QrPendJmpNivel2: TIntegerField;
    QrPendJmpGGX: TIntegerField;
    QrPendRmsMovID: TIntegerField;
    QrPendRmsNivel1: TIntegerField;
    QrPendRmsNivel2: TIntegerField;
    QrPendRmsGGX: TIntegerField;
    QrPendGSPSrcMovID: TIntegerField;
    QrPendGSPSrcNiv2: TIntegerField;
    QrPendGSPJmpMovID: TIntegerField;
    QrPendGSPJmpNiv2: TIntegerField;
    QrPendDtCorrApo: TDateTimeField;
    QrPendMovCodPai: TIntegerField;
    QrPendIxxMovIX: TSmallintField;
    QrPendIxxFolha: TIntegerField;
    QrPendIxxLinha: TIntegerField;
    QrPendAWServerID: TIntegerField;
    QrPendAWStatSinc: TSmallintField;
    QrPendIuvpei: TIntegerField;
    QrPendCusFrtAvuls: TFloatField;
    QrPendCusFrtMOEnv: TFloatField;
    QrPendCusFrtMORet: TFloatField;
    QrPendCustoMOPc: TFloatField;
    QrPendCustoComiss: TFloatField;
    QrPendPerceComiss: TFloatField;
    QrPendCusKgComiss: TFloatField;
    QrPendCredValrImposto: TFloatField;
    QrPendCredPereImposto: TFloatField;
    QrPendRpICMS: TFloatField;
    QrPendRpICMSST: TFloatField;
    QrPendRpPIS: TFloatField;
    QrPendRpCOFINS: TFloatField;
    QrPendRvICMS: TFloatField;
    QrPendRvICMSST: TFloatField;
    QrPendRvPIS: TFloatField;
    QrPendRvCOFINS: TFloatField;
    QrPendRpIPI: TFloatField;
    QrPendRvIPI: TFloatField;
    QrPendGraGru1: TIntegerField;
    QrPendNO_PRD_TAM_COR: TWideStringField;
    Panel5: TPanel;
    GroupBox13: TGroupBox;
    TPCompraIni: TdmkEditDateTimePicker;
    CkCompraIni: TCheckBox;
    CkCompraFim: TCheckBox;
    TPCompraFim: TdmkEditDateTimePicker;
    GroupBox1: TGroupBox;
    TPViagemIni: TdmkEditDateTimePicker;
    CkViagemIni: TCheckBox;
    CkViagemFim: TCheckBox;
    TPViagemFim: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    TPEntradaIni: TdmkEditDateTimePicker;
    CkEntradaIni: TCheckBox;
    CkEntradaFim: TCheckBox;
    TPEntradaFim: TdmkEditDateTimePicker;
    CkItensNaoEncer: TCheckBox;
    CkItensComSaldo: TCheckBox;
    frxWET_CURTI_241_01: TfrxReport;
    frxDsPend: TfrxDBDataset;
    EdMarca: TdmkEdit;
    Label1: TLabel;
    QrItens: TMySQLQuery;
    frxDsItens: TfrxDBDataset;
    QrItensPecas: TFloatField;
    QrItensPesoKg: TFloatField;
    QrItensAreaM2: TFloatField;
    QrItensMediaM2: TFloatField;
    QrItenskgM2: TFloatField;
    QrItensCodigo: TIntegerField;
    QrItensMovimCod: TIntegerField;
    QrItensControle: TIntegerField;
    QrItensDstGGX: TIntegerField;
    QrItensSrcGGX: TIntegerField;
    QrItensSrcNivel1: TIntegerField;
    QrItensSrcNivel2: TIntegerField;
    QrItensGraGru1: TIntegerField;
    QrItensNO_PRD_TAM_COR: TWideStringField;
    QrItensObserv: TWideStringField;
    QrItensMarca: TWideStringField;
    QrPendPercPc: TFloatField;
    QrItensPercPc: TFloatField;
    QrSumP: TMySQLQuery;
    QrSumPPercPc: TFloatField;
    QrSumPPecas: TFloatField;
    QrSumPSdoVrtPeca: TFloatField;
    QrSumPPesoKg: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_241_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrPendAfterScroll(DataSet: TDataSet);
    procedure QrItensCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure AbreOutrosDados();
  public
    { Public declarations }
  end;

  var
  FmVSInnNatPend: TFmVSInnNatPend;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, ModuleGeral;

{$R *.DFM}

procedure TFmVSInnNatPend.AbreOutrosDados;
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCur, Dmod.MyDB, [
  'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, ',
  'SUM(-vmi.QtdGerArM2) AreaM2, ',
  'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, ',
  'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2',
  'FROM vsmovits vmi   ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=vmi.DstGGX ',
  'WHERE SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  'AND NOT cou.Bastidao IN (8,9) ',
  'AND SrcNivel1 IN (' + Corda + ')',
  ' ']);
  //Geral.MB_Teste(QrSumCur.SQL.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumGel, Dmod.MyDB, [
  'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, ',
  'SUM(-vmi.QtdGerArM2) AreaM2, ',
  'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, ',
  'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2',
  'FROM vsmovits vmi   ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=vmi.DstGGX ',
  'WHERE SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  'AND cou.Bastidao = 8 ',
  'AND SrcNivel1 IN (' + Corda + ')',
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumTap, Dmod.MyDB, [
  'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, ',
  'SUM(-vmi.QtdGerArM2) AreaM2, ',
  'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, ',
  'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2',
  'FROM vsmovits vmi   ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=vmi.DstGGX ',
  'WHERE SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  'AND cou.Bastidao = 9 ',
  'AND SrcNivel1 IN (' + Corda + ')',
  ' ']);
*)

(*
object QrSumCur: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
    'SUM(-vmi.QtdGerArM2) AreaM2, '
    'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
    'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
    'FROM vsmovits vmi   ')
  Left = 392
  Top = 57
  object QrSumCurPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrSumCurPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
  object QrSumCurAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrSumCurMediaM2: TFloatField
    FieldName = 'MediaM2'
  end
  object QrSumCurkgM2: TFloatField
    FieldName = 'kgM2'
  end
end
object QrSumGel: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
    'SUM(-vmi.QtdGerArM2) AreaM2, '
    'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
    'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
    'FROM vsmovits vmi   ')
  Left = 392
  Top = 105
  object QrSumGelPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrSumGelPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
  object QrSumGelAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrSumGelMediaM2: TFloatField
    FieldName = 'MediaM2'
  end
  object QrSumGelkgM2: TFloatField
    FieldName = 'kgM2'
  end
end
object QrSumTap: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT SUM(-vmi.Pecas) Pecas, SUM(-vmi.PesoKg) PesoKg, '
    'SUM(-vmi.QtdGerArM2) AreaM2, '
    'SUM(vmi.QtdGerArM2) / SUM(vmi.Pecas) MediaM2, '
    'SUM(vmi.PesoKg) / SUM(vmi.QtdGerArM2) kgM2'
    'FROM vsmovits vmi   ')
  Left = 392
  Top = 157
  object QrSumTapPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrSumTapPesoKg: TFloatField
    FieldName = 'PesoKg'
  end
  object QrSumTapAreaM2: TFloatField
    FieldName = 'AreaM2'
  end
  object QrSumTapMediaM2: TFloatField
    FieldName = 'MediaM2'
  end
  object QrSumTapkgM2: TFloatField
    FieldName = 'kgM2'
  end
end

*)

end;

procedure TFmVSInnNatPend.BtOKClick(Sender: TObject);
var
  SQL_Compra, SQL_Viagem, SQL_Entrada, SQL_ItensNaoEncer, SQL_ItensComSaldo,
  SQL_Marca: String;
  Corda: String;
begin
  SQL_ItensNaoEncer := '';
  SQL_ItensComSaldo := '';
  SQL_Marca := '';
  //
  SQL_Compra := dmkPF.SQL_Periodo('AND vic.DtCompra ', // vmi.DataHora '
      TPCompraIni.Date, TPCompraFim.Date, CkCompraIni.Checked, CkCompraFim.Checked);
  SQL_Viagem := dmkPF.SQL_Periodo('AND vic.DtViagem ',
      TPViagemIni.Date, TPViagemFim.Date, CkViagemIni.Checked, CkViagemFim.Checked);
  SQL_Entrada := dmkPF.SQL_Periodo('AND vic.DtEntrada ',
      TPEntradaIni.Date, TPEntradaFim.Date, CkEntradaIni.Checked, CkEntradaFim.Checked);
  //
  if CkItensComSaldo.Checked then
    SQL_ItensComSaldo := 'AND vmi.SdoVrtPeca > 0  ';
  if CkItensNaoEncer.Checked then
    SQL_ItensNaoEncer := 'AND vic.DtEnceRend < 2  ';
  //
  if EdMarca.Text <> '' then
    SQL_Marca := 'AND vmi.Marca LIKE "' + EdMarca.Text + '"';

  UnDmkDAC_PF.AbreMySQLQuery0(QrPend, Dmod.MyDB, [
  'SELECT  ',
  'vmi.SdoVrtPeca / vmi.Pecas * 100 PercPc, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,   ',
  ' ',
  'vmi.Pecas, vmi.SdoVrtPeca, vmi.*   ',
  'FROM vsmovits vmi  ',
  'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo  ',
  ' ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vmi.MovimID=1  ',
  (*'AND vic.DtCompra BETWEEN "2000-01-01" AND "2099-12-31"  ',
  'AND vic.DtViagem BETWEEN "2000-01-01" AND "2099-12-31"  ',
  'AND vic.DtEntrada BETWEEN "2000-01-01" AND "2099-12-31"  ',*)
  SQL_Compra, SQL_Viagem, SQL_Entrada,
  (*'AND vmi.SdoVrtPeca>0  ',*)
  SQL_ItensComSaldo,
  (*'AND vic.DtEnceRend < 2  ',*)
  SQL_ItensNaoEncer,
  SQL_Marca,
  ' ']);
  Corda := MyObjects.CordaDeQuery(QrPend, 'Codigo', '0');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumP, Dmod.MyDB, [
  'SELECT SUM(vmi.SdoVrtPeca) / SUM(vmi.Pecas) * 100 PercPc,  ',
  'SUM(vmi.Pecas) Pecas, SUM(vmi.SdoVrtPeca) SdoVrtPeca, ',
  'SUM(vmi.PesoKg) PesoKg ',
  'FROM vsmovits vmi   ',
  'LEFT JOIN vsinncab vic ON vic.Codigo=vmi.Codigo   ',
  'WHERE vmi.MovimID=1  ',
  (*'AND vic.DtCompra BETWEEN "2000-01-01" AND "2099-12-31"  ',
  'AND vic.DtViagem BETWEEN "2000-01-01" AND "2099-12-31"  ',
  'AND vic.DtEntrada BETWEEN "2000-01-01" AND "2099-12-31"  ',*)
  SQL_Compra, SQL_Viagem, SQL_Entrada,
  (*'AND vmi.SdoVrtPeca>0  ',*)
  SQL_ItensComSaldo,
  (*'AND vic.DtEnceRend < 2  ',*)
  SQL_ItensNaoEncer,
  SQL_Marca,
  '']);
  //Geral.MB_Teste(QrSumP.SQL.Text);
  //
  AbreOutrosDados();
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_241_01, [
    DmodG.frxDsDono,
    frxDsPend,
    frxDsItens
  ]);
  //
  MyObjects.FrxMostra(frxWET_CURTI_241_01, 'In Natura Pendente');
end;

procedure TFmVSInnNatPend.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnNatPend.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnNatPend.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPCompraIni.Date := Date - 30;
  TPCompraFim.Date := Date;
  //
  TPEntradaIni.Date := Date - 30;
  TPEntradaFim.Date := Date;
  //
  TPViagemIni.Date := Date - 30;
  TPViagemFim.Date := Date;
  //
end;

procedure TFmVSInnNatPend.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInnNatPend.frxWET_CURTI_241_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_PeriodoCompra' then
    Value := dmkPF.PeriodoImp1(TPCompraIni.Date, TPCompraFim.Date,
    CkCompraIni.Checked, CkCompraFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_PeriodoViagem' then
    Value := dmkPF.PeriodoImp1(TPViagemIni.Date, TPViagemFim.Date,
    CkViagemIni.Checked, CkViagemFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_PeriodoEntrada' then
    Value := dmkPF.PeriodoImp1(TPEntradaIni.Date, TPEntradaFim.Date,
    CkEntradaIni.Checked, CkEntradaFim.Checked, '', 'at�', '')
  else
  if VarName ='VARF_Marca' then
    Value := EdMarca.ValueVariant
  else
  if VarName = 'VARF_PecasP' then
    Value := QrSumPPecas.Value
  else
  if VarName = 'VARF_KgP' then
    Value := QrSumPPesoKg.Value
  else
  if VarName = 'VARF_SdoPcP' then
    Value := QrSumPSdoVrtPeca.Value
  else
  if VarName = 'VARF_PercPcP' then
  begin
    if QrSumPPecas.Value > 0 then
      Value := QrSumPSdoVrtPeca.Value / QrSumPPecas.Value * 100
    else
      Value := 0;
  end
(*
  else
  if VarName = 'VARF_PecasI' then
    Value := QrSumCurPecas.Value
  else
  if VarName = 'VARF_AreaM2I' then
    Value := QrSumCurAreaM2.Value
  else
  if VarName = 'VARF_MediaM2' then
    Value := QrSumCurMediaM2.Value
  else
  if VarName = 'VARF_PesoKgI' then
    Value := QrSumCurPesoKg.Value
  else
  if VarName = 'VARF_KgM2I' then
    Value := QrSumCurKgM2.Value
  else
  if VarName = 'VARF_PercI' then
  begin
    if QrSumPPecas.Value > 0 then
      Value := QrSumCurPecas.Value / QrSumPPecas.Value * 100
    else
      Value := 0;
  end
  else
  if VarName = 'VARF_PecasG' then
    Value := QrSumGelPecas.Value
  else
  if VarName = 'VARF_PesoKgG' then
    Value := QrSumGelPesoKg.Value
  else
  if VarName = 'VARF_PercG' then
  begin
    if QrSumPPecas.Value > 0 then
      Value := QrSumGelPecas.Value / QrSumPPecas.Value * 100
    else
      Value := 0;
  end
  else
  if VarName = 'VARF_PecasT' then
    Value := QrSumTapPecas.Value
  else
  if VarName = 'VARF_PesoKgT' then
    Value := QrSumTapPesoKg.Value
  else
  if VarName = 'VARF_PercT' then
  begin
    if QrSumPPecas.Value > 0 then
      Value := QrSumTapPecas.Value / QrSumPPecas.Value * 100
    else
      Value := 0;
  end
*)
end;

procedure TFmVSInnNatPend.QrItensCalcFields(DataSet: TDataSet);
begin
  if QrPendPecas.Value > 0 then
    QrItensPercPc.Value :=
      QrItensPecas.Value / QrPendPecas.Value * 100
  else
    QrItensPercPc.Value := 0;
end;

procedure TFmVSInnNatPend.QrPendAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItens, Dmod.MyDB, [
  'SELECT -vmi.Pecas Pecas, -vmi.PesoKg PesoKg, ',
  '-vmi.QtdGerArM2 AreaM2, ',
  'vmi.QtdGerArM2 / vmi.Pecas MediaM2, ',
  'vmi.PesoKg / vmi.QtdGerArM2 kgM2,   ',
  'vmi.Codigo, vmi.MovimCod, vmi.Controle,   ',
  'vmi.DstGGX, vmi.SrcGGX, vmi.SrcNivel1, vmi.SrcNivel2,  ',
  'ggx.GraGru1, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR,  ',
  'vmi.Marca, vmi.Observ  ',
  'FROM vsmovits vmi   ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.DstGGX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE SrcMovID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  'AND SrcNivel1=' + Geral.FF0(QrPendCodigo.Value),
  ' ']);
  //Geral.MB_Teste(QrItens.SQL.Text);
end;

end.
