object FmVSMarcaAjusta: TFmVSMarcaAjusta
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-270 :: Ajuste de Marcas'
  ClientHeight = 629
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 358
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 310
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 212
        Height = 32
        Caption = 'Ajuste de Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 212
        Height = 32
        Caption = 'Ajuste de Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 212
        Height = 32
        Caption = 'Ajuste de Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 406
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 402
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 406
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 260
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 258
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtSubstituir: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Substituir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtSubstituirClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 406
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 45
      Width = 406
      Height = 422
      Align = alClient
      DataSource = DsAjusMarca
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ajustado'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 406
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 812
      object LaSrc: TLabel
        Left = 16
        Top = 4
        Width = 113
        Height = 13
        Caption = 'Caracter(es) a substituir:'
      end
      object LaDst: TLabel
        Left = 168
        Top = 4
        Width = 127
        Height = 13
        Caption = 'Caracter(es) substituinte(s):'
      end
      object EdSrc: TdmkEdit
        Left = 16
        Top = 20
        Width = 140
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDst: TdmkEdit
        Left = 168
        Top = 20
        Width = 140
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object DsAjusMarca: TDataSource
    DataSet = QrAjusMarca
    Left = 52
    Top = 308
  end
  object QrAjusMarca: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAjusMarcaAfterOpen
    BeforeClose = QrAjusMarcaBeforeClose
    SQL.Strings = (
      'SELECT Controle, Marca, SUBSTRING_INDEX(Marca, " ", 1) P, '
      'REPLACE(Marca, "  ", " ") Ajustado  '
      'FROM vsmovits '
      'WHERE Marca LIKE "%  %" '
      'ORDER BY Controle ')
    Left = 53
    Top = 256
    object QrAjusMarcaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAjusMarcaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrAjusMarcaAjustado: TWideStringField
      FieldName = 'Ajustado'
    end
  end
end
