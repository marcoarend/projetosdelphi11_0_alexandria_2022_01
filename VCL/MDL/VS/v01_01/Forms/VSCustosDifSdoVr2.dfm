object FmVSCustosDifSdoVr2: TFmVSCustosDifSdoVr2
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-253 :: Custos Divergentes de Saldo Virtual'
  ClientHeight = 587
  ClientWidth = 868
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 868
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 820
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 772
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 438
        Height = 32
        Caption = 'Custos Divergentes de Saldo Virtual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 438
        Height = 32
        Caption = 'Custos Divergentes de Saldo Virtual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 438
        Height = 32
        Caption = 'Custos Divergentes de Saldo Virtual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 460
    Width = 868
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 864
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 383
        Height = 17
        Caption = 'D'#234' um duplo clique no item para ver seus imeis de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 383
        Height = 17
        Caption = 'D'#234' um duplo clique no item para ver seus imeis de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 864
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 517
    Width = 868
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 722
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 720
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label5: TLabel
        Left = 288
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Fator limite:'
      end
      object SbFatorLimite: TSpeedButton
        Left = 380
        Top = 20
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbFatorLimiteClick
      end
      object BtEntrada: TBitBtn
        Tag = 18
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Entrada'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEntradaClick
      end
      object BtBaixa: TBitBtn
        Tag = 11
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Baixa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtBaixaClick
      end
      object EdFatorLimite: TdmkEdit
        Left = 288
        Top = 20
        Width = 89
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 10
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '3'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,0000000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 868
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 0
      Top = 298
      Width = 868
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 285
    end
    object Label1: TLabel
      Left = 0
      Top = 81
      Width = 868
      Height = 13
      Align = alTop
      Caption = 'Itens de ENTRADA com valores divergentes'
      ExplicitWidth = 211
    end
    object Label3: TLabel
      Left = 0
      Top = 303
      Width = 868
      Height = 13
      Align = alTop
      Caption = 'Itens de BAIXA do item selecionado'
      ExplicitWidth = 169
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 94
      Width = 868
      Height = 204
      Align = alTop
      DataSource = DmModVS.DsCustosDifSdoVr2
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnCellClick = DGDadosCellClick
      OnDblClick = DGDadosDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Arquivo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas_A'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg_A'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2_A'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT_A'
          Title.Caption = '$ total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo P'#231
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtArM2'
          Title.Caption = 'Sdo m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoUnit_A'
          Title.Caption = '$ unit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtValT_A'
          Title.Caption = 'Sdo Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas_B'
          Title.Caption = 'Baixa P'#231
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg_B'
          Title.Caption = 'Baixa kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2_B'
          Title.Caption = 'Baixa m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT_B'
          Title.Caption = 'Baixa Val'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifPc'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifM2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifVal'
          Title.Caption = '$ Diferen'#231'a'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatorLimitante'
          Title.Caption = 'Fator Limite'
          Visible = True
        end>
    end
    object PnPesquisa: TPanel
      Left = 0
      Top = 0
      Width = 868
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label2: TLabel
        Left = 8
        Top = 40
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label8: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label4: TLabel
        Left = 556
        Top = 40
        Width = 85
        Height = 13
        Caption = '$ margem de erro:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 56
        Width = 481
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 16
        Width = 477
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 0
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object BtTeste2: TBitBtn
        Tag = 22
        Left = 668
        Top = 36
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtTeste2Click
      end
      object CkTemIMEiMrt: TCheckBox
        Left = 552
        Top = 16
        Width = 197
        Height = 17
        Caption = 'Inclui o arquivo morto na pesquisa.'
        TabOrder = 5
      end
      object EdMargemErro: TdmkEdit
        Left = 556
        Top = 56
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 10
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0100000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.010000000000000000
        ValWarn = False
      end
    end
    object DBGItens: TdmkDBGridZTO
      Left = 0
      Top = 316
      Width = 868
      Height = 96
      Align = alClient
      DataSource = DsOrigens
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGItensDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimCod'
          Title.Caption = 'IME-C'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimID'
          Title.Caption = 'Mov.ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimNiv'
          Title.Caption = 'Mov.Niv'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = '$ total'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoUnit'
          Title.Caption = '$ unit.'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Arquivo'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65531
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, ggy.Nome NO_GraGruY, '
      'ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'unm.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 212
    Top = 60
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object QrGraGruXNO_GraGruY: TWideStringField
      FieldName = 'NO_GraGruY'
      Size = 255
    end
    object QrGraGruXGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 212
    Top = 112
  end
  object QrOrigens: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Codigo, MovimCod, Controle, '
      'MovimID, MovimNiv, GraGruX, Pecas, PesoKg, '
      'AreaM2, ValorT'
      'FROM bluederm.vsmovits vmi  '
      'WHERE SrcNivel2 = 2')
    Left = 452
    Top = 248
    object QrOrigensCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOrigensMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrOrigensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOrigensMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrOrigensMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrOrigensGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrOrigensPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrOrigensPesoKg: TFloatField
      DefaultExpression = '#,###,###,##0.000;-#,###,###,##0.00; '
      FieldName = 'PesoKg'
      Required = True
    end
    object QrOrigensAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrOrigensValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrigensCustoUnit: TFloatField
      FieldName = 'CustoUnit'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrOrigensNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Size = 6
    end
    object QrOrigensID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsOrigens: TDataSource
    DataSet = QrOrigens
    Left = 452
    Top = 296
  end
  object PMEntrada: TPopupMenu
    OnPopup = PMEntradaPopup
    Left = 52
    Top = 441
    object Recalculaselecionado1: TMenuItem
      Caption = 'Recalcula saldo do item &Selecionado'
      OnClick = Recalculaselecionado1Click
    end
    object RecalculaTodos1: TMenuItem
      Caption = 'Recalcula saldo de &Todos itens ativos'
      OnClick = RecalculaTodos1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CorrigeovalordetodositensdeBaixa1: TMenuItem
      Caption = '1. Corrige o valor de todos itens de Baixa'
      OnClick = CorrigeovalordetodositensdeBaixa1Click
    end
    object Distribuidiferenaentreositens1: TMenuItem
      Caption = '2. Distribui a diferen'#231'a entre os itens de BAIXA'
      object DaEntradaselecionada1: TMenuItem
        Caption = 'Da &entrada selecionada '
        OnClick = DaEntradaselecionada1Click
      end
      object Detodasentradas1: TMenuItem
        Caption = 'De &todas entradas'
        OnClick = Detodasentradas1Click
      end
    end
  end
  object PMBaixa: TPopupMenu
    OnPopup = PMBaixaPopup
    Left = 188
    Top = 456
    object RecalculaValorpelaentrada1: TMenuItem
      Caption = 'Corrige o &Valor pela entrada do item selecionado'
      OnClick = RecalculaValorpelaentrada1Click
    end
  end
end
