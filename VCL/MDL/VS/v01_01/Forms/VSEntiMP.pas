unit VSEntiMP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGridZTO, Db, DmkDAC_PF,
  mySQLDbTables, dmkDBGrid, DBCtrls, dmkEdit, ComCtrls, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkGeral, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmVSEntiMP = class(TForm)
    PainelBase: TPanel;
    Panel1: TPanel;
    QrVSEntiMP: TmySQLQuery;
    DsVSEntiMP: TDataSource;
    PnInsUpd: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTI: TWideStringField;
    PnControla: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnConfirma: TPanel;
    Panel6: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Panel7: TPanel;
    GroupBox4: TGroupBox;
    RgCMIUnida: TRadioGroup;
    RgCMIMoeda: TRadioGroup;
    RgCMIPerio: TRadioGroup;
    GroupBox3: TGroupBox;
    RgAGIUnida: TRadioGroup;
    RgAGIMoeda: TRadioGroup;
    RgAGIPerio: TRadioGroup;
    GroupBox2: TGroupBox;
    RgCPLUnida: TRadioGroup;
    RgCPLMoeda: TRadioGroup;
    RgCPLPerio: TRadioGroup;
    GroupBox1: TGroupBox;
    RgCMPUnida: TRadioGroup;
    RgCMPMoeda: TRadioGroup;
    RgCMPPerio: TRadioGroup;
    GroupBox5: TGroupBox;
    RGCPLSit: TRadioGroup;
    Label13: TLabel;
    RgCMP_IDQV: TRadioGroup;
    Panel8: TPanel;
    dmkEdCMPPreco: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    dmkEdCMPFrete: TdmkEdit;
    dmkEdCMP_LPQV: TdmkEdit;
    Label14: TLabel;
    Panel9: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    dmkEdCPL_LPQV: TdmkEdit;
    Panel10: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdAGI_LPQV: TdmkEdit;
    Panel11: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dmkEdCMI_LPQV: TdmkEdit;
    dmkEdCPLPreco: TdmkEdit;
    dmkEdCPLFrete: TdmkEdit;
    dmkEdAGIPreco: TdmkEdit;
    dmkEdAGIFrete: TdmkEdit;
    dmkEdCMIPreco: TdmkEdit;
    dmkEdCMIFrete: TdmkEdit;
    RgCPL_IDQV: TRadioGroup;
    RgAGI_IDQV: TRadioGroup;
    RgCMI_IDQV: TRadioGroup;
    Panel12: TPanel;
    EdContatos: TdmkEdit;
    Label01: TLabel;
    EdLocalEntrg: TdmkEdit;
    Label4: TLabel;
    EdCorretor: TdmkEditCB;
    Label5: TLabel;
    CBCorretor: TdmkDBLookupComboBox;
    dmkEdComissao: TdmkEdit;
    Label6: TLabel;
    EdCondPagto: TdmkEdit;
    Label7: TLabel;
    EdCondComis: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    dmkEdDescAdiant: TdmkEdit;
    QrCorretores: TmySQLQuery;
    DsCorretores: TDataSource;
    QrCorretoresCodigo: TIntegerField;
    QrCorretoresNOMEENTI: TWideStringField;
    QrTransporte: TmySQLQuery;
    QrTransporteCodigo: TIntegerField;
    QrTransporteNOMEENTIDADE: TWideStringField;
    DsTransporte: TDataSource;
    Label23: TLabel;
    EdTransporte: TdmkEditCB;
    CBTransporte: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    RGAbate: TRadioGroup;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    dmkDBGrid1: TdmkDBGridZTO;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet3: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid4: TDBGrid;
    Label24: TLabel;
    dmkEdSdoIniPele: TdmkEdit;
    Memo1: TMemo;
    Panel14: TPanel;
    Panel5: TPanel;
    LaFornecedor: TLabel;
    CBEntiMP: TdmkDBLookupComboBox;
    EdEntiMP: TdmkEditCB;
    Panel15: TPanel;
    Panel13: TPanel;
    Label25: TLabel;
    EdLoteLetras: TdmkEdit;
    EdLoteFormat: TdmkEdit;
    Panel4: TPanel;
    Label11: TLabel;
    EdMaskLetras: TdmkEdit;
    EdMaskFormat: TdmkEdit;
    RgPreDescarn: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel16: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label26: TLabel;
    EdNotaVS: TdmkEdit;
    EdSiglaVS: TdmkEdit;
    Label27: TLabel;
    QrVSEntiMPCodigo: TIntegerField;
    QrVSEntiMPCMP_LPQV: TFloatField;
    QrVSEntiMPCPL_LPQV: TFloatField;
    QrVSEntiMPAGI_LPQV: TFloatField;
    QrVSEntiMPCMI_LPQV: TFloatField;
    QrVSEntiMPCMP_IDQV: TSmallintField;
    QrVSEntiMPCPL_IDQV: TSmallintField;
    QrVSEntiMPAGI_IDQV: TSmallintField;
    QrVSEntiMPCMI_IDQV: TSmallintField;
    QrVSEntiMPCPLSit: TSmallintField;
    QrVSEntiMPMaskLetras: TWideStringField;
    QrVSEntiMPMaskFormat: TWideStringField;
    QrVSEntiMPPreDescarn: TSmallintField;
    QrVSEntiMPCMPUnida: TSmallintField;
    QrVSEntiMPCMPMoeda: TSmallintField;
    QrVSEntiMPCMPPerio: TSmallintField;
    QrVSEntiMPCMPPreco: TFloatField;
    QrVSEntiMPCMPFrete: TFloatField;
    QrVSEntiMPCPLUnida: TSmallintField;
    QrVSEntiMPCPLMoeda: TSmallintField;
    QrVSEntiMPCPLPerio: TSmallintField;
    QrVSEntiMPCPLPreco: TFloatField;
    QrVSEntiMPCPLFrete: TFloatField;
    QrVSEntiMPAGIUnida: TSmallintField;
    QrVSEntiMPAGIMoeda: TSmallintField;
    QrVSEntiMPAGIPerio: TSmallintField;
    QrVSEntiMPAGIPreco: TFloatField;
    QrVSEntiMPAGIFrete: TFloatField;
    QrVSEntiMPCMIUnida: TSmallintField;
    QrVSEntiMPCMIMoeda: TSmallintField;
    QrVSEntiMPCMIPerio: TSmallintField;
    QrVSEntiMPCMIPreco: TFloatField;
    QrVSEntiMPCMIFrete: TFloatField;
    QrVSEntiMPContatos: TWideStringField;
    QrVSEntiMPLocalEntrg: TWideStringField;
    QrVSEntiMPCorretor: TIntegerField;
    QrVSEntiMPComissao: TFloatField;
    QrVSEntiMPCondPagto: TWideStringField;
    QrVSEntiMPCondComis: TWideStringField;
    QrVSEntiMPDescAdiant: TFloatField;
    QrVSEntiMPAbate: TIntegerField;
    QrVSEntiMPTransporte: TIntegerField;
    QrVSEntiMPSdoIniPele: TFloatField;
    QrVSEntiMPLoteLetras: TWideStringField;
    QrVSEntiMPLoteFormat: TWideStringField;
    QrVSEntiMPLk: TIntegerField;
    QrVSEntiMPDataCad: TDateField;
    QrVSEntiMPDataAlt: TDateField;
    QrVSEntiMPUserCad: TIntegerField;
    QrVSEntiMPUserAlt: TIntegerField;
    QrVSEntiMPAlterWeb: TSmallintField;
    QrVSEntiMPAtivo: TSmallintField;
    QrVSEntiMPNotaVS: TFloatField;
    QrVSEntiMPSiglaVS: TWideStringField;
    QrVSEntiMPEntiMP: TIntegerField;
    QrVSEntiMPCMPUNIDANOME: TWideStringField;
    QrVSEntiMPCPLUNIDANOME: TWideStringField;
    QrVSEntiMPAGIUNIDANOME: TWideStringField;
    QrVSEntiMPCMIUNIDANOME: TWideStringField;
    QrVSEntiMPCMPMOEDANOME: TWideStringField;
    QrVSEntiMPCPLMOEDANOME: TWideStringField;
    QrVSEntiMPAGIMOEDANOME: TWideStringField;
    QrVSEntiMPCMIMOEDANOME: TWideStringField;
    QrVSEntiMPCMPPERIONOME: TWideStringField;
    QrVSEntiMPCPLPERIONOME: TWideStringField;
    QrVSEntiMPAGIPERIONOME: TWideStringField;
    QrVSEntiMPCMIPERIONOME: TWideStringField;
    QrVSEntiMPPreDescarnNOME: TWideStringField;
    QrVSEntiMPCPLSit_NOME: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure QrVSEntiMPCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
  public
    { Public declarations }
    procedure ReopenVSEntiMP(Codigo: Integer);
  end;

  var
  FmVSEntiMP: TFmVSEntiMP;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnGOTOy, UMySQLModule, Principal;

{$R *.DFM}

procedure TFmVSEntiMP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSEntiMP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenVSEntiMP(QrVSEntiMPCodigo.Value);
end;

procedure TFmVSEntiMP.FormResize(Sender: TObject);
begin
   MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSEntiMP.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmVSEntiMP.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      dmkDBGrid1.Enabled := True;
      PnControla.Visible   := True;
      PnConfirma.Visible   := False;
      PnInsUpd.Visible     := False;
    end;
    1:
    begin
      PnConfirma.Visible := True;
      PnInsUpd.Visible   := True;
      PnControla.Visible := False;
      if SQLType = stIns then
      begin
        EdEntiMP.Text              := '';
        CBEntiMP.KeyValue          := NULL;
        //
        RgCMPUnida.ItemIndex       := 0;
        RgCMPMoeda.ItemIndex       := 0;
        RgCMPPerio.ItemIndex       := 0;
        dmkEdCMPPreco.ValueVariant := 0;
        dmkEdCMPFrete.ValueVariant := 0;
        //
        RgCPLUnida.ItemIndex       := 0;
        RgCPLMoeda.ItemIndex       := 0;
        RgCPLPerio.ItemIndex       := 0;
        dmkEdCPLPreco.ValueVariant := 0;
        dmkEdCPLFrete.ValueVariant := 0;
        //
        RgAGIUnida.ItemIndex       := 0;
        RgAGIMoeda.ItemIndex       := 0;
        RgAGIPerio.ItemIndex       := 0;
        dmkEdAGIPreco.ValueVariant := 0;
        dmkEdAGIFrete.ValueVariant := 0;
        //
        RgCMIUnida.ItemIndex       := 0;
        RgCMIMoeda.ItemIndex       := 0;
        RgCMIPerio.ItemIndex       := 0;
        dmkEdCMIPreco.ValueVariant := 0;
        dmkEdCMIFrete.ValueVariant := 0;
        //
        dmkEdCMP_LPQV.ValueVariant := 0;
        dmkEdCPL_LPQV.ValueVariant := 0;
        dmkEdAGI_LPQV.ValueVariant := 0;
        dmkEdCMI_LPQV.ValueVariant := 0;
        //
        RgCMP_IDQV.ItemIndex       := 1;
        RgCPL_IDQV.ItemIndex       := 0;
        RgAGI_IDQV.ItemIndex       := 0;
        RgCMI_IDQV.ItemIndex       := 0;
        //
        RgCPLSit.ItemIndex         := 0;
        RgPreDescarn.ItemIndex     := 0;
        //
        EdLoteLetras.Text            := 'ABC';
        EdLoteFormat.Text            := 'sa';
        //
        EdMaskLetras.Text            := 'ABC';
        EdMaskFormat.Text            := 'yymmdd';
        //
        EdLocalEntrg.Text            := '';
        EdCondPagto.Text             := '';
        EdContatos.Text              := '';
        dmkEdDescAdiant.ValueVariant := 0;
        EdCorretor.Text              := '';
        CBCorretor.KeyValue          := NULL;
        dmkEdComissao.ValueVariant   := 0;
        EdCondComis.Text             := '';
        RGAbate.ItemIndex            := 1;
        EdTransporte.Text            := '';
        CBTransporte.KeyValue        := NULL;
        //
        dmkEdSdoIniPele.ValueVariant := 0;
        //
        EdNotaVS.ValueVariant        := 0;
        EdSiglaVS.Text               := '';
        //
      end else begin
        EdEntiMP.Text           := IntToStr(QrVSEntiMPCodigo.Value);
        CBEntiMP.KeyValue       := QrVSEntiMPCodigo.Value;
        //
        RgCMPUnida.ItemIndex       := QrVSEntiMPCMPUnida.Value;
        RgCMPMoeda.ItemIndex       := QrVSEntiMPCMPMoeda.Value;
        RgCMPPerio.ItemIndex       := QrVSEntiMPCMPPerio.Value;
        dmkEdCMPPreco.ValueVariant := QrVSEntiMPCMPPreco.Value;
        dmkEdCMPFrete.ValueVariant := QrVSEntiMPCMPFrete.Value;
        //
        RgCPLUnida.ItemIndex       := QrVSEntiMPCPLUnida.Value;
        RgCPLMoeda.ItemIndex       := QrVSEntiMPCPLMoeda.Value;
        RgCPLPerio.ItemIndex       := QrVSEntiMPCPLPerio.Value;
        dmkEdCPLPreco.ValueVariant := QrVSEntiMPCPLPreco.Value;
        dmkEdCPLFrete.ValueVariant := QrVSEntiMPCPLFrete.Value;
        //
        RgAGIUnida.ItemIndex       := QrVSEntiMPAGIUnida.Value;
        RgAGIMoeda.ItemIndex       := QrVSEntiMPAGIMoeda.Value;
        RgAGIPerio.ItemIndex       := QrVSEntiMPAGIPerio.Value;
        dmkEdAGIPreco.ValueVariant := QrVSEntiMPAGIPreco.Value;
        dmkEdAGIFrete.ValueVariant := QrVSEntiMPAGIFrete.Value;
        //
        RgCMIUnida.ItemIndex       := QrVSEntiMPCMIUnida.Value;
        RgCMIMoeda.ItemIndex       := QrVSEntiMPCMIMoeda.Value;
        RgCMIPerio.ItemIndex       := QrVSEntiMPCMIPerio.Value;
        dmkEdCMIPreco.ValueVariant := QrVSEntiMPCMIPreco.Value;
        dmkEdCMIFrete.ValueVariant := QrVSEntiMPCMIFrete.Value;
        //
        dmkEdCMP_LPQV.ValueVariant := QrVSEntiMPCMP_LPQV.Value;
        dmkEdCPL_LPQV.ValueVariant := QrVSEntiMPCPL_LPQV.Value;
        dmkEdAGI_LPQV.ValueVariant := QrVSEntiMPAGI_LPQV.Value;
        dmkEdCMI_LPQV.ValueVariant := QrVSEntiMPCMI_LPQV.Value;
        //
        RgCMP_IDQV.ItemIndex       := QrVSEntiMPCMP_IDQV.Value;
        RgCPL_IDQV.ItemIndex       := QrVSEntiMPCPL_IDQV.Value;
        RgAGI_IDQV.ItemIndex       := QrVSEntiMPAGI_IDQV.Value;
        RgCMI_IDQV.ItemIndex       := QrVSEntiMPCMI_IDQV.Value;
        //
        RgCPLSit.ItemIndex         := QrVSEntiMPCPLSit.Value;
        RgPreDescarn.ItemIndex     := QrVSEntiMPPreDescarn.Value;
        //
        EdLoteLetras.Text            := QrVSEntiMPLoteLetras.Value;
        EdLoteFormat.Text            := QrVSEntiMPLoteFormat.Value;
        //
        EdMaskLetras.Text            := QrVSEntiMPMaskLetras.Value;
        EdMaskFormat.Text            := QrVSEntiMPMaskFormat.Value;
        //
        EdLocalEntrg.Text            := QrVSEntiMPLocalEntrg.Value;
        EdCondPagto.Text             := QrVSEntiMPCondPagto.Value;
        EdContatos.Text              := QrVSEntiMPContatos.Value;
        dmkEdDescAdiant.ValueVariant := QrVSEntiMPDescAdiant.Value;
        EdCorretor.Text              := IntToStr(QrVSEntiMPCorretor.Value);
        CBCorretor.KeyValue          := QrVSEntiMPCorretor.Value;
        dmkEdComissao.ValueVariant   := QrVSEntiMPComissao.Value;
        EdCondComis.Text             := QrVSEntiMPCondComis.Value;
        //
        RGAbate.ItemIndex            := QrVSEntiMPAbate.Value;
        EdTransporte.Text            := IntToStr(QrVSEntiMPTransporte.Value);
        CBTransporte.KeyValue        := QrVSEntiMPTransporte.Value;
        //
        dmkEdSdoIniPele.ValueVariant := QrVSEntiMPSdoIniPele.Value;
        //
        EdNotaVS.ValueVariant        := QrVSEntiMPNotaVS.Value;
        EdSiglaVS.Text               := QrVSEntiMPSiglaVS.Value;
        //
        dmkDBGrid1.Enabled := False;
      end;
      EdEntiMP.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo > 0 then
    ReopenVSEntiMP(Codigo)
end;

procedure TFmVSEntiMP.ReopenVSEntiMP(Codigo: Integer);
begin
  QrVSEntiMP.Close;
  UnDmkDAC_PF.AbreQuery(QrVSEntiMP, Dmod.MyDB);
  //
  QrVSEntiMP.Locate('Codigo', Codigo, []);
end;

procedure TFmVSEntiMP.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmVSEntiMP.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, QrVSEntiMPCodigo.Value);
end;

procedure TFmVSEntiMP.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o da entidade selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM vsentimp ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo='+IntToStr(QrVSEntiMPCodigo.Value));
    Dmod.QrUpd.ExecSQL;
    ReopenVSEntiMP(0);
  end;
end;

procedure TFmVSEntiMP.BtConfirmaClick(Sender: TObject);
var
  SQLType: TSQLType;
  EntiMP: Integer;
  NotaVS: Double;
  SiglaVS: String;
begin
  EntiMP := Geral.IMV(EdEntiMP.Text);
  //
  if EntiMP = 0 then
  begin
    Application.MessageBox('� necess�rio definir a entidade!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  NotaVS  := EdNotaVS.ValueVariant;
  SiglaVS := EdSiglaVS.Text;
  if MyObjects.FIC(Trim(SiglaVS) = '', EdSiglaVS,
  'Informe a sigla do formecedor!') then
    Exit;
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Codigo FROM vsentimp ');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := EntiMP;
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsentimp', False, [
    'CMP_LPQV', 'CPL_LPQV',
    'AGI_LPQV', 'CMI_LPQV',
    'CMP_IDQV', 'CPL_IDQV',
    'AGI_IDQV', 'CMI_IDQV',
    'CPLSit',
    'PreDescarn', 'MaskLetras', 'MaskFormat',
    'CMPUnida', 'CMPMoeda', 'CMPPerio', 'CMPPreco', 'CMPFrete',
    'CPLUnida', 'CPLMoeda', 'CPLPerio', 'CPLPreco', 'CPLFrete',
    'AGIUnida', 'AGIMoeda', 'AGIPerio', 'AGIPreco', 'AGIFrete',
    'CMIUnida', 'CMIMoeda', 'CMIPerio', 'CMIPreco', 'CMIFrete',
    'LocalEntrg', 'CondPagto', 'Contatos',
    'DescAdiant', 'Corretor',
    'Comissao', 'CondComis',
    'Transporte', 'Abate', 'SdoIniPele',
    'LoteLetras', 'LoteFormat',
    'NotaVS', 'SiglaVS'
  ], ['Codigo'], [
    dmkEdCMP_LPQV.ValueVariant, dmkEdCPL_LPQV.ValueVariant,
    dmkEdAGI_LPQV.ValueVariant, dmkEdCMI_LPQV.ValueVariant,
    RgCMP_IDQV.ItemIndex, RgCPL_IDQV.ItemIndex,
    RgAGI_IDQV.ItemIndex, RgCMI_IDQV.ItemIndex,
    RgCPLSit.ItemIndex,
    RgPreDescarn.ItemIndex, EdMaskLetras.Text, EdMaskFormat.Text,
    RgCMPUnida.ItemIndex, RgCMPMoeda.ItemIndex, RgCMPPerio.ItemIndex,
      dmkEdCMPPreco.ValueVariant, dmkEdCMPFrete.ValueVariant,
    RgCPLUnida.ItemIndex, RgCPLMoeda.ItemIndex, RgCPLPerio.ItemIndex,
      dmkEdCPLPreco.ValueVariant, dmkEdCPLFrete.ValueVariant,
    RgAGIUnida.ItemIndex, RgAGIMoeda.ItemIndex, RgAGIPerio.ItemIndex,
      dmkEdAGIPreco.ValueVariant, dmkEdAGIFrete.ValueVariant,
    RgCMIUnida.ItemIndex, RgCMIMoeda.ItemIndex, RgCMIPerio.ItemIndex,
      dmkEdCMIPreco.ValueVariant, dmkEdCMIFrete.ValueVariant,
    EdLocalEntrg.Text, EdCondPagto.Text, EdContatos.Text,
    dmkEdDescAdiant.ValueVariant, Geral.IMV(EdCorretor.Text),
    dmkEdComissao.ValueVariant, EdCondComis.Text,
    Geral.IMV(EdTransporte.Text), RGAbate.ItemIndex,
    dmkEdSdoIniPele.ValueVariant,
    EdLoteLetras.Text, EdLoteFormat.Text,
    NotaVS, SiglaVS
  ], [EntiMP], True) then MostraEdicao(0, stLok, EntiMP);
end;

procedure TFmVSEntiMP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VAR_ENTIDADE := QrVSEntiMPCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSEntiMP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCorretores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporte, Dmod.MyDB);
  PnControla.Align := alClient;
end;

procedure TFmVSEntiMP.QrVSEntiMPCalcFields(DataSet: TDataSet);
begin
  case QrVSEntiMPPreDescarn.Value of
    0: QrVSEntiMPPreDescarnNOME.Value := 'No curtume';
    1: QrVSEntiMPPreDescarnNOME.Value := 'N�o pr�-descarna';
    2: QrVSEntiMPPreDescarnNOME.Value := 'J� vem pr�-descarnado';
    else QrVSEntiMPPreDescarnNOME.Value := '? ? ? ?';
  end;
  {
  case QrVSEntiMPIDQV.Value of
    0: QrVSEntiMPIDQV_NOME.Value := 'Nada';
    1: QrVSEntiMPIDQV_NOME.Value := 'Tudo';
    2: QrVSEntiMPIDQV_NOME.Value := 'Excedente';
    else QrVSEntiMPIDQV_NOME.Value := '? ? ? ?';
  end;
  }
  case QrVSEntiMPCPLSit.Value of
    0: QrVSEntiMPCPLSit_NOME.Value := 'Nota Fiscal';
    1: QrVSEntiMPCPLSit_NOME.Value := 'Entrada';
    else QrVSEntiMPCPLSit_NOME.Value := '? ? ? ?';
  end;
  QrVSEntiMPCMPUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrVSEntiMPCMPUnida.Value);
  QrVSEntiMPCPLUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrVSEntiMPCPLUnida.Value);
  QrVSEntiMPAGIUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrVSEntiMPAGIUnida.Value);
  QrVSEntiMPCMIUNIDANOME.Value := FmPrincipal.ObtemUnidadeTxt(QrVSEntiMPCMIUnida.Value);
  //
  QrVSEntiMPCMPMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrVSEntiMPCMPMoeda.Value);
  QrVSEntiMPCPLMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrVSEntiMPCPLMoeda.Value);
  QrVSEntiMPAGIMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrVSEntiMPAGIMoeda.Value);
  QrVSEntiMPCMIMOEDANOME.Value := FmPrincipal.ObtemMoedaTxt(QrVSEntiMPCMIMoeda.Value);
  //
  QrVSEntiMPCMPPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrVSEntiMPCMPPerio.Value);
  QrVSEntiMPCPLPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrVSEntiMPCPLPerio.Value);
  QrVSEntiMPAGIPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrVSEntiMPAGIPerio.Value);
  QrVSEntiMPCMIPERIONOME.Value := FmPrincipal.ObtemPeriodoTxt(QrVSEntiMPCMIPerio.Value);
  //
end;

end.

