object FmVSOutPeso: TFmVSOutPeso
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-112 :: Item de Venda por Peso'
  ClientHeight = 675
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 536
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 291
        Height = 32
        Caption = 'Item de Venda por Peso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 291
        Height = 32
        Caption = 'Item de Venda por Peso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 291
        Height = 32
        Caption = 'Item de Venda por Peso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 561
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 551
        Height = 16
        Caption = 
          'Para baixar v'#225'rios itens em sequ'#234'ncia sem selecionar os itens, a' +
          'penas informe o total a baixar!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 551
        Height = 16
        Caption = 
          'Para baixar v'#225'rios itens em sequ'#234'ncia sem selecionar os itens, a' +
          'penas informe o total a baixar!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 605
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DBGVMI: TdmkDBGridZTO
    Left = 0
    Top = 277
    Width = 1008
    Height = 112
    Align = alTop
    DataSource = DsVSMovIts
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    OnAfterMultiselect = DBGVMIAfterMultiselect
    OnDblClick = DBGVMIDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'IME-I'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_SerieFch'
        Title.Caption = 'S'#233'rie'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ficha'
        Title.Caption = 'Ficha RMP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORNECE'
        Title.Caption = 'Fornecedor'
        Width = 172
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtPeso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'Saldo peso Kg'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtPeca'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'Sdo P'#231
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MovimCod'
        Title.Caption = 'ID Estoque'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PesoKg'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGrayText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'Peso Kg'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pecas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGrayText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'Pe'#231'as'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorT'
        Title.Caption = '$ toda Ficha'
        Visible = True
      end>
  end
  object Panel7: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 165
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Caption = ' Dados do cabe'#231'alho:'
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 20
        Width = 53
        Height = 13
        Caption = 'ID entrada:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 96
        Top = 20
        Width = 55
        Height = 13
        Caption = 'ID estoque:'
        FocusControl = DBEdMovimCod
      end
      object Label3: TLabel
        Left = 180
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object Label7: TLabel
        Left = 228
        Top = 20
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        FocusControl = DBEdDtEntrada
      end
      object Label8: TLabel
        Left = 344
        Top = 20
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdCliVenda
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdMovimCod: TdmkDBEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 180
        Top = 36
        Width = 45
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdDtEntrada: TdmkDBEdit
        Left = 228
        Top = 36
        Width = 112
        Height = 21
        TabStop = False
        DataField = 'DtVenda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdCliVenda: TdmkDBEdit
        Left = 344
        Top = 36
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
    object RGTipoCouro: TRadioGroup
      Left = 0
      Top = 65
      Width = 1008
      Height = 100
      Align = alClient
      Caption = ' Tipo de Couro: '
      Columns = 2
      Items.Strings = (
        '...sListaGraGruY_VS')
      TabOrder = 1
      OnClick = RGTipoCouroClick
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 213
    Width = 1008
    Height = 64
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 617
      Height = 64
      Align = alLeft
      Caption = ' Dados do item: '
      TabOrder = 0
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label1: TLabel
        Left = 96
        Top = 16
        Width = 155
        Height = 13
        Caption = 'Mat'#233'ria-prima (F4 - '#250'ltima usada):'
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 152
        Top = 32
        Width = 457
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 2
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraGruX: TdmkEditCB
        Left = 96
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        OnKeyDown = EdGraGruXKeyDown
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GroupBox3: TGroupBox
      Left = 617
      Top = 0
      Width = 391
      Height = 64
      Align = alClient
      Caption = ' Filtros: '
      TabOrder = 1
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 495
    Width = 1008
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Label20: TLabel
      Left = 423
      Top = 16
      Width = 29
      Height = 13
      Caption = 'Folha:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label21: TLabel
      Left = 515
      Top = 16
      Width = 29
      Height = 13
      Caption = 'Linha:'
      Color = clBtnFace
      ParentColor = False
    end
    object RGIxxMovIX: TRadioGroup
      Left = 0
      Top = 0
      Width = 416
      Height = 41
      Align = alLeft
      Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Entrada de couro'
        'Classe / reclasse'
        'Sa'#237'da de couro')
      TabOrder = 0
    end
    object EdIxxFolha: TdmkEdit
      Left = 456
      Top = 12
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdIxxLinha: TdmkEdit
      Left = 548
      Top = 12
      Width = 33
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnQtd: TPanel
    Left = 0
    Top = 410
    Width = 1008
    Height = 85
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 8
    object Label19: TLabel
      Left = 744
      Top = 4
      Width = 149
      Height = 13
      Caption = 'Material usado para emitir NF-e:'
    end
    object Label48: TLabel
      Left = 8
      Top = 3
      Width = 114
      Height = 13
      Caption = 'Item de pedido atrelado:'
    end
    object Label12: TLabel
      Left = 676
      Top = 4
      Width = 61
      Height = 13
      Caption = 'Item da NFe:'
    end
    object LaPecas: TLabel
      Left = 292
      Top = 4
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
      Enabled = False
    end
    object LaAreaM2: TLabel
      Left = 368
      Top = 4
      Width = 39
      Height = 13
      Caption = #193'rea m'#178':'
      Enabled = False
    end
    object LaAreaP2: TLabel
      Left = 444
      Top = 4
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
      Enabled = False
    end
    object LaPeso: TLabel
      Left = 528
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object Label18: TLabel
      Left = 601
      Top = 4
      Width = 53
      Height = 13
      Caption = 'Custo total:'
      Enabled = False
    end
    object Label22: TLabel
      Left = 8
      Top = 45
      Width = 36
      Height = 13
      Caption = 'Moeda:'
    end
    object Label4: TLabel
      Left = 156
      Top = 44
      Width = 31
      Height = 13
      Caption = 'Pre'#231'o:'
    end
    object Label9: TLabel
      Left = 240
      Top = 44
      Width = 77
      Height = 13
      Caption = '$ Total (moeda):'
      Enabled = False
    end
    object Label15: TLabel
      Left = 324
      Top = 45
      Width = 60
      Height = 13
      Caption = '% Desconto:'
    end
    object Label16: TLabel
      Left = 396
      Top = 45
      Width = 52
      Height = 13
      Caption = '% Tributos:'
    end
    object Label17: TLabel
      Left = 468
      Top = 44
      Width = 78
      Height = 13
      Caption = '$ Bruto (moeda):'
      Enabled = False
    end
    object Label10: TLabel
      Left = 552
      Top = 44
      Width = 72
      Height = 13
      Caption = '$ L'#237'q. (moeda):'
      Enabled = False
    end
    object Label13: TLabel
      Left = 636
      Top = 44
      Width = 43
      Height = 13
      Caption = 'Cota'#231#227'o:'
    end
    object Label14: TLabel
      Left = 752
      Top = 44
      Width = 50
      Height = 13
      Caption = 'Valor total:'
      Enabled = False
    end
    object Label23: TLabel
      Left = 836
      Top = 44
      Width = 54
      Height = 13
      Caption = 'Valor bruto:'
      Enabled = False
    end
    object Label24: TLabel
      Left = 920
      Top = 44
      Width = 62
      Height = 13
      Caption = 'Valor l'#237'quido:'
      Enabled = False
    end
    object EdGGXRcl: TdmkEditCB
      Left = 744
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GGXRcl'
      UpdCampo = 'GGXRcl'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXRcl
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXRcl: TdmkDBLookupComboBox
      Left = 800
      Top = 20
      Width = 201
      Height = 21
      DropDownWidth = 400
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXRcl
      TabOrder = 9
      dmkEditCB = EdGGXRcl
      QryCampo = 'GGXRcl'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPedItsVda: TdmkEditCB
      Left = 8
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdPedItsVdaRedefinido
      DBLookupComboBox = CBPedItsVda
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPedItsVda: TdmkDBLookupComboBox
      Left = 64
      Top = 19
      Width = 225
      Height = 21
      DropDownWidth = 400
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsVSPedIts
      TabOrder = 1
      dmkEditCB = EdPedItsVda
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdItemNFe: TdmkEdit
      Left = 676
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdItemNFeRedefinido
    end
    object EdPecas: TdmkEdit
      Left = 292
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdPecasRedefinido
    end
    object EdAreaM2: TdmkEditCalc
      Left = 368
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdAreaM2Redefinido
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 444
      Top = 20
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdPesoKg: TdmkEdit
      Left = 528
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPesoKgRedefinido
    end
    object EdValorT: TdmkEdit
      Left = 603
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdValorTKeyDown
    end
    object EdPrecoMoeda: TdmkEditCB
      Left = 8
      Top = 60
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdPrecoMoedaKeyDown
      OnRedefinido = EdPrecoMoedaRedefinido
      DBLookupComboBox = CBPrecoMoeda
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPrecoMoeda: TdmkDBLookupComboBox
      Left = 30
      Top = 60
      Width = 123
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsCambioMda
      TabOrder = 11
      OnKeyDown = CBPrecoMoedaKeyDown
      dmkEditCB = EdPrecoMoeda
      UpdType = utNil
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPrecoVal: TdmkEdit
      Left = 156
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPrecoValKeyDown
      OnRedefinido = EdPrecoValRedefinido
    end
    object EdMdaTotal: TdmkEdit
      Left = 240
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPercDesco: TdmkEdit
      Left = 324
      Top = 60
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'PercDesco'
      UpdCampo = 'PercDesco'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPercDescoKeyDown
      OnRedefinido = EdPercDescoRedefinido
    end
    object EdPercTrib: TdmkEdit
      Left = 396
      Top = 60
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'PercTrib'
      UpdCampo = 'PercTrib'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPercTribKeyDown
      OnRedefinido = EdPercTribRedefinido
    end
    object EdMdaBruto: TdmkEdit
      Left = 468
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'MdaBruto'
      UpdCampo = 'MdaBruto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdMdaLiqui: TdmkEdit
      Left = 551
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 17
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'MdaLiqui'
      UpdCampo = 'MdaLiqui'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCambio: TdmkEdit
      Left = 636
      Top = 60
      Width = 113
      Height = 21
      Alignment = taRightJustify
      TabOrder = 18
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 10
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000000000'
      QryCampo = 'Cambio'
      UpdCampo = 'Cambio'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCambioRedefinido
    end
    object EdNacTotal: TdmkEdit
      Left = 752
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 19
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacTotal'
      UpdCampo = 'NacTotal'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdNacBruto: TdmkEdit
      Left = 836
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 20
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacBruto'
      UpdCampo = 'NacBruto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdNacLiqui: TdmkEdit
      Left = 920
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 21
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacLiqui'
      UpdCampo = 'NacLiqui'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 40
    Top = 220
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 40
    Top = 272
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Grandeza'
      'FROM defpecas')
    Left = 104
    Top = 220
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 104
    Top = 272
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSMovItsAfterOpen
    BeforeClose = QrVSMovItsBeforeClose
    SQL.Strings = (
      'SELECT fch.Nome NO_SerieFch, ggx.GraGruY, wmi.*, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE   '
      'FROM vsmovits wmi  '
      'LEFT JOIN gragrux  ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN vsserfch fch ON fch.Codigo=wmi.SerieFch  '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=70'
      'AND wmi.MovimID=23'
      'AND wmi.SdoVrtPeso > 0 '
      'ORDER BY DataHora, Controle ')
    Left = 176
    Top = 220
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TIntegerField
      FieldName = 'Misturou'
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVSMovItsEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVSMovItsNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVSMovItsFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVSMovItsFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVSMovItsLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 176
    Top = 272
  end
  object QrVSMovSum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 220
    object QrVSMovSumSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 352
    Top = 240
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 352
    Top = 292
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 220
    Top = 112
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 220
    Top = 160
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 428
    Top = 204
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 428
    Top = 156
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPedItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPedItsPrecoTipo: TSmallintField
      FieldName = 'PrecoTipo'
      Required = True
    end
    object QrVSPedItsPrecoMoeda: TIntegerField
      FieldName = 'PrecoMoeda'
      Required = True
    end
    object QrVSPedItsPrecoVal: TFloatField
      FieldName = 'PrecoVal'
      Required = True
    end
    object QrVSPedItsPercTrib: TFloatField
      FieldName = 'PercTrib'
    end
    object QrVSPedItsPercDesco: TFloatField
      FieldName = 'PercDesco'
      Required = True
    end
    object QrVSPedItsSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
      Required = True
    end
  end
end
