unit VSImpMOEnvRet2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc, mySQLDbTables, frxClass, frxDBSet, BluedermConsts, dmkEditCB,
  dmkDBLookupComboBox, UnDmkProcFunc, dmkDBGridZTO, dmkEditDateTimePicker,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSImpMOEnvRet2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PCRelatorios: TPageControl;
    TsListaEnv: TTabSheet;
    Panel5: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Qr00NFes: TmySQLQuery;
    Qr00NFesEmpresa: TIntegerField;
    Qr00NFesSerNF: TIntegerField;
    Qr00NFesnNF: TIntegerField;
    Qr00NFesPecas: TFloatField;
    Qr00NFesPesoKg: TFloatField;
    Qr00NFesAreaM2: TFloatField;
    Qr00NFesAreaP2: TFloatField;
    Qr00NFesValorT: TFloatField;
    Ds00NFes: TDataSource;
    Splitter1: TSplitter;
    Qr00NFeIts: TmySQLQuery;
    Qr00NFeItsTipoFrete: TWideStringField;
    Qr00NFeItsTabela: TWideStringField;
    Qr00NFeItsEmpresa: TIntegerField;
    Qr00NFeItsTerceiro: TIntegerField;
    Qr00NFeItsSerNF: TIntegerField;
    Qr00NFeItsnNF: TIntegerField;
    Qr00NFeItsPecas: TFloatField;
    Qr00NFeItsPesoKg: TFloatField;
    Qr00NFeItsAreaM2: TFloatField;
    Qr00NFeItsAreaP2: TFloatField;
    Qr00NFeItsValorT: TFloatField;
    Ds00NFeIts: TDataSource;
    Qr00NFeItsDataHora: TDateTimeField;
    Qr00NFeItsNO_Terceiro: TWideStringField;
    Qr00NFeItsVSVMI_MovimCod: TIntegerField;
    Qr00NFeItsCodigo: TIntegerField;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    Label2: TLabel;
    TsRemessa: TTabSheet;
    Qr01NFes: TmySQLQuery;
    Qr01NFesEmpresa: TIntegerField;
    Qr01NFesSerNF: TIntegerField;
    Qr01NFesnNF: TIntegerField;
    Qr01NFesPecas: TFloatField;
    Qr01NFesPesoKg: TFloatField;
    Qr01NFesAreaM2: TFloatField;
    Qr01NFesAreaP2: TFloatField;
    Qr01NFesValorT: TFloatField;
    Ds01NFes: TDataSource;
    Panel8: TPanel;
    Splitter2: TSplitter;
    dmkDBGridZTO2: TdmkDBGridZTO;
    Panel9: TPanel;
    Label3: TLabel;
    Qr01EnvRet: TmySQLQuery;
    Ds01EnvRet: TDataSource;
    Qr01EnvRetCodigo: TIntegerField;
    Qr01EnvRetNFCMO_FatID: TIntegerField;
    Qr01EnvRetNFCMO_FatNum: TIntegerField;
    Qr01EnvRetNFCMO_Empresa: TIntegerField;
    Qr01EnvRetNFCMO_Terceiro: TIntegerField;
    Qr01EnvRetNFCMO_nItem: TIntegerField;
    Qr01EnvRetNFCMO_SerNF: TIntegerField;
    Qr01EnvRetNFCMO_nNF: TIntegerField;
    Qr01EnvRetNFCMO_Pecas: TFloatField;
    Qr01EnvRetNFCMO_PesoKg: TFloatField;
    Qr01EnvRetNFCMO_AreaM2: TFloatField;
    Qr01EnvRetNFCMO_AreaP2: TFloatField;
    Qr01EnvRetNFCMO_CusMOM2: TFloatField;
    Qr01EnvRetNFCMO_CusMOKG: TFloatField;
    Qr01EnvRetNFCMO_ValorT: TFloatField;
    Qr01EnvRetNFRMP_FatID: TIntegerField;
    Qr01EnvRetNFRMP_FatNum: TIntegerField;
    Qr01EnvRetNFRMP_Empresa: TIntegerField;
    Qr01EnvRetNFRMP_Terceiro: TIntegerField;
    Qr01EnvRetNFRMP_nItem: TIntegerField;
    Qr01EnvRetNFRMP_SerNF: TIntegerField;
    Qr01EnvRetNFRMP_nNF: TIntegerField;
    Qr01EnvRetNFRMP_Pecas: TFloatField;
    Qr01EnvRetNFRMP_PesoKg: TFloatField;
    Qr01EnvRetNFRMP_AreaM2: TFloatField;
    Qr01EnvRetNFRMP_AreaP2: TFloatField;
    Qr01EnvRetNFRMP_ValorT: TFloatField;
    Qr01EnvRetLk: TIntegerField;
    Qr01EnvRetDataCad: TDateField;
    Qr01EnvRetDataAlt: TDateField;
    Qr01EnvRetUserCad: TIntegerField;
    Qr01EnvRetUserAlt: TIntegerField;
    Qr01EnvRetAlterWeb: TSmallintField;
    Qr01EnvRetAWServerID: TIntegerField;
    Qr01EnvRetAWStatSinc: TSmallintField;
    Qr01EnvRetAtivo: TSmallintField;
    Qr01EnvRetNome: TWideStringField;
    Qr01EnvRetDataHora: TDateTimeField;
    Qr01EnvRetCFTPA_FatID: TIntegerField;
    Qr01EnvRetCFTPA_FatNum: TIntegerField;
    Qr01EnvRetCFTPA_Empresa: TIntegerField;
    Qr01EnvRetCFTPA_Terceiro: TIntegerField;
    Qr01EnvRetCFTPA_nItem: TIntegerField;
    Qr01EnvRetCFTPA_SerCT: TIntegerField;
    Qr01EnvRetCFTPA_nCT: TIntegerField;
    Qr01EnvRetCFTPA_Pecas: TFloatField;
    Qr01EnvRetCFTPA_PesoKg: TFloatField;
    Qr01EnvRetCFTPA_AreaM2: TFloatField;
    Qr01EnvRetCFTPA_AreaP2: TFloatField;
    Qr01EnvRetCFTPA_PesTrKg: TFloatField;
    Qr01EnvRetCFTPA_CusTrKg: TFloatField;
    Qr01EnvRetCFTPA_ValorT: TFloatField;
    dmkDBGridZTO3: TdmkDBGridZTO;
    Qr01NFesSdoPeca: TFloatField;
    Qr01NFesSdoPeso: TFloatField;
    Qr01NFesSdoArM2: TFloatField;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    Label1: TLabel;
    Label35: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    Panel55: TPanel;
    GroupBox23: TGroupBox;
    TP24DataIni: TdmkEditDateTimePicker;
    Ck24DataIni: TCheckBox;
    Ck24DataFim: TCheckBox;
    TP24DataFim: TdmkEditDateTimePicker;
    GroupBox24: TGroupBox;
    Panel57: TPanel;
    Label57: TLabel;
    Label58: TLabel;
    SpeedButton1: TSpeedButton;
    Ed24NFeIni: TdmkEdit;
    Ed24NFeFim: TdmkEdit;
    Ck24Serie: TCheckBox;
    Ed24Serie: TdmkEdit;
    GBRemessa: TGroupBox;
    Panel10: TPanel;
    CkSaldo: TCheckBox;
    Qr01NFesTerceiro: TIntegerField;
    Qr01NFesNO_TERCEIRO: TWideStringField;
    Qr01NFesDataHora: TDateTimeField;
    BtImprime: TBitBtn;
    frxDs01NFes: TfrxDBDataset;
    frxDs01EnvRet: TfrxDBDataset;
    frxWET_CURTI_168_01_0: TfrxReport;
    frxDs00NFes: TfrxDBDataset;
    frxDs00NFeIts: TfrxDBDataset;
    frxWET_CURTI_168_01_1: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Qr00NFesAfterScroll(DataSet: TDataSet);
    procedure Qr00NFesBeforeClose(DataSet: TDataSet);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure Qr01NFesAfterScroll(DataSet: TDataSet);
    procedure PCRelatoriosChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxWET_CURTI_168_01_0GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FVSMOEnvNFeLst, FVSMOEnvNFeEnv, FVSMOEnvNFeRet, FVSMO: String;
    //
    procedure ReabrePesquisaNFes((*Query: TmySQLQuery; *)FromEnvAvu, FromEnvEnv:
              Boolean; NomeTabTmp: String);
    procedure Reopen00NFes(Query: TmySQLQuery; NomeTabTmp: String);
    procedure Reopen01NFes(Query: TmySQLQuery; NomeTabTmp: String);
  public
    { Public declarations }
  end;

  var
  FmVSImpMOEnvRet2: TFmVSImpMOEnvRet2;

implementation

uses Module, UnMyObjects, DmkDAC_PF, CreateVS, ModuleGeral, UnVS_PF,
  UMySQLModule, MyDBCheck, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSImpMOEnvRet2.BtImprimeClick(Sender: TObject);
begin
  case PCRelatorios.ActivePageIndex of
    0: // Lista de NF-es Simples e de Remessa
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_168_01_0, [
        DModG.frxDsDono,
        frxDs00NFes,
        frxDs00NFeIts
      ]);
      //
      MyObjects.frxMostra(frxWET_CURTI_168_01_0, 'Cobran�a de MO');
    end;
    1: // Notas de remessa de MP para M.O.
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_168_01_1, [
        DModG.frxDsDono,
        frxDs01NFes,
        frxDs01EnvRet
      ]);
      //
      MyObjects.frxMostra(frxWET_CURTI_168_01_1, 'Cobran�a de MO');
    end;
    else Geral.MB_Erro('Impress�o para ' +
      PCRelatorios.Pages[PCRelatorios.ActivePageIndex].Caption +
      ' n�o implementado!');
  end;
(*
  Grupo1 := frxWET_CURTI_168_01.FindObject('GH001') as TfrxGroupHeader;
  Grupo1.Visible := True;
  Me_GH1  := frxWET_CURTI_168_01.FindObject('MeGrupo1Head') as TfrxMemoView;
  Me_FT1  := frxWET_CURTI_168_01.FindObject('MeGrupo1Foot') as TfrxMemoView;
  MeTitNF := frxWET_CURTI_168_01.FindObject('MeTitNF') as TfrxMemoView;
  MeNumNF := frxWET_CURTI_168_01.FindObject('MeNumNF') as TfrxMemoView;
  case RGRelatorio.ItemIndex of
    1:
    begin
      Grupo1.Condition := 'frxDsVSMO."SerieNF_VMI"';
      Me_GH1.Memo.Text := 'NF Remessa: [frxDsVSMO."SerieNF_VMI"] [VARF_SALDO_ABERTO_NF]';
      Me_FT1.Memo.Text := 'Total da NF [frxDsVSMO."SerieNF_VMI"]: ';
      MeTitNF.Memo.Text := 'NF MO';
      MeNumNF.Memo.Text := '[frxDsVSMO."NFCMO_nNF"]';
    end;
    2:
    begin
      Grupo1.Condition := 'frxDsVSMO."SerieNF_CMO"';
      Me_GH1.Memo.Text := 'NF M�o de obra: [frxDsVSMO."SerieNF_CMO"]';
      Me_FT1.Memo.Text := 'Total da NF [frxDsVSMO."SerieNF_CMO"]: ';
      MeTitNF.Memo.Text := 'NF Rem';
      MeNumNF.Memo.Text := '[frxDsVSMO."VSVMI_nNF"]';
    end;
    else Grupo1.Condition := '***ERRO***';
  end;
  //
*)
end;

procedure TFmVSImpMOEnvRet2.BtPesquisaClick(Sender: TObject);
var
  FromEnvAvu, FromEnvEnv: Boolean;
  NomeTabTmp: String;
begin
  case PCRelatorios.ActivePageIndex of
    0: // Lista de NF-es Simples e de Remessa
    begin
      FromEnvAvu := True;
      FromEnvEnv := True;
      NomeTabTmp := '_vs_mo_env_nfe_ger_lst';
      ReabrePesquisaNFes(FromEnvAvu, FromEnvEnv, NomeTabTmp);
      Reopen00NFes(Qr00NFes, NomeTabTmp);
    end;
    1: // Notas de remessa de MP para M.O.
    begin
      FromEnvAvu := False;
      FromEnvEnv := True;
      NomeTabTmp := '_vs_mo_env_nfe_ger_env';
      ReabrePesquisaNFes(FromEnvAvu, FromEnvEnv, NomeTabTmp);
      Reopen01NFes(Qr01NFes, NomeTabTmp);
    end;
    else Geral.MB_Erro('Pesquisa para ' +
      PCRelatorios.Pages[PCRelatorios.ActivePageIndex].Caption +
      ' n�o implementado!');
  end;
  //
end;

procedure TFmVSImpMOEnvRet2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpMOEnvRet2.DBGrid2DblClick(Sender: TObject);
const
  sProcName = 'TFmVSImpMOEnvRet2.DBGrid2DblClick()';
begin
  if (Qr00NFeIts.State <> dsInactive) and (Qr00NFeIts.RecordCount > 0) then
  begin
    if Qr00NFeItsVSVMI_MovimCod.Value <> 0 then
      VS_CRC_PF.MostraFormVS_Do_IMEC(Qr00NFeItsVSVMI_MovimCod.Value, '')
    else
    begin
      if Qr00NFeItsTabela.Value = CO_TAB_VSMOEnvAvu then
        VS_PF.MostraFormVSMOEnvAvuGer(Qr00NFeItsCodigo.Value)
      else
      if Qr00NFeItsTabela.Value = CO_TAB_VSMOEnvEnv then
        VS_PF.MostraFormVSMOEnvEnvGer(Qr00NFeItsCodigo.Value)
      else
      if Qr00NFeItsTabela.Value = CO_TAB_VSMOEnvAvu then
        VS_PF.MostraFormVSMOEnvRetGer(Qr00NFeItsCodigo.Value)
      else
        Geral.MB_Erro('Tabela "' + Qr00NFeItsTabela.Value +
        '" n�o implementada em ' + sProcName);
    end;
  end;
end;

procedure TFmVSImpMOEnvRet2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  GBRemessa.Enabled := PCRelatorios.ActivePage = TsRemessa;
end;

procedure TFmVSImpMOEnvRet2.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  //
  PCRelatorios.ActivePage := TsListaEnv;
  Data := DMOdG.ObtemAgora();
  TP24DataIni.Date := Data - 90;
  TP24DataFim.Date := Data - 60;
end;

procedure TFmVSImpMOEnvRet2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpMOEnvRet2.frxWET_CURTI_168_01_0GetValue(const VarName: string;
  var Value: Variant);
var
  SQL_Serie: String;
  SerieRem: Integer;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt('', CBEmpresa.Text, EdEmpresa.ValueVariant)
  else
  if VarName = 'VARF_FORNEC_MO' then
    Value := dmkPF.ParValueCodTxt('', CBFornecMO.Text, EdFornecMO.ValueVariant)
  else
(*
  if VarName = 'VARF_SALDO_ABERTO_NF' then
  begin
    SQL_Serie := '';
    SerieRem := Ed24Serie.ValueVariant;
    //if SerieRem <> -1 then
    if Ck24Serie.Checked then
      SQL_Serie := 'AND SerieRem=' + Geral.FF0(SerieRem);
    //
    UnDmkDAC_PF.AbremySQLQuery0(QrSaldo, Dmod.MyDB, [
    'SELECT SUM(PecasSdo) PecasSdo,  ',
    'SUM(AreaSdoM2) AreaSdoM2, SUM(PesoKgSdo) PesoKgSdo  ',
    'FROM vspwecab  ',
    'WHERE PecasSdo<>0 ',
    'AND NFeRem=' + Geral.FF0(QrVSMOVSVMI_nNF.Value),
    SQL_Serie,
    '']);
    if QrSaldoPecasSdo.Value <> 0 then
    begin
      Value := ' (Saldo: ' + FloatToStr(QrSaldoPecasSdo.Value) + ' Pe�as ';
      if QrSaldoPesoKgSdo.Value <> 0 then
        Value := Value + FloatToStr(QrSaldoPesoKgSdo.Value) + ' kg) '
      else
        Value := Value + Geral.FFT(QrSaldoAreaSdoM2.Value, 2, siNegativo) + ' m�) ';
    end else
      Value := ' - Encerrado';
  end;
*)
end;

procedure TFmVSImpMOEnvRet2.PCRelatoriosChange(Sender: TObject);
begin
  GBRemessa.Enabled := PCRelatorios.ActivePage = TsRemessa;
end;

procedure TFmVSImpMOEnvRet2.Qr00NFesAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr00NFeIts, DModG.MyPID_DB, [
  'SELECT ecg.*, ',
  'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Terceiro ',
  'FROM ' + FVSMOEnvNFeLst + ' ecg',
  'LEFT JOIN ' + TMeuDB + '.entidades tra ON tra.Codigo=ecg.Terceiro ',
  'WHERE ecg.Empresa=' + Geral.FF0(Qr00NFesEmpresa.Value),
  'AND ecg.SerNF=' + Geral.FF0(Qr00NFesSerNF.Value),
  'AND ecg.nNF=' + Geral.FF0(Qr00NFesnNF.Value),
  '']);
end;

procedure TFmVSImpMOEnvRet2.Qr00NFesBeforeClose(DataSet: TDataSet);
begin
  Qr00NFeIts.Close;
end;

procedure TFmVSImpMOEnvRet2.Qr01NFesAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr01EnvRet, Dmod.MyDB, [
  'SELECT ret.* ',
  'FROM vsmoenvret ret ',
  'LEFT JOIN vsmoenvrvmi rei ON rei.VSMOEnvRet=ret.Codigo ',
  'LEFT JOIN vsmoenvenv env ON rei.VSMOEnvEnv=env.Codigo ',
  'WHERE env.NFEMP_Empresa=' + Geral.FF0(Qr01NFesEmpresa.Value),
  'AND env.NFEMP_SerNF=' + Geral.FF0(Qr01NFesSerNF.Value),
  'AND env.NFEMP_nNF=' + Geral.FF0(Qr01NFesnNF.Value),
  '']);
end;

procedure TFmVSImpMOEnvRet2.ReabrePesquisaNFes((*Query: TmySQLQuery; *)FromEnvAvu,
FromEnvEnv: Boolean; NomeTabTmp: String);
var
  Empresa: Integer;
  SQL_EnvAvu, SQL_EnvEnv, SQL_UNION, SQL_WHERE, SQL_Periodo, SQL_Limit, xOrdem: String;
begin
  FVSMOEnvNFeLst := UnCreateVS.RecriaTempTableNovo(ntrttVSMOEnvNFeGer,
    DModG.QrUpdPID1, False, 1, NomeTabTmp);
  //
  Empresa      := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
    Empresa := DModG.QrEmpresasCodigo.Value;
  //
  SQL_EnvAvu   := '';
  SQL_EnvEnv   := '';
  SQL_UNION    := '';
  SQL_WHERE    := '';
  SQL_Limit    := '';
  xOrdem       := '';
  SQL_Periodo  := dmkPF.SQL_Periodo('AND DataHora',
    TP24DataIni.Date, TP24DataFim.Date, Ck24DataIni.Checked, Ck24DataFim.Checked);
  //
  if FromEnvAvu then
  begin
    SQL_EnvAvu := Geral.ATS([
    'SELECT DataHora, Codigo, VSVMI_MovimCod, ',
    '"Simples" TipoFrete, "' + CO_TAB_VSMOEnvavu + '" Tabela,  ',
    'NFEMA_Empresa Empresa, ',
    'NFEMA_Terceiro Terceiro, ',
    'NFEMA_SerNF SerNF, NFEMA_nNF nNF,  ',
    'NFEMA_Pecas Pecas, NFEMA_PesoKg PesoKg, ',
    'NFEMA_AreaM2 AreaM2, NFEMA_AreaP2 AreaP2,  ',
    'NFEMA_ValorT ValorT, 0 SdoPeca, ',
    '0 SdoPeso, 0 SdoArM2 ',
    'FROM ' + TMeuDB + '.vsmoenvavu ',
    'WHERE NFEMA_Empresa=' + Geral.FF0(Empresa),
    SQL_Periodo,
    //
    Geral.ATS_IF(Ck24Serie.Checked,
      ['AND NFEMA_SerNF=' + Geral.FF0(Ed24Serie.ValueVariant)]),
    'AND NFEMA_nNF>=' + Geral.FF0(Ed24NFeIni.ValueVariant),
    Geral.ATS_IF(Ed24NFeFim.ValueVariant > Ed24NFeIni.ValueVariant, [
    'AND NFEMA_nNF<=' + Geral.FF0(Ed24NFeFim.ValueVariant)]),
    //
    '']);
  end;
  //
  if FromEnvAvu and FromEnvEnv then
    SQL_UNION := 'UNION ';
  //
  if FromEnvEnv then
  begin
    SQL_EnvEnv := Geral.ATS([
    'SELECT DataHora, Codigo, VSVMI_MovimCod, ',
    '"Envio" TipoFrete, "' + CO_TAB_VSMOEnvenv + '" Tabela, ',
    'NFEMP_Empresa Empresa, ',
    'NFEMP_Terceiro Terceiro, ',
    'NFEMP_SerNF SerNF, NFEMP_nNF nNF, ',
    'NFEMP_Pecas Pecas, NFEMP_PesoKg PesoKg, ',
    'NFEMP_AreaM2 AreaM2, NFEMP_AreaP2 AreaP2, ',
    'NFEMP_ValorT ValorT, NFEMP_SdoPeca SdoPeca, ',
    'NFEMP_SdoPeso SdoPeso, NFEMP_SdoArM2 SdoArM2 ',
    'FROM ' + TMeuDB + '.vsmoenvenv ',
    'WHERE NFEMP_Empresa=' + Geral.FF0(Empresa),
    SQL_Periodo,
    //
    Geral.ATS_IF(Ck24Serie.Checked,
      ['AND NFEMP_SerNF=' + Geral.FF0(Ed24Serie.ValueVariant)]),
    'AND NFEMP_nNF>=' + Geral.FF0(Ed24NFeIni.ValueVariant),
    Geral.ATS_IF(Ed24NFeFim.ValueVariant > Ed24NFeIni.ValueVariant, [
    'AND NFEMP_nNF<=' + Geral.FF0(Ed24NFeFim.ValueVariant)]),
    //
    SQL_WHERE]);
  end;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + NomeTabTmp,
  ' ',
  SQL_EnvAvu,
  ' ',
  SQL_UNION,
  ' ',
  SQL_EnvEnv,
  ' ',
  '']);
  //
  //Geral.MB_SQL(self, DModG.QrUpdPID1);
end;

procedure TFmVSImpMOEnvRet2.Reopen00NFes(Query: TmySQLQuery;
  NomeTabTmp: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
  'SELECT ecg.Empresa, ',
  'ecg.SerNF, ecg.nNF, ',
  'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg, ',
  'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2, ',
  'SUM(ecg.ValorT) ValorT, ',
  'SUM(ecg.SdoPeca) SdoPeca, SUM(ecg.SdoPeso) SdoPeso, ',
  'SUM(ecg.SdoArM2) SdoArM2 ',
  'FROM ' + NomeTabTmp + ' ecg',
  'GROUP BY Empresa, SerNF, nNF ',
  'ORDER BY Empresa, SerNF, nNF ',
  '']);
end;

procedure TFmVSImpMOEnvRet2.Reopen01NFes(Query: TmySQLQuery;
  NomeTabTmp: String);
begin
  Query.Close;
  if CkSaldo.Checked then
  begin
    //SQL_Saldo := 'WHERE ecg.SdoPeca > 0';
    Query.Filter := 'SdoPeca>0';
    Query.Filtered := True;
  end else
  begin
    Query.Filter := '';
    Query.Filtered := False;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
  'SELECT ecg.Empresa, ecg.Terceiro, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Terceiro, ',
  'MAX(ecg.DataHora) DataHora, ',
  'ecg.SerNF, ecg.nNF, ',
  'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg, ',
  'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2, ',
  'SUM(ecg.ValorT) ValorT, ',
  'SUM(ecg.SdoPeca) SdoPeca, SUM(ecg.SdoPeso) SdoPeso, ',
  'SUM(ecg.SdoArM2) SdoArM2 ',
  'FROM ' + NomeTabTmp + ' ecg',
  'LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=ecg.Terceiro ',
  'GROUP BY Empresa, SerNF, nNF ',
  'ORDER BY Empresa, SerNF, nNF ',
  '']);
  //
  //Geral.MB_SQL(Self, Query);
end;

procedure TFmVSImpMOEnvRet2.SpeedButton1Click(Sender: TObject);
const
  MinData = 0;
  HrDefault = 0;
  HabilitaHora = False;
  //
  UsaSerie = False;
  NFeIni = 0;
  NFeFim = 0;
var
  Data: TDateTime;
  Qry: TmySQLQuery;
  Serie: Integer;
begin
  Data := DModG.ObtemAgora() - 90;
  case PCRelatorios.ActivePageIndex of
    0,
    1:
    begin
      DBCheck.ObtemData(Data, Data, MinData, HrDefault, HabilitaHora,
        'Data da NFe Inicial');
      //
      try
        Qry := TmySQLQuery.Create(Dmod);
        VS_PF.AbreSQLListaNFesEmitidasVS(Qry, UsaSerie, Serie, NFeIni, NFeFim,
        //VS_PF.AbreSQLListaNFesEmitidasVS(Qry, Ck24Serie.Checked,
        //Ed24Serie.ValueVariant, Ed24NFeIni.ValueVariant, Ed24NFeFim.ValueVariant,
        Geral.ATS([
        'SELECT Serie, NFe',
        'FROM ' + CO_NFes_XX,
        'WHERE Data>="' + Geral.FDT(data, 1) + '"',
        'ORDER BY Serie, NFe ',
        '']));
        //Geral.MB_SQL(nil, Qry);
        Serie := Qry.FieldByName('Serie').AsInteger;
        Ed24Serie.ValueVariant := Serie;
        Ed24NFeIni.ValueVariant := Qry.FieldByName('NFe').AsInteger;
        Qry.Last;
        Ck24Serie.Checked := Serie = Qry.FieldByName('Serie').AsInteger;
        Ed24NFeFim.ValueVariant := Qry.FieldByName('NFe').AsInteger;
      finally
        Qry.Free;
      end;
    end;
    (*
    2:
    begin
      /
    end;
    *)
    else Geral.MB_Aviso('"' +
    PCRelatorios.Pages[PCRelatorios.ActivePageIndex].Caption +
    '" sem implementa��o para pesquisa de NFe inicial!');
  end;
end;

end.
