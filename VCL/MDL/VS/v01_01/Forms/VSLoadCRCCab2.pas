unit VSLoadCRCCab2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  Variants, Grids, DBGrids, TypInfo, Vcl.ComCtrls,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, UnDmkProcFunc, UnDmkENums,
  dmkEditDateTimePicker, dmkDBGridZTO, UnProjGroup_Consts, UnProjGroup_PF,
  AppListas, UnAppEnums;

type
  TArrResIdx = array [0..9] of Variant;
  TRecriaRegistro = (recrregNao=0, recrregSim=1);
  TFmVSLoadCRCCab2 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    QrVSLoadCRCCab: TmySQLQuery;
    DsVSLoadCRCCab: TDataSource;
    QrVSLoadCRCTbs: TmySQLQuery;
    DsVSLoadCRCTbs: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSLoadCRCCabCodigo: TIntegerField;
    QrVSLoadCRCCabDataHora: TDateTimeField;
    QrVSLoadCRCCabLk: TIntegerField;
    QrVSLoadCRCCabDataCad: TDateField;
    QrVSLoadCRCCabDataAlt: TDateField;
    QrVSLoadCRCCabUserCad: TIntegerField;
    QrVSLoadCRCCabUserAlt: TIntegerField;
    QrVSLoadCRCCabAlterWeb: TSmallintField;
    QrVSLoadCRCCabAWServerID: TIntegerField;
    QrVSLoadCRCCabAWStatSinc: TSmallintField;
    QrVSLoadCRCCabAtivo: TSmallintField;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrItensPorTab: TmySQLQuery;
    QrItensPorTabCodigo: TIntegerField;
    QrItensPorTabNome: TWideStringField;
    QrItensPorTabItens: TLargeintField;
    QrItensPorTabAtivo: TSmallintField;
    DsItensPorTab: TDataSource;
    QrTabelas: TmySQLQuery;
    QrRegistros: TmySQLQuery;
    PB1: TProgressBar;
    DBEdit1: TDBEdit;
    BtImporta: TBitBtn;
    QrIdx: TmySQLQuery;
    QrVSLoadCRCCabOriServrID: TIntegerField;
    EdOriServrID: TdmkEdit;
    Label5: TLabel;
    QrSel: TmySQLQuery;
    DsSel: TDataSource;
    QrPsqCross: TmySQLQuery;
    ItsImporta1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GBImportacao: TGroupBox;
    Panel6: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    DBGSel: TDBGrid;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DGDados: TDBGrid;
    PB2: TProgressBar;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Pesquisanovosdados1: TMenuItem;
    QrPNI1: TmySQLQuery;
    QrPNI2: TmySQLQuery;
    Mostraitensdatabela1: TMenuItem;
    Query1: TmySQLQuery;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Panel7: TPanel;
    RGPalletsAtrelados: TRadioGroup;
    Query2: TmySQLQuery;
    QrVSLoadCRCWrn: TmySQLQuery;
    QrVSLoadCRCWrnCodigo: TIntegerField;
    QrVSLoadCRCWrnControle: TIntegerField;
    DsVSLoadCRCCabWrn: TDataSource;
    QrVSLoadCRCWrnNome: TWideStringField;
    QrVSLoadCRCWrnLinha: TIntegerField;
    QrVSLoadCRCWrnLk: TIntegerField;
    QrVSLoadCRCWrnDataCad: TDateField;
    QrVSLoadCRCWrnDataAlt: TDateField;
    QrVSLoadCRCWrnUserCad: TIntegerField;
    QrVSLoadCRCWrnUserAlt: TIntegerField;
    QrVSLoadCRCWrnAlterWeb: TSmallintField;
    QrVSLoadCRCWrnAWServerID: TIntegerField;
    QrVSLoadCRCWrnAWStatSinc: TSmallintField;
    QrVSLoadCRCWrnAtivo: TSmallintField;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrPsqTbs: TmySQLQuery;
    QrPsqTbsCodigo: TIntegerField;
    QrPsqTbsControle: TIntegerField;
    QrPsqTbsTabela: TWideStringField;
    QrPsqTbsRegistros: TIntegerField;
    QrVSLoadCRCTbsCodigo: TIntegerField;
    QrVSLoadCRCTbsControle: TIntegerField;
    QrVSLoadCRCTbsTabela: TWideStringField;
    QrVSLoadCRCTbsRegistros: TIntegerField;
    Corrigir011: TMenuItem;
    Mostraitemjimportadodoitemdatabelanoreimportado1: TMenuItem;
    GroupBox1: TGroupBox;
    QrItReInn: TmySQLQuery;
    DsItReInn: TDataSource;
    PnReInn: TPanel;
    DBGReInn: TDBGrid;
    Splitter1: TSplitter;
    QrDBMQeLnk: TmySQLQuery;
    QrDBMQeLnkKLAskrTab: TWideStringField;
    QrDBMQeLnkKLAskrCol: TWideStringField;
    QrDBMQeLnkKLRplyTab: TWideStringField;
    QrDBMQeLnkKLRplyCol: TWideStringField;
    QrDBMQeLnkAlterWeb: TSmallintField;
    QrDBMQeLnkAWServerID: TIntegerField;
    QrDBMQeLnkAWStatSinc: TSmallintField;
    QrDBMQeLnkAtivo: TSmallintField;
    QrDBMQeLnkKLPurpose: TSmallintField;
    QrDBMQeLnkTbPurpose: TSmallintField;
    QrDBMQeLnkTbManage: TSmallintField;
    QrDBMQeLnkKLMTbsCol: TWideStringField;
    Query: TmySQLQuery;
    CkDescreve: TCheckBox;
    PesquisanovosdadoseImporta1: TMenuItem;
    TabSheet4: TTabSheet;
    MeDescreve: TMemo;
    QrItsPorNoTab: TmySQLQuery;
    QrItsPorNoTabKLAskrCol: TWideStringField;
    QrOrfaos: TmySQLQuery;
    QrItsPorNoTabKLMTbsCol: TWideStringField;
    QrItsPorNoTabKLAskrTab: TWideStringField;
    QrDBMQeLnkKLMTabDel: TWideStringField;
    QrItsPorNoTabTbManage: TIntegerField;
    QrItsPorNoTabKLRplyTab: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure ItsImporta1Click(Sender: TObject);
    procedure Pesquisanovosdados1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSLoadCRCCabAfterOpen(DataSet: TDataSet);
    procedure QrVSLoadCRCCabAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSLoadCRCCabBeforeClose(DataSet: TDataSet);
    procedure QrVSLoadCRCCabBeforeOpen(DataSet: TDataSet);
    procedure Mostraitensdatabela1Click(Sender: TObject);
    procedure QrItensPorTabAfterScroll(DataSet: TDataSet);
    procedure QrItensPorTabBeforeClose(DataSet: TDataSet);
    procedure QrVSLoadCRCWrnAfterOpen(DataSet: TDataSet);
    procedure QrItensPorTabAfterOpen(DataSet: TDataSet);
    procedure Corrigir011Click(Sender: TObject);
    procedure Mostraitemjimportadodoitemdatabelanoreimportado1Click(
      Sender: TObject);
    procedure QrSelAfterScroll(DataSet: TDataSet);
    procedure QrSelBeforeClose(DataSet: TDataSet);
    procedure QrItReInnAfterOpen(DataSet: TDataSet);
    procedure QrItReInnBeforeClose(DataSet: TDataSet);
    procedure PesquisanovosdadoseImporta1Click(Sender: TObject);
  private
    FStrActios: String;
    FStrActLig: Boolean;
    FStrActCnt: Integer;
    FMemoLinesCount, FTabLstCount: Integer;
    FTabRead, FTabWrite, FSQLCorda, FFldsPriComERP, FFldsPriSemERP,
    FSQLsPriComERP, FSQLsPriSemERP: array of String;
    FCamposName: array of array of String;
    FLstTabs: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure InsUpdVSLoadCRCTbs(const NomeTab: String(*; var Controle: Integer*));
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure AdicionaAoMemo(Texto: String);
    //
    procedure DefineTamanhosArrayTabelas(K: Integer);
    //
    function  ExcluiVSMovIts_de_VSMovItz(TabFromRead, TabToDel: String;
              IndiceToDel: Integer; AtrelouInseriuToDel: TERPAtrelIns;
              QueryFromRead: TmySQLQuery; ArrResIdxToDel: TArrResIdx):
              Boolean;
    //
    procedure InsereDadosTabelaEmIndice(Tabela: String; Index:
              Integer = -1);
    //
    procedure MostraVSLoadCRCIts(SQLType: TSQLType);
    function  PesquisaNovosDadosAImportar(ServerID: Integer): Boolean;
    //
    function  ObtemAtrelou(AtrelIns: TERPAtrelIns): Boolean;
    function  ObtemInseriu(AtrelIns: TERPAtrelIns): Boolean;
    function  ObtemAtrelouInseriu_Indice(const Tabela: String; const QrPsq:
              TmySQLQuery; const Indice: Integer; const OthrIdx:
              array of Integer; var AtrelIns: TERPAtrelIns): TArrResIdx;
    function  ObtemIndiceDeTabela(Tabela: String; AddOnNoFind: Boolean = True): Integer;
    procedure PreDefineTabelasCamposETextosSQL();
    function  QtdIndicesARI(ARI: TArrResIdx): Integer;
    procedure ReopenSel(Indice: Integer; SQLExtra: String);
    function  VerificaIdenticidadeDeTabelas(const DBOri: TmySQLDataBase; const
              TabOri: String; const DBDst: TmySQLDatabase; const TabDst: String;
              var SQLDst: String; var SQLOri: String): Boolean;
    procedure SubstituiAWStatSincEmSQL(var SQL: String);

///////////////////////////////// N O V O //////////////////////////////////////
    //function  PesquisaNovosDados(): Boolean;
    function  AtrelaItensDaTabela(Tabela: String): Boolean;
    function  DefineCamposEValoresItem(const Tabela: String; const TabIndex:
              Integer; var Itens: Integer): Boolean;
    function  DeletaItensEmTabela(Tabela: String): Boolean;
{
    function  ExcluiInserido(var Inseriu: Boolean; const Tabela: String; const
              IdxFlds: array of String; const IdxVals: array of Variant):
              Boolean;
}
    function  ExcluiAtreladoEInserid2(const Atrelou: Boolean; var Inseriu:
              Boolean; const Tabela: String; const TabIndex: Integer; const
              ArrResIdx: TArrResIdx): Boolean;
    procedure ExecutaImportacao();
    function  InsereItensEmTabela(Tabela: String): Boolean;
    function  InsereRegistro_Arr3(Tabela: String; TabIndex: Integer; SQL_WHR:
              String;  CamposDst: array of String; ValoresDst:
              array of Variant): Boolean;
    function  NeutralizaOrige2(Tabela: String; SQL_WHR: String): Boolean;
    function  ObtemValorSubstituto(const QrPsq: TmySQLQuery; const Indice:
              Integer; var ValorRes: Variant): Boolean;
  public
    { Public declarations }
    FSeq, FCabIni(*, FCodOnImport*): Integer;
    FLocIni: Boolean;
    //
    function  AtrelaTabelas_Codigo(Tabela: String; Ori_Codigo, Dst_Codigo,
              AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
    function  AtrelaTabelas_Int(Tabela, Campo: String; Ori_int, Dst_Int,
              AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSLoadCRCTbs(Controle: Integer);
    procedure ReopenVSLoadCRCWrn(Controle: Integer);
    function  NeutralizaOrigem(Tabela: String; Campos: array of String;
              Valores: array of Variant): Boolean;
    function  OlvidaOrigem(Tabela: String; Campos: array of String;
              Valores: array of Variant): Boolean;
    procedure OlvidaTodaTabelaVSCacItsA();
  end;

var
  FmVSLoadCRCCab2: TFmVSLoadCRCCab2;

const
  FFormatFloat = '00000';

var
  FCampos: array of String;
  FValores: array of Variant;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSLoadCRCIts, UnVS_CRC_PF,
  CreateGeral, ModuleGeral, UnERPSinc_Tabs, UnVS_Jan, GetValor;

{$R *.DFM}

const
  TabelasIdxDifere: array[0..1] of String = ('vspalleta', 'VSEntiMP');

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSLoadCRCCab2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSLoadCRCCab2.Mostraitemjimportadodoitemdatabelanoreimportado1Click(
  Sender: TObject);
var
  Qry: TmySQLQuery;
  sSQL, W_A, Fld: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MySyncDB, [
    'SHOW INDEX FROM ' + QrItensPorTabNome.Value,
    'WHERE Key_name="PRIMARY" ',
    '']);
    //
    sSQL :=  '';
    W_A := 'WHERE ';
    while not Qry.Eof do
    begin
      Fld := Qry.FieldByName('Column_name').AsString;
      if Fld <> 'AWServerID' then
      begin
        sSQL := sSQL + W_A + Fld + '="' +
        QrSel.FieldByName(Fld).AsString + '"';
        W_A := 'AND ';
      end;
      //
      Qry.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrItReInn, Dmod.MyDB, [
    'SELECT * FROM ' + QrItensPorTabNome.Value,
    sSQL,
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSLoadCRCCab2.Mostraitensdatabela1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MySyncDB, [
  'SELECT * ',
  'FROM ' + Lowercase(QrItensPorTabNome.Value),
  'WHERE AWServerID <> 0 ',
  //'AND AWStatSinc NOT IN (8,10) ',
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  '']);
  //

end;

procedure TFmVSLoadCRCCab2.MostraVSLoadCRCIts(SQLType: TSQLType);
begin
  //  Nada!!
end;

function TFmVSLoadCRCCab2.NeutralizaOrige2(Tabela, SQL_WHR: String): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
  SQL_Update, SQL_UPD: String;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnSinc); // 8!
  //
  SQL_UPD        := 'SET AWStatSinc=8 ';
  SQL_Update := Geral.ATS(['UPDATE ' + LowerCase(Tabela),
    SQL_UPD,
    SQL_WHR, //'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  //
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Update,
  '']);
{
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
}
end;

function TFmVSLoadCRCCab2.NeutralizaOrigem(Tabela: String;
  Campos: array of String; Valores: array of Variant): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnSinc); // 8!
  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
end;

function TFmVSLoadCRCCab2.ObtemAtrelou(AtrelIns: TERPAtrelIns): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemAtrelou()';
begin
  case AtrelIns of
    //TERPAtrelIns.atrinsIndef
    TERPAtrelIns.atrinsNoAtrelNoIns:  Result := False;
    TERPAtrelIns.atrinsNoAtrelButIns: Result := False;
    TERPAtrelIns.atrinsNoInsButAtrel: Result := True;
    TERPAtrelIns.atrinsAtrelAndIns:   Result := True;
    else
    begin
      Result := False;
      AdicionaAoMemo('AtrelIns indefinido em "' + sProcName + '"');
    end;
  end;
end;

function TFmVSLoadCRCCab2.ObtemAtrelouInseriu_Indice(const Tabela: String;
  const QrPsq: TmySQLQuery; const Indice: Integer; const OthrIdx:
  array of Integer; var AtrelIns: TERPAtrelIns): TArrResIdx;
var
  Atrelou, Inseriu: Boolean;
  SQLPsqCross, SQLPsqInsertedIts, IdxTxt: String;
  I: Integer;
  IdxDifere: Boolean;
begin
  IdxDifere := True;
  for I := Low(TabelasIdxDifere) to High(TabelasIdxDifere) do
    if Lowercase(Tabela) = Lowercase(TabelasIdxDifere[I]) then
      IdxDifere := False;
  //
  Result[0]     := 0; // Quantidade de �ndices
  Result[1]     := 0; // �ndice 1
  Result[2]     := 0; // �ndice 2
  Result[3]     := 0; // etc...
  Result[4]     := 0;
  Result[5]     := 0;
  Result[6]     := 0;
  Result[7]     := 0;
  Result[8]     := 0;
  Result[9]     := 0;
  AtrelIns      := TERPAtrelIns.atrinsIndef;
  Atrelou       := False;
  Inseriu       := False;
  //
  if QrPsq <> nil then
    IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString
  else
    IdxTxt := Geral.FF0(OthrIdx[0]);
  //
  SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + FCamposName[Indice][0] + '="' + IdxTxt + '"';
  //
  //SQLPsqInsertedIts := 'WHERE ' + FCamposName[Indice][0] + '="' + IdxTxt + '"';
  //
  for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
  begin
    IdxTxt := Geral.FF0(OthrIdx[I]);
    //
    SQLPsqCross := SQLPsqCross + sLineBreak +
    'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    //
    (*SQLPsqInsertedIts := SQLPsqInsertedIts + sLineBreak +
    'AND ' + FCamposName[Indice][I] + '="' + IdxTxt + '"';*)
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
  FSQLsPriComERP[Indice], //SQLIndexQry,
  SQLPsqCross,
  '']);
  Atrelou := QrPsqCross.RecordCount > 0;
  if Atrelou then
  begin
    Result[0]     := Length(FCamposName[Indice]); // Quantidade de �ndices
    for I := 0 to Result[0] - 1 do
    begin
      Result[I + 1] := QrPsqCross.Fields[I].Value;
    end;
  end;
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
  FSQLsPriSemERP[Indice], //SQLPsqInsertedCab,
  SQLPsqInsertedIts,
  '']);
  //
*)
  if (not IdxDifere) or Atrelou then
  begin
    if IdxDifere then
      IdxTxt := Geral.FF0(Integer(Result[1]))
    else
    begin
      if QrPsq <> nil then
        IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString
      else
        IdxTxt := Geral.FF0(OthrIdx[0]);
    end;
    //
    SQLPsqInsertedIts := 'WHERE ' + FCamposName[Indice][0] + '="' + IdxTxt + '"';
    //
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      if IdxDifere then
        IdxTxt := Geral.FF0(Integer(Result[I+1]))
      else
      begin
        if QrPsq <> nil then
          IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString
        else
          IdxTxt := Geral.FF0(OthrIdx[I]);
      end;
      //
      SQLPsqInsertedIts := SQLPsqInsertedIts + sLineBreak +
      'AND ' + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    FSQLsPriSemERP[Indice], //SQLPsqInsertedCab,
    SQLPsqInsertedIts,
    '']);
    //
    Inseriu := QrPsqCross.RecordCount > 0;
    //
  end;
  //
  if Atrelou then
  begin
    if Inseriu then
      AtrelIns := TERPAtrelIns.atrinsAtrelAndIns (*4*)
    else
      AtrelIns := TERPAtrelIns.atrinsNoInsButAtrel(*3*)
  end else
  if Inseriu then
    AtrelIns := TERPAtrelIns.atrinsNoAtrelButIns(*2*)
  else
     AtrelIns := TERPAtrelIns.atrinsNoAtrelNoIns;(*1*)
  //
end;

function TFmVSLoadCRCCab2.ObtemIndiceDeTabela(Tabela: String; AddOnNoFind: Boolean): Integer;
  function Obtem(var N: Integer): Boolean;
  var
    I: Integer;
  begin
    N := -1;
    for I := Low(FTabRead) to High(FTabRead) do
      if Lowercase(FTabRead[I]) = LowerCase(Tabela) then
      begin
        N := I;
        Exit;
      end;
  end;
  //
begin
  Obtem(Result);
  //
  if (Result = -1) and  AddOnNoFind then
  begin
    InsereDadosTabelaEmIndice(Tabela);
    Obtem(Result);
  end;
end;

function TFmVSLoadCRCCab2.ObtemInseriu(AtrelIns: TERPAtrelIns): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemInseriu()';
begin
  case AtrelIns of
    //TERPAtrelIns.atrinsIndef
    TERPAtrelIns.atrinsNoAtrelNoIns:  Result := False;
    TERPAtrelIns.atrinsNoAtrelButIns: Result := True;
    TERPAtrelIns.atrinsNoInsButAtrel: Result := False;
    TERPAtrelIns.atrinsAtrelAndIns:   Result := True;
    else
    begin
      Result := False;
      AdicionaAoMemo('AtrelIns indefinido em "' + sProcName + '"');
    end;
  end;
end;

function TFmVSLoadCRCCab2.ObtemValorSubstituto(const QrPsq: TmySQLQuery;
  const Indice: Integer; var ValorRes: Variant): Boolean;
  //
  function VerificaSeFoiExcluido(KLRplyTab, KLMTabDel, KLRplyCol, Valor: String): Variant;
  const
    sProcName = 'VerificaSeFoiExcluido()';
  var
    AWServerID, AWStatSinc, Dst_Int: Integer;
    AtrelouInseriu: TERPAtrelIns;
  begin
    Result := Null;
    if Lowercase(KLMTabDel) = 'ctrlexcltb' then
    begin
      AdicionaAoMemo('Tabela "' + KLMTabDel + '" n�o implementada em "' + sProcName + '"');
    end else
    begin
      UndmkDAC_PF.AbreMySQLQuery0(QrPsqCross,  DModG.MySyncDB, [
      'SELECT ' + KLRplyCol,
      'FROM ' + KLMTabDel,
      'WHERE ' + KLRplyCol + '="' +  Valor + '"',
      '']);
      AdicionaAoMemo('DELETADO: ' + KLRplyTab + '.' + KLRplyCol + ' > ' + Valor);
      //Ori_Int := Integer(Valor);
      UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
      'SELECT Dst_' + KLRplyCol,
      'FROM erpsync_' + Lowercase(KLRplyTab),
      'WHERE Ori_' + KLRplyCol + '="' + Valor + '"',
      '']);
      //
      if QrOrfaos.FieldByName('Dst_' + KLRplyCol).AsInteger = 0 then
      begin
        //Geral.MB_Info('Adicionar ao erpsync_' + Lowercase(QrItsPorNoTabKLMTbsCol.Value));
        // Parei aqui
        AWServerID     := QrSel.FieldByName('AWServerID').AsInteger;
        AWStatSinc     := QrSel.FieldByName('AWStatSinc').AsInteger;
        AtrelouInseriu := atrinsNoAtrelNoIns;
        Dst_Int := UMyMod.BPGS1I32(KLRplyTab, KLRplyCol, '', '', tsPos, stIns, 0);
        //
        AtrelaTabelas_Int(KLRplyTab, KLRplyCol, Integer(Valor), Dst_Int,
          AWServerID, AWStatSinc, AtrelouInseriu);
        //
        Result := Dst_Int;
      end;
    end;
    //if ? KLMTabDel
  end;
  //
  //
  //
  function ObtemValorComutadoArqMorto(KLAskrTab, KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol: String): Variant;
  var
    SQLPsqCross, IdxTxt, Corda: String;
    I: Integer;
  begin
    Result := Null;
    Corda  := '';
    IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString;
    //
    if FStrActLig then
      Corda := FCamposName[Indice][0];
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + FCamposName[Indice][0] + '="' + IdxTxt + '"';
    //
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      if FStrActLig then
        Corda := Corda + '+' + FCamposName[Indice][I];
      //
      IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString;
      //
      SQLPsqCross := SQLPsqCross + sLineBreak +
      'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    FSQLsPriComERP[Indice], //SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName('Dst_' + KLAskrCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLAskrTab + '.' + Corda + '= "' +
        Geral.VariavelToString(Result) + '"';
    end else
    begin
      Result := VerificaSeFoiExcluido(KLRplyTab, KLMTabDel, KLRplyCol, IdxTxt);
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLRplyTab + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;
  //
  //
  //ObtemValorComutadoSimples(KLAskrCol: String): Variant;
  function ObtemValorComutadoSimples(KLAskrTab, KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol: String): Variant;
  var
    SQLPsqCross, IdxTxt, Corda: String;
    I: Integer;
  begin
    Result := Null;
    Corda  := '';
    IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString;
    //
    if FStrActLig then
      Corda := FCamposName[Indice][0];
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + FCamposName[Indice][0] + '="' + IdxTxt + '"';
    //
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      if FStrActLig then
        Corda := Corda + '+' + FCamposName[Indice][I];
      //
      IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString;
      //
      SQLPsqCross := SQLPsqCross + sLineBreak +
      'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    FSQLsPriComERP[Indice], //SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName('Dst_' + KLAskrCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLAskrTab + '.' + Corda + '= "' +
        Geral.VariavelToString(Result) + '"';
    end else
    begin
      Result := VerificaSeFoiExcluido(KLRplyTab, KLMTabDel, KLRplyCol, IdxTxt);
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLRplyTab + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;
  //

  //ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol: String): Variant;
  function ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol:
  String): Variant;
  const
   sProcName = 'ObtemValorComutadoMulTabs()';
  var
    SQLIndexQry, SQLPsqCross, IdxTxt, Tabela: String;
    I: Integer;
  begin
    Result := Null;
////////////////////////////////////////////////////////////////////////////////
    if Lowercase(KLRplyTab) = Lowercase(CO_VSXxxTab) then
    begin
      Tabela := ProjGroup_PF.ObtemNomeTabelaMulTabs(TypeInfo(TEstqMovimID),
        QrPsq.FieldByName(KLMTbsCol).AsInteger, True);
    end else
    if Lowercase(KLRplyTab) = Lowercase('TEstqDefMulFldEMxx') then
    begin
      Tabela := ProjGroup_PF.ObtemNomeTabelaMulTabs(TypeInfo(TEstqDefMulFldEMxx),
        QrPsq.FieldByName(KLMTbsCol).AsInteger, True);
    end else
    begin
      Tabela := KLRplyTab;
      Geral.MB_Erro('Tabela "' + KLRplyTab + '" n�o implementada em ' + sProcName);
    end;
    if Lowercase(Tabela) = Lowercase(CO_VS___Cab) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Query2, DModG.MySyncDB, [
      'SELECT MovimID ',
      'FROM vsmovcab ',
      'WHERE Codigo=' + Geral.FF0(QrPsq.FieldByName('MovimCod').AsInteger), // 421
      'AND CodigoID=' + Geral.FF0(QrPsq.FieldByName('Codigo').AsInteger), // 98
      '']);
      Tabela := ProjGroup_PF.ObtemNomeTabelaMulTabs(TypeInfo(TEstqMovimID),
        Query2.FieldByName('MovimID').AsInteger, True);
    end;
////////////////////////////////////////////////////////////////////////////////
    IdxTxt := QrPsq.FieldByName(KLAskrCol).AsString;
    //
    SQLIndexQry := 'SELECT ' + CO_ERPSync_FldDst + KLRplyCol + sLineBreak +
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + Tabela);
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + KLRplyCol + '="' + IdxTxt + '"';
{
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString;
      //
      SQLPsqCross := SQLPsqCross + sLineBreak +
      'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName(CO_ERPSync_FldDst + KLRplyCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + Tabela + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;

  //ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLRplyCol: String): Variant;
  function ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol: String): Variant;
  var
    SQLIndexQry, SQLPsqCross, IdxTxt: String;
    I: Integer;
  begin
    Result := Null;
    IdxTxt := QrPsq.FieldByName(KLAskrCol).AsString;
    //
    SQLIndexQry := 'SELECT ' + CO_ERPSync_FldDst + KLRplyCol + sLineBreak +
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + KLRplyTab);
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + KLRplyCol + '="' + IdxTxt + '"';
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName(CO_ERPSync_FldDst + KLRplyCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLRplyTab + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end else
    begin
      Result := VerificaSeFoiExcluido(KLRplyTab, KLMTabDel, KLRplyCol, IdxTxt);
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLRplyTab + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;

  function ObtemValorComutadoSemTab(KLAskrCol, KLMTbsCol, KLRplyCol: String): Variant;
  var
    SQLIndexQry, SQLPsqCross, IdxTxt: String;
    I: Integer;
  begin
    Result := Null;
    IdxTxt := QrPsq.FieldByName(KLAskrCol).AsString;
    //
    SQLIndexQry := 'SELECT ' + CO_ERPSync_FldDst + KLRplyCol + sLineBreak +
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + KLMTbsCol);
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + KLRplyCol + '="' + IdxTxt + '"';
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName(CO_ERPSync_FldDst + KLRplyCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLMTbsCol + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;

const
  sProcName = 'FmVSLoadCRCCab2.ObtemValorSubstituto()';
var
  KLAskrTab, KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol, KLMTabDel: String;
  KLPurpose, TbPurpose, TbManage: Integer;
begin
  try
    Result := False;
    KLAskrTab := QrDBMQeLnkKLAskrTab.Value;
    KLAskrCol := QrDBMQeLnkKLAskrCol.Value;
    if QrPsq.FieldByName(KLAskrCol).AsVariant = 0 then
    begin
      Result := True;
      Exit;
    end;
    KLRplyTab := QrDBMQeLnkKLRplyTab.Value;
    KLRplyCol := QrDBMQeLnkKLRplyCol.Value;
    KLMTbsCol := QrDBMQeLnkKLMTbsCol.Value;
    KLPurpose := QrDBMQeLnkKLPurpose.Value;
    TbPurpose := QrDBMQeLnkTbPurpose.Value;
    TbManage  := QrDBMQeLnkTbManage.Value;
    KLMTabDel := QrDBMQeLnkKLMTabDel.Value;
    //
    case TItemTuplePurpose(KLPurpose) of
      // Indices prim�rios com necessidade de comutar no ERP.
      (*01*)TItemTuplePurpose.itpCDRIncremSync,
      (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
      begin
        //QtdFldIdx := QtdFldIdx + 1;
        ValorRes := ObtemValorComutadoSimples(KLAskrTab, KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol);
        Result := True;
      end;
      // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
      (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
      (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
      begin
        //QtdFldIdx := QtdFldIdx + 1;
        Result := True;
      end;
      // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
      (*02*)TItemTuplePurpose.itpERPRelatnSync,
      (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
      begin
        Result := True;
      end;
      // Campos com relacionamento e com necessidade de comutar
      (*04*)TItemTuplePurpose.itpCDRRelatnSync:
      begin
        //QtdFldSyn := QtdFldSyn + 1;
        ValorRes := ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol);
        Result := True;
      end;
      (*10*)TItemTuplePurpose.itpCDRRelSncOrfao:
      begin
        //QtdFldSyn := QtdFldSyn + 1;
        ValorRes := ObtemValorComutadoSemTab(KLAskrCol, KLMTbsCol, KLRplyCol);
        Result := True;
      end;
      (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
      begin
        ValorRes := ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol);
        Result := True;
      end;
      // Outros campos sem necessidade de comutar
      (*05*)TItemTuplePurpose.itpUsrPrimtivData,
      (*06*)TItemTuplePurpose.itpSysOrCalcData,
      (*07*)TItemTuplePurpose.itpERPSync,
      (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
      (*12*)TItemTuplePurpose.itpDeprecado,
      (*13*)TItemTuplePurpose.itpInutilizado:
      begin
        // Nada!
        Result := True;
      end;
      (*15*)TItemTuplePurpose.itpCDRDeleteSync:
      begin
        // Parei aqui! ver o que fazer!
        ValorRes := ObtemValorComutadoArqMorto(KLAskrTab, KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol);
        Result := True;
      end;
      // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
      // Somente copiar ao ERP se n�o existir.
      (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
      begin
        ValorRes := ObtemValorComutadoSimples(KLAskrTab, KLAskrCol, KLRplyTab, KLMTabDel, KLRplyCol);
        Result := True;
      end;
      // (*00*)TItemTuplePurpose.itpIndef,
      else
      begin
        // Nada! J� � falso por default
      end;
    end;
    if Result = False then
      AdicionaAoMemo('KLAskrTab ' + KLAskrTab + '. ' + KLAskrCol +
      GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
      ' n�o implementado em ' + sProcName);
  except
    Geral.MB_Erro('KLAskrTab ' + KLAskrTab + '. ' + KLAskrCol + sLineBreak +
      'KLReplTab ' + KLRplyTab + '. ' + KLRplyCol + sLineBreak +
      GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
      ' erro em ' + sProcName);
    raise;
  end;
end;

function TFmVSLoadCRCCab2.OlvidaOrigem(Tabela: String; Campos: array of String;
  Valores: array of Variant): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnNoSinc); // 10!
  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
end;

procedure TFmVSLoadCRCCab2.OlvidaTodaTabelaVSCacItsA;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  'UPDATE vscacitsa ',
  'SET AWStatSinc=' + Geral.FF0(Integer(stDwnNoSinc)),
  'WHERE AWServerID=' + Geral.FF0(QrVSLoadCRCCabOriServrID.Value),
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  '']);
end;

procedure TFmVSLoadCRCCab2.Pesquisanovosdados1Click(Sender: TObject);
begin
  PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value);
end;

function TFmVSLoadCRCCab2.PesquisaNovosDadosAImportar(ServerID: Integer): Boolean;
begin
  FMemoLinesCount := 0;
  Memo1.Lines.Clear;
  Result := VS_CRC_PF.PesquisaNovosDadosCDR(stDwnSinc, FLstTabs,
  DModG.MySyncDB, QrItensPorTab, (*QrTabelas,*) QrRegistros, PB1,
  LaAviso1, LaAviso2, (*CkEnviarTudo.Checked*)False, ServerID);
  //
  BtConfirma.Enabled := Result;
  if not Result then
    Geral.MB_Info('N�o h� dados para serem importados ao ERP!');
end;

procedure TFmVSLoadCRCCab2.PesquisanovosdadoseImporta1Click(Sender: TObject);
begin
  if PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value) then
    if QrItensPorTab.State <> dsInactive then
      ExecutaImportacao();
end;

procedure TFmVSLoadCRCCab2.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSLoadCRCCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSLoadCRCCab, QrVSLoadCRCTbs);
end;

procedure TFmVSLoadCRCCab2.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSLoadCRCCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSLoadCRCTbs);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSLoadCRCTbs);
  //
  MyObjects.HabilitaMenuItemItsDel(ItsImporta1, QrItensPorTab);
  MyObjects.HabilitaMenuItemItsIns(Pesquisanovosdados1, QrVSLoadCRCCab);
  //
  Mostraitemjimportadodoitemdatabelanoreimportado1.Enabled :=
    (QrSel.State <> dsInactive) and (QrSel.RecordCount > 0);
end;

procedure TFmVSLoadCRCCab2.PreDefineTabelasCamposETextosSQL();
var
  I, CamposCount: Integer;
  TabOri, TabDst, (*SQLDst, *)SQLOri: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa');
  FTabLstCount := QrItensPorTab.RecordCount;
  DefineTamanhosArrayTabelas(FTabLstCount);
  //
  while not QrItensPorTab.Eof do
  begin
    I      := QrItensPorTab.RecNo - 1;
    InsereDadosTabelaEmIndice(QrItensPorTabNome.Value, I);
    //
    QrItensPorTab.Next;
  end;
  //
  MyObjects.UpdPBOnly(PB1);
end;

procedure TFmVSLoadCRCCab2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmVSLoadCRCCab2.VerificaIdenticidadeDeTabelas(const DBOri:
  TmySQLDataBase; const TabOri: String; const DBDst: TmySQLDatabase; const
  TabDst: String; var SQLDst: String; var SQLOri: String): Boolean;
var
  CamposOri, CamposDst: String;
  OkOri, OkDst: Boolean;
  QtdReg1, QtdReg2: Integer;
begin
  OkOri := False;
  OkDst := False;
  CamposOri := '';
  CamposDst := '';
  //
  CamposOri := UMyMod.ObtemCamposDeTabelaIdentica(DBOri, TabOri, '', QtdReg1);
  SQLOri := Geral.ATS(['SELECT ', CamposOri, 'FROM ' + TabDst]);
  UnDmkDAC_PF.AbreMySQLQuery0(Query1, DBDst, [SQLOri, 'LIMIT 1']);
  OkOri := Query1.State <> dsInactive;
  //
  CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(DBDst, TabDst, '', QtdReg1);
  SQLDst := Geral.ATS(['SELECT ', CamposDst, 'FROM ' + TabOri]);
  UnDmkDAC_PF.AbreMySQLQuery0(Query1, DBOri, [SQLDst, 'LIMIT 1']);
  OkDst := Query1.State <> dsInactive;
  //
  Result := OkOri and OkDst;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSLoadCRCCab2.DefParams;
begin
  VAR_GOTOTABELA := 'vsloadcrccab';
  VAR_GOTOMYSQLTABLE := QrVSLoadCRCCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsloadcrccab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

function TFmVSLoadCRCCab2.DeletaItensEmTabela(Tabela: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.DeletaItensEmTabela()';
var
  TabIndex, QtdFldIdx, Itens: Integer;
  Valor: Variant;
  //Campos: array of String;
  //Valores: array of Variant;
  ARI: TArrResIdx;
  Atrelou, Inseriu: Boolean;
  AtrelouInseriu: TERPAtrelIns;
begin
  Result  := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrDBMQeLnk, Dmod.MyDB, [
  'SELECT * ',
  'FROM dbmqeilnk ',
  'WHERE LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
  '']);
(*
  if not (TCRCTableManage(QrDBMQeLnkTbManage.Value) in CO_SeqCRCTableManageInsUpd) then
    Exit;
  AllUpAndSyncOnlyIns := TCRCTableManage(QrDBMQeLnkTbManage.Value) = crctmAllUpAndSyncOnlyIns;
*)  MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True, 'Excluindo ' + Tabela);
  //
(*
  Inseriu  := False;
*)
  TabIndex := ObtemIndiceDeTabela(Tabela, False);
  if TabIndex >= 0 then
  begin
    QtdFldIdx  := 0;
(*
    QtdFldSyn  := 0;
    ERPIncOver := False;
*)
    ReopenSel(TabIndex, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    //
    QrDBMQeLnk.First;
    while not QrDBMQeLnk.Eof do
    begin
      case TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) of
        (*01*)TItemTuplePurpose.itpCDRIncremSync,
        (*02*)TItemTuplePurpose.itpERPRelatnSync,
        (*03*)TItemTuplePurpose.itpSysRelatnPrDf,
        (*04*)TItemTuplePurpose.itpCDRRelatnSync,
        (*05*)TItemTuplePurpose.itpUsrPrimtivData,
        (*06*)TItemTuplePurpose.itpSysOrCalcData,
        (*07*)TItemTuplePurpose.itpERPSync,
        (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
        (*09*)TItemTuplePurpose.itpERPSrvrIncOver,
        (*10*)TItemTuplePurpose.itpCDRRelSncOrfao,
        (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
        (*12*)TItemTuplePurpose.itpDeprecado,
        (*13*)TItemTuplePurpose.itpInutilizado,
        (*14*)TItemTuplePurpose.itpCDRRelSncMulTab,
        (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync,
        (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
        begin
          // Nada!
        end;
        (*15*)TItemTuplePurpose.itpCDRDeleteSync:
        begin
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // (*00*)TItemTuplePurpose.itpIndef,
        else begin
          AdicionaAoMemo('Tabela ' + Tabela + '. ' +
          GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
          ' n�o implementado em ' + sProcName);
          Exit;
        end;
      end;
      //
      QrDBMQeLnk.Next;
    end;
    if QtdFldIdx = 0 then
    begin
      AdicionaAoMemo('Tabela ' + Tabela + '. Nenhum �ndice prim�rio definido!');
    end;
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
(*
      NaoInserirJaInseridoEOk := False;
*)
      Itens := 0;
      SetLength(FCampos, 0);
      SetLength(FValores, 0);
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, TabIndex, [], AtrelouInseriu);
      Atrelou := ObtemAtrelou(AtrelouInseriu);
      Inseriu := ObtemInseriu(AtrelouInseriu);
      //
      case AtrelouInseriu of
        TERPAtrelIns.atrinsNoAtrelNoIns,
        TERPAtrelIns.atrinsNoAtrelButIns,
        TERPAtrelIns.atrinsNoInsButAtrel,
        TERPAtrelIns.atrinsAtrelAndIns:
        begin
          QrDBMQeLnk.First;
          while not QrDBMQeLnk.Eof do
          begin
            case TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) of
              (*15*)TItemTuplePurpose.itpCDRDeleteSync:
              begin
                if not DefineCamposEValoresItem(Tabela, TabIndex, Itens) then
                  Exit;
                // Parei Aqui! Excluir ' + CO_SEL_TAB_VMI + '!
              end;
              {
              (*00*)TItemTuplePurpose.itpIndef,
              (*01*)TItemTuplePurpose.itpCDRIncremSync,
              (*02*)TItemTuplePurpose.itpERPRelatnSync,
              (*03*)TItemTuplePurpose.itpSysRelatnPrDf,
              (*04*)TItemTuplePurpose.itpCDRRelatnSync,
              (*05*)TItemTuplePurpose.itpUsrPrimtivData,
              (*06*)TItemTuplePurpose.itpSysOrCalcData,
              (*07*)TItemTuplePurpose.itpERPSync,
              (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
              (*09*)TItemTuplePurpose.itpERPSrvrIncOver,
              (*10*)TItemTuplePurpose.itpCDRRelSncOrfao,
              (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
              (*12*)TItemTuplePurpose.itpDeprecado,
              (*13*)TItemTuplePurpose.itpInutilizado,
              (*14*)TItemTuplePurpose.itpCDRRelSncMulTab,
              (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync,
              (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
              }
              else begin
                AdicionaAoMemo('Tabela ' + Tabela + '. ' +
                GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
                ' n�o implementado em ' + sProcName);
                Exit;
              end;
            end;
            //
            QrDBMQeLnk.Next;
          end;
          Geral.MB_Info(FCampos[0] + '=' + Geral.FF0(FValores[0]));
{         desmarcar aqui e continuar c�digo!!
          FldIdx := FCamposName[TabIndex][0];
          ValIdx := QrSel.FieldByName(FCamposName[TabIndex][0]).AsString;
          SQL_WHR := 'WHERE ' + FldIdx + '=' + Geral.FF0(ValIdx);
          for I := 1 to High(FCamposName[TabIndex]) (*CamposCount - 1*) do
          begin
            FldIdx := FCamposName[TabIndex][I];
            ValIdx := QrSel.FieldByName(FCamposName[TabIndex][I]).AsString;
            SQL_WHR := SQL_WHR + ' AND ' + FldIdx + '=' + Geral.FF0(ValIdx);
          end;
          if Inseriu and (AllUpAndSyncOnlyIns or NaoInserirJaInseridoEOk) then
          begin
            // Nada! Apenas neutralliza!
            //Cadastro j� existe no servidor e n�o deve ser sobrescrito!
            NeutralizaOrige2(Tabela, SQL_WHR);
            if NaoInserirJaInseridoEOk then
            begin
              if FStrActLig then
              begin
                FStrActios :=  FStrActios + Geral.FF0(ARI[1]);
                for I := 2 to ARI[0] do
                  FStrActios :=  FStrActios + '.' + Geral.FF0(ARI[I]);
                FStrActios :=  FStrActios + ' j� inserido!' + sLineBreak;
              end;
            end;
          end else
          begin
            if ExcluiAtreladoEInserid2(Atrelou, Inseriu, Tabela, TabIndex, ARI) then
              //if InsereRegistro_Arr2(Tabela, TabIndex, QrSel, Campos, Valores) then
              if InsereRegistro_Arr3(Tabela, TabIndex, SQL_WHR, Campos, Valores) then
                  NeutralizaOrige2(Tabela, SQL_WHR);
          end;
}
        end;
        else
        begin
          Result := False;
          AdicionaAoMemo('Tabela ' + Tabela + ' TERPAtrelIns n�o implementado em ' + sProcName);
        end
      end;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + '-x-x-x-x-x-x-x-x-x-x-x-x-x-' + sLineBreak;
      //
      QrSel.Next;
    end;
  end;
  // no final.... se tudo der certo!
  FStrActCnt := FStrActCnt + 1;
  Result := True;
end;

function TFmVSLoadCRCCab2.ExcluiAtreladoEInserid2(const Atrelou: Boolean;
  var Inseriu: Boolean; const Tabela: String; const TabIndex: Integer;
  const ArrResIdx: TArrResIdx): Boolean;
var
  I: Integer;
  SQLDel: String;
begin
  Result := False;
  if Atrelou and Inseriu then
  begin
    SQLDel := 'DELETE FROM ' + Tabela + sLineBreak +
    'WHERE ' + FCamposName[TabIndex][0] + '=' +
    Geral.VariavelToString(ArrResIdx[1]);
    for I := 2 to ArrResIdx[0] do
    begin
      SQLDel := SQLDel + sLineBreak + 'AND ' + FCamposName[TabIndex][I-1] + '=' +
      Geral.VariavelToString(ArrResIdx[I]);
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    SQLDel,
    '']);
    //
    Inseriu := False; // Inserir de novo!
    //
    Result := True;
  end else
    Result := True;
end;

function TFmVSLoadCRCCab2.ExcluiVSMovIts_de_VSMovItz(TabFromRead, TabToDel: String;
  IndiceToDel: Integer; AtrelouInseriuToDel: TERPAtrelIns; QueryFromRead: TmySQLQuery;
  ArrResIdxToDel: TArrResIdx): Boolean;
var
  OriControle, DstControle: Integer;
begin
  DstControle := ArrResIdxToDel[1];
  Result := VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(DstControle,
    Integer(TEstqMotivDel.emtdWetCurti208), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
  if Result then
  begin
    OriControle := QueryFromRead.FieldByName('Controle').AsInteger;
    NeutralizaOrigem(TabFromRead, ['Controle'], [OriControle]);
  end;
end;

procedure TFmVSLoadCRCCab2.ExecutaImportacao();
const
  sProcName = 'FmVSLoadCRCCab2.ExecutaImportacao()';
var
  I, Codigo: Integer;
begin
  try
    Codigo := QrVSLoadCRCCabCodigo.Value;
    PB1.Position := 0;
    PB1.Max := QrItensPorTab.RecordCount * 2;
    PreDefineTabelasCamposETextosSQL();
    //
  { Parei Aqui! Desmarcar quanto pronto!
    AdicionaAoMemo('Desmarcar quanto pronto! "AtrelaItensDaTabela()"');}
    QrItensPorTab.First;
    while not QrItensPorTab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
{
      if (Lowercase(QrItensPorTabNome.Value) = Lowercase('VSMovimTwn'))
      or (Lowercase(QrItensPorTabNome.Value) = Lowercase('MovimTwn'))
      or (Lowercase(QrItensPorTabNome.Value) = Lowercase('VSMovIts')) then
}
      if (Lowercase(QrItensPorTabNome.Value) = Lowercase('VSMovItz')) then
        Geral.MB_Info(QrItensPorTabNome.Value);
      if not AtrelaItensDaTabela(QrItensPorTabNome.Value) then Exit;
      //
      QrItensPorTab.Next;
    end;
    //
  {}
    FStrActios := '';
    //
    QrItensPorTab.First;
    while not QrItensPorTab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      if FStrActLig then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + 'Inserindo na Tabela: ' + QrItensPorTabNome.Value + sLineBreak;
      end;
      //if Lowercase(QrItensPorTabNome.Value) = 'vsmulfrncab' then
        //Geral.MB_Info(QrItensPorTabNome.Value);
      if not InsereItensEmTabela(QrItensPorTabNome.Value) then Exit;
      //
      if FStrActLig and (FStrActCnt > 0) then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + ' Abortado para an�lise de tabela �nica!!!!';
        Exit;
      end;
      QrItensPorTab.Next;
    end;
    //
    //
    QrItensPorTab.First;
    while not QrItensPorTab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      if FStrActLig then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + 'Deletando da Tabela: ' + QrItensPorTabNome.Value + sLineBreak;
      end;
      if not DeletaItensEmTabela(QrItensPorTabNome.Value) then Exit;
      //
      if FStrActLig and (FStrActCnt > 0) then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + ' Abortado para an�lise de tabela �nica!!!!';
        Exit;
      end;
      QrItensPorTab.Next;
    end;
    //
    //
    //...
    //
  finally
    if FStrActios <> '' then
    begin
      MeDescreve.Text := FStrActios;
      PageControl1.ActivePageIndex := 3;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSLoadCRCCab2.FormCreate(Sender: TObject);
begin
  //FCodOnImport := 0;
  ImgTipo.SQLType := stLok;
  PageControl1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  BtImporta.Enabled := False;
  FLstTabs := UnCreateGeral.RecriaTempTableNovo(ntrtt_ItensPorCod,
    DModG.QrUpdPID1, False, 1, '_Lst_Tabs_CRC_Load_');
  //
end;

function TFmVSLoadCRCCab2.InsereRegistro_Arr3(Tabela: String; TabIndex: Integer;
  SQL_WHR: String; CamposDst: array of String; ValoresDst: array of Variant): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.InsereRegistro_Arra3()';
var
  SQL_SEL, SQL_Insert: String;
  I: Integer;
begin
  Result := False;
  if Length(CamposDst) <> Length(ValoresDst) then
  begin
    AdicionaAoMemo(sprocName + sLineBreak + 'Quantidade de itens de campos (' +
    Geral.FF0(Length(CamposDst)) + ') difere de valores(' +
    Geral.FF0(Length(ValoresDst)) + ')');
    Exit;
  end;

  //SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[TabIndex], FldIdx, ValIdx);
  SQL_SEL := FSQLCorda[TabIndex];
  //
  for I := Low(CamposDst) to High(CamposDst) do
  begin
    SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(SQL_SEL, CamposDst[I], ValoresDst[I]);
  end;
  //
  SQL_Insert := Geral.ATS(['INSERT INTO ' + TMeuDB + '.' + LowerCase(Tabela),
    SQL_SEL,
    SQL_WHR, //'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  SubstituiAWStatSincEmSQL(SQL_Insert);
  //
  //Geral.MB_Info(SQL_Insert);
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Insert,
  '']);
end;

procedure TFmVSLoadCRCCab2.InsereDadosTabelaEmIndice(Tabela: String;
  Index: Integer);
var
  CamposCount, I: Integer;
  TabOri, TabDst, TabERP, SQLOri: String;
begin
  if Index = -1 then
  begin
    FTabLstCount := FTabLstCount + 1;
    DefineTamanhosArrayTabelas(FTabLstCount);
    I := FTabLstCount - 1;
  end else
    I := Index;
  //

  TabOri := Tabela;
  TabDst := Tabela;
  if LowerCase(Tabela) = LowerCase('VSMovItz') then
    TabERP := CO_TAB_VMI
  else
    TabERP := Tabela;
  //
  FTabRead[I]       := Tabela;
  FTabWrite[I]      := TabDst;
  FSQLCorda[I]      := '';
  FFldsPriSemERP[I] := '';
  FFldsPriComERP[I] := '';
  FSQLsPriSemERP[I] := '';
  FSQLsPriComERP[I] := '';
  //
  begin
    if not VerificaIdenticidadeDeTabelas(DModG.MySyncDB, TabOri,
    Dmod.MyDB, TabDst, FSQLCorda[I](*SQLDst*), SQLOri) then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIdx, Dmod.MyDB, [
    'SHOW INDEX FROM ' + TabERP,
    'WHERE Key_name="PRIMARY" ',
    '']);
    CamposCount := 0;
    QrIdx.First;
    while not QrIdx.Eof do
    begin
      if FFldsPriSemERP[I] <> '' then
      begin
        FFldsPriSemERP[I] := FFldsPriSemERP[I] + ', ';
        FFldsPriComERP[I] := FFldsPriComERP[I] + ', ';
      end;
      FFldsPriComERP[I] := FFldsPriComERP[I] + CO_ERPSync_FldDst + QrIdx.FieldByName('Column_name').AsString;
      FFldsPriSemERP[I] := FFldsPriSemERP[I] + QrIdx.FieldByName('Column_name').AsString;
      //SQLOriIndex := SQLOriIndex + '=' + //
      //
      CamposCount := CamposCount + 1;
      SetLength(FCamposName[I], CamposCount);
      FCamposName[I][CamposCount - 1] := QrIdx.FieldByName('Column_name').AsString;
      //
      QrIdx.Next;
    end;
    FSQLsPriComERP[I] := Geral.ATS(['SELECT ' + FFldsPriComERP[I],
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + TabERP)]);
    //
    FSQLsPriSemERP[I] := Geral.ATS(['SELECT ' + FFldsPriSemERP[I],
    'FROM ' + Lowercase(TabERP)]);
    //
  end;
end;

function TFmVSLoadCRCCab2.InsereItensEmTabela(Tabela: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.InsereItensEmTabela()';
var
  Atrelou, Inseriu: Boolean;
  ARI: TArrResIdx;
  AtrelouInseriu: TERPAtrelIns;
{
  QtdFldIdx, I, LoadCRCCod: Integer;
  SQLPsqCross, SQLPsqInsertedIts: String;
  //
  Purpose: TItemTuplePurpose;
  CamposOri, CamposDst: array of String;
  ValoresOri, ValoresDst, T: array of Variant;
  Campo: String;
  VT: TVarType;
  //ContinuaBool: Boolean;
  ContinuaQtde: Integer;
}
  TabIndex, QtdFldIdx, QtdFldSyn(*, ItmFldIdx, ItmFldSyn*), Itens: Integer;
  ERPIncOver: Boolean;
  Campo: String;
  Valor: Variant;
(*
  Campos: array of String;
  Valores: array of Variant;
*)
  vt: TVarType;
  //
  SQL_WHR, FldIdx: String;
  ValIdx: Variant;
  I: Integer;
  AllUpAndSyncOnlyIns, NaoInserirJaInseridoEOk: Boolean;
  //
  function DefineCamposEValores(): Boolean;
  begin
    //~~
    Result := DefineCamposEValoresItem(Tabela, TabIndex, Itens);
{~~
    Campo := QrDBMQeLnkKLAskrCol.Value;
    //if Lowercase(Campo) = Lowercase('SrcNivel2') then
      //Geral.MB_Info(Campo);
    Valor := QrSel.FieldByName(QrDBMQeLnkKLAskrCol.Value).AsVariant;
    if FStrActLig then
      FStrActios :=  FStrActios + Tabela + '.' + Campo + '= "' +
      Geral.VariavelToString(Valor) + '" >> ';
    if (VarType(Valor) = 3) then
    begin
      if (Valor = 0) then
      begin
        Result := True;
        if FStrActLig then
          FStrActios :=  FStrActios + '<<' + sLineBreak;
      end else
      begin
        if ObtemValorSubstituto(QrSel, TabIndex, Valor) then
        begin
          Itens := Itens + 1;
          SetLength(Campos, Itens);
          SetLength(Valores, Itens);
          Campos[Itens - 1]  := Campo;
          Valores[Itens - 1] := Valor;
          Result := True;
          //
          if FStrActLig then
            FStrActios :=  FStrActios + sLineBreak;
        end
        else
          Result := False;
      end;
    end else
    begin
      //
      AdicionaAoMemo('Tabela ' + Tabela + '. ' + Campo + ' >> TypeInfo: ' +
        GetEnumName(TypeInfo(TVarType), VarType(Valor)) + ' = ' + String(Valor));
      Exit;
    end;
~~}
  end;
begin
  Result  := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrDBMQeLnk, Dmod.MyDB, [
  'SELECT * ',
  'FROM dbmqeilnk ',
  'WHERE LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
  '']);
  if not (TCRCTableManage(QrDBMQeLnkTbManage.Value) in CO_SeqCRCTableManageInsUpd) then
    Exit;
  AllUpAndSyncOnlyIns := TCRCTableManage(QrDBMQeLnkTbManage.Value) = crctmAllUpAndSyncOnlyIns;
  MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True, 'Inserindo ' + Tabela);
  //
  Inseriu  := False;
  TabIndex := ObtemIndiceDeTabela(Tabela, False);
  if TabIndex >= 0 then
  begin
    QtdFldIdx  := 0;
    QtdFldSyn  := 0;
    ERPIncOver := False;
    ReopenSel(TabIndex, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    //
    QrDBMQeLnk.First;
    while not QrDBMQeLnk.Eof do
    begin
      case TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) of
        // Indices prim�rios com necessidade de comutar no ERP.
        (*01*)TItemTuplePurpose.itpCDRIncremSync,
        (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
        begin
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
        (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
        (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
        begin
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
        (*02*)TItemTuplePurpose.itpERPRelatnSync,
        (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
        begin
        end;
        // Campos com relacionamento e com necessidade de comutar
        (*04*)TItemTuplePurpose.itpCDRRelatnSync,
        (*10*)TItemTuplePurpose.itpCDRRelSncOrfao,
        (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
        begin
          QtdFldSyn := QtdFldSyn + 1;
        end;
        // Outros campos sem necessidade de comutar
        (*05*)TItemTuplePurpose.itpUsrPrimtivData,
        (*06*)TItemTuplePurpose.itpSysOrCalcData,
        (*07*)TItemTuplePurpose.itpERPSync,
        (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
        (*12*)TItemTuplePurpose.itpDeprecado,
        (*13*)TItemTuplePurpose.itpInutilizado,
        (*15*)TItemTuplePurpose.itpCDRDeleteSync:
        begin
          // Nada!
        end;
        // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
        // Somente copiar ao ERP se n�o existir.
        (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
        begin
          ERPIncOver := True;
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // (*00*)TItemTuplePurpose.itpIndef,
        else begin
          AdicionaAoMemo('Tabela ' + Tabela + '. ' +
          GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
          ' n�o implementado em ' + sProcName);
          Exit;
        end;
      end;
      //
      QrDBMQeLnk.Next;
    end;
    if QtdFldIdx = 0 then
    begin
      AdicionaAoMemo('Tabela ' + Tabela + '. Nenhum �ndice prim�rio definido!');
    end;
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
      NaoInserirJaInseridoEOk := False;
      //ItmFldIdx := 0;
      //ItmFldSyn := 0;
      Itens := 0;
      //~~
      //SetLength(Campos, 0);
      //SetLength(Valores, 0);
      SetLength(FCampos, 0);
      SetLength(FValores, 0);
      //~~
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, TabIndex, [], AtrelouInseriu);
      Atrelou := ObtemAtrelou(AtrelouInseriu);
      Inseriu := ObtemInseriu(AtrelouInseriu);
      //
      case AtrelouInseriu of
        TERPAtrelIns.atrinsNoAtrelNoIns,
        TERPAtrelIns.atrinsNoAtrelButIns,
        TERPAtrelIns.atrinsNoInsButAtrel,
        TERPAtrelIns.atrinsAtrelAndIns:
        begin
          QrDBMQeLnk.First;
          while not QrDBMQeLnk.Eof do
          begin
            //if Lowercase(QrDBMQeLnkKLAskrCol.Value) = Lowercase('MovimTwn') then
              //Geral.MB_Info(QrDBMQeLnkKLAskrCol.Value);
            case TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) of
              // Indices prim�rios com necessidade de comutar no ERP.
              (*01*)TItemTuplePurpose.itpCDRIncremSync,
              (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
              (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
              (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
              begin
                NaoInserirJaInseridoEOk := True;
              end;
              // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
              (*02*)TItemTuplePurpose.itpERPRelatnSync,
              (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
              begin
                // Nada
              end;
              // Campos com relacionamento e com necessidade de comutar
              (*04*)TItemTuplePurpose.itpCDRRelatnSync:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              (*10*)TItemTuplePurpose.itpCDRRelSncOrfao:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              // Outros campos sem necessidade de comutar
              (*05*)TItemTuplePurpose.itpUsrPrimtivData,
              (*06*)TItemTuplePurpose.itpSysOrCalcData,
              (*07*)TItemTuplePurpose.itpERPSync,
              (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
              (*12*)TItemTuplePurpose.itpDeprecado,
              (*13*)TItemTuplePurpose.itpInutilizado,
              (*15*)TItemTuplePurpose.itpCDRDeleteSync:
              begin
                // Nada!
              end;
              // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
              // Somente copiar ao ERP se n�o existir.
              (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
              begin
                ERPIncOver := True;
                //
                if not DefineCamposEValores() then
                  Exit;
              end;
              // (*00*)TItemTuplePurpose.itpIndef,
              else begin
                AdicionaAoMemo('Tabela ' + Tabela + '. ' +
                GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkKLPurpose.Value) +
                ' n�o implementado em ' + sProcName);
                Exit;
              end;
            end;
            //
            QrDBMQeLnk.Next;
          end;
          FldIdx := FCamposName[TabIndex][0];
          ValIdx := QrSel.FieldByName(FCamposName[TabIndex][0]).AsString;
          SQL_WHR := 'WHERE ' + FldIdx + '=' + Geral.FF0(ValIdx);
          for I := 1 to High(FCamposName[TabIndex]) (*CamposCount - 1*) do
          begin
            FldIdx := FCamposName[TabIndex][I];
            ValIdx := QrSel.FieldByName(FCamposName[TabIndex][I]).AsString;
            SQL_WHR := SQL_WHR + ' AND ' + FldIdx + '=' + Geral.FF0(ValIdx);
          end;
          if Inseriu and (AllUpAndSyncOnlyIns or NaoInserirJaInseridoEOk) then
          begin
            // Nada! Apenas neutralliza!
            //Cadastro j� existe no servidor e n�o deve ser sobrescrito!
            NeutralizaOrige2(Tabela, SQL_WHR);
            if NaoInserirJaInseridoEOk then
            begin
              if FStrActLig then
              begin
                FStrActios :=  FStrActios + Geral.FF0(ARI[1]);
                for I := 2 to ARI[0] do
                  FStrActios :=  FStrActios + '.' + Geral.FF0(ARI[I]);
                FStrActios :=  FStrActios + ' j� inserido!' + sLineBreak;
              end;
            end;
          end else
          begin
            if ExcluiAtreladoEInserid2(Atrelou, Inseriu, Tabela, TabIndex, ARI) then
              //if InsereRegistro_Arr2(Tabela, TabIndex, QrSel, Campos, Valores) then
//~~              if InsereRegistro_Arr3(Tabela, TabIndex, SQL_WHR, Campos, Valores) then
              if InsereRegistro_Arr3(Tabela, TabIndex, SQL_WHR, FCampos, FValores) then
//~~
                  NeutralizaOrige2(Tabela, SQL_WHR);
          end;
        end;
        else
        begin
          Result := False;
          AdicionaAoMemo('Tabela ' + Tabela + ' TERPAtrelIns n�o implementado em ' + sProcName);
        end
      end;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + '-x-x-x-x-x-x-x-x-x-x-x-x-x-' + sLineBreak;
      //
      QrSel.Next;
    end;
  end;
  // no final.... se tudo der certo!
  FStrActCnt := FStrActCnt + 1;
  Result := True;
end;

procedure TFmVSLoadCRCCab2.InsUpdVSLoadCRCTbs(const NomeTab: String);
var
  SQLType: TSQLType;
  Controle, Registros, Codigo: Integer;
begin
  Controle := 0;
  Codigo := QrVSLoadCRCCabCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqTbs, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrctbs ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND LOWER(Tabela)=LOWER("' + Trim(NomeTab) + '") ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM ' + Lowercase(NomeTab),
  'WHERE LoadCRCCod=' + Geral.FF0(Codigo),
  '']);
  Registros := Dmod.QrAux.FieldByName('ITENS').AsInteger;
  //
  if QrPsqTbs.RecordCount > 0 then
  begin
    Controle := QrPsqTbsControle.Value;
    if Controle = 0 then Exit;
    SQLType := stUpd;
  end;
  if Controle = 0 then
  begin
    SQLType  := stIns;
    Controle := UMyMod.BPGS1I32('vsloadcrctbs', 'Controle', '', '', tsPos, SQLType, 0);
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrctbs', False, [
  'Codigo', 'Tabela', 'Registros'], [
  'Controle'], [
  Codigo, NomeTab, Registros], [
  Controle], True);
end;

procedure TFmVSLoadCRCCab2.ItsImporta1Click(Sender: TObject);
begin
  ExecutaImportacao();
end;

procedure TFmVSLoadCRCCab2.ItsAltera1Click(Sender: TObject);
begin
  MostraVSLoadCRCIts(stUpd);
end;

procedure TFmVSLoadCRCCab2.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSLoadCRCCab2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSLoadCRCCab2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSLoadCRCCab2.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSLoadCRCIts', 'Controle', QrVSLoadCRCItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSLoadCRCIts,
      QrVSLoadCRCItsControle, QrVSLoadCRCItsControle.Value);
    ReopenVSLoadCRCTbs(Controle);
  end;
}
end;

procedure TFmVSLoadCRCCab2.ReopenSel(Indice: Integer; SQLExtra: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MySyncDB, [
  (*SQLDst*)FSQLCorda[Indice],
  'WHERE AWServerID=' + Geral.FF0(QrVSLoadCRCCabOriServrID.Value),
(*
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stUpSinc)) + ') ', //stDwnSync e stUpSinc
*)
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  SQLExtra,
  '']);
end;

procedure TFmVSLoadCRCCab2.ReopenVSLoadCRCTbs(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSLoadCRCTbs, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrctbs ',
  'WHERE Codigo=' + Geral.FF0(QrVSLoadCRCCabCodigo.Value),
  '']);
  //
  QrVSLoadCRCTbs.Locate('Controle', Controle, []);
end;

procedure TFmVSLoadCRCCab2.ReopenVSLoadCRCWrn(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSLoadCRCWrn, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrcwrn ',
  'WHERE Codigo=' + Geral.FF0(QrVSLoadCRCCabCodigo.Value),
  '']);
  //
  QrVSLoadCRCWrn.Locate('Controle', Controle, []);
end;

function TFmVSLoadCRCCab2.DefineCamposEValoresItem(const Tabela: String;
  const TabIndex: Integer; var Itens: Integer): Boolean;
var
  Campo: String;
  Valor: Variant;
begin
  begin
    Campo := QrDBMQeLnkKLAskrCol.Value;
    Valor := QrSel.FieldByName(QrDBMQeLnkKLAskrCol.Value).AsVariant;
    //if Lowercase(Campo) = Lowercase('SrcNivel2') then
      //Geral.MB_Info(Campo);
    //
    if FStrActLig then
      FStrActios :=  FStrActios + Tabela + '.' + Campo + '= "' +
      Geral.VariavelToString(Valor) + '" >> ';
    if (VarType(Valor) = 3) then
    begin
      if (Valor = 0) then
      begin
        Result := True;
        if FStrActLig then
          FStrActios :=  FStrActios + '<<' + sLineBreak;
      end else
      begin
        if ObtemValorSubstituto(QrSel, TabIndex, Valor) then
        begin
          Itens := Itens + 1;
          SetLength(FCampos, Itens);
          SetLength(FValores, Itens);
          FCampos[Itens - 1]  := Campo;
          FValores[Itens - 1] := Valor;
          Result := True;
          //
          if FStrActLig then
            FStrActios :=  FStrActios + sLineBreak;
        end
        else
          Result := False;
      end;
    end else
    begin
      //
      AdicionaAoMemo('Tabela ' + Tabela + '. ' + Campo + ' >> TypeInfo: ' +
        GetEnumName(TypeInfo(TVarType), VarType(Valor)) + ' = ' + String(Valor));
      Exit;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.DefineONomeDoForm;
begin
end;

procedure TFmVSLoadCRCCab2.DefineTamanhosArrayTabelas(K: Integer);
begin
  SetLength(FTabRead, K);
  SetLength(FTabWrite, K);
  SetLength(FSQLCorda, K);
  SetLength(FFldsPriSemERP, K);
  SetLength(FFldsPriComERP, K);
  SetLength(FSQLsPriSemERP, K);
  SetLength(FSQLsPriComERP, K);
  SetLength(FCamposName, K);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSLoadCRCCab2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSLoadCRCCab2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSLoadCRCCab2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSLoadCRCCab2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSLoadCRCCab2.SubstituiAWStatSincEmSQL(var SQL: String);
begin
  SQL := dmkPF.SQLStringReplaceFldByValFld(
    SQL, 'AWStatSinc', Integer(stDwnSinc)(*8*));
//  SQL := Geral.Substitui(SQL, ', AWStatSinc', ', 8 AWStatSinc');
end;

procedure TFmVSLoadCRCCab2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSLoadCRCCab2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSLoadCRCCabCodigo.Value;
  Close;
end;

procedure TFmVSLoadCRCCab2.ItsInclui1Click(Sender: TObject);
begin
  MostraVSLoadCRCIts(stIns);
end;

procedure TFmVSLoadCRCCab2.CabAltera1Click(Sender: TObject);
begin
  //FCodOnImport := 0;
  BtConfirma.Enabled := False;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSLoadCRCCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsloadcrccab');
  //TPData.Date              := QrVSLoadCRCCabDataHora.Value;
  //EdHora.ValueVariant      := QrVSLoadCRCCabDataHora.Value;
  //
  PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value);
end;

procedure TFmVSLoadCRCCab2.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSLoadCRCCab2.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  Codigo, OriServrID, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  OriServrID     := EdOriServrID.ValueVariant;
  if MyObjects.FIC(OriServrID = 0, EdOriServrID,
  'Informe o Server ID de origem!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsloadcrccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrccab', False, [
  'DataHora', 'OriServrID'], [
  'Codigo'], [
  DataHora, OriServrID], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stPsq;
(**)
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
(**)
    //FCodOnImport := Codigo;
    BtImporta.Enabled := True;
    BtConfirma.Enabled := False;
  end;
end;

procedure TFmVSLoadCRCCab2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  //FCodOnImport := 0;
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsloadcrccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsloadcrccab', 'Codigo');
end;

procedure TFmVSLoadCRCCab2.BtItsClick(Sender: TObject);
begin
  FStrActLig := CkDescreve.Checked;
  FStrActCnt := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSLoadCRCCab2.AdicionaAoMemo(Texto: String);
var
  Nome: String;
  Codigo, Controle, Linha, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  FMemoLinesCount := FMemoLinesCount + 1;
  Memo1.Lines.Add(Geral.FF0(FMemoLinesCount) + '. ' + Texto);
  //
  SQLType        := stIns;
  Codigo         := QrVSLoadCRCCabCodigo.Value;
  Controle       := 0;
  Linha          := FMemoLinesCount;
  Nome           := Copy(Texto, 1, 255);
  //
  Controle := UMyMod.BPGS1I32('vsloadcrcwrn', 'Controle', '', '', tsPos, SQLType, Controle);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrcwrn', False, [
  'Codigo', 'Linha', 'Nome'], [
  'Controle'], [
  Codigo, Linha, Nome], [
  Controle], True);
end;

function TFmVSLoadCRCCab2.AtrelaItensDaTabela(Tabela: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.AtrelaItensDaTabela()';
var
  Atrelou, Inseriu: Boolean;
  AtrelouInseriu: TERPAtrelIns;
  QtdFldIdx, I, Indice, LoadCRCCod: Integer;
  SQLPsqCross, SQLPsqInsertedIts: String;
  ARI: TArrResIdx;
  //
  Purpose: TItemTuplePurpose;
  CamposOri, CamposDst: array of String;
  ValoresOri, ValoresDst, T: array of Variant;
  Campo, NTCampo, TabPsq: String;
  VT: TVarType;
  //ContinuaBool: Boolean;
  ContinuaQtde, Ori_Int, Dst_Int, AWServerID, AWStatSinc: Integer;
  Orfao: Variant;
begin
  Result  := True;
  Atrelou := False;
  Inseriu := False;
  Indice := ObtemIndiceDeTabela(Tabela, False);
  if Indice >= 0 then
  begin
    ReopenSel(Indice, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    // Tabela de campos da tabela atual orf�os de tabela m�e:



(*
SELECT KLAskrTab, KLPurpose, COUNT(KLAskrTab) Itens
FROM dbmqeilnk
WHERE KLPurpose IN (1,8,9,15,16,17)
GROUP BY KLAskrTab
vsfchrmpcab tem 3 itens!
*)

    UnDmkDAC_PF.AbreMySQLQuery0(QrItsPorNoTab, Dmod.MyDB, [
    'SELECT * ',
    'FROM dbmqeilnk ',
    //'WHERE KLPurpose=' + Geral.FF0(Integer(itpCDRRelSncOrfao)),
    'WHERE KLPurpose IN (' + CO_ALL_CODS_PRI_IDX_TUPLE + ')',
    'AND LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
    '']);
    //
    if QrItsPorNoTab.RecordCount > 1 then
    begin
      Result := False;
      Geral.MB_Erro('Tabela com mais de um �ndice prim�rio: ' + Tabela +
      sLineBreak + sProcName);
      Exit;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDBMQeLnk, Dmod.MyDB, [
    'SELECT * ',
    'FROM dbmqeilnk ',
    'WHERE LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
    '']);
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Atrelando ' + Tabela);
      //
////////////////////////////////////////////////////////////////////////////////
      QrItsPorNoTab.First;
      while not QrItsPorNoTab.Eof do
      begin
        NTCampo := QrItsPorNoTabKLAskrCol.Value;
        Orfao := QrSel.FieldByName(NTCampo).AsVariant;
        if (VarType(Orfao) = 3) then
        begin
          if Orfao <> 0 then
          begin
            if QrItsPorNoTabTbManage.Value in ([6,7]) then // crctmCRCUpAndSyncDelSelf=6
              TabPsq := QrItsPorNoTabKLRplyTab.Value       // crctmCRCUpAndSyncDelGnrc=7
            else
              TabPsq := QrItsPorNoTabKLAskrTab.Value;
            //
            Ori_Int := Integer(Orfao);
            UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
            'SELECT Dst_' + NTCampo,
            //'FROM erpsync_' + Lowercase(QrItsPorNoTabKLMTbsCol.Value),
            'FROM erpsync_' + Lowercase(TabPsq),
            'WHERE Ori_' + NTCampo + '=' + Geral.FF0(Ori_Int),
            '']);
            //
            if QrOrfaos.FieldByName('Dst_' + NTCampo).AsInteger = 0 then
            begin
              //Geral.MB_Info('Adicionar ao erpsync_' + Lowercase(QrItsPorNoTabKLMTbsCol.Value));
              // Parei aqui
              AWServerID     := QrSel.FieldByName('AWServerID').AsInteger;
              AWStatSinc     := QrSel.FieldByName('AWStatSinc').AsInteger;
              AtrelouInseriu := atrinsNoAtrelNoIns;
              (*Dst_Int := UMyMod.BPGS1I32(QrItsPorNoTabKLAskrTab.Value,
                QrItsPorNoTabKLAskrCol.Value, '', '', tsPos, stIns, 0);*)
              Dst_Int := UMyMod.BPGS1I32(TabPsq, NTCampo, '', '', tsPos, stIns, 0);
              //
              //AtrelaTabelas_Int(QrItsPorNoTabKLMTbsCol.Value,
              AtrelaTabelas_Int(TabPsq,
                QrItsPorNoTabKLAskrCol.Value, Ori_Int, Dst_Int,
                AWServerID, AWStatSinc, AtrelouInseriu);
            end;
          end;
        end else
        begin
          Result := False;
          Geral.MB_Erro('"TypInfo" n�o implementado em ' + sProcName);
          Exit;
        end;
        //
        QrItsPorNoTab.Next;
      end;
////////////////////////////////////////////////////////////////////////////////
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, Indice, [], AtrelouInseriu);
      QtdFldIdx := Length(FCamposName[Indice]);
      //
      case AtrelouInseriu of
        TERPAtrelIns.atrinsNoAtrelNoIns,
        TERPAtrelIns.atrinsNoAtrelButIns:
        begin
          SetLength(CamposOri, QtdFldIdx);
          SetLength(CamposDst, QtdFldIdx);
          SetLength(ValoresOri, QtdFldIdx);
          SetLength(ValoresDst, QtdFldIdx);
          ContinuaQtde := 0;
          for I := 0 to QtdFldIdx -1 do
          begin
            //ContinuaBool := False;
            Campo := FCamposName[Indice][I];
            CamposOri[I] := 'Ori_' + Campo;
            CamposDst[I] := 'Dst_' + Campo;
            //
            ValoresOri[I] := QrSel.FieldByName(Campo).AsVariant;
            //ValoresDst[I] := Null;
            (*
            VT := VarType(ValoresOri[I]);
            if VT <> vtInteger then
            begin
              Geral.MB_Erro('Valor difere de integer em ' + sProcName);
              Result := False;
              Exit;
            end;
            *)
            if QrDBMQeLnk.Locate('KLAskrCol', Campo, [loCaseInsensitive]) then
            begin
              if QrDBMQeLnkKLPurpose.Value <> QrDBMQeLnkTbPurpose.Value then
              begin
                // 15 e 01
                if (TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) =
                TItemTuplePurpose.itpCDRDeleteSync)
                and (TItemTuplePurpose(QrDBMQeLnkTbPurpose.Value) =
                TItemTuplePurpose.itpCDRIncremSync) then
                begin
                  // Nada!. Primary que ser� exclu�do! n�o precisa atrelar!
                end else
                  AdicionaAoMemo(sprocName + ' KLPurpose = ' +
                  Geral.FF0(QrDBMQeLnkKLPurpose.Value) + ' difere de TbPurpose = '
                  + Geral.FF0(QrDBMQeLnkTbPurpose.Value));
              end else
              begin
                //ContinuaBool := True;
                ContinuaQtde := ContinuaQtde + 1;
(*
              end;
              if ContinuaBool then
              begin
*)
                Purpose := TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value);
                if (Purpose in CO_PrimaryIncremenPurpose) then
                  ValoresDst[I] := UMyMod.BPGS1I32(Tabela, FCamposName[Indice][I], '', '', tsPos, stIns, 0)
                else
                  ValoresDst[I] := ValoresOri[I];
              end;
            end;
          end;
          if ContinuaQtde > 0 then
          begin
            //Mais := QtdFldIdx + 4;
            SetLength(CamposOri, QtdFldIdx + 4);
            SetLength(ValoresOri, QtdFldIdx + 4);
            //
            CamposOri[QtdFldIdx + 0] := 'AWServerID';
            ValoresOri[QtdFldIdx + 0] := QrSel.FieldByName('AWServerID').AsInteger;
            //
            CamposOri[QtdFldIdx + 1] := 'AWStatSinc';
            ValoresOri[QtdFldIdx + 1] := QrSel.FieldByName('AWStatSinc').AsInteger;
            //
            CamposOri[QtdFldIdx + 2] := 'AtrelIns';
            ValoresOri[QtdFldIdx + 2] := Integer(AtrelouInseriu);
            //
            CamposOri[QtdFldIdx + 3] := 'LoadCRCCod';
            ValoresOri[QtdFldIdx + 3] := QrVSLoadCRCCabCodigo.Value;
            //
            Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'erpsync_' + LowerCase(Tabela), False,
            CamposOri, CamposDst, ValoresOri, ValoresDst, False);
          end;
        end;
        TERPAtrelIns.atrinsNoInsButAtrel,
        TERPAtrelIns.atrinsAtrelAndIns: ; // Nada
        else
        begin
          Result := False;
          AdicionaAoMemo('Tabela ' + Tabela + ' TERPAtrelIns n�o implementado em ' + sProcName);
        end
      end;
      //
      QrSel.Next;
    end;
  end;
  // no final.... se tudo der certo!
  Result := True;
end;

function  TFmVSLoadCRCCab2.AtrelaTabelas_Codigo(Tabela: String; Ori_Codigo,
  Dst_Codigo, AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
var
  SQLType: TSQLType;
  LoadCRCCod: Integer;
begin
  Result         := False;
  SQLType        := stIns;
  LoadCRCCod     := QrVSLoadCRCCabCodigo.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'erpsync_' + LowerCase(Tabela), False, [
  'Ori_Codigo', 'AWServerID', 'AWStatSinc',
  'AtrelIns', 'LoadCRCCod'], [
  'Dst_Codigo'], [
  Ori_Codigo, AWServerID, AWStatSinc,
  AtrelIns, LoadCRCCod], [
  Dst_Codigo], False);
  if Result then
  begin
    //?
  end;
end;

function  TFmVSLoadCRCCab2.AtrelaTabelas_Int(Tabela, Campo: String; Ori_Int,
  Dst_Int, AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
var
  SQLType: TSQLType;
  LoadCRCCod: Integer;
begin
  Result         := False;
  SQLType        := stIns;
  LoadCRCCod     := QrVSLoadCRCCabCodigo.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'erpsync_' + LowerCase(Tabela), False, [
  'Ori_' + Campo, 'AWServerID', 'AWStatSinc',
  'AtrelIns', 'LoadCRCCod'], [
  'Dst_' + Campo], [
  Ori_Int, AWServerID, AWStatSinc,
  AtrelIns, LoadCRCCod], [
  Dst_Int], False);
  if Result then
  begin
    //?
  end;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabAfterOpen(DataSet: TDataSet);
begin
  if QrItensPorTab.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabAfterScroll(DataSet: TDataSet);
begin
  QrSel.Close;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabBeforeClose(DataSet: TDataSet);
begin
  QrSel.Close;
end;

procedure TFmVSLoadCRCCab2.QrItReInnAfterOpen(DataSet: TDataSet);
begin
  DBGReInn.Visible := True;
end;

procedure TFmVSLoadCRCCab2.QrItReInnBeforeClose(DataSet: TDataSet);
begin
  DBGReInn.Visible := False;
end;

procedure TFmVSLoadCRCCab2.QrSelAfterScroll(DataSet: TDataSet);
begin
  QrItReInn.Close;
end;

procedure TFmVSLoadCRCCab2.QrSelBeforeClose(DataSet: TDataSet);
begin
  QrItReInn.Close;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSLoadCRCTbs(0);
  ReopenVSLoadCRCWrn(0);
end;

procedure TFmVSLoadCRCCab2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSLoadCRCCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSLoadCRCCab2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSLoadCRCCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsloadcrccab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSLoadCRCCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSLoadCRCCab2.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
  ServerID: Integer;
  vSrvrID: Variant;
begin
  //FCodOnImport := 0;
  ServerID := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, ServerID, 0, 0,
  '1', '', True, 'Server ID', 'Informe o ID do Servidor ClareCo: ', 0, vSrvrID)
  then begin
    ServerID := vSrvrID;
    if ServerID > 0 then
    begin
      if PesquisaNovosDadosAImportar(ServerID) then
      begin
        UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSLoadCRCCab, [PnDados],
        [PnEdita], TPData, ImgTipo, 'vsloadcrccab');
        //
        EdOriServrID.ValueVariant := ServerID;
        Agora := DModG.ObtemAgora();
        TPData.Date         := Agora;
        EdHora.ValueVariant := Agora;
      end;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.Corrigir011Click(Sender: TObject);
var
  Tabela: String;
  Qry: TmySQLQuery;
begin
  Tabela := '...';
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Verificando altera��es na tabela' + Tabela);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW TABLES LIKE "erpsync_%"',
    '']);
    PB2.Position := 0;
    PB2.Max      := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      Tabela := Qry.Fields[0].AsString;
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Verificando altera��es na tabela' + Tabela);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + Tabela,
      'SET LoadCRCCod=1 ',
      'WHERE LoadCRCCod=0 ',
      '']);
      //
      InsUpdVSLoadCRCTbs(Tabela);
      //
      Qry.Next;
    end;
  except
    Qry.Free;
  end;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabBeforeClose(
  DataSet: TDataSet);
begin
  QrItensPorTab.Close;
  QrVSLoadCRCTbs.Close;
  QrVSLoadCRCWrn.Close;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSLoadCRCCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCWrnAfterOpen(DataSet: TDataSet);
begin
  if (QrVSLoadCRCWrn.RecordCount > 0)
  and (
    (QrItensPorTab.State = dsInactive)
    or
    (QrItensPorTab.RecordCount = 0)
  ) then
    PageControl1.ActivePageIndex := 2;
end;

function TFmVSLoadCRCCab2.QtdIndicesARI(ARI: TArrResIdx): Integer;
begin
  Result := Integer(ARI[0]);
end;

(*

/*DModG.QrUpdSync*/
/*********** SQL ***********/
INSERT INTO ' + TMeuDB + '' + CO_SEL_TAB_VMI + '
SELECT
 847 Codigo
, 11041 Controle
, 2461 MovimCod
, MovimNiv
, MovimTwn
, Empresa
, Terceiro
, CliVenda
, MovimID
, LnkIDXtr
, LnkNivXtr1
, LnkNivXtr2
, DataHora
, Pallet
, GraGruX
, Pecas
, PesoKg
, AreaM2
, AreaP2
, ValorT
, SrcMovID
, 146 SrcNivel1
, Null SrcNivel2
, SrcGGX
, SdoVrtPeca
, SdoVrtPeso
, SdoVrtArM2
, Observ
, SerieFch
, Ficha
, Misturou
, FornecMO
, CustoMOKg
, CustoMOTot
, ValorMP
, DstMovID
, DstNivel1
, DstNivel2
, DstGGX
, QtdGerPeca
, QtdGerPeso
, QtdGerArM2
, QtdGerArP2
, QtdAntPeca
, QtdAntPeso
, QtdAntArM2
, QtdAntArP2
, AptoUso
, NotaMPAG
, Marca
, TpCalcAuto
, Zerado
, EmFluxo
, NotFluxo
, FatNotaVNC
, FatNotaVRC
, PedItsLib
, PedItsFin
, PedItsVda
, Lk
, DataCad
, DataAlt
, UserCad
, UserAlt
, AlterWeb
, Ativo
, GSPInnNiv2
, GSPArtNiv2
, CustoMOM2
, ReqMovEstq
, StqCenLoc
, ItemNFe
, VSMorCab
, VSMulFrnCab
, ClientMO
, CustoPQ
, KgCouPQ
, NFeSer
, NFeNum
, VSMulNFeCab
, GGXRcl
, JmpMovID
, JmpNivel1
, JmpNivel2
, JmpGGX
, RmsMovID
, RmsNivel1
, RmsNivel2
, RmsGGX
, GSPSrcMovID
, GSPSrcNiv2
, GSPJmpMovID
, GSPJmpNiv2
, DtCorrApo
, MovCodPai
, IxxMovIX
, IxxFolha
, IxxLinha
, AWServerID
, 8 AWStatSinc

FROM ' + CO_SEL_TAB_VMI + '

WHERE Controle=1361



/********* FIM SQL *********/

/*****Query sem parametros*******/


MySQL Error Code: (1048)
Column 'SrcNivel2' cannot be null

' + CO_SEL_TAB_VMI + '.SrcNivel2= "1357" >>

*)

end.

