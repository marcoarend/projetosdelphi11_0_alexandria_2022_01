unit VSESCCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, AppListas, UnAppEnums;

type
  TFmVSESCCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSESCCab: TmySQLQuery;
    DsVSESCCab: TDataSource;
    QrVSESCIts: TmySQLQuery;
    DsVSESCIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSESCCabCodigo: TIntegerField;
    QrVSESCCabMovimCod: TIntegerField;
    QrVSESCCabEmpresa: TIntegerField;
    QrVSESCCabDataHora: TDateTimeField;
    QrVSESCCabPecas: TFloatField;
    QrVSESCCabPesoKg: TFloatField;
    QrVSESCCabAreaM2: TFloatField;
    QrVSESCCabAreaP2: TFloatField;
    QrVSESCCabLk: TIntegerField;
    QrVSESCCabDataCad: TDateField;
    QrVSESCCabDataAlt: TDateField;
    QrVSESCCabUserCad: TIntegerField;
    QrVSESCCabUserAlt: TIntegerField;
    QrVSESCCabAlterWeb: TSmallintField;
    QrVSESCCabAtivo: TSmallintField;
    QrVSESCCabNO_EMPRESA: TWideStringField;
    QrVSESCItsPecas: TFloatField;
    QrVSESCItsPesoKg: TFloatField;
    QrVSESCItsAreaM2: TFloatField;
    QrVSESCItsAreaP2: TFloatField;
    QrVSESCItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    QrVSESCItsNO_PALLET: TWideStringField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    frxWET_RECUR_010_01: TfrxReport;
    frxDsVSESCIts: TfrxDBDataset;
    frxDsVSESCCab: TfrxDBDataset;
    QrVSESCItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaPecas: TFloatField;
    QrPalletClaAreaM2: TFloatField;
    QrPalletClaAreaP2: TFloatField;
    QrPalletClaPesoKg: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxWET_RECUR_010_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrVSESCItsNO_FORNECE: TWideStringField;
    QrVSESCItsDataHora: TDateTimeField;
    QrVSESCItsValorT: TFloatField;
    QrVSESCItsSdoVrtPeca: TFloatField;
    QrVSESCItsSdoVrtArM2: TFloatField;
    QrVSESCItsSdoVrtPeso: TFloatField;
    QrVSESCItsCustoMOKg: TFloatField;
    QrVSESCItsCustoMOTot: TFloatField;
    QrVSESCItsValorMP: TFloatField;
    QrVSESCItsQtdGerPeca: TFloatField;
    QrVSESCItsQtdGerPeso: TFloatField;
    QrVSESCItsQtdGerArM2: TFloatField;
    QrVSESCItsQtdGerArP2: TFloatField;
    QrVSESCItsQtdAntPeca: TFloatField;
    QrVSESCItsQtdAntPeso: TFloatField;
    QrVSESCItsQtdAntArM2: TFloatField;
    QrVSESCItsQtdAntArP2: TFloatField;
    QrVSESCItsNotaMPAG: TFloatField;
    QrVSESCItsCustoMOM2: TFloatField;
    QrVSESCItsCUS_PesoKg: TFloatField;
    QrVSESCItsCUS_AreaM2: TFloatField;
    QrVSESCItsCUS_AreaP2: TFloatField;
    QrVSESCItsCUS_Peca: TFloatField;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSESCCabTemIMEIMrt: TSmallintField;
    QrVSESCItsFornecMO: TLargeintField;
    QrVSESCItsNO_TTW: TWideStringField;
    QrVSESCItsCodigo: TLargeintField;
    QrVSESCItsControle: TLargeintField;
    QrVSESCItsMovimCod: TLargeintField;
    QrVSESCItsMovimNiv: TLargeintField;
    QrVSESCItsMovimTwn: TLargeintField;
    QrVSESCItsEmpresa: TLargeintField;
    QrVSESCItsTerceiro: TLargeintField;
    QrVSESCItsCliVenda: TLargeintField;
    QrVSESCItsMovimID: TLargeintField;
    QrVSESCItsPallet: TLargeintField;
    QrVSESCItsGraGruX: TLargeintField;
    QrVSESCItsSrcMovID: TLargeintField;
    QrVSESCItsSrcNivel1: TLargeintField;
    QrVSESCItsSrcNivel2: TLargeintField;
    QrVSESCItsSrcGGX: TLargeintField;
    QrVSESCItsSerieFch: TLargeintField;
    QrVSESCItsFicha: TLargeintField;
    QrVSESCItsMisturou: TLargeintField;
    QrVSESCItsDstMovID: TLargeintField;
    QrVSESCItsDstNivel1: TLargeintField;
    QrVSESCItsDstNivel2: TLargeintField;
    QrVSESCItsDstGGX: TLargeintField;
    QrVSESCItsID_TTW: TLargeintField;
    QrVSESCItsStqCenLoc: TLargeintField;
    QrVSESCItsReqMovEstq: TLargeintField;
    QrVSESCCabClienteMO: TIntegerField;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    QrVSESCCabNO_CLIENTEMO: TWideStringField;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrVSESCItsIxxMovIX: TLargeintField;
    QrVSESCItsIxxFolha: TLargeintField;
    QrVSESCItsIxxLinha: TLargeintField;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    Edemi_serie: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    Edemi_nNF: TdmkEdit;
    DBEdit23: TDBEdit;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit26: TDBEdit;
    QrVSESCCabide_nNF: TIntegerField;
    QrVSESCCabide_serie: TSmallintField;
    QrVSESCCabemi_serie: TIntegerField;
    QrVSESCCabemi_nNF: TIntegerField;
    QrMovIDAsc: TmySQLQuery;
    IntegerField2: TIntegerField;
    DsMovIDAsc: TDataSource;
    QrMovIDAscNO_MovimID: TWideStringField;
    EdMovIDAsc: TdmkEditCB;
    Label4: TLabel;
    CBMovIDAsc: TdmkDBLookupComboBox;
    QrVSESCCabMovIDAsc: TIntegerField;
    QrVSESCCabNO_MovIDAsc: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSESCCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSESCCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSESCCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSESCCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_RECUR_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSESCIts(SQLType: TSQLType);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSESCIts(Controle: Integer);
    procedure ReopenMovIDAsc();
    procedure AtualizaNFeItens();

  end;

var
  FmVSESCCab: TFmVSESCCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSESCIts, ModuleGeral,
  Principal, UnVS_CRC_PF, CreateVS, ModVS_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSESCCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSESCCab.MostraFormVSESCIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSESCIts, FmVSESCIts, afmoNegarComAviso) then
  begin
    FmVSESCIts.ImgTipo.SQLType := SQLType;
    FmVSESCIts.FQrCab := QrVSESCCAB;
    FmVSESCIts.FDsCab := DsVSESCCAB;
    FmVSESCIts.FQrIts := QrVSESCIts;
    FmVSESCIts.FDataHora := QrVSESCCABDataHora.Value;
    FmVSESCIts.FEmpresa  := QrVSESCCABEmpresa.Value;
    FmVSESCIts.FClientMO := QrVSESCCABClienteMO.Value;
    if SQLType = stIns then
    begin
      //FmVSESCIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSESCIts.EdControle.ValueVariant := QrVSESCItsControle.Value;
      //
      FmVSESCIts.EdGragruX.ValueVariant    := QrVSESCItsGraGruX.Value;
      FmVSESCIts.CBGragruX.KeyValue        := QrVSESCItsGraGruX.Value;
      FmVSESCIts.EdFornecedor.ValueVariant := QrVSESCItsTerceiro.Value;
      FmVSESCIts.CBFornecedor.KeyValue     := QrVSESCItsTerceiro.Value;
      FmVSESCIts.EdSerieFch.ValueVariant   := QrVSESCItsSerieFch.Value;
      FmVSESCIts.CBSerieFch.KeyValue       := QrVSESCItsSerieFch.Value;
      FmVSESCIts.EdFicha.ValueVariant      := QrVSESCItsFicha.Value;
      FmVSESCIts.EdPallet.ValueVariant     := QrVSESCItsPallet.Value;
      FmVSESCIts.CBPallet.KeyValue         := QrVSESCItsPallet.Value;
      FmVSESCIts.EdPecas.ValueVariant      := QrVSESCItsPecas.Value;
      FmVSESCIts.EdPesoKg.ValueVariant     := QrVSESCItsPesoKg.Value;
      FmVSESCIts.EdAreaM2.ValueVariant     := QrVSESCItsAreaM2.Value;
      FmVSESCIts.EdAreaP2.ValueVariant     := QrVSESCItsAreaP2.Value;
      FmVSESCIts.EdValorMP.ValueVariant    := QrVSESCItsValorMP.Value;
      FmVSESCIts.EdCustoMOKg.ValueVariant  := QrVSESCItsCustoMOKg.Value;
      FmVSESCIts.EdCustoMOM2.ValueVariant  := QrVSESCItsCustoMOM2.Value;
      FmVSESCIts.EdFornecMO.ValueVariant   := QrVSESCItsFornecMO.Value;
      FmVSESCIts.CBFornecMO.KeyValue       := QrVSESCItsFornecMO.Value;
      FmVSESCIts.EdObserv.ValueVariant     := QrVSESCItsObserv.Value;
      FmVSESCIts.EdStqCenLoc.ValueVariant  := QrVSESCItsStqCenLoc.Value;
      FmVSESCIts.CBStqCenLoc.KeyValue      := QrVSESCItsStqCenLoc.Value;
      FmVSESCIts.EdReqMovEstq.ValueVariant := QrVSESCItsReqMovEstq.Value;
      FmVSESCIts.RGIxxMovIX.ItemIndex      := QrVSESCItsIxxMovIX.Value;
      FmVSESCIts.EdIxxFolha.ValueVariant   := QrVSESCItsIxxFolha.Value;
      FmVSESCIts.EdIxxLinha.ValueVariant   := QrVSESCItsIxxLinha.Value;
      //
      if QrVSESCItsDstNivel2.Value <> 0 then
      begin
        FmVSESCIts.EdNivel2.ValueVariant     := QrVSESCItsDstNivel2.Value;
        FmVSESCIts.CBNivel2.KeyValue         := QrVSESCItsDstNivel2.Value;
      end else
      begin
        FmVSESCIts.EdNivel2.ValueVariant     := QrVSESCItsSrcNivel2.Value;
        FmVSESCIts.CBNivel2.KeyValue         := QrVSESCItsSrcNivel2.Value;
      end;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSESCIts.EdCustoMOTot.ValueVariant := QrVSESCItsCustoMOTot.Value;
      FmVSESCIts.EdValorT.ValueVariant     := QrVSESCItsValorT.Value;
    end;
    FmVSESCIts.ShowModal;
    FmVSESCIts.Destroy;
  end;
end;

procedure TFmVSESCCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSESCCAB);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSESCCAB, QrVSESCIts);
end;

procedure TFmVSESCCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSESCCAB);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSESCIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSESCIts);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSESCItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSESCCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSESCCABCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSESCCab.DefParams;
var
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('mid.Codigo', 'NO_MovIDAsc', pvNO, True,
    sEstqMovimIDESC);
  //
  VAR_GOTOTABELA := 'vsesccab';
  VAR_GOTOMYSQLTABLE := QrVSESCCAB;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_CLIENTEMO, ');
  VAR_SQLx.Add(ATT_MovimID);
  VAR_SQLx.Add('FROM vsesccab wic');
  VAR_SQLx.Add('LEFT JOIN vsmovimid mid ON mid.Codigo=wic.MovIDAsc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cmo ON cmo.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  VAR_SQLx.Add('AND wic.Codigo=:P0');  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSESCCab.Estoque1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSESCCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSESCCABEmpresa.Value;
  N := 0;
  QrVSESCIts.First;
  while not QrVSESCIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSESCItsPallet.Value;
    //
    QrVSESCIts.Next;
  end;
  if N > 0 then
    VS_CRC_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSESCCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSESCIts(stUpd);
end;

procedure TFmVSESCCab.CabExclui1Click(Sender: TObject);
begin
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
  //////  Fazer exclus�o de mentira!!!!!!!!!!!!!!!!!!!!!!!!!!
end;

procedure TFmVSESCCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSESCCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSESCCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSESCItsCodigo.Value;
  MovimCod := QrVSESCItsMovimCod.Value;
  //
  if VS_CRC_PF.ExcluiControleVSMovIts(QrVSESCIts, TIntegerField(QrVSESCItsMovimCod),
  QrVSESCItsControle.Value, CtrlBaix, QrVSESCItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti023)) then
  begin
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsesccab', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSESCCab.ReopenMovIDAsc();
var
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('Codigo', 'NO_MovimID', pvNO, True,
    sEstqMovimIDESC);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovIDAsc, Dmod.MyDB, [
  'SELECT mid.Codigo, ',
  ATT_MovimID,
  'FROM vsmovimid mid ',
  'ORDER BY NO_MovimID ',
  '']);
end;

procedure TFmVSESCCab.ReopenVSESCIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSESCCABTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.PesoKg=0, 0, vmi.ValorT / vmi.PesoKg) CUS_PesoKg, ',
  'IF(vmi.AreaM2=0, 0, vmi.ValorT / vmi.AreaM2) CUS_AreaM2, ',
  'IF(vmi.AreaP2=0, 0, vmi.ValorT / vmi.AreaP2) CUS_AreaP2, ',
  'IF(vmi.Pecas=0, 0, vmi.ValorT / vmi.Pecas) CUS_Peca',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSESCCABMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSESCIts, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSESCIts);
  //
  QrVSESCIts.Locate('Controle', Controle, []);
end;


procedure TFmVSESCCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSESCCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSESCCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSESCCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSESCCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSESCCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSESCCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSESCCABCodigo.Value;
  Close;
end;

procedure TFmVSESCCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSESCIts(stIns);
end;

procedure TFmVSESCCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSESCCAB, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsesccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSESCCABEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSESCCab.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  ide_Serie, ide_nNF, emi_Serie, emi_nNF,
  Codigo, MovimCod, Empresa, ClienteMO, Terceiro, MovIDAsc: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
begin
 Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  ClienteMO      := EdClienteMO.ValueVariant;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  ide_Serie      := Edide_Serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_Serie      := Edemi_Serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
  //
  MovIDAsc       := EdMovIDAsc.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina uma data de compra!') then Exit;
  if MyObjects.FIC(MovIDAsc = 0, EdMovIDAsc, 'Informe "O que aconteceu para dar entrada do couro por esta janela"') then Exit;
  if MyObjects.FIC(CBMovIDAsc.Text = 'zzz??????', EdMovIDAsc, '"O que aconteceu para dar entrada do couro por esta janela" inv�lido') then Exit;

  //
  Codigo := UMyMod.BPGS1I32('vsesccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsesccab', False, [
  'MovimCod', 'Empresa', CO_DATA_HORA_GRL,
  'ClienteMO', 'ide_Serie', 'ide_nNF',
  'emi_Serie', 'emi_nNF', 'MovIDAsc'

  (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',*)], [
  'Codigo'], [
  MovimCod, Empresa, DataHora,
  ClienteMO, ide_Serie, ide_nNF,
  emi_Serie, emi_nNF, MovIDAsc
  (*, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidInnSemCob, Codigo)
    else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_VMI, 'ClientMO'
      ], ['MovimCod'], [
      Empresa, DataHora, ClienteMO
      ], [MovimCod], True);
    end;
    //
    if Edide_nNF.ValueVariant <> 0 then
      AtualizaNFeItens();
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //
    MostraFormVSESCIts(stIns);
    //
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSESCCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsesccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsesccab', 'Codigo');
end;

procedure TFmVSESCCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSESCCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSESCCabMovimCod.Value, TEstqMovimID.emidInnSemCob, [(**)], [eminSemNiv]);
end;

procedure TFmVSESCCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSESCCAB, QrVSESCCABDataHora.Value,
  BtCab, PMCab, [CabInclui1]);
end;

procedure TFmVSESCCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  ReopenMovIDAsc();
end;

procedure TFmVSESCCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSESCCABCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSESCCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSESCCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSESCCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSESCCABCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE ' + CO_UPD_TAB_VMI,
  'SET MovimID=' + Geral.FF0(Integer(emidInnSemCob)),
  'WHERE MovimID=' + Geral.FF0(Integer(emidInnSemCob)),
  'AND MovimCod IN ( ',
  '     SELECT MovimCod ',
  '     FROM vsesccab ',
  ') ',
  '']));
  Geral.MB_Aviso('Atualiza��o 0 > 13 OK!');
end;

procedure TFmVSESCCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSESCCab.QrVSESCCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSESCCab.QrVSESCCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSESCIts(0);
end;

procedure TFmVSESCCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSESCCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSESCCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSESCCABCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSESCCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSESCCABCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsesccab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSESCCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSESCCab.frxWET_RECUR_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSESCCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSESCCAB, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsesccab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSESCCab.Classificao1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxWET_RECUR_010_01, 'Ajuste estoque Couros');
end;

procedure TFmVSESCCab.QrVSESCCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSESCIts.Close;
end;

procedure TFmVSESCCab.QrVSESCCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSESCCABCodigo.DisplayFormat := FFormatFloat;
end;

end.

