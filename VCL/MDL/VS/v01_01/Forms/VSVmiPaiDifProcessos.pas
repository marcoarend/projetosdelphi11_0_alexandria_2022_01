unit VSVmiPaiDifProcessos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts,
  AppListas;

type
  THackDBGrid = class(TDBGrid);
  TFmVSVmiPaiDifProcessos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    PnPesquisa: TPanel;
    BtTeste2: TBitBtn;
    CkIMERT: TCheckBox;
    QrInn: TMySQLQuery;
    QrInnControle: TIntegerField;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSVmiPaiDifProcessos: TFmVSVmiPaiDifProcessos;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSVmiPaiDifProcessos.BtOKClick(Sender: TObject);
var
  sControle, sVmiPai, Tabela: String;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrVmiPaiDifProcessos.First;
  PB1.Position := 0;
  PB1.Max := DmModVS.QrVmiPaiDifProcessos.RecordCount;
  while not DmModVS.QrVmiPaiDifProcessos.Eof do
  begin
    (*
    if DmModVS.QrVmiPaiDifProcessosControle.Value = 18411 then
      Geral.MB_Info('18411');
    *)
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if DmModVS.QrVmiPaiDifProcessosMovCodPai.Value <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrInn, Dmod.MyDB, [
      'SELECT Controle  ',
      'FROM vsmovits ',
      'WHERE MovimNiv IN (' + CO_ALL_CODS_NIV_INN_PROCESSO_VS + ')', //8,21,30,35,40,45,50,55,62,66
      'AND MovimCod=' + Geral.FF0(DmModVS.QrVmiPaiDifProcessosMovCodPai.Value),
      '']);
    end else
    begin
      MovimID := TEstqMovimID(DmModVS.QrVmiPaiDifProcessosMovimID.Value);
      MovimNiv := VS_PF.ObtemMovimNivInnDeMovimID(MovimID);
      UnDmkDAC_PF.AbreMySQLQuery0(QrInn, Dmod.MyDB, [
      'SELECT Controle  ',
      'FROM vsmovits ',
      //'WHERE MovimID=' + Geral.FF0(DmModVS.QrVmiPaiDifProcessosMovimID.Value),
      //'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
      'WHERE MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
      'AND MovimCod=' + Geral.FF0(DmModVS.QrVmiPaiDifProcessosMovimCod.Value),
      '']);
    end;
    if QrInn.RecordCount = 1 then
    begin
      case DmModVS.QrVmiPaiDifProcessosID_TTW.Value of
        0: Tabela := CO_TAB_VMI;
        1: Tabela := CO_TAB_VMB;
        else Tabela := '???';
      end;
      sControle := Geral.FF0(DmModVS.QrVmiPaiDifProcessosControle.Value);
      sVmiPai   := Geral.FF0(QrInnControle.Value);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE ' + Tabela + ' SET ' +
      ' VmiPai = ' + sVmiPai +
      ' WHERE Controle=' + sControle);
      //
    end else
    if QrInn.RecordCount > 1 then
    begin
      Geral.MB_Erro('IME-C ' +
      Geral.FF0(DmModVS.QrVmiPaiDifProcessosMovimCod.Value) +
      ' com mais de um item de entrada!' + sLineBreak +
      'Corre��o n�o ser� realizada');
    end else
      Geral.MB_Erro('IME-C ' +
      Geral.FF0(DmModVS.QrVmiPaiDifProcessosMovimCod.Value) +
      ' com sem nenhum item de entrada!' + sLineBreak +
      'Corre��o n�o ser� realizada');

    DmModVS.QrVmiPaiDifProcessos.Next;
  end;
  //DmModVS.QrVmiPaiDifProcessos.Close;
  //DmModVS.QrVmiPaiDifProcessos.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSVmiPaiDifProcessos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSVmiPaiDifProcessos.BtTeste2Click(Sender: TObject);
const
  ForcaMostrarForm = False;
  SelfCall = True;
var
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := Geral.BoolToInt(CkIMERT.Checked);
  DmModVS.VmiPaiDifProcessos(ForcaMostrarForm,
    SelfCall, TemIMEiMrt, LaAviso1, LaAviso2);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foram encontrados ' +
  Geral.FF0(DmModVS.QrVmiPaiDifProcessos.RecordCount) +
  ' VmiPai em IMEIS de baixa de processos zerados!');
end;

procedure TFmVSVmiPaiDifProcessos.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrVmiPaiDifProcessosControle.Value)
  else
  if Campo = 'SrcCtrl' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrVmiPaiDifProcessosSrcNivel2.Value);
end;

procedure TFmVSVmiPaiDifProcessos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSVmiPaiDifProcessos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSVmiPaiDifProcessos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
