unit VSCalSubPrdItsMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls, dmkEditDateTimePicker, UnAppEnums, Vcl.Menus;

type
  TFmVSCalSubPrdItsMul = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdValorT: TdmkEdit;
    Label7: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    EdCustoMOTot: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    EdObserv: TdmkEdit;
    Label16: TLabel;
    EdValorMP: TdmkEdit;
    Label8: TLabel;
    Label12: TLabel;
    EdCustoMOKg: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel7: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    DGDadosOri: TDBGrid;
    QrVSCalOriIMEI: TmySQLQuery;
    QrVSCalOriIMEICodigo: TLargeintField;
    QrVSCalOriIMEIControle: TLargeintField;
    QrVSCalOriIMEIMovimCod: TLargeintField;
    QrVSCalOriIMEIMovimNiv: TLargeintField;
    QrVSCalOriIMEIMovimTwn: TLargeintField;
    QrVSCalOriIMEIEmpresa: TLargeintField;
    QrVSCalOriIMEITerceiro: TLargeintField;
    QrVSCalOriIMEICliVenda: TLargeintField;
    QrVSCalOriIMEIMovimID: TLargeintField;
    QrVSCalOriIMEIDataHora: TDateTimeField;
    QrVSCalOriIMEIPallet: TLargeintField;
    QrVSCalOriIMEIGraGruX: TLargeintField;
    QrVSCalOriIMEIPecas: TFloatField;
    QrVSCalOriIMEIPesoKg: TFloatField;
    QrVSCalOriIMEIAreaM2: TFloatField;
    QrVSCalOriIMEIAreaP2: TFloatField;
    QrVSCalOriIMEIValorT: TFloatField;
    QrVSCalOriIMEISrcMovID: TLargeintField;
    QrVSCalOriIMEISrcNivel1: TLargeintField;
    QrVSCalOriIMEISrcNivel2: TLargeintField;
    QrVSCalOriIMEISrcGGX: TLargeintField;
    QrVSCalOriIMEISdoVrtPeca: TFloatField;
    QrVSCalOriIMEISdoVrtPeso: TFloatField;
    QrVSCalOriIMEISdoVrtArM2: TFloatField;
    QrVSCalOriIMEIObserv: TWideStringField;
    QrVSCalOriIMEISerieFch: TLargeintField;
    QrVSCalOriIMEIFicha: TLargeintField;
    QrVSCalOriIMEIMisturou: TLargeintField;
    QrVSCalOriIMEIFornecMO: TLargeintField;
    QrVSCalOriIMEICustoMOKg: TFloatField;
    QrVSCalOriIMEICustoMOM2: TFloatField;
    QrVSCalOriIMEICustoMOTot: TFloatField;
    QrVSCalOriIMEIValorMP: TFloatField;
    QrVSCalOriIMEIDstMovID: TLargeintField;
    QrVSCalOriIMEIDstNivel1: TLargeintField;
    QrVSCalOriIMEIDstNivel2: TLargeintField;
    QrVSCalOriIMEIDstGGX: TLargeintField;
    QrVSCalOriIMEIQtdGerPeca: TFloatField;
    QrVSCalOriIMEIQtdGerPeso: TFloatField;
    QrVSCalOriIMEIQtdGerArM2: TFloatField;
    QrVSCalOriIMEIQtdGerArP2: TFloatField;
    QrVSCalOriIMEIQtdAntPeca: TFloatField;
    QrVSCalOriIMEIQtdAntPeso: TFloatField;
    QrVSCalOriIMEIQtdAntArM2: TFloatField;
    QrVSCalOriIMEIQtdAntArP2: TFloatField;
    QrVSCalOriIMEINotaMPAG: TFloatField;
    QrVSCalOriIMEINO_PALLET: TWideStringField;
    QrVSCalOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSCalOriIMEINO_TTW: TWideStringField;
    QrVSCalOriIMEIID_TTW: TLargeintField;
    QrVSCalOriIMEINO_FORNECE: TWideStringField;
    QrVSCalOriIMEINO_SerieFch: TWideStringField;
    QrVSCalOriIMEIReqMovEstq: TLargeintField;
    QrVSCalOriIMEICustoPQ: TFloatField;
    QrVSCalOriIMEIVSMulFrnCab: TLargeintField;
    QrVSCalOriIMEIClientMO: TLargeintField;
    DsVSCalOriIMEI: TDataSource;
    QrSum: TmySQLQuery;
    QrSumPesoKg: TFloatField;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    SbStqCenLoc: TSpeedButton;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSCalOriIMEIMarca: TWideStringField;
    QrVSPallet: TMySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    Label20: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    LaVSRibCla: TLabel;
    SbPallet1: TSpeedButton;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    //procedure SbMarcaClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    //procedure ReopenVSPallet();
    procedure ReopenVSSubPrdIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();

  public
    { Public declarations }
    //FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FUsaGSPJmp: Boolean;
    FGSPSrcMovID, FGSPJmpMovID: TEstqMovimID;
    FGSPSrcNiv2, FGSPJmpNiv2, FCodigo, FMovimCod, FEmpresa,
    FTerceiro, FFicha, FSerieFch, FVSMulFrnCab, FCouNiv2EmProc: Integer;
  end;

  var
  FmVSCalSubPrdItsMul: TFmVSCalSubPrdItsMul;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, UnVS_PF, ModuleGeral, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmVSCalSubPrdItsMul.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  SrcGGX     = 0;
  DstGGX     = 0;
  //Pallet     = 0; 2023-12-27
  CustoMOM2  = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  EdPalletX  = nil;
  EdAreaM2X  = nil;
  EdFicha    = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  JmpMovID   = TEstqMovimID(0);
  JmpNivel1  = 0;
  JmpNivel2  = 0;
  JmpGGX     = 0;
  RmsMovID   = TEstqMovimID(0);
  RmsNivel1  = 0;
  RmsNivel2  = 0;
  RmsGGX     = 0;
var
  DataHora, Observ, Marca: String;
  GSPSrcMovID, GSPJmpMovID: TESTQMovimID;
  Ficha, SerieFch, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  GraGruY, FornecMO, GSPSrcNiv2, GSPJmpNiv2, StqCenLoc, ReqMovEstq, ItemNFe,
  VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType, GeraNovo: TSQLType;
  //
  TotPecas, TotPesoKg, TotAreaM2, TotAreaP2, TotCustoMOKg, TotCustoMOTot,
  TotValorMP, TotValorT, Fator: Double;
  Qry: TmySQLQuery;
  Pallet: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    SQLType := ImgTipo.SQLType;
    if MyObjects.FIC(FEmpresa=0, nil, 'Empresa n�o definido!') then Exit;
    if MyObjects.FIC(FTerceiro=0, nil, 'Terceiro n�o definido!') then Exit;
    Codigo         := FCodigo;
    MovimCod       := FMovimCod;
    Empresa        := FEmpresa;
    ClientMO       := EdClientMO.ValueVariant;
    Terceiro       := FTerceiro;
    VSMulFrnCab    := FVSMulFrnCab;
    //DataHora       := Geral.FDT(FDataHora, 109);
    DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
    Ficha          := FFicha;
    SerieFch       := FSerieFch;
    //
    TotPecas       := EdPecas.ValueVariant;
    TotPesoKg      := EdPesoKg.ValueVariant;
    TotAreaM2      := EdAreaM2.ValueVariant;
    TotAreaP2      := EdAreaP2.ValueVariant;
    TotCustoMOKg   := EdCustoMOKg.ValueVariant;
    TotCustoMOTot  := EdCustoMOTot.ValueVariant;
    TotValorMP     := EdValorMP.ValueVariant;
    TotValorT      := EdValorT.ValueVariant;
    //
    Controle       := EdControle.ValueVariant;
    MovimID        := emidGeraSubProd;
    MovimNiv       := eminSemNiv;
    GraGruX        := EdGragruX.ValueVariant;
    Observ         := EdObserv.Text;
    GraGruY        := QrGraGruXGraGruY.Value;
    FornecMO       := EdFornecMO.ValueVariant;
    //Marca          := EdMarca.ValueVariant;
    //
    GSPSrcMovID    := FGSPSrcMovID;
    GSPSrcNiv2     := FGSPSrcNiv2;
    GSPJmpMovID    := FGSPJmpMovID;
    GSPJmpNiv2     := FGSPJmpNiv2;
    //
    StqCenLoc      := EdStqCenLoc.ValueVariant;
    ReqMovEstq     := EdReqMovEstq.ValueVariant;
    //
    Pallet         := EdPallet.ValueVariant;
    //
    if MyObjects.FIC(TPData.Date<2, TPData, 'Data/hora n�o definida!') then Exit;
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o sub produto!') then Exit;
    if MyObjects.FIC(TotPesoKg = 0, EdPesoKg, 'Informe o peso!') then Exit;
    if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then Exit;
    if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina quem fica com o subproduto!') then Exit;
    if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque!') then Exit;
    if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o pallet (bag)!') then Exit;
    //
    if (SQLType = stIns) and (Codigo = 0) then
      GeraNovo := stIns
    else
      GeraNovo := stUpd;
    Codigo := UMyMod.BPGS1I32('vssubprdcab', 'Codigo', '', '', tsPos, GeraNovo, Codigo);
    //
    if (SQLType = stIns) and (MovimCod = 0) then
      GeraNovo := stIns
    else
      GeraNovo := stUpd;
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, GeraNovo, MovimCod);
    //
    QrVSCalOriIMEI.First;
    while not QrVSCalOriIMEI.Eof do
    begin
      Fator          := QrVSCalOriIMEIPesoKg.Value / QrSumPesoKg.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE Controle=' + Geral.FF0(QrVSCalOriIMEISrcNivel2.Value),
      '']);
      GSPSrcMovID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
      GSPSrcNiv2  := Qry.FieldByName('Controle').AsInteger;
      if FUsaGSPJmp then
      begin
        GSPJmpMovID := TEstqMovimID(QrVSCalOriIMEIMovimID.Value);
        GSPJmpNiv2  := QrVSCalOriIMEIControle.Value;
      end;
      //
      Pecas          := Round(Fator * TotPecas);
      PesoKg         := Fator * TotPesoKg;
      AreaM2         := Fator * TotAreaM2;
      AreaP2         := Fator * TotAreaP2;
      CustoMOKg      := Fator * TotCustoMOKg;
      CustoMOTot     := Fator * TotCustoMOTot;
      ValorMP        := Fator * TotValorMP;
      ValorT         := Fator * TotValorT;
      //
      Marca          := QrVSCalOriIMEIMarca.Value;
      //
      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
        ImgTipo.SQLType, Controle);
      if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
      Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
      ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
      LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
      ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
      QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
      TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
      RmsGGX, GSPJmpMovID, GSPJmpNiv2,
    (*
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
    *)
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei009(*Gera��o de subproduto (MUL) em processo de caleiro*)) then
      begin
        VS_PF.AtualizaTotaisVSXxxCab('vssubprdcab', MovimCod);
        VS_PF.AtualizaSaldoIMEI(Controle, True);
      end;
      QrVSCalOriIMEI.Next;
    end;
    ReopenVSSubPrdIts(Controle);
    Close;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSCalSubPrdItsMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCalSubPrdItsMul.CalculaCustos();
var
  ValorMP, CustoMOKg, CustoMOTot, ValorT, PesoKg: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOKg    := EdCustoMOKg.ValueVariant;
  PesoKg       := EdPesoKg.ValueVariant;
  //
  CustoMOTot := CustoMOKg * PesoKg;
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSCalSubPrdItsMul.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSCalSubPrdItsMul.Criar1Click(Sender: TObject);
var
  GraGruX, Pallet, ClientMO: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ClientMO := EdClientMO.ValueVariant;
  //
  Pallet := VS_PF.CadastraPalletRibCla(FEmpresa, ClientMO, EdPallet, CBPallet,
    QrVSPallet, emidEmProcCal, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
  if Pallet <> 0 then
  begin
    EdPallet.ValueVariant := Pallet;
    CBPallet.KeyValue := Pallet;
  end;
end;

procedure TFmVSCalSubPrdItsMul.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSCalSubPrdItsMul.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Ficha: Integer;
*)
begin
(*
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
*)
end;

procedure TFmVSCalSubPrdItsMul.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSCalSubPrdItsMul.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSCalSubPrdItsMul.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSCalSubPrdItsMul.FormActivate(Sender: TObject);
begin
(*
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
*)
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSCalSubPrdItsMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUsaGSPJmp := False;
  FUltGGX := 0;
  TPData.Date := Now();
  EdHora.ValueVariant := Now();
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
  // ini 2023-12-27
  //'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0512_VSSubPrd));
  Geral.ATS([
  'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1536_VSCouCal),
  'AND nv2.Codigo > 1 '])); // Diferente de couro
  // fim 2023-12-27
  //
  //UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  //VS_PF.AbreVSSerFch(QrVSSerFch);
  //ReopenVSPallet();
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, emidGeraSubProd);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
end;

procedure TFmVSCalSubPrdItsMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCalSubPrdItsMul.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(EdPallet.ValueVariant);
end;

procedure TFmVSCalSubPrdItsMul.ReopenVSSubPrdIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

(*
procedure TFmVSSubPrdIts.ReopenVSPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM vspalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;
*)

(*
procedure TFmVSSubPrdItsMul.SbMarcaClick(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    LaMarca.Enabled := True;
    EdMarca.Enabled := True;
    EdMarca.SetFocus;
  end;
end;
*)

procedure TFmVSCalSubPrdItsMul.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

procedure TFmVSCalSubPrdItsMul.SbStqCenLocClick(Sender: TObject);
begin
  VS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidGeraSubProd);
end;

procedure TFmVSCalSubPrdItsMul.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

(*
object SbMarca: TSpeedButton
  Left = 104
  Top = 152
  Width = 23
  Height = 22
  Caption = '<'
  OnClick = SbMarcaClick
end
object EdMarca: TdmkEdit
  Left = 12
  Top = 152
  Width = 93
  Height = 21
  Enabled = False
  TabOrder = 13
  FormatType = dmktfString
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  QryCampo = 'Marca'
  UpdCampo = 'Marca'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = ''
  ValWarn = False
end
object LaMarca: TLabel
  Left = 12
  Top = 136
  Width = 33
  Height = 13
  Caption = 'Marca:'
  Enabled = False
end
*)
end.
