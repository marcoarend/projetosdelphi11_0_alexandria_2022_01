unit VSDsnIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmVSDsnIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBVSCacCod: TdmkDBLookupComboBox;
    EdVSCacCod: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrVSGerRclA: TmySQLQuery;
    DsVSGerRclA: TDataSource;
    QrVSGerRclACacCod: TIntegerField;
    QrVSGerRclAVSPallet: TIntegerField;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure ReopenVSGerRclA();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSDsnIts: TFmVSDsnIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSDsnIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, VSCacCod: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  VSCacCod       := EdVSCacCod.ValueVariant;
  if MyObjects.FIC(VSCacCod = 0, EdVSCacCod, 'Informe o IME-C / pallet!') then
    Exit;
  Controle := UMyMod.BPGS1I32('vsdsnits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsdsnits', False, [
  'Codigo', 'VSCacCod'], [
  'Controle'], [
  Codigo, VSCacCod], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdVSCacCod.ValueVariant  := 0;
      CBVSCacCod.KeyValue      := Null;
      //
      ReopenVSGerRclA();
      //
      EdVSCacCod.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSDsnIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSDsnIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSDsnIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenVSGerRclA();
end;

procedure TFmVSDsnIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSDsnIts.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSDsnIts.ReopenVSGerRclA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerRclA, Dmod.MyDB, [
  'SELECT CacCod, VSPallet ',
  'FROM vsgerrcla ',
  'WHERE NOT CacCod IN ( ',
  //'WHERE NOT Codigo IN ( ',
  '  SELECT VSCacCod ',
  '  FROM vsdsnits) ',
  'ORDER BY VSPallet ',
  '']);
end;

procedure TFmVSDsnIts.SpeedButton1Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
*)
end;

end.
