object FmVSCorrigeMulFrn: TFmVSCorrigeMulFrn
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-127 :: Corrige IME-Is sem Fornecedor'
  ClientHeight = 629
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 927
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 879
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Corrige IME-Is sem Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Corrige IME-Is sem Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Corrige IME-Is sem Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 975
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 975
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter2: TSplitter
        Left = 781
        Top = 0
        Height = 467
        Align = alRight
        ExplicitLeft = 864
        ExplicitTop = 12
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 781
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 165
          Width = 777
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 225
          ExplicitWidth = 808
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 777
          Height = 150
          Align = alTop
          Caption = 'Tipos iniciais de estoque x IME-Cs sem defini'#231#227'o de fornecedor:'
          TabOrder = 0
          object DBGSemIni: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 463
            Height = 133
            Align = alLeft
            DataSource = DsSemIni
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'MovimID'
                Title.Caption = 'ID tipo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MovimID'
                Title.Caption = 'Nome tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo do ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovimCod'
                Title.Caption = 'IME-C'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Qtde de IME-Is'
                Visible = True
              end>
          end
          object DGDados: TdmkDBGridZTO
            Left = 465
            Top = 15
            Width = 310
            Height = 133
            Align = alClient
            DataSource = DsIMEIsIni
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor / Cliente'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SerieFch'
                Title.Caption = 'ID Serie'
                Width = 43
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Pallet'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Custo total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_PesoKg'
                Title.Caption = '$ / Kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_AreaM2'
                Title.Caption = '$ / m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_AreaP2'
                Title.Caption = '$ / ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_Peca'
                Title.Caption = '$ / pe'#231'a'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Width = 300
                Visible = True
              end>
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 170
          Width = 777
          Height = 295
          Align = alClient
          Caption = 'Tipos descendentes x IME-Cs sem defini'#231#227'o de fornecedor:'
          TabOrder = 1
          object DBGPesquisa: TDBGrid
            Left = 2
            Top = 15
            Width = 463
            Height = 278
            Align = alLeft
            DataSource = DsPesquisa
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'MovimID'
                Title.Caption = 'ID tipo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MovimID'
                Title.Caption = 'Nome tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo do ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MovimCod'
                Title.Caption = 'IME-C'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Qtde de IME-Is'
                Visible = True
              end>
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 465
            Top = 15
            Width = 310
            Height = 278
            Align = alClient
            DataSource = DsIMEIsPsq
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor / Cliente'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SerieFch'
                Title.Caption = 'ID Serie'
                Width = 43
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Pallet'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Custo total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_PesoKg'
                Title.Caption = '$ / Kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_AreaM2'
                Title.Caption = '$ / m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_AreaP2'
                Title.Caption = '$ / ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUS_Peca'
                Title.Caption = '$ / pe'#231'a'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Width = 300
                Visible = True
              end>
          end
        end
      end
      object Memo1: TMemo
        Left = 784
        Top = 0
        Width = 191
        Height = 467
        Align = alRight
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Lucida Console'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 975
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 971
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 975
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 829
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 827
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 144
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Total de itens:'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 412
        Top = 4
        Width = 115
        Height = 13
        Caption = 'Somente IDs: Ex. 1,6,17'
      end
      object SpeedButton1: TSpeedButton
        Left = 608
        Top = 20
        Width = 57
        Height = 22
        Caption = 'Reabrir'
        OnClick = SpeedButton1Click
      end
      object BtAcao: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcaoClick
      end
      object DBEdit1: TDBEdit
        Left = 144
        Top = 20
        Width = 112
        Height = 21
        DataField = 'ITENS'
        DataSource = DsTotal
        TabOrder = 1
      end
      object BtParar: TBitBtn
        Tag = 126
        Left = 280
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Parar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPararClick
      end
      object EdMovimIDs: TdmkEdit
        Left = 412
        Top = 20
        Width = 193
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object QrPesquisa: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPesquisaBeforeClose
    AfterScroll = QrPesquisaAfterScroll
    SQL.Strings = (
      'SELECT '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado",' +
        '"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","P' +
        'r'#233' reclasse","Compra de Classificado","Baixa extra","Saldo anter' +
        'ior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Pr' +
        'oduto","Reclasse Mul.") NO_MovimID,'
      'MovimID, MovimCod, Codigo, COUNT(Controle) ITENS  '
      'FROM vsmovits vmi '
      'WHERE Terceiro=0 '
      'AND VSMulFrnCab=0 '
      'GROUP BY MovimID, MovimCod '
      'ORDER BY Controle')
    Left = 32
    Top = 276
    object QrPesquisaNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrPesquisaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPesquisaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrPesquisaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesquisaITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsPesquisa: TDataSource
    DataSet = QrPesquisa
    Left = 32
    Top = 324
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CAST(COUNT(Controle) AS SIGNED) ITENS   '
      'FROM vsmovits  '
      'WHERE Terceiro=0  '
      'AND VSMulFrnCab=0  ')
    Left = 252
    Top = 124
    object QrTotalITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 248
    Top = 172
  end
  object QrSemIni: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSemIniBeforeClose
    AfterScroll = QrSemIniAfterScroll
    SQL.Strings = (
      'SELECT '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado",' +
        '"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","P' +
        'r'#233' reclasse","Compra de Classificado","Baixa extra","Saldo anter' +
        'ior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Pr' +
        'oduto","Reclasse Mul.") NO_MovimID,'
      'MovimID, MovimCod, Codigo, COUNT(Controle) ITENS  '
      'FROM vsmovits vmi '
      'WHERE Terceiro=0 '
      'AND VSMulFrnCab=0 '
      'GROUP BY MovimID, MovimCod '
      'ORDER BY Controle')
    Left = 36
    Top = 120
    object QrSemIniNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrSemIniMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrSemIniMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrSemIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSemIniITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSemIni: TDataSource
    DataSet = QrSemIni
    Left = 36
    Top = 168
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 20
    Top = 584
    object Rastrearecorrigirfornecedornostiposfilhos1: TMenuItem
      Caption = 'Rastrear e corrigir fornecedor nos tipos filhos'
      OnClick = Rastrearecorrigirfornecedornostiposfilhos1Click
    end
    object MostraJanela1: TMenuItem
      Caption = 'Mostra Janela'
      object Descendente1: TMenuItem
        Caption = 'Descendente'
        OnClick = Descendente1Click
      end
      object Inicial1: TMenuItem
        Caption = 'Inicial'
        OnClick = Inicial1Click
      end
    end
    object DefinirFornecedorinicial1: TMenuItem
      Caption = '&Definir Fornecedor nos tipos iniciais selecionados'
      OnClick = DefinirFornecedorinicial1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Setarfornecmultiplocomo11: TMenuItem
      Caption = '&Setar fornec. multiplo como -1 (Saldo inicial)'
      OnClick = Setarfornecmultiplocomo11Click
    end
  end
  object QrFilhos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 408
    object QrFilhosControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrIMEIsIni: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 104
    Top = 117
    object QrIMEIsIniCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIsIniControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIsIniMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIsIniMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIsIniMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIsIniEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIsIniTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIsIniCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIsIniMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIsIniDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIsIniPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIsIniGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIsIniPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsIniPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsIniAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIsIniSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIsIniSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIsIniSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIsIniSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsIniSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsIniSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIsIniSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEIsIniFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEIsIniMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEIsIniFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIsIniCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIsIniDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIsIniDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIsIniDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIsIniQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsIniQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsIniQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsIniQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsIniQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsIniNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsIniPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEIsIniMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIsIniStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsIniNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEIsIniNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIsIniNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsIniNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIsIniID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIsIniReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsIMEIsIni: TDataSource
    DataSet = QrIMEIsIni
    Left = 104
    Top = 161
  end
  object QrIMEIsPsq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 108
    Top = 277
    object QrIMEIsPsqCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIsPsqControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIsPsqMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIsPsqMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIsPsqMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIsPsqEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIsPsqTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIsPsqCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIsPsqMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIsPsqDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIsPsqPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIsPsqGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIsPsqPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPsqPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPsqAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIsPsqSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIsPsqSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIsPsqSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIsPsqSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsPsqSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPsqSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIsPsqSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEIsPsqFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEIsPsqMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEIsPsqFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIsPsqCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIsPsqDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIsPsqDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIsPsqDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIsPsqQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsPsqQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPsqQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsPsqQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPsqQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsPsqNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsPsqPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEIsPsqMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIsPsqStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsPsqNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEIsPsqNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIsPsqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsPsqNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIsPsqID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIsPsqReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsIMEIsPsq: TDataSource
    DataSet = QrIMEIsPsq
    Left = 108
    Top = 321
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 848
    Top = 68
    object QrVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
  end
end
