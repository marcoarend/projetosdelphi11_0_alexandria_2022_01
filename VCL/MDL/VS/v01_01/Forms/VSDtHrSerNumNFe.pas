unit VSDtHrSerNumNFe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker;

type
  TFmVSDtHrSerNumNFe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    LaHora: TLabel;
    Label57: TLabel;
    EdSerieNFe: TdmkEdit;
    EdNumeroNFe: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirmou: Boolean;
  end;

  var
  FmVSDtHrSerNumNFe: TFmVSDtHrSerNumNFe;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmVSDtHrSerNumNFe.BtOKClick(Sender: TObject);
begin
  FConfirmou := True;
  //
  Close;
end;

procedure TFmVSDtHrSerNumNFe.BtSaidaClick(Sender: TObject);
begin
  FConfirmou := False;
  //
  Close;
end;

procedure TFmVSDtHrSerNumNFe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSDtHrSerNumNFe.FormCreate(Sender: TObject);
begin
  FConfirmou := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSDtHrSerNumNFe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
