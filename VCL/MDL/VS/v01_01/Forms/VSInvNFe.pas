unit VSInvNFe;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmVSInvNFe = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSInvNFe: TmySQLQuery;
    DsVSInvNFe: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdMovimCod: TdmkEdit;
    Label3: TLabel;
    EdMovimID: TdmkEditCB;
    Label4: TLabel;
    CBMovimID: TdmkDBLookupComboBox;
    QrVSMovimID: TmySQLQuery;
    QrVSMovimIDCodigo: TIntegerField;
    QrVSMovimIDNome: TWideStringField;
    DsVSMovimID: TDataSource;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdMovCodOri: TdmkEdit;
    Label5: TLabel;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label57: TLabel;
    Label6: TLabel;
    EdNFeStatus: TdmkEditCB;
    CBNFeStatus: TdmkDBLookupComboBox;
    QrNFeMsg: TmySQLQuery;
    DsNFeMsg: TDataSource;
    QrVSInvNFeCodigo: TIntegerField;
    QrVSInvNFeMovimCod: TIntegerField;
    QrVSInvNFeMovimID: TIntegerField;
    QrVSInvNFeNome: TWideStringField;
    QrVSInvNFeEmpresa: TIntegerField;
    QrVSInvNFeide_serie: TIntegerField;
    QrVSInvNFeide_nNF: TIntegerField;
    QrVSInvNFeNFeStatus: TIntegerField;
    QrVSInvNFeMovCodOri: TIntegerField;
    QrVSInvNFeLk: TIntegerField;
    QrVSInvNFeDataCad: TDateField;
    QrVSInvNFeDataAlt: TDateField;
    QrVSInvNFeUserCad: TIntegerField;
    QrVSInvNFeUserAlt: TIntegerField;
    QrVSInvNFeAlterWeb: TSmallintField;
    QrVSInvNFeAtivo: TSmallintField;
    QrVSInvNFeTerceiro: TIntegerField;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label2: TLabel;
    DBEdit10: TDBEdit;
    QrVSInvNFeData: TDateTimeField;
    GBStatusAlt: TGroupBox;
    LaPrompt: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EdVSVmcWrn: TdmkEditCB;
    CBVSVmcWrn: TdmkDBLookupComboBox;
    EdVSVmcSeq: TdmkEdit;
    EdVSVmcObs: TdmkEdit;
    QrVSInvNFeVSVmcWrn: TSmallintField;
    QrVSInvNFeVSVmcObs: TWideStringField;
    QrVSVmcWrn: TmySQLQuery;
    QrVSVmcWrnCodigo: TIntegerField;
    QrVSVmcWrnNome: TWideStringField;
    DsVSVmcWrn: TDataSource;
    QrVSInvNFeVSVmcSeq: TWideStringField;
    GBStatusShw: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSInvNFeAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSInvNFeBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVSInvNFe: TFmVSInvNFe;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, NFe_PF, ModuleGeral, DmkDAC_PF, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSInvNFe.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSInvNFe.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSInvNFeCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSInvNFe.DefParams;
begin
  VAR_GOTOTABELA := 'vsinvnfe';
  VAR_GOTOMYSQLTABLE := QrVSInvNFe;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsinvnfe');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSInvNFe.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSInvNFe.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSInvNFe.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSInvNFe.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSInvNFe.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSInvNFe.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSInvNFe.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSInvNFe.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInvNFe.BtAlteraClick(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSInvNFe, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsinvnfe');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSinvNFeEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSInvNFe.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSInvNFeCodigo.Value;
  Close;
end;

procedure TFmVSInvNFe.BtConfirmaClick(Sender: TObject);
var
  Nome, Data, VSVmcSeq, VSVmcObs: String;
  Codigo, MovimCod, MovimID, Empresa, ide_serie, ide_nNF, NFeStatus, MovCodOri,
  VSVmcWrn: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;;
  MovimID        := EdMovimID.ValueVariant;
  Nome           := EdNome.Text;
  Empresa        := EdEmpresa.ValueVariant;
  Data           := Geral.FDT(TPData.Date, 1);
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  NFeStatus      := EdNFeStatus.ValueVariant;
  MovCodOri      := EdMovCodOri.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  if MyObjects.Fic(Empresa = 0, EdEmpresa, 'Selecione a empresa!') then Exit;
  Empresa        := DmodG.QrEmpresasCodigo.Value;
  //
  VSVmcWrn    := EdVSVmcWrn.ValueVariant;
  VSVmcSeq    := EdVSVmcSeq.Text;
  VSVmcObs    := EdVSVmcObs.Text;
  //
  Codigo := UMyMod.BPGS1I32('vsinvnfe', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinvnfe', False, [
  'MovimCod', 'MovimID', 'Nome',
  'Empresa', 'Data', 'ide_serie',
  'ide_nNF', 'NFeStatus', 'MovCodOri',
  'VSVmcWrn', 'VSVmcSeq', 'VSVmcObs'], [
  'Codigo'], [
  MovimCod, MovimID, Nome,
  Empresa, Data, ide_serie,
  ide_nNF, NFeStatus, MovCodOri,
  VSVmcWrn, VSVmcSeq, VSVmcObs], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSInvNFe.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsinvnfe', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSInvNFe.BtExcluiClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := QrVSInvNFeCodigo.Value;
  if DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSInvNFe, 'vsinvnfe', ['Codigo'],
  ['Codigo'], True, 'Confirma a exclus�o deste registro?') then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSInvNFe.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSInvNFe, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsinvnfe');
end;

procedure TFmVSInvNFe.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  GBStatusShw.Align := alClient;
  GBStatusAlt.Align := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrVSVmcWrn, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMovimID, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeMsg, DModG.AllID_DB, [
  'SELECT Codigo, Nome ',
  'FROM nfemsg ',
  'ORDER BY Nome ',
  '']);
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  //EdDtCompra.ValueVariant := Agora;
end;

procedure TFmVSInvNFe.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSInvNFeCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInvNFe.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSInvNFe.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSInvNFeCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInvNFe.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSInvNFe.QrVSInvNFeAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSInvNFe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if (EdEmpresa.ValueVariant > 0) and (EdEmpresa.Focused) then
    EdNome.SetFocus;
end;

procedure TFmVSInvNFe.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSInvNFeCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsinvnfe', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSInvNFe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInvNFe.QrVSInvNFeBeforeOpen(DataSet: TDataSet);
begin
  QrVSInvNFeCodigo.DisplayFormat := FFormatFloat;
end;

end.

