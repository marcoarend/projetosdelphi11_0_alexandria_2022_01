unit TXBxaItsIMEIs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Menus, UnProjGroup_Consts, dmkEditCB, dmkDBLookupComboBox, dmkEdit,
  UnAppEnums;

type
  TFmTXBxaItsIMEIs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGruY: TmySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    DsGraGruY: TDataSource;
    QrIMEIs: TmySQLQuery;
    DsIMEIs: TDataSource;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsCodigo: TIntegerField;
    QrIMEIsControle: TIntegerField;
    QrIMEIsMovimCod: TIntegerField;
    QrIMEIsMovimNiv: TIntegerField;
    QrIMEIsMovimTwn: TIntegerField;
    QrIMEIsEmpresa: TIntegerField;
    QrIMEIsTerceiro: TIntegerField;
    QrIMEIsCliVenda: TIntegerField;
    QrIMEIsMovimID: TIntegerField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TIntegerField;
    QrIMEIsGraGruX: TIntegerField;
    QrIMEIsQtde: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TIntegerField;
    QrIMEIsSrcNivel1: TIntegerField;
    QrIMEIsSrcNivel2: TIntegerField;
    QrIMEIsSrcGGX: TIntegerField;
    QrIMEIsSdoVrtQtd: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsFornecMO: TIntegerField;
    QrIMEIsCustoMOUni: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TIntegerField;
    QrIMEIsDstNivel1: TIntegerField;
    QrIMEIsDstNivel2: TIntegerField;
    QrIMEIsDstGGX: TIntegerField;
    QrIMEIsQtdGer: TFloatField;
    QrIMEIsQtdAnt: TFloatField;
    QrIMEIsAptoUso: TSmallintField;
    QrIMEIsMarca: TWideStringField;
    QrIMEIsLk: TIntegerField;
    QrIMEIsDataCad: TDateField;
    QrIMEIsDataAlt: TDateField;
    QrIMEIsUserCad: TIntegerField;
    QrIMEIsUserAlt: TIntegerField;
    QrIMEIsAlterWeb: TSmallintField;
    QrIMEIsAtivo: TSmallintField;
    QrIMEIsTpCalcAuto: TIntegerField;
    QrIMEIsGraGruY: TIntegerField;
    CkContinuar: TCheckBox;
    Label1: TLabel;
    EdSenha: TEdit;
    Panel5: TPanel;
    Splitter1: TSplitter;
    DBGGraGruY: TdmkDBGridZTO;
    Panel6: TPanel;
    RGPosiNega: TRadioGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtProvisorio: TBitBtn;
    PMProvisorio: TPopupMenu;
    ObterIMEIs11: TMenuItem;
    QrIMEIsTXMulFrnCab: TIntegerField;
    QrIMEIsClientMO: TIntegerField;
    Panel7: TPanel;
    DBGIMEIs: TdmkDBGridZTO;
    Panel8: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    GBDadosItem1: TGroupBox;
    Label6: TLabel;
    Label2: TLabel;
    Label53: TLabel;
    Label49: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdCouNiv2: TdmkEditCB;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    CBCouNiv2: TdmkDBLookupComboBox;
    QrIMEIsStqCenLoc: TIntegerField;
    QrIMEIsSerieTal: TIntegerField;
    QrIMEIsTalao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGGraGruYAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure BtOKClick(Sender: TObject);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure ObterIMEIs11Click(Sender: TObject);
    procedure BtProvisorioClick(Sender: TObject);
    procedure QrStqCenCadAfterScroll(DataSet: TDataSet);
    procedure EdControleRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdStqCenLocChange(Sender: TObject);
    procedure EdCouNiv2Redefinido(Sender: TObject);
    procedure EdCouNiv1Redefinido(Sender: TObject);
  private
    { Private declarations }
    procedure BaixaIMEIAtual();
    procedure ReopenIMEIs();
    procedure SetaTodos(Ativo: Boolean);
  public
    { Public declarations }
    FControle: Integer;
    FCodigo, FMovimCod, FEmpresa: Integer;
  end;

  var
  FmTXBxaItsIMEIs: TFmTXBxaItsIMEIs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnTX_PF, ModuleGeral, UMySQLModule,
  UnGrade_PF;

{$R *.DFM}

procedure TFmTXBxaItsIMEIs.BaixaIMEIAtual();
const
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq  = 0;
  //
  ItemNFe     = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  SrcNivel1, SrcNivel2, GraGruY, SrcGGX,
  TXMulFrnCab, ClientMO, FornecMO, StqCenLoc, SerieTal, Talao: Integer;
  Qtde, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
  Qry: TmySQLQuery;
begin
  SrcMovID       := TEstqmovimID(QrIMEIsMovimID.Value);
  SrcNivel1      := QrIMEIsCodigo.Value;
  SrcNivel2      := QrIMEIsControle.Value;
  SrcGGX         := QrIMEIsGraGruX.Value;
  //
  Codigo         := FCodigo;
  Controle       := 0;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  ClientMO       := QrIMEIsClientMO.Value;
  FornecMO       := QrIMEIsFornecMO.Value;
  if FornecMO = 0 then
    FornecMO := QrIMEIsEmpresa.Value;
  StqCenLoc      := QrIMEIsStqCenLoc.Value;
  Terceiro       := QrIMEIsTerceiro.Value;
  TXMulFrnCab    := QrIMEIsTXMulFrnCab.Value;
  DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidForcado;
  MovimNiv       := eminSemNiv;
  Pallet         := QrIMEIsPallet.Value;
  GraGruX        := QrIMEIsGraGruX.Value;
  Qtde           := -QrIMEIsSdoVrtQtd.Value;
  ValorT         := 0;
  Observ         := '';
  Marca          := QrIMEIsMarca.Value;
  GraGruY        := QrIMEIsGraGruY.Value;
  //
  SerieTal       := QrIMEIsSerieTal.Value;
  Talao          := QrIMEIsTalao.Value;
  //
  if not TX_PF.ObtemControleIMEI(ImgTipo.SQLType, Controle, EdSenha.Text) then
  begin
    Close;
    Exit;
  end;
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, ReqMovEstq,
  StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei058(*Baixa for�ada 2/2*)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txbxacab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FControle := Controle;
  end;
end;

procedure TFmTXBxaItsIMEIs.BtNenhumClick(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmTXBxaItsIMEIs.BtOKClick(Sender: TObject);
var
  I: Integer;
begin
  if (DBGIMEIs.SelectedRows.Count > 0) then
  begin
    try
      for I := 0 to DBGIMEIs.SelectedRows.Count - 1 do
      begin
        //QrIMEIs.GotoBookmark(pointer(DBGIMEIs.SelectedRows.Items[I]));
        QrIMEIs.GotoBookmark(DBGIMEIs.SelectedRows.Items[I]);
        //
        BaixaIMEIAtual();
      end;
    finally
      ReopenIMEIs();
    end;
    if not CkContinuar.Checked then
      Close;
  end else
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!');
end;

procedure TFmTXBxaItsIMEIs.BtProvisorioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisorio, BtProvisorio);
end;

procedure TFmTXBxaItsIMEIs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXBxaItsIMEIs.BtTodosClick(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmTXBxaItsIMEIs.DBGGraGruYAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  ReopenIMEIs();
  GBDadosItem1.Visible := True;
end;

procedure TFmTXBxaItsIMEIs.EdControleRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.EdCouNiv1Redefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.EdCouNiv2Redefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.EdStqCenCadRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.EdStqCenLocChange(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmTXBxaItsIMEIs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXBxaItsIMEIs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  FControle := 0;
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  TX_PF.AbreGraGruXY(QrGraGruX, '');
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
end;

procedure TFmTXBxaItsIMEIs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXBxaItsIMEIs.ObterIMEIs11Click(Sender: TObject);
var
  IMEIs: String;
begin
  IMEIs := '';
  QrIMEIS.First;
  while not QrIMEIS.Eof do
  begin
    IMEIs := IMEIs + Geral.FF0(QrIMEIsControle.Value) + ',';
    //
    QrIMEIs.Next;
  end;
  Geral.MB_Info(IMEIs);
end;

procedure TFmTXBxaItsIMEIs.QrStqCenCadAfterScroll(DataSet: TDataSet);
begin
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, QrStqCenCadCodigo.Value, 0);
end;

procedure TFmTXBxaItsIMEIs.ReopenIMEIs();
var
  I: Integer;
  GraGruYs, Sinal: String;
var
  Data: TDateTime;
  GraGruX, StqCenCad, StqCenloc, ImeiSrc, CouNiv1, CouNiv2: Integer;
var
  SQL_GraGruYs, SQL_GraGruX, SQL_StqCenCad, SQL_StqCenLoc, SQL_CouNiv1,
  SQL_CouNiv2: String;
begin
  SQL_GraGruYs := '';
  SQL_StqCenCad := '';
  SQL_StqCenLoc := '';
  SQL_CouNiv1 := '';
  SQL_CouNiv2 := '';

  //begin
  GraGruYs := '';
  if (DBGGraGruY.SelectedRows.Count > 0) and
  (DBGGraGruY.SelectedRows.Count < QrGraGruY.RecordCount) then
  begin
    for I := 0 to DBGGraGruY.SelectedRows.Count - 1 do
    begin
      //QrGraGruY.GotoBookmark(pointer(DBGGraGruY.SelectedRows.Items[I]));
      QrGraGruY.GotoBookmark(DBGGraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(QrGraGruYCodigo.Value);
    end;
  end;
  if Trim(GraGruYs) <> '' then
    SQL_GraGruYs := 'AND ggx.GraGruY IN (' + GraGruYs + ') ';
  //
  StqCenCad  := EdStqCenCad.ValueVariant;
  StqCenLoc  := EdStqCenLoc.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  //
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND tmi.GraGruX=' + Geral.FF0(GraGruX);
  if StqCenCad <> 0 then
    SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad);
  if StqCenLoc <> 0 then
    SQL_StqCenLoc := 'AND tmi.StqCenLoc=' + Geral.FF0(StqCenLoc);
  if CouNiv1 <> 0 then
    SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1);
  if CouNiv2 <> 0 then
    SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2);
  //

  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ggx.GraGruY, tmi.* ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=tmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE tmi.SdoVrtQtd' + Sinal + '0 ',
  'AND tmi.Empresa=' + Geral.FF0(FEmpresa),
  SQL_GraGruYs,
  SQL_GraGruX,
  SQL_StqCenCad,
  SQL_StqCenLoc,
  SQL_CouNiv1,
  SQL_CouNiv2,
  'ORDER BY tmi.Controle ',
  '']);
end;

procedure TFmTXBxaItsIMEIs.RGPosiNegaClick(Sender: TObject);
begin
  ReopenIMEIs();
  BtTodos.Enabled := RGPosiNega.ItemIndex = 1;
  BtNenhum.Enabled := RGPosiNega.ItemIndex = 1;
end;

procedure TFmTXBxaItsIMEIs.SetaTodos(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBGIMEIs))
  else
    DBGIMEIs.SelectedRows.Clear;
end;

end.
