object FmVSMOEnvEnvGer: TFmVSMOEnvEnvGer
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-226 :: Gerenciamento de Frete de Envio para Retorno'
  ClientHeight = 691
  ClientWidth = 861
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 861
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 861
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 532
      Width = 861
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 721
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 861
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 861
      Height = 313
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 857
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 76
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdNome
        end
        object Label34: TLabel
          Left = 709
          Top = 1
          Width = 32
          Height = 13
          Caption = 'IME-C:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSMOEnvEnv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 136
          Top = 16
          Width = 569
          Height = 21
          Color = clWhite
          DataField = 'NO_Empresa'
          DataSource = DsVSMOEnvEnv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 76
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'CFTMP_Empresa'
          DataSource = DsVSMOEnvEnv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 708
          Top = 16
          Width = 66
          Height = 21
          DataField = 'VSVMI_MovimCod'
          DataSource = DsVSMOEnvEnv
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 2
        Top = 157
        Width = 857
        Height = 154
        Align = alBottom
        Caption = ' CTe Cobran'#231'a de frete: '
        TabOrder = 1
        ExplicitTop = 217
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 853
          Height = 137
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label32: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            Enabled = False
          end
          object Label33: TLabel
            Left = 44
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label35: TLabel
            Left = 120
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label36: TLabel
            Left = 156
            Top = 4
            Width = 49
            Height = 13
            Caption = 'Num CF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label37: TLabel
            Left = 232
            Top = 4
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label38: TLabel
            Left = 12
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label39: TLabel
            Left = 220
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label40: TLabel
            Left = 324
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label42: TLabel
            Left = 672
            Top = 44
            Width = 45
            Height = 13
            Caption = '$ Total:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label43: TLabel
            Left = 472
            Top = 44
            Width = 81
            Height = 13
            Caption = 'Peso kg frete:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 572
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Cus. frete kg:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label45: TLabel
            Left = 268
            Top = 4
            Width = 84
            Height = 13
            Caption = 'Transportador:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label41: TLabel
            Left = 116
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label3: TLabel
            Left = 12
            Top = 84
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object DBEdit2: TDBEdit
            Left = 12
            Top = 20
            Width = 28
            Height = 21
            DataField = 'CFTMP_FatID'
            DataSource = DsVSMOEnvEnv
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 44
            Top = 20
            Width = 72
            Height = 21
            DataField = 'CFTMP_FatNum'
            DataSource = DsVSMOEnvEnv
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 120
            Top = 20
            Width = 32
            Height = 21
            DataField = 'CFTMP_SerCT'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 156
            Top = 20
            Width = 72
            Height = 21
            DataField = 'CFTMP_nCT'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object DBEdit6: TDBEdit
            Left = 232
            Top = 20
            Width = 32
            Height = 21
            DataField = 'CFTMP_nItem'
            DataSource = DsVSMOEnvEnv
            TabOrder = 4
          end
          object DBEdit7: TDBEdit
            Left = 268
            Top = 20
            Width = 56
            Height = 21
            DataField = 'CFTMP_Terceiro'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit8: TDBEdit
            Left = 328
            Top = 20
            Width = 442
            Height = 21
            DataField = 'NO_Transpor'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object DBEdit9: TDBEdit
            Left = 12
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMP_Pecas'
            DataSource = DsVSMOEnvEnv
            TabOrder = 7
          end
          object DBEdit10: TDBEdit
            Left = 116
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMP_PesoKg'
            DataSource = DsVSMOEnvEnv
            TabOrder = 8
          end
          object DBEdit11: TDBEdit
            Left = 220
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMP_AreaM2'
            DataSource = DsVSMOEnvEnv
            TabOrder = 9
          end
          object DBEdit12: TDBEdit
            Left = 324
            Top = 60
            Width = 100
            Height = 21
            DataField = 'CFTMP_AreaP2'
            DataSource = DsVSMOEnvEnv
            TabOrder = 10
          end
          object DBEdit13: TDBEdit
            Left = 472
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMP_PesTrKg'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 11
          end
          object DBEdit14: TDBEdit
            Left = 572
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMP_CusTrKg'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object DBEdit15: TDBEdit
            Left = 672
            Top = 60
            Width = 96
            Height = 21
            DataField = 'CFTMP_ValorT'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
          end
          object DBMemo1: TDBMemo
            Left = 12
            Top = 100
            Width = 645
            Height = 33
            DataField = 'Nome'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 55
        Width = 857
        Height = 102
        Align = alClient
        Caption = ' NFe Envio mat'#233'ria-prima: '
        TabOrder = 2
        ExplicitHeight = 162
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 853
          Height = 85
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 145
          object Label11: TLabel
            Left = 712
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label5: TLabel
            Left = 120
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 156
            Top = 4
            Width = 50
            Height = 13
            Caption = 'Num NF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 392
            Top = 44
            Width = 36
            Height = 13
            Caption = '$ Total:'
          end
          object Label31: TLabel
            Left = 232
            Top = 4
            Width = 171
            Height = 13
            Caption = 'Prestador do servi'#231'o da M.O.:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 12
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label6: TLabel
            Left = 104
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label8: TLabel
            Left = 200
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label10: TLabel
            Left = 296
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label13: TLabel
            Left = 12
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            Enabled = False
          end
          object Label15: TLabel
            Left = 44
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label16: TLabel
            Left = 488
            Top = 44
            Width = 62
            Height = 13
            Caption = 'Saldo pe'#231'as:'
            FocusControl = DBEdit29
          end
          object Label17: TLabel
            Left = 584
            Top = 44
            Width = 72
            Height = 13
            Caption = 'Saldo peso Kg:'
            FocusControl = DBEdit30
          end
          object Label18: TLabel
            Left = 680
            Top = 44
            Width = 69
            Height = 13
            Caption = 'Saldo '#193'rea m'#178':'
            FocusControl = DBEdit31
          end
          object DBEdit16: TDBEdit
            Left = 12
            Top = 20
            Width = 28
            Height = 21
            DataField = 'NFEMP_FatID'
            DataSource = DsVSMOEnvEnv
            TabOrder = 0
          end
          object DBEdit17: TDBEdit
            Left = 44
            Top = 20
            Width = 72
            Height = 21
            DataField = 'NFEMP_FatNum'
            DataSource = DsVSMOEnvEnv
            TabOrder = 1
          end
          object DBEdit18: TDBEdit
            Left = 120
            Top = 20
            Width = 32
            Height = 21
            DataField = 'NFEMP_SerNF'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit19: TDBEdit
            Left = 156
            Top = 20
            Width = 72
            Height = 21
            DataField = 'NFEMP_nNF'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object DBEdit20: TDBEdit
            Left = 232
            Top = 20
            Width = 32
            Height = 21
            DataField = 'NFEMP_nItem'
            DataSource = DsVSMOEnvEnv
            TabOrder = 4
          end
          object DBEdit21: TDBEdit
            Left = 268
            Top = 20
            Width = 56
            Height = 21
            DataField = 'NFEMP_Terceiro'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit22: TDBEdit
            Left = 328
            Top = 20
            Width = 381
            Height = 21
            DataField = 'NO_FornMO'
            DataSource = DsVSMOEnvEnv
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object DBEdit23: TDBEdit
            Left = 12
            Top = 60
            Width = 88
            Height = 21
            DataField = 'NFEMP_Pecas'
            DataSource = DsVSMOEnvEnv
            TabOrder = 7
          end
          object DBEdit24: TDBEdit
            Left = 104
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_PesoKg'
            DataSource = DsVSMOEnvEnv
            TabOrder = 8
          end
          object DBEdit25: TDBEdit
            Left = 200
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_AreaM2'
            DataSource = DsVSMOEnvEnv
            TabOrder = 9
          end
          object DBEdit26: TDBEdit
            Left = 296
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_AreaP2'
            DataSource = DsVSMOEnvEnv
            TabOrder = 10
          end
          object DBEdit27: TDBEdit
            Left = 712
            Top = 20
            Width = 58
            Height = 21
            DataField = 'NFEMP_Empresa'
            DataSource = DsVSMOEnvEnv
            TabOrder = 11
          end
          object DBEdit28: TDBEdit
            Left = 392
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_ValorT'
            DataSource = DsVSMOEnvEnv
            TabOrder = 12
          end
          object DBEdit29: TDBEdit
            Left = 488
            Top = 60
            Width = 88
            Height = 21
            DataField = 'NFEMP_SdoPeca'
            DataSource = DsVSMOEnvEnv
            TabOrder = 13
          end
          object DBEdit30: TDBEdit
            Left = 584
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_SdoPeso'
            DataSource = DsVSMOEnvEnv
            TabOrder = 14
          end
          object DBEdit31: TDBEdit
            Left = 680
            Top = 60
            Width = 92
            Height = 21
            DataField = 'NFEMP_SdoArM2'
            DataSource = DsVSMOEnvEnv
            TabOrder = 15
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 531
      Width = 861
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 164
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 338
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10134
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dados CTe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 223
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-Is'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DBGVSMOEnvAVMI: TdmkDBGridZTO
      Left = 0
      Top = 313
      Width = 861
      Height = 84
      Align = alTop
      DataSource = DsVSMOEnvEVMI
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovIts'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorFrete'
          Title.Caption = '$ Frete'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 72
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 861
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 813
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 597
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 575
        Height = 32
        Caption = 'Gerenciamento de Frete de Envio para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 575
        Height = 32
        Caption = 'Gerenciamento de Frete de Envio para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 575
        Height = 32
        Caption = 'Gerenciamento de Frete de Envio para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 861
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 857
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSMOEnvEnv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSMOEnvEnvBeforeOpen
    AfterOpen = QrVSMOEnvEnvAfterOpen
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT mea.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transpor,'
      'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornMO'
      'FROM vsmoenvenv mea'
      'LEFT JOIN entidades emp ON emp.Codigo=mea.CFTMP_Empresa'
      'LEFT JOIN entidades tra ON tra.Codigo=mea.CFTMP_Terceiro'
      'LEFT JOIN entidades fmo ON fmo.Codigo=mea.NFEMP_Terceiro'
      'WHERE mea.Codigo>0')
    Left = 180
    Top = 285
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEnvNFEMP_SdoPeca: TFloatField
      FieldName = 'NFEMP_SdoPeca'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEnvNFEMP_SdoPeso: TFloatField
      FieldName = 'NFEMP_SdoPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEnvNFEMP_SdoArM2: TFloatField
      FieldName = 'NFEMP_SdoArM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSMOEnvEnvNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrVSMOEnvEnvNO_Transpor: TWideStringField
      FieldName = 'NO_Transpor'
      Size = 100
    end
    object QrVSMOEnvEnvNO_FornMO: TWideStringField
      FieldName = 'NO_FornMO'
      Size = 100
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 180
    Top = 333
  end
  object QrVSMOEnvEVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mei.*'
      'FROM vsmoenvavmi mei'
      'WHERE mei.VSMOEnvAvu>0'
      '')
    Left = 272
    Top = 285
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 272
    Top = 329
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 428
    Top = 332
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 308
    Top = 428
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Visible = False
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = CabExclui1Click
    end
  end
end
