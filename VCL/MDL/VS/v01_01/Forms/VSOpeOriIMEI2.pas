unit VSOpeOriIMEI2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSOpeOriIMEI2 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaPecas: TLabel;
    LaPesoKg: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoquePecas: TFloatField;
    QrEstoquePesoKg: TFloatField;
    QrEstoqueAreaM2: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrEstoqueVSMulFrnCab: TIntegerField;
    QrEstoqueSdoVrtArM2: TFloatField;
    QrEstoqueSdoVrtArP2: TFloatField;
    QrEstoqueSdoVrtPeca: TFloatField;
    QrEstoqueSdoVrtPeso: TFloatField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel3: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    //
    procedure InsereIMEI_Atual(InsereTudo: Boolean;
              ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenVSGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    function  BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
  public
    { Public declarations }
    //FVSOpeOriIMEI, FVSOpeOriPallet,
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FFornecMO, FStqCenLoc, FOrigMovimNiv, FOrigMovimCod,
    FOrigCodigo, FNewGraGruX, FTipoArea: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSOpeOriIMEI2: TFmVSOpeOriIMEI2;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
VSOpeCab, UnVS_CRC_PF, AppListas;

{$R *.DFM}

procedure TFmVSOpeOriIMEI2.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

function TFmVSOpeOriIMEI2.BaixaIMEIParcial(): Integer;
var
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
begin
  Result := 0;
  Pecas  := EdPecas.ValueVariant;
  PesoKg := EdPesoKg.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  SobraPecas  := Pecas;
  SobraPesoKg := PesoKg;
  SobraAreaM2 := AreaM2;
  SobraAreaP2 := AreaP2;
  //
  if MyObjects.FIC(Pecas <= 0, EdPecas, 'Informe a quantidade de pe�as') then
    Exit;
  InsereIMEI_Atual(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2);
  Result := 1;
end;

function TFmVSOpeOriIMEI2.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      InsereIMEI_Atual(True, 0, 0, 0, 0);
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmVSOpeOriIMEI2.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  if FParcial then
    N := BaixaIMEIParcial()
  else
    N := BaixaIMEITotal();
  //
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSOpecab', MovimCod);
    MovimCod := FmVSOpeCab.QrVSOpeCabMovimCod.Value;
    VS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmVSOpeCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        EdControle.ValueVariant := 0;
        EdPecas.ValueVariant := 0;
        EdPesoKg.ValueVariant := 0;
        EdAreaM2.ValueVariant := 0;
        EdAreaP2.ValueVariant := 0;
        EdObserv.ValueVariant := '';
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
        LiberaEdicao(False, False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
(*
  // Nao se aplica. Calcula com function propria a seguir.
  //VS_CRC_PF.AtualizaTotaisVSXxxCab('VSOpecab', MovimCod);
  MovimCod := FmVSOpeCab.QrVSOpeCabMovimCod.Value;
  VS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmVSOpeCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
end;

procedure TFmVSOpeOriIMEI2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOpeOriIMEI2.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSOpeOriIMEI2.DefineTipoArea();
begin
  LaAreaM2.Enabled := FTipoArea = 0;
  EdAreaM2.Enabled := FTipoArea = 0;

  LaAreaP2.Enabled := FTipoArea = 1;
  EdAreaP2.Enabled := FTipoArea = 1;
end;

procedure TFmVSOpeOriIMEI2.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSOpeOriIMEI2.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSOpeOriIMEI2.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSOpeOriIMEI2.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSOpeOriIMEI2.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmVSOpeOriIMEI2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmVSOpeOriIMEI2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +
    ')');
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +
    ')');

  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
end;

procedure TFmVSOpeOriIMEI2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOpeOriIMEI2.InsereIMEI_Atual(InsereTudo: Boolean;
 ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2: Double);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieFch,
  Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX,
  ReqMovEstq, VSMulFrnCab, ClientMO, FornecMO, StqCenLoc, GGXRcl: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
begin
  SrcMovID       := QrEstoqueMovimID.Value;
  SrcNivel1      := QrEstoqueCodigo.Value;
  SrcNivel2      := QrEstoqueControle.Value;
  SrcGGX         := QrEstoqueGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  Terceiro       := QrEstoqueTerceiro.Value;
  VSMulFrnCab    := QrEstoqueVSMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmOperacao;
  MovimNiv       := eminSorcOper;
  Pallet         := QrEstoquePallet.Value;
  GraGruX        := QrEstoqueGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  if not InsereTudo then
  begin
    Pecas        := -ParcPecas;
    PesoKg       := -ParcPesoKg;
    AreaM2       := -ParcAreaM2;
    AreaP2       := -ParcAreaP2;
  end else
  begin
    Pecas          := -QrEstoquePecas.Value;
    PesoKg         := -QrEstoquePesoKg.Value;
    AreaM2         := -QrEstoqueAreaM2.Value;
    AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  end;
  if (QrEstoqueAreaM2.Value > 0) then
    Valor := -AreaM2 *
    (QrEstoqueValorT.Value / QrEstoqueAreaM2.Value)
  else
  if QrEstoquePecas.Value > 0 then
    Valor := -Pecas *
    (QrEstoqueValorT.Value / QrEstoquePecas.Value)
  else
    Valor := 0;
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := 0; //QrEstoqueSerieFch.Value;
  Ficha          := 0;  //QrEstoqueFicha.Value;
  Marca          := ''; //QrEstoqueMarca.Value;
  //Misturou       := QrEstoqueMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrEstoqueGraGruY.Value;
  //
  StqCenLoc      := FStqCenLoc;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei063(*Baixa de couro em opera��o por IMEI*)) then
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, True);
  end;
end;

procedure TFmVSOpeOriIMEI2.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled := not Status;
    EdFicha.Enabled := not Status;
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdPecas.Enabled) and (EdPecas.Visible) then
      EdPecas.SetFocus;
  end;
end;

procedure TFmVSOpeOriIMEI2.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSOpeOriIMEI2.ReopenItensAptos();
var
  GraGruX: Integer;
  SQL_GraGruX: String;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.Empresa, vmi.ClientMO, ',
  'vmi.GraGruX, vmi.PesoKg, vmi.Pecas, vmi.AreaM2, ',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
  'FLOOR((vmi.SdoVrtArM2 / 0.09290304)) + ',
  'FLOOR(((MOD((vmi.SdoVrtArM2 / 0.09290304), 1)) + ',
  '0.12499) * 4) * 0.25 SdoVrtArP2, ValorT, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
  'vmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"" NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, ',
  'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
  'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ',
  'vmi.VSMulFrnCab, vmi.SerieFch, vmi.Ficha, vmi.Marca, ',
  '1 Ativo ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
  'WHERE vmi.Controle <> 0 ',
  SQL_GraGruX,
  'AND vmi.SdoVrtPeca > 0 ',
  'AND vmi.Pallet = 0 ',
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  ' ']);
end;

end.
