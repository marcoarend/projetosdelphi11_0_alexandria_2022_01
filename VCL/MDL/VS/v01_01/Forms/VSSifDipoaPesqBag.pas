unit VSSifDipoaPesqBag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmVSSifDipoaPesqBag = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    EdNumero: TdmkEdit;
    Label1: TLabel;
    SbNumero: TSpeedButton;
    QrVSPallet: TMySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    QrVSPalletDtHrEndAdd: TDateTimeField;
    QrVSPalletDtHrEndAdd_TXT: TWideStringField;
    QrVSPalletQtdPrevPc: TIntegerField;
    QrVSPalletGerRclCab: TIntegerField;
    QrVSPalletDtHrFimRcl: TDateTimeField;
    QrVSPalletMovimIDGer: TIntegerField;
    QrVSPalletClientMO: TIntegerField;
    QrVSPalletStatPall: TIntegerField;
    QrVSPalletSeqSifDipoa: TIntegerField;
    QrVSPalletObsSifDipoa: TWideStringField;
    DsVSPallet: TDataSource;
    DBG04Estq: TdmkDBGridZTO;
    QrVSPalletDataFab: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesq();
    procedure LocalizaBag();
  public
    { Public declarations }
    FPallet: Integer;
  end;

  var
  FmVSSifDipoaPesqBag: TFmVSSifDipoaPesqBag;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSSifDipoaPesqBag.BtOKClick(Sender: TObject);
begin
  LocalizaBag();
end;

procedure TFmVSSifDipoaPesqBag.BtSaidaClick(Sender: TObject);
begin
  FPallet := 0;
  Close;
end;

procedure TFmVSSifDipoaPesqBag.DBG04EstqDblClick(Sender: TObject);
begin
  LocalizaBag();
end;

procedure TFmVSSifDipoaPesqBag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSSifDipoaPesqBag.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPallet := 0;
end;

procedure TFmVSSifDipoaPesqBag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSSifDipoaPesqBag.LocalizaBag();
begin
  if (QrVSPallet.State <> dsInactive) and
  (QrVSPallet.RecordCount > 0) then
  begin
    FPallet := QrVSPalletCodigo.Value;
    Close;
  end;
end;

procedure TFmVSSifDipoaPesqBag.ReopenPesq();
var
  Numero: Integer;
begin
  Numero := EdNumero.ValueVariant;
  if Numero <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT let.*,   ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS    ',
    'FROM vspalleta let   ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'WHERE SeqSifDipoa=' + Geral.FF0(Numero),
    'ORDER BY NO_PRD_TAM_COR ',
    '']);
  end else
    QrVSPallet.Close;
end;

procedure TFmVSSifDipoaPesqBag.SbNumeroClick(Sender: TObject);
begin
  ReopenPesq();
end;

end.
