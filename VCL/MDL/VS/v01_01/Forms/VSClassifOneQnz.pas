unit VSClassifOneQnz;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, UnInternalConsts,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkDBLookupComboBox, dmkEditCB, UnDmkWeb, ShellAPI,
  UnProjGroup_Consts, UnAppEnums, UnVS_CRC_PF;

type
  TFmVSClassifOneQnz = class(TForm)
    PanelOC: TPanel;
    PnBoxesAll: TPanel;
    PnBoxesT01: TPanel;
    PnBoxesT02: TPanel;
    PnBoxT1L1: TPanel;
    Panel2: TPanel;
    QrVSPaClaCab: TmySQLQuery;
    DsVSPaClaCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    DsVSGerArtNew: TDataSource;
    QrVSPaClaCabNO_TIPO: TWideStringField;
    QrVSPaClaCabGraGruX: TIntegerField;
    QrVSPaClaCabNome: TWideStringField;
    QrVSPaClaCabTipoArea: TSmallintField;
    QrVSPaClaCabEmpresa: TIntegerField;
    QrVSPaClaCabMovimCod: TIntegerField;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaClaCabNO_EMPRESA: TWideStringField;
    QrVSPallet01: TmySQLQuery;
    QrVSPallet01Codigo: TIntegerField;
    QrVSPallet01Nome: TWideStringField;
    QrVSPallet01Lk: TIntegerField;
    QrVSPallet01DataCad: TDateField;
    QrVSPallet01DataAlt: TDateField;
    QrVSPallet01UserCad: TIntegerField;
    QrVSPallet01UserAlt: TIntegerField;
    QrVSPallet01AlterWeb: TSmallintField;
    QrVSPallet01Ativo: TSmallintField;
    QrVSPallet01Empresa: TIntegerField;
    QrVSPallet01Status: TIntegerField;
    QrVSPallet01CliStat: TIntegerField;
    QrVSPallet01GraGruX: TIntegerField;
    QrVSPallet01NO_CLISTAT: TWideStringField;
    QrVSPallet01NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet01NO_STATUS: TWideStringField;
    DsVSPallet01: TDataSource;
    QrVSPallet02: TmySQLQuery;
    QrVSPallet02Codigo: TIntegerField;
    QrVSPallet02Nome: TWideStringField;
    QrVSPallet02Lk: TIntegerField;
    QrVSPallet02DataCad: TDateField;
    QrVSPallet02DataAlt: TDateField;
    QrVSPallet02UserCad: TIntegerField;
    QrVSPallet02UserAlt: TIntegerField;
    QrVSPallet02AlterWeb: TSmallintField;
    QrVSPallet02Ativo: TSmallintField;
    QrVSPallet02Empresa: TIntegerField;
    QrVSPallet02Status: TIntegerField;
    QrVSPallet02CliStat: TIntegerField;
    QrVSPallet02GraGruX: TIntegerField;
    QrVSPallet02NO_CLISTAT: TWideStringField;
    QrVSPallet02NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet02NO_STATUS: TWideStringField;
    DsVSPallet02: TDataSource;
    QrVSPallet03: TmySQLQuery;
    QrVSPallet03Codigo: TIntegerField;
    QrVSPallet03Nome: TWideStringField;
    QrVSPallet03Lk: TIntegerField;
    QrVSPallet03DataCad: TDateField;
    QrVSPallet03DataAlt: TDateField;
    QrVSPallet03UserCad: TIntegerField;
    QrVSPallet03UserAlt: TIntegerField;
    QrVSPallet03AlterWeb: TSmallintField;
    QrVSPallet03Ativo: TSmallintField;
    QrVSPallet03Empresa: TIntegerField;
    QrVSPallet03Status: TIntegerField;
    QrVSPallet03CliStat: TIntegerField;
    QrVSPallet03GraGruX: TIntegerField;
    QrVSPallet03NO_CLISTAT: TWideStringField;
    QrVSPallet03NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet03NO_STATUS: TWideStringField;
    DsVSPallet03: TDataSource;
    QrVSPallet04: TmySQLQuery;
    QrVSPallet04Codigo: TIntegerField;
    QrVSPallet04Nome: TWideStringField;
    QrVSPallet04Lk: TIntegerField;
    QrVSPallet04DataCad: TDateField;
    QrVSPallet04DataAlt: TDateField;
    QrVSPallet04UserCad: TIntegerField;
    QrVSPallet04UserAlt: TIntegerField;
    QrVSPallet04AlterWeb: TSmallintField;
    QrVSPallet04Ativo: TSmallintField;
    QrVSPallet04Empresa: TIntegerField;
    QrVSPallet04Status: TIntegerField;
    QrVSPallet04CliStat: TIntegerField;
    QrVSPallet04GraGruX: TIntegerField;
    QrVSPallet04NO_CLISTAT: TWideStringField;
    QrVSPallet04NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet04NO_STATUS: TWideStringField;
    DsVSPallet04: TDataSource;
    QrVSPallet05: TmySQLQuery;
    QrVSPallet05Codigo: TIntegerField;
    QrVSPallet05Nome: TWideStringField;
    QrVSPallet05Lk: TIntegerField;
    QrVSPallet05DataCad: TDateField;
    QrVSPallet05DataAlt: TDateField;
    QrVSPallet05UserCad: TIntegerField;
    QrVSPallet05UserAlt: TIntegerField;
    QrVSPallet05AlterWeb: TSmallintField;
    QrVSPallet05Ativo: TSmallintField;
    QrVSPallet05Empresa: TIntegerField;
    QrVSPallet05Status: TIntegerField;
    QrVSPallet05CliStat: TIntegerField;
    QrVSPallet05GraGruX: TIntegerField;
    QrVSPallet05NO_CLISTAT: TWideStringField;
    QrVSPallet05NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet05NO_STATUS: TWideStringField;
    DsVSPallet05: TDataSource;
    QrVSPallet06: TmySQLQuery;
    QrVSPallet06Codigo: TIntegerField;
    QrVSPallet06Nome: TWideStringField;
    QrVSPallet06Lk: TIntegerField;
    QrVSPallet06DataCad: TDateField;
    QrVSPallet06DataAlt: TDateField;
    QrVSPallet06UserCad: TIntegerField;
    QrVSPallet06UserAlt: TIntegerField;
    QrVSPallet06AlterWeb: TSmallintField;
    QrVSPallet06Ativo: TSmallintField;
    QrVSPallet06Empresa: TIntegerField;
    QrVSPallet06Status: TIntegerField;
    QrVSPallet06CliStat: TIntegerField;
    QrVSPallet06GraGruX: TIntegerField;
    QrVSPallet06NO_CLISTAT: TWideStringField;
    QrVSPallet06NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet06NO_STATUS: TWideStringField;
    DsVSPallet06: TDataSource;
    QrVSPalClaIts01: TmySQLQuery;
    QrVSPalClaIts01Controle: TIntegerField;
    QrVSPalClaIts01DtHrIni: TDateTimeField;
    DsVSPalClaIts01: TDataSource;
    QrVSPalClaIts02: TmySQLQuery;
    QrVSPalClaIts02Controle: TIntegerField;
    QrVSPalClaIts02DtHrIni: TDateTimeField;
    DsVSPalClaIts02: TDataSource;
    QrVSPalClaIts03: TmySQLQuery;
    QrVSPalClaIts03Controle: TIntegerField;
    QrVSPalClaIts03DtHrIni: TDateTimeField;
    DsVSPalClaIts03: TDataSource;
    QrVSPalClaIts04: TmySQLQuery;
    QrVSPalClaIts04Controle: TIntegerField;
    QrVSPalClaIts04DtHrIni: TDateTimeField;
    DsVSPalClaIts04: TDataSource;
    QrVSPalClaIts05: TmySQLQuery;
    QrVSPalClaIts05Controle: TIntegerField;
    QrVSPalClaIts05DtHrIni: TDateTimeField;
    DsVSPalClaIts05: TDataSource;
    QrVSPalClaIts06: TmySQLQuery;
    QrVSPalClaIts06Controle: TIntegerField;
    QrVSPalClaIts06DtHrIni: TDateTimeField;
    DsVSPalClaIts06: TDataSource;
    QrItens01: TmySQLQuery;
    DsItens01: TDataSource;
    QrSum01: TmySQLQuery;
    DsSum01: TDataSource;
    QrSum01Pecas: TFloatField;
    QrSum01AreaM2: TFloatField;
    QrSum01AreaP2: TFloatField;
    QrItens01Controle: TLargeintField;
    QrItens01Pecas: TFloatField;
    QrItens01AreaM2: TFloatField;
    QrItens01AreaP2: TFloatField;
    QrItens02: TmySQLQuery;
    QrItens02Controle: TLargeintField;
    QrItens02Pecas: TFloatField;
    QrItens02AreaM2: TFloatField;
    QrItens02AreaP2: TFloatField;
    DsItens02: TDataSource;
    QrSum02: TmySQLQuery;
    QrSum02Pecas: TFloatField;
    QrSum02AreaM2: TFloatField;
    QrSum02AreaP2: TFloatField;
    DsSum02: TDataSource;
    QrItens03: TmySQLQuery;
    QrItens03Controle: TLargeintField;
    QrItens03Pecas: TFloatField;
    QrItens03AreaM2: TFloatField;
    QrItens03AreaP2: TFloatField;
    DsItens03: TDataSource;
    QrSum03: TmySQLQuery;
    QrSum03Pecas: TFloatField;
    QrSum03AreaM2: TFloatField;
    QrSum03AreaP2: TFloatField;
    DsSum03: TDataSource;
    QrItens04: TmySQLQuery;
    QrItens04Controle: TLargeintField;
    QrItens04Pecas: TFloatField;
    QrItens04AreaM2: TFloatField;
    QrItens04AreaP2: TFloatField;
    DsItens04: TDataSource;
    QrSum04: TmySQLQuery;
    QrSum04Pecas: TFloatField;
    QrSum04AreaM2: TFloatField;
    QrSum04AreaP2: TFloatField;
    DsSum04: TDataSource;
    QrItens05: TmySQLQuery;
    QrItens05Controle: TLargeintField;
    QrItens05Pecas: TFloatField;
    QrItens05AreaM2: TFloatField;
    QrItens05AreaP2: TFloatField;
    DsItens05: TDataSource;
    QrSum05: TmySQLQuery;
    QrSum05Pecas: TFloatField;
    QrSum05AreaM2: TFloatField;
    QrSum05AreaP2: TFloatField;
    DsSum05: TDataSource;
    QrItens06: TmySQLQuery;
    QrItens06Controle: TLargeintField;
    QrItens06Pecas: TFloatField;
    QrItens06AreaM2: TFloatField;
    QrItens06AreaP2: TFloatField;
    DsItens06: TDataSource;
    QrSum06: TmySQLQuery;
    QrSum06Pecas: TFloatField;
    QrSum06AreaM2: TFloatField;
    QrSum06AreaP2: TFloatField;
    DsSum06: TDataSource;
    QrSumT_: TmySQLQuery;
    DsSumT: TDataSource;
    PnBoxT1L2: TPanel;
    DBGAll: TdmkDBGridZTO;
    QrAll: TmySQLQuery;
    QrAllControle: TLargeintField;
    QrAllPecas: TFloatField;
    QrAllAreaM2: TFloatField;
    QrAllAreaP2: TFloatField;
    DsAll: TDataSource;
    QrAllBox: TIntegerField;
    QrAllVSPaClaIts: TIntegerField;
    QrAllVSPallet: TIntegerField;
    PnBoxT1L3: TPanel;
    PnBoxT2L1: TPanel;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    Panel7: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    Panel12: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEDAreaT: TDBEdit;
    Panel13: TPanel;
    Label19: TLabel;
    Panel14: TPanel;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdit24: TDBEdit;
    PMItens01: TPopupMenu;
    EncerrarPallet01: TMenuItem;
    RemoverPallet01: TMenuItem;
    AdicionarPallet01: TMenuItem;
    PMItens02: TPopupMenu;
    AdicionarPallet02: TMenuItem;
    EncerrarPallet02: TMenuItem;
    RemoverPallet02: TMenuItem;
    QrVSPalClaIts01VMI_Baix: TIntegerField;
    QrVSPalClaIts01VMI_Dest: TIntegerField;
    QrVSPalClaIts02VMI_Dest: TIntegerField;
    QrVSPalClaIts03VMI_Dest: TIntegerField;
    QrVSPalClaIts04VMI_Dest: TIntegerField;
    QrVSPalClaIts05VMI_Dest: TIntegerField;
    QrVSPalClaIts06VMI_Dest: TIntegerField;
    QrAllVMI_Sorc: TIntegerField;
    QrAllVMI_Dest: TIntegerField;
    QrSumPal01: TmySQLQuery;
    DsSumPal01: TDataSource;
    QrSumPal01Pecas: TFloatField;
    QrSumPal01AreaM2: TFloatField;
    QrSumPal01AreaP2: TFloatField;
    QrSumPal02: TmySQLQuery;
    QrSumPal02Pecas: TFloatField;
    QrSumPal02AreaM2: TFloatField;
    QrSumPal02AreaP2: TFloatField;
    DsSumPal02: TDataSource;
    QrSumPal03: TmySQLQuery;
    QrSumPal03Pecas: TFloatField;
    QrSumPal03AreaM2: TFloatField;
    QrSumPal03AreaP2: TFloatField;
    DsSumPal03: TDataSource;
    QrSumPal04: TmySQLQuery;
    QrSumPal04Pecas: TFloatField;
    QrSumPal04AreaM2: TFloatField;
    QrSumPal04AreaP2: TFloatField;
    DsSumPal04: TDataSource;
    QrSumPal05: TmySQLQuery;
    QrSumPal05Pecas: TFloatField;
    QrSumPal05AreaM2: TFloatField;
    QrSumPal05AreaP2: TFloatField;
    DsSumPal05: TDataSource;
    QrSumPal06: TmySQLQuery;
    QrSumPal06Pecas: TFloatField;
    QrSumPal06AreaM2: TFloatField;
    QrSumPal06AreaP2: TFloatField;
    DsSumPal06: TDataSource;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    BtEncerra: TBitBtn;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    PMItens03: TPopupMenu;
    AdicionarPallet03: TMenuItem;
    EncerrarPallet03: TMenuItem;
    RemoverPallet03: TMenuItem;
    PMItens04: TPopupMenu;
    AdicionarPallet04: TMenuItem;
    EncerrarPallet04: TMenuItem;
    RemoverPallet04: TMenuItem;
    PMItens05: TPopupMenu;
    AdicionarPallet05: TMenuItem;
    EncerrarPallet05: TMenuItem;
    RemoverPallet05: TMenuItem;
    PMItens06: TPopupMenu;
    AdicionarPallet06: TMenuItem;
    EncerrarPallet06: TMenuItem;
    RemoverPallet06: TMenuItem;
    BtImprime: TBitBtn;
    BtDigitacao: TButton;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrVMIsDePal: TmySQLQuery;
    QrVMIsDePalVMI_Dest: TIntegerField;
    QrVSPalClaIts02VMI_Baix: TIntegerField;
    QrVSPalClaIts03VMI_Baix: TIntegerField;
    QrVSPalClaIts04VMI_Baix: TIntegerField;
    QrVSPalClaIts05VMI_Baix: TIntegerField;
    QrVSPalClaIts06VMI_Baix: TIntegerField;
    Panel42: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    Label22: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label24: TLabel;
    Label18: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit11: TDBEdit;
    Panel43: TPanel;
    Label71: TLabel;
    Label70: TLabel;
    CBRevisor: TdmkDBLookupComboBox;
    CBDigitador: TdmkDBLookupComboBox;
    EdDigitador: TdmkEditCB;
    EdRevisor: TdmkEditCB;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    QrVSMrtCad: TmySQLQuery;
    DsVSMrtCad: TDataSource;
    QrVSMrtCadCodigo: TIntegerField;
    QrVSMrtCadNome: TWideStringField;
    PnMartelo: TPanel;
    Label72: TLabel;
    CBVSMrtCad: TdmkDBLookupComboBox;
    EdVSMrtCad: TdmkEditCB;
    BtReabre: TBitBtn;
    QrVSPallet01QtdPrevPc: TIntegerField;
    QrVSPallet02QtdPrevPc: TIntegerField;
    QrVSPallet03QtdPrevPc: TIntegerField;
    QrVSPallet04QtdPrevPc: TIntegerField;
    QrVSPallet05QtdPrevPc: TIntegerField;
    QrVSPallet06QtdPrevPc: TIntegerField;
    Mensagemdedados1: TMenuItem;
    Mensagemdedados2: TMenuItem;
    Mensagemdedados3: TMenuItem;
    Mensagemdedados4: TMenuItem;
    Mensagemdedados5: TMenuItem;
    Mensagemdedados6: TMenuItem;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    Label85: TLabel;
    EdTempo: TEdit;
    CkMartelo: TCheckBox;
    CkSubClass: TCheckBox;
    PnSubClass: TPanel;
    Label86: TLabel;
    EdSubClass: TdmkEdit;
    Panel1: TPanel;
    PnEqualize: TPanel;
    Panel70: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    CkNota: TCheckBox;
    DBGrid2: TDBGrid;
    PnNota: TPanel;
    DBEdit42: TDBEdit;
    QrAllSubClass: TWideStringField;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnBox01: TPanel;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label54: TLabel;
    DBEdit4: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdArea1: TDBEdit;
    DBEdit50: TDBEdit;
    GroupBox2: TGroupBox;
    Panel16: TPanel;
    Label52: TLabel;
    Label55: TLabel;
    Label53: TLabel;
    Label73: TLabel;
    DBEdSumPcPal01: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit57: TDBEdit;
    EdMedia01: TdmkEdit;
    Memo1: TMemo;
    Panel4: TPanel;
    Panel8: TPanel;
    Panel11: TPanel;
    DBEdit3: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit1: TDBEdit;
    Panel50: TPanel;
    Label79: TLabel;
    Panel51: TPanel;
    Panel64: TPanel;
    CkSubClass1: TCheckBox;
    PnBox06: TPanel;
    PnBox03: TPanel;
    Panel22: TPanel;
    GroupBox5: TGroupBox;
    Panel34: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label36: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdArea3: TDBEdit;
    DBEdit61: TDBEdit;
    GroupBox6: TGroupBox;
    Panel35: TPanel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label75: TLabel;
    DBEdSumPcPal03: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    EdMedia03: TdmkEdit;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel55: TPanel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit30: TDBEdit;
    Panel56: TPanel;
    Label83: TLabel;
    Panel57: TPanel;
    Panel65: TPanel;
    CkSubClass3: TCheckBox;
    Panel25: TPanel;
    GroupBox11: TGroupBox;
    Panel40: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    DBEdit40: TDBEdit;
    DBEdSumPcIMEI06: TDBEdit;
    DBEdArea6: TDBEdit;
    DBEdit72: TDBEdit;
    GroupBox12: TGroupBox;
    Panel41: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label78: TLabel;
    DBEdSumPcPal06: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    EdMedia06: TdmkEdit;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel58: TPanel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit37: TDBEdit;
    Panel59: TPanel;
    Label84: TLabel;
    Panel60: TPanel;
    Panel66: TPanel;
    CkSubClass6: TCheckBox;
    PnBox02: TPanel;
    Panel19: TPanel;
    GroupBox3: TGroupBox;
    Panel26: TPanel;
    Label5: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    DBEdit15: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdArea2: TDBEdit;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel30: TPanel;
    Label31: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label74: TLabel;
    DBEdSumPcPal02: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    EdMedia02: TdmkEdit;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel44: TPanel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit22: TDBEdit;
    Panel45: TPanel;
    Label80: TLabel;
    Panel46: TPanel;
    Panel62: TPanel;
    CkSubClass2: TCheckBox;
    QrVSPallet01FatorInt: TFloatField;
    QrVSPallet02FatorInt: TFloatField;
    QrVSPallet03FatorInt: TFloatField;
    QrVSPallet04FatorInt: TFloatField;
    QrVSPallet05FatorInt: TFloatField;
    QrVSPallet06FatorInt: TFloatField;
    QrVSGerArtNewFatorInt: TFloatField;
    QrSumT: TmySQLQuery;
    QrSumTAreaM2: TFloatField;
    QrSumT_Pecas: TFloatField;
    QrSumT_AreaM2: TFloatField;
    QrSumT_AreaP2: TFloatField;
    QrSumT_FALTA_PECA: TFloatField;
    QrSumT_FALTA_AREA: TFloatField;
    QrSumT_IntOuPart: TFloatField;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTPecas: TFloatField;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoIMEI2: TMenuItem;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    QrAllNO_MARTELO: TWideStringField;
    Panel3: TPanel;
    PnIntMei01: TPanel;
    DBGPallet1: TdmkDBGridZTO;
    Panel67: TPanel;
    PnIntMei02: TPanel;
    DBGPallet2: TdmkDBGridZTO;
    Panel71: TPanel;
    PnIntMei06: TPanel;
    DBGPallet6: TdmkDBGridZTO;
    Panel72: TPanel;
    PnIntMei03: TPanel;
    DBGPallet3: TdmkDBGridZTO;
    Panel73: TPanel;
    PnBoxT3L3: TPanel;
    PnBoxT3L2: TPanel;
    PnBoxT3L1: TPanel;
    PnBoxT1L4: TPanel;
    PnBox04: TPanel;
    Panel33: TPanel;
    GroupBox7: TGroupBox;
    Panel36: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label51: TLabel;
    DBEdit28: TDBEdit;
    DBEdSumPcIMEI04: TDBEdit;
    DBEdArea4: TDBEdit;
    DBEdit56: TDBEdit;
    GroupBox8: TGroupBox;
    Panel37: TPanel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label76: TLabel;
    DBEdSumPcPal04: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    EdMedia04: TdmkEdit;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel47: TPanel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit51: TDBEdit;
    Panel48: TPanel;
    Label81: TLabel;
    Panel49: TPanel;
    Panel61: TPanel;
    CkSubClass4: TCheckBox;
    Panel68: TPanel;
    PnIntMei04: TPanel;
    DBGPallet4: TdmkDBGridZTO;
    PnBoxT1L5: TPanel;
    PnBox05: TPanel;
    Panel29: TPanel;
    GroupBox9: TGroupBox;
    Panel38: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label46: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdArea5: TDBEdit;
    DBEdit68: TDBEdit;
    GroupBox10: TGroupBox;
    Panel39: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label77: TLabel;
    DBEdSumPcPal05: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    EdMedia05: TdmkEdit;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel52: TPanel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit44: TDBEdit;
    Panel53: TPanel;
    Label82: TLabel;
    Panel54: TPanel;
    Panel63: TPanel;
    CkSubClass5: TCheckBox;
    Panel69: TPanel;
    PnIntMei05: TPanel;
    DBGPallet5: TdmkDBGridZTO;
    PnBoxT2L2: TPanel;
    PnBoxT2L3: TPanel;
    PnBoxT2L4: TPanel;
    PnBoxT2L5: TPanel;
    PnBoxT3L4: TPanel;
    PnBoxT3L5: TPanel;
    QrVSPallet07: TmySQLQuery;
    QrVSPallet07FatorInt: TFloatField;
    QrVSPallet07Codigo: TIntegerField;
    QrVSPallet07Nome: TWideStringField;
    QrVSPallet07Lk: TIntegerField;
    QrVSPallet07DataCad: TDateField;
    QrVSPallet07DataAlt: TDateField;
    QrVSPallet07UserCad: TIntegerField;
    QrVSPallet07UserAlt: TIntegerField;
    QrVSPallet07AlterWeb: TSmallintField;
    QrVSPallet07Ativo: TSmallintField;
    QrVSPallet07Empresa: TIntegerField;
    QrVSPallet07Status: TIntegerField;
    QrVSPallet07CliStat: TIntegerField;
    QrVSPallet07GraGruX: TIntegerField;
    QrVSPallet07NO_CLISTAT: TWideStringField;
    QrVSPallet07NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet07NO_STATUS: TWideStringField;
    QrVSPallet07QtdPrevPc: TIntegerField;
    DsVSPallet07: TDataSource;
    QrVSPalClaIts07: TmySQLQuery;
    QrVSPalClaIts07VMI_Baix: TIntegerField;
    QrVSPalClaIts07VMI_Dest: TIntegerField;
    QrVSPalClaIts07Controle: TIntegerField;
    QrVSPalClaIts07DtHrIni: TDateTimeField;
    DsVSPalClaIts07: TDataSource;
    QrItens07: TmySQLQuery;
    QrItens07Controle: TLargeintField;
    QrItens07Pecas: TFloatField;
    QrItens07AreaM2: TFloatField;
    QrItens07AreaP2: TFloatField;
    DsItens07: TDataSource;
    QrSum07: TmySQLQuery;
    QrSum07Pecas: TFloatField;
    QrSum07AreaM2: TFloatField;
    QrSum07AreaP2: TFloatField;
    DsSum07: TDataSource;
    QrSumPal07: TmySQLQuery;
    QrSumPal07Pecas: TFloatField;
    QrSumPal07AreaM2: TFloatField;
    QrSumPal07AreaP2: TFloatField;
    DsSumPal07: TDataSource;
    QrVSPallet08: TmySQLQuery;
    QrVSPallet08FatorInt: TFloatField;
    QrVSPallet08Codigo: TIntegerField;
    QrVSPallet08Nome: TWideStringField;
    QrVSPallet08Lk: TIntegerField;
    QrVSPallet08DataCad: TDateField;
    QrVSPallet08DataAlt: TDateField;
    QrVSPallet08UserCad: TIntegerField;
    QrVSPallet08UserAlt: TIntegerField;
    QrVSPallet08AlterWeb: TSmallintField;
    QrVSPallet08Ativo: TSmallintField;
    QrVSPallet08Empresa: TIntegerField;
    QrVSPallet08Status: TIntegerField;
    QrVSPallet08CliStat: TIntegerField;
    QrVSPallet08GraGruX: TIntegerField;
    QrVSPallet08NO_CLISTAT: TWideStringField;
    QrVSPallet08NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet08NO_STATUS: TWideStringField;
    QrVSPallet08QtdPrevPc: TIntegerField;
    DsVSPallet08: TDataSource;
    QrVSPalClaIts08: TmySQLQuery;
    QrVSPalClaIts08VMI_Baix: TIntegerField;
    QrVSPalClaIts08VMI_Dest: TIntegerField;
    QrVSPalClaIts08Controle: TIntegerField;
    QrVSPalClaIts08DtHrIni: TDateTimeField;
    DsVSPalClaIts08: TDataSource;
    QrItens08: TmySQLQuery;
    QrItens08Controle: TLargeintField;
    QrItens08Pecas: TFloatField;
    QrItens08AreaM2: TFloatField;
    QrItens08AreaP2: TFloatField;
    DsItens08: TDataSource;
    QrSum08: TmySQLQuery;
    QrSum08Pecas: TFloatField;
    QrSum08AreaM2: TFloatField;
    QrSum08AreaP2: TFloatField;
    DsSum08: TDataSource;
    QrSumPal08: TmySQLQuery;
    QrSumPal08Pecas: TFloatField;
    QrSumPal08AreaM2: TFloatField;
    QrSumPal08AreaP2: TFloatField;
    DsSumPal08: TDataSource;
    QrVSPallet09: TmySQLQuery;
    QrVSPallet09FatorInt: TFloatField;
    QrVSPallet09Codigo: TIntegerField;
    QrVSPallet09Nome: TWideStringField;
    QrVSPallet09Lk: TIntegerField;
    QrVSPallet09DataCad: TDateField;
    QrVSPallet09DataAlt: TDateField;
    QrVSPallet09UserCad: TIntegerField;
    QrVSPallet09UserAlt: TIntegerField;
    QrVSPallet09AlterWeb: TSmallintField;
    QrVSPallet09Ativo: TSmallintField;
    QrVSPallet09Empresa: TIntegerField;
    QrVSPallet09Status: TIntegerField;
    QrVSPallet09CliStat: TIntegerField;
    QrVSPallet09GraGruX: TIntegerField;
    QrVSPallet09NO_CLISTAT: TWideStringField;
    QrVSPallet09NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet09NO_STATUS: TWideStringField;
    QrVSPallet09QtdPrevPc: TIntegerField;
    DsVSPallet09: TDataSource;
    QrVSPalClaIts09: TmySQLQuery;
    QrVSPalClaIts09VMI_Baix: TIntegerField;
    QrVSPalClaIts09VMI_Dest: TIntegerField;
    QrVSPalClaIts09Controle: TIntegerField;
    QrVSPalClaIts09DtHrIni: TDateTimeField;
    DsVSPalClaIts09: TDataSource;
    QrItens09: TmySQLQuery;
    QrItens09Controle: TLargeintField;
    QrItens09Pecas: TFloatField;
    QrItens09AreaM2: TFloatField;
    QrItens09AreaP2: TFloatField;
    DsItens09: TDataSource;
    QrSum09: TmySQLQuery;
    QrSum09Pecas: TFloatField;
    QrSum09AreaM2: TFloatField;
    QrSum09AreaP2: TFloatField;
    DsSum09: TDataSource;
    QrSumPal09: TmySQLQuery;
    QrSumPal09Pecas: TFloatField;
    QrSumPal09AreaM2: TFloatField;
    QrSumPal09AreaP2: TFloatField;
    DsSumPal09: TDataSource;
    QrVSPallet10: TmySQLQuery;
    QrVSPallet10FatorInt: TFloatField;
    QrVSPallet10Codigo: TIntegerField;
    QrVSPallet10Nome: TWideStringField;
    QrVSPallet10Lk: TIntegerField;
    QrVSPallet10DataCad: TDateField;
    QrVSPallet10DataAlt: TDateField;
    QrVSPallet10UserCad: TIntegerField;
    QrVSPallet10UserAlt: TIntegerField;
    QrVSPallet10AlterWeb: TSmallintField;
    QrVSPallet10Ativo: TSmallintField;
    QrVSPallet10Empresa: TIntegerField;
    QrVSPallet10Status: TIntegerField;
    QrVSPallet10CliStat: TIntegerField;
    QrVSPallet10GraGruX: TIntegerField;
    QrVSPallet10NO_CLISTAT: TWideStringField;
    QrVSPallet10NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet10NO_STATUS: TWideStringField;
    QrVSPallet10QtdPrevPc: TIntegerField;
    DsVSPallet10: TDataSource;
    QrVSPalClaIts10: TmySQLQuery;
    QrVSPalClaIts10VMI_Baix: TIntegerField;
    QrVSPalClaIts10VMI_Dest: TIntegerField;
    QrVSPalClaIts10Controle: TIntegerField;
    QrVSPalClaIts10DtHrIni: TDateTimeField;
    DsVSPalClaIts10: TDataSource;
    QrItens10: TmySQLQuery;
    QrItens10Controle: TLargeintField;
    QrItens10Pecas: TFloatField;
    QrItens10AreaM2: TFloatField;
    QrItens10AreaP2: TFloatField;
    DsItens10: TDataSource;
    QrSum10: TmySQLQuery;
    QrSum10Pecas: TFloatField;
    QrSum10AreaM2: TFloatField;
    QrSum10AreaP2: TFloatField;
    DsSum10: TDataSource;
    QrSumPal10: TmySQLQuery;
    QrSumPal10Pecas: TFloatField;
    QrSumPal10AreaM2: TFloatField;
    QrSumPal10AreaP2: TFloatField;
    DsSumPal10: TDataSource;
    QrVSPallet11: TmySQLQuery;
    QrVSPallet11FatorInt: TFloatField;
    QrVSPallet11Codigo: TIntegerField;
    QrVSPallet11Nome: TWideStringField;
    QrVSPallet11Lk: TIntegerField;
    QrVSPallet11DataCad: TDateField;
    QrVSPallet11DataAlt: TDateField;
    QrVSPallet11UserCad: TIntegerField;
    QrVSPallet11UserAlt: TIntegerField;
    QrVSPallet11AlterWeb: TSmallintField;
    QrVSPallet11Ativo: TSmallintField;
    QrVSPallet11Empresa: TIntegerField;
    QrVSPallet11Status: TIntegerField;
    QrVSPallet11CliStat: TIntegerField;
    QrVSPallet11GraGruX: TIntegerField;
    QrVSPallet11NO_CLISTAT: TWideStringField;
    QrVSPallet11NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet11NO_STATUS: TWideStringField;
    QrVSPallet11QtdPrevPc: TIntegerField;
    DsVSPallet11: TDataSource;
    QrVSPalClaIts11: TmySQLQuery;
    QrVSPalClaIts11VMI_Baix: TIntegerField;
    QrVSPalClaIts11VMI_Dest: TIntegerField;
    QrVSPalClaIts11Controle: TIntegerField;
    QrVSPalClaIts11DtHrIni: TDateTimeField;
    DsVSPalClaIts11: TDataSource;
    QrItens11: TmySQLQuery;
    QrItens11Controle: TLargeintField;
    QrItens11Pecas: TFloatField;
    QrItens11AreaM2: TFloatField;
    QrItens11AreaP2: TFloatField;
    DsItens11: TDataSource;
    QrSum11: TmySQLQuery;
    QrSum11Pecas: TFloatField;
    QrSum11AreaM2: TFloatField;
    QrSum11AreaP2: TFloatField;
    DsSum11: TDataSource;
    QrSumPal11: TmySQLQuery;
    QrSumPal11Pecas: TFloatField;
    QrSumPal11AreaM2: TFloatField;
    QrSumPal11AreaP2: TFloatField;
    DsSumPal11: TDataSource;
    QrVSPallet12: TmySQLQuery;
    QrVSPallet12FatorInt: TFloatField;
    QrVSPallet12Codigo: TIntegerField;
    QrVSPallet12Nome: TWideStringField;
    QrVSPallet12Lk: TIntegerField;
    QrVSPallet12DataCad: TDateField;
    QrVSPallet12DataAlt: TDateField;
    QrVSPallet12UserCad: TIntegerField;
    QrVSPallet12UserAlt: TIntegerField;
    QrVSPallet12AlterWeb: TSmallintField;
    QrVSPallet12Ativo: TSmallintField;
    QrVSPallet12Empresa: TIntegerField;
    QrVSPallet12Status: TIntegerField;
    QrVSPallet12CliStat: TIntegerField;
    QrVSPallet12GraGruX: TIntegerField;
    QrVSPallet12NO_CLISTAT: TWideStringField;
    QrVSPallet12NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet12NO_STATUS: TWideStringField;
    QrVSPallet12QtdPrevPc: TIntegerField;
    DsVSPallet12: TDataSource;
    QrVSPalClaIts12: TmySQLQuery;
    QrVSPalClaIts12VMI_Baix: TIntegerField;
    QrVSPalClaIts12VMI_Dest: TIntegerField;
    QrVSPalClaIts12Controle: TIntegerField;
    QrVSPalClaIts12DtHrIni: TDateTimeField;
    DsVSPalClaIts12: TDataSource;
    QrItens12: TmySQLQuery;
    QrItens12Controle: TLargeintField;
    QrItens12Pecas: TFloatField;
    QrItens12AreaM2: TFloatField;
    QrItens12AreaP2: TFloatField;
    DsItens12: TDataSource;
    QrSum12: TmySQLQuery;
    QrSum12Pecas: TFloatField;
    QrSum12AreaM2: TFloatField;
    QrSum12AreaP2: TFloatField;
    DsSum12: TDataSource;
    QrSumPal12: TmySQLQuery;
    QrSumPal12Pecas: TFloatField;
    QrSumPal12AreaM2: TFloatField;
    QrSumPal12AreaP2: TFloatField;
    DsSumPal12: TDataSource;
    QrVSPallet13: TmySQLQuery;
    QrVSPallet13FatorInt: TFloatField;
    QrVSPallet13Codigo: TIntegerField;
    QrVSPallet13Nome: TWideStringField;
    QrVSPallet13Lk: TIntegerField;
    QrVSPallet13DataCad: TDateField;
    QrVSPallet13DataAlt: TDateField;
    QrVSPallet13UserCad: TIntegerField;
    QrVSPallet13UserAlt: TIntegerField;
    QrVSPallet13AlterWeb: TSmallintField;
    QrVSPallet13Ativo: TSmallintField;
    QrVSPallet13Empresa: TIntegerField;
    QrVSPallet13Status: TIntegerField;
    QrVSPallet13CliStat: TIntegerField;
    QrVSPallet13GraGruX: TIntegerField;
    QrVSPallet13NO_CLISTAT: TWideStringField;
    QrVSPallet13NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet13NO_STATUS: TWideStringField;
    QrVSPallet13QtdPrevPc: TIntegerField;
    DsVSPallet13: TDataSource;
    QrVSPalClaIts13: TmySQLQuery;
    QrVSPalClaIts13VMI_Baix: TIntegerField;
    QrVSPalClaIts13VMI_Dest: TIntegerField;
    QrVSPalClaIts13Controle: TIntegerField;
    QrVSPalClaIts13DtHrIni: TDateTimeField;
    DsVSPalClaIts13: TDataSource;
    QrItens13: TmySQLQuery;
    QrItens13Controle: TLargeintField;
    QrItens13Pecas: TFloatField;
    QrItens13AreaM2: TFloatField;
    QrItens13AreaP2: TFloatField;
    DsItens13: TDataSource;
    QrSum13: TmySQLQuery;
    QrSum13Pecas: TFloatField;
    QrSum13AreaM2: TFloatField;
    QrSum13AreaP2: TFloatField;
    DsSum13: TDataSource;
    QrSumPal13: TmySQLQuery;
    QrSumPal13Pecas: TFloatField;
    QrSumPal13AreaM2: TFloatField;
    QrSumPal13AreaP2: TFloatField;
    DsSumPal13: TDataSource;
    QrVSPallet14: TmySQLQuery;
    QrVSPallet14FatorInt: TFloatField;
    QrVSPallet14Codigo: TIntegerField;
    QrVSPallet14Nome: TWideStringField;
    QrVSPallet14Lk: TIntegerField;
    QrVSPallet14DataCad: TDateField;
    QrVSPallet14DataAlt: TDateField;
    QrVSPallet14UserCad: TIntegerField;
    QrVSPallet14UserAlt: TIntegerField;
    QrVSPallet14AlterWeb: TSmallintField;
    QrVSPallet14Ativo: TSmallintField;
    QrVSPallet14Empresa: TIntegerField;
    QrVSPallet14Status: TIntegerField;
    QrVSPallet14CliStat: TIntegerField;
    QrVSPallet14GraGruX: TIntegerField;
    QrVSPallet14NO_CLISTAT: TWideStringField;
    QrVSPallet14NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet14NO_STATUS: TWideStringField;
    QrVSPallet14QtdPrevPc: TIntegerField;
    DsVSPallet14: TDataSource;
    QrVSPalClaIts14: TmySQLQuery;
    QrVSPalClaIts14VMI_Baix: TIntegerField;
    QrVSPalClaIts14VMI_Dest: TIntegerField;
    QrVSPalClaIts14Controle: TIntegerField;
    QrVSPalClaIts14DtHrIni: TDateTimeField;
    DsVSPalClaIts14: TDataSource;
    QrItens14: TmySQLQuery;
    QrItens14Controle: TLargeintField;
    QrItens14Pecas: TFloatField;
    QrItens14AreaM2: TFloatField;
    QrItens14AreaP2: TFloatField;
    DsItens14: TDataSource;
    QrSum14: TmySQLQuery;
    QrSum14Pecas: TFloatField;
    QrSum14AreaM2: TFloatField;
    QrSum14AreaP2: TFloatField;
    DsSum14: TDataSource;
    QrSumPal14: TmySQLQuery;
    QrSumPal14Pecas: TFloatField;
    QrSumPal14AreaM2: TFloatField;
    QrSumPal14AreaP2: TFloatField;
    DsSumPal14: TDataSource;
    QrVSPallet15: TmySQLQuery;
    QrVSPallet15FatorInt: TFloatField;
    QrVSPallet15Codigo: TIntegerField;
    QrVSPallet15Nome: TWideStringField;
    QrVSPallet15Lk: TIntegerField;
    QrVSPallet15DataCad: TDateField;
    QrVSPallet15DataAlt: TDateField;
    QrVSPallet15UserCad: TIntegerField;
    QrVSPallet15UserAlt: TIntegerField;
    QrVSPallet15AlterWeb: TSmallintField;
    QrVSPallet15Ativo: TSmallintField;
    QrVSPallet15Empresa: TIntegerField;
    QrVSPallet15Status: TIntegerField;
    QrVSPallet15CliStat: TIntegerField;
    QrVSPallet15GraGruX: TIntegerField;
    QrVSPallet15NO_CLISTAT: TWideStringField;
    QrVSPallet15NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet15NO_STATUS: TWideStringField;
    QrVSPallet15QtdPrevPc: TIntegerField;
    DsVSPallet15: TDataSource;
    QrVSPalClaIts15: TmySQLQuery;
    QrVSPalClaIts15VMI_Baix: TIntegerField;
    QrVSPalClaIts15VMI_Dest: TIntegerField;
    QrVSPalClaIts15Controle: TIntegerField;
    QrVSPalClaIts15DtHrIni: TDateTimeField;
    DsVSPalClaIts15: TDataSource;
    QrItens15: TmySQLQuery;
    QrItens15Controle: TLargeintField;
    QrItens15Pecas: TFloatField;
    QrItens15AreaM2: TFloatField;
    QrItens15AreaP2: TFloatField;
    DsItens15: TDataSource;
    QrSum15: TmySQLQuery;
    QrSum15Pecas: TFloatField;
    QrSum15AreaM2: TFloatField;
    QrSum15AreaP2: TFloatField;
    DsSum15: TDataSource;
    QrSumPal15: TmySQLQuery;
    QrSumPal15Pecas: TFloatField;
    QrSumPal15AreaM2: TFloatField;
    QrSumPal15AreaP2: TFloatField;
    DsSumPal15: TDataSource;
    PnBox07: TPanel;
    Panel115: TPanel;
    GroupBox19: TGroupBox;
    Panel116: TPanel;
    Label109: TLabel;
    Label110: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    DBEdit94: TDBEdit;
    DBEdit95: TDBEdit;
    DBEdit96: TDBEdit;
    DBEdit97: TDBEdit;
    GroupBox20: TGroupBox;
    Panel117: TPanel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    DBEdSumPcPal07: TDBEdit;
    DBEdit99: TDBEdit;
    DBEdit100: TDBEdit;
    EdMedia07: TdmkEdit;
    Panel118: TPanel;
    Panel119: TPanel;
    Panel120: TPanel;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    DBEdit103: TDBEdit;
    Panel121: TPanel;
    Label117: TLabel;
    Panel122: TPanel;
    Panel123: TPanel;
    CheckBox4: TCheckBox;
    Panel124: TPanel;
    PnIntMei07: TPanel;
    dmkDBGridZTO4: TdmkDBGridZTO;
    PnBox08: TPanel;
    Panel74: TPanel;
    GroupBox21: TGroupBox;
    Panel75: TPanel;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    DBEdit49: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit88: TDBEdit;
    DBEdit98: TDBEdit;
    GroupBox22: TGroupBox;
    Panel126: TPanel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    DBEdSumPcPal08: TDBEdit;
    DBEdit104: TDBEdit;
    DBEdit105: TDBEdit;
    EdMedia08: TdmkEdit;
    Panel127: TPanel;
    Panel128: TPanel;
    Panel129: TPanel;
    DBEdit106: TDBEdit;
    DBEdit107: TDBEdit;
    DBEdit108: TDBEdit;
    Panel130: TPanel;
    Label126: TLabel;
    Panel131: TPanel;
    Panel132: TPanel;
    CheckBox5: TCheckBox;
    Panel133: TPanel;
    PnIntMei08: TPanel;
    dmkDBGridZTO5: TdmkDBGridZTO;
    PnBox09: TPanel;
    Panel137: TPanel;
    GroupBox23: TGroupBox;
    Panel138: TPanel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    DBEdit114: TDBEdit;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    DBEdit117: TDBEdit;
    GroupBox24: TGroupBox;
    Panel139: TPanel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    DBEdSumPcPal09: TDBEdit;
    DBEdit119: TDBEdit;
    DBEdit120: TDBEdit;
    EdMedia09: TdmkEdit;
    Panel140: TPanel;
    Panel141: TPanel;
    Panel142: TPanel;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    DBEdit123: TDBEdit;
    Panel143: TPanel;
    Label135: TLabel;
    Panel144: TPanel;
    Panel145: TPanel;
    CheckBox6: TCheckBox;
    Panel146: TPanel;
    PnIntMei09: TPanel;
    dmkDBGridZTO6: TdmkDBGridZTO;
    PnBox10: TPanel;
    Panel148: TPanel;
    GroupBox25: TGroupBox;
    Panel149: TPanel;
    Label136: TLabel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    DBEdit124: TDBEdit;
    DBEdit125: TDBEdit;
    DBEdit126: TDBEdit;
    DBEdit127: TDBEdit;
    GroupBox26: TGroupBox;
    Panel150: TPanel;
    Label140: TLabel;
    Label141: TLabel;
    Label142: TLabel;
    Label143: TLabel;
    DBEdSumPcPal10: TDBEdit;
    DBEdit129: TDBEdit;
    DBEdit130: TDBEdit;
    EdMedia10: TdmkEdit;
    Panel151: TPanel;
    Panel152: TPanel;
    Panel153: TPanel;
    DBEdit131: TDBEdit;
    DBEdit132: TDBEdit;
    DBEdit133: TDBEdit;
    Panel154: TPanel;
    Label144: TLabel;
    Panel155: TPanel;
    Panel156: TPanel;
    CheckBox7: TCheckBox;
    Panel157: TPanel;
    PnIntMei10: TPanel;
    dmkDBGridZTO7: TdmkDBGridZTO;
    PnBox11: TPanel;
    Panel102: TPanel;
    GroupBox17: TGroupBox;
    Panel103: TPanel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    DBEdit84: TDBEdit;
    DBEdSumPcIMEI11: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    GroupBox18: TGroupBox;
    Panel104: TPanel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    DBEdSumPcPal11: TDBEdit;
    DBEdit87: TDBEdit;
    DBEdit89: TDBEdit;
    EdMedia11: TdmkEdit;
    Panel105: TPanel;
    Panel106: TPanel;
    Panel107: TPanel;
    DBEdit90: TDBEdit;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    Panel108: TPanel;
    Label108: TLabel;
    Panel109: TPanel;
    Panel110: TPanel;
    CheckBox3: TCheckBox;
    Panel111: TPanel;
    PnIntMei11: TPanel;
    dmkDBGridZTO3: TdmkDBGridZTO;
    PnBox12: TPanel;
    Panel89: TPanel;
    GroupBox15: TGroupBox;
    Panel90: TPanel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    DBEdit69: TDBEdit;
    DBEdSumPcIMEI12: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit76: TDBEdit;
    GroupBox16: TGroupBox;
    Panel91: TPanel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    DBEdSumPcPal12: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit79: TDBEdit;
    EdMedia12: TdmkEdit;
    Panel92: TPanel;
    Panel93: TPanel;
    Panel94: TPanel;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    Panel95: TPanel;
    Label99: TLabel;
    Panel96: TPanel;
    Panel97: TPanel;
    CheckBox2: TCheckBox;
    Panel98: TPanel;
    PnIntMei12: TPanel;
    dmkDBGridZTO2: TdmkDBGridZTO;
    PnBox13: TPanel;
    Panel76: TPanel;
    GroupBox13: TGroupBox;
    Panel77: TPanel;
    Label11: TLabel;
    Label29: TLabel;
    Label35: TLabel;
    Label40: TLabel;
    DBEdit17: TDBEdit;
    DBEdSumPcIMEI13: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    GroupBox14: TGroupBox;
    Panel78: TPanel;
    Label45: TLabel;
    Label50: TLabel;
    Label87: TLabel;
    Label89: TLabel;
    DBEdSumPcPal13: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit54: TDBEdit;
    EdMedia13: TdmkEdit;
    Panel79: TPanel;
    Panel80: TPanel;
    Panel81: TPanel;
    DBEdit55: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit62: TDBEdit;
    Panel82: TPanel;
    Label90: TLabel;
    Panel83: TPanel;
    Panel84: TPanel;
    CheckBox1: TCheckBox;
    Panel85: TPanel;
    PnIntMei13: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    PnBox14: TPanel;
    Panel87: TPanel;
    GroupBox27: TGroupBox;
    Panel88: TPanel;
    Label145: TLabel;
    Label146: TLabel;
    Label147: TLabel;
    Label148: TLabel;
    DBEdit65: TDBEdit;
    DBEdSumPcIMEI14: TDBEdit;
    DBEdit83: TDBEdit;
    DBEdit93: TDBEdit;
    GroupBox28: TGroupBox;
    Panel100: TPanel;
    Label149: TLabel;
    Label150: TLabel;
    Label151: TLabel;
    Label152: TLabel;
    DBEdSumPcPal14: TDBEdit;
    DBEdit109: TDBEdit;
    DBEdit110: TDBEdit;
    EdMedia14: TdmkEdit;
    Panel101: TPanel;
    Panel113: TPanel;
    Panel114: TPanel;
    DBEdit111: TDBEdit;
    DBEdit112: TDBEdit;
    DBEdit113: TDBEdit;
    Panel135: TPanel;
    Label153: TLabel;
    Panel136: TPanel;
    Panel159: TPanel;
    CheckBox8: TCheckBox;
    Panel160: TPanel;
    PnIntMei14: TPanel;
    dmkDBGridZTO8: TdmkDBGridZTO;
    PnBox15: TPanel;
    Panel162: TPanel;
    GroupBox29: TGroupBox;
    Panel163: TPanel;
    Label154: TLabel;
    Label155: TLabel;
    Label156: TLabel;
    Label157: TLabel;
    DBEdit118: TDBEdit;
    DBEdSumPcIMEI15: TDBEdit;
    DBEdit128: TDBEdit;
    DBEdit134: TDBEdit;
    GroupBox30: TGroupBox;
    Panel164: TPanel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    DBEdSumPcPal15: TDBEdit;
    DBEdit135: TDBEdit;
    DBEdit136: TDBEdit;
    EdMedia15: TdmkEdit;
    Panel165: TPanel;
    Panel166: TPanel;
    Panel167: TPanel;
    DBEdit137: TDBEdit;
    DBEdit138: TDBEdit;
    DBEdit139: TDBEdit;
    Panel168: TPanel;
    Label162: TLabel;
    Panel169: TPanel;
    Panel170: TPanel;
    CheckBox9: TCheckBox;
    Panel171: TPanel;
    PnIntMei15: TPanel;
    dmkDBGridZTO9: TdmkDBGridZTO;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    Palletdobox07: TMenuItem;
    Palletdobox08: TMenuItem;
    Palletdobox09: TMenuItem;
    Palletdobox10: TMenuItem;
    Palletdobox11: TMenuItem;
    Palletdobox12: TMenuItem;
    Palletdobox13: TMenuItem;
    Palletdobox14: TMenuItem;
    Palletdobox15: TMenuItem;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    ImprimirNmeroPallet1: TMenuItem;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    DadosPaletsemWordPad1: TMenuItem;
    N3: TMenuItem;
    rocarVichaRMP1: TMenuItem;
    rocarIMEI1: TMenuItem;
    QrVSPaClaCabLstPal07: TIntegerField;
    QrVSPaClaCabLstPal08: TIntegerField;
    QrVSPaClaCabLstPal09: TIntegerField;
    QrVSPaClaCabLstPal10: TIntegerField;
    QrVSPaClaCabLstPal11: TIntegerField;
    QrVSPaClaCabLstPal12: TIntegerField;
    QrVSPaClaCabLstPal13: TIntegerField;
    QrVSPaClaCabLstPal14: TIntegerField;
    QrVSPaClaCabLstPal15: TIntegerField;
    QrVSGerArtNewClientMO: TIntegerField;
    QrVSGerArtNewFornecMO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaClaCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDigitacaoClick(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtReabreClick(Sender: TObject);
    procedure DBEdSumPcPal06Change(Sender: TObject);
    procedure DBEdSumPcPal05Change(Sender: TObject);
    procedure DBEdSumPcPal02Change(Sender: TObject);
    procedure DBEdSumPcPal04Change(Sender: TObject);
    procedure DBEdSumPcPal01Change(Sender: TObject);
    procedure DBEdSumPcPal03Change(Sender: TObject);
    procedure ImprimirNmeroPallet1Click(Sender: TObject);
    procedure DadosPaletsemWordPad1Click(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure rocarVichaRMP1Click(Sender: TObject);
    procedure CkSubClassClick(Sender: TObject);
    procedure CkMarteloClick(Sender: TObject);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    procedure EdSubClassExit(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure rocarIMEI1Click(Sender: TObject);
    procedure QrVSPallet01AfterOpen(DataSet: TDataSet);
    procedure QrVSGerArtNewAfterOpen(DataSet: TDataSet);
    procedure QrVSPallet02AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet03AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet04AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet05AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet06AfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoIMEI1Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoIMEI2Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure DBEdSumPcPal07Change(Sender: TObject);
    procedure DBEdSumPcPal08Change(Sender: TObject);
    procedure DBEdSumPcPal09Change(Sender: TObject);
    procedure DBEdSumPcPal10Change(Sender: TObject);
    procedure DBEdSumPcPal11Change(Sender: TObject);
    procedure DBEdSumPcPal12Change(Sender: TObject);
    procedure DBEdSumPcPal13Change(Sender: TObject);
    procedure DBEdSumPcPal14Change(Sender: TObject);
    procedure DBEdSumPcPal15Change(Sender: TObject);
    procedure QrVSPallet07AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet08AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet09AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet10AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet11AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet12AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet13AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet14AfterOpen(DataSet: TDataSet);
    procedure QrVSPallet15AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FCriando: Boolean;
    FDifTime: TDateTime;
    //
(*
    FLast_vsgerarta, FLast_vspaclacaba, FLast_LstPal01, FLast_LstPal02,
    FLast_LstPal03, FLast_LstPal04, FLast_LstPal05, FLast_LstPal06: Double;
*)
    //
    function  EncerraOC(Forca: Boolean): Boolean;
    procedure RealinhaBoxes();
    procedure ReopenVSGerArtDst();
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalClaIts:
              TmySQLQuery; Pallet: Integer; QrVSPalletFatorInt: TFloatField);
    procedure ReopenItens(VSPaClaIts, VSPallet: Integer; QrIts, QrSum,
              QrSumPal: TmySQLQuery);
    procedure InsereCouroAtual();
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure AtualizaInfoOC();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    function  BoxDeComp(Compo: TObject): Integer;
    function  ObtemDadosBox(const Box: Integer; var VSPaClaIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet, QrItens, QrSum,
              QrSumPal: TmySQLQuery): Boolean;
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure AdicionaPallet(Box: Integer);
    procedure ReopenSumPal(QrSumPal: TmySQLQuery; VSPallet: Integer);
    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure LiberaDigitacao();
    function  SubstituiOC(): Boolean;
    procedure VerificaBoxes();
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenEqualize(Automatico: Boolean);
    procedure TentarFocarEdArea();
    procedure ConfigPnSubClass();
    procedure ReconfiguraPaineisIntMei(Qry: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoIMEI();
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    //
    procedure ReopenVSPaClaCab();
  end;

var
  FmVSClassifOneQnz: TFmVSClassifOneQnz;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAdd,
  MyDBCheck, UnVS_PF, VSClaArtPrpQnz, VSPalNumImp, UnDmkProcFunc;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX_06] of Boolean;

procedure TFmVSClassifOneQnz.AdicionaPallet(Box: Integer);
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAdd, FmVSClaArtPalAdd, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAdd.FMovimIDGer              := emidClassArtXXUni;
    FmVSClaArtPalAdd.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAdd.FFornecMO                := QrVSGerArtNewFornecMO.Value;
    FmVSClaArtPalAdd.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAdd.FFornecedor              := QrVSGerArtNewTerceiro.Value;
    FmVSClaArtPalAdd.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAdd.FMovimID                 := FMovimID;
    FmVSClaArtPalAdd.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAdd.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAdd.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAdd.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAdd.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAdd.EdCodigo.ValueVariant    := QrVSPaClaCabCodigo.Value;
    FmVSClaArtPalAdd.EdSrcPallet.ValueVariant := 0;
    FmVSClaArtPalAdd.FExigeSrcPallet          := False;
    FmVSClaArtPalAdd.EdMovimCod.ValueVariant  := QrVSPaClaCabMovimCod.Value;
    FmVSClaArtPalAdd.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAdd.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAdd.FBox := Box;
    FmVSClaArtPalAdd.PnBox.Caption := Geral.FF0(Box);
    //
    FmVSClaArtPalAdd.FPal01 := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPalAdd.FPal02 := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPalAdd.FPal03 := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPalAdd.FPal04 := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPalAdd.FPal05 := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPalAdd.FPal06 := QrVSPaClaCabLstPal06.Value;
    //
    VS_PF.ReopenVSPallet(FmVSClaArtPalAdd.QrVSPallet,
      FmVSClaArtPalAdd.FEmpresa, FmVSClaArtPalAdd.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAdd.ShowModal;
    FmVSClaArtPalAdd.Destroy;
    //
    ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneQnz.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneQnz.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSClassifOneQnz.AtualizaInfoOC();
var
  AreaT: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ',
  'cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ',
  'cia.SubClass, mrt.Nome NO_MARTELO',
  'FROM vscacitsa cia',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo',
  'WHERE cia.CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY cia.Controle DESC ',
(*
  'SELECT Controle, VSPaClaIts, VSPallet, Pecas, ',
  'AreaM2, AreaP2, VMI_Sorc, VMI_Dest, Box, ',
  'SubClass',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY Controle DESC ',
*)
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
(*
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(Pecas * FatorIntDst / FatorIntSrc) IntOuPart ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
*)
  'SELECT Pecas, AreaM2, SdoVrtPeca, SdoVrtArM2',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(QrVSGerArtNewControle.Value),
  '']);
  //
(*
  if QrSumTAreaM2.Value > 0 then
  begin
    if QrVSPaClaCabTipoArea.Value = 0 then // m2
    begin
      AreaT := QrSumTAreaM2.Value;
      //
      EdPercent1.ValueVariant := QrSum1AreaM2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaM2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaM2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaM2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaM2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaM2.Value / AreaT * 100;
    end else
    begin
      AreaT := Geral.ConverteArea(QrSumTAreaM2.Value, ctM2toP2, cfQuarto);
      //
      EdPercent1.ValueVariant := QrSum1AreaP2.Value / AreaT * 100;
      EdPercent2.ValueVariant := QrSum2AreaP2.Value / AreaT * 100;
      EdPercent3.ValueVariant := QrSum3AreaP2.Value / AreaT * 100;
      EdPercent4.ValueVariant := QrSum4AreaP2.Value / AreaT * 100;
      EdPercent5.ValueVariant := QrSum5AreaP2.Value / AreaT * 100;
      EdPercent6.ValueVariant := QrSum6AreaP2.Value / AreaT * 100;
    end;
  end else
  begin
    EdPercent1.ValueVariant := 0;
    EdPercent2.ValueVariant := 0;
    EdPercent3.ValueVariant := 0;
    EdPercent4.ValueVariant := 0;
    EdPercent5.ValueVariant := 0;
    EdPercent6.ValueVariant := 0;
  end;
  //
*)
  if QrSumPal01Pecas.Value > 0 then
    EdMedia01.ValueVariant := QrSumPal01AreaM2.Value / QrSumPal01Pecas.Value
  else
    EdMedia01.ValueVariant := 0;
  //
  if QrSumPal02Pecas.Value > 0 then
    EdMedia02.ValueVariant := QrSumPal02AreaM2.Value / QrSumPal02Pecas.Value
  else
    EdMedia02.ValueVariant := 0;
  //
  if QrSumPal03Pecas.Value > 0 then
    EdMedia03.ValueVariant := QrSumPal03AreaM2.Value / QrSumPal03Pecas.Value
  else
    EdMedia03.ValueVariant := 0;
  //
  if QrSumPal04Pecas.Value > 0 then
    EdMedia04.ValueVariant := QrSumPal04AreaM2.Value / QrSumPal04Pecas.Value
  else
    EdMedia04.ValueVariant := 0;
  //
  if QrSumPal05Pecas.Value > 0 then
    EdMedia05.ValueVariant := QrSumPal05AreaM2.Value /  QrSumPal05Pecas.Value
  else
    EdMedia05.ValueVariant := 0;
  //
  if QrSumPal06Pecas.Value > 0 then
    EdMedia06.ValueVariant := QrSumPal06AreaM2.Value /  QrSumPal06Pecas.Value
  else
    EdMedia06.ValueVariant := 0;
  //
  if QrSumPal07Pecas.Value > 0 then
    EdMedia07.ValueVariant := QrSumPal07AreaM2.Value / QrSumPal07Pecas.Value
  else
    EdMedia07.ValueVariant := 0;
  //
  if QrSumPal08Pecas.Value > 0 then
    EdMedia08.ValueVariant := QrSumPal08AreaM2.Value / QrSumPal08Pecas.Value
  else
    EdMedia08.ValueVariant := 0;
  //
  if QrSumPal09Pecas.Value > 0 then
    EdMedia09.ValueVariant := QrSumPal09AreaM2.Value / QrSumPal09Pecas.Value
  else
    EdMedia09.ValueVariant := 0;
  //
  if QrSumPal10Pecas.Value > 0 then
    EdMedia10.ValueVariant := QrSumPal10AreaM2.Value / QrSumPal10Pecas.Value
  else
    EdMedia10.ValueVariant := 0;
  //
  if QrSumPal11Pecas.Value > 0 then
    EdMedia11.ValueVariant := QrSumPal11AreaM2.Value / QrSumPal11Pecas.Value
  else
    EdMedia11.ValueVariant := 0;
  //
  if QrSumPal12Pecas.Value > 0 then
    EdMedia12.ValueVariant := QrSumPal12AreaM2.Value / QrSumPal12Pecas.Value
  else
    EdMedia12.ValueVariant := 0;
  //
  if QrSumPal13Pecas.Value > 0 then
    EdMedia13.ValueVariant := QrSumPal13AreaM2.Value / QrSumPal13Pecas.Value
  else
    EdMedia13.ValueVariant := 0;
  //
  if QrSumPal14Pecas.Value > 0 then
    EdMedia14.ValueVariant := QrSumPal14AreaM2.Value / QrSumPal14Pecas.Value
  else
    EdMedia14.ValueVariant := 0;
  //
  if QrSumPal15Pecas.Value > 0 then
    EdMedia15.ValueVariant := QrSumPal15AreaM2.Value / QrSumPal15Pecas.Value
  else
    EdMedia15.ValueVariant := 0;
  //
end;

function TFmVSClassifOneQnz.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  Result := Geral.IMV(CompName[Length(CompName)]);
end;

function TFmVSClassifOneQnz.BoxInBoxes(Box: Integer): Boolean;
(*
var
  I: Integer;
*)
begin
(*
  Result := False;
  if Box <> 0 then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if FBoxes[I] then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
*)
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX_06) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSClassifOneQnz.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSClassifOneQnz.BtImprimeClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, 4);
end;

procedure TFmVSClassifOneQnz.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneQnz.CkMarteloClick(Sender: TObject);
begin
  PnMartelo.Visible := CkMartelo.Checked;
end;

procedure TFmVSClassifOneQnz.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSClassifOneQnz.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneQnz.CkSubClassClick(Sender: TObject);
begin
  ConfigPnSubClass();
end;

procedure TFmVSClassifOneQnz.ConfigPnSubClass();
begin
  if not CkSubClass.Checked then
    CkSubClass.Checked :=
    (CkSubClass1.Checked and PnBox01.Visible) or
    (CkSubClass2.Checked and PnBox02.Visible)  or
    (CkSubClass3.Checked and PnBox03.Visible)  or
    (CkSubClass4.Checked and PnBox04.Visible)  or
    (CkSubClass5.Checked and PnBox05.Visible)  or
    (CkSubClass6.Checked and PnBox06.Visible) ;
  //
  PnSubClass.Visible := CkSubClass.Checked;
end;

procedure TFmVSClassifOneQnz.DadosPaletsemWordPad1Click(Sender: TObject);
(*
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    ReAviso.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
*)
var
  Dir, NomeArq, Arq: String;
begin
  Dir := 'C:\Dermatek\OCsCapt\';
  NomeArq := 'OC' + DBEdCodigo.Text +
    Geral.SoNumero_TT(Geral.FDT(DModG.ObtemAgora(), 109)) + '.jpg';
  try
    Arq := DmkWeb.CapturaTela(Dir, NomeArq);
    ShellExecute(Application.Handle, 'open', pchar(Arq), nil, nil, sw_ShowNormal);
  except
    ForceDirectories(Dir);
    raise;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal01Change(Sender: TObject);
begin
  if (QrSumPal01.State <> dsInactive) and (QrVSPAllet01.State <> dsInactive) then
  begin
    if QrSumPal01Pecas.Value >= QrVSPAllet01QtdPrevPc.Value then
    begin
      DBEdSumPcPal01.Font.Style := DBEdSumPcPal01.Font.Style + [fsBold];
      DBEdSumPcPal01.Font.Color := clRed;
      DBEdSumPcPal01.Color := clYellow;
    end else
    begin
      DBEdSumPcPal01.Font.Style := DBEdSumPcPal01.Font.Style - [fsBold];
      DBEdSumPcPal01.Font.Color := clWindowText;
      DBEdSumPcPal01.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal02Change(Sender: TObject);
begin
  if (QrSumPal02.State <> dsInactive) and (QrVSPAllet02.State <> dsInactive) then
  begin
    if QrSumPal02Pecas.Value >= QrVSPAllet02QtdPrevPc.Value then
    begin
      DBEdSumPcPal02.Font.Style := DBEdSumPcPal02.Font.Style + [fsBold];
      DBEdSumPcPal02.Font.Color := clRed;
      DBEdSumPcPal02.Color := clYellow;
    end else
    begin
      DBEdSumPcPal02.Font.Style := DBEdSumPcPal02.Font.Style - [fsBold];
      DBEdSumPcPal02.Font.Color := clWindowText;
      DBEdSumPcPal02.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal03Change(Sender: TObject);
begin
  if (QrSumPal03.State <> dsInactive) and (QrVSPAllet03.State <> dsInactive) then
  begin
    if QrSumPal03Pecas.Value >= QrVSPAllet03QtdPrevPc.Value then
    begin
      DBEdSumPcPal03.Font.Style := DBEdSumPcPal03.Font.Style + [fsBold];
      DBEdSumPcPal03.Font.Color := clRed;
      DBEdSumPcPal03.Color := clYellow;
    end else
    begin
      DBEdSumPcPal03.Font.Style := DBEdSumPcPal03.Font.Style - [fsBold];
      DBEdSumPcPal03.Font.Color := clWindowText;
      DBEdSumPcPal03.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal04Change(Sender: TObject);
begin
  if (QrSumPal04.State <> dsInactive) and (QrVSPAllet04.State <> dsInactive) then
  begin
    if QrSumPal04Pecas.Value >= QrVSPAllet04QtdPrevPc.Value then
    begin
      DBEdSumPcPal04.Font.Style := DBEdSumPcPal04.Font.Style + [fsBold];
      DBEdSumPcPal04.Font.Color := clRed;
      DBEdSumPcPal04.Color := clYellow;
    end else
    begin
      DBEdSumPcPal04.Font.Style := DBEdSumPcPal04.Font.Style - [fsBold];
      DBEdSumPcPal04.Font.Color := clWindowText;
      DBEdSumPcPal04.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal05Change(Sender: TObject);
begin
  if (QrSumPal05.State <> dsInactive) and (QrVSPAllet05.State <> dsInactive) then
  begin
    if QrSumPal05Pecas.Value >= QrVSPAllet05QtdPrevPc.Value then
    begin
      DBEdSumPcPal05.Font.Style := DBEdSumPcPal05.Font.Style + [fsBold];
      DBEdSumPcPal05.Font.Color := clRed;
      DBEdSumPcPal05.Color := clYellow;
    end else
    begin
      DBEdSumPcPal05.Font.Style := DBEdSumPcPal05.Font.Style - [fsBold];
      DBEdSumPcPal05.Font.Color := clWindowText;
      DBEdSumPcPal05.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal06Change(Sender: TObject);
begin
  if (QrSumPal06.State <> dsInactive) and (QrVSPAllet06.State <> dsInactive) then
  begin
    if QrSumPal06Pecas.Value >= QrVSPAllet06QtdPrevPc.Value then
    begin
      DBEdSumPcPal06.Font.Style := DBEdSumPcPal06.Font.Style + [fsBold];
      DBEdSumPcPal06.Font.Color := clRed;
      DBEdSumPcPal06.Color := clYellow;
    end else
    begin
      DBEdSumPcPal06.Font.Style := DBEdSumPcPal06.Font.Style - [fsBold];
      DBEdSumPcPal06.Font.Color := clWindowText;
      DBEdSumPcPal06.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal07Change(Sender: TObject);
begin
  if (QrSumPal07.State <> dsInactive) and (QrVSPAllet07.State <> dsInactive) then
  begin
    if QrSumPal07Pecas.Value >= QrVSPAllet07QtdPrevPc.Value then
    begin
      DBEdSumPcPal07.Font.Style := DBEdSumPcPal07.Font.Style + [fsBold];
      DBEdSumPcPal07.Font.Color := clRed;
      DBEdSumPcPal07.Color := clYellow;
    end else
    begin
      DBEdSumPcPal07.Font.Style := DBEdSumPcPal07.Font.Style - [fsBold];
      DBEdSumPcPal07.Font.Color := clWindowText;
      DBEdSumPcPal07.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal08Change(Sender: TObject);
begin
  if (QrSumPal08.State <> dsInactive) and (QrVSPAllet08.State <> dsInactive) then
  begin
    if QrSumPal08Pecas.Value >= QrVSPAllet08QtdPrevPc.Value then
    begin
      DBEdSumPcPal08.Font.Style := DBEdSumPcPal08.Font.Style + [fsBold];
      DBEdSumPcPal08.Font.Color := clRed;
      DBEdSumPcPal08.Color := clYellow;
    end else
    begin
      DBEdSumPcPal08.Font.Style := DBEdSumPcPal08.Font.Style - [fsBold];
      DBEdSumPcPal08.Font.Color := clWindowText;
      DBEdSumPcPal08.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal09Change(Sender: TObject);
begin
  if (QrSumPal09.State <> dsInactive) and (QrVSPAllet09.State <> dsInactive) then
  begin
    if QrSumPal09Pecas.Value >= QrVSPAllet09QtdPrevPc.Value then
    begin
      DBEdSumPcPal09.Font.Style := DBEdSumPcPal09.Font.Style + [fsBold];
      DBEdSumPcPal09.Font.Color := clRed;
      DBEdSumPcPal09.Color := clYellow;
    end else
    begin
      DBEdSumPcPal09.Font.Style := DBEdSumPcPal09.Font.Style - [fsBold];
      DBEdSumPcPal09.Font.Color := clWindowText;
      DBEdSumPcPal09.Color := clWindow;
    end;
  end;

end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal10Change(Sender: TObject);
begin
  if (QrSumPal10.State <> dsInactive) and (QrVSPAllet10.State <> dsInactive) then
  begin
    if QrSumPal10Pecas.Value >= QrVSPAllet10QtdPrevPc.Value then
    begin
      DBEdSumPcPal10.Font.Style := DBEdSumPcPal10.Font.Style + [fsBold];
      DBEdSumPcPal10.Font.Color := clRed;
      DBEdSumPcPal10.Color := clYellow;
    end else
    begin
      DBEdSumPcPal10.Font.Style := DBEdSumPcPal10.Font.Style - [fsBold];
      DBEdSumPcPal10.Font.Color := clWindowText;
      DBEdSumPcPal10.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal11Change(Sender: TObject);
begin
  if (QrSumPal11.State <> dsInactive) and (QrVSPAllet11.State <> dsInactive) then
  begin
    if QrSumPal11Pecas.Value >= QrVSPAllet11QtdPrevPc.Value then
    begin
      DBEdSumPcPal11.Font.Style := DBEdSumPcPal11.Font.Style + [fsBold];
      DBEdSumPcPal11.Font.Color := clRed;
      DBEdSumPcPal11.Color := clYellow;
    end else
    begin
      DBEdSumPcPal11.Font.Style := DBEdSumPcPal11.Font.Style - [fsBold];
      DBEdSumPcPal11.Font.Color := clWindowText;
      DBEdSumPcPal11.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal12Change(Sender: TObject);
begin
  if (QrSumPal12.State <> dsInactive) and (QrVSPAllet12.State <> dsInactive) then
  begin
    if QrSumPal12Pecas.Value >= QrVSPAllet12QtdPrevPc.Value then
    begin
      DBEdSumPcPal12.Font.Style := DBEdSumPcPal12.Font.Style + [fsBold];
      DBEdSumPcPal12.Font.Color := clRed;
      DBEdSumPcPal12.Color := clYellow;
    end else
    begin
      DBEdSumPcPal12.Font.Style := DBEdSumPcPal12.Font.Style - [fsBold];
      DBEdSumPcPal12.Font.Color := clWindowText;
      DBEdSumPcPal12.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal13Change(Sender: TObject);
begin
  if (QrSumPal13.State <> dsInactive) and (QrVSPAllet13.State <> dsInactive) then
  begin
    if QrSumPal13Pecas.Value >= QrVSPAllet13QtdPrevPc.Value then
    begin
      DBEdSumPcPal13.Font.Style := DBEdSumPcPal13.Font.Style + [fsBold];
      DBEdSumPcPal13.Font.Color := clRed;
      DBEdSumPcPal13.Color := clYellow;
    end else
    begin
      DBEdSumPcPal13.Font.Style := DBEdSumPcPal13.Font.Style - [fsBold];
      DBEdSumPcPal13.Font.Color := clWindowText;
      DBEdSumPcPal13.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal14Change(Sender: TObject);
begin
  if (QrSumPal14.State <> dsInactive) and (QrVSPAllet14.State <> dsInactive) then
  begin
    if QrSumPal14Pecas.Value >= QrVSPAllet14QtdPrevPc.Value then
    begin
      DBEdSumPcPal14.Font.Style := DBEdSumPcPal14.Font.Style + [fsBold];
      DBEdSumPcPal14.Font.Color := clRed;
      DBEdSumPcPal14.Color := clYellow;
    end else
    begin
      DBEdSumPcPal14.Font.Style := DBEdSumPcPal14.Font.Style - [fsBold];
      DBEdSumPcPal14.Font.Color := clWindowText;
      DBEdSumPcPal14.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.DBEdSumPcPal15Change(Sender: TObject);
begin
  if (QrSumPal15.State <> dsInactive) and (QrVSPAllet15.State <> dsInactive) then
  begin
    if QrSumPal15Pecas.Value >= QrVSPAllet15QtdPrevPc.Value then
    begin
      DBEdSumPcPal15.Font.Style := DBEdSumPcPal15.Font.Style + [fsBold];
      DBEdSumPcPal15.Font.Color := clRed;
      DBEdSumPcPal15.Color := clYellow;
    end else
    begin
      DBEdSumPcPal15.Font.Style := DBEdSumPcPal15.Font.Style - [fsBold];
      DBEdSumPcPal15.Font.Color := clWindowText;
      DBEdSumPcPal15.Color := clWindow;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.BtDigitacaoClick(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Ant, Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
  Boxes: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdit21.Text;
  //
  if InputQuery('Itens a classificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    Boxes := 0;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    begin
      if FBoxes[I] then
        Boxes := Boxes + 1;
    end;
    //
    Ant := 0;
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Trunc(Random(1000) / 166.666); //  Random(6)
        if BoxInBoxes(Box) then
        begin
          if ((Boxes > 2) and (Ant <> Box))
          or (Boxes <=2) then
          begin
            OK := True;
            EdBox.ValueVariant := Box;
            Ant := Box;
          end;
        end;
      end;
      if QrSumTSdoVrtPeca.Value > 1 then
      begin
        Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
        Randomize;
        Desvio := Random(3000);
        Fator := 1 + ((Desvio - 1500) / 10000);
        Area := Area * Fator;
      end else
        Area := QrSumTSdoVrtArM2.Value;
      EdArea.ValueVariant := Area * 100;
      InsereCouroAtual();
    end;
  end;
end;

procedure TFmVSClassifOneQnz.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
  //if (EdBox.ValueVariant > 0) and (EdBox.ValueVariant <= VAR_CLA_ART_RIB_MAX_BOX) then
    if PnSubClass.Visible then
      EdSubClass.SetFocus
    else
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end else
  if (EdBox.Text = '-') then
    UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneQnz.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneQnz.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneQnz.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSClassifOneQnz.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSClassifOneQnz.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
begin
(*
  if Key = VK_RETURN then
  begin
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
*)
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
    end;
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
end;

procedure TFmVSClassifOneQnz.EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereCouroAtual();
end;

function TFmVSClassifOneQnz.EncerraOC(Forca: Boolean): Boolean;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
var
  DtHrFimCla: String;
  Codigo, I, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
begin
  Result := False;
  if Forca or (Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES) then
  begin
    { DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaClaCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    }
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    Codigo     := QrVSGerArtNewCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      //'DtHrFimCla'], ['Codigo'], [DtHrFimCla
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06
      ], [Codigo], True);
(*
      then
      begin
        if not SubstituiOC() then
          Close;
      end;
*)
    end;
  end;

end;

function TFmVSClassifOneQnz.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  FromBox: Variant;
begin
  Result := False;
  //
  FromBox := Box;
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  Result := VS_PF.EncerraPalletNew(VSPallet, Pergunta);
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneQnz.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneQnz.EstaOCOrdemdeclassificao1Click(Sender: TObject);
begin
  if EncerraOC(False) then
        if not SubstituiOC() then
          Close;
end;

procedure TFmVSClassifOneQnz.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneQnz.UpdDelCouroSelecionado(SQLTipo: TSQLType);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt: String;
  Continua: Boolean;
  //
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaClaIts := QrAllVSPaClaIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      Pecas  := 1;
      //
      if QrVSPaClaCabTipoArea.Value = 0 then
        Txt := FloatToStr(QrAllAreaM2.Value * 100)
      else
        Txt := FloatToStr(QrAllAreaP2.Value * 100);
      //
      if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
      begin
        AreaM2 := Geral.IMV(Txt);
        //
        if QrVSPaClaCabTipoArea.Value = 0 then
        begin
          AreaM2 := AreaM2 / 100;
          AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
        end else
        begin
          AreaP2 := AreaM2 / 100;
          AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
        end;
        Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                      'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                      [Controle], False);
      end;
    end;
    stDel:
    begin
      Continua := VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle,
      Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
    Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
    //
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    //
    ReopenItens(All_VSPaClaIts, All_VSPallet, QrItens, QrSum, QrSumPal);
    AtualizaInfoOC();
  end;
end;

procedure TFmVSClassifOneQnz.VerificaBoxes();
begin
(*
  if not Mostrartodosboxes1.Checked then
  begin
    PnBoxT1L1.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBoxT1L2.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBoxT1L3.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBoxT2L1.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBoxT2L2.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBoxT2L3.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBoxT1L1.Visible := True;
    PnBoxT1L2.Visible := True;
    PnBoxT1L3.Visible := True;
    PnBoxT2L1.Visible := True;
    PnBoxT2L2.Visible := True;
    PnBoxT2L3.Visible := True;
  end;
*)
  if not Mostrartodosboxes1.Checked then
  begin
    PnBox01.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBox02.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBox03.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBox04.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBox05.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBox06.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBox01.Visible := True;
    PnBox02.Visible := True;
    PnBox03.Visible := True;
    PnBox04.Visible := True;
    PnBox05.Visible := True;
    PnBox06.Visible := True;
  end;
{
  if ((PnBoxT1L3.Visible = False) or (PnBoxT1L1.Visible = False)) and (PnBoxT1L2.Visible = True) then
    PnBoxT1L2.Align := alLeft
  else
    PnBoxT1L2.Align := alClient;
  //
  if ((PnBoxT2L3.Visible = False) or (PnBoxT2L1.Visible = False)) and (PnBoxT2L2.Visible = True) then
    PnBoxT2L2.Align := alLeft
  else
    PnBoxT2L2.Align := alClient;
  //
}
  RealinhaBoxes();
end;

procedure TFmVSClassifOneQnz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSClassifOneQnz.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriando := True;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
    FBoxes[I] := False;
  RealinhaBoxes();
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
  //
  DBGAll.PopupMenu := PMAll;
  //
  BtDigitacao.Enabled := VAR_USUARIO = -1;
end;

procedure TFmVSClassifOneQnz.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_MULTIPLY then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
  end;
end;

procedure TFmVSClassifOneQnz.FormResize(Sender: TObject);
begin
  RealinhaBoxes();
end;

procedure TFmVSClassifOneQnz.ImprimeFluxoDeMovimentoDoIMEI();
begin
  VS_PF.MostraRelatoroFluxoIMEI(QrVSGerArtNewControle.Value);
end;

procedure TFmVSClassifOneQnz.ImprimirfluxodemovimentodoIMEI1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneQnz.ImprimirfluxodemovimentodoIMEI2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneQnz.ImprimirNmeroPallet1Click(Sender: TObject);
  procedure ConfigBox(Box: Integer; QrVSPallet: TmySQLQuery; Check: TCheckBox);
  var
    Pallet: Integer;
  begin
    Pallet := QrVSPallet.FieldByName('Codigo').AsInteger;
    if (QrVSPallet01.State = dsInactive) then
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box);
      Check.Enabled := False;
      FmVSPalNumImp.Boxes[Box] := 0;
    end else
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box) + ' - Pallet ' + Geral.FF0(Pallet);
      Check.Enabled := True;
      FmVSPalNumImp.Boxes[Box] := Pallet;
    end;
  end;
begin
{
  if DBCheck.CriaFm(TFmVSPalNumImp, FmVSPalNumImp, afmoNegarComAviso) then
  begin
    ConfigBox(01, QrVSPallet01, FmVSPalNumImp.Ck01);
    ConfigBox(02, QrVSPallet02, FmVSPalNumImp.Ck02);
    ConfigBox(03, QrVSPallet03, FmVSPalNumImp.Ck03);
    ConfigBox(04, QrVSPallet04, FmVSPalNumImp.Ck04);
    ConfigBox(05, QrVSPallet05, FmVSPalNumImp.Ck05);
    ConfigBox(06, QrVSPallet06, FmVSPalNumImp.Ck06);
    ConfigBox(07, QrVSPallet07, FmVSPalNumImp.Ck07);
    ConfigBox(08, QrVSPallet08, FmVSPalNumImp.Ck08);
    ConfigBox(09, QrVSPallet09, FmVSPalNumImp.Ck09);
    ConfigBox(10, QrVSPallet10, FmVSPalNumImp.Ck10);
    ConfigBox(11, QrVSPallet11, FmVSPalNumImp.Ck11);
    ConfigBox(12, QrVSPallet12, FmVSPalNumImp.Ck12);
    ConfigBox(13, QrVSPallet13, FmVSPalNumImp.Ck13);
    ConfigBox(14, QrVSPallet14, FmVSPalNumImp.Ck14);
    ConfigBox(15, QrVSPallet15, FmVSPalNumImp.Ck15);
    //
    FmVSPalNumImp.ShowModal;
    FmVSPalNumImp.Destroy;
  end;
}
end;

procedure TFmVSClassifOneQnz.InsereCouroAtual;
var
  VSPaClaIts: Integer;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Tempo: TDateTime;
var
  DataHora, SubClass: String;
  CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri, VSPaRclIts, VSPallet, VMI_Sorc,
  VMI_Baix, VMI_Dest, Box, Revisor, Digitador, Martelo: Integer;
  Controle: Int64;
  Pecas, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
begin
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  //
  if MyObjects.FIC(Revisor = 0, EdRevisor, 'Informe o classificador!') then
    Exit;
  if MyObjects.FIC(Digitador = 0, EdDigitador, 'Informe o digitador!') then
    Exit;
  //
  CacCod         := FCacCod;
  CacID          := Integer(emidClassArtXXUni);
  Codigo         := FCodigo;
  Controle       := 0;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  //
  if PnMartelo.Visible then
  begin
    if CBVSMrtCad.KeyValue = QrVSMrtCadNome.Value then
      Martelo := QrVSMrtCadCodigo.Value
    else
      Martelo := 0;
  end else
    Martelo := 0;
  //
  //VSPaClaIts     := ;   ver abaixo!!!
  VSPaRclIts     := 0;
  //VSPallet       := ;
  //VMI_Sorc       := ;
  //VMI_Dest       := ;
  Box            := EdBox.ValueVariant;
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  DataHora       := Geral.FDT(Now() + FDifTime, 109);
  if PnSubClass.Visible then
    SubClass     := dmkPF.SoTextoLayout(EdSubClass.Text)
  else
    SubClass     := '';
//begin
  if EdArea.ValueVariant < 0.01 then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
    Exit;
  end;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    Pecas          := 1;
    //
    if QrVSPaClaCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
      Exit;
    if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
      Exit;
    //
    //
    FatorIntSrc    := QrVSGerArtNewFatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
   //
    VMI_Sorc := QrVSGerArtNewControle.Value;
    //VMI_Dest :=
    Controle   := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);
    ClaAPalOri := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'Martelo', CO_DATA_HORA_GRL,
    'SubClass', 'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix, VMI_Dest,
    Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, Martelo, DataHora,
    SubClass, FatorIntSrc, FatorIntDst], [
    Controle], False) then
    begin
      Tempo := Now();
      VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
        QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      Tempo := Now() - Tempo;
      EdTempo.Text := FormatDateTime('ss:zzz', Tempo);
      //
      EdSubClass.Text     := '';
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
      //
      ReopenItens(VSPaClaIts, VSPallet, QrItens, QrSum, QrSumPal);
      AtualizaInfoOC();
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSClassifOneQnz.LiberaDigitacao();
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  BtDigitacao.Visible := Libera;
end;

procedure TFmVSClassifOneQnz.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  QrSum, QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
begin
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
  case Box of
    1:
    begin
      QrSum     := QrSum01;
      QrSumPal  := QrSumPal01;
      //EdPercent := EdPercent01;
      EdMedia   := EdMedia01;
    end;
    2:
    begin
      QrSum     := QrSum02;
      QrSumPal  := QrSumPal02;
      //EdPercent := EdPercent02;
      EdMedia   := EdMedia02;
    end;
    3:
    begin
      QrSum     := QrSum03;
      QrSumPal  := QrSumPal03;
      //EdPercent := EdPercent03;
      EdMedia   := EdMedia03;
    end;
    4:
    begin
      QrSum     := QrSum04;
      QrSumPal  := QrSumPal04;
      //EdPercent := EdPercent04;
      EdMedia   := EdMedia04;
    end;
    5:
    begin
      QrSum     := QrSum05;
      QrSumPal  := QrSumPal05;
      //EdPercent := EdPercent05;
      EdMedia   := EdMedia05;
    end;
    6:
    begin
      QrSum     := QrSum06;
      QrSumPal  := QrSumPal06;
      //EdPercent := EdPercent06;
      EdMedia   := EdMedia06;
    end;
    7:
    begin
      QrSum     := QrSum07;
      QrSumPal  := QrSumPal07;
      //EdPercent := EdPercent07;
      EdMedia   := EdMedia07;
    end;
    8:
    begin
      QrSum     := QrSum08;
      QrSumPal  := QrSumPal08;
      //EdPercent := EdPercent08;
      EdMedia   := EdMedia08;
    end;
    9:
    begin
      QrSum     := QrSum09;
      QrSumPal  := QrSumPal09;
      //EdPercent := EdPercent09;
      EdMedia   := EdMedia09;
    end;
    10:
    begin
      QrSum     := QrSum10;
      QrSumPal  := QrSumPal10;
      //EdPercent := EdPercent10;
      EdMedia   := EdMedia10;
    end;
    11:
    begin
      QrSum     := QrSum11;
      QrSumPal  := QrSumPal11;
      //EdPercent := EdPercent11;
      EdMedia   := EdMedia11;
    end;
    12:
    begin
      QrSum     := QrSum12;
      QrSumPal  := QrSumPal12;
      //EdPercent := EdPercent12;
      EdMedia   := EdMedia12;
    end;
    13:
    begin
      QrSum     := QrSum13;
      QrSumPal  := QrSumPal13;
      //EdPercent := EdPercent13;
      EdMedia   := EdMedia13;
    end;
    14:
    begin
      QrSum     := QrSum14;
      QrSumPal  := QrSumPal14;
      //EdPercent := EdPercent14;
      EdMedia   := EdMedia14;
    end;
    15:
    begin
      QrSum     := QrSum15;
      QrSumPal  := QrSumPal15;
      //EdPercent := EdPercent15;
      EdMedia   := EdMedia15;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaClaCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O DA FICHA RMP:  ' + QrVSGerArtNewNO_Ficha.Value);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
end;

procedure TFmVSClassifOneQnz.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSClassifOneQnz.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSClassifOneQnz.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  case Box of
    1: Result := CkSubClass1.Checked;
    2: Result := CkSubClass2.Checked;
    3: Result := CkSubClass3.Checked;
    4: Result := CkSubClass4.Checked;
    5: Result := CkSubClass5.Checked;
    6: Result := CkSubClass6.Checked;
  end;
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSClassifOneQnz.ObtemDadosBox(const Box: Integer; var
VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
begin
  VMI_Sorc := QrVSGerArtNewControle.Value;
  //
  case Box of
    01:
    begin
      VSPaClaIts := QrVSPalClaIts01Controle.Value;
      VSPallet   := QrVSPallet01Codigo.Value;
      VMI_Baix   := QrVSPalClaIts01VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts01VMI_Dest.Value;
    end;
    02:
    begin
      VSPaClaIts := QrVSPalClaIts02Controle.Value;
      VSPallet   := QrVSPallet02Codigo.Value;
      VMI_Baix   := QrVSPalClaIts02VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts02VMI_Dest.Value;
    end;
    03:
    begin
      VSPaClaIts := QrVSPalClaIts03Controle.Value;
      VSPallet   := QrVSPallet03Codigo.Value;
      VMI_Baix   := QrVSPalClaIts03VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts03VMI_Dest.Value;
    end;
    04:
    begin
      VSPaClaIts := QrVSPalClaIts04Controle.Value;
      VSPallet   := QrVSPallet04Codigo.Value;
      VMI_Baix   := QrVSPalClaIts04VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts04VMI_Dest.Value;
    end;
    05:
    begin
      VSPaClaIts := QrVSPalClaIts05Controle.Value;
      VSPallet   := QrVSPallet05Codigo.Value;
      VMI_Baix   := QrVSPalClaIts05VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts05VMI_Dest.Value;
    end;
    06:
    begin
      VSPaClaIts := QrVSPalClaIts06Controle.Value;
      VSPallet   := QrVSPallet06Codigo.Value;
      VMI_Baix   := QrVSPalClaIts06VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts06VMI_Dest.Value;
    end;
    07:
    begin
      VSPaClaIts := QrVSPalClaIts07Controle.Value;
      VSPallet   := QrVSPallet07Codigo.Value;
      VMI_Baix   := QrVSPalClaIts07VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts07VMI_Dest.Value;
    end;
    08:
    begin
      VSPaClaIts := QrVSPalClaIts08Controle.Value;
      VSPallet   := QrVSPallet08Codigo.Value;
      VMI_Baix   := QrVSPalClaIts08VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts08VMI_Dest.Value;
    end;
    09:
    begin
      VSPaClaIts := QrVSPalClaIts09Controle.Value;
      VSPallet   := QrVSPallet09Codigo.Value;
      VMI_Baix   := QrVSPalClaIts09VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts09VMI_Dest.Value;
    end;
    10:
    begin
      VSPaClaIts := QrVSPalClaIts10Controle.Value;
      VSPallet   := QrVSPallet10Codigo.Value;
      VMI_Baix   := QrVSPalClaIts10VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts10VMI_Dest.Value;
    end;
    11:
    begin
      VSPaClaIts := QrVSPalClaIts11Controle.Value;
      VSPallet   := QrVSPallet11Codigo.Value;
      VMI_Baix   := QrVSPalClaIts11VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts11VMI_Dest.Value;
    end;
    12:
    begin
      VSPaClaIts := QrVSPalClaIts12Controle.Value;
      VSPallet   := QrVSPallet12Codigo.Value;
      VMI_Baix   := QrVSPalClaIts12VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts12VMI_Dest.Value;
    end;
    13:
    begin
      VSPaClaIts := QrVSPalClaIts13Controle.Value;
      VSPallet   := QrVSPallet13Codigo.Value;
      VMI_Baix   := QrVSPalClaIts13VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts13VMI_Dest.Value;
    end;
    14:
    begin
      VSPaClaIts := QrVSPalClaIts14Controle.Value;
      VSPallet   := QrVSPallet14Codigo.Value;
      VMI_Baix   := QrVSPalClaIts14VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts14VMI_Dest.Value;
    end;
    15:
    begin
      VSPaClaIts := QrVSPalClaIts15Controle.Value;
      VSPallet   := QrVSPallet15Codigo.Value;
      VMI_Baix   := QrVSPalClaIts15VMI_Baix.Value;
      VMI_Dest   := QrVSPalClaIts15VMI_Dest.Value;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemDadosBox"');
      Exit;
    end;
  end;
  Result := True;
end;

function TFmVSClassifOneQnz.ObtemQryesBox(const Box: Integer; var QrVSPallet,
  QrItens, QrSum, QrSumPal: TmySQLQuery): Boolean;
begin
  case Box of
    01:
    begin
      QrVSPallet := QrVSPallet01;
      QrItens    := QrItens01;
      QrSum      := QrSum01;
      QrSumPal   := QrSumPal01;
    end;
    02:
    begin
      QrVSPallet := QrVSPallet02;
      QrItens    := QrItens02;
      QrSum      := QrSum02;
      QrSumPal   := QrSumPal02;
    end;
    03:
    begin
      QrVSPallet := QrVSPallet03;
      QrItens    := QrItens03;
      QrSum      := QrSum03;
      QrSumPal   := QrSumPal03;
    end;
    04:
    begin
      QrVSPallet := QrVSPallet04;
      QrItens    := QrItens04;
      QrSum      := QrSum04;
      QrSumPal   := QrSumPal04;
    end;
    05:
    begin
      QrVSPallet := QrVSPallet05;
      QrItens    := QrItens05;
      QrSum      := QrSum05;
      QrSumPal   := QrSumPal05;
    end;
    06:
    begin
      QrVSPallet := QrVSPallet06;
      QrItens    := QrItens06;
      QrSum      := QrSum06;
      QrSumPal   := QrSumPal06;
    end;
    07:
    begin
      QrVSPallet := QrVSPallet07;
      QrItens    := QrItens07;
      QrSum      := QrSum07;
      QrSumPal   := QrSumPal07;
    end;
    08:
    begin
      QrVSPallet := QrVSPallet08;
      QrItens    := QrItens08;
      QrSum      := QrSum08;
      QrSumPal   := QrSumPal08;
    end;
    09:
    begin
      QrVSPallet := QrVSPallet09;
      QrItens    := QrItens09;
      QrSum      := QrSum09;
      QrSumPal   := QrSumPal09;
    end;
    10:
    begin
      QrVSPallet := QrVSPallet10;
      QrItens    := QrItens10;
      QrSum      := QrSum10;
      QrSumPal   := QrSumPal10;
    end;
    11:
    begin
      QrVSPallet := QrVSPallet11;
      QrItens    := QrItens11;
      QrSum      := QrSum11;
      QrSumPal   := QrSumPal11;
    end;
    12:
    begin
      QrVSPallet := QrVSPallet12;
      QrItens    := QrItens12;
      QrSum      := QrSum12;
      QrSumPal   := QrSumPal12;
    end;
    13:
    begin
      QrVSPallet := QrVSPallet13;
      QrItens    := QrItens13;
      QrSum      := QrSum13;
      QrSumPal   := QrSumPal13;
    end;
    14:
    begin
      QrVSPallet := QrVSPallet14;
      QrItens    := QrItens14;
      QrSum      := QrSum14;
      QrSumPal   := QrSumPal14;
    end;
    15:
    begin
      QrVSPallet := QrVSPallet15;
      QrItens    := QrItens15;
      QrSum      := QrSum15;
      QrSumPal   := QrSumPal15;
    end;
    else
    begin
      Result := False;
      Geral.MB_Aviso('Box n�o implementado! "ObtemQryesBox"');
      Exit;
    end;
  end;
  Result := True;
end;

procedure TFmVSClassifOneQnz.PMEncerraPopup(Sender: TObject);
begin
  Palletdobox01.Enabled :=
    (QrVSPallet01.State <> dsInactive) and (QrVSPallet01.RecordCount > 0);
  Palletdobox02.Enabled :=
    (QrVSPallet02.State <> dsInactive) and (QrVSPallet02.RecordCount > 0);
  Palletdobox03.Enabled :=
    (QrVSPallet03.State <> dsInactive) and (QrVSPallet03.RecordCount > 0);
  Palletdobox04.Enabled :=
    (QrVSPallet04.State <> dsInactive) and (QrVSPallet04.RecordCount > 0);
  Palletdobox05.Enabled :=
    (QrVSPallet05.State <> dsInactive) and (QrVSPallet05.RecordCount > 0);
  Palletdobox06.Enabled :=
    (QrVSPallet06.State <> dsInactive) and (QrVSPallet06.RecordCount > 0);
  Palletdobox07.Enabled :=
    (QrVSPallet07.State <> dsInactive) and (QrVSPallet07.RecordCount > 0);
  Palletdobox08.Enabled :=
    (QrVSPallet08.State <> dsInactive) and (QrVSPallet08.RecordCount > 0);
  Palletdobox09.Enabled :=
    (QrVSPallet09.State <> dsInactive) and (QrVSPallet09.RecordCount > 0);
  Palletdobox10.Enabled :=
    (QrVSPallet10.State <> dsInactive) and (QrVSPallet10.RecordCount > 0);
  Palletdobox11.Enabled :=
    (QrVSPallet11.State <> dsInactive) and (QrVSPallet11.RecordCount > 0);
  Palletdobox12.Enabled :=
    (QrVSPallet12.State <> dsInactive) and (QrVSPallet12.RecordCount > 0);
  Palletdobox13.Enabled :=
    (QrVSPallet13.State <> dsInactive) and (QrVSPallet13.RecordCount > 0);
  Palletdobox14.Enabled :=
    (QrVSPallet14.State <> dsInactive) and (QrVSPallet14.RecordCount > 0);
  Palletdobox15.Enabled :=
    (QrVSPallet15.State <> dsInactive) and (QrVSPallet15.RecordCount > 0);
end;

procedure TFmVSClassifOneQnz.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  //AdicionarPallet1.Enabled := (QrVSPallet1.State = dsInactive) or (QrVSPallet1.RecordCount = 0);
  //MyObjects.HabilitaMenuItemCabUpd(EncerrarPallet1, QrVSPallet1);
  //MyObjects.HabilitaMenuItemCabDel(RemoverPallet1, QrVSPallet1, QrItens1);
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemItsDel(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSClassifOneQnz.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    //0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    0: QrSumTJaFoi_AREA.Value := QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value;
    //1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    1: QrSumTJaFoi_AREA.Value := Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  //QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
  QrSumTJaFoi_PECA.Value := QrSumTPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSClassifOneQnz.QrVSGerArtNewAfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet01, PnIntMei01);
  ReconfiguraPaineisIntMei(QrVSPallet02, PnIntMei02);
  ReconfiguraPaineisIntMei(QrVSPallet03, PnIntMei03);
  ReconfiguraPaineisIntMei(QrVSPallet04, PnIntMei04);
  ReconfiguraPaineisIntMei(QrVSPallet05, PnIntMei05);
  ReconfiguraPaineisIntMei(QrVSPallet06, PnIntMei06);
  ReconfiguraPaineisIntMei(QrVSPallet07, PnIntMei07);
  ReconfiguraPaineisIntMei(QrVSPallet08, PnIntMei08);
  ReconfiguraPaineisIntMei(QrVSPallet09, PnIntMei09);
  ReconfiguraPaineisIntMei(QrVSPallet10, PnIntMei10);
  ReconfiguraPaineisIntMei(QrVSPallet11, PnIntMei11);
  ReconfiguraPaineisIntMei(QrVSPallet12, PnIntMei12);
  ReconfiguraPaineisIntMei(QrVSPallet13, PnIntMei13);
  ReconfiguraPaineisIntMei(QrVSPallet14, PnIntMei14);
  ReconfiguraPaineisIntMei(QrVSPallet15, PnIntMei15);
end;

procedure TFmVSClassifOneQnz.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
var
  Campo: String;
  I: Integer;
begin
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_06 do
  begin
    if QrVSPaClaCab.FieldByName(VS_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  case QrVSPaClaCabTipoArea.Value of
    0: Campo := 'AreaM2';
    1: Campo := 'AreaP2';
    else Campo := 'Area?2';
  end;
  DBGAll.Columns[2].FieldName := Campo;
  //
  DBGPallet1.Columns[0].FieldName := Campo;
  DBGPallet2.Columns[0].FieldName := Campo;
  DBGPallet3.Columns[0].FieldName := Campo;
  DBGPallet4.Columns[0].FieldName := Campo;
  DBGPallet5.Columns[0].FieldName := Campo;
  DBGPallet6.Columns[0].FieldName := Campo;
  //
  DBEdArea1.DataField := Campo;
  DBEdArea2.DataField := Campo;
  DBEdArea3.DataField := Campo;
  DBEdArea4.DataField := Campo;
  DBEdArea5.DataField := Campo;
  DBEdArea6.DataField := Campo;
  //
  //DBEDAreaT.DataField := Campo;
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  ReopenVSPallet(01, QrVSPallet01, QrVSPalClaIts01, QrVSPaClaCabLstPal01.Value, QrVSPallet01FatorInt);
  ReopenVSPallet(02, QrVSPallet02, QrVSPalClaIts02, QrVSPaClaCabLstPal02.Value, QrVSPallet02FatorInt);
  ReopenVSPallet(03, QrVSPallet03, QrVSPalClaIts03, QrVSPaClaCabLstPal03.Value, QrVSPallet03FatorInt);
  ReopenVSPallet(04, QrVSPallet04, QrVSPalClaIts04, QrVSPaClaCabLstPal04.Value, QrVSPallet04FatorInt);
  ReopenVSPallet(05, QrVSPallet05, QrVSPalClaIts05, QrVSPaClaCabLstPal05.Value, QrVSPallet05FatorInt);
  ReopenVSPallet(06, QrVSPallet06, QrVSPalClaIts06, QrVSPaClaCabLstPal06.Value, QrVSPallet06FatorInt);
  ReopenVSPallet(07, QrVSPallet07, QrVSPalClaIts07, QrVSPaClaCabLstPal07.Value, QrVSPallet07FatorInt);
  ReopenVSPallet(08, QrVSPallet08, QrVSPalClaIts08, QrVSPaClaCabLstPal08.Value, QrVSPallet08FatorInt);
  ReopenVSPallet(09, QrVSPallet09, QrVSPalClaIts09, QrVSPaClaCabLstPal09.Value, QrVSPallet09FatorInt);
  ReopenVSPallet(10, QrVSPallet10, QrVSPalClaIts10, QrVSPaClaCabLstPal10.Value, QrVSPallet10FatorInt);
  ReopenVSPallet(11, QrVSPallet11, QrVSPalClaIts11, QrVSPaClaCabLstPal11.Value, QrVSPallet11FatorInt);
  ReopenVSPallet(12, QrVSPallet12, QrVSPalClaIts12, QrVSPaClaCabLstPal12.Value, QrVSPallet12FatorInt);
  ReopenVSPallet(13, QrVSPallet13, QrVSPalClaIts13, QrVSPaClaCabLstPal13.Value, QrVSPallet13FatorInt);
  ReopenVSPallet(14, QrVSPallet14, QrVSPalClaIts14, QrVSPaClaCabLstPal14.Value, QrVSPallet14FatorInt);
  ReopenVSPallet(15, QrVSPallet15, QrVSPalClaIts15, QrVSPaClaCabLstPal15.Value, QrVSPallet15FatorInt);
  //
  ReopenItens(QrVSPalClaIts01Controle.Value, QrVSPallet01Codigo.Value, QrItens01, QrSum01, QrSumPal01);
  ReopenItens(QrVSPalClaIts02Controle.Value, QrVSPallet02Codigo.Value, QrItens02, QrSum02, QrSumPal02);
  ReopenItens(QrVSPalClaIts03Controle.Value, QrVSPallet03Codigo.Value, QrItens03, QrSum03, QrSumPal03);
  ReopenItens(QrVSPalClaIts04Controle.Value, QrVSPallet04Codigo.Value, QrItens04, QrSum04, QrSumPal04);
  ReopenItens(QrVSPalClaIts05Controle.Value, QrVSPallet05Codigo.Value, QrItens05, QrSum05, QrSumPal05);
  ReopenItens(QrVSPalClaIts06Controle.Value, QrVSPallet06Codigo.Value, QrItens06, QrSum06, QrSumPal06);
  ReopenItens(QrVSPalClaIts07Controle.Value, QrVSPallet07Codigo.Value, QrItens07, QrSum07, QrSumPal07);
  ReopenItens(QrVSPalClaIts08Controle.Value, QrVSPallet08Codigo.Value, QrItens08, QrSum08, QrSumPal08);
  ReopenItens(QrVSPalClaIts09Controle.Value, QrVSPallet09Codigo.Value, QrItens09, QrSum09, QrSumPal09);
  ReopenItens(QrVSPalClaIts10Controle.Value, QrVSPallet10Codigo.Value, QrItens10, QrSum10, QrSumPal10);
  ReopenItens(QrVSPalClaIts11Controle.Value, QrVSPallet11Codigo.Value, QrItens11, QrSum11, QrSumPal11);
  ReopenItens(QrVSPalClaIts12Controle.Value, QrVSPallet12Codigo.Value, QrItens12, QrSum12, QrSumPal12);
  ReopenItens(QrVSPalClaIts13Controle.Value, QrVSPallet13Codigo.Value, QrItens13, QrSum13, QrSumPal13);
  ReopenItens(QrVSPalClaIts14Controle.Value, QrVSPallet14Codigo.Value, QrItens14, QrSum14, QrSumPal14);
  ReopenItens(QrVSPalClaIts15Controle.Value, QrVSPallet15Codigo.Value, QrItens15, QrSum15, QrSumPal15);
  //
  AtualizaInfoOC();
  //
end;

procedure TFmVSClassifOneQnz.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
begin
  PnBox01.Visible := True;
  PnBox02.Visible := True;
  PnBox03.Visible := True;
  PnBox04.Visible := True;
  PnBox05.Visible := True;
  PnBox06.Visible := True;
  //
  QrVSGerArtNew.Close;
  //
  QrVSPallet01.Close;
  QrVSPallet02.Close;
  QrVSPallet03.Close;
  QrVSPallet04.Close;
  QrVSPallet05.Close;
  QrVSPallet06.Close;
  QrVSPallet07.Close;
  QrVSPallet08.Close;
  QrVSPallet09.Close;
  QrVSPallet10.Close;
  QrVSPallet11.Close;
  QrVSPallet12.Close;
  QrVSPallet13.Close;
  QrVSPallet14.Close;
  QrVSPallet15.Close;
  //
  QrVSPalClaIts01.Close;
  QrVSPalClaIts02.Close;
  QrVSPalClaIts03.Close;
  QrVSPalClaIts04.Close;
  QrVSPalClaIts05.Close;
  QrVSPalClaIts06.Close;
  QrVSPalClaIts07.Close;
  QrVSPalClaIts08.Close;
  QrVSPalClaIts09.Close;
  QrVSPalClaIts10.Close;
  QrVSPalClaIts11.Close;
  QrVSPalClaIts12.Close;
  QrVSPalClaIts13.Close;
  QrVSPalClaIts14.Close;
  QrVSPalClaIts15.Close;
  //
  QrItens01.Close;
  QrItens02.Close;
  QrItens03.Close;
  QrItens04.Close;
  QrItens05.Close;
  QrItens06.Close;
  QrItens07.Close;
  QrItens08.Close;
  QrItens09.Close;
  QrItens10.Close;
  QrItens11.Close;
  QrItens12.Close;
  QrItens13.Close;
  QrItens14.Close;
  QrItens15.Close;
  //
  QrSum01.Close;
  QrSum02.Close;
  QrSum03.Close;
  QrSum04.Close;
  QrSum05.Close;
  QrSum06.Close;
  QrSum07.Close;
  QrSum08.Close;
  QrSum09.Close;
  QrSum10.Close;
  QrSum11.Close;
  QrSum12.Close;
  QrSum13.Close;
  QrSum14.Close;
  QrSum15.Close;
  //
end;

procedure TFmVSClassifOneQnz.QrVSPaClaCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    0: QrVSPaClaCabNO_TIPO.Value := 'm�';
    1: QrVSPaClaCabNO_TIPO.Value := 'ft�';
    else QrVSPaClaCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaClaCabNO_TIPO.Value;
end;

procedure TFmVSClassifOneQnz.QrVSPallet01AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet01, PnIntMei01);
end;

procedure TFmVSClassifOneQnz.QrVSPallet02AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet02, PnIntMei02);
end;

procedure TFmVSClassifOneQnz.QrVSPallet03AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet03, PnIntMei03);
end;

procedure TFmVSClassifOneQnz.QrVSPallet04AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet04, PnIntMei04);
end;

procedure TFmVSClassifOneQnz.QrVSPallet05AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet05, PnIntMei05);
end;

procedure TFmVSClassifOneQnz.QrVSPallet06AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet06, PnIntMei06);
end;

procedure TFmVSClassifOneQnz.QrVSPallet07AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet07, PnIntMei07);
end;

procedure TFmVSClassifOneQnz.QrVSPallet08AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet08, PnIntMei08);
end;

procedure TFmVSClassifOneQnz.QrVSPallet09AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet09, PnIntMei09);
end;

procedure TFmVSClassifOneQnz.QrVSPallet10AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet10, PnIntMei10);
end;

procedure TFmVSClassifOneQnz.QrVSPallet11AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet11, PnIntMei11);
end;

procedure TFmVSClassifOneQnz.QrVSPallet12AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet12, PnIntMei12);
end;

procedure TFmVSClassifOneQnz.QrVSPallet13AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet13, PnIntMei13);
end;

procedure TFmVSClassifOneQnz.QrVSPallet14AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet14, PnIntMei14);
end;

procedure TFmVSClassifOneQnz.QrVSPallet15AfterOpen(DataSet: TDataSet);
begin
  ReconfiguraPaineisIntMei(QrVSPallet15, PnIntMei15);
end;

procedure TFmVSClassifOneQnz.RealinhaBoxes();
var
  H_T, W_T, H_1, W_1: Integer;
begin
{
  H_T := PnBoxesAll.Height;
  W_T := PnBoxesAll.Width;
  //
  H_1 := H_T div 2;
  W_1 := W_T div 3;
  //
  PnBoxesT01.Height := H_1;
  //PnBoxesT02.Height := H_1;

  PnBoxT1L1.Width := W_1;
  PnBoxT1L2.Width := W_1;
  PnBoxT1L3.Width := W_1;
  PnBoxT1L4.Width := W_1;
  PnBoxT1L5.Width := W_1;

  PnBoxT2L1.Width := W_1;
  PnBoxT2L2.Width := W_1;
  PnBoxT2L3.Width := W_1;
}
end;

procedure TFmVSClassifOneQnz.ReconfiguraPaineisIntMei(Qry: TmySQLQuery;
Panel: TPanel);
var
  Habilita: Boolean;
begin
  Habilita := Qry.FieldByName('FatorInt').AsFloat < QrVSGerArtNewFatorInt.Value;
  if Habilita <> Panel.Visible then
    Panel.Visible := Habilita;
end;

procedure TFmVSClassifOneQnz.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo: String;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, Codigo: Integer;
  DtHrFim: String;
begin
  Campo := VS_PF.CampoLstPal(Box);
  Codigo := QrVSPaClaCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  if VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaClaIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaClaCab();
      end;
    end;
  end;
end;

procedure TFmVSClassifOneQnz.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneQnz.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
end;

procedure TFmVSClassifOneQnz.ReopenItens(VSPaClaIts, VSPallet: Integer; QrIts,
  QrSum, QrSumPal: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  'ORDER BY Controle DESC ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaClaIts=' + Geral.FF0(VSPaClaIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  '']);
  //
  ReopenSumPal(QrSumPal, VSPallet);
end;

procedure TFmVSClassifOneQnz.ReopenSumPal(QrSumPal: TmySQLQuery;
  VSPallet: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPal, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
  '']);
end;

procedure TFmVSClassifOneQnz.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, cn1.FatorInt ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND vmi.MovimCod=' + Geral.FF0(QrVSPaClaCabMovimCod.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSClassifOneQnz.ReopenVSPaClaCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vspaclacaba pcc',
  'LEFT JOIN vsgerarta  vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
end;

procedure TFmVSClassifOneQnz.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalClaIts: TmySQLQuery; Pallet: Integer; QrVSPalletFatorInt: TFloatField);
begin
  if Pallet > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
    'SELECT let.*, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
    ' CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt ',
    'FROM vspalleta let ',
    'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE let.Codigo=' + Geral.FF0(Pallet),
    '']);
    if (QrVSPallet.RecordCount > 0) and (QrVSPalletFatorInt.Value <= 0) then
    begin
      if Tecla = 0 then
        Geral.MB_Aviso(
        'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!'
        + sLineBreak + 'Para evitar erros de estoque esta janela ser� fechada!');
        Close;
        Exit;
    end;
    //
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalClaIts, Dmod.MyDB, [
    'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
    'FROM vspaclaitsa ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    'AND Tecla=' + Geral.FF0(Tecla),
    'AND VSPallet=' + Geral.FF0(Pallet),
    'ORDER BY DtHrIni DESC, Controle DESC ',
    'LIMIT 1 ',
    '']);
  end else
  begin
    QrVSPallet.Close;
  end;
  //
end;

procedure TFmVSClassifOneQnz.rocarIMEI1Click(Sender: TObject);
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
begin
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  VS_PF.MostraFormVSClassifOneRetIMEI_06(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
end;

procedure TFmVSClassifOneQnz.rocarVichaRMP1Click(Sender: TObject);
(*
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
*)
begin
(* Form removido para eviatr erro de usuario na ecolha do IMEI!
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  //
  VS_PF.MostraFormVSClassifOneRetFichaRMP(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
*)
end;

procedure TFmVSClassifOneQnz.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

function TFmVSClassifOneQnz.SubstituiOC(): Boolean;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSClaArtPrpQnz, FmVSClaArtPrpQnz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpQnz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpQnz.EdPallet01.ValueVariant := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpQnz.EdPallet02.ValueVariant := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpQnz.EdPallet03.ValueVariant := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpQnz.EdPallet04.ValueVariant := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpQnz.EdPallet05.ValueVariant := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpQnz.EdPallet06.ValueVariant := QrVSPaClaCabLstPal06.Value;
    FmVSClaArtPrpQnz.EdPallet07.ValueVariant := QrVSPaClaCabLstPal07.Value;
    FmVSClaArtPrpQnz.EdPallet08.ValueVariant := QrVSPaClaCabLstPal08.Value;
    FmVSClaArtPrpQnz.EdPallet09.ValueVariant := QrVSPaClaCabLstPal09.Value;
    FmVSClaArtPrpQnz.EdPallet10.ValueVariant := QrVSPaClaCabLstPal10.Value;
    FmVSClaArtPrpQnz.EdPallet11.ValueVariant := QrVSPaClaCabLstPal11.Value;
    FmVSClaArtPrpQnz.EdPallet12.ValueVariant := QrVSPaClaCabLstPal12.Value;
    FmVSClaArtPrpQnz.EdPallet13.ValueVariant := QrVSPaClaCabLstPal13.Value;
    FmVSClaArtPrpQnz.EdPallet14.ValueVariant := QrVSPaClaCabLstPal14.Value;
    FmVSClaArtPrpQnz.EdPallet15.ValueVariant := QrVSPaClaCabLstPal15.Value;
    //
    FmVSClaArtPrpQnz.CBPallet01.KeyValue := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpQnz.CBPallet02.KeyValue := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpQnz.CBPallet03.KeyValue := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpQnz.CBPallet04.KeyValue := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpQnz.CBPallet05.KeyValue := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpQnz.CBPallet06.KeyValue := QrVSPaClaCabLstPal06.Value;
    FmVSClaArtPrpQnz.CBPallet07.KeyValue := QrVSPaClaCabLstPal07.Value;
    FmVSClaArtPrpQnz.CBPallet08.KeyValue := QrVSPaClaCabLstPal08.Value;
    FmVSClaArtPrpQnz.CBPallet09.KeyValue := QrVSPaClaCabLstPal09.Value;
    FmVSClaArtPrpQnz.CBPallet10.KeyValue := QrVSPaClaCabLstPal10.Value;
    FmVSClaArtPrpQnz.CBPallet11.KeyValue := QrVSPaClaCabLstPal11.Value;
    FmVSClaArtPrpQnz.CBPallet12.KeyValue := QrVSPaClaCabLstPal12.Value;
    FmVSClaArtPrpQnz.CBPallet13.KeyValue := QrVSPaClaCabLstPal13.Value;
    FmVSClaArtPrpQnz.CBPallet14.KeyValue := QrVSPaClaCabLstPal14.Value;
    FmVSClaArtPrpQnz.CBPallet15.KeyValue := QrVSPaClaCabLstPal15.Value;
    //
    FmVSClaArtPrpQnz.ShowModal;
    Codigo := FmVSClaArtPrpQnz.FCodigo;
    CacCod := FmVSClaArtPrpQnz.FCacCod;
    MovimID := FmVSClaArtPrpQnz.FMovimID;
    FmVSClaArtPrpQnz.Destroy;
    //
    if Codigo <> 0 then
    begin
      FCodigo := Codigo;
      FCacCod := CacCod;
      FMovimID := MovimID;
      ReopenVSPaClaCab();
      //
      Result := True;
    end else
      Result := False;
  end;
end;

procedure TFmVSClassifOneQnz.TentarFocarEdArea();
begin
  ConfigPnSubClass();
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

(*

*)
end.
