unit VSOutPeso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, AppListas, BlueDermConsts, dmkDBGridZTO, UnAppEnums;

type
  TFmVSOutPeso = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    DBGVMI: TdmkDBGridZTO;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    RGTipoCouro: TRadioGroup;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    GroupBox3: TGroupBox;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsZerado: TSmallintField;
    QrVSMovItsEmFluxo: TSmallintField;
    QrVSMovItsNotFluxo: TIntegerField;
    QrVSMovItsFatNotaVNC: TFloatField;
    QrVSMovItsFatNotaVRC: TFloatField;
    QrVSMovItsLnkIDXtr: TIntegerField;
    QrVSMovSum: TmySQLQuery;
    QrVSMovSumSdoVrtPeso: TFloatField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    QrCambioMda: TMySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    PnQtd: TPanel;
    Label19: TLabel;
    Label48: TLabel;
    Label12: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    EdPedItsVda: TdmkEditCB;
    CBPedItsVda: TdmkDBLookupComboBox;
    EdItemNFe: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label18: TLabel;
    EdValorT: TdmkEdit;
    DsVSPedIts: TDataSource;
    QrVSPedIts: TMySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    QrVSPedItsGraGruX: TIntegerField;
    QrVSPedItsPrecoTipo: TSmallintField;
    QrVSPedItsPrecoMoeda: TIntegerField;
    QrVSPedItsPrecoVal: TFloatField;
    QrVSPedItsSaldoQtd: TFloatField;
    QrVSPedItsPercDesco: TFloatField;
    QrVSPedItsPercTrib: TFloatField;
    Label22: TLabel;
    EdPrecoMoeda: TdmkEditCB;
    CBPrecoMoeda: TdmkDBLookupComboBox;
    EdPrecoVal: TdmkEdit;
    Label4: TLabel;
    Label9: TLabel;
    EdMdaTotal: TdmkEdit;
    EdPercDesco: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    EdPercTrib: TdmkEdit;
    EdMdaBruto: TdmkEdit;
    Label17: TLabel;
    Label10: TLabel;
    EdMdaLiqui: TdmkEdit;
    EdCambio: TdmkEdit;
    Label13: TLabel;
    EdNacTotal: TdmkEdit;
    Label14: TLabel;
    Label23: TLabel;
    EdNacBruto: TdmkEdit;
    EdNacLiqui: TdmkEdit;
    Label24: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXChange(Sender: TObject);
    procedure DBGVMIDblClick(Sender: TObject);
    procedure RGTipoCouroClick(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrVSMovItsBeforeClose(DataSet: TDataSet);
    procedure QrVSMovItsAfterOpen(DataSet: TDataSet);
    procedure DBGVMIAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure EdPrecoMoedaRedefinido(Sender: TObject);
    procedure EdPrecoValKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercDescoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPedItsVdaRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure EdPrecoValRedefinido(Sender: TObject);
    procedure EdPercDescoRedefinido(Sender: TObject);
    procedure EdPercTribRedefinido(Sender: TObject);
    procedure EdCambioRedefinido(Sender: TObject);
    procedure EdItemNFeRedefinido(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    FMultiplo: Boolean;
    //
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure ReopenVSMovIts((*GraGruX: Integer*));
    procedure ReopenGGXY();
{
    function  BaixaEstoqueMultiplo((*Pallet,*) Codigo, MovimCod, Empresa,
              ClientMO, FornecMO: Integer; BxaPecas, BxaPesoKg, BxaAreaM2,
              BxaAreaP2, BxaValorT: Double): Boolean;
    function  BaixaPesoKgParciais(InsPesoKg, InsPecas, InsAreaM2, InsAreaP2:
              Double): Boolean;
}
    procedure DefineDadosItemPedido();
    procedure CalculaValorFaturado();
    procedure InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2, PesoKg:
              Double);
    procedure DefineGGXRcl();

  public
    { Public declarations }
    FParcial: Boolean;
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FPedido: Integer;
  end;

  var
  FmVSOutPeso: TFmVSOutPeso;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
VSOutCab, UnVS_CRC_PF, StqCenLoc, UnVS_PF;

{$R *.DFM}

procedure TFmVSOutPeso.BtOKClick(Sender: TObject);
var
  Codigo, MovimCod, Empresa, ClientMO, FornecMO, GGXRcl, ItemNFe: Integer;
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
  Tem: Boolean;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
  OK: Boolean;
function BaixaPesoKgParciais(InsPesoKg, InsPecas, InsAreaM2,
  InsAreaP2: Double): Boolean;
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  Pallet     = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  //PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
//
JmpMovID: TEstqMovimID = emidAjuste;
JmpNivel1: Integer = 0;
JmpNivel2: Integer = 0;
JmpGGX: Integer = 0;
RmsMovID: TEstqMovimID = emidAjuste;
RmsNivel1: Integer = 0;
RmsNivel2: Integer = 0;
RmsGGX: Integer = 0;
GSPJmpMovID: TEstqMovimID = emidAjuste;
GSPJmpNiv2: Integer = 0;
MovCodPai: Integer = 0;
var
  DataHora, Observ, Marca: String;
  Controle, GraGruX, Terceiro, SerieFch, Ficha, SrcNivel1, SrcNivel2, GraGruY,
  SrcGGX, ItemNFe, VSMulFrnCab, GGXRcl, ClientMO, StqCenLoc, PedItsVda: Integer;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Result         := False;
  //
  SrcMovID       := TEstqMovimID(QrVSMovItsMovimID.Value);
  SrcNivel1      := QrVSMovItsCodigo.Value;
  SrcNivel2      := QrVSMovItsControle.Value;
  SrcGGX         := QrVSMovItsGraGruX.Value;
  //
  //Codigo         := ;
  Controle       := 0;
  //MovimCod       := ;
  //Empresa        := ;
  ClientMO       := QrVsMovItsClientMO.Value;
  StqCenLoc      := CO_STQCENLOC_ZERO;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidVenda;
  MovimNiv       := eminSemNiv;
  //Pallet         := QrVSMovItsPallet.Value;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Pecas          := InsPecas;
  PesoKg         := InsPesoKg;
  AreaM2         := InsAreaM2;
  AreaP2         := InsAreaP2;
  ValorMP        := 0;
  ValorT         := 0;
  Observ         := '';
  //
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Marca          := QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  //
  ItemNFe        := EdItemNFe.ValueVariant;
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  PedItsVda      := EdPedItsVda.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, Controle);
  Result := VS_CRC_PF.InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_FALSE_ExigeStqLoc,
  iuvpei094(*Item de venda de produto por peso*));
  if Result then
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(QrVSMovItsControle.Value, False);
{$IfDef sAllVS}
    //Fazer! pata IMEIS tamb�m
    InsereVSOutFat(Controle, Pecas, AreaM2, AreaP2, PesoKg);
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  end;
end;
  //


function BaixaEstoqueMultiplo((*Pallet,*) Codigo, MovimCod,
  Empresa, ClientMO, FornecMO: Integer; BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
  BxaValorT: Double): Boolean;
  //
var
  InsPesoKg, InsAreaM2, InsAreaP2, InsPecas: Double;
  RestoPesoKg, RestoAreaM2, RestoPecas: Double;
begin
  Result        := False;
  PnQtd.Enabled := False;
  RestoPesoKg   := BxaPesoKg;
  RestoAreaM2   := BxaAreaM2;
  RestoPecas    := BxaPecas;
  //
  QrVSMovIts.First;
  //
  while RestoPesoKg > 0 do
  begin
    if RestoPesoKg <= QrVSMovItsSdoVrtPeso.Value then
    begin
      InsPesoKg   := RestoPesoKg;
      InsAreaM2   := RestoAreaM2;
      InsPecas    := RestoPecas;
      //
      RestoPesoKg := 0;
      RestoAreaM2 := 0;
      RestoPecas  := 0;
    end else
    begin
      InsPesoKg  := QrVSMovItsSdoVrtPeso.Value;
      InsAreaM2  := QrVSMovItsSdoVrtArM2.Value;
      InsPecas   := QrVSMovItsSdoVrtPeca.Value;
      //
      RestoPesoKg := RestoPesoKg - InsPesoKg;
      RestoAreaM2 := RestoAreaM2 - InsAreaM2;
      RestoPecas  := RestoPecas  - InsPecas;
    end;
    // Evitar Loop infinito
    if InsPesoKg <= 0 then
    begin
      Geral.MB_Erro('Peso residual em estoque a baixar < 0 !!!' + sLineBreak +
      'Avise a DERMATEK!!!!');
      InsPesoKg  := RestoPesoKg;
      InsAreaM2  := RestoAreaM2;
      InsPecas   := RestoPecas;
      //
      RestoPesoKg := 0;
      RestoAreaM2 := 0;
      RestoPecas  := 0;
    end;
    InsAreaP2  := Geral.ConverteArea(InsAreaM2, ctM2toP2, cfQuarto);
    //
    BaixaPesoKgParciais(-InsPesoKg, -InsPecas, -InsAreaM2, -InsAreaP2);
    //
    QrVSMovIts.Next;
  end;
  Result := True;
end;
var
  I, PedItsVda, QtdDifGGX: Integer;
  InsPecas, InsPesoKg, InsAreaM2, InsAreaP2: Double;
begin
  Tem := (QrVSMovSum.State <> dsInactive) or (QrVSMovSum.RecordCount = 0);
  if MyObjects.FIC(Tem = False, EdPesoKg,
  'Baixa cancelada! N�o h� item pesquisado!') then
    Exit;
  //
  Codigo     := Geral.IMV(DBEdCodigo.Text);
  MovimCod   := Geral.IMV(DBEdMovimCod.Text);
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  ClientMO   := QrVSMovItsClientMO.Value;
  FornecMO   := 0;// ver!
  BxaPecas   := EdPecas.ValueVariant;
  BxaPesoKg  := EdPesoKg.ValueVariant;
  BxaAreaM2  := EdAreaM2.ValueVariant;
  BxaAreaP2  := EdAreaP2.ValueVariant;
  BxaValorT  := EdValorT.ValueVariant;
  GGXRcl     := EdGGXRcl.ValueVariant;
  //
  if MyObjects.FIC(BxaPesoKg > QrVSMovSumSdoVrtPeso.Value, EdPesoKg,
  'Baixa cancelada! Peso em estoque insuficiente!') then
    Exit;
  //
  if MyObjects.FIC(GGXRcl = 0, EdGGXRcl,
  'Informe o material usado para emitir NFe!') then
    Exit;
  //
  ItemNFe := EdItemNFe.ValueVariant;
  if MyObjects.FIC(ItemNFe = 0, EdItemNFe,
  'Informe o item da NFe!') then
    Exit;
  //
  PedItsVda      := EdPedItsVda.ValueVariant;
  if MyObjects.FIC((FPedido <> 0) and (PedItsVda = 0), EdPedItsVda,
  'Item de pedido n�o informado para o pedido desta sa�da!') then
  begin
    //if Geral.MB_Pergunta('Deseja continuar sem informar o item do pedido!') <> ID_YES then
      Exit;
  end;
  if PedItsVda > 0 then
  begin
    QtdDifGGX := 0;
    with DBGVMI.DataSource.DataSet do
    for I := 0 to DBGVMI.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGVMI.SelectedRows.Items[I]);
      //
      if QrVSMovItsGraGruX.Value <> QrVSPedItsGraGruX.Value then
        QtdDifGGX := QtdDifGGX + 1;
    end;
    if QtdDifGGX > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(QtdDifGGX) +
      ' itens em que o reduzido difere entre o pedido e o selecionado!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
    end;
  end;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  if FMultiplo then
  begin
    //testar!
    with DBGVMI.DataSource.DataSet do
    for I := 0 to DBGVMI.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGVMI.SelectedRows.Items[I]));
      GotoBookmark(DBGVMI.SelectedRows.Items[I]);
      InsPecas  := QrVSMovItsSdoVrtPeca.Value;
      InsPesoKg := QrVSMovItsSdoVrtPeso.Value;
      InsAreaM2 := QrVSMovItsSdoVrtArM2.Value;
      //
      InsAreaP2  := Geral.ConverteArea(InsAreaM2, ctM2toP2, cfQuarto);
      BaixaPesoKgParciais(-InsPesoKg, -InsPecas, -InsAreaM2, -InsAreaP2);
    end;
    //
    OK := True;
  end else
  begin
    OK := BaixaEstoqueMultiplo((*Pallet,*) Codigo, MovimCod, Empresa, ClientMO,
      FornecMO, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT);
  end;
  FmVSOutCab.AtualizaNFeItens(FmVSOutCab.QrVSOutCabMovimCod.Value);
  //
  if OK then
  begin
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
    FmVSOutCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(0);
    //
    if CkContinuar.Checked then
    begin
      PnQtd.Enabled := True;
      //
      QrVSMovIts.Close;
      EdGraGruX.ValueVariant := 0;
      CBGraGruX.KeyValue     := Null;
      EdPecas.ValueVariant   := 0;
      EdAreaM2.ValueVariant  := 0;
      EdAreaP2.ValueVariant  := 0;
      EdPesoKg.ValueVariant  := 0;
      EdValorT.ValueVariant  := 0;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
 end;

procedure TFmVSOutPeso.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutPeso.CalculaValorFaturado();
var
  PrecoTipo: Integer;
  //
  PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio, NacTotal, NacBruto, NacLiqui,
  Pecas, PesoKg, AreaM2, AreaP2, ValTot, PrecoVal, PercDesco: Double;
begin
  PrecoTipo := QrVSPedItsPrecoTipo.Value;
  Pecas     := EdPecas.ValueVariant;
  PesoKg    := EdPesoKg.ValueVariant;
  AreaM2    := EdAreaM2.ValueVariant;
  AreaP2    := EdAreaP2.ValueVariant;
  ValTot    := EdValorT.ValueVariant;
  PrecoVal  := EdPrecoVal.ValueVariant;
  PercDesco := EdPercDesco.ValueVariant;
  //
  PercTrib := EdPercTrib.ValueVariant;
  Cambio   := EdCambio.ValueVariant;
  //
  MdaTotal := VS_PF.CalculaValorCouros(TTipoCalcCouro(PrecoTipo), PrecoVal,
    Pecas, PesoKg, AreaM2, AreaP2, ValTot);
  MdaBruto := Geral.RoundC(MdaTotal * (1 - (PercDesco / 100)), 2);
  MdaLiqui := Geral.RoundC(MdaBruto * (1 - (PercTrib / 100)), 2);
  //
  NacTotal := Geral.RoundC(MdaTotal * Cambio, 2);
  NacBruto := Geral.RoundC(MdaBruto * Cambio, 2);
  NacLiqui := Geral.RoundC(MdaLiqui * Cambio, 2);
  //
  EdMdaTotal.ValueVariant := MdaTotal;
  EdMdaBruto.ValueVariant := MdaBruto;
  EdMdaLiqui.ValueVariant := MdaLiqui;
  //
  EdNacTotal.ValueVariant := NacTotal;
  EdNacBruto.ValueVariant := NacBruto;
  EdNacLiqui.ValueVariant := NacLiqui;
end;

procedure TFmVSOutPeso.CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutPeso.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  //
  //Valor := 0;
  (*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
  *)
      if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
        Valor := QrVSMovItsSdoVrtArM2.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
      else
      if QrVSMovItsPecas.Value > 0 then
        Valor := QrVSMovItsSdoVrtPeca.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  (*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
    end;
  end;
  *)
  EdValorT.ValueVariant := Valor;
end;

procedure TFmVSOutPeso.DBGVMIAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  Pecas, PesoKg, AreaM2: Double;
  I: Integer;
begin
  if DBGVMI.SelectedRows.Count > 0 then
  begin
    LaPeso.Enabled := False;
    EdPesoKg.Enabled := False;
    //
    Pecas  := 0;
    PesoKg := 0;
    AreaM2 := 0;
    with DBGVMI.DataSource.DataSet do
    for I := 0 to DBGVMI.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGVMI.SelectedRows.Items[I]));
      GotoBookmark(DBGVMI.SelectedRows.Items[I]);
      GotoBookmark(DBGVMI.SelectedRows.Items[I]);
      Pecas  := Pecas  + QrVSMovItsSdoVrtPeca.Value;
      PesoKg := PesoKg + QrVSMovItsSdoVrtPeso.Value;
      AreaM2 := AreaM2 + QrVSMovItsSdoVrtArM2.Value;
    end;
    EdPecas.ValueVariant  := Pecas;
    EdAreaM2.ValueVariant := AreaM2;
    EdPesoKg.ValueVariant := PesoKg;
    FMultiplo := True;
  end
  else
  begin
    FMultiplo := False;
    LaPeso.Enabled := True;
    EdPesoKg.Enabled := True;
  end;
end;

procedure TFmVSOutPeso.DBGVMIDblClick(Sender: TObject);
begin
{
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;
  //
  try
    EdPecas.SetFocus;
  except
    //
  end;
}
  DefineGGXRcl();
end;

procedure TFmVSOutPeso.DefineDadosItemPedido;
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    //FDefinindoDadosItemPedido := True;
    EdPrecoMoeda.ValueVariant := QrVSPedItsPrecoMoeda.Value;
    CBPrecoMoeda.KeyValue     := QrVSPedItsPrecoMoeda.Value;
    EdPrecoVal.ValueVariant   := QrVSPedItsPrecoVal.Value;
    EdPercDesco.ValueVariant  := QrVSPedItsPercDesco.Value;
    EdPercTrib.ValueVariant   := QrVSPedItsPercTrib.Value;
  end;
end;

procedure TFmVSOutPeso.DefineGGXRcl();
var
  ItemNFe: Integer;
begin
  ItemNFe := EdItemNFe.ValueVariant;
  if FmVSOutCab.QrVSOutIts.Locate('ItemNFe', ItemNFe, []) then
  begin
    EdGGXRcl.ValueVariant := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    CBGGXRcl.KeyValue := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    EdGGXRcl.Enabled := False;
    CBGGXRcl.Enabled := False;
  end else
  begin
    EdGGXRcl.Enabled := True;
    CBGGXRcl.Enabled := True;
  end;
end;

procedure TFmVSOutPeso.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pecas, AreaM2, Valor: Double;
begin
  if Key = VK_F5 then
  begin
    Pecas  := EdPecas.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    Valor := 0;
    (*
    case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
      encPecas: ;//Valor := 0;
      encArea: ;//Valor := 0;
      encValor:
      begin
    *)
        if (QrVSMovItsAreaM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
          Valor := AreaM2 * (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
        else
        if QrVSMovItsPecas.Value > 0 then
          Valor := Pecas * (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
    (*
      end;
      else
      begin
        //Valor := 0;
        Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
      end;
    end;
    *)
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmVSOutPeso.EdAreaM2Redefinido(Sender: TObject);
begin
  EdValorT.ValueVariant := 0;
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdCambioRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdGraGruXChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutPeso.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSOutPeso.EdItemNFeRedefinido(Sender: TObject);
begin
  DefineGGXRcl();
end;

procedure TFmVSOutPeso.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmVSOutPeso.EdPecasRedefinido(Sender: TObject);
begin
  EdValorT.ValueVariant := 0;
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdPedItsVdaRedefinido(Sender: TObject);
begin
  DefineDadosItemPedido();
end;

procedure TFmVSOutPeso.EdPercDescoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPeso.EdPercDescoRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdPercTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPeso.EdPercTribRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutPeso.EdPrecoMoedaRedefinido(Sender: TObject);
begin
  if EdPrecoMoeda.ValueVariant = Dmod.QrControle.FieldByName('MoedaBr').AsInteger then
  begin
    EdCambio.Enabled := False;
    EdCambio.ValueVariant := 1.0000000000;
  end else
    EdCambio.Enabled := True;
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.EdPrecoValKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPeso.EdPrecoValRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPeso.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutPeso.FormCreate(Sender: TObject);
var
  Default: Integer;
begin
  ImgTipo.SQLType := stLok;
  DBGVMI.Align := alClient;
  FUltGGX := 0;
  FMultiplo := False;
  //
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
  //
  Default := VS_CRC_PF.ObtemIDTipoCouro_de_GraGruY(CO_GraGruY_0512_VSSubPrd);
  VS_CRC_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS, Default);
end;

procedure TFmVSOutPeso.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutPeso.InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2,
  PesoKg: Double);
var
  PrecoTipo, PrecoMoeda: Integer;
  Qtde, PrecoVal, PercDesco, PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio,
  NacTotal, NacBruto, NacLiqui, (* TPecas, TPesoKg, TAreaM2, TAreaP2, QTot,*) Fator:
  Double;
  SQLType: TSQLType;
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    SQLType        := ImgTipo.SQLType;
    //
(*
    TPecas         := EdSdoVrtPeca.ValueVariant;
    TPesoKg        := EdSdoVrtPeso.ValueVariant;
    TAreaM2        := EdSdoVrtArM2.ValueVariant;
    TAreaP2        := EdSdoVrtArP2.ValueVariant;
*)
    PrecoTipo      := QrVSPedItsPrecoTipo.Value;
(*
    QTot           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      TPecas, TPesoKg, TAreaM2, TAreaP2));
*)
    Qtde           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      Pecas, PesoKg, AreaM2, AreaP2));
    //
(*
    if QTot <> 0 then
      Fator := ABS(Qtde / QTot)
    else
      Fator  := 0;
*)
    Fator := 1;
    //
    PrecoMoeda     := EdPrecoMoeda.ValueVariant;
    PrecoVal       := EdPrecoVal.ValueVariant;
    PercDesco      := EdPercDesco.ValueVariant;
    PercTrib       := EdPercTrib.ValueVariant;
    MdaTotal       := EdMdaTotal.ValueVariant * Fator;
    MdaBruto       := EdMdaBruto.ValueVariant * Fator;
    MdaLiqui       := EdMdaLiqui.ValueVariant * Fator;
    Cambio         := EdCambio.ValueVariant;
    NacTotal       := EdNacTotal.ValueVariant * Fator;
    NacBruto       := EdNacBruto.ValueVariant * Fator;
    NacLiqui       := EdNacLiqui.ValueVariant * Fator;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsoutfat', False, [
    'Qtde', 'PrecoTipo', 'PrecoMoeda',
    'PrecoVal', 'PercDesco', 'PercTrib',
    'MdaTotal', 'MdaBruto', 'MdaLiqui',
    'Cambio', 'NacTotal', 'NacBruto',
    'NacLiqui'], [
    'Controle'], [
    Qtde, PrecoTipo, PrecoMoeda,
    PrecoVal, PercDesco, PercTrib,
    MdaTotal, MdaBruto, MdaLiqui,
    Cambio, NacTotal, NacBruto,
    NacLiqui], [
    Controle], True);
  end;
end;

procedure TFmVSOutPeso.QrVSMovItsAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrVSMovIts.RecordCount > 0;
end;

procedure TFmVSOutPeso.QrVSMovItsBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmVSOutPeso.ReopenGGXY();
var
  GraGruY: Integer;
begin
  GraGruY := VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(GraGruY));
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
    //'AND ggx.GragruY=' + Geral.FF0(GraGruY));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
end;

procedure TFmVSOutPeso.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutPeso.ReopenVSMovIts((*GraGruX: Integer*));
var
  GraGruX, Empresa, Pallet, Ficha: Integer;
  SQL_Ficha: String;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  SQL_Ficha  := '';
  Pallet     := 0;
  Ficha      := 0;
  if Ficha <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT fch.Nome NO_SerieFch, ggx.GraGruY, vmi.*,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE   ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux  ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch  ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  // ini 2023-05-01
  //'AND vmi.MovimID=' + Geral.FF0(Integer(emidGeraSubProd)),
  'AND vmi.AreaM2 = 0.00 ',
  // fim 2023-05-01
  'AND vmi.SdoVrtPeso > 0 ',
  SQL_Ficha,
  'ORDER BY DataHora, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSMovIts);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovSum, Dmod.MyDB, [
  'SELECT SUM(vmi.SdoVrtPeso) SdoVrtPeso ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  // ini 2023-05-01
  //'AND vmi.MovimID=' + Geral.FF0(Integer(emidGeraSubProd)),
  'AND vmi.AreaM2 = 0.00 ',
  // fim 2023-05-01
  'AND vmi.SdoVrtPeso > 0 ',
  SQL_Ficha,
  '']);
  //Geral.MB_SQL(Self, QrVSMovSum);
end;

procedure TFmVSOutPeso.RGTipoCouroClick(Sender: TObject);
begin
  ReopenGGXY();
end;

procedure TFmVSOutPeso.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
