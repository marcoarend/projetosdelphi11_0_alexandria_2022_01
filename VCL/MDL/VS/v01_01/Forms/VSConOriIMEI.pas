unit VSConOriIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSConOriIMEI = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaPecas: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdPecas: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoquePecas: TFloatField;
    QrEstoquePesoKg: TFloatField;
    QrEstoqueAreaM2: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstoque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    QrEstoqueVSMulFrnCab: TIntegerField;
    QrEstoqueClientMO: TIntegerField;
    EdPesoKg: TdmkEdit;
    Label3: TLabel;
    QrEstoqueSerieFch: TIntegerField;
    QrEstoqueFicha: TIntegerField;
    QrEstoqueMarca: TWideStringField;
    QrEstoqueNO_SerieFch: TWideStringField;
    QrEstoqueSdoVrtArM2: TFloatField;
    QrEstoqueSdoVrtArP2: TFloatField;
    QrEstoqueSdoVrtPeca: TFloatField;
    QrEstoqueSdoVrtPeso: TFloatField;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrEstoqueGGXInProc: TIntegerField;
    CkSoClientMO: TCheckBox;
    QrStqCenLoc_: TmySQLQuery;
    QrStqCenLoc_Controle: TIntegerField;
    QrStqCenLoc_NO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    EdStqCenLoc_: TdmkEditCB;
    Label49: TLabel;
    CBStqCenLoc_: TdmkDBLookupComboBox;
    SbStqCenLoc: TSpeedButton;
    CkContinua: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    //
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcPecas, ParcPesoKg,
              ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenVSGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    procedure RedefineComponentes();
    function  BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
  public
    { Public declarations }
    //FVSConOriIMEI, FVSConOriPallet,
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FOrigMovimNiv, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea,
    (*FGGXRmsDst, FGGXRmsSrc,*) FStqCenLoc, FFornecMO: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSConOriIMEI: TFmVSConOriIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModVS, VSConCab, UnVS_PF, AppListas, UnDmkProcFunc, ModVS_CRC, UnVS_CRC_PF,
  StqCenLoc;

{$R *.DFM}

procedure TFmVSConOriIMEI.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

function TFmVSConOriIMEI.BaixaIMEIParcial(): Integer;
var
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
  QtdAntPeca, QtdAntPeso: Double;
begin
  Result := 0;
  Pecas  := EdPecas.ValueVariant;
  PesoKg := EdPesoKg.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  SobraPecas  := Pecas;
  SobraPesoKg := PesoKg;
  SobraAreaM2 := AreaM2;
  SobraAreaP2 := AreaP2;
  //
  QtdAntPeca  := SobraPecas;
  //QtdAntPeso  := EdQtdAntPeso.ValueVariant;
  QtdAntPeso  := SobraPesoKg;
  //
  if MyObjects.FIC(Pecas <= 0, EdPecas, 'Informe a quantidade de pe�as') then
    Exit;
  InsereIMEI_Atual(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2,
    QtdAntPeca, QtdAntPeso);
  Result := 1;
end;

function TFmVSConOriIMEI.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      InsereIMEI_Atual(True, 0, 0, 0, 0, 0, 0);
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmVSConOriIMEI.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  //FGGXRmsDst := EdRmsGGX.ValueVariant;
  //FGGXRmsSrc := FmVSConCab.QrVSConCabGGXSrc.Value;
  //
  FStqCenLoc := FmVSConCab.QrVSConAtuStqCenLoc.Value;
  FFornecMO  := FmVSConCab.QrVSConAtuFornecMO.Value;

  //
  //if MyObjects.FIC(FGGXRmsDst = 0, EdRmsGGX, 'Informe a mat�ria-prima PDA!') then Exit;
  //
  if MyObjects.FIC(FStqCenLoc = 0, nil, 'Informe a localiza��o no cabe�alho!') then Exit;
  if MyObjects.FIC(FFornecMO = 0, nil, 'Informe o fornecedor do servi�o no cabe�alho!') then Exit;
  //
  if QrEstoqueSdoVrtPeso.Value = 0 then
  begin
    if Geral.MB_Pergunta('O Saldo Peso Kg do IMEI ' + Geral.FF0(QrEstoqueIMEI.Value)
      + ' est� zerado!' + sLineBreak + 'Deseja informar o peso?') = ID_YES then
    begin
      VS_PF.MostraFormVSInnCab(QrEstoqueCodigo.Value, QrEstoqueIMEI.Value, 0);
    end;
    RedefineComponentes();
    Exit;
  end;
  //
  if FParcial then
    N := BaixaIMEIParcial()
  else
    N := BaixaIMEITotal();
  //
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSConcab', MovimCod);
    MovimCod := FmVSConCab.QrVSConCabMovimCod.Value;
    VS_PF.AtualizaTotaisVSConCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmVSConCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinua.Checked then
        RedefineComponentes()
      else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
(*
  // Nao se aplica. Calcula com function propria a seguir.
  //VS_PF.AtualizaTotaisVSXxxCab('VSConcab', MovimCod);
  MovimCod := FmVSConCab.QrVSConCabMovimCod.Value;
  VS_PF.AtualizaTotaisVSConCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmVSConCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
end;

procedure TFmVSConOriIMEI.RedefineComponentes();
begin
  EdControle.ValueVariant   := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdAreaM2.ValueVariant     := 0;
  EdAreaP2.ValueVariant     := 0;
  EdObserv.ValueVariant     := '';
  //EdQtdAntPeso.ValueVariant := 0;
  //
  ReopenItensAptos();
  //
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
  //
  LiberaEdicao(False, False);
end;

procedure TFmVSConOriIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSConOriIMEI.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  I, J, OK, N, GraGruX: Integer;
  LstReduz(*, LstCount*): array of Integer;
begin
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    //
    GraGruX := QrEstoqueGraGruX.Value;
    OK := -1;
    for J := Low(LstReduz) to High(LstReduz) do
      if LstReduz[J] = GraGruX then
        OK := J;
    if OK = -1 then
    begin
      N := Length(LstReduz);
      SetLength(LstReduz, N + 1);
      LstReduz[N] := GraGruX
    end;
  end;
{
  if Length(LstReduz) = 1 then
  begin
    EdRmsGGX.ValueVariant := QrEstoqueGGXInProc.Value;
    CBRmsGGX.KeyValue     := QrEstoqueGGXInProc.Value;
  end else
  begin
    EdRmsGGX.ValueVariant := 0;
    CBRmsGGX.KeyValue     := Null;
  end;
}
end;

procedure TFmVSConOriIMEI.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSConOriIMEI.DefineTipoArea();
begin
(*
  LaAreaM2.Enabled := FTipoArea = 0;
  EdAreaM2.Enabled := FTipoArea = 0;

  LaAreaP2.Enabled := FTipoArea = 1;
  EdAreaP2.Enabled := FTipoArea = 1;
*)
end;

procedure TFmVSConOriIMEI.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSConOriIMEI.EdGraGruXRedefinido(Sender: TObject);
begin
  //FechaPesquisa();
  ReopenItensAptos();
end;

procedure TFmVSConOriIMEI.EdPecasChange(Sender: TObject);
var
  Pecas, PesoKg: Double;
begin
  Pecas  := EdPecas.ValueVariant;
  PesoKg := QrEstoquePesoKg.Value /QrEstoquePecas.Value;
  EdPesoKg.ValueVariant := Pecas * PesoKg;
end;

procedure TFmVSConOriIMEI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdPecas.ValueVariant := QrEstoqueSdoVrtPeca.Value;
    //EdQtdAntPeso.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    EdPesoKg.ValueVariant := QrEstoqueSdoVrtPeso.Value;
  end;
end;

procedure TFmVSConOriIMEI.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSConOriIMEI.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSConOriIMEI.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmVSConOriIMEI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmVSConOriIMEI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  VS_PF.AbreVSSerFch(QrVSSerFch);
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1024_VSNatCad) +
    ')');
  //
{
  VS_PF.AbreGraGruXY(QrRmsGGX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1195_VSNatPDA));
}
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0(*, TEstqMovimID.???*));
end;

procedure TFmVSConOriIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSConOriIMEI.InsereIMEI_Atual(InsereTudo: Boolean;
 ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  QtdAntArM2  = 0;
  QtdAntArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, ReqMovEstq, VSMulFrnCab, ClientMO, StqCenLoc, FornecMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor, QtdAntPeca, QtdAntPeso: Double;
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX, RmsStqCenLoc: Integer;
begin
  FornecMO       := FFornecMO;
  StqCenLoc      := FStqCenLoc;
  SrcMovID       := QrEstoqueMovimID.Value;
  SrcNivel1      := QrEstoqueCodigo.Value;
  SrcNivel2      := QrEstoqueControle.Value;
  SrcGGX         := QrEstoqueGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  Terceiro       := QrEstoqueTerceiro.Value;
  VSMulFrnCab    := QrEstoqueVSMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmProcCon;
  MovimNiv       := eminSorcCon;
  Pallet         := QrEstoquePallet.Value;
  GraGruX        := QrEstoqueGraGruX.Value;
  if not InsereTudo then
  begin
    Pecas        := -ParcPecas;
    PesoKg       := -ParcPesoKg;
    AreaM2       := -ParcAreaM2;
    AreaP2       := -ParcAreaP2;
    QtdAntPeca   := ParcQtdAntPeca;
    QtdAntPeso   := ParcQtdAntPeso;
  //
  end else
  begin
    Pecas          := -QrEstoqueSdoVrtPeca.Value;
    PesoKg         := -QrEstoqueSdoVrtPeso.Value;
    AreaM2         := -QrEstoqueSdoVrtArM2.Value;
    AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    QtdAntPeca     := QrEstoqueSdoVrtPeca.Value;
    QtdAntPeso     := QrEstoqueSdoVrtPeso.Value;
  end;
  if (QrEstoquePesoKg.Value > 0) then
    Valor := -PesoKg *
    (QrEstoqueValorT.Value / QrEstoquePesoKg.Value)
  else
  if (QrEstoqueAreaM2.Value > 0) then
    Valor := -AreaM2 *
    (QrEstoqueValorT.Value / QrEstoqueAreaM2.Value)
  else
  if QrEstoquePecas.Value > 0 then
    Valor := -Pecas *
    (QrEstoqueValorT.Value / QrEstoquePecas.Value)
  else
    Valor := 0;
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := QrEstoqueSerieFch.Value;
  Ficha          := QrEstoqueFicha.Value;
  Marca          := QrEstoqueMarca.Value;
  //Misturou       := QrEstoqueMisturou.Value;
  DstMovID       := TEstqMovimID(FmVSConCab.QrVSConAtuMovimID.Value);
  DstNivel1      := FmVSConCab.QrVSConAtuCodigo.Value;
  DstNivel2      := FmVSConCab.QrVSConAtuControle.Value;
  DstGGX         := FmVSConCab.QrVSConAtuGraGruX.Value;
  RmsGGX         := QrEstoqueGraGruX.Value; //EdRmsGGX.ValueVariant;
  //
  GraGruY        := QrEstoqueGraGruY.Value;
  //
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  ClientMO       := QrEstoqueClientMO.Value;
  RmsStqCenLoc   := 0;//StqCenLoc;
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  RmsGGX, //CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei112(*Mat�ria prima origem na conserva��o*)) then
  begin
(*
    //  Criar lancamento de couro PDA com RmsMovID, RmsNivel1 e RmsNivel2
    VS_PF.CriaVMIRmsCon(Controle, FGGXRmsSrc, FGGXRmsDst,
     RmsGGX, //FmVSConCab.QrVSConCabGraGruX.Value,
    QrEstoquePecas.Value, QrEstoqueAreaM2.Value, QrEstoquePesoKg.Value,
    //-Pecas, -AreaM2, -PesoKg, -ValorT, RmsStqCenLoc);
    QtdAntPeca, QtdAntArM2, QtdAntPeso, -ValorT, RmsStqCenLoc, Codigo, MovimCod);
*)
    //
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, True);
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
  end;
end;

procedure TFmVSConOriIMEI.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled    := not Status;
    EdFicha.Enabled    := not Status;
    LaGraGruX.Enabled  := not Status;
    EdGraGruX.Enabled  := not Status;
    CBGraGruX.Enabled  := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled   := not Status;
    //
    if Libera then
    begin
{
      if QrRmsGGX.Locate('Controle', QrEstoqueGGXInProc.Value, []) then
      begin
        EdRmsGGX.ValueVariant := QrEstoqueGGXInProc.Value;
        CBRmsGGX.KeyValue     := QrEstoqueGGXInProc.Value;
        if (EdPecas.Enabled) and (EdPecas.Visible) then
          EdPecas.SetFocus;
      end
      else
      if (EdRmsGGX.Enabled) and (EdRmsGGX.Visible) then
        EdRmsGGX.SetFocus;
}
      EdPecas.SetFocus;
    end;
  end;
end;

procedure TFmVSConOriIMEI.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSConOriIMEI.ReopenItensAptos();
var
  GraGruX: Integer;
  SQL_GraGruX, SQL_ClientMO: String;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  if CkSoClientMO.Checked then
    SQL_ClientMO := 'AND vmi.ClientMO=' +
      Geral.FF0(FmVSConCab.QrVSConAtuClientMO.Value)
  else
    SQL_ClientMO := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.Empresa, vmi.ClientMO, ',
  'vmi.GraGruX, vmi.PesoKg, vmi.Pecas, vmi.AreaM2, ',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
  'FLOOR((vmi.SdoVrtArM2 / 0.09290304)) + ',
  'FLOOR(((MOD((vmi.SdoVrtArM2 / 0.09290304), 1)) + ',
  '0.12499) * 4) * 0.25 SdoVrtArP2, ValorT, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
  'vmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"" NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, ',
  'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
  'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ',
  'vmi.VSMulFrnCab, vmi.SerieFch, vmi.Ficha, vmi.Marca, ',
  'vsf.Nome NO_SerieFch, vnc.GGXInProc,',
  '1 Ativo ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN vsnatcad   vnc ON vnc.GraGruX=vmi.GraGruX ',
  'WHERE ggx.GragruY IN (' +
  Geral.FF0(CO_GraGruY_1024_VSNatCad) + ')',
  'AND vmi.Controle <> 0 ',
  SQL_GraGruX,
  SQL_ClientMO,
  'AND vmi.SdoVrtPeca > 0 ',
  'AND vmi.Pallet = 0 ',
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  ' ']);
  //Geral.MB_SQL(self, QrEstoque);
end;

{
object Label63: TLabel
  Left = 156
  Top = 8
  Width = 91
  Height = 13
  Caption = 'Mat'#233'ria-prima PDA:'
end
object EdRmsGGX: TdmkEditCB
  Left = 256
  Top = 4
  Width = 56
  Height = 21
  Alignment = taRightJustify
  TabOrder = 3
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  DBLookupComboBox = CBRmsGGX
  IgnoraDBLookupComboBox = False
  AutoSetIfOnlyOneReg = setregOnFormActivate
end
object CBRmsGGX: TdmkDBLookupComboBox
  Left = 312
  Top = 4
  Width = 361
  Height = 21
  KeyField = 'Controle'
  ListField = 'NO_PRD_TAM_COR'
  ListSource = DsRmsGGX
  TabOrder = 4
  dmkEditCB = EdRmsGGX
  UpdType = utYes
  LocF7SQLMasc = '$#'
  LocF7PreDefProc = f7pNone
end
}


{
object EdQtdAntPeso: TdmkEdit
  Left = 172
  Top = 32
  Width = 72
  Height = 21
  Alignment = taRightJustify
  TabOrder = 2
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 3
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,000'
  QryCampo = 'QtdGerPeso'
  UpdCampo = 'QtdGerPeso'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object LaPesoKg: TLabel
  Left = 172
  Top = 16
  Width = 68
  Height = 13
  Caption = 'Peso kg ful'#227'o:'
end
}

{
object QrRmsGGX: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
    'FROM vsribcad wmp'
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
    '')
  Left = 288
  Top = 220
  object QrRmsGGXGraGru1: TIntegerField
    FieldName = 'GraGru1'
  end
  object QrRmsGGXControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrRmsGGXNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrRmsGGXSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrRmsGGXCODUSUUNIDMED: TIntegerField
    FieldName = 'CODUSUUNIDMED'
  end
  object QrRmsGGXNOMEUNIDMED: TWideStringField
    FieldName = 'NOMEUNIDMED'
    Size = 30
  end
end
object DsRmsGGX: TDataSource
  DataSet = QrRmsGGX
  Left = 288
  Top = 268
end
}

end.
