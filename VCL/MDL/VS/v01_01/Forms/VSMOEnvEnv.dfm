object FmVSMOEnvEnv: TFmVSMOEnvEnv
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-218 :: Edi'#231#227'o de Controle de Envio para MO'
  ClientHeight = 617
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 624
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 576
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 459
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Envio para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 459
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Envio para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 459
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Envio para MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 672
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 192
      Width = 672
      Height = 263
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Label3: TLabel
        Left = 12
        Top = 212
        Width = 100
        Height = 13
        Caption = 'Descri'#231#227'o (opcional):'
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 672
        Height = 104
        Align = alTop
        Caption = ' NFe Envio mat'#233'ria-prima: '
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 668
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label7: TLabel
            Left = 12
            Top = 44
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label10: TLabel
            Left = 44
            Top = 44
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label11: TLabel
            Left = 592
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 12
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 48
            Top = 4
            Width = 50
            Height = 13
            Caption = 'Num NF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 124
            Top = 44
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object LaPecas: TLabel
            Left = 160
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object LaAreaM2: TLabel
            Left = 232
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object LaAreaP2: TLabel
            Left = 316
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object LaPeso: TLabel
            Left = 400
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label14: TLabel
            Left = 484
            Top = 44
            Width = 36
            Height = 13
            Caption = '$ Total:'
          end
          object SbRMP: TSpeedButton
            Left = 568
            Top = 20
            Width = 21
            Height = 21
            Caption = '>'
            OnClick = SbRMPClick
          end
          object Label31: TLabel
            Left = 124
            Top = 4
            Width = 171
            Height = 13
            Caption = 'Prestador do servi'#231'o da M.O.:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdNFEMP_FatID: TdmkEdit
            Left = 12
            Top = 60
            Width = 29
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_FatNum: TdmkEdit
            Left = 44
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_Empresa: TdmkEdit
            Left = 592
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_SerNF: TdmkEdit
            Left = 12
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFEMP_nNF: TdmkEdit
            Left = 47
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_nItem: TdmkEdit
            Left = 123
            Top = 60
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFEMP_Pecas: TdmkEdit
            Left = 160
            Top = 60
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFEMP_AreaM2: TdmkEditCalc
            Left = 232
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdNFEMP_AreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdNFEMP_AreaP2: TdmkEditCalc
            Left = 316
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdNFEMP_AreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdNFEMP_PesoKg: TdmkEdit
            Left = 400
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFEMP_ValorT: TdmkEdit
            Left = 484
            Top = 60
            Width = 165
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFEMP_Terceiro: TdmkEditCB
            Left = 124
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNFEMP_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFEMP_Terceiro: TdmkDBLookupComboBox
            Left = 180
            Top = 20
            Width = 385
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador
            ParentFont = False
            TabOrder = 3
            dmkEditCB = EdNFEMP_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 104
        Width = 672
        Height = 104
        Align = alTop
        Caption = ' CTe Cobran'#231'a de frete: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 668
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label32: TLabel
            Left = 72
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            Enabled = False
          end
          object Label33: TLabel
            Left = 104
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label34: TLabel
            Left = 12
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label35: TLabel
            Left = 508
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label36: TLabel
            Left = 544
            Top = 4
            Width = 49
            Height = 13
            Caption = 'Num CF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label37: TLabel
            Left = 620
            Top = 4
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label38: TLabel
            Left = 12
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label39: TLabel
            Left = 84
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label40: TLabel
            Left = 168
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label41: TLabel
            Left = 252
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 524
            Top = 44
            Width = 45
            Height = 13
            Caption = '$ Total:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SbCF: TSpeedButton
            Left = 628
            Top = 60
            Width = 21
            Height = 21
            Enabled = False
            Visible = False
          end
          object Label43: TLabel
            Left = 336
            Top = 44
            Width = 81
            Height = 13
            Caption = 'Peso kg frete:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 440
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Cus. frete kg:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label45: TLabel
            Left = 180
            Top = 4
            Width = 84
            Height = 13
            Caption = 'Transportador:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdCFTMP_FatID: TdmkEdit
            Left = 72
            Top = 20
            Width = 29
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTMP_FatNum: TdmkEdit
            Left = 104
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTMP_Empresa: TdmkEdit
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTMP_SerCT: TdmkEdit
            Left = 507
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdCFTMP_nCT: TdmkEdit
            Left = 543
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdCFTMP_nCTKeyDown
          end
          object EdCFTMP_nItem: TdmkEdit
            Left = 619
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTMP_Pecas: TdmkEdit
            Left = 12
            Top = 60
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTMP_AreaM2: TdmkEditCalc
            Left = 84
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdCFTMP_AreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdCFTMP_AreaP2: TdmkEditCalc
            Left = 168
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdCFTMP_AreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdCFTMP_PesoKg: TdmkEdit
            Left = 252
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTMP_ValorT: TdmkEdit
            Left = 524
            Top = 60
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTMP_PesTrKg: TdmkEdit
            Left = 336
            Top = 60
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCFTMP_PesTrKgRedefinido
          end
          object EdCFTMP_CusTrKg: TdmkEdit
            Left = 440
            Top = 60
            Width = 81
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCFTMP_CusTrKgRedefinido
          end
          object EdCFTMP_Terceiro: TdmkEditCB
            Left = 180
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCFTMP_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCFTMP_Terceiro: TdmkDBLookupComboBox
            Left = 236
            Top = 20
            Width = 265
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsTransporta
            ParentFont = False
            TabOrder = 4
            dmkEditCB = EdCFTMP_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object MeNome: TdmkMemo
        Left = 12
        Top = 228
        Width = 645
        Height = 33
        MaxLength = 240
        TabOrder = 2
        OnKeyDown = MeNomeKeyDown
        UpdType = utYes
      end
    end
    object DBGIMEIs: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 672
      Height = 147
      TabStop = False
      Align = alClient
      DataSource = DsVMI
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnAfterMultiselect = DBGIMEIsAfterMultiselect
      OnExit = DBGIMEIsExit
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRDA_TAM_COR'
          Title.Caption = 'Nome'
          Width = 252
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 62
          Visible = True
        end>
    end
    object Panel5: TPanel
      Left = 0
      Top = 147
      Width = 672
      Height = 45
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 464
        Top = 1
        Width = 29
        Height = 13
        Caption = 'Data: '
      end
      object LaHora: TLabel
        Left = 580
        Top = 1
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 209
        Height = 45
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtItem: TBitBtn
          Tag = 11
          Left = 5
          Top = 3
          Width = 200
          Height = 40
          Caption = '&Editar dados item selecionado'
          NumGlyphs = 2
          TabOrder = 0
          TabStop = False
          OnClick = BtItemClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 209
        Top = 0
        Width = 249
        Height = 45
        Align = alLeft
        Caption = ' Dados do registro: '
        Enabled = False
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label2: TLabel
          Left = 136
          Top = 20
          Width = 32
          Height = 13
          Caption = 'IME-C:'
        end
        object EdCodigo: TdmkEdit
          Left = 52
          Top = 16
          Width = 80
          Height = 21
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object EdVSVMI_MovimCod: TdmkEdit
          Left = 172
          Top = 16
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object TPData: TdmkEditDateTimePicker
        Left = 464
        Top = 17
        Width = 113
        Height = 21
        Date = 40656.000000000000000000
        Time = 40656.000000000000000000
        ShowCheckbox = True
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdHora: TdmkEdit
        Left = 580
        Top = 17
        Width = 80
        Height = 21
        TabOrder = 3
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 503
    Width = 672
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 668
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 547
    Width = 672
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 526
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 524
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtMONaoCobr: TBitBtn
        Tag = 14
        Left = 320
        Top = 4
        Width = 180
        Height = 40
        Caption = '&MO ainda n'#227'o cobrada!'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtMONaoCobrClick
      end
      object BtContinoaCobranca: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 180
        Height = 40
        Caption = '&Igual a '#250'ltima MO lan'#231'ada!'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtContinoaCobrancaClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 7
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 24
    Top = 88
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 24
    Top = 136
  end
  object QrPrestador: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 152
    Top = 88
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 152
    Top = 136
  end
  object QrVMI: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVMIAfterOpen
    SQL.Strings = (
      'SELECT * FROM _vmi_qtd_')
    Left = 208
    Top = 88
    object QrVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMIPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '0;-0; '
    end
    object QrVMIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVMINO_PRDA_TAM_COR: TWideStringField
      FieldName = 'NO_PRDA_TAM_COR'
      Size = 255
    end
    object QrVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVMIIDItem: TIntegerField
      FieldName = 'IDItem'
    end
  end
  object DsVMI: TDataSource
    DataSet = QrVMI
    Left = 208
    Top = 136
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    Left = 260
    Top = 88
    object QrSumPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrSumPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumAreaKg: TFloatField
      FieldName = 'AreaKg'
    end
    object QrSumValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
end
