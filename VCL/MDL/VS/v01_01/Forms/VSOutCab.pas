unit VSOutCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet,
  UnProjGroup_Consts, dmkDBGridZTO, UnGrl_Consts, UnGrl_Geral, UnAppEnums,
  UnProjGroup_Vars;

type
  THackDBGrid = class(TDBGrid);
  TFmVSOutCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    QrVSOutCab: TmySQLQuery;
    DsVSOutCab: TDataSource;
    QrVSOutIts: TMySQLQuery;
    DsVSOutIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExcluiIMEI1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtVenda: TdmkEditDateTimePicker;
    EdDtVenda: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrega: TdmkEditDateTimePicker;
    EdDtEntrega: TdmkEdit;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSOutCabCodigo: TIntegerField;
    QrVSOutCabMovimCod: TIntegerField;
    QrVSOutCabEmpresa: TIntegerField;
    QrVSOutCabDtVenda: TDateTimeField;
    QrVSOutCabDtViagem: TDateTimeField;
    QrVSOutCabDtEntrega: TDateTimeField;
    QrVSOutCabCliente: TIntegerField;
    QrVSOutCabTransporta: TIntegerField;
    QrVSOutCabPecas: TFloatField;
    QrVSOutCabPesoKg: TFloatField;
    QrVSOutCabAreaM2: TFloatField;
    QrVSOutCabAreaP2: TFloatField;
    QrVSOutCabLk: TIntegerField;
    QrVSOutCabDataCad: TDateField;
    QrVSOutCabDataAlt: TDateField;
    QrVSOutCabUserCad: TIntegerField;
    QrVSOutCabUserAlt: TIntegerField;
    QrVSOutCabAlterWeb: TSmallintField;
    QrVSOutCabAtivo: TSmallintField;
    QrVSOutCabNO_EMPRESA: TWideStringField;
    QrVSOutCabNO_CLIENTE: TWideStringField;
    QrVSOutCabNO_TRANSPORTA: TWideStringField;
    GBIts: TGroupBox;
    ItemComrastreio1: TMenuItem;
    ItemSemrastreio1: TMenuItem;
    AntigoAdd1: TMenuItem;
    PorIMEI1: TMenuItem;
    PorPallet1: TMenuItem;
    QrVSOutCabNFV: TIntegerField;
    EdNFV: TdmkEdit;
    Label20: TLabel;
    PMImprime: TPopupMenu;
    PackingList1: TMenuItem;
    QrPallets: TmySQLQuery;
    DsPallets: TDataSource;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDados: TdmkDBGridZTO;
    DBGrid1: TDBGrid;
    QrPalletsArtigoImp: TWideStringField;
    QrPalletsClasseImp: TWideStringField;
    QrPalletsSEQ: TIntegerField;
    frxDsPallets: TfrxDBDataset;
    frxWET_CURTI_019_00_A1: TfrxReport;
    QrPalletsMediaM2: TFloatField;
    QrPalletsFaixaMediaM2: TFloatField;
    Porkgsubproduto1: TMenuItem;
    InformaNmerodaRME1: TMenuItem;
    TabSheet3: TTabSheet;
    QrTribIncIts: TmySQLQuery;
    QrTribIncItsFatID: TIntegerField;
    QrTribIncItsFatNum: TIntegerField;
    QrTribIncItsFatParcela: TIntegerField;
    QrTribIncItsEmpresa: TIntegerField;
    QrTribIncItsControle: TIntegerField;
    QrTribIncItsData: TDateField;
    QrTribIncItsHora: TTimeField;
    QrTribIncItsValorFat: TFloatField;
    QrTribIncItsBaseCalc: TFloatField;
    QrTribIncItsValrTrib: TFloatField;
    QrTribIncItsPercent: TFloatField;
    QrTribIncItsLk: TIntegerField;
    QrTribIncItsDataCad: TDateField;
    QrTribIncItsDataAlt: TDateField;
    QrTribIncItsUserCad: TIntegerField;
    QrTribIncItsUserAlt: TIntegerField;
    QrTribIncItsAlterWeb: TSmallintField;
    QrTribIncItsAtivo: TSmallintField;
    QrTribIncItsTributo: TIntegerField;
    QrTribIncItsVUsoCalc: TFloatField;
    QrTribIncItsOperacao: TSmallintField;
    QrTribIncItsNO_Tributo: TWideStringField;
    DsTribIncIts: TDataSource;
    InformaItemdeNFe1: TMenuItem;
    BtTribIncIts: TBitBtn;
    PMTribIncIts: TPopupMenu;
    IncluiTributo1: TMenuItem;
    AlteraTributoAtual1: TMenuItem;
    ExcluiTributoAtual1: TMenuItem;
    QrTribIncItsFatorDC: TSmallintField;
    QrPalletsTXT_ARTIGO: TWideStringField;
    QrVSOutItsCodigo: TLargeintField;
    QrVSOutItsControle: TLargeintField;
    QrVSOutItsMovimCod: TLargeintField;
    QrVSOutItsMovimNiv: TLargeintField;
    QrVSOutItsMovimTwn: TLargeintField;
    QrVSOutItsEmpresa: TLargeintField;
    QrVSOutItsTerceiro: TLargeintField;
    QrVSOutItsCliVenda: TLargeintField;
    QrVSOutItsMovimID: TLargeintField;
    QrVSOutItsDataHora: TDateTimeField;
    QrVSOutItsPallet: TLargeintField;
    QrVSOutItsGraGruX: TLargeintField;
    QrVSOutItsPecas: TFloatField;
    QrVSOutItsPesoKg: TFloatField;
    QrVSOutItsAreaM2: TFloatField;
    QrVSOutItsAreaP2: TFloatField;
    QrVSOutItsValorT: TFloatField;
    QrVSOutItsSrcMovID: TLargeintField;
    QrVSOutItsSrcNivel1: TLargeintField;
    QrVSOutItsSrcNivel2: TLargeintField;
    QrVSOutItsSrcGGX: TLargeintField;
    QrVSOutItsSdoVrtPeca: TFloatField;
    QrVSOutItsSdoVrtPeso: TFloatField;
    QrVSOutItsSdoVrtArM2: TFloatField;
    QrVSOutItsObserv: TWideStringField;
    QrVSOutItsSerieFch: TLargeintField;
    QrVSOutItsFicha: TLargeintField;
    QrVSOutItsMisturou: TLargeintField;
    QrVSOutItsFornecMO: TLargeintField;
    QrVSOutItsCustoMOKg: TFloatField;
    QrVSOutItsCustoMOM2: TFloatField;
    QrVSOutItsCustoMOTot: TFloatField;
    QrVSOutItsValorMP: TFloatField;
    QrVSOutItsDstMovID: TLargeintField;
    QrVSOutItsDstNivel1: TLargeintField;
    QrVSOutItsDstNivel2: TLargeintField;
    QrVSOutItsDstGGX: TLargeintField;
    QrVSOutItsQtdGerPeca: TFloatField;
    QrVSOutItsQtdGerPeso: TFloatField;
    QrVSOutItsQtdGerArM2: TFloatField;
    QrVSOutItsQtdGerArP2: TFloatField;
    QrVSOutItsQtdAntPeca: TFloatField;
    QrVSOutItsQtdAntPeso: TFloatField;
    QrVSOutItsQtdAntArM2: TFloatField;
    QrVSOutItsQtdAntArP2: TFloatField;
    QrVSOutItsNotaMPAG: TFloatField;
    QrVSOutItsPedItsFin: TLargeintField;
    QrVSOutItsMarca: TWideStringField;
    QrVSOutItsStqCenLoc: TLargeintField;
    QrVSOutItsNO_PALLET: TWideStringField;
    QrVSOutItsNO_PRD_TAM_COR: TWideStringField;
    QrVSOutItsNO_TTW: TWideStringField;
    QrVSOutItsID_TTW: TLargeintField;
    QrVSOutItsReqMovEstq: TLargeintField;
    QrVSOutItsNO_FORNECE: TWideStringField;
    QrVSOutCabTemIMEIMrt: TIntegerField;
    QrPalletsPallet: TLargeintField;
    QrPalletsGraGruX: TLargeintField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsAreaP2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsSerieFch: TLargeintField;
    QrPalletsFicha: TLargeintField;
    QrPalletsNO_TTW: TWideStringField;
    QrPalletsID_TTW: TLargeintField;
    N1: TMenuItem;
    CorrigeFornecedores1: TMenuItem;
    QrVSOutNFeCab: TmySQLQuery;
    QrVSOutNFeCabCodigo: TIntegerField;
    QrVSOutNFeCabMovimCod: TIntegerField;
    QrVSOutNFeCabide_serie: TIntegerField;
    QrVSOutNFeCabide_nNF: TIntegerField;
    QrVSOutNFeCabOriCod: TIntegerField;
    QrVSOutNFeCabLk: TIntegerField;
    QrVSOutNFeCabDataCad: TDateField;
    QrVSOutNFeCabDataAlt: TDateField;
    QrVSOutNFeCabUserCad: TIntegerField;
    QrVSOutNFeCabUserAlt: TIntegerField;
    QrVSOutNFeCabAlterWeb: TSmallintField;
    QrVSOutNFeCabAtivo: TSmallintField;
    DsVSOutNFeCab: TDataSource;
    QrVSOutNFeIts: TmySQLQuery;
    QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField;
    QrVSOutNFeItsCodigo: TIntegerField;
    QrVSOutNFeItsControle: TIntegerField;
    QrVSOutNFeItsItemNFe: TIntegerField;
    QrVSOutNFeItsGraGruX: TIntegerField;
    QrVSOutNFeItsPecas: TFloatField;
    QrVSOutNFeItsPesoKg: TFloatField;
    QrVSOutNFeItsAreaM2: TFloatField;
    QrVSOutNFeItsAreaP2: TFloatField;
    QrVSOutNFeItsValorT: TFloatField;
    QrVSOutNFeItsTpCalcVal: TSmallintField;
    QrVSOutNFeItsValorU: TFloatField;
    QrVSOutNFeItsTribDefSel: TIntegerField;
    QrVSOutNFeItsLk: TIntegerField;
    QrVSOutNFeItsDataCad: TDateField;
    QrVSOutNFeItsDataAlt: TDateField;
    QrVSOutNFeItsUserCad: TIntegerField;
    QrVSOutNFeItsUserAlt: TIntegerField;
    QrVSOutNFeItsAlterWeb: TSmallintField;
    QrVSOutNFeItsAtivo: TSmallintField;
    DsVSOutNFeIts: TDataSource;
    BtVSOutNFeCab: TBitBtn;
    BtVSOutNFeIts: TBitBtn;
    PMVSOutNFeCab: TPopupMenu;
    IncluiSerieNumeroDeNFe1: TMenuItem;
    AlteraSerieNumeroDeNFe1: TMenuItem;
    ExcluiSerieNumeroDeNFe1: TMenuItem;
    PMVSOutNFeIts: TPopupMenu;
    IncluiItemdeNFeIts1: TMenuItem;
    AlteraItemdeNFeIts1: TMenuItem;
    ExcluiItemdeNFeIts1: TMenuItem;
    DBGrid5: TDBGrid;
    DBGrid2: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid4: TDBGrid;
    frxWET_CURTI_019_00_B: TfrxReport;
    PackingListModelo21: TMenuItem;
    frxWET_CURTI_019_00_A2: TfrxReport;
    PackingListModeloA21: TMenuItem;
    QrVSOutItsItemNFe: TLargeintField;
    RecalculaTotais1: TMenuItem;
    CorrigeNFeitensVS1: TMenuItem;
    PMNovo: TPopupMenu;
    CorrigeNFeitensVSapartirdesteprocesso1: TMenuItem;
    EdSerieV: TdmkEdit;
    QrVSOutCabSerieV: TSmallintField;
    QrVSOutCabNome: TWideStringField;
    frxDsVSOutCab: TfrxDBDataset;
    ItsExcluiPallet1: TMenuItem;
    EdNome: TdmkEdit;
    CorrigeMovimIDapartirdestavenda1: TMenuItem;
    QrClienteCNPJ_CPF: TWideStringField;
    LaCliente: TLabel;
    QrVSOutItsGGXRcl: TLargeintField;
    Corrigeartigodebaixa1: TMenuItem;
    IMEIdeorigem1: TMenuItem;
    QrVSOutItsIxxMovIX: TLargeintField;
    QrVSOutItsIxxFolha: TLargeintField;
    QrVSOutItsIxxLinha: TLargeintField;
    otal1: TMenuItem;
    Parcial1: TMenuItem;
    QrVSMOEnvAvu: TmySQLQuery;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    DsVSMOEnvAVMI: TDataSource;
    TsFrCompr: TTabSheet;
    PnFrCompr: TPanel;
    DBGVSMOEnvAvu: TdmkDBGridZTO;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    N2: TMenuItem;
    N3: TMenuItem;
    AtrelamentoFreteCompra1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrVSOutItsCusFrtAvuls: TFloatField;
    DsCli: TDataSource;
    QrCli: TMySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliUF: TFloatField;
    QrCliLograd: TFloatField;
    QrCliCEP: TFloatField;
    QrCliNUMERO: TFloatField;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    Label23: TLabel;
    Label65: TLabel;
    SpeedButton5: TSpeedButton;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    EdPedido: TdmkEdit;
    DBEdit21: TDBEdit;
    GroupBox1: TGroupBox;
    Label24: TLabel;
    DBText1: TDBText;
    Label25: TLabel;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit24: TDBEdit;
    MeEnderecoEntrega1: TMemo;
    QrEntregaCli: TMySQLQuery;
    QrEntregaCliE_ALL: TWideStringField;
    QrEntregaCliCNPJ_TXT: TWideStringField;
    QrEntregaCliNOME_TIPO_DOC: TWideStringField;
    QrEntregaCliTE1_TXT: TWideStringField;
    QrEntregaCliNUMERO_TXT: TWideStringField;
    QrEntregaCliCEP_TXT: TWideStringField;
    QrEntregaCliL_CNPJ: TWideStringField;
    QrEntregaCliL_CPF: TWideStringField;
    QrEntregaCliL_Nome: TWideStringField;
    QrEntregaCliLograd: TSmallintField;
    QrEntregaCliNOMELOGRAD: TWideStringField;
    QrEntregaCliRUA: TWideStringField;
    QrEntregaCliNUMERO: TIntegerField;
    QrEntregaCliCOMPL: TWideStringField;
    QrEntregaCliBAIRRO: TWideStringField;
    QrEntregaCliLCodMunici: TIntegerField;
    QrEntregaCliNO_MUNICI: TWideStringField;
    QrEntregaCliNOMEUF: TWideStringField;
    QrEntregaCliCEP: TIntegerField;
    QrEntregaCliCodiPais: TIntegerField;
    QrEntregaCliNO_PAIS: TWideStringField;
    QrEntregaCliENDEREF: TWideStringField;
    QrEntregaCliTE1: TWideStringField;
    QrEntregaCliEmail: TWideStringField;
    QrEntregaCliL_IE: TWideStringField;
    QrEntregaEnti: TMySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiNUMERO: TIntegerField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiCEP: TIntegerField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    QrEntregaEntiE_ALL: TWideStringField;
    QrEntregaEntiCNPJ_TXT: TWideStringField;
    QrEntregaEntiNOME_TIPO_DOC: TWideStringField;
    QrEntregaEntiTE1_TXT: TWideStringField;
    QrEntregaEntiFAX_TXT: TWideStringField;
    QrEntregaEntiNUMERO_TXT: TWideStringField;
    QrEntregaEntiCEP_TXT: TWideStringField;
    PCDados: TPageControl;
    TabSheet4: TTabSheet;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    TabSheet5: TTabSheet;
    GroupBox2: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label37: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    GroupBox4: TGroupBox;
    Label35: TLabel;
    DBText2: TDBText;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBMemo3: TDBMemo;
    DBEdit31: TDBEdit;
    MeEnderecoEntrega2: TMemo;
    QrVSOutCabPedido: TIntegerField;
    QrVSPedCab: TMySQLQuery;
    QrVSPedCabNOMEVENDEDOR: TWideStringField;
    QrVSPedCabNOMECLIENTE: TWideStringField;
    QrVSPedCabNOMETRANSP: TWideStringField;
    QrVSPedCabNO_CONDICAOPG: TWideStringField;
    QrVSPedCabCodigo: TIntegerField;
    QrVSPedCabEmpresa: TIntegerField;
    QrVSPedCabCliente: TIntegerField;
    QrVSPedCabVendedor: TIntegerField;
    QrVSPedCabDataF: TDateField;
    QrVSPedCabValor: TFloatField;
    QrVSPedCabObz: TWideStringField;
    QrVSPedCabTransp: TIntegerField;
    QrVSPedCabCondicaoPg: TIntegerField;
    QrVSPedCabPedidCli: TWideStringField;
    QrVSPedCabNome: TWideStringField;
    QrVSPedCabLk: TIntegerField;
    QrVSPedCabDataCad: TDateField;
    QrVSPedCabDataAlt: TDateField;
    QrVSPedCabUserCad: TIntegerField;
    QrVSPedCabUserAlt: TIntegerField;
    QrVSPedCabAlterWeb: TSmallintField;
    QrVSPedCabPecas: TFloatField;
    QrVSPedCabPesoKg: TFloatField;
    QrVSPedCabAreaM2: TFloatField;
    QrVSPedCabAreaP2: TFloatField;
    QrVSPedCabAtivo: TSmallintField;
    QrVSPedCabNOMEEMP: TWideStringField;
    DsVSPedCab: TDataSource;
    QrVSPedCabEntregaEnti: TIntegerField;
    QrVSPedCabFilial: TIntegerField;
    QrVSPedCabL_Ativo: TSmallintField;
    QrCorda: TMySQLQuery;
    QrCordaCodigo: TIntegerField;
    QrVSOutItsPedItsVda: TLargeintField;
    DBEdit20: TDBEdit;
    DBEdit27: TDBEdit;
    frxWET_CURTI_019_01: TfrxReport;
    QrVmiNfIt: TMySQLQuery;
    QrVmiNfItItemNFe: TIntegerField;
    QrVmiNfItGGXRcl: TIntegerField;
    QrVmiNfItNO_PRD_TAM_COR: TWideStringField;
    N4: TMenuItem;
    SadaxPedido1: TMenuItem;
    frxDsVmiNfIt: TfrxDBDataset;
    QrVmiNfImeis: TMySQLQuery;
    frxDsVmiNfImeis: TfrxDBDataset;
    QrVmiNfItPedItsVda: TIntegerField;
    QrVmiNfImeisPallet: TIntegerField;
    QrVmiNfImeisGraGruX: TIntegerField;
    QrVmiNfImeisPecas: TFloatField;
    QrVmiNfImeisPesoKg: TFloatField;
    QrVmiNfImeisAreaM2: TFloatField;
    QrVmiNfImeisAreaP2: TFloatField;
    QrVmiNfImeisSigla: TWideStringField;
    QrVmiNfImeisSimbolo: TWideStringField;
    QrVmiNfImeisPrecoVal: TFloatField;
    QrVmiNfImeisTotalItem: TFloatField;
    QrVmiNfImeisMediaM2: TFloatField;
    QrVmiNfImeisMediaP2: TFloatField;
    QrVmiNfImeisPrecoTipo: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSOutCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSOutCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSOutCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExcluiIMEI1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSOutCabBeforeClose(DataSet: TDataSet);
    procedure ItemComrastreio1Click(Sender: TObject);
    procedure ItemSemrastreio1Click(Sender: TObject);
    procedure PorIMEI1Click(Sender: TObject);
    procedure PorPallet1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PackingList1Click(Sender: TObject);
    procedure QrPalletsCalcFields(DataSet: TDataSet);
    procedure frxWET_CURTI_019_00_A1GetValue(const VarName: string;
      var Value: Variant);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrVSOutItsAfterOpen(DataSet: TDataSet);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure IncluiitemdeNF1Click(Sender: TObject);
    procedure AlteraitemdeNF1Click(Sender: TObject);
    procedure InformaItemdeNFe1Click(Sender: TObject);
    procedure BtTribIncItsClick(Sender: TObject);
    procedure IncluiTributo1Click(Sender: TObject);
    procedure AlteraTributoAtual1Click(Sender: TObject);
    procedure ExcluiTributoAtual1Click(Sender: TObject);
    procedure CorrigeFornecedores1Click(Sender: TObject);
    procedure IncluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure AlteraSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiItemdeNFeIts1Click(Sender: TObject);
    procedure PMVSOutNFeItsPopup(Sender: TObject);
    procedure PMVSOutNFeCabPopup(Sender: TObject);
    procedure QrVSOutNFeItsAfterScroll(DataSet: TDataSet);
    procedure QrVSOutNFeItsBeforeClose(DataSet: TDataSet);
    procedure QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
    procedure QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
    procedure BtVSOutNFeCabClick(Sender: TObject);
    procedure BtVSOutNFeItsClick(Sender: TObject);
    procedure PackingListModelo21Click(Sender: TObject);
    procedure PackingListModeloA21Click(Sender: TObject);
    procedure RecalculaTotais1Click(Sender: TObject);
    procedure CorrigeNFeitensVS1Click(Sender: TObject);
    procedure CorrigeNFeitensVSapartirdesteprocesso1Click(Sender: TObject);
    procedure ItsExcluiPallet1Click(Sender: TObject);
    procedure CorrigeMovimIDapartirdestavenda1Click(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure Corrigeartigodebaixa1Click(Sender: TObject);
    procedure IMEIdeorigem1Click(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrEntregaEntiCalcFields(DataSet: TDataSet);
    procedure QrEntregaCliCalcFields(DataSet: TDataSet);
    procedure EdPedidoRedefinido(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrCliAfterOpen(DataSet: TDataSet);
    procedure QrVSPedCabAfterOpen(DataSet: TDataSet);
    procedure QrVSPedCabBeforeClose(DataSet: TDataSet);
    procedure SadaxPedido1Click(Sender: TObject);
    procedure QrVmiNfItAfterScroll(DataSet: TDataSet);
  private
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEnderecoDeEntrega2();
    procedure MostraFormVSOutUnk(SQLType: TSQLType);
    procedure MostraFormVSOutKno(SQLType: TSQLType);
    procedure MostraFormVSOutPeso(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraFormVSOutPall(SQLType: TSQLType);
    procedure MostraFormVSOutAltVMI(SQLType: TSQLType);
    procedure ReopenVSOutNFeCab(Codigo: Integer);
    procedure ReopenVSOutNFeIts(Controle: Integer);
    procedure ReopenVSPedCab(Pedido: Integer);
    //
    function  ObtemItemNFe(): Integer;
    //
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens(MovimCod: Integer);
    procedure ReopenVSOutIts(Controle: Integer);
    procedure ReopenTribIncIts(Controle: Integer);
    procedure CorrigeFornecedores();
    procedure CorrigeNFes();

  end;

var
  FmVSOutCab: TFmVSOutCab;
const
  FFormatFloat = '00000';
  FThisMovimID = TEstqMovimID.emidVenda;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
(*VSOutUnk,*) VSOutKno, UnVS_CRC_PF, VSOutPal, VSOutPeso(*, VSOutNFI*),
  {$IfDef sAllVS}UnTributos_PF, ModVS, UnVS_PF, {$EndIf}
  VSOutAltVMI, ModVS_CRC, VSOutPedPes;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSOutCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSOutCab.EdClienteRedefinido(Sender: TObject);
begin
  if EdCliente.VAlueVariant = 0 then
    LaCliente.Caption := '...'
  else
    LaCliente.Caption := Geral.FormataCNPJ_TT(QrClienteCNPJ_CPF.Value);
end;

procedure TFmVSOutCab.EdPedidoRedefinido(Sender: TObject);
begin
  ReopenVSPedCab(EdPedido.ValueVariant);
  if (EdPedido.ValueVariant <> 0) and (EdPedido.Enabled = True) then
  begin
    if QrVSPedCab.RecordCount = 0 then
    begin
      Geral.MB_Aviso(
      'Pedido n�o localizado ou com o status diferente de liberado (Status=' +
      Geral.FF0(Integer(TStausPedVda.spvLiberado)) + ')!');
      EdPedido.ValueVariant := 0;
    end else
    begin
      EdEmpresa.ValueVariant := QrVSPedCabFilial.Value;
      CBEmpresa.KeyValue     := QrVSPedCabFilial.Value;
      EdCliente.ValueVariant := QrVSPedCabCliente.Value;
      CBCliente.KeyValue     := QrVSPedCabCliente.Value;
    end;
  end;
end;

procedure TFmVSOutCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI);
  //
  LocCod(QrVSOutCabCodigo.Value,QrVSOutCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.ExcluiItemdeNFeIts1Click(Sender: TObject);
var
  Controle: Integer;
begin
{$IfDef sAllVS}
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfeIts', 'Controle', QrVSOutNFeItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeIts,
      QrVSOutNFeItsControle, QrVSOutNFeItsControle.Value);
    ReopenVSOutNFeIts(Controle);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.MostraEnderecoDeEntrega2();
var
  Entidade: Integer;
begin
  if (QrVSPedCabL_Ativo.Value = 1) and (QrVSPedCabEntregaEnti.Value = 0) then
  begin
    QrEntregaCli.Close;
    QrEntregaCli.Params[00].AsInteger := QrVSPedCabCliente.Value;
    UMyMod.AbreQuery(QrEntregaCli, Dmod.MyDB, 'TFmFatDivGer2.QrCliAfterOpen()');
    //
    //DsEntregaEnti.DataSet := QrEntregaCli;
    MeEnderecoEntrega1.Text := QrEntregaCliE_ALL.Value;
    MeEnderecoEntrega2.Text := QrEntregaCliE_ALL.Value;
  end else
  begin
    if QrVSPedCabEntregaEnti.Value = 0 then
      Entidade := QrVSPedCabCliente.Value
    else
      Entidade := QrVSPedCabEntregaEnti.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntregaEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY NOME_ENT',
    '']);
    MeEnderecoEntrega1.Text := QrEntregaEntiE_ALL.Value;
    MeEnderecoEntrega2.Text := QrEntregaEntiE_ALL.Value;
  end;
end;

procedure TFmVSOutCab.MostraFormVSOutAltVMI(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType <> stUpd then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSOutCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSOutAltVMI, FmVSOutAltVMI, afmoNegarComAviso) then
  begin
    FmVSOutAltVMI.ImgTipo.SQLType := SQLType;
    FmVSOutAltVMI.FQrCab := QrVSOutCab;
    FmVSOutAltVMI.FDsCab := DsVSOutCab;
    FmVSOutAltVMI.FQrIts := QrVSOutIts;
    FmVSOutAltVMI.FDataHora := QrVSoutCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmVSOutAltVMI.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSOutAltVMI.FDataHora := QrVSOutCabDtVenda.Value;
      FmVSOutAltVMI.EdControle.ValueVariant := QrVSOutItsControle.Value;
      //
      FmVSOutAltVMI.EdItemNFe.ValueVariant := QrVSOutItsItemNFe.Value;
      FmVSOutAltVMI.EdGGXRcl.ValueVariant  := QrVSOutItsGGXRcl.Value;
      FmVSOutAltVMI.CBGGXRcl.KeyValue      := QrVSOutItsGGXRcl.Value;
      //
      FmVSOutAltVMI.EdPecas.ValueVariant    := -QrVSOutItsPecas.Value;
      FmVSOutAltVMI.EdPesoKg.ValueVariant   := -QrVSOutItsPesoKg.Value;
      FmVSOutAltVMI.EdAreaM2.ValueVariant   := -QrVSOutItsAreaM2.Value;
      FmVSOutAltVMI.EdAreaP2.ValueVariant   := -QrVSOutItsAreaP2.Value;
      FmVSOutAltVMI.EdValorT.ValueVariant   := -QrVSOutItsValorT.Value;
      //
    end;
    FmVSOutAltVMI.ShowModal;
    FmVSOutAltVMI.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSOutCab.MostraFormVSOutKno(SQLType: TSQLType);
var
  ItemNFe, Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSOutCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSOutKno, FmVSOutKno, afmoNegarComAviso) then
  begin
    FmVSOutKno.ImgTipo.SQLType := SQLType;
    FmVSOutKno.FQrCab := QrVSOutCab;
    FmVSOutKno.FDsCab := DsVSOutCab;
    FmVSOutKno.FQrIts := QrVSOutIts;
    FmVSOutKno.FDataHora := QrVSoutCabDtVenda.Value;
    FmVSOutKno.FPedido  := QrVSOutCabPedido.Value;
    VS_CRC_PF.ReopenItensPedido(FmVSOutKno.QrVSPedIts, QrVSOutCabPedido.Value);
    if SQLType = stIns then
    begin
      ItemNFe  := ObtemItemNFe();
      FmVSOutKno.EdItemNFe.ValueVariant := ItemNFe;
      //FmVSOutKno.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmVSOutKno.FDataHora := QrVSOutCabDtVenda.Value;
      FmVSOutKno.EdControle.ValueVariant := QrVSOutItsControle.Value;
      //
      FmVSOutKno.EdGragruX.ValueVariant  := QrVSOutItsGraGruX.Value;
      FmVSOutKno.CBGragruX.KeyValue      := QrVSOutItsGraGruX.Value;
      FmVSOutKno.EdPecas.ValueVariant    := -QrVSOutItsPecas.Value;
      FmVSOutKno.EdPesoKg.ValueVariant   := -QrVSOutItsPesoKg.Value;
      FmVSOutKno.EdAreaM2.ValueVariant   := -QrVSOutItsAreaM2.Value;
      FmVSOutKno.EdAreaP2.ValueVariant   := -QrVSOutItsAreaP2.Value;
      //FmVSOutKno.EdValorT.ValueVariant   := -QrVSOutItsValorT.Value;
      FmVSOutKno.EdPallet.ValueVariant   := QrVSOutItsPallet.Value;
      //
    end;
}
    end;
    FmVSOutKno.ShowModal;
    FmVSOutKno.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSOutCab.MostraFormVSOutPall(SQLType: TSQLType);
var
  ItemNFe, Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSOutCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSOutPal, FmVSOutPal, afmoNegarComAviso) then
  begin
    FmVSOutPal.ImgTipo.SQLType := SQLType;
    FmVSOutPal.FQrCab := QrVSOutCab;
    FmVSOutPal.FDsCab := DsVSOutCab;
    FmVSOutPal.FQrIts := QrVSOutIts;
    FmVSOutPal.FDataHora := QrVSoutCabDtVenda.Value;
    FmVSOutPal.FEmpresa  := QrVSOutCabEmpresa.Value;
    FmVSOutPal.FPedido  := QrVSOutCabPedido.Value;
    VS_CRC_PF.ReopenItensPedido(FmVSOutPal.QrVSPedIts, QrVSOutCabPedido.Value);
    FmVSOutPal.PesquisaPallets();
    //
    ItemNFe  := ObtemItemNFe();
    FmVSOutPal.EdItemNFe.ValueVariant := ItemNFe;
    //
    FmVSOutPal.ShowModal;
    FmVSOutPal.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSOutCab.MostraFormVSOutPeso(SQLType: TSQLType; Quanto: TPartOuTodo);
var
  Codigo: Integer;
begin
  VS_CRC_PF.MostraFormVSOutPeso(stIns, QrVSOutCab, DsVSOutCab, QrVSOutIts,
    QrVSOutCabDtVenda.Value,  Quanto, QrVSOutCabPedido.Value);
  (*
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSOutCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSOutPeso, FmVSOutPeso, afmoNegarComAviso) then
  begin
    FmVSOutPeso.ImgTipo.SQLType := SQLType;
    FmVSOutPeso.FQrCab := QrVSOutCab;
    FmVSOutPeso.FDsCab := DsVSOutCab;
    FmVSOutPeso.FQrIts := QrVSOutIts;
    FmVSOutPeso.FDataHora := QrVSoutCabDtVenda.Value;
    if SQLType = stIns then
    begin
    end else
    begin
    end;
    FmVSOutPeso.ShowModal;
    FmVSOutPeso.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
*)
end;

procedure TFmVSOutCab.MostraFormVSOutUnk(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmVSOutUnk, FmVSOutUnk, afmoNegarComAviso) then
  begin
    FmVSOutUnk.ImgTipo.SQLType := SQLType;
    FmVSOutUnk.FEmpresa := QrVSoutCabEmpresa.Value;
    FmVSOutUnk.FQrCab := QrVSOutCab;
    FmVSOutUnk.FDsCab := DsVSOutCab;
    FmVSOutUnk.FQrIts := QrVSOutIts;
    FmVSOutUnk.FDataHora := QrVSoutCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmVSOutUnk.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSOutUnk.FDataHora := QrVSOutCabDtVenda.Value;
      FmVSOutUnk.EdControle.ValueVariant := QrVSOutItsControle.Value;
      //
      FmVSOutUnk.EdGragruX.ValueVariant    := QrVSOutItsGraGruX.Value;
      FmVSOutUnk.CBGragruX.KeyValue        := QrVSOutItsGraGruX.Value;
      FmVSOutUnk.EdFornecedor.ValueVariant := QrVSOutItsTerceiro.Value;
      FmVSOutUnk.CBFornecedor.KeyValue     := QrVSOutItsTerceiro.Value;
      FmVSOutUnk.EdPecas.ValueVariant      := -QrVSOutItsPecas.Value;
      FmVSOutUnk.EdPesoKg.ValueVariant     := -QrVSOutItsPesoKg.Value;
      FmVSOutUnk.EdAreaM2.ValueVariant     := -QrVSOutItsAreaM2.Value;
      FmVSOutUnk.EdAreaP2.ValueVariant     := -QrVSOutItsAreaP2.Value;
      FmVSOutUnk.EdValorT.ValueVariant     := -QrVSOutItsValorT.Value;
      // Deve ser por ultimo
      FmVSOutUnk.EdPallet.ValueVariant     := QrVSOutItsPallet.Value;
      FmVSOutUnk.CBPallet.KeyValue         := QrVSOutItsPallet.Value;
      //
    end;
    FmVSOutUnk.ShowModal;
    FmVSOutUnk.Destroy;
  end;
*)
end;

function TFmVSOutCab.ObtemItemNFe(): Integer;
var
  Qry: TmySQLQuery;
  Itens: array of String;
  I: Integer;
begin
  Result := 0;
  if QrVSOutIts.RecordCount > 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(ItemNFe) ItemNFe ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
      '']);
      Result := Qry.FieldByName('ItemNFe').AsInteger;
    finally
      Qry.Free;
    end;
  end;
  Result := Result + 1;
  if Result > 1 then
  begin
    SetLength(Itens, Result);
    for I := 0 to Result - 1 do
      Itens[I] := 'Item ' + Geral.FF0(I + 1);
    Result := MyObjects.SelRadioGroup('Item da NFe',
    'Escolha o item da NFe', Itens, Result);
    Result := Result + 1;
  end;
end;

procedure TFmVSOutCab.otal1Click(Sender: TObject);
begin
  MostraFormVSOutPeso(stIns, TPartOuTodo.ptTotal);
end;

procedure TFmVSOutCab.PackingList1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_019_00_A1, [
    DModG.frxDsDono,
    frxDsVSOutCab,
    frxDsPallets
    ]);
  MyObjects.frxMostra(frxWET_CURTI_019_00_A1, 'Packing List');
end;

procedure TFmVSOutCab.PackingListModelo21Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_019_00_B, [
    DModG.frxDsDono,
    frxDsVSOutCab,
    frxDsPallets
    ]);
  MyObjects.frxMostra(frxWET_CURTI_019_00_B, 'Packing List');
end;

procedure TFmVSOutCab.PackingListModeloA21Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_019_00_A2, [
    DModG.frxDsDono,
    frxDsVSOutCab,
    frxDsPallets
    ]);
  MyObjects.frxMostra(frxWET_CURTI_019_00_A2, 'Packing List');
end;

procedure TFmVSOutCab.Parcial1Click(Sender: TObject);
begin
  MostraFormVSOutPeso(stIns, TPartOuTodo.ptParcial);
end;

procedure TFmVSOutCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSOutCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSOutCab, QrVSOutIts);
end;

procedure TFmVSOutCab.PMItsPopup(Sender: TObject);
var
  HabilitaIMEI: Boolean;
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSOutCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSOutIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiIMEI1, QrVSOutIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiPallet1, QrPallets);
  //
  ItsExcluiIMEI1.Enabled   := ItsExcluiIMEI1.Enabled and (PCItens.ActivePageIndex = 1);
  ItsExcluiPallet1.Enabled := ItsExcluiPallet1.Enabled and (PCItens.ActivePageIndex = 0);
  //
  InformaNmerodaRME1.Enabled := ItsExcluiIMEI1.Enabled;
  InformaItemdeNFe1.Enabled  := ItsExcluiIMEI1.Enabled;
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSOutItsID_TTW.Value, [ItsAltera1, ItsExcluiIMEI1]);
  ItsAltera1.Enabled := ItsAltera1.Enabled and (PCItens.ActivePageIndex = 1);
  //
  HabilitaIMEI := (PCItens.ActivePageIndex = 1) and
    (QrVSOutIts.State <> dsInactive) and (QrVSOutIts.RecordCount > 0);
  Corrigeartigodebaixa1.Enabled := HabilitaIMEI;
  IMEIdeorigem1.Enabled := HabilitaIMEI;
  //
end;

procedure TFmVSOutCab.PMVSOutNFeCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSerieNumeroDeNFe1, QrVSOutCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSerieNumeroDeNFe1, QrVSOutNFeCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiSerieNumeroDeNFe1, QrVSOutNFeCab, QrVSOutNFeIts);
end;

procedure TFmVSOutCab.PMVSOutNFeItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiItemdeNFeIts1, QrVSOutNFeCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraItemdeNFeIts1, QrVSOutNFeIts);
  MyObjects.HabilitaMenuItemCabDel(ExcluiItemdeNFeIts1, QrVSOutNFeIts, QrTribIncIts);
end;

procedure TFmVSOutCab.PorIMEI1Click(Sender: TObject);
begin
  MostraFormVSOutKno(stIns);
end;

procedure TFmVSOutCab.PorPallet1Click(Sender: TObject);
begin
  MostraFormVSOutPall(stIns);
end;

procedure TFmVSOutCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSOutCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSOutCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsoutcab';
  VAR_GOTOMYSQLTABLE := QrVSOutCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA');
  VAR_SQLx.Add('FROM vsoutcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.transporta');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSOutCab.DGDadosDblClick(Sender: TObject);
(*
var
  Campo, Texto: String;
  Controle, Inteiro: Integer;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  if (Campo = 'IxxFolha') or (Campo = 'IxxLinha') then
  begin
    Inteiro := QrVSOutIts.FieldByName(Campo).AsInteger;
    Texto := Geral.FF0(Inteiro);
    if InputQuery('Novo valor para "' + Campo + '"', 'Informe o novo valor:',
    Texto) then
    begin
      Inteiro := Geral.IMV(Texto);
      Controle := QrVSOutItsControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_VMI, False, [
      Campo], ['Controle'], [Inteiro], [Controle], True) then
      begin
        ReopenVSOutIts(QrVSOutItsControle.Value);
        PCItens.ActivePageIndex := 1;
      end
    end;
  end;
*)
begin
  if VS_CRC_PF.EditaIxx(TDBGrid(DGDados), QrVSOutIts) then
    PCItens.ActivePageIndex := 1;
end;

procedure TFmVSOutCab.ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfDef sAllVS}
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfecab', 'Codigo', QrVSOutNFeCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeCab,
      QrVSOutNFeCabCodigo, QrVSOutNFeCabCodigo.Value);
    ReopenVSOutNFeCab(Codigo);
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.ExcluiTributoAtual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrTribIncItsControle.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de tributo?',
  'tribincits', 'Controle', Controle, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribIncIts,
      QrTribIncItsControle, QrTribIncItsControle.Value);
    ReopenTribIncIts(Controle);
  end;
end;

procedure TFmVSOutCab.IMEIdeorigem1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovIts(QrVSOutItsSrcNivel2.VAlue);
end;

procedure TFmVSOutCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, QrVSOutIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSOutCabCliente.Value, QrVSOutCabSerieV.Value, QrVSOutCabNFV.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSOutCabMovimCod.Value, 0);
  //
  LocCod(QrVSOutCabCodigo.Value,QrVSOutCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.IncluiitemdeNF1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
    QrVSOutCabEmpresa.Value, QrVSOutCabDtVenda.Value,
    siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSOutNfeCabMovimCod.Value, TEstqMovimID.emidVenda, [(**)], [eminSemNiv]);
end;
{
var
  Qry: TmySQLQuery;
  ItemNFe: Integer;
  procedure InsereAtual();
  var
    Codigo, Controle, GraGruX, TpCalcVal, TribDefSel: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT, ValorU: Double;
  begin
    Codigo         := QrVSOutCabCodigo.Value;
    Controle       := 0;
    //ItemNFe        := Item + 1;
    GraGruX        := Qry.FieldByName('GraGruX').AsInteger;
    Pecas          := -Qry.FieldByName('Pecas').AsFloat;
    PesoKg         := -Qry.FieldByName('PesoKg').AsFloat;
    AreaM2         := -Qry.FieldByName('AreaM2').AsFloat;
    AreaP2         := -Qry.FieldByName('AreaP2').AsFloat;
    ValorT         := 0;
    TpCalcVal      := Integer(ptcAreaM2);
    ValorU         := 0;
    TribDefSel     := 0;
    //
    Controle := UMyMod.BPGS1I32('vsoutnfi', 'Controle', '', '', tsPos, stIns, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsoutnfi', False, [
    'Codigo', 'ItemNFe', 'GraGruX',
    'Pecas', 'PesoKg', 'AreaM2',
    'AreaP2', 'ValorT', 'TpCalcVal',
    'ValorU', 'TribDefSel'], [
    'Controle'], [
    Codigo, ItemNFe, GraGruX,
    Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, TpCalcVal,
    ValorU, TribDefSel], [
    Controle], True) then
    begin
    end;
  end;
var
  Itens: array of Integer;
  I: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.ItemNFe ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
    'AND vmi.ItemNFe <> 0 ',
    'AND ( ',
    '  NOT vmi.ItemNFe IN ',
    '  ( ',
    '    SELECT ItemNFe ',
    '    FROM vsoutnfi ',
    '    WHERE Codigo=' + Geral.FF0(QrVSOutCabCodigo.Value),
    '  ) ',
    ') ',
    'ORDER BY ItemNFe ',
    '']);
    if Qry.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� item a ser criado!');
      Exit;
    end;
    SetLength(Itens, Qry.RecordCount);
    Qry.First;
    while not Qry.Eof do
    begin
      Itens[Qry.RecNo -1] := Qry.FieldByName('ItemNFe').AsInteger;
      //
      Qry.Next;
    end;
    for I := Low(Itens) to High(Itens) do
    begin
      ItemNFe := I + 1;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas, ',
      'SUM(vmi.AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(PesoKg) PesoKg ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
      'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
      'AND ItemNFe=' + Geral.FF0(ItemNFe),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        InsereAtual();
        //
        Qry.Next;
      end;
    end;
    ReopenVSOutNFI(0);
  finally
    Qry.Free;
  end;
end;
}

procedure TFmVSOutCab.IncluiSerieNumeroDeNFe1Click(Sender: TObject);
const
  Codigo    = 0;
var
  ide_serie, ide_nNF: Integer;
  Qry: TmySQLQuery;
begin
  ide_serie := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Max(ide_serie)ide_serie ',
    'FROM vsoutnfecab ',
    '']);
    ide_serie := Qry.FieldByName('ide_serie').AsInteger;
  finally
    Qry.Free;
  end;
  ide_nNF := QrVSOutCabNFV.Value;
  if VS_CRC_PF.MostraFormVSNFeOutCab(stIns, QrVSOutNfeCab, QrVSOutCabMovimCod.Value,
  QrVSOutCabCodigo.Value, Codigo, ide_serie, ide_nNF) then
    VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
      QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSOutCabEmpresa.Value, QrVSOutCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSOutNfeCabMovimCod.Value, TEstqMovimID.emidVenda, [(**)], [eminSemNiv]);
end;

procedure TFmVSOutCab.IncluiTributo1Click(Sender: TObject);
const
  Controle = 0;
  Operacao = 0;
  Tributo  = 0;
  ValorFat = 0.00;
  BaseCalc = 0.00;
  Percent  = 0.00;
  VUsoCalc = 0.00;
  ValrTrib = 0.00;
var
  FatNum, FatParcela, Empresa: Integer;
  Data, Hora: TDateTime;
begin
{$IfDef sAllVS}
  FatNum     := QrVSOutNFeCabCodigo.Value;
  FatParcela := QrVSOutNFeItsItemNFe.Value;
  Empresa    := QrVSOutCabEmpresa.Value;
  Data       := QrVSOutCabDtVenda.Value;
  Hora       := QrVSOutCabDtVenda.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stIns, QrTribIncIts, VAR_FATID_1009,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siNegativo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}end;

procedure TFmVSOutCab.InformaItemdeNFe1Click(Sender: TObject);
var
  Txt: String;
  ItemNFe, Controle: Integer;
begin
  Txt := '1';
  if InputQuery('Item de NFe', 'Informe o n�mero do item', Txt) then
  begin
    ItemNFe := Geral.IMV(Txt);
    if (ItemNFe > 0) and (ItemNFe < 1000) then
    begin
      Controle := QrVSOutItsControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'ItemNFe'], ['Controle'], [
      ItemNFe], [Controle], True) then
      begin
        ReopenVSOutIts(Controle);
        PCItens.ActivePageIndex := 1;
      end;
    end else
      Geral.MB_Aviso('N�mero de item de NFe inv�lido!');
  end;
end;

procedure TFmVSOutCab.InformaNmerodaRME1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.InfoReqMovEstq(QrVSOutItsControle.Value,
    QrVSOutItsReqMovEstq.Value, QrVSOutIts);
  ReopenVSOutIts(QrVSOutItsControle.Value);
  PCItens.ActivePageIndex := 1;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.ItemComrastreio1Click(Sender: TObject);
begin
  MostraFormVSOutKno(stIns);
end;

procedure TFmVSOutCab.ItemSemrastreio1Click(Sender: TObject);
begin
  MostraFormVSOutUnk(stIns);
end;

procedure TFmVSOutCab.ItsAltera1Click(Sender: TObject);
begin
  { cuidado!!!!
  Se for desmarcar:
    1. Fazer edi��o do vsoutfat tamb�m!
    2. Mudar o GGXRcl de todos itens do mesmo ItemNFe
  MostraFormVSOutAltVMI(stUpd);
  }
end;

procedure TFmVSOutCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSOutCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast); Desabilita botao quando fecha mes!
end;

procedure TFmVSOutCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSOutCab.ItsExcluiIMEI1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, I, N, PedItsVda: Integer;
  //
  procedure ExcluiAtual();
  begin
    Codigo    := QrVSOutItsCodigo.Value;
    MovimCod  := QrVSOutItsMovimCod.Value;
    PedItsVda := QrVSOutItsPedItsVda.Value;
    //
    if VS_CRC_PF.ExcluiControleVSMovIts(QrVSOutIts, TIntegerField(QrVSOutItsControle),
    QrVSOutItsControle.Value, CtrlBaix, QrVSOutItsSrcNivel2.Value, False,
    Integer(TEstqMotivDel.emtdWetCurti019), False, False) then
    begin
      VS_CRC_PF.ExcluiVSOutFat(QrVSOutItsControle.Value);
{$IfDef sAllVS}
    //Fazer! pata IMEIS tamb�m
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
    end;
  end;
begin
  if DGDados.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta(
    'Confirma a exclus�o dos itens selecionados?') <> ID_YES then
      Exit;
    try
      N := 0;
      with DGdados.DataSource.DataSet do
      for I := 0 to DGdados.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DGdados.SelectedRows.Items[I]));
        GotoBookmark(DGdados.SelectedRows.Items[I]);
        //
        ExcluiAtual();
        N := N + 1;
      end;
    finally
      VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
      LocCod(Codigo, Codigo);
      Geral.MB_Info(Geral.FF0(N) + ' itens foram exclu�dos!');
    end;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSOutCab.RecalculaTotais1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSOutCabCodigo.Value;
  MovimCod := QrVSOutCabMovimCod.Value;
  QrVSOutIts.First;
  while not QrVSOutIts.Eof do
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(QrVSOutItsSrcNivel2.Value, False);
    // Como fazer?
    //VS_CRC_PF.AtualizaVSPedIts_Vda(PedItsVda);
    //
    QrVSOutIts.Next;
  end;
  VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
  FmVSOutCab.LocCod(Codigo, Codigo);
end;

procedure TFmVSOutCab.ItsExcluiPallet1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, Prox, MovimCod, Pallet, Itens, PedItsVda: Integer;
begin
  Pallet := QrPalletsPallet.Value;
  Itens  := 0;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o do pallet ' + Geral.FF0(Pallet) + '?') = ID_YES then
  begin
    QrVSOutIts.DisableControls;
    try
      QrVSOutIts.First;
      while not QrVSOutIts.Eof do
      begin
        if QrVSOutItsPallet.Value = Pallet then
          Itens := Itens + 1;
        //
        QrVSOutIts.Next;
      end;
    finally
      QrVSOutIts.EnableControls;
    end;
    if Itens > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      QrVSOutIts.DisableControls;
      try
        QrVSOutIts.First;
        //
        while not QrVSOutIts.Eof do
        begin
          if QrVSOutItsPallet.Value = Pallet then
          begin
            Codigo    := QrVSOutItsCodigo.Value;
            MovimCod  := QrVSOutItsMovimCod.Value;
            PedItsVda := QrVSOutItsPedItsVda.Value;
            //
            if VS_CRC_PF.ExcluiControleVSMovIts(QrVSOutIts, TIntegerField(QrVSOutItsControle),
              QrVSOutItsControle.Value, CtrlBaix, QrVSOutItsSrcNivel2.Value, False,
              Integer(TEstqMotivDel.emtdWetCurti019), False, False) then
            begin
              VS_CRC_PF.ExcluiVSOutFat(QrVSOutItsControle.Value);
              VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
{$IfDef sAllVS}
    //Fazer! pata IMEIS tamb�m
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
            end;
          end;
          //
          QrVSOutIts.Next;
        end;
      finally
        LocCod(Codigo, Codigo);
        //
        Prox := GOTOy.LocalizaPriorNextIntQr(QrVSOutIts,
                  TIntegerField(QrVSOutItsControle), QrVSOutItsControle.Value);
        //
        QrVSOutIts.Close;
        UnDmkDAC_PF.AbreQuery(QrVSOutIts, Dmod.MyDB);
        QrVSOutIts.Locate('Controle', Prox, []);
        //
        QrVSOutIts.EnableControls;
        //
        Screen.Cursor := crDefault;
      end;
    end else
      Geral.MB_Erro('N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSOutCab.ReopenVSPedCab(Pedido: Integer);
var
  Corda: String;
begin
  Screen.Cursor := crHourGlass;
  try
  if EdPedido.Enabled = True then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT DISTINCT Codigo ',
    'FROM vspedits ',
    'WHERE Status=' + Geral.FF0(Integer(TStausPedVda.spvLiberado)),
    '']);
    Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Codigo', '-999999999');
  end else
    Corda := Geral.FF0(Pedido);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVSPedCab, Dmod.MyDB, [
  'SELECT ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
  'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ',
  'IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP, ',
  'pc.Nome NO_CONDICAOPG, emp.Filial, emp.L_Ativo, ',
  'vpc.* ',
  'FROM vspedcab vpc ',
  'LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa ',
  'LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente ',
  'LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor ',
  'LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp ',
  'LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg ',
  'WHERE vpc.Codigo IN (' + Corda + ')',
  'AND vpc.Codigo=' + Geral.FF0(Pedido),
  '']);
  finally
      Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSOutCab.ReopenTribIncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
    'SELECT tii.*, tdd.Nome NO_Tributo  ',
    'FROM tribincits tii ',
    'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo',
    'WHERE tii.FatID=' + Geral.FF0(VAR_FATID_1009),
    'AND tii.FatNum=' + Geral.FF0(QrVSOutNFeCabCodigo.Value),
    'AND tii.FatParcela=' + Geral.FF0(QrVSOutNFeItsItemNFe.Value),
    'ORDER BY tii.Controle DESC ',
    '']);
end;

procedure TFmVSOutCab.ReopenVSOutIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVsOutCabTemIMEIMrt.Value;
  //
  // Por IME-I
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM  v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  'ORDER BY vmi.Controle ',
  '']);
*)
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet, vmi.PedItsVda ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVsOutIts, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVsInnIts);
  //
  QrVSOutIts.Locate('Controle', Controle, []);
  // Por Pallet
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  ' SELECT IF(cou.ArtigoImp = "",  ',
  '  CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) TXT_ARTIGO, ',
  'cou.ArtigoImp, cou.ClasseImp,  ',
  'vmi.Pallet, SUM(vmi.Pecas) Pecas,  ',
  'SUM(vmi.AreaM2) AreaM2, SUM(vmi.AreaP2) AreaP2,  ',
  'IF(SUM(vmi.Pecas) = 0, 0, SUM(vmi.AreaM2) / SUM(vmi.Pecas)) MediaM2,  ',
  'CASE  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) < cou.MediaMinM2 THEN 0.000  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) > cou.MediaMaxM2 THEN 2.000  ',
  'ELSE 1.000 END FaixaMediaM2,  ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet  ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle  ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  'GROUP BY vmi.Pallet  ',
  'ORDER BY vmi.Pallet ',
  ' ',
  '']);
*)
  //
  SQL_Flds := Geral.ATS([
  ', IF(cou.ArtigoImp = "",  ',
  '  CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) TXT_ARTIGO, ',
  'cou.ArtigoImp, cou.ClasseImp,  ',
  'IF(SUM(vmi.Pecas) = 0, 0, SUM(vmi.AreaM2) / SUM(vmi.Pecas)) MediaM2,  ',
  'CASE  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) < cou.MediaMinM2 THEN 0.000  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) > cou.MediaMaxM2 THEN 2.000  ',
  'ELSE 1.000 END FaixaMediaM2  ',
  '']);
  SQL_Left := Geral.ATS([
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle  ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Pallet ',
  'ORDER BY vmi.ItemNFe, vmi.Pallet, vmi.Controle ',
  '']);
  //Geral.MB_SQL(nil, QrPallets);
end;

procedure TFmVSOutCab.ReopenVSOutNFeCab(Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsoutnfecab ',
  'WHERE MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.ReopenVSOutNFeIts(Controle: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeIts, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, voi.* ',
  'FROM vsoutnfeits voi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE voi.Codigo=' + Geral.FF0(QrVSOutNFeCabCodigo.Value),
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
  Texto: String;
begin
  if Column.FieldName = 'FaixaMediaM2' then
  begin
    VS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(QrPalletsFaixaMediaM2.Value,
    CorFundo, CorTexto, Texto);
    MyObjects.DesenhaTextoEmDBGrid(DBGrid1, Rect, CorTexto, CorFundo,
      Column.Alignment, Texto(*Column.Field.DisplayText*));
  end;
end;

procedure TFmVSOutCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSOutCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSOutCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSOutCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSOutCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSOutCab.SpeedButton5Click(Sender: TObject);
var
  Pedido, Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmVSOutPedPes, FmVSOutPedPes, afmoNegarComAviso) then
  begin
    Pedido := EdPedido.ValueVariant;
    Empresa := EdEmpresa.ValueVariant;
    FmVSOutPedPes.FEdPedido := EdPedido;
    if Empresa <> 0 then
    begin
      FmVSOutPedPes.EdEmpresa.ValueVariant := Empresa;
      FmVSOutPedPes.CBEmpresa.KeyValue     := Empresa;
    end;
    FmVSOutPedPes.ShowModal;
    FmVSOutPedPes.Destroy;
    EdEmpresa.SetFocus;
  end;
end;

procedure TFmVSOutCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutCab.BtTribIncItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTribIncIts, BtTribIncIts);
end;

procedure TFmVSOutCab.BtVSOutNFeCabClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeCab, BtVSOutNFeCab);
end;

procedure TFmVSOutCab.BtVSOutNFeItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeIts, BtVSOutNFeIts);
end;

procedure TFmVSOutCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSOutCabCodigo.Value;
  Close;
end;

procedure TFmVSOutCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  EdPedido.Enabled := False;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSOutCab, [PnDados],
    [PnEdita], EdEmpresa, ImgTipo, 'vsoutcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSOutCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSOutCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtVenda, DtViagem, DtEntrega, DataHora: String;
  Codigo, MovimCod, Empresa, Cliente, Transporta, CliVenda, SerieV, NFV: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
  Forma, Pedido: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtVenda        := Geral.FDT_TP_Ed(TPDtVenda.Date, EdDtVenda.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  Cliente        := EdCliente.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  NFV            := EdNFV.ValueVariant;
  SerieV         := EdSerieV.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Pedido         := EdPedido.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if not VS_CRC_PF.ValidaCampoNF(0, EdNFV, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtVenda.DateTime < 2, TPDtVenda, 'Defina uma data de Venda!') then Exit;
  //DataChegadaInvalida := TPDtEntrega.Date < TPDtViagem.Date;
  //if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de chegada v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsoutcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsoutcab', False, [
  'MovimCod', 'Empresa', 'DtVenda',
  'DtViagem', 'DtEntrega', 'Cliente',
  'Transporta'(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*),
  'NFV', 'SerieV', 'Nome',
  'Pedido'], [
  'Codigo'], [
  MovimCod, Empresa, DtVenda,
  DtViagem, DtEntrega, Cliente,
  Transporta(* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*),
  NFV, SerieV, Nome,
  Pedido], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidVenda, Codigo)
    else
    begin
      DataHora := DtVenda;
      CliVenda := Cliente;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
      //
      if EdNFV.ValueVariant <> 0 then
        AtualizaNFeItens(MovimCod);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSOutIts.RecordCount = 0) then
    begin
      //MostraVSGArtOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', ['Pallet', 'IME-I',
      'Peso kg (sub produto) - Total (um ou v�rios)',
      'Peso kg (sub produto) - Parcial (um de cada vez)'
      ], -1);
      case Forma of
        0: MostraFormVSOutPall(stIns);
        1: MostraFormVSOutKno(stIns);
        2: MostraFormVSOutPeso(stIns, TPartOuTodo.ptTotal);
        3: MostraFormVSOutPeso(stIns, TPartOuTodo.ptParcial);
      end;
    end;
  end;
end;

procedure TFmVSOutCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsoutcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsoutcab', 'Codigo');
end;

procedure TFmVSOutCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSOutCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stUpd, QrVSOutIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSOutCabCliente.Value, QrVSOutCabSerieV.Value, QrVSOutCabNFV.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSOutCabMovimCod.Value, 0);
  //
  LocCod(QrVSOutCabCodigo.Value,QrVSOutCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.AlteraitemdeNF1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSNFeOutIts(stUpd, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
    QrVSOutCabEmpresa.Value, QrVSOutCabDtVenda.Value,
    siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSOutNfeCabMovimCod.Value, TEstqMovimID.emidVenda, [(**)], [eminSemNiv]);
end;

procedure TFmVSOutCab.AlteraSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo, ide_serie, ide_nNF: Integer;
begin
  Codigo    := QrVSOutNFeCabCodigo.Value;
  ide_serie := QrVSOutNFeCabide_serie.Value;
  ide_nNF   := QrVSOutNFeCabide_nNF.Value;
  //
  VS_CRC_PF.MostraFormVSNFeOutCab(stUpd, QrVSOutNfeCab, QrVSOutCabMovimCod.Value,
  QrVSOutCabCodigo.Value, Codigo, ide_serie, ide_nNF);
end;

procedure TFmVSOutCab.AlteraTributoAtual1Click(Sender: TObject);
var
  Operacao, Tributo, FatID, FatNum, FatParcela, Empresa, Controle: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib: Double;
begin
{$IfDef sAllVS}
  FatNum     := QrTribIncItsFatNum.Value;
  FatParcela := QrTribIncItsFatParcela.Value;
  Empresa    := QrTribIncItsEmpresa.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Controle   := QrTribIncItsControle.Value;
  Operacao   := QrTribIncItsOperacao.Value;
  Tributo    := QrTribIncItsTributo.Value;
  ValorFat   := QrTribIncItsValorFat.Value;
  BaseCalc   := QrTribIncItsBaseCalc.Value;
  Percent    := QrTribIncItsPercent.Value;
  VUsoCalc   := QrTribIncItsVUsoCalc.Value;
  ValrTrib   := QrTribIncItsValrTrib.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stUpd, QrTribIncIts, VAR_FATID_1009,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siNegativo);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.AtualizaNFeItens(MovimCod: Integer);
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  MovimCod, TEstqMovimID.emidVenda, [(**)], [eminSemNiv]);
end;

procedure TFmVSOutCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSOutCab, QrVSOutCabDtVenda.Value,
  BtCab, PMCab, [CabInclui1, CorrigeFornecedores1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSOutCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  PCDados.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  FAtualizando := False;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSOutCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSOutCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSOutCab.SadaxPedido1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiNfIt, Dmod.MyDB, [
  'SELECT DISTINCT vmi.ItemNFe, vmi.PedItsVda, vmi.GGXRcl, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsmovits vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GGXRcl ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidVenda)),
  'AND vmi.Codigo=' + Geral.FF0(QrVSOutCabCodigo.Value),
  'ORDER BY vmi.ItemNFe, vmi.PedItsVda, vmi.GGXRcl ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_019_01, [
  DModG.frxDsDono,
  frxDsVmiNfIt,
  frxDsVmiNfImeis
  ]);
  //
  MyObjects.frxMostra(frxWET_CURTI_019_01, 'Sa�da x Pedido');
  //
end;

procedure TFmVSOutCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSOutCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSOutCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSOutCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSOutCab.QrCliAfterOpen(DataSet: TDataSet);
begin
  EdCliente.ValueVariant := QrCliCodigo.Value;
  CBCliente.KeyValue     := QrCliCodigo.Value;
end;

procedure TFmVSOutCab.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrCliCNPJ_CPF.Value) + ':';
  QrCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrCliRua.Value, Trunc(QrCliNumero.Value), False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
end;

procedure TFmVSOutCab.QrEntregaCliCalcFields(DataSet: TDataSet);
begin
  QrEntregaCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliTe1.Value);
  QrEntregaCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaCliRua.Value, QrEntregaCliNumero.Value, False);
  //
/////////////////////////////////////////////////////////////////////////////////
  QrEntregaCliE_ALL.Value := QrEntregaCliL_Nome.Value;
  if QrEntregaCliL_CNPJ.Value <> '' then
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CNPJ: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CNPJ.Value)
  else
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CPF: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CPF.Value);
  if Trim(QrEntregaCliL_IE.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' I.E. ' + QrEntregaCliL_IE.Value;
  if Trim(QrEntregaCliTE1.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Fone: ' + QrEntregaCliTE1_TXT.Value;
  if Trim(QrEntregaCliEMAIL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Email: ' + QrEntregaCliEMAIL.Value;

  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaCliNOMELOGRAD.Value);
  if Trim(QrEntregaCliE_ALL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ';
  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + Uppercase(QrEntregaCliRua.Value);
  if Trim(QrEntregaCliRua.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNUMERO_TXT.Value;
  if Trim(QrEntregaCliCompl.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ' + Uppercase(QrEntregaCliCompl.Value);
  if Trim(QrEntregaCliBairro.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliBairro.Value);
  if QrEntregaCliCEP.Value > 0 then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  if Trim(QrEntregaCliNO_MUNICI.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliNO_MUNICI.Value);
  if Trim(QrEntregaCliNOMEUF.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNOMEUF.Value;
  if Trim(QrEntregaCliNO_PAIS.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + QrEntregaCliNO_PAIS.Value;
  //
end;

procedure TFmVSOutCab.QrEntregaEntiCalcFields(DataSet: TDataSet);
begin
  QrEntregaEntiTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiTe1.Value);
  QrEntregaEntiFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiFax.Value);
  QrEntregaEntiNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaEntiRua.Value, QrEntregaEntiNumero.Value, False);
  //
  QrEntregaEntiE_ALL.Value := QrEntregaEntiNOME_ENT.Value;
  case QrEntregaEntiTipo.Value of
    0: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CNPJ: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
    1: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CPF: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  end;
  if Trim(QrEntregaEntiIE.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' I.E. ' + QrEntregaEntiIE.Value;
  if Trim(QrEntregaEntiTE1.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Fone: ' + QrEntregaEntiTE1_TXT.Value;
  if Trim(QrEntregaEntiEMAIL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Email: ' + QrEntregaEntiEMAIL.Value;

  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaEntiNOMELOGRAD.Value);
  if Trim(QrEntregaEntiE_ALL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ';
  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + Uppercase(QrEntregaEntiRua.Value);
  if Trim(QrEntregaEntiRua.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNUMERO_TXT.Value;
  if Trim(QrEntregaEntiCompl.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ' + Uppercase(QrEntregaEntiCompl.Value);
  if Trim(QrEntregaEntiBairro.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiBairro.Value);
  if QrEntregaEntiCEP.Value > 0 then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  if Trim(QrEntregaEntiCidade.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiCidade.Value);
  if Trim(QrEntregaEntiNOMEUF.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNOMEUF.Value;
  if Trim(QrEntregaEntiPais.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + QrEntregaEntiPais.Value;
  //
end;

procedure TFmVSOutCab.QrPalletsCalcFields(DataSet: TDataSet);
begin
  QrPalletsSEQ.Value := QrPallets.RecNo;
end;

procedure TFmVSOutCab.QrVmiNfItAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiNfImeis, DMod.MyDB, [
  'SELECT vmi.Pallet, vmi.GraGruX, pvi.PrecoTipo, ',
  'ABS(SUM(vmi.Pecas)) Pecas, ',
  'ABS(SUM(vmi.PesoKg)) PesoKg, ABS(SUM(vmi.AreaM2)) AreaM2, ',
  'ABS(SUM(vmi.AreaP2)) AreaP2, mda.Sigla, mda.Simbolo, pvi.PrecoVal, ',
  'ABS(SUM(vmi.AreaM2)/SUM(vmi.Pecas)) MediaM2, ',
  'ABS(SUM(vmi.AreaP2)/SUM(vmi.Pecas)) MediaP2, ',
  'ABS( ',
  '  CASE PrecoTipo   ',
  '    WHEN 0 THEN 0.0000000000 ',
  '    WHEN 1 THEN vmi.Pecas ',
  '    WHEN 2 THEN vmi.PesoKg ',
  '    WHEN 3 THEN vmi.AreaM2 ',
  '    WHEN 4 THEN vmi.AreaP2 ',
  '    ELSE 0.0000000000  ',
  '  END * pvi.PrecoVal ',
  ') TotalItem ',
  'FROM vsmovits vmi ',
  'LEFT JOIN vspedits pvi ON pvi.Controle=vmi.PedItsVda ',
  'LEFT JOIN cambiomda  mda ON mda.Codigo=pvi.PrecoMoeda ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOutCabMovimCod.Value),
  'AND vmi.ItemNFe=' + Geral.FF0(QrVmiNfItItemNFe.Value),
  'AND vmi.PedItsVda=' + Geral.FF0(QrVmiNfItPedItsVda.Value),
  'AND vmi.GGXRcl=' + Geral.FF0(QrVmiNfItGGXRcl.Value),
  'GROUP BY vmi.Pallet ',
  '']);
end;

procedure TFmVSOutCab.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi, QrVSMOEnvAvuCodigo.VAlue, 0);
end;

procedure TFmVSOutCab.QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSOutCab.QrVSOutCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSOutCab.QrVSOutCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSOutIts(0);
{$IfDef sAllVS}
  ReopenVSOutNFeCab(0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSOutCabMovimCod.Value, 0);
  ReopenVSPedCab(QrVSOutCabPedido.Value);
end;

procedure TFmVSOutCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSOutCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSOutCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidVenda, [], stPsq, Codigo, Controle,
  False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSOutIts(Controle);
  end;
end;

procedure TFmVSOutCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutCab.frxWET_CURTI_019_00_A1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := QrVSOutCabNO_EMPRESA.Value
  else
  if VarName = 'VARF_CLIENTE' then
    Value := QrVSOutCabNO_CLIENTE.Value
  else
  if VarName = 'VARF_CODIGO' then
    Value := QrVSOutCabCodigo.Value
  else
  if VarName = 'VARF_NF' then
    Value := QrVSOutCabNFV.Value
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PEDIDO_CLI' then
    Value := QrVSPedCabPedidCli.Value
  else
  if VarName = 'VARF_PRECO_UNIT' then
  begin
    case QrVmiNfImeisPrecoTipo.Value of
      0: Value := 'Pre�o';
      1: Value := 'Pre�o p�';
      2: Value := 'Pre�o kg';
      3: Value := 'Pre�o m�';
      4: Value := 'Pre�o ft�';
      else Value := 'Pre�o';
    end;
  end
  else
end;

procedure TFmVSOutCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  //2017-10-05 => N�o tem sentido ter este c�digo
  //if not VS_CRC_PF.MovimIDCorreto(FThisMovimID) then Exit;
  //
  EdPedido.Enabled := True;
  QrVSPedCab.Close;
  MeEnderecoEntrega1.Lines.Clear;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSOutCab, [PnDados],
    [PnEdita], EdPedido, ImgTipo, 'vsoutcab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDtVenda.Date := Agora;
  EdDtVenda.ValueVariant := Agora;
  TPDtViagem.Date := Agora;
  TPDtEntrega.Date := Agora;
  //if EdEmpresa.ValueVariant > 0 then
    //TPDtVenda.SetFocus;
end;

procedure TFmVSOutCab.Corrigeartigodebaixa1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  GGXSrc, GGXBxa, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGruX  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(QrVSOutItsSrcNivel2.Value),
    ' ']);
    //
    GGXBxa := QrVSOutItsGraGruX.Value;
    GGXSrc := Qry.FieldByName('GraGruX').AsInteger;
    if GGXBxa <> GGXSrc then
    begin
      if GGXSrc <> 0 then
      begin
        if Geral.MB_Pergunta('O artigo de baixa reduzido = ' + Geral.FF0(GGXBxa)
        + ' est� diferente do reduzido de origem > ' + Geral.FF0(GGXSrc) + '!' +
        sLineBreak + 'Deseja corrig�-lo?') = ID_YES then
        begin
          Controle := QrVSOutItsControle.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_TAB_VMI, False, [
          'GraGruX'], ['Controle'], [GGXSrc], [Controle], True) then
          begin
            UnDmkDAC_PF.AbreQuery(QrVSOutIts, Dmod.MyDB);
            Geral.MB_Info('O artigo foi corrigido!');
          end;
        end;
     end else
        Geral.MB_Info(
        'O artigo origem est� com o reduzido zerado. N�o ser� poss�vel corrigir!');
    end else
      Geral.MB_Info('O artigo est� correto e n�o precisa de corre��o!');
  finally
    Qry.Free;
  end;
end;

procedure TFmVSOutCab.CorrigeFornecedores();
begin
  QrVSOutIts.First;
  while not QrVSOutIts.Eof do
  begin
    VS_CRC_PF.AtualizaFornecedorOut(QrVSOutItsControle.Value);
    //
    QrVSOutIts.Next;
  end;
  LocCod(QrVSOutCabCodigo.Value, QrVSOutCabCodigo.Value);
end;

procedure TFmVSOutCab.CorrigeFornecedores1Click(Sender: TObject);
begin
  CorrigeFornecedores();
end;

procedure TFmVSOutCab.CorrigeMovimIDapartirdestavenda1Click(Sender: TObject);
const
  SQL_Extra = '';
begin
  VS_CRC_PF.CorrigeMovimID(QrVSOutCabCodigo.Value, emidVenda, emidResiduoReclas,
  eminSemNiv, eminSemNiv, SQL_Extra, LaAviso1, LaAviso2, FAtualizando);
end;

procedure TFmVSOutCab.CorrigeNFeitensVS1Click(Sender: TObject);
begin
  AtualizaNFeItens(QrVSOutCabMovimCod.Value);
end;

procedure TFmVSOutCab.CorrigeNFeitensVSapartirdesteprocesso1Click(
  Sender: TObject);
begin
  VS_CRC_PF.CorrigeNFes(QrVSOutCabCodigo.Value, emidVenda, [(**)], [eminSemNiv],
  LaAviso1, LaAviso2, FAtualizando);
end;

procedure TFmVSOutCab.CorrigeNFes();
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsoutcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSOutCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o IME-C ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        AtualizaNFeItens(Qry.FieldByName('MovimCod').AsInteger);
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSOutCab.QrVSOutCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvAvu.Close;
  QrVSOutIts.Close;
  QrVSOutNFeCab.Close;
end;

procedure TFmVSOutCab.QrVSOutCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSOutCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSOutCab.QrVSOutItsAfterOpen(DataSet: TDataSet);
begin
  if QrVSOutIts.RecordCount > 0 then
  begin
    if QrVSOutItsPallet.Value > 0 then
      PCItens.ActivePageIndex := 0
    else
      PCItens.ActivePageIndex := 1;
  end;
end;

procedure TFmVSOutCab.QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
begin
{$IfDef sAllVS}
  ReopenVSOutNFeIts(0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSOutCab.QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
begin
  QrVSOutNFeIts.Close;
end;

procedure TFmVSOutCab.QrVSOutNFeItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTribIncIts(0);
end;

procedure TFmVSOutCab.QrVSOutNFeItsBeforeClose(DataSet: TDataSet);
begin
  QrTribIncIts.Close;
end;

procedure TFmVSOutCab.QrVSPedCabAfterOpen(DataSet: TDataSet);
begin
  QrCli.Close;
  QrCli.Params[00].AsInteger := QrVSPedCabCliente.Value;
  UMyMod.AbreQuery(QrCli, Dmod.MyDB, 'TFmFatPedCab.QrVSPedCabAfterOpen()');
  //
  MostraEnderecoDeEntrega2();
end;

procedure TFmVSOutCab.QrVSPedCabBeforeClose(DataSet: TDataSet);
begin
  QrCli.Close;
  QrEntregaEnti.Close;
end;

end.

