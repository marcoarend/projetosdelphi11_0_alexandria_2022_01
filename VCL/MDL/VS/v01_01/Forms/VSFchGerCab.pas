unit VSFchGerCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (*UnGOTOy,*) UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, AppListas, Variants, UnProjGroup_Consts,
  UnGrl_Consts;

type
  TFmVSFchGerCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrVSFchGerCab: TmySQLQuery;
    DsVSFchGerCab: TDataSource;
    QrVSFchGerIDs: TmySQLQuery;
    DsVSFchGerIDs: TDataSource;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSFchGerCabITENS: TLargeintField;
    QrVSFchGerCabRefFirst: TDateTimeField;
    QrVSFchGerCabRefLast: TDateTimeField;
    QrVSFchGerCabNO_SERIEFCH: TWideStringField;
    QrVSFchGerIDsITENS: TLargeintField;
    QrVSFchGerIDsRefFirst: TDateTimeField;
    QrVSFchGerIDsRefLast: TDateTimeField;
    QrVSFchGerIDsNO_MovimID: TWideStringField;
    Splitter1: TSplitter;
    DBGIDs: TDBGrid;
    DBGIMEIs: TDBGrid;
    QrIMEIs: TmySQLQuery;
    DsIMEIs: TDataSource;
    DBGrid1: TDBGrid;
    Splitter2: TSplitter;
    QrVSFchGerNivs: TmySQLQuery;
    DsVSFchGerNivs: TDataSource;
    QrVSFchGerNivsRefFirst: TDateTimeField;
    QrVSFchGerNivsRefLast: TDateTimeField;
    QrVSFchGerNivsNO_MovimID: TWideStringField;
    QrVSFchGerNivsPecas: TFloatField;
    QrVSFchGerNivsAreaM2: TFloatField;
    PMIMEIs: TPopupMenu;
    Irparajaneladomovimento1: TMenuItem;
    IrparajaneladedadosdoIMEI1: TMenuItem;
    N3: TMenuItem;
    ExcluiIMEIselecionado1: TMenuItem;
    AlteraIMEIorigemdoIMEISelecionado1: TMenuItem;
    teste1: TMenuItem;
    QrLotes: TmySQLQuery;
    BtClassesGeradas: TBitBtn;
    IrparajaneladoPallet1: TMenuItem;
    GBCabecalho: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label20: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    QrVSXxxCab: TmySQLQuery;
    QrVSXxxCabDtCompra: TDateTimeField;
    QrVSXxxCabDtViagem: TDateTimeField;
    QrVSXxxCabDtEntrada: TDateTimeField;
    QrVSXxxCabPecas: TFloatField;
    QrVSXxxCabPesoKg: TFloatField;
    QrVSXxxCabAreaM2: TFloatField;
    QrVSXxxCabAreaP2: TFloatField;
    QrVSXxxCabNO_EMPRESA: TWideStringField;
    QrVSXxxCabNO_FORNECE: TWideStringField;
    QrVSXxxCabNO_TRANSPORTA: TWideStringField;
    QrVSXxxCabValorT: TFloatField;
    QrVSXxxCabPlaca: TWideStringField;
    QrVSXxxCabNO_CLIENTEMO: TWideStringField;
    QrVSXxxCabNO_PROCEDNC: TWideStringField;
    QrVSXxxCabNO_MOTORISTA: TWideStringField;
    DsVSXxxCab: TDataSource;
    QrIMEIsCodigo: TLargeintField;
    QrIMEIsControle: TLargeintField;
    QrIMEIsMovimCod: TLargeintField;
    QrIMEIsMovimNiv: TLargeintField;
    QrIMEIsMovimTwn: TLargeintField;
    QrIMEIsEmpresa: TLargeintField;
    QrIMEIsTerceiro: TLargeintField;
    QrIMEIsCliVenda: TLargeintField;
    QrIMEIsMovimID: TLargeintField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TLargeintField;
    QrIMEIsGraGruX: TLargeintField;
    QrIMEIsPecas: TFloatField;
    QrIMEIsPesoKg: TFloatField;
    QrIMEIsAreaM2: TFloatField;
    QrIMEIsAreaP2: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TLargeintField;
    QrIMEIsSrcNivel1: TLargeintField;
    QrIMEIsSrcNivel2: TLargeintField;
    QrIMEIsSrcGGX: TLargeintField;
    QrIMEIsSdoVrtPeca: TFloatField;
    QrIMEIsSdoVrtPeso: TFloatField;
    QrIMEIsSdoVrtArM2: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsSerieFch: TLargeintField;
    QrIMEIsFicha: TLargeintField;
    QrIMEIsMisturou: TLargeintField;
    QrIMEIsFornecMO: TLargeintField;
    QrIMEIsCustoMOKg: TFloatField;
    QrIMEIsCustoMOM2: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TLargeintField;
    QrIMEIsDstNivel1: TLargeintField;
    QrIMEIsDstNivel2: TLargeintField;
    QrIMEIsDstGGX: TLargeintField;
    QrIMEIsQtdGerPeca: TFloatField;
    QrIMEIsQtdGerPeso: TFloatField;
    QrIMEIsQtdGerArM2: TFloatField;
    QrIMEIsQtdGerArP2: TFloatField;
    QrIMEIsQtdAntPeca: TFloatField;
    QrIMEIsQtdAntPeso: TFloatField;
    QrIMEIsQtdAntArM2: TFloatField;
    QrIMEIsQtdAntArP2: TFloatField;
    QrIMEIsNotaMPAG: TFloatField;
    QrIMEIsStqCenLoc: TLargeintField;
    QrIMEIsReqMovEstq: TLargeintField;
    QrIMEIsGraGru1: TLargeintField;
    QrIMEIsNO_EstqMovimID: TWideStringField;
    QrIMEIsNO_MovimID: TWideStringField;
    QrIMEIsNO_MovimNiv: TWideStringField;
    QrIMEIsNO_PALLET: TWideStringField;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsNO_FORNECE: TWideStringField;
    QrIMEIsNO_SerieFch: TWideStringField;
    QrIMEIsNO_TTW: TWideStringField;
    QrIMEIsID_TTW: TLargeintField;
    QrIMEIsNO_LOC_CEN: TWideStringField;
    QrLotesSerieFch: TLargeintField;
    QrLotesFicha: TLargeintField;
    QrLotesSerieEFicha: TWideStringField;
    QrLotesNO_TTW: TWideStringField;
    QrLotesID_TTW: TLargeintField;
    DsLotes: TDataSource;
    Panel7: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label21: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit29: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    DBGrid2: TDBGrid;
    QrVSFchGerCabSerieFch: TLargeintField;
    QrVSFchGerCabFicha: TLargeintField;
    QrVSFchGerCabMovimID: TLargeintField;
    QrVSFchGerCabCodigo01: TLargeintField;
    QrVSFchGerCabCodigo13: TLargeintField;
    QrVSFchGerCabCodigo16: TLargeintField;
    QrVSFchGerCabNO_TTW: TWideStringField;
    QrVSFchGerCabID_TTW: TLargeintField;
    QrVSFchGerCabMovimCod: TLargeintField;
    QrVSFchGerCabCodID: TLargeintField;
    QrVSXxxCabCodigo: TLargeintField;
    QrVSXxxCabMovimCod: TLargeintField;
    QrVSXxxCabEmpresa: TLargeintField;
    QrVSXxxCabFornecedor: TLargeintField;
    QrVSXxxCabTransporta: TLargeintField;
    QrVSXxxCabClienteMO: TLargeintField;
    QrVSXxxCabProcednc: TLargeintField;
    QrVSXxxCabMotorista: TLargeintField;
    QrVSFchGerIDsMovimID: TLargeintField;
    QrVSFchGerIDsSerieFch: TLargeintField;
    QrVSFchGerIDsFicha: TLargeintField;
    QrVSFchGerNivsMovimID: TLargeintField;
    QrVSFchGerNivsITENS: TLargeintField;
    QrVSFchGerNivsSerieFch: TLargeintField;
    QrVSFchGerNivsFicha: TLargeintField;
    QrVSFchGerNivsMovimNiv: TLargeintField;
    PMImprime: TPopupMenu;
    PorArtigo1: TMenuItem;
    PorArtigoeMartelo1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSFchGerCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSFchGerCabAfterScroll(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSFchGerCabBeforeClose(DataSet: TDataSet);
    procedure QrVSFchGerIDsAfterScroll(DataSet: TDataSet);
    procedure QrVSFchGerIDsBeforeClose(DataSet: TDataSet);
    procedure QrVSFchGerNivsAfterScroll(DataSet: TDataSet);
    procedure QrVSFchGerNivsBeforeClose(DataSet: TDataSet);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure IrparajaneladedadosdoIMEI1Click(Sender: TObject);
    procedure ExcluiIMEIselecionado1Click(Sender: TObject);
    procedure AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
    procedure teste1Click(Sender: TObject);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure PorArtigo1Click(Sender: TObject);
    procedure PorArtigoeMartelo1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenVSFchGerCab(SerieFch, Ficha: Integer);
    procedure ReopenVSFchGerIDs(SerieFch, Ficha, MovimID: Integer);
    procedure ReopenVSFchGerNivs(MovimNiv: Integer);
    procedure ReopenIMEIs(Controle: Integer);
    procedure ReopenVSInnCab();

  end;

var
  FmVSFchGerCab: TFmVSFchGerCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnVS_PF, VSMovItsAlt;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSFchGerCab.LocCod(Atual, Codigo: Integer);
begin
  //DefParams;
  //GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSFchGerCab.PMCabPopup(Sender: TObject);
begin
(*
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCadastro_Com_Itens_CAB);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrCadastro_Com_Itens_CAB, QrCadastro_Com_Itens_ITS);
*)
end;

procedure TFmVSFchGerCab.PMItsPopup(Sender: TObject);
begin
(*
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCadastro_Com_Itens_CAB);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCadastro_Com_Itens_ITS);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCadastro_Com_Itens_ITS);
*)
end;

procedure TFmVSFchGerCab.PorArtigo1Click(Sender: TObject);
const
  MostraJanela = False;
  PorMartelo = False;
begin
  VS_PF.ImprimeClassFichaRMP_Uni_New(QrVSFchGerCabSerieFch.Value,
    QrVSFchGerCabFicha.Value, QrVSFchGerCabNO_SERIEFCH.Value, MostraJanela,
    PorMartelo);
end;

procedure TFmVSFchGerCab.PorArtigoeMartelo1Click(Sender: TObject);
const
  PorMartelo = True;
  MostraJanela = False;
begin
  VS_PF.ImprimeClassFichaRMP_Uni_New(QrVSFchGerCabSerieFch.Value,
    QrVSFchGerCabFicha.Value, QrVSFchGerCabNO_SERIEFCH.Value, MostraJanela,
    PorMartelo);
end;

procedure TFmVSFchGerCab.Va(Para: TVaiPara);
begin
(*
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCadastro_Com_Itens_CABCodigo.Value, LaRegistro.Caption[2]);
*)
  Geral.MB_Info('N�o implementado! Solicite � DERMATEK!');
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSFchGerCab.DefParams;
begin
(*
  VAR_GOTOTABELA := 'cadastro_com_itens_cab';
  VAR_GOTOMYSQLTABLE := QrCadastro_Com_Itens_CAB;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cadastro_com_itens_cab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
*)
end;

procedure TFmVSFchGerCab.ExcluiIMEIselecionado1Click(Sender: TObject);
begin
(*
  if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(QrIMEIsControle.Value,
  Integer(TEstqMotivDel.emtdWetCurti013), Dmod.QrUpd, Dmod.MyDB) then
  begin
    LocCod(QrVSPalletCodigo.Value, QrVSPalletCodigo.Value);
  end;
*)
end;

procedure TFmVSFchGerCab.CriaOForm;
begin
  DefineONomeDoForm;
  //DefParams;
  //Va(vpLast);
end;

procedure TFmVSFchGerCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSFchGerCab.IrparajaneladedadosdoIMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(QrIMEIsControle.Value);
end;

procedure TFmVSFchGerCab.Irparajaneladomovimento1Click(Sender: TObject);
begin
  if QrVSFchGerNivs.RecordCount > 0 then
    VS_PF.MostraFormVS_Do_IMEI(QrIMEISControle.Value);
end;

procedure TFmVSFchGerCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrIMEIsPallet.Value);
end;

procedure TFmVSFchGerCab.ReopenIMEIs(Controle: Integer);
(*
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIS, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas,',
  'vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT,',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2,',
  ATT_MovimID,
  ATT_MovimNiv,
  'vmi.MovimID, vmi.MovimNiv, vmi.Pallet, ',
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, vmi.DataHora, ',
  'vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
  'vmi.DstMovID, vmi.DstNivel1, vmi.DstNivel2 ',
  'FROM v s m o v i t s vmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerNivsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerNivsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerNivsMovimID.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(QrVSFchGerNivsMovimNiv.Value),
  '']);
  //Geral.MB_SQL(self, QrIMEIS);
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
*)
const
  TemIMEIMrt = 1;
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerNivsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerNivsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerNivsMovimID.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(QrVSFchGerNivsMovimNiv.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
end;

procedure TFmVSFchGerCab.ReopenVSFchGerCab(SerieFch, Ficha: Integer);
begin
(*
  Juntar Ativo morto!
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerCab, Dmod.MyDB, [
  'SELECT vmi.Codigo, vmi.SerieFch, vmi.Ficha,   ',
  '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  ',
  'vsf.Nome NO_SERIEFCH   ',
  'FROM v s m o v i t s vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerCab, Dmod.MyDB, [
  'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwB),
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
  'CAST(vmi.Codigo AS SIGNED) CodID, ',
  'CAST(IF(vmi.MovimID=1, vmi.Codigo, 0) AS SIGNED)  Codigo01, ',
  'CAST(IF(vmi.MovimID=13, vmi.Codigo, 0) AS SIGNED) Codigo13, ',
  'CAST(IF(vmi.MovimID=16, vmi.Codigo, 0) AS SIGNED) Codigo16, ',
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(COUNT(vmi.Codigo) AS SIGNED) ITENS, ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast, ',
  'CAST(vsf.Nome AS CHAR) NO_SERIEFCH ',
  'FROM ' + CO_TAB_VMB + ' vmi ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimID IN (' + CO_ALL_CODS_INN_ENTRA_VS + ') ',
  'AND vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha ',
  ' ',
  'UNION ',
  ' ',
  'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwA),
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
  'CAST(vmi.Codigo AS SIGNED) CodID, ',
  'CAST(IF(vmi.MovimID=1, vmi.Codigo, 0) AS SIGNED)  Codigo01, ',
  'CAST(IF(vmi.MovimID=13, vmi.Codigo, 0) AS SIGNED) Codigo13, ',
  'CAST(IF(vmi.MovimID=16, vmi.Codigo, 0) AS SIGNED) Codigo16, ',
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(COUNT(vmi.Codigo) AS SIGNED) ITENS, ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast, ',
  'CAST(vsf.Nome AS CHAR) NO_SERIEFCH ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimID IN (' + CO_ALL_CODS_INN_ENTRA_VS + ') ',
  'AND vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha ',
  ''])
end;

procedure TFmVSFchGerCab.ReopenVSFchGerIDs(SerieFch, Ficha, MovimID: Integer);
var
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerIDs, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,   ',
  '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  ',
  ATT_MovimID,
  'vsf.Nome NO_SERIEFCH ',
  'FROM v s m o v i t s vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID  ',
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerIDs, Dmod.MyDB, [
  'SELECT ', //+ VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwB),
  ATT_MovimID,
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(COUNT(vmi.Codigo) AS UNSIGNED) ITENS,   ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast  ',
  'FROM ' + CO_TAB_VMB + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID  ',
  '',
  'UNION',
  '',
  'SELECT ', // + VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwA),
  ATT_MovimID,
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(COUNT(vmi.Codigo) AS UNSIGNED) ITENS,   ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID  ',
  '',
  'ORDER BY RefFirst, MovimID  ',
  '']);
  //
  QrVSFchGerIDs.Locate('MovimID', MovimID, []);
end;


procedure TFmVSFchGerCab.ReopenVSFchGerNivs(MovimNiv: Integer);
var
  ATT_MovimNiv: String;
begin
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerNivs, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.MovimNiv, vmi.SerieFch, vmi.Ficha, ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, ',
  ATT_MovimNiv,
  'SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2 ',
  'FROM v s m o v i t s vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerIDsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerIDsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerIDsMovimID.Value),
  'GROUP BY vmi.MovimNiv  ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerNivs, Dmod.MyDB, [
  'SELECT ', //+ VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwB),
  ATT_MovimNiv,
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
  'CAST(COUNT(vmi.Codigo) AS UNSIGNED) ITENS,   ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast,  ',
  'SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2 ',
  'FROM ' + CO_TAB_VMB + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerIDsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerIDsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerIDsMovimID.Value),
  'GROUP BY vmi.MovimNiv  ',
  '',
  'UNION',
  '',
  'SELECT ', // + VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwA),
  ATT_MovimNiv,
  'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
  'CAST(vmi.Ficha AS SIGNED) Ficha, ',
  'CAST(vmi.MovimID AS SIGNED) MovimID, ',
  'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
  'CAST(COUNT(vmi.Codigo) AS UNSIGNED) ITENS,   ',
  'CAST(MIN(vmi.DataHora) AS DATETIME) RefFirst, ',
  'CAST(MAX(vmi.DataHora) AS DATETIME) RefLast,  ',
  'SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerIDsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerIDsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerIDsMovimID.Value),
  'GROUP BY vmi.MovimNiv  ',
  '',
  'ORDER BY RefFirst, MovimID  ',
  '']);
  //
  QrVSFchGerNivs.Locate('MovimNiv', MovimNiv, []);
end;

procedure TFmVSFchGerCab.ReopenVSInnCab();
var
  Tabela: String;
begin
  //ver todos MovimID 1, 13 e 16 positivos!
  if (QrVSFchGerCabCodigo01.Value <> 0)
  or (QrVSFchGerCabCodigo16.Value <> 0) then
  begin
    if QrVSFchGerCabCodigo01.Value <> 0 then
    begin
      Tabela := 'vsinncab';
      GBCabecalho.Caption := 'Dados do cabe�alho da entrada In Natura: ';
    end else
    if QrVSFchGerCabCodigo16.Value <> 0 then
    begin
      Tabela := 'vsplccab';
      GBCabecalho.Caption := 'Dados do cabe�alho da entrada de Wet Blue: ';
    end else
    begin
      Tabela := CO_VSXxxTab;
      GBCabecalho.Caption := 'Dados do cabe�alho da compra de ???????: ';
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSXxxCab, Dmod.MyDB, [
    'SELECT ',
    'CAST(cab.Codigo AS SIGNED) Codigo, ',
    'CAST(cab.Empresa AS SIGNED) Empresa, ',
    'CAST(cab.DtCompra AS DATETIME) DtCompra, ',
    'CAST(cab.DtViagem AS DATETIME) DtViagem, ',
    'CAST(cab.DtEntrada AS DATETIME) DtEntrada,',
    'CAST(cab.MovimCod AS SIGNED) MovimCod, ',
    'CAST(cab.ClienteMO AS SIGNED) ClienteMO, ',
    'CAST(cab.Fornecedor AS SIGNED) Fornecedor, ',
    'CAST(cab.Procednc AS SIGNED) Procednc, ',
    'CAST(cab.Transporta AS SIGNED) Transporta, ',
    'CAST(cab.Motorista AS SIGNED) Motorista, ',
    'CAST(cab.Placa AS CHAR) Placa, ',
    'CAST(cab.AreaM2 AS DECIMAL(15,2)) AreaM2, ',
    'CAST(cab.AreaP2 AS DECIMAL(15,2)) AreaP2,',
    'CAST(cab.PesoKg AS DECIMAL(15,3)) PesoKg, ',
    'CAST(cab.Pecas AS DECIMAL(15,3)) Pecas, ',
    'CAST(cab.ValorT AS DECIMAL(15,2)) ValorT, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, ',
    'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, ',
    'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA ',
    'FROM ' + Tabela + ' cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa ',
    'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornecedor ',
    'LEFT JOIN entidades trn ON trn.Codigo=cab.Transporta ',
    'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteMO',
    'LEFT JOIN entidades prc ON prc.Codigo=cab.Procednc ',
    'LEFT JOIN entidades mot ON mot.Codigo=cab.Motorista  ',
    'WHERE cab.Codigo=' + Geral.FF0(QrVSFchGerCabCodigo01.Value),
    '']);
  end else
  if QrVSFchGerCabCodigo13.Value <> 0 then
  begin
    GBCabecalho.Caption := 'Dados do cabe�alho do Ajuste em Invent�rio: ';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSXxxCab, Dmod.MyDB, [
    'SELECT ',
    'CAST(cab.Codigo AS SIGNED) Codigo, ',
    'CAST(cab.Empresa AS SIGNED) Empresa, ',
    'CAST(cab.DataHora AS DATETIME) DtCompra, ',
    'CAST(cab.DataHora AS DATETIME) DtViagem, ',
    'CAST(cab.DataHora AS DATETIME) DtEntrada,',
    'CAST(cab.MovimCod AS SIGNED) MovimCod, ',
    'CAST(0 AS SIGNED) ClienteMO, ',
    'CAST(0 AS SIGNED) Fornecedor, ',
    'CAST(0 AS SIGNED) Procednc, ',
    'CAST(0 AS SIGNED) Transporta, ',
    'CAST(0 AS SIGNED) Motorista, ',
    'CAST("" AS CHAR) Placa, ',
    'CAST(cab.AreaM2 AS DECIMAL(15,2)) AreaM2, ',
    'CAST(cab.AreaP2 AS DECIMAL(15,2)) AreaP2,',
    'CAST(cab.PesoKg AS DECIMAL(15,3)) PesoKg, ',
    'CAST(cab.Pecas AS DECIMAL(15,3)) Pecas, ',
    'CAST(cab.ValorT AS DECIMAL(15,2)) ValorT, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
    '/*IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)*/  "" NO_FORNECE, ',
    '/*IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome)*/  "" NO_TRANSPORTA, ',
    '/*IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)*/  "" NO_CLIENTEMO, ',
    '/*IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome)*/  ""  NO_PROCEDNC, ',
    '/*IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome)*/  ""  NO_MOTORISTA ',
    'FROM vsajscab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa ',
    '/*LEFT JOIN entidades frn ON frn.Codigo=cab.Fornecedor ',
    'LEFT JOIN entidades trn ON trn.Codigo=cab.Transporta ',
    'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteMO',
    'LEFT JOIN entidades prc ON prc.Codigo=cab.Procednc ',
    'LEFT JOIN entidades mot ON mot.Codigo=cab.Motorista  */',
    'WHERE cab.Codigo=' + Geral.FF0(QrVSFchGerCabCodigo13.Value),
    '']);
  end else
    QrVSXxxCab.Close;
end;

procedure TFmVSFchGerCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSFchGerCab.SpeedButton1Click(Sender: TObject);
begin
  //Va(vpFirst);
  QrLotes.First;
end;

procedure TFmVSFchGerCab.SpeedButton2Click(Sender: TObject);
begin
  //Va(vpPrior);
  QrLotes.Prior;
end;

procedure TFmVSFchGerCab.SpeedButton3Click(Sender: TObject);
begin
  //Va(vpNext);
  QrLotes.Next;
end;

procedure TFmVSFchGerCab.SpeedButton4Click(Sender: TObject);
begin
  //Va(vpLast);
  QrLotes.Last;
end;

procedure TFmVSFchGerCab.teste1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
begin
  VS_PF.MostraFormVSMovItsAlt(QrIMEIsControle.Value, AtualizaSaldoModoGenerico,
    [eegbDadosArtigo]);
end;

procedure TFmVSFchGerCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSFchGerCab.BtSaidaClick(Sender: TObject);
begin
  //VAR_CADASTRO := QrCadastro_Com_Itens_CABCodigo.Value;
  Close;
end;

procedure TFmVSFchGerCab.BtDesisteClick(Sender: TObject);
(*
var
  Codigo : Integer;
*)
begin
(*
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cadastro_com_itens_cab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cadastro_com_itens_cab', 'Codigo');
*)
end;

procedure TFmVSFchGerCab.BtItsClick(Sender: TObject);
begin
//  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSFchGerCab.AlteraIMEIorigemdoIMEISelecionado1Click(
  Sender: TObject);
(*
var
  ResVar: Variant;
  Controle, SrcNivel2: Integer;
*)
begin
(*
  if VAR_USUARIO <> -1 then
  begin
    Geral.MB_Aviso('Usu�rio deve ser Master!');
    Exit;
  end;
  SrcNivel2 := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    SrcNivel2 := Geral.IMV(ResVar);
    Controle  := QrIMEIsControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, v s m o v i t s , False, [
    'SrcNivel2'], ['Controle'], [
    SrcNivel2], [Controle], True) then
    begin
      ReopenIMEIs(Controle);
    end;
  end;
*)
end;

procedure TFmVSFchGerCab.BtCabClick(Sender: TObject);
begin
//  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSFchGerCab.BtClassesGeradasClick(Sender: TObject);
const
  MostraJanela = True;
  PorMartelo   = False;
begin
  VS_PF.ImprimeClassFichaRMP_Uni_New(QrVSFchGerCabSerieFch.Value,
    QrVSFchGerCabFicha.Value, QrVSFchGerCabNO_SERIEFCH.Value,
    MostraJanela, PorMartelo);
end;

procedure TFmVSFchGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DBGIMEIs.Align    := alClient;
  CriaOForm;
  FSeq := 0;
  //UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT DISTINCT ',
  VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwC),
  'CAST(SerieFch AS SIGNED) SerieFch, ',
  'CAST(Ficha AS SIGNED) Ficha,   ',
  'CONCAT(SerieFch, ".", Ficha) SerieEFicha   ',
  'FROM ' + CO_SEL_TAB_VMI + '  ',
  'WHERE Ficha <> 0  ',
  'AND (  ',
  '  CONCAT(SerieFch, ".", Ficha) IN   ',
  '  (  ',
  '    SELECT   ',
  '      CONCAT(SerieFch, ".", Ficha)  ',
  '      FROM ' + CO_TAB_VMB + '  ',
  '      WHERE Ficha <> 0  ',
  '  )  ',
  ') ',
  '',
  'UNION',
  '',
  'SELECT DISTINCT ',
  VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwB),
  'CAST(SerieFch AS SIGNED) SerieFch, ',
  'CAST(Ficha AS SIGNED) Ficha,   ',
  'CONCAT(SerieFch, ".", Ficha) SerieEFicha   ',
  'FROM ' + CO_TAB_VMB + '  ',
  'WHERE Ficha <> 0  ',
  'AND NOT (  ',
  '  CONCAT(SerieFch, ".", Ficha) IN   ',
  '  (  ',
  '    SELECT   ',
  '      CONCAT(SerieFch, ".", Ficha)  ',
  '      FROM ' + CO_SEL_TAB_VMI + '  ',
  '      WHERE Ficha <> 0  ',
  '  )  ',
  ')  ',
  '  ',
  'UNION  ',
  '  ',
  'SELECT DISTINCT ',
  VS_PF.TabMovVS_Fld_IMEI(TTabToWork.ttwA),
  'CAST(SerieFch AS SIGNED) SerieFch, ',
  'CAST(Ficha AS SIGNED) Ficha,   ',
  'CONCAT(SerieFch, ".", Ficha) SerieEFicha   ',
  'FROM ' + CO_SEL_TAB_VMI + '  ',
  'WHERE Ficha <> 0  ',
  'AND NOT (  ',
  '  CONCAT(SerieFch, ".", Ficha) IN   ',
  '  (  ',
  '    SELECT   ',
  '      CONCAT(SerieFch, ".", Ficha)  ',
  '      FROM ' + CO_TAB_VMB + '  ',
  '      WHERE Ficha <> 0  ',
  '  )  ',
  ')  ',
  '  ',
  'ORDER BY Ficha, SerieFch  ',
  '  ',
  '  ']);
  //
  //Geral.MB_SQL(Self, QrLotes);
  QrLotes.Last;
end;

procedure TFmVSFchGerCab.SbNumeroClick(Sender: TObject);
begin
  SbQueryClick(Self);
end;

procedure TFmVSFchGerCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSFchGerCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSFchGerCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSFchGerCab.QrLotesAfterScroll(DataSet: TDataSet);
begin
  ReopenVSFchGerCab(QrLotesSerieFch.Value, QrLotesFicha.Value);
end;

procedure TFmVSFchGerCab.QrVSFchGerCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSFchGerCab.QrVSFchGerCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSFchGerIDs(QrVSFchGerCabSerieFch.Value, QrVSFchGerCabFicha.Value, 0);
  ReopenVSInnCab();
end;

procedure TFmVSFchGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    //CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    //if QrCadastro_Com_Itens_CABCodigo.Value <> FCabIni then
      //Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSFchGerCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  SerieFch, Ficha: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq2(SerieFch, Ficha);
  //
  if Localizou then
  begin
    QrLotes.Locate('SerieFch;Ficha', VarArrayOf([SerieFch, Ficha]), []);
    //ReopenVSFchGerCab(SerieFch, Ficha);
  end;
end;

procedure TFmVSFchGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSFchGerCab.QrVSFchGerCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSFchGerIDs.Close;
  QrVSXxxCab.Close;
end;

procedure TFmVSFchGerCab.QrVSFchGerIDsAfterScroll(DataSet: TDataSet);
begin
  ReopenVSFchGerNivs(0);
end;

procedure TFmVSFchGerCab.QrVSFchGerIDsBeforeClose(DataSet: TDataSet);
begin
  QrVSFchGerNivs.Close;
end;

procedure TFmVSFchGerCab.QrVSFchGerNivsAfterScroll(DataSet: TDataSet);
begin
  ReopenIMEIs(0);
end;

procedure TFmVSFchGerCab.QrVSFchGerNivsBeforeClose(DataSet: TDataSet);
begin
  QrIMEIs.Close;
end;

end.

