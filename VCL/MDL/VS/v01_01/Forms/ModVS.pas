unit ModVS;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables, Vcl.StdCtrls,
  Vcl.ComCtrls, AppListas, dmkGeral, UnDmkEnums, UnInternalConsts,
  UnProjGroup_Consts, UnProjGroup_Vars, UnAppEnums, UnGrl_Consts;

type
  TDmModVS = class(TDataModule)
    Qr14GraGruX: TmySQLQuery;
    IntegerField5: TIntegerField;
    IntegerField11: TIntegerField;
    StringField5: TWideStringField;
    StringField11: TWideStringField;
    IntegerField12: TIntegerField;
    StringField12: TWideStringField;
    Ds14GraGruX: TDataSource;
    Qr14Fornecedor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    Ds14Fornecedor: TDataSource;
    Qr14VSSerFch: TmySQLQuery;
    Qr14VSSerFchCodigo: TIntegerField;
    Qr14VSSerFchNome: TWideStringField;
    Ds14VSSerFch: TDataSource;
    Qr13Sintetico: TmySQLQuery;
    Qr13SinteticoGraGruY: TIntegerField;
    Qr13SinteticoInteiros: TFloatField;
    Qr13SinteticoSerieFch: TIntegerField;
    Qr13SinteticoFicha: TIntegerField;
    Qr13SinteticoIMEI_Src: TIntegerField;
    Qr13SinteticoPallet: TIntegerField;
    Qr13SinteticoAtivo: TSmallintField;
    Qr13SinteticoNO_SERFCH: TWideStringField;
    Qr13SinteticoNO_GGY: TWideStringField;
    Qr13DtHr: TmySQLQuery;
    Qr13DtHrDataHora: TDateTimeField;
    Qr13SinteticoOperacao: TIntegerField;
    Qr16Fatores: TmySQLQuery;
    Ds16Fatores: TDataSource;
    Qr16FatoresDtHrFimCla: TDateTimeField;
    Qr16FatoresCodigoClaRcl: TIntegerField;
    Qr16FatoresOC: TIntegerField;
    Qr16FatoresIDGeracao: TIntegerField;
    Qr16FatoresPallet_Dst: TIntegerField;
    Qr16FatoresFatorIntSrc: TFloatField;
    Qr16FatoresFatorIntDst: TFloatField;
    Qr16FatoresClaRcl: TFloatField;
    Qr16FatoresPallet_Src: TFloatField;
    Qr13VSSerFch: TmySQLQuery;
    Qr13VSSerFchCodigo: TIntegerField;
    Qr13VSSerFchNome: TWideStringField;
    Ds13VSSerFch: TDataSource;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrUniFrn: TmySQLQuery;
    QrMulFrn: TmySQLQuery;
    QrUniFrnTerceiro: TIntegerField;
    QrUniFrnVSMulFrnCab: TIntegerField;
    QrUniFrnPecas: TFloatField;
    QrLocMFI: TmySQLQuery;
    QrLocMFIControle: TIntegerField;
    QrLocMFC: TmySQLQuery;
    QrLocMFCCodigo: TIntegerField;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnPecas: TFloatField;
    QrMulFrnSiglaVS: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnPecas: TFloatField;
    QrMFDst: TmySQLQuery;
    QrMFSrc: TmySQLQuery;
    QrMFSrcControle: TIntegerField;
    QrMFOrfaos: TmySQLQuery;
    QrMFOrfaosControle: TSmallintField;
    QrMFOrfaosTerceiro: TIntegerField;
    QrMFOrfaosVSMulFrnCab: TIntegerField;
    QrIMEI: TmySQLQuery;
    QrIMEICodigo: TIntegerField;
    QrIMEIControle: TIntegerField;
    QrIMEIMovimCod: TIntegerField;
    QrIMEIMovimNiv: TIntegerField;
    QrIMEIMovimTwn: TIntegerField;
    QrIMEIEmpresa: TIntegerField;
    QrIMEITerceiro: TIntegerField;
    QrIMEICliVenda: TIntegerField;
    QrIMEIMovimID: TIntegerField;
    QrIMEILnkIDXtr: TIntegerField;
    QrIMEILnkNivXtr1: TIntegerField;
    QrIMEILnkNivXtr2: TIntegerField;
    QrIMEIDataHora: TDateTimeField;
    QrIMEIPallet: TIntegerField;
    QrIMEIGraGruX: TIntegerField;
    QrIMEIPecas: TFloatField;
    QrIMEIPesoKg: TFloatField;
    QrIMEIAreaM2: TFloatField;
    QrIMEIAreaP2: TFloatField;
    QrIMEIValorT: TFloatField;
    QrIMEISrcMovID: TIntegerField;
    QrIMEISrcNivel1: TIntegerField;
    QrIMEISrcNivel2: TIntegerField;
    QrIMEISrcGGX: TIntegerField;
    QrIMEISdoVrtPeca: TFloatField;
    QrIMEISdoVrtPeso: TFloatField;
    QrIMEISdoVrtArM2: TFloatField;
    QrIMEIObserv: TWideStringField;
    QrIMEISerieFch: TIntegerField;
    QrIMEIFicha: TIntegerField;
    QrIMEIMisturou: TSmallintField;
    QrIMEIFornecMO: TIntegerField;
    QrIMEICustoMOKg: TFloatField;
    QrIMEICustoMOTot: TFloatField;
    QrIMEIValorMP: TFloatField;
    QrIMEIDstMovID: TIntegerField;
    QrIMEIDstNivel1: TIntegerField;
    QrIMEIDstNivel2: TIntegerField;
    QrIMEIDstGGX: TIntegerField;
    QrIMEIQtdGerPeca: TFloatField;
    QrIMEIQtdGerPeso: TFloatField;
    QrIMEIQtdGerArM2: TFloatField;
    QrIMEIQtdGerArP2: TFloatField;
    QrIMEIQtdAntPeca: TFloatField;
    QrIMEIQtdAntPeso: TFloatField;
    QrIMEIQtdAntArM2: TFloatField;
    QrIMEIQtdAntArP2: TFloatField;
    QrIMEIAptoUso: TSmallintField;
    QrIMEINotaMPAG: TFloatField;
    QrIMEIMarca: TWideStringField;
    QrIMEITpCalcAuto: TIntegerField;
    QrIMEIZerado: TSmallintField;
    QrIMEIEmFluxo: TSmallintField;
    QrIMEINotFluxo: TIntegerField;
    QrIMEIFatNotaVNC: TFloatField;
    QrIMEIFatNotaVRC: TFloatField;
    QrIMEIPedItsLib: TIntegerField;
    QrIMEIPedItsFin: TIntegerField;
    QrIMEIPedItsVda: TIntegerField;
    QrIMEILk: TIntegerField;
    QrIMEIDataCad: TDateField;
    QrIMEIDataAlt: TDateField;
    QrIMEIUserCad: TIntegerField;
    QrIMEIUserAlt: TIntegerField;
    QrIMEIAlterWeb: TSmallintField;
    QrIMEIAtivo: TSmallintField;
    QrIMEICustoMOM2: TFloatField;
    QrIMEIReqMovEstq: TIntegerField;
    QrIMEIStqCenLoc: TIntegerField;
    QrIMEIItemNFe: TIntegerField;
    QrIMEIVSMorCab: TIntegerField;
    QrIMEIVSMulFrnCab: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    TbCouNiv1: TmySQLTable;
    DsCouNiv1: TDataSource;
    TbCouNiv1Codigo: TIntegerField;
    TbCouNiv1Nome: TWideStringField;
    TbCouNiv1FatorInt: TFloatField;
    QrCorda: TMySQLQuery;
    QrVSProQui: TmySQLQuery;
    QrSCus2: TmySQLQuery;
    QrSCus2MPIn: TIntegerField;
    QrSCus2Custo: TFloatField;
    QrSCus2Pecas: TFloatField;
    QrSCus2Peso: TFloatField;
    QrSCus2MinDta: TDateTimeField;
    QrSCus2MaxDta: TDateTimeField;
    QrSCus2Fuloes: TLargeintField;
    QrSCus2Setor: TIntegerField;
    QrVMI: TmySQLQuery;
    QrVMICodigo: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    QrVMIMovimTwn: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMITerceiro: TIntegerField;
    QrVMICliVenda: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMILnkNivXtr1: TIntegerField;
    QrVMILnkNivXtr2: TIntegerField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMISrcMovID: TIntegerField;
    QrVMISrcNivel1: TIntegerField;
    QrVMISrcNivel2: TIntegerField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMIFicha: TIntegerField;
    QrVMIMisturou: TSmallintField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMILk: TIntegerField;
    QrVMIDataCad: TDateField;
    QrVMIDataAlt: TDateField;
    QrVMIUserCad: TIntegerField;
    QrVMIUserAlt: TIntegerField;
    QrVMIAlterWeb: TSmallintField;
    QrVMIAtivo: TSmallintField;
    QrVMISrcGGX: TIntegerField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMISerieFch: TIntegerField;
    QrVMIFornecMO: TIntegerField;
    QrVMIValorMP: TFloatField;
    QrVMIDstMovID: TIntegerField;
    QrVMIDstNivel1: TIntegerField;
    QrVMIDstNivel2: TIntegerField;
    QrVMIDstGGX: TIntegerField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMIAptoUso: TSmallintField;
    QrVMINotaMPAG: TFloatField;
    QrVMIMarca: TWideStringField;
    QrVMITpCalcAuto: TIntegerField;
    QrVMIZerado: TSmallintField;
    QrVMIEmFluxo: TSmallintField;
    QrVMILnkIDXtr: TIntegerField;
    QrVMICustoMOM2: TFloatField;
    QrVMINotFluxo: TIntegerField;
    QrVMIFatNotaVNC: TFloatField;
    QrVMIFatNotaVRC: TFloatField;
    QrVMIPedItsLib: TIntegerField;
    QrVMIPedItsFin: TIntegerField;
    QrVMIPedItsVda: TIntegerField;
    QrVMIReqMovEstq: TIntegerField;
    QrVMIStqCenLoc: TIntegerField;
    QrVMIItemNFe: TIntegerField;
    QrVMIVSMorCab: TIntegerField;
    QrVMIVSMulFrnCab: TIntegerField;
    QrVSProQuiCodigo: TIntegerField;
    QrVSProQuiControle: TIntegerField;
    QrVSProQuiSetorID: TSmallintField;
    QrVSProQuiMovimCod: TIntegerField;
    QrVSProQuiMovimNiv: TIntegerField;
    QrVSProQuiMovimTwn: TIntegerField;
    QrVSProQuiEmpresa: TIntegerField;
    QrVSProQuiTerceiro: TIntegerField;
    QrVSProQuiMovimID: TIntegerField;
    QrVSProQuiPecas: TFloatField;
    QrVSProQuiPesoKg: TFloatField;
    QrVSProQuiAreaM2: TFloatField;
    QrVSProQuiCusPQ: TFloatField;
    QrVSProQuiFuloes: TIntegerField;
    QrVSProQuiMinDta: TDateField;
    QrVSProQuiMaxDta: TDateField;
    QrVSProQuiEstqPeca: TFloatField;
    QrVSProQuiEstqPeso: TFloatField;
    QrVSProQuiEstqArM2: TFloatField;
    QrVSProQuiLk: TIntegerField;
    QrVSProQuiDataCad: TDateField;
    QrVSProQuiDataAlt: TDateField;
    QrVSProQuiUserCad: TIntegerField;
    QrVSProQuiUserAlt: TIntegerField;
    QrVSProQuiAlterWeb: TSmallintField;
    QrVSProQuiAtivo: TSmallintField;
    QrVSMovItsClientMO: TIntegerField;
    QrSumEmit: TmySQLQuery;
    QrSumEmitCusto: TFloatField;
    QrVMIs: TmySQLQuery;
    QrVMIsControle: TIntegerField;
    QrSumVMI: TmySQLQuery;
    QrVMIsValorMP: TFloatField;
    QrSumEmitPeso: TFloatField;
    QrUniNFe: TmySQLQuery;
    QrUniNFeNFeSer: TSmallintField;
    QrUniNFeNFeNum: TIntegerField;
    QrUniNFeVSMulNFeCab: TIntegerField;
    QrUniNFePecas: TFloatField;
    QrMulNFe: TmySQLQuery;
    QrMulNFeNFeSer: TSmallintField;
    QrMulNFeNFeNum: TIntegerField;
    QrMulNFePecas: TFloatField;
    QrTotNFe: TmySQLQuery;
    QrTotNFePecas: TFloatField;
    QrVSCabNFe: TmySQLQuery;
    QrVSCabNFeSerie: TSmallintField;
    QrVSCabNFenNF: TIntegerField;
    QrMFSrcMovimID: TIntegerField;
    QrMFSrcMovimNiv: TIntegerField;
    QrMFSrcMovimCod: TIntegerField;
    QrVSMovCab: TmySQLQuery;
    QrVMIsQtde: TFloatField;
    QrSumVMIQtde: TFloatField;
    QrVSBalEmp: TmySQLQuery;
    QrVSBalEmpAnoMes: TIntegerField;
    QrX999_: TmySQLQuery;
    QrRedzConfltDstGGX: TMySQLQuery;
    DsRedzConfltDstGGX: TDataSource;
    QrRedzConfltDstGGXControle: TIntegerField;
    QrRedzConfltDstGGXGraGruX: TIntegerField;
    QrRedzConfltDstGGXSrcCtrl: TIntegerField;
    QrRedzConfltDstGGXDstGGX: TIntegerField;
    DsRedzConfltPreClsRcl: TDataSource;
    QrRedzConfltPreClsRcl: TMySQLQuery;
    QrRedzConfltPreClsRclCodigo: TIntegerField;
    QrRedzConfltPreClsRclControle: TIntegerField;
    QrRedzConfltPreClsRclMovimID: TIntegerField;
    QrRedzConfltPreClsRclMovimNiv: TIntegerField;
    QrRedzConfltPreClsRclGraGruX: TIntegerField;
    QrRedzConfltPreClsRclPecas: TFloatField;
    QrRedzConfltPreClsRclSrcCtrl: TIntegerField;
    QrRedzConfltPreClsRclSrcID: TIntegerField;
    QrRedzConfltPreClsRclSrcNiv: TIntegerField;
    QrRedzConfltPreClsRclSrcNivel2: TIntegerField;
    QrRedzConfltPreClsRclSrcGGX: TIntegerField;
    QrRedzConfltPreClsRclSrcPc: TFloatField;
    QrCustosDifSrcDst: TMySQLQuery;
    DsCustosDifSrcDst: TDataSource;
    QrCustosDifSrcDstRpControle: TIntegerField;
    QrCustosDifSrcDstRpTpCalc: TWideStringField;
    QrCustosDifSrcDstRpCusValr: TFloatField;
    QrCustosDifSrcDstRnCusValr: TFloatField;
    QrCustosDifSrcDstRnControle: TIntegerField;
    QrCustosDifSrcDstRnMovimID: TIntegerField;
    QrCustosDifSrcDstRnCusQtde: TFloatField;
    QrCustosDifSrcDstPerc: TFloatField;
    QrCustosDifSdoVr2: TMySQLQuery;
    DsCustosDifSdoVr2: TDataSource;
    QrCustosDifSdoVr2Controle: TIntegerField;
    QrCustosDifSdoVr2Pallet: TIntegerField;
    QrCustosDifSdoVr2Pecas_A: TFloatField;
    QrCustosDifSdoVr2PesoKg_A: TFloatField;
    QrCustosDifSdoVr2AreaM2_A: TFloatField;
    QrCustosDifSdoVr2ValorT_A: TFloatField;
    QrCustosDifSdoVr2SdoVrtPeca: TFloatField;
    QrCustosDifSdoVr2SdoVrtPeso: TFloatField;
    QrCustosDifSdoVr2SdoVrtArM2: TFloatField;
    QrCustosDifSdoVr2CustoUnit_A: TFloatField;
    QrCustosDifSdoVr2SdoVrtValT_A: TFloatField;
    QrCustosDifSdoVr2SrcNivel2: TIntegerField;
    QrCustosDifSdoVr2Pecas_B: TFloatField;
    QrCustosDifSdoVr2PesoKg_B: TFloatField;
    QrCustosDifSdoVr2AreaM2_B: TFloatField;
    QrCustosDifSdoVr2ValorT_B: TFloatField;
    QrCustosDifSdoVr2DifVal: TFloatField;
    QrGroupConcat: TMySQLQuery;
    QrCustosDifSdoVr2NO_TTW: TWideStringField;
    QrCustosDifSdoVr2ID_TTW: TLargeintField;
    QrCustosDifSrcDstRpNO_TTW: TWideStringField;
    QrCustosDifSrcDstRpID_TTW: TLargeintField;
    QrCustosDifSrcDstRnNO_TTW: TWideStringField;
    QrCustosDifSrcDstRnID_TTW: TLargeintField;
    QrCustosDifSdoVr2MovimID_A: TIntegerField;
    QrCustosDifSdoVr2MovimNiv_A: TIntegerField;
    QrCustosDifSdoVr2DifKg: TFloatField;
    QrCustosDifSdoVr2DifPc: TFloatField;
    QrCustosDifSdoVr2DifM2: TFloatField;
    QrFldPosNegMesmoReg: TMySQLQuery;
    QrFldPosNegMesmoRegCodigo: TIntegerField;
    QrFldPosNegMesmoRegControle: TIntegerField;
    QrFldPosNegMesmoRegMovimCod: TIntegerField;
    QrFldPosNegMesmoRegMovimID: TIntegerField;
    QrFldPosNegMesmoRegMovimNiv: TIntegerField;
    QrFldPosNegMesmoRegPallet: TIntegerField;
    QrFldPosNegMesmoRegPecas: TFloatField;
    QrFldPosNegMesmoRegPesoKg: TFloatField;
    QrFldPosNegMesmoRegAreaM2: TFloatField;
    QrFldPosNegMesmoRegValorT: TFloatField;
    QrFldPosNegMesmoRegSdoVrtPeca: TFloatField;
    QrFldPosNegMesmoRegSdoVrtPeso: TFloatField;
    QrFldPosNegMesmoRegSdoVrtArM2: TFloatField;
    QrFldPosNegMesmoRegSrcMovID: TIntegerField;
    QrFldPosNegMesmoRegSrcNivel1: TIntegerField;
    QrFldPosNegMesmoRegSrcNivel2: TIntegerField;
    QrFldPosNegMesmoRegDstMovID: TIntegerField;
    QrFldPosNegMesmoRegDstNivel1: TIntegerField;
    QrFldPosNegMesmoRegDstNivel2: TIntegerField;
    DsFldPosNegMesmoReg: TDataSource;
    QrCustosDifSrcDstValorT: TFloatField;
    QrCustosDifSrcDstValorMP: TFloatField;
    QrCustosDifSrcDstCustoPQ: TFloatField;
    QrCustosDifSrcDstCustoMOTot: TFloatField;
    QrCustosDifSdoVr2FatorLimitante: TFloatField;
    QrRedConfltGerA: TMySQLQuery;
    DsRedConfltGerA: TDataSource;
    QrRedConfltGerACodigo: TIntegerField;
    QrRedConfltGerAMovimCod: TIntegerField;
    QrRedConfltGerAGraGruX: TIntegerField;
    QrRedConfltGerADstGGX: TIntegerField;
    QrRedConfltGerAControle: TIntegerField;
    QrRedConfltGerADstNivel2: TIntegerField;
    QrRedConfltBaixa: TMySQLQuery;
    DsRedConfltBaixa: TDataSource;
    QrRedConfltBaixaMovimCod_Bxa: TIntegerField;
    QrRedConfltBaixaControle_Bxa: TIntegerField;
    QrRedConfltBaixaMovimID_Bxa: TIntegerField;
    QrRedConfltBaixaGraGruX_Bxa: TIntegerField;
    QrRedConfltBaixaSrcGGX_Bxa: TIntegerField;
    QrRedConfltBaixaMovimCod_Ori: TIntegerField;
    QrRedConfltBaixaControle_Ori: TIntegerField;
    QrRedConfltBaixaMovimID_Ori: TIntegerField;
    QrRedConfltBaixaGraGruX_Ori: TIntegerField;
    QrCustosDifPreRecl: TMySQLQuery;
    DsCustosDifPreRecl: TDataSource;
    QrCustosDifPreReclDstNivel2: TIntegerField;
    QrCustosDifPreReclValorT: TFloatField;
    QrCustosDifPreReclID_TTW: TLargeintField;
    QrCustosDifReclasse: TMySQLQuery;
    DsCustosDifReclasse: TDataSource;
    QrCustosDifReclasseDataHora: TDateTimeField;
    QrCustosDifReclasseMovimTwn_1: TIntegerField;
    QrCustosDifReclasseValorT_1: TFloatField;
    QrCustosDifReclasseAreaM2_1: TFloatField;
    QrCustosDifReclasseMovimTwn_2: TIntegerField;
    QrCustosDifReclasseControle_2: TIntegerField;
    QrCustosDifReclasseValorT_2: TFloatField;
    QrCustosDifReclasseAreaM2_2: TFloatField;
    QrCustosDifReclasseID_TTW: TLargeintField;
    QrCorreCustosIMECs: TMySQLQuery;
    DsCorreCustosIMECs: TDataSource;
    QrCorreCustosIMECsDataHora: TDateTimeField;
    QrCorreCustosIMECsCodigo: TIntegerField;
    QrCorreCustosIMECsControle: TIntegerField;
    QrCorreCustosIMECsMovimCod: TIntegerField;
    QrCorreCustosIMECsMovimID: TIntegerField;
    QrCorreCustosIMECsMovimNiv: TIntegerField;
    QrCorreCustosIMECsSrcMovID: TIntegerField;
    QrCorreCustosIMECsSrcGGX: TIntegerField;
    QrCorreCustosIMECsSrcNivel2: TIntegerField;
    QrCorreCustosIMECsAreaM2: TFloatField;
    QrCorreCustosIMECsValorT: TFloatField;
    QrCorreCustosIMECsCustoM2: TFloatField;
    QrCorreCustosIMECsID_TTW: TLargeintField;
    QrVmiPaiDifProcessos: TMySQLQuery;
    DsVmiPaiDifProcessos: TDataSource;
    QrVmiPaiDifProcessosCodigo: TIntegerField;
    QrVmiPaiDifProcessosControle: TIntegerField;
    QrVmiPaiDifProcessosMovimCod: TIntegerField;
    QrVmiPaiDifProcessosMovimID: TIntegerField;
    QrVmiPaiDifProcessosMovimNiv: TIntegerField;
    QrVmiPaiDifProcessosSrcMovID: TIntegerField;
    QrVmiPaiDifProcessosSrcNivel1: TIntegerField;
    QrVmiPaiDifProcessosSrcNivel2: TIntegerField;
    QrVmiPaiDifProcessosID_TTW: TLargeintField;
    QrCustosDifSdoVr3: TMySQLQuery;
    QrCustosDifSdoVr3Controle: TIntegerField;
    QrCustosDifSdoVr3Pallet: TIntegerField;
    QrCustosDifSdoVr3Pecas_A: TFloatField;
    QrCustosDifSdoVr3PesoKg_A: TFloatField;
    QrCustosDifSdoVr3AreaM2_A: TFloatField;
    QrCustosDifSdoVr3ValorT_A: TFloatField;
    QrCustosDifSdoVr3SdoVrtPeca: TFloatField;
    QrCustosDifSdoVr3SdoVrtPeso: TFloatField;
    QrCustosDifSdoVr3SdoVrtArM2: TFloatField;
    QrCustosDifSdoVr3CustoUnit_A: TFloatField;
    QrCustosDifSdoVr3SdoVrtValT_A: TFloatField;
    QrCustosDifSdoVr3SrcVmi: TIntegerField;
    QrCustosDifSdoVr3Pecas_B: TFloatField;
    QrCustosDifSdoVr3PesoKg_B: TFloatField;
    QrCustosDifSdoVr3AreaM2_B: TFloatField;
    QrCustosDifSdoVr3ValorT_B: TFloatField;
    QrCustosDifSdoVr3DifVal: TFloatField;
    QrCustosDifSdoVr3NO_TTW: TWideStringField;
    QrCustosDifSdoVr3ID_TTW: TLargeintField;
    QrCustosDifSdoVr3MovimID_A: TIntegerField;
    QrCustosDifSdoVr3MovimNiv_A: TIntegerField;
    QrCustosDifSdoVr3DifKg: TFloatField;
    QrCustosDifSdoVr3DifPc: TFloatField;
    QrCustosDifSdoVr3DifM2: TFloatField;
    QrCustosDifSdoVr3FatorLimitante: TFloatField;
    DsCustosDifSdoVr3: TDataSource;
    QrVmiPaiDifProcessosMovCodPai: TIntegerField;
    QrPcPosNegXValTNegPos: TMySQLQuery;
    DsPcPosNegXValTNegPos: TDataSource;
    QrPcPosNegXValTNegPosCodigo: TIntegerField;
    QrPcPosNegXValTNegPosControle: TIntegerField;
    QrPcPosNegXValTNegPosMovimCod: TIntegerField;
    QrPcPosNegXValTNegPosMovimID: TIntegerField;
    QrPcPosNegXValTNegPosMovimNiv: TIntegerField;
    QrPcPosNegXValTNegPosPecas: TFloatField;
    QrPcPosNegXValTNegPosPesoKg: TFloatField;
    QrPcPosNegXValTNegPosAreaM2: TFloatField;
    QrPcPosNegXValTNegPosValorT: TFloatField;
    QrPcPosNegXValTNegPosID_TTW: TLargeintField;
    QrVlrPosNegSameReg: TMySQLQuery;
    DsVlrPosNegSameReg: TDataSource;
    QrVlrPosNegSameRegControle: TIntegerField;
    QrVlrPosNegSameRegMovimID: TIntegerField;
    QrVlrPosNegSameRegMovimNiv: TIntegerField;
    QrVlrPosNegSameRegValorMP: TFloatField;
    QrVlrPosNegSameRegValorT: TFloatField;
    QrVlrPosNegSameRegCustoMOTot: TFloatField;
    QrVlrPosNegSameRegCusFrtAvuls: TFloatField;
    QrVlrPosNegSameRegDiferenca: TFloatField;
    QrVlrPosNegSameRegID_TTW: TLargeintField;
    QrVsSifDipoa: TMySQLQuery;
    QrVsSifDipoaEmpresa: TIntegerField;
    QrVsSifDipoaRegistro: TWideStringField;
    QrVsSifDipoaDdValid: TIntegerField;
    QrVsSifDipoaArqLogoFilial: TWideStringField;
    QrVsSifDipoaArqLogoSIF: TWideStringField;
    QrVsSifDipoaQualDtFab: TSmallintField;
    QrAux: TMySQLQuery;
    procedure TbCouNiv1AfterEdit(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
    procedure PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod:
              Integer; DataHora: String; NewVMI: Integer; SQLType: TSQLType);
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    procedure AtualizaVSMulFrnCabNew(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldEMxx;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
    procedure InformaSemFornecedor();
    function  PreparaPalletParaReclassificar(const SQLType: TSQLType; const
              CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo,
              MovimCod, Controle, Pallet, GraGruX, GraGruY: Integer;
              DtHr: TDateTime; StqCenLoc: Integer; IuvpeiInn, IuvpeiBxa:
              TInsUpdVMIPrcExecID): Boolean;
    function  AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;

























    procedure Atualiza13SinteticoDtHr(Empresa: Integer; VSFluxIncon: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
(*
    procedure AtualizaVSMulFrnCabOld(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldEMxx;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
*)
    procedure DefinePeriodoBalVSEmpresaLogada();
    procedure InformaSemNFe();
    //function  RegistrosX999(SQL: array of String): Integer;
    procedure Reopen16Fatores(TP16DataIni_Date, TP16DataFim_Date:
              TDateTime; Ck16DataIni_Checked, Ck16DataFim_Checked: Boolean);
    function  DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2: TLabel): Boolean;
    function  SrcGGXDiferentesDePreClasseParaClasseEReclasse(ForcaMostrarForm,
              SelfCall, TemIMERT: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  RedConfltGerA(ForcaMostrarForm,
              SelfCall, TemIMERT: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  RedConfltBaixa(ForcaMostrarForm,
              SelfCall, TemIMERT: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  VmiPaiDifProcessos(ForcaMostrarForm, SelfCall: Boolean;
              TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    (*
    function  CustosDiferentesOrigemParaDestino1(Empresa, GraGruX: Integer;
              MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
              TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    *)
    function  CustosDiferentesOrigemParaDestino2(Empresa, GraGruX: Integer;
              MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
              TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    function  CustosDifPreRecl(Empresa, GraGruX: Integer;
              MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
              TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    function  CustosDifReclasse(Empresa, GraGruX: Integer;
              MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
              TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    function  CorreCustosIMECs(Empresa, GraGruX, IMECIni: Integer; MargemErro,
              CustoMin: Double; JaCorrigidos: Boolean; Avisa, ForcaMostrarForm,
              SelfCall: Boolean; TemIMEIMrt: Integer; sMovimIDs: String;
              LaAviso1, LaAviso2: TLabel): Boolean;
    (*
    function  CustosDiferentesSaldosVirtuais(Empresa, GraGruX: Integer; Avisa,
              ForcaMostrarForm, SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    *)
    function  CustosDiferentesSaldosVirtuais2(Empresa, GraGruX, TemIMEIMrt:
              Integer; MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall:
              Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  CustosDiferentesSaldosVirtuais3(Empresa, GraGruX, TemIMEIMrt:
              Integer; MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall:
              Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  QtdPosNegSameReg(TemIMEIMrt: Integer; Avisa, ForcaMostrarForm,
              SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  VlrPosNegSameReg(TemIMEIMrt: Integer; Avisa, ForcaMostrarForm,
              SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    function  VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt:
              Integer; MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall:
              Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure ZeraValoMP_de_MovimID_2();
    procedure ReopenVSSifDipoa(Empresa: Integer);
    function  NaoEhCouroEmProcesso(GraGruX: Integer; Avisar: Boolean): Boolean;

  end;

var
  DmModVS: TDmModVS;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmModVS }

uses
  ModVS_CRC, MyDBCheck, UnVS_CRC_PF,
  DmkDAC_PF, ModuleGeral, UnMyObjects, UnVS_PF, UMySQLModule, Module,
  UnDmkProcFunc, StqCenLoc, GetValor, VSRedzConfltDstGGX, VSCustosDifSrcDst,
  VSRedzConfltPreClasRecl, VSQtdPosNegSameReg, VSVlrPosNegSameReg, VSRedConfltGerA,
  VSRedConfltBaixa, VSCustosDifPreRecl, VSCustosDifReclasse, VSCorreCustosIMECs,
  VSVmiPaiDifProcessos, VSCustosDifSdoVr2, VSCustosDifSdoVr3,
  VSPcPosNegXValTNegPos;
  //, (*ModProd,*) GraGruX;

procedure TDmModVS.Atualiza13SinteticoDtHr(Empresa: Integer; VSFluxIncon: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
var
  LastDtHr, BalDtHr: String;
  SerieFch, Ficha, IMEI_Src, Pallet, Operacao: Integer;
  //
  procedure DefineLastDtHr();
  begin
(*
    if VS_PF.ImpedePeloBalanco(Qr13DtHrDataHora.Value, False) then
      LastDtHr := BalDtHr
    else
      LastDtHr := Geral.FDT(Qr13DtHrDataHora.Value, 109);
*)
    LastDtHr := Geral.FDT(Qr13DtHrDataHora.Value, 109);
  end;
begin
  LastDtHr := '';
  if VS_PF.ImpedePeloBalanco(Now(), False, False) then
    BalDtHr := Geral.FDT(VS_PF.ObtemDataBalanco(Empresa), 109)
  else
    BalDtHr := Geral.FDT(Now(), 109);
  UnDmkDAC_PF.AbreMySQLQuery0(Qr13Sintetico, DModG.MyPID_DB, [
  'SELECT vfi.*, vsf.Nome NO_SERFCH, ggy.Nome NO_GGY  ',
  'FROM ' + VSFluxIncon + ' vfi ',
  'LEFT JOIN ' + TMeuDB + '.gragruy ggy ON ggy.Codigo=vfi.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch vsf ON vsf.Codigo=vfi.SerieFch ',
  'ORDER BY vfi.GraGruY, vfi.SerieFch,  ',
  'vfi.Ficha, vfi.IMEI_Src, vfi.Pallet ',
  '']);
  PB1.Position := 0;
  PB1.Max := Qr13Sintetico.RecordCount;
  Qr13Sintetico.First;
  while not Qr13Sintetico.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    case Qr13SinteticoGraGruY.Value of
      1024:
      begin
        SerieFch := Qr13SinteticoSerieFch.Value;
        Ficha    := Qr13SinteticoFicha.Value;
        if (SerieFch <> 0) and (Ficha <> 0) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcNivel2 IN ',
          '( ',
          '  SELECT Controle ',
          '  FROM ' + CO_SEL_TAB_VMI + ' ',
          '  WHERE SerieFch=' + Geral.FF0(SerieFch),
          '  AND Ficha=' + Geral.FF0(Ficha),
          '  AND MovimID=1 ',
          ') ',
          ' ']);
          //
          DefineLastDtHr();
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['SerieFch', 'Ficha'], [
          LastDtHr], [SerieFch, Ficha], False);
        end;
      end;
      2048:
      begin
        IMEI_Src := Qr13SinteticoIMEI_Src.Value;
        if IMEI_Src <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE SrcNivel2=' + Geral.FF0(IMEI_Src),
          '']);
          DefineLastDtHr();
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['IMEI_Src'], [
          LastDtHr], [IMEI_Src], False);
        end;
      end;
      3072:
      begin
        Pallet := Qr13SinteticoPallet.Value;
        if Pallet <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora  ',
          'FROM ' + CO_SEL_TAB_VMI + '  ',
          'WHERE SrcNivel2 IN  ',
          '(  ',
          '  SELECT Controle  ',
          '  FROM ' + CO_SEL_TAB_VMI + '  ',
          '  WHERE Pallet=' + Geral.FF0(Pallet),
          '  AND Pecas>0 ',
          ')  ',
          '']);
          DefineLastDtHr();
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['Pallet'], [
          LastDtHr], [Pallet], False);
        end;
      end;
      4096:
      begin
        Operacao := Qr13SinteticoOperacao.Value;
        if (Operacao <> 0) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qr13DtHr, Dmod.MyDB, [
          'SELECT MAX(DataHora) DataHora ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
(*
          'WHERE SrcNivel2 IN ',
          '( ',
          '  SELECT Controle ',
          '  FROM ' + CO_SEL_TAB_VMI + ' ',
*)
          '  WHERE Codigo=' + Geral.FF0(Operacao),
          '  AND MovimID=11 ',
(*
          '  AND MovimNiv=8 ',
          ') ',
*)
          ' ']);
          //
          DefineLastDtHr();
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VSFluxIncon, False, [
          'LastDtHr'], ['Operacao'], [
          LastDtHr], [Operacao], False);
        end;
      end;
      else
      begin
        Geral.MB_Erro('"GraGruY" n�o implementado!');
        LastDtHr := '';
      end;
    end;
    Qr13Sintetico.Next;
  end;
end;

{
function TDmModVS.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
const
  TipoStqMovIts = VAR_FATID_0104;
var
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
  EstqPC, EstqM2, EstqP2, EstqKg: Double;
  Encerrado: Integer;
//var
  PecasCal, PecasCur, PecasNeg,
  PesoCal, PesoCur, CusPQCal, CusPQCur, CustoPQ: Double;
  MinDtaCal, MinDtaCur, MinDtaEnx,
  MaxDtaCal, MaxDtaCur, MaxDtaEnx: String;
  FuloesCal, FuloesCur: Integer;
(*
var
  MinDtaCal, MinDtaCur, MinDtaRec, MinDtaEnx, MaxDtaCal, MaxDtaCur, MaxDtaRec, MaxDtaEnx: String;
  Controle, FuloesCal, FuloesCur, FuloesRec, Encerrado: Integer;
  PecasCal, PecasCur, PecasRec, PecasExp, M2Exp, P2Exp, PesoExp, PecasNeg, PesoCal, PesoCur, PesoRec, CusPQCal, CusPQCur, CusPQRec, Estq_Pecas, Estq_M2, Estq_P2, Estq_Peso: Double;
  SQLType: TSQLType;
*)begin
  //Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSProQui, Dmod.MyDB, [
  'SELECT *  ',
  'FROM vsproqui ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
(*
  /
    //
  QrSumCus.Close;
  QrSumCus.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSumCus, MyDB);
SELECT SUM(Custo) Custo
FROM emitcus
WHERE MPIn=:P0
  //
  QrSPerdas.Close;
  QrSPerdas.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrSPerdas, MyDB);
  PecasNeg := QrSPerdasPecas.Value;
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSCus2, Dmod.MyDB, [
  'SELECT emc.MPIn, SUM(emc.Custo) Custo,',
  'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,',
  'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,',
  'COUNT(emc.MPIn) Fuloes, rec.Setor',
  'FROM emitcus emc',
  'LEFT JOIN formulas rec ON rec.Numero=emc.Formula',
  'WHERE emc.VSMovIts=' + Geral.FF0(Controle),
  'GROUP BY rec.Setor',
  '']);
  //
  PecasCal  := 0;
  PecasCur  := 0;
  PesoCal   := 0;
  PesoCur   := 0;
  CusPQCal  := 0;
  CusPQCur  := 0;
  FuloesCal := 0;
  FuloesCur := 0;
  MinDtaCal := '0000-00-00';
  MinDtaCur := '0000-00-00';
  MinDtaEnx := '0000-00-00';
  MaxDtaCal := '0000-00-00';
  MaxDtaCur := '0000-00-00';
  MaxDtaEnx := '0000-00-00';
  if TEstqMovimID(QrVMIMovimID.Value) = emidCompra then
  begin
    while not QrSCus2.Eof do
    begin
      if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
      begin
        PecasCal  := PecasCal  + QrSCus2Pecas.Value;
        PesoCal   := PesoCal   + QrSCus2Peso.Value;
        CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
        FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
        MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
      end else
      if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
      begin
        PecasCur  := PecasCur  + QrSCus2Pecas.Value;
        PesoCur   := PesoCur   + QrSCus2Peso.Value;
        CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
        FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
        MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
      end
      else Geral.MB_Aviso(
      'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
  end;
  //
  EstqPC := 0;
  EstqM2 := 0;
  EstqP2 := 0;
  EstqKg := 0;
  //
(*
  DmProd.ObtemQuantidadesBaixa(TipoStqMovIts, Controle, Qtde, Pecas, Peso, AreaM2, AreaP2);
*)
{
  SQLType        := ImgTipo.SQLType?;
  Controle       := ;
  PecasCal       := ;
  PecasCur       := ;
  PecasRec       := ;
  PecasExp       := ;
  M2Exp          := ;
  P2Exp          := ;
  PesoExp        := ;
  PecasNeg       := ;
  PesoCal        := ;
  PesoCur        := ;
  PesoRec        := ;
  CusPQCal       := ;
  CusPQCur       := ;
  CusPQRec       := ;
  FuloesCal      := ;
  FuloesCur      := ;
  FuloesRec      := ;
  MinDtaCal      := ;
  MinDtaCur      := ;
  MinDtaRec      := ;
  MinDtaEnx      := ;
  MaxDtaCal      := ;
  MaxDtaCur      := ;
  MaxDtaRec      := ;
  MaxDtaEnx      := ;
  Estq_Pecas     := ;
  Estq_M2        := ;
  Estq_P2        := ;
  Estq_Peso      := ;
  Encerrado      := ;
  /
  Encerrado := QrMPInForcaEncer.Value;
  if Encerrado = 0  then
    if (Pecas + PecasNeg >=  QrMPInPecas.Value)
    and (QrMPInPecas.Value > 0) then
      Encerrado := 1;
  //
  if (Encerrado = 0) then
  begin
    EstqPC := QrMPInPecas.Value - QrMPInPecasNeg.Value - Pecas;
    if EstqPC > 0 then
    begin
      EstqM2 := QrMPInM2.Value - AreaM2;
      if EstqM2 < 0 then EstqM2 := 0;
      //
      EstqP2 := QrMPInP2.Value - AreaP2;
      if EstqP2 < 0 then EstqP2 := 0;
      //
      EstqKg := QrMPInPLE.Value - Peso;
      if EstqKg < 0 then EstqKg := 0;
      //
    end else EstqPC := 0;
  end;
  //
  CustoPQ := CusPQCal + CusPQCur;
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'vsproqui', auto_increment?[
'PecasCal', 'PecasCur', 'PecasRec',
'PecasExp', 'M2Exp', 'P2Exp',
'PesoExp', 'PecasNeg', 'PesoCal',
'PesoCur', 'PesoRec', 'CusPQCal',
'CusPQCur', 'CusPQRec', 'FuloesCal',
'FuloesCur', 'FuloesRec', 'MinDtaCal',
'MinDtaCur', 'MinDtaRec', 'MinDtaEnx',
'MaxDtaCal', 'MaxDtaCur', 'MaxDtaRec',
'MaxDtaEnx', 'Estq_Pecas', 'Estq_M2',
'Estq_P2', 'Estq_Peso', 'Encerrado'], [
'Controle'], [
PecasCal, PecasCur, PecasRec,
PecasExp, M2Exp, P2Exp,
PesoExp, PecasNeg, PesoCal,
PesoCur, PesoRec, CusPQCal,
CusPQCur, CusPQRec, FuloesCal,
FuloesCur, FuloesRec, MinDtaCal,
MinDtaCur, MinDtaRec, MinDtaEnx,
MaxDtaCal, MaxDtaCur, MaxDtaRec,
MaxDtaEnx, Estq_Pecas, Estq_M2,
Estq_P2, Estq_Peso, Encerrado], [
Controle], UserDataAlterweb?, IGNORE?

  UMyMod.SQLInsUpd(QrUpd, stUpd, 'mpin', False, [
    'PecasCal', 'PecasCur', 'PecasNeg',
    'PesoCal', 'PesoCur', 'CusPQCal', 'CusPQCur',
    'MinDtaCal', 'MinDtaCur', 'MinDtaEnx',
    'MaxDtaCal', 'MaxDtaCur', 'MaxDtaEnx',
    'FuloesCal', 'FuloesCur',
    'PecasExp', 'M2Exp', 'P2Exp', 'PesoExp',
    'Estq_Pecas', 'Estq_M2', 'Estq_P2', 'Estq_Peso',
    'CustoPQ', 'Encerrado'
  ], ['Controle'], [
    PecasCal, PecasCur, PecasNeg,
    PesoCal, PesoCur, CusPQCal, CusPQCur,
    MinDtaCal, MinDtaCur, MinDtaEnx,
    MaxDtaCal, MaxDtaCur, MaxDtaEnx,
    FuloesCal, FuloesCur,
    Pecas, AreaM2, AreaP2, Peso,
    EstqPC, EstqM2, EstqP2, EstqKg,
    CustoPQ, Encerrado
  ], [Controle], True);
  //
  Result := True;
end;
}



{
procedure TDmModVS.AtualizaVSMulFrnCabOld(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldEMxx; MovTypeCod: Integer; MovimIDs:
  array of TEstqMovimID; MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
  //
  function ReInsereNovoVSMulFrnCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulfrncab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulFrn)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrnits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulfrncab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulfrncab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulFrn, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulfrnits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND Fornece=' + Geral.FF0(Fornece),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulfrnits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulfrnits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'Fornece', 'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    Fornece, Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  Terceiro, VSMulFrnCab, I: Integer;
  //Terceiros: array of Integer;
  //Pecas: array of Double;
  //
  Codigo, Remistura, CodRemix, Fornece: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  SiglaVS, Nome, Percentual, FldUpd: String;
  sSrcNivel2: String;
begin
  SQLMovimID  := '';
  FldUpd      := '???';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    if SQLMovimID <> '' then
      SQLMovimID :=  SQLMovimID + ',';
    SQLMovimID :=  Geral.FF0(Integer(MovimIDs[I]));
  end;
  if SQLMovimID <> '' then
    SQLMovimID :=  'AND MovimID IN (' + SQLMovimID + ')';
  //
  SQLMovimNiv  := '';
  for I := Low(MovimNivsOri) to High(MovimNivsOri) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  // IMEIs de origem
  case DefMulFrn of
    TEstqDefMulFldEMxx.edmfSrcNiv2:
    begin
(**)
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=(',
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_SEL_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      ') ',
      '']);
(**)
(*
      UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_SEL_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      '']);
      //
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
      '']);
*)
      FldUpd := 'Controle';
    end;
    TEstqDefMulFldEMxx.edmfMovCod:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, SUM(Pecas) Pecas  ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
      SQLMovimID,
      SQLMovimNiv,
      'GROUP BY Terceiro, VSMulFrnCab ',
      'ORDER BY Terceiro,  VSMulFrnCab ',
      ' ',
      '']);
      FldUpd := 'MovimCod';
    end;
    else
    begin
      Geral.MB_Erro('"Defini��o de fornecedor abortada!' + sLineBreak +
      '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulFrn)));
      Exit;
    end;
  end;
  //
  //Geral.MB_SQL(nil, QrUniFrn);
  // Se tiver Item sem fornecedor, desiste!
  if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
  begin
    if VAR_MEMO_DEF_VS_MUL_FRN <> nil then
       VAR_MEMO_DEF_VS_MUL_FRN.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
       sLineBreak + VAR_MEMO_DEF_VS_MUL_FRN.Text
    else
      InformaSemFornecedor();
    Exit;
  end;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  //
  // Se tiver s� um registro...
  if QrUniFrn.RecordCount = 1 then
  begin
    if QrUniFrnTerceiro.Value <> 0 then
      Terceiro := QrUniFrnTerceiro.Value
    else
      VSMulFrnCab := QrUniFrnVSMulFrnCab.Value;
  end else
  // Se tiver varios registros...
  begin
    Terceiro := 0;
    Codigo   := ReInsereNovoVSMulFrnCab();
    while not QrUniFrn.Eof do
    begin
      if (QrUniFrnTerceiro.Value <> 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
      begin
        Remistura := 0;
        CodRemix  := 0;
        Fornece   := QrUniFrnTerceiro.Value;
        Pecas     := QrUniFrnPecas.Value;
        ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, True);
      end else
      if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value <> 0) then
      begin
        Remistura := 1;
        CodRemix  := QrUniFrnVSMulFrnCab.Value;
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
        'SELECT Fornece, SUM(Pecas) Pecas, "" SiglaVS',
        'FROM vsmulfrnits',
        'WHERE Codigo=' + Geral.FF0(CodRemix),
        'GROUP BY Fornece ',
        '']);
        TotMul := 0;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          TotMul := TotMul + QrMulFrnPecas.Value;
          //
          QrMulFrn.Next;
        end;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          //Pecas     := QrMulFrnPecas.Value;
          if TotMul <> 0 then
            Pecas := (QrMulFrnPecas.Value / TotMul) * QrUniFrnPecas.Value
          else
            Pecas := 0;
          //
          Fornece   := QrMulFrnFornece.Value;
          ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, False);
          //
          QrMulFrn.Next;
        end;
      end;
      //
      QrUniFrn.Next;
    end;
    // Atualiza VSMulFrnCab!
    UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
    'SELECT mfi.Fornece, SUM(mfi.Pecas) Pecas, vem.SiglaVS',
    'FROM vsmulfrnits mfi',
    'LEFT JOIN vsentimp vem ON vem.Codigo=mfi.Fornece',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY mfi.Fornece',
    'ORDER BY Pecas DESC',
    '']);
    UnDMkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
    'SELECT SUM(mfi.Pecas) Pecas ',
    'FROM vsmulfrnits mfi',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    '']);
    Nome := '';
    while not QrMulFrn.Eof do
    begin
      if QrMulFrnSiglaVS.Value = '' then
        SiglaVS := VS_PF.DefineSiglaVS(QrMulFrnFornece.Value)
      else
        SiglaVS := QrMulFrnSiglaVS.Value;
      //
      if QrTotFrnPecas.Value <> 0 then
      begin
        PerPecas   := QrMulFrnPecas.Value / QrTotFrnPecas.Value * 100;
        Percentual := Geral.FI64(Round(PerPecas));
      end else
      begin
        PerPecas   := 0;
        Percentual := '??';
      end;
      //
      Nome := Nome + SiglaVS + ' ' + Percentual + '% ';
      //
      QrMulFrn.Next;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrncab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
      VSMulFrnCab := Codigo;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_VMI,
  'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
  'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
(**)
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE SrcNivel2 IN (',
  '  SELECT Controle',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  ') ',
  '']);
(**)
(*
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle', EmptyStr);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
  '']);
*)
  QrMFSrc.First;
  while not QrMFSrc.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
    'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
    'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
    '']);
    //
    QrMFSrc.Next;
  end;
(**)
  // Orfaos!
  UnDMkDAC_PF.AbreMySQLQuery0(QrMFOrfaos, Dmod.MyDB, [
  'SELECT Controle, Terceiro, VSMulFrnCab',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE SrcNivel2<>0',
  '  AND Terceiro=0',
  '  AND VSMulFrnCab=0',
  ') ',
  'AND (',
  '  Terceiro<>0',
  '  OR',
  '  VSMulFrnCab<>0',
  ')',
  '']);
  QrMFOrfaos.First;
  while not QrMFOrfaos.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(QrMFOrfaosTerceiro.Value) + ', ',
    'VSMulFrnCab=' + Geral.FF0(QrMFOrfaosVSMulFrnCab.Value),
    'WHERE SrcNivel2<>0 ',
    'AND SrcNivel2=' + Geral.FF0(QrMFOrfaosControle.Value),
    'AND Terceiro=0 ',
    'AND VSMulFrnCab=0 ',
    '']);
    //
    QrMFOrfaos.Next;
  end;
(**)
end;
}

procedure TDmModVS.DefinePeriodoBalVSEmpresaLogada();
begin
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSBalEmp, Dmod.MyDB, [
  'SELECT MAX(AnoMes) AnoMes ',
  //'FROM vsbalemp ',
  'FROM spedefdicmsipience ',
  'WHERE MovimXX=1 ', // 1=Feito
  'AND Empresa IN (' + VAR_LIB_EMPRESAS + ')',
  '']);
  //
  VAR_VS_PERIODO_BAL :=
    Geral.AnoMesToPeriodo(QrVSBalEmpAnoMes.Value);
end;

procedure TDmModVS.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2: TLabel): Boolean;
  procedure Reabre();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRedzConfltDstGGX, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _t03_; ',
    'CREATE TABLE _t03_ ',
    'SELECT Controle SrcCtrl, DstNivel2, DstGGX  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE DstNivel2 <> 0; ',
    ' ',
    ' ',
    'SELECT vmi.Controle, vmi.GraGruX, ',
    't03.SrcCtrl, t03.DstGGX  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN _t03_ t03 ON t03.DstNivel2=vmi.Controle ',
    'WHERE vmi.GraGruX<>t03.DstGGX; ',
    '']);
  end;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos em entrada e baixa');
  Reabre();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  Result := QrRedzConfltDstGGX.RecordCount > 0;
  if Result then
  begin
    if DBCheck.CriaFm(TFmVSRedzConfltDstGGX, FmVSRedzConfltDstGGX, afmoLiberado) then
    begin
      FmVSRedzConfltDstGGX.ShowModal;
      FmVSRedzConfltDstGGX.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos em entrada e baixa');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'False');
    Result := QrRedzConfltDstGGX.RecordCount > 0;
  end;
end;

function TDmModVS.QtdPosNegSameReg(TemIMEIMrt:
  Integer; Avisa, ForcaMostrarForm, SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL, Mensagem: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID,  ',
    'MovimNiv, Pallet, Pecas, PesoKg, AreaM2, ValorT, ',
    'SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,  ',
    'SrcMovID, SrcNivel1, SrcNivel2, ',
    'DstMovID, DstNivel1, DstNivel2 ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE GraGruX = 1 ',
    'AND ( ',
    '(pecas > 0 AND (PesoKg < 0 OR AreaM2 <0)) ',
    'OR ',
    '(PesoKg > 0 AND (Pecas < 0 OR AreaM2 <0)) ',
    'OR ',
    '(AreaM2 > 0 AND (PesoKg < 0 OR AreaM2 <0)) ',
    ') ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrFldPosNegMesmoReg, DMod.MyDB, [SQL]);
    if Avisa then
    begin
      if QrFldPosNegMesmoReg.RecordCount > 0 then
        Mensagem := 'Existem ' + Geral.FF0(QrFldPosNegMesmoReg.RecordCount) +
          ' IME-Is com campos positivos e negativos no mesmo registro!'
      else
        Mensagem :=
        'N�o foi localizado nenhum IME-I com campos positivos e negativos no mesmo registro';
      //
      if SelfCall then
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
      else
        if QrFldPosNegMesmoReg.RecordCount > 0 then
          Geral.MB_Aviso(Mensagem);
    end;
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is com campos positivos e negativos no mesmo registro');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    Result := QrFldPosNegMesmoReg.RecordCount > 0;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSQtdPosNegSameReg, FmVSQtdPosNegSameReg, afmoLiberado) then
    begin
      if ForcaMostrarForm then
        FmVSQtdPosNegSameReg.PnPesquisa.Visible := True;
      FmVSQtdPosNegSameReg.ShowModal;
      FmVSQtdPosNegSameReg.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is com campos positivos e negativos no mesmo registro');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'False');
    Result := QrFldPosNegMesmoReg.RecordCount > 0;
  end;
end;

procedure TDmModVS.InformaSemFornecedor();
begin
  if Dmod.QrControleVSWarNoFrn.Value = 1 then
    Geral.MB_Info('Defini��o de fornecedor abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem fornecedor definido!');
end;

procedure TDmModVS.InformaSemNFe();
begin
  if Dmod.QrControleVSWarSemNF.Value = 1 then
    Geral.MB_Info('Defini��o de NFe abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem NFe definida!');
end;

function TDmModVS.NaoEhCouroEmProcesso(GraGruX: Integer; Avisar: Boolean): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT nv2.Codigo',
  'FROM gragrux ggx',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '']);
  Result := QrAux.FieldByName('Codigo').AsInteger <> 1; // Couro
  if (Result = True) and (Avisar = True) then
    Geral.MB_Aviso('O artigo em processo n�o � couro.' + slineBreak +
    'Utilize o bot�o "Destino" para gerar subproduto processado e baixar do estoque do artigo em processo!');
end;

{
function TDmModVS.RegistrosX999(SQL: array of String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrX999, Dmod.MyDB, SQL);
  //Geral.MB_SQL(Self, QrX999);
  Result := QrX999.RecordCount;
end;
}

procedure TDmModVS.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

function TDmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, TemIMERT: Boolean;
  LaAviso1, LaAviso2: TLabel): Boolean;
  function Reabre(): Boolean;
  var
    Corda: String;
  begin
    Result := False;
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _vs_err_ggx_bxa_; ',
    'CREATE TABLE _vs_err_ggx_bxa_ ',
    'SELECT Codigo, MovimCod,  ',
    'Controle, MovimID, MovimNiv, ',
    'GraGruX, SrcGGX, SrcNivel2 ',
    'FROM ' + TMeuDB + '.vsmovits ',
    'WHERE Pecas < 0 ',
    //'AND GraGruX=' ,
    '; ',
    'SELECT DISTINCT SrcNivel2 ',
    'FROM _vs_err_ggx_bxa_; ',
    '']);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'SrcNivel2', '0');
    if Corda <> '0' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrRedConfltBaixa, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _vs_err_ggx_ori_; ',
      'CREATE TABLE _vs_err_ggx_ori_ ',
      'SELECT Codigo, MovimCod, Controle, ',
      'MovimID, MovimNiv, GraGruX ',
      'FROM ' + TMeuDB + '.vsmovits ',
      'WHERE Controle IN (' + Corda + ') ',
      '; ',
      'SELECT bxa.MovimCod MovimCod_Bxa,  ',
      'bxa.Controle Controle_Bxa,  ',
      'bxa.MovimID MovimID_Bxa, ',
      'bxa.GraGruX GraGruX_Bxa,  ',
      'bxa.SrcGGX SrcGGX_Bxa, ',
      'ori.MovimCod MovimCod_Ori,  ',
      'ori.Controle Controle_Ori, ',
      'ori.MovimID MovimID_Ori, ',
      'ori.GraGruX GraGruX_Ori ',
      'FROM _vs_err_ggx_bxa_ bxa ',
      'LEFT JOIN _vs_err_ggx_ori_ ori ON ori.Controle=bxa.SrcNivel2 ',
      'WHERE ori.GraGruX<>bxa.GraGruX ',
      '']);
      //Geral.MB_Teste(QrRedConfltBaixa.SQL.Text);
      Result := QrRedConfltBaixa.RecordCount > 0;
    end;
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos nas baixas');
    Result := Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSRedConfltBaixa, FmVSRedConfltBaixa, afmoLiberado) then
    begin
      FmVSRedConfltBaixa.CkIMERT.Checked := TemIMERT;
      if ForcaMostrarForm then
        FmVSRedConfltBaixa.PnPesquisa.Visible := True;
      //
      FmVSRedConfltBaixa.ShowModal;
      FmVSRedConfltBaixa.Destroy;
    end;
    if Result then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos nas baixas');
      Result := Reabre();
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
  end;
end;

function TDmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, TemIMERT: Boolean;
  LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRedConfltGerA, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _ger_confl_13_; ',
    'CREATE TABLE _ger_confl_13_ ',
    'SELECT Codigo, MovimCod, GraGruX ',
    'FROM ' + TMeuDB + '.vsmovits ',
    'WHERE MovimNiv=13 ',
    '; ',
    'DROP TABLE IF EXISTS _ger_confl_15_; ',
    'CREATE TABLE _ger_confl_15_ ',
    'SELECT Controle, DstNivel2, Codigo, MovimCod, DstGGX ',
    'FROM ' + TMeuDB + '.vsmovits ',
    'WHERE MovimNiv=15 ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _ger_confl_er_; ',
    'CREATE TABLE _ger_confl_er_ ',
    'SELECT _13.Codigo, _13.MovimCod, _13.GraGruX, ',
    '_15.DstGGX, _15.Controle, _15.DstNivel2   ',
    'FROM _ger_confl_13_ _13 ',
    'LEFT JOIN _ger_confl_15_ _15 ON _13.Codigo=_15.Codigo ',
    'AND _13.GraGruX <> _15.DstGGX ',
    '; ',
    'SELECT *  ',
    'FROM _ger_confl_er_ ',
    'WHERE GraGruX <> DstGGX ',
    'AND DstGGX <> 0 ',
    '']);
    //Geral.MB_Teste(QrRedConfltGerA.SQL.Text);
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos nas gera��es de artigo ');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrRedConfltGerA.RecordCount > 0;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSRedConfltGerA, FmVSRedConfltGerA, afmoLiberado) then
    begin
      FmVSRedConfltGerA.CkIMERT.Checked := TemIMERT;
      if ForcaMostrarForm then
        FmVSRedConfltGerA.PnPesquisa.Visible := True;
      //
      FmVSRedConfltGerA.ShowModal;
      FmVSRedConfltGerA.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos nas gera��es de artigo ');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrRedConfltGerA.RecordCount > 0;
  end;
end;

procedure TDmModVS.Reopen16Fatores(TP16DataIni_Date, TP16DataFim_Date:
  TDateTime; Ck16DataIni_Checked, Ck16DataFim_Checked: Boolean);
var
  SQL_Periodo: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('AND pra.DtHrFimCla ', TP16DataIni_Date,
  TP16DataFim_Date, Ck16DataIni_Checked, Ck16DataFim_Checked);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr16Fatores, Dmod.MyDB, [
  'SELECT 1.000 ClaRcl, pra.DtHrFimCla, pra.Codigo CodigoClaRcl, ',
  'pra.CacCod OC, pra.VSGerArt IDGeracao,',
  '0.000 Pallet_Src, pri.VSPallet Pallet_Dst, ',
  'c1s.FatorInt FatorIntSrc, c1d.FatorInt FatorIntDst ',
  'FROM vspaclaitsa pri',
  'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + '    vmi ON vmi.Controle=pra.VSMovIts',
  'LEFT JOIN vspalleta   pad ON pad.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou  xcs ON xcs.GraGruX=vmi.GraGruX',
  'LEFT JOIN gragruxcou  xcd ON xcd.GraGruX=pad.GraGruX',
  'LEFT JOIN couniv1     c1s ON c1s.Codigo=xcs.CouNiv1 ',
  'LEFT JOIN couniv1     c1d ON c1d.Codigo=xcd.CouNiv1 ',
  'WHERE c1s.FatorInt <> c1d.FatorInt',
  SQL_Periodo,
  '',
  'UNION',
  '',
  'SELECT 2.000 ClaRcl, pra.DtHrFimCla, pra.Codigo CodigoClaRcl, ',
  'pra.CacCod OC, pra.VSGerRcl IDGeracao,',
  'pra.VSPallet + 0.000 Pallet_Src, pri.VSPallet Pallet_Dst, ',
  'c1s.FatorInt FatorIntSrc, c1d.FatorInt FatorIntDst ',
  'FROM vsparclitsa pri',
  'LEFT JOIN vsparclcaba pra ON pra.Codigo=pri.Codigo',
  'LEFT JOIN vspalleta   pas ON pas.Codigo=pra.VSPallet',
  'LEFT JOIN vspalleta   pad ON pad.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou  xcs ON xcs.GraGruX=pas.GraGruX',
  'LEFT JOIN gragruxcou  xcd ON xcd.GraGruX=pad.GraGruX',
  'LEFT JOIN couniv1     c1s ON c1s.Codigo=xcs.CouNiv1 ',
  'LEFT JOIN couniv1     c1d ON c1d.Codigo=xcd.CouNiv1 ',
  'WHERE c1s.FatorInt <> c1d.FatorInt',
  SQL_Periodo,
  '',
  'ORDER BY DtHrFimCla',
  '']);
end;

procedure TDmModVS.ReopenVSSifDipoa(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSSifDipoa, Dmod.MyDB, [
  'SELECT * ',
  'FROM vssifdipoa ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(
  ForcaMostrarForm, SelfCall, TemIMERT: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRedzConfltPreClsRcl, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _t03_; ',
    'CREATE TABLE _t03_ ',
    'SELECT Controle SrcCtrl, MovimID SrcID,',
    'MovimNiv SrcNiv, SrcNivel2, SrcGGX,',
    'Pecas SrcPc  ',
    'FROM ' + TMeuDB + '.vsmovits ',
    'WHERE SrcNivel2 <> 0',
    'AND MovimID IN (7,8); ',
    ' ',
    'SELECT vmi.Codigo, vmi.Controle, vmi.MovimID,',
    'vmi.MovimNiv, vmi.GraGruX, vmi.Pecas, t03.*  ',
    'FROM ' + TMeuDB + '.vsmovits vmi ',
    'LEFT JOIN _t03_ t03 ON t03.SrcNivel2=vmi.Controle ',
    'WHERE vmi.GraGruX<>t03.SrcGGX',
    'AND vmi.MovimID=15',
    '']);
    //Geral.MB_Teste(QrRedzConfltPreClsRcl.SQL.Text);
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos em pr� classe / reclasse');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrRedzConfltPreClsRcl.RecordCount > 0;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSRedzConfltPreClasRecl, FmVSRedzConfltPreClasRecl, afmoLiberado) then
    begin
      FmVSRedzConfltPreClasRecl.CkIMERT.Checked := TemIMERT;
      if ForcaMostrarForm then
        FmVSRedzConfltPreClasRecl.PnPesquisa.Visible := True;
      //
      FmVSRedzConfltPreClasRecl.ShowModal;
      FmVSRedzConfltPreClasRecl.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando igualdade de reduzidos em pr� classe / reclasse');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrRedzConfltPreClsRcl.RecordCount > 0;
  end;
end;

function TDmModVS.VlrPosNegSameReg(TemIMEIMrt: Integer; Avisa, ForcaMostrarForm,
  SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL, Mensagem: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados');
    SQL_Flds := Geral.ATS([
    'Controle, MovimID, MovimNiv,  ',
    'ValorMP, ValorT, CustoMOTot, CusFrtAvuls, ',
    'ValorT-ValorMP Diferenca ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE ValorMP <> 0 ',
    'AND ValorT <> 0 ',
    'AND ( ',
    ' ( ',
    '  ValorMP < 0  ',
    '  AND  ',
    '  ValorT > 0 ',
    ' ) ',
    ' OR ',
    ' ( ',
    '  ValorMP > 0  ',
    '  AND  ',
    '  ValorT < 0 ',
    ' ) ',
    ') ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVlrPosNegSameReg, DMod.MyDB, [SQL]);
    if Avisa then
    begin
      if QrVlrPosNegSameReg.RecordCount > 0 then
        Mensagem := 'Existem ' + Geral.FF0(QrVlrPosNegSameReg.RecordCount) +
          ' IME-Is com valores positivos e negativos no mesmo registro!'
      else
        Mensagem :=
        'N�o foi localizado nenhum IME-I com valores positivos e negativos no mesmo registro';
      //
      if SelfCall then
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
      else
        if QrVlrPosNegSameReg.RecordCount > 0 then
          Geral.MB_Aviso(Mensagem);
    end;
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is com valores positivos e negativos no mesmo registro');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    Result := QrVlrPosNegSameReg.RecordCount > 0;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSVlrPosNegSameReg, FmVSVlrPosNegSameReg, afmoLiberado) then
    begin
      if ForcaMostrarForm then
        FmVSVlrPosNegSameReg.PnPesquisa.Visible := True;
      FmVSVlrPosNegSameReg.ShowModal;
      FmVSVlrPosNegSameReg.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is com valores positivos e negativos no mesmo registro');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'False');
    Result := QrVlrPosNegSameReg.RecordCount > 0;
  end;
end;

function TDmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall: Boolean;
  TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;

  procedure Reabre();
  var
    Corda, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IME-Cs de processos');
    SQL_Flds := Geral.ATS([
    'MovimCod ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimID IN (11,19,26,27,29,32,33,38,39) ',
    '']);
    SQL_Group := 'GROUP BY MovimCod';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _corrigir_vmi_pai_a_; ',
    'CREATE TABLE _corrigir_vmi_pai_a_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ';',
    'SELECT DISTINCT MovimCod FROM _corrigir_vmi_pai_a_',
    ';']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'MovimCod', '-999999999');
    //
    //
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID, MovimNiv,  ',
    'SrcMovID, SrcNivel1, SrcNivel2, MovCodPai ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimCod IN ( ' + Corda +  ') ',
    'AND (',
    '  ( ',
    '    NOT (MovimNiv IN (7,20,27,29,34,39,44,49,54,65)) ',
    '    AND  ',
    '    Pecas < 0 ',
    '  ) ',
    '  OR MovimID=41 ',
    ') ',
    'AND VmiPai=0 ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    '']);
    //Geral.MB_Teste(SQL);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados pesquisados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmiPaiDifProcessos, DModG.MyPID_DB, [SQL]);
    //
    if QrVmiPaiDifProcessos.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrVmiPaiDifProcessos.RecordCount) +
      ' registros com VmiPai zerado.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com VmiPai zerado.')
  end;
begin
  Result := False;
  if ForcaMostrarForm  = False then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando VmiPai em IMEIS de baixa de processos');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrVmiPaiDifProcessos.RecordCount > 0;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSVmiPaiDifProcessos, FmVSVmiPaiDifProcessos, afmoLiberado) then
    begin
      FmVSVmiPaiDifProcessos.CkIMERT.Checked := Geral.IntToBool(TemIMEIMrt);
      if ForcaMostrarForm then
        FmVSVmiPaiDifProcessos.PnPesquisa.Visible := True;
      //
      FmVSVmiPaiDifProcessos.ShowModal;
      FmVSVmiPaiDifProcessos.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando VmiPai em IMEIS de baixa de processos');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrVmiPaiDifProcessos.RecordCount > 0;
  end;
end;

function TDmModVS.VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt: Integer;
  MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean; LaAviso1,
  LaAviso2: TLabel): Boolean;
  procedure Reabre();
  var
    SQL_GGX, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, ',
    'MovimCod, MovimID, MovimNiv, ',
    'Pecas, PesoKg, AreaM2, ValorT ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE ( ',
    '  (Pecas < 0 AND ((PesoKg<0) OR (AreaM2<0)) AND ValorT > 0 ) ',
    '   OR ',
    '  (Pecas > 0 AND ((PesoKg>0) OR (AreaM2>0)) AND ValorT < 0 ) ',
    ') ',
    'AND NOT (MovimID IN (9,12,17,41)) ',
  ' ',
    '']);
    SQL_Group := 'ORDER BY Controle ';
    SQL := Geral.ATS([
    //'DROP TABLE IF EXISTS _?_; ',
    //'CREATE TABLE _?_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPcPosNegXValTNegPos, DModG.MyPID_DB, [SQL]);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  //Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Result := False;
  //if (GraGruX <> 0) and (Empresa <> 0) then
  //if Empresa <> 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pe�as positivas/negativas X Valor total negativos/positivos');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrPcPosNegXValTNegPos.RecordCount > 0;
    if Avisa then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrPcPosNegXValTNegPos.RecordCount) +
        ' registros das Pe�as positivas/negativas X Valor total negativos/positivos do reduzido ' +
        Geral.FF0(GraGruX);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem);
        Geral.MB_Aviso(Mensagem);
      end;
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSVSPcPosNegXValTNegPos, FmVSVSPcPosNegXValTNegPos, afmoLiberado) then
    begin
      FmVSVSPcPosNegXValTNegPos.DGDados.DataSource := DsPcPosNegXValTNegPos;
      FmVSVSPcPosNegXValTNegPos.CkTemIMEiMrt.Checked := Geral.IntToBool(TemIMEiMrt);
      (*
      FmVSVSPcPosNegXValTNegPos.EdGraGruX.ValueVariant := GraGruX;
      FmVSVSPcPosNegXValTNegPos.CBGraGruX.KeyValue := GraGruX;
      *)
      FmVSVSPcPosNegXValTNegPos.PnPesquisa.Visible := True; //ForcaMostrarForm;
      FmVSVSPcPosNegXValTNegPos.ShowModal;
      FmVSVSPcPosNegXValTNegPos.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pe�as positivas/negativas X Valor total negativos/positivos');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrPcPosNegXValTNegPos.RecordCount > 0;
  end;
end;

procedure TDmModVS.TbCouNiv1AfterEdit(DataSet: TDataSet);
var
  ResVar: Variant;
  CasasDecimais: Integer;
  Qtde: Double;
begin
  Qtde          := TbCouNiv1FatorInt.Value;
  CasasDecimais := 3;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Qtde, CasasDecimais, 0, '0', '1', True, 'Fator', 'Informe o fator: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    TbCouNiv1FatorInt.ReadOnly := False;
    TbCouNiv1FatorInt.Value := Qtde;
    TbCouNiv1.Post;
    TbCouNiv1FatorInt.ReadOnly := True;
  end;
end;

procedure TDmModVS.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

procedure TDmModVS.ZeraValoMP_de_MovimID_2;
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE vsmovits SET ValorMP=0.00 WHERE MovimID=2 AND ValorMP <> 0');
end;

procedure TDmModVS.AtualizaVSMulFrnCabNew(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldEMxx; MovTypeCod: Integer; MovimIDs:
  array of TEstqMovimID; MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
begin
  DmModVS_CRC.AtualizaVSMulFrnCabNew(SinalOrig, DefMulFrn, MovTypeCod, MovimIDs,
  MovimNivsOri, MovimNivsDst);
end;

(*
function TDmModVS.CustosDiferentesOrigemParaDestino1(Empresa, GraGruX: Integer;
  MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean; TemIMEIMrt: Integer;
  LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID,  ',
    'MovimNiv, Pallet, Pecas, PesoKg, AreaM2, ValorT, ',
    'Pecas SdoVrtPeca, AreaM2 SdoVrtArM2,  ',
    'SrcMovID, SrcNivel1, SrcNivel2, ',
    'DstMovID, DstNivel1, DstNivel2 ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) ',
    'AND (NOT (MovimID IN (9,12,17,41))) ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _rastro_posit_; ',
    'CREATE TABLE _rastro_posit_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de Baixa');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID,   ',
    'MovimNiv, Pecas, PesoKg, AreaM2, ValorT,  ',
    '0.000 SdoVrtPeca, 0.00 SdoVrtArM2,   ',
    'SrcMovID, SrcNivel1, SrcNivel2,  ',
    'DstMovID, DstNivel1, DstNivel2  ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE (  ',
    '  (Pecas < 0)  ',
    '  OR  ',
    '  (AreaM2 < 0)   ',
    '  OR  ',
    '  (pesoKg < 0)  ',
    '  OR   ',
    '  (MovimID IN (9,12,17,41))  ',
    ')  ',
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _rastro_negat_;  ',
    'CREATE TABLE _rastro_negat_  ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados pesquisados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifSrcDst, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _rastro_difer_; ',
    'CREATE TABLE _rastro_difer_  ',
    'SELECT rp.NO_TTW RpNO_TTW, rp.ID_TTW RpID_TTW, ',
    'rp.Controle RpControle, ',
    'IF(rp.AreaM2 <> 0, "AreaM2", "PesoKg") RpTpCalc, ',
    'IF(rp.AreaM2 <> 0, rp.ValorT/rp.AreaM2,  ',
    '  IF(rp.PesoKg <> 0, rp.ValorT/rp.PesoKg, 0.00)) RpCusValr, ',
    'IF(rp.AreaM2 <> 0, rn.ValorT/rn.AreaM2,  ',
    '  IF(rn.PesoKg <> 0, rn.ValorT/rn.PesoKg, 0.00)) RnCusValr, ',
    'rn.Controle RnControle, rn.MovimID RnMovimID, ',
    'IF(rp.AreaM2 <> 0, rn.AreaM2,  ',
    '  IF(rn.PesoKg <> 0, rn.PesoKg, 0.00)) RnCusQtde, ',
    'rn.NO_TTW RnNO_TTW, rn.ID_TTW RnID_TTW ',
    'FROM _rastro_negat_ rn ',
    'LEFT JOIN _rastro_posit_  rp ON rp.Controle=rn.SrcNivel2; ',
    ' ',
    'SELECT rd.*,  ',
    'IF(rd.RnCusValr > rd.RpCusValr, rd.RnCusValr / rd.RpCusValr, ',
    '  rd.RpCusValr / rd.RnCusValr) * 100 Perc ',
    'FROM _rastro_difer_ rd ',
    'WHERE ROUND(rd.RnCusValr, 2) <> ROUND(rd.RpCusValr, 2)  ',
    'AND RnCusQtde <> 0 ',
    'AND ABS(rd.RnCusValr - rd.RpCusValr) * ABS(rd.RnCusQtde) >= ' +
    Geral.FFT_Dot(MargemErro, 10, siNegativo),
    '']);
    //Geral.MB_Teste(QrCustosDifSrcDst.SQL.Text);
    //
    if QrCustosDifSrcDst.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrCustosDifSrcDst.RecordCount) +
      ' registros com custo divergente.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com custo divergente.')
  end;
var
  Mensagem: String;
begin
  Result := False;
  if (GraGruX <> 0) and (Empresa <> 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSrcDst.RecordCount > 0;
    if (Avisa = True) or (LaAviso1 <> nil) then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifSrcDst.RecordCount) +
          ' registros onde o custo diverge entre a origem e o destino';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end else
      begin
        Mensagem := 'N�o foram encontrados registros onde o custo diverge entre a origem e o destino';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end;
      if LaAviso1 <> nil then
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
      else
        Geral.MB_Aviso(Mensagem);
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifSrcDst, FmVSCustosDifSrcDst, afmoLiberado) then
    begin
      FmVSCustosDifSrcDst.PnPesquisa.Visible := ForcaMostrarForm;
      FmVSCustosDifSrcDst.ShowModal;
      FmVSCustosDifSrcDst.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSrcDst.RecordCount > 0;
  end;
end;
*)


function TDmModVS.CorreCustosIMECs(Empresa, GraGruX, IMECIni: Integer;
  MargemErro, CustoMin: Double; JaCorrigidos: Boolean; Avisa, ForcaMostrarForm,
  SelfCall: Boolean; TemIMEIMrt: Integer; sMovimIDs: String; LaAviso1,
  LaAviso2: TLabel): Boolean;
  var
    SQL_GraGruX: String;

  procedure Reabre();
  var
    SQL_JaCorrigidos, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL, SQL_MovimIDs: String;
  begin
    if GraGruX <> 0 then
      SQL_GraGruX := 'AND DstGGX=' + Geral.FF0(GraGruX)
    else
      SQL_GraGruX := '';
    if JaCorrigidos = False then
      SQL_JaCorrigidos := 'AND Corrigido=0'
    else
      SQL_JaCorrigidos := '';
    if sMovimIDs <> '' then
      SQL_MovimIDs := 'AND MovimID IN (' + sMovimIDs + ')';
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados');
    SQL_Flds := Geral.ATS([
    'vmi.DataHora, vmi.Codigo, vmi.Controle,  ',
    'vmi.MovimCod, vmi.MovimID, vmi.MovimNiv,  ',
    'vmi.SrcMovID, vmi.SrcGGX, vmi.SrcNivel2,  ',
    'vmi.AreaM2, vmi.ValorT, ',
    'ABS(vmi.ValorT / vmi.AreaM2) CustoM2 ',
    '']);
    SQL_Left := 'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ';
    SQL_Wher := Geral.ATS([
    'WHERE vmi.AreaM2 <> 0 ',
    'AND ggx.GraGruY IN (2048,3072) ',
    'AND NOT (MovimID IN (41)) ',
    'AND ABS(vmi.ValorT / vmi.AreaM2) < ' + Geral.FFT_Dot(CustoMin, 6, siPositivo),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND MovimCod>=' + Geral.FF0(IMECIni),
    //'AND SrcMovID=27', // Teste curidos com PQ
    SQL_GraGruX,
    SQL_JaCorrigidos,
    SQL_MovimIDs,
    '; ',
    '']);
    //Geral.MB_Teste(SQL);
    //
    SQL := Geral.ATS([
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorreCustosIMECs, Dmod.MyDB, SQL);
    //
    if QrCorreCustosIMECs.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrCorreCustosIMECs.RecordCount) +
      ' registros com custo divergente.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com custo divergente.')
  end;
var
  Mensagem: String;
begin
  //if (GraGruX <> 0) and (Empresa <> 0) then
  //if SelfCall = True then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCorreCustosIMECs.RecordCount > 0;
    if (Avisa = True) or (LaAviso1 <> nil) then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCorreCustosIMECs.RecordCount) +
          ' registros onde o custo diverge na pr� reclasse';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end else
      begin
        if LaAviso1 <> nil then
        begin
          Mensagem := 'N�o foram encontrados registros onde o custo diverge na pr� reclasse';
          if GraGruX <> 0 then
            Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
        end;
      end;
      if Mensagem <> '' then
      begin
        if LaAviso1 <> nil then
          MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
        else
          Geral.MB_Aviso(Mensagem);
      end;
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCorreCustosIMECs, FmVSCorreCustosIMECs, afmoLiberado) then
    begin
      FmVSCorreCustosIMECs.PnPesquisa.Visible := True;//ForcaMostrarForm;
      FmVSCorreCustosIMECs.EdGraGrux.ValueVariant := GraGruX;
      FmVSCorreCustosIMECs.CBGraGrux.KeyValue := GraGruX;
      FmVSCorreCustosIMECs.ShowModal;
      FmVSCorreCustosIMECs.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCorreCustosIMECs.RecordCount > 0;
  end;
end;


function TDmModVS.CustosDiferentesOrigemParaDestino2(Empresa, GraGruX: Integer;
  MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
  TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
  //
  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID,  ',
    'MovimNiv, Pallet, Pecas, PesoKg, AreaM2, ValorT CustoT, ',
    'Pecas SdoVrtPeca, AreaM2 SdoVrtArM2,  ',
    'SrcMovID, SrcNivel1, SrcNivel2, ',
    'DstMovID, DstNivel1, DstNivel2 ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) ',
    'AND (NOT (MovimID IN (9,12,17,41))) ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _rastro_posit_; ',
    'CREATE TABLE _rastro_posit_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de Baixa');
    SQL_Flds := Geral.ATS([
    'Codigo, Controle, MovimCod, MovimID,    ',
    'MovimNiv, Pecas, PesoKg, AreaM2,  ',
    'ValorMP, CustoPQ, CustoMOTot, ValorT,',
    'ValorT CustoT,',
    '/*',
    'IF(ValorT > 0.00, "ValorT",  ',
    '  IF(ValorMP <> 0.00, "ValorMP", "Custos")) Campo, ',
    'IF(ValorT > 0.00, ValorT,  ',
    '  IF(ValorMP <> 0.00, ValorMP,  ',
    '  ValorT - (CustoPQ + CustoMOTot))) CustoT, ',
    'ValorMP CustroT ',
    '*/',
    '0.000 SdoVrtPeca, 0.00 SdoVrtArM2,    ',
    'SrcMovID, SrcNivel1, SrcNivel2,   ',
    'DstMovID, DstNivel1, DstNivel2   ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE (  ',
    '  (Pecas < 0)  ',
    '  OR  ',
    '  (AreaM2 < 0)   ',
    '  OR  ',
    '  (pesoKg < 0)  ',
    '  OR   ',
    '  (MovimID IN (9,12,17,41))  ',
    ')  ',
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _rastro_negat_;  ',
    'CREATE TABLE _rastro_negat_  ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados pesquisados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifSrcDst, DModG.MyPID_DB, [
(*
    'DROP TABLE IF EXISTS _rastro_difer_; ',
    'CREATE TABLE _rastro_difer_  ',
    'SELECT rp.NO_TTW RpNO_TTW, rp.ID_TTW RpID_TTW, ',
    'rp.Controle RpControle, rn.Campo, ',
    'IF(rp.AreaM2 <> 0, "AreaM2", "PesoKg") RpTpCalc, ',
    // ini 2023-04-15
    //'IF(rp.AreaM2 <> 0, rp.ValorT/rp.AreaM2,  ',
    //'  IF(rp.PesoKg <> 0, rp.ValorT/rp.PesoKg, 0.00)) RpCusValr, ',
    //'IF(rp.AreaM2 <> 0, rn.ValorT/rn.AreaM2,  ',
    //'  IF(rn.PesoKg <> 0, rn.CustoT/rn.PesoKg, 0.00)) RnCusValr, ',
    'IF(rp.AreaM2 <> 0, rp.CustoT/rp.AreaM2,  ',
    '  IF(rp.PesoKg <> 0, rp.CustoT/rp.PesoKg, 0.00)) RpCusValr, ',
    'IF(rp.AreaM2 <> 0, rn.CustoT/rn.AreaM2,  ',
    '  IF(rn.PesoKg <> 0, rn.CustoT/rn.PesoKg, 0.00)) RnCusValr, ',
    // fim 2023-04-15
    'rn.Controle RnControle, rn.MovimID RnMovimID, ',
    'IF(rp.AreaM2 <> 0, rn.AreaM2,  ',
    '  IF(rn.PesoKg <> 0, rn.PesoKg, 0.00)) RnCusQtde, ',
    'rn.NO_TTW RnNO_TTW, rn.ID_TTW RnID_TTW ',
    'FROM _rastro_negat_ rn ',
    'LEFT JOIN _rastro_posit_  rp ON rp.Controle=rn.SrcNivel2; ',
    ' ',
    'SELECT rd.*,  ',
    'IF(rd.RnCusValr > rd.RpCusValr, rd.RnCusValr / rd.RpCusValr, ',
    '  rd.RpCusValr / rd.RnCusValr) * 100 Perc ',
    'FROM _rastro_difer_ rd ',
    'WHERE ROUND(rd.RnCusValr, 2) <> ROUND(rd.RpCusValr, 2)  ',
    'AND RnCusQtde <> 0 ',
    'AND ABS(rd.RnCusValr - rd.RpCusValr) * ABS(rd.RnCusQtde) >= ' +
    Geral.FFT_Dot(MargemErro, 10, siNegativo),
*)
    'DROP TABLE IF EXISTS _rastro_difer_;  ',
    'CREATE TABLE _rastro_difer_   ',
    'SELECT rp.NO_TTW RpNO_TTW, rp.ID_TTW RpID_TTW,  ',
    'rp.Controle RpControle,   ',
    'IF(rp.AreaM2 <> 0, "AreaM2", "PesoKg") RpTpCalc,  ',
    'IF(rp.AreaM2 <> 0, rp.CustoT/rp.AreaM2,   ',
    '  IF(rp.PesoKg <> 0, rp.CustoT/rp.PesoKg, 0.00)) RpCusValr,  ',
    'IF(rp.AreaM2 <> 0, rn.CustoT/rn.AreaM2,   ',
    '  IF(rn.PesoKg <> 0, rn.CustoT/rn.PesoKg, 0.00)) RnCusValr,  ',
    'rn.Controle RnControle, rn.MovimID RnMovimID,  ',
    'IF(rp.AreaM2 <> 0, rn.AreaM2,   ',
    '  IF(rn.PesoKg <> 0, rn.PesoKg, 0.00)) RnCusQtde,  ',
    'rn.ValorMP, rn.CustoPQ, rn.CustoMOTot, rn.ValorT, ',
    'rn.NO_TTW RnNO_TTW, rn.ID_TTW RnID_TTW ',
    'FROM _rastro_negat_ rn  ',
    'LEFT JOIN _rastro_posit_  rp ON rp.Controle=rn.SrcNivel2;  ',
    '  ',
    'SELECT rd.*,    ',
    '((IF(rd.RnCusValr > rd.RpCusValr, rd.RnCusValr / rd.RpCusValr,   ',
    '  rd.RpCusValr / rd.RnCusValr)) - 1) * 100 Perc   ',
    'FROM _rastro_difer_ rd   ',
    'WHERE ROUND(rd.RnCusValr, 2) <> ROUND(rd.RpCusValr, 2)    ',
    'AND RnCusQtde <> 0   ',
    'AND ABS(rd.RnCusValr - rd.RpCusValr) * ABS(rd.RnCusQtde) >= ', // 0.01
    Geral.FFT_Dot(MargemErro, 10, siNegativo),
    '']);
    //Geral.MB_Teste(QrCustosDifSrcDst.SQL.Text);
    //
    if QrCustosDifSrcDst.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrCustosDifSrcDst.RecordCount) +
      ' registros com custo divergente.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com custo divergente.')
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Result := False;
  if (GraGruX <> 0) and (Empresa <> 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSrcDst.RecordCount > 0;
    if (Avisa = True) or (LaAviso1 <> nil) then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifSrcDst.RecordCount) +
          ' registros onde o custo diverge entre a origem e o destino';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end else
      begin
        Mensagem := 'N�o foram encontrados registros onde o custo diverge entre a origem e o destino';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end;
      if LaAviso1 <> nil then
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
      else
        Geral.MB_Aviso(Mensagem);
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifSrcDst, FmVSCustosDifSrcDst, afmoLiberado) then
    begin
      FmVSCustosDifSrcDst.PnPesquisa.Visible := True;//ForcaMostrarForm;
      FmVSCustosDifSrcDst.EdGraGrux.ValueVariant := GraGruX;
      FmVSCustosDifSrcDst.CBGraGrux.KeyValue := GraGruX;
      FmVSCustosDifSrcDst.ShowModal;
      FmVSCustosDifSrcDst.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSrcDst.RecordCount > 0;
  end;
end;

(*
function TDmModVS.CustosDiferentesSaldosVirtuais1(Empresa, GraGruX: Integer;
  Avisa, ForcaMostrarForm, SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
  procedure Reabre();
  var
    Corda, SQL_GGX: String;
  begin
    SQL_GGX := '';
    if GraGruX <> 0 then
      SQL_GGX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrGroupConcat, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _t05a_; ',
    'CREATE TABLE _t05a_ ',
    'SELECT Controle, Pallet, Pecas Pecas_A,  ',
    'PesoKg PesoKg_A, AreaM2 AreaM2_A,  ',
    'ValorT ValorT_A, SdoVrtPeca,  ',
    'SdoVrtPeso, SdoVrtArM2, ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg,   ',
    '0)) CustoUnit_A,  ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2 * SdoVrtArM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg * SdoVrtPeso,   ',
    '0)) SdoVrtValT_A  ',
    ' ',
    'FROM ' + TMeuDB + '.vsmovits vmi  ',
    'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
    SQL_GGX,
    'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) ',
    'AND (NOT (MovimID IN (9,12,17,41))) ',
    '; ',
    //'SELECT GROUP_CONCAT(DISTINCT(Controle)) sControles ',
    'SELECT DISTINCT(Controle) Controle ',
    'FROM _t05a_; ',
    '']);
    Corda := MyObjects.CordaDeQuery(QrGroupConcat, 'Controle', '');
    if Corda <> EmptyStr then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifSdoVrt, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _t05b_; ',
      'CREATE TABLE _t05b_ ',
      'SELECT SrcNivel2, SUM(Pecas) Pecas_B,  ',
      'SUM(PesoKg) PesoKg_B, SUM(AreaM2) AreaM2_B,  ',
      'SUM(ValorT) ValorT_B  ',
      'FROM ' + TMeuDB + '.vsmovits vmi  ',
      'WHERE SrcNivel2 IN ( ' + Corda + ')  ',
      'GROUP BY SrcNivel2; ',
      ' ',
      'SELECT t05a.*, t05b.*, ',
      '(t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B DifVal, ',
      '((t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B) / ValorT_A * 100000 FatorLimitante ',
      'FROM _t05a_ t05a ',
      'LEFT JOIN _t05b_ t05b ON t05b.SrcNivel2=t05a.Controle ',
      'WHERE ABS((t05a.ValorT_A - t05a.SdoVrtValT_A)  ',
      '  + t05b.ValorT_B) > 0.01 ',
      ' ']);
    end;
  end;
var
  Mensagem: String;
begin
  Result := False;
  //if (GraGruX <> 0) and (Empresa <> 0) then
  if Empresa <> 0 then
  begin
    Reabre();
    Result := QrCustosDifSdoVrt.RecordCount > 0;
    if Avisa then
    begin
      if Result then
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifSdoVrt.RecordCount) +
        ' registros onde o saldo virtual do valor diverge  para o reduzido ' +
        Geral.FF0(GraGruX)
      else
        Geral.MB_Aviso(
        'N�o foram encontrados registros onde diverge o saldo virtual do valor para o reduzido ' +
        Geral.FF0(GraGruX));
      //
      if SelfCall then
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
      else
        Geral.MB_Aviso(Mensagem);
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifSdoVrt, FmVSCustosDifSdoVrt, afmoLiberado) then
    begin
      FmVSCustosDifSdoVrt.PnPesquisa.Visible := True; //ForcaMostrarForm;
      FmVSCustosDifSdoVrt.ShowModal;
      FmVSCustosDifSdoVrt.Destroy;
    end;
    Reabre();
    Result := QrCustosDifSdoVrt.RecordCount > 0;
  end;
end;
*)

function TDmModVS.CustosDiferentesSaldosVirtuais2(Empresa, GraGruX, TemIMEIMrt:
  Integer; MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
  LaAviso1, LaAviso2: TLabel): Boolean;
  procedure Reabre();
  var
    Corda, SQL_GGX, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    SQL_GGX := '';
    if GraGruX <> 0 then
      SQL_GGX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEIs de entrada');
    SQL_Flds := Geral.ATS([
    'Controle, MovimID MovimID_A, MovimNiv MovimNiv_A, Pallet, Pecas Pecas_A,  ',
    'PesoKg PesoKg_A, AreaM2 AreaM2_A,  ',
    'ValorT ValorT_A, SdoVrtPeca,  ',
    'SdoVrtPeso, SdoVrtArM2, ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg,   ',
    '0)) CustoUnit_A,  ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2 * SdoVrtArM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg * SdoVrtPeso,   ',
    '0)) SdoVrtValT_A  ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
    SQL_GGX,
    'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) ',
    'AND (NOT (MovimID IN (9,12,17,41))) ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _t05a_; ',
    'CREATE TABLE _t05a_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    '; ',
    'SELECT DISTINCT(Controle) Controle ',
    'FROM _t05a_; ',
    '']);
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.AbreMySQLQuery0(QrGroupConcat, DModG.MyPID_DB, [SQL]);
    //

    //Criar IMEI PAi para lozalizar processados?

    //PareAqui! 2023-05-11
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEIs de baixa');
    Corda := MyObjects.CordaDeQuery(QrGroupConcat, 'Controle', '');
    if Corda <> EmptyStr then
    begin
      SQL_Flds := Geral.ATS([
      'SrcNivel2, SUM(Pecas) Pecas_B,  ',
      'SUM(PesoKg) PesoKg_B, SUM(AreaM2) AreaM2_B,  ',
      // ini 2023-04-15
      // ini 2023-05-10
      //'SUM(ValorT) ValorT_B  ',

      //'SUM(ValorT * IF(MovimID IN (30), -1, 1)) ValorT_B ',

      //'IF(MovimID IN (30), SUM(-ValorT),  SUM(ValorT)) ValorT_B ',
      'IF(MovimID IN (26), SUM(ValorMP), SUM(ValorT)) ValorT_B ',
      // fim 2023-05-10
      //'IF(ValorT > 0.00, ValorT, IF(ValorMP <> 0, ValorMP, ValorT - (CustoPQ + CustoMOTot))) ValorT_B ',
      // fim 2023-04-15
      '']);
      SQL_Left := '';
      SQL_Wher := Geral.ATS([
      'WHERE SrcNivel2 IN ( ' + Corda + ')  ',
      '']);
      SQL_Group := 'GROUP BY SrcNivel2 ';
      SQL := Geral.ATS([
      'DROP TABLE IF EXISTS _t05b_; ',
      'CREATE TABLE _t05b_ ',
      VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
      VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
      '; ',
      ' ',
      'SELECT t05a.*, t05b.*, ',
      '(t05a.PesoKg_A - t05a.SdoVrtPeso) + t05b.PesoKg_B DifKg, ',
      '(t05a.Pecas_A - t05a.SdoVrtPeca) + t05b.Pecas_B DifPc, ',
      '(t05a.AreaM2_A - t05a.SdoVrtArM2) + t05b.AreaM2_B DifM2, ',
      '(t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B DifVal, ',
      '((t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B) / ValorT_A * 100000 FatorLimitante ',
      'FROM _t05a_ t05a ',
      'LEFT JOIN _t05b_ t05b ON t05b.SrcNivel2=t05a.Controle ',
(*
      'WHERE ABS((t05a.ValorT_A - t05a.SdoVrtValT_A)  ',
      '  + t05b.ValorT_B) >= 0.01', // + Geral.FFT_Dot(MargemErro, 10, siNegativo),
*)
      'WHERE ABS(ABS((t05a.ValorT_A - t05a.SdoVrtValT_A) ',
      '  -ABS( t05b.ValorT_B))) >= 0.01 ',
      // ini 2023-04-15
      'OR ABS(PesoKg_A - SdoVrtPeso) <> ABS(PesoKg_B) ',
      'OR ABS(Pecas_A - SdoVrtPeca) <> ABS(Pecas_B) ',
      'OR ABS(AreaM2_A - SdoVrtArM2) <> ABS(AreaM2_B) ',
      // fim 2023-04-15
      ' ']);
      //Geral.MB_Teste(SQL);
      UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifSdoVr2, DModG.MyPID_DB, [SQL]);
      //
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  //Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Result := False;
  //if (GraGruX <> 0) and (Empresa <> 0) then
  if Empresa <> 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando saldos virtuais');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSdoVr2.RecordCount > 0;
    if Avisa then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifSdoVr2.RecordCount) +
        ' registros onde o saldo virtual do valor diverge  para o reduzido ' +
        Geral.FF0(GraGruX);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem);
        Geral.MB_Aviso(Mensagem);
      end;
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifSdoVr2, FmVSCustosDifSdoVr2, afmoLiberado) then
    begin
      FmVSCustosDifSdoVr2.FVersao := 2;
      FmVSCustosDifSdoVr2.CkTemIMEiMrt.Checked := Geral.IntToBool(TemIMEiMrt);
      FmVSCustosDifSdoVr2.EdGraGruX.ValueVariant := GraGruX;
      FmVSCustosDifSdoVr2.CBGraGruX.KeyValue := GraGruX;
      FmVSCustosDifSdoVr2.PnPesquisa.Visible := True; //ForcaMostrarForm;
      FmVSCustosDifSdoVr2.ShowModal;
      FmVSCustosDifSdoVr2.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando saldos virtuais');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSdoVr2.RecordCount > 0;
  end;
end;

function TDmModVS.CustosDiferentesSaldosVirtuais3(Empresa, GraGruX,
  TemIMEIMrt: Integer; MargemErro: Double; Avisa, ForcaMostrarForm,
  SelfCall: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
  procedure Reabre();
  var
    Corda, SQL_GGX, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    SQL_GGX := '';
    if GraGruX <> 0 then
      SQL_GGX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEIs de entrada');
    SQL_Flds := Geral.ATS([
    'Controle, MovimID MovimID_A, MovimNiv MovimNiv_A, Pallet, Pecas Pecas_A,  ',
    'PesoKg PesoKg_A, AreaM2 AreaM2_A,  ',
    'ValorT ValorT_A, SdoVrtPeca,  ',
    'SdoVrtPeso, SdoVrtArM2, ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg,   ',
    '0)) CustoUnit_A,  ',
    ' ',
    'IF(AreaM2 > 0, ValorT / AreaM2 * SdoVrtArM2,   ',
    '  IF(PesoKg > 0, ValorT / PesoKg * SdoVrtPeso,   ',
    '0)) SdoVrtValT_A  ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
    SQL_GGX,
    'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) ',
    'AND (NOT (MovimID IN (9,12,17,41))) ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _t05a_; ',
    'CREATE TABLE _t05a_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    '; ',
    'SELECT DISTINCT(Controle) Controle ',
    'FROM _t05a_; ',
    '']);
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.AbreMySQLQuery0(QrGroupConcat, DModG.MyPID_DB, [SQL]);
    //

    //Criar IMEI PAi para lozalizar processados?

    //PareAqui! 2023-05-11
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEIs de baixa');
    Corda := MyObjects.CordaDeQuery(QrGroupConcat, 'Controle', '');
    if Corda <> EmptyStr then
    begin
      SQL_Flds := Geral.ATS([
      'IF(VmiPai<>0, VmiPai, SrcNivel2) SrcVmi, SUM(Pecas) Pecas_B,  ',
      'SUM(PesoKg) PesoKg_B, SUM(AreaM2) AreaM2_B,  ',
      'IF(MovimID IN (26), SUM(ValorMP), SUM(ValorT)) ValorT_B ',
      '']);
      SQL_Left := '';
      SQL_Wher := Geral.ATS([
      'WHERE (IF(VmiPai<>0, VmiPai, SrcNivel2) IN ( ' + Corda + '))  ',
      '']);
      SQL_Group := 'GROUP BY SrcVmi ';
      SQL := Geral.ATS([
      'DROP TABLE IF EXISTS _t05b_; ',
      'CREATE TABLE _t05b_ ',
      VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
      VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
      '; ',
      ' ',
      'SELECT t05a.*, t05b.*, ',
      '(t05a.PesoKg_A - t05a.SdoVrtPeso) + t05b.PesoKg_B DifKg, ',
      '(t05a.Pecas_A - t05a.SdoVrtPeca) + t05b.Pecas_B DifPc, ',
      '(t05a.AreaM2_A - t05a.SdoVrtArM2) + t05b.AreaM2_B DifM2, ',
      '(t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B DifVal, ',
      '((t05a.ValorT_A - t05a.SdoVrtValT_A) + t05b.ValorT_B) / ValorT_A * 100000 FatorLimitante ',
      'FROM _t05a_ t05a ',
      'LEFT JOIN _t05b_ t05b ON t05b.SrcVmi=t05a.Controle ',
(*
      'WHERE ABS((t05a.ValorT_A - t05a.SdoVrtValT_A)  ',
      '  + t05b.ValorT_B) >= 0.01', // + Geral.FFT_Dot(MargemErro, 10, siNegativo),
*)
(*
      'WHERE ABS(ABS((t05a.ValorT_A - t05a.SdoVrtValT_A) ',
      '  -ABS( t05b.ValorT_B))) >= 0.01 ',
*)
      'WHERE ABS(ABS((t05a.ValorT_A - t05a.SdoVrtValT_A) ',
      '  -ABS( t05b.ValorT_B))) >= ' + Geral.FFT_Dot(MargemErro, 10, siNegativo),
      // ini 2023-04-15
      'OR ABS(PesoKg_A - SdoVrtPeso) <> ABS(PesoKg_B) ',
      'OR ABS(Pecas_A - SdoVrtPeca) <> ABS(Pecas_B) ',
      'OR ABS(AreaM2_A - SdoVrtArM2) <> ABS(AreaM2_B) ',
      // fim 2023-04-15
      ' ']);
      //Geral.MB_Teste(SQL);
      UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifSdoVr3, DModG.MyPID_DB, [SQL]);
      //
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  //Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Result := False;
  //if (GraGruX <> 0) and (Empresa <> 0) then
  if Empresa <> 0 then
  begin
    //FmVSCustosDifSdoVr3.DGDados.DataSource := DsCustosDifSdoVr3;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando saldos virtuais');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSdoVr3.RecordCount > 0;
    if Avisa then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifSdoVr3.RecordCount) +
        ' registros onde o saldo virtual do valor diverge  para o reduzido ' +
        Geral.FF0(GraGruX);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem);
        Geral.MB_Aviso(Mensagem);
      end;
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifSdoVr3, FmVSCustosDifSdoVr3, afmoLiberado) then
    begin
      FmVSCustosDifSdoVr3.FVersao := 3;
      FmVSCustosDifSdoVr3.DGDados.DataSource := DsCustosDifSdoVr3;
      FmVSCustosDifSdoVr3.CkTemIMEiMrt.Checked := Geral.IntToBool(TemIMEiMrt);
      FmVSCustosDifSdoVr3.EdGraGruX.ValueVariant := GraGruX;
      FmVSCustosDifSdoVr3.CBGraGruX.KeyValue := GraGruX;
      FmVSCustosDifSdoVr3.PnPesquisa.Visible := True; //ForcaMostrarForm;
      FmVSCustosDifSdoVr3.ShowModal;
      FmVSCustosDifSdoVr3.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando saldos virtuais');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifSdoVr3.RecordCount > 0;
  end;
end;

function TDmModVS.CustosDifPreRecl(Empresa, GraGruX: Integer;
  MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
  TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
  var
    SQL_GraGruX: String;

  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
    'Codigo, MovimCod, Controle, ValorT ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimID = 15 ',
    'AND MovimNiv = 12 ',
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_GraGruX,
    '; ',
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _fix_recl_15_12_; ',
    'CREATE TABLE _fix_recl_15_12_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de baixa');
    SQL_Flds := Geral.ATS([
    'DstNivel2, ABS(SUM(ValorT)) ValorT ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimID = 15 ',
    'AND MovimNiv = 11 ',
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_GraGruX,
    '']);
    SQL_Group := 'GROUP BY DstNivel2;';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _fix_recl_15_11_; ',
    'CREATE TABLE _fix_recl_15_11_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados pesquisados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifPreRecl, DModG.MyPID_DB, [
    //
    'SELECT dst.ID_TTW, src.DstNivel2, src.ValorT  ',
    'FROM _fix_recl_15_12_ dst ',
    'LEFT JOIN _fix_recl_15_11_ src ON src.DstNivel2=dst.Controle ',
    'WHERE dst.ValorT <> src.ValorT ',
    'ORDER BY DstNivel2; ',
    ' ',
    '']);
    //Geral.MB_Teste(QrCustosDifPreRecl.SQL.Text);
    //
    if QrCustosDifPreRecl.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrCustosDifPreRecl.RecordCount) +
      ' registros com custo divergente.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com custo divergente.')
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Result := False;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  //if (GraGruX <> 0) and (Empresa <> 0) then
  //if SelfCall = True then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifPreRecl.RecordCount > 0;
    if (Avisa = True) or (LaAviso1 <> nil) then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifPreRecl.RecordCount) +
          ' registros onde o custo diverge na pr� reclasse';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end else
      begin
        if LaAviso1 <> nil then
        begin
          Mensagem := 'N�o foram encontrados registros onde o custo diverge na pr� reclasse';
          if GraGruX <> 0 then
            Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
        end;
      end;
      if Mensagem <> '' then
      begin
        if LaAviso1 <> nil then
          MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
        else
          Geral.MB_Aviso(Mensagem);
      end;
    end;
  end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifPreRecl, FmVSCustosDifPreRecl, afmoLiberado) then
    begin
      FmVSCustosDifPreRecl.PnPesquisa.Visible := True;//ForcaMostrarForm;
      FmVSCustosDifPreRecl.EdGraGrux.ValueVariant := GraGruX;
      FmVSCustosDifPreRecl.CBGraGrux.KeyValue := GraGruX;
      FmVSCustosDifPreRecl.ShowModal;
      FmVSCustosDifPreRecl.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifPreRecl.RecordCount > 0;
  end;
end;

function TDmModVS.CustosDifReclasse(Empresa, GraGruX: Integer;
  MargemErro: Double; Avisa, ForcaMostrarForm, SelfCall: Boolean;
  TemIMEIMrt: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
  var
    SQL_GraGruX: String;

  procedure Reabre();
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL: String;
  begin
    if GraGruX <> 0 then
      SQL_GraGruX := 'AND DstGGX=' + Geral.FF0(GraGruX)
    else
      SQL_GraGruX := '';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
    'DataHora, MovimTwn MovimTwn_1,  ',
    'ValorT ValorT_1, AreaM2 AreaM2_1 ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimTwn>0 ',
    'AND MovimNiv=1 ',
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_GraGruX,
    '; ',
    '']);
    SQL_Group := ';';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _dif_custo_twn_1_; ',
    'CREATE TABLE _dif_custo_twn_1_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    if GraGruX <> 0 then
      SQL_GraGruX := 'AND GraGruX=' + Geral.FF0(GraGruX)
    else
      SQL_GraGruX := '';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de baixa');
    SQL_Flds := Geral.ATS([
    'MovimTwn MovimTwn_2, Controle Controle_2,  ',
    'ValorT ValorT_2, AreaM2 AreaM2_2 ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE MovimTwn>0 ',
    'AND MovimNiv=2 ',
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_GraGruX,
    '']);
    SQL_Group := ';';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _dif_custo_twn_2_; ',
    'CREATE TABLE _dif_custo_twn_2_ ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados pesquisados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCustosDifReclasse, DModG.MyPID_DB, [
    //
    'SELECT twn1.*, twn2.* ',
    'FROM _dif_custo_twn_1_ twn1 ',
    'LEFT JOIN _dif_custo_twn_2_ twn2 ON twn2.MovimTwn_2=twn1.MovimTwn_1 ',
    'WHERE ABS(twn1.ValorT_1) <> ABS(twn2.ValorT_2) ',
    '']);
    //Geral.MB_Teste(QrCustosDifReclasse.SQL.Text);
    //
    if QrCustosDifReclasse.RecordCount > 0 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, ' Foram encontrados ' +
      Geral.FF0(QrCustosDifReclasse.RecordCount) +
      ' registros com custo divergente.')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi encontrado nenhum registro com diferen�a com custo divergente.')
  end;
var
  Mensagem: String;
begin
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //////////////////////////
  Result := False;
  Exit;
  //////////////////////////
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  //if (GraGruX <> 0) and (Empresa <> 0) then
  //if SelfCall = True then
  //begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifReclasse.RecordCount > 0;
    if (Avisa = True) or (LaAviso1 <> nil) then
    begin
      if Result then
      begin
        Mensagem := 'Foram encontrados ' + Geral.FF0(QrCustosDifReclasse.RecordCount) +
          ' registros onde o custo diverge na pr� reclasse';
        if GraGruX <> 0 then
          Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
      end else
      begin
        if LaAviso1 <> nil then
        begin
          Mensagem := 'N�o foram encontrados registros onde o custo diverge na pr� reclasse';
          if GraGruX <> 0 then
            Mensagem := Mensagem + ' para o reduzido ' + Geral.FF0(GraGruX);
        end;
      end;
      if Mensagem <> '' then
      begin
        if LaAviso1 <> nil then
          MyObjects.Informa2(LaAviso1, LaAviso2, False, Mensagem)
        else
          Geral.MB_Aviso(Mensagem);
      end;
    end;
  //end;
  if (Result or ForcaMostrarForm) and (SelfCall = False) then
  begin
    if DBCheck.CriaFm(TFmVSCustosDifReclasse, FmVSCustosDifReclasse, afmoLiberado) then
    begin
      FmVSCustosDifReclasse.PnPesquisa.Visible := True;//ForcaMostrarForm;
      FmVSCustosDifReclasse.EdGraGrux.ValueVariant := GraGruX;
      FmVSCustosDifReclasse.CBGraGrux.KeyValue := GraGruX;
      FmVSCustosDifReclasse.ShowModal;
      FmVSCustosDifReclasse.Destroy;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando custo de origem e destino');
    Reabre();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Result := QrCustosDifReclasse.RecordCount > 0;
  end;
end;

function TDmModVS.PreparaPalletParaReclassificar(const SQLType: TSQLType; const
  CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo, MovimCod,
  Controle, Pallet, GraGruX, GraGruY: Integer; DtHr: TDateTime; StqCenLoc:
  Integer; IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := DmModVS_CRC.PreparaPalletParaReclassificar(SQLType,
  CkReclasse_Checked, Empresa, ClientMO, Codigo, MovimCod,
  Controle, Pallet, GraGruX, GraGruY, DtHr, StqCenLoc,
  IuvpeiInn, IuvpeiBxa);
end;

procedure TDmModVS.PreparaPalletParaReclass_InsereBaixaAtual(Codigo,
  MovimCod: Integer; DataHora: String; NewVMI: Integer; SQLType: TSQLType);
begin
  DmModVS.PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod,
  DataHora, NewVMI, SQLType);
end;

function TDmModVS.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
begin
  Result := DmModVS_CRC.AtualizaVSMovIts_CusEmit(Controle);
end;

end.
