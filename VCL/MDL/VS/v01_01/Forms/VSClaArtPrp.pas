unit VSClaArtPrp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, Vcl.Mask, UnProjGroup_Vars, UnProjGroup_Consts,
  UnDmkProcFunc;

type
  TFmVSClaArtPrp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    PnPartida: TPanel;
    QrVSPallet1: TmySQLQuery;
    QrVSPallet1Codigo: TIntegerField;
    QrVSPallet1Nome: TWideStringField;
    QrVSPallet1Lk: TIntegerField;
    QrVSPallet1DataCad: TDateField;
    QrVSPallet1DataAlt: TDateField;
    QrVSPallet1UserCad: TIntegerField;
    QrVSPallet1UserAlt: TIntegerField;
    QrVSPallet1AlterWeb: TSmallintField;
    QrVSPallet1Ativo: TSmallintField;
    QrVSPallet1Empresa: TIntegerField;
    QrVSPallet1NO_EMPRESA: TWideStringField;
    QrVSPallet1Status: TIntegerField;
    QrVSPallet1CliStat: TIntegerField;
    QrVSPallet1GraGruX: TIntegerField;
    QrVSPallet1NO_CLISTAT: TWideStringField;
    QrVSPallet1NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet1NO_STATUS: TWideStringField;
    DsVSPallet1: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    DsVSGerArtNew: TDataSource;
    PnPallets: TPanel;
    GBTecla: TGroupBox;
    PnTecla: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPallet1: TdmkEditCB;
    CBPallet1: TdmkDBLookupComboBox;
    LaVSRibCla: TLabel;
    SbPallet1: TSpeedButton;
    PnTecla1: TPanel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    SBPallet2: TSpeedButton;
    EdPallet2: TdmkEditCB;
    CBPallet2: TdmkDBLookupComboBox;
    PnTecla2: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrVSPallet2: TmySQLQuery;
    DsVSPallet2: TDataSource;
    QrVSPallet2Codigo: TIntegerField;
    QrVSPallet2Nome: TWideStringField;
    QrVSPallet2Lk: TIntegerField;
    QrVSPallet2DataCad: TDateField;
    QrVSPallet2DataAlt: TDateField;
    QrVSPallet2UserCad: TIntegerField;
    QrVSPallet2UserAlt: TIntegerField;
    QrVSPallet2AlterWeb: TSmallintField;
    QrVSPallet2Ativo: TSmallintField;
    QrVSPallet2Empresa: TIntegerField;
    QrVSPallet2NO_EMPRESA: TWideStringField;
    QrVSPallet2Status: TIntegerField;
    QrVSPallet2CliStat: TIntegerField;
    QrVSPallet2GraGruX: TIntegerField;
    QrVSPallet2NO_CLISTAT: TWideStringField;
    QrVSPallet2NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet2NO_STATUS: TWideStringField;
    Panel9: TPanel;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Panel12: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    SBPallet3: TSpeedButton;
    EdPallet3: TdmkEditCB;
    CBPallet3: TdmkDBLookupComboBox;
    PnTecla3: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Panel14: TPanel;
    GroupBox4: TGroupBox;
    Panel15: TPanel;
    Panel16: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    SBPallet4: TSpeedButton;
    EdPallet4: TdmkEditCB;
    CBPallet4: TdmkDBLookupComboBox;
    PnTecla4: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel18: TPanel;
    GroupBox5: TGroupBox;
    Panel19: TPanel;
    Panel20: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    SBPallet5: TSpeedButton;
    EdPallet5: TdmkEditCB;
    CBPallet5: TdmkDBLookupComboBox;
    PnTecla5: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Panel22: TPanel;
    GroupBox6: TGroupBox;
    Panel23: TPanel;
    Panel24: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    SBPallet6: TSpeedButton;
    EdPallet6: TdmkEditCB;
    CBPallet6: TdmkDBLookupComboBox;
    PnTecla6: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Panel26: TPanel;
    QrVSPallet3: TmySQLQuery;
    DsVSPallet3: TDataSource;
    QrVSPallet4: TmySQLQuery;
    DsVSPallet4: TDataSource;
    QrVSPallet5: TmySQLQuery;
    DsVSPallet5: TDataSource;
    QrVSPallet6: TmySQLQuery;
    DsVSPallet6: TDataSource;
    QrVSPallet3Codigo: TIntegerField;
    QrVSPallet3Nome: TWideStringField;
    QrVSPallet3Lk: TIntegerField;
    QrVSPallet3DataCad: TDateField;
    QrVSPallet3DataAlt: TDateField;
    QrVSPallet3UserCad: TIntegerField;
    QrVSPallet3UserAlt: TIntegerField;
    QrVSPallet3AlterWeb: TSmallintField;
    QrVSPallet3Ativo: TSmallintField;
    QrVSPallet3Empresa: TIntegerField;
    QrVSPallet3NO_EMPRESA: TWideStringField;
    QrVSPallet3Status: TIntegerField;
    QrVSPallet3CliStat: TIntegerField;
    QrVSPallet3GraGruX: TIntegerField;
    QrVSPallet3NO_CLISTAT: TWideStringField;
    QrVSPallet3NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet3NO_STATUS: TWideStringField;
    QrVSPallet4Codigo: TIntegerField;
    QrVSPallet4Nome: TWideStringField;
    QrVSPallet4Lk: TIntegerField;
    QrVSPallet4DataCad: TDateField;
    QrVSPallet4DataAlt: TDateField;
    QrVSPallet4UserCad: TIntegerField;
    QrVSPallet4UserAlt: TIntegerField;
    QrVSPallet4AlterWeb: TSmallintField;
    QrVSPallet4Ativo: TSmallintField;
    QrVSPallet4Empresa: TIntegerField;
    QrVSPallet4NO_EMPRESA: TWideStringField;
    QrVSPallet4Status: TIntegerField;
    QrVSPallet4CliStat: TIntegerField;
    QrVSPallet4GraGruX: TIntegerField;
    QrVSPallet4NO_CLISTAT: TWideStringField;
    QrVSPallet4NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet4NO_STATUS: TWideStringField;
    QrVSPallet5Codigo: TIntegerField;
    QrVSPallet5Nome: TWideStringField;
    QrVSPallet5Lk: TIntegerField;
    QrVSPallet5DataCad: TDateField;
    QrVSPallet5DataAlt: TDateField;
    QrVSPallet5UserCad: TIntegerField;
    QrVSPallet5UserAlt: TIntegerField;
    QrVSPallet5AlterWeb: TSmallintField;
    QrVSPallet5Ativo: TSmallintField;
    QrVSPallet5Empresa: TIntegerField;
    QrVSPallet5NO_EMPRESA: TWideStringField;
    QrVSPallet5Status: TIntegerField;
    QrVSPallet5CliStat: TIntegerField;
    QrVSPallet5GraGruX: TIntegerField;
    QrVSPallet5NO_CLISTAT: TWideStringField;
    QrVSPallet5NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet5NO_STATUS: TWideStringField;
    QrVSPallet6Codigo: TIntegerField;
    QrVSPallet6Nome: TWideStringField;
    QrVSPallet6Lk: TIntegerField;
    QrVSPallet6DataCad: TDateField;
    QrVSPallet6DataAlt: TDateField;
    QrVSPallet6UserCad: TIntegerField;
    QrVSPallet6UserAlt: TIntegerField;
    QrVSPallet6AlterWeb: TSmallintField;
    QrVSPallet6Ativo: TSmallintField;
    QrVSPallet6Empresa: TIntegerField;
    QrVSPallet6NO_EMPRESA: TWideStringField;
    QrVSPallet6Status: TIntegerField;
    QrVSPallet6CliStat: TIntegerField;
    QrVSPallet6GraGruX: TIntegerField;
    QrVSPallet6NO_CLISTAT: TWideStringField;
    QrVSPallet6NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet6NO_STATUS: TWideStringField;
    Panel13: TPanel;
    LaVSRibCad: TLabel;
    EdIMEI: TdmkEditCB;
    CBIMEI: TdmkDBLookupComboBox;
    SbIMEI: TSpeedButton;
    Panel17: TPanel;
    EdCodigo: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    EdMovimCod: TdmkEdit;
    EdCacCod: TdmkEdit;
    Label32: TLabel;
    QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    BtReclasif: TBitBtn;
    Label33: TLabel;
    EdDVIMEI: TdmkEdit;
    Label34: TLabel;
    MeLeitura: TMemo;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure SbIMEIClick(Sender: TObject);
    procedure EdPallet1Change(Sender: TObject);
    procedure EdPallet2Change(Sender: TObject);
    procedure SBPallet2Click(Sender: TObject);
    procedure SBPallet3Click(Sender: TObject);
    procedure SBPallet4Click(Sender: TObject);
    procedure SBPallet5Click(Sender: TObject);
    procedure SBPallet6Click(Sender: TObject);
    procedure EdPallet3Change(Sender: TObject);
    procedure EdPallet4Change(Sender: TObject);
    procedure EdPallet5Change(Sender: TObject);
    procedure EdPallet6Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdIMEIRedefinido(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure MeLeituraChange(Sender: TObject);
    procedure EdIMEIChange(Sender: TObject);
  private
    { Private declarations }
    procedure CadastraPallet(EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
    procedure LiberaConfig();
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery);
    procedure ReopenVSGerArtDst(Controle: Integer);
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSClaArtPrp: TFmVSClaArtPrp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, VSPalletAdd, MyDBCheck, ModuleGeral,
MyListas, UMySQLModule, UnVS_PF;

{$R *.DFM}

procedure TFmVSClaArtPrp.BtOKClick(Sender: TObject);
var
  Empresa, Fornecedor, Codigo, MovimCod, VSGerArt, VsMovIts, I, Controle,
  LstPal01, LstPal02, LstPal03, LstPal04, LstPal05, LstPal06, BxaGraGruX,
  GraGruX1, CtrlSorc1, CtrlDest1, GraGruX2, CtrlSorc2, CtrlDest2,
  GraGruX3, CtrlSorc3, CtrlDest3, GraGruX4, CtrlSorc4, CtrlDest4,
  GraGruX5, CtrlSorc5, CtrlDest5, GraGruX6, CtrlSorc6, CtrlDest6,
  BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, CacCod,
  VSMulFrnCab: Integer;
  DtHrIni, DtHrCfgCla: String;
  //
begin
  if MyObjects.FIC(EdIMEI.ValueVariant = 0, EdIMEI, 'IME-I inv�lido!') then
    Exit;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  CacCod         := EdCacCod.ValueVariant;
  VSGerArt       := QrVSGerArtNewCodigo.Value;
  VSMovIts       := QrVSGerArtNewControle.Value;
  Empresa        := QrVSGerArtNewEmpresa.Value;
  Fornecedor     := QrVSGerArtNewTerceiro.Value;
  VSMulFrnCab    := QrVSGerArtNewVSMulFrnCab.Value;
  if MyObjects.FIC(EdIMEI.ValueVariant <> VSMovIts, EdIMEI, 'Processo corrompido!' +
  'Feche a janela e tente novamente!') then
    Exit;
  case TEstqMovimID(QrVSGerArtNewMovimID.Value) of
    (*6*)emidIndsXX: FMovimID := emidClassArtXXUni;
    (*7*)emidClassArtXXUni,
    (*8*)emidReclasXXUni: FMovimID := emidReclasXXUni;
    else
    begin
      Geral.MB_Aviso('"MovimID" n�o implementado em "FmVSClaArtPrp.BtOKClick()"');
      Exit;
    end;
  end;
  // Nao permitir duas vezes o mesmo pallet
  LstPal01 := EdPallet1.ValueVariant;
  LstPal02 := EdPallet2.ValueVariant;
  LstPal03 := EdPallet3.ValueVariant;
  LstPal04 := EdPallet4.ValueVariant;
  LstPal05 := EdPallet5.ValueVariant;
  LstPal06 := EdPallet6.ValueVariant;
  //
  GraGruX1 := QrVSPallet1GraGruX.Value;
  GraGruX2 := QrVSPallet2GraGruX.Value;
  GraGruX3 := QrVSPallet3GraGruX.Value;
  GraGruX4 := QrVSPallet4GraGruX.Value;
  GraGruX5 := QrVSPallet5GraGruX.Value;
  GraGruX6 := QrVSPallet6GraGruX.Value;
  //
  if VS_PF.PalletDuplicado(VAR_CLA_ART_RIB_MAX_BOX,
    LstPal01, LstPal02, LstPal03, LstPal04, LstPal05, LstPal06) then
      Exit;
  //
  // Ver se selecionou pelo menos um pallet!
  if MyObjects.FIC((LstPal01 = 0) and (LstPal02 = 0) and (LstPal03 = 0)
  and (LstPal04 = 0) and (LstPal05 = 0) and (LstPal06 = 0), nil,
  'Informe pelo menos um pallet!') then
     Exit;
  //
  //
  //Veificar se a empresa dos pallets selecionados eh a mesma do pallet a classificar!
  if MyObjects.FIC((LstPal01 <> 0) and (QrVSPallet1Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 1!') then Exit;
  if MyObjects.FIC((LstPal02 <> 0) and (QrVSPallet2Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 2!') then Exit;
  if MyObjects.FIC((LstPal03 <> 0) and (QrVSPallet3Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 3!') then Exit;
  if MyObjects.FIC((LstPal04 <> 0) and (QrVSPallet4Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 4!') then Exit;
  if MyObjects.FIC((LstPal05 <> 0) and (QrVSPallet5Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 5!') then Exit;
  if MyObjects.FIC((LstPal06 <> 0) and (QrVSPallet6Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 6!') then Exit;
  //
  DtHrCfgCla     := Geral.FDT(DModG.ObtemAgora(), 109);
  CacCod :=
    UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, CacCod);
  MovimCod :=
    UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Codigo :=
    UMyMod.BPGS1I32('vspaclacaba', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  if ImgTipo.SQLType = stIns then
    VS_PF.InsereVSCacCab(CacCod, emidClassArtXXUni, Codigo);
  //
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vspaclacaba', False, [
  'VSGerArt', 'VSMovIts', 'CacCod',
  'LstPal01', 'LstPal02', 'LstPal03',
  'LstPal04', 'LstPal05', 'LstPal06'], [
  'Codigo'], [
  VSGerArt, VSMovIts, CacCod,
  LstPal01, LstPal02, LstPal03,
  LstPal04, LstPal05, LstPal06], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidClassArtXXUni, Codigo);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    'DtHrCfgCla', 'CacCod'
    ], ['Codigo'], [
    DtHrCfgCla, CacCod
    ], [VSGerArt], True);
    //
    BxaGraGruX   := QrVSGerArtNewGraGruX.Value;
    BxaMovimNiv  := QrVSGerArtNewMovimNiv.Value;
    BxaSrcNivel1 := QrVSGerArtNewCodigo.Value;
    BxaSrcNivel2 := QrVSGerArtNewControle.Value;
    BxaMovimID   := QrVSGerArtNewMovimID.Value;
    //
    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      1, LstPal01, GraGruX1, LaAviso1, LaAviso2, CtrlSorc1, CtrlDest1);

    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      2, LstPal02, GraGruX2, LaAviso1, LaAviso2, CtrlSorc2, CtrlDest2);

    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      3, LstPal03, GraGruX3, LaAviso1, LaAviso2, CtrlSorc3, CtrlDest3);

    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      4, LstPal04, GraGruX4, LaAviso1, LaAviso2, CtrlSorc4, CtrlDest4);

    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      5, LstPal05, GraGruX5, LaAviso1, LaAviso2, CtrlSorc5, CtrlDest5);

    VS_PF.InsAltVSPalCla(Empresa, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
      6, LstPal06, GraGruX6, LaAviso1, LaAviso2, CtrlSorc6, CtrlDest6);

    //
    FCodigo := Codigo;
    FCacCod := CacCod;
    Close;
  end;
end;

procedure TFmVSClaArtPrp.BtReclasifClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
  ReopenVSGerArtDst(0);
end;

procedure TFmVSClaArtPrp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSClaArtPrp.CadastraPallet(EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
const
  GraGruX = 0;
begin
  VS_PF.CadastraPalletRibCla(QrVSGerArtNewEmpresa.Value, EdPallet, CBPallet,
    QrVSPallet, emidClassArtXXUni, GraGruX);
end;

procedure TFmVSClaArtPrp.EdIMEIChange(Sender: TObject);
begin
(*
  if MeLeitura.Focused then
  begin
    EdIMEI.SetFocus;
    CBIMEI.SetFocus;
    if (EdIMEI.ValueVariant <> 0) and
    (QrVSGerArtNew.Locate('Controle', EdIMEI.ValueVariant, [])) then
    begin
      if SbIMEI.Enabled then
      begin
        SbIMEIClick(Self);
        if PnPallets.Visible then
        begin
          EdPallet1.SetFocus;
        end;
      end;
    end else
  end;
*)
end;

procedure TFmVSClaArtPrp.EdIMEIRedefinido(Sender: TObject);
begin
  SbIMEI.Enabled := EdIMEI.ValueVariant > 0;
end;

procedure TFmVSClaArtPrp.EdPallet1Change(Sender: TObject);
begin
  PnTecla1.Visible := EdPallet1.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.EdPallet2Change(Sender: TObject);
begin
  PnTecla2.Visible := EdPallet2.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.EdPallet3Change(Sender: TObject);
begin
  PnTecla3.Visible := EdPallet3.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.EdPallet4Change(Sender: TObject);
begin
  PnTecla4.Visible := EdPallet4.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.EdPallet5Change(Sender: TObject);
begin
  PnTecla5.Visible := EdPallet5.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.EdPallet6Change(Sender: TObject);
begin
  PnTecla6.Visible := EdPallet6.ValueVariant <> 0;
end;

procedure TFmVSClaArtPrp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClaArtPrp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  //FMovimID := emidReclasVS;
  //
  //CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  ReopenVSGerArtDst(0);
end;

procedure TFmVSClaArtPrp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClaArtPrp.LiberaConfig();
begin
  if (EdIMEI.ValueVariant <> 0) and
  (QrVSGerArtNew.Locate('Controle', EdIMEI.ValueVariant, [])) then
  begin
    if SbIMEI.Enabled then
    begin
      SbIMEIClick(Self);
      if PnPallets.Visible then
      begin
        EdPallet1.SetFocus;
      end;
    end;
  end;
end;

procedure TFmVSClaArtPrp.MeLeituraChange(Sender: TObject);
var
  Leitura: String;
begin
  if MeLeitura.Lines.Count > 1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Processando dados da leitura');
      Leitura := MeLeitura.Lines[0];
      while Length(Leitura) < 13 do
      begin
        Leitura := '0' + Leitura;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, Leitura);
      MeLeitura.Text := Leitura;
      //
      EdIMEI.ValueVariant := 0;
      //
      if DmkPF.CheckSumEAN13(Leitura, True) then
      begin
        EdDVIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 1, 3));
        EdIMEI.ValueVariant := Geral.IMV(Copy(Leitura, 4, 9));
        //
        LiberaConfig();
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVSClaArtPrp.ReopenVSGerArtDst(Controle: Integer);
const
  Ficha = 0;
  SoNaoZerados = True;
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(wmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(wmi.Ficha=0, "V�rias", wmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(wmi.Controle, " ", gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  'IF(wmi.Ficha=0, "", CONCAT(" - S�rie / Ficha RMP ", wmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM v s m o v i t s wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'WHERE wmi.MovimNiv=' + Geral.FF0(Integer(eminDestInds)),
  // Aqui v� se j� foi liberado para configurar e ainda n�o foi configurado!
  'AND wmi.MovimCod IN ( ',
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrLibCla > "1900-01-01" ',
  '  AND DtHrCfgCla < "1900-01-01" ',
  ') ',
  'ORDER BY NO_PRD_TAM_COR, wmi.Codigo ',
  '']);
  QrVSGerArtNew.Locate('Controle', Controle, []);
*)
  VS_PF.ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew, Ficha, Controle,
  SoNaoZerados);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSClaArtPrp.ReopenVSPallet(QrVSPallet: TmySQLQuery);
begin
  VS_PF.ReopenVSPallet(QrVSPallet, QrVSGerArtNewEmpresa.Value, 0, '', []);
end;

procedure TFmVSClaArtPrp.SbPallet1Click(Sender: TObject);
begin
  CadastraPallet(EdPallet1, CBPallet1, QrVSPallet1);
end;

procedure TFmVSClaArtPrp.SBPallet2Click(Sender: TObject);
begin
  CadastraPallet(EdPallet2, CBPallet2, QrVSPallet2);
end;

procedure TFmVSClaArtPrp.SBPallet3Click(Sender: TObject);
begin
  CadastraPallet(EdPallet3, CBPallet3, QrVSPallet3);
end;

procedure TFmVSClaArtPrp.SBPallet4Click(Sender: TObject);
begin
  CadastraPallet(EdPallet4, CBPallet4, QrVSPallet4);
end;

procedure TFmVSClaArtPrp.SBPallet5Click(Sender: TObject);
begin
  CadastraPallet(EdPallet5, CBPallet5, QrVSPallet5);
end;

procedure TFmVSClaArtPrp.SBPallet6Click(Sender: TObject);
begin
  CadastraPallet(EdPallet6, CBPallet6, QrVSPallet6);
end;

procedure TFmVSClaArtPrp.SbIMEIClick(Sender: TObject);
begin
  if MyObjects.FIC(EdIMEI.ValueVariant = 0, EdIMEI,
  'Informe o pallet de artigo de ribeira!') then
    Exit
  else
  begin
    if DmkPF.DigitoVerificardorDmk3CasasInteger(EdIMEI.ValueVariant) = EdDVIMEI.ValueVariant then
    begin
      PnPartida.Enabled := False;
      PnPallets.Visible := True;
      BtOK.Enabled      := True;
      //
      ReopenVSPallet(QrVSPallet1);
      ReopenVSPallet(QrVSPallet2);
      ReopenVSPallet(QrVSPallet3);
      ReopenVSPallet(QrVSPallet4);
      ReopenVSPallet(QrVSPallet5);
      ReopenVSPallet(QrVSPallet6);
    end else
      Geral.MB_Aviso('DV do IME-I inv�lido!');
  end;
end;

end.
