unit VSAjsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts,
  UnProjGroup_Vars, Vcl.Menus, UnAppEnums;

type
  TFmVSAjsIts = class(TForm)
    GroupBox1: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    QrVSPallet: TmySQLQuery;
    DsVSPallet: TDataSource;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdMovimCod: TdmkDBEdit;
    Label3: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Panel5: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrSrcNivel2: TmySQLQuery;
    DsSrcNivel2: TDataSource;
    QrSrcNivel2NO_PRD_TAM_COR: TWideStringField;
    QrSrcNivel2Controle: TIntegerField;
    QrSrcNivel2Codigo: TIntegerField;
    QrSrcNivel2MovimID: TIntegerField;
    QrSrcNivel2GraGruX: TIntegerField;
    QrSrcNivel2MovimNiv: TIntegerField;
    Panel6: TPanel;
    PnIMEIOrigem: TPanel;
    RGMovimentacao: TRadioGroup;
    Label18: TLabel;
    EdNivel2: TdmkEditCB;
    CBNivel2: TdmkDBLookupComboBox;
    Panel7: TPanel;
    GBDados: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label7: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label9: TLabel;
    Label16: TLabel;
    SBNewPallet: TSpeedButton;
    Label17: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    EdValorT: TdmkEdit;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdValorMP: TdmkEdit;
    EdCustoMOKg: TdmkEdit;
    EdCustoMOTot: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    EdFicha: TdmkEdit;
    EdMarca: TdmkEdit;
    EdObserv: TdmkEdit;
    EdCustoMOM2: TdmkEdit;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    SbImei: TSpeedButton;
    PMImei: TPopupMenu;
    FichaRMP1: TMenuItem;
    Pallet1: TMenuItem;
    IMEI1: TMenuItem;
    Panel8: TPanel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    Label19: TLabel;
    EdIxxLinha: TdmkEdit;
    Label20: TLabel;
    SBNewByInfo: TSpeedButton;
    Panel1: TPanel;
    RGModoContinuarInserindo: TRadioGroup;
    Panel9: TPanel;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdValorMPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure RGMovimentacaoClick(Sender: TObject);
    procedure SbImeiClick(Sender: TObject);
    procedure FichaRMP1Click(Sender: TObject);
    procedure Pallet1Click(Sender: TObject);
    procedure IMEI1Click(Sender: TObject);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure SBNewByInfoClick(Sender: TObject);
  private
    { Private declarations }
    FForcaIMEI, FUltGGX: Integer;
    //
    procedure ReopenSrcNivel2();
    procedure ReopenVSPallet();
    procedure ReopenVSAjsIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();
    procedure PreencheDadosIMEI(IMEI: Integer);

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO: Integer;
    FPreco: Double;
  end;

  var
  FmVSAjsIts: TFmVSAjsIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSAjsCab, Principal, UnVS_PF, ModuleGeral, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmVSAjsIts.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0;
  JmpNivel2: Integer = 0;
  JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0;
  RmsNivel2: Integer = 0;
  RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste;
  GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Terceiro, Ficha,
  GraGruY, FornecMO, SerieFch, StqCenLoc, ReqMovEstq, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdPalletX, EdAreaM2X: TdmkEdit;
  PodePallet: Boolean;
  //
  SrcMovID, DstMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, DstNivel1, DstNivel2, SrcGGX, DstGGX, SrcMoviNiv,
  DstMoviNiv: Integer;
  MyEdFicha: TdmkEdit;
  Qry: TmySQLQuery;
  EstqMovWay: TEstqMovWay;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClienteMO      := FClientMO;
  ClientMO       := FClientMO;
  Terceiro       := EdFornecedor.ValueVariant;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidInventario;
  MovimNiv       := eminSemNiv;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Ficha          := EdFicha.ValueVariant;
  GraGruY        := QrGraGruXGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  SerieFch       := EdSerieFch.ValueVariant;
  Marca          := EdMarca.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  //
  case GraGruY of
    CO_GraGruY_0512_VSSubPrd,
    CO_GraGruY_0683_VSPSPPro,
    CO_GraGruY_0853_VSPSPEnd,
    CO_GraGruY_1024_VSNatCad,
    CO_GraGruY_1072_VSNatInC,
    CO_GraGruY_1088_VSNatCon,
    CO_GraGruY_2048_VSRibCad,
    CO_GraGruY_7168_VSRepMer:
    begin
      EdPalletX := nil;
      EdAreaM2X := nil;
    end;
    CO_GraGruY_3072_VSRibCla:
    begin
      EdPalletX := EdPallet;
      EdAreaM2X := EdAreaM2;
    end;
    CO_GraGruY_4096_VSRibOpe,
    CO_GraGruY_5120_VSWetEnd,
    CO_GraGruY_6144_VSFinCla:
    begin
      EdPalletX := nil;
      EdAreaM2X := EdAreaM2;
    end;
    else
    begin
      Geral.MB_Erro('"GraGruY" n�o implementado em "FmVSAjsIts.BtOKClick()"');
    end;
  end;
  //
  if RGMovimentacao.ItemIndex <> 1 then
  begin
    MyEdFicha := EdFicha;
    if VS_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha, True) then
      Exit;
  end else
    MyEdFicha := nil;
  //
  //
  if Pallet <> 0 then
  begin
    PodePallet :=
      (QrGraGruXGraGruY.Value = CO_GraGruY_6144_VSFinCla)
    or
      (QrGraGruXGraGruY.Value = CO_GraGruY_3072_VSRibCla);
    //
    if MyObjects.FIC(not PodePallet, nil,
    'Somente artigo classificado ou acabado permite informa��o de pallet!') then
      Exit;
    //
(*
    if MyObjects.FIC(QrGraGruXGraGruY.Value <> CO_GraGruY_3072_VSRibCla, nil,
    'Somente artigo classificado permite informa��o de pallet!') then
      Exit;
*)
    //
    if MyObjects.FIC(GraGruX <> QrVSPalletGraGruX.Value, EdPallet,
    'Artigo do pallet selecionado difere do artigo selecionado!') then
      Exit;
  end;
  //
  if MyObjects.FIC(FornecMO = 0, EdFornecMO,
  'Informe o fornecedor da m�o de obra!') then
    Exit;
  //
  if RGMovimentacao.ItemIndex = 1 then
  begin
    if Pecas >=0 then
      if Geral.MB_Pergunta(
      'Foi informado "Movimenta��o = Baixa", mas a quantidade de pe�as n�o � negativa.'
      + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;
  if VS_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  if MyObjects.FIC_MCI(CkContinuar, RGModoContinuarInserindo) then
    Exit;
  //
  if VS_PF.VSFic(GraGruX, Empresa, ClienteMO, Terceiro, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPalletX, MyEdFicha, EdPecas, EdAreaM2X,
    EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  //
  SrcMovID   := TEstqMovimID(0);
  SrcNivel1  := 0;
  SrcNivel2  := 0;
  SrcGGX     := 0;
  SrcMoviNiv := 0;
  //
  DstMovID   := TEstqMovimID(0);
  DstNivel1  := 0;
  DstNivel2  := 0;
  DstGGX     := 0;
  DstMoviNiv := 0;
  //if QrSrcNivel2.State <> dsInactive then
  if PnIMEIOrigem.Visible then
  begin
    if MyObjects.FIC(EdNivel2.ValueVariant = 0, EdNivel2, 'Informe o IME-I de baixa!') then
      Exit;
    EstqMovWay := TEstqMovWay.emwyNon;
    case TEstqMovimID(QrSrcNivel2MovimID.Value) of
      (*00*)emidAjuste,
      (*01*)emidCompra,
      (*02*)emidVenda,
      (*03*)emidReclasWE,
      (*04*)emidBaixa,
      (*05*)emidIndsWE,
      (*06*)emidIndsXX,
      (*07*)emidClassArtXXUni,
      (*08*)emidReclasXXUni,
      (*09*)emidForcado,
      (*10*)emidSemOrigem,
      (*12*)emidResiduoReclas,
      (*13*)emidInventario,
      (*14*)emidClassArtXXMul,
      (*15*)emidPreReclasse,
      (*16*)emidEntradaPlC,
      (*17*)emidExtraBxa,
      (*18*)emidSaldoAnterior,
      (*20*)emidFinished,
      (*21*)emidDevolucao,
      (*22*)emidRetrabalho,
      (*23*)emidGeraSubProd,
      (*24*)emidReclasXXMul,
      (*25*)emidTransfLoc,
      (*28*)emidDesclasse,
      (*29*)emidCaleado,
      (*30*)emidEmRibPDA,
      (*31*)emidEmRibDTA,
      (*40*)emidConservado(*,
      (*42*)(*emidSPCaleirad*): EstqMovWay := TEstqMovWay.emwySrc;
      //
      (*11*)emidEmOperacao,
      (*19*)emidEmProcWE,
      (*26*)emidEmProcCal,
      (*27*)emidEmProcCur,
      (*32*)emidEmProcSP,
      (*33*)emidEmReprRM,
      (*39*)emidEmProcCon: EstqMovWay := TEstqMovWay.emwyDst;
      //
      else
      begin
        Geral.MB_Erro(
          '"EstqMovWay" indefinido em "OK de item ajuste". Avise a Dermatek!');
        Exit;
      end;
    end;
    case EstqMovWay of
      TEstqMovWay.emwySrc:
      begin
        SrcNivel2  := EdNivel2.ValueVariant;
        //
        SrcMovID   := TEstqMovimID(QrSrcNivel2MovimID.Value);
        SrcNivel1  := QrSrcNivel2Codigo.Value;
        SrcGGX     := QrSrcNivel2GraGruX.Value;
        SrcMoviNiv := QrSrcNivel2MovimNiv.Value;
      end;
      TEstqMovWay.emwyDst:
      begin
        SrcNivel2  := EdNivel2.ValueVariant;
        //
        SrcMovID   := TEstqMovimID(QrSrcNivel2MovimID.Value);
        SrcNivel1  := QrSrcNivel2Codigo.Value;
        SrcGGX     := QrSrcNivel2GraGruX.Value;
        SrcMoviNiv := QrSrcNivel2MovimNiv.Value;
(*
        DstNivel2  := EdNivel2.ValueVariant;
        //
        DstMovID   := TEstqMovimID(QrSrcNivel2MovimID.Value);
        DstNivel1  := QrSrcNivel2Codigo.Value;
        DstGGX     := QrSrcNivel2GraGruX.Value;
        DstMoviNiv := QrSrcNivel2MovimNiv.Value;
*)
      end;
    end;
  end;
(*
  if SrcNivel2 <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT vmi.Controle ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
      'WHERE vmi.Controle=' + Geral.FF0(SrcNivel2),
      'AND (NOT (MovimNiv IN (' + CO_CODS_NIV_NO_BXA_AJS + ')) ',
      'AND ( ',
      ' (vmi.Pecas>=0  AND vmi.SdoVrtPeca>0 )  ',
      '  OR ',
      '  (vmi.PesoKg>0 AND vmi.SdoVrtPeso>0 ) ',
      ') ',
      ') ',
      '']);
      //Geral.MB_SQL(Self, Qry);
      if Qry.RecordCount = 0 then
      begin
        if Geral.MB_Pergunta(
        'Talvez o IME-I de baixa selecionado n�o tenha seu saldo modificado!' +
        sLineBreak + 'Deseja continuar assim mesmo?') <> ID_Yes then
        Exit;
      end;
    finally
      Qry.Free;
    end;
  end;
*)
////////////////////////////////////////////////////////////////////////////////
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei055(*Ajuste de estoque*)) then
  begin
    SBNewPallet.Enabled := True;
    FForcaIMEI := 0;
    VS_PF.AtualizaTotaisVSXxxCab('vsajscab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    FmVSAjsCab.LocCod(Codigo, Codigo);
    FmVSAjsCab.ReopenVSAjsIts(Controle);
    if SrcNivel2 <> 0 then
      VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(SrcNivel2, Integer(SrcMovID),
      SrcMoviNiv);
    if DstNivel2 <> 0 then
      VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(DstNivel2, Integer(DstMovID),
      DstMoviNiv);

    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      if not VS_PF.ConfigContinuarInserindoFolhaLinha(
        RGModoContinuarInserindo, EdIxxFolha, EdIxxLinha) then
      begin
        EdGraGruX.ValueVariant     := 0;
        CBGraGruX.KeyValue         := Null;
        EdFicha.ValueVariant       := 0;
        EdPallet.ValueVariant      := 0;
        CBPallet.KeyValue          := Null;
        RGIxxMovIX.ItemIndex       := 0;
        EdIxxFolha.ValueVariant    := 0;
        EdIxxLinha.ValueVariant    := 0;
      end;
(*
      EdFornecedor.ValueVariant  := 0;
      CBFornecedor.KeyValue      := Null;
      EdSerieFch.ValueVariant    := 0;
      CBSerieFch.KeyValue        := Null;
*)
      EdValorT.ValueVariant      := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdCustoMOKg.ValueVariant   := 0;
(*
      EdFornecMO.ValueVariant    := 0;
      CBFornecMO.KeyValue        := Null;
*)
      EdObserv.Text              := '';
      //
      EdValorT.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOTot.ValueVariant  := 0;
      //
      SBNewPallet.Enabled := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSAjsIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSAjsIts.CalculaCustos();
var
  ValorMP, CustoMOKg, CustoMOM2, CustoMOTot, ValorT, PesoKg, AreaM2: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOKg    := EdCustoMOKg.ValueVariant;
  PesoKg       := EdPesoKg.ValueVariant;
  CustoMOM2    := EdCustoMOM2.ValueVariant;
  AreaM2       := EdAreaM2.ValueVariant;
  //
  CustoMOTot := (CustoMOKg * PesoKg) + (CustoMOM2 * AreaM2);
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSAjsIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSAjsIts.EdAreaM2Redefinido(Sender: TObject);
var
  Area, Peso, Pecs: Double;
begin
  if FPreco <> 0 then
  begin
    Area  := EdAreaM2.ValueVariant;
    Peso  := EdPesoKg.ValueVariant;
    Pecs  := EdPecas.ValueVariant;
    if Area >= 0.01 then
      EdValorMP.ValueVariant := Area * FPreco
    else
    if Peso >= 0.01 then
      EdValorMP.ValueVariant := Peso * FPreco
    else
      EdValorMP.ValueVariant := Pecs * FPreco
  end;
end;

procedure TFmVSAjsIts.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSAjsIts.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ficha: Integer;
begin
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
end;

procedure TFmVSAjsIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSAjsIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmVSAjsIts.EdPalletRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmVSAjsIts.EdPecasRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmVSAjsIts.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSAjsIts.EdPesoKgRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmVSAjsIts.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSAjsIts.EdValorMPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Area, Peso, Pecs: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      FPreco := Geral.DMV(Preco_TXT);
      Area  := EdAreaM2.ValueVariant;
      Peso  := EdPesoKg.ValueVariant;
      Pecs  := EdPecas.ValueVariant;
      if Area >= 0.01 then
        EdValorMP.ValueVariant := Area * FPreco
      else
      if Peso >= 0.01 then
        EdValorMP.ValueVariant := Peso * FPreco
      else
        EdValorMP.ValueVariant := Pecs * FPreco
    end;
  end;
end;

procedure TFmVSAjsIts.FichaRMP1Click(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  VAR_IMEI_SEL := 0;
  Localizou := VS_PF.MostraFormVSRMPPsq(emidAjuste, [], stPsq, Codigo, Controle,
  True);
  if VAR_IMEI_SEL <> 0 then
    PreencheDadosIMEI(VAR_IMEI_SEL);
  VAR_IMEI_SEL := 0;
end;

procedure TFmVSAjsIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSAjsIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  FForcaIMEI := 0;
  FPreco := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_VS + ') ');
  ReopenVSPallet();
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  VS_PF.AbreVSSerFch(QrVSSerFch);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSAjsIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSAjsIts.IMEI1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSMovIts(EdNivel2.ValueVariant);
  if VAR_CADASTRO <> 0 then
  begin
    if Geral.MB_Pergunta('Confirma a sele��o do IME-I ' + Geral.FF0(
    VAR_CADASTRO) + '?') = ID_YES then
      PreencheDadosIMEI(VAR_CADASTRO);
  end;
  VAR_CADASTRO := 0;
end;

procedure TFmVSAjsIts.Pallet1Click(Sender: TObject);
begin
  VAR_IMEI_SEL := 0;
  VS_PF.MostraFormVSPallet(0);
  if VAR_IMEI_SEL <> 0 then
    PreencheDadosIMEI(VAR_IMEI_SEL);
  VAR_IMEI_SEL := 0;
end;

procedure TFmVSAjsIts.PreencheDadosIMEI(IMEI: Integer);
var
  Qry: TmySQLQuery;
  //
  function ValorMP(): Double;
  var
    SdoVrtArM2, SdoVrtPeca, SdoVrtPeso, ValorT, AreaM2, Pecas, PesoKg: Double;
  begin
    Result := 0;
    ValorT := Qry.FieldByName('ValorT').AsFloat;
    AreaM2 := Qry.FieldByName('AreaM2').AsFloat;
    PesoKg := Qry.FieldByName('PesoKg').AsFloat;
    Pecas  := Qry.FieldByName('Pecas').AsFloat;
    //
    SdoVrtArM2 := -EdAreaM2.ValueVariant;
    SdoVrtPeca := -EdPecas.ValueVariant;
    SdoVrtPeso := -EdPesoKg.ValueVariant;
    //
    if (AreaM2 <> 0) then Result := SdoVrtArM2 * (ValorT / AreaM2)
    else
    if (PesoKg <> 0) then Result := SdoVrtPeso * (ValorT / AreaM2)
    else
    if Pecas <> 0 then
      Result := SdoVrtPeca * (ValorT / Pecas)
    else
      Result := 0;
    Result := -Result;
  end;
begin
  FForcaIMEI := IMEI;
  if IMEI = 0 then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    if Qry.RecordCount > 0 then
    begin
      ReopenSrcNivel2();
      EdNivel2.ValueVariant     := FForcaIMEI;
      CBNivel2.KeyValue         := FForcaIMEI;
      //
      EdGraGruX.ValueVariant    := Qry.FieldByName('GraGruX').AsInteger;
      CBGraGruX.KeyValue        := Qry.FieldByName('GraGruX').AsInteger;
      //
      EdSerieFch.ValueVariant   := Qry.FieldByName('SerieFch').AsInteger;
      CBSerieFch.KeyValue       := Qry.FieldByName('SerieFch').AsInteger;
      EdFicha.ValueVariant      := Qry.FieldByName('Ficha').AsInteger;
      //
      EdPallet.ValueVariant     := Qry.FieldByName('Pallet').AsInteger;
      CBPallet.KeyValue         := Qry.FieldByName('Pallet').AsInteger;
      //
      EdPecas.ValueVariant      := -Qry.FieldByName('SdoVrtPeca').AsFloat;
      EdPesoKg.ValueVariant     := -Qry.FieldByName('SdoVrtPeso').AsFloat;
      EdAreaM2.ValueVariant     := -Qry.FieldByName('SdoVrtArM2').AsFloat;
      EdValorMP.ValueVariant    := ValorMP();
      //
      EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
      CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
      //
      EdFornecMO.ValueVariant   := Qry.FieldByName('FornecMO').AsInteger;
      CBFornecMO.KeyValue       := Qry.FieldByName('FornecMO').AsInteger;
      //
      EdStqCenLoc.ValueVariant  := Qry.FieldByName('StqCenLoc').AsInteger;
      CBStqCenLoc.KeyValue      := Qry.FieldByName('StqCenLoc').AsInteger;
      //
      EdGraGruX.SetFocus;
      // ????
      EdNivel2.ValueVariant     := FForcaIMEI;
      CBNivel2.KeyValue         := FForcaIMEI;
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSAjsIts.RGMovimentacaoClick(Sender: TObject);
begin
  PnIMEIOrigem.Visible := RGMovimentacao.ItemIndex = 1;
  GBDados.Visible := RGMovimentacao.ItemIndex > 0;
end;

procedure TFmVSAjsIts.ReopenSrcNivel2();
var
  GraGruY, GraGruX, Pallet: Integer;
  SQL_IMEI: String;
  IMEISel: Integer;
begin
  QrSrcNivel2.Close;
  //
  GraGruX := EdGraGruX.ValueVariant;
  if (GraGruX <> 0) or (FForcaIMEI <> 0) then
  begin
    GraGruY := QrGraGruXGraGruY.Value;
    Pallet  := EdPallet.ValueVariant;
    if (EdPecas.ValueVariant < 0) or ((GraGruY < 1024) and (EdPesoKg.ValueVariant < 0))
    or (FForcaIMEI <> 0) then
    begin
      if FForcaIMEI = 0 then
        SQL_IMEI := ''
      else
        SQL_IMEI := 'OR (vmi.Controle=' + Geral.FF0(FForcaIMEI) + ')';
      //
      IMEISel := EdNivel2.ValueVariant;
      UnDmkDAC_PF.AbreMySQLQuery0(QrSrcNivel2, Dmod.MyDB, [
      'SELECT vmi.Controle, vmi.Codigo, vmi.MovimID,  ',
      'vmi.MovimNiv, vmi.GraGruX, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'NO_PRD_TAM_COR',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'WHERE ((vmi.Pecas>0 OR vmi.PesoKg>0) ',
      'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
      'AND vmi.Pallet=' + Geral.FF0(Pallet),
      ')',
      SQL_IMEI,
      'ORDER BY vmi.Controle DESC',
      '']);
      //Geral.MB_SQL(Self, QrSrcNivel2);
      if IMEISel <> 0 then
      begin
        if QrSrcNivel2.Locate('Controle', IMEISel, []) then
        begin
          EdNivel2.ValueVariant := IMEISel;
          CBNivel2.KeyValue     := IMEISel;
        end;
      end;
    end;
  end;
end;

procedure TFmVSAjsIts.ReopenVSAjsIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSAjsIts.ReopenVSPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM vspalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;

procedure TFmVSAjsIts.SBNewByInfoClick(Sender: TObject);
begin
  VS_PF.CadastraPalletInfo(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  QrGraGruXGraGruY.Value, QrVSPallet, EdPallet, CBPallet, SBNewPallet,
  EdPecas);
end;
{
var
  Nome, DtHrEndAdd: String;
  Empresa, ClientMO, Status, CliStat, GraGruX, QtdPrevPc, Pallet: Integer;
  PodePallet: Boolean;
  MovimIDGer: TEstqMovimID;
begin
  //Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := TEstqMovimID.emidAjuste;
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  PodePallet :=
    (QrGraGruXGraGruY.Value = CO_GraGruY_6144_VSFinCla)
  or
    (QrGraGruXGraGruY.Value = CO_GraGruY_3072_VSRibCla);
  //
  if MyObjects.FIC(not PodePallet, nil,
  'Somente artigo classificado ou acabado permite informa��o de pallet!') then
    Exit;
  //
  Pallet := VS_PF.CadastraPalletRibCla(Empresa, ClientMO, EdPallet, CBPallet,
  QrVSPallet, MovimIDGer, GraGruX);
  begin
    VS_PF.AtualizaStatPall(Pallet);
    ReopenVSPallet();
    EdPallet.ValueVariant := Pallet;
    CBPallet.KeyValue := Pallet;
    //
    SBNewPallet.Enabled := False;
    EdPecas.SetFocus;
  end;
end;
}
procedure TFmVSAjsIts.SBNewPalletClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
  PodePallet: Boolean;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  PodePallet :=
    (QrGraGruXGraGruY.Value = CO_GraGruY_6144_VSFinCla)
  or
    (QrGraGruXGraGruY.Value = CO_GraGruY_3072_VSRibCla);
  //
  if MyObjects.FIC(not PodePallet, nil,
  'Somente artigo classificado ou acabado permite informa��o de pallet!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    VS_PF.AtualizaStatPall(Codigo);
    ReopenVSPallet();
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
    EdPecas.SetFocus;
  end;
end;

procedure TFmVSAjsIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrVSPallet, VAR_CADASTRO);
  EdPecas.SetFocus;
end;

procedure TFmVSAjsIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;


procedure TFmVSAjsIts.SbImeiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImei, SbImei);
end;

end.
