unit VSImpClaIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  frxClass, frxDBSet, UnAppEnums;

type
  TFmVSImpClaIMEI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsGraGruX: TIntegerField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsGraGru1: TIntegerField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    frxWET_CURTI_036_001: TfrxReport;
    frxDsVSCacIts: TfrxDBDataset;
    QrVSCacSum: TmySQLQuery;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    QrVSGerArt: TmySQLQuery;
    QrVSGerArtCodigo: TIntegerField;
    QrVSGerArtMovimCod: TIntegerField;
    QrVSGerArtEmpresa: TIntegerField;
    QrVSGerArtNO_EMPRESA: TWideStringField;
    QrVSGerArtDtHrAberto: TDateTimeField;
    QrVSGerArtDtHrLibCla: TDateTimeField;
    QrVSGerArtDtHrCfgCla: TDateTimeField;
    QrVSGerArtDtHrFimCla: TDateTimeField;
    QrVSGerArtNome: TWideStringField;
    QrVSGerArtLk: TIntegerField;
    QrVSGerArtDataCad: TDateField;
    QrVSGerArtDataAlt: TDateField;
    QrVSGerArtUserCad: TIntegerField;
    QrVSGerArtUserAlt: TIntegerField;
    QrVSGerArtAlterWeb: TSmallintField;
    QrVSGerArtAtivo: TSmallintField;
    QrVSGerArtPecasMan: TFloatField;
    QrVSGerArtAreaManM2: TFloatField;
    QrVSGerArtAreaManP2: TFloatField;
    QrVSGerArtTipoArea: TSmallintField;
    QrVSGerArtNO_TIPO: TWideStringField;
    QrVSGerArtNO_DtHrFimCla: TWideStringField;
    QrVSGerArtNO_DtHrLibCla: TWideStringField;
    QrVSGerArtNO_DtHrCfgCla: TWideStringField;
    QrVSGerArtCacCod: TIntegerField;
    QrVSGerArtGraGruX: TIntegerField;
    QrVSGerArtCustoManMOKg: TFloatField;
    QrVSGerArtCustoManMOTot: TFloatField;
    QrVSGerArtValorManMP: TFloatField;
    QrVSGerArtValorManT: TFloatField;
    QrVSGerArtNew: TmySQLQuery;
    frxDsVSGerArtNew: TfrxDBDataset;
    QrVSGerArtOld: TmySQLQuery;
    frxDSVSGerArtOld: TfrxDBDataset;
    QrVSGerArtNewCodigo: TLargeintField;
    QrVSGerArtNewControle: TLargeintField;
    QrVSGerArtNewMovimCod: TLargeintField;
    QrVSGerArtNewMovimNiv: TLargeintField;
    QrVSGerArtNewMovimTwn: TLargeintField;
    QrVSGerArtNewEmpresa: TLargeintField;
    QrVSGerArtNewTerceiro: TLargeintField;
    QrVSGerArtNewCliVenda: TLargeintField;
    QrVSGerArtNewMovimID: TLargeintField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewPallet: TLargeintField;
    QrVSGerArtNewGraGruX: TLargeintField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewSrcMovID: TLargeintField;
    QrVSGerArtNewSrcNivel1: TLargeintField;
    QrVSGerArtNewSrcNivel2: TLargeintField;
    QrVSGerArtNewSrcGGX: TLargeintField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewSerieFch: TLargeintField;
    QrVSGerArtNewFicha: TLargeintField;
    QrVSGerArtNewMisturou: TLargeintField;
    QrVSGerArtNewFornecMO: TLargeintField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewCustoMOM2: TFloatField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TLargeintField;
    QrVSGerArtNewDstNivel1: TLargeintField;
    QrVSGerArtNewDstNivel2: TLargeintField;
    QrVSGerArtNewDstGGX: TLargeintField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNotaMPAG: TFloatField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewNO_TTW: TWideStringField;
    QrVSGerArtNewID_TTW: TLargeintField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewReqMovEstq: TLargeintField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewNO_LOC_CEN: TWideStringField;
    QrVSGerArtNewMarca: TWideStringField;
    QrVSGerArtNewPedItsLib: TLargeintField;
    QrVSGerArtNewStqCenLoc: TLargeintField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtOldCodigo: TLargeintField;
    QrVSGerArtOldControle: TLargeintField;
    QrVSGerArtOldMovimCod: TLargeintField;
    QrVSGerArtOldMovimNiv: TLargeintField;
    QrVSGerArtOldMovimTwn: TLargeintField;
    QrVSGerArtOldEmpresa: TLargeintField;
    QrVSGerArtOldTerceiro: TLargeintField;
    QrVSGerArtOldCliVenda: TLargeintField;
    QrVSGerArtOldMovimID: TLargeintField;
    QrVSGerArtOldDataHora: TDateTimeField;
    QrVSGerArtOldPallet: TLargeintField;
    QrVSGerArtOldGraGruX: TLargeintField;
    QrVSGerArtOldPecas: TFloatField;
    QrVSGerArtOldPesoKg: TFloatField;
    QrVSGerArtOldAreaM2: TFloatField;
    QrVSGerArtOldAreaP2: TFloatField;
    QrVSGerArtOldValorT: TFloatField;
    QrVSGerArtOldSrcMovID: TLargeintField;
    QrVSGerArtOldSrcNivel1: TLargeintField;
    QrVSGerArtOldSrcNivel2: TLargeintField;
    QrVSGerArtOldSrcGGX: TLargeintField;
    QrVSGerArtOldSdoVrtPeca: TFloatField;
    QrVSGerArtOldSdoVrtPeso: TFloatField;
    QrVSGerArtOldSdoVrtArM2: TFloatField;
    QrVSGerArtOldObserv: TWideStringField;
    QrVSGerArtOldSerieFch: TLargeintField;
    QrVSGerArtOldFicha: TLargeintField;
    QrVSGerArtOldMisturou: TLargeintField;
    QrVSGerArtOldFornecMO: TLargeintField;
    QrVSGerArtOldCustoMOKg: TFloatField;
    QrVSGerArtOldCustoMOM2: TFloatField;
    QrVSGerArtOldCustoMOTot: TFloatField;
    QrVSGerArtOldValorMP: TFloatField;
    QrVSGerArtOldDstMovID: TLargeintField;
    QrVSGerArtOldDstNivel1: TLargeintField;
    QrVSGerArtOldDstNivel2: TLargeintField;
    QrVSGerArtOldDstGGX: TLargeintField;
    QrVSGerArtOldQtdGerPeca: TFloatField;
    QrVSGerArtOldQtdGerPeso: TFloatField;
    QrVSGerArtOldQtdGerArM2: TFloatField;
    QrVSGerArtOldQtdGerArP2: TFloatField;
    QrVSGerArtOldQtdAntPeca: TFloatField;
    QrVSGerArtOldQtdAntPeso: TFloatField;
    QrVSGerArtOldQtdAntArM2: TFloatField;
    QrVSGerArtOldQtdAntArP2: TFloatField;
    QrVSGerArtOldNotaMPAG: TFloatField;
    QrVSGerArtOldNO_PALLET: TWideStringField;
    QrVSGerArtOldNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtOldNO_TTW: TWideStringField;
    QrVSGerArtOldID_TTW: TLargeintField;
    QrVSGerArtOldNO_FORNECE: TWideStringField;
    QrVSGerArtOldNO_SerieFch: TWideStringField;
    QrVSGerArtOldReqMovEstq: TLargeintField;
    frxWET_CURTI_036_002: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_036_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure QrVSGerArtCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FIMEI, FVSGerArt, FVSMulCab_MovimCod: Integer;
    procedure ImprimeClassIMEI();
  end;

  var
  FmVSImpClaIMEI: TFmVSImpClaIMEI;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSImpClaIMEI.BtOKClick(Sender: TObject);
begin
   ImprimeClassIMEI();
end;

procedure TFmVSImpClaIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpClaIMEI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpClaIMEI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FVSGerArt := 0;
  FVSMulCab_MovimCod := 0;
end;

procedure TFmVSImpClaIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpClaIMEI.frxWET_CURTI_036_001GetValue(const VarName: string;
  var Value: Variant);
begin
(*
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
*)
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_IMEI' then
    Value := Geral.FF0(FIMEI)
  else
(*
  if VarName ='VARF_WBNIVGER' then
    Value := Dmod.QrControleWBNivGer.Value
  else
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro.Text, Ed01Terceiro.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PALLET_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Pallet.Text, Ed01Pallet.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_GRAGRUX_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01GraGruX.Text, Ed01GraGruX.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni.Date, TP01DataFim.Date,
    Ck01DataIni.Checked, Ck01DataFim.Checked, '', 'at�', '')
  else
*)
  if QrVSGerArt.State <> dsInactive then
  begin
    if VarName ='VARF_SERIE_FICHA' then
      Value := QrVSGerArtNewNO_FICHA.Value
    else
    if VarName ='VARF_DTHR_GERACAO' then
      Value := Geral.FDT(QrVSGerArtDtHrAberto.Value, 106, True)
    else
    if VarName ='VARF_DTHR_LIBCLASS' then
      Value := QrVSGerArtNO_DtHrLibCla.Value
    else
    if VarName ='VARF_DTHR_CNFCLASS' then
      Value := QrVSGerArtNO_DtHrCfgCla.Value
    else
    if VarName ='VARF_DTHR_FIMCLASS' then
      Value := QrVSGerArtNO_DtHrFimCla.Value
  end else
  if FVSMulCab_MovimCod > 0 then
  begin
    if VarName ='VARF_SERIE_FICHA' then
      Value := QrVSGerArtNewNO_FICHA.Value
    else
    if VarName ='VARF_DTHR_GERACAO' then
      Value := Geral.FDT(QrVSGerArtNewDataHora.Value, 106, True)
    else
    if VarName ='VARF_DTHR_LIBCLASS' then
      Value := Geral.FDT(QrVSGerArtNewDataHora.Value, 106, True)
    else
    if VarName ='VARF_DTHR_CNFCLASS' then
      Value := Geral.FDT(QrVSGerArtNewDataHora.Value, 106, True)
    else
    if VarName ='VARF_DTHR_FIMCLASS' then
      Value := Geral.FDT(QrVSGerArtNewDataHora.Value, 106, True)
  end else
  begin
    if VarName ='VARF_SERIE_FICHA' then
      Value := ''
    else
    if VarName ='VARF_DTHR_GERACAO' then
      Value := ''
    else
    if VarName ='VARF_DTHR_LIBCLASS' then
      Value := ''
    else
    if VarName ='VARF_DTHR_CNFCLASS' then
      Value := ''
    else
    if VarName ='VARF_DTHR_FIMCLASS' then
      Value := ''
  end
end;

procedure TFmVSImpClaIMEI.ImprimeClassIMEI();
const
  TemIMEIMrt = 0; // Ver o que fazer !
begin
  if FVSGerArt <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArt, Dmod.MyDB, [
    'SELECT vga.*, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ',
    'FROM vsgerarta vga ',
    'LEFT JOIN entidades ent ON ent.Codigo=vga.Empresa ',
    'WHERE vga.Codigo=' + Geral.FF0(FVSGerArt),
    '']);
    VS_CRC_PF.ReopenVSGerArtDst(QrVSGerArtNew, QrVSGerArtMovimCod.Value, 0, TemIMEIMrt, eminDestCurtiXX);
    VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArtOld, QrVSGerArtMovimCod.Value, 0, TemIMEIMrt, '', eminSorcCurtiXX);
  end else
  if FVSMulCab_MovimCod <> 0 then
  begin
    VS_CRC_PF.ReopenVSGerArtDst(QrVSGerArtNew, FVSMulCab_MovimCod, 0, TemIMEIMrt, eminDestPreReclas);
    VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArtOld, FVSMulCab_MovimCod, 0, TemIMEIMrt, '', eminSorcPreReclas);//eminSorcCurtiXX);
  end else
  begin
    Geral.MB_Erro('Impress�o de classes geradas sem informa��o de gera��o de artigo!');
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, Dmod.MyDB, [
  'SELECT SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2 ',
  'FROM vscacitsa cia ',
  'WHERE cia.vmi_Sorc=' + Geral.FF0(FIMEI),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, Dmod.MyDB, [
  'SELECT pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE cia.vmi_Sorc=' + Geral.FF0(FIMEI),
  'GROUP BY pla.GraGruX ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_036_001, [
  DModG.frxDsDono,
  frxDsVSCacIts,
  frxDsVSGerArtNew,
  frxDsVSGerArtOld
  ]);
  MyObjects.frxMostra(frxWET_CURTI_036_001, 'Romaneio de Artigo de Ribeira');
end;

procedure TFmVSImpClaIMEI.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSImpClaIMEI.QrVSGerArtCalcFields(DataSet: TDataSet);
begin
  case QrVSGerArtTipoArea.Value of
    0: QrVSGerArtNO_TIPO.Value := 'm�';
    1: QrVSGerArtNO_TIPO.Value := 'ft�';
    else QrVSGerArtNO_TIPO.Value := '???';
  end;
  QrVSGerArtNO_DtHrLibCla.Value := Geral.FDT(QrVSGerArtDtHrLibCla.Value, 106, True);
  QrVSGerArtNO_DtHrFimCla.Value := Geral.FDT(QrVSGerArtDtHrFimCla.Value, 106, True);
  QrVSGerArtNO_DtHrCfgCla.Value := Geral.FDT(QrVSGerArtDtHrCfgCla.Value, 106, True);
end;

(*

SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW,
CAST(vmi.Codigo AS SIGNED) Codigo,
CAST(vmi.Controle AS SIGNED) Controle,
CAST(vmi.MovimCod AS SIGNED) MovimCod,
CAST(vmi.MovimNiv AS SIGNED) MovimNiv,
CAST(vmi.MovimTwn AS SIGNED) MovimTwn,
CAST(vmi.Empresa AS SIGNED) Empresa,
CAST(vmi.ClientMO AS SIGNED) ClientMO,
CAST(vmi.Terceiro AS SIGNED) Terceiro,
CAST(vmi.CliVenda AS SIGNED) CliVenda,
CAST(vmi.MovimID AS SIGNED) MovimID,
CAST(vmi.DataHora AS DATETIME) DataHora,
CAST(vmi.Pallet AS SIGNED) Pallet,
CAST(vmi.GraGruX AS SIGNED) GraGruX,
CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas,
CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg,
CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2,
CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2,
CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT,
CAST(vmi.SrcMovID AS SIGNED) SrcMovID,
CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1,
CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2,
CAST(vmi.SrcGGX AS SIGNED) SrcGGX,
CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca,
CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso,
CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2,
CAST(vmi.Observ AS CHAR) Observ,
CAST(vmi.SerieFch AS SIGNED) SerieFch,
CAST(vmi.Ficha AS SIGNED) Ficha,
CAST(vmi.Misturou AS UNSIGNED) Misturou,
CAST(vmi.FornecMO AS SIGNED) FornecMO,
CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg,
CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot,
CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP,
CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ,
CAST(vmi.DstMovID AS SIGNED) DstMovID,
CAST(vmi.DstNivel1 AS SIGNED) DstNivel1,
CAST(vmi.DstNivel2 AS SIGNED) DstNivel2,
CAST(vmi.DstGGX AS SIGNED) DstGGX,
CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca,
CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso,
CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2,
CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2,
CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca,
CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso,
CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2,
CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2,
CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG,
CAST(vmi.Marca AS CHAR) Marca,
CAST(vmi.PedItsLib AS SIGNED) PedItsLib,
CAST(vmi.PedItsFin AS SIGNED) PedItsFin,
CAST(vmi.PedItsVda AS SIGNED) PedItsVda,
CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2,
CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq,
CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc,
CAST(vmi.ItemNFe AS SIGNED) ItemNFe,
CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab,
CAST(vmi.NFeSer AS SIGNED) NFeSer,
CAST(vmi.NFeNum AS SIGNED) NFeNum,
CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab,
CAST(vmi.JmpMovID AS SIGNED) JmpMovID,
CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1,
CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2,
CAST(vmi.RmsMovID AS SIGNED) RmsMovID,
CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1,
CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2,
CAST(vmi.GGXRcl AS SIGNED) GGXRcl,
CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),
IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))
NO_PRD_TAM_COR,

IF(vmi.Terceiro=0, "V�rios",
  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)
) NO_FORNECE,
IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL,
"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA,
IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2,
IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2,
CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN,
vsp.Nome NO_Pallet


FROM ' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX
LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC
LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad
LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1

LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet
LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch
LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro
LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc
LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo


WHERE vmi.MovimCod=1361
AND vmi.MovimNiv=13






ORDER BY Pallet, Controle





















///////////////////////////////














SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW,
CAST(vmi.Codigo AS SIGNED) Codigo,
CAST(vmi.Controle AS SIGNED) Controle,
CAST(vmi.MovimCod AS SIGNED) MovimCod,
CAST(vmi.MovimNiv AS SIGNED) MovimNiv,
CAST(vmi.MovimTwn AS SIGNED) MovimTwn,
CAST(vmi.Empresa AS SIGNED) Empresa,
CAST(vmi.ClientMO AS SIGNED) ClientMO,
CAST(vmi.Terceiro AS SIGNED) Terceiro,
CAST(vmi.CliVenda AS SIGNED) CliVenda,
CAST(vmi.MovimID AS SIGNED) MovimID,
CAST(vmi.DataHora AS DATETIME) DataHora,
CAST(vmi.Pallet AS SIGNED) Pallet,
CAST(vmi.GraGruX AS SIGNED) GraGruX,
CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas,
CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg,
CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2,
CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2,
CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT,
CAST(vmi.SrcMovID AS SIGNED) SrcMovID,
CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1,
CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2,
CAST(vmi.SrcGGX AS SIGNED) SrcGGX,
CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca,
CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso,
CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2,
CAST(vmi.Observ AS CHAR) Observ,
CAST(vmi.SerieFch AS SIGNED) SerieFch,
CAST(vmi.Ficha AS SIGNED) Ficha,
CAST(vmi.Misturou AS UNSIGNED) Misturou,
CAST(vmi.FornecMO AS SIGNED) FornecMO,
CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg,
CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot,
CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP,
CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ,
CAST(vmi.DstMovID AS SIGNED) DstMovID,
CAST(vmi.DstNivel1 AS SIGNED) DstNivel1,
CAST(vmi.DstNivel2 AS SIGNED) DstNivel2,
CAST(vmi.DstGGX AS SIGNED) DstGGX,
CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca,
CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso,
CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2,
CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2,
CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca,
CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso,
CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2,
CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2,
CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG,
CAST(vmi.Marca AS CHAR) Marca,
CAST(vmi.PedItsLib AS SIGNED) PedItsLib,
CAST(vmi.PedItsFin AS SIGNED) PedItsFin,
CAST(vmi.PedItsVda AS SIGNED) PedItsVda,
CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2,
CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq,
CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc,
CAST(vmi.ItemNFe AS SIGNED) ItemNFe,
CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab,
CAST(vmi.NFeSer AS SIGNED) NFeSer,
CAST(vmi.NFeNum AS SIGNED) NFeNum,
CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab,
CAST(vmi.JmpMovID AS SIGNED) JmpMovID,
CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1,
CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2,
CAST(vmi.RmsMovID AS SIGNED) RmsMovID,
CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1,
CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2,
CAST(vmi.GGXRcl AS SIGNED) GGXRcl,
CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),
IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))
NO_PRD_TAM_COR,

IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,
vsf.Nome NO_SerieFch,
vsp.Nome NO_Pallet


FROM ' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX
LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC
LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad
LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1

LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet
LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch
LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro


WHERE vmi.MovimCod=1361
AND vmi.MovimNiv=14






ORDER BY NO_Pallet, Controle
*)

end.
