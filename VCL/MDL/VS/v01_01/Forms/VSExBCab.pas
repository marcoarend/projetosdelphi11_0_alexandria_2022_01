unit VSExBCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnProjGroup_Consts, UnAppEnums,
  UnGrl_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSExBCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSExBCab: TmySQLQuery;
    DsVSExBCab: TDataSource;
    QrVSExBIts: TmySQLQuery;
    DsVSExBIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtBaixa: TdmkEditDateTimePicker;
    EdDtBaixa: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    EdNome: TdmkEdit;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    N1: TMenuItem;
    AdicionaIMEIscompletos1: TMenuItem;
    Label4: TLabel;
    QrVSMotivBxa: TmySQLQuery;
    DsVSMotivBxa: TDataSource;
    EdMotivBxa: TdmkEditCB;
    CBMotivBxa: TdmkDBLookupComboBox;
    QrVSMotivBxaNome: TWideStringField;
    QrVSMotivBxaCodigo: TIntegerField;
    QrVSExBCabNO_MOTIVBXA: TWideStringField;
    QrVSExBCabCodigo: TIntegerField;
    QrVSExBCabNome: TWideStringField;
    QrVSExBCabMovimCod: TIntegerField;
    QrVSExBCabEmpresa: TIntegerField;
    QrVSExBCabDtBaixa: TDateTimeField;
    QrVSExBCabPecas: TFloatField;
    QrVSExBCabPesoKg: TFloatField;
    QrVSExBCabAreaM2: TFloatField;
    QrVSExBCabAreaP2: TFloatField;
    QrVSExBCabValorT: TFloatField;
    QrVSExBCabMotivBxa: TIntegerField;
    QrVSExBCabLk: TIntegerField;
    QrVSExBCabDataCad: TDateField;
    QrVSExBCabDataAlt: TDateField;
    QrVSExBCabUserCad: TIntegerField;
    QrVSExBCabUserAlt: TIntegerField;
    QrVSExBCabAlterWeb: TSmallintField;
    QrVSExBCabAtivo: TSmallintField;
    QrVSExBCabNO_EMPRESA: TWideStringField;
    Label5: TLabel;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    QrVSExBItsCodigo: TLargeintField;
    QrVSExBItsControle: TLargeintField;
    QrVSExBItsMovimCod: TLargeintField;
    QrVSExBItsMovimNiv: TLargeintField;
    QrVSExBItsMovimTwn: TLargeintField;
    QrVSExBItsEmpresa: TLargeintField;
    QrVSExBItsTerceiro: TLargeintField;
    QrVSExBItsCliVenda: TLargeintField;
    QrVSExBItsMovimID: TLargeintField;
    QrVSExBItsDataHora: TDateTimeField;
    QrVSExBItsPallet: TLargeintField;
    QrVSExBItsGraGruX: TLargeintField;
    QrVSExBItsPecas: TFloatField;
    QrVSExBItsPesoKg: TFloatField;
    QrVSExBItsAreaM2: TFloatField;
    QrVSExBItsAreaP2: TFloatField;
    QrVSExBItsValorT: TFloatField;
    QrVSExBItsSrcMovID: TLargeintField;
    QrVSExBItsSrcNivel1: TLargeintField;
    QrVSExBItsSrcNivel2: TLargeintField;
    QrVSExBItsSrcGGX: TLargeintField;
    QrVSExBItsSdoVrtPeca: TFloatField;
    QrVSExBItsSdoVrtPeso: TFloatField;
    QrVSExBItsSdoVrtArM2: TFloatField;
    QrVSExBItsObserv: TWideStringField;
    QrVSExBItsSerieFch: TLargeintField;
    QrVSExBItsFicha: TLargeintField;
    QrVSExBItsMisturou: TLargeintField;
    QrVSExBItsFornecMO: TLargeintField;
    QrVSExBItsCustoMOKg: TFloatField;
    QrVSExBItsCustoMOM2: TFloatField;
    QrVSExBItsCustoMOTot: TFloatField;
    QrVSExBItsValorMP: TFloatField;
    QrVSExBItsDstMovID: TLargeintField;
    QrVSExBItsDstNivel1: TLargeintField;
    QrVSExBItsDstNivel2: TLargeintField;
    QrVSExBItsDstGGX: TLargeintField;
    QrVSExBItsQtdGerPeca: TFloatField;
    QrVSExBItsQtdGerPeso: TFloatField;
    QrVSExBItsQtdGerArM2: TFloatField;
    QrVSExBItsQtdGerArP2: TFloatField;
    QrVSExBItsQtdAntPeca: TFloatField;
    QrVSExBItsQtdAntPeso: TFloatField;
    QrVSExBItsQtdAntArM2: TFloatField;
    QrVSExBItsQtdAntArP2: TFloatField;
    QrVSExBItsNotaMPAG: TFloatField;
    QrVSExBItsNO_PALLET: TWideStringField;
    QrVSExBItsNO_PRD_TAM_COR: TWideStringField;
    QrVSExBItsNO_TTW: TWideStringField;
    QrVSExBItsID_TTW: TLargeintField;
    QrVSExBCabTemIMEIMrt: TSmallintField;
    QrVSExBItsCustoM2: TFloatField;
    QrVSExBItsCustoKg: TFloatField;
    N2: TMenuItem;
    Corrigir1: TMenuItem;
    QrSrc: TMySQLQuery;
    QrSrcPecas: TFloatField;
    QrSrcPesoKg: TFloatField;
    QrSrcSdoVrtPeca: TFloatField;
    QrSrcSdoVrtPeso: TFloatField;
    QrSrcSdoVrtArM2: TFloatField;
    QrSrcAreaM2: TFloatField;
    QrSrcValorT: TFloatField;
    Corrigirpesospositivosdebaixas1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSExBCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSExBCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSExBCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSExBCabBeforeClose(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AdicionaIMEIscompletos1Click(Sender: TObject);
    procedure Corrigir1Click(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure Corrigirpesospositivosdebaixas1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSExBIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSExBIts(Controle: Integer);

  end;

var
  FmVSExBCab: TFmVSExBCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
VSExBIts, UnVS_PF, VSExBItsIMEIs, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSExBCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSExBCab.MostraFormVSExBIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSExBIts, FmVSExBIts, afmoNegarComAviso) then
  begin
    FmVSExBIts.ImgTipo.SQLType := SQLType;
    FmVSExBIts.FQrCab    := QrVSExBCab;
    FmVSExBIts.FDsCab    := DsVSExBCab;
    FmVSExBIts.FQrIts    := QrVSExBIts;
    FmVSExBIts.FDataHora := QrVSExBCabDtBaixa.Value;
    FmVSExBIts.FEmpresa  := QrVSExBCabEmpresa.Value;
    if SQLType = stIns then
    begin
      //FmVSExBIts.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmVSExBIts.FDataHora := QrVSExBCabDtVenda.Value;
      FmVSExBIts.EdControle.ValueVariant := QrVSExBItsControle.Value;
      //
      FmVSExBIts.EdGragruX.ValueVariant  := QrVSExBItsGraGruX.Value;
      FmVSExBIts.CBGragruX.KeyValue      := QrVSExBItsGraGruX.Value;
      FmVSExBIts.EdPecas.ValueVariant    := -QrVSExBItsPecas.Value;
      FmVSExBIts.EdPesoKg.ValueVariant   := -QrVSExBItsPesoKg.Value;
      FmVSExBIts.EdAreaM2.ValueVariant   := -QrVSExBItsAreaM2.Value;
      FmVSExBIts.EdAreaP2.ValueVariant   := -QrVSExBItsAreaP2.Value;
      //FmVSExBIts.EdValorT.ValueVariant   := -QrVSExBItsValorT.Value;
      FmVSExBIts.EdPallet.ValueVariant   := QrVSExBItsPallet.Value;
      //
    end;
}
    FmVSExBIts.ShowModal;
    FmVSExBIts.Destroy;
    end;
  end;
end;

procedure TFmVSExBCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSExBCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSExBCab, QrVSExBIts);
end;

procedure TFmVSExBCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSExBCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSExBIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSExBIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSExBItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSExBCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSExBCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSExBCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsexbcab';
  VAR_GOTOMYSQLTABLE := QrVSExBCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vmb.Nome NO_MOTIVBXA, wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM vsexbcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN vsmotivbxa vmb ON vmb.Codigo=wic.MotivBxa');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSExBCab.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  //if Column.FieldName = 'Controle' then
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(QrVSExBItsControle.Value)
  else
  if Campo = 'SrcNivel2' then
      VS_PF.MostraFormVSMovIts(QrVSExBItsSrcNivel2.Value);
end;

procedure TFmVSExBCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSExBCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSExBCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSExBCab.ItsAltera1Click(Sender: TObject);
begin
// Nao fazer!
end;

procedure TFmVSExBCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSExBItsCodigo.Value;
  MovimCod := QrVSExBItsMovimCod.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSExBIts, TIntegerField(QrVSExBItsControle),
  QrVSExBItsControle.Value, CtrlBaix, QrVSExBItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti078)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSExBCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSExBIts(stIns);
end;

procedure TFmVSExBCab.ReopenVSExBIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSExBIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSExBCabMovimCod.Value),
  'ORDER BY vmi.Controle ',
  '']);
  //
  QrVSExBIts.Locate('Controle', Controle, []);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSExBCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(vmi.AreaM2 <> 0.00, vmi.ValorT/vmi.AreaM2, 0.00) CustoM2, ',
  'IF(vmi.PesoKg <> 0.00, vmi.ValorT/vmi.PesoKg, 0.00) CustoKg, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSExBCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSExBIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  //Geral.MB_teste(QrVSExBIts.SQL.Text);
  //
  QrVSExBIts.Locate('Controle', Controle, []);
end;


procedure TFmVSExBCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSExBCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSExBCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSExBCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSExBCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSExBCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSExBCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSExBCabCodigo.Value;
  Close;
end;

procedure TFmVSExBCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSExBCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsexbcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSExBCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSExBCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtBaixa, DataHora: String;
  Codigo, MovimCod, Empresa, MotivBxa: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtBaixa        := Geral.FDT_TP_Ed(TPDtBaixa.Date, EdDtBaixa.Text);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  MotivBxa       := EdMotivBxa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(MotivBxa = 0, EdMotivBxa, 'Defina o motivo da baixa extra!') then Exit;
  if MyObjects.FIC(TPDtBaixa.DateTime < 2, TPDtBaixa,
    'Defina uma data de baixa!') then Exit;

  Codigo := UMyMod.BPGS1I32('vsexbcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsexbcab', False, [
  'Nome', 'MovimCod', 'Empresa',
  'DtBaixa', 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',
  'MotivBxa'], [
  'Codigo'], [
  Nome, MovimCod, Empresa,
  DtBaixa, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT,
  MotivBxa], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if FSeq = 0 then
        VS_PF.InsereVSMovCab(MovimCod, emidExtraBxa, Codigo);
    end
    else
    begin
(*
      N�o fazer! Data / hora � individual do IME=I!
      DataHora := DtBaixa;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
*)
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if Codigo = QrVSExBCabCodigo.Value then
    begin
      if FSeq = 1 then
      begin
        FSeq := 2; // Inseriu = True
        Close;
      end;
    end;
  end;
end;

procedure TFmVSExBCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsexbcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsexbcab', 'Codigo');
end;

procedure TFmVSExBCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSExBCab.AdicionaIMEIscompletos1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmVSExBItsIMEIs, FmVSExBItsIMEIs, afmoNegarComAviso) then
  begin
    FmVSExBItsIMEIs.ImgTipo.SQLType := stIns;
    //
    FmVSExBItsIMEIs.FCodigo   := QrVSExBCabCodigo.Value;
    FmVSExBItsIMEIs.FMovimCod := QrVSExBCabMovimCod.Value;
    FmVSExBItsIMEIs.FEmpresa  := QrVSExBCabEmpresa.Value;
    //
    FmVSExBItsIMEIs.FDataHora := QrVSExBCabDtBaixa.Value;
    FmVSExBItsIMEIs.TPDataSenha.Date := Int(QrVSExBCabDtBaixa.Value);
    FmVSExBItsIMEIs.RGDataHora.Items[1] := 'Do cabe�alho: ' +
      Geral.FDT(QrVSExBCabDtBaixa.Value, 109);
    //
    FmVSExBItsIMEIs.ShowModal;
    Controle := FmVSExBItsIMEIs.FControle;
    FmVSExBItsIMEIs.Destroy;
    //
    ReopenVSExBIts(Controle);
  end;
end;

procedure TFmVSExBCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSExBCab, QrVSExBCabDtBaixa.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSExBCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDMkDAC_PF.AbreQuery(QrVSMotivBxa, Dmod.MyDB);
end;

procedure TFmVSExBCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSExBCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSExBCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmVSExBCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExBCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSExBCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExBCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSExBCab.QrVSExBCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSExBCab.QrVSExBCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSExBIts(0);
end;

procedure TFmVSExBCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSExBCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSExBCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSExBCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsexbcab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSExBCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSExBCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSExBCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsexbcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDtBaixa.Date := DModG.ObtemAgora();
  EdDtBaixa.ValueVariant := DModG.ObtemAgora();
  if EdEmpresa.ValueVariant > 0 then
    TPDtBaixa.SetFocus;
end;

procedure TFmVSExBCab.Corrigir1Click(Sender: TObject);
const
  Gera = False;
var
  I, N, SrcNivel2, Controle, Codigo, MovimCod: Integer;
  AreaM2, PesoKg, Preco, ValorT: Double;
  sAreaM2, sPesoKg, sValorT: String;
begin
  if DGDados.SelectedRows.Count > 0 then
  begin
    N := 0;
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    Geral.MB_Info('Por seguran�a, somente itens de origem com saldo de pe�as zerado ser�o corrigidos!');
    Screen.Cursor := crHourGlass;
    try
      Codigo    := QrVSExBCabCodigo.Value;
      MovimCod  := QrVSExBCabMovimCod.Value;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo �rea, peso e valor total na baixa!');
      with DGDados.DataSource.DataSet do
      for I := 0 to DGDados.SelectedRows.Count-1 do
      begin
        GotoBookmark(DGDados.SelectedRows.Items[I]);
        //
        Controle  := QrVSExBItsControle.Value;
        SrcNivel2 := QrVSExBItsSrcNivel2.Value;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrSrc, Dmod.MyDB, [
        'SELECT Pecas, PesoKg, AreaM2, ValorT, ',
        'SdoVrtPeca, SdoVrtPeso, SdoVrtArM2 ',
        'FROM vsmovits ',
        'WHERE Controle=' + Geral.FF0(SrcNivel2),
        '']);
        if QrSrcSdoVrtPeca.Value = 0 then
        begin
          AreaM2 := QrVSExBItsAreaM2.Value;
          PesoKg := QrVSExBItsPesoKg.Value;
          ValorT := QrVSExBItsvalorT.Value;
          if ((AreaM2 <> 0) or (PesoKg <> 0)) and (ValorT = 0) then
          begin
            if QrSrcAreaM2.Value <> 0 then
              ValorT := AreaM2 * (QrSrcValorT.Value / QrSrcAreaM2.Value)
            else
            if QrSrcPesoKg.Value <> 0 then
              ValorT := PesoKg * (QrSrcValorT.Value / QrSrcPesoKg.Value)
            else
              ValorT := 0.00;
            //
            if ValorT <> 0 then
            begin
              sValorT := Geral.FFT_Dot(ValorT, 2, siNegativo);
              //
              if ValorT >= 0 then sValorT := ' + ' + sValorT;

              UnDmkDAC_PF.ExecutaDB(DMod.MyDB,
              ' UPDATE vsmovits SET ' +
              ' ValorT=ValorT' + sValorT +
              ' WHERE Controle = ' + Geral.FF0(Controle) +
              ' ');
              //
              VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, Gera);
              //
              N := N + 1;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    LocCod(Codigo, Codigo);
    Geral.MB_Info(Geral.FF0(N) + ' itens foram atualizados!');
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSExBCab.Corrigirpesospositivosdebaixas1Click(Sender: TObject);
const
  Gera = False;
var
  I, N, SrcNivel2, Controle, Codigo, MovimCod: Integer;
  AreaM2, PesoKg, Preco, ValorT: Double;
  sAreaM2, sPesoKg, sValorT: String;
begin
  if DGDados.SelectedRows.Count > 0 then
  begin
    N := 0;
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    Screen.Cursor := crHourGlass;
    try
      Codigo    := QrVSExBCabCodigo.Value;
      MovimCod  := QrVSExBCabMovimCod.Value;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo pesos positivos na baixa!');
      with DGDados.DataSource.DataSet do
      for I := 0 to DGDados.SelectedRows.Count-1 do
      begin
        GotoBookmark(DGDados.SelectedRows.Items[I]);
        //
        if (QrVSExBItsPecas.Value < 0) and (QrVSExBItsPesoKg.Value > 0) then
        begin
          Controle  := QrVSExBItsControle.Value;
          SrcNivel2 := QrVSExBItsSrcNivel2.Value;
          //
          UnDmkDAC_PF.ExecutaDB(DMod.MyDB,
          ' UPDATE vsmovits SET ' +
          ' PesoKg=PesoKg*-1 ' +
          ' WHERE Controle = ' + Geral.FF0(Controle) +
          ' ');
          //
          VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, Gera);
          //
          N := N + 1;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    //
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    LocCod(Codigo, Codigo);
    Geral.MB_Info(Geral.FF0(N) + ' itens foram atualizados!');
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSExBCab.QrVSExBCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSExBIts.Close;
end;

procedure TFmVSExBCab.QrVSExBCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSExBCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

