unit VSArvoreArtigosOld;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, Vcl.ImgList;

type
  TFmVSArvoreArtigosOld = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Qr013: TmySQLQuery;
    Qr013Ficha: TIntegerField;
    Qr013SerieFch: TIntegerField;
    Qr013Marca: TWideStringField;
    Qr013Terceiro: TIntegerField;
    QrTwns: TmySQLQuery;
    QrTwnsMovimTwn: TIntegerField;
    Qr007: TmySQLQuery;
    Qr007Controle: TIntegerField;
    Qr007MovimNiv: TIntegerField;
    Qr007MovimID: TIntegerField;
    TreeView1: TTreeView;
    ImageList1: TImageList;
    Qr013MovimID: TIntegerField;
    Qr013MovimNiv: TIntegerField;
    Qr013Pecas: TFloatField;
    Qr013AreaM2: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeView1GetImageIndex(Sender: TObject; Node: TTreeNode);
  private
    { Private declarations }
    FAtualizados: Integer;
    FItensAtualizados: array of Integer;
    //FTotalPecas, FTotalAreaM2: Double;
    //
    function  DefineImageIndexETextoMovimA(const MovimID, MovimNiv: Integer;
              var Indice: Integer; var Texto: String): Boolean;
    function  DefineImageIndexETextoMovimB(const MovimID, MovimNiv, SrcMovID:
              Integer; var Indice: Integer; var Texto: String): Boolean;
    procedure InjetaDadosFilhosDeInNatura(IMEI: Integer; Noh_Pai: TTreeNode);
    procedure InjetaDadosFilhosDeArtigoGerado(IMEI: Integer; Noh_Pai: TTreeNode;
              QtdItensPai, QtdTotalPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado_PreClasse(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado_Operacao(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoPreReclassificado(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; PercentPai: Double = 0);
    function  InjetaNodeAtual_A(Titulo, Texto: String; PK, Index: Integer;
              NodePai: TTreeNode; MovimType: TEstqMovimType;
              ItensParte: Double = 0; ItensTotal: Double = 0; PercentPai: Double = 100): TTreeNode;
              //ItensParte: Double; ItensTotal: Double; PercentPai: Double): TTreeNode;
    function  InjetaNodeAtual_B(Titulo, Texto: String; PK, Index: Integer;
              NodePai: TTreeNode; MovimType: TEstqMovimType;
              //ItensParte: Double = 0; ItensTotal: Double = 0; PercentPai: Double = 100): TTreeNode;
              ItensParte: Double; ItensTotal: Double; PercentPai: Double;
              ContaIMEI: Boolean): TTreeNode;
    procedure DefineQtdeItensDeIMEI(const Qry: TmySQLQuery; var QtdItens:
              Double; var TipoCalc: TTipoCalcCouro);
    public
    { Public declarations }
    //
    procedure MontaArvore(IMEI_Pai: Integer);
  end;

  var
  FmVSArvoreArtigosOld: TFmVSArvoreArtigosOld;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, AppListas;

{$R *.DFM}

const
  CO_IDX_IMG_INDEFINIDO     = 00;
  CO_IDX_IMG_GEMEOS         = 01;
  CO_IDX_IMG_ITENSARECLAS   = 02;
  CO_IDX_IMG_CLASSIFICAR    = 03;
  CO_IDX_IMG_RECLASSIFICAR  = 03;
  CO_IDX_IMG_PREPARA_RECLAS = 04;
  CO_IDX_IMG_PREPARA_OPERAR = 04;
  CO_IDX_IMG_OPERCAO        = 05;
  CO_IDX_IMG_PROCESSO       = 06;
  CO_IDX_IMG_INNATURA       = 07;
  CO_IDX_IMG_CURTIDO        = 08;
  CO_IDX_IMG_SEMCLASSE      = 09;
  CO_IDX_IMG_CLASSETR       = 10;
  CO_IDX_IMG_INNATURA_ADD   = 11;
  CO_IDX_IMG_INNATURA_OUT   = 12;
  CO_IDX_IMG_CURTIDO_ADD    = 13;
  CO_IDX_IMG_CURTIDO_OUT    = 14;
  CO_IDX_IMG_CLASSEVI       = 15;

procedure TFmVSArvoreArtigosOld.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSArvoreArtigosOld.DefineImageIndexETextoMovimA(const MovimID, MovimNiv:
  Integer; var Indice: Integer; var Texto: String): Boolean;
const
  SrcMovID = 0;
begin
  DefineImageIndexETextoMovimB(MovimID, MovimNiv, SrcMovID, Indice, Texto);
end;

function TFmVSArvoreArtigosOld.DefineImageIndexETextoMovimB(const MovimID,
  MovimNiv, SrcMovID: Integer; var Indice: Integer; var Texto: String): Boolean;
begin
  Result := True;
  Indice := CO_IDX_IMG_INDEFINIDO;
  Texto := sEstqMovimID_FRENDLY[MovimID];
  case TEstqMovimID(MovimID) of
    emidCompra: Indice := CO_IDX_IMG_INNATURA;
    emidIndsXX:
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminDestCurtiXX: Indice := CO_IDX_IMG_CURTIDO;
        eminSorcCurtiXX: Indice := CO_IDX_IMG_CURTIDO_ADD;
        eminBaixCurtiXX: Indice := CO_IDX_IMG_INNATURA_OUT;
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [1]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      Texto := Texto + ' (' + sEstqMovimNiv[MovimNiv] + ')';
    end;
    emidClassArtXXUni,
    emidClassArtXXMul:
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Indice := CO_IDX_IMG_SEMCLASSE;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [2A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [2B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidReclasVS:
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Indice := CO_IDX_IMG_CLASSETR;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [3A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [3B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidPreReclasse:
    begin
      Indice := CO_IDX_IMG_INDEFINIDO;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        eminSorcPreReclas: Texto := Texto + ' (Item de baixa)';
        eminDestPreReclas: Texto := Texto + ' (Somat�rio dos itens)';
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [4A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcPreReclas:
        begin
          case TEstqMovimID(SrcMovID) of
            emidAjuste: ; //
            emidClassArtXXUni,
            emidClassArtXXMul: Indice := CO_IDX_IMG_CLASSETR;
            emidReclasVS:      Indice := CO_IDX_IMG_CLASSEVI;
            else Geral.MB_Info(
            '"SrcMovID" n�o implementado "DefineImageIndexETextoMovimB" [4C]' +
            sLineBreak + 'SrcMovID = ' + Geral.FF0(SrcMovID));;
          end;
        end;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        eminDestPreReclas: Indice := CO_IDX_IMG_CLASSEVI;
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [4B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidEmOperacao:
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcOper:  Indice := CO_IDX_IMG_CURTIDO_OUT;
        eminEmOperInn: Indice := CO_IDX_IMG_CURTIDO_ADD;
        eminDestOper:  Indice := CO_IDX_IMG_CLASSEVI;
        eminEmOperBxa: Indice := CO_IDX_IMG_CURTIDO_OUT;
        else Geral.MB_Info(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [5]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    else
    begin
      Result := False;
      Indice := 0;
      Texto := '"Image index" n�o definido!' + slineBreak +
      'MovimID = ' + Geral.FF0(MovimID) + slineBreak +
      'MovimNiv = ' + Geral.FF0(MovimNiv);
      Geral.MB_Info(Texto);
    end;
  end;
end;

procedure TFmVSArvoreArtigosOld.DefineQtdeItensDeIMEI(const Qry: TmySQLQuery;
  var QtdItens: Double; var TipoCalc: TTipoCalcCouro);
begin
  if (Qry.FieldByName('MovimID').AsInteger = 1) then
  begin
    QtdItens := Qry.FieldByName('Pecas').AsFloat;
    TipoCalc := ptcPecas;
  end else
  begin
    if Qry.FieldByName('AreaM2').AsFloat > 0 then
    begin
      QtdItens := Qry.FieldByName('AreaM2').AsFloat;
      TipoCalc := ptcAreaM2;
    end else
    begin
      QtdItens := Qry.FieldByName('Pecas').AsFloat;
      TipoCalc := ptcPecas;
    end;
  end;
end;

procedure TFmVSArvoreArtigosOld.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSArvoreArtigosOld.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSArvoreArtigosOld.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeArtigoClassificado(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
var
  QryIMEC, QryIMEP, QryIMEI, QryPrep: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovID_Pai, MovimID, MovimNiv, Controle, SrcMovID,
  PalletFilho: Integer;
  NohMovimCod, NohPallet, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
begin
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryPrep := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  // listar todas cabecalhos de todas Pre Reclassificacoes
  // Pode ter varias para o mesmo Pallet!!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod, MovimID  ',
  'FROM vsmovits ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    MovID_Pai := QryIMEC.FieldByName('MovimID').AsInteger;
    case TEstqMovimID(MovID_Pai) of
      emidPreReclasse: InjetaDadosFilhosDeArtigoClassificado_PreClasse(
                       IMEI, Pallet, Noh_Pai, QryIMEC, QtdItensPai, QtdTotalPai, PercentPai);
      emidEmOperacao: InjetaDadosFilhosDeArtigoClassificado_Operacao(
                       IMEI, Pallet, Noh_Pai, QryIMEC, QtdItensPai, QtdTotalPai, PercentPai);
      else Geral.MB_Erro('"MovimID" n�o implementado! [5]' + sLineBreak +
      'MovimID = ' + Geral.FF0(MovID_Pai) + sLineBreak +
      'IME-I = ' + Geral.FF0(IMEI));
    end;
    QryIMEC.Next;
  end;
  finally
    QryPrep.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeArtigoClassificado_Operacao(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai,
  QtdTotalPai, PercentPai: Double);
var
  MovimCod, Indice, MovimID, MovimNiv, Controle, SrcMovID, PalletFilho,
  MovimTwn, SrcNivel2: Integer;
  Texto: String;
  NohMovimCod, NohPallet, NohIMEI, Noh_07, Noh_08, Noh_09, Noh_10: TTreeNode;
  QryIMEI_07, QryIMEI_08, QryIMEP, QryPall: TmySQLQuery;
  QtdItens, QtdTotal, QtdSoma, PercentNew: Double;
  ContaIMEI: Boolean;
begin
  QryIMEI_07 := TmySQLQuery.Create(Dmod);
  try
  QryIMEI_08 := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  QtdItens := 0;
  QtdTotal := 0;
  // Injetar noh do IMEC
  MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
  Indice := CO_IDX_IMG_OPERCAO;
  Texto  := 'Opera��o em Artigo';
  NohMovimCod := InjetaNodeAtual_A('IME-C ', Texto, MovimCod,
    Indice, Noh_Pai, emitIME_C, 0, 0, 0);
  // Injetar noh para separar Baixas de origem (Niv=7), Totalizador (Niv=8)
  // Geracao de destino (9) + Baixas de destino (10)
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_07 := InjetaNodeAtual_A('Baixas ', Texto, MovimCod,
    Indice, NohMovimCod, emitAvulsos, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_08 := InjetaNodeAtual_A('Em opera��o ', Texto, MovimCod,
    Indice, NohMovimCod, emitAvulsos, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_10 := InjetaNodeAtual_A('Operados ', Texto, MovimCod,
    Indice, NohMovimCod, emitAvulsos, 0, 0, 0);
  //
  QtdSoma  := 0;
  QtdTotal := 0;
  // Abrir o 08 antes para saber o total de area!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_08, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmOperInn)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_08.First;
  while not QryIMEI_08.Eof do
  begin
    QtdTotal := QtdTotal + QryIMEI_08.FieldByName('AreaM2').AsFloat;
    //
    QryIMEI_08.Next;
  end;
  // Abrir o 07 depois mas inserir antes do 08
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_07, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_07.First;
  while not QryIMEI_07.Eof do
  begin
    MovimID  := QryIMEI_07.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_07.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_07.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_07.FieldByName('Controle').AsInteger;
    SrcNivel2 := QryIMEI_07.FieldByName('SrcNivel2').AsInteger;
    // somente do IMEI em pequisa!
    if SrcNivel2 = IMEI then
    begin
      QtdItens  := - QryIMEI_07.FieldByName('AreaM2').AsFloat;
      QtdSoma   := QtdSoma + QtdItens;
      ContaIMEI := True;
    end else
    begin
      QtdItens  := 0;
      ContaIMEI := False;
    end;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    NohIMEI := InjetaNodeAtual_B('IME-I ', Texto, Controle, Indice,
      Noh_07, emitIME_I, QtdItens, QtdTotal, PercentPai, ContaIMEI);
    //
    QryIMEI_07.Next;
  end;
  // Deve ter soh um, mas mostrar todos para evidenciar algum erro!
  QryIMEI_08.First;
  while not QryIMEI_08.Eof do
  begin
    MovimID  := QryIMEI_08.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_08.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_08.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_08.FieldByName('Controle').AsInteger;
    // Resultado da soma acima do 07
    QtdItens := QtdSoma;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      Noh_08, emitIME_I, QtdItens, QtdTotal, PercentPai);
    //
    //
    QryIMEI_08.Next;
  end;

  //
  if QtdTotal = 0 then
    PercentNew := 0
  else
    PercentNew := PercentPai * (QtdSoma / QtdTotal);
  //  Listar gemeos de pallets retornados!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
  'SELECT DISTINCT MovimTwn ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' + Geral.FF0(Integer(eminDestOper)) + // 9
  ', ' + Geral.FF0(Integer(eminEmOperBxa)) + ') ',         // 10
  'ORDER BY MovimCod ',
  '']);
  QryIMEP.First;
  while not QryIMEP.Eof do
  begin
    // Injetar noh do IMEP
    MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de defini��o de pallet de artigo operado';
    Noh_09 := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
      Indice, Noh_10, emitIME_P, 0, 0);

    // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_07, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovits ',
    'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
    'ORDER BY MovimNiv DESC', // 10 depois 09
    '']);
    //
    QryIMEI_07.First;
    while not QryIMEI_07.Eof do
    begin
      MovimID  := QryIMEI_07.FieldByName('MovimID').AsInteger;
      MovimNiv := QryIMEI_07.FieldByName('MovimNiv').AsInteger;
      Controle := QryIMEI_07.FieldByName('Controle').AsInteger;
      Pallet   := QryIMEI_07.FieldByName('Pallet').AsInteger;
      DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
      //
      QtdItens := 0;
      QtdTotal := 0;
      if TEstqMovimNiv(MovimNiv) = eminDestOper then
      begin
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI_07.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          // se nao for pallet, ver do IMEI Gerado!
          UnDmkDAC_PF.AbreMySQLQuery0(QryPall, Dmod.MyDB, [
          'SELECT * ',
          'FROM vsmovits ',
          'WHERE Controle=' + Geral.FF0(Controle),
          '']);
          QtdItens := QryIMEI_07.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end;
      end;
      NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
        Noh_09, emitIME_I, QtdItens, QtdTotal, PercentNew);
      //
      if TEstqMovimNiv(MovimNiv) = eminDestOper then
      begin
(*
        if QtdTotal = 0 then
          PercentNew := 0
        else
          PercentNew := PercentNew * (QtdItens / QtdTotal);
*)
        InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
        QtdItens, QtdTotal, PercentNew);
      end;
      //
      QryIMEI_07.Next;
    end;
    //
    QryIMEP.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEI_08.Free;
  end;
  finally
    QryIMEI_07.Free;
  end;
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeArtigoClassificado_PreClasse(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai,
  QtdTotalPai, PercentPai: Double);
var
  MovimCod, Indice, MovimID, MovimNiv, Controle, SrcMovID, PalletFilho: Integer;
  Texto: String;
  NohMovimCod, NohPallet, NohIMEI: TTreeNode;
  QryIMEI, QryPrep, QryPall: TmySQLQuery;
  QtdItens, QtdTotal, PercentNew: Double;
begin
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPrep := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  // Injetar noh do IMEC
  MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
  Indice := CO_IDX_IMG_PREPARA_RECLAS;
  Texto  := 'Prepara��o de Reclassifica��o de Artigo Classificado';
  NohMovimCod := InjetaNodeAtual_A('IME-C ', Texto, MovimCod,
    Indice, Noh_Pai, emitIME_C, 0, 0, 0);
  // Injetar noh para separar Baixas (Niv=11) do Totalizador (Niv=12)!
  Indice := CO_IDX_IMG_ITENSARECLAS;
  Texto  := 'Prepara��o de Reclassifica��o de Artigo Classificado';
  NohPallet := InjetaNodeAtual_A('Pallet ', Texto, Pallet,
    Indice, NohMovimCod, emitPallet, QtdItensPai, QtdTotalPai, PercentPai);
  // Listar os itens (IME-Is) do pallet a ser reclassificado
  // Pode ter mais de um
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)),
  'ORDER BY Controle ',
  '']);
  //
  QryIMEI.First;
  while not QryIMEI.Eof do
  begin
    MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
    Controle := QryIMEI.FieldByName('Controle').AsInteger;
    PalletFilho := QryIMEI.FieldByName('Pallet').AsInteger;
    //
    if PalletFilho > 0 then
    begin
      VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
      QtdItens := -QryIMEI.FieldByName('AreaM2').AsFloat;
      QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
    end else
    begin
      QtdItens := 0;
      QtdTotal := 0;
    end;
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohPallet, emitIME_I, QtdItens, QtdTotal, PercentPai);
    QryIMEI.Next;
  end;

  // Novo IME-I �nico do pallet preparado para reclassificacao
  // Se houver mais de um � erro. Mostrar itens mesmo assim.
  UnDmkDAC_PF.AbreMySQLQuery0(QryPrep, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestPreReclas)),
  'ORDER BY Controle ',
  '']);
  //
  QryPrep.First;
  while not QryPrep.Eof do
  begin
    MovimID     := QryPrep.FieldByName('MovimID').AsInteger;
    MovimNiv    := QryPrep.FieldByName('MovimNiv').AsInteger;
    Controle    := QryPrep.FieldByName('Controle').AsInteger;
    SrcMovID    := QryPrep.FieldByName('SrcMovID').AsInteger;
    PalletFilho := QryPrep.FieldByName('Pallet').AsInteger;
    //
    if PalletFilho > 0 then
    begin
      VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
      QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
      QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
    end else
    begin
      QtdItens := 0;
      QtdTotal := 0;
    end;
    //
    DefineImageIndexETextoMovimB(MovimID, MovimNiv, SrcMovID, Indice, Texto);
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohMovimCod, emitIME_I, QtdItens, QtdTotal, PercentPai);
    //
    if QtdTotal = 0 then
      PercentNew := 0
    else
    begin
      PercentNew := (QtdItens / QtdTotal);
      if PercentNew < 0 then
        PercentNew := -PercentNew;
      PercentNew := PercentPai * PercentNew;
    end;
    InjetaDadosFilhosDeArtigoPreReclassificado(Controle, PalletFilho, NohIMEI,
    (*QtdItens, QtdTotal,*) PercentNew);
    //
    QryPrep.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryPrep.Free;
  end;
  finally
    QryIMEI.Free;
  end;
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeArtigoGerado(IMEI: Integer;
  Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai: Double);
var
  QryIMEC, QryIMEP, QryIMEI, QryPall: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle, Pallet: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  PercentPai, QtdItens, QtdTotal: Double;
begin
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  if QtdTotalPai = 0 then
    PercentPai := 0
  else
    PercentPai := QtdItensPai / QtdTotalPai * 100;
  // listar todas cabecalhos de Artigos em Classificacao deste Artigo Gerado
  // Deveria ter so um. Mostrar erro caso tenha mais!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod  ',
  'FROM vsmovits ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    // Injetar noh do IMEC
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    Indice := CO_IDX_IMG_CLASSIFICAR;
    Texto  := 'Classifica��o de Artigo Gerado';
    NohMovimCod := InjetaNodeAtual_A('IME-C ', Texto, MovimCod,
      Indice, Noh_Pai, emitIME_C, 0, 0);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn  ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de defini��o de classe';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P, 0, 0);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsmovits ',
      'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle := QryIMEI.FieldByName('Controle').AsInteger;
        Pallet   := QryIMEI.FieldByName('Pallet').AsInteger;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
        //
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          QtdItens := 0;
          QtdTotal := 0;
        end;
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        case TEstqMovimID(MovimID) of
          emidClassArtXXUni,
          emidClassArtXXMul,
          emidPreReclasse:
          begin
            if TEstqMovimNiv(MovimNiv) = eminDestClass then
            begin
              //Fazer Percent Filho := QtdItens / QtdTotal * PercentPai ????
              InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
              QtdItens, QtdTotal, PercentPai);
            end;
          end;
          else Geral.MB_Info('"MovimID n�o implementado (FilhosDeArtigoGerado)"'
          + sLineBreak + 'MovimID: ' + Geral.FF0(MovimID));
        end;
        //
        QryIMEI.Next;
      end;
      //
      QryIMEP.Next;
    end;
    //
    QryIMEC.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeArtigoPreReclassificado(IMEI,
  Pallet: Integer; Noh_Pai: TTreeNode; PercentPai: Double);
var
  QryIMEC, QryIMEP, QryIMEI, QryPall: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle, PalletFilho: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  QtdItens, QtdTotal: Double;
begin
(*
  if IMEI = 12067 then
    Geral.MB_Aviso('12067');
*)
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  // listar todas cabecalhos de Artigos em Reclassificacao deste Artigo Cassificado
  // Pode ter varios???
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod  ',
  'FROM vsmovits ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    // Injetar noh do IMEC
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    Indice := CO_IDX_IMG_CLASSIFICAR;
    Texto  := 'Reclassifica��o de Artigo Classificado';
    NohMovimCod := InjetaNodeAtual_A('IME-C ', Texto, MovimCod, Indice, Noh_Pai,
      emitIME_C);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn  ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de defini��o de classe';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo Classificado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsmovits ',
      'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        MovimID     := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv    := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle    := QryIMEI.FieldByName('Controle').AsInteger;
        PalletFilho := QryIMEI.FieldByName('Pallet').AsInteger;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);

        //
        if (PalletFilho > 0) and (TEstqMovimNiv(MovimNiv) = eminDestClass) then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, PalletFilho);
          QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          QtdItens := 0;
          QtdTotal := 0;
        end;

(*
if Controle = 12078 then
  Geral.MB_Aviso('12078');
*)
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        if TEstqMovimNiv(MovimNiv) = eminDestClass then
          InjetaDadosFilhosDeArtigoClassificado(Controle, PalletFilho, NohIMEI,
          QtdItens, QtdTotal, PercentPai);
        //
        QryIMEI.Next;
        // Parei Aqui
      end;
      //
      QryIMEP.Next;
    end;
    //
    QryIMEC.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigosOld.InjetaDadosFilhosDeInNatura(IMEI: Integer;
  Noh_Pai: TTreeNode);
const
  PercentPai = 100;
var
  QryIMEC, QryIMEP, QryIMEI: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  QtdItens, QtdTotal, QtdSoma: Double;
  TipoCalc: TTipoCalcCouro;
begin
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QtdSoma := 0;
  // listar todas cabecalhos de Artigos Gerados deste In Natura
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod  ',
  'FROM vsmovits ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    // Injetar noh do IMEC
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    Indice := CO_IDX_IMG_PROCESSO;
    Texto  := 'Gera��o de Artigo';
    NohMovimCod := InjetaNodeAtual_A('IME-C ', Texto, MovimCod,
      Indice, Noh_Pai, emitIME_C, 0, 0);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn  ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de Baixa-Gera��o';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P, 0, 0);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsmovits ',
      'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        QtdItens := 0;
        //QtdTotal := 0;
        MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle := QryIMEI.FieldByName('Controle').AsInteger;
        //DefineQtdeItensDeIMEI(QryIMEI, QtdItens, TipoCalc);
        if TEstqMovimNiv(MovimNiv) = eminBaixCurtiXX then // 15
        begin
          if QryIMEI.FieldByName('SrcNivel2').AsInteger = IMEI then
            QtdItens := QtdItens + (-QryIMEI.FieldByName('QtdGerArM2').AsFloat);
        end;
        QtdTotal := QtdItens;
        QtdSoma  := QtdSoma + QtdItens;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        QryIMEI.Next;
      end;
      //
      QryIMEP.Next;
    end;
    //
    // IME-I do totalizando os itens Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
    'ORDER BY MovimNiv ',
    '']);
    // Soh tem um, mas mostra erro sem houver mais!
    while not QryIMEI.Eof do
    begin
      MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
      MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
      Controle := QryIMEI.FieldByName('Controle').AsInteger;
      DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
      QtdItens := QtdSoma;
      QtdTotal := QryIMEI.FieldByName('AreaM2').AsFloat;
      NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
        NohMovimCod, emitIME_I, QtdItens, QtdTotal, PercentPai);
      // Inserir itens classificados do IMEI Atual!
      InjetaDadosFilhosDeArtigoGerado(Controle, NohIMEI, QtdItens, QtdTotal);
      //
      QryIMEI.Next;
    end;
    //
    QryIMEC.Next;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

function TFmVSArvoreArtigosOld.InjetaNodeAtual_A(Titulo, Texto: String; PK, Index:
  Integer; NodePai: TTreeNode; MovimType: TEstqMovimType;
  ItensParte: Double; ItensTotal: Double; PercentPai: Double): TTreeNode;
const
  ContaIMEI = True;
begin
  Result := InjetaNodeAtual_B(Titulo, Texto, PK, Index, NodePai, MovimType,
  ItensParte, ItensTotal, PercentPai, ContaIMEI);
end;

function TFmVSArvoreArtigosOld.InjetaNodeAtual_B(Titulo, Texto: String; PK,
  Index: Integer; NodePai: TTreeNode; MovimType: TEstqMovimType; ItensParte,
  ItensTotal, PercentPai: Double; ContaIMEI: Boolean): TTreeNode;
var
  Sentenca: String;
  Pallet: Integer;
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, Percentual: Double;
begin
  if PK = 12095 then
    Geral.MB_Info('12095');
(*  if PK = 1202 then
    Geral.MB_Info('1202');  *)
  Sentenca := Trim(Titulo) + ' ' + Geral.FF0(PK);
  if Trim(Texto) <> '' then
    Sentenca := Sentenca + ' :: ' + Texto;
  //
  case MovimType of
    //emitIndef=0,
    emitIME_I: //1
    begin
      Qry := TmySQLQuery.Create(Dmod.MyDB);
      try
        VS_PF.ReopenVSIts_Controle_If(Qry, PK);
        //
        Pallet  := Qry.FieldByName('Pallet').AsInteger;
        Pecas   := Qry.FieldByName('Pecas').AsFloat;
        PesoKg  := Qry.FieldByName('PesoKg').AsFloat;
        AreaM2  := Qry.FieldByName('AreaM2').AsFloat;
        //
        if Pallet <> 0 then
          Sentenca := Sentenca + ' Pallet ' + Geral.FF0(Pallet);
        Sentenca := Sentenca + ' ' + Qry.FieldByName('NO_PRD_TAM_COR').AsString + ' (';
        if Pecas <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFI(Pecas) + ' pe�as';
        if PesoKg <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFI(PesoKg) + ' kg';
        if AreaM2 <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFT(AreaM2, 2, siNegativo) + ' m�';
        Sentenca := Sentenca + ')';
      finally
        Qry.Free;
      end;
      if ContaIMEI then
      begin
        SetLength(FItensAtualizados, Length(FItensAtualizados) + 1);
        FItensAtualizados[Length(FItensAtualizados)-1] := PK;
        FAtualizados := FAtualizados + 1;
      end;
    end;
    emitIME_C: ; //2
    emitIME_P: ; //3
    emitPallet: ; //4
    emitAvulsos: ; //5
    else Geral.MB_Info(
    '"MovimType" n�o implementado "InjetaNodeAtual" [1]');
  end;
  if ItensParte <> 0 then
  begin
    if ItensTotal > 0 then
      Percentual := ItensParte / ItensTotal
    else
      Percentual := 0;
    //
    Percentual := Int(Percentual * PercentPai);
    Sentenca := Sentenca + ' (' + Geral.FFI(Percentual) + '%)';
  end;
  if NodePai <> nil then
    Result := TreeView1.Items.AddChild(NodePai, Sentenca)
  else
    Result := TreeView1.Items.Add(nil, Sentenca);
  Result.ImageIndex := Index;
  Result.StateIndex := Index;
(*
  if IMEI = 12017 then
    Geral.MB_Info('12017');
  if IMEI = 12018 then
    Geral.MB_Info('12018');
  if IMEI = 12019 then
    Geral.MB_Info('12019');
*)
end;

procedure TFmVSArvoreArtigosOld.MontaArvore(IMEI_Pai: Integer);
const
  PercentPai = 100;
var
  New013SerieFch, New013Ficha, New013Terceiro: Integer;
  New013Marca, IMEIS, Texto: String;
  //
  Controle, GraGruX, SrcGGX, MovimID, MovimNiv, Indice, ID_Pai,
  Niv_Pai: Integer;
  //
  TvIMEI_06_13, TvTwn_06_to_07, TvIMEI_07_01, TvIMEI_07_02: TTreeNode;
  QtdItens, QtdTotal: Double;
  TipoCalc: TTipoCalcCouro;
begin
  try
  FAtualizados := 0;
  SetLength(FItensAtualizados, 0);
  UnDmkDAC_PF.AbreMySQLQuery0(Qr013, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE Controle=' + Geral.FF0(IMEI_Pai),
  '']);
  //
  ID_Pai  := Qr013MovimID.Value;
  Niv_Pai := Qr013MovimID.Value;
  DefineQtdeItensDeIMEI(Qr013, QtdItens, TipoCalc);
  QtdTotal := QtdItens;
  DefineImageIndexETextoMovimA(ID_Pai, Niv_PAi, Indice, Texto);
  TvIMEI_06_13 := InjetaNodeAtual_A('IME-I ', Texto, IMEI_Pai,
    Indice, nil, emitIME_I, QtdItens, QtdTotal, PercentPai);
  //
  //
  New013SerieFch := Qr013SerieFch.Value;
  New013Ficha    := Qr013Ficha.Value;
  New013Terceiro := Qr013Terceiro.Value;
  New013Marca    := Qr013Marca.Value;
  //
  case TEstqMovimID(ID_Pai) of
    emidCompra:  // 1
    begin
      InjetaDadosFilhosDeInNatura(IMEI_Pai, TvIMEI_06_13);
    end;
{
    emidIndsXX:  // 6
    begin
      // Abrir IME-Gs(Gemeos) (MovimID = 7) filhos do IME-I Pai (MovimID = 6)
      UnDmkDAC_PF.AbreMySQLQuery0(QrTwns, Dmod.MyDB, [
      'SELECT DISTINCT MovimTwn ',
      'FROM vsmovits ',
      'WHERE SrcNivel2=' + Geral.FF0(IMEI_Pai),
      'ORDER BY MovimTwn',
      '']);
      QrTwns.First;
      while not QrTwns.Eof do
      begin
        Indice := CO_IDX_IMG_GEMEOS;
        Texto  := 'Par de g�meos';
        TvTwn_06_to_07 := InjetaNodeAtual_A('IME-P ', Texto, QrTwnsMovimTwn.Value,
          Indice, TvIMEI_06_13, emitIME_P);
        //
        // Abrir IME-Is (MovimID = 7) filhos do IME-I Pai (MovimID = 6) do IME-P (par) do registro atual
        UnDmkDAC_PF.AbreMySQLQuery0(Qr007, Dmod.MyDB, [
        'SELECT Controle, Pecas, PesoKg, AreaM2,  ',
        'SdoVrtPeca, SdoVrtArM2, SrcNivel2, ',
        'GraGruX, MovimTwn, SrcGGX,  ',
        'SerieFch, Ficha, MovimID, MovimNiv  ',
        'FROM vsmovits ',
        'WHERE MovimTwn=' + Geral.FF0(QrTwnsMovimTwn.Value),
        'ORDER BY MovimNiv',
        '']);
        //
        Qr007.First;
        while not Qr007.Eof do
        begin
          Controle := Qr007Controle.Value;
          MovimNiv := Qr007MovimNiv.Value;
          MovimID  := Qr007MovimID.Value;
          //
          DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
          //
          case TEstqMovimNiv(MovimNiv) of
            TEstqMovimNiv.eminSorcClass: TvIMEI_07_01 :=
             InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice, TvTwn_06_to_07, emitIME_I);
            TEstqMovimNiv.eminDestClass:
            begin
              TvIMEI_07_02 := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice, TvTwn_06_to_07, emitIME_I);
              InjetaDadosMovimCod(Controle, MovimID, MovimNiv, TvIMEI_07_02);
            end;
            else Geral.MB_Erro('"MovimNiv" n�o implementado! [1]');
          end;
          Qr007.Next;
        end;
        QrTwns.Next;
      end;
    end;
}
    else Geral.MB_Erro('"MovimID" n�o implementado! [Raiz]' +
    sLineBreak + 'MovimID = ' + Geral.FF0(ID_Pai));
  end;
  //
  IMEIS := VS_PF.CordaIMEIS(FItensAtualizados);
  Geral.MB_Info('Itens atualizados: ' + Geral.FF0(FAtualizados) +
  sLineBreak + 'IMEIS atualizados: ' + (*sLineBreak +*) IMEIs);
  //
  finally
    TreeView1.FullExpand;
  end;
end;

procedure TFmVSArvoreArtigosOld.TreeView1GetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  if Node.Focused then
    Exit;
end;

end.
