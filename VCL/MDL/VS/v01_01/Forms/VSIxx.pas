unit VSIxx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.Mask, UnProjGroup_Consts;

type
  TFmVSIxx = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    Label1: TLabel;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkIDXtr: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsZerado: TSmallintField;
    QrVSMovItsEmFluxo: TSmallintField;
    QrVSMovItsNotFluxo: TIntegerField;
    QrVSMovItsFatNotaVNC: TFloatField;
    QrVSMovItsFatNotaVRC: TFloatField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsGSPInnNiv2: TIntegerField;
    QrVSMovItsGSPArtNiv2: TIntegerField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsItemNFe: TIntegerField;
    QrVSMovItsVSMorCab: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrVSMovItsCustoPQ: TFloatField;
    QrVSMovItsKgCouPQ: TFloatField;
    QrVSMovItsNFeSer: TSmallintField;
    QrVSMovItsNFeNum: TIntegerField;
    QrVSMovItsVSMulNFeCab: TIntegerField;
    QrVSMovItsGGXRcl: TIntegerField;
    QrVSMovItsJmpMovID: TIntegerField;
    QrVSMovItsJmpNivel1: TIntegerField;
    QrVSMovItsJmpNivel2: TIntegerField;
    QrVSMovItsJmpGGX: TIntegerField;
    QrVSMovItsRmsMovID: TIntegerField;
    QrVSMovItsRmsNivel1: TIntegerField;
    QrVSMovItsRmsNivel2: TIntegerField;
    QrVSMovItsRmsGGX: TIntegerField;
    QrVSMovItsGSPSrcMovID: TIntegerField;
    QrVSMovItsGSPSrcNiv2: TIntegerField;
    QrVSMovItsGSPJmpMovID: TIntegerField;
    QrVSMovItsGSPJmpNiv2: TIntegerField;
    QrVSMovItsDtCorrApo: TDateTimeField;
    QrVSMovItsMovCodPai: TIntegerField;
    QrVSMovItsIxxMovIX: TSmallintField;
    QrVSMovItsIxxFolha: TIntegerField;
    QrVSMovItsIxxLinha: TIntegerField;
    DBEdit1: TDBEdit;
    DsVSMovIts: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FQrIMEI: TmySQLQuery;
    FIMEI: Integer;
    FResult: Boolean;
    //
    procedure PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha: Integer);
  end;

  var
  FmVSIxx: TFmVSIxx;

implementation

uses UnMyObjects, Module, UnVS_CRC_PF, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSIxx.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  //
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Controle := FIMEI;
  //
  IxxMovIX := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha := EdIxxFolha.ValueVariant;
  IxxLinha := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_VMI, False, [
  'IxxMovIX', 'IxxFolha', 'IxxLinha'], ['Controle'], [
  IxxMovIX, IxxFolha, IxxLinha], [Controle], True) then
  begin
    FResult := True;
    //
    MyObjects.IncrementaFolhaLinha(EdIxxFolha, EdIxxLinha, CO_VS_MaxLin);
    Close;
  end
end;

procedure TFmVSIxx.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSIxx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSIxx.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FResult := False;
end;

procedure TFmVSIxx.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSIxx.PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(FIMEI),
  '']);
  //
  if QrVSMovItsIxxMovIX.Value > 0 then
  begin
    RGIxxMovIX.ItemIndex    := QrVSMovItsIxxMovIX.Value;
    EdIxxFolha.ValueVariant := QrVSMovItsIxxFolha.Value;
    EdIxxLinha.ValueVariant := QrVSMovItsIxxLinha.Value;
  end else
  if IxxMovIX > 0 then
  begin
    RGIxxMovIX.ItemIndex    := IxxMovIX;
    EdIxxFolha.ValueVariant := IxxFolha;
    EdIxxLinha.ValueVariant := IxxLinha;
  end;
end;

end.
