object FmVSInnCab: TFmVSInnCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-006 :: Entrada de Peles In Natura'
  ClientHeight = 707
  ClientWidth = 1264
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 1264
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 504
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label5: TLabel
        Left = 504
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaPecas: TLabel
        Left = 672
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 752
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 832
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 912
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SbFornecedor: TSpeedButton
        Left = 968
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object SbTransportador: TSpeedButton
        Left = 968
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportadorClick
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object SbClienteMO: TSpeedButton
        Left = 480
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteMOClick
      end
      object Label21: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object SbProcedenc: TSpeedButton
        Left = 480
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbProcedencClick
      end
      object Label22: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 348
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object Label23: TLabel
        Left = 372
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label28: TLabel
        Left = 456
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 492
        Top = 136
        Width = 62
        Height = 13
        Caption = 'NFe entrada:'
      end
      object Label30: TLabel
        Left = 564
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label31: TLabel
        Left = 600
        Top = 136
        Width = 59
        Height = 13
        Caption = 'NFe fornec.:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtCompra: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 44613.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        DatePurpose = dmkdpNone
        OnRedefInPlace = TPDtCompraRedefInPlace
      end
      object EdDtCompra: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 44613.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrada: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 44613.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDtEntrada: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFornecedor: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOME_E_DOC_ENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 13
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 17
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPecas: TdmkEdit
        Left = 672
        Top = 152
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 752
        Top = 152
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 832
        Top = 152
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 27
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 912
        Top = 152
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 28
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 11
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdProcednc: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procednc'
        UpdCampo = 'Procednc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcednc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcednc: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcednc
        TabOrder = 15
        dmkEditCB = EdProcednc
        QryCampo = 'Procednc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMotorista: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Motorista'
        UpdCampo = 'Motorista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorista: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 277
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 19
        dmkEditCB = EdMotorista
        QryCampo = 'Motorista'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPlaca: TdmkEdit
        Left = 372
        Top = 152
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Placa'
        UpdCampo = 'Placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_serie: TdmkEdit
        Left = 456
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 492
        Top = 152
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = Edide_nNFKeyDown
      end
      object Edemi_serie: TdmkEdit
        Left = 564
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_serie'
        UpdCampo = 'emi_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_nNF: TdmkEdit
        Left = 600
        Top = 152
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_nNF'
        UpdCampo = 'emi_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = Edemi_nNFKeyDown
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 538
      Width = 1264
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1124
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 245
      Width = 1264
      Height = 54
      Align = alTop
      Caption = ' Avisos: '
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1260
        Height = 37
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label36: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label37: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object ProgressBar1: TProgressBar
          Left = 0
          Top = 20
          Width = 1260
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 1264
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 181
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 468
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 584
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 700
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 748
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 604
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Frete:'
      end
      object Label18: TLabel
        Left = 676
        Top = 136
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object Label19: TLabel
        Left = 828
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label9: TLabel
        Left = 916
        Top = 136
        Width = 50
        Height = 13
        Caption = 'Valor total:'
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 176
        Height = 13
        Caption = 'Motorista: (Tip.entid.: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 444
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label32: TLabel
        Left = 196
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label33: TLabel
        Left = 232
        Top = 136
        Width = 62
        Height = 13
        Caption = 'NFe entrada:'
      end
      object Label34: TLabel
        Left = 320
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label35: TLabel
        Left = 356
        Top = 136
        Width = 59
        Height = 13
        Caption = 'NFe fornec.:'
      end
      object Label38: TLabel
        Left = 816
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data encer. rendim.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label40: TLabel
        Left = 528
        Top = 136
        Width = 46
        Height = 13
        Caption = 'Valor MP:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSInnCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSInnCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 333
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSInnCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 468
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsVSInnCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 584
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsVSInnCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 700
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsVSInnCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSInnCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsVSInnCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSInnCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsVSInnCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsVSInnCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 748
        Top = 152
        Width = 77
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSInnCab
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 828
        Top = 152
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSInnCab
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 604
        Top = 152
        Width = 69
        Height = 21
        DataField = 'CusFrtAvMoER'
        DataSource = DsVSInnCab
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 676
        Top = 152
        Width = 68
        Height = 21
        DataField = 'CustoComiss'
        DataSource = DsVSInnCab
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 916
        Top = 152
        Width = 81
        Height = 21
        DataField = 'ValorT'
        DataSource = DsVSInnCab
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsVSInnCab
        TabOrder = 16
      end
      object DBEdit17: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsVSInnCab
        TabOrder = 17
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 152
        Width = 32
        Height = 21
        DataField = 'Motorista'
        DataSource = DsVSInnCab
        TabOrder = 18
      end
      object DBEdit19: TDBEdit
        Left = 48
        Top = 152
        Width = 145
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsVSInnCab
        TabOrder = 19
      end
      object DBEdit20: TDBEdit
        Left = 444
        Top = 152
        Width = 80
        Height = 21
        DataField = 'Placa'
        DataSource = DsVSInnCab
        TabOrder = 20
      end
      object DBEdit21: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsVSInnCab
        TabOrder = 21
      end
      object DBEdit22: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsVSInnCab
        TabOrder = 22
      end
      object DBEdit23: TDBEdit
        Left = 196
        Top = 152
        Width = 33
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsVSInnCab
        TabOrder = 23
      end
      object DBEdit24: TDBEdit
        Left = 232
        Top = 152
        Width = 85
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsVSInnCab
        TabOrder = 24
      end
      object DBEdit25: TDBEdit
        Left = 320
        Top = 152
        Width = 33
        Height = 21
        DataField = 'emi_serie'
        DataSource = DsVSInnCab
        TabOrder = 25
      end
      object DBEdit26: TDBEdit
        Left = 356
        Top = 152
        Width = 85
        Height = 21
        DataField = 'emi_nNF'
        DataSource = DsVSInnCab
        TabOrder = 26
      end
      object DBEdit27: TDBEdit
        Left = 816
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtEnceRend_TXT'
        DataSource = DsVSInnCab
        TabOrder = 27
      end
      object GroupBox3: TGroupBox
        Left = 1002
        Top = 11
        Width = 103
        Height = 164
        Caption = ' Confer'#234'ncia: '
        TabOrder = 28
        object Label39: TLabel
          Left = 8
          Top = 16
          Width = 28
          Height = 13
          Caption = '$ MP:'
          FocusControl = EdPagM
        end
        object Label42: TLabel
          Left = 8
          Top = 56
          Width = 36
          Height = 13
          Caption = '$ Frete:'
          FocusControl = EdPagM
        end
        object Label43: TLabel
          Left = 8
          Top = 96
          Width = 57
          Height = 13
          Caption = '$ Comiss'#227'o:'
          FocusControl = EdPagM
        end
        object EdPagM: TdmkEdit
          Left = 8
          Top = 32
          Width = 88
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPagT: TdmkEdit
          Left = 8
          Top = 72
          Width = 88
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPagC: TdmkEdit
          Left = 8
          Top = 112
          Width = 88
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object DBEdit28: TDBEdit
        Left = 528
        Top = 152
        Width = 73
        Height = 21
        DataField = 'CusFrtAvMoER'
        DataSource = DsVSInnCab
        TabOrder = 29
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 537
      Width = 1264
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 106
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 280
        Top = 15
        Width = 982
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 872
          Top = 0
          Width = 110
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 0
            Top = 4
            Width = 104
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 420
          Left = 109
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtReclasif: TBitBtn
          Tag = 421
          Left = 525
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gera artigos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtReclasifClick
        end
        object BtSubProduto: TBitBtn
          Tag = 605
          Left = 421
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sub Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSubProdutoClick
        end
        object BtTribIncIts: TBitBtn
          Tag = 502
          Left = 629
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtTribIncItsClick
        end
        object BtFinanceiro: TBitBtn
          Tag = 1000156
          Left = 733
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Financeiro'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtFinanceiroClick
        end
        object BtFiscal: TBitBtn
          Tag = 1000516
          Left = 213
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&NF-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtFiscalClick
        end
        object BtCTe: TBitBtn
          Tag = 1000516
          Left = 317
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = 'C&T-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = BtCTeClick
        end
      end
    end
    object PCItens: TPageControl
      Left = 8
      Top = 216
      Width = 1008
      Height = 285
      ActivePage = TabSheet1
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Dados da entrada'
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GBIts: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 257
            Align = alClient
            Caption = ' Itens da entrada: '
            TabOrder = 0
            object Splitter3: TSplitter
              Left = 2
              Top = 69
              Width = 996
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitTop = 99
              ExplicitWidth = 998
            end
            object Splitter1: TSplitter
              Left = 581
              Top = 15
              Width = 5
              Height = 54
              Align = alRight
              ExplicitLeft = 734
              ExplicitHeight = 81
            end
            object DGDados: TDBGrid
              Left = 2
              Top = 15
              Width = 579
              Height = 54
              Align = alClient
              DataSource = DsVSInnIts
              PopupMenu = PMVInnIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'StqCenLoc'
                  Title.Caption = 'Local'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ReqMovEstq'
                  Title.Caption = 'N'#176' RME'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_SerieFch'
                  Title.Caption = 'S'#233'rie RMP'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Title.Caption = 'Ficha RMP'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Marca'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Mat'#233'ria-prima'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CustoKg'
                  Title.Caption = 'Custo kg'
                  Width = 56
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'RendKgm2_TXT'
                  Title.Alignment = taCenter
                  Title.Caption = 'Rend. Kg/m'#178
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Misturou_TXT'
                  Title.Caption = 'Mistura'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'NotaMPAG_TXT'
                  Title.Alignment = taCenter
                  Title.Caption = 'Nota MPAG'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorMP'
                  Title.Caption = 'Valor MP'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CustoComiss'
                  Title.Caption = '$ comiss'#227'o'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CredValrImposto'
                  Title.Caption = 'Cr'#233'd.Tributos'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CusFrtAvuls'
                  Title.Caption = 'Frete compra'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CusFrtMOEnv'
                  Title.Caption = 'Frete envio MO'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor total'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoVrtPeca'
                  Title.Caption = 'Sdo virtual p'#231
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGerArM2'
                  Title.Caption = 'm'#178' gerados'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end>
            end
            object PCSubItens: TPageControl
              Left = 586
              Top = 15
              Width = 412
              Height = 54
              ActivePage = TabSheet4
              Align = alRight
              TabOrder = 1
              object TabSheet4: TTabSheet
                Caption = 'Tributos do item '
                ImageIndex = 1
                object DBGrid4: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 404
                  Height = 26
                  Align = alClient
                  DataSource = DsTribIncIts
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_Tributo'
                      Title.Caption = 'Tributo'
                      Width = 68
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Percent'
                      Title.Caption = '%'
                      Width = 36
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValrTrib'
                      Title.Caption = 'Valor '
                      Width = 72
                      Visible = True
                    end>
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'NFs do item  (simples)'
                ImageIndex = 2
                object DBGrid5: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 404
                  Height = 48
                  Align = alClient
                  DataSource = DsVSInnNFs
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'ide_nNF'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'emi_nNF'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Width = 62
                      Visible = True
                    end>
                end
              end
            end
            object PCBottom: TPageControl
              Left = 2
              Top = 74
              Width = 996
              Height = 181
              ActivePage = TabSheet7
              Align = alBottom
              TabOrder = 2
              object TabSheet7: TTabSheet
                Caption = ' Itens baixados '
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 153
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Splitter4: TSplitter
                    Left = 629
                    Top = 0
                    Height = 153
                    Align = alRight
                    ExplicitLeft = 622
                    ExplicitHeight = 154
                  end
                  object DBGrid3: TDBGrid
                    Left = 632
                    Top = 0
                    Width = 356
                    Height = 153
                    Align = alRight
                    DataSource = DsVSSubPrdIts
                    PopupMenu = PMVInnIts
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'NO_TTW'
                        Title.Caption = 'Tabela'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'IME-I'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'GraGruX'
                        Title.Caption = 'Reduzido'
                        Width = 48
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SeqSifDipoa'
                        Title.Caption = 'SIF/DIPOA'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_PRD_TAM_COR'
                        Title.Caption = 'Mat'#233'ria-prima'
                        Width = 152
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'PesoKg'
                        Title.Caption = 'Peso kg'
                        Width = 72
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SdoVrtPeso'
                        Title.Caption = 'Sdo virtual kg'
                        Width = 68
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_SerieFch'
                        Title.Caption = 'S'#233'rie RMP'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Ficha'
                        Title.Caption = 'Ficha RMP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Marca'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Observ'
                        Title.Caption = 'Observa'#231#245'es'
                        Width = 300
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValorT'
                        Title.Caption = 'Valor total'
                        Width = 72
                        Visible = True
                      end>
                  end
                  object GroupBox2: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 629
                    Height = 153
                    Align = alClient
                    Caption = 'Itens baixados do item de entrada selecionado acima: '
                    TabOrder = 1
                    object Splitter5: TSplitter
                      Left = 561
                      Top = 15
                      Height = 136
                      ExplicitLeft = 525
                      ExplicitHeight = 138
                    end
                    object DBGBaixa: TDBGrid
                      Left = 2
                      Top = 15
                      Width = 559
                      Height = 136
                      Align = alLeft
                      DataSource = DsVSItsBxa
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      OnDblClick = DBGBaixaDblClick
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'NO_TTW'
                          Title.Caption = 'Tabela'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DataHora'
                          Title.Caption = 'Data/hora'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'MovimID'
                          Title.Caption = 'Mov.ID'
                          Width = 38
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Controle'
                          Title.Caption = 'IME-I'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Pecas'
                          Title.Caption = 'Pe'#231'as'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QtdGerArM2'
                          Title.Caption = 'm'#178' gerados'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'PesoKg'
                          Title.Caption = 'Peso kg'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CustoKg'
                          Title.Caption = 'Custo kg'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ValorT'
                          Title.Caption = 'Valor total'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NotaMPAG'
                          Title.Caption = 'Nota MPAG'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Kg/Couro'
                          Width = 52
                          Visible = True
                        end>
                    end
                    object GroupBox1: TGroupBox
                      Left = 564
                      Top = 15
                      Width = 63
                      Height = 136
                      Align = alClient
                      Caption = 'Itens gerados do item de baixa selecionado ao lado: '
                      TabOrder = 1
                      object DBGrid2: TDBGrid
                        Left = 2
                        Top = 15
                        Width = 59
                        Height = 119
                        Align = alClient
                        DataSource = DsVSItsGer
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'NO_TTW'
                            Title.Caption = 'Tabela'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Controle'
                            Title.Caption = 'IME-I'
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Misturou_TXT'
                            Title.Caption = 'Mistura'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NotaMPAG'
                            Title.Caption = 'Nota MPAG'
                            Width = 68
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Artigo'
                            Width = 195
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Pecas'
                            Title.Caption = 'Pe'#231'as'
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'AreaM2'
                            Title.Caption = #193'rea m'#178
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'AreaP2'
                            Title.Caption = #193'rea ft'#178
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ValorT'
                            Title.Caption = 'Valor total'
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'PesoKg'
                            Title.Caption = 'Peso kg'
                            Width = 72
                            Visible = True
                          end>
                      end
                    end
                  end
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'NF-es de entrada'
                ImageIndex = 1
                object Splitter6: TSplitter
                  Left = 0
                  Top = 85
                  Width = 988
                  Height = 5
                  Cursor = crVSplit
                  Align = alTop
                  ExplicitTop = 65
                end
                object DBGInnNFsIts: TDBGrid
                  Left = 0
                  Top = 90
                  Width = 988
                  Height = 63
                  Align = alClient
                  DataSource = DsEfdInnNFsIts
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Conta'
                      Title.Caption = 'ID'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CFOP'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NCM'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGru1'
                      Title.Caption = 'C'#243'digo'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR'
                      Title.Caption = 'Nome do Produto'
                      Width = 180
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UNID'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTD'
                      Title.Caption = 'Quantid.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'xLote'
                      Title.Caption = 'Lote'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ITEM'
                      Title.Caption = '$ Item'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DESC'
                      Title.Caption = '$ Desc.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CST_ICMS'
                      Title.Caption = 'CST ICMS'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS'
                      Title.Caption = '$ BC ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_ICMS'
                      Title.Caption = '% ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS'
                      Title.Caption = '$ ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CST_IPI'
                      Title.Caption = 'CST IPI'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_IPI'
                      Title.Caption = '$ BC IPI'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_IPI'
                      Title.Caption = '% IPI'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_IPI'
                      Title.Caption = '$ IPI'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CST_PIS'
                      Title.Caption = 'CST PIS'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_PIS'
                      Title.Caption = '$ BC PIS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_PIS_p'
                      Title.Caption = '% PIS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_PIS'
                      Title.Caption = '$ PIS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CST_COFINS'
                      Title.Caption = 'CST COFINS'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_COFINS'
                      Title.Caption = '$ BC COFINS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_COFINS_p'
                      Title.Caption = '% COFINS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_COFINS'
                      Title.Caption = '$ COFINS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS_ST'
                      Title.Caption = '$ BC ICMS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_ST'
                      Title.Caption = '% ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS_ST'
                      Title.Caption = '$ ICMS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_ENQ'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_CTA'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ABAT_NT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QUANT_BC_PIS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_PIS_r'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QUANT_BC_COFINS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_COFINS_r'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DtCorrApo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ori_IPIpIPI'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_MOV'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_APUR'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_vProd'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_vFrete'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_vSeg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_vOutro'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ori_IPIvIPI'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GerBxaEstq'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_NAT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UnidMed'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLAUNIDMED'
                      Title.Caption = 'Unidade'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ex_TIPI'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Grandeza'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Tipo_Item'
                      Visible = True
                    end>
                end
                object DBGInnNFsCab: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 85
                  Align = alTop
                  DataSource = DsEfdInnNFsCab
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Terceiro'
                      Title.Caption = 'Fornecedor'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_TER'
                      Title.Caption = 'Nome do fornecedor'
                      Width = 140
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SER'
                      Title.Caption = 'S'#233'rie'
                      Width = 26
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUM_DOC'
                      Title.Caption = 'NF'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_DOC'
                      Title.Caption = 'Emiss'#227'o'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_E_S'
                      Title.Caption = 'Entrada'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DOC'
                      Title.Caption = '$ Docum.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DESC'
                      Title.Caption = '$ Desc.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_MERC'
                      Title.Caption = '$ Produtos'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_FRT'
                      Title.Caption = '$ Frete'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_SEG'
                      Title.Caption = '$ Seguro'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_OUT_DA'
                      Title.Caption = '$ Outros'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS'
                      Title.Caption = '$ BC ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS'
                      Title.Caption = '$ ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_IPI'
                      Title.Caption = '$ IPI'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_PIS'
                      Title.Caption = '$ PIS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_COFINS'
                      Title.Caption = '$ COFINS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_PIS_ST'
                      Title.Caption = '$ PIS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_COFINS_ST'
                      Title.Caption = '$ COFINS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS_ST'
                      Title.Caption = '$ BC ICMS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS_ST'
                      Title.Caption = '$ ICMS ST'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_FatID'
                      Title.Caption = 'FatID NFe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_FatNum'
                      Title.Caption = 'Fat.Num.NFe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_StaLnk'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovFatID'
                      Title.Caption = 'FatID Mov'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovFatNum'
                      Title.Caption = 'Fat.Num. Mov'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovimCod'
                      Title.Caption = 'IME-C'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_MOD'
                      Title.Caption = 'Modelo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CHV_NFE'
                      Title.Caption = 'Chave NFe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_SIT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_PGTO'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_FRT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFeStatus'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CliInt'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaP2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motorista'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Placa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ABAT_NT'
                      Visible = True
                    end>
                end
              end
              object TabSheet9: TTabSheet
                Caption = 'CT-es de entrada'
                ImageIndex = 2
                object DBGrid7: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 153
                  Align = alClient
                  DataSource = DsEfdInnCTsCab
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Terceiro'
                      Title.Caption = 'Fornecedor'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_TER'
                      Title.Caption = 'Nome do fornecedor'
                      Width = 140
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SER'
                      Title.Caption = 'S'#233'rie'
                      Width = 26
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SUB'
                      Title.Caption = 'Sub'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUM_DOC'
                      Title.Caption = 'NF'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_DOC'
                      Title.Caption = 'Emiss'#227'o'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_A_P'
                      Title.Caption = 'Aquisi'#231#227'o'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_MERC'
                      Title.Caption = '$ Servi'#231'os'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DESC'
                      Title.Caption = '$ Desc.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DOC'
                      Title.Caption = '$ Docum.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS'
                      Title.Caption = '$ BC ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS'
                      Title.Caption = '$ ICMS'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_FatID'
                      Title.Caption = 'FatID NFe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_FatNum'
                      Title.Caption = 'Fat.Num.NFe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFe_StaLnk'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovFatID'
                      Title.Caption = 'FatID Mov'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovFatNum'
                      Title.Caption = 'Fat.Num. Mov'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MovimCod'
                      Title.Caption = 'IME-C'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_MOD'
                      Title.Caption = 'Modelo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CHV_CTE'
                      Title.Caption = 'Chave CTe'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_SIT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_FRT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CTeStatus'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CliInt'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaP2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motorista'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Placa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_NT'
                      Title.Caption = 'VL_ABAT_NT'
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Hist'#243'rico da Ficha RMP selecionada'
        ImageIndex = 1
        object Splitter2: TSplitter
          Left = 870
          Top = 0
          Width = 5
          Height = 257
          ExplicitHeight = 260
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 870
          Height = 257
          Align = alLeft
          DataSource = DsVSHisFch
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Palavras chave [TAG]'
              Width = 187
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Texto hist'#243'rico:'
              Width = 460
              Visible = True
            end>
        end
        object DBMemo1: TDBMemo
          Left = 875
          Top = 0
          Width = 125
          Height = 257
          Align = alClient
          DataField = 'Observ'
          DataSource = DsVSHisFch
          TabOrder = 1
        end
      end
      object TsFrCompr: TTabSheet
        Caption = 'Dados frete compra (entrada empresa)'
        ImageIndex = 2
        object PnFrCompr: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvAvu: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 257
            Align = alLeft
            DataSource = DsVSMOEnvAvu
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvAVMI: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 257
            Align = alClient
            DataSource = DsVSMOEnvAVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 3
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 257
            Align = alLeft
            DataSource = DsVSMOEnvEnv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvEVMI: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 257
            Align = alClient
            DataSource = DsVSMOEnvEVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Pagamentos'
        ImageIndex = 4
        object GroupBox7: TGroupBox
          Left = 0
          Top = 0
          Width = 216
          Height = 257
          Align = alClient
          Caption = ' Fornecedor: '
          TabOrder = 0
          object GridF: TDBGrid
            Left = 2
            Top = 15
            Width = 212
            Height = 240
            Align = alClient
            Color = clWhite
            DataSource = DsEmissM
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 300
                Visible = True
              end>
          end
        end
        object GroupBox6: TGroupBox
          Left = 608
          Top = 0
          Width = 392
          Height = 257
          Align = alRight
          Caption = ' Comiss'#227'o: '
          TabOrder = 1
          object GridT: TDBGrid
            Left = 2
            Top = 15
            Width = 388
            Height = 240
            Align = alClient
            Color = clWhite
            DataSource = DsEmissC
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 300
                Visible = True
              end>
          end
        end
        object GroupBox5: TGroupBox
          Left = 216
          Top = 0
          Width = 392
          Height = 257
          Align = alRight
          Caption = ' Tranportadora: '
          TabOrder = 2
          object DBGrid6: TDBGrid
            Left = 2
            Top = 15
            Width = 388
            Height = 240
            Align = alClient
            Color = clWhite
            DataSource = DsEmissT
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 300
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 1000
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 328
        Height = 32
        Caption = 'Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 32
        Caption = 'Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 328
        Height = 32
        Caption = 'Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1264
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1260
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrVSInnCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSInnCabBeforeOpen
    AfterOpen = QrVSInnCabAfterOpen
    BeforeClose = QrVSInnCabBeforeClose
    AfterScroll = QrVSInnCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0')
    Left = 212
    Top = 49
    object QrVSInnCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSInnCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSInnCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSInnCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrVSInnCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSInnCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrVSInnCabFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrVSInnCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSInnCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSInnCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSInnCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSInnCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSInnCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSInnCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSInnCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSInnCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSInnCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSInnCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSInnCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrVSInnCabProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrVSInnCabMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrVSInnCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrVSInnCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSInnCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrVSInnCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
    object QrVSInnCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSInnCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSInnCabide_serie: TSmallintField
      FieldName = 'ide_serie'
    end
    object QrVSInnCabemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrVSInnCabemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrVSInnCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSInnCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
    end
    object QrVSInnCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrVSInnCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrVSInnCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
    end
    object QrVSInnCabDtEnceRend: TDateTimeField
      FieldName = 'DtEnceRend'
    end
    object QrVSInnCabTpEnceRend: TSmallintField
      FieldName = 'TpEnceRend'
    end
    object QrVSInnCabDtEnceRend_TXT: TWideStringField
      DisplayWidth = 20
      FieldName = 'DtEnceRend_TXT'
    end
    object QrVSInnCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSInnCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVSInnCabValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSInnCabCustoComiss: TFloatField
      FieldName = 'CustoComiss'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSInnCabCusFrtAvMoER: TFloatField
      FieldName = 'CusFrtAvMoER'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSInnCabCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrVSInnCabUF_EMPRESA: TSmallintField
      FieldName = 'UF_EMPRESA'
    end
    object QrVSInnCabUF_FORNECE: TSmallintField
      FieldName = 'UF_FORNECE'
    end
  end
  object DsVSInnCab: TDataSource
    DataSet = QrVSInnCab
    Left = 212
    Top = 97
  end
  object QrVSInnIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSInnItsBeforeClose
    AfterScroll = QrVSInnItsAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,'
      'IF(wmi.SdoVrtPeca > 0, 0,'
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),'
      '  ",", ""), ".", ",")) RendKgm2_TXT,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE('
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,'
      'IF(wmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT,'
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3),'
      '  ",", ""), ".", ",")) m2_CouroTXT,'
      'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE wmi.MovimCod=0'
      'ORDER BY NO_Pallet, wmi.Controle')
    Left = 292
    Top = 49
    object QrVSInnItsCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSInnItsCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSInnItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSInnItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSInnItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSInnItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSInnItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSInnItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSInnItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSInnItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSInnItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSInnItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSInnItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSInnItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSInnItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSInnItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSInnItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSInnItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSInnItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSInnItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSInnItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSInnItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSInnItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSInnItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSInnItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSInnItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSInnItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSInnItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSInnItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSInnItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSInnItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSInnItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSInnItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSInnItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSInnItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSInnItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSInnItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSInnItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSInnItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSInnItsRendKgm2: TFloatField
      FieldName = 'RendKgm2'
      DisplayFormat = '0.000'
    end
    object QrVSInnItsNotaMPAG_TXT: TWideStringField
      FieldName = 'NotaMPAG_TXT'
      Size = 30
    end
    object QrVSInnItsRendKgm2_TXT: TWideStringField
      FieldName = 'RendKgm2_TXT'
      Size = 30
    end
    object QrVSInnItsMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Size = 3
    end
    object QrVSInnItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSInnItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSInnItsm2_CouroTXT: TWideStringField
      FieldName = 'm2_CouroTXT'
      Size = 30
    end
    object QrVSInnItsKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
      DisplayFormat = '0.000'
    end
    object QrVSInnItsVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSInnItsClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSInnItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSInnItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSInnItsCustoComiss: TFloatField
      FieldName = 'CustoComiss'
      Required = True
    end
    object QrVSInnItsPerceComiss: TFloatField
      FieldName = 'PerceComiss'
      Required = True
    end
    object QrVSInnItsCusKgComiss: TFloatField
      FieldName = 'CusKgComiss'
      Required = True
    end
    object QrVSInnItsCredValrImposto: TFloatField
      FieldName = 'CredValrImposto'
      Required = True
    end
    object QrVSInnItsCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
      Required = True
    end
    object QrVSInnItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVSInnItsRpICMS: TFloatField
      FieldName = 'RpICMS'
      Required = True
    end
    object QrVSInnItsRpICMSST: TFloatField
      FieldName = 'RpICMSST'
      Required = True
    end
    object QrVSInnItsRpPIS: TFloatField
      FieldName = 'RpPIS'
      Required = True
    end
    object QrVSInnItsRpCOFINS: TFloatField
      FieldName = 'RpCOFINS'
      Required = True
    end
    object QrVSInnItsRvICMS: TFloatField
      FieldName = 'RvICMS'
      Required = True
    end
    object QrVSInnItsRvICMSST: TFloatField
      FieldName = 'RvICMSST'
      Required = True
    end
    object QrVSInnItsRvPIS: TFloatField
      FieldName = 'RvPIS'
      Required = True
    end
    object QrVSInnItsRvCOFINS: TFloatField
      FieldName = 'RvCOFINS'
      Required = True
    end
    object QrVSInnItsRpIPI: TFloatField
      FieldName = 'RpIPI'
      Required = True
    end
    object QrVSInnItsRvIPI: TFloatField
      FieldName = 'RvIPI'
      Required = True
    end
    object QrVSInnItsCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,##0.0000'
    end
  end
  object DsVSInnIts: TDataSource
    DataSet = QrVSInnIts
    Left = 296
    Top = 97
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 440
    Top = 624
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AtrelamentoFreteCompra1: TMenuItem
      Caption = 'Atrelamento Frete Compra'
      OnClick = AtrelamentoFreteCompra1Click
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      OnClick = AtrelamentoNFsdeMO1Click
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object NFes1: TMenuItem
      Caption = 'NF-es'
      object IncluiNFe1: TMenuItem
        Caption = '&Inclui NF-e'
        OnClick = IncluiNFe1Click
      end
      object AlteraNFe1: TMenuItem
        Caption = '&Altera NF-e'
        OnClick = AlteraNFe1Click
      end
      object ExcluiNFe1: TMenuItem
        Caption = '&Exclui NF-e'
        OnClick = ExcluiNFe1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizaestoque1: TMenuItem
      Caption = 'Atuali&za estoque'
      OnClick = Atualizaestoque1Click
    end
    object Corrigepesodebaixas1: TMenuItem
      Caption = '&Corrige peso de baixas'
      OnClick = Corrigepesodebaixas1Click
    end
    object Histrico1: TMenuItem
      Caption = '&Hist'#243'rico'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
        OnClick = Altera1Click
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui1Click
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 316
    Top = 620
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 844
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrFornecedorNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 844
    Top = 44
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 924
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 924
    Top = 48
  end
  object QrVSItsBxa: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSItsBxaBeforeClose
    AfterScroll = QrVSItsBxaAfterScroll
    Left = 244
    Top = 333
    object QrVSItsBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSItsBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSItsBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSItsBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSItsBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSItsBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSItsBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSItsBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSItsBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSItsBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSItsBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSItsBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSItsBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSItsBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSItsBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSItsBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSItsBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSItsBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSItsBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSItsBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSItsBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSItsBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSItsBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSItsBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSItsBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSItsBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSItsBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSItsBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSItsBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '#,##0.00'
    end
    object QrVSItsBxaPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSItsBxaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSItsBxaStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSItsBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSItsBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSItsBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSItsBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSItsBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSItsBxaKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
      DisplayFormat = '0.000'
    end
    object QrVSItsBxaCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,##0.0000'
    end
  end
  object DsVSItsBxa: TDataSource
    DataSet = QrVSItsBxa
    Left = 244
    Top = 381
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 28
    Top = 36
    object AnliseMPAG1: TMenuItem
      Caption = 'An'#225'lise MPAG do IME-I selecionado Modelo 1'
      OnClick = AnliseMPAG1Click
    end
    object AnliseMPAGdoIMEIselecionadoModelo1A1: TMenuItem
      Caption = 'An'#225'lise MPAG do IME-I selecionado Modelo 1-A'
      OnClick = AnliseMPAGdoIMEIselecionadoModelo1A1Click
    end
    object AnliseMPAG2: TMenuItem
      Caption = 'An'#225'lise MPAG do IME-I selecionado Modelo 2'
      OnClick = AnliseMPAG2Click
    end
    object AnliseMPAGdoIMEIselecionadoModelo2A1: TMenuItem
      Caption = 'An'#225'lise MPAG do IME-I selecionado Modelo 2-A'
      OnClick = AnliseMPAGdoIMEIselecionadoModelo2A1Click
    end
    object Resultados1: TMenuItem
      Caption = 'Resultados'
      OnClick = Resultados1Click
    end
    object MapadeDefeitos1: TMenuItem
      Caption = 'Mapa de Defeitos'
      OnClick = MapadeDefeitos1Click
    end
    object ClassesGeradas1: TMenuItem
      Caption = 'Classes Geradas'
      OnClick = ClassesGeradas1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
    object Encerrarendimento1: TMenuItem
      Caption = 'Encerra rendimento'
      OnClick = Encerrarendimento1Click
    end
  end
  object QrVSItsGer: TMySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 333
    object QrVSItsGerCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSItsGerControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSItsGerMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSItsGerMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSItsGerMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSItsGerEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSItsGerTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSItsGerCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSItsGerMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSItsGerDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSItsGerPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSItsGerGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSItsGerPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsGerPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsGerAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSItsGerSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSItsGerSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSItsGerSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSItsGerSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSItsGerSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsGerSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSItsGerSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSItsGerFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSItsGerMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSItsGerFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSItsGerCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSItsGerDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSItsGerDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSItsGerDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSItsGerQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSItsGerQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsGerQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSItsGerQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSItsGerQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSItsGerNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSItsGerPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSItsGerMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSItsGerStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSItsGerNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSItsGerNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSItsGerNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSItsGerID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSItsGerReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSItsGerMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Size = 3
    end
    object QrVSItsGerNO_FORNECMO: TWideStringField
      FieldName = 'NO_FORNECMO'
      Size = 100
    end
    object QrVSItsGerCusto_m2: TFloatField
      FieldName = 'Custo_m2'
    end
  end
  object DsVSItsGer: TDataSource
    DataSet = QrVSItsGer
    Left = 328
    Top = 381
  end
  object frxWET_CURTI_006_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GFGeradosOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Altura: Double;'
      '  Visivel: Boolean;                                             '
      'begin'
      '  if <VARF_REGISTROS_GERADOS> < 2 then  '
      '    Altura := 0'
      '  else              '
      '    Altura := 0.55;        '
      '  //GFGerados.Height := Altura;'
      
        '  Visivel := Altura > 0;                                        ' +
        '            '
      '  MeTitTot.Visible := Visivel;        '
      '  MeTotPec.Visible := Visivel;        '
      '  MeTotAM2.Visible := Visivel;        '
      '  MeFiller.Visible := Visivel;        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_006_01GetValue
    Left = 400
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
      end
      item
        DataSet = frxDsVSInnCab
        DataSetName = 'frxDsVSInnCab'
      end
      item
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
      end
      item
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
      end
      item
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
      end
      item
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504020000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'An'#225'lise MPAG Modelo 1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP: [frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 64.252010000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEntrada"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000010000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."MovimCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 83.149660000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_TRANSPORTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 151.181200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima In Natura:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 102.047310000000000000
          Width = 529.134200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."GraGruX"] - [frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 170.078801180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 480.000310000000000000
        Visible = False
        Width = 680.315400000000000000
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
        RowCount = 0
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DataField = 'KgMedioCouro'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999980000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do Artigo de Ribeira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999980000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IME-I Gerou')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 15.118119999999980000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mistura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 15.118119999999980000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSItsBxa."QtdGerArM2">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 15.118119999999980000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Prestador MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 15.118119999999980000
          Width = 30.236093540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 15.118119999999980000
          Width = 41.574803149606300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 532.913730000000000000
        Visible = False
        Width = 680.315400000000000000
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
        RowCount = 0
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999860000
          Width = 45.354323390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 113.385826771654000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'Misturou_TXT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999860000
          Width = 589.606643390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECMO'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_FORNECMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOKg'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'QtdAntPeso'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."QtdAntPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOTot'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118119999999860000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Custo_m2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999860000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ / m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."Controle"'
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg / couro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 585.827150000000000000
        Visible = False
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 937.323440000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 657.638220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSMovIts."MovimID"'
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677180000000020000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677180000000020000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 22.677180000000020000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 22.677180000000020000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsVSMovIts."MovimID">)] - [frxDsVSMovIts."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg gerado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg ful'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677180000000020000
          Width = 120.944881889763800000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% kg/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 718.110700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
        RowCount = 0
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_DST]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_ORI]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsVSMovIts."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM> / <frxDsVSMovIts."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI> <> 0,<VARF_VAL_ITEM> / <VARF_KG_ITEM_O' +
              'RI> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881889763800000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (<frxDsVSMovIts."PesoKg">' +
              ' > 0), <frxDsVSMovIts."PesoKg"> / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (<frxDsVSMovIts."PesoKg"' +
              '> > 0), <frxDsVSMovIts."PesoKg"> / <frxDsVSInnIts."PesoKg"> * 10' +
              '0, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 755.906000000000000000
        Width = 680.315400000000000000
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."AreaM2">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsVSMovIts."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000022000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo152: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsVSMovIts."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsVSMovIts."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsVSMovIts."Pes' +
              'oKg">,MasterData2) > 0), SUM(<frxDsVSMovIts."PesoKg">,MasterData' +
              '2) / <frxDsVSInnIts."PesoKg"> * 100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 884.410020000000000000
        Width = 680.315400000000000000
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 11.338589999999950000
          Width = 37.795268270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 11.338589999999950000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 11.338589999999950000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."PesoKg"> <> 0,SUM(<VARF_VAL_ITEM>,MasterDat' +
              'a2) / <frxDsVSInnIts."PesoKg">,0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338589999999950000
          Width = 385.512028270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 11.338589999999950000
          Width = 166.299202830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 616.063390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSMovIts."ID_GRUPO"'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSMovIts."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 801.260360000000000000
        Width = 680.315400000000000000
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."AreaM2">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsVSMovIts."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000022000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Top = 18.897650000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Misturou?:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Rend. Kg/m'#178':')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."RendKgm2_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 18.897650000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 18.897650000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."QtdGerArM2"] m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nota MPAG:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NotaMPAG_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 18.897650000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Top = 18.897650000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"] Kg')
          ParentFont = False
          WordWrap = False
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 661.417750000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSInnIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Obs.:')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Compra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 18.897650000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#233'dia / couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."m2_CouroTXT"] m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Kg/Couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
        RowCount = 0
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370105590000000000
          Height = 20.787401570000000000
          DataField = 'Controle'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          DataField = 'Pecas'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 147.401618740000000000
          Height = 20.787401570000000000
          DataField = 'Observ'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 56.692918270000000000
          Height = 20.787401570000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771704800000000000
          Height = 20.787401570000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GHGerados: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSGerados."MovimID"'
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370105590000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 147.401616300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 56.692918270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771704800000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GFGerados: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GFGeradosOnBeforePrint'
        object MeTotAM2: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTotPec: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."Pecas">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitTot: TfrxMemoView
          AllowVectorExport = True
          Width = 306.141834800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'TOTAL GERADO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFiller: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 204.094524800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsVSInnCab: TfrxDBDataset
    UserName = 'frxDsVSInnCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'ValorT=ValorT'
      'ClienteMO=ClienteMO'
      'Procednc=Procednc'
      'Motorista=Motorista'
      'Placa=Placa'
      'NO_CLIENTEMO=NO_CLIENTEMO'
      'NO_PROCEDNC=NO_PROCEDNC'
      'NO_MOTORISTA=NO_MOTORISTA'
      'TemIMEIMrt=TemIMEIMrt'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'emi_serie=emi_serie'
      'emi_nNF=emi_nNF'
      'NFeStatus=NFeStatus'
      'VSVmcWrn=VSVmcWrn'
      'VSVmcObs=VSVmcObs'
      'VSVmcSeq=VSVmcSeq'
      'VSVmcSta=VSVmcSta'
      'DtEnceRend=DtEnceRend'
      'TpEnceRend=TpEnceRend'
      'DtEnceRend_TXT=DtEnceRend_TXT')
    DataSet = QrVSInnCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 212
    Top = 144
  end
  object frxDsVSInnIts: TfrxDBDataset
    UserName = 'frxDsVSInnIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'PedItsFin=PedItsFin'
      'Marca=Marca'
      'StqCenLoc=StqCenLoc'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'ReqMovEstq=ReqMovEstq'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrVSInnIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 296
    Top = 144
  end
  object frxDsVSItsBxa: TfrxDBDataset
    UserName = 'frxDsVSItsBxa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_Pallet=NO_Pallet'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'MovimTwn=MovimTwn'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'SdoVrtPeso=SdoVrtPeso'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'FornecMO=FornecMO'
      'SerieFch=SerieFch'
      'NotaMPAG=NotaMPAG'
      'KgMedioCouro=KgMedioCouro')
    DataSet = QrVSItsBxa
    BCDToCurrency = False
    DataSetOptions = []
    Left = 244
    Top = 428
  end
  object frxDsVSItsGer: TfrxDBDataset
    UserName = 'frxDsVSItsGer'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_Pallet=NO_Pallet'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'MovimTwn=MovimTwn'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'SdoVrtPeso=SdoVrtPeso'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'FornecMO=FornecMO'
      'SerieFch=SerieFch'
      'NotaMPAG=NotaMPAG'
      'Misturou_TXT=Misturou_TXT'
      'NO_FORNECMO=NO_FORNECMO'
      'Custo_m2=Custo_m2')
    DataSet = QrVSItsGer
    BCDToCurrency = False
    DataSetOptions = []
    Left = 328
    Top = 428
  end
  object QrClienteMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 876
    Top = 480
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 876
    Top = 528
  end
  object QrProcednc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 956
    Top = 480
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcednc: TDataSource
    DataSet = QrProcednc
    Left = 956
    Top = 528
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 808
    Top = 480
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 808
    Top = 528
  end
  object QrVSMovDif: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovdif'
      'WHERE Controle=1')
    Left = 772
    Top = 144
    object QrVSMovDifControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'vsmovdif.Controle'
    end
    object QrVSMovDifInfPecas: TFloatField
      FieldName = 'InfPecas'
      Origin = 'vsmovdif.InfPecas'
    end
    object QrVSMovDifInfPesoKg: TFloatField
      FieldName = 'InfPesoKg'
      Origin = 'vsmovdif.InfPesoKg'
    end
    object QrVSMovDifInfAreaM2: TFloatField
      FieldName = 'InfAreaM2'
      Origin = 'vsmovdif.InfAreaM2'
    end
    object QrVSMovDifInfAreaP2: TFloatField
      FieldName = 'InfAreaP2'
      Origin = 'vsmovdif.InfAreaP2'
    end
    object QrVSMovDifInfValorT: TFloatField
      FieldName = 'InfValorT'
      Origin = 'vsmovdif.InfValorT'
    end
    object QrVSMovDifDifPecas: TFloatField
      FieldName = 'DifPecas'
      Origin = 'vsmovdif.DifPecas'
    end
    object QrVSMovDifDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
      Origin = 'vsmovdif.DifPesoKg'
    end
    object QrVSMovDifDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
      Origin = 'vsmovdif.DifAreaM2'
    end
    object QrVSMovDifDifAreaP2: TFloatField
      FieldName = 'DifAreaP2'
      Origin = 'vsmovdif.DifAreaP2'
    end
    object QrVSMovDifDifValorT: TFloatField
      FieldName = 'DifValorT'
      Origin = 'vsmovdif.DifValorT'
    end
    object QrVSMovDifLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsmovdif.Lk'
    end
    object QrVSMovDifDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsmovdif.DataCad'
    end
    object QrVSMovDifDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsmovdif.DataAlt'
    end
    object QrVSMovDifUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsmovdif.UserCad'
    end
    object QrVSMovDifUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsmovdif.UserAlt'
    end
    object QrVSMovDifAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsmovdif.AlterWeb'
    end
    object QrVSMovDifAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsmovdif.Ativo'
    end
    object QrVSMovDifPerQbrViag: TFloatField
      FieldName = 'PerQbrViag'
      Origin = 'vsmovdif.PerQbrViag'
    end
    object QrVSMovDifPerQbrSal: TFloatField
      FieldName = 'PerQbrSal'
      Origin = 'vsmovdif.PerQbrSal'
    end
    object QrVSMovDifRstCouPc: TFloatField
      FieldName = 'RstCouPc'
      Origin = 'vsmovdif.RstCouPc'
    end
    object QrVSMovDifRstCouKg: TFloatField
      FieldName = 'RstCouKg'
      Origin = 'vsmovdif.RstCouKg'
    end
    object QrVSMovDifRstCouVl: TFloatField
      FieldName = 'RstCouVl'
      Origin = 'vsmovdif.RstCouVl'
    end
    object QrVSMovDifRstSalKg: TFloatField
      FieldName = 'RstSalKg'
      Origin = 'vsmovdif.RstSalKg'
    end
    object QrVSMovDifRstSalVl: TFloatField
      FieldName = 'RstSalVl'
      Origin = 'vsmovdif.RstSalVl'
    end
    object QrVSMovDifRstTotVl: TFloatField
      FieldName = 'RstTotVl'
      Origin = 'vsmovdif.RstTotVl'
    end
    object QrVSMovDifTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
    object QrVSMovDifPesoSalKg: TFloatField
      FieldName = 'PesoSalKg'
    end
  end
  object QrVSHisFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vshisfch '
      'WHERE VSMovIts=0 '
      'OR ( '
      '  SerieFch=0 '
      '  AND '
      '  Ficha=0 '
      ') ')
    Left = 404
    Top = 336
    object QrVSHisFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSHisFchVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSHisFchSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSHisFchFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSHisFchDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSHisFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSHisFchObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSHisFchLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSHisFchDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSHisFchDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSHisFchUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSHisFchUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSHisFchAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSHisFchAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSHisFch: TDataSource
    DataSet = QrVSHisFch
    Left = 404
    Top = 384
  end
  object PMVInnIts: TPopupMenu
    Left = 132
    Top = 368
    object IrparajaneladegerenciamentodeFichaRMP1: TMenuItem
      Caption = '&Ir para janela de gerenciamento de Ficha RMP'
      OnClick = IrparajaneladegerenciamentodeFichaRMP1Click
    end
  end
  object PMSubProduto: TPopupMenu
    OnPopup = PMSubProdutoPopup
    Left = 744
    Top = 620
    object IncluiSubProduto1: TMenuItem
      Caption = '&Inclui Sub Produto do Item de entrada selecionado'
      OnClick = IncluiSubProduto1Click
    end
    object AlteraSubProduto1: TMenuItem
      Caption = '&Altera Sub Produto Atual'
      OnClick = AlteraSubProduto1Click
    end
    object ExcluiSubProduto1: TMenuItem
      Caption = '&Exclui SubProduto Atual'
      OnClick = ExcluiSubProduto1Click
    end
  end
  object QrVSSubPrdIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'WHERE vmi.GSPInnNiv2=1'
      'ORDER BY vmi.DataHora, vmi.Controle ')
    Left = 56
    Top = 501
    object QrVSSubPrdItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSSubPrdItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSSubPrdItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSSubPrdItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSSubPrdItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSSubPrdItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSSubPrdItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSSubPrdItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSSubPrdItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSSubPrdItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSSubPrdItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSSubPrdItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSSubPrdItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSSubPrdItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSSubPrdItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSSubPrdItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSSubPrdItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSSubPrdItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSSubPrdItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSSubPrdItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSSubPrdItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSSubPrdItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSSubPrdItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSSubPrdItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSSubPrdItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSSubPrdItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSSubPrdItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSSubPrdItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSSubPrdItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSSubPrdItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSSubPrdItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVSSubPrdItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSSubPrdItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSSubPrdItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSSubPrdItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSSubPrdItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSSubPrdItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSSubPrdItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSSubPrdItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSSubPrdItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSSubPrdItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSSubPrdItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSSubPrdItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSSubPrdItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSSubPrdItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSSubPrdItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSSubPrdItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSSubPrdItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSSubPrdItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSSubPrdItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSSubPrdItsSeqSifDipoa: TLargeintField
      FieldName = 'SeqSifDipoa'
      DisplayFormat = '000;-000; '
    end
  end
  object DsVSSubPrdIts: TDataSource
    DataSet = QrVSSubPrdIts
    Left = 56
    Top = 545
  end
  object QrTribIncIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 480
    Top = 328
    object QrTribIncItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTribIncItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrTribIncItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTribIncItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTribIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTribIncItsData: TDateField
      FieldName = 'Data'
    end
    object QrTribIncItsHora: TTimeField
      FieldName = 'Hora'
    end
    object QrTribIncItsValorFat: TFloatField
      FieldName = 'ValorFat'
    end
    object QrTribIncItsBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTribIncItsPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '0.00'
    end
    object QrTribIncItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTribIncItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTribIncItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTribIncItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTribIncItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTribIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTribIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTribIncItsTributo: TIntegerField
      FieldName = 'Tributo'
    end
    object QrTribIncItsVUsoCalc: TFloatField
      FieldName = 'VUsoCalc'
    end
    object QrTribIncItsOperacao: TSmallintField
      FieldName = 'Operacao'
    end
    object QrTribIncItsNO_Tributo: TWideStringField
      FieldName = 'NO_Tributo'
      Size = 60
    end
    object QrTribIncItsFatorDC: TSmallintField
      FieldName = 'FatorDC'
    end
  end
  object DsTribIncIts: TDataSource
    DataSet = QrTribIncIts
    Left = 480
    Top = 376
  end
  object PMTribIncIts: TPopupMenu
    OnPopup = PMTribIncItsPopup
    Left = 928
    Top = 624
    object IncluiTributo1: TMenuItem
      Caption = '&Inclui Tributo'
      OnClick = IncluiTributo1Click
    end
    object AlteraTributoAtual1: TMenuItem
      Caption = '&Altera Tributo Atual'
      OnClick = AlteraTributoAtual1Click
    end
    object ExcluiTributoAtual1: TMenuItem
      Caption = '&Exclui Tributo Atual'
      OnClick = ExcluiTributoAtual1Click
    end
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 1 '
      '  WHEN 23 THEN 1 '
      '  WHEN 26 THEN 2 '
      '  WHEN 27 THEN 2 '
      'ELSE 9 END ID_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN "RECEITAS" '
      '  WHEN 23 THEN "RECEITAS" '
      '  WHEN 26 THEN "DESPESAS" '
      '  WHEN 27 THEN "DESPESAS" '
      'ELSE "OUTROS" END NO_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 101 '
      '  WHEN 23 THEN 102 '
      '  WHEN 26 THEN 201 '
      '  WHEN 27 THEN 202 '
      'ELSE 999 END ORDEM2,   '
      'vmi.*,  '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento") NO_MovimID, '
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento") NO_MovimNiv, '
      'ggx.GraGru1, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=16 '
      'AND (NOT MovimNiv IN (14,15,30,35))  '
      'ORDER BY ORDEM2  '
      '  ')
    Left = 204
    Top = 492
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVSMovItsEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVSMovItsLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVSMovItsNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVSMovItsFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVSMovItsFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVSMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSMovItsVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrVSMovItsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrVSMovItsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrVSMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSMovItsCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrVSMovItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrVSMovItsKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrVSMovItsNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Required = True
      Size = 8
    end
    object QrVSMovItsID_GRUPO: TFloatField
      FieldName = 'ID_GRUPO'
    end
    object QrVSMovItsORDEM2: TFloatField
      FieldName = 'ORDEM2'
    end
  end
  object frxDsVSMovIts: TfrxDBDataset
    UserName = 'frxDsVSMovIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'CODUSUUNIDMED=CODUSUUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'KgCouPQ=KgCouPQ'
      'ID_GRUPO=ID_GRUPO'
      'NO_GRUPO=NO_GRUPO'
      'ORDEM2=ORDEM2')
    OpenDataSource = False
    DataSet = QrVSMovIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 204
    Top = 540
  end
  object QrVSGerados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 1 '
      '  WHEN 23 THEN 1 '
      '  WHEN 26 THEN 2 '
      '  WHEN 27 THEN 2 '
      'ELSE 9 END ID_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN "RECEITAS" '
      '  WHEN 23 THEN "RECEITAS" '
      '  WHEN 26 THEN "DESPESAS" '
      '  WHEN 27 THEN "DESPESAS" '
      'ELSE "OUTROS" END NO_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 101 '
      '  WHEN 23 THEN 102 '
      '  WHEN 26 THEN 201 '
      '  WHEN 27 THEN 202 '
      'ELSE 999 END ORDEM2,   '
      'vmi.*,  '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento") NO_MovimID, '
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento") NO_MovimNiv, '
      'ggx.GraGru1, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=16 '
      'AND (NOT MovimNiv IN (14,15,30,35))  '
      'ORDER BY ORDEM2  '
      '  ')
    Left = 284
    Top = 492
    object QrVSGeradosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGeradosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGeradosMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGeradosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGeradosMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGeradosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGeradosTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGeradosCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGeradosMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGeradosLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGeradosLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGeradosDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSGeradosPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGeradosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGeradosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGeradosPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGeradosAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSGeradosAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSGeradosValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSGeradosSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGeradosSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGeradosSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGeradosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGeradosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSGeradosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGeradosFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGeradosMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGeradosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSGeradosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSGeradosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGeradosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGeradosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGeradosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGeradosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGeradosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGeradosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGeradosSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSGeradosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGeradosSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGeradosFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSGeradosValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSGeradosDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGeradosDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGeradosDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGeradosDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSGeradosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGeradosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSGeradosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSGeradosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSGeradosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGeradosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSGeradosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSGeradosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSGeradosAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSGeradosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSGeradosMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGeradosTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSGeradosZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVSGeradosEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVSGeradosLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVSGeradosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVSGeradosNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVSGeradosFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVSGeradosFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVSGeradosPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSGeradosPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSGeradosPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSGeradosReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSGeradosStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSGeradosItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSGeradosVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrVSGeradosVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSGeradosClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSGeradosCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrVSGeradosNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrVSGeradosNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrVSGeradosGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSGeradosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGeradosSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSGeradosCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrVSGeradosNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSGeradosGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSGeradosNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrVSGeradosKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrVSGeradosNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Required = True
      Size = 8
    end
    object QrVSGeradosID_GRUPO: TFloatField
      FieldName = 'ID_GRUPO'
    end
    object QrVSGeradosORDEM2: TFloatField
      FieldName = 'ORDEM2'
    end
  end
  object frxDsVSGerados: TfrxDBDataset
    UserName = 'frxDsVSGerados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'CODUSUUNIDMED=CODUSUUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'KgCouPQ=KgCouPQ'
      'NO_GRUPO=NO_GRUPO'
      'ID_GRUPO=ID_GRUPO'
      'ORDEM2=ORDEM2')
    OpenDataSource = False
    DataSet = QrVSGerados
    BCDToCurrency = False
    DataSetOptions = []
    Left = 284
    Top = 540
  end
  object QrVSInnNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsinnnfs')
    Left = 372
    Top = 112
    object QrVSInnNFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSInnNFsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSInnNFsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrVSInnNFsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSInnNFsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSInnNFsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSInnNFsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSInnNFsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSInnNFsMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrVSInnNFsPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrVSInnNFside_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSInnNFside_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSInnNFsemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrVSInnNFsemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrVSInnNFsNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSInnNFsVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
    end
    object QrVSInnNFsVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrVSInnNFsVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrVSInnNFsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSInnNFsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSInnNFsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSInnNFsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSInnNFsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSInnNFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSInnNFsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSInnNFsNo_Motorista: TWideStringField
      FieldName = 'No_Motorista'
      Size = 100
    end
  end
  object DsVSInnNFs: TDataSource
    DataSet = QrVSInnNFs
    Left = 372
    Top = 160
  end
  object frxWET_CURTI_006_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GFGeradosOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Altura: Double;'
      '  Visivel: Boolean;                                             '
      'begin'
      '  if <VARF_REGISTROS_GERADOS> < 2 then  '
      '    Altura := 0'
      '  else              '
      '    Altura := 0.55;        '
      '  //GFGerados.Height := Altura;'
      
        '  Visivel := Altura > 0;                                        ' +
        '            '
      '  MeTitTot.Visible := Visivel;        '
      '  MeTotPec.Visible := Visivel;        '
      '  MeTotAM2.Visible := Visivel;        '
      '  MeFiller.Visible := Visivel;        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_006_01GetValue
    Left = 400
    Top = 488
    Datasets = <
      item
        DataSet = frxDsDespesas
        DataSetName = 'frxDsDespesas'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsReceitas
        DataSetName = 'frxDsReceitas'
      end
      item
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
      end
      item
        DataSet = frxDsVSInnCab
        DataSetName = 'frxDsVSInnCab'
      end
      item
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
      end
      item
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
      end
      item
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504020000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'An'#225'lise MPAG Modelo 2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP: [frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 64.252010000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEntrada"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."MovimCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 83.149660000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_TRANSPORTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 151.181200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima In Natura:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 102.047310000000000000
          Width = 529.134200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."GraGruX"] - [frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 170.078801180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
        RowCount = 0
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DataField = 'KgMedioCouro'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999980000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do Artigo de Ribeira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999980000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IME-I Gerou')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 15.118119999999980000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mistura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 15.118119999999980000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSItsBxa."QtdGerArM2">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 15.118119999999980000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Prestador MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 15.118119999999980000
          Width = 30.236093540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 15.118119999999980000
          Width = 41.574803149606300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
        RowCount = 0
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999920000
          Width = 45.354323390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 113.385826771654000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'Misturou_TXT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999920000
          Width = 589.606643390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECMO'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_FORNECMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOKg'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'QtdAntPeso'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."QtdAntPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOTot'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Custo_m2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999920000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ / m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 419.527830000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."Controle"'
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg / couro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 563.149970000000000000
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 1145.197590000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 634.961040000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceitas."MovimID"'
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677179999999910000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsReceitas."MovimID">)] - [frxDsReceitas."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677179999999910000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo184: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% kg/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 695.433520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsReceitas
        DataSetName = 'frxDsReceitas'
        RowCount = 0
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM_C]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsReceitas."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsReceitas."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM_C> / <frxDsReceitas."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI> <> 0,<VARF_VAL_ITEM_C> / <VARF_KG_ITEM' +
              '_ORI> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsReceitas."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (<frxDsReceitas."PesoKg">' +
              ' > 0), <frxDsReceitas."PesoKg"> / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."MetaDivKgPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."ResDivKgPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (<frxDsReceitas."PesoKg"' +
              '> > 0), <frxDsReceitas."PesoKg"> / <frxDsVSInnIts."PesoKg"> * 10' +
              '0, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."MetaPerKgKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo169: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."ResPerKgKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 733.228820000000000000
        Width = 680.315400000000000000
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_C>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsReceitas."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo152: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsReceitas."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."MetaDivKgPc">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsReceitas."MetaDivKgPc">) <> 0, '
            '('
            
              'IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsReceitas."PesoK' +
              'g">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2)' +
              ' / <frxDsVSInnIts."Pecas">, 0) '
            ''
            '/ SUM(<frxDsReceitas."MetaDivKgPc">)) *100, 0)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsReceitas."Pes' +
              'oKg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData' +
              '2) / <frxDsVSInnIts."PesoKg"> * 100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."MetaPerKgKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo170: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsReceitas."MetaPerKgKg">) <> 0 , '
            '('
            
              'IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsReceitas."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."PesoKg"> * 100, 0)'
            '/ SUM(<frxDsReceitas."MetaPerKgKg">)) * 100, 0)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 1092.284170000000000000
        Width = 680.315400000000000000
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 11.338590000000070000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<VARF_VAL_ITEM_C>,MasterData2) + SUM(<VARF_VAL_ITEM_D>,Mast' +
              'erData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 11.338590000000070000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 11.338590000000070000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsVSInnIts."PesoKg"> <> 0,'
            
              '(SUM(<VARF_VAL_ITEM_C>,MasterData2) + SUM(<VARF_VAL_ITEM_D>,Mast' +
              'erData5))'
            '/ <frxDsVSInnIts."PesoKg">,0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000070000
          Width = 389.291558270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 11.338590000000070000
          Width = 132.283432830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceitas."ID_GRUPO"'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceitas."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 778.583180000000000000
        Width = 680.315400000000000000
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_C>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsReceitas."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo183: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110236220470000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574830000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 45.354360000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Misturou?:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 30.236240000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 34.015770000000010000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 15.118110236220470000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"] Kg')
          ParentFont = False
          WordWrap = False
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 15.118119999999980000
          Width = 661.417750000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSInnIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 18.897650000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Obs.:')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Compra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 86.929126540000000000
          Height = 15.118110236220470000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Kg/Couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 56.692950000000010000
          Height = 15.118110236220470000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 64.252010000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dt enc.rend.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Width = 86.929126540000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEnceRend_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
        RowCount = 0
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          DataField = 'Controle'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 132.283498740000000000
          Height = 20.787401570000000000
          DataField = 'Observ'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 90.708661420000000000
          Height = 20.787401570000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677165354330700000
          Width = 45.354330710000000000
          Height = 20.787401570000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          AllowVectorExport = True
          Width = 181.417322830000000000
          Height = 20.787401570000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291343460000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSGerados."Pecas"> = 0, 0, <frxDsVSGerados."AreaM2"> ' +
              '/ <frxDsVSGerados."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo217: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251970000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."Pecas"> = 0, 0, <frxDsVSInnIts."PesoKg"> / ' +
              '<frxDsVSInnIts."Pecas">) * (IIF(<frxDsVSGerados."AreaM2"> = 0, 0' +
              ', <frxDsVSGerados."Pecas"> / <frxDsVSGerados."AreaM2">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GHGerados: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSGerados."MovimID"'
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 64.251968500000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 132.283496300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 90.708661420000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677165354330700000
          Width = 45.354330710000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          AllowVectorExport = True
          Width = 181.417322834645700000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291343460000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251968503900000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GFGerados: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GFGeradosOnBeforePrint'
        object MeTotAM2: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 90.708661420000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTotPec: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 52.913385830000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."Pecas">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitTot: TfrxMemoView
          AllowVectorExport = True
          Width = 245.669354800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'TOTAL GERADO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFiller: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 185.196874800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291343460000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsVSGerados."Pecas">,MasterData4) = 0, 0, SUM(<frxD' +
              'sVSGerados."AreaM2">,MasterData4) / SUM(<frxDsVSGerados."Pecas">' +
              ',MasterData4))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984251970000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."Pecas"> = 0, 0, <frxDsVSInnIts."PesoKg"> / ' +
              '<frxDsVSInnIts."Pecas">) * '
            'SUM(<frxDsVSGerados."Pecas">,MasterData4)'
            '/'
            'SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 865.512370000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDespesas."MovimID"'
        object Memo156: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo159: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677180000000020000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo160: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo161: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo162: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo163: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 22.677180000000020000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo164: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999908000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsDespesas."MovimID">)] - [frxDsDespesas."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo165: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg gerado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo166: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg ful'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo167: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo168: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677179999999910000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo209: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% '#8593' Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 925.984850000000000000
        Width = 680.315400000000000000
        DataSet = frxDsDespesas
        DataSetName = 'frxDsDespesas'
        RowCount = 0
        object Memo171: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo174: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo175: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo176: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo177: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsDespesas."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo178: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo179: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_DST_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo180: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_ORI_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo181: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsDespesas."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM_D> / <frxDsDespesas."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI_D> <> 0,<VARF_VAL_ITEM_D> / <VARF_KG_IT' +
              'EM_ORI_D> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo182: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsDespesas."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo210: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsDespesas."PesoKg"> <> 0, ((-<VARF_KG_ITEM_DST_D> / <f' +
              'rxDsDespesas."PesoKg">)-1)*100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 963.780150000000000000
        Width = 680.315400000000000000
        object Memo185: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."PesoKg">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo188: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Pecas">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo189: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."AreaM2">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo190: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo191: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsDespesas."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo211: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsDespesas."PesoKg"> <> 0, ((-SUM(<VARF_KG_ITEM_DST_D>,' +
              'MasterData5) / SUM(<frxDsDespesas."PesoKg">,MasterData5))-1)*100' +
              ', 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 823.937540000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDespesas."ID_GRUPO"'
        object Memo198: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDespesas."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 1009.134510000000000000
        Width = 680.315400000000000000
        object Memo199: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo200: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779529999999908000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."PesoKg">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779529999999908000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Pecas">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 3.779529999999908000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."AreaM2">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo203: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779529999999908000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo204: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779529999999908000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo205: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779529999999908000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779529999999908000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo207: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999908000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsDespesas."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo208: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779529999999908000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo212: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779529999999908000
          Width = 34.015696770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsReceitas: TfrxDBDataset
    UserName = 'frxDsReceitas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'CODUSUUNIDMED=CODUSUUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'KgCouPQ=KgCouPQ'
      'NO_GRUPO=NO_GRUPO'
      'ID_GRUPO=ID_GRUPO'
      'ORDEM2=ORDEM2'
      'MetaDivKgPc=MetaDivKgPc'
      'MetaPerKgKg=MetaPerKgKg'
      'ResDivKgPc=ResDivKgPc'
      'ResPerKgKg=ResPerKgKg')
    DataSet = QrReceitas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 804
    Top = 444
  end
  object frxDsDespesas: TfrxDBDataset
    UserName = 'frxDsDespesas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'CODUSUUNIDMED=CODUSUUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'KgCouPQ=KgCouPQ'
      'NO_GRUPO=NO_GRUPO'
      'ID_GRUPO=ID_GRUPO'
      'ORDEM2=ORDEM2')
    DataSet = QrDespesas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 876
    Top = 444
  end
  object QrReceitas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrReceitasCalcFields
    SQL.Strings = (
      'SELECT  '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 1 '
      '  WHEN 23 THEN 1 '
      '  WHEN 26 THEN 2 '
      '  WHEN 27 THEN 2 '
      'ELSE 9 END ID_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN "RECEITAS" '
      '  WHEN 23 THEN "RECEITAS" '
      '  WHEN 26 THEN "DESPESAS" '
      '  WHEN 27 THEN "DESPESAS" '
      'ELSE "OUTROS" END NO_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 101 '
      '  WHEN 23 THEN 102 '
      '  WHEN 26 THEN 201 '
      '  WHEN 27 THEN 202 '
      'ELSE 999 END ORDEM2,   '
      'vmi.*,  '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento") NO_MovimID, '
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento") NO_MovimNiv, '
      'ggx.GraGru1, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=16 '
      'AND (NOT MovimNiv IN (14,15,30,35))  '
      'ORDER BY ORDEM2  '
      '  ')
    Left = 804
    Top = 392
    object QrReceitasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrReceitasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReceitasMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrReceitasMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrReceitasMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrReceitasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrReceitasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrReceitasCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrReceitasMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrReceitasLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrReceitasLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrReceitasDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrReceitasPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrReceitasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrReceitasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrReceitasPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrReceitasAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrReceitasAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrReceitasValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrReceitasSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrReceitasSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrReceitasSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrReceitasSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrReceitasSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrReceitasObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrReceitasFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrReceitasMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrReceitasCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrReceitasCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrReceitasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrReceitasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrReceitasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrReceitasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrReceitasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrReceitasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrReceitasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrReceitasSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrReceitasSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrReceitasSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrReceitasFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrReceitasValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrReceitasDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrReceitasDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrReceitasDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrReceitasDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrReceitasQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrReceitasQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrReceitasQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrReceitasQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrReceitasQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrReceitasQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrReceitasQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrReceitasQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrReceitasAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrReceitasNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrReceitasMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrReceitasTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrReceitasZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrReceitasEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrReceitasLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrReceitasCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrReceitasNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrReceitasFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrReceitasFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrReceitasPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrReceitasPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrReceitasPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrReceitasReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrReceitasStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrReceitasItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrReceitasVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrReceitasVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrReceitasClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrReceitasCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrReceitasNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrReceitasNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrReceitasGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrReceitasNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrReceitasSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrReceitasCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrReceitasNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrReceitasGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrReceitasNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrReceitasKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrReceitasNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Required = True
      Size = 8
    end
    object QrReceitasID_GRUPO: TFloatField
      FieldName = 'ID_GRUPO'
    end
    object QrReceitasORDEM2: TFloatField
      FieldName = 'ORDEM2'
    end
    object QrReceitasMetaDivKgPc: TFloatField
      FieldKind = fkLookup
      FieldName = 'MetaDivKgPc'
      LookupDataSet = QrVSCPMRSBIt
      LookupKeyFields = 'GraGruX'
      LookupResultField = 'DivKgSPpcCou'
      KeyFields = 'GraGruX'
      DisplayFormat = '0.000'
      Lookup = True
    end
    object QrReceitasMetaPerKgKg: TFloatField
      FieldKind = fkLookup
      FieldName = 'MetaPerKgKg'
      LookupDataSet = QrVSCPMRSBIt
      LookupKeyFields = 'GraGruX'
      LookupResultField = 'PercKgSPKgCou'
      KeyFields = 'GraGruX'
      DisplayFormat = '0.00'
      Lookup = True
    end
    object QrReceitasResDivKgPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ResDivKgPc'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object QrReceitasResPerKgKg: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ResPerKgKg'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object QrDespesas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 1 '
      '  WHEN 23 THEN 1 '
      '  WHEN 26 THEN 2 '
      '  WHEN 27 THEN 2 '
      'ELSE 9 END ID_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN "RECEITAS" '
      '  WHEN 23 THEN "RECEITAS" '
      '  WHEN 26 THEN "DESPESAS" '
      '  WHEN 27 THEN "DESPESAS" '
      'ELSE "OUTROS" END NO_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 101 '
      '  WHEN 23 THEN 102 '
      '  WHEN 26 THEN 201 '
      '  WHEN 27 THEN 202 '
      'ELSE 999 END ORDEM2,   '
      'vmi.*,  '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento") NO_MovimID, '
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento") NO_MovimNiv, '
      'ggx.GraGru1, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=16 '
      'AND (NOT MovimNiv IN (14,15,30,35))  '
      'ORDER BY ORDEM2  '
      '  ')
    Left = 876
    Top = 388
    object QrDespesasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDespesasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDespesasMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrDespesasMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrDespesasMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrDespesasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrDespesasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrDespesasCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrDespesasMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrDespesasLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrDespesasLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrDespesasDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrDespesasPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrDespesasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrDespesasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrDespesasPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrDespesasAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrDespesasAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrDespesasValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrDespesasSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrDespesasSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrDespesasSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrDespesasSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrDespesasSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrDespesasObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrDespesasFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrDespesasMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrDespesasCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrDespesasCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrDespesasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDespesasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDespesasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDespesasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDespesasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDespesasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDespesasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrDespesasSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrDespesasSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrDespesasSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrDespesasFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrDespesasValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrDespesasDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrDespesasDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrDespesasDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrDespesasDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrDespesasQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrDespesasQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrDespesasQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrDespesasQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrDespesasQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrDespesasQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrDespesasQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrDespesasQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrDespesasAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrDespesasNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrDespesasMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrDespesasTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrDespesasZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrDespesasEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrDespesasLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrDespesasCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrDespesasNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrDespesasFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrDespesasFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrDespesasPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrDespesasPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrDespesasPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrDespesasReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrDespesasStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrDespesasItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrDespesasVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrDespesasVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrDespesasClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrDespesasCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrDespesasNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrDespesasNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrDespesasGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrDespesasNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrDespesasSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrDespesasCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrDespesasNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrDespesasGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrDespesasNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrDespesasKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrDespesasNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Required = True
      Size = 8
    end
    object QrDespesasID_GRUPO: TFloatField
      FieldName = 'ID_GRUPO'
    end
    object QrDespesasORDEM2: TFloatField
      FieldName = 'ORDEM2'
    end
  end
  object QrVSCPMRSBCb: TMySQLQuery
    Database = Dmod.MyDB
    Left = 740
    Top = 4
    object QrVSCPMRSBCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCPMRSBCbNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSCPMRSBCbDataIni: TDateField
      FieldName = 'DataIni'
    end
    object QrVSCPMRSBCbDataFim: TDateField
      FieldName = 'DataFim'
    end
  end
  object QrVSCPMRSBIt: TMySQLQuery
    Database = Dmod.MyDB
    Left = 744
    Top = 52
    object QrVSCPMRSBItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSCPMRSBItGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCPMRSBItDivKgSPpcCou: TFloatField
      FieldName = 'DivKgSPpcCou'
      DisplayFormat = '0.00'
    end
    object QrVSCPMRSBItPercKgSPKgCou: TFloatField
      FieldName = 'PercKgSPKgCou'
      DisplayFormat = '0.0000'
    end
  end
  object QrVSMOEnvEnv: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 564
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 564
    Top = 384
  end
  object QrVSMOEnvEVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 564
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 564
    Top = 484
  end
  object QrVSMOEnvAvu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvAvuBeforeClose
    AfterScroll = QrVSMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 660
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrVSMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrVSMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrVSMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrVSMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrVSMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrVSMOEnvAvuCFTMA_Pecas: TFloatField
      FieldName = 'CFTMA_Pecas'
    end
    object QrVSMOEnvAvuCFTMA_PesoKg: TFloatField
      FieldName = 'CFTMA_PesoKg'
    end
    object QrVSMOEnvAvuCFTMA_AreaM2: TFloatField
      FieldName = 'CFTMA_AreaM2'
    end
    object QrVSMOEnvAvuCFTMA_AreaP2: TFloatField
      FieldName = 'CFTMA_AreaP2'
    end
    object QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
    end
    object QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
    end
    object QrVSMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvAvu: TDataSource
    DataSet = QrVSMOEnvAvu
    Left = 660
    Top = 384
  end
  object QrVSMOEnvAVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 660
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField
      FieldName = 'VSMOEnvAvu'
    end
    object QrVSMOEnvAVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvAVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvAVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvAVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvAVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvAVMI: TDataSource
    DataSet = QrVSMOEnvAVMI
    Left = 660
    Top = 484
  end
  object QrMedias: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' '
      'DROP TABLE IF EXISTS _VMI_MEDIAS_AREA_SEM_GELATINA_;'
      ''
      'CREATE TABLE _VMI_MEDIAS_AREA_SEM_GELATINA_'
      ''
      'SELECT SUM(Pecas) Pecas, '
      'SUM(QtdGerArM2) QtdGerArM2,  '
      'SUM(PesoKg) PesoKg'
      ' '
      'FROM bluederm_meza.vsmovits vmi  '
      'WHERE vmi.SrcMovID=1 '
      'AND vmi.SrcNivel1=366 '
      'AND vmi.SrcNivel2=41003 '
      'AND vmi.MovimNiv IN (15,29)  '
      'AND vmi.QtdGerArM2 <> 0 '
      'AND vmi.QtdGerPeso = 0 '
      ''
      'UNION'
      ''
      'SELECT SUM(Pecas) Pecas, '
      'SUM(QtdGerArM2) QtdGerArM2,  '
      'SUM(PesoKg) PesoKg'
      ' '
      'FROM bluederm_meza.vsmovitb vmi  '
      'WHERE vmi.SrcMovID=1 '
      'AND vmi.SrcNivel1=366 '
      'AND vmi.SrcNivel2=41003 '
      'AND vmi.MovimNiv IN (15,29)  '
      'AND vmi.QtdGerArM2 <> 0 '
      'AND vmi.QtdGerPeso = 0 '
      ';'
      ''
      'SELECT SUM(Pecas) Pecas, '
      'SUM(QtdGerArM2) QtdGerArM2, '
      'SUM(QtdGerArM2) / SUM(Pecas) M2_PC, '
      'SUM(PesoKg) PesoKg, '
      'SUM(PesoKg) / SUM(Pecas) Kg_PC, '
      'SUM(PesoKg) / SUM(QtdGerArM2) Kg_M2  '
      'FROM _VMI_MEDIAS_AREA_SEM_GELATINA_'
      ';'
      'DROP TABLE IF EXISTS _VMI_MEDIAS_AREA_SEM_GELATINA_;')
    Left = 96
    Top = 310
    object QrMediasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMediasQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrMediasM2_PC: TFloatField
      FieldName = 'M2_PC'
    end
    object QrMediasPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrMediasKg_PC: TFloatField
      FieldName = 'Kg_PC'
    end
    object QrMediasKg_M2: TFloatField
      FieldName = 'Kg_M2'
    end
  end
  object frxWET_CURTI_006_01_SemGelatina: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GFGeradosOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Altura: Double;'
      '  Visivel: Boolean;                                             '
      'begin'
      '  if <VARF_REGISTROS_GERADOS> < 2 then  '
      '    Altura := 0'
      '  else              '
      '    Altura := 0.55;        '
      '  //GFGerados.Height := Altura;'
      
        '  Visivel := Altura > 0;                                        ' +
        '            '
      '  MeTitTot.Visible := Visivel;        '
      '  MeTotPec.Visible := Visivel;        '
      '  MeTotAM2.Visible := Visivel;        '
      '  MeFiller.Visible := Visivel;        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_006_01GetValue
    Left = 400
    Top = 536
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
      end
      item
        DataSet = frxDsVSInnCab
        DataSetName = 'frxDsVSInnCab'
      end
      item
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
      end
      item
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
      end
      item
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
      end
      item
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504020000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'An'#225'lise MPAG Modelo 1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP: [frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 64.252010000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEntrada"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000010000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."MovimCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 83.149660000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_TRANSPORTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 151.181200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima In Natura:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 102.047310000000000000
          Width = 529.134200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."GraGruX"] - [frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 170.078801180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 480.000310000000000000
        Visible = False
        Width = 680.315400000000000000
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
        RowCount = 0
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DataField = 'KgMedioCouro'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsBxa."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999980000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Nome do Artigo de Ribeira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999980000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IME-I Gerou')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 15.118119999999980000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 15.118119999999980000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mistura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 15.118119999999980000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118119999999980000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsVSItsBxa."QtdGerArM2">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsBxa."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 15.118119999999980000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Prestador MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 15.118119999999980000
          Width = 30.236093540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 15.118119999999980000
          Width = 41.574803149606300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999980000
          Width = 56.692803540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 532.913730000000000000
        Visible = False
        Width = 680.315400000000000000
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
        RowCount = 0
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 15.118119999999860000
          Width = 45.354323390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 113.385826771654000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'Misturou_TXT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 15.118119999999860000
          Width = 589.606643390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 68.031393540000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECMO'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSItsGer."NO_FORNECMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Width = 30.236208270000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOKg'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'QtdAntPeso'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."QtdAntPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CustoMOTot'
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."CustoMOTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 15.118119999999860000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSItsGer."Custo_m2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 15.118119999999860000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsGer
          DataSetName = 'frxDsVSItsGer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ / m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."Controle"'
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg / couro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Width = 381.732493390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 64.251978270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 585.827150000000000000
        Visible = False
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 937.323440000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 657.638220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSMovIts."MovimID"'
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677180000000020000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677180000000020000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 22.677180000000020000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 22.677180000000020000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsVSMovIts."MovimID">)] - [frxDsVSMovIts."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg gerado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg ful'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677180000000020000
          Width = 120.944881889763800000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% kg/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 718.110700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
        RowCount = 0
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMovIts."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_DST]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_ORI]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsVSMovIts."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM> / <frxDsVSMovIts."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI> <> 0,<VARF_VAL_ITEM> / <VARF_KG_ITEM_O' +
              'RI> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881889763800000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (<frxDsVSMovIts."PesoKg">' +
              ' > 0), <frxDsVSMovIts."PesoKg"> / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (<frxDsVSMovIts."PesoKg"' +
              '> > 0), <frxDsVSMovIts."PesoKg"> / <frxDsVSInnIts."PesoKg"> * 10' +
              '0, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 755.906000000000000000
        Width = 680.315400000000000000
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."AreaM2">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsVSMovIts."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000022000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo152: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsVSMovIts."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsVSMovIts."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsVSMovIts."Pes' +
              'oKg">,MasterData2) > 0), SUM(<frxDsVSMovIts."PesoKg">,MasterData' +
              '2) / <frxDsVSInnIts."PesoKg"> * 100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 884.410020000000000000
        Width = 680.315400000000000000
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 11.338589999999950000
          Width = 37.795268270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 11.338589999999950000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 11.338589999999950000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."PesoKg"> <> 0,SUM(<VARF_VAL_ITEM>,MasterDat' +
              'a2) / <frxDsVSInnIts."PesoKg">,0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338589999999950000
          Width = 385.512028270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 11.338589999999950000
          Width = 166.299202830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 616.063390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSMovIts."ID_GRUPO"'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSMovIts."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 801.260360000000000000
        Width = 680.315400000000000000
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMovIts."AreaM2">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsVSMovIts."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 3.779530000000022000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 661.417750000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSInnIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Misturou?:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Rend. Kg/m'#178':')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SemGelatina_KG_M2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 18.897650000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea:')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 18.897650000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."QtdGerArM2"] m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nota MPAG:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NotaMPAG_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"] Kg')
          ParentFont = False
          WordWrap = False
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 37.795300000000000000
          Width = 332.598640000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Obs.:')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Compra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 18.897650000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#233'dia / couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SemGelatina_M2_PC] m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Kg/Couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 18.897650000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SemGelatina_KG_PC]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
        RowCount = 0
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370105590000000000
          Height = 20.787401570000000000
          DataField = 'Controle'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          DataField = 'Pecas'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 147.401618740000000000
          Height = 20.787401570000000000
          DataField = 'Observ'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 56.692918270000000000
          Height = 20.787401570000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771704800000000000
          Height = 20.787401570000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GHGerados: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSGerados."MovimID"'
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370105590000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 147.401616300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 56.692918270000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          AllowVectorExport = True
          Width = 226.771704800000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GFGerados: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GFGeradosOnBeforePrint'
        object MeTotAM2: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 105.826808270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTotPec: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 64.251978270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."Pecas">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitTot: TfrxMemoView
          AllowVectorExport = True
          Width = 306.141834800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'TOTAL GERADO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFiller: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 204.094524800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrEmissT: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissTCalcFields
    Left = 180
    Top = 284
    object QrEmissTCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissTDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissTNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissTDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissTCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissTSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissTVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissTFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissTData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissTAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissTGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissTCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissTLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissTNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissTNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissTNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissTID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissTSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissTFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissTBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissTLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissTCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissTLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissTPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissTMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissTFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissTCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissTControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissTID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissTCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissTQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissTFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissTOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissTLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissTForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissTMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissTMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissTProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissTDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissTCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissTNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissTVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissTAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissTICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissTICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissTDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissTDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissTDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissTUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissTUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissTEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissTContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissTCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissTFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissTSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissTQtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrEmissTAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissTGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissTGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissTGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrEmissTCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object DsEmissT: TDataSource
    DataSet = QrEmissT
    Left = 204
    Top = 284
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 232
    Top = 285
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumTDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrEmissM: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissMCalcFields
    Left = 348
    Top = 288
    object QrEmissMData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissMTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissMCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissMAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissMGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissMDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissMNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissMDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissMCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissMCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissMDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissMSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissMVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissMFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissMNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissMNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissMNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissMID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissMSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissMFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissMBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissMLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissMCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissMLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissMPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissMMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissMFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissMCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissMControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissMID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissMCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissMQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissMFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissMOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissMLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissMForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissMMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissMMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissMProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissMDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissMCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissMNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissMVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissMAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissMICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissMICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissMDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissMDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissMDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissMEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissMContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissMCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissMFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissMAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissMQtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrEmissMSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissMGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissMGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissMGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrEmissMCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object DsEmissM: TDataSource
    DataSet = QrEmissM
    Left = 372
    Top = 288
  end
  object QrSumM: TMySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 289
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumMDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object PMFinanceiro: TPopupMenu
    Left = 1044
    Top = 614
    object MatriaPrima1: TMenuItem
      Caption = 'Mat'#233'ria-Prima'
      object Inclui2: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui2Click
      end
      object Altera2: TMenuItem
        Caption = '&Altera'
        OnClick = Altera2Click
      end
      object Exclui2: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui2Click
      end
    end
    object Frete1: TMenuItem
      Caption = 'Frete'
      object Inclui3: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui3Click
      end
      object Altera3: TMenuItem
        Caption = '&Altera'
        OnClick = Altera3Click
      end
      object Exclui3: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui3Click
      end
    end
    object Comisso1: TMenuItem
      Caption = 'Comiss'#227'o'
      object Inclui4: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui4Click
      end
      object Altera4: TMenuItem
        Caption = '&Altera'
        OnClick = Altera4Click
      end
      object Exclui4: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui4Click
      end
    end
  end
  object QrEmissC: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissCCalcFields
    Left = 528
    Top = 284
    object QrEmissCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissCCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissCAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissCDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissCNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissCDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissCCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissCCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissCSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissCVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissCFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissCNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissCNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissCNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissCID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissCSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissCFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissCBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissCLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissCCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissCLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissCPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissCMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissCFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissCID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissCCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissCQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissCFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissCOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissCLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissCForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissCMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissCMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissCProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissCDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissCCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissCNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissCVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissCAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissCICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissCICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissCDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissCDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissCContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissCCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissCFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissCAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissCQtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrEmissCSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissCGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissCGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissCGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrEmissCCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object DsEmissC: TDataSource
    DataSet = QrEmissC
    Left = 552
    Top = 284
  end
  object QrSumC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 580
    Top = 285
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumCDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxWET_CURTI_006_02_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GFGeradosOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Altura: Double;'
      '  Visivel: Boolean;                                             '
      'begin'
      '  if <VARF_REGISTROS_GERADOS> < 2 then  '
      '    Altura := 0'
      '  else              '
      '    Altura := 0.55;        '
      '  //GFGerados.Height := Altura;'
      '  Visivel := Altura > 0;'
      '  MeTitTot.Visible := Visivel;        '
      '  MeTotPec.Visible := Visivel;        '
      '  MeTotAM2.Visible := Visivel;        '
      '  MeFiller.Visible := Visivel;        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_006_01GetValue
    Left = 484
    Top = 512
    Datasets = <
      item
        DataSet = frxDsDespesas
        DataSetName = 'frxDsDespesas'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsReceitas
        DataSetName = 'frxDsReceitas'
      end
      item
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
      end
      item
        DataSet = frxDsVSInnCab
        DataSetName = 'frxDsVSInnCab'
      end
      item
        DataSet = frxDsVSItsBxa
        DataSetName = 'frxDsVSItsBxa'
      end
      item
        DataSet = frxDsVSItsGer
        DataSetName = 'frxDsVSItsGer'
      end
      item
        DataSet = frxDsVSMovIts
        DataSetName = 'frxDsVSMovIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504020000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'An'#225'lise MPAG Modelo 2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP: [frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 64.252010000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEntrada"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 64.252010000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."MovimCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 83.149660000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."NO_TRANSPORTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 151.181200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima In Natura:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 102.047310000000000000
          Width = 529.134200000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."GraGruX"] - [frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca:')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 170.078801180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 971.339210000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 461.102660000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceitas."MovimID"'
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677179999999910000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsReceitas."MovimID">)] - [frxDsReceitas."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677179999999910000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% Meta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo184: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% kg/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 521.575140000000000000
        Width = 680.315400000000000000
        DataSet = frxDsReceitas
        DataSetName = 'frxDsReceitas'
        RowCount = 0
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM_C]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsReceitas."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsReceitas."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM_C> / <frxDsReceitas."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI> <> 0,<VARF_VAL_ITEM_C> / <VARF_KG_ITEM' +
              '_ORI> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsReceitas."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (<frxDsReceitas."PesoKg">' +
              ' > 0), <frxDsReceitas."PesoKg"> / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."MetaDivKgPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."ResDivKgPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (<frxDsReceitas."PesoKg"' +
              '> > 0), <frxDsReceitas."PesoKg"> / <frxDsVSInnIts."PesoKg"> * 10' +
              '0, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."MetaPerKgKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo169: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceitas."ResPerKgKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_C>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsReceitas."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo152: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsReceitas."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."Pecas">, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."MetaDivKgPc">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsReceitas."MetaDivKgPc">) <> 0, '
            '('
            
              'IIF((<frxDsVSInnIts."Pecas"> > 0) AND (SUM(<frxDsReceitas."PesoK' +
              'g">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2)' +
              ' / <frxDsVSInnIts."Pecas">, 0) '
            ''
            '/ SUM(<frxDsReceitas."MetaDivKgPc">)) *100, 0)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsReceitas."Pes' +
              'oKg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData' +
              '2) / <frxDsVSInnIts."PesoKg"> * 100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."MetaPerKgKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo170: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsReceitas."MetaPerKgKg">) <> 0 , '
            '('
            
              'IIF((<frxDsVSInnIts."PesoKg"> > 0) AND (SUM(<frxDsReceitas."Peso' +
              'Kg">,MasterData2) > 0), SUM(<frxDsReceitas."PesoKg">,MasterData2' +
              ') / <frxDsVSInnIts."PesoKg"> * 100, 0)'
            '/ SUM(<frxDsReceitas."MetaPerKgKg">)) * 100, 0)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 918.425790000000000000
        Width = 680.315400000000000000
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 11.338590000000070000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<VARF_VAL_ITEM_C>,MasterData2) + SUM(<VARF_VAL_ITEM_D>,Mast' +
              'erData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 11.338590000000070000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 11.338590000000070000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsVSInnIts."PesoKg"> <> 0,'
            
              '(SUM(<VARF_VAL_ITEM_C>,MasterData2) + SUM(<VARF_VAL_ITEM_D>,Mast' +
              'erData5))'
            '/ <frxDsVSInnIts."PesoKg">,0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000070000
          Width = 389.291558270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 11.338590000000070000
          Width = 132.283432830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMovIts
          DataSetName = 'frxDsVSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 419.527830000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceitas."ID_GRUPO"'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceitas."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 604.724800000000000000
        Width = 680.315400000000000000
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."PesoKg">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReceitas."Pecas">,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_C>,MasterData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsReceitas."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267714090000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 3.779530000000022000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo183: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110236220470000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574830000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 45.354360000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Misturou?:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 30.236240000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Misturou_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 34.015770000000010000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 15.118110236220470000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"] Kg')
          ParentFont = False
          WordWrap = False
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 15.118119999999980000
          Width = 661.417750000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSInnIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 18.897650000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Obs.:')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Compra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 86.929126540000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 52.913420000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Kg/Couro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 56.692950000000010000
          Height = 15.118110236220470000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."KgMedioCouro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dt enc.rend.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 94.488186540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnCab."DtEnceRend_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSGerados
        DataSetName = 'frxDsVSGerados'
        RowCount = 0
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 56.692913385826770000
          Height = 20.787401570000000000
          DataField = 'Controle'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 45.354330708661420000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 177.637858740000000000
          Height = 20.787401570000000000
          DataField = 'Observ'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 79.370078740157480000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984215350000000000
          Width = 56.692920710000000000
          Height = 20.787401570000000000
          DataField = 'NotaMPAG'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerados."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          AllowVectorExport = True
          Width = 166.299212598425200000
          Height = 20.787401570000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsVSGerados."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716513460000000000
          Width = 49.133858267716540000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSGerados."Pecas"> = 0, 0, <frxDsVSGerados."AreaM2"> ' +
              '/ <frxDsVSGerados."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo217: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850361970000000000
          Width = 49.133858270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."Pecas"> = 0, 0, <frxDsVSInnIts."PesoKg"> / ' +
              '<frxDsVSInnIts."Pecas">) * (IIF(<frxDsVSGerados."AreaM2"> = 0, 0' +
              ', <frxDsVSGerados."Pecas"> / <frxDsVSGerados."AreaM2">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GHGerados: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSGerados."MovimID"'
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 56.692913385826770000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 45.354330708661420000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 177.637856300000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 79.370078740157480000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984215350000000000
          Width = 56.692920710000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          AllowVectorExport = True
          Width = 166.299212598425200000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716513460000000000
          Width = 49.133858267716540000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850361970000000000
          Width = 49.133858270000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GFGerados: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GFGeradosOnBeforePrint'
        object MeTitTot: TfrxMemoView
          AllowVectorExport = True
          Width = 222.992174800000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'TOTAL GERADO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          AllowVectorExport = True
          Left = 600.944981970000000000
          Width = 79.370093390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_REAIS_M2_TOT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTotAM2: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 79.370071420000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTotPec: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 45.354325830000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSGerados."Pecas">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716513460000000000
          Width = 49.133853390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsVSGerados."Pecas">,MasterData4) = 0, 0, SUM(<frxD' +
              'sVSGerados."AreaM2">,MasterData4) / SUM(<frxDsVSGerados."Pecas">' +
              ',MasterData4))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo221: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850361970000000000
          Width = 49.133853390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsVSInnIts."Pecas"> = 0, 0, <frxDsVSInnIts."PesoKg"> / ' +
              '<frxDsVSInnIts."Pecas">) * '
            'SUM(<frxDsVSGerados."Pecas">,MasterData4)'
            '/'
            'SUM(<frxDsVSGerados."AreaM2">,MasterData4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo220: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 98.267743390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'R$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFiller: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataSet = frxDsVSGerados
          DataSetName = 'frxDsVSGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 691.653990000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDespesas."MovimID"'
        object Memo156: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo159: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 22.677180000000020000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo160: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677179999999910000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo161: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$ D/C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo162: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 22.677180000000020000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo163: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 22.677180000000020000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' gerados')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo164: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999908000
          Width = 665.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[(<frxDsDespesas."MovimID">)] - [frxDsDespesas."NO_MovimID"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo165: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 22.677180000000020000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg gerado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo166: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Kg ful'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo167: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '$/Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo168: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677179999999910000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo209: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677180000000020000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '% '#8593' Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 752.126470000000000000
        Width = 680.315400000000000000
        DataSet = frxDsDespesas
        DataSetName = 'frxDsDespesas'
        RowCount = 0
        object Memo171: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo174: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo175: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo176: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo177: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsDespesas."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo178: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo179: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_DST_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo180: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_KG_ITEM_ORI_D]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo181: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsDespesas."PesoKg"> > 0,'
            '  <VARF_VAL_ITEM_D> / <frxDsDespesas."PesoKg">,'
            
              '  (IIF(<VARF_KG_ITEM_ORI_D> <> 0,<VARF_VAL_ITEM_D> / <VARF_KG_IT' +
              'EM_ORI_D> ,0))'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo182: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 120.944881890000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsDespesas."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo210: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsDespesas."PesoKg"> <> 0, ((-<VARF_KG_ITEM_DST_D> / <f' +
              'rxDsDespesas."PesoKg">)-1)*100, 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 789.921770000000000000
        Width = 680.315400000000000000
        object Memo185: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779530000000022000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."PesoKg">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo188: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779530000000022000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Pecas">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo189: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."AreaM2">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo190: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo191: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000022000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsDespesas."NO_MovimID"] : ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779530000000022000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo211: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779530000000022000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsReceitas
          DataSetName = 'frxDsReceitas'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsDespesas."PesoKg"> <> 0, ((-SUM(<VARF_KG_ITEM_DST_D>,' +
              'MasterData5) / SUM(<frxDsDespesas."PesoKg">,MasterData5))-1)*100' +
              ', 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 650.079160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDespesas."ID_GRUPO"'
        object Memo198: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDespesas."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 835.276130000000000000
        Width = 680.315400000000000000
        object Memo199: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          ParentFont = False
        end
        object Memo200: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 3.779529999999908000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."PesoKg">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779529999999908000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Pecas">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 3.779529999999908000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."AreaM2">,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo203: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 3.779529999999908000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_DST_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo204: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779529999999908000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_KG_ITEM_ORI_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo205: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM_D>,MasterData5)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 3.779529999999908000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo207: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999908000
          Width = 162.519758270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSItsBxa
          DataSetName = 'frxDsVSItsBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsDespesas."NO_GRUPO"]: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo208: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 3.779529999999908000
          Width = 98.267706770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo212: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 3.779529999999908000
          Width = 34.015696770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDespesas
          DataSetName = 'frxDsDespesas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PMFiscal: TPopupMenu
    OnPopup = PMFiscalPopup
    Left = 520
    Top = 626
    object IncluiDocumento1: TMenuItem
      Caption = '&Inclui Documento'
      OnClick = IncluiDocumento1Click
    end
    object AlteraDocumento1: TMenuItem
      Caption = '&Altera Documento'
      OnClick = AlteraDocumento1Click
    end
    object ExcluiDocumento1: TMenuItem
      Caption = '&Exclui Documento'
      OnClick = ExcluiDocumento1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object IncluiItemdodocumento1: TMenuItem
      Caption = 'Inclui Item do documento'
      OnClick = IncluiItemdodocumento1Click
    end
    object AlteraoItemselecionadododocumento1: TMenuItem
      Caption = 'Altera o  Item selecionado do documento'
      OnClick = AlteraoItemselecionadododocumento1Click
    end
    object ExcluioItemselecionadododocumento1: TMenuItem
      Caption = 'Exclui o  Item selecionado do documento'
      OnClick = ExcluioItemselecionadododocumento1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object otalizarfisicopeloescritural1: TMenuItem
      Caption = '&Totalizar fisico pelo escritural'
      OnClick = otalizarfisicopeloescritural1Click
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, '
      'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1>0')
    Left = 1052
    Top = 388
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      Required = True
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Required = True
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Required = True
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
  end
  object QrSumNFsInn: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, '
      'SUM(PesoKg) PesoKg, '
      'SUM(ValorT) ValorT '
      'FROM efdinnnfscab '
      'WHERE MovFatID>0 '
      'AND MovFatNum>0 ')
    Left = 1040
    Top = 490
    object QrSumNFsInnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumNFsInnPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrSumNFsInnValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrEfdInnNFsCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEfdInnNFsCabBeforeClose
    AfterScroll = QrEfdInnNFsCabAfterScroll
    SQL.Strings = (
      'SELECT vin.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnnfscab vin '
      'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
      ' ')
    Left = 844
    Top = 290
    object QrEfdInnNFsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnNFsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnNFsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnNFsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnNFsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnNFsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnNFsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnNFsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnNFsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnNFsCabCOD_MOD: TSmallintField
      FieldName = 'COD_MOD'
      Required = True
    end
    object QrEfdInnNFsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnNFsCabSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrEfdInnNFsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnNFsCabCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEfdInnNFsCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrEfdInnNFsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEfdInnNFsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnNFsCabVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnNFsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnNFsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnNFsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnNFsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnNFsCabTpEntrd: TIntegerField
      FieldName = 'TpEntrd'
    end
  end
  object DsEfdInnNFsCab: TDataSource
    DataSet = QrEfdInnNFsCab
    Left = 844
    Top = 340
  end
  object QrEfdInnNFsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
      'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
      'pgt.Tipo_Item) Tipo_Item '
      'FROM efdinnnfsits     vin'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 956
    Top = 286
    object QrEfdInnNFsItsMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsItsMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrEfdInnNFsItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrEfdInnNFsItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEfdInnNFsItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrEfdInnNFsItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnNFsItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrEfdInnNFsItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrEfdInnNFsItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrEfdInnNFsItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrEfdInnNFsItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnNFsItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnNFsItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrEfdInnNFsItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnNFsItsCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEfdInnNFsItsCOD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrEfdInnNFsItsVL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEfdInnNFsItsxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
      FieldName = 'Ori_IPIpIPI'
      Required = True
    end
    object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
      FieldName = 'Ori_IPIvIPI'
      Required = True
    end
    object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrEfdInnNFsItsGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrEfdInnNFsItsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrEfdInnNFsItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrEfdInnNFsItsEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrEfdInnNFsItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEfdInnNFsItsTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrEfdInnNFsItsCST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEfdInnNFsItsCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
  end
  object DsEfdInnNFsIts: TDataSource
    DataSet = QrEfdInnNFsIts
    Left = 956
    Top = 336
  end
  object QrEfdInnCTsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vic.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnctscab vic '
      'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ')
    Left = 1048
    Top = 287
    object QrEfdInnCTsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnCTsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnCTsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnCTsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnCTsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnCTsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnCTsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnCTsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnCTsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnCTsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnCTsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnCTsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnCTsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnCTsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnCTsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnCTsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnCTsCabIND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabIND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnCTsCabSER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEfdInnCTsCabSUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEfdInnCTsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEfdInnCTsCabCTeStatus: TIntegerField
      FieldName = 'CTeStatus'
      Required = True
    end
    object QrEfdInnCTsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnCTsCabDT_A_P: TDateField
      FieldName = 'DT_A_P'
      Required = True
    end
    object QrEfdInnCTsCabTP_CT_e: TSmallintField
      FieldName = 'TP_CT_e'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEfdInnCTsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnCTsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnCTsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnCTsCabVL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Required = True
    end
    object QrEfdInnCTsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_NT: TFloatField
      FieldName = 'VL_NT'
      Required = True
    end
    object QrEfdInnCTsCabCOD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEfdInnCTsCabCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 30
    end
    object QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField
      FieldName = 'COD_MUN_ORIG'
      Required = True
    end
    object QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnCTsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnCTsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnCTsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnCTsCabCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabCFOP: TIntegerField
      FieldName = 'CFOP'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_OPR: TFloatField
      FieldName = 'VL_OPR'
      Required = True
    end
    object QrEfdInnCTsCabVL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Required = True
    end
    object QrEfdInnCTsCabCOD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
    object QrEfdInnCTsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnCTsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnCTsCabIND_NAT_FRT: TWideStringField
      FieldName = 'IND_NAT_FRT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnCTsCabCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnCTsCabNAT_BC_CRED: TWideStringField
      FieldName = 'NAT_BC_CRED'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_PIS: TFloatField
      FieldName = 'ALIQ_PIS'
      Required = True
    end
    object QrEfdInnCTsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnCTsCabCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_COFINS: TFloatField
      FieldName = 'ALIQ_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
  end
  object DsEfdInnCTsCab: TDataSource
    DataSet = QrEfdInnCTsCab
    Left = 1048
    Top = 340
  end
  object PMCTe: TPopupMenu
    OnPopup = PMCTePopup
    Left = 632
    Top = 635
    object IncluiConhecimentodefrete1: TMenuItem
      Caption = '&Inclui Conhecimento de frete'
      OnClick = IncluiConhecimentodefrete1Click
    end
    object AlteraConhecimentodefrete1: TMenuItem
      Caption = '&Altera Conhecimento de frete'
      OnClick = AlteraConhecimentodefrete1Click
    end
    object ExcluiConhecimentodefrete1: TMenuItem
      Caption = '&Exclui Conhecimento de frete'
      OnClick = ExcluiConhecimentodefrete1Click
    end
  end
  object QrNFeItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/* SQL PREFIXO*/'
      ' SELECT pgt.IND_MOV, itsi.*,  '
      'itsn.ICMS_Orig, itsn.ICMS_CST, itsn.ICMS_vBC, '
      'itsn.ICMS_pICMS, itsn.ICMS_vICMS, itsn.ICMS_vBCST, '
      'itsn.ICMS_pICMSST, itsn.ICMS_vICMSST,  '
      ' '
      'itso.IPI_cEnq, itso.IND_APUR, itso.IPI_CST,  '
      'itso.IPI_vBC, itso.IPI_pIPI, itso.IPI_vIPI, '
      ' '
      'itsq.PIS_CST, itsq.PIS_vBC,  '
      'itsq.PIS_pPIS, itsq.PIS_vPIS, '
      ' '
      'itsr.PISST_vBC,  '
      'itsr.PISST_pPIS, itsr.PISST_vPIS, '
      ' '
      'itss.COFINS_CST, itss.COFINS_vBC,  '
      'itss.COFINS_pCOFINS, itss.COFINS_vCOFINS, '
      ' '
      'itst.COFINSST_vBC,  '
      'itst.COFINSST_pCOFINS, itst.COFINSST_vCOFINS '
      ' '
      ' '
      'FROM nfeitsi itsi '
      ' '
      'LEFT JOIN nfeitsn itsn ON  '
      '  itsn.FatID=itsi.FatID '
      '  AND itsn.FatNum=itsi.FatNum '
      '  AND itsn.Empresa=itsi.Empresa '
      '  AND itsn.nItem=itsi.nItem '
      ' '
      'LEFT JOIN nfeitso itso ON '
      '  itso.FatID=itsi.FatID '
      '  AND itso.FatNum=itsi.FatNum '
      '  AND itso.Empresa=itsi.Empresa '
      '  AND itso.nItem=itsi.nItem '
      ' '
      'LEFT JOIN nfeitsq itsq ON '
      '  itsq.FatID=itsi.FatID '
      '  AND itsq.FatNum=itsi.FatNum '
      '  AND itsq.Empresa=itsi.Empresa '
      '  AND itsq.nItem=itsi.nItem '
      ' '
      'LEFT JOIN nfeitsr itsr ON '
      '  itsr.FatID=itsi.FatID '
      '  AND itsr.FatNum=itsi.FatNum '
      '  AND itsr.Empresa=itsi.Empresa '
      '  AND itsr.nItem=itsi.nItem '
      ' '
      'LEFT JOIN nfeitss itss ON '
      '  itss.FatID=itsi.FatID '
      '  AND itss.FatNum=itsi.FatNum '
      '  AND itss.Empresa=itsi.Empresa '
      '  AND itss.nItem=itsi.nItem '
      ' '
      'LEFT JOIN nfeitst itst ON '
      '  itst.FatID=itsi.FatID '
      '  AND itst.FatNum=itsi.FatNum '
      '  AND itst.Empresa=itsi.Empresa '
      '  AND itst.nItem=itsi.nItem '
      ' '
      'LEFT JOIN gragrux ggx ON ggx.Controle=itsi.prod_cProd'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip')
    Left = 632
    Top = 566
    object QrNFeItensFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrNFeItensFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFeItensEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNFeItensnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrNFeItensprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFeItensprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrNFeItensprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrNFeItensprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrNFeItensprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFeItensprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrNFeItensprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrNFeItensprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrNFeItensprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrNFeItensprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFeItensprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrNFeItensprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Required = True
    end
    object QrNFeItensprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFeItensprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Required = True
    end
    object QrNFeItensprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Required = True
    end
    object QrNFeItensprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrNFeItensprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrNFeItensprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrNFeItensprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFeItensprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Required = True
    end
    object QrNFeItensprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Required = True
    end
    object QrNFeItensprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrNFeItensprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrNFeItensprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrNFeItensprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrNFeItensprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrNFeItensprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeItensprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeItensTem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Required = True
    end
    object QrNFeItens_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Required = True
    end
    object QrNFeItensInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrNFeItensEhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrNFeItensUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrNFeItensICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrNFeItensICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrNFeItensICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrNFeItensICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrNFeItensIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrNFeItensIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrNFeItensIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrNFeItensIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrNFeItensPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrNFeItensPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrNFeItensPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrNFeItensPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrNFeItensCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrNFeItensCOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrNFeItensCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrNFeItensCOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrNFeItensMeuID: TIntegerField
      FieldName = 'MeuID'
      Required = True
    end
    object QrNFeItensNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrNFeItensGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrNFeItensUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrNFeItensUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrNFeItensICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrNFeItensICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrNFeItensICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrNFeItensTem_II: TSmallintField
      FieldName = 'Tem_II'
      Required = True
    end
    object QrNFeItensprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrNFeItensStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrNFeItensAtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
    object QrNFeItensICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFeItensICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNFeItensICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNFeItensICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNFeItensICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNFeItensICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNFeItensICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNFeItensICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNFeItensIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrNFeItensIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrNFeItensIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrNFeItensIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrNFeItensIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrNFeItensIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrNFeItensPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrNFeItensPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrNFeItensPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrNFeItensPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrNFeItensPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrNFeItensPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrNFeItensPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrNFeItensCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrNFeItensCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrNFeItensCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrNFeItensCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
    object QrNFeItensCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrNFeItensCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrNFeItensCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrNFeItensIND_MOV: TSmallintField
      FieldName = 'IND_MOV'
    end
  end
end
