unit VSMovimID;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmVSMovimID = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSMovimID: TmySQLQuery;
    DsVSMovimID: TDataSource;
    QrVSMovIDLoc: TmySQLQuery;
    DsVSMovIDLoc: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrVSMovIDLocCodigo: TIntegerField;
    QrVSMovIDLocMovimID: TIntegerField;
    QrVSMovIDLocStqCenLoc: TIntegerField;
    QrVSMovIDLocLk: TIntegerField;
    QrVSMovIDLocDataCad: TDateField;
    QrVSMovIDLocDataAlt: TDateField;
    QrVSMovIDLocUserCad: TIntegerField;
    QrVSMovIDLocUserAlt: TIntegerField;
    QrVSMovIDLocAlterWeb: TSmallintField;
    QrVSMovIDLocAtivo: TSmallintField;
    QrVSMovIDLocNO_LOC_CEN: TWideStringField;
    QrVSMovimIDCodigo: TIntegerField;
    QrVSMovimIDNome: TWideStringField;
    QrVSMovimIDLk: TIntegerField;
    QrVSMovimIDDataCad: TDateField;
    QrVSMovimIDDataAlt: TDateField;
    QrVSMovimIDUserCad: TIntegerField;
    QrVSMovimIDUserAlt: TIntegerField;
    QrVSMovimIDAlterWeb: TSmallintField;
    QrVSMovimIDAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSMovimIDAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSMovimIDBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSMovimIDAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSMovimIDBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSMovIDLoc(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSMovIDLoc(Codigo: Integer);

  end;

var
  FmVSMovimID: TFmVSMovimID;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSMovIDLoc;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSMovimID.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSMovimID.MostraVSMovIDLoc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSMovIDLoc, FmVSMovIDLoc, afmoNegarComAviso) then
  begin
    FmVSMovIDLoc.ImgTipo.SQLType := SQLType;
    FmVSMovIDLoc.FQrCab := QrVSMovimID;
    FmVSMovIDLoc.FDsCab := DsVSMovimID;
    FmVSMovIDLoc.FQrIts := QrVSMovIDLoc;
    if SQLType = stIns then
      //FmVSMovIDLoc.Ed??
    else
    begin
      FmVSMovIDLoc.EdCodigo.ValueVariant := QrVSMovIDLocCodigo.Value;
      //
      FmVSMovIDLoc.EdStqCenLoc.ValueVariant := QrVSMovIDLocStqCenLoc.Value;
      FmVSMovIDLoc.CBStqCenLoc.KeyValue     := QrVSMovIDLocStqCenLoc.Value;
    end;
    FmVSMovIDLoc.ShowModal;
    FmVSMovIDLoc.Destroy;
  end;
end;

procedure TFmVSMovimID.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSMovimID);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSMovimID, QrVSMovIDLoc);
end;

procedure TFmVSMovimID.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSMovimID);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSMovIDLoc);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSMovIDLoc);
end;

procedure TFmVSMovimID.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSMovimIDCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSMovimID.DefParams;
begin
  VAR_GOTOTABELA := 'vsmovimid';
  VAR_GOTOMYSQLTABLE := QrVSMovimID;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsmovimid');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSMovimID.ItsAltera1Click(Sender: TObject);
begin
  MostraVSMovIDLoc(stUpd);
end;

procedure TFmVSMovimID.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSMovimID.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSMovimID.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSMovimID.ItsExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSMovIDLoc', 'Codigo', QrVSMovIDLocCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSMovIDLoc,
      QrVSMovIDLocCodigo, QrVSMovIDLocCodigo.Value);
    ReopenVSMovIDLoc(Codigo);
  end;
end;

procedure TFmVSMovimID.ReopenVSMovIDLoc(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIDLoc, Dmod.MyDB, [
  'SELECT mil.*, CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN  ',
  'FROM vsmovidloc mil ',
  'LEFT JOIN stqcenloc scl ON mil.StqCenLoc=scl.Controle ',
  'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo  ',
  'WHERE mil.MovimID=' + Geral.FF0(QrVSMovimIDCodigo.Value),
  '']);
  //
  QrVSMovIDLoc.Locate('Codigo', Codigo, []);
end;


procedure TFmVSMovimID.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSMovimID.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSMovimID.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSMovimID.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSMovimID.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSMovimID.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovimID.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSMovimIDCodigo.Value;
  Close;
end;

procedure TFmVSMovimID.ItsInclui1Click(Sender: TObject);
begin
  MostraVSMovIDLoc(stIns);
end;

procedure TFmVSMovimID.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSMovimID, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmovimid');
end;

procedure TFmVSMovimID.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsmovimid', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsmovimid', 'Codigo');
end;

procedure TFmVSMovimID.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSMovimID.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSMovimID.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSMovimID.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSMovimIDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMovimID.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSMovimID.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSMovimIDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMovimID.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSMovimID.QrVSMovimIDAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSMovimID.QrVSMovimIDAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMovIDLoc(0);
end;

procedure TFmVSMovimID.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSMovimIDCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSMovimID.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSMovimIDCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsmovimid', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSMovimID.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovimID.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSMovimID, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmovimid');
end;

procedure TFmVSMovimID.QrVSMovimIDBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMovIDLoc.Close;
end;

procedure TFmVSMovimID.QrVSMovimIDBeforeOpen(DataSet: TDataSet);
begin
  QrVSMovimIDCodigo.DisplayFormat := FFormatFloat;
end;

end.

