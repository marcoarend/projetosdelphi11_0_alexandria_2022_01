unit VSImpClaFichaRMP2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  frxClass, frxDBSet, dmkDBGridZTO, Vcl.Mask, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSImpClaFichaRMP2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    frxWET_CURTI_070_001: TfrxReport;
    frxDsVSCacIts: TfrxDBDataset;
    QrVSCacSum: TMySQLQuery;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    QrVSGerArtOld: TmySQLQuery;
    QrVSGerArtOldPecas: TFloatField;
    QrVSGerArtOldNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtOldNO_PALLET: TWideStringField;
    QrVSGerArtOldObserv: TWideStringField;
    QrVSGerArtOldNO_FORNECE: TWideStringField;
    QrVSGerArtOldNO_SerieFch: TWideStringField;
    QrVSGerArtOldNotaMPAG: TFloatField;
    frxDSVSGerArtOld: TfrxDBDataset;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DsVSCacIts: TDataSource;
    Panel5: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DSVSCacSum: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    QrVSGerArtOldFicha: TLargeintField;
    QrVSGerArtOldSerieFch: TLargeintField;
    QrVSGerArtOldGraGruX: TLargeintField;
    QrVSGerArtOldPesoKg: TFloatField;
    QrVSCacItsGraGruX: TLargeintField;
    QrVSCacItsGraGru1: TLargeintField;
    QrVSGerArtOldInteiros: TFloatField;
    QrVSCacItsInteiros: TFloatField;
    frxWET_CURTI_070_002: TfrxReport;
    QrVSGerArtOldMarca: TWideStringField;
    QrVSCacItsMartelo: TLargeintField;
    QrVSCacItsNO_MARTELO: TWideStringField;
    frxWET_CURTI_070_003: TfrxReport;
    QrMarca: TmySQLQuery;
    QrMarcaMarca: TWideStringField;
    frxDsMarca: TfrxDBDataset;
    QrMarcaPecas: TFloatField;
    QrMarcaPesoKg: TFloatField;
    QrCorda: TMySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_070_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure QrVSGerArtOldAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraFrx();
    procedure ReopenVSCacIts(Mul: Boolean);
    procedure ReopenVSGerArtSrc(Mul: Boolean);
    procedure ReopenVSCacSum(Mul: Boolean);
  public
    { Public declarations }
    FNO_Serie, FFichas: String;
    FFicha, FSerieFch: Integer;
    FPorMartelo: Boolean;
    procedure ImprimeClassFichaRMP_Uni(Preview: Boolean);
    procedure ImprimeClassFichaRMP_Mul(Preview: Boolean; PB: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    // ini 2022-03-15
    function  ReopenArtigosGerados(const Mul: Boolean; var Corda: String): Boolean;
    function  ReopenArtigosDestinos(var Corda: String): Boolean;
    procedure ReopenVSCacSum_2(Mul: Boolean; Corda: String);
    procedure ReopenVSCacIts_2(Mul: Boolean; Corda: String);
    procedure ReopenVSGerArtSrc_2(Mul: Boolean);
  end;

  var
  FmVSImpClaFichaRMP2: TFmVSImpClaFichaRMP2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnVS_PF;

{$R *.DFM}

procedure TFmVSImpClaFichaRMP2.BtOKClick(Sender: TObject);
begin
   MostraFrx();
end;

procedure TFmVSImpClaFichaRMP2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpClaFichaRMP2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpClaFichaRMP2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSImpClaFichaRMP2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpClaFichaRMP2.frxWET_CURTI_070_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_SERIE_E_FICHA_RMP' then
    Value := FNO_Serie + ' ' + Geral.FF0(FFicha)
  else
(*
  if VarName ='VARF_MARCA' then
  begin
    Value := '';
    while not QrMarca.Eof do
    begin
      Value :=  QrMarcaMarca.Value + FloatToStr(;
    end;
  end;
*)
end;

procedure TFmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Mul(Preview: Boolean;
  PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
const
  Mul = True;
var
  Corda: String;
begin
  PB.Position := 0;
  PB.Max := 6;
  //
  MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
    'Verificando itens de origem');
  ReopenVSGerArtSrc_2(Mul);
  //
  MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
    'Verificando artigos gerados');
  if ReopenArtigosGerados(Mul, Corda) then
  begin
    //Geral.MB_Teste(Corda);
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
      'Verificando artigos de destino');
    if ReopenArtigosDestinos(Corda) then
    begin
      //Geral.MB_Teste(Corda);
      // Deve ser antes!!!
      MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
        'Verificando somas de classificados');
      ReopenVSCacSum_2(Mul, Corda);
      //
      MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
        'Verificando itens individuais de classificados');
      ReopenVSCacIts_2(Mul, Corda);
      //
      if Preview then
      begin
        MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
        'Preparando impress�o');
        MostraFrx();
      end;
      MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, False, '...');
    end else
    Geral.MB_Info('N�o foi localizado nenhum artigo de destino!');
  end else
    Geral.MB_Info('N�o foi localizado nenhum artigo gerado!');
end;

procedure TFmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Uni(Preview: Boolean);
const
  Uni = False;
begin
  ReopenVSGerArtSrc(Uni);
  // Deve ser antes!!!
  ReopenVSCacSum(Uni);
  ReopenVSCacIts(Uni);
  //
  if Preview then
    MostraFrx();
end;

procedure TFmVSImpClaFichaRMP2.MostraFrx();
var
  Report: TfrxReport;
begin
  if FPorMartelo then
    Report := frxWET_CURTI_070_003
  else
    Report := frxWET_CURTI_070_002;
    //
  //MyObjects.frxDefineDataSets(frxWET_CURTI_070_001, [
  MyObjects.frxDefineDataSets(Report, [
  DModG.frxDsDono,
  frxDsVSCacIts,
  //frxDsVSGerArtNew,
  frxDsMarca,
  frxDsVSGerArtOld
  ]);
  //MyObjects.frxMostra(frxWET_CURTI_070_001, 'Romaneio de Artigo de Ribeira');
  MyObjects.frxMostra(Report, 'Romaneio de Artigo de Ribeira');
end;

procedure TFmVSImpClaFichaRMP2.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSImpClaFichaRMP2.QrVSGerArtOldAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMarca, Dmod.MyDB, [
  'SELECT Marca, Pecas, PesoKg ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=1 ',
  'AND SerieFch=' + Geral.FF0(QrVSGerArtOldSerieFch.Value),
  'AND Ficha=' + Geral.FF0(QrVSGerArtOldFicha.Value),
  '']);
end;

function TFmVSImpClaFichaRMP2.ReopenArtigosDestinos(var Corda: String): Boolean;
  function GeraSQLVSMovItx(TabMov(*, TabCac*): TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT DISTINCT DstNivel2 ',
    'FROM vsmovits vmi  ',
    'WHERE vmi.Controle IN (' + Corda + ') ',
    'AND DstMovID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)), // 6
    '']);
  end;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB),
  ' UNION ',
  GeraSQLVSMovItx(ttwA),
  '']);
  //Geral.MB_Teste(QrCorda.SQL.Text);
  Corda := MyObjects.CordaDeQuery(QrCorda, 'DstNivel2', EmptyStr);
  Result := Trim(Corda) <> EmptyStr;
end;

function TFmVSImpClaFichaRMP2.ReopenArtigosGerados(const Mul: Boolean; var Corda: String): Boolean;
  function GeraSQLVSMovItx(TabMov(*, TabCac*): TTabToWork): String;
  var
    SQL_Ficha: String;
  begin
    if Mul then
      SQL_Ficha := 'AND vmi.Ficha IN (' + FFichas + ')'
    else
      SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(FFicha);
    //

    Result := Geral.ATS([
    'SELECT vmi.Controle ',
    'FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    'WHERE vmi.SerieFch=' + Geral.FF0(FSerieFch),
    SQL_Ficha,
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)), // 6
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
    '']);
  end;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB),
  ' UNION ',
  GeraSQLVSMovItx(ttwA),
  '']);
  //Geral.MB_Teste(QrArtGer.SQL.Text);
  Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', EmptyStr);
  Result := Trim(Corda) <> EmptyStr;
end;

procedure TFmVSImpClaFichaRMP2.ReopenVSCacIts(Mul: Boolean);
var
  ORDER_BY, GROUP_BY: String;
  //
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
    SQL_Fichas, Corda: String;
  begin
    if Mul then
      SQL_Fichas := '  AND Ficha IN (' + FFichas + ')'
    else
      SQL_Fichas := '  AND Ficha=' + Geral.FF0(FFicha);

    // Ini Corda
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
    '  SELECT Controle ',
    '  FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    '  WHERE SerieFch=' + Geral.FF0(FSerieFch),
    SQL_Fichas,
    '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    // ini 2022-03-14
    '  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
 (*
    '  AND (MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)), // 13
    '   OR  vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
    '  ) ',
 *)
    // fim 2022-03-14
    '']);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', EmptyStr);
    // Fim Corda

    if Corda = EmptyStr then
      Result := ''
    else
    begin
      Une := not ((TabMov = ttwA) and (TabCac = ttwA));
      //
      Result := Geral.ATS([
  //    'SELECT',
      'SELECT cia.Martelo, mrt.Nome NO_MARTELO, ',
       VS_PF.SQL_NO_GGX(),
      'CAST(vsp.GraGruX AS SIGNED) GraGruX, ',
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
      'SUM(cia.Pecas) Pecas, ',
      'SUM(cia.AreaM2) AreaM2, ',
      'SUM(cia.AreaP2) AreaP2, ',
      'CAST(SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) AS DECIMAL (15,2)) Inteiros ',
      'FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
      'LEFT JOIN ' + TMeuDB + '.vspalleta vsp ON vsp.Codigo=cia.VSPallet ',
      'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
      'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'LEFT JOIN ' + TMeuDB + '.vsmrtcad   mrt ON mrt.Codigo=cia.Martelo ',
      'WHERE VMI_Sorc IN ( ' + Corda,
      (*
      '  SELECT Controle ',
      '  FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
      '  WHERE SerieFch=' + Geral.FF0(FSerieFch),
      SQL_Fichas,
      '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      '  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
      *)
      ') ',
      //'GROUP BY GraGruX',
      GROUP_BY,
      Geral.ATS_If(Une, ['', 'UNION']),
      '']);
    end;
  end;
begin
  if FPorMartelo then
  begin
    GROUP_BY := 'GROUP BY Martelo, GraGruX';
    ORDER_BY := 'ORDER BY Martelo, AreaM2 DESC;';
  end else
  begin
    GROUP_BY := 'GROUP BY GraGruX';
    ORDER_BY := 'ORDER BY AreaM2 DESC;';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_its_; ',
  'CREATE TABLE _vs037_cia_its_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  ';',
  'SELECT NO_MARTELO, ',
  'CAST(Martelo AS SIGNED) Martelo, ',
  //'SELECT',
  'CAST(GraGruX AS SIGNED) GraGruX, ',
  'CAST(GraGru1 AS SIGNED) GraGru1, ',
  'SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(Inteiros) Inteiros, ',
  'NO_PRD_TAM_COR ',
  'FROM _vs037_cia_its_',
  GROUP_BY,
  ORDER_BY,
  '']);
  //Geral.MB_Teste(QrVSCacIts.SQL.Text);
end;

procedure TFmVSImpClaFichaRMP2.ReopenVSCacIts_2(Mul: Boolean; Corda: String);
var
  ORDER_BY, GROUP_BY: String;
  //
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
    SQL_Fichas: String;
  begin
      Une := not ((TabMov = ttwA) and (TabCac = ttwA));
      //
      Result := Geral.ATS([
       'SELECT cia.Martelo, mrt.Nome NO_MARTELO, ',
       VS_PF.SQL_NO_GGX(),
      'CAST(vsp.GraGruX AS SIGNED) GraGruX, ',
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
      'SUM(cia.Pecas) Pecas, ',
      'SUM(cia.AreaM2) AreaM2, ',
      'SUM(cia.AreaP2) AreaP2, ',
      'CAST(SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) AS DECIMAL (15,2)) Inteiros ',
      'FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
      'LEFT JOIN ' + TMeuDB + '.vspalleta vsp ON vsp.Codigo=cia.VSPallet ',
      'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
      'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'LEFT JOIN ' + TMeuDB + '.vsmrtcad   mrt ON mrt.Codigo=cia.Martelo ',
      'WHERE VMI_Sorc IN ( ' + Corda,
      ') ',
      //'GROUP BY GraGruX',
      GROUP_BY,
      Geral.ATS_If(Une, ['', 'UNION']),
      '']);
  end;
begin
  if FPorMartelo then
  begin
    GROUP_BY := 'GROUP BY Martelo, GraGruX';
    ORDER_BY := 'ORDER BY Martelo, AreaM2 DESC;';
  end else
  begin
    GROUP_BY := 'GROUP BY GraGruX';
    ORDER_BY := 'ORDER BY AreaM2 DESC;';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_its_; ',
  'CREATE TABLE _vs037_cia_its_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  ';',
  'SELECT NO_MARTELO, ',
  'CAST(Martelo AS SIGNED) Martelo, ',
  //'SELECT',
  'CAST(GraGruX AS SIGNED) GraGruX, ',
  'CAST(GraGru1 AS SIGNED) GraGru1, ',
  'SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(Inteiros) Inteiros, ',
  'NO_PRD_TAM_COR ',
  'FROM _vs037_cia_its_',
  GROUP_BY,
  ORDER_BY,
  '']);
  //Geral.MB_Teste(QrVSCacIts.SQL.Text);
end;

(*
procedure TFmVSImpClaFichaRMP.ReopenVSCacSum(Mul: Boolean);
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
    SQL_Ficha: String;
  begin
    if Mul then
      SQL_Ficha := 'AND Ficha IN (' + FFichas + ')'
    else
      SQL_Ficha := 'AND Ficha=' + Geral.FF0(FFicha);
    //
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    Result := Geral.ATS([
    'SELECT SUM(cia.Pecas) Pecas,  ',
    'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2  ',
    'FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
    'WHERE VMI_Sorc IN (  ',
    '  SELECT Controle  ',
    '  FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    '  WHERE SerieFch=' + Geral.FF0(FSerieFch),
    SQL_Ficha,
    '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    '  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
    ')  ',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_sum_; ',
  'CREATE TABLE _vs037_cia_sum_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  '; ',
  'SELECT SUM(Pecas) Pecas,  ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  ',
  'FROM _vs037_cia_sum_; ',
  '']);
end;
*)

procedure TFmVSImpClaFichaRMP2.ReopenVSCacSum(Mul: Boolean);
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
    SQL_Ficha, Corda: String;
  begin
    if Mul then
      SQL_Ficha := '  AND Ficha IN (' + FFichas + ')'
    else
      SQL_Ficha := '  AND Ficha=' + Geral.FF0(FFicha);
    //
    // Ini Corda
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
    '  SELECT Controle  ',
    '  FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    '  WHERE SerieFch=' + Geral.FF0(FSerieFch),
    SQL_Ficha,
    '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
    // ini 2022-03-14
    //'  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
    '  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCurtiXX)),
{
    '  AND MovimNiv IN (',
    Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)) + ',', // 13
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcCurtiXX)) + ')', // 14
}
    // fim 2022-03-14
    '']);
    //Geral.MB_Teste(QrCorda.SQL.Text);

    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', EmptyStr);
    // Fim Corda
    if Corda = EmptyStr then
      Result := ''
    else
    begin
      Une := not ((TabMov = ttwA) and (TabCac = ttwA));
      //
      Result := Geral.ATS([
      'SELECT SUM(cia.Pecas) Pecas,  ',
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2  ',
      'FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
      'WHERE VMI_Sorc IN (  ', Corda,
      (*
      '  SELECT Controle  ',
      '  FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
      '  WHERE SerieFch=' + Geral.FF0(FSerieFch),
      SQL_Ficha,
      '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      '  AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
      *)
      ')  ',
      Geral.ATS_If(Une, ['', 'UNION']),
      '']);
    end;
  end;
begin
{ ini 2022-03-14
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_sum_; ',
  'CREATE TABLE _vs037_cia_sum_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  '; ',
  'SELECT SUM(Pecas) Pecas,  ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  ',
  'FROM _vs037_cia_sum_; ',
  '']);
  //Geral.MB_Teste(QrVSCacSum.SQL.Text);
 fim 2022-03-14}
end;

procedure TFmVSImpClaFichaRMP2.ReopenVSCacSum_2(Mul: Boolean; Corda: String);
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
    SQL_Ficha: String;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    //
    Result := Geral.ATS([
    'SELECT SUM(cia.Pecas) Pecas,  ',
    'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2  ',
    'FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
    'WHERE VMI_Sorc IN (  ', Corda, ')  ',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_sum_; ',
  'CREATE TABLE _vs037_cia_sum_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  '; ',
  'SELECT SUM(Pecas) Pecas,  ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  ',
  'FROM _vs037_cia_sum_; ',
  '']);
  //Geral.MB_Teste(QrVSCacSum.SQL.Text);
end;

procedure TFmVSImpClaFichaRMP2.ReopenVSGerArtSrc(Mul: Boolean);
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  var
    SQL_Ficha: String;
  begin
    if Mul then
      SQL_Ficha := 'AND vmi.Ficha IN (' + FFichas + ')'
    else
      SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(FFicha);
    //
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.NotaMPAG AS DECIMAL(15,8)) NotaMPAG, ',
      'CAST(vmi.Pecas AS DECIMAL(15,3)) Pecas, ',
      'CAST(vmi.PesoKg AS DECIMAL(15,3)) PesoKg, ',
      'CAST(vmi.Observ AS CHAR) Observ,',
      'CAST(vmi.Marca AS CHAR) Marca,',
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
      'CAST(vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) AS DECIMAL (15,2)) Inteiros, ',
      VS_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet,',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
      'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE vmi.SerieFch=' + Geral.FF0(FSerieFch),
      SQL_Ficha,
      'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      // ini 2022-03-14
      'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)), // 13
      //'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
      // fim 2022-03-14
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
const
  TemIMEIMrt = 1;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtOld, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB, TemIMEIMrt),
  GeraSQLVSMovItx(ttwA),
  //'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_Teste(QrVSGerArtOld.SQL.Text);
end;

procedure TFmVSImpClaFichaRMP2.ReopenVSGerArtSrc_2(Mul: Boolean);
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  var
    SQL_Ficha: String;
  begin
    if Mul then
      SQL_Ficha := 'AND vmi.Ficha IN (' + FFichas + ')'
    else
      SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(FFicha);
    //
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.NotaMPAG AS DECIMAL(15,8)) NotaMPAG, ',
      'CAST(vmi.Pecas AS DECIMAL(15,3)) Pecas, ',
      'CAST(vmi.PesoKg AS DECIMAL(15,3)) PesoKg, ',
      'CAST(vmi.Observ AS CHAR) Observ,',
      'CAST(vmi.Marca AS CHAR) Marca,',
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
      'CAST(vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) AS DECIMAL (15,2)) Inteiros, ',
      VS_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet,',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
      'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE vmi.SerieFch=' + Geral.FF0(FSerieFch),
      SQL_Ficha,
      'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
const
  TemIMEIMrt = 1;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtOld, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB, TemIMEIMrt),
  GeraSQLVSMovItx(ttwA),
  '']);
  //Geral.MB_Teste(QrVSGerArtOld.SQL.Text);
end;

end.
