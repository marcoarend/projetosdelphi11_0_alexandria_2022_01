unit VSPSPDst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup, Vcl.Menus,
  UnProjGroup_Consts, dmkCheckBox, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnAppEnums;

type
  TFmVSPSPDst = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdCtrl1: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    EdFicha: TdmkEdit;
    Label4: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    GroupBox3: TGroupBox;
    CkBaixa: TCheckBox;
    PnBaixa: TPanel;
    Label8: TLabel;
    EdBxaPecas: TdmkEdit;
    LaBxaPesoKg: TLabel;
    EdBxaPesoKg: TdmkEdit;
    LaBxaAreaM2: TLabel;
    EdBxaAreaM2: TdmkEditCalc;
    LaBxaAreaP2: TLabel;
    EdBxaAreaP2: TdmkEditCalc;
    Label16: TLabel;
    EdBxaValorT: TdmkEdit;
    EdCtrl2: TdmkEdit;
    Label17: TLabel;
    EdBxaObserv: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    Label20: TLabel;
    LaVSRibCla: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet1: TSpeedButton;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdPedItsFin: TdmkEditCB;
    Label48: TLabel;
    CBPedItsFin: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    CkEncerraPallet: TdmkCheckBox;
    SBNewPallet: TSpeedButton;
    MeAviso: TMemo;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    CkCpermitirCrust: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkBaixaClick(Sender: TObject);
    procedure EdBxaAreaM2Change(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure EdBxaPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaAreaM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkCpermitirCrustClick(Sender: TObject);
    procedure EdAreaP2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdBxaPesoKgChange(Sender: TObject);
  private
    { Private declarations }
    FUltGGX, FGraGruX: Integer;
    //
    procedure ReopenVSInnIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FFornecMO: Integer;
    FValKg: Double;
    FPallOnEdit: array of Integer;
  end;

  var
  FmVSPSPDst: TFmVSPSPDst;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSPSPCab, Principal, UnVS_PF, AppListas, UnGrade_PF, ModuleGeral;

{$R *.DFM}

procedure TFmVSPSPDst.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0;
  //Pallet     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 1;
  NotaMPAG   = 0;
  //
  Fornecedor = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  //
  TpCalcAuto = -1;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  //PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  NoPallet   = nil;
  NoPecas    = nil;
var
  DataHora, Observ, Marca: String;
  //Misturou,
  Codigo, Controle, MovimCod, Empresa, GraGruX, SerieFch, Ficha, Ctrl1, Ctrl2,
  MovimTwn, Pallet, PedItsFin, StqCenLoc, ReqMovEstq, ClientMO, FornecMO,
  PalToClose: Integer;
  Pecas, PesoKg, ValorT, AreaM2, AreaP2: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
  //
begin
  if MyObjects.FIC(TPData.Date<2, TPData, 'Data/hora n�o definida!') then Exit;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := 0;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Ctrl1          := EdCtrl1.ValueVariant;
  Ctrl2          := EdCtrl2.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidEmProcSP;
  MovimNiv       := eminDestPSP;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;
  Marca          := EdMarca.Text;
  Pallet         := EdPallet.ValueVariant;
  //Misturou       := RGMisturou.ItemIndex;
  PedItsFin      := EdPedItsFin.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  if VS_PF.FichaErro(EdSerieFch, Empresa, Ctrl1, Ficha) then
    Exit;
  // Pode ser algo por peso sem custo!
  if VS_PF.VSFic(GraGruX, Empresa, ClientMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, NoPallet, (*EdFicha*)nil, NoPecas,
    EdAreaM2, EdPesoKg, (*EdValorT*)nil, ExigeFornecedor, CO_GraGruY_0853_VSPSPEnd,
    ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
(*
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
*)
  //
  PalToClose := Pallet;
  if CkBaixa.Checked then
  begin
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  end else
    MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl1);
  if VS_PF.InsUpdVSMovIts2(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei083(*Gera��o de subproduto processado em processamento de subproduto*)) then
  begin
    Ctrl1  := Controle;
    Pallet := 0;
    VS_PF.AtualizaSaldoIMEI(Ctrl1, True);
    VS_PF.AtualizaVSPedIts_Fin(PedItsFin);
    //
    if CkBaixa.Checked then
    begin
      GraGruX        := FmVSPSPCab.QrVSPSPAtuGraGruX.Value;
      SerieFch       := FmVSPSPCab.QrVSPSPAtuSerieFch.Value;
      Ficha          := FmVSPSPCab.QrVSPSPAtuFicha.Value;
      Marca          := FmVSPSPCab.QrVSPSPAtuMarca.Value;
      MovimID        := emidEmProcSP;
      MovimNiv       := eminEmPSPBxa;
      AreaM2         := -EdBxaAreaM2.ValueVariant;
      AreaP2         := -EdBxaAreaP2.ValueVariant;
      Pecas          := -EdBxaPecas.ValueVariant;
      PesoKg         := -EdBxaPesoKg.ValueVariant;
      ValorT         := -EdBxaValorT.ValueVariant;
      Observ         := EdBxaObserv.Text;
      //
      //PedItsLib      := 0;
      PedItsFin      := 0;
      //PedItsVda      := 0;
      //GSPSrcMovID     := 0;
      //GSPSrcNiv2     := 0;
      ReqMovEstq     := 0;
      if Ctrl2 = 0 then
        SQLType := stIns
      else
        SQLType := ImgTipo.SQLType;
      //
      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Ctrl2);
      if VS_PF.InsUpdVSMovIts2(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
      MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
      CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
      SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
      AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei084(* = 'Baixa de subproduto em processo na gera��o de produto processado em processamento de subproduto*)) then
      begin
        Ctrl2 := Controle;
        //VS_PF.AtualizaSaldoIMEI(Ctrl2, True);
        //VS_PF.AtualizaSaldoIMEI(FmVSPSPCab.QrVSPSPAtuControle.Value, True);
      end;
    end;
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSPSPCab', MovimCod);
    VS_PF.AtualizaTotaisVSPSPCab(FmVSPSPCab.QrVSPSPCabMovimCod.Value);
    FmVSPSPCab.AtualizaNFeItens();
    //
    if CkEncerraPallet.Checked then
      VS_PF.EncerraPalletSimples(PalToClose, FEmpresa, FClientMO, QrVSPallet, FPallOnEdit);
    VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcSP, MovimCod);
    FmVSPSPCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Ctrl1);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdCtrl1.ValueVariant       := 0;
      EdCtrl2.ValueVariant       := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdObserv.Text              := '';
      EdFicha.ValueVariant       := 0;
      EdMarca.ValueVariant       := '';
      //
      CkBaixa.Checked            := False;
      EdBxaPecas.ValueVariant    := 0;
      EdBxaPesoKg.ValueVariant   := 0;
      EdBxaAreaM2.ValueVariant   := 0;
      EdBxaAreaP2.ValueVariant   := 0;
      EdBxaValorT.ValueVariant   := 0;
      EdBxaObserv.ValueVariant   := '';
      //
      EdGraGruX.Enabled          := True;
      CBGraGruX.Enabled          := True;
      //EdSerieFch.Enabled         := True;
      //CBSerieFch.Enabled         := True;
      //EdFicha.Enabled            := True;
      //EdMarca.Enabled            := True;
      //EdValorT.Enabled           := True;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSPSPDst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPSPDst.CkBaixaClick(Sender: TObject);
begin
  PnBaixa.Visible := CkBaixa.Checked;
end;

procedure TFmVSPSPDst.CkCpermitirCrustClick(Sender: TObject);
begin
  if CkCpermitirCrust.Checked then
    VS_PF.AbreGraGruXY(QrGraGruX,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0512_VSSubPrd))
  else
    VS_PF.AbreGraGruXY(QrGraGruX,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0853_VSPSPEnd));
end;

procedure TFmVSPSPDst.Criar1Click(Sender: TObject);
(*
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  VS_PF.CadastraPalletWetEnd(FEmpresa, EdPallet, CBPallet, QrVSPallet,
    emidEmProcSP, GraGruX);
*)
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  VS_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrVSPallet, emidEmProcSP, GraGruX);
end;

procedure TFmVSPSPDst.EdAreaP2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if FmVSPSPCab.QrVSPSPCabPecasINI.Value > 0 then
      EdAreaP2.ValueVariant := EdPecas.ValueVariant *
      FmVSPSPCab.QrVSPSPCabAreaINIP2.Value / FmVSPSPCab.QrVSPSPCabPecasINI.Value
    else
      EdAreaP2.ValueVariant := 0;
  end;
end;

procedure TFmVSPSPDst.EdBxaAreaM2Change(Sender: TObject);
begin
  EdBxaValorT.ValueVariant := FValKg * EdBxaAreaM2.ValueVariant;
end;

procedure TFmVSPSPDst.EdBxaAreaM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdBxaAreaM2.ValueVariant := EdAreaM2.ValueVariant;
end;

procedure TFmVSPSPDst.EdBxaPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FatorIntSrc, FatorIntDst, //: Double;
  Fator: Integer;
begin
  if Key = VK_F4 then
  begin
    VS_PF.ObtemFatorInteiro(FmVSPSPCab.QrVSPSPAtuGraGruX.Value, FatorIntSrc, False, 1, MeAviso);
    VS_PF.ObtemFatorInteiro(EdGraGruX.ValueVariant, FatorIntDst, True, FatorIntSrc, MeAviso);
    EdBxaPecas.ValueVariant := EdPecas.ValueVariant * FatorIntSrc / FatorIntDst;
  end;
  if Key = VK_F3 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdBxaPecas.ValueVariant := EdPecas.ValueVariant / Fator;
  end;
end;

procedure TFmVSPSPDst.EdBxaPesoKgChange(Sender: TObject);
begin
  EdValorT.ValueVariant := FValKg * EdBxaPesoKg.ValueVariant;
end;

procedure TFmVSPSPDst.EdBxaPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    //EdBxaPesoKg.ValueVariant := EdPesoKg.ValueVariant;
    EdBxaPesoKg.ValueVariant := FMVSPSPCab.QrVSPSPCabPesoKgSdo.Value;
end;

procedure TFmVSPSPDst.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ficha: Integer;
begin
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
end;

procedure TFmVSPSPDst.EdGraGruXChange(Sender: TObject);
begin
  FGraGruX := EdGraGruX.ValueVariant;
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue     := Null;
  if FGraGruX = 0 then
    QrVSPallet.Close
  else
    VS_PF.ReopenVSPallet(QrVSPallet, FEmpresa, FClientMO, FGraGruX, '', FPallOnEdit);
end;

procedure TFmVSPSPDst.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdPecas.ValueVariant  := FmVSPSPCab.QrVSPSPCabPecasSdo.Value;
    EdPesoKg.ValueVariant := FmVSPSPCab.QrVSPSPCabPesoKgSdo.Value;
    EdAreaM2.ValueVariant := FmVSPSPCab.QrVSPSPCabAreaSdoM2.Value;
  end;
end;

procedure TFmVSPSPDst.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSPSPDst.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  SetLength(FPallOnEdit, 0);
  FUltGGX  := 0;
  FGraGruX := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0853_VSPSPEnd));

  VS_PF.AbreVSSerFch(QrVSSerFch);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSPSPDst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPSPDst.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(EdPallet.ValueVariant);
end;

procedure TFmVSPSPDst.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSPSPDst.SBNewPalletClick(Sender: TObject);
begin
  VS_PF.GeraNovoPallet(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  EdGraGruX, EdPallet, CBPallet, SBNewPallet, QrVSPallet, FPallOnEdit);
end;

procedure TFmVSPSPDst.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

end.
