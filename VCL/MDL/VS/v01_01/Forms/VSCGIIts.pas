unit VSCGIIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO,
  UnProjGroup_Consts;

type
  TFmVSCGIIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrIMEIs: TmySQLQuery;
    QrIMEIsVMI_Sorc: TIntegerField;
    QrIMEIsNO_SerieFch: TWideStringField;
    QrIMEIsFicha: TIntegerField;
    QrIMEIsNO_FORNECE: TWideStringField;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    DBGIMEIs: TdmkDBGridZTO;
    DsIMEIs: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSCGIIts(Controle: Integer);
    procedure ReopenIMEIs();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSCGIIts: TFmVSCGIIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSCGIIts.BtOKClick(Sender: TObject);
var
  I, N, Codigo, Controle, VSMovIts: Integer;
  //
  procedure InsereAtual();
  begin
    Controle := UMyMod.BPGS1I32('vscgiits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    VSMovIts := QrIMEIsVMI_Sorc.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vscgiits', False, [
    'Codigo', CO_FLD_TAB_VMI], [
    'Controle'], [
    Codigo, VSMovIts], [
    Controle], True) then
    begin
    end;
  end;
begin
  QrIMEIs.DisableControls;
  try
    Codigo   := Geral.IMV(DBEdCodigo.Text);
    Controle := 0;
    VSMovIts := 0;
    //
    N := 0;
    with DBGIMEIs.DataSource.DataSet do
    for I := 0 to DBGIMEIs.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGIMEIs.SelectedRows.Items[I]));
      GotoBookmark(DBGIMEIs.SelectedRows.Items[I]);
      //
      InsereAtual();
      N := N + 1;
    end;
  finally
    QrIMEIs.EnableControls;
  end;
  if N = 0 then
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!')
  else
  begin
    ReopenVSCGIIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      ReopenIMEIs();
    end else Close;
  end;
end;

procedure TFmVSCGIIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCGIIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSCGIIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenIMEIs();
end;

procedure TFmVSCGIIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCGIIts.ReopenIMEIs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  'SELECT DISTINCT VMI_Sorc, vsf.Nome NO_SerieFch, vmi.Ficha,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,',
  'CONCAT(gg1.Nome,',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))',
  '  )',
  '  NO_PRD_TAM_COR',
  'FROM vspaclaitsa  pci',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pci.VMI_Sorc',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'WHERE NOT ( ',
  '  pci.VMI_Sorc IN ( ',
  '    SELECT DISTINCT VSMovIts ',
  '    FROM vscgiits ',
  '  ) ',
  ') ',
  'ORDER BY VMI_Sorc DESC ',
  '']);
end;

procedure TFmVSCGIIts.ReopenVSCGIIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
