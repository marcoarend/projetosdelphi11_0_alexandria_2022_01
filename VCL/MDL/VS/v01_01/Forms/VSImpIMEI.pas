unit VSImpIMEI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  mySQLDbTables, dmkGeral, UnInternalConsts, UnDmkEnums, UnProjGroup_Consts,
  UnDmkProcFunc, UnVS_CRC_PF, UnProjGroup_PF, UnAppEnums;

type
  TFmVSImpIMEI = class(TForm)
    QrOld01: TmySQLQuery;
    QrOld01Pecas: TFloatField;
    QrOld01PesoKg: TFloatField;
    QrOld01NO_PRD_TAM_COR: TWideStringField;
    QrOld01Observ: TWideStringField;
    QrOld01NO_FORNECE: TWideStringField;
    QrOld01NO_SerieFch: TWideStringField;
    frxDsOld01: TfrxDBDataset;
    QrOld02: TmySQLQuery;
    frxDsOld02: TfrxDBDataset;
    QrOld03: TmySQLQuery;
    frxDsOld03: TfrxDBDataset;
    QrOld04: TmySQLQuery;
    frxDsOld04: TfrxDBDataset;
    QrOld05: TmySQLQuery;
    frxDsOld05: TfrxDBDataset;
    frxDsEstqR4: TfrxDBDataset;
    frxWET_CURTI_025_01: TfrxReport;
    QrVSGerArtOld: TmySQLQuery;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Codigo: TIntegerField;
    QrEstqR4Controle: TIntegerField;
    QrEstqR4MovimCod: TIntegerField;
    QrEstqR4MovimNiv: TIntegerField;
    QrEstqR4MovimID: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4NO_EMPRESA: TWideStringField;
    QrEstqR4Cliente: TIntegerField;
    QrEstqR4NO_CLIENTE: TWideStringField;
    QrEstqR4Fornece: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Status: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    QrEstqR4DataHora: TDateTimeField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4OrdGGY: TIntegerField;
    QrEstqR4GraGruY: TIntegerField;
    QrEstqR4NO_GGY: TWideStringField;
    QrEstqR4NO_FICHA: TWideStringField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4TERCEIRO: TIntegerField;
    QrEstqR4TIPO_TERCEIRO: TWideStringField;
    QrEstqR4NO_TIPO_COURO: TWideStringField;
    QrEstqR4NO_ACAO_COURO: TWideStringField;
    frxWET_CURTI_025_02_A: TfrxReport;
    QrEstqR4ReqMovEstq: TIntegerField;
    QrEstqR4Observ: TWideStringField;
    QrEstqR4PedItsLib: TIntegerField;
    frxWET_CURTI_025_03: TfrxReport;
    QrOPs: TmySQLQuery;
    frxDsOPs: TfrxDBDataset;
    QrEstqR4NO_FRNCALC: TWideStringField;
    QrVSGerArtOldCodigo: TLargeintField;
    QrVSGerArtOldControle: TLargeintField;
    QrVSGerArtOldMovimCod: TLargeintField;
    QrVSGerArtOldMovimNiv: TLargeintField;
    QrVSGerArtOldMovimTwn: TLargeintField;
    QrVSGerArtOldEmpresa: TLargeintField;
    QrVSGerArtOldTerceiro: TLargeintField;
    QrVSGerArtOldCliVenda: TLargeintField;
    QrVSGerArtOldMovimID: TLargeintField;
    QrVSGerArtOldDataHora: TDateTimeField;
    QrVSGerArtOldPallet: TLargeintField;
    QrVSGerArtOldGraGruX: TLargeintField;
    QrVSGerArtOldPecas: TFloatField;
    QrVSGerArtOldPesoKg: TFloatField;
    QrVSGerArtOldAreaM2: TFloatField;
    QrVSGerArtOldAreaP2: TFloatField;
    QrVSGerArtOldValorT: TFloatField;
    QrVSGerArtOldSrcMovID: TLargeintField;
    QrVSGerArtOldSrcNivel1: TLargeintField;
    QrVSGerArtOldSrcNivel2: TLargeintField;
    QrVSGerArtOldSrcGGX: TLargeintField;
    QrVSGerArtOldSdoVrtPeca: TFloatField;
    QrVSGerArtOldSdoVrtPeso: TFloatField;
    QrVSGerArtOldSdoVrtArM2: TFloatField;
    QrVSGerArtOldObserv: TWideStringField;
    QrVSGerArtOldSerieFch: TLargeintField;
    QrVSGerArtOldFicha: TLargeintField;
    QrVSGerArtOldMisturou: TLargeintField;
    QrVSGerArtOldFornecMO: TLargeintField;
    QrVSGerArtOldCustoMOKg: TFloatField;
    QrVSGerArtOldCustoMOM2: TFloatField;
    QrVSGerArtOldCustoMOTot: TFloatField;
    QrVSGerArtOldValorMP: TFloatField;
    QrVSGerArtOldDstMovID: TLargeintField;
    QrVSGerArtOldDstNivel1: TLargeintField;
    QrVSGerArtOldDstNivel2: TLargeintField;
    QrVSGerArtOldDstGGX: TLargeintField;
    QrVSGerArtOldQtdGerPeca: TFloatField;
    QrVSGerArtOldQtdGerPeso: TFloatField;
    QrVSGerArtOldQtdGerArM2: TFloatField;
    QrVSGerArtOldQtdGerArP2: TFloatField;
    QrVSGerArtOldQtdAntPeca: TFloatField;
    QrVSGerArtOldQtdAntPeso: TFloatField;
    QrVSGerArtOldQtdAntArM2: TFloatField;
    QrVSGerArtOldQtdAntArP2: TFloatField;
    QrVSGerArtOldNotaMPAG: TFloatField;
    QrVSGerArtOldNO_PALLET: TWideStringField;
    QrVSGerArtOldNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtOldNO_TTW: TWideStringField;
    QrVSGerArtOldID_TTW: TLargeintField;
    QrVSGerArtOldNO_FORNECE: TWideStringField;
    QrVSGerArtOldNO_SerieFch: TWideStringField;
    QrVSGerArtOldReqMovEstq: TLargeintField;
    QrOld01Controle: TLargeintField;
    QrOld01GraGruX: TLargeintField;
    QrOld01Ficha: TLargeintField;
    QrOld02Controle: TLargeintField;
    QrOld02GraGruX: TLargeintField;
    QrOld02Pecas: TFloatField;
    QrOld02PesoKg: TFloatField;
    QrOld02NO_PRD_TAM_COR: TWideStringField;
    QrOld02Observ: TWideStringField;
    QrOld02Ficha: TLargeintField;
    QrOld02NO_FORNECE: TWideStringField;
    QrOld02NO_SerieFch: TWideStringField;
    QrOld03Controle: TLargeintField;
    QrOld03GraGruX: TLargeintField;
    QrOld03Pecas: TFloatField;
    QrOld03PesoKg: TFloatField;
    QrOld03NO_PRD_TAM_COR: TWideStringField;
    QrOld03Observ: TWideStringField;
    QrOld03Ficha: TLargeintField;
    QrOld03NO_FORNECE: TWideStringField;
    QrOld03NO_SerieFch: TWideStringField;
    QrOld04Controle: TLargeintField;
    QrOld04GraGruX: TLargeintField;
    QrOld04Pecas: TFloatField;
    QrOld04PesoKg: TFloatField;
    QrOld04NO_PRD_TAM_COR: TWideStringField;
    QrOld04Observ: TWideStringField;
    QrOld04Ficha: TLargeintField;
    QrOld04NO_FORNECE: TWideStringField;
    QrOld04NO_SerieFch: TWideStringField;
    QrOld05Controle: TLargeintField;
    QrOld05GraGruX: TLargeintField;
    QrOld05Pecas: TFloatField;
    QrOld05PesoKg: TFloatField;
    QrOld05NO_PRD_TAM_COR: TWideStringField;
    QrOld05Observ: TWideStringField;
    QrOld05Ficha: TLargeintField;
    QrOld05NO_FORNECE: TWideStringField;
    QrOld05NO_SerieFch: TWideStringField;
    frxWET_CURTI_025_04: TfrxReport;
    frxReport1: TfrxReport;
    QrEstqR4SdoVrtArP2: TFloatField;
    frxWET_CURTI_025_02_B: TfrxReport;
    QrVSPWEDst: TmySQLQuery;
    QrVSPWEDstCodigo: TLargeintField;
    QrVSPWEDstControle: TLargeintField;
    QrVSPWEDstMovimCod: TLargeintField;
    QrVSPWEDstMovimNiv: TLargeintField;
    QrVSPWEDstMovimTwn: TLargeintField;
    QrVSPWEDstEmpresa: TLargeintField;
    QrVSPWEDstTerceiro: TLargeintField;
    QrVSPWEDstCliVenda: TLargeintField;
    QrVSPWEDstMovimID: TLargeintField;
    QrVSPWEDstDataHora: TDateTimeField;
    QrVSPWEDstPallet: TLargeintField;
    QrVSPWEDstGraGruX: TLargeintField;
    QrVSPWEDstPecas: TFloatField;
    QrVSPWEDstPesoKg: TFloatField;
    QrVSPWEDstAreaM2: TFloatField;
    QrVSPWEDstAreaP2: TFloatField;
    QrVSPWEDstValorT: TFloatField;
    QrVSPWEDstSrcMovID: TLargeintField;
    QrVSPWEDstSrcNivel1: TLargeintField;
    QrVSPWEDstSrcNivel2: TLargeintField;
    QrVSPWEDstSrcGGX: TLargeintField;
    QrVSPWEDstSdoVrtPeca: TFloatField;
    QrVSPWEDstSdoVrtPeso: TFloatField;
    QrVSPWEDstSdoVrtArM2: TFloatField;
    QrVSPWEDstObserv: TWideStringField;
    QrVSPWEDstSerieFch: TLargeintField;
    QrVSPWEDstFicha: TLargeintField;
    QrVSPWEDstMisturou: TLargeintField;
    QrVSPWEDstFornecMO: TLargeintField;
    QrVSPWEDstCustoMOKg: TFloatField;
    QrVSPWEDstCustoMOM2: TFloatField;
    QrVSPWEDstCustoMOTot: TFloatField;
    QrVSPWEDstValorMP: TFloatField;
    QrVSPWEDstDstMovID: TLargeintField;
    QrVSPWEDstDstNivel1: TLargeintField;
    QrVSPWEDstDstNivel2: TLargeintField;
    QrVSPWEDstDstGGX: TLargeintField;
    QrVSPWEDstQtdGerPeca: TFloatField;
    QrVSPWEDstQtdGerPeso: TFloatField;
    QrVSPWEDstQtdGerArM2: TFloatField;
    QrVSPWEDstQtdGerArP2: TFloatField;
    QrVSPWEDstQtdAntPeca: TFloatField;
    QrVSPWEDstQtdAntPeso: TFloatField;
    QrVSPWEDstQtdAntArM2: TFloatField;
    QrVSPWEDstQtdAntArP2: TFloatField;
    QrVSPWEDstNotaMPAG: TFloatField;
    QrVSPWEDstNO_PALLET: TWideStringField;
    QrVSPWEDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDstNO_TTW: TWideStringField;
    QrVSPWEDstID_TTW: TLargeintField;
    QrVSPWEDstNO_FORNECE: TWideStringField;
    QrVSPWEDstNO_SerieFch: TWideStringField;
    QrVSPWEDstReqMovEstq: TLargeintField;
    QrVSPWEDstPedItsFin: TLargeintField;
    QrVSPWEDstMarca: TWideStringField;
    QrVSPWEDstStqCenLoc: TLargeintField;
    frxDsVSPWEDst: TfrxDBDataset;
    QrVSPWEBxa: TmySQLQuery;
    QrVSPWEBxaCodigo: TLargeintField;
    QrVSPWEBxaControle: TLargeintField;
    QrVSPWEBxaMovimCod: TLargeintField;
    QrVSPWEBxaMovimNiv: TLargeintField;
    QrVSPWEBxaMovimTwn: TLargeintField;
    QrVSPWEBxaEmpresa: TLargeintField;
    QrVSPWEBxaTerceiro: TLargeintField;
    QrVSPWEBxaCliVenda: TLargeintField;
    QrVSPWEBxaMovimID: TLargeintField;
    QrVSPWEBxaDataHora: TDateTimeField;
    QrVSPWEBxaPallet: TLargeintField;
    QrVSPWEBxaGraGruX: TLargeintField;
    QrVSPWEBxaPecas: TFloatField;
    QrVSPWEBxaPesoKg: TFloatField;
    QrVSPWEBxaAreaM2: TFloatField;
    QrVSPWEBxaAreaP2: TFloatField;
    QrVSPWEBxaValorT: TFloatField;
    QrVSPWEBxaSrcMovID: TLargeintField;
    QrVSPWEBxaSrcNivel1: TLargeintField;
    QrVSPWEBxaSrcNivel2: TLargeintField;
    QrVSPWEBxaSrcGGX: TLargeintField;
    QrVSPWEBxaSdoVrtPeca: TFloatField;
    QrVSPWEBxaSdoVrtPeso: TFloatField;
    QrVSPWEBxaSdoVrtArM2: TFloatField;
    QrVSPWEBxaObserv: TWideStringField;
    QrVSPWEBxaSerieFch: TLargeintField;
    QrVSPWEBxaFicha: TLargeintField;
    QrVSPWEBxaMisturou: TLargeintField;
    QrVSPWEBxaFornecMO: TLargeintField;
    QrVSPWEBxaCustoMOKg: TFloatField;
    QrVSPWEBxaCustoMOM2: TFloatField;
    QrVSPWEBxaCustoMOTot: TFloatField;
    QrVSPWEBxaValorMP: TFloatField;
    QrVSPWEBxaDstMovID: TLargeintField;
    QrVSPWEBxaDstNivel1: TLargeintField;
    QrVSPWEBxaDstNivel2: TLargeintField;
    QrVSPWEBxaDstGGX: TLargeintField;
    QrVSPWEBxaQtdGerPeca: TFloatField;
    QrVSPWEBxaQtdGerPeso: TFloatField;
    QrVSPWEBxaQtdGerArM2: TFloatField;
    QrVSPWEBxaQtdGerArP2: TFloatField;
    QrVSPWEBxaQtdAntPeca: TFloatField;
    QrVSPWEBxaQtdAntPeso: TFloatField;
    QrVSPWEBxaQtdAntArM2: TFloatField;
    QrVSPWEBxaQtdAntArP2: TFloatField;
    QrVSPWEBxaNotaMPAG: TFloatField;
    QrVSPWEBxaNO_PALLET: TWideStringField;
    QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEBxaNO_TTW: TWideStringField;
    QrVSPWEBxaID_TTW: TLargeintField;
    QrVSPWEBxaNO_FORNECE: TWideStringField;
    QrVSPWEBxaNO_SerieFch: TWideStringField;
    QrVSPWEBxaReqMovEstq: TLargeintField;
    frxDsVSPWEBxa: TfrxDBDataset;
    QrVSPWEDstAreaM2WB: TFloatField;
    QrVSPWEDstPercRendim: TFloatField;
    frxWET_CURTI_025_05: TfrxReport;
    frxWET_CURTI_025_02_C: TfrxReport;
    QrVSCurOriIMEI: TmySQLQuery;
    QrVSCurOriIMEICodigo: TLargeintField;
    QrVSCurOriIMEIControle: TLargeintField;
    QrVSCurOriIMEIMovimCod: TLargeintField;
    QrVSCurOriIMEIMovimNiv: TLargeintField;
    QrVSCurOriIMEIMovimTwn: TLargeintField;
    QrVSCurOriIMEIEmpresa: TLargeintField;
    QrVSCurOriIMEITerceiro: TLargeintField;
    QrVSCurOriIMEICliVenda: TLargeintField;
    QrVSCurOriIMEIMovimID: TLargeintField;
    QrVSCurOriIMEIDataHora: TDateTimeField;
    QrVSCurOriIMEIPallet: TLargeintField;
    QrVSCurOriIMEIGraGruX: TLargeintField;
    QrVSCurOriIMEIPecas: TFloatField;
    QrVSCurOriIMEIPesoKg: TFloatField;
    QrVSCurOriIMEIAreaM2: TFloatField;
    QrVSCurOriIMEIAreaP2: TFloatField;
    QrVSCurOriIMEIValorT: TFloatField;
    QrVSCurOriIMEISrcMovID: TLargeintField;
    QrVSCurOriIMEISrcNivel1: TLargeintField;
    QrVSCurOriIMEISrcNivel2: TLargeintField;
    QrVSCurOriIMEISrcGGX: TLargeintField;
    QrVSCurOriIMEISdoVrtPeca: TFloatField;
    QrVSCurOriIMEISdoVrtPeso: TFloatField;
    QrVSCurOriIMEISdoVrtArM2: TFloatField;
    QrVSCurOriIMEIObserv: TWideStringField;
    QrVSCurOriIMEISerieFch: TLargeintField;
    QrVSCurOriIMEIFicha: TLargeintField;
    QrVSCurOriIMEIMisturou: TLargeintField;
    QrVSCurOriIMEIFornecMO: TLargeintField;
    QrVSCurOriIMEICustoMOKg: TFloatField;
    QrVSCurOriIMEICustoMOM2: TFloatField;
    QrVSCurOriIMEICustoMOTot: TFloatField;
    QrVSCurOriIMEIValorMP: TFloatField;
    QrVSCurOriIMEIDstMovID: TLargeintField;
    QrVSCurOriIMEIDstNivel1: TLargeintField;
    QrVSCurOriIMEIDstNivel2: TLargeintField;
    QrVSCurOriIMEIDstGGX: TLargeintField;
    QrVSCurOriIMEIQtdGerPeca: TFloatField;
    QrVSCurOriIMEIQtdGerPeso: TFloatField;
    QrVSCurOriIMEIQtdGerArM2: TFloatField;
    QrVSCurOriIMEIQtdGerArP2: TFloatField;
    QrVSCurOriIMEIQtdAntPeca: TFloatField;
    QrVSCurOriIMEIQtdAntPeso: TFloatField;
    QrVSCurOriIMEIQtdAntArM2: TFloatField;
    QrVSCurOriIMEIQtdAntArP2: TFloatField;
    QrVSCurOriIMEINotaMPAG: TFloatField;
    QrVSCurOriIMEINO_PALLET: TWideStringField;
    QrVSCurOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSCurOriIMEINO_TTW: TWideStringField;
    QrVSCurOriIMEIID_TTW: TLargeintField;
    QrVSCurOriIMEINO_FORNECE: TWideStringField;
    QrVSCurOriIMEINO_SerieFch: TWideStringField;
    QrVSCurOriIMEIReqMovEstq: TLargeintField;
    QrVSCurOriIMEICustoPQ: TFloatField;
    frxDsVSCurOriIMEI: TfrxDBDataset;
    QrVSDesclDst: TmySQLQuery;
    QrVSDesclDstCodigo: TLargeintField;
    QrVSDesclDstControle: TLargeintField;
    QrVSDesclDstMovimCod: TLargeintField;
    QrVSDesclDstMovimNiv: TLargeintField;
    QrVSDesclDstMovimTwn: TLargeintField;
    QrVSDesclDstEmpresa: TLargeintField;
    QrVSDesclDstTerceiro: TLargeintField;
    QrVSDesclDstCliVenda: TLargeintField;
    QrVSDesclDstMovimID: TLargeintField;
    QrVSDesclDstDataHora: TDateTimeField;
    QrVSDesclDstPallet: TLargeintField;
    QrVSDesclDstGraGruX: TLargeintField;
    QrVSDesclDstPecas: TFloatField;
    QrVSDesclDstPesoKg: TFloatField;
    QrVSDesclDstAreaM2: TFloatField;
    QrVSDesclDstAreaP2: TFloatField;
    QrVSDesclDstValorT: TFloatField;
    QrVSDesclDstSrcMovID: TLargeintField;
    QrVSDesclDstSrcNivel1: TLargeintField;
    QrVSDesclDstSrcNivel2: TLargeintField;
    QrVSDesclDstSrcGGX: TLargeintField;
    QrVSDesclDstSdoVrtPeca: TFloatField;
    QrVSDesclDstSdoVrtPeso: TFloatField;
    QrVSDesclDstSdoVrtArM2: TFloatField;
    QrVSDesclDstObserv: TWideStringField;
    QrVSDesclDstSerieFch: TLargeintField;
    QrVSDesclDstFicha: TLargeintField;
    QrVSDesclDstMisturou: TLargeintField;
    QrVSDesclDstFornecMO: TLargeintField;
    QrVSDesclDstCustoMOKg: TFloatField;
    QrVSDesclDstCustoMOM2: TFloatField;
    QrVSDesclDstCustoMOTot: TFloatField;
    QrVSDesclDstValorMP: TFloatField;
    QrVSDesclDstDstMovID: TLargeintField;
    QrVSDesclDstDstNivel1: TLargeintField;
    QrVSDesclDstDstNivel2: TLargeintField;
    QrVSDesclDstDstGGX: TLargeintField;
    QrVSDesclDstQtdGerPeca: TFloatField;
    QrVSDesclDstQtdGerPeso: TFloatField;
    QrVSDesclDstQtdGerArM2: TFloatField;
    QrVSDesclDstQtdGerArP2: TFloatField;
    QrVSDesclDstQtdAntPeca: TFloatField;
    QrVSDesclDstQtdAntPeso: TFloatField;
    QrVSDesclDstQtdAntArM2: TFloatField;
    QrVSDesclDstQtdAntArP2: TFloatField;
    QrVSDesclDstNotaMPAG: TFloatField;
    QrVSDesclDstNO_PALLET: TWideStringField;
    QrVSDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrVSDesclDstNO_TTW: TWideStringField;
    QrVSDesclDstID_TTW: TLargeintField;
    QrVSDesclDstNO_FORNECE: TWideStringField;
    QrVSDesclDstNO_SerieFch: TWideStringField;
    QrVSDesclDstReqMovEstq: TLargeintField;
    QrVSDesclDstPedItsFin: TLargeintField;
    QrVSDesclDstMarca: TWideStringField;
    QrVSDesclDstStqCenLoc: TLargeintField;
    frxDsVSDesclDst: TfrxDBDataset;
    QrOPsCodigo: TIntegerField;
    QrOPsControle: TIntegerField;
    QrOPsMovimCod: TIntegerField;
    QrOPsMovimNiv: TIntegerField;
    QrOPsMovimTwn: TIntegerField;
    QrOPsEmpresa: TIntegerField;
    QrOPsTerceiro: TIntegerField;
    QrOPsCliVenda: TIntegerField;
    QrOPsMovimID: TIntegerField;
    QrOPsLnkIDXtr: TIntegerField;
    QrOPsLnkNivXtr1: TIntegerField;
    QrOPsLnkNivXtr2: TIntegerField;
    QrOPsDataHora: TDateTimeField;
    QrOPsPallet: TIntegerField;
    QrOPsGraGruX: TIntegerField;
    QrOPsPecas: TFloatField;
    QrOPsPesoKg: TFloatField;
    QrOPsAreaM2: TFloatField;
    QrOPsAreaP2: TFloatField;
    QrOPsValorT: TFloatField;
    QrOPsSrcMovID: TIntegerField;
    QrOPsSrcNivel1: TIntegerField;
    QrOPsSrcNivel2: TIntegerField;
    QrOPsSrcGGX: TIntegerField;
    QrOPsSdoVrtPeca: TFloatField;
    QrOPsSdoVrtPeso: TFloatField;
    QrOPsSdoVrtArM2: TFloatField;
    QrOPsObserv: TWideStringField;
    QrOPsSerieFch: TIntegerField;
    QrOPsFicha: TIntegerField;
    QrOPsMisturou: TSmallintField;
    QrOPsFornecMO: TIntegerField;
    QrOPsCustoMOKg: TFloatField;
    QrOPsCustoMOTot: TFloatField;
    QrOPsValorMP: TFloatField;
    QrOPsDstMovID: TIntegerField;
    QrOPsDstNivel1: TIntegerField;
    QrOPsDstNivel2: TIntegerField;
    QrOPsDstGGX: TIntegerField;
    QrOPsQtdGerPeca: TFloatField;
    QrOPsQtdGerPeso: TFloatField;
    QrOPsQtdGerArM2: TFloatField;
    QrOPsQtdGerArP2: TFloatField;
    QrOPsQtdAntPeca: TFloatField;
    QrOPsQtdAntPeso: TFloatField;
    QrOPsQtdAntArM2: TFloatField;
    QrOPsQtdAntArP2: TFloatField;
    QrOPsAptoUso: TSmallintField;
    QrOPsNotaMPAG: TFloatField;
    QrOPsMarca: TWideStringField;
    QrOPsTpCalcAuto: TIntegerField;
    QrOPsZerado: TSmallintField;
    QrOPsEmFluxo: TSmallintField;
    QrOPsNotFluxo: TIntegerField;
    QrOPsFatNotaVNC: TFloatField;
    QrOPsFatNotaVRC: TFloatField;
    QrOPsPedItsLib: TIntegerField;
    QrOPsPedItsFin: TIntegerField;
    QrOPsPedItsVda: TIntegerField;
    QrOPsLk: TIntegerField;
    QrOPsDataCad: TDateField;
    QrOPsDataAlt: TDateField;
    QrOPsUserCad: TIntegerField;
    QrOPsUserAlt: TIntegerField;
    QrOPsAlterWeb: TSmallintField;
    QrOPsAtivo: TSmallintField;
    QrOPsGSPInnNiv2: TIntegerField;
    QrOPsGSPArtNiv2: TIntegerField;
    QrOPsCustoMOM2: TFloatField;
    QrOPsReqMovEstq: TIntegerField;
    QrOPsStqCenLoc: TIntegerField;
    QrOPsItemNFe: TIntegerField;
    QrOPsVSMorCab: TIntegerField;
    QrOPsVSMulFrnCab: TIntegerField;
    QrOPsClientMO: TIntegerField;
    QrOPsCustoPQ: TFloatField;
    QrOPsKgCouPQ: TFloatField;
    QrOPsNFeSer: TSmallintField;
    QrOPsNFeNum: TIntegerField;
    QrOPsVSMulNFeCab: TIntegerField;
    QrOPsGGXRcl: TIntegerField;
    QrOPsJmpMovID: TIntegerField;
    QrOPsJmpNivel1: TIntegerField;
    QrOPsJmpNivel2: TIntegerField;
    QrOPsJmpGGX: TIntegerField;
    QrOPsRmsMovID: TIntegerField;
    QrOPsRmsNivel1: TIntegerField;
    QrOPsRmsNivel2: TIntegerField;
    QrOPsRmsGGX: TIntegerField;
    QrOPsGSPSrcMovID: TIntegerField;
    QrOPsGSPSrcNiv2: TIntegerField;
    QrOPsGSPJmpMovID: TIntegerField;
    QrOPsGSPJmpNiv2: TIntegerField;
    QrOPsDtCorrApo: TDateTimeField;
    QrOPsNO_MovimID: TWideStringField;
    QrOPsNO_MovimNiv: TWideStringField;
    QrOPsNO_CliVenda: TWideStringField;
    QrOPsNO_FornecMO: TWideStringField;
    QrOPsInteiros: TFloatField;
    QrOPsSdoIntei: TFloatField;
    QrOPsNO_FORNECE: TWideStringField;
    QrOPsNO_PRD_TAM_COR: TWideStringField;
    procedure frxWET_CURTI_025_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR4AfterScroll(DataSet: TDataSet);
    procedure QrEstqR4CalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_025_04GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSPWEDstCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FVSImpIMEI: String;
    //
    function  SQL_ListaControles(): String;
  public
    { Public declarations }
    FVSImpImeiKind: TXXImpImeiKind;
    FControles: array of Integer;
    FLPFMO, FNFeRem: String;
    FQryCab: TmySQLQuery;
    //
    procedure ImprimeIMEI();
    procedure ImprimeOPsAbertasUni(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimID: TEstqMovimID);
    procedure ImprimeOPsAbertasMul(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimIDs: array of TEstqMovimID; Ck18Cor_Checked,
              Ck18Especificos_Checked: Boolean; Ed18Especificos_Text: String;
              Ed18Fornecedor_ValueVariant: Integer);
  end;

var
  FmVSImpIMEI: TFmVSImpIMEI;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, CreateVS,
  AppListas;

{$R *.dfm}

{ TForm1 }

procedure TFmVSImpIMEI.FormCreate(Sender: TObject);
begin
  FVSImpImeiKind := viikArtigoGerado;
  FLPFMO  := '';
  FNFeRem := '';
end;

procedure TFmVSImpIMEI.frxWET_CURTI_025_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_LPFMO' then
  begin
    Value := '';
    if Trim(FLPFMO) <> '' then
      Value := 'OP ' + FLPFMO + '  ';
    if Trim(FNFeRem) <> '' then
       Value := Value + 'NF Rem.: ' + FNFeRem;
  end else
  if VarName = 'VARF_SoCincoMostrados' then
    Value := QrVSGerArtOld.RecordCount
  else
  if VarName = 'VARF_ITENS_OLD' then
    Value := QrVSGerArtOld.RecordCount
  else
  if VarName = 'VARF_DV_DO_IMEI' then
    Value := Geral.FFN(
      DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR4Controle.Value), 3)
  else
  if VarName = 'VARF_CODIGO_BARRA' then
    Value := Geral.FFN(
      DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR4Controle.Value), 3) +
      Geral.FFN(QrEstqR4Controle.Value, 9)
  else
  if VarName = 'VARF_OBS_OPE' then
  begin
    Value := '';
    if FQryCab <> nil then
      Value := FQryCab.FieldByName('Nome').AsString;
  end else
end;

procedure TFmVSImpIMEI.frxWET_CURTI_025_04GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_EMPRESA' then
    Value := VAR_LIB_EMPRESAS_NOME
  else
  if VarName ='VARF_LETRA_OX' then
  begin
    case TEStqMovimID(QrOPsMovimID.Value) of
      emidEmProcWE: Value := 'P';
      emidEmOperacao: Value := 'O';
      else Value := '?';
    end;
  end
  else
end;

procedure TFmVSImpIMEI.ImprimeIMEI();
const
  CtrlNxt = 0;
  TemIMEIMrt = 1;
  SQL_Limit = '';
var
  //SQL_Limit, Cab_DtHr: String;
  //Empresa: Integer;
  //SQL_Empresa: String;
  Relatorio: TfrxReport;
begin
  if (Length(FControles) = 0) or ((Length(FControles) = 1) and (FControles[0] = 0)) then
    Geral.MB_Info('Nenhum IME-I foi informado!');
  FVSImpIMEI :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSImpIMEI, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR4, DModG.MyPID_DB, [
  DELETE_FROM + ' ' + FVSImpIMEI + '; ',
  'INSERT INTO  ' + FVSImpIMEI,
  'SELECT vsi.Codigo, vsi.Controle, vsi.MovimCod, vsi.MovimNiv, vsi.MovimID, ',
  'vsi.GraGruX, vsi.Pecas, ',
  'vsi.PesoKg, vsi.AreaM2, vsi.AreaP2, vsi.ValorT, ',
  'vsi.SdoVrtPeca, vsi.SdoVrtPeso, vsi.SdoVrtArM2,    ',
  'ggx.GraGru1, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, vsi.Pallet, vsp.Nome NO_Pallet,   ',
  'vsi.Empresa, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) Cliente,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE, ',
  'vsi.Terceiro Fornece, IF(vsi.Terceiro = 0, "V�rios", ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE, ',
  'IF(vsp.Status IS NULL, -1, vsp.Status), vps.Nome NO_STATUS, DataHora, 0 OrdGGX,   ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  'IF(vsi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vsi.Ficha)) NO_FICHA, ',
  'vsi.ReqMovEstq, vsi.Observ, vsi.PedItsLib, ',
  '1 Ativo   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vsi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsi.GraGruX   ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY   ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vsi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsi.Empresa   ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status   ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vsi.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vsi.Terceiro',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=vsp.CliStat  ',
  'WHERE vsi.Controle IN (' + SQL_ListaControles() + ') ',
  //SQL_Empresa,
  '; ',
  'SELECT er4.* ',
  'FROM ' + FVSImpIMEI + ' er4 ',
  'ORDER BY er4.Controle; ',
  '']);
  if QrEstqR4.RecordCount = 0 then
    Geral.MB_Info('Nenhum IME-I foi encontrado na tabela ativa!');
  //
  case FVSImpImeiKind of
    viikOrdemOperacao: Relatorio := frxWET_CURTI_025_02_A;
    viikOPPreenchida:
    begin
      VS_CRC_PF.ReopenVSOpePrcDst(QrVSPWEDst, QrEstqR4MovimCod.Value, CtrlNxt,
      TemIMEIMrt, TEstqMovimNiv.eminDestWEnd);
      //
      VS_CRC_PF.ReopenVSPWEDesclDst(QrVSDesclDst, QrEstqR4Codigo.Value,
      QrEstqR4Controle.Value, (*Pallet*)0, TemIMEIMrt);

      Relatorio := frxWET_CURTI_025_02_B;
    end;
    viikOPaPreencher: Relatorio := frxWET_CURTI_025_05;
    viikProcCurt:
    begin
      //VS_EFD_ICMS_IPI.
      ProjGroup_PF.ReopenVSOpePrcOriIMEI(QrVSCurOriIMEI, QrEstqR4MovimCod.Value,
      CtrlNxt, TemIMEIMrt, eminSorcCur, SQL_Limit);
      //
      Relatorio := frxWET_CURTI_025_02_C;
    end;
    else // viik1
       Relatorio := frxWET_CURTI_025_01;
  end;
  case FVSImpImeiKind of
    viikOPPreenchida:
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05,
      frxDsVSPWEDst,
      frxDsVSDesclDst
      ]);
    end;
    viikProcCurt:
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05,
      frxDsVSCurOriIMEI
      ]);
    end;
    else
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05
      ]);
    end;
  end;
  MyObjects.frxMostra(Relatorio, 'Romaneio de Artigo de Ribeira',
    'IMEI_' + Geral.FF0(FControles[0]));
end;

procedure TFmVSImpIMEI.ImprimeOPsAbertasMul(RG18Ordem1_ItemIndex,
  RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
  MovimIDs: array of TEstqMovimID; Ck18Cor_Checked, Ck18Especificos_Checked:
  Boolean; Ed18Especificos_Text: String; Ed18Fornecedor_ValueVariant: Integer);
const
  Ordens: array[0..3] of String = ('NO_CliVenda', 'NO_FornecMO',
    'NO_PRD_TAM_COR', 'Codigo');
  Tituls: array[0..3] of String = ('Cliente', 'Fornecedor MO',
    'Artigo', 'Ordem');
var
  I: Integer;
  //Titulo,
  Campo, TitGru, LetraOX, SQL_IDs, SQLNivs, ATT_MovimID, ATT_MovimNiv,
  SQL_IMEIS, SQL_FornecMO: String;
  //MovimNiv: TEstqMovimNiv;
  MovimID: TEstqMovimID;
  Cor1, Cor2: TColor;
begin
  SQL_IDs := '';
  SQLNivs := '';
  SQL_FornecMO := '';
  if Ck18Especificos_Checked and (Trim(Ed18Especificos_Text) <> '') then
    SQL_IMEIS := 'AND vmi.MovimCod IN (' + Ed18Especificos_Text + ')'
  else
    SQL_IMEIS := 'AND vmi.SdoVrtPeca>0  ';
  if Ed18Fornecedor_ValueVariant <> 0 then
    SQL_FornecMO := 'AND vmi.FornecMO=' + Geral.FF0(Ed18Fornecedor_ValueVariant)
  else
    SQL_FornecMO := '';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    MovimID := MovimIDs[I];
    if SQL_IDs <> '' then
    begin
      SQL_IDs := SQL_IDs + ',';
      SQLNivs := SQLNivs + ',';
    end;
    SQL_IDs := SQL_IDs + Geral.FF0(Integer(MovimID));
    //
    case MovimID of
      (*
      emidAjuste: ;
      emidCompra: ;
      emidVenda: ;
      emidReclasWE: ;
      emidBaixa: ;
      emidIndsWE: ;
      emidIndsXX: ;
      emidClassArtXXUni: ;
      emidReclasXXUni: ;
      emidForcado: ;
      emidSemOrigem: ;
      *)
      emidEmOperacao:
      begin
        LetraOX := 'O';
        //Titulo  := 'Ordens de Opera��o Abertas';
        //MovimNiv := TEstqMovimNiv.eminEmOperInn; // 8
        SQLNivs := SQLNivs + Geral.FF0(Integer(TEstqMovimNiv.eminEmOperInn));
      end;
      (*
      emidResiduoReclas: ;
      emidInventario: ;
      emidClassArtXXMul: ;
      emidPreReclasse: ;
      emidEntradaPlC: ;
      emidExtraBxa: ;
      emidSaldoAnterior: ;
      *)
      emidEmProcWE:
      begin
        LetraOX := 'P';
        //Titulo  := 'Ordens de Produ��o Abertas';
        //MovimNiv := TEstqMovimNiv.eminEmWEndInn; // 21
        SQLNivs := SQLNivs + Geral.FF0(Integer(TEstqMovimNiv.eminEmWEndInn));
      end;
      (*
      emidFinished: ;
      emidDevolucao: ;
      emidRetrabalho: ;
      emidGeraSubProd: ;
      emidReclasXXMul: ;
      *)
      else
      begin
        LetraOX := '?';
        //Titulo  := 'Ordens de ??????? Abertas';
      end;
    end;
  end;

  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOPs, Dmod.MyDB, [
  'SELECT CASE vmi.MovimID  ',
  '  WHEN 11 THEN ope.NFeRem ',
  '  WHEN 19 THEN pwe.NFeRem ',
  '  ELSE -1.000 END NFeRem,  ',
  'vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei, ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(fmp.Tipo=0, fmp.RazaoSocial, fmp.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades   cli ON cli.Codigo=vmi.CliVenda ',
  'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
  'LEFT JOIN entidades   fmp ON fmp.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ',
  'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1     cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN vsopecab    ope ON ope.MovimCod=vmi.MovimCod ',
  'LEFT JOIN vspwecab    pwe ON pwe.MovimCod=vmi.MovimCod ',
  'WHERE vmi.MovimID IN (' + SQL_IDs + ') ', //11 ou 19
  'AND vmi.MovimNiv IN (' + SQLNivs + ') ', //8 ou 21
  'AND vmi.Empresa IN (' + VAR_LIB_EMPRESAS + ') ',
  SQL_IMEIS,
  SQL_FornecMO,
  'ORDER BY vmi.MovimID, ' +
  Ordens[RG18Ordem1_ItemIndex] + ', ' +
  Ordens[RG18Ordem2_ItemIndex] + ', ' +
  Ordens[RG18Ordem3_ItemIndex],
  '']);
  //Geral.MB_SQL(Self, QrOPs);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_025_04, [
    DModG.frxDsDono,
    frxDsOPs
  ]);
(*
  if VarName = 'VARF_AGRUP1' then
    Value := RG04Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP2' then
    Value := RG04Agrupa.ItemIndex >= 2
  else
  GF001.Visible := <VARF_AGRUP1>;
  GH001.Condition := <VARF_GRUPO1>;
  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;
  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;

  GH002.Visible := <VARF_AGRUP2>;
  GF002.Visible := <VARF_AGRUP2>;
  GH002.Condition := <VARF_GRUPO2>;
  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;
  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;
*)
  frxWET_CURTI_025_04.Variables['VARF_AGRUP1']   := RG18Agrupa_ItemIndex >= 1;
  frxWET_CURTI_025_04.Variables['VARF_AGRUP2']   := RG18Agrupa_ItemIndex >= 2;

  Campo  := Ordens[RG18Ordem1_ItemIndex];
  TitGru := Tituls[RG18Ordem1_ItemIndex];
  frxWET_CURTI_025_04.Variables['VARF_GRUPO1']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxWET_CURTI_025_04.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxWET_CURTI_025_04.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  Campo  := Ordens[RG18Ordem2_ItemIndex];
  TitGru := Tituls[RG18Ordem2_ItemIndex];
  frxWET_CURTI_025_04.Variables['VARF_GRUPO2']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxWET_CURTI_025_04.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxWET_CURTI_025_04.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  frxWET_CURTI_025_04.Variables['VARF_LETRA_OX'] := QuotedStr(LetraOX);
  //frxWET_CURTI_025_04.Variables['VARF_TITULO']   := QuotedStr(Titulo);

  //
  if Ck18Cor_Checked then
  begin
    Cor1 := $00E6C29B;
    Cor2 := $00B4E0C6;
  end else
  begin
    Cor1 := clWhite;
    Cor2 := clWhite;
  end;
  (frxWET_CURTI_025_04.FindObject('Me_GH1') as TfrxMemoView).Color      := Cor1;
  (frxWET_CURTI_025_04.FindObject('Me_GH2') as TfrxMemoView).Color      := Cor2;
  (frxWET_CURTI_025_04.FindObject('MeGH_02_F01') as TfrxMemoView).Color := Cor1;
  (frxWET_CURTI_025_04.FindObject('MeMD_F01') as TfrxMemoView).Color    := Cor1;
  (frxWET_CURTI_025_04.FindObject('MeMD_F02') as TfrxMemoView).Color    := Cor2;
  (frxWET_CURTI_025_04.FindObject('MeFT_02_F01') as TfrxMemoView).Color := Cor1;
  (frxWET_CURTI_025_04.FindObject('MeFT_02_F02') as TfrxMemoView).Color := Cor2;
  (frxWET_CURTI_025_04.FindObject('MeFT_01') as TfrxMemoView).Color     := Cor1;
  //
  (frxWET_CURTI_025_04.FindObject('MeMD_F01') as TfrxMemoView).Visible  := RG18Agrupa_ItemIndex > 0;
  (frxWET_CURTI_025_04.FindObject('MeMD_F02') as TfrxMemoView).Visible  := RG18Agrupa_ItemIndex > 1;
  //
  MyObjects.frxMostra(frxWET_CURTI_025_04, 'Ordens em Aberto', 'OPsEmAberto');
end;

procedure TFmVSImpIMEI.ImprimeOPsAbertasUni(RG18Ordem1_ItemIndex,
  RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
  MovimID: TEstqMovimID);
const
  Ordens: array[0..3] of String = ('NO_CliVenda', 'NO_FornecMO',
    'NO_PRD_TAM_COR', 'Codigo');
  Tituls: array[0..3] of String = ('Cliente', 'Fornecedor MO',
    'Artigo', 'Ordem');
var
  Campo, TitGru, LetraOX, Titulo, ATT_MovimID, ATT_MovimNiv: String;
  MovimNiv: TEstqMovimNiv;
begin
  case MovimID of
    (*
    emidAjuste: ;
    emidCompra: ;
    emidVenda: ;
    emidReclasWE: ;
    emidBaixa: ;
    emidIndsWE: ;
    emidIndsXX: ;
    emidClassArtXXUni: ;
    emidReclasXXUni: ;
    emidForcado: ;
    emidSemOrigem: ;
    *)
    emidEmOperacao:
    begin
      LetraOX := 'O';
      Titulo  := 'Ordens de Opera��o Abertas';
      MovimNiv := TEstqMovimNiv.eminEmOperInn; // 8
    end;
    (*
    emidResiduoReclas: ;
    emidInventario: ;
    emidClassArtXXMul: ;
    emidPreReclasse: ;
    emidEntradaPlC: ;
    emidExtraBxa: ;
    emidSaldoAnterior: ;
    *)
    emidEmProcWE:
    begin
      LetraOX := 'P';
      Titulo  := 'Ordens de Produ��o Abertas';
      MovimNiv := TEstqMovimNiv.eminEmWEndInn; // 21
    end;
    (*
    emidFinished: ;
    emidDevolucao: ;
    emidRetrabalho: ;
    emidGeraSubProd: ;
    emidReclasXXMul: ;
    *)
    else
    begin
      LetraOX := '?';
      Titulo  := 'Ordens de ??????? Abertas';
    end;
  end;
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
(*
  'SELECT vmi.*,   ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda,  ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FornecMO,  ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros,  ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei,  ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR  ',
  '  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN entidades  cli ON cli.Codigo=vmi.CliVenda  ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.FornecMO  ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrOPs, Dmod.MyDB, [
  'SELECT vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei, ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(fmp.Tipo=0, fmp.RazaoSocial, fmp.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades   cli ON cli.Codigo=vmi.CliVenda ',
  'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
  'LEFT JOIN entidades   fmp ON fmp.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ',
  'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(MovimID)), //11 ou 19
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //8 ou 21
  'AND vmi.Empresa IN (' + VAR_LIB_EMPRESAS + ')',
  'AND vmi.SdoVrtPeca>0  ',
  'ORDER BY ' +
  Ordens[RG18Ordem1_ItemIndex] + ', ' +
  Ordens[RG18Ordem2_ItemIndex] + ', ' +
  Ordens[RG18Ordem3_ItemIndex],
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_025_03, [
    DModG.frxDsDono,
    frxDsOPs
  ]);
(*
  if VarName = 'VARF_AGRUP1' then
    Value := RG04Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP2' then
    Value := RG04Agrupa.ItemIndex >= 2
  else
  GF001.Visible := <VARF_AGRUP1>;
  GH001.Condition := <VARF_GRUPO1>;
  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;
  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;

  GH002.Visible := <VARF_AGRUP2>;
  GF002.Visible := <VARF_AGRUP2>;
  GH002.Condition := <VARF_GRUPO2>;
  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;
  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;
*)
  frxWET_CURTI_025_03.Variables['VARF_AGRUP1']   := RG18Agrupa_ItemIndex >= 1;
  frxWET_CURTI_025_03.Variables['VARF_AGRUP2']   := RG18Agrupa_ItemIndex >= 2;

  Campo  := Ordens[RG18Ordem1_ItemIndex];
  TitGru := Tituls[RG18Ordem1_ItemIndex];
  frxWET_CURTI_025_03.Variables['VARF_GRUPO1']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxWET_CURTI_025_03.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxWET_CURTI_025_03.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  Campo  := Ordens[RG18Ordem2_ItemIndex];
  TitGru := Tituls[RG18Ordem2_ItemIndex];
  frxWET_CURTI_025_03.Variables['VARF_GRUPO2']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxWET_CURTI_025_03.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxWET_CURTI_025_03.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  frxWET_CURTI_025_03.Variables['VARF_LETRA_OX'] := QuotedStr(LetraOX);
  frxWET_CURTI_025_03.Variables['VARF_TITULO']   := QuotedStr(Titulo);

  MyObjects.frxMostra(frxWET_CURTI_025_03, Titulo);
end;

procedure TFmVSImpIMEI.QrEstqR4AfterScroll(DataSet: TDataSet);
const
  TemIMEIMrt = 0;  // Ver o que fazer !
begin
  VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArtOld, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, '', eminSorcCurtiXX);
  //
  VS_CRC_PF.ReopenVSGerArtSrc(QrOld01, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 0,1 ', eminSorcCurtiXX);
  VS_CRC_PF.ReopenVSGerArtSrc(QrOld02, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 1,1 ', eminSorcCurtiXX);
  VS_CRC_PF.ReopenVSGerArtSrc(QrOld03, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 2,1 ', eminSorcCurtiXX);
  VS_CRC_PF.ReopenVSGerArtSrc(QrOld04, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 3,1 ', eminSorcCurtiXX);
  VS_CRC_PF.ReopenVSGerArtSrc(QrOld05, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 4,1 ', eminSorcCurtiXX);
end;

procedure TFmVSImpIMEI.QrEstqR4CalcFields(DataSet: TDataSet);
var
  Tipo: Integer;
  Texto: String;
begin
  Tipo := -1;
  Texto := '?????';
  QrEstqR4NO_ACAO_COURO.Value := sEstqMovimID[QrEstqR4MovimID.Value];
  QrEstqR4NO_TIPO_COURO.Value := sEstqMovimIDLong[QrEstqR4MovimID.Value];
  Tipo := iEstqMovimID[QrEstqR4MovimID.Value];
  case Tipo of
    1:
    begin
      QrEstqR4TERCEIRO.Value := QrEstqR4Fornece.Value;
      QrEstqR4NO_FRNCALC.Value := QrEstqR4NO_FORNECE.Value;
      QrEstqR4TIPO_TERCEIRO.Value := 'Proced�ncia';
    end;
    2:
    begin
      QrEstqR4TERCEIRO.Value := QrEstqR4Cliente.Value;
      QrEstqR4NO_FRNCALC.Value := QrEstqR4NO_CLIENTE.Value;
      QrEstqR4TIPO_TERCEIRO.Value := 'Cliente';
    end;
    else
    begin
      QrEstqR4TERCEIRO.Value := 0;
      QrEstqR4NO_FRNCALC.Value := '';
      QrEstqR4TIPO_TERCEIRO.Value := '';
    end;
  end;
  QrEstqR4SdoVrtArP2.Value := Geral.ConverteArea(QrEstqR4SdoVrtArM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmVSImpIMEI.QrVSPWEDstCalcFields(DataSet: TDataSet);
const
  Controle = 0;
  SQL_Limit = '';
  TemIMEIMrt = 1;
begin
  VS_CRC_PF.ReopenVSOpePrcBxa(QrVSPWEBxa, QrEstqR4MovimCod.Value,
  QrVSPWEDstMovimTwn.Value, Controle, TemIMEIMrt, eminEmWEndBxa, SQL_Limit);
  // Area WB
  QrVSPWEDstAreaM2WB.Value := QrVSPWEBxaAreaM2.Value;
  // Rendimento FI sobre WB
  if QrVSPWEDstAreaM2WB.Value = 0 then
    QrVSPWEDstPercRendim.Value := 0
  else
    QrVSPWEDstPercRendim.Value := (QrVSPWEDstAreaM2.Value +
    QrVSPWEDstAreaM2WB.Value) / -QrVSPWEDstAreaM2WB.Value * 100;
end;

function TFmVSImpIMEI.SQL_ListaControles(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FControles);
  for I := Low(FControles) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FControles[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

end.
