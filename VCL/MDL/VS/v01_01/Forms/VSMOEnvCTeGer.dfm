object FmVSMOEnvCTeGer: TFmVSMOEnvCTeGer
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-228 :: Gerenciamento de Fretes VS'
  ClientHeight = 629
  ClientWidth = 816
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 768
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 720
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 352
        Height = 32
        Caption = 'Gerenciamento de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 352
        Height = 32
        Caption = 'Gerenciamento de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 352
        Height = 32
        Caption = 'Gerenciamento de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 816
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 816
        Height = 65
        Align = alTop
        Caption = ' Pesquisa: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 812
          Height = 48
          Align = alClient
          TabOrder = 0
          object CkLimitar: TCheckBox
            Left = 8
            Top = 4
            Width = 85
            Height = 17
            Caption = 'Limitar (0, -1)'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object Ed1: TdmkEdit
            Left = 8
            Top = 22
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object Ed2: TdmkEdit
            Left = 68
            Top = 22
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1000
            ValWarn = False
          end
          object CkDESC: TCheckBox
            Left = 132
            Top = 24
            Width = 133
            Height = 17
            Caption = 'Ordem descendente.'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 65
        Width = 816
        Height = 243
        Align = alClient
        DataSource = DsCTes
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Terceiro'
            Title.Caption = 'C'#243'd. Transp.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Transp'
            Title.Caption = 'Nome do transportador'
            Width = 292
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SerCT'
            Title.Caption = 'S'#233'rie'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nCT'
            Title.Caption = 'N'#250'mero CT'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'Area m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesTrKg'
            Title.Caption = 'Kg transporte'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 308
        Width = 816
        Height = 159
        Align = alBottom
        DataSource = DsCTeIts
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataCad'
            Title.Caption = 'Data Cad.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso Kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'Area m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TipoFrete'
            Title.Caption = 'Tipo de Frete'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesTrKg'
            Title.Caption = 'Kg transporte'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CusTrKg'
            Title.Caption = '$ / kg frete'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Valor total'
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 816
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 812
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 816
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 670
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 668
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtReabre: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtReabreClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrCTes: TmySQLQuery
   
    BeforeClose = QrCTesBeforeClose
    AfterScroll = QrCTesAfterScroll
    SQL.Strings = (
      'SELECT ecg.Empresa, ecg.Terceiro,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,'
      'ecg.SerCT, ecg.nCT,'
      'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,'
      'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,'
      'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,'
      'SUM(ecg.ValorT) ValorT'
      'FROM _vs_mo_env_cte_ger ecg'
      'LEFT JOIN bluederm_meza.entidades tra ON tra.Codigo=ecg.Terceiro'
      'GROUP BY Empresa, Terceiro, SerCT, nCT'
      'ORDER BY ecg.DataCad DESC')
    Left = 316
    Top = 296
    object QrCTesEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTesTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrCTesSerCT: TIntegerField
      FieldName = 'SerCT'
    end
    object QrCTesnCT: TIntegerField
      FieldName = 'nCT'
    end
    object QrCTesNO_Transp: TWideStringField
      FieldName = 'NO_Transp'
      Size = 100
    end
    object QrCTesPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCTesPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTesAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTesAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTesPesTrKg: TFloatField
      FieldName = 'PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTesCusTrKg: TFloatField
      FieldName = 'CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCTesValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCTes: TDataSource
    DataSet = QrCTes
    Left = 316
    Top = 344
  end
  object QrCTeIts: TmySQLQuery
   
    SQL.Strings = (
      'SELECT *'
      'FROM _vs_mo_env_cte_ger'
      'WHERE Empresa=-11'
      'AND Terceiro=9'
      'AND SerCT=0'
      'AND nCT=1258')
    Left = 400
    Top = 296
    object QrCTeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeItsTipoFrete: TWideStringField
      FieldName = 'TipoFrete'
      Required = True
      Size = 10
    end
    object QrCTeItsTabela: TWideStringField
      FieldName = 'Tabela'
      Required = True
      Size = 15
    end
    object QrCTeItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrCTeItsSerCT: TIntegerField
      FieldName = 'SerCT'
    end
    object QrCTeItsnCT: TIntegerField
      FieldName = 'nCT'
    end
    object QrCTeItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCTeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeItsPesTrKg: TFloatField
      FieldName = 'PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeItsCusTrKg: TFloatField
      FieldName = 'CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCTeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCTeIts: TDataSource
    DataSet = QrCTeIts
    Left = 400
    Top = 344
  end
end
