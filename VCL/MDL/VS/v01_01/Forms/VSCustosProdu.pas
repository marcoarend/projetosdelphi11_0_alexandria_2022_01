unit VSCustosProdu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, CreateVS, frxClass, frxDBSet;

type
  TFmVSCustosProdu = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    DsGerados: TDataSource;
    PB1: TProgressBar;
    frxDsGerados: TfrxDBDataset;
    GroupBox3: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    frxWET_CURTI_254_1: TfrxReport;
    CkSdoPositiv: TCheckBox;
    QrGraGruXGrandeza: TSmallintField;
    QrGraGruY: TMySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    QrGraGruYLk: TIntegerField;
    QrGraGruYDataCad: TDateField;
    QrGraGruYDataAlt: TDateField;
    QrGraGruYUserCad: TIntegerField;
    QrGraGruYUserAlt: TIntegerField;
    QrGraGruYAlterWeb: TSmallintField;
    QrGraGruYAtivo: TSmallintField;
    DsGraGruY: TDataSource;
    PCTipoRel: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Panel6: TPanel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdGraGruY: TdmkEditCB;
    CBGraGruY: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrVSInnIts: TMySQLQuery;
    DsVSInnIts: TDataSource;
    frxDsVSInnIts: TfrxDBDataset;
    QrVSInnItsCodigo: TIntegerField;
    QrVSInnItsControle: TIntegerField;
    QrVSInnItsMovimCod: TIntegerField;
    QrVSInnItsDataHora: TDateTimeField;
    QrVSInnItsGraGruX: TIntegerField;
    QrVSInnItsPecas: TFloatField;
    QrVSInnItsPesoKg: TFloatField;
    QrVSInnItsValorT: TFloatField;
    QrVSInnItsCustoKg: TFloatField;
    QrVSInnItsSdoVrtPeca: TFloatField;
    QrVSInnItsSdoVrtPeso: TFloatField;
    QrVSInnItsObserv: TWideStringField;
    QrVSInnItsSerieFch: TIntegerField;
    QrVSInnItsFicha: TIntegerField;
    QrVSInnItsValorMP: TFloatField;
    QrVSInnItsQtdGerArM2: TFloatField;
    QrVSInnItsNotaMPAG: TFloatField;
    QrVSInnItsMarca: TWideStringField;
    QrVSInnItsNFeNum: TIntegerField;
    QrVSInnItsCusFrtAvuls: TFloatField;
    QrVSInnItsCustoComiss: TFloatField;
    QrVSInnItsCusKgComiss: TFloatField;
    QrVSInnItsCredValrImposto: TFloatField;
    QrVSInnItsCredPereImposto: TFloatField;
    QrVSInnItsNO_PRD_TAM_COR: TWideStringField;
    QrCorda: TMySQLQuery;
    QrGerados: TMySQLQuery;
    QrGerM2: TMySQLQuery;
    QrGerKg: TMySQLQuery;
    QrGerM2SrcNivel2: TIntegerField;
    QrGerM2Pecas: TFloatField;
    QrGerM2PesoKg: TFloatField;
    QrGerM2QtdGerArM2: TFloatField;
    QrGerM2m2Pc: TFloatField;
    QrGerM2kgm2: TFloatField;
    QrGerKgSrcNivel2: TIntegerField;
    QrGerKgPecas: TFloatField;
    QrGerKgPesoKg: TFloatField;
    QrGerKgQtdGerPeso: TFloatField;
    QrGerKgkgPc: TFloatField;
    QrVSInnItsArePecas: TFloatField;
    QrVSInnItsArePesoKg: TFloatField;
    QrVSInnItsm2Pc: TFloatField;
    QrVSInnItskgm2: TFloatField;
    QrVSInnItspesPecas: TFloatField;
    QrVSInnItsPesQtdGerPeso: TFloatField;
    QrVSInnItskgPc: TFloatField;
    QrGeradosCodigo: TIntegerField;
    QrGeradosControle: TIntegerField;
    QrGeradosMovimCod: TIntegerField;
    QrGeradosDataHora: TDateTimeField;
    QrGeradosGGXAnt: TIntegerField;
    QrGeradosPecas: TFloatField;
    QrGeradosValorTAnt: TFloatField;
    QrGeradosAreaM2Atu: TFloatField;
    QrGeradosCustoM2Ant: TFloatField;
    QrGeradosPesoKgAtu: TFloatField;
    QrGeradosCustoKgAnt: TFloatField;
    QrGeradosPesoKgAnt: TFloatField;
    QrGeradosKgPecaAnt: TFloatField;
    QrGeradosSdoVrtPeca: TFloatField;
    QrGeradosSdoVrtArM2: TFloatField;
    QrGeradosCustoMOKg: TFloatField;
    QrGeradosCustoMOTot: TFloatField;
    QrGeradosInteirosSVPc: TFloatField;
    QrGeradosValorTAtu: TFloatField;
    QrGeradosCustoM2Atu: TFloatField;
    QrGeradosCustoKgAtu: TFloatField;
    QrGeradosSerieFch: TIntegerField;
    QrGeradosFicha: TIntegerField;
    QrGeradosValorMPAnt: TFloatField;
    QrGeradosNotaMPAG: TFloatField;
    QrGeradosMarca: TWideStringField;
    QrGeradosNFeNum: TIntegerField;
    QrGeradosInteirosPeca: TFloatField;
    QrGeradosNO_PRD_TAM_COR_Ant: TWideStringField;
    QrGeradosNO_PRD_TAM_COR_Atu: TWideStringField;
    QrGeradosObserv: TWideStringField;
    QrGeradosDstGGX: TIntegerField;
    QrGeradosCredPereImposto: TFloatField;
    QrGeradosCredValrImposto: TFloatField;
    QrGeradosCustoMOAtu: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_248_A_1GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FEmpresa, FGraGruX, FGraGruY: Integer;
    //
  public
    { Public declarations }
  end;

  var
  FmVSCustosProdu: TFmVSCustosProdu;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UnGrl_Consts,
  UnDmkProcFunc, UMySQLModule;

{$R *.DFM}

procedure TFmVSCustosProdu.BtOKClick(Sender: TObject);
var
  SQL, SQL_GraGruY, SQL_GraGruX, SQL_Empresa, SQL_Periodo, Corda1, Corda2,
  SQL_SdoPositiv: String;
  Report: TfrxReport;
  GGY_OK: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
  FEmpresa     := EdEmpresa.ValueVariant;
  FGraGruX     := EdGraGruX.ValueVariant;
  FGraGruY     := EdGraGruY.ValueVariant;
  //
  case PCTipoRel.ActivePageIndex of
    // GraGruX
    0: if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
    // GraGruY
    1: if MyObjects.FIC(FGraGruY = 0, EdGraGruY, 'Informe o grupo de estoque!') then Exit;
  end;
  SQL_GraGruY := '';
  SQL_GraGruX := '';
  case PCTipoRel.ActivePageIndex of
    0: SQL_GraGruY := 'AND vmi.GraGruX=' + Geral.FF0(FGraGruX);
    1: SQL_GraGruY := 'AND ggx.GraGruY=' + Geral.FF0(FGraGruY);
  end;
  //
  SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(FEmpresa);
  SQL_Periodo :=  dmkPF.SQL_Periodo('WHERE vmi.DataHora ',
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  if CkSdoPositiv.Checked then
    SQL_SdoPositiv := Geral.ATS([
    'IF(vmi.SdoVrtPeca > 0.0, vmi.SdoVrtPeca, 0.0) SdoVrtPeca,',
    'IF(vmi.SdoVrtPeca > 0.000, vmi.SdoVrtPeso, 0.000) SdoVrtPeso,'
    ])
  else
    SQL_SdoPositiv := 'SdoVrtPeca, SdoVrtPeso,';
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - IME-Is');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT vmi.Controle ',
  'FROM vsmovits vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  SQL_Periodo,
  SQL_GraGruY,
  SQL_GraGruX,
  'AND vmi.Pecas > 0 ',
  SQL_Empresa,
  'AND NOT (vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + '))', //    = '9,17,28,41';
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle ',
  '']);
  Corda1 := MyObjects.CordaDeQuery(QrCorda, 'Controle');
  if Corda1 = EmptyStr then
  begin
    Geral.MB_Aviso('N�o foram encontrados artigos de origem na pesquisa!');
    Exit;
  end;
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Gera��o de �rea');
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _vs_custo_produ_ger_area_;',
  'CREATE TABLE _vs_custo_produ_ger_area_',
  'SELECT SrcNivel2, -SUM(Pecas) Pecas, ',
  '-SUM(PesoKg) PesoKg, -SUM(QtdGerArM2) QtdGerArM2,',
  'SUM(QtdGerArM2) / SUM(Pecas) m2Pc, ',
  'SUM(PesoKg) / SUM(QtdGerArM2) kgm2',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE QtdGerArM2 <> 0',
  'AND SrcNivel2 IN (' + Corda1 + ')',
  'GROUP BY SrcNivel2;',
  //'SELECT * FROM _vs_custo_produ_ger_area_;',
  '']));
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Gera��o de peso');
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _vs_custo_produ_ger_peso_;',
  'CREATE TABLE _vs_custo_produ_ger_peso_',
  'SELECT SrcNivel2, -SUM(Pecas) Pecas,  ',
  '-SUM(PesoKg) PesoKg, -SUM(QtdGerPeso) QtdGerPeso, ',
  'SUM(QtdGerPeso) / SUM(Pecas) kgPc',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE QtdGerArM2 = 0 ',
  'AND QtdGerPeso <> 0',
  'AND SrcNivel2 IN (' + Corda1 + ')',
  'GROUP BY SrcNivel2;',
  //'SELECT * FROM _vs_custo_produ_ger_peso_;',
  '']));
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Entrada');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnIts, DModG.MyPID_DB, [
  'SELECT vmi.Codigo, vmi.Controle, vmi.MovimCod,  ',
  'vmi.DataHora, vmi.GraGruX, vmi.Pecas, vmi.PesoKg,  ',
  'vmi.ValorT, vmi.ValorT / vmi.PesoKg CustoKg,  ',
  SQL_SdoPositiv,
  'vmi.Observ, vmi.SerieFch,   ',
  'vmi.Ficha, vmi.ValorMP, vmi.QtdGerArM2, vmi.NotaMPAG,  ',
  'vmi.Marca, vmi.NFeNum, vmi.CusFrtAvuls, vmi.CustoComiss,  ',
  'vmi.CusKgComiss, CredValrImposto, CredPereImposto, ',
  'CONCAT(gg1.Nome,   ',
  '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))   ',
  '    NO_PRD_TAM_COR,  ',
  'are.Pecas ArePecas, are.PesoKg ArePesoKg, are.m2Pc, are.kgm2, ',
  'pes.Pecas pesPecas, pes.QtdGerPeso PesQtdGerPeso, pes.kgPc ',
  'FROM ' + TMeuDB + '.vsmovits vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN _vs_custo_produ_ger_area_ are ON are.SrcNivel2=vmi.Controle   ',
  'LEFT JOIN _vs_custo_produ_ger_peso_ pes ON pes.SrcNivel2=vmi.Controle   ',
  'WHERE vmi.Controle IN (' + Corda1 + ')',
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle ',
  '']);
  //Geral.MB_Teste(QrVSInnIts.SQL.Text);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Baixa');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod ',
  'FROM vsmovits vmi ',
  'WHERE vmi.SrcNivel2 IN ( ' + Corda1 + ') ',
  '']);
  Corda2 := MyObjects.CordaDeQuery(QrCorda, 'MovimCod', '-999999999');
  if Corda2 = '-999999999' then
  begin
    Geral.MB_Aviso('N�o foram encontrados artigos de destino na pesquisa!');
    //Exit;
  end;
  //Geral.MB_Teste(QrCorda.SQL.Text);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Totais gerados');
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _vs_custo_produ_06_13_; ',
  'CREATE TABLE _vs_custo_produ_06_13_ ',
  'SELECT vmi.Codigo, vmi.Controle, vmi.MovimCod, ',
  'vmi.DataHora, vmi.GraGruX, vmi.Pecas, ',
  'vmi.ValorT / vmi.AreaM2 CustoM2,  ',
  'vmi.ValorT / vmi.PesoKg CustoKg,  ',
  'vmi.QtdAntPeso, vmi.QtdAntPeso / vmi.Pecas KgPeca,  ',
  'vmi.SdoVrtPeca, vmi.SdoVrtArM2,  ',
  'vmi.ValorMP, vmi.CusFrtAvuls, vmi.CustoComiss,  ',
  'vmi.CusKgComiss, vmi.CredValrImposto, vmi.CredPereImposto,  ',
  'vmi.CustoMOKg, vmi.CustoMOTot,  ',
  'CONCAT(gg1.Nome,  ',
  '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
  '    NO_PRD_TAM_COR  ',
  'FROM ' + TMeuDB + '.vsmovits vmi  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimCod IN (' + Corda2 + ') ',
  'AND vmi.MovimNiv=13 ',
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle   ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //Geral.MB_Teste(SQL);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados de gera��o de artigo');
  UnDmkDAC_PF.AbreMySQLQuery0(QrGerados, DModG.MyPID_DB, [
  'SELECT vmi.DstGGX, ',
  'vmi.Codigo, vmi.Controle, vmi.MovimCod, ',
  'vmi.DataHora, vmi.GraGruX  GGXAnt, -vmi.Pecas Pecas, -vmi.ValorT ValorTAnt,  ',
  '-vmi.QtdGerArM2 AreaM2Atu, vmi.ValorT / vmi.QtdGerArM2 CustoM2Ant,  ',
  '-vmi.QtdGerPeso PesoKgAtu, vmi.ValorT / vmi.QtdGerPeso CustoKgAnt, ',
  '-vmi.PesoKg PesoKgAnt,  ',
  'vmi.PesoKg / vmi.Pecas KgPecaAnt,  ',
  '-(vmi.Pecas/g13.Pecas * g13.SdoVrtPeca) SdoVrtPeca, ',
  '-(vmi.Pecas/g13.Pecas * g13.SdoVrtArM2) SdoVrtArM2, ',
  'g13.CustoMOKg, g13.CustoMOTot, g13.CredPereImposto, ',
  '-vmi.PesoKg / g13.QtdAntPeso * g13.CustoMOTot CustoMOAtu, ',
  '-vmi.PesoKg / g13.QtdAntPeso * g13.CredValrImposto CredValrImposto, ',
  '-(vmi.Pecas/g13.Pecas * g13.SdoVrtPeca) * ',
  '  IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosSVPc,  ',
  '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg) - (-vmi.PesoKg / g13.QtdAntPeso * g13.CredValrImposto)) ValorTAtu,  ',
  '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg) - (-vmi.PesoKg / g13.QtdAntPeso * g13.CredValrImposto)) / -vmi.QtdGerArM2 CustoM2Atu,  ',
  '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg) - (-vmi.PesoKg / g13.QtdAntPeso * g13.CredValrImposto)) / -vmi.QtdGerPeso CustoKgAtu, ',
  'vmi.SerieFch,  ',
  'vmi.Ficha, -vmi.ValorMP ValorMPAnt, vmi.NotaMPAG,  ',
  'vmi.Marca, vmi.NFeNum,   ',
  '-vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosPeca,  ',
  'CONCAT(gg1.Nome,  ',
  '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
  '    NO_PRD_TAM_COR_Ant, ',
  'g13.NO_PRD_TAM_COR NO_PRD_TAM_COR_Atu, vmi.Observ  ',
  'FROM ' + TMeuDB + '.vsmovits vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'LEFT JOIN _vs_custo_produ_06_13_ g13 ON g13.MovimCod=vmi.MovimCod',
  'WHERE vmi.SrcNivel2 IN (' + Corda1 + ') ',
  'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminBaixCurtiXX)),
  //'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
  'ORDER BY vmi.DstGGX, GGXAnt, vmi.DataHora, vmi.Controle ',
  '']);
  //Geral.MB_Teste(QrGerados.SQL.Text);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando Relat�rio');
  MyObjects.frxDefineDatasets(frxWET_CURTI_254_1, [
  DModG.frxDsDono,
  frxDsVSInnIts,
  frxDsGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_254_1, 'Custos de Produ��o');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  finally
    Screen.Cursor := crDefault;
  end;
end;

(*
procedure TFmVSCustosProdu.BtOKClick(Sender: TObject);
var
  SQL_GraGruY, SQL_GraGruX, SQL_Empresa, SQL_Periodo, Corda1, Corda2,
  SQL_SdoPositiv: String;
  Report: TfrxReport;
  GGY_OK: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
  FEmpresa     := EdEmpresa.ValueVariant;
  FGraGruX     := EdGraGruX.ValueVariant;
  FGraGruY     := EdGraGruY.ValueVariant;
  //
  case PCTipoRel.ActivePageIndex of
    // GraGruX
    0: if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
    // GraGruY
    1: if MyObjects.FIC(FGraGruY = 0, EdGraGruY, 'Informe o grupo de estoque!') then Exit;
  end;
  SQL_GraGruY := '';
  SQL_GraGruX := '';
  case PCTipoRel.ActivePageIndex of
    0: SQL_GraGruY := 'AND vmi.GraGruX=' + Geral.FF0(FGraGruX);
    1: SQL_GraGruY := 'AND ggx.GraGruY=' + Geral.FF0(FGraGruY);
  end;
  //
  SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(FEmpresa);
  SQL_Periodo :=  dmkPF.SQL_Periodo('WHERE vmi.DataHora ',
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  if CkSdoPositiv.Checked then
    SQL_SdoPositiv := Geral.ATS([
    'IF(vmi.SdoVrtPeca > 0.0, vmi.SdoVrtPeca, 0.0) SdoVrtPeca,',
    'IF(vmi.SdoVrtPeca > 0.000, vmi.SdoVrtPeso, 0.000) SdoVrtPeso,'
    ])
  else
    SQL_SdoPositiv := 'SdoVrtPeca, SdoVrtPeso,';
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - IME-Is');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT vmi.Controle ',
  'FROM vsmovits vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  SQL_Periodo,
  SQL_GraGruY,
  SQL_GraGruX,
  'AND vmi.Pecas > 0 ',
  SQL_Empresa,
  'AND NOT (vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + '))', //    = '9,17,28,41';
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle ',
  '']);
  Corda1 := MyObjects.CordaDeQuery(QrCorda, 'Controle');
  if Corda1 = EmptyStr then
  begin
    Geral.MB_Aviso('N�o foram encontrados artigos de origem na pesquisa!');
    Exit;
  end;
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Gera��o de �rea');
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _vs_custo_produ_ger_area_;',
  'CREATE TABLE _vs_custo_produ_ger_area_',
  'SELECT SrcNivel2, -SUM(Pecas) Pecas, ',
  '-SUM(PesoKg) PesoKg, -SUM(QtdGerArM2) QtdGerArM2,',
  'SUM(QtdGerArM2) / SUM(Pecas) m2Pc, ',
  'SUM(PesoKg) / SUM(QtdGerArM2) kgm2',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE QtdGerArM2 <> 0',
  'AND SrcNivel2 IN (' + Corda1 + ')',
  'GROUP BY SrcNivel2;',
  //'SELECT * FROM _vs_custo_produ_ger_area_;',
  '']));
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Gera��o de peso');
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DROP TABLE IF EXISTS _vs_custo_produ_ger_peso_;',
  'CREATE TABLE _vs_custo_produ_ger_peso_',
  'SELECT SrcNivel2, -SUM(Pecas) Pecas,  ',
  '-SUM(PesoKg) PesoKg, -SUM(QtdGerPeso) QtdGerPeso, ',
  'SUM(QtdGerPeso) / SUM(Pecas) kgPc',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE QtdGerArM2 = 0 ',
  'AND QtdGerPeso <> 0',
  'AND SrcNivel2 IN (' + Corda1 + ')',
  'GROUP BY SrcNivel2;',
  //'SELECT * FROM _vs_custo_produ_ger_peso_;',
  '']));
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Entrada');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnIts, DModG.MyPID_DB, [
  'SELECT vmi.Codigo, vmi.Controle, vmi.MovimCod,  ',
  'vmi.DataHora, vmi.GraGruX, vmi.Pecas, vmi.PesoKg,  ',
  'vmi.ValorT, vmi.ValorT / vmi.PesoKg CustoKg,  ',
  SQL_SdoPositiv,
  'vmi.Observ, vmi.SerieFch,   ',
  'vmi.Ficha, vmi.ValorMP, vmi.QtdGerArM2, vmi.NotaMPAG,  ',
  'vmi.Marca, vmi.NFeNum, vmi.CusFrtAvuls, vmi.CustoComiss,  ',
  'vmi.CusKgComiss, CredValrImposto, CredPereImposto, ',
  'CONCAT(gg1.Nome,   ',
  '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))   ',
  '    NO_PRD_TAM_COR,  ',
  'are.Pecas ArePecas, are.PesoKg ArePesoKg, are.m2Pc, are.kgm2, ',
  'pes.Pecas pesPecas, pes.QtdGerPeso PesQtdGerPeso, pes.kgPc ',
  'FROM ' + TMeuDB + '.vsmovits vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN _vs_custo_produ_ger_area_ are ON are.SrcNivel2=vmi.Controle   ',
  'LEFT JOIN _vs_custo_produ_ger_peso_ pes ON pes.SrcNivel2=vmi.Controle   ',
  'WHERE vmi.Controle IN (' + Corda1 + ')',
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle ',
  '']);
  //Geral.MB_Teste(QrVSInnIts.SQL.Text);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados do In Natura - Baixa');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod ',
  'FROM vsmovits vmi ',
  'WHERE vmi.SrcNivel2 IN ( ' + Corda1 + ') ',
  '']);
  //Geral.MB_Teste(QrCorda.SQL.Text);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados de gera��o de artigo');
  Corda2 := MyObjects.CordaDeQuery(QrCorda, 'MovimCod', '-999999999');
  if Corda2 = '-999999999' then
  begin
    Geral.MB_Aviso('N�o foram encontrados artigos de destino na pesquisa!');
    //Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGerados, Dmod.MyDB, [
  'SELECT vmi.Codigo, vmi.Controle, vmi.MovimCod, ',
  'vmi.DataHora, vmi.GraGruX, vmi.Pecas, vmi.AreaM2, ',
  'vmi.ValorT, vmi.ValorT / vmi.AreaM2 CustoM2, ',
  'vmi.QtdAntPeso, vmi.QtdAntPeso / vmi.Pecas KgPeca, ',

  'vmi.SdoVrtPeca, vmi.SdoVrtArM2, vmi.Observ, vmi.SerieFch, ',
  'vmi.Ficha, vmi.ValorMP, vmi.QtdGerArM2, vmi.NotaMPAG, ',
  'vmi.Marca, vmi.NFeNum, vmi.CusFrtAvuls, vmi.CustoComiss, ',
  'vmi.CusKgComiss, vmi.CredValrImposto, vmi.CredPereImposto, ',
  'vmi.CustoMOKg, vmi.CustoMOTot, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosPeca, ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosSVPc, ',
  'CONCAT(gg1.Nome, ',
  '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  '    NO_PRD_TAM_COR ',
  'FROM vsmovits vmi ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'WHERE MovimCod IN ( ' + Corda2 + ') ',
  'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),
  'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle ',
  '']);
  //Geral.MB_Teste(QrGerados.SQL.Text);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando Relat�rio');
  MyObjects.frxDefineDatasets(frxWET_CURTI_254_1, [
  DModG.frxDsDono,
  frxDsVSInnIts,
  frxDsGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_254_1, 'Custos de Produ��o');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  finally
    Screen.Cursor := crDefault;
  end;
end;
*)

procedure TFmVSCustosProdu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCustosProdu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCustosProdu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataFim.Date := Geral.PrimeiroDiaDoMes(DModG.ObtemAgora()) - 1;
  TPDataIni.Date := Geral.PrimeiroDiaDoMes(TPDataFim.Date);

  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  //
  EdGraGruY.Enabled      := False;
  CBGraGruY.Enabled      := False;
  EdGraGruY.ValueVariant := 1024;
  CBGraGruY.KeyValue     := 1024;
end;

procedure TFmVSCustosProdu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCustosProdu.frxWET_CURTI_248_A_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked, '', '', '')
  else
  if VarName ='VARF_GGX_GGY' then
  begin
    case PCTipoRel.ActivePageIndex of
      0: Value := 'Arigo: ' + Geral.FF0(EdGraGruX.ValueVariant) + ' - ' + CBGraGruX.Text;
      1: Value := 'Grupo de estoque: ' + Geral.FF0(EdGraGruY.ValueVariant) + ' - ' + CBGraGruY.Text;
    end;
  end;
end;

end.
