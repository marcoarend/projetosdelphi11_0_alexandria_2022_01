unit VSVlrPosNegSameReg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSVlrPosNegSameReg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    PnPesquisa: TPanel;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa();
  public
    { Public declarations }
  end;

  var
  FmVSVlrPosNegSameReg: TFmVSVlrPosNegSameReg;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSVlrPosNegSameReg.BtOKClick(Sender: TObject);
var
  NaoImplem: Integer;
  Tabela, sControle: String;
begin
  Screen.Cursor := crHourGlass;
  try
  NaoImplem := 0;
  DmModVS.QrVlrPosNegSameReg.First;
  PB1.Position := 0;
  PB1.Max := DmModVS.QrVlrPosNegSameReg.recordCount;
  while not DmModVS.QrVlrPosNegSameReg.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    sControle := Geral.FF0(DmModVS.QrVlrPosNegSameRegControle.Value);
    case DmModVS.QrVlrPosNegSameRegID_TTW.Value of
      0: Tabela := CO_TAB_VMI;
      1: Tabela := CO_TAB_VMB;
      else Tabela := '???';
    end;
    case TEstqMovimID(DmModVS.QrVlrPosNegSameRegMovimID.Value) of
      (*6*)TEstqMovimID.emidIndsXX:
      begin
        case TEstqMovimNiv(DmModVS.QrVlrPosNegSameRegMovimNiv.Value)  of
          (*14*)TEstqMovimNiv.eminSorcCurtiXX:
          begin
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE ' + Tabela + ' SET ' +
            ' ValorMP = ABS(ValorMP), ' +
            ' CustoMOTot = ABS(CustoMOTot), ' +
            ' ValorT = ABS(ValorMP) + ABS(CustoMOTot) ' +
            ' WHERE Controle=' + sControle);
          end;
          (*15*)TEstqMovimNiv.eminBaixCurtiXX:
          begin
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE ' + Tabela + ' SET ' +
            ' ValorMP = -ABS(ValorMP), ' +
            ' CustoMOTot = -ABS(CustoMOTot), ' +
            ' ValorT = -(ABS(ValorMP) + ABS(CustoMOTot)) ' +
            ' WHERE Controle=' + sControle);
          end;
          else
        end;
      end;
      else
      begin
        NaoImplem := NaoImplem + 1;
      end;
    end;
    //
    DmModVS.QrVlrPosNegSameReg.Next;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
  Pesquisa();
  if NaoImplem > 0 then
    Geral.MB_Info(Geral.FF0(NaoImplem) +
    ' itens n�o foram corrigidos pois o MovimID e/ou o MovimNiv n�o foram implementados!')
end;

procedure TFmVSVlrPosNegSameReg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSVlrPosNegSameReg.BtTeste2Click(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmVSVlrPosNegSameReg.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrVlrPosNegSameRegControle.Value)
end;

procedure TFmVSVlrPosNegSameReg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSVlrPosNegSameReg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DGDados.Datasource := DmModVS.DsVlrPosNegSameReg;
end;

procedure TFmVSVlrPosNegSameReg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSVlrPosNegSameReg.Pesquisa();
const
  Avisa = True;
  ForcaMostrarForm = False;
  SelfCall = True;
var
  TemIMEiMrt: Integer;
begin
  TemIMEiMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2);
end;

end.
