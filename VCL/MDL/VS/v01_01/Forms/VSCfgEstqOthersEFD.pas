unit VSCfgEstqOthersEFD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, Vcl.Mask, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  AppListas, UnProjGroup_Vars, UnProjGroup_Consts, mySQLDirectQuery,
  dmkCheckGroup, UMySQLDB, Vcl.Menus, UnAppEnums;

type
  TFmVSCfgEstqOthersEFD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    Splitter1: TSplitter;
    Panel5: TPanel;
    BtCorrige: TBitBtn;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrBxa: TmySQLQuery;
    QrBxaGGXBxa: TIntegerField;
    QrBxaIMEIBxa: TIntegerField;
    QrBxaDataHora: TDateTimeField;
    QrBxaMovimID: TIntegerField;
    QrBxaMovimNiv: TIntegerField;
    QrBxaSrcMovID: TIntegerField;
    QrBxaPecas: TFloatField;
    QrBxaAreaM2: TFloatField;
    QrBxaPesoKg: TFloatField;
    QrBxaSrcNivel2: TIntegerField;
    DsBxa: TDataSource;
    PMCorrigeID01: TPopupMenu;
    CorrigePesosBaixaInNatura1: TMenuItem;
    CorrigePesosBaixaInNatura_Atual1: TMenuItem;
    CorrigePesosBaixaInNatura_Selecionadospossveis1: TMenuItem;
    CorrigePesosBaixaInNatura_Todospossveis1: TMenuItem;
    ID01N2CorrigepeasepesobaizaInNatura1: TMenuItem;
    QrPsqSrcGGX: TIntegerField;
    QrPsqDstGGX: TIntegerField;
    QrPsqGGXRcl: TIntegerField;
    QrPsqJmpGGX: TIntegerField;
    QrPsqRmsGGX: TIntegerField;
    QrPsqGGXInn: TIntegerField;
    QrPsqIMEIInn: TIntegerField;
    QrPsqDataHora: TDateTimeField;
    QrPsqMovimID: TIntegerField;
    QrPsqMovimNiv: TIntegerField;
    QrPsqSrcMovID: TIntegerField;
    QrPsqPecasInn: TFloatField;
    QrPsqAreaM2Inn: TFloatField;
    QrPsqPesoKgInn: TFloatField;
    QrPsqSrcNivel2: TIntegerField;
    QrPsqPecasBxa: TFloatField;
    QrPsqPesoKgBxa: TFloatField;
    QrPsqAreaM2Bxa: TFloatField;
    QrPsqSdoPc: TFloatField;
    QrPsqSdoM2: TFloatField;
    QrPsqSdoKg: TFloatField;
    QrPsqReduzido: TIntegerField;
    QrPsqNO_PRD_TAM_COR: TWideStringField;
    DGPsq: TdmkDBGridZTO;
    DGGer: TdmkDBGridZTO;
    BtReopen: TBitBtn;
    QrPsqNO_MovimID: TWideStringField;
    QrPsqNO_MovimNiv: TWideStringField;
    QrBxaNO_PRD_TAM_COR: TWideStringField;
    QrBxaNO_MovimID: TWideStringField;
    QrBxaNO_MovimNiv: TWideStringField;
    Atualizasaldodoitemgerado1: TMenuItem;
    CorrigeIMEIDestinodageraogradeacima1: TMenuItem;
    CorrigeIMEIdeBaixagradeabaixo1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterScroll(DataSet: TDataSet);
    procedure QrBxaAfterOpen(DataSet: TDataSet);
    procedure QrBxaBeforeClose(DataSet: TDataSet);
    procedure PMCorrigeID01Popup(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Atual1Click(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Todospossveis1Click(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Selecionadospossveis1Click(
      Sender: TObject);
    procedure ID01N2CorrigepeasepesobaizaInNatura1Click(Sender: TObject);
    procedure BtReopenClick(Sender: TObject);
    procedure Atualizasaldodoitemgerado1Click(Sender: TObject);
    procedure CorrigeIMEIDestinodageraogradeacima1Click(Sender: TObject);
    procedure CorrigeIMEIdeBaixagradeabaixo1Click(Sender: TObject);
  private
    { Private declarations }
    FDiaIni, FDiaFim: TDateTime;
    //
    procedure CorrigeBaixasPesoKgAtual();
    function  HabilitaCorrecaoPesoBaixaPsqAtual(): Boolean;
    function  HabilitaCorrecaoPecaEPesoBaixaPsqAtual(): Boolean;
    procedure RemoveIMEI(IMEIInn: Integer);
    procedure ReopenBaixados(IMEIBxa: Integer);
    procedure ReopenSdoInn(IMEIInn: Integer);
    procedure PesquisaErros();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    //
    procedure PreencheAnoMesPeriodo(AnoMes: Integer);
  end;

  var
  FmVSCfgEstqOthersEFD: TFmVSCfgEstqOthersEFD;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, UnVS_CRC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgEstqOthersEFD.Atualizasaldodoitemgerado1Click(
  Sender: TObject);
var
  Controle, MovimID, MovimNiv: Integer;
begin
  Controle := QrPsqIMEIInn.Value;
  MovimID  := QrPsqMovimID.Value;
  MovimNiv := QrPsqMovimNiv.Value;
  //
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
  ReopenSdoInn(Controle);
end;

procedure TFmVSCfgEstqOthersEFD.BtCorrigeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCorrigeID01, BtCorrige);
end;

procedure TFmVSCfgEstqOthersEFD.BtOKClick(Sender: TObject);
begin
  PesquisaErros();
end;

procedure TFmVSCfgEstqOthersEFD.BtReopenClick(Sender: TObject);
begin
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqOthersEFD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEstqOthersEFD.CorrigeBaixasPesoKgAtual();
var
  MediaKg, PesoKg, SaldoKg, SaldoPc: Double;
  Controle: Integer;
begin
{
  MediaKg := 0;
  if QrPsqPecasInn.Value > 0 then
    MediaKg := QrPsqPesoKgInn.Value / QrPsqPecasInn.Value;
  //
  if (MediaKg > 0) and HabilitaCorrecaoPesoBaixaPsqAtual() then
  begin
    SaldoKg := QrPsqPesoKgInn.Value;
    SaldoPc := QrPsqPecasInn.Value;
    //
    QrBxa.First;
    while not QrBxa.Eof do
    begin
      Controle := QrBxaIMEIBxa.Value;
      //
      PesoKg := (Round(QrBxaPecas.Value * MediaKg * 1000)) /1000;
      SaldoKg := SaldoKg + PesoKg;
      SaldoPc := SaldoPc + QrBxaPecas.Value;
      //
      if (QrBxa.RecNo = QrBxa.RecordCount) and (SaldoPc = 0) and
      (SaldoKg <> 0) then
        PesoKg := PesoKg - SaldoKg;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'PesoKg'], ['Controle'], [
      PesoKg], [Controle], True);
      //
      QrBxa.Next;
    end;
    Controle := QrPsqIMEIInn.Value;
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    RemoveIMEI(Controle);
  end;
}
end;

procedure TFmVSCfgEstqOthersEFD.CorrigeIMEIdeBaixagradeabaixo1Click(
  Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrBxaIMEIBxa.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenBaixados(Controle);
end;

procedure TFmVSCfgEstqOthersEFD.CorrigeIMEIDestinodageraogradeacima1Click(
  Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
  Gerandos, Gerado: Double;
begin
  Controle := QrPsqIMEIInn.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenSdoInn(Controle);
end;

procedure TFmVSCfgEstqOthersEFD.CorrigePesosBaixaInNatura_Atual1Click(
  Sender: TObject);
begin
  CorrigeBaixasPesoKgAtual();
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqOthersEFD.CorrigePesosBaixaInNatura_Selecionadospossveis1Click(
  Sender: TObject);
var
  I: Integer;
begin
  with DGPsq.DataSource.DataSet do
  for I := 0 to DGPsq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DGPsq.SelectedRows.Items[I]));
    GotoBookmark(DGPsq.SelectedRows.Items[I]);
    //
    CorrigeBaixasPesoKgAtual();
  end;
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqOthersEFD.CorrigePesosBaixaInNatura_Todospossveis1Click(
  Sender: TObject);
begin
  QrPsq.First;
  while not QrPsq.Eof do
  begin
    CorrigeBaixasPesoKgAtual();
    //
    QrPsq.Next;
  end;
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqOthersEFD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgEstqOthersEFD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSCfgEstqOthersEFD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSCfgEstqOthersEFD.HabilitaCorrecaoPecaEPesoBaixaPsqAtual: Boolean;
begin
  Result := (QrPsqSdoPc.Value < 0) and (QrPsqSdoKg.Value < 0) and
  (QrBxa.State <> dsInactive) and (QrBxa.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra));
end;

function TFmVSCfgEstqOthersEFD.HabilitaCorrecaoPesoBaixaPsqAtual(): Boolean;
begin
  Result := (QrBxa.State <> dsInactive) and (QrBxa.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra)) and (QrPsqSdoPc.Value = 0);
end;

procedure TFmVSCfgEstqOthersEFD.ID01N2CorrigepeasepesobaizaInNatura1Click(
  Sender: TObject);
var
  Pecas, PesoKg: Double;
  Controle: Integer;
begin
{
  Controle := QrBxaIMEIBxa.Value;
  //
  Pecas  := QrBxaPecas.Value - QrPsqSdoPc.Value;
  PesoKg := QrBxaPesoKg.Value - QrPsqSdoKg.Value;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'Pecas', 'PesoKg'], ['Controle'], [
  Pecas, PesoKg], [Controle], True);
  //
  Controle := QrPsqIMEIInn.Value;
  VS_PF.AtualizaSaldoIMEI(Controle, True);
  RemoveIMEI(Controle);
  ReopenSdoInn();
}
end;

procedure TFmVSCfgEstqOthersEFD.PesquisaErros();
begin
  Screen.Cursor := crHourGlass;
  try
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '1/5 - Criando tabela tempor�ria de entrada / gera��o');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Baixados_LCT_INN_;  ',
  'CREATE TABLE _Baixados_LCT_INN_  ',
  'SELECT  ',
  'SrcGGX, DstGGX, GGXRcl, JmpGGX, RmsGGX, ',
  'GraGruX GGXInn,  ',
  'Controle IMEIInn,   ',
  'DataHora, MovimID, MovimNiv,   ',
  'SrcMovID, Pecas PecasInn,   ',
  'AreaM2 AreaM2Inn, PesoKg PesoKgInn   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE  ',
(*
  '   (MovimID =  6 AND MovimNiv = 13) ',
  'OR (MovimID =  7 AND MovimNiv =  2) ',
  'OR (MovimID =  8 AND MovimNiv =  2) ',
  'OR (MovimID = 11 AND MovimNiv =  8) ',
  'OR (MovimID = 11 AND MovimNiv =  9) ',
  'OR (MovimID = 13 AND MovimNiv =  0) ',
  'OR (MovimID = 14 AND MovimNiv =  2) ',
  'OR (MovimID = 15 AND MovimNiv = 12) ',
  'OR (MovimID = 16 AND MovimNiv =  0) ',
  'OR (MovimID = 19 AND MovimNiv = 21) ',
  'OR (MovimID = 20 AND MovimNiv = 22) ',
  'OR (MovimID = 21 AND MovimNiv =  0) ',
  'OR (MovimID = 22 AND MovimNiv =  0) ',
  'OR (MovimID = 23 AND MovimNiv =  0) ',
  'OR (MovimID = 24 AND MovimNiv =  2) ',
  'OR (MovimID = 25 AND MovimNiv = 28) ',
  'OR (MovimID = 26 AND MovimNiv = 30) ',
  'OR (MovimID = 27 AND MovimNiv = 35) ',
  'OR (MovimID = 28 AND MovimNiv =  2) ',
  ' ',
  'OR (MovimID = 29 AND MovimNiv = 31) ',
  ' ',
  'OR (MovimID = 32 AND MovimNiv = 51) ',
  'OR (MovimID = 33 AND MovimNiv = 56) ',
*)
  VS_PF.SQL_MovIDeNiv_Pos_All,
  ' ',
  'ORDER BY DataHora  ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '2/5 - Criando tabela tempor�ria de baixa');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Baixados_lct_bxa_;  ',
  'CREATE TABLE _Baixados_lct_bxa_  ',
  ' ',
  'SELECT GraGruX GGXBxa, Controle IMEIBxa,  ',
  'DataHora, MovimID, MovimNiv,  ',
  'SrcMovID, Pecas, AreaM2, PesoKg,  ',
  'SrcNivel2  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
  'WHERE (vmi.Pecas + vmi.PesoKg < 0  ',
  'OR vmi.MovimID IN (9,17,28)  ',
  ')  ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '3/5 - Criando tabela tempor�ria de soma de baixa');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Baixados_SUM_BXA_;  ',
  'CREATE TABLE _Baixados_SUM_BXA_  ',
  ' ',
  'SELECT SrcNivel2, SUM(vmi.Pecas) PecasBxa,  ',
  'SUM(vmi.PesoKg) PesoKgBxa,  ',
  'SUM(vmi.AreaM2) AreaM2Bxa ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE (vmi.Pecas + vmi.PesoKg < 0  ',
  'OR vmi.MovimID IN (9,17,28)  ',
  ')  ',
  'GROUP BY SrcNivel2 ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '4/5 - Criando tabela tempor�ria de saldos');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Baixados_SDO_GER_;  ',
  'CREATE TABLE _Baixados_SDO_GER_  ',
  'SELECT inn.*, bxa.*,  ',
  'inn.PecasInn+bxa.PecasBxa SdoPc,  ',
  'inn.AreaM2Inn+bxa.AreaM2Bxa SdoM2,  ',
  'inn.PesoKgInn+bxa.PesoKgBxa SdoKg   ',
  'FROM _Baixados_LCT_INN_ inn   ',
  'LEFT JOIN _Baixados_SUM_BXA_ bxa ON inn.IMEIInn=bxa.SrcNivel2  ',
  ';  ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '5/5 - Abrindo tabela tempor�ria de saldos');
  ReopenSdoInn(0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCfgEstqOthersEFD.PMCorrigeID01Popup(Sender: TObject);
var
  Habilita1, Habilita2, Habilita3, Habilita4: Boolean;
begin
  Habilita1 := (QrPsq.State <> dsInactive);
  CorrigePesosBaixaInNatura1.Enabled := Habilita1;
  //
  Habilita2 := Habilita1 and (QrPsq.RecordCount > 0);
  CorrigePesosBaixaInNatura_Selecionadospossveis1.Enabled := Habilita2;
  CorrigePesosBaixaInNatura_Todospossveis1.Enabled := Habilita2;
  //
  Habilita3 := Habilita2 and HabilitaCorrecaoPesoBaixaPsqAtual();
  CorrigePesosBaixaInNatura_Atual1.Enabled := Habilita3;
  //////////////////////////////////////////////////////////////////////////////
  Habilita4 := Habilita1 and HabilitaCorrecaoPecaEPesoBaixaPsqAtual();
  ID01N2CorrigepeasepesobaizaInNatura1.Enabled := Habilita4;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmVSCfgEstqOthersEFD.PreencheAnoMesPeriodo(AnoMes: Integer);
begin
end;

procedure TFmVSCfgEstqOthersEFD.QrBxaAfterOpen(DataSet: TDataSet);
begin
  BtCorrige.Enabled := QrBxa.RecordCount > 0;
end;

procedure TFmVSCfgEstqOthersEFD.QrBxaBeforeClose(DataSet: TDataSet);
begin
  BtCorrige.Enabled := False;
end;

procedure TFmVSCfgEstqOthersEFD.QrPsqAfterScroll(DataSet: TDataSet);
begin
  ReopenBaixados(0);
end;

procedure TFmVSCfgEstqOthersEFD.QrPsqBeforeClose(DataSet: TDataSet);
begin
  QrBxa.Close;
end;

procedure TFmVSCfgEstqOthersEFD.RemoveIMEI(IMEIInn: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Baixados_lct_bxa_',
  'WHERE SrcNivel2=' + Geral.FF0(IMEIInn),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Baixados_SDO_GER_',
  'WHERE IMEIInn=' + Geral.FF0(IMEIInn),
  '']);
end;

procedure TFmVSCfgEstqOthersEFD.ReopenBaixados(IMEIBxa: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('bxa.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('bxa.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBxa, DModG.MyPID_DB, [
  'SELECT ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR,  ',
  ATT_MovimID,
  ATT_MovimNiv,
  'bxa.* ',
  'FROM _Baixados_lct_bxa_ bxa',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=bxa.GGXBxa ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE SrcNivel2=' + Geral.FF0(QrPsqIMEIInn.Value),
  'ORDER BY IMEIBxa DESC',
  '']);
  //Geral.MB_SQL(self, QrBxa);
  QrBxa.Locate('IMEIBxa', IMEIBxa, []);
end;

procedure TFmVSCfgEstqOthersEFD.ReopenSdoInn(IMEIInn: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('sdo.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('sdo.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
  'SELECT sdo.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.Controle Reduzido, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM _Baixados_SDO_GER_ sdo ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=sdo.GGXInn ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE SdoPc <= 0 ',
  'AND ( ',
  '  SdoM2 <> 0 ',
  '  OR ',
  '  SdoKg <> 0 ',
  ') ',
  'ORDER BY DataHora ',
  '']);
  //Geral.MB_SQL(self, QrPsq);
  QrPsq.Locate('IMEIInn', IMEIInn, []);
end;

end.
