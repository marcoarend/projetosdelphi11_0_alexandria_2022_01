unit VSMOEnvEnvGer;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBGridZTO, dmkDBLookupComboBox, dmkEditCB,
  dmkEditCalc, UnProjGroup_Vars;

type
  TFmVSMOEnvEnvGer = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrVSMOEnvEnv: TmySQLQuery;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    DsVSMOEnvEVMI: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    Panel7: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label34: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label41: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label3: TLabel;
    DBMemo1: TDBMemo;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label11: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label31: TLabel;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvNFEMP_SdoPeca: TFloatField;
    QrVSMOEnvEnvNFEMP_SdoPeso: TFloatField;
    QrVSMOEnvEnvNFEMP_SdoArM2: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvNome: TWideStringField;
    QrVSMOEnvEnvNO_Empresa: TWideStringField;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    QrVSMOEnvEnvNO_Transpor: TWideStringField;
    QrVSMOEnvEnvNO_FornMO: TWideStringField;
    DBEdit28: TDBEdit;
    Label16: TLabel;
    DBEdit29: TDBEdit;
    Label17: TLabel;
    DBEdit30: TDBEdit;
    Label18: TLabel;
    DBEdit31: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSMOEnvEnvAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSMOEnvEnvBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSMOEnvAVMI(SQLType: TSQLType);
    procedure MostraJanelaIMEC();
    procedure Relocaliza();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSMOEnvAVMI(Codigo: Integer);

  end;

var
  FmVSMOEnvEnvGer: TFmVSMOEnvEnvGer;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSMOEnvAVMI, UnVS_PF,
  ModVS_CRC, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSMOEnvEnvGer.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSMOEnvEnvGer.MostraFormVSMOEnvAVMI(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmVSMOEnvAVMI, FmVSMOEnvAVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvAVMI.ImgTipo.SQLType := SQLType;
    FmVSMOEnvAVMI.FQrCab := QrVSMOEnvEnv;
    FmVSMOEnvAVMI.FDsCab := DsVSMOEnvEnv;
    FmVSMOEnvAVMI.FQrIts := QrVSMOEnvEVMI;
    if SQLType = stIns then
      FmVSMOEnvAVMI.EdCPF1.ReadOnly := False
    else
    begin
      FmVSMOEnvAVMI.EdControle.ValueVariant := QrVSMOEnvEVMIControle.Value;
      //
      FmVSMOEnvAVMI.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrVSMOEnvEVMICNPJ_CPF.Value);
      FmVSMOEnvAVMI.EdNomeEmiSac.Text := QrVSMOEnvEVMINome.Value;
      FmVSMOEnvAVMI.EdCPF1.ReadOnly := True;
    end;
    FmVSMOEnvAVMI.ShowModal;
    FmVSMOEnvAVMI.Destroy;
  end;
*)
end;

procedure TFmVSMOEnvEnvGer.MostraJanelaIMEC();
begin
  VS_CRC_PF.MostraFormVS_Do_IMEC(QrVSMOEnvEnvVSVMI_MovimCod.Value,
  'Este item de frete n�o pode ser gerenciado aqui!' + sLineBreak +
  'Deseja visualizar a janela de movimento do IME-C onde o item pode ser gerenciado?');
end;

procedure TFmVSMOEnvEnvGer.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemCabUpd(CabExclui1, QrVSMOEnvEnv);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSMOEnvEnv, QrVSMOEnvEVMI);
end;

procedure TFmVSMOEnvEnvGer.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSMOEnvEVMI);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSMOEnvEVMI);
end;

procedure TFmVSMOEnvEnvGer.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSMOEnvEnvCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSMOEnvEnvGer.DefParams;
begin
  VAR_GOTOTABELA := 'vsmoenvenv';
  VAR_GOTOMYSQLTABLE := QrVSMOEnvEnv;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mea.*, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa, ');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transpor, ');
  VAR_SQLx.Add('IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornMO ');
  VAR_SQLx.Add('FROM vsmoenvenv mea ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=mea.CFTMP_Empresa ');
  VAR_SQLx.Add('LEFT JOIN entidades tra ON tra.Codigo=mea.CFTMP_Terceiro ');
  VAR_SQLx.Add('LEFT JOIN entidades fmo ON fmo.Codigo=mea.NFEMP_Terceiro ');
  VAR_SQLx.Add('WHERE mea.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND mea.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSMOEnvEnvGer.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSMOEnvAVMI(stUpd);
end;

procedure TFmVSMOEnvEnvGer.CabExclui1Click(Sender: TObject);
begin
  VAR_VSMOEnvEnv := QrVSMOEnvEnvCodigo.Value;
  //
  if QrVSMOEnvEnvVSVMI_MovimCod.Value = 0 then
    DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI)
  else
    MostraJanelaIMEC();
  //
  Relocaliza();
end;

procedure TFmVSMOEnvEnvGer.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSMOEnvEnvGer.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSMOEnvEnvGer.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSMOEnvAVMI', 'Controle', QrVSMOEnvEVMIControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSMOEnvEVMI,
      QrVSMOEnvEVMIControle, QrVSMOEnvEVMIControle.Value);
    ReopenVSMOEnvAVMI(Controle);
  end;
}
end;

procedure TFmVSMOEnvEnvGer.Relocaliza();
var
  Codigo: Integer;
begin
  if VAR_VSMOEnvEnv <> 0 then
    Codigo := VAR_VSMOEnvEnv
  else
    Codigo := QrVSMOEnvEnvCodigo.Value;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSMOEnvEnvGer.ReopenVSMOEnvAVMI(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvEVMI, Dmod.MyDB, [
  'SELECT mei.* ',
  'FROM vsmoenvevmi mei ',
  'WHERE mei.VSMOEnvEnv=' + Geral.FF0(QrVSMOEnvEnvCodigo.Value),
  '']);
  //
  QrVSMOEnvEVMI.Locate('Codigo', Codigo, []);
end;


procedure TFmVSMOEnvEnvGer.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSMOEnvEnvGer.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSMOEnvEnvGer.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSMOEnvEnvGer.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSMOEnvEnvGer.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSMOEnvEnvGer.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvEnvGer.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSMOEnvEnvCodigo.Value;
  Close;
end;

procedure TFmVSMOEnvEnvGer.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSMOEnvAVMI(stIns);
end;

procedure TFmVSMOEnvEnvGer.CabAltera1Click(Sender: TObject);
begin
(*
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSMOEnvEnv, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsmoenvenv');
*)
  VAR_VSMOEnvEnv := QrVSMOEnvEnvCodigo.Value;
  //
  if QrVSMOEnvEnvVSVMI_MovimCod.Value = 0 then
    VS_PF.MostraFormVSMOEnvEnv(stUpd, (*QrIMEIDest*)nil, QrVSMOEnvEnv, QrVSMOEnvEVMI,
    siPositivo, 0, 0)
  else
    MostraJanelaIMEC();
  //
  Relocaliza();
end;

procedure TFmVSMOEnvEnvGer.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('vsmoenvenv', 'Codigo', ImgTipo.SQLType,
    QrVSMOEnvEnvCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('vsmoenvenv', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrVSMOEnvEnvCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'vsmoenvenv',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmVSMOEnvEnvGer.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsmoenvenv', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsmoenvenv', 'Codigo');
end;

procedure TFmVSMOEnvEnvGer.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSMOEnvEnvGer.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSMOEnvEnvGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGVSMOEnvAVMI.Align := alClient;
  CriaOForm;
  FSeq := 0;
  VAR_PC_FRETE_VS_NAME := 'TsEnvioMO';
end;

procedure TFmVSMOEnvEnvGer.FormDestroy(Sender: TObject);
begin
  VAR_PC_FRETE_VS_NAME := '';
end;

procedure TFmVSMOEnvEnvGer.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSMOEnvEnvCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMOEnvEnvGer.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSMOEnvEnvGer.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSMOEnvEnvCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSMOEnvEnvGer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSMOEnvEnvGer.QrVSMOEnvEnvAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSMOEnvEnvGer.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMOEnvAVMI(0);
end;

procedure TFmVSMOEnvEnvGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSMOEnvEnvCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSMOEnvEnvGer.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSMOEnvEnvCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsmoenvenv', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSMOEnvEnvGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvEnvGer.CabInclui1Click(Sender: TObject);
begin
{
  VAR_VSMOEnvEnv := QrVSMOEnvEnvCodigo.Value;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, (*QrIMEIDest*)nil, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, ?, ?);
  //
  Relocaliza();
}
end;

procedure TFmVSMOEnvEnvGer.QrVSMOEnvEnvBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

procedure TFmVSMOEnvEnvGer.QrVSMOEnvEnvBeforeOpen(DataSet: TDataSet);
begin
  QrVSMOEnvEnvCodigo.DisplayFormat := FFormatFloat;
end;

end.

