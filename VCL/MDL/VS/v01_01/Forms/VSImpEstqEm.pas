unit VSImpEstqEm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  frxClass, frxDBSet, Data.DB, mySQLDbTables,
  dmkGeral, DmkEditCB, dmkDBGridZTO, UnDmkProcFunc, AppListas, UnInternalConsts,
  UnProjGroup_Consts, Vcl.Grids, Vcl.DBGrids, UnDmkEnums, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.ExtCtrls, UnAppEnums;

type
  TFmVSImpEstqEm = class(TForm)
    QrOpePWE: TmySQLQuery;
    QrOpePWEPecas: TFloatField;
    QrOpePWEAreaM2: TFloatField;
    QrOpePWEPesoKg: TFloatField;
    mySQLQuery1: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    QrOpePWEMID: TIntegerField;
    QrOpePWECod: TIntegerField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FVmiEstqEmVmi, FVmiEstqEmAtzA, FVmiEstqEmAtzB, FVmiEstqEmS01: String;
    //
    procedure Atualiza_Ope_e_PWE();
    procedure AtualizaZerados();
    procedure GeraItensEm();
    procedure InsereVMIsDeTabela(TabelaSrc: String);
  public
    { Public declarations }
    FEntidade, FFilial, Ed00Terceiro_ValueVariant, RG00_Ordem1_ItemIndex,
    RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
    RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex, Ed00StqCenCad_ValueVariant,
    RG00ZeroNegat_ItemIndex: Integer;
    // 2021-01-09 ini
    //FGraCusPrc: FInteger;
    FRelatorio: TRelatorioVSImpEstoque;
    // 2021-01-09 fim
    FGraCusPrc,
    FEd00NFeIni_ValueVariant,
    FEd00NFeFim_ValueVariant, FEd00Serie_ValueVariant, FMovimCod, FClientMO:
    Integer;
    FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text: String;
    TPDataRelativa_Date: TDateTime;
    Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked, FMostraFrx: Boolean;
    //EdEmpresa: TDmkEditCB;
    DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
    Ck00DescrAgruNoItm_Checked, FCk00Serie_Checked: Boolean;
    //
    FLaAviso1, FLaAviso2: TLabel;
    //
    FDataEm: TDateTime;
    //
    function  ImprimeEstoqueEm(): Boolean;
  end;

var
  FmVSImpEstqEm: TFmVSImpEstqEm;

implementation

{$R *.dfm}

uses ModuleGeral, CreateVS, UnMyObjects, DmkDAC_PF, UMySQLModule, Module,
  MyDBCheck, UnVS_PF, UnVS_CRC_PF;

{ TFmVSImpEstqEm }

procedure TFmVSImpEstqEm.AtualizaZerados();
const
  Zerado = 1;
  SdoVrtPeca = 0;
  SdoVrtPeso = 0;
  SdoVrtArM2 = 0;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True,
    'Gerando saldo zerado de zerados for�ados pelo usu�rio');
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVmiEstqEmVmi, False, [
  'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
  'Zerado'], [
  SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [
  Zerado], False);
end;

procedure TFmVSImpEstqEm.Atualiza_Ope_e_PWE();
const
  TxtPre = 'Gerando saldos de OOs e OPs. Item ';
var
  //MovimCod,
  Codigo, MovimID, MovimNiv: Integer;
  Pecas, PesoKg, AreaM2(*, AreaP2, ValorT*): Double;
  TxtNum, TxtPos: String;
begin
  FVmiEstqEmAtzA := '_vmi_estq_em_atz_a';
  FVmiEstqEmAtzB := '_vmi_estq_em_atz_b';
  UnDMkDAC_PF.AbreMySQLQuery0(QrOpePWE, DModG.MyPID_DB, [
////////////////////////////////////////////////////////////////////////////////
  'DROP TABLE IF EXISTS ' + FVmiEstqEmAtzA + ';  ',
  'CREATE TABLE ' + FVmiEstqEmAtzA + ' ',
(* 2017-11-25
  'SELECT COUNT(IF(SdoVrtPeca>0, 1, 0)) Itens, ',
*)
  'SELECT COUNT(IF(SdoVrtPeca>0 OR (SdoVrtPeso>0 AND Pecas=0), 1, 0)) Itens, ',
(* Fim 2017-11-25*)
(*  2017-11-08
  'IF(MovimID IN (6,11,19,26,27,33), IF(MovimNiv IN (15,34,37), SrcMovID, MovimID), IF(SrcMovID=20 AND MovimID=28, 19, SrcMovID)) MID,',
  'IF(MovimID IN (6,11,19,26,27,33), IF(MovimNiv IN (15,34,37), SrcNivel1, Codigo), SrcNivel1) Cod, ',
*)
(* Ini 2022-08-08
  'IF(MovimID IN (6,11,19,26,27,32,33), IF(MovimNiv IN (15,34,37), SrcMovID, MovimID),  ',
fim 2022-08-08*)
  //'IF(MovimID IN (6,11,19,26,27,32,33,39), IF(MovimNiv IN (15,34,37), SrcMovID, MovimID),  ',  // 6,11,19,26,27,32,33,39
  'IF(MovimID IN (' + CO_ALL_CODS_INN_PARCIAL_SPED_VS_MID + '), IF(MovimNiv IN (15,34,37), SrcMovID, MovimID),  ',  // 6,11,19,26,27,32,33,39
  '  IF(SrcMovID=20 AND MovimID=28, 19, ',
  '    IF(MovimID=29 AND SrcMovID=0, 26, ',
  // exemplo aqui: Saldo do Couro em conserva��o com baixa parcial para conservado
  // somei MovID 39 + MovID.40 alterando o 40 para 39 no campo MID, e somando
  // o MovimNiv.Inn positivo (66) com o MovimNiv.Bxa negativo (68) na
  // constante CO_CODS_OPER_PROC_NIV_GET - 2022-08-08
  '      IF(MovimID=40, 39, SrcMovID)))) MID, ',
  //'IF(MovimID IN (6,11,19,26,27,29,32,33,39,40), IF(MovimNiv IN (15,34,37), SrcNivel1, Codigo), SrcNivel1) Cod,  ',
  'IF(MovimID IN (' + CO_ALL_CODS_INN_PARCIAL_SPED_VS_Cod + '), IF(MovimNiv IN (15,34,37), SrcNivel1, Codigo), SrcNivel1) Cod,  ',
  //  Fim 2017-11-08
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * Pecas) Pecas,  ',
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * AreaM2) AreaM2,  ',
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * AreaP2) AreaP2,  ',
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * PesoKg) PesoKg,  ',
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * ValorT) ValorT  ',
  'FROM ' + FVmiEstqEmVmi,
  'WHERE ( ',
  '  MovimID IN (' + CO_CODS_OPER_PROC_ID_GET + ')  ', // 2017-11-08 adicionado 29
  '  AND MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_FRO + ')  ',
  ') OR ( ',
  '    MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ')  ',
  '    AND SrcNivel2 IN ',
  '    ( ',
  '      SELECT Controle ',
  '      FROM ' + FVmiEstqEmVmi + ' ',
  '      WHERE MovimID IN (' + CO_CODS_OPER_PROC_ID_GET + ') ',
  '    ) ',
  ') OR (',
  '  MovimID=6 AND SrcMovID=27 ',
  ') ',
  'GROUP BY MID, Cod ',
  ';  ',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmAtzB + ';  ',
  'CREATE TABLE ' + FVmiEstqEmAtzB + ' ',
  'SELECT Itens, MID, Cod, SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, SUM(AreaM2) AreaM2  ',
  'FROM ' + FVmiEstqEmAtzA + ' ',
  'GROUP BY MID, Cod ',
  '; ',
  //
  'UPDATE ' + FVmiEstqEmVmi,
  'SET SdoVrtPeca=0, SdoVrtPeso=0, SdoVrtArM2=0 ', //, SdoVrtArP2=0 ',
  //'WHERE MovimID IN (26,27) AND MovimNiv IN (30,35);', 2022-08-08
  'WHERE MovimID IN (26,27,39) AND MovimNiv IN (30,35,66);',
  //
  'SELECT * FROM ' + FVmiEstqEmAtzB + ' ',
  'WHERE Pecas>0  ',
  'OR (MID=32 AND PesoKg>0) ', // 2017-11-25
  '']);
  //Geral.MB_SQL(Self, QrOpePWE);
  //
  TxtPos := Geral.FF0(QrOpePWE.RecordCount);
  //
  QrOpePWE.First;
  while not QrOpePWE.Eof do
  begin
    TxtNum := Geral.FF0(QrOpePWE.RecNo);
    MyObjects.Informa2(FLaAviso1, FLaAviso2, True, TxtPre + TxtNum + TxtPos);
    //
    Codigo  := QrOpePWECod.Value;
    MovimID := QrOpePWEMID.Value;
    Pecas   := 0;
    PesoKg  := 0;
    AreaM2  := 0;
    //
    case TEstqMovimID(QrOpePWEMID.Value) of
      emidCompra:     MovimNiv := Integer(TEstqMovimNiv.eminEmCalInn);  // 30
      emidEmOperacao: MovimNiv := Integer(TEstqMovimNiv.eminEmOperInn); //  8
      emidEmProcWE:   MovimNiv := Integer(TEstqMovimNiv.eminEmWEndInn); // 21
      emidEmProcCon:  MovimNiv := Integer(TEstqMovimNiv.eminEmConInn);  // 66
      emidEmProcCal:  MovimNiv := Integer(TEstqMovimNiv.eminEmCalInn);  // 30
      emidEmProcCur:  MovimNiv := Integer(TEstqMovimNiv.eminEmCurInn);  // 35
      emidEmProcSP:   MovimNiv := Integer(TEstqMovimNiv.eminEmPSPInn);  // 50
      emidEmReprRM:   MovimNiv := Integer(TEstqMovimNiv.eminEmRRMInn);  // 55
      else
      begin
        MovimNiv := -1;
        Geral.MB_Erro(
        '"MovimID" n�o implementado em "FmVSImpEstqEm.Atualiza_Ope_e_PWE()"' +
        slineBreak + 'MovimID: ' + Geral.FF0(QrOpePWEMID.Value));
      end;
    end;
    //
    if QrOpePWEPecas.Value >= 0 then
    begin
      Pecas  := QrOpePWEPecas .Value;
      PesoKg := QrOpePWEPesoKg.Value;
      AreaM2 := QrOpePWEAreaM2.Value;
    end else
    if (TEstqMovimID(QrOpePWEMID.Value) = emidEmProcSP(*32*))
    and (QrOpePWEPesoKg.Value >= 0) then
    begin
      Pecas  := QrOpePWEPecas .Value;
      PesoKg := QrOpePWEPesoKg.Value;
      AreaM2 := QrOpePWEAreaM2.Value;
    end;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVmiEstqEmVmi, False, [
    'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
    'Codigo', 'MovimID', 'MovimNiv'], [
    Pecas, PesoKg, AreaM2], [
    Codigo, MovimID, MovimNiv], False);
    //
    QrOpePWE.Next;
  end;
end;

procedure TFmVSImpEstqEm.FormCreate(Sender: TObject);
begin
  FMostraFrx := True;
end;

procedure TFmVSImpEstqEm.GeraItensEm();
var
  ID_e_Niv: String;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo');
  FVmiEstqEmVmi := UnCreateVS.RecriaTempTableNovo(ntrttVmiEstqEmVmi,
    DModG.QrUpdPID1, False);
  FVmiEstqEmS01 := '_vmi_estq_em_s01_';
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Morto');
  InsereVMIsDeTabela(CO_TAB_VMB);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Ativo');
  InsereVMIsDeTabela(CO_SEL_TAB_VMI);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de In Natura');
  // MovimID=1 (Entrada ID=1Niv=0 e baixa por ID6Niv14 + IDs9Niv0 e ID17Niv0
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FVmiEstqEmVmi,
  'SET SdoVrtPeca=Pecas, SdoVrtPeso=PesoKg, SdoVrtArM2=AreaM2 ',
  'WHERE MovimID=1;',
  '',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FVmiEstqEmS01 + ' ',
  'SELECT SrcNivel2,  ',
  'SUM(vmi.Pecas) Pecas,  ',
  'SUM(vmi.PesoKg) PesoKg,  ',
  'SUM(vmi.AreaM2) AreaM2 ',
  'FROM ' + FVmiEstqEmVmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN ( ',
  '  SELECT Controle ',
  '  FROM ' + FVmiEstqEmVmi + ' ',
  '  WHERE MovimID=1 ',
  ') ',
  'AND (vmi.Pecas < 0  ',
  //
  'OR (vmi.Pecas = 0 AND vmi.PesoKg<0) ',
  //
  'OR (vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + '))  ',
  ') ',
  'GROUP BY SrcNivel2 ',
  '; ',
  'UPDATE ' + FVmiEstqEmVmi + ' vmi  ',
  'LEFT JOIN ' + FVmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtPeca = vmi.Pecas  + s01.Pecas, ',
  '  vmi.SdoVrtPeso = vmi.PesoKg + s01.PesoKg, ',
  '  vmi.SdoVrtArM2 = vmi.AreaM2 + s01.AreaM2 ',
  'WHERE vmi.MovimID=1 ',
  'AND NOT s01.Pecas IS NULL ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de movimentos');
  ID_e_Niv := VS_PF.SQL_MovIDeNiv_Pos_All();
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FVmiEstqEmVmi,
  'SET SdoVrtPeca=Pecas, SdoVrtPeso=PesoKg, SdoVrtArM2=AreaM2 ',
  'WHERE (' + ID_e_Niv + ');',
  '',
  'DROP TABLE IF EXISTS ' + FVmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FVmiEstqEmS01 + ' ',
  'SELECT vmi.SrcNivel2, SUM(vmi.Pecas) Pecas,  ',
  'SUM(vmi.PesoKg) PesoKg,  ',
  'SUM(vmi.AreaM2) AreaM2,  ',
  'SUM(vmi.QtdGerPeca) QtdGerPeca,  ',
  'SUM(vmi.QtdGerPeso) QtdGerPeso,  ',
  'SUM(vmi.QtdGerArM2) QtdGerArM2,  ',
  'SUM(vmi.QtdGerArP2) QtdGerArP2  ',
  'FROM ' + FVmiEstqEmVmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN (  ',
  '  SELECT Controle  ',
  '  FROM ' + FVmiEstqEmVmi + '  ',
  '  WHERE (' + ID_e_Niv + ') ',
  ')  ',
  'AND (vmi.Pecas + vmi.PesoKg < 0  ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ')  ',
  ')  ',
  'GROUP BY SrcNivel2  ',
  '; ',
  'UPDATE ' + FVmiEstqEmVmi + ' vmi  ',
  'LEFT JOIN ' + FVmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtPeca = vmi.Pecas  + s01.Pecas, ',
  '  vmi.SdoVrtPeso = vmi.PesoKg + s01.PesoKg, ',
  '  vmi.SdoVrtArM2 = vmi.AreaM2 + s01.AreaM2 ',
  'WHERE ((' + ID_e_Niv + '))',
  'AND NOT s01.Pecas IS NULL ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
end;

function TFmVSImpEstqEm.ImprimeEstoqueEm(): Boolean;
var
  DataRetroativa: String;
begin
  Result := False;
  GeraItensEm();  // _vmi_estq_em_s01_
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de OOs e OPs.');
  Atualiza_Ope_e_PWE();
  AtualizaZerados();
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando relat�rio de "estoque em"');
  DataRetroativa := Geral.FDT(FDataEm, 2);
  Result := VS_CRC_PF.ImprimeEstoqueReal(FEntidade, FFilial, Ed00Terceiro_ValueVariant,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex,
  Ck00DescrAgruNoItm_Checked, Ed00StqCenCad_ValueVariant,
  RG00ZeroNegat_ItemIndex, FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text,
  TPDataRelativa_Date, Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked,
  DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2, Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2, FVmiEstqEmVmi, DataRetroativa,
  // 2021-01-09 ini
  //FGraCusPrc,
  FRelatorio,
  // 2021-01-09 fim

  FDataEm,
  FEd00NFeIni_ValueVariant, FEd00NFeFim_ValueVariant,
  FCk00Serie_Checked, FEd00Serie_ValueVariant, FMovimCod, FClientMO,
  FMostraFrx);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, False, '...');
end;

procedure TFmVSImpEstqEm.InsereVMIsDeTabela(TabelaSrc: String);
begin
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVmiEstqEmVmi,
  'SELECT  ',
  'Codigo, Controle, ',
  'MovimCod, MovimNiv,  ',
  'MovimTwn, Empresa, Terceiro,  ',
  'CliVenda, MovimID, LnkIDXtr,  ',
  'LnkNivXtr1, LnkNivXtr2, DataHora,  ',
  'Pallet, GraGruX, Pecas,  ',
  'PesoKg, AreaM2, AreaP2,  ',
  'ValorT, SrcMovID, SrcNivel1,  ',
  'SrcNivel2, SrcGGX, 0 SdoVrtPeca,  ',
  '0 SdoVrtPeso, 0 SdoVrtArM2, Observ,  ',
  'SerieFch, Ficha, Misturou,  ',
  'FornecMO, CustoMOKg, CustoMOTot,  ',
  'ValorMP, DstMovID, DstNivel1,  ',
  'DstNivel2, DstGGX, QtdGerPeca,  ',
  'QtdGerPeso, QtdGerArM2, QtdGerArP2,  ',
  'QtdAntPeca, QtdAntPeso, QtdAntArM2,  ',
  'QtdAntArP2, AptoUso, NotaMPAG,  ',
  'Marca, TpCalcAuto, Zerado,  ',
  'EmFluxo, NotFluxo, FatNotaVNC,  ',
  'FatNotaVRC, PedItsLib, PedItsFin,  ',
  'PedItsVda, GSPSrcMovID, GSPSrcNiv2,   ',
  'CustoMOM2, ReqMovEstq, StqCenLoc,  ',
  'ItemNFe, VSMorCab, VSMulFrnCab, ',
  'ClientMO, ',
  'NFeSer, NFeNum, VSMulNFeCab, ',
  'Ativo ',
  'FROM ' + TMeuDB + '.' + TabelaSrc,
  ' ',
  'WHERE DataHora<"' + Geral.FDT(Int(FDataEm + 1), 1) + '" ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
end;

end.
