unit VSCorreCustosIMECs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, UnProjGroup_Consts, mySQLDirectQuery;

type
  THackDBGrid = class(TDBGrid);
  TFmVSCorreCustosIMECs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrGraGruXGrandeza: TSmallintField;
    DsGraGruX: TDataSource;
    PnPesquisa: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    EdCustoMinM2: TdmkEdit;
    Label1: TLabel;
    QrVmi2: TMySQLQuery;
    QrSum: TMySQLQuery;
    QrVmi1: TMySQLQuery;
    QrVmi1Codigo: TIntegerField;
    QrVmi1MovimCod: TIntegerField;
    QrVmi1Controle: TIntegerField;
    QrVmi1Pecas: TFloatField;
    QrVmi1AreaM2: TFloatField;
    QrVmi1ValorT: TFloatField;
    QrVmi1SrcMovID: TIntegerField;
    QrVmi1SrcNivel1: TIntegerField;
    QrVmi1SrcNivel2: TIntegerField;
    QrVmi1SrcGGX: TIntegerField;
    QrVmi2Codigo: TIntegerField;
    QrVmi2MovimCod: TIntegerField;
    QrVmi2Controle: TIntegerField;
    QrVmi2Pecas: TFloatField;
    QrVmi2AreaM2: TFloatField;
    QrVmi2ValorT: TFloatField;
    QrVmi2SrcMovID: TIntegerField;
    QrVmi2SrcNivel1: TIntegerField;
    QrVmi2SrcNivel2: TIntegerField;
    QrVmi2SrcGGX: TIntegerField;
    PB1: TProgressBar;
    QrVmi3: TMySQLQuery;
    QrVmi3Codigo: TIntegerField;
    QrVmi3MovimCod: TIntegerField;
    QrVmi3Controle: TIntegerField;
    QrVmi3Pecas: TFloatField;
    QrVmi3AreaM2: TFloatField;
    QrVmi3ValorT: TFloatField;
    QrVmi3SrcMovID: TIntegerField;
    QrVmi3SrcNivel1: TIntegerField;
    QrVmi3SrcNivel2: TIntegerField;
    QrVmi3SrcGGX: TIntegerField;
    CkJaCorrigidos: TCheckBox;
    QrVmi4: TMySQLQuery;
    QrVmi4Codigo: TIntegerField;
    QrVmi4MovimCod: TIntegerField;
    QrVmi4Controle: TIntegerField;
    QrVmi4Pecas: TFloatField;
    QrVmi4AreaM2: TFloatField;
    QrVmi4ValorT: TFloatField;
    QrVmi4SrcMovID: TIntegerField;
    QrVmi4SrcNivel1: TIntegerField;
    QrVmi4SrcNivel2: TIntegerField;
    QrVmi4SrcGGX: TIntegerField;
    QrVmi1DstNivel2: TIntegerField;
    QrVmi2DstNivel2: TIntegerField;
    QrVmi3DstNivel2: TIntegerField;
    QrVmi4DstNivel2: TIntegerField;
    Label3: TLabel;
    EdIMECIni: TdmkEdit;
    Qr0615: TMySQLQuery;
    Qr0615Controle: TIntegerField;
    Qr0615MovimTwn: TIntegerField;
    Qr0615MovCodPai: TIntegerField;
    Qr0615JmpMovID: TIntegerField;
    Qr0615SrcNivel2: TIntegerField;
    DqAux: TMySQLDirectQuery;
    Qr0615Pecas: TFloatField;
    Qr0615PesoKg: TFloatField;
    Qr0615ValorMP: TFloatField;
    Qr0615ValorT: TFloatField;
    EdMovimIDs: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
    FCount: Integer;
    MovimCods11, MovimCods15 : Array of Integer;
    //
    procedure AlteraParaCorrigido(Controle: Integer);
    procedure AtualizaCustoDeOrigemA(Controle, SrcNivel2: Integer; AreaM2,
              ValorT: Double; Sinal: TSinal);
    procedure CorrigePreReclasse(MovimCod: Integer);
    procedure CorrigeVSOperacao(MovimCod: Integer);
    procedure CorrigeVSGerArt(MovimCod: Integer);

  public
    { Public declarations }
  end;

  var
  FmVSCorreCustosIMECs: TFmVSCorreCustosIMECs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF, UMySQLDB, ModuleGeral;

{$R *.DFM}

procedure TFmVSCorreCustosIMECs.AlteraParaCorrigido(Controle: Integer);
begin
  UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB,
  ' UPDATE vsmovits SET Corrigido=1' +
  ' WHERE Controle=' + Geral.FF0(Controle));
end;

procedure TFmVSCorreCustosIMECs.AtualizaCustoDeOrigemA(Controle,
  SrcNivel2: Integer; AreaM2, ValorT: Double; Sinal: TSinal);
var
  NewValorT: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi2, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE Controle=' + Geral.FF0(SrcNivel2),
  '']);
  if QrVmi2AreaM2.Value <> 0 then
  begin
    if QrVmi2AreaM2.Value <> 0 then
      NewValorT := ABS(QrVmi2ValorT.Value / QrVmi2AreaM2.Value * AreaM2)
    else
      NewValorT := 0.0000;
    if Sinal = TSinal.siNegativo then
      NewValorT := - NewValorT;
    if ABS(NewValorT) > ABS(ValorT) then
    begin
      UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
      Geral.FFT_Dot(NewValorT, 6, siNegativo) +
      ', Corrigido=1 ' +
      ' WHERE Controle=' + Geral.FF0(Controle));
      FCount := FCount + 1;
    end else
      AlteraParaCorrigido(Controle);
  end else
    AlteraParaCorrigido(Controle);
end;

procedure TFmVSCorreCustosIMECs.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmVSCorreCustosIMECs.BtOKClick()';
var
  Controle: Integer;
  Tabela: String;
  ValorT: Double;
begin
  Screen.Cursor := crHourGlass;
  FCount := 0;
  try
  DmModVS.QrCorreCustosIMECs.First;
  PB1.Position := 0;
  PB1.Max := DmModVS.QrCorreCustosIMECs.RecordCount;
  while not DmModVS.QrCorreCustosIMECs.Eof do
  begin
    Controle := DmModVS.QrCorreCustosIMECsControle.Value;
    if Controle <> 0 then
    begin
      case TEstqMovimID(DmModVS.QrCorreCustosIMECsMovimID.Value) of
        (*1*)emidCompra,
        (*12*)emidResiduoReclas,
        (*16*)emidEntradaPlC,
        (*17*)emidExtraBxa:
        begin
          AlteraParaCorrigido(Controle);
        end;
        (*2*)emidVenda:
        begin
          AtualizaCustoDeOrigemA(
          DmModVS.QrCorreCustosIMECsControle.Value,
          DmModVS.QrCorreCustosIMECsSrcNivel2.Value,
          DmModVS.QrCorreCustosIMECsAreaM2.Value,
          DmModVS.QrCorreCustosIMECsValorT.Value,
          TSinal.siNegativo);
        end;
        (*6*)emidIndsXX:
        begin
          CorrigeVSGerArt(DmModVS.QrCorreCustosIMECsMovimCod.Value);
        end;
        (*7*)emidClassArtXXUni,
        (*8*)emidReclasXXUni:
        begin
          case TEstqMovimNiv(DmModVS.QrCorreCustosIMECsMovimNiv.Value) of
            (*1*)eminSorcClass: AtualizaCustoDeOrigemA(
              DmModVS.QrCorreCustosIMECsControle.Value,
              DmModVS.QrCorreCustosIMECsSrcNivel2.Value,
              DmModVS.QrCorreCustosIMECsAreaM2.Value,
              DmModVS.QrCorreCustosIMECsValorT.Value,
              TSinal.siNegativo);
            (*2*)eminDestClass: AtualizaCustoDeOrigemA(
              DmModVS.QrCorreCustosIMECsControle.Value,
              DmModVS.QrCorreCustosIMECsSrcNivel2.Value,
              DmModVS.QrCorreCustosIMECsAreaM2.Value,
              DmModVS.QrCorreCustosIMECsValorT.Value,
              TSinal.siPositivo);
            else
              Geral.MB_Info('MovimNiv:' + Geral.FF0(DmModVS.QrCorreCustosIMECsMovimNiv.Value) +
              ' n�o implementado em ' + sProcName + sLineBreak +
              'O processo ser� interrompido (MovimID=' +
              Geral.FF0(DmModVS.QrCorreCustosIMECsMovimNiv.Value) +')!');
          end;
        end;
        (*11*)emidEmOperacao:
        begin
          CorrigeVSOperacao(DmModVS.QrCorreCustosIMECsMovimCod.Value);
        end;
        (*15*)emidPreReclasse:
        begin
          CorrigePreReclasse(DmModVS.QrCorreCustosIMECsMovimCod.Value);
        end;
        //....
        //....
        else
        begin
          Geral.MB_Info('MovimID:' + Geral.FF0(DmModVS.QrCorreCustosIMECsMovimID.Value) +
          ' n�o implementado em ' + sProcName + sLineBreak +
          'O processo ser� interrompido!');
          Exit;
        end;
      end;
    //
    end;
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    DmModVS.QrCorreCustosIMECs.Next;
  end;
  //
  finally
    Screen.Cursor := crDefault;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, LaAviso1.Caption +
    ' Corrigidos: ' + Geral.FF0(FCount));
end;

procedure TFmVSCorreCustosIMECs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCorreCustosIMECs.BtTeste2Click(Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = False;
  SelfCall = True;
  MargemErro = 0.001;
var
  Filial, Empresa, GraGruX, TemIMEIMrt, IMECIni: Integer;
  sMovimIDs: String;
begin
  sMovimIDs := EdMovimIDs.ValueVariant;
  //
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  GraGruX := EdGraGruX.ValueVariant;
  IMECIni := EdIMECIni.ValueVariant;
//  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
  //
  DmModVS.CorreCustosIMECs(Empresa, GraGruX, IMECIni, MargemErro,
  EdCustoMinM2.ValueVariant, CkJaCorrigidos.Checked, Avisa, ForcaMostrarForm,
  SelfCall, TemIMEIMrt, sMovimIDs, LaAviso1, LaAviso2);
end;

procedure TFmVSCorreCustosIMECs.CorrigePreReclasse(MovimCod: Integer);
var
  NewValorT, ValorT, AllAreaM2, CustoM2, ItsValorT: Double;
  Controle1, Controle2, N, I: Integer;
begin
  N := Length(MovimCods15);
  for I := 0 to N - 1 do
  begin
    if MovimCods15[I] = MovimCod then
    begin
      Break;
      Exit;
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi1, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)), //11 ',
  '']);
  QrVmi1.First;
  while not QrVmi1.Eof do
  begin
    AtualizaCustoDeOrigemA(QrVmi1Controle.Value, QrVmi1SrcNivel2.Value,
    QrVmi1AreaM2.Value, QrVmi1ValorT.Value, TSinal.siNegativo);
    //
   QrVmi1.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(ValorT) ValorT, SUM(AreaM2) AreaM2  ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)), //11 ',
  '']);
  //
  NewValorT := ABS(QrSum.Fields[0].AsFloat);
  AllAreaM2 := ABS(QrSum.Fields[1].AsFloat);
  if AllAreaM2 <> 0 then
    CustoM2   := NewValorT / AllAreaM2
  else
    CustoM2 := 0.0000;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi2, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestPreReclas)), //=12
  '']);
  ValorT := ABS(QrVmi2ValorT.Value);
  Controle1 := QrVmi2Controle.Value;
  if Controle1 <> 0 then
  begin
    if NewValorT > ValorT then
    begin
      UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
      Geral.FFT_Dot(NewValorT, 6, siPositivo) +
      ', Corrigido=1 ' +
      ' WHERE Controle=' + Geral.FF0(Controle1));
      FCount := FCount + 1;
    end else
      AlteraParaCorrigido(Controle1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVmi3, Dmod.MyDB, [
    'SELECT Codigo, MovimCod, Controle,  ',
    'Pecas, AreaM2, ValorT,  ',
    'SrcMovID, SrcNivel1, ',
    'SrcNivel2, SrcGGX, DstNivel2 ',
    'FROM vsmovits ',
    'WHERE SrcNivel2=' + Geral.FF0(Controle1),
    '']);
    while not QrVmi3.Eof  do
    begin
      ItsValorT := Geral.RoundC(CustoM2 * QrVmi3AreaM2.Value, 4);
      Controle2 := QrVmi3Controle.Value;
      //
      if Controle2 <> 0 then
      begin
        if ABS(ItsValorT) > ABS(QrVmi3ValorT.Value) then
        begin
            UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
            Geral.FFT_Dot(-ItsValorT, 6, siNegativo) +
            ', Corrigido=1 ' +
            ' WHERE Controle=' + Geral.FF0(Controle2));
            FCount := FCount + 1;
        end else
          AlteraParaCorrigido(Controle2);
      end;
      QrVmi3.Next;
    end;
  end;
  //
  //
  N := Length(MovimCods15);
  SetLength(MovimCods15, N + 1);
  MovimCods15[N] := MovimCod;
end;

procedure TFmVSCorreCustosIMECs.CorrigeVSGerArt(MovimCod: Integer);
const
  sProcName = 'TFmVSCorreCustosIMECs.CorrigeVSGerArt()';
var
  CustoMOKg, CredPereImposto, CustoMOUni, CustoMOTot, KgOri, PcOri, VMOri,
  VTOri, CusUnit, ValorMP, CustoPQ, ValorT: Double;
  sMovimCod, sValorMP, sValorT: String;
begin
(*
  if (MovimCod = 2634) or (MovimCod = 2657) then
    Geral.MB_Aviso('2634, 2647');
*)
  sMovimCod := Geral.FF0(MovimCod);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr0615, Dmod.MyDB, [
  'SELECT Controle, SrcNivel2, MovimTwn, ',
  'MovCodPai, JmpMovID, Pecas, PesoKg, ',
  'ValorMP, ValorT ',
  'FROM vsmovits ',
  'WHERE MovimID=6 ',
  'AND MovimNiv=15 ',
  'AND MovimCod=' + sMovimCod,
  'ORDER BY Controle ',
  '']);
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT CustoMOKg, CredPereImposto ',
  'FROM vsmovits ',
  'WHERE MovimNiv=13 ',
  'AND MovimCod=' + sMovimCod,
  '']);
  CustoMOKg := USQLDB.Flu(DqAux, 'CustoMOKg');
  CredPereImposto := USQLDB.Flu(DqAux, 'CredPereImposto');
  CustoMOUni := CustoMOKg * (1- (CredPereImposto / 100));
  //
  Qr0615.First;
  while not Qr0615.Eof do
  begin
    //if Qr0615MovCodPai.Value = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
      // ValorT deve ser igual a ValorMP + CustoPQ + CustoMOTot
      'SELECT PesoKg, Pecas, ValorT ',
      'FROM vsmovits ',
      'WHERE Controle=' + Geral.FF0(Qr0615SrcNivel2.Value),
      '']);
      PcOri := ABS(USQLDB.Flu(DqAux, 'Pecas'));
      KgOri := ABS(USQLDB.Flu(DqAux, 'PesoKg'));
      VTOri := ABS(USQLDB.Flu(DqAux, 'ValorT'));
      //
      if KgOri >= 0.001 then
      begin
        CusUnit := VTOri / KgOri;
        ValorMP := ABS(Qr0615PesoKg.Value * CusUnit);
      end else
      if PcOri >= 0.001 then
      begin
        CusUnit := VTOri / PcOri;
        ValorMP := ABS(Qr0615Pecas.Value * CusUnit);
      end
      else
        ValorMP := 0.00;
      //
      CustoMOTot := ABS(CustoMOUni * Qr0615PesoKg.Value);
      ValorT     := ValorMP + CustoMOTot;
      //
      sValorMP := Geral.FFT_Dot(ValorMP, 4, siPositivo);
      sValorT := Geral.FFT_Dot(ValorT, 4, siPositivo);
      //
      if (-ValorMP <> Qr0615ValorMP.Value) (*or (-ValorT <> Qr0615ValorT.Value)*) then
      begin
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE vsmovits SET ValorMP=-' + sValorMP +
        ', ValorT=-' + sValorMP + // ValorT=ValorMP para Niv 15
        ' WHERE Controle=' + Geral.FF0(Qr0615Controle.Value));
      end;
      //
      UnDmkDAC_PF.ExecutaDB(DMod.MyDB,
      ' UPDATE vsmovits SET ValorMP=' + sValorMP +
      ' , ValorT=' + sValorT +
      ' WHERE MovimTwn=' + Geral.FF0(Qr0615MovimTwn.Value) +
      ' AND MovimNiv=14 ' +
      ' AND MovimCod=' + sMovimCod +
      '');
   (*
    end else
    begin
      if Qr0615JmpMovID.Value = 34 then
      begin
        //Fazer!
        Geral.MB_Aviso('Parei-Aqui');
      end else
        Geral.MB_Aviso('"JmpMovID" n�o implementado em ' + sProcName);
    *)
    end;
    //
    Qr0615.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(ValorMP) ValorMP, SUM(ValorT) ValorT ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + sMovimCod,
  'AND MovimNiv=14 ',
  '']);
  sValorMP := Geral.FFT_Dot(USQLDB.Flu(DqAux, 'ValorMP'), 4, siPositivo);
  sValorT := Geral.FFT_Dot(USQLDB.Flu(DqAux, 'ValorT'), 4, siPositivo);
  //
  UnDmkDAC_PF.ExecutaDB(DMod.MyDB,
  ' UPDATE vsmovits SET ValorMP=' + sValorMP +
  ', ValorT=' + sValorT +
  ' WHERE MovimNiv=13 AND MovimCod=' + sMovimCod);
    //
end;

procedure TFmVSCorreCustosIMECs.CorrigeVSOperacao(MovimCod: Integer);
var
  NewValorT, ValorT, AllAreaM2, CustoM2, ItsValorT: Double;
  Controle, I, N: Integer;
begin
  N := Length(MovimCods11);
  for I := 0 to N - 1 do
  begin
    if MovimCods11[I] = MovimCod then
    begin
      Break;
      Exit;
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi1, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),//7
  '']);
  QrVmi1.First;
  while not QrVmi1.Eof do
  begin
    AtualizaCustoDeOrigemA(QrVmi1Controle.Value, QrVmi1SrcNivel2.Value,
    QrVmi1AreaM2.Value, QrVmi1ValorT.Value, TSinal.siNegativo);
    //
    QrVmi1.Next;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(ValorT) ValorT, SUM(AreaM2) AreaM2   ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),//7
  '']);
  NewValorT := ABS(QrSum.Fields[0].AsFloat);
  AllAreaM2 := ABS(QrSum.Fields[1].AsFloat);
  if AllAreaM2 <> 0 then
    CustoM2   := NewValorT / AllAreaM2
  else
    CustoM2 := 0.0000;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi2, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmOperInn)),//8
  '']);
  //
  ValorT := ABS(QrVmi2ValorT.Value);
  Controle := QrVmi2Controle.Value;
  if NewValorT > ValorT then
  begin
    //
    UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
    Geral.FFT_Dot(NewValorT, 6, siPositivo) +
    ', Corrigido=1 ' +
    ' WHERE Controle=' + Geral.FF0(Controle));
    FCount := FCount + 1;
  end else
    AlteraParaCorrigido(Controle);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmi3, Dmod.MyDB, [
  'SELECT Codigo, MovimCod, Controle,  ',
  'Pecas, AreaM2, ValorT,  ',
  'SrcMovID, SrcNivel1, ',
  'SrcNivel2, SrcGGX , DstNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  'AND MovimNiv IN (' + Geral.FF0(Integer(eminDestOper)) + //9
  ', ' + Geral.FF0(Integer(eminEmOperBxa)) + ')', //10
  '']);
  while not QrVmi3.Eof  do
  begin
    ItsValorT := Geral.RoundC(CustoM2 * QrVmi3AreaM2.Value, 4);
    Controle := QrVmi3Controle.Value;
    //
    if ABS(ItsValorT) > ABS(QrVmi3ValorT.Value) then
    begin
      UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
      Geral.FFT_Dot(ItsValorT, 6, siPositivo) +
      ', Corrigido=1 ' +
      ' WHERE Controle=' + Geral.FF0(Controle));
      FCount := FCount + 1;
    end else
      AlteraParaCorrigido(Controle);
    QrVmi3.Next;
  end;
  //
  N := Length(MovimCods11);
  SetLength(MovimCods11, N + 1);
  MovimCods11[N] := MovimCod;
end;

procedure TFmVSCorreCustosIMECs.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCorreCustosIMECsControle.Value)
  else
  if Campo = 'SrcNivel2' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCorreCustosIMECsSrcNivel2.Value)
end;

procedure TFmVSCorreCustosIMECs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCorreCustosIMECs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSCorreCustosIMECs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
