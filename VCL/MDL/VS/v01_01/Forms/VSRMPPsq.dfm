object FmVSRMPPsq: TFmVSRMPPsq
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-037 :: Pesquisa de Ficha RMP'
  ClientHeight = 676
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 514
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 514
        Align = alClient
        TabOrder = 0
        object DGDados: TDBGrid
          Left = 2
          Top = 257
          Width = 1004
          Height = 255
          Align = alClient
          DataSource = DsPesq
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DGDadosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 118
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Title.Caption = 'Ficha RMP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'Descri'#231#227'o do n'#237'vel'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo / tamanho / cor'
              Width = 153
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliVenda'
              Title.Caption = 'Cli.Venda'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOKg'
              Title.Caption = 'Custo MO Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOM2'
              Title.Caption = 'Custo MO M2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = '$ Total'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 242
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 808
            Height = 242
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label11: TLabel
              Left = 99
              Top = 16
              Width = 83
              Height = 13
              Caption = 'S'#233'rie Ficha RMP:'
            end
            object Label4: TLabel
              Left = 316
              Top = 16
              Width = 56
              Height = 13
              Caption = 'Ficha RMP:'
            end
            object Label12: TLabel
              Left = 416
              Top = 16
              Width = 33
              Height = 13
              Caption = 'Marca:'
            end
            object Label6: TLabel
              Left = 12
              Top = 16
              Width = 28
              Height = 13
              Caption = 'IME-I:'
            end
            object Label1: TLabel
              Left = 516
              Top = 16
              Width = 29
              Height = 13
              Caption = 'Pallet:'
            end
            object Label2: TLabel
              Left = 12
              Top = 56
              Width = 57
              Height = 13
              Caption = 'Fornecedor:'
            end
            object Label22: TLabel
              Left = 480
              Top = 56
              Width = 135
              Height = 13
              Caption = 'Motorista: (entrada in natura)'
            end
            object Label3: TLabel
              Left = 12
              Top = 96
              Width = 77
              Height = 13
              Caption = 'IME-I de origem:'
            end
            object Label5: TLabel
              Left = 96
              Top = 96
              Width = 80
              Height = 13
              Caption = 'IME-I de destino:'
            end
            object Label7: TLabel
              Left = 180
              Top = 96
              Width = 359
              Height = 13
              Caption = 
                'Observa'#231#245'es (Pode ser parcial. Use % para ignorar partes no meio' +
                ' do texto):'
            end
            object Label8: TLabel
              Left = 12
              Top = 136
              Width = 134
              Height = 13
              Caption = 'Fornecedor de m'#227'o de obra:'
            end
            object Label9: TLabel
              Left = 408
              Top = 136
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object Label10: TLabel
              Left = 632
              Top = 196
              Width = 63
              Height = 13
              Caption = 'N'#250'mero NFe:'
            end
            object Label13: TLabel
              Left = 572
              Top = 96
              Width = 55
              Height = 13
              Caption = 'Data inicial:'
            end
            object Label14: TLabel
              Left = 688
              Top = 96
              Width = 48
              Height = 13
              Caption = 'Data final:'
            end
            object CBSerieFch: TdmkDBLookupComboBox
              Left = 139
              Top = 32
              Width = 169
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVSSerFch
              TabOrder = 2
              dmkEditCB = EdSerieFch
              QryCampo = 'SerieFch'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdSerieFch: TdmkEditCB
              Left = 99
              Top = 32
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'SerieFch'
              UpdCampo = 'SerieFch'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSerieFch
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdFicha: TdmkEdit
              Left = 316
              Top = 32
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pecas'
              UpdCampo = 'Pecas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdMarca: TdmkEdit
              Left = 416
              Top = 32
              Width = 93
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Marca'
              UpdCampo = 'Marca'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdControle: TdmkEdit
              Left = 12
              Top = 32
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 683
              Top = 21
              Width = 120
              Height = 40
              Caption = '&Reabre'
              NumGlyphs = 2
              TabOrder = 21
              OnClick = BtReabreClick
            end
            object EdPallet: TdmkEdit
              Left = 516
              Top = 32
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pecas'
              UpdCampo = 'Pecas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdFornecedor: TdmkEditCB
              Left = 12
              Top = 72
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFornecedor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFornecedor: TdmkDBLookupComboBox
              Left = 68
              Top = 72
              Width = 408
              Height = 21
              DropDownRows = 7
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFornecedor
              TabOrder = 8
              dmkEditCB = EdFornecedor
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMotorista: TdmkEditCB
              Left = 480
              Top = 73
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Motorista'
              UpdCampo = 'Motorista'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMotorista
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMotorista: TdmkDBLookupComboBox
              Left = 536
              Top = 73
              Width = 265
              Height = 21
              DropDownRows = 7
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsMotorista
              TabOrder = 10
              dmkEditCB = EdMotorista
              QryCampo = 'Motorista'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdSrcNivel2: TdmkEdit
              Left = 12
              Top = 112
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdDstNivel2: TdmkEdit
              Left = 96
              Top = 112
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdObserv: TdmkEdit
              Left = 180
              Top = 112
              Width = 389
              Height = 21
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Marca'
              UpdCampo = 'Marca'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdFornecMO: TdmkEditCB
              Left = 12
              Top = 152
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFornecMO
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFornecMO: TdmkDBLookupComboBox
              Left = 68
              Top = 152
              Width = 336
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsPrestador
              TabOrder = 17
              dmkEditCB = EdFornecMO
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCliente: TdmkEditCB
              Left = 408
              Top = 152
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 18
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliente: TdmkDBLookupComboBox
              Left = 464
              Top = 152
              Width = 336
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsCliente
              TabOrder = 19
              dmkEditCB = EdCliente
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CkSdoPositivo: TdmkCheckBox
              Left = 12
              Top = 176
              Width = 165
              Height = 17
              Caption = 'Somente com saldo positivo.'
              TabOrder = 20
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object RGTabela: TRadioGroup
              Left = 612
              Top = 0
              Width = 65
              Height = 53
              Caption = 'Tabela:'
              ItemIndex = 0
              Items.Strings = (
                'Ativa'
                'Morto')
              TabOrder = 6
            end
            object GroupBox2: TGroupBox
              Left = 176
              Top = 176
              Width = 401
              Height = 61
              Caption = ' Pesquisa avan'#231'ada: '
              TabOrder = 22
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 397
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object EdPecas: TdmkEdit
                  Left = 9
                  Top = 20
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Pecas'
                  UpdCampo = 'Pecas'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkPecas: TCheckBox
                  Left = 8
                  Top = 0
                  Width = 89
                  Height = 17
                  Caption = 'Pe'#231'as:'
                  TabOrder = 1
                end
                object CkAreaM2: TCheckBox
                  Left = 104
                  Top = 0
                  Width = 89
                  Height = 17
                  Caption = #193'rea m'#178':'
                  TabOrder = 2
                end
                object EdAreaM2: TdmkEdit
                  Left = 105
                  Top = 20
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Pecas'
                  UpdCampo = 'Pecas'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAreaP2: TdmkEdit
                  Left = 201
                  Top = 20
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Pecas'
                  UpdCampo = 'Pecas'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkAreaP2: TCheckBox
                  Left = 200
                  Top = 0
                  Width = 89
                  Height = 17
                  Caption = #193'rea ft'#178':'
                  TabOrder = 5
                end
                object EdPesoKg: TdmkEdit
                  Left = 297
                  Top = 20
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Pecas'
                  UpdCampo = 'Pecas'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkPesoKg: TCheckBox
                  Left = 296
                  Top = 0
                  Width = 89
                  Height = 17
                  Caption = 'Peso kg:'
                  TabOrder = 7
                end
              end
            end
            object EdNFeNum: TdmkEdit
              Left = 632
              Top = 212
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 25
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pecas'
              UpdCampo = 'Pecas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkNFeSer: TCheckBox
              Left = 580
              Top = 192
              Width = 89
              Height = 17
              Caption = 'S'#233'rie:'
              Enabled = False
              TabOrder = 23
            end
            object EdNFeSer: TdmkEdit
              Left = 581
              Top = 212
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 24
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pecas'
              UpdCampo = 'Pecas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPIni: TdmkEditDateTimePicker
              Left = 572
              Top = 112
              Width = 112
              Height = 21
              Date = 44661.000000000000000000
              Time = 0.317081238426908400
              TabOrder = 14
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPFim: TdmkEditDateTimePicker
              Left = 688
              Top = 112
              Width = 112
              Height = 21
              Date = 44661.000000000000000000
              Time = 0.317081238426908400
              TabOrder = 15
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object DBGMovimNiv: TdmkDBGridZTO
            Left = 808
            Top = 0
            Width = 196
            Height = 242
            Align = alClient
            DataSource = DsMovimNiv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'MovimNiv'
                Title.Caption = 'Cod'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MovimNiv'
                Title.Caption = 'Descri'#231#227'o do n'#237'vel'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtDefIMEI: TBitBtn
        Tag = 104
        Left = 132
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Copiar IMEI'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDefIMEIClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 252
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 8
    Top = 460
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 8
    Top = 412
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT ABS(vmi.Pecas) Pecas, ABS(vmi.PesoKg) PesoKg,'
      'ABS(vmi.AreaM2) AreaM2, ABS(vmi.AreaP2) AreaP2,'
      'ABS(vmi.ValorT) ValorT,'
      'CONCAT(gg1.Nome,'
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),'
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome' +
        ')))'
      'NO_PRD_TAM_COR,'
      ''
      'IF(vmi.Terceiro <> 0,'
      '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome),'
      '  mfc.Nome) NO_FORNECE,'
      ''
      'vmi.Codigo, vmi.MovimCod, vmi.Controle, vsf.Nome NO_SerieFch,'
      'vmi.Ficha, vmi.Marca, vmi.SerieFch,'
      'vmi.DataHora, vmi.CliVenda, vmi.CustoMOKg, vmi.CustoMOM2,'
      
        ' ELT(MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destino cl' +
        'assifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de arti' +
        'go","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Origem o' +
        'pera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino oper' +
        'a'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa em ' +
        'pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo Ger' +
        'ado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Artigo' +
        ' In Natura","Artigo In Natura","Artigo Gerado","Artigo Classific' +
        'ado","Artigo em Opera'#231#227'o","Origem semi acabado em processo","Sem' +
        'i acabado em processo","Destino semi acabado em processo","Baixa' +
        ' de semi acabado em processo","Artigo Semi Acabado","Artigo Acab' +
        'ado","Sub produto","Origem de transf. de local","Destino de tran' +
        'sf. de local","Origem caleiro em processo","Caleiro em processo"' +
        ',"Destino caleiro em processo","Baixa de caleiro em processo","A' +
        'rtigo de caleiro","Origem curtimento em processo","Curtimento em' +
        ' processo","Destino curtimento em processo","Baixa de curtimento' +
        ' em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o","P' +
        'DA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em opera' +
        #231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o","De' +
        'stino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de DTA' +
        '","Origem PSP em processo","PSP processo","Destino PSP em proces' +
        'so","Baixa de PSP em processo","Artigo de PSP","Origem RRM em re' +
        'processo","RRM reprocesso","Destino RRM em reprocesso","Baixa de' +
        ' RRM em reprocesso","Artigo de Reprocesso / reparo","Baixa de in' +
        'sumo em mistura","Gera'#231#227'o de insumo em mistura","Origem industri' +
        'aliza'#231#227'o","Em industrializa'#231#227'o","Destino industrializa'#231#227'o","Baix' +
        'a de industrializa'#231#227'o","Origem in natura em conserva'#231#227'o","In nat' +
        'ura em conserva'#231#227'o","Destino in natura em conserva'#231#227'o","Baixa de' +
        ' in natura em conserva'#231#227'o","Couro conservado") NO_MovimNiv'
      'FROM vsmovits vmi'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      ''
      'LEFT JOIN entidades   frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab'
      ''
      'WHERE vmi.MovimID=2'
      'AND vmi.CliVenda=110'
      'ORDER BY vmi.Controle DESC'
      '')
    Left = 80
    Top = 412
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrPesqFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrPesqMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrPesqSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrPesqNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrPesqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPesqNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrPesqMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrPesqCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrPesqCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrPesqCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPesqPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrPesqPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrPesqAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrPesqAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrPesqValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 80
    Top = 460
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 156
    Top = 412
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 156
    Top = 460
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 232
    Top = 412
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 232
    Top = 460
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 356
    Top = 412
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 356
    Top = 460
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 296
    Top = 412
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 296
    Top = 460
  end
  object QrMovimNiv: TMySQLQuery
    Database = Dmod.MyDB
    Left = 428
    Top = 412
    object QrMovimNivNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrMovimNivMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
    end
  end
  object DsMovimNiv: TDataSource
    DataSet = QrMovimNiv
    Left = 428
    Top = 460
  end
  object frxWET_CURTI_037: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_037GetValue
    Left = 76
    Top = 320
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692940240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Pesquisa de IME-Is')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 41.574830000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000400000
          Width = 302.362204720000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Marca=Marca'
      'SerieFch=SerieFch'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_FORNECE=NO_FORNECE'
      'MovimCod=MovimCod'
      'CliVenda=CliVenda'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'DataHora=DataHora'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT')
    DataSet = QrPesq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 80
    Top = 364
  end
end
