unit VSExcItsIMEIs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Menus, dmkEditDateTimePicker, dmkEdit, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSExcItsIMEIs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGruY: TmySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    DsGraGruY: TDataSource;
    QrIMEIs: TmySQLQuery;
    DsIMEIs: TDataSource;
    DBGIMEIs: TdmkDBGridZTO;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsCodigo: TIntegerField;
    QrIMEIsControle: TIntegerField;
    QrIMEIsMovimCod: TIntegerField;
    QrIMEIsMovimNiv: TIntegerField;
    QrIMEIsMovimTwn: TIntegerField;
    QrIMEIsEmpresa: TIntegerField;
    QrIMEIsTerceiro: TIntegerField;
    QrIMEIsCliVenda: TIntegerField;
    QrIMEIsMovimID: TIntegerField;
    QrIMEIsLnkNivXtr1: TIntegerField;
    QrIMEIsLnkNivXtr2: TIntegerField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TIntegerField;
    QrIMEIsGraGruX: TIntegerField;
    QrIMEIsPecas: TFloatField;
    QrIMEIsPesoKg: TFloatField;
    QrIMEIsAreaM2: TFloatField;
    QrIMEIsAreaP2: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TIntegerField;
    QrIMEIsSrcNivel1: TIntegerField;
    QrIMEIsSrcNivel2: TIntegerField;
    QrIMEIsSrcGGX: TIntegerField;
    QrIMEIsSdoVrtPeca: TFloatField;
    QrIMEIsSdoVrtPeso: TFloatField;
    QrIMEIsSdoVrtArM2: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsSerieFch: TIntegerField;
    QrIMEIsFicha: TIntegerField;
    QrIMEIsMisturou: TSmallintField;
    QrIMEIsFornecMO: TIntegerField;
    QrIMEIsCustoMOKg: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TIntegerField;
    QrIMEIsDstNivel1: TIntegerField;
    QrIMEIsDstNivel2: TIntegerField;
    QrIMEIsDstGGX: TIntegerField;
    QrIMEIsQtdGerPeca: TFloatField;
    QrIMEIsQtdGerPeso: TFloatField;
    QrIMEIsQtdGerArM2: TFloatField;
    QrIMEIsQtdGerArP2: TFloatField;
    QrIMEIsQtdAntPeca: TFloatField;
    QrIMEIsQtdAntPeso: TFloatField;
    QrIMEIsQtdAntArM2: TFloatField;
    QrIMEIsQtdAntArP2: TFloatField;
    QrIMEIsAptoUso: TSmallintField;
    QrIMEIsNotaMPAG: TFloatField;
    QrIMEIsMarca: TWideStringField;
    QrIMEIsLk: TIntegerField;
    QrIMEIsDataCad: TDateField;
    QrIMEIsDataAlt: TDateField;
    QrIMEIsUserCad: TIntegerField;
    QrIMEIsUserAlt: TIntegerField;
    QrIMEIsAlterWeb: TSmallintField;
    QrIMEIsAtivo: TSmallintField;
    QrIMEIsTpCalcAuto: TIntegerField;
    QrIMEIsGraGruY: TIntegerField;
    QrIMEIsMediaSdoVrtArM2: TFloatField;
    QrIMEIsFaixaMediaM2: TFloatField;
    Panel5: TPanel;
    DBGGraGruY: TdmkDBGridZTO;
    BtNenhumIMEI: TBitBtn;
    BtTodosIMEIS: TBitBtn;
    BtProvisorio: TBitBtn;
    PMProvisorio: TPopupMenu;
    ObterIMEIs11: TMenuItem;
    Selecionarimeisfixos1: TMenuItem;
    Panel7: TPanel;
    CkContinuar: TCheckBox;
    RGDataHora: TRadioGroup;
    TPDataSenha: TdmkEditDateTimePicker;
    EdHoraSenha: TdmkEdit;
    Label2: TLabel;
    QrLastMov: TmySQLQuery;
    QrLastMovDataHora: TDateTimeField;
    Label1: TLabel;
    EdSenha: TEdit;
    QrIMEIsVSMulFrnCab: TIntegerField;
    QrIMEIsClientMO: TIntegerField;
    QrIMEIsStqCenLoc: TIntegerField;
    Panel6: TPanel;
    BtTodosGGY: TBitBtn;
    BtNenhumGGY: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGGraGruYAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure BtOKClick(Sender: TObject);
    procedure DBGIMEIsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure BtTodosIMEISClick(Sender: TObject);
    procedure BtNenhumIMEIClick(Sender: TObject);
    procedure ObterIMEIs11Click(Sender: TObject);
    procedure BtProvisorioClick(Sender: TObject);
    procedure Selecionarimeisfixos1Click(Sender: TObject);
    procedure RGDataHoraClick(Sender: TObject);
    procedure BtTodosGGYClick(Sender: TObject);
    procedure BtNenhumGGYClick(Sender: TObject);
    procedure QrIMEIsBeforeClose(DataSet: TDataSet);
    procedure QrIMEIsAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FBalDtHr: String;
    //
    procedure BaixaIMEIAtual();
    procedure ReopenIMEIs();
    procedure SetaTodosIMEIS(Ativo: Boolean);
    procedure SetaTodosGGY(Ativo: Boolean);
  public
    { Public declarations }
    FDataHora: TDateTime;
    FControle: Integer;
    FCodigo, FMovimCod, FEmpresa: Integer;
  end;

  var
  FmVSExcItsIMEIs: TFmVSExcItsIMEIs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UMySQLModule,
  UnVS_CRC_PF, MyDBCheck, StqCenLoc;

{$R *.DFM}

procedure TFmVSExcItsIMEIs.BaixaIMEIAtual();
var
  BalDtHr: String;
  //
  function DefineLastDtHr(DataHora: TDateTime): String;
  begin
    if VS_PF.ImpedePeloBalanco(DataHora, False, False) then
      Result := FBalDtHr
    else
      Result := Geral.FDT(DataHora, 109);
  end;
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe     = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieFch,
  Ficha, SrcNivel1, SrcNivel2, (*Misturou,*) GraGruY, SrcGGX,
  VSMulFrnCab, ClientMO, FornecMO, StqCenLoc: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
begin
  SrcMovID       := TEstqmovimID(QrIMEIsMovimID.Value);
  SrcNivel1      := QrIMEIsCodigo.Value;
  SrcNivel2      := QrIMEIsControle.Value;
  SrcGGX         := QrIMEIsGraGruX.Value;
  //
  Codigo         := FCodigo;
  Controle       := 0;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  ClientMO       := QrIMEIsClientMO.Value;
  FornecMO       := QrIMEIsFornecMO.Value;
  StqCenLoc      := QrIMEIsStqCenLoc.Value;
  Terceiro       := QrIMEIsTerceiro.Value;
  VSMulFrnCab    := QrIMEIsVSMulFrnCab.Value;
  //DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidEntraExced;
  MovimNiv       := eminSemNiv;
  Pallet         := QrIMEIsPallet.Value;
  GraGruX        := QrIMEIsGraGruX.Value;
  Pecas          := -QrIMEIsSdoVrtPeca.Value;
  PesoKg         := -QrIMEIsSdoVrtPeso.Value;
  AreaM2         := -QrIMEIsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  if (AreaM2 <> 0) and (QrIMEIsAreaM2.Value <> 0) then
    ValorT := AreaM2 / QrIMEIsAreaM2.Value * QrIMEIsValorT.Value
  else
  if (PesoKg <> 0) and (QrIMEIsPesoKg.Value <> 0) then
    ValorT := PesoKg / QrIMEIsPesoKg.Value * QrIMEIsValorT.Value
  else
    ValorT         := 0.00;
  Observ         := '';
  //
  SerieFch       := QrIMEIsSerieFch.Value;
  Ficha          := QrIMEIsFicha.Value;
  Marca          := QrIMEIsMarca.Value;
  //Misturou       := QrIMEIsMisturou.Value;
  //
  GraGruY        := QrIMEIsGraGruY.Value;
  //
(*
  ExigeAreaouPeca :=
    (QrIMEIsSdoVrtPeso.Value > 0) or (QrIMEIsSdoVrtArM2.Value > 0);
  //
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if not VS_PF.ObtemControleIMEI(ImgTipo.SQLType, Controle, EdSenha.Text) then
  begin
    Close;
    Exit;
  end;
  case RGDataHora.ItemIndex of
    1: DataHora := Geral.FDT(FDataHora, 109);
    2: DataHora := Geral.FDT(TPDataSenha.Date, 1) + ' ' + EdHoraSenha.Text;
    3:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLastMov, Dmod.MyDB, [
      'SELECT MAX(DataHora) DataHora ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE SrcNivel2=' + Geral.FF0(QrIMEIsControle.Value),
      '']);
      DataHora := DefineLastDtHr(QrLastMovDataHora.Value);
    end;
    else
    begin
      Geral.MB_Erro('"Fonte de data / hora" n�o implementada!');
      DataHora := Geral.FDT(DmodG.ObtemAgora(), 109);
    end;
  end;
(*
  if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
*)
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab,
  ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei115(*Entrada excedente de couro*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FControle := Controle;
  end;
end;

procedure TFmVSExcItsIMEIs.BtNenhumIMEIClick(Sender: TObject);
begin
  SetaTodosIMEIs(False);
end;

procedure TFmVSExcItsIMEIs.BtNenhumGGYClick(Sender: TObject);
begin
  SetaTodosGGY(False);
end;

procedure TFmVSExcItsIMEIs.BtOKClick(Sender: TObject);
var
  I: Integer;
  Senha: String;
begin
  if MyObjects.FIC(RGDataHora.ItemIndex = 0, RGDataHora,
    'Informe a " Fonte da data / hora de baixa"!') then
      Exit;
  Senha := EdSenha.Text;
  if Trim(Senha) <> '' then
  begin
    if Senha <> CO_MASTER then
    begin
      Geral.MB_Aviso('Senha invalida!');
      Halt(0);
    end;
  end;
  if VS_PF.ImpedePeloBalanco(Now(), False, False) then
    FBalDtHr := Geral.FDT(VS_PF.ObtemDataBalanco(FEmpresa), 109)
  else
    FBalDtHr := Geral.FDT(Now(), 109);
  if (DBGIMEIs.SelectedRows.Count > 0) then
  begin
    try
      for I := 0 to DBGIMEIs.SelectedRows.Count - 1 do
      begin
        //QrIMEIs.GotoBookmark(pointer(DBGIMEIs.SelectedRows.Items[I]));
        QrIMEIs.GotoBookmark(DBGIMEIs.SelectedRows.Items[I]);
        //
        BaixaIMEIAtual();
      end;
    finally
      ReopenIMEIs();
    end;
    if not CkContinuar.Checked then
      Close;
  end else
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!');
end;

procedure TFmVSExcItsIMEIs.BtProvisorioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisorio, BtProvisorio);
end;

procedure TFmVSExcItsIMEIs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSExcItsIMEIs.BtTodosIMEISClick(Sender: TObject);
begin
  SetaTodosIMEIS(True);
end;

procedure TFmVSExcItsIMEIs.BtTodosGGYClick(Sender: TObject);
begin
  SetaTodosGGY(True);
end;

procedure TFmVSExcItsIMEIs.DBGGraGruYAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  ReopenIMEIs();
end;

procedure TFmVSExcItsIMEIs.DBGIMEIsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
var
  CorTexto, CorFundo: TColor;
  Texto: String;
begin
  // Problemas de desenha
(*
  if (Column.FieldName = 'MediaSdoVrtArM2') then
  begin
    if QrIMEIsMediaSdoVrtArM2.Value <= 0 then
    begin
      Cor := clRed;
      Font.Style := [fsBold];
    end else
    begin
      Cor := clBlack;
      Font.Style := [];
    end;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, cor, clWindow,
      Column.Alignment, Column.Field.DisplayText);
  end
  else
*)
(*
  if (Column.FieldName = 'SdoVrtArM2') then
  begin
    if QrIMEIsSdoVrtArM2.Value <= 0 then
    begin
      Cor := clRed;
      Font.Style := [fsBold];
    end else
    begin
      Cor := clBlack;
      Font.Style := [];
    end;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, cor, clWindow,
      Column.Alignment, Column.Field.DisplayText);
  end else
*)
  if Column.FieldName = 'FaixaMediaM2' then
  begin
    VS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(QrIMEIsFaixaMediaM2.Value,
    CorFundo, CorTexto, Texto);
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, CorTexto, CorFundo,
      Column.Alignment, Texto(*Column.Field.DisplayText*));
  end;
end;

procedure TFmVSExcItsIMEIs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSExcItsIMEIs.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  FControle := 0;
  Agora := DModG.ObtemAgora();
  TPDataSenha.Date := Agora;
  //EdHoraSenha.ValueVariant := Agora;
end;

procedure TFmVSExcItsIMEIs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSExcItsIMEIs.ObterIMEIs11Click(Sender: TObject);
var
  IMEIs: String;
  Step: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  IMEIs := '';
  Step := 0;
  QrIMEIS.First;
  while not QrIMEIS.Eof do
  begin
    IMEIs := IMEIs + Geral.FF0(QrIMEIsControle.Value) + ', ';
    Step := Step + 1;
    if Step = 8 then
    begin
      Step := 0;
      IMEIs := IMEIs + sLineBreak;
    end;
    //
    QrIMEIs.Next;
  end;
  Geral.MB_Info(IMEIs);
end;

procedure TFmVSExcItsIMEIs.QrIMEIsAfterOpen(DataSet: TDataSet);
begin
  BtTodosIMEIs.Enabled := QrIMEIs.RecordCount > 0;
  BtNenhumIMEI.Enabled := QrIMEIs.RecordCount > 0;
end;

procedure TFmVSExcItsIMEIs.QrIMEIsBeforeClose(DataSet: TDataSet);
begin
  BtTodosIMEIs.Enabled := False;
  BtNenhumIMEI.Enabled := False;
end;

procedure TFmVSExcItsIMEIs.RGDataHoraClick(Sender: TObject);
begin
//
end;

procedure TFmVSExcItsIMEIs.ReopenIMEIs();
var
  I: Integer;
  GraGruYs, SQL_GraGruY: String;
begin
  GraGruYs := '';
  if (DBGGraGruY.SelectedRows.Count > 0) and
  (DBGGraGruY.SelectedRows.Count < QrGraGruY.RecordCount) then
  begin
    for I := 0 to DBGGraGruY.SelectedRows.Count - 1 do
    begin
      //QrGraGruY.GotoBookmark(pointer(DBGGraGruY.SelectedRows.Items[I]));
      QrGraGruY.GotoBookmark(DBGGraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(QrGraGruYCodigo.Value);
    end;
    SQL_GraGruY := 'AND ggx.GraGruY IN (' + GraGruYs + ') ';
  end
  else
    SQL_GraGruY := EmptyStr;
  //
  if Trim(GraGruYs) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
    'SELECT vmi.SdoVrtArM2 / vmi.SdoVrtPeca  MediaSdoVrtArM2, ',
    'CASE ',
    'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca < cou.MediaMinM2 THEN 0.000 ',
    'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca > cou.MediaMaxM2 THEN 2.000 ',
    'ELSE 1.000 END FaixaMediaM2, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ggx.GraGruY, vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'WHERE vmi.Empresa=' + Geral.FF0(FEmpresa),
    'AND ',
    '  (vmi.SdoVrtPeca<0 ',
    '  OR ',
    '  ( ',
    '   vmi.SdoVrtPeca=0 ',
    '   AND ',
    '    ( ',
    '     vmi.SdoVrtPeso <> 0 ',
    '     OR ',
    '     vmi.SdoVrtArM2 <> 0 ',
    '    ) ',
    '  )) ',
    SQL_GraGruY,
    'ORDER BY vmi.Controle ',
    '']);
    //Geral.MB_Teste(QrIMEIs.SQL.Text);
  end else
    QrIMEIs.Close;
end;

procedure TFmVSExcItsIMEIs.RGPosiNegaClick(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSExcItsIMEIs.Selecionarimeisfixos1Click(Sender: TObject);
begin
(* Corre��o pontual no Cialeather!
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
    UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
    'SELECT vmi.SdoVrtArM2 / vmi.SdoVrtPeca  MediaSdoVrtArM2, ',
    'CASE ',
    'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca < cou.MediaMinM2 THEN 0.000 ',
    'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca > cou.MediaMaxM2 THEN 2.000 ',
    'ELSE 1.000 END FaixaMediaM2, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ggx.GraGruY, vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'WHERE vmi.Controle IN ',
    '( ',
    '3197, 3205, 3206, 3230, 3242, 3248, 3250, 3251,  ',
    '3267, 3277, 3279, 3397, 3438, 3450, 3457, 3483,  ',
    '3555, 3564, 3570, 3609, 3630, 3647, 3650, 3675,  ',
    '3678, 3729, 3742, 3824, 3829, 3830, 3899, 3921,  ',
    '4066, 4069, 4115, 4116, 4128, 4137, 4150, 4199,  ',
    '4202, 4207, 4316, 4322, 4356, 4380, 4401, 4439,  ',
    '4519, 4520, 4521, 4522, 4767, 4900, 4933, 4934,  ',
    '4967, 4973, 4989, 5256, 5298, 5408, 5488, 5608,  ',
    '5619, 5631, 5709, 5761, 5780, 5836, 5851, 5858,  ',
    '5881, 5934, 5954, 6032, 6043, 6078, 6126, 6206,  ',
    '6253, 6265, 6301, 6682, 6715, 6939, 6982, 7305,  ',
    '7321, 7351, 7420, 7443, 7462, 7464, 7488, 7490,  ',
    '7491, 7511, 7540, 7551, 7561, 7617, 7673, 7674,  ',
    '7755, 7787, 8235, 8261, 8265, 8278, 8312, 8351,  ',
    '8374, 8542, 8552, 8557, 8618, 8670, 8717, 8742,  ',
    '8764, 8860) ',
    'ORDER BY vmi.Controle ',
    '']);
*)
end;

procedure TFmVSExcItsIMEIs.SetaTodosGGY(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBGGraGruY))
  else
    DBGGraGruY.SelectedRows.Clear;
  ReopenIMEIs();
end;

procedure TFmVSExcItsIMEIs.SetaTodosIMEIs(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBGIMEIs))
  else
    DBGIMEIs.SelectedRows.Clear;
end;

end.
