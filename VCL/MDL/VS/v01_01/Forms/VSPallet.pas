unit VSPallet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet,
  Vcl.ComCtrls, dmkEditDateTimePicker, System.Variants, dmkDBGridZTO,
  UnProjGroup_Consts, UnVS_Tabs, UnGrl_Consts, UnProjGroup_Vars, UnAppEnums;

type
  TFmVSPallet = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrVSPallet: TmySQLQuery;
    DsVSPallet: TDataSource;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIMEI: TBitBtn;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCliStat: TdmkEditCB;
    CBCliStat: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrVSPalSta: TmySQLQuery;
    DsVSPalSta: TDataSource;
    QrVSPalStaNome: TWideStringField;
    QrVSPalStaCodigo: TIntegerField;
    SBCliente: TSpeedButton;
    LaVSRibCla: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    Label4: TLabel;
    Label12: TLabel;
    TPDtHrEndAdd: TdmkEditDateTimePicker;
    EdDtHrEndAdd: TdmkEdit;
    QrVSPalletDtHrEndAdd: TDateTimeField;
    QrVSPalletDtHrEndAdd_TXT: TWideStringField;
    QrSumPall: TmySQLQuery;
    QrIMEIs: TmySQLQuery;
    DsSumPall: TDataSource;
    DsIMEIs: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrSumPallGraGruX: TIntegerField;
    QrSumPallPecas: TFloatField;
    QrSumPallPesoKg: TFloatField;
    QrSumPallAreaM2: TFloatField;
    QrSumPallAreaP2: TFloatField;
    QrSumPallValorT: TFloatField;
    QrSumPallSdoVrtPeca: TFloatField;
    QrSumPallSdoVrtPeso: TFloatField;
    QrSumPallSdoVrtArM2: TFloatField;
    QrSumPallGraGru1: TIntegerField;
    QrSumPallNO_PRD_TAM_COR: TWideStringField;
    QrSumPallDataHora: TDateTimeField;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    QrVSCacItsAOpn: TmySQLQuery;
    QrVSCacItsAOpnCacCod: TIntegerField;
    QrVSCacItsAOpnCacID: TIntegerField;
    QrVSCacItsAOpnCodigo: TIntegerField;
    QrVSCacItsAOpnControle: TLargeintField;
    QrVSCacItsAOpnClaAPalOri: TIntegerField;
    QrVSCacItsAOpnRclAPalOri: TIntegerField;
    QrVSCacItsAOpnVSPaClaIts: TIntegerField;
    QrVSCacItsAOpnVSPaRclIts: TIntegerField;
    QrVSCacItsAOpnVSPallet: TIntegerField;
    QrVSCacItsAOpnVMI_Sorc: TIntegerField;
    QrVSCacItsAOpnVMI_Dest: TIntegerField;
    QrVSCacItsAOpnBox: TIntegerField;
    QrVSCacItsAOpnPecas: TFloatField;
    QrVSCacItsAOpnAreaM2: TFloatField;
    QrVSCacItsAOpnAreaP2: TFloatField;
    QrVSCacItsAOpnRevisor: TIntegerField;
    QrVSCacItsAOpnDigitador: TIntegerField;
    QrVSCacItsAOpnDataHora: TDateTimeField;
    QrVSCacItsAOpnAlterWeb: TSmallintField;
    QrVSCacItsAOpnAtivo: TSmallintField;
    QrVSCacItsAOpnSumido: TSmallintField;
    QrVSCacItsAOpnRclAPalDst: TIntegerField;
    DsVSCacItsAOpn: TDataSource;
    DBGrid2: TDBGrid;
    N1: TMenuItem;
    Encerraclassificao1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    Label23: TLabel;
    EdQtdPrevPc: TdmkEdit;
    Label24: TLabel;
    QrVSPalletQtdPrevPc: TIntegerField;
    QrVSPalletGerRclCab: TIntegerField;
    QrVSPalletDtHrFimRcl: TDateTimeField;
    QrVSPalletMovimIDGer: TIntegerField;
    N2: TMenuItem;
    Atualizasaldo1: TMenuItem;
    DBGVSPaIts: TdmkDBGridZTO;
    QrVSPaIts: TmySQLQuery;
    DsVSPaIts: TDataSource;
    QrVSPaItsVSPallet: TIntegerField;
    QrVSPaItsVSPaClaIts: TIntegerField;
    QrVSPaItsVSPaRclIts: TIntegerField;
    QrVSPaItsVMI_Sorc: TIntegerField;
    QrVSPaItsVMI_Dest: TIntegerField;
    QrVSPaItsPecas: TFloatField;
    QrVSPaItsAreaM2: TFloatField;
    QrVSPaItens: TmySQLQuery;
    DsVSPaItens: TDataSource;
    QrVSPaItensCodigo: TIntegerField;
    QrVSPaItensControle: TIntegerField;
    QrVSPaItensVSPallet: TIntegerField;
    QrVSPaItensVMI_Sorc: TIntegerField;
    QrVSPaItensVMI_Baix: TIntegerField;
    QrVSPaItensTecla: TIntegerField;
    QrVSPaItensDtHrIni: TDateTimeField;
    QrVSPaItensDtHrFim: TDateTimeField;
    QrVSPaItensQIt_Sorc: TIntegerField;
    QrVSPaItensQIt_Baix: TIntegerField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSPaItensVMI_Dest: TIntegerField;
    PMIMEI: TPopupMenu;
    Irparajaneladomovimento1: TMenuItem;
    IrparajaneladedadosdoIMEI1: TMenuItem;
    N3: TMenuItem;
    ExcluiIMEIselecionado1: TMenuItem;
    TabSheet5: TTabSheet;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    GroupBox2: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrOriRcl: TmySQLQuery;
    GroupBox3: TGroupBox;
    dmkDBGridZTO3: TdmkDBGridZTO;
    Splitter2: TSplitter;
    GroupBox4: TGroupBox;
    dmkDBGridZTO4: TdmkDBGridZTO;
    DsOriRcl: TDataSource;
    QrOriCla: TmySQLQuery;
    DsOriCla: TDataSource;
    QrDestino: TmySQLQuery;
    DsDestino: TDataSource;
    Panel6: TPanel;
    Label25: TLabel;
    EdDestino: TdmkEdit;
    Panel7: TPanel;
    Label26: TLabel;
    EdOriCla: TdmkEdit;
    Panel8: TPanel;
    Label27: TLabel;
    EdOriRcl: TdmkEdit;
    QrPallDst: TmySQLQuery;
    QrPallDstVSPallet: TIntegerField;
    QrPallDstPecas: TFloatField;
    QrPallDstVMI_Dest: TIntegerField;
    QrPallDstDtHrEndAdd: TDateTimeField;
    QrPallDstDtHrEndAdd_TXT: TWideStringField;
    DsPallDst: TDataSource;
    QrPallOri: TmySQLQuery;
    DsPallOri: TDataSource;
    QrPallOriClaRcl: TFloatField;
    QrPallOriNO_ClaRcl: TWideStringField;
    QrPallOriCacCod: TIntegerField;
    QrPallOriCodigo: TIntegerField;
    QrPallOriPecas: TFloatField;
    QrPallOriVSMovIts: TIntegerField;
    QrPallOriVSPallet: TFloatField;
    QrPallOriDtHrFimCla: TDateTimeField;
    QrPallOriDtHrFimCla_TXT: TWideStringField;
    GroupBox5: TGroupBox;
    dmkDBGridZTO5: TdmkDBGridZTO;
    GroupBox6: TGroupBox;
    dmkDBGridZTO6: TdmkDBGridZTO;
    AlteraIMEIorigemdoIMEISelecionado1: TMenuItem;
    EncerraPallet1: TMenuItem;
    Atualizasaldo2: TMenuItem;
    AtualizaStatus1: TMenuItem;
    QrVSCacItsAOpnVMI_Baix: TIntegerField;
    QrVSCacItsAOpnMartelo: TIntegerField;
    QrVSCacItsAOpnFrmaIns: TSmallintField;
    QrVSCacItsAOpnSubClass: TWideStringField;
    RecalculaIMEIsdeclassereclasse1: TMenuItem;
    IMEIsdegerao1: TMenuItem;
    PMCacIts: TPopupMenu;
    Alteradadosdiitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    PMImprime: TPopupMenu;
    FichaCOMonomedoPallet1: TMenuItem;
    FichaSEMonomedoPallet1: TMenuItem;
    AlteraFornecedor1: TMenuItem;
    QrIMEIsCodigo: TLargeintField;
    QrIMEIsControle: TLargeintField;
    QrIMEIsMovimCod: TLargeintField;
    QrIMEIsMovimNiv: TLargeintField;
    QrIMEIsMovimTwn: TLargeintField;
    QrIMEIsEmpresa: TLargeintField;
    QrIMEIsTerceiro: TLargeintField;
    QrIMEIsCliVenda: TLargeintField;
    QrIMEIsMovimID: TLargeintField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TLargeintField;
    QrIMEIsGraGruX: TLargeintField;
    QrIMEIsPecas: TFloatField;
    QrIMEIsPesoKg: TFloatField;
    QrIMEIsAreaM2: TFloatField;
    QrIMEIsAreaP2: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TLargeintField;
    QrIMEIsSrcNivel1: TLargeintField;
    QrIMEIsSrcNivel2: TLargeintField;
    QrIMEIsSrcGGX: TLargeintField;
    QrIMEIsSdoVrtPeca: TFloatField;
    QrIMEIsSdoVrtPeso: TFloatField;
    QrIMEIsSdoVrtArM2: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsSerieFch: TLargeintField;
    QrIMEIsFicha: TLargeintField;
    QrIMEIsMisturou: TLargeintField;
    QrIMEIsFornecMO: TLargeintField;
    QrIMEIsCustoMOKg: TFloatField;
    QrIMEIsCustoMOM2: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TLargeintField;
    QrIMEIsDstNivel1: TLargeintField;
    QrIMEIsDstNivel2: TLargeintField;
    QrIMEIsDstGGX: TLargeintField;
    QrIMEIsQtdGerPeca: TFloatField;
    QrIMEIsQtdGerPeso: TFloatField;
    QrIMEIsQtdGerArM2: TFloatField;
    QrIMEIsQtdGerArP2: TFloatField;
    QrIMEIsQtdAntPeca: TFloatField;
    QrIMEIsQtdAntPeso: TFloatField;
    QrIMEIsQtdAntArM2: TFloatField;
    QrIMEIsQtdAntArP2: TFloatField;
    QrIMEIsNotaMPAG: TFloatField;
    QrIMEIsStqCenLoc: TLargeintField;
    QrIMEIsReqMovEstq: TLargeintField;
    QrIMEIsGraGru1: TLargeintField;
    QrIMEIsNO_EstqMovimID: TWideStringField;
    QrIMEIsNO_MovimID: TWideStringField;
    QrIMEIsNO_MovimNiv: TWideStringField;
    QrIMEIsNO_PALLET: TWideStringField;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsNO_FORNECE: TWideStringField;
    QrIMEIsNO_SerieFch: TWideStringField;
    QrIMEIsNO_TTW: TWideStringField;
    QrIMEIsID_TTW: TLargeintField;
    QrIMEIsNO_LOC_CEN: TWideStringField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrPallOriSerieFch: TIntegerField;
    QrPallOriFicha: TIntegerField;
    QrPallOriNO_SerieFch: TWideStringField;
    QrVSPalletClientMO: TIntegerField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    udo1: TMenuItem;
    ApenasaDescrio1: TMenuItem;
    frxDsVSCacItsAOpnImp: TfrxDBDataset;
    frxWET_CURTI_013_A: TfrxReport;
    N4: TMenuItem;
    Itensnopallet1: TMenuItem;
    frxDsVSPallet: TfrxDBDataset;
    QrVSCacItsAOpnImp: TmySQLQuery;
    QrVSCacItsAOpnImpCacCod: TIntegerField;
    QrVSCacItsAOpnImpCacID: TIntegerField;
    QrVSCacItsAOpnImpCodigo: TIntegerField;
    QrVSCacItsAOpnImpControle: TLargeintField;
    QrVSCacItsAOpnImpClaAPalOri: TIntegerField;
    QrVSCacItsAOpnImpRclAPalOri: TIntegerField;
    QrVSCacItsAOpnImpVSPaClaIts: TIntegerField;
    QrVSCacItsAOpnImpVSPaRclIts: TIntegerField;
    QrVSCacItsAOpnImpVSPallet: TIntegerField;
    QrVSCacItsAOpnImpVMI_Sorc: TIntegerField;
    QrVSCacItsAOpnImpVMI_Dest: TIntegerField;
    QrVSCacItsAOpnImpBox: TIntegerField;
    QrVSCacItsAOpnImpPecas: TFloatField;
    QrVSCacItsAOpnImpAreaM2: TFloatField;
    QrVSCacItsAOpnImpAreaP2: TFloatField;
    QrVSCacItsAOpnImpRevisor: TIntegerField;
    QrVSCacItsAOpnImpDigitador: TIntegerField;
    QrVSCacItsAOpnImpDataHora: TDateTimeField;
    QrVSCacItsAOpnImpAlterWeb: TSmallintField;
    QrVSCacItsAOpnImpAtivo: TSmallintField;
    QrVSCacItsAOpnImpSumido: TSmallintField;
    QrVSCacItsAOpnImpRclAPalDst: TIntegerField;
    QrVSCacItsAOpnImpVMI_Baix: TIntegerField;
    QrVSCacItsAOpnImpMartelo: TIntegerField;
    QrVSCacItsAOpnImpFrmaIns: TSmallintField;
    QrVSCacItsAOpnImpSubClass: TWideStringField;
    QrVSCacItsAOpnImpNoRev: TWideStringField;
    QrVSCacItsAOpnImpNoDig: TWideStringField;
    IncluiManualmente1: TMenuItem;
    Qr1: TmySQLQuery;
    QrIMEIsCustoM2: TFloatField;
    N5: TMenuItem;
    Mudademeiosparainteiro1: TMenuItem;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXUsaSifDipoa: TSmallintField;
    QrVSPalletStatPall: TIntegerField;
    QrVSPalletSeqSifDipoa: TIntegerField;
    QrVSPalletObsSifDipoa: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Panel10: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    GroupBox8: TGroupBox;
    Panel11: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    DBEdit20: TDBEdit;
    DBEdit22: TDBEdit;
    PMNumero: TPopupMenu;
    NdoPallet1: TMenuItem;
    NdeidentificaodoSIFDIPOA1: TMenuItem;
    GroupBox7: TGroupBox;
    Panel9: TPanel;
    LaSeqSifDipoa: TLabel;
    LaObsSifDipoa: TLabel;
    EdSeqSifDipoa: TdmkEdit;
    EdObsSifDipoa: TdmkEdit;
    LaDataFab: TLabel;
    TPDataFab: TdmkEditDateTimePicker;
    QrVSPalletDataFab: TDateField;
    DBEdit21: TDBEdit;
    Label30: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPalletAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPalletBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPalletAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtIMEIClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrVSPalletBeforeClose(DataSet: TDataSet);
    procedure QrVSPalArtBeforeClose(DataSet: TDataSet);
    procedure QrVSPalArtAfterScroll(DataSet: TDataSet);
    procedure SBClienteClick(Sender: TObject);
    procedure QrVSPalletCalcFields(DataSet: TDataSet);
    procedure Encerraclassificao1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure Atualizasaldo1Click(Sender: TObject);
    procedure QrVSPaItsAfterScroll(DataSet: TDataSet);
    procedure QrVSPaItsBeforeClose(DataSet: TDataSet);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure PMIMEIPopup(Sender: TObject);
    procedure IrparajaneladedadosdoIMEI1Click(Sender: TObject);
    procedure ExcluiIMEIselecionado1Click(Sender: TObject);
    procedure AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
    procedure EncerraPallet1Click(Sender: TObject);
    procedure Atualizasaldo2Click(Sender: TObject);
    procedure AtualizaStatus1Click(Sender: TObject);
    procedure IMEIsdegerao1Click(Sender: TObject);
    procedure PMCacItsPopup(Sender: TObject);
    procedure Alteradadosdiitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure FichaCOMonomedoPallet1Click(Sender: TObject);
    procedure FichaSEMonomedoPallet1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AlteraFornecedor1Click(Sender: TObject);
    procedure ApenasaDescrio1Click(Sender: TObject);
    procedure udo1Click(Sender: TObject);
    procedure Itensnopallet1Click(Sender: TObject);
    procedure frxWET_CURTI_013_AGetValue(const VarName: string;
      var Value: Variant);
    procedure PageControl1Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure IncluiManualmente1Click(Sender: TObject);
    procedure Mudademeiosparainteiro1Click(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure NdoPallet1Click(Sender: TObject);
    procedure NdeidentificaodoSIFDIPOA1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    procedure AtualizaStatPallDeTodosPallets();
    procedure AtualizaIMEIsDeGeracaoDeTodosPallets();
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    //procedure EncerraPalletCla();
    //procedure EncerraPalletRcl();
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCourosEmClassificacao(Controle: Int64);
    procedure ReopenClassificacoes();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    //procedure AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc: Integer);
    procedure ImprimePallet(InfoNO_Pallet: Boolean);
    procedure ReabreQuerys(Tab: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    //procedure ReopenGraGruX();
    procedure ReopenSumPall();
    procedure ReopenIMEIs(Controle: Integer);
    procedure ReopenVSPaIts();
    procedure ReopenVSPaItens();

  end;

var
  FmVSPallet: TFmVSPallet;
  FmVSPallet1: TFmVSPallet;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, (*WBPalArt, WBPalFrn,*)
ModuleGeral, MyListas, UnVS_CRC_PF, AppListas, GetValor, VSSifDipoaPesqBag;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPallet.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPallet.Mudademeiosparainteiro1Click(Sender: TObject);
var
  CtrlAnt, CtrlAtu, SeqPar, VMI_SorcAnt, VMI_SorcAtu, VMI_BAixAnt, VMI_BaixAtu,
  VMI_DestAnt, VMI_DestAtu: Integer;
  Pecas, AreaM2, AreaP2, PecasAnt, PecasAtu, AreaM2Ant, AreaM2Atu, AreaP2Ant,
  AreaP2Atu: Double;
  CorrigiuSim, CorrigiuNao: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if Geral.MB_Pergunta('Este procedimento ir� somar pares de itens com �rea identica consecutiva.' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    CorrigiuSim := 0;
    CorrigiuNao := 0;
    QrVSCacItsAOpn.DisableControls;
    try
      Screen.Cursor := crHourGlass;
      SeqPar := 0;
      QrVSCacItsAOpn.First;
      while not QrVSCacItsAOpn.Eof do
      begin
        if SeqPar = 0 then
        begin
          CtrlAnt   := QrVSCacItsAOpnControle.Value;
          PecasAnt  := QrVSCacItsAOpnPecas.Value;
          AreaM2Ant := QrVSCacItsAOpnAreaM2.Value;
          AreaP2Ant := QrVSCacItsAOpnAreaP2.Value;
          //
          VMI_SorcAnt := QrVSCacItsAOpnVMI_Sorc.Value;
          VMI_BAixAnt := QrVSCacItsAOpnVMI_Baix.Value;
          VMI_DestAnt := QrVSCacItsAOpnVMI_Dest.Value;
          //
          SeqPar := 1;
        end else
        begin
          CtrlAtu   := QrVSCacItsAOpnControle.Value;
          PecasAtu  := QrVSCacItsAOpnPecas.Value;
          AreaM2Atu := QrVSCacItsAOpnAreaM2.Value;
          AreaP2Atu := QrVSCacItsAOpnAreaP2.Value;
          //
          VMI_SorcAtu := QrVSCacItsAOpnVMI_Sorc.Value;
          VMI_BAixAtu := QrVSCacItsAOpnVMI_Baix.Value;
          VMI_DestAtu := QrVSCacItsAOpnVMI_Dest.Value;
          //
          if (VMI_SorcAnt = VMI_SorcAtu)
          and (VMI_BaixAnt = VMI_BaixAtu)
          and (VMI_DestAnt = VMI_DestAtu)
          and (Trunc(PecasAnt * 1000) = Trunc(PecasAtu * 1000))
          and (Trunc(AreaM2Ant * 100) = Trunc(AreaM2Atu * 100))
          and (Trunc(AreaP2Ant * 100) = Trunc(AreaP2Atu * 100))
          and (PecasAnt <> 0) then
          begin
            CorrigiuSim := CorrigiuSim + 1;
            //
            Pecas := PecasAnt + PecasAtu;
            if Pecas > 1 then
              Pecas := 1;
            AreaM2 := AreaM2Ant + AreaM2Atu;
            AreaP2 := AreaP2Ant + AreaP2Atu;
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
            'UPDATE vscacitsa SET Pecas=0, AreaM2=0, AreaP2=0 ',
            'WHERE Controle=' + Geral.FF0(CtrlAnt),
            ';',
            'UPDATE vscacitsa SET Pecas=' + Geral.FFT_Dot(Pecas, 3, siNegativo),
            ', AreaM2=' + Geral.FFT_Dot(AreaM2, 2, siNegativo),
            ', AreaP2=' + Geral.FFT_Dot(AreaP2, 2, siNegativo),
            'WHERE Controle=' + Geral.FF0(CtrlAtu),
            ';',
            '']));
            //
            SeqPar := 0;
          end else
          begin
            CorrigiuNao := CorrigiuNao + 1;
          end;
        end;
        //
        QrVSCacItsAOpn.Next;
      end;
    finally
      QrVSCacItsAOpn.EnableControls;
      Screen.Cursor := crDefault;
    end;
    ReopenCourosEmClassificacao(0);
    //
    Geral.MB_Info('Itens Corrigidos: ' + Geral.FF0(CorrigiuSim) + sLineBreak +
    'Itens N�O Corrigidos: ' + Geral.FF0(CorrigiuNao) + sLineBreak +
    '');
  end;
end;

procedure TFmVSPallet.NdeidentificaodoSIFDIPOA1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if DBCheck.CriaFm(TFmVSSifDipoaPesqBag, FmVSSifDipoaPesqBag, afmoSoBoss) then
  begin
    FmVSSifDipoaPesqBag.ShowModal;
    Codigo := FmVSSifDipoaPesqBag.FPallet;
    FmVSSifDipoaPesqBag.Destroy;
    //
    if Codigo <> 0 then
    begin
      LocCod(Codigo, Codigo);
      if QrVSPalletCodigo.Value <> Codigo then
        Geral.MB_Erro('O pallet ' + Geral.FF0(Codigo) + ' n�o foi localizado!');
    end;
  end;
end;

procedure TFmVSPallet.NdoPallet1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPalletCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPallet.ReabreQuerys(Tab: Integer);
begin
  case Tab of
   //-1: ReopenSumPall();
    0: ReopenIMEIs(0);
    1: ReopenVSPaIts();
    2: ReopenCourosEmClassificacao(0);
    3: ReopenClassificacoes();
    4: ReopenClassificacoes();
    else
    begin
      ReopenIMEIs(0);
      ReopenVSPaIts();
      ReopenCourosEmClassificacao(0);
      ReopenClassificacoes();
      ReopenClassificacoes();
      //ReopenVSPalArt(0);
      //ReopenSumPall();
    end;
  end;
end;

procedure TFmVSPallet.PageControl1Change(Sender: TObject);
begin
  ReabreQuerys(PageControl1.ActivePageIndex);
end;

procedure TFmVSPallet.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPallet);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSPallet, Qr???);
  Encerraclassificao1.Enabled :=
    (QrVSPallet.State <> dsInactive) and (QrVSPalletDtHrEndAdd.Value < 2);
  IncluiManualmente1.Enabled := VAR_VSInsPalManu (*Dmod.QrControleVSInsPalManu.Value*) = 1;
  Mudademeiosparainteiro1.Enabled := (PageControl1.ActivePageIndex = 2) and
    (QrVSCacItsAOpn.RecordCount > 0);
end;

procedure TFmVSPallet.PMCacItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Alteradadosdiitematual1, QrVSCacItsAOpn);
end;

procedure TFmVSPallet.PMIMEIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Irparajaneladomovimento1, QrIMEIs);
  MyObjects.HabilitaMenuItemItsUpd(IrparajaneladedadosdoIMEI1, QrIMEIs);
  MyObjects.HabilitaMenuItemItsUpd(AlteraIMEIorigemdoIMEISelecionado1, QrIMEIs);
end;

procedure TFmVSPallet.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPalletCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPallet.DefParams;
begin
  VAR_GOTOTABELA := 'VSPalleta';
  VAR_GOTOMYSQLTABLE := QrVSPallet;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT let.*,  ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,');
  VAR_SQLx.Add(' CONCAT(gg1.Nome, ');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR, vps.Nome NO_STATUS   ');
  VAR_SQLx.Add('FROM vspalleta let  ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  ');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  ');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ');
  VAR_SQLx.Add('LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status');

  VAR_SQLx.Add('WHERE let.Codigo > 0');
  //
  VAR_SQL1.Add('AND let.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND let.Nome Like :P0');
  //
end;

procedure TFmVSPallet.Desfazencerramento1Click(Sender: TObject);
var
  Codigo, GraGruX: Integer;
begin
  Codigo := QrVSPalletCodigo.Value;
  GraGruX := QrVSPalletGraGruX.Value;
  if VS_CRC_PF.DesfazEncerramentoPallet(Codigo, GraGruX) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPallet.EdGraGruXRedefinido(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := QrGraGruXUsaSifDipoa.Value = 1;
  LaSeqSifDipoa.Enabled := Habilita;
  EdSeqSifDipoa.Enabled := Habilita;
  LaObsSifDipoa.Enabled := Habilita;
  EdObsSifDipoa.Enabled := Habilita;
end;

procedure TFmVSPallet.Encerraclassificao1Click(Sender: TObject);
const
  Pergunta = True;
begin
{
  // Nao pode!! Deve se usar a janela de gerenciamento de classifica��o ou reclassifica��o!
  if QrVSPalletDtHrEndAdd.Value < 2 then
  begin
    case TEstqMovimID(QrVSPalletMovimIDGer.Value) of
       (*7*)emidClassArtXXUni: EncerraPalletCla(); //Geral.MB_Info('Utilize a janela de gerenciamento de classifica��o!');
       (*8*)emidReclasVS     : EncerraPalletRcl();
       else Geral.MB_Info(
         'Tipo de movimento n�o definido em "Encerraclassificao1"!');
    end;
  end else
    Geral.MB_Info('Pallet j� encerrado!');
}
  VS_CRC_PF.EncerraPalletNew(QrVSPalletCodigo.Value, Pergunta);
end;

procedure TFmVSPallet.EncerraPallet1Click(Sender: TObject);
const
  Pergunta = True;
begin
  //EncerraPalletCla();
  VS_CRC_PF.EncerraPalletNew(QrVSPalletCodigo.Value, Pergunta);
end;

{
procedure TFmVSPallet.EncerraPalletCla();
const
  EncerrandoTodos = False;
  FromRcl  = False;
  Pergunta = True;
  Encerra  = True;
  OC       = 0;
var
  Codigo: Integer;
  FromBox: Variant;
begin
  FromBox := Null;
  Codigo := QrVSPalletCodigo.Value;
  VS_CRC_PF.EncerraPalletOld(Codigo, OC, EncerrandoTodos, FromBox, emidAjuste, Pergunta,
    Encerra, FromRcl);
  LocCod(Codigo, Codigo);
end;
}

{
procedure TFmVSPallet.EncerraPalletRcl();
const
  EncerrandoTodos = False;
  Pergunta = True;
var
  VSPaClaIts, Box, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet,
  CacCod, Codigo: Integer;
  ReabreVSPaRclCab: Boolean;
begin
  Box        := -1; // para nao remover
  VSPaClaIts := 0;
  VSPallet   := QrVSPalletCodigo.Value;
  VMI_Sorc   := 0;
  VMI_Baix   := 0;
  VMI_Dest   := 0;
  //
  CacCod     := 0;
  Codigo     := 0;
  ReabreVSPaRclCab := False;
  if QrVSPalletDtHrEndAdd.Value < 2 then
  begin
    (*Result := *)VS_CRC_PF.EncerraPalletReclassificacaoOld(CacCod,
    Codigo, VSPallet, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
    EncerrandoTodos, Pergunta, ReabreVSPaRclCab);
    LocCod(VSPallet, VSPallet);
  end else
    Geral.MB_Aviso('Pallet j� encerrado!');
end;
}

procedure TFmVSPallet.ExcluiIMEIselecionado1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(QrIMEIsControle.Value) + '?') = ID_YES then
  begin
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(QrIMEIsControle.Value,
      Integer(TEstqMotivDel.emtdWetCurti013), Dmod.QrUpd, Dmod.MyDB, '') then
    begin
      LocCod(QrVSPalletCodigo.Value, QrVSPalletCodigo.Value);
    end;
  end;
end;

procedure TFmVSPallet.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSPallet.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSPallet.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
  //SbNumeroClick(Self); Erro no win server 2003
end;

procedure TFmVSPallet.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPallet.ReopenClassificacoes();
var
  SQL_IMEI: String;
begin
  SQL_IMEI := Geral.ATS([
  '0 Codigo, 0 IMEC, 0 IMEI, ',
  '0 MovimID, 0 MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDestino, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.VSPallet, ',
  'vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
  '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
  '0 SdoVrtPeso, 0 SdoVrtArM2, -cia.Pecas LmbVrtPeca, ',
  '0 LmbVrtPeso, -cia.AreaM2 LmbVrtArM2,',
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, rca.VSPallet Pallet, vsp.Nome NO_Pallet,',
  '0 Terceiro,',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
  'IF(vsp.Status IS NULL, 0, vsp.Status),',
  '"" NO_FORNECE,',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'vps.Nome NO_STATUS,',
  'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  //
  '2 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  '-cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
  '1 Ativo  ',
  'FROM ' + TMeuDB + '.vscacitsa cia',
  'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=rca.VSPallet',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  '', // Sem terceiro???
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
  'LEFT JOIN ' + TMeuDB + '.vspalsta  vps ON vps.Codigo=vsp.Status',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
////////////  2015-03-05 ///////////////////////////////////////////////////////
  'WHERE cia.VSPaRclIts IN (',
  '     SELECT Controle ',
  '     FROM ' + TMeuDB + '.vsparclitsa pri ',
  '     WHERE DtHrFim < "1900-01-01"',
  ')',
  //
  'AND rca.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  '']);

////////////////////////////////////////////////////////////////////////////////
///
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriRcl, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.VSPallet, ',
  'vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
  '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
  '0 SdoVrtPeso, 0 SdoVrtArM2, cia.Pecas LmbVrtPeca, ',
  '0 LmbVrtPeso, cia.AreaM2 LmbVrtArM2,',
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet,',
  '0 Terceiro,',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
  'IF(vsp.Status IS NULL, 0, vsp.Status),',
  '"" NO_FORNECE,',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'vps.Nome NO_STATUS,',
  'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  //
  '1 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  'cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
  '1 Ativo  ',
  'FROM ' + TMeuDB + '.vscacitsa cia',
  'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  '', // Sem terceiro???
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
////////////  2015-03-05 ///////////////////////////////////////////////////////
  'WHERE cia.VSPaRclIts IN (',
  '     SELECT Controle ',
  '     FROM ' + TMeuDB + '.vsparclitsa pri ',
  '     WHERE DtHrFim < "1900-01-01"',
  ')',
  'AND cia.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  '']);

////////////////////////////////////////////////////////////////////////////////
///
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriCla, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.VSPallet, ',
  'vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
  '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
  '0 SdoVrtPeso, 0 SdoVrtArM2, cia.Pecas LmbVrtPeca, ',
  '0 LmbVrtPeso, cia.AreaM2 LmbVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet, ',
  '0 Terceiro, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
  'IF(vsp.Status IS NULL, 0, vsp.Status), ',
  '"" NO_FORNECE, ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "", ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  'vsp.DtHrEndAdd DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  '1 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  'cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
  '1 Ativo ',
  'FROM ' + TMeuDB + '.vscacitsa cia ',
  'LEFT JOIN ' + TMeuDB + '.vspaclacaba rca ON rca.Codigo=cia.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE ( ',
  '(cia.VSPaClaIts <> 0) AND ',
  '(cia.VSPaClaIts IN ( ',
  '     SELECT DISTINCT Controle ',
  '     FROM ' + TMeuDB + '.vspaclaitsa ',
  '     WHERE DtHrFim < "1900-01-01") ',
  ')) ',
  'AND cia.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  '']);
  //
  EdDestino.ValueVariant := Geral.FF0(QrDestino.RecordCount);
  EdOriCla.ValueVariant := Geral.FF0(QrOriCla.RecordCount);
  EdOriRcl.ValueVariant := Geral.FF0(QrOriRcl.RecordCount);



//  Fazer historia de pallets destino em aberto!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallDst, Dmod.MyDB, [
  'SELECT cia.VSPallet, SUM(Pecas) Pecas, VMI_Dest, pal.DtHrEndAdd, ',
  'IF(pal.DtHrEndAdd < "1900-01-01", "",  ',
  'DATE_FORMAT(pal.DtHrEndAdd, "%d/%m/%Y %H:%i:%s")) DtHrEndAdd_TXT  ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vsparclcaba rcl ON cia.CacCod=rcl.CacCod ',
  'LEFT JOIN vspalleta   pal ON pal.Codigo=cia.VSPallet ',
  'WHERE rcl.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'GROUP BY VMI_Dest ',
  '']);
  //Um dos itens da sql acima do pallet 489 tem 64 couros e eh para o Pallet 474
  //que estah removido!
  //Fazer tambem a origem no pallet 474 pois ele tem 248 couros no total!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallOri, Dmod.MyDB, [
  'SELECT 2.000 ClaRcl, "Reclassifica��o" NO_ClaRcl, ',
  'cia.CacCod, cia.Codigo, SUM(cia.Pecas) Pecas, ',
  'pcc.VSMovIts, pcc.VSPallet + 0.000 VSPallet, pcc.DtHrFimCla, ',
  'vmi.SerieFch, vmi.Ficha, vsf.Nome NO_SerieFch, ',
  'IF(pcc.DtHrFimCla < "1900-01-01", "",   ',
  'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT  ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vsparclcaba pcc ON pcc.CacCod=cia.CacCod ',
  //'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=pcc.VSMovIts ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pcc.VSMovIts ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE cia.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'AND cia.CacID=8 ',
  'GROUP BY cia.CacCod, cia.Codigo ',
  ' ',
  'UNION ',
  ' ',
  'SELECT 1.000 ClaRcl, "Classifica��o" NO_ClaRcl,  ',
  'cia.CacCod, cia.Codigo, SUM(cia.Pecas) Pecas, ',
  'pcc.VSMovIts, 0 VSPallet, pcc.DtHrFimCla, ',
  'vmi.SerieFch, vmi.Ficha, vsf.Nome NO_SerieFch, ',
  'IF(pcc.DtHrFimCla < "1900-01-01", "",   ',
  'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT  ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspaclacaba pcc ON pcc.CacCod=cia.CacCod ',
  //'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=pcc.VSMovIts ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pcc.VSMovIts ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE cia.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'AND cia.CacID=7 ',
  'GROUP BY cia.CacCod, cia.Codigo ',
  ' ']);
(*
object QrPallOriSerieFch: TIntegerField
  FieldName = 'SerieFch'
end
object QrPallOriFicha: TIntegerField
  FieldName = 'Ficha'
end
object QrPallOriNO_SerieFch: TWideStringField
  FieldName = 'NO_SerieFch'
  Size = 60
end
*)
end;

procedure TFmVSPallet.ReopenCourosEmClassificacao(Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacItsAOpn, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa',
  'WHERE VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  '']);
  if Controle > 0 then
    QrVSCacItsAOpn.Locate('Controle', Controle, []);
end;

procedure TFmVSPallet.ReopenIMEIs(Controle: Integer);
const
  TemIMEIMrt = 1;
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'vmi.ValorT/vmi.AreaM2 CustoM2',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Pallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEIS);
  //
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
end;

procedure TFmVSPallet.ReopenVSPaItens();
var
  Tabela, Codigo: String;
begin
  if QrVSPaItsVSPaRclIts.Value > 0 then
  begin
    Tabela := 'vsparclitsa';
    Codigo := Geral.FF0(QrVSPaItsVSPaRclIts.Value);
  end else
  begin
    Tabela := 'vspaclaitsa';
    Codigo := Geral.FF0(QrVSPaItsVSPaClaIts.Value);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaItens, Dmod.MyDB, [
  'SELECT Codigo, Controle, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, ',
  'Tecla, DtHrIni, DtHrFim, QIt_Sorc, QIt_Baix  ',
  'FROM ' + Tabela,
  'WHERE Controle=' + Codigo,
  '']);
end;

procedure TFmVSPallet.ReopenVSPaIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaIts, Dmod.MyDB, [
  'SELECT VSPallet, VSPaClaIts, VSPaRclIts, VMI_Sorc, VMI_Dest,  ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'GROUP BY VSPallet, VSPaClaIts, VSPaRclIts,  ',
  'VMI_Sorc, VMI_Dest ',
  '']);
end;

procedure TFmVSPallet.ReopenSumPall();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPall, Dmod.MyDB, [
  'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas, ',
  'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT, ',
  'SUM(vmi.SdoVrtPeca) SdoVrtPeca, SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(vmi.SdoVrtArM2) SdoVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, MAX(vmi.DataHora) DataHora ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE vmi.Pallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  //2015-05-05
  'AND vmi.Pecas>0 ',
  // FIM 2015-05-05
  'GROUP BY vmi.GraGruX ',
  '']);
end;

procedure TFmVSPallet.DBGrid1DblClick(Sender: TObject);
begin
  VAR_IMEI_SEL := QrIMEIsControle.Value;
  Close;
end;

procedure TFmVSPallet.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPallet.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPallet.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPallet.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPallet.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPallet.udo1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'VSPalleta');
  Empresa := DmodG.ObtemFilialDeEntidade(QrVSPalletEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  TPDtHrEndAdd.Enabled := True;
  EdDtHrEndAdd.Enabled := True;
  //
  EdEmpresa.Enabled := False;
  CBEmpresa.Enabled := False;
  EdClientMO.Enabled := False;
  CBClientMO.Enabled := False;
end;

procedure TFmVSPallet.UpdDelCouroSelecionado(SQLTipo: TSQLType);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
(*
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
*)
  Controle: Int64;
  //QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt: String;
  Continua: Boolean;
  VMI_Dest, VMI_Sorc, VMI_Baix, Codigo: Integer;
begin
(*
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest) then
    Exit;
*)

  Codigo         := QrVSPalletCodigo.Value;
  Controle       := QrVSCacItsAOpnControle.Value;
  VMI_Dest       := QrVSCacItsAOpnVMI_Dest.Value;
  VMI_Sorc       := QrVSCacItsAOpnVMI_Sorc.Value;
  VMI_Baix       := QrVSCacItsAOpnVMI_Baix.Value;
(*
  All_VSPaClaIts := QrAllVSPaClaIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
*)
  case SQLTipo of
    stUpd:
    begin
      Pecas  := 1;
      //
      (*
      if QrVSPaClaCabTipoArea.Value = 0 then
      *)
        Txt := FloatToStr(QrVSCacItsAOpnAreaM2.Value * 100);
      (*
      else
        Txt := FloatToStr(QrAllAreaP2.Value * 100);
      *)
      //
      if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
      begin
        AreaM2 := Geral.IMV(Txt);
        //
        (*
        if QrVSPaClaCabTipoArea.Value = 0 then
        begin
        *)
          AreaM2 := AreaM2 / 100;
          AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
        (*
        end else
        begin
          AreaP2 := AreaM2 / 100;
          AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
        end;
        *)
        Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                      'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                      [Controle], True);
      end;
    end;
    stDel:
    begin
      Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_CRC_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix);
    //
    LocCod(Codigo, Codigo);
    ReopenCourosEmClassificacao(Controle);
  end;
end;

procedure TFmVSPallet.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPallet.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPalletCodigo.Value;
  Close;
end;

procedure TFmVSPallet.Alteradadosdiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSPallet.AlteraFornecedor1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Reclassifica��o';
  Prompt = 'Informe a reclassifica��o';
  Campo  = 'Descricao';
var
  Terceiro, Controle, ThisCtrl: Integer;
  Resp: Variant;
begin
  ThisCtrl := QrIMEIsControle.Value;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
    'FROM entidades ent ',
    'WHERE ent.Fornece1="V" ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    Terceiro := Resp;
    QrIMEIs.First;
    while not QrIMEIs.Eof do
    begin
      Controle := QrIMEIsControle.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Terceiro'], [
      'Controle'], [
      Terceiro], [
      Controle], True);
      //
      QrIMEIs.Next;
    end;
    //
    ReopenIMEIs(ThisCtrl);
  end;
end;

procedure TFmVSPallet.AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
var
  ResVar: Variant;
  Controle, SrcNivel2: Integer;
begin
  if VAR_USUARIO <> -1 then
  begin
    Geral.MB_Aviso('Usu�rio deve ser Master!');
    Exit;
  end;
  SrcNivel2 := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    SrcNivel2 := Geral.IMV(ResVar);
    Controle  := QrIMEIsControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'SrcNivel2'], ['Controle'], [
    SrcNivel2], [Controle], True) then
    begin
      ReopenIMEIs(Controle);
    end;
  end;
end;

procedure TFmVSPallet.ApenasaDescrio1Click(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Nome := QrVSPalletNome.Value;
  if InputQuery('Descri��o', 'Informe a nova descri��o:', Nome) then
  begin
    Codigo := QrVSPalletCodigo.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'VSPalleta', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
    begin
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmVSPallet.AtualizaIMEIsDeGeracaoDeTodosPallets();
var
  Qry: TmySQLQuery;
  Pallet: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vspalleta ',
    'ORDER BY Codigo DESC',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Pallet := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Pallet));
        //
        VS_CRC_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet, QrSumVMI);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSPallet.Atualizasaldo1Click(Sender: TObject);
begin
  QrIMEIS.DisableControls;
  try
    QrIMEIs.First;
    while not QrIMEIs.Eof do
    begin
      VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrIMEIsControle.Value,
        QrIMEIsMovimID.Value, QrIMEIsMovimNiv.Value);
      QrIMEIs.Next;
    end;
    //
    LocCod(QrVSPalletCodigo.Value, QrVSPalletCodigo.Value);
  finally
    QrIMEIS.EnableControls;
  end;
end;

procedure TFmVSPallet.Atualizasaldo2Click(Sender: TObject);
const
  Gera = False;
begin
  VS_CRC_PF.AtualizaSaldoIMEI(QrIMEIsControle.Value, Gera);
end;

procedure TFmVSPallet.AtualizaStatPallDeTodosPallets();
var
  Qry: TmySQLQuery;
  Pallet: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vspalleta ',
    'ORDER BY Codigo DESC',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Pallet := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Pallet));
        //
        VS_CRC_PF.AtualizaStatPall(Pallet);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSPallet.AtualizaStatus1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaStatPall(QrVSPalletCodigo.Value);
end;

procedure TFmVSPallet.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrEndAdd, ObsSifDipoa, DataFab: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc,
  Controle, Terceiro, SrcGGX, DstGGX, SeqSifDipoa: Integer;
  TipoTrocaGgxVmiPall: TTipoTrocaGgxVmiPall;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Status         := EdStatus.ValueVariant;
  CliStat        := EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT_TP_Ed(TPDtHrEndAdd.Date, EdDtHrEndAdd.Text);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := EdQtdPrevPc.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  SeqSifDipoa    := EdSeqSifDipoa.ValueVariant;
  DataFab    := Geral.FDT(TPDataFab.Date, 1);
  ObsSifDipoa    := EdObsSifDipoa.ValueVariant;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina dono do couro!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO',
  'SeqSifDipoa', 'DataFab', 'ObsSifDipoa'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO,
  SeqSifDipoa, DataFab, ObsSifDipoa], [
  Codigo], True) then
  begin
    //Parei Aqui!! Alterar artigo de IMEIs !!!
    if Geral.MB_Pergunta('Deseja alterar o artigo dos IME-Is?') = ID_YES then
    begin
      //Parei aqui! somente dos destinos!
      QrIMEIs.First;
      while not QrIMEIs.Eof do
      begin
        Controle := QrIMEIsControle.Value;
        //
        // Ini 2019-25-01
{
        if VS_CRC_PF.RedefineReduzidoOnPallet(
        TEstqMovimID(QrIMEIsMovimID.Value),
        TEstqMovimNiv(QrIMEIsMovimNiv.Value)) = tsPos then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'GraGruX'], [
          'Controle'], [
          GraGruX], [
          Controle], True);
        end;
}
        TipoTrocaGgxVmiPall := VS_CRC_PF.RedefineReduzidoOnPallet(
          TEstqMovimID(QrIMEIsMovimID.Value),
          TEstqMovimNiv(QrIMEIsMovimNiv.Value));
        case TipoTrocaGgxVmiPall of
          TTipoTrocaGgxVmiPall.ttvpNenhum: ; // Nada!
          TTipoTrocaGgxVmiPall.ttvpGraGrux:
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'GraGruX'], [
            'Controle'], [
            GraGruX], [
            Controle], True);
          end;
          TTipoTrocaGgxVmiPall.ttvpClaRcl:
          begin
            SrcGGX := GraGruX;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'GraGruX', 'SrcGGX'], [
            'Controle'], [
            GraGruX, SrcGGX], [
            Controle], True);
            //
            UnDmkDAC_PF.AbreMySQLQuery0(Qr1, Dmod.MyDB, [
            'SELECT Controle ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE MovimTwn<>0 ',
            'AND MovimTwn=' + Geral.FF0(QrIMEIsMovimTwn.Value),
            'AND MovimNiv=1',
            '']);
            Controle := Qr1.FieldByName('Controle').AsInteger;
            if Controle <> 0 then
            begin
              DstGGX := GraGruX;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
              'DstGGX'], [
              'Controle'], [
              DstGGX], [
              Controle], True);
            end else
              Geral.MB_Erro('Controle do MovimNiv 1 do MovimTwn ' +
              Geral.FF0(QrIMEIsMovimTwn.Value) +
              ' n�o localizado! Avise a Dermatek!');
          end;
        end;
        // Fim 2019-25-01
        //
        QrIMEIs.Next;
      end;
    end;
    (*
    Descri��o: Foi criado um bot�o na janela principal em: Ferramentas => Corre��es => Atualiza Artigos De Pallets
    Motivo: Melhoria do desempenho desta janela
    Data: 25/04/2017
    //
    VS_CRC_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
    *)
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSPallet.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'VSPalleta', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'VSPalleta', 'Codigo');
end;

procedure TFmVSPallet.BtIMEIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIMEI, BtIMEI);
end;

procedure TFmVSPallet.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPallet.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  GBEdita.Align   := alClient;
  //DBGFrn.Align    := alClient;
  PageControl1.Align   := alClient;
  PageControl1.ActivePageIndex := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CriaOForm;
  FSeq := 0;
  //
  //UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrVSRibCla, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSPalSta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  //ReopenGraGruX();
  VS_CRC_PF.ReopenGraGruX_Pallet(QrGraGruX);
  Data := DModG.ObtemAgora();
  TPDataFab.Date := Data;
end;

procedure TFmVSPallet.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSPallet.SBClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliStat.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliStat.Text := IntToStr(VAR_ENTIDADE);
    CBCliStat.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPallet.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPallet.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSPallet.SbNovoClick(Sender: TObject);
begin
(*
  FAtualizando := not FAtualizando;
  if FAtualizando then
    AtualizaStatPallDeTodosPallets();
*)
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
    AtualizaIMEIsDeGeracaoDeTodosPallets();
end;

procedure TFmVSPallet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPallet.QrVSPaItsAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaItens();
end;

procedure TFmVSPallet.QrVSPaItsBeforeClose(DataSet: TDataSet);
begin
  QrVSPaItens.Close;
end;

procedure TFmVSPallet.QrVSPalArtAfterScroll(DataSet: TDataSet);
begin
  //ReopenVSPalFrn(0);
end;

procedure TFmVSPallet.QrVSPalArtBeforeClose(DataSet: TDataSet);
begin
  //QrVSPalFrn.Close;
end;

procedure TFmVSPallet.QrVSPalletAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPallet.QrVSPalletAfterScroll(DataSet: TDataSet);
begin
  ReopenSumPall();
  ReabreQuerys(PageControl1.ActivePageIndex);
end;

procedure TFmVSPallet.FichaCOMonomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(True);
end;

procedure TFmVSPallet.FichaSEMonomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(False);
end;

procedure TFmVSPallet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPalletCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPallet.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSPalletCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'VSPalleta', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSPallet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPallet.frxWET_CURTI_013_AGetValue(const VarName: string;
  var Value: Variant);
(*
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
*)
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
(*
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa.Date
*)
  else
end;

procedure TFmVSPallet.IMEIsdegerao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  VMI_Dest, VMI_Sorc, VMI_Baix, Pallet, Controle: Integer;
  Pecas, AreaM2, AreaP2, SdoVrtPeca, SdoVrtArM2: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    Pallet := QrVSPalletCodigo.Value;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT cia.VMI_Baix, cia.VMI_Dest, cia.VMI_Sorc, ',
      'SUM(cia.Pecas) Pecas',
      'FROM vscacitsa cia',
      //'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=cia.VMI_Baix',
      //'LEFT JOIN v s m o v i t s vmd ON vmd.Controle=cia.VMI_Dest',
      'WHERE cia.VSPallet=' + Geral.FF0(Pallet),
      'GROUP BY cia.VMI_Baix, cia.VMI_Dest, cia.VMI_Sorc',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando item ' +
        Geral.FF0(Qry.RecNo) + ' de ' + Geral.FF0(Qry.RecordCount));
        //
        VMI_Dest := Qry.FieldByName('VMI_Dest').AsInteger;
        VMI_Sorc := Qry.FieldByName('VMI_Sorc').AsInteger;
        VMI_Baix := Qry.FieldByName('VMI_Baix').AsInteger;
        if VMI_Dest <> 0 then
        begin
          VS_CRC_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix);
        end;
        //
        Qry.Next;
      end;
      LocCod(Pallet, Pallet);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSPallet.ImprimePallet(InfoNO_Pallet: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  ReabreQuerys(-1);
  //
  if QrSumPallSdoVrtPeca.Value <= 0 then
    Geral.MB_Aviso('O Pallet s� ser� impresso se tiver saldo positivo de pe�as!');
  Empresa  := QrVSPalletEmpresa.Value;
  ClientMO := QrVSPalletClientMO.Value;
  Pallet   := QrVSPalletCodigo.Value;
  TempTab  := Self.Name;
  //
  VS_CRC_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_Pallet);
end;

procedure TFmVSPallet.IncluiManualmente1Click(Sender: TObject);
var
  Pallet: Integer;
begin
  Pallet := VS_CRC_PF.CadastraPalletRibCla((*Empresa*)VAR_LIB_EMPRESA_SEL,
  (*ClientMO*)0, (*EdPallet*)nil, (*CBPallet*)nil, (*QrVSPallet*)nil,
  (*MovimIDGer*) TEstqMovimID.emidAjuste, (*GraGruX*)0);
  if Pallet <> 0 then
    LocCod(Pallet, Pallet);
end;

procedure TFmVSPallet.IrparajaneladedadosdoIMEI1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovIts(QrIMEIsControle.Value);
end;

procedure TFmVSPallet.Irparajaneladomovimento1Click(Sender: TObject);
begin
  if QrIMEIs.RecordCount > 0 then
    VS_CRC_PF.MostraFormVS_Do_IMEI(QrIMEISControle.Value);
end;

procedure TFmVSPallet.Itensnopallet1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacItsAOpnImp, Dmod.MyDB, [
  'SELECT its.*, rev.Nome NoRev, dig.Nome NoDig ',
  'FROM vscacitsa its ',
  'LEFT JOIN entidades rev ON rev.Codigo=its.Revisor ',
  'LEFT JOIN entidades dig ON dig.Codigo=its.Digitador ',
  'WHERE its.VSPallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'ORDER BY its.DataHora, its.Controle ',
  '']);
  MyObjects.frxDefineDataSets(frxWET_CURTI_013_A, [
  DmodG.frxDsDono,
  frxDsVSPallet,
  frxDsVSCacItsAOpnImp
  ]);
  MyObjects.frxMostra(frxWET_CURTI_013_A, 'Itens em Pallet');
end;

procedure TFmVSPallet.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'VSPalleta');
  EdStatus.ValueVariant := CO_STAT_VSPALLET_0100_DISPONIVEL;
  CBStatus.KeyValue     := CO_STAT_VSPALLET_0100_DISPONIVEL;
  TPDtHrEndAdd.Enabled := False;
  EdDtHrEndAdd.Enabled := False;
  TPDtHrEndAdd.Date := 0;
  EdDtHrEndAdd.ValueVariant := 0;
  EdNome.ValueVariant := '';
  //
  EdEmpresa.Enabled := True;
  CBEmpresa.Enabled := True;
  EdClientMO.Enabled := True;
  CBClientMO.Enabled := True;
end;

procedure TFmVSPallet.QrVSPalletBeforeClose(
  DataSet: TDataSet);
begin
  //QrVSPalArt.Close;
  QrSumPall.Close;
  QrIMEIs.Close;
  QrVSPaIts.Close;
end;

procedure TFmVSPallet.QrVSPalletBeforeOpen(DataSet: TDataSet);
begin
  QrVSPalletCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSPallet.QrVSPalletCalcFields(DataSet: TDataSet);
begin
  QrVSPalletDtHrEndAdd_TXT.Value := Geral.FDT(QrVSPalletDtHrEndAdd.Value, 106, True);
end;

end.


