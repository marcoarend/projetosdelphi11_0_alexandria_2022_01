unit VSInfOut;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Menus, UnProjGroup_Consts;

type
  TFmVSInfOut = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSInfOut: TmySQLQuery;
    QrVSInfOutCodigo: TIntegerField;
    DsVSInfOut: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdResponsa: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataHora: TdmkEditDateTimePicker;
    Label52: TLabel;
    EdDataHora: TdmkEdit;
    Label53: TLabel;
    EdNumIni: TdmkEdit;
    EdNumFim: TdmkEdit;
    Label55: TLabel;
    QrVSInfOutEmpresa: TIntegerField;
    QrVSInfOutDataHora: TDateTimeField;
    QrVSInfOutNumIni: TIntegerField;
    QrVSInfOutNumFim: TIntegerField;
    QrVSInfOutLk: TIntegerField;
    QrVSInfOutDataCad: TDateField;
    QrVSInfOutDataAlt: TDateField;
    QrVSInfOutUserCad: TIntegerField;
    QrVSInfOutUserAlt: TIntegerField;
    QrVSInfOutAlterWeb: TSmallintField;
    QrVSInfOutAtivo: TSmallintField;
    QrVSInfOutResponsa: TWideStringField;
    QrVSInfOutNO_EMP: TWideStringField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrVSInfOutQuantidade: TFloatField;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    PMImprime: TPopupMenu;
    N1vianormal1: TMenuItem;
    N1viareduzido1: TMenuItem;
    N2viasnormal1: TMenuItem;
    N2viasreduzido1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSInfOutAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSInfOutBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure N1vianormal1Click(Sender: TObject);
    procedure N1viareduzido1Click(Sender: TObject);
    procedure N2viasnormal1Click(Sender: TObject);
    procedure N2viasreduzido1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVSInfOut: TFmVSInfOut;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, VSImpRequis;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSInfOut.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSInfOut.N1vianormal1Click(Sender: TObject);
begin
  ImprimeRMEs(1, False);
end;

procedure TFmVSInfOut.N1viareduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(1, True);
end;

procedure TFmVSInfOut.N2viasnormal1Click(Sender: TObject);
begin
  ImprimeRMEs(2, False);
end;

procedure TFmVSInfOut.N2viasreduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(2, True);
end;

procedure TFmVSInfOut.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSInfOutCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSInfOut.DefParams;
begin
  VAR_GOTOTABELA := 'vsinfout';
  VAR_GOTOMYSQLTABLE := QrVSInfOut;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'Responsa';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vrm.*, vrm.NumFim - vrm.NumIni + 1.000 Quantidade, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP');
  VAR_SQLx.Add('FROM vsinfout vrm');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vrm.Empresa ');
  VAR_SQLx.Add('WHERE vrm.Codigo > 0');
  //
  VAR_SQL1.Add('AND vrm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND vrm.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vrm.Responsa LIKE :P0');
  //
end;

procedure TFmVSInfOut.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSInfOut.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSInfOut.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmVSInfOut.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSInfOut.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSInfOut.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSInfOut.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSInfOut.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInfOut.BtAlteraClick(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSInfOut, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vsinfout');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSInfOutEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSInfOut.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSInfOutCodigo.Value;
  Close;
end;

procedure TFmVSInfOut.BtConfirmaClick(Sender: TObject);
var
  DataHora, Responsa: String;
  Codigo, Empresa, NumIni, NumFim: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT_TP_Ed(TPDataHora.Date, EdDataHora.Text);
  NumIni         := EdNumIni.ValueVariant;
  NumFim         := EdNumFim.ValueVariant;
  Responsa       := EdResponsa.Text;
  //
  if MyObjects.FIC(Length(Responsa) = 0, EdResponsa, 'Informe o responsável!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('vsinfout', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinfout', False, [
  'Empresa', CO_DATA_HORA_GRL, 'NumIni',
  'NumFim', 'Responsa'], [
  'Codigo'], [
  Empresa, DataHora, NumIni,
  NumFim, Responsa], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSInfOut.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsinfout', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSInfOut.BtIncluiClick(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSInfOut, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vsinfout');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSInfOut.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSInfOut.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSInfOutCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInfOut.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSInfOut.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSInfOut.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSInfOutCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInfOut.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSInfOut.QrVSInfOutAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSInfOut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInfOut.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSInfOutCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsinfout', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSInfOut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInfOut.ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
begin
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
  FmVSImpRequis.FEmpresa_Cod   := QrVSInfOutEmpresa.Value;
  FmVSImpRequis.FEmpresa_Txt   := QrVSInfOutNO_EMP.Value;
  FmVSImpRequis.FResponsa      := QrVSInfOutResponsa.Value;
  //
  FmVSImpRequis.FQuantidade    :=  Trunc(QrVSInfOutQuantidade.Value);
  FmVSImpRequis.FNumInicial    :=  QrVSInfOutNumIni.Value;
  FmVSImpRequis.FNumVias       :=  Vias;
  FmVSImpRequis.FReduzido      :=  Reduzido;
  //
  FmVSImpRequis.ImprimeISC();
  //
  FmVSImpRequis.Destroy;
end;

procedure TFmVSInfOut.QrVSInfOutBeforeOpen(DataSet: TDataSet);
begin
  QrVSInfOutCodigo.DisplayFormat := FFormatFloat;
end;

end.

