object FmVSImpClaIMEI: TFmVSImpClaIMEI
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-036 :: Classes Geradas - IMEI'
  ClientHeight = 318
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 527
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 479
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 431
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Classes Geradas - IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Classes Geradas - IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Classes Geradas - IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 527
    Height = 156
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 527
      Height = 156
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 527
        Height = 156
        Align = alClient
        Caption = 'vs'
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 204
    Width = 527
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 523
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 248
    Width = 527
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 381
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 379
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrVSCacIts: TMySQLQuery
    OnCalcFields = QrVSCacItsCalcFields
    SQL.Strings = (
      'SELECT pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE cia.vmi_Sorc=5'
      'GROUP BY pla.GraGruX ')
    Left = 156
    Top = 56
    object QrVSCacItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSCacItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSCacItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCacItsPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      Calculated = True
    end
    object QrVSCacItsPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
  end
  object frxWET_CURTI_036_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41979.425569537040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_036_001GetValue
    Left = 56
    Top = 104
    Datasets = <
      item
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
      end
      item
        DataSet = frxDsVSGerArtNew
        DataSetName = 'frxDsVSGerArtNew'
      end
      item
        DataSet = frxDSVSGerArtOld
        DataSetName = 'frxDsVSGerArtOld'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149645350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Resultado de Classifica'#231#227'o do IMEI [VARF_IMEI]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 41.574830000000000000
          Width = 132.283464566929100000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data/hora gera'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Top = 60.472480000000000000
          Width = 132.283464566929100000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_GERACAO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 132.283550000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Liberado p/ classificar:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 132.283550000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_LIBCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 264.567100000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Configur. p/ classif.:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 264.567100000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_CNFCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 396.850650000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fim classifica'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          Left = 396.850650000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_FIMCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 529.134200000000000000
          Top = 41.574830000000000000
          Width = 151.181004720000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Ficha RMP:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          Left = 529.134200000000000000
          Top = 60.472480000000000000
          Width = 151.181004720000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SERIE_FICHA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677165350000000000
        ParentFont = False
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000010000
          Width = 317.480324720000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSCacIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496062992130000
          Height = 22.677165354330710000
          DataField = 'GraGruX'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 532.913730000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 480.000310000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779529999999994000
          Width = 385.511864720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 532.913730000000000000
          Top = 3.779529999999994000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Top = 3.779529999999994000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDSVSGerArtOld
        DataSetName = 'frxDsVSGerArtOld'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_SerieFch"] [frxDsVSGerArtOld."Ficha"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'GraGruX'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."GraGruX"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 196.535560000000000000
          Width = 113.385826771653500000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 309.921460000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'NotaMPAG'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NotaMPAG"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 355.275820000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."Pecas"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 400.630180000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."PesoKg"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 445.984540000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_FORNECE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 536.693260000000000000
          Width = 143.622047244094500000
          Height = 18.897650000000000000
          DataField = 'Observ'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."Observ"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo15: TfrxMemoView
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S'#233'rie /Ficha RMP')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 196.535560000000000000
          Width = 113.385826771653500000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria Prima')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 309.921460000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 355.275820000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 400.630180000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 445.984540000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 536.693260000000000000
          Width = 143.622047240000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000000000
          Width = 317.480324720000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPecas: TfrxMemoView
          Left = 532.913730000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 480.000310000000000000
          Width = 52.913385830000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsVSCacIts: TfrxDBDataset
    UserName = 'frxDsVSCacIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'PercM2=PercM2'
      'PercPc=PercPc')
    DataSet = QrVSCacIts
    BCDToCurrency = False
    Left = 156
    Top = 104
  end
  object QrVSCacSum: TMySQLQuery
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2'
      'FROM vscacitsa cia '
      'WHERE cia.vmi_Sorc=5')
    Left = 240
    Top = 56
    object QrVSCacSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrVSGerArt: TMySQLQuery
    OnCalcFields = QrVSGerArtCalcFields
    SQL.Strings = (
      'SELECT vga.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsgerarta vga'
      'LEFT JOIN entidades ent ON ent.Codigo=vga.Empresa'
      'WHERE vga.Codigo > 0')
    Left = 240
    Top = 105
    object QrVSGerArtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSGerArtDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrVSGerArtDtHrLibCla: TDateTimeField
      FieldName = 'DtHrLibCla'
    end
    object QrVSGerArtDtHrCfgCla: TDateTimeField
      FieldName = 'DtHrCfgCla'
    end
    object QrVSGerArtDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrVSGerArtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSGerArtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtPecasMan: TFloatField
      FieldName = 'PecasMan'
    end
    object QrVSGerArtAreaManM2: TFloatField
      FieldName = 'AreaManM2'
    end
    object QrVSGerArtAreaManP2: TFloatField
      FieldName = 'AreaManP2'
    end
    object QrVSGerArtTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSGerArtNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSGerArtNO_DtHrFimCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimCla'
      Calculated = True
    end
    object QrVSGerArtNO_DtHrLibCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibCla'
      Calculated = True
    end
    object QrVSGerArtNO_DtHrCfgCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgCla'
      Calculated = True
    end
    object QrVSGerArtCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSGerArtGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
    end
    object QrVSGerArtCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
    end
    object QrVSGerArtValorManMP: TFloatField
      FieldName = 'ValorManMP'
    end
    object QrVSGerArtValorManT: TFloatField
      FieldName = 'ValorManT'
    end
  end
  object QrVSGerArtNew: TMySQLQuery
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 320
    Top = 53
    object QrVSGerArtNewCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSGerArtNewControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSGerArtNewMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSGerArtNewMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSGerArtNewMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSGerArtNewEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSGerArtNewTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSGerArtNewCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSGerArtNewMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSGerArtNewDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSGerArtNewPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSGerArtNewGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSGerArtNewSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSGerArtNewSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSGerArtNewSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSGerArtNewFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSGerArtNewMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSGerArtNewFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerArtNewDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerArtNewDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSGerArtNewDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSGerArtNewID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSGerArtNewMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerArtNewPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSGerArtNewStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
  end
  object frxDsVSGerArtNew: TfrxDBDataset
    UserName = 'frxDsVSGerArtNew'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'CustoMOKg=CustoMOKg'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'Ficha=Ficha'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Misturou=Misturou'
      'CustoMOTot=CustoMOTot'
      'SdoVrtPeso=SdoVrtPeso'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NO_FORNECE=NO_FORNECE'
      'NO_FICHA=NO_FICHA'
      'CUSTO_M2=CUSTO_M2'
      'CUSTO_P2=CUSTO_P2'
      'NotaMPAG=NotaMPAG')
    DataSet = QrVSGerArtNew
    BCDToCurrency = False
    Left = 316
    Top = 100
  end
  object QrVSGerArtOld: TMySQLQuery
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 400
    Top = 53
    object QrVSGerArtOldCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSGerArtOldControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSGerArtOldMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSGerArtOldMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSGerArtOldMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSGerArtOldEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSGerArtOldTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSGerArtOldCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSGerArtOldMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSGerArtOldDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSGerArtOldPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSGerArtOldGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerArtOldPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtOldPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtOldAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSGerArtOldSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSGerArtOldSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSGerArtOldSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSGerArtOldSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSGerArtOldSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtOldSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtOldSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSGerArtOldFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSGerArtOldMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSGerArtOldFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSGerArtOldCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerArtOldDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerArtOldDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSGerArtOldDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSGerArtOldQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSGerArtOldQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtOldQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSGerArtOldQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtOldQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtOldNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSGerArtOldNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtOldNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtOldNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSGerArtOldID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSGerArtOldNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtOldNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtOldReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object frxDSVSGerArtOld: TfrxDBDataset
    UserName = 'frxDsVSGerArtOld'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'CustoMOKg=CustoMOKg'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOTot=CustoMOTot'
      'SdoVrtPeso=SdoVrtPeso'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NO_FORNECE=NO_FORNECE'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'NotaMPAG=NotaMPAG')
    DataSet = QrVSGerArtOld
    BCDToCurrency = False
    Left = 400
    Top = 100
  end
  object frxWET_CURTI_036_002: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41979.425569537040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_036_001GetValue
    Left = 156
    Top = 156
    Datasets = <
      item
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
      end
      item
        DataSet = frxDsVSGerArtNew
        DataSetName = 'frxDsVSGerArtNew'
      end
      item
        DataSet = frxDSVSGerArtOld
        DataSetName = 'frxDsVSGerArtOld'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149645350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Resultado de Classifica'#231#227'o do IMEI [VARF_IMEI]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 41.574830000000000000
          Width = 132.283464566929100000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data/hora gera'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Top = 60.472480000000000000
          Width = 132.283464566929100000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_GERACAO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 132.283550000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Liberado p/ classificar:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 132.283550000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_LIBCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 264.567100000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Configur. p/ classif.:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 264.567100000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_CNFCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 396.850650000000000000
          Top = 41.574830000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fim classifica'#231#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo11: TfrxMemoView
          Left = 396.850650000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DTHR_FIMCLASS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 529.134200000000000000
          Top = 41.574830000000000000
          Width = 151.181004720000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Ficha RMP:')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          Left = 529.134200000000000000
          Top = 60.472480000000000000
          Width = 151.181004720000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SERIE_FICHA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677165350000000000
        ParentFont = False
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSCacIts
        DataSetName = 'frxDsVSCacIts'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000010000
          Width = 317.480324720000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSCacIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496062992130000
          Height = 22.677165354330710000
          DataField = 'GraGruX'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 532.913730000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 480.000310000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercM2'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 22.677165350000000000
          DataField = 'PercPc'
          DataSet = frxDsVSCacIts
          DataSetName = 'frxDsVSCacIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacIts."PercPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779529999999994000
          Width = 385.511864720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 532.913730000000000000
          Top = 3.779529999999994000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Top = 3.779529999999994000
          Width = 147.401608980000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacIts."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDSVSGerArtOld
        DataSetName = 'frxDsVSGerArtOld'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_SerieFch"] [frxDsVSGerArtOld."Ficha"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'GraGruX'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."GraGruX"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 196.535560000000000000
          Width = 113.385826771653500000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 309.921460000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'NotaMPAG'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NotaMPAG"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 355.275820000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."Pecas"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 400.630180000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."PesoKg"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 445.984540000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."NO_FORNECE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 536.693260000000000000
          Width = 143.622047244094500000
          Height = 18.897650000000000000
          DataField = 'Observ'
          DataSet = frxDSVSGerArtOld
          DataSetName = 'frxDsVSGerArtOld'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSGerArtOld."Observ"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo15: TfrxMemoView
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S'#233'rie /Ficha RMP')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 196.535560000000000000
          Width = 113.385826771653500000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria Prima')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 309.921460000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota MPAG')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 355.275820000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 400.630180000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 445.984540000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 536.693260000000000000
          Width = 143.622047240000000000
          Height = 18.897650000000000000
          DataSet = frxDsVSGerArtNew
          DataSetName = 'frxDsVSGerArtNew'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000000000
          Width = 317.480324720000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPecas: TfrxMemoView
          Left = 532.913730000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 480.000310000000000000
          Width = 52.913385830000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% '#193'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
      end
    end
  end
end
