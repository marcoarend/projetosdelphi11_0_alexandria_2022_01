unit VSPalletAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, AppListas, UnAppEnums,
  dmkEditDateTimePicker;

type
  TFmVSPalletAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    DsClientes: TDataSource;
    QrVSPalSta: TmySQLQuery;
    QrVSPalStaCodigo: TIntegerField;
    QrVSPalStaNome: TWideStringField;
    DsVSPalSta: TDataSource;
    QrVSRibCla: TmySQLQuery;
    QrVSRibClaGraGruX: TIntegerField;
    QrVSRibClaNO_PRD_TAM_COR: TWideStringField;
    DsVSRibCla: TDataSource;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    SBCliente: TSpeedButton;
    LaVSRibCla: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCliStat: TdmkEditCB;
    CBCliStat: TdmkDBLookupComboBox;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdQtdPrevPc: TdmkEdit;
    QrVSRibClaPrevPcPal: TIntegerField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    EdClientMO: TdmkEditCB;
    Label20: TLabel;
    CBClientMO: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    LaSeqSifDipoa: TLabel;
    EdSeqSifDipoa: TdmkEdit;
    LaObsSifDipoa: TLabel;
    EdObsSifDipoa: TdmkEdit;
    QrVSRibClaUsaSifDipoa: TSmallintField;
    SbGraGruX: TSpeedButton;
    QrVSRibClaGraGruY: TIntegerField;
    QrLoc: TMySQLQuery;
    QrLocSeqSifDipoa: TIntegerField;
    TPDataFab: TdmkEditDateTimePicker;
    LaDataFab: TLabel;
    SbSeqSifDipoa: TSpeedButton;
    SbObsSifDipoa: TSpeedButton;
    QrSel: TMySQLQuery;
    QrSelObsSifDipoa: TWideStringField;
    DsSel: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure EdSeqSifDipoaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbSeqSifDipoaClick(Sender: TObject);
    procedure SbObsSifDipoaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenRibCla();
    procedure AtualizaSifDipoa();
    procedure ProximoSeqSifDipoa();
    procedure DefineObsSifDipoa();

  public
    { Public declarations }
    FMovimIDGer: TEstqMovimID;
    FPallet, FNewPallet: Integer;
  end;

  var
  FmVSPalletAdd: TFmVSPalletAdd;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnGrade_Jan,
  ModVS, GerlShowGrid;

{$R *.DFM}

procedure TFmVSPalletAdd.AtualizaSifDipoa();
var
  Empresa: Integer;
begin
  if (EdEmpresa.ValueVariant <> 0) and (ImgTipo.SQLType = stIns) then
  begin
    if EdGraGruX.ValueVariant <> 0 then
    begin
      Empresa := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
      DmModVS.ReopenVSSifDipoa(Empresa);
    end;
  end;
end;

procedure TFmVSPalletAdd.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc,
  SeqSifDipoa: Integer;
  ObsSifDipoa, DataFab: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Status         := EdStatus.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  CliStat        := EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  MovimIDGer     := Integer(FMovimIDGer);
  QtdPrevPc      := EdQtdPrevPc.ValueVariant;
  SeqSifDipoa    := EdSeqSifDipoa.ValueVariant;
  DataFab    := Geral.FDT(TPDataFab.Date, 1);
  ObsSifDipoa    := EdObsSifDipoa.ValueVariant;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then Exit;
  //
  if (SQLType = stIns) and (FNewPallet <> 0) then
    Codigo := FNewPallet
  else
    Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'MovimIDGer',
  'QtdPrevPc', 'ClientMO',
  'SeqSifDipoa', 'DataFab', 'ObsSifDipoa'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, MovimIDGer,
  QtdPrevPc, ClientMO,
  SeqSifDipoa, DataFab, ObsSifDipoa], [
  Codigo], True) then
  begin
    FNewPallet := 0;
    FPallet    := Codigo;
(*
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
*)
      Close;
  end;
end;

procedure TFmVSPalletAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPalletAdd.DefineObsSifDipoa();
var
  GraGruX: Integer;
begin
  GraGruX := QrVSRibClaGraGruX.Value;
  if GraGruX > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSel, Dmod.MyDB, [
    'SELECT DISTINCT(ObsSifDipoa) ObsSifDipoa ',
    'FROM vspalleta ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    'AND ObsSifDipoa <> ""',
    'ORDER BY ObsSifDipoa ',
    '']);




    if QrSel.RecordCount > 0 then
    begin
      if QrSel.RecordCount = 1 then
        EdObsSifDipoa.ValueVariant := QrSelObsSifDipoa.Value
      else
      begin
        Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
        FmGerlShowGrid.MeAvisos.Lines.Add('Selecione o texto');
        //FmGerlShowGrid.MeAvisos.Lines.Add('
        FmGerlShowGrid.DBGSel.DataSource := DsSel;
        FmGerlShowGrid.ShowModal;
        if FmGerlShowGrid.FSelecionou then
        begin
          EdObsSifDipoa.ValueVariant := QrSelObsSifDipoa.Value;
        end;
        FmGerlShowGrid.Destroy;
      end;
    end;
  end else
    Geral.MB_Aviso('Informe o artigo!');
end;

procedure TFmVSPalletAdd.EdEmpresaRedefinido(Sender: TObject);
var
  Empresa: Integer;
begin
  if (EdEmpresa.ValueVariant <> 0) and (ImgTipo.SQLType = stIns) then
  begin
    Empresa := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    EdClientMO.ValueVariant := Empresa;
    CBClientMO.KeyValue     := Empresa;
    //
    AtualizaSifDipoa();
  end;
end;

procedure TFmVSPalletAdd.EdGraGruXRedefinido(Sender: TObject);
var
  Habilita: Boolean;
begin
  if (QrVSRibClaPrevPcPal.Value <> 0) and (ImgTipo.SQLType = stIns) then
    EdQtdPrevPc.ValueVariant := QrVSRibClaPrevPcPal.Value;
  //
  Habilita := QrVSRibClaUsaSifDipoa.Value = 1;
  LaSeqSifDipoa.Enabled := Habilita;
  EdSeqSifDipoa.Enabled := Habilita;
  SbSeqSifDipoa.Enabled := Habilita;
  LaObsSifDipoa.Enabled := Habilita;
  EdObsSifDipoa.Enabled := Habilita;
  SbObsSifDipoa.Enabled := Habilita;
  //
  if ImgTipo.SQLType = stIns then
  begin
    EdSeqSifDipoa.ValueVariant := 0;
    AtualizaSifDipoa();
  end;
end;

procedure TFmVSPalletAdd.EdSeqSifDipoaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    ProximoSeqSifDipoa();
end;

procedure TFmVSPalletAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPalletAdd.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FPallet := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSPalSta, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrVSRibCla, Dmod.MyDB);
  ReopenRibCla();
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  Agora := DModG.ObtemAgora();
  TPDataFab.Date := Trunc(Agora) + DmModVS.QrVsSifDipoaDdValid.Value;
end;

procedure TFmVSPalletAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPalletAdd.ProximoSeqSifDipoa();
var
  GraGruX, SeqSifDipoa: Integer;
begin
  GraGruX := QrVSRibClaGraGruX.Value;
  if GraGruX > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT MAX(SeqSifDipoa) SeqSifDipoa ',
    'FROM vspalleta ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    SeqSifDipoa := QrLocSeqSifDipoa.Value + 1;
    EdSeqSifDipoa.ValueVariant := SeqSifDipoa;
  end else
    Geral.MB_Aviso('Informe o artigo!');
end;

procedure TFmVSPalletAdd.ReopenRibCla();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibCla, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cou.PrevPcPal, UsaSifDipoa, ',
  'ggx.GraGruY ',
  'FROM gragrux ggx ',
  'LEFT JOIN vsribcla wmp ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'WHERE ggx.GragruY IN (' +
  Geral.FF0(CO_GraGruY_1536_VSCouCal) + ',' +
  //Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
  Geral.FF0(CO_GraGruY_3072_VSRibCla)// + ',' +
  //Geral.FF0(CO_GraGruY_6144_VSFinCla) +
  + ')',
  'ORDER BY ggx.GragruY DESC, NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSPalletAdd.SBClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliStat.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliStat.Text := IntToStr(VAR_ENTIDADE);
    CBCliStat.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPalletAdd.SbObsSifDipoaClick(Sender: TObject);
begin
  DefineObsSifDipoa();
end;

procedure TFmVSPalletAdd.SbSeqSifDipoaClick(Sender: TObject);
begin
  ProximoSeqSifDipoa();
end;

procedure TFmVSPalletAdd.SbGraGruXClick(Sender: TObject);
var
  GraGruY, GraGruX: Integer;
begin
  VAR_CADASTRO := 0;
  //
  GraGruY := 0;
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    GraGruY := QrVSRibClaGraGruY.Value;
  Grade_Jan.MostraFormGraGruY(GraGruY, GraGruX, '');
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrVSRibCla, VAR_CADASTRO);
    //
    EdGraGruX.SetFocus;
  end;
end;

(*
object QrGraGruX_: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
    'FROM vsnatcad wmp'
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
  Left = 636
  Top = 68
  object QrGraGruX_GraGru1: TIntegerField
    FieldName = 'GraGru1'
  end
  object QrGraGruX_Controle: TIntegerField
    FieldName = 'Controle'
  end
  object QrGraGruX_NO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrGraGruX_SIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrGraGruX_CODUSUUNIDMED: TIntegerField
    FieldName = 'CODUSUUNIDMED'
  end
  object QrGraGruX_NOMEUNIDMED: TWideStringField
    FieldName = 'NOMEUNIDMED'
    Size = 30
  end
end
object DsGraGruX_: TDataSource
  DataSet = QrGraGruX_
  Left = 636
  Top = 116
end

*)
end.
