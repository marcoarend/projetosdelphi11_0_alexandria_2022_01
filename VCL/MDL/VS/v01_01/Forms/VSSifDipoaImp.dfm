object FmVSSifDipoaImp: TFmVSSifDipoaImp
  Left = 339
  Top = 185
  Caption = 'SIF-DIPOA-003 :: Impress'#245'es SIF/DIPOA'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 284
        Height = 32
        Caption = 'Impress'#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 284
        Height = 32
        Caption = 'Impress'#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 284
        Height = 32
        Caption = 'Impress'#245'es SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 384
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtTudoClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 121
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object LaVSRibCla: TLabel
        Left = 8
        Top = 40
        Width = 84
        Height = 13
        Caption = 'Artigo (Reduzido):'
      end
      object Label1: TLabel
        Left = 8
        Top = 80
        Width = 92
        Height = 13
        Caption = 'Pallets espec'#237'ficos:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdEmpresaRedefinido
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 385
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 67
        Top = 56
        Width = 382
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object GroupBox3: TGroupBox
        Left = 457
        Top = 10
        Width = 256
        Height = 68
        Caption = ' Per'#237'odo: '
        TabOrder = 4
        object TPDataIni: TdmkEditDateTimePicker
          Left = 8
          Top = 40
          Width = 112
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 45288.000000000000000000
          Time = 0.777157974502188200
          TabOrder = 1
          OnClick = TPDataIniClick
          OnChange = TPDataIniChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object CkDataIni: TCheckBox
          Left = 8
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data inicial'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CkDataIniClick
        end
        object CkDataFim: TCheckBox
          Left = 124
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data final'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CkDataFimClick
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 124
          Top = 40
          Width = 112
          Height = 21
          Date = 45288.000000000000000000
          Time = 0.777203761601413100
          TabOrder = 3
          OnClick = TPDataFimClick
          OnChange = TPDataFimChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
      object RGModelo: TRadioGroup
        Left = 716
        Top = 10
        Width = 90
        Height = 67
        Caption = ' Modelo: '
        ItemIndex = 0
        Items.Strings = (
          'N/I'
          'A')
        TabOrder = 5
      end
      object EdPallets: TdmkEdit
        Left = 8
        Top = 96
        Width = 797
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object DBGBags: TDBGrid
      Left = 0
      Top = 121
      Width = 812
      Height = 346
      Align = alClient
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Pallet'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SeqSifDipoa'
          Title.Caption = 'N'#186' Identif.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFab'
          Title.Caption = 'Validade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ObsSifDipoa'
          Title.Caption = 'Observa'#231#227'o'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Dt/h in'#237'cio'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmp.GraGruX, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, cou.PrevPcPal'
      'FROM vsribcla wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'WHERE ggx.GragruY IN (1536,2048)'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 232
    Top = 188
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrGraGruXPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrGraGruXUsaSifDipoa: TSmallintField
      FieldName = 'UsaSifDipoa'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 232
    Top = 236
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 40
    Top = 233
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrVSPalletAfterScroll
    SQL.Strings = (
      'SELECT '
      
        'cou.ArtigoImp, GROUP_CONCAT(DISTINCT Marca ORDER BY Marca ASC SE' +
        'PARATOR ", ") MARCAS, '
      'vmi.Pallet, '
      'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) '
      'PesoKg, let.GraGruX,  let.DataFab, '
      'MIN(vmi.DataHora) MIN_DataHora,'
      'MAX(vmi.DataHora) MAX_DataHora,'
      'let.SeqSifDipoa, let.DataFab, let.ObsSifDipoa,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, '
      ' CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS  '
      'FROM vsmovits vmi'
      'LEFT JOIN vspalleta let ON let.Codigo=vmi.Pallet  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'WHERE vmi.Pallet > 0 '
      'AND let.Empresa=-11'
      'AND let.Codigo IN (12, 14, 15, 16, 17)'
      'GROUP BY vmi.Pallet'
      'ORDER BY NO_PRD_TAM_COR, let.SeqSifDipoa  ')
    Left = 140
    Top = 185
    object QrVSPalletPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPalletPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletSeqSifDipoa: TIntegerField
      FieldName = 'SeqSifDipoa'
    end
    object QrVSPalletObsSifDipoa: TWideStringField
      FieldName = 'ObsSifDipoa'
      Size = 60
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrVSPalletDataFab: TDateField
      FieldName = 'DataFab'
    end
    object QrVSPalletMARCAS: TWideMemoField
      FieldName = 'MARCAS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrVSPalletMIN_DataHora: TDateTimeField
      FieldName = 'MIN_DataHora'
    end
    object QrVSPalletMAX_DataHora: TDateTimeField
      FieldName = 'MAX_DataHora'
    end
  end
  object QrPals: TMySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 324
    object QrPalsPallet: TIntegerField
      FieldName = 'Pallet'
    end
  end
  object frxSIF_DIPOA_003_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 45288.730250810190000000
    ReportOptions.LastChange = 45288.730250810190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_LogoFilial_Find> = True then'
      '    Picture1.LoadFromFile(<VARF_LogoFilial_Path>);'
      '  if <VARF_LogoSifDipoa_Find> = True then'
      '    Picture2.LoadFromFile(<VARF_LogoSifDipoa_Path>);  '
      'end.')
    OnGetValue = frxSIF_DIPOA_003_AGetValue
    Left = 140
    Top = 284
    Datasets = <
      item
        DataSet = frxDsVSPallet
        DataSetName = 'frxDsVSPallet'
      end
      item
        DataSet = frxDsVsSifDipoa
        DataSetName = 'frxDsVsSifDipoa'
      end
      item
        DataSet = frxDSVMI
        DataSetName = 'frxDSVMI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 521.574803149606300000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = frxDsVSPallet
        DataSetName = 'frxDsVSPallet'
        RowCount = 0
        StartNewPage = True
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOME_ARTIGO]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Top = 68.031540000000000000
          Width = 275.905690000000000000
          Height = 113.385777950000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Registro no Minist'#233'rio da Agricultura SIF/DIPOA sob n'#186' [frxDsVsS' +
              'ifDipoa."Registro"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 226.771653540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DATA DA PRODU'#199#195'O DO LOTE')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 64.252010000000000000
          Width = 207.874015750000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DATA DE VALIDADE')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 102.047310000000000000
          Width = 226.771653540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 102.047310000000000000
          Width = 207.874015750000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE / RASTREABILIDADE')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 79.370130000000000000
          Width = 226.771653540000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DATA_PRODUCAO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 79.370130000000000000
          Width = 207.874015750000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DATA_VALIDADE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 117.165430000000000000
          Width = 226.771653540000000000
          Height = 52.913385826771650000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PESO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 117.165430000000000000
          Width = 207.874015750000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_LOTES]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Picture2: TfrxPictureView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 260.787570000000000000
          Width = 196.535560000000000000
          Height = 170.078727950000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 181.417440000000000000
          Width = 434.645950000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial Black'
          Font.Style = [fsBold, fsItalic, fsUnderline]
          Frame.Style = fsDash
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 1.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PRODUTO N'#195'O COMEST'#205'VEL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 519.685039370078700000
          Width = 718.110236220000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 185.196970000000000000
          Width = 275.905690000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA_NOMEFILIAL]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Top = 215.433210000000000000
          Width = 275.905690000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'UNIDADE DE BENEFICIAMENTO DE '
            'PRODUTOS N'#195'O COMEST'#205'VEIS')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 260.787570000000000000
          Width = 275.905690000000000000
          Height = 105.826840000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CNPJ/MF [VARF_EMPRESA_CNPJ_CPF]'
            'Endere'#231'o: [VARF_ENDERECO]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 226.771800000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' DE IDENTIFICA'#199#195'O')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 245.669450000000000000
          Width = 226.771653540000000000
          Height = 68.031515590000000000
          DisplayFormat.FormatStr = '000;-000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPallet."SeqSifDipoa"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 400.630180000000000000
          Width = 434.645798660000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#195'O CONT'#201'M GL'#218'TEN PARA USO INDUSTRIAL')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 226.771800000000000000
          Width = 200.314938660000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'MINIST'#201'RIO DA'
            'AGRICULTURA')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 442.205010000000000000
          Width = 718.110700000000000000
          Height = 56.692950000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -64
          Font.Name = 'CalIbri'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A R G')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 381.732530000000000000
          Width = 506.457020000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Ingredientes: Pele conservada de bovino, hidr'#243'xido de c'#225'lcio e '#225 +
              'gua.')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 419.527830000000000000
          Width = 506.457020000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Produto armazenado em local seco e arejado, em temperatura ambie' +
              'nte at'#233' 40'#730'C.')
          ParentFont = False
        end
      end
      object DD001: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 600.945270000000000000
        Width = 718.110700000000000000
        DataSet = frxDSVMI
        DataSetName = 'frxDSVMI'
        RowCount = 0
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSVMI."Controle"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSVMI."PesoKg"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 83.149660000000000000
          Height = 15.118110240000000000
          DataField = 'NO_SerieFch'
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDSVMI."NO_SerieFch"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataField = 'Ficha'
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSVMI."Ficha"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataField = 'Marca'
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDSVMI."Marca"]')
          ParentFont = False
        end
      end
      object HD001: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 563.149970000000000000
        Width = 718.110700000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 83.149660000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S'#233'rie ficha')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ficha')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = frxDSVMI
          DataSetName = 'frxDSVMI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
      end
      object FT001: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 638.740570000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxDsVSPallet: TfrxDBDataset
    UserName = 'frxDsVSPallet'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'GraGruX=GraGruX'
      'DataHora=DataHora'
      'SeqSifDipoa=SeqSifDipoa'
      'ObsSifDipoa=ObsSifDipoa'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_STATUS=NO_STATUS'
      'ArtigoImp=ArtigoImp'
      'DataFab=DataFab')
    DataSet = QrVSPallet
    BCDToCurrency = False
    DataSetOptions = []
    Left = 140
    Top = 236
  end
  object frxDsVsSifDipoa: TfrxDBDataset
    UserName = 'frxDsVsSifDipoa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'Registro=Registro'
      'DdValid=DdValid'
      'ArqLogoFilial=ArqLogoFilial'
      'ArqLogoSIF=ArqLogoSIF'
      'QualDtFab=QualDtFab')
    DataSet = DmModVS.QrVsSifDipoa
    BCDToCurrency = False
    DataSetOptions = []
    Left = 140
    Top = 332
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, '
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, '
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, '
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, '
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, '
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, '
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, '
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, '
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, '
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, '
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, '
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, '
      'CAST(vmi.IxxMovIX AS UNSIGNED) IxxMovIX, '
      'CAST(vmi.IxxFolha AS SIGNED) IxxFolha, '
      'CAST(vmi.IxxLinha AS SIGNED) IxxLinha, '
      'CAST(vmi.CusFrtAvuls AS DECIMAL (15,4)) CusFrtAvuls, '
      'CAST(vmi.CusFrtMOEnv AS DECIMAL (15,4)) CusFrtMOEnv, '
      'CAST(vmi.CusFrtMORet AS DECIMAL (15,4)) CusFrtMORet, '
      'CAST(vmi.CustoMOPc AS DECIMAL (15,6)) CustoMOPc, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome' +
        '))) '
      'NO_PRD_TAM_COR, '
      ''
      'IF(vmi.Terceiro <> 0, '
      '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome), '
      '  mfc.Nome) NO_FORNECE, '
      ''
      'IF(vmi.ClientMO <> 0, '
      '  IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome), '
      '  "") NO_ClientMO, '
      ''
      'vsf.Nome NO_SerieFch, '
      'vsp.Nome NO_Pallet'
      'FROM bluederm_panorama.vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN entidades   frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab '
      ''
      'LEFT JOIN entidades   cmo ON cmo.Codigo=vmi.ClientMO '
      ''
      'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch '
      ''
      ''
      'WHERE vmi.Pallet=14'
      ''
      ''
      ''
      ''
      ''
      'ORDER BY NO_Pallet, Controle ')
    Left = 232
    Top = 284
    object QrVMINO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVMIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVMICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrVMITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVMISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrVMIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIFornecMO: TLargeintField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrVMICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      Required = True
    end
    object QrVMICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
    end
    object QrVMIValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrVMICustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrVMIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      Required = True
    end
    object QrVMIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      Required = True
    end
    object QrVMIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      Required = True
    end
    object QrVMIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      Required = True
    end
    object QrVMIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      Required = True
    end
    object QrVMIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      Required = True
    end
    object QrVMIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      Required = True
    end
    object QrVMIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      Required = True
    end
    object QrVMINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrVMIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMIPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVMIPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVMIPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVMICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      Required = True
    end
    object QrVMIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrVMIStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVMIItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVMIVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrVMINFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVMIVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrVMIJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrVMIJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrVMIJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrVMIRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrVMIRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrVMIRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrVMIGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrVMIRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrVMIJmpGGX: TLargeintField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrVMIDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVMIIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrVMIIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrVMIIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrVMICusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrVMICusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      Required = True
    end
    object QrVMICusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      Required = True
    end
    object QrVMICustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
      Required = True
    end
    object QrVMINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVMINO_ClientMO: TWideStringField
      FieldName = 'NO_ClientMO'
      Size = 100
    end
    object QrVMINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVMINO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
  end
  object frxDSVMI: TfrxDBDataset
    UserName = 'frxDSVMI'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'ClientMO=ClientMO'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'CustoPQ=CustoPQ'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'CustoMOM2=CustoMOM2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMulFrnCab=VSMulFrnCab'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'JmpMovID=JmpMovID'
      'JmpNivel1=JmpNivel1'
      'JmpNivel2=JmpNivel2'
      'RmsMovID=RmsMovID'
      'RmsNivel1=RmsNivel1'
      'RmsNivel2=RmsNivel2'
      'GGXRcl=GGXRcl'
      'RmsGGX=RmsGGX'
      'JmpGGX=JmpGGX'
      'DtCorrApo=DtCorrApo'
      'IxxMovIX=IxxMovIX'
      'IxxFolha=IxxFolha'
      'IxxLinha=IxxLinha'
      'CusFrtAvuls=CusFrtAvuls'
      'CusFrtMOEnv=CusFrtMOEnv'
      'CusFrtMORet=CusFrtMORet'
      'CustoMOPc=CustoMOPc'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_FORNECE=NO_FORNECE'
      'NO_ClientMO=NO_ClientMO'
      'NO_SerieFch=NO_SerieFch'
      'NO_Pallet=NO_Pallet')
    DataSet = QrVMI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 232
    Top = 336
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        'cou.ArtigoImp, GROUP_CONCAT(DISTINCT Marca ORDER BY Marca ASC SE' +
        'PARATOR ", ") MARCAS, '
      'vmi.Pallet, '
      'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) '
      'PesoKg, let.GraGruX,  let.DataFab, '
      'MIN(vmi.DataHora) MIN_DataHora,'
      'MAX(vmi.DataHora) MAX_DataHora,'
      'let.SeqSifDipoa, let.DataFab, let.ObsSifDipoa,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, '
      ' CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS  '
      'FROM vsmovits vmi'
      'LEFT JOIN vspalleta let ON let.Codigo=vmi.Pallet  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'WHERE vmi.Pallet > 0 '
      'AND let.Empresa=-11'
      'AND let.Codigo IN (12, 14, 15, 16, 17)'
      'GROUP BY vmi.Pallet'
      'ORDER BY NO_PRD_TAM_COR, let.SeqSifDipoa  ')
    Left = 40
    Top = 185
    object QrPesqPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPesqPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrPesqPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPesqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPesqSeqSifDipoa: TIntegerField
      FieldName = 'SeqSifDipoa'
      DisplayFormat = '000;-000; '
    end
    object QrPesqObsSifDipoa: TWideStringField
      FieldName = 'ObsSifDipoa'
      Size = 60
    end
    object QrPesqNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPesqNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrPesqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPesqNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrPesqArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrPesqDataFab: TDateField
      FieldName = 'DataFab'
    end
    object QrPesqMARCAS: TWideMemoField
      FieldName = 'MARCAS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqMIN_DataHora: TDateTimeField
      FieldName = 'MIN_DataHora'
    end
    object QrPesqMAX_DataHora: TDateTimeField
      FieldName = 'MAX_DataHora'
    end
  end
  object PMImprime: TPopupMenu
    Left = 248
    Top = 519
    object Itematual1: TMenuItem
      Caption = 'Item &atual'
      OnClick = Itematual1Click
    end
    object Selecionados1: TMenuItem
      Caption = 'Itens &selecionados'
      OnClick = Selecionados1Click
    end
    object odosdapesquisa1: TMenuItem
      Caption = 'Todos da &pesquisa'
      OnClick = odosdapesquisa1Click
    end
  end
end
