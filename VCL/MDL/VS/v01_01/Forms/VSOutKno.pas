unit VSOutKno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, dmkDBGridZTO, BlueDermConsts, UnGrl_Consts, UnAppEnums;

type
  TFmVSOutKno = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    DsVSPallet: TDataSource;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrVSMovItsMediaM2: TFloatField;
    BtReabre: TBitBtn;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsClientMO: TIntegerField;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    GBDadosItem1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    Label53: TLabel;
    Label49: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    RGTipoCouro: TRadioGroup;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    EdPsqPallet: TdmkEdit;
    EdPsqFicha: TdmkEdit;
    EdPsqIMEI: TdmkEdit;
    EdImeiSrc: TdmkEdit;
    CkDataMinima: TCheckBox;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel14: TPanel;
    Label4: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Label14: TLabel;
    Label9: TLabel;
    Label17: TLabel;
    Label12: TLabel;
    EdSrcMovID: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcGGX: TdmkEdit;
    QrVSMovItsNFeSer: TSmallintField;
    QrVSMovItsNFeNum: TIntegerField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    Label38: TLabel;
    EdCouNiv2: TdmkEditCB;
    Label39: TLabel;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    CBCouNiv2: TdmkDBLookupComboBox;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXGrandeza: TFloatField;
    QrCambioMda: TMySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    Panel15: TPanel;
    Label48: TLabel;
    Label26: TLabel;
    EdPedItsVda: TdmkEditCB;
    CBPedItsVda: TdmkDBLookupComboBox;
    EdItemNFe: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdValorT: TdmkEdit;
    LaValorT: TLabel;
    SbValorT: TSpeedButton;
    EdMediaM2Pc: TdmkEdit;
    Label18: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    Label19: TLabel;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    DsVSPedIts: TDataSource;
    QrVSPedIts: TMySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    QrVSPedItsGraGruX: TIntegerField;
    QrVSPedItsPrecoTipo: TSmallintField;
    QrVSPedItsPrecoMoeda: TIntegerField;
    QrVSPedItsPrecoVal: TFloatField;
    QrVSPedItsSaldoQtd: TFloatField;
    QrVSPedItsPercDesco: TFloatField;
    QrVSPedItsPercTrib: TFloatField;
    CkContinuar: TCheckBox;
    Label22: TLabel;
    EdPrecoMoeda: TdmkEditCB;
    CBPrecoMoeda: TdmkDBLookupComboBox;
    EdPrecoVal: TdmkEdit;
    Label15: TLabel;
    Label23: TLabel;
    EdMdaTotal: TdmkEdit;
    EdPercDesco: TdmkEdit;
    Label24: TLabel;
    Label25: TLabel;
    EdPercTrib: TdmkEdit;
    EdMdaBruto: TdmkEdit;
    Label27: TLabel;
    Label28: TLabel;
    EdMdaLiqui: TdmkEdit;
    EdCambio: TdmkEdit;
    Label29: TLabel;
    EdNacTotal: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    EdNacBruto: TdmkEdit;
    EdNacLiqui: TdmkEdit;
    Label32: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
    procedure RGTipoCouroClick(Sender: TObject);
    procedure EdPsqPalletChange(Sender: TObject);
    procedure EdPsqFichaChange(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdImeiSrcRedefinido(Sender: TObject);
    procedure EdPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrStqCenCadBeforeClose(DataSet: TDataSet);
    procedure QrStqCenCadAfterScroll(DataSet: TDataSet);
    procedure EdStqCenLocRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure EdPrecoMoedaRedefinido(Sender: TObject);
    procedure EdPrecoValKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercDescoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPedItsVdaRedefinido(Sender: TObject);
    procedure EdPrecoValRedefinido(Sender: TObject);
    procedure EdPercDescoRedefinido(Sender: TObject);
    procedure EdPercTribRedefinido(Sender: TObject);
    procedure EdCambioRedefinido(Sender: TObject);
    procedure EdItemNFeRedefinido(Sender: TObject);

  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure CalculaParcial();
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure ReopenVSMovIts((*GraGruX: Integer*));
    procedure HabilitaInclusao();
    procedure ReopenGGXY();
    procedure CalculaValorT();
    procedure CalculaValorFaturado();
    procedure DefineDadosItemPedido();
    procedure InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2, PesoKg:
              Double);
    procedure DefineGGXRcl();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FPedido: Integer;
  end;

  var
  FmVSOutKno: TFmVSOutKno;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
{$IfDef sAllVS}UnVS_PF, {$EndIf}
VSOutCab, UnVS_CRC_PF, CalcParc4Val;

{$R *.DFM}

procedure TFmVSOutKno.BtOKClick(Sender: TObject);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0;
  JmpNivel2: Integer = 0;
  JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0;
  RmsNivel2: Integer = 0;
  RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste;
  GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, CliVenda,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, PedItsVda, ItemNFe, VSMulFrnCab, ClientMO, GGXRcl, FornecMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  MyEdPecas: TdmkEdit;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha, StqCenLoc: Integer;
begin
  StqCenLoc      := CO_STQCENLOC_ZERO;
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  SrcGGX         := EdSrcGGX.ValueVariant;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVSMovItsClientMO.Value;
  FornecMO       := CO_FORNECMO_ZERO;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidVenda;
  MovimNiv       := eminSemNiv;
  Pallet         := QrVSMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  FUltGGX        := GraGruX;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorMP        := -EdValorT.ValueVariant;
  ValorT         := ValorMP;
  //
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Marca          := QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
(*
  if QrVSMovItsPedItsFin.Value <> 0 then
    PedItsVda := QrVSMovItsPedItsFin.Value
  else
    PedItsVda := QrVSMovItsPedItsLib.Value;
*)
  //
  PedItsVda      := EdPedItsVda.ValueVariant;
  if MyObjects.FIC((FPedido <> 0) and (PedItsVda = 0), EdPedItsVda,
  'Item de pedido n�o informado para o pedido desta sa�da!') then
  begin
    //if Geral.MB_Pergunta('Deseja continuar sem informar o item do pedido!') <> ID_YES then
      Exit;
  end;
  if PedItsVda > 0 then
  begin
(*
    QtdDifGGX := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      //
*)
      if QrVSMovItsGraGruX.Value <> QrVSPedItsGraGruX.Value then
(*[        QtdDifGGX := QtdDifGGX + 1;
    end;
    if QtdDifGGX > 0 then
*)
    begin
(*
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(QtdDifGGX) +
      ' itens em que o reduzido difere entre o pedido e o selecionado!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
*)
      if Geral.MB_Pergunta('O reduzido difere entre o pedido e o selecionado!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
    end;
  end;
  //
  if QrVSMovItsPecas.Value = 0 then
    MyEdPecas := nil
  else
    MyEdPecas := EdPecas;
  if VS_CRC_PF.VSFic(GraGruX, Empresa, 0, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, nil, nil(*EdPallet*), EdFicha, MyEdPecas,
  nil, nil, nil(*EdValorT*), ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  nil) then
    Exit;
  if MyObjects.FIC((QrVSMovItsSdoVrtArM2.Value > 0) and (AreaM2 = 0),
  EdAreaM2, 'Informe a �rea!') then
    Exit else
  if MyObjects.FIC((QrVSMovItsSdoVrtPeso.Value > 0) and (PesoKg = 0),
  EdPesoKg, 'Informe o peso!') then
    Exit;
  //
  if MyObjects.FIC(GGXRcl = 0, EdGGXRcl,
  'Informe o material usado para emitir NFe!') then
    Exit;
  //
  ItemNFe     := EdItemNFe.ValueVariant;
  if MyObjects.FIC(ItemNFe = 0, EdItemNFe,
  'Informe o item da NFe!') then
    Exit;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
(*
  if Dmod.VSFic(GraGruX, Empresa, Terceiro, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT) then
    Exit;
*)

  //
  PedItsVda      := EdPedItsVda.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_FALSE_ExigeStqLoc,
  iuvpei092(*Item de venda de produto com rastreio*)) then
  begin
    //
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
{$IfDef sAllVS}
    InsereVSOutFat(Controle, Pecas, AreaM2, AreaP2, PesoKg);
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
    FmVSOutCab.AtualizaNFeItens(MovimCod);
    FmVSOutCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      //
      EdValorT.Enabled           := False;
      LaValorT.Enabled           := False;
      //
      EdSrcMovID.ValueVariant    := 0;
      EdSrcNivel1.ValueVariant   := 0;
      EdSrcNivel2.ValueVariant   := 0;
      EdSrcGGX.ValueVariant      := 0;
      //
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdPallet.ValueVariant      := 0;//DefineProximoPallet();
      //
      EdGraGruX.SetFocus;
      //
      ReopenVSMovIts();
    end else
      Close;
  end;
end;

procedure TFmVSOutKno.BtReabreClick(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutKno.CalculaParcial();
var
  Item, AreaTotal: String;
  It: Integer;
  ArIt: Double;
begin
  AreaTotal := '0,00';
  if InputQuery('Item da NFe', 'Informe o item da NFe:', Item) then
  begin
    if InputQuery('Area total', 'Informe a area total:', AreaTotal) then
    begin
      It := Geral.IMV(Item);
      ArIt := 0;
      with FmVSOutCab do
      begin
        QrVSOutIts.First;
        while not QrVSOutIts.Eof do
        begin
          if QrVSOutItsItemNFe.Value = It then
            ArIt := ArIt + QrVSOutItsAreaM2.Value;
          QrVSOutIts.Next;
        end;
      end;
      // Parei Aqui!
      Application.CreateForm(TFmCalcParc4Val, FmCalcParc4Val);
      FmCalcParc4Val.FCalcAuto := True;
      FmCalcParc4Val.FCalcExec := calcexec1;
      FmCalcParc4Val.EdA2.Text := AreaTotal;
      FmCalcParc4Val.EdB2.ValueVariant := -ArIt;//-FQrCab.FieldByName('AreaM2').AsFloat;
      FmCalcParc4Val.Ed04Peca.ValueVariant := QrVSMovItsSdoVrtPeca.Value;
      FmCalcParc4Val.Ed04ArM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
      FmCalcParc4Val.ShowModal;
      //
      if FmCalcParc4Val.FUsaDados then
      begin
        EdPecas.ValueVariant := FmCalcParc4Val.Ed03Peca.ValueVariant;
        EdAreaM2.ValueVariant := FmCalcParc4Val.Ed03ArM2.ValueVariant;
      end;
      FmCalcParc4Val.Destroy;
    end;
  end;
end;

procedure TFmVSOutKno.CalculaValorFaturado();
var
  PrecoTipo: Integer;
  //
  PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio, NacTotal, NacBruto, NacLiqui,
  Pecas, PesoKg, AreaM2, AreaP2, ValTot, PrecoVal, PercDesco: Double;
begin
  PrecoTipo := QrVSPedItsPrecoTipo.Value;
  Pecas     := EdPecas.ValueVariant;
  PesoKg    := EdPesoKg.ValueVariant;
  AreaM2    := EdAreaM2.ValueVariant;
  AreaP2    := EdAreaP2.ValueVariant;
  ValTot    := EdValorT.ValueVariant;
  PrecoVal  := EdPrecoVal.ValueVariant;
  PercDesco := EdPercDesco.ValueVariant;
  //
  PercTrib := EdPercTrib.ValueVariant;
  Cambio   := EdCambio.ValueVariant;
  //
  MdaTotal := VS_PF.CalculaValorCouros(TTipoCalcCouro(PrecoTipo), PrecoVal,
    Pecas, PesoKg, AreaM2, AreaP2, ValTot);
  MdaBruto := Geral.RoundC(MdaTotal * (1 - (PercDesco / 100)), 2);
  MdaLiqui := Geral.RoundC(MdaBruto * (1 - (PercTrib / 100)), 2);
  //
  NacTotal := Geral.RoundC(MdaTotal * Cambio, 2);
  NacBruto := Geral.RoundC(MdaBruto * Cambio, 2);
  NacLiqui := Geral.RoundC(MdaLiqui * Cambio, 2);
  //
  EdMdaTotal.ValueVariant := MdaTotal;
  EdMdaBruto.ValueVariant := MdaBruto;
  EdMdaLiqui.ValueVariant := MdaLiqui;
  //
  EdNacTotal.ValueVariant := NacTotal;
  EdNacBruto.ValueVariant := NacBruto;
  EdNacLiqui.ValueVariant := NacLiqui;
end;

procedure TFmVSOutKno.CalculaValorT();
var
  Pecas, AreaM2, PesoKg, Valor, Media: Double;
begin
  begin
    Pecas  := EdPecas.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    PesoKg := EdPesoKg.ValueVariant;
    Valor := 0;
    (*
    case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
      encPecas: ;//Valor := 0;
      encArea: ;//Valor := 0;
      encValor:
      begin
    *)
        if (QrVSMovItsAreaM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
          Valor := AreaM2 * (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
        else
        if QrVSMovItsPesoKg.Value > 0 then
          Valor := PesoKg * (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value)
        else
        if QrVSMovItsPecas.Value > 0 then
          Valor := Pecas * (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
    (*
      end;
      else
      begin
        //Valor := 0;
        Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
      end;
    end;
    *)
    EdValorT.ValueVariant := Valor;
    //
    if Pecas > 0 then
      Media := AreaM2 / Pecas
    else
      Media := 0;
    EdMediaM2Pc.ValueVariant := Media;
  end;
end;

procedure TFmVSOutKno.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSOutKno.CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutKno.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  if (QrVSMovItsSdoVrtPeso.Value > 0) and (QrVSMovItsAreaM2.Value = 0) then
    EdPesoKg.ValueVariant := QrVSMovItsSdoVrtPeso.Value;
  //
  //Valor := 0;
  (*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
  *)
      if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
        Valor := QrVSMovItsSdoVrtArM2.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
      else
      if QrVSMovItsPecas.Value > 0 then
        Valor := QrVSMovItsSdoVrtPeca.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  (*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
    end;
  end;
  *)
  EdValorT.ValueVariant := Valor;
end;

procedure TFmVSOutKno.DBGrid1DblClick(Sender: TObject);
begin
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;
  DefineGGXRcl();
  //
  try
    EdPecas.SetFocus;
  except
    //
  end;
end;

procedure TFmVSOutKno.DefineDadosItemPedido;
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    //FDefinindoDadosItemPedido := True;
    EdPrecoMoeda.ValueVariant := QrVSPedItsPrecoMoeda.Value;
    CBPrecoMoeda.KeyValue     := QrVSPedItsPrecoMoeda.Value;
    EdPrecoVal.ValueVariant   := QrVSPedItsPrecoVal.Value;
    EdPercDesco.ValueVariant  := QrVSPedItsPercDesco.Value;
    EdPercTrib.ValueVariant   := QrVSPedItsPercTrib.Value;
  end;
end;

procedure TFmVSOutKno.DefineGGXRcl();
var
  ItemNFe: Integer;
begin
  ItemNFe := EdItemNFe.ValueVariant;
  if FmVSOutCab.QrVSOutIts.Locate('ItemNFe', ItemNFe, []) then
  begin
    EdGGXRcl.ValueVariant := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    CBGGXRcl.KeyValue := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    EdGGXRcl.Enabled := False;
    CBGGXRcl.Enabled := False;
  end else
  begin
    EdGGXRcl.Enabled := True;
    CBGGXRcl.Enabled := True;
  end;
end;

{
function TFmVSOutKno.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmVSOutKno.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutKno.EdStqCenCadRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.EdStqCenLocRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    CalculaValorT();
end;

procedure TFmVSOutKno.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutKno.EdAreaM2Redefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdCambioRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSOutKno.EdImeiSrcRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.EdItemNFeRedefinido(Sender: TObject);
begin
  DefineGGXRcl();
end;

procedure TFmVSOutKno.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
  if Key = VK_F3 then
    CalculaParcial();
end;

procedure TFmVSOutKno.EdPecasRedefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPedItsVdaRedefinido(Sender: TObject);
begin
  DefineDadosItemPedido();
end;

procedure TFmVSOutKno.EdPercDescoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutKno.EdPercDescoRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPercTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutKno.EdPercTribRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PecasOri, PecasDst, PesoDst, PesoOri: Double;
begin
  if Key = VK_F4 then
  begin
    if QrVSMovItsPecas.Value = 0 then
    begin
      EdPesoKg.ValueVariant := QrVSMovItsSdoVrtPeso.Value;
    end else
    begin
      PesoOri  := QrVSMovItsSdoVrtPeso.Value;
      PecasOri := QrVSMovItsSdoVrtPeca.Value;
      PecasDst := EdPecas.ValueVariant;
      PesoDst  := 0;
      //
      if PecasOri > 0 then
      begin
        PesoDst := PesoOri * (PecasDst / PecasOri);
        EdPesoKg.ValueVariant := PesoDst;
      end;
    end;
  end;
end;

procedure TFmVSOutKno.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorT();
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutKno.EdPrecoMoedaRedefinido(Sender: TObject);
begin
  if EdPrecoMoeda.ValueVariant = Dmod.QrControle.FieldByName('MoedaBr').AsInteger then
  begin
    EdCambio.Enabled := False;
    EdCambio.ValueVariant := 1.0000000000;
  end else
    EdCambio.Enabled := True;
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPrecoValKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutKno.EdPrecoValRedefinido(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutKno.EdPsqFichaChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.EdPsqPalletChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutKno.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutKno.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutKno.FormCreate(Sender: TObject);
const
  //Default = 2;
  Default = -1;
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.Align := alClient;
  FUltGGX := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  ReopenGGXY();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vspalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
  //
  VS_CRC_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS - 1, Default);
end;

procedure TFmVSOutKno.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutKno.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmVSOutKno.InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2,
  PesoKg: Double);
var
  PrecoTipo, PrecoMoeda: Integer;
  Qtde, PrecoVal, PercDesco, PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio,
  NacTotal, NacBruto, NacLiqui, (* TPecas, TPesoKg, TAreaM2, TAreaP2, QTot,*) Fator:
  Double;
  SQLType: TSQLType;
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    SQLType        := ImgTipo.SQLType;
    //
(*
    TPecas         := EdSdoVrtPeca.ValueVariant;
    TPesoKg        := EdSdoVrtPeso.ValueVariant;
    TAreaM2        := EdSdoVrtArM2.ValueVariant;
    TAreaP2        := EdSdoVrtArP2.ValueVariant;
*)
    PrecoTipo      := QrVSPedItsPrecoTipo.Value;
(*
    QTot           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      TPecas, TPesoKg, TAreaM2, TAreaP2));
*)
    Qtde           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      Pecas, PesoKg, AreaM2, AreaP2));
    //
(*
    if QTot <> 0 then
      Fator := ABS(Qtde / QTot)
    else
      Fator  := 0;
*)
    Fator := 1;
    //
    PrecoMoeda     := EdPrecoMoeda.ValueVariant;
    PrecoVal       := EdPrecoVal.ValueVariant;
    PercDesco      := EdPercDesco.ValueVariant;
    PercTrib       := EdPercTrib.ValueVariant;
    MdaTotal       := EdMdaTotal.ValueVariant * Fator;
    MdaBruto       := EdMdaBruto.ValueVariant * Fator;
    MdaLiqui       := EdMdaLiqui.ValueVariant * Fator;
    Cambio         := EdCambio.ValueVariant;
    NacTotal       := EdNacTotal.ValueVariant * Fator;
    NacBruto       := EdNacBruto.ValueVariant * Fator;
    NacLiqui       := EdNacLiqui.ValueVariant * Fator;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsoutfat', False, [
    'Qtde', 'PrecoTipo', 'PrecoMoeda',
    'PrecoVal', 'PercDesco', 'PercTrib',
    'MdaTotal', 'MdaBruto', 'MdaLiqui',
    'Cambio', 'NacTotal', 'NacBruto',
    'NacLiqui'], [
    'Controle'], [
    Qtde, PrecoTipo, PrecoMoeda,
    PrecoVal, PercDesco, PercTrib,
    MdaTotal, MdaBruto, MdaLiqui,
    Cambio, NacTotal, NacBruto,
    NacLiqui], [
    Controle], True);
  end;
end;

procedure TFmVSOutKno.QrStqCenCadAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, QrStqCenCadCodigo.Value, 0);
end;

procedure TFmVSOutKno.QrStqCenCadBeforeClose(DataSet: TDataSet);
begin
  QrStqCenLoc.Close;
end;

procedure TFmVSOutKno.ReopenGGXY();
begin
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
  //
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
    //'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
end;

procedure TFmVSOutKno.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutKno.ReopenVSMovIts();
var
  Data: TDateTime;
  GraGruX, Empresa, Pallet, Ficha, IMEI, StqCenCad, StqCenloc, ImeiSrc, CouNiv1,
  CouNiv2: Integer;
  //SQL_GraGruX, SQL_Pallet, SQL_Ficha, SQL_IMEI: String;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  (*SQL_Pallet := '';
  SQL_Ficha  := '';
  SQL_IMEI   := '';*)
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  StqCenLoc  := EdStqCenLoc.ValueVariant;
  ImeiSrc    := EdImeiSrc.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  if CkDataMinima.Checked then
    Data := 0
  else
    Data := FmVSOutCab.QrVSOutCabDtVenda.Value;
  //
  VS_CRC_PF.ReopenIMEIsPositivos(QrVSMovIts, Empresa, GraGruX, Pallet, Ficha, IMEI,
    RGTipoCouro.ItemIndex, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1, CouNiv2,
    Data);
  //Geral.MB_SQL(Self, QrVSMovIts);
end;

procedure TFmVSOutKno.RGTipoCouroClick(Sender: TObject);
begin
  GBDadosItem1.Visible := RGTipoCouro.ItemIndex >= 0;
  ReopenGGXY();
end;

procedure TFmVSOutKno.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_CRC_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSOutKno.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  LaValorT.Enabled := True;
  EdValorT.SetFocus;
end;

procedure TFmVSOutKno.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
