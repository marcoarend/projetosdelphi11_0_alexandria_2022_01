unit VSGerArtFil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids;

type
  TFmVSGerArtFil = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Label6: TLabel;
    EdControle: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    RGMisturou: TdmkRadioGroup;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosCodigo: TIntegerField;
    QrAptosControle: TIntegerField;
    QrAptosMovimCod: TIntegerField;
    QrAptosMovimNiv: TIntegerField;
    QrAptosMovimTwn: TIntegerField;
    QrAptosEmpresa: TIntegerField;
    QrAptosTerceiro: TIntegerField;
    QrAptosCliVenda: TIntegerField;
    QrAptosMovimID: TIntegerField;
    QrAptosLnkNivXtr1: TIntegerField;
    QrAptosLnkNivXtr2: TIntegerField;
    QrAptosDataHora: TDateTimeField;
    QrAptosPallet: TIntegerField;
    QrAptosGraGruX: TIntegerField;
    QrAptosPecas: TFloatField;
    QrAptosPesoKg: TFloatField;
    QrAptosAreaM2: TFloatField;
    QrAptosAreaP2: TFloatField;
    QrAptosValorT: TFloatField;
    QrAptosSrcMovID: TIntegerField;
    QrAptosSrcNivel1: TIntegerField;
    QrAptosSrcNivel2: TIntegerField;
    QrAptosSdoVrtPeca: TFloatField;
    QrAptosSdoVrtArM2: TFloatField;
    QrAptosObserv: TWideStringField;
    QrAptosLk: TIntegerField;
    QrAptosDataCad: TDateField;
    QrAptosDataAlt: TDateField;
    QrAptosUserCad: TIntegerField;
    QrAptosUserAlt: TIntegerField;
    QrAptosAlterWeb: TSmallintField;
    QrAptosAtivo: TSmallintField;
    QrAptosFicha: TIntegerField;
    QrAptosMisturou: TSmallintField;
    QrAptosCustoMOKg: TFloatField;
    QrAptosCustoMOTot: TFloatField;
    QrAptosNO_PRD_TAM_COR: TWideStringField;
    QrAptosSdoVrtPeso: TFloatField;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    Label7: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdFicha: TdmkEdit;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure FechaPesquisa();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa: Integer;
    //
    procedure ReopenItensAptos();
  end;

  var
  FmVSGerArtFil: TFmVSGerArtFil;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSGerArtFil.BitBtn1Click(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSGerArtFil.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
{
  MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle? := EdControle.ValueVariant;
  Controle? := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_its', 'Controle', ImgTipo.SQLType, Controle);
ou > ? := UMyMod.BPGS1I32('cadastro_com_itens_its', 'Controle', '', '', tsPosNeg?, ImgTipo.SQLType, Controle);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'cadastro_com_itens_its', auto_increment?[
capos?], [
'Controle'], [
valores?], [
Controle], UserDataAlterweb?, IGNORE?
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
}
end;

procedure TFmVSGerArtFil.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtFil.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtFil.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtFil.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtFil.FechaPesquisa();
begin
  QrAptos.Close;
end;

procedure TFmVSGerArtFil.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtFil.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmVSGerArtFil.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtFil.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSGerArtFil.ReopenItensAptos();
var
  SQL_GraGruX, SQL_Terceiro, SQL_Ficha: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
  SQL_Ficha    := '';
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND wmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND wmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
  if EdFicha.ValueVariant <> 0 then
    SQL_Ficha  := 'AND wmi.Ficha=' + Geral.FF0(EdFicha.ValueVariant);

  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsmovits wmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  'AND SdoVrtPeca>0 ',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY wmi.Controle ',
  '']);
end;

end.
