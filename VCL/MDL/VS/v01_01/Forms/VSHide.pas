unit VSHide;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus,
  mySQLDbTables,
  dmkEditCB, dmkDBLookupComboBox, UnGrl_Vars, Data.DB,
  UMySQLModule, UnDmkEnums, UnAppEnums;

type
  TFmVSHide = class(TForm)
    PMVSMovIDLoc: TPopupMenu;
    CadastraLocalemCentrodeEstoque1: TMenuItem;
    HabilitaLocalemMovimID1: TMenuItem;
    procedure CadastraLocalemCentrodeEstoque1Click(Sender: TObject);
    procedure HabilitaLocalemMovimID1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FStqCenCadSel, FStqcenLocSel: Integer;
    FMovimIDSel: TEstqMovimID;
    FEdStqCenLoc: TdmkEditCB;
    FCBStqCenLoc: TdmkDBLookupComboBox;
    FQrStqCenLoc: TmySQLQuery;
  end;

var
  FmVSHide: TFmVSHide;

implementation

uses
  UnVS_CRC_PF;

{$R *.dfm}

procedure TFmVSHide.CadastraLocalemCentrodeEstoque1Click(Sender: TObject);
begin
  VAR_CAD_STQCENLOC := 0;
  VS_CRC_PF.MostraFormStqCenCad(FStqCenCadSel, FStqcenLocSel);
  UMyMod.SetaCodigoPesquisado(FEdStqCenLoc, FCBStqCenLoc, FQrStqCenLoc,
    VAR_CAD_STQCENLOC, 'Controle');
end;

procedure TFmVSHide.FormCreate(Sender: TObject);
begin
  FMovimIDSel   := TEstqMovimID.emidAjuste;
  FStqCenCadSel := 0;
  FStqcenLocSel := 0;
  FEdStqCenLoc := nil;
  FCBStqCenLoc := nil;
  FQrStqCenLoc := nil;
end;

procedure TFmVSHide.HabilitaLocalemMovimID1Click(Sender: TObject);
var
  StqCenCad: Variant;
  StqCenLoc: Integer;
begin
  if FQrStqCenLoc.ParamCount = 1 then
    StqCenCad := FQrStqCenLoc.Params[0].Value
  else
    StqCenCad := Null;
  VAR_CAD_STQCENLOC := 0;
  VS_CRC_PF.MostraFormVSMovimID(FMovimIDSel);
  VS_CRC_PF.ReopenStqCenLoc(FQrStqCenLoc, StqCenCad, 0, FMovimIDSel);
  if FQrStqCenLoc.RecordCount = 1 then
  begin
    StqCenLoc := FQrStqCenLoc.FieldByName('Controle').AsInteger;
    UMyMod.SetaCodigoPesquisado(FEdStqCenLoc, FCBStqCenLoc, FQrStqCenLoc,
    StqCenLoc, 'Controle'); //VAR_CAD_STQCENLOC);
  end;
end;

end.
