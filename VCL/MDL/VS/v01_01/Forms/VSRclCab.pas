unit VSRclCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSRclCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSRclCab: TmySQLQuery;
    DsVSRclCab: TDataSource;
    QrVSRclIts: TmySQLQuery;
    DsVSRclIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label53: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSRclCabCodigo: TIntegerField;
    QrVSRclCabMovimCod: TIntegerField;
    QrVSRclCabEmpresa: TIntegerField;
    QrVSRclCabDtHrIni: TDateTimeField;
    QrVSRclCabDtHrFim: TDateTimeField;
    QrVSRclCabPecas: TFloatField;
    QrVSRclCabPesoKg: TFloatField;
    QrVSRclCabAreaM2: TFloatField;
    QrVSRclCabAreaP2: TFloatField;
    QrVSRclCabLk: TIntegerField;
    QrVSRclCabDataCad: TDateField;
    QrVSRclCabDataAlt: TDateField;
    QrVSRclCabUserCad: TIntegerField;
    QrVSRclCabUserAlt: TIntegerField;
    QrVSRclCabAlterWeb: TSmallintField;
    QrVSRclCabAtivo: TSmallintField;
    QrVSRclCabNO_EMPRESA: TWideStringField;
    QrVSRclItsCodigo: TIntegerField;
    QrVSRclItsControle: TIntegerField;
    QrVSRclItsMovimCod: TIntegerField;
    QrVSRclItsEmpresa: TIntegerField;
    QrVSRclItsMovimID: TIntegerField;
    QrVSRclItsGraGruX: TIntegerField;
    QrVSRclItsPecas: TFloatField;
    QrVSRclItsPesoKg: TFloatField;
    QrVSRclItsAreaM2: TFloatField;
    QrVSRclItsAreaP2: TFloatField;
    QrVSRclItsLk: TIntegerField;
    QrVSRclItsDataCad: TDateField;
    QrVSRclItsDataAlt: TDateField;
    QrVSRclItsUserCad: TIntegerField;
    QrVSRclItsUserAlt: TIntegerField;
    QrVSRclItsAlterWeb: TSmallintField;
    QrVSRclItsAtivo: TSmallintField;
    QrVSRclItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    QrReclasif: TmySQLQuery;
    DsReclasif: TDataSource;
    QrReclasifCodigo: TIntegerField;
    QrReclasifControle: TIntegerField;
    QrReclasifMovimCod: TIntegerField;
    QrReclasifEmpresa: TIntegerField;
    QrReclasifMovimID: TIntegerField;
    QrReclasifGraGruX: TIntegerField;
    QrReclasifPecas: TFloatField;
    QrReclasifPesoKg: TFloatField;
    QrReclasifAreaM2: TFloatField;
    QrReclasifAreaP2: TFloatField;
    QrReclasifLk: TIntegerField;
    QrReclasifDataCad: TDateField;
    QrReclasifDataAlt: TDateField;
    QrReclasifUserCad: TIntegerField;
    QrReclasifUserAlt: TIntegerField;
    QrReclasifAlterWeb: TSmallintField;
    QrReclasifAtivo: TSmallintField;
    QrReclasifNO_PRD_TAM_COR: TWideStringField;
    QrVSRclItsSrcMovID: TIntegerField;
    QrVSRclItsSrcNivel1: TIntegerField;
    QrVSRclItsSrcNivel2: TIntegerField;
    QrSumWII: TmySQLQuery;
    QrSumWIIPecas: TFloatField;
    QrSumWIISrcNivel2: TIntegerField;
    QrVSRclItsPC_RECLAS: TFloatField;
    QrVSRclItsPallet: TIntegerField;
    QrVSRclItsNO_PALLET: TWideStringField;
    QrReclasifPallet: TIntegerField;
    QrReclasifNO_Pallet: TWideStringField;
    QrVSRclItsSdoVrtArM2: TFloatField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    QrResulClas: TmySQLQuery;
    QrResulClasCodigo: TIntegerField;
    QrResulClasControle: TIntegerField;
    QrResulClasMovimCod: TIntegerField;
    QrResulClasEmpresa: TIntegerField;
    QrResulClasMovimID: TIntegerField;
    QrResulClasGraGruX: TIntegerField;
    QrResulClasPecas: TFloatField;
    QrResulClasPesoKg: TFloatField;
    QrResulClasAreaM2: TFloatField;
    QrResulClasAreaP2: TFloatField;
    QrResulClasLk: TIntegerField;
    QrResulClasDataCad: TDateField;
    QrResulClasDataAlt: TDateField;
    QrResulClasUserCad: TIntegerField;
    QrResulClasUserAlt: TIntegerField;
    QrResulClasAlterWeb: TSmallintField;
    QrResulClasAtivo: TSmallintField;
    QrResulClasNO_PRD_TAM_COR: TWideStringField;
    QrResulClasPallet: TIntegerField;
    QrResulClasNO_Pallet: TWideStringField;
    frxWET_RECUR_001_01: TfrxReport;
    frxDsResulClas: TfrxDBDataset;
    QrResulClasSrcNivel2: TIntegerField;
    QrResulClasGGX_REFER: TWideStringField;
    QrResulClasNO_ORI_PALLET: TWideStringField;
    QrResulClasPALLET_M2: TFloatField;
    frxDsResulTota: TfrxDBDataset;
    QrResulTota: TmySQLQuery;
    QrResulTotaNO_Pallet: TWideStringField;
    QrResulTotaPecas: TFloatField;
    QrResulTotaGGX_REFER: TWideStringField;
    QrResulTotaGraGruX: TIntegerField;
    QrPalletInn: TmySQLQuery;
    QrResulTotaPERCENT_CLASS: TFloatField;
    QrResulClasPERCENT_CLASS: TFloatField;
    QrResulTotaCalcM2: TFloatField;
    QrVSInnSum: TmySQLQuery;
    QrVSInnSumAreaM2: TFloatField;
    QrVSInnSumPecas: TFloatField;
    frxDsVSRclIts: TfrxDBDataset;
    frxDsVSRclCab: TfrxDBDataset;
    QrVSRclItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    Entradascomestoquevirtual1: TMenuItem;
    Palletsdosclassificados1: TMenuItem;
    frxWET_RECUR_001_02: TfrxReport;
    QrPalletInnCodigo: TIntegerField;
    QrPalletInnControle: TIntegerField;
    QrPalletInnMovimCod: TIntegerField;
    QrPalletInnMovimNiv: TIntegerField;
    QrPalletInnEmpresa: TIntegerField;
    QrPalletInnTerceiro: TIntegerField;
    QrPalletInnMovimID: TIntegerField;
    QrPalletInnDataHora: TDateTimeField;
    QrPalletInnPallet: TIntegerField;
    QrPalletInnGraGruX: TIntegerField;
    QrPalletInnPecas: TFloatField;
    QrPalletInnPesoKg: TFloatField;
    QrPalletInnAreaM2: TFloatField;
    QrPalletInnAreaP2: TFloatField;
    QrPalletInnSrcMovID: TIntegerField;
    QrPalletInnSrcNivel1: TIntegerField;
    QrPalletInnSrcNivel2: TIntegerField;
    QrPalletInnLk: TIntegerField;
    QrPalletInnDataCad: TDateField;
    QrPalletInnDataAlt: TDateField;
    QrPalletInnUserCad: TIntegerField;
    QrPalletInnUserAlt: TIntegerField;
    QrPalletInnAlterWeb: TSmallintField;
    QrPalletInnAtivo: TSmallintField;
    QrPalletInnSdoVrtPeca: TFloatField;
    QrPalletInnObserv: TWideStringField;
    QrPalletInnNO_PRD_TAM_COR: TWideStringField;
    QrPalletInnNO_Pallet: TWideStringField;
    QrPalletInnNO_EMPRESA: TWideStringField;
    frxDsPalletInn: TfrxDBDataset;
    QrPalletInnNO_FORNECE: TWideStringField;
    QrVSRclCabValorT: TFloatField;
    QrVSRclItsValorT: TFloatField;
    QrReclasifValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    PMReclasif: TPopupMenu;
    Incluireclassificao1: TMenuItem;
    Alterareclassificao1: TMenuItem;
    Excluireclassificao1: TMenuItem;
    QrReclasifSrcMovID: TIntegerField;
    QrReclasifSrcNivel1: TIntegerField;
    QrReclasifSrcNivel2: TIntegerField;
    QrResulTotaAreaM2: TFloatField;
    QrVSRclItsSdoVrtPeca: TFloatField;
    QrVSRclItsMovimTwn: TIntegerField;
    QrReclasifMovimTwn: TIntegerField;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasCOMnomedoPallet2: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRclCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRclCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSRclCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSRclCabBeforeClose(DataSet: TDataSet);
    procedure QrVSRclItsBeforeClose(DataSet: TDataSet);
    procedure QrVSRclItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrResulTotaCalcFields(DataSet: TDataSet);
    procedure frxWET_RECUR_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure Entradascomestoquevirtual1Click(Sender: TObject);
    procedure Palletsdosclassificados1Click(Sender: TObject);
    procedure Incluireclassificao1Click(Sender: TObject);
    procedure PMReclasifPopup(Sender: TObject);
    procedure Alterareclassificao1Click(Sender: TObject);
    procedure Excluireclassificao1Click(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSInnIts(SQLType: TSQLType);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSInnIts(Controle: Integer);
    procedure ReopenReclasif(Controle: Integer);

  end;

var
  FmVSRclCab: TFmVSRclCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSInnIts, ModuleGeral,
Principal, VSMovImp, UnVS_PF, CreateVS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSRclCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSRclCab.MostraFormVSInnIts(SQLType: TSQLType);
begin
  Geral.MB_Aviso('VSInnIts ???');
end;

procedure TFmVSRclCab.Palletsdosclassificados1Click(Sender: TObject);
(*
var
  Qry: TmySQLQuery;
  Pallets: String;
*)
begin
(*
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Pallet ',
    'FROM v s m o v i t s wmi ',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrVSRclItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrVSRclItsCodigo.Value),
    'AND wmi.SrcNivel2=' + Geral.FF0(QrVSRclItsControle.Value),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    'ORDER BY wmi.Controle ',
    '']);
    Pallets := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Pallets := Pallets + ', ' +
        Geral.FF0(Qry.FieldByName('Pallet').AsInteger);
      //
      Qry.next;
    end;
    Pallets := Copy(Pallets, 3);
    //
    if Pallets <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPalletCla, Dmod.MyDB, [
      'SELECT wmi.GragruX, wmi.Pallet, MAX(DataHora) DataHora,  ',
      'SUM(wmi.Pecas) Pecas,  ',
      'IF(MIN(AreaM2) > 0.01, SUM(wmi.AreaM2), 0) AreaM2,  ',
      'IF(MIN(AreaP2) > 0.01, SUM(wmi.AreaP2), 0) AreaP2,  ',
      'IF(MIN(PesoKg) > 0.001, SUM(wmi.PesoKg), 0) PesoKg,  ',
      'CONCAT(gg1.Nome,   ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, wmi.Terceiro,  ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  ',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  ',
      'FROM v s m o v i t s wmi   ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX   ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet   ',
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa ',
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro  ',
      'WHERE wmi.Pallet IN (' + Pallets + ')  ',
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa, wmi.Terceiro  ',
      '']);
      //Geral.MB_Aviso(QrPalletCla.SQL.Text);
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_03, [
        //DModG.frxDsDono,
        frxDsPalletCla
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_03,
        'Pallets - (Re)Classificados');
    end else
      Geral.MB_Aviso('Nenhum pallet foi encontrado!');
  finally
    Qry.Free;
  end;
*)
end;

procedure TFmVSRclCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSRclCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSRclCab, QrVSRclIts);
end;

procedure TFmVSRclCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSRclCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSRclIts);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrVSRclIts, QrReclasif);
end;

procedure TFmVSRclCab.PMReclasifPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluireclassificao1, QrVSRclCab);
  MyObjects.HabilitaMenuItemItsUpd(Alterareclassificao1, QrReclasif);
  MyObjects.HabilitaMenuItemItsDel(Excluireclassificao1, QrReclasif);
end;

procedure TFmVSRclCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRclCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRclCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsrclcab';
  VAR_GOTOMYSQLTABLE := QrVSRclCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA');
  VAR_SQLx.Add('FROM vsrclcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSRclCab.Entradascomestoquevirtual1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPalletInn, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrVSRclCabMovimCod.Value),
  'AND SdoVrtPeca>=0.001',
  'ORDER BY wmi.Controle ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_RECUR_001_02, [
    //DModG.frxDsDono,
    frxDsPalletInn
  ]);
  //
  MyObjects.frxMostra(frxWET_RECUR_001_02,
    'Pallets - Entrada com estoque virtual');
end;

procedure TFmVSRclCab.Estoque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVSMovImp, FmVSMovImp, afmoNegarComAviso) then
  begin
    FmVSMovImp.ShowModal;
    FmVSMovImp.Destroy;
  end;
end;

procedure TFmVSRclCab.Excluireclassificao1Click(Sender: TObject);
var
  VSRclCab, VSRclIts, MovimCod, SrcNivel2: Integer;
begin
  Geral.MB_Aviso('Falta fazer !!!');
(*
  VSRclCab    := QrVSRclCabCodigo.Value;
  VSRclIts    := QrVSRclItsControle.Value;
  //
  MovimCod  := QrReclasifMovimCod.Value;
  SrcNivel2 := QrReclasifSrcNivel2.Value;
  //
  if VS_PF.ExcluiMovimCodVSMovIts(QrReclasif, QrReclasifMovimCod,
  QrReclasifMovimCod.Value, QrReclasifControle.Value,
  QrReclasifSrcNivel2.Value) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsrclcab', MovimCod);
    //Dmod.AtualizaSaldoIMEI(SrcNivel2);
    LocCod(VSRclCab, VSRclCab);
    QrVSRclIts.Locate('Controle', VSRclIts, []);
  end;
*)
end;

procedure TFmVSRclCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSRclCabEmpresa.Value;
  N := 0;
  QrVSRclIts.First;
  while not QrVSRclIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSRclItsPallet.Value;
    //
    QrVSRclIts.Next;
  end;
  if N > 0 then
    VS_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSRclCab.Incluireclassificao1Click(Sender: TObject);
const
  MovimTwn = 0;
var
  Filial, MovimCod, Codigo, Controle: Integer;
  ValorT, AreaM2: Double;

begin
  Geral.MB_Aviso('Falta fazer !!!');
{
  Filial   := QrVSRclCabEmpresa.Value;
  MovimCod := QrVSRclCabMovimCod.Value;
  Codigo   := QrVSRclCabCodigo.Value;
(*
  Controle := QrVSRclItsControle.Value;
  ValorT   := QrVSRclItsValorT.Value;
  AreaM2   := QrVSRclItsAreaM2.Value;
*)
  Controle := 0;
  ValorT   := 0;
  AreaM2   := 0;
  //
  VS_PF.MostraFormVSRclIns(stIns, Filial, MovimCod, MovimTwn, Codigo,
    Controle, ValorT, AreaM2, QrReclasif, 0, vsrEstoque);
  ReopenVSInnIts(QrVSRclItsControle.Value);
}
end;

procedure TFmVSRclCab.ItsAltera1Click(Sender: TObject);
begin
//  MostraFormVSInnIts(stUpd);
end;

procedure TFmVSRclCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSRclCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSRclCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRclCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
{
  Codigo   := QrVSRclItsCodigo.Value;
  MovimCod := QrVSRclItsMovimCod.Value;
  //
  if Dmod.ExcluiControleVSMovIts(QrVSRclIts, QrVSRclItsControle,
  QrVSRclItsControle.Value, QrVSRclItsSrcNivel2.Value) then
  begin
    Dmod.AtualizaTotaisVSXxxCab('vsrclcab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmVSRclCab.ReopenReclasif(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReclasif, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrVSRclCabMovimCod.Value),
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'AND wmi.MovimTwn=' + Geral.FF0(QrVSRclItsMovimTwn.Value),
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrReclasif.Locate('Controle', Controle, []);
end;

procedure TFmVSRclCab.ReopenVSInnIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRclIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrVSRclCabMovimCod.Value),
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
  'ORDER BY NO_Pallet, wmi.Controle ',
  '']);
  //
  QrVSRclIts.Locate('Controle', Controle, []);
end;


procedure TFmVSRclCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRclCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRclCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRclCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRclCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRclCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRclCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRclCabCodigo.Value;
  Close;
end;

procedure TFmVSRclCab.ItsInclui1Click(Sender: TObject);
begin
//  MostraFormVSInnIts(stIns);
end;

procedure TFmVSRclCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSRclCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsrclcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSRclCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSRclCab.BtConfirmaClick(Sender: TObject);
var
  DtHrIni, DtHrFim, DataHora: String;
  Codigo, MovimCod, Empresa, Terceiro: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  Geral.MB_Aviso('Falta fazer !!!');
{
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtHrIni       := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim       := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtHrIni.DateTime < 2, TPDtHrIni, 'Defina uma data de in�cio!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsrclcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsrclcab', False, [
  'MovimCod', 'Empresa', 'DtHrIni',
  'DtHrFim' (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)], [
  'Codigo'], [
  MovimCod, Empresa, DtHrIni,
  DtHrFim (* Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      Dmod.InsereVSMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      //DataHora := DtHrIni;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'v s m o v i t s', False, [
      'Empresa'(*, CO_DATA_HORA_VMI*)], ['MovimCod'], [
      Empresa, Terceiro, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmVSRclCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsrclcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsrclcab', 'Codigo');
end;

procedure TFmVSRclCab.BtItsClick(Sender: TObject);
begin
//  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSRclCab.Alterareclassificao1Click(Sender: TObject);
var
  MovimCod, MovimTwn, Codigo, Controle, Filial: Integer;
  ValorT, AreaM2: Double;
begin
  Geral.MB_Aviso('Falta fazer !!!');
{
  MovimCod := QrReclasifMovimCod.Value;
  MovimTwn := QrReclasifMovimTwn.Value;
  Codigo   := QrReclasifCodigo.Value;
  Controle := QrReclasifControle.Value;
  ValorT   := QrReclasifValorT.Value;
  AreaM2   := QrReclasifAreaM2.Value;
  Filial   := QrVSRclCabEmpresa.Value;
  //
  VS_PF.MostraFormVSRclIns(stUpd, Filial, MovimCod, MovimTwn, Codigo,
    Controle, ValorT, AreaM2, QrReclasif, 0, vsrEstoque);
  ReopenVSInnIts(QrVSRclItsControle.Value);
}
end;

procedure TFmVSRclCab.BtReclasifClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReclasif, BtReclasif);
end;

procedure TFmVSRclCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSRclCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSRclCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSRclCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSRclCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSRclCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSRclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSRclCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRclCab.QrResulTotaCalcFields(DataSet: TDataSet);
var
  Total, Item: Double;
begin
  Geral.MB_Aviso('Falta fazer calc fields!!!');
{
  if Dmod.QrControleVSNivGer.Value = Integer(encPecas) then
  begin
    Total := QrVSInnSumPecas.Value;
    Item  := QrResulTotaPecas.Value;
  end else
  begin
    Total := QrVSInnSumAreaM2.Value;
    Item  := QrResulTotaAreaM2.Value;
  end;
  if Total = 0 then
    QrResulTotaPERCENT_CLASS.Value := 0
  else
    QrResulTotaPERCENT_CLASS.Value := 100 * Item / Total;
  QrResulTotaCalcM2.Value := QrVSInnSumAreaM2.Value;
}
end;

procedure TFmVSRclCab.QrVSRclCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRclCab.QrVSRclCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSInnIts(0);
end;

procedure TFmVSRclCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSRclCab.FichasCOMnomedoPallet2Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSRclCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSRclCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSRclCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSRclCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsrclcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSRclCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRclCab.frxWET_RECUR_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSRclCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSRclCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsrclcab');
end;

procedure TFmVSRclCab.Classificao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Niveis2: String;
  //DBCross1ColumnTotal0: TfrxMemoView;
  DBCross1: TfrxDBCrossView;
begin
  Geral.MB_Aviso('Fazer ???');
{
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT SrcNivel2 ',
    'FROM v s m o v i t s wmi',
    'WHERE wmi.SrcMovID=' + Geral.FF0(QrVSRclItsMovimCod.Value),
    'AND wmi.SrcNivel1=' + Geral.FF0(QrVSRclItsCodigo.Value),
    //'AND wmi.SrcNivel2=' + Geral.FF0(QrVSRclItsControle.Value),
    'AND wmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
    '']);
    Niveis2 := '';
    Qry.First;
    while not Qry.Eof do
    begin
      Niveis2 := Niveis2 + ', ' +
        Geral.FF0(Qry.FieldByName('SrcNivel2').AsInteger);
      //
      Qry.next;
    end;
    Niveis2 := Copy(Niveis2, 3);
    //
    if Niveis2 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnSum, Dmod.MyDB, [
      'SELECT SUM(wmi.Pecas) Pecas, SUM(wmi.AreaM2) AreaM2 ',
      'FROM v s m o v i t s wmi',
      'WHERE wmi.Controle IN (' + Niveis2 + ') ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulTota, Dmod.MyDB, [
      'SELECT "TOTAL" NO_Pallet, SUM(wmi.Pecas) Pecas, ',
      'SUM(wmi.AreaM2) AreaM2, ',
      'IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, wmi.GraGruX',
      'FROM v s m o v i t s wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrVSRclItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrVSRclItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrVSRclItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'GROUP BY GraGruX',
      '']);
      //
      // SQL copiado do QrReclasif
      UnDmkDAC_PF.AbreMySQLQuery0(QrResulClas, Dmod.MyDB, [
      'SELECT IF(gg1.Referencia <> "", gg1.Referencia, CONCAT("#", ',
      'ggx.Controle)) GGX_REFER, ',
      'wmi.*, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet ',
      'FROM v s m o v i t s wmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
      'WHERE wmi.SrcMovID=' + Geral.FF0(QrVSRclItsMovimCod.Value),
      'AND wmi.SrcNivel1=' + Geral.FF0(QrVSRclItsCodigo.Value),
      //'AND wmi.SrcNivel2=' + Geral.FF0(QrVSRclItsControle.Value),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
      'ORDER BY wmi.Controle ',
      '']);
      //
      //DBCross1ColumnTotal0 := frxWET_RECUR_001_01.FindObject('DBCross1ColumnTotal0') as TfrxMemoView;
      DBCross1 := frxWET_RECUR_001_01.FindObject('DBCross1') as TfrxDBCrossView;
      DBCross1.CellFields.Clear;
      if Dmod.QrControleVSNivGer.Value = Integer(encPecas) then
      begin
        //DBCross1ColumnTotal0.Memo.Text := 'Pe�as';
        DBCross1.CellFields.Add('Pecas');
      end else
      begin
        //DBCross1ColumnTotal0.Memo.Text := '�rea m�';
        DBCross1.CellFields.Add('AreaM2');
      end;
      //
      MyObjects.frxDefineDataSets(frxWET_RECUR_001_01, [
        DModG.frxDsDono,
        frxDsVSRclCab,
        frxDsVSRclIts,
        frxDsResulClas,
        frxDsResulTota
      ]);
      //
      MyObjects.frxMostra(frxWET_RECUR_001_01, 'Controle Couros');
    end else
      Geral.MB_Aviso('Nenhum item foi encontrado!');
  finally
    Qry.Free;
  end;
  // Erro itens DBGrid!
  // FastReport esta gerando erro no dataset?
  LocCod(QrVSRclCabCodigo.Value,QrVSRclCabCodigo.Value);
}
end;

procedure TFmVSRclCab.QrVSRclCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSRclIts.Close;
end;

procedure TFmVSRclCab.QrVSRclCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSRclCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSRclCab.QrVSRclItsAfterScroll(DataSet: TDataSet);
begin
  ReopenReclasif(0);
end;

procedure TFmVSRclCab.QrVSRclItsBeforeClose(DataSet: TDataSet);
begin
  QrReclasif.Close;
end;

end.

