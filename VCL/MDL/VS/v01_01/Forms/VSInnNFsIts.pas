unit VSInnNFsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkMemo, dmkRadioGroup,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEditDateTimePicker, UnPQ_PF,
  dmkDBEdit, Vcl.Menus;

type
  TFmVSInnNFsIts = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrLocalizaPQ: TmySQLQuery;
    Panel1: TPanel;
    Painel1: TPanel;
    PainelDados: TPanel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    SBProduto: TSpeedButton;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1PQ: TIntegerField;
    Panel3: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label2: TLabel;
    EdLinha: TdmkEdit;
    Label1: TLabel;
    QrPQCli: TmySQLQuery;
    QrPQCliCustoPadrao: TFloatField;
    QrPQCliMoedaPadrao: TIntegerField;
    QrPQCliNOMEMOEDAPADRAO: TWideStringField;
    DsPQCli: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    PMProduto: TPopupMenu;
    Cadastrodoproduto1: TMenuItem;
    Dadosfiscais1: TMenuItem;
    Panel6: TPanel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Panel7: TPanel;
    GBRecuImpost: TGroupBox;
    Panel2: TPanel;
    Label13: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    EdALIQ_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    EdALIQ_PIS: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdALIQ_COFINS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    Panel8: TPanel;
    Label19: TLabel;
    EdQTD: TdmkEdit;
    Label10: TLabel;
    Label5: TLabel;
    EdVL_ITEM: TdmkEdit;
    EdValorkg: TdmkEdit;
    Label8: TLabel;
    EdCustoItem: TdmkEdit;
    QrCFOP: TMySQLQuery;
    QrCFOPNome: TWideStringField;
    DsCFOP: TDataSource;
    QrCFOPCodigo: TLargeintField;
    EdALIQ_IPI: TdmkEdit;
    Label16: TLabel;
    EdVL_IPI: TdmkEdit;
    Label476: TLabel;
    EdICMS_CST: TdmkEdit;
    EdUCTextoB: TdmkEdit;
    Label477: TLabel;
    EdCST_IPI: TdmkEdit;
    EdUCTextoIPI_CST: TdmkEdit;
    Label478: TLabel;
    EdCST_PIS: TdmkEdit;
    EdUCTextoPIS_CST: TdmkEdit;
    Label480: TLabel;
    EdCST_COFINS: TdmkEdit;
    EdUCTextoCOFINS_CST: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_BC_PIS: TdmkEdit;
    EdVL_BC_IPI: TdmkEdit;
    EdVL_BC_COFINS: TdmkEdit;
    Edprod_vOutro: TdmkEdit;
    Label17: TLabel;
    Edprod_vProd: TdmkEdit;
    Label18: TLabel;
    Label20: TLabel;
    EdVL_BC_ICMS_ST: TdmkEdit;
    EdALIQ_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    EdVL_DESC: TdmkEdit;
    Label21: TLabel;
    Edprod_vSeg: TdmkEdit;
    Label22: TLabel;
    Edprod_vFrete: TdmkEdit;
    Label23: TLabel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    DsGraGruX: TDataSource;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label9: TLabel;
    EdControle: TdmkEdit;
    Label11: TLabel;
    EdConta: TdmkEdit;
    Label15: TLabel;
    EdMovimNiv: TdmkEdit;
    Label25: TLabel;
    EdMovimTwn: TdmkEdit;
    Label27: TLabel;
    EdEmpresa: TdmkEdit;
    DBEdit1: TDBEdit;
    EdIND_MOV: TdmkEdit;
    Label28: TLabel;
    EdIND_MOV_TXT: TdmkEdit;
    EdxLote: TdmkEdit;
    Label12: TLabel;
    Label66: TLabel;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    SbCFOP: TSpeedButton;
    Label4: TLabel;
    EdOri_IPIpIPI: TdmkEdit;
    EdOri_IPIvIPI: TdmkEdit;
    Label7: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVL_ITEMChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBProdutoClick(Sender: TObject);
    procedure EdOri_IPIpIPIChange(Sender: TObject);
    procedure EdOri_IPIvIPIChange(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure QrPQCliCalcFields(DataSet: TDataSet);
    procedure EdALIQ_ICMSChange(Sender: TObject);
    procedure EdALIQ_PISChange(Sender: TObject);
    procedure EdALIQ_COFINSChange(Sender: TObject);
    procedure Cadastrodoproduto1Click(Sender: TObject);
    procedure Dadosfiscais1Click(Sender: TObject);
    procedure SbCFOPClick(Sender: TObject);
    procedure EdICMS_CSTChange(Sender: TObject);
    procedure EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_IPIChange(Sender: TObject);
    procedure EdCST_PISChange(Sender: TObject);
    procedure EdCST_COFINSChange(Sender: TObject);
    procedure EdCST_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_ICMSChange(Sender: TObject);
    procedure EdVL_BC_IPIChange(Sender: TObject);
    procedure EdVL_BC_PISChange(Sender: TObject);
    procedure EdVL_BC_COFINSChange(Sender: TObject);
    procedure EdALIQ_IPIChange(Sender: TObject);
    procedure EdUCTextoIPI_CSTRedefinido(Sender: TObject);
    procedure EdCST_IPIExit(Sender: TObject);
    procedure EdIND_MOVChange(Sender: TObject);
    procedure EdIND_MOVKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    F_IND_MOV_EFD: MyArrayLista;
    //FVol, FkgU, FkgT: Double;
    //FQuaisInsumosMostra: TPartOuTodo;
    procedure CalculaIPI();
    procedure ReopenPQCli();
    procedure CalculaRetornoImpostos(Qual: TRetornImpost);
    procedure ConfiguraImpostos(Insumo: Integer);
  public
    { Public declarations }
    FConta: Integer;
    //
    procedure CalculaOnEdit;
    //procedure MostraTodosPQs();
    procedure Pesos(Index: Integer);
    procedure ReopenPQ(Quais: TPartOuTodo);
    procedure ReopenCFOP();
    procedure ReopenGraGruX();
  end;

var
  FmVSInnNFsIts: TFmVSInnNFsIts;

implementation

uses UnMyObjects, PQE, Module, Principal, BlueDermConsts, UMySQLModule,
  UnAppPF, UnMoedas, UnGrade_Jan, ModProd, GetValor, UnFinanceiro, NFe_PF;

{$R *.DFM}


procedure TFmVSInnNFsIts.CalculaOnEdit();
var
  Volumes, QTD, PesoVB, CustoItem, ValorItem, IPI_vIPI,
  Valor, CFin, prod_vProd, prod_vDesc, prod_vFrete, prod_vSeg, prod_vOutro: Double;
begin
  prod_vProd := Edprod_vProd.ValueVariant;
  prod_vDesc := EdVL_DESC.ValueVariant;
  prod_vFrete := Edprod_vFrete.ValueVariant;
  prod_vSeg := Edprod_vSeg.ValueVariant;
  prod_vOutro := Edprod_vOutro.ValueVariant;
  //ValorItem  := EdVlrItem.ValueVariant;
  ValorItem  := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;
  EdVL_ITEM.ValueVariant := ValorItem;
  // fim 2022-04-05 SPED EFD
  QTD := EdQTD.ValueVariant;
  //
(*  J� est� incluso!
  IPI_vIPI  := EdOri_IPIvIPI.ValueVariant;
  CustoItem := ValorItem + IPI_vIPI;
  if QTD > 0 then
    Valor := CustoItem / QTD
  else
    Valor := 0;
  //
*)
  EdCustoItem.ValueVariant       := CustoItem;
  EdVL_BC_ICMS.ValueVariant      := ValorItem;
  EdVL_BC_IPI.ValueVariant       := ValorItem;
  EdVL_BC_PIS.ValueVariant       := ValorItem;
  EdVL_BC_COFINS.ValueVariant    := ValorItem;
  //
  EdValorkg.ValueVariant       := Valor;
end;

procedure TFmVSInnNFsIts.CalculaRetornoImpostos(Qual: TRetornImpost);
const
  sProcName = 'FmPQEIts.CalculaRetornoImpostos()';
var
  pICMS, pIPI, pPIS, pCofins, vICMS, vPIS, vCofins, vIPI, ICMS_vBC, IPI_vBC,
  PISS_vBC, COFINS_vBC: Double;
begin
  //VlrItem   := EdVlrItem.ValueVariant;

  ICMS_vBC   := EdVL_BC_ICMS.ValueVariant;
  IPI_vBC    := EdVL_BC_IPI.ValueVariant;
  PISS_vBC   := EdVL_BC_PIS.ValueVariant;
  COFINS_vBC := EdVL_BC_COFINS.ValueVariant;

  pICMS     := EdALIQ_ICMS.ValueVariant;
  pIPI      := EdALIQ_IPI.ValueVariant;
  pPIS      := EdALIQ_PIS.ValueVariant;
  pCofins   := EdALIQ_COFINS.ValueVariant;
  vICMS     := Geral.RoundC(ICMS_vBC * pICMS / 100, 2);
  vIPI      := Geral.RoundC(IPI_vBC * pIPI / 100, 2);
  vPIS      := Geral.RoundC(PISS_vBC * pPIS / 100, 2);
  vCofins   := Geral.RoundC(COFINS_vBC * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdVL_ICMS.ValueVariant := vICMS;
      EdVL_IPI.ValueVariant := vIPI;
      EdVL_PIS.ValueVariant := vPIS;
      EdVL_COFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdVL_ICMS.ValueVariant := vICMS;
    retimpostIPI: EdVL_IPI.ValueVariant := vIPI;
    retimpostPIS: EdVL_PIS.ValueVariant := vPIS;
    retimpostCOFINS: EdVL_COFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
end;

procedure TFmVSInnNFsIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInnNFsIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConta := 0;
  //
  F_IND_MOV_EFD := UnNFe_PF.ListaIND_MOV_EFD();
  //FQuaisInsumosMostra := TPartOuTodo.ptParcial;
  //
  ReopenGraGruX();
  ReopenCFOP();
  //
  EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdICMS_CST.Text));
  EdCST_COFINS.Text := UFinanceiro.ListaDeCST_COFINS();
  EdCST_PIS.Text := UFinanceiro.ListaDeCST_PIS();
  EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(Geral.IMV(EdCST_IPI.Text));
end;

procedure TFmVSInnNFsIts.BtConfirmaClick(Sender: TObject);
var
  UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ, COD_CTA, DtCorrApo,
  xLote: String;
  Codigo, MovimCod, MovimNiv, MovimTwn, Empresa, Controle, Conta, IDNiv4,
  GraGru1, GraGruX, CST_ICMS, CFOP, CST_PIS, CST_COFINS: Integer;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r, VL_COFINS, VL_ABAT_NT, Ori_IPIpIPI,
  Ori_IPIvIPI: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  MovimNiv       := EdMovimNiv.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Conta          := EdConta.ValueVariant;
  GraGru1        := QrGraGruXGraGru1.Value;
  GraGruX        := EdGraGruX.ValueVariant;
  QTD            := EdQTD.ValueVariant;
  UNID           := QrGraGruXSIGLAUNIDMED.Value;
  VL_ITEM        := EdVL_ITEM.ValueVariant;
  VL_DESC        := EdVL_DESC.ValueVariant;
  IND_MOV        := EdIND_MOV.ValueVariant;
  CST_ICMS       := EdICMS_CST.ValueVariant;
  CFOP           := Geral.IMV(Geral.SoNumero_TT(EdCFOP.Text));
  COD_NAT        := ''; //EdCOD_NAT.ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS.ValueVariant;
  ALIQ_ICMS      := EdALIQ_ICMS.ValueVariant;
  VL_ICMS        := EdVL_ICMS.ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  ALIQ_ST        := EdALIQ_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST.ValueVariant;
  IND_APUR       := ''; //EdIND_APUR  .ValueVariant;
  CST_IPI        := EdCST_IPI.ValueVariant;
  COD_ENQ        := ''; //EdCOD_ENQ   .ValueVariant;
  VL_BC_IPI      := EdVL_BC_IPI.ValueVariant;
  ALIQ_IPI       := EdALIQ_IPI.ValueVariant;
  VL_IPI         := EdVL_IPI.ValueVariant;
  CST_PIS        := EdCST_PIS.ValueVariant;
  VL_BC_PIS      := EdVL_BC_PIS .ValueVariant;
  ALIQ_PIS_p     := EdALIQ_PIS.ValueVariant;
  QUANT_BC_PIS   := 0.000; //EdQUANT_BC_PIS   .ValueVariant;
  ALIQ_PIS_r     := 0.0000; //EdALIQ_PIS_r     .ValueVariant;
  VL_PIS         := EdVL_PIS.ValueVariant;
  CST_COFINS     := EdCST_COFINS.ValueVariant;
  VL_BC_COFINS   := EdVL_BC_COFINS.ValueVariant;
  ALIQ_COFINS_p  := EdALIQ_COFINS.ValueVariant;
  QUANT_BC_COFINS:= 0.000; //EdQUANT_BC_COFINS.ValueVariant;
  ALIQ_COFINS_r  := 0.0000; //EdALIQ_COFINS_r  .ValueVariant;
  VL_COFINS      := EdVL_COFINS.ValueVariant;
  COD_CTA        := ''; //EdCOD_CTA.ValueVariant;
  VL_ABAT_NT     := 0.00; //EdVL_ABAT_NT     .ValueVariant;
  DtCorrApo      := Geral.FDT(TPDtCorrApo.Date, 1);
  xLote          := EdxLote.ValueVariant;
  Ori_IPIpIPI    := EdOri_IPIpIPI.ValueVariant;
  Ori_IPIvIPI    := EdOri_IPIvIPI.ValueVariant;

  //
  Conta := UMyMod.BPGS1I32('vsinnnfsits', 'Conta', '', '', tsPos, SQLType, IDNiv4);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinnnfsits', False, [
  'Codigo', 'MovimCod', 'MovimNiv',
  'MovimTwn', 'Empresa', 'Controle',
  'GraGru1', 'GraGruX',
  'QTD', 'UNID', 'VL_ITEM',
  'VL_DESC', 'IND_MOV', 'CST_ICMS',
  'CFOP', 'COD_NAT', 'VL_BC_ICMS',
  'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
  'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
  'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
  'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
  'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
  'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
  'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
  'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA',
  'VL_ABAT_NT', 'DtCorrApo', 'xLote',
  'Ori_IPIpIPI', 'Ori_IPIvIPI'], [
  'Conta'], [
  Codigo, MovimCod, MovimNiv,
  MovimTwn, Empresa, Controle,
  GraGru1, GraGruX,
  QTD, UNID, VL_ITEM,
  VL_DESC, IND_MOV, CST_ICMS,
  CFOP, COD_NAT, VL_BC_ICMS,
  ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, IND_APUR,
  CST_IPI, COD_ENQ, VL_BC_IPI,
  ALIQ_IPI, VL_IPI, CST_PIS,
  VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  ALIQ_PIS_r, VL_PIS, CST_COFINS,
  VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
  ALIQ_COFINS_r, VL_COFINS, COD_CTA,
  VL_ABAT_NT, DtCorrApo, xLote,
  Ori_IPIpIPI, Ori_IPIvIPI], [
  Conta], True) then
  begin
    AtualizaTotaisVSInnNFs :

 SELECT SUM(VL_DESC) VL_DESC,
SUM(prod_vProd) VL_MERC,
SUM(prod_vFrete) VL_FRT,
SUM(prod_vSeg) VL_SEG,
SUM(prod_vOutro) VL_OUT_DA,
SUM(VL_BC_ICMS) VL_BC_ICMS,
SUM(VL_ICMS) VL_ICMS,
SUM(VL_BC_ICMS_ST) VL_BC_ICMS_ST,
SUM(VL_ICMS_ST) VL_ICMS_ST,
SUM(VL_BC_IPI) VL_BC_IPI,
SUM(VL_IPI) VL_IPI,
SUM(VL_BC_PIS) VL_BC_PIS,
SUM(VL_PIS) VL_PIS,
SUM(VL_BC_COFINS) VL_BC_COFINS,
SUM(VL_COFINS) VL_COFINS

FROM vsinnnfsits
WHERE MovimCod>0



















    Close;
  end;
end;

procedure TFmVSInnNFsIts.EdOri_IPIpIPIChange(Sender: TObject);
begin
  CalculaIPI();
end;

procedure TFmVSInnNFsIts.EdVL_BC_IPIChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmVSInnNFsIts.EdOri_IPIvIPIChange(Sender: TObject);
begin
  CalculaOnEdit();
end;

procedure TFmVSInnNFsIts.EdALIQ_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmVSInnNFsIts.EdALIQ_ICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmVSInnNFsIts.EdALIQ_IPIChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostIPI);
end;

procedure TFmVSInnNFsIts.EdALIQ_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmVSInnNFsIts.EdVL_ITEMChange(Sender: TObject);
begin
  CalculaIPI();
  CalculaOnEdit;
  CalculaRetornoImpostos(TRetornImpost.retimpostTodos);
end;

procedure TFmVSInnNFsIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnNFsIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInnNFsIts.EdUCTextoIPI_CSTRedefinido(Sender: TObject);
begin
  if (EdUCTextoIPI_CST.Text = EmptyStr) then
  begin
    EdCST_IPI.ValueVariant := '0';
  end;
end;

procedure TFmVSInnNFsIts.EdCST_COFINSChange(Sender: TObject);
begin
  EdUCTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(Geral.IMV(EdCST_COFINS.Text));
end;

procedure TFmVSInnNFsIts.EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_COFINS.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmVSInnNFsIts.EdVL_BC_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmVSInnNFsIts.EdICMS_CSTChange(Sender: TObject);
begin
  EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdICMS_CST.Text));
end;

procedure TFmVSInnNFsIts.EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdICMS_CST.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
(*if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();*)
end;

procedure TFmVSInnNFsIts.EdVL_BC_ICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmVSInnNFsIts.EdIND_MOVChange(Sender: TObject);
begin
  EdIND_MOV_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_MOV_EFD,  EdIND_Mov.ValueVariant);
end;

procedure TFmVSInnNFsIts.EdIND_MOVKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_MOV.Text := Geral.SelecionaItem(F_IND_MOV_EFD, 0,
      'SEL-LISTA-000 :: Movimenta��o f�sica',
      TitCols, Screen.Width)
  end;
end;

procedure TFmVSInnNFsIts.EdCST_IPIChange(Sender: TObject);
begin
  EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(Geral.IMV(EdCST_IPI.Text));
end;

procedure TFmVSInnNFsIts.EdCST_IPIExit(Sender: TObject);
begin
  EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(Geral.IMV(EdCST_IPI.Text));
end;

procedure TFmVSInnNFsIts.EdCST_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_IPI.Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmVSInnNFsIts.EdCST_PISChange(Sender: TObject);
begin
  EdUCTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(Geral.IMV(EdCST_PIS.Text));
end;

procedure TFmVSInnNFsIts.EdCST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_PIS.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmVSInnNFsIts.EdVL_BC_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmVSInnNFsIts.Pesos(Index: Integer);
begin
end;

procedure TFmVSInnNFsIts.QrPQCliCalcFields(DataSet: TDataSet);
begin
  QrPQCliNOMEMOEDAPADRAO.Value := UMoedas.ObtemNomeMoeda(QrPQCliMoedaPadrao.Value);
end;

procedure TFmVSInnNFsIts.ReopenCFOP();
begin
  //WHERE Codigo=CONCAT(SUBSTRING(1234, 1, 1), '.', SUBSTRING(1234, 2, 4))
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmVSInnNFsIts.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSInnNFsIts.ReopenPQ(Quais: TPartOuTodo);
var
  SQL_IQ: String;
begin
(*
  if Quais = ptIndef then
    UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB)
  else
  begin
    if Quais = ptTotal then
      SQL_IQ := ''
    else
      SQL_IQ := 'AND pfo.IQ=' + Geral.FF0(FmPQE.QrPQEIQ.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
    'SELECT DISTINCT pq.Nome NOMEPQ, pfo.PQ ',
    'FROM pqfor pfo ',
    'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo ',
    'WHERE pfo.CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    SQL_IQ,
    'AND pfo.Empresa=' + Geral.FF0(FEmpresa),
    'AND pq.Ativo = 1 ',
    'AND pq.GGXNiv2 > 0 ',
    'ORDER BY pq.Nome ',
    '']);
  end;
*)
end;

procedure TFmVSInnNFsIts.ReopenPQCli();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
    'SELECT CustoPadrao, MoedaPadrao ',
    'FROM pqcli ',
    'WHERE PQ=' + Geral.FF0(QrPQ1PQ.Value),
    'AND CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    '']);
end;

procedure TFmVSInnNFsIts.ConfiguraImpostos(Insumo: Integer);
begin
{
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
    EdRpICMS.ValueVariant   := QrGraGru1ICMSRec_pAliq.Value;
    EdRpPIS.ValueVariant    := QrGraGru1PISRec_pAliq.Value;
    EdRpCOFINS.ValueVariant := QrGraGru1COFINSRec_pAliq.Value;
  end else
  begin
    EdRpICMS.ValueVariant   := 0.00;
    EdRpPIS.ValueVariant    := 0.00;
    EdRpCOFINS.ValueVariant := 0.00;

    GBRecuImpost.Enabled := False;
    GBRecuImpost.Caption := 'Recupera��o de impostos: Op��es espec�ficas -> Geral - > N�o recupera impostos na entrada de insumos.!!!!!!!!!!!!!!';
  end;
}
end;

procedure TFmVSInnNFsIts.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmVSInnNFsIts.Dadosfiscais1Click(Sender: TObject);
var
  Insumo: Integer;
begin
{
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  Grade_Jan.MostraFormGraGruN(Insumo, 0, tpGraCusPrc, tsfDadosFiscais);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
}
end;

procedure TFmVSInnNFsIts.Cadastrodoproduto1Click(Sender: TObject);
var
  Insumo: Integer;
begin
{
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  FmPrincipal.CadastroPQ(Insumo);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
}
end;

procedure TFmVSInnNFsIts.CalculaIPI();
begin
  EdOri_IPIvIPI.ValueVariant :=
    EdOri_IPIpIPI.ValueVariant *
    EdVL_ITEM.ValueVariant / 100;
end;

procedure TFmVSInnNFsIts.SbCFOPClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP();
    UMyMod.SetaCodigoPesquisado(EdCFOP, CBCFOP, QrCFOP, VAR_CADASTRO);
  end;
end;

procedure TFmVSInnNFsIts.SBProdutoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProduto, SBProduto);
end;

end.


