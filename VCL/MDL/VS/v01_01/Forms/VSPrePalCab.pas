unit VSPrePalCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, UnGrl_Consts, UnAppEnums;

type
  TFmVSPrePalCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSPrePalCab: TmySQLQuery;
    DsVSPrePalCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSPrePalCabCodigo: TIntegerField;
    QrVSPrePalCabPallet: TIntegerField;
    QrVSPrePalCabLk: TIntegerField;
    QrVSPrePalCabDataCad: TDateField;
    QrVSPrePalCabDataAlt: TDateField;
    QrVSPrePalCabUserCad: TIntegerField;
    QrVSPrePalCabUserAlt: TIntegerField;
    QrVSPrePalCabAlterWeb: TSmallintField;
    QrVSPrePalCabAtivo: TSmallintField;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    QrVSPrePalCabNome: TWideStringField;
    QrVSPrePalCabEmpresa: TIntegerField;
    QrVSPrePalCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPrePalCabGraGruX: TIntegerField;
    Label4: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Label5: TLabel;
    QrVSPrePalDst: TmySQLQuery;
    QrVSPrePalDstCodigo: TLargeintField;
    QrVSPrePalDstControle: TLargeintField;
    QrVSPrePalDstMovimCod: TLargeintField;
    QrVSPrePalDstMovimNiv: TLargeintField;
    QrVSPrePalDstMovimTwn: TLargeintField;
    QrVSPrePalDstEmpresa: TLargeintField;
    QrVSPrePalDstTerceiro: TLargeintField;
    QrVSPrePalDstCliVenda: TLargeintField;
    QrVSPrePalDstMovimID: TLargeintField;
    QrVSPrePalDstDataHora: TDateTimeField;
    QrVSPrePalDstPallet: TLargeintField;
    QrVSPrePalDstGraGruX: TLargeintField;
    QrVSPrePalDstPecas: TFloatField;
    QrVSPrePalDstPesoKg: TFloatField;
    QrVSPrePalDstAreaM2: TFloatField;
    QrVSPrePalDstAreaP2: TFloatField;
    QrVSPrePalDstValorT: TFloatField;
    QrVSPrePalDstSrcMovID: TLargeintField;
    QrVSPrePalDstSrcNivel1: TLargeintField;
    QrVSPrePalDstSrcNivel2: TLargeintField;
    QrVSPrePalDstSrcGGX: TLargeintField;
    QrVSPrePalDstSdoVrtPeca: TFloatField;
    QrVSPrePalDstSdoVrtPeso: TFloatField;
    QrVSPrePalDstSdoVrtArM2: TFloatField;
    QrVSPrePalDstObserv: TWideStringField;
    QrVSPrePalDstSerieFch: TLargeintField;
    QrVSPrePalDstFicha: TLargeintField;
    QrVSPrePalDstMisturou: TLargeintField;
    QrVSPrePalDstFornecMO: TLargeintField;
    QrVSPrePalDstCustoMOKg: TFloatField;
    QrVSPrePalDstCustoMOM2: TFloatField;
    QrVSPrePalDstCustoMOTot: TFloatField;
    QrVSPrePalDstValorMP: TFloatField;
    QrVSPrePalDstDstMovID: TLargeintField;
    QrVSPrePalDstDstNivel1: TLargeintField;
    QrVSPrePalDstDstNivel2: TLargeintField;
    QrVSPrePalDstDstGGX: TLargeintField;
    QrVSPrePalDstQtdGerPeca: TFloatField;
    QrVSPrePalDstQtdGerPeso: TFloatField;
    QrVSPrePalDstQtdGerArM2: TFloatField;
    QrVSPrePalDstQtdGerArP2: TFloatField;
    QrVSPrePalDstQtdAntPeca: TFloatField;
    QrVSPrePalDstQtdAntPeso: TFloatField;
    QrVSPrePalDstQtdAntArM2: TFloatField;
    QrVSPrePalDstQtdAntArP2: TFloatField;
    QrVSPrePalDstNotaMPAG: TFloatField;
    QrVSPrePalDstPedItsFin: TLargeintField;
    QrVSPrePalDstMarca: TWideStringField;
    QrVSPrePalDstStqCenLoc: TLargeintField;
    QrVSPrePalDstNO_PALLET: TWideStringField;
    QrVSPrePalDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPrePalDstNO_TTW: TWideStringField;
    QrVSPrePalDstID_TTW: TLargeintField;
    QrVSPrePalDstReqMovEstq: TLargeintField;
    DsVSPrePalDst: TDataSource;
    GBOri: TGroupBox;
    DBGrid1: TDBGrid;
    GBDst: TGroupBox;
    Splitter1: TSplitter;
    QrVSPrePalOri: TmySQLQuery;
    QrVSPrePalOriCodigo: TLargeintField;
    QrVSPrePalOriControle: TLargeintField;
    QrVSPrePalOriMovimCod: TLargeintField;
    QrVSPrePalOriMovimNiv: TLargeintField;
    QrVSPrePalOriMovimTwn: TLargeintField;
    QrVSPrePalOriEmpresa: TLargeintField;
    QrVSPrePalOriTerceiro: TLargeintField;
    QrVSPrePalOriCliVenda: TLargeintField;
    QrVSPrePalOriMovimID: TLargeintField;
    QrVSPrePalOriDataHora: TDateTimeField;
    QrVSPrePalOriPallet: TLargeintField;
    QrVSPrePalOriGraGruX: TLargeintField;
    QrVSPrePalOriPecas: TFloatField;
    QrVSPrePalOriPesoKg: TFloatField;
    QrVSPrePalOriAreaM2: TFloatField;
    QrVSPrePalOriAreaP2: TFloatField;
    QrVSPrePalOriValorT: TFloatField;
    QrVSPrePalOriSrcMovID: TLargeintField;
    QrVSPrePalOriSrcNivel1: TLargeintField;
    QrVSPrePalOriSrcNivel2: TLargeintField;
    QrVSPrePalOriSrcGGX: TLargeintField;
    QrVSPrePalOriSdoVrtPeca: TFloatField;
    QrVSPrePalOriSdoVrtPeso: TFloatField;
    QrVSPrePalOriSdoVrtArM2: TFloatField;
    QrVSPrePalOriObserv: TWideStringField;
    QrVSPrePalOriSerieFch: TLargeintField;
    QrVSPrePalOriFicha: TLargeintField;
    QrVSPrePalOriMisturou: TLargeintField;
    QrVSPrePalOriFornecMO: TLargeintField;
    QrVSPrePalOriCustoMOKg: TFloatField;
    QrVSPrePalOriCustoMOM2: TFloatField;
    QrVSPrePalOriCustoMOTot: TFloatField;
    QrVSPrePalOriValorMP: TFloatField;
    QrVSPrePalOriDstMovID: TLargeintField;
    QrVSPrePalOriDstNivel1: TLargeintField;
    QrVSPrePalOriDstNivel2: TLargeintField;
    QrVSPrePalOriDstGGX: TLargeintField;
    QrVSPrePalOriQtdGerPeca: TFloatField;
    QrVSPrePalOriQtdGerPeso: TFloatField;
    QrVSPrePalOriQtdGerArM2: TFloatField;
    QrVSPrePalOriQtdGerArP2: TFloatField;
    QrVSPrePalOriQtdAntPeca: TFloatField;
    QrVSPrePalOriQtdAntPeso: TFloatField;
    QrVSPrePalOriQtdAntArM2: TFloatField;
    QrVSPrePalOriQtdAntArP2: TFloatField;
    QrVSPrePalOriNotaMPAG: TFloatField;
    QrVSPrePalOriPedItsFin: TLargeintField;
    QrVSPrePalOriMarca: TWideStringField;
    QrVSPrePalOriStqCenLoc: TLargeintField;
    QrVSPrePalOriNO_PALLET: TWideStringField;
    QrVSPrePalOriNO_PRD_TAM_COR: TWideStringField;
    QrVSPrePalOriNO_TTW: TWideStringField;
    QrVSPrePalOriID_TTW: TLargeintField;
    QrVSPrePalOriReqMovEstq: TLargeintField;
    DsVSPrePalOri: TDataSource;
    DBGrid2: TDBGrid;
    QrVSPrePalOriNO_FORNECE: TWideStringField;
    QrVSPrePalDstNO_FORNECE: TWideStringField;
    N1: TMenuItem;
    CorrigeFornecedor1: TMenuItem;
    QrVSPrePalCabMovimCod: TIntegerField;
    QrVSPrePalCabTemIMEIMrt: TSmallintField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    QrVSPrePalOriCustoM2: TFloatField;
    QrVSPrePalDstCustoM2: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPrePalCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPrePalCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPrePalCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSPrePalCabBeforeClose(DataSet: TDataSet);
    procedure CorrigeFornecedor1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrePalDst(Controle: Integer);
    procedure ReopenPrePalOri(Controle: Integer);

  end;

var
  FmVSPrePalCab: TFmVSPrePalCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPrePalCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

{
procedure TFmVSPrePalCab.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrCadastro_Com_Itens_ITS;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrCadastro_Com_Itens_ITSControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrCadastro_Com_Itens_ITSCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrCadastro_Com_Itens_ITSNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
end;
}

procedure TFmVSPrePalCab.PMCabPopup(Sender: TObject);
begin
  CabAltera1.Enabled := False;
  CabExclui1.Enabled := False;
(*
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPrePalCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSPrePalCab, QrCadastro_Com_Itens_ITS);
*)
end;

procedure TFmVSPrePalCab.PMItsPopup(Sender: TObject);
begin
(*
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSPrePalCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCadastro_Com_Itens_ITS);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCadastro_Com_Itens_ITS);
*)
end;

procedure TFmVSPrePalCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPrePalCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPrePalCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsprepalcab';
  VAR_GOTOMYSQLTABLE := QrVSPrePalCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ppc.*, vsp.Nome, vsp.Empresa,');
  VAR_SQLx.Add('CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR, vsp.GraGruX,');
  VAR_SQLx.Add('vmc.Codigo MovimCod, vmc.TemIMEIMrt ');
  VAR_SQLx.Add('FROM vsprepalcab ppc');
  VAR_SQLx.Add('LEFT JOIN vspalleta vsp ON vsp.Codigo=ppc.Pallet');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=vsp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN vsmovcab vmc ON vmc.CodigoID=ppc.Codigo ');
  VAR_SQLx.Add('  AND vmc.MovimID=' + Geral.FF0(Integer(emidPreReclasse)));
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE ppc.Codigo > 0');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND (vsp.Nome OR CONCAT(gg1.Nome, ');
  VAR_SQLa.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLa.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ');
  VAR_SQLa.Add(') Like :P0');
  //
end;

procedure TFmVSPrePalCab.ItsAltera1Click(Sender: TObject);
begin
  //MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmVSPrePalCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSPrePalCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSPrePalCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPrePalCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
  Integer(TEstqMotivDel.emtdWetCurti???), Dmod.QrUpd, Dmod.MyDB, CO_MASTER?) then

    begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCadastro_Com_Itens_ITS,
      QrCadastro_Com_Itens_ITSControle, QrCadastro_Com_Itens_ITSControle.Value);
    ReopenPrePalOri(Controle);
  end;
}
end;

procedure TFmVSPrePalCab.ReopenPrePalDst(Controle: Integer);
const
  TemIMEIMrt = 1;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet, ',
  'vmi.ValorT / vmi.AreaM2 CustoM2 ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Codigo=' + Geral.FF0(QrVSPrePalCabCodigo.Value),
  'AND vmi.MovimID=' + Geral.FF0(Integer(emidPreReclasse)), //15
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestPreReclas)), //12
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPrePalDst, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVsInnIts);
  //
  QrVSPrePalDst.Locate('Controle', Controle, []);
(*
  // Por Pallet  ????
  //
*)
end;

procedure TFmVSPrePalCab.ReopenPrePalOri(Controle: Integer);
const
  TemIMEIMrt = 1;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet, ',
  'vmi.ValorT / vmi.AreaM2 CustoM2 ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Codigo=' + Geral.FF0(QrVSPrePalCabCodigo.Value),
  'AND vmi.MovimID=' + Geral.FF0(Integer(emidPreReclasse)), //15
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)), //11
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPrePalOri, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVsInnIts);
  //
  QrVSPrePalOri.Locate('Controle', Controle, []);
(*
  // Por Pallet  ????
  //
*)
end;


procedure TFmVSPrePalCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPrePalCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPrePalCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPrePalCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPrePalCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPrePalCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPrePalCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPrePalCabCodigo.Value;
  Close;
end;

procedure TFmVSPrePalCab.ItsInclui1Click(Sender: TObject);
begin
  //MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmVSPrePalCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPrePalCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsprepalcab');
end;

procedure TFmVSPrePalCab.BtConfirmaClick(Sender: TObject);
{
var
  Codigo: Integer;
  Nome: String;
}
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('vsprepalcab', 'Codigo', ImgTipo.SQLType,
    QrVSPrePalCabCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('vsprepalcab', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrVSPrePalCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'vsprepalcab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmVSPrePalCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsprepalcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsprepalcab', 'Codigo');
end;

procedure TFmVSPrePalCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSPrePalCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPrePalCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSPrePalCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPrePalCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPrePalCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSPrePalCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSPrePalCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPrePalCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPrePalCab.QrVSPrePalCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPrePalCab.QrVSPrePalCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPrePalOri(0);
  ReopenPrePalDst(0);
end;

procedure TFmVSPrePalCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPrePalCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPrePalCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSPrePalCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsprepalcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSPrePalCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPrePalCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPrePalCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsprepalcab');
end;

procedure TFmVSPrePalCab.CorrigeFornecedor1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaFornecedorPreRcl(QrVSPrePalCabMovimCod.Value);
  LocCod(QrVSPrePalCabCodigo.Value, QrVSPrePalCabCodigo.Value);
end;

procedure TFmVSPrePalCab.QrVSPrePalCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPrePalOri.Close;
  QrVSPrePalDst.Close;
end;

procedure TFmVSPrePalCab.QrVSPrePalCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPrePalCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

