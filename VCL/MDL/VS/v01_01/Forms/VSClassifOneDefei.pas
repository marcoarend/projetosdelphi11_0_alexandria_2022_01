unit VSClassifOneDefei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Vcl.Imaging.pngimage, Vcl.Imaging.jpeg, dmkRadioGroup, mySQLDbTables,
  dmkDBGridZTO;

type
  TFmVSClassifOneDefei = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ImgCouro: TImage;
    QrDefeitosTip: TmySQLQuery;
    QrDefeitosTipCodigo: TIntegerField;
    QrDefeitosTipNome: TWideStringField;
    QrDefeitosTipLk: TIntegerField;
    QrDefeitosTipDataCad: TDateField;
    QrDefeitosTipDataAlt: TDateField;
    QrDefeitosTipUserCad: TIntegerField;
    QrDefeitosTipUserAlt: TIntegerField;
    QrDefeitosTipProcede: TIntegerField;
    QrDefeitosTipSigla: TWideStringField;
    QrDefeitosTipSimbolo: TIntegerField;
    DsDefeitosTip: TDataSource;
    DBGDefeitos: TdmkDBGridZTO;
    Panel5: TPanel;
    SGDefeiDefin: TStringGrid;
    Splitter1: TSplitter;
    Image1: TImage;
    QrDefeitosTipCorBorda: TIntegerField;
    QrDefeitosTipCorMiolo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImgCouroClick(Sender: TObject);
    procedure ImgCouroMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure QrDefeitosTipAfterOpen(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure SGDefeiDefinKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FX, FY, FItens: Integer;
    //
    procedure AdicionaDefeito(Item, X, Y, Defeito: Integer; Nome: String;
              Tipo, CorBorda, CorMiolo: Integer);

  public
    { Public declarations }
    FSeqDefIni: Integer;
    FSGDefeiDefin: TStringGrid;
  end;

  var
  FmVSClassifOneDefei: TFmVSClassifOneDefei;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSClassifOneDefei.AdicionaDefeito(Item, X, Y, Defeito: Integer;
  Nome: String; Tipo, CorBorda, CorMiolo: Integer);
begin
  SGDefeiDefin.RowCount := Item + 1;
  (*Seq.*)     SGDefeiDefin.Cells[0, Item] := IntToStr(Item);
  (*X*)        SGDefeiDefin.Cells[1, Item] := IntToStr(X);
  (*Y*)        SGDefeiDefin.Cells[2, Item] := IntToStr(Y);
  (*C�digo*)   SGDefeiDefin.Cells[3, Item] := IntToStr(Defeito);
  (*Nome def.*)SGDefeiDefin.Cells[4, Item] := Nome;
  (*Tipo*)     SGDefeiDefin.Cells[5, Item] := IntToStr(Tipo);
  (*CorBorda*) SGDefeiDefin.Cells[6, Item] := IntToStr(CorBorda);
  (*CorMiolo*) SGDefeiDefin.Cells[7, Item] := IntToStr(CorMiolo);
  //
  BtOK.Enabled := True;
end;

procedure TFmVSClassifOneDefei.BtOKClick(Sender: TObject);
var
  I, N: Integer;
begin
  for I := 1 to SGDefeiDefin.RowCount - 1 do
  begin
    N := FSeqDefIni + I -1;
    FSGDefeiDefin.RowCount := N + 1;
    FSGDefeiDefin.Cells[0, N] := IntToStr(N);
    FSGDefeiDefin.Cells[1, N] := SGDefeiDefin.Cells[1, I];
    FSGDefeiDefin.Cells[2, N] := SGDefeiDefin.Cells[2, I];
    FSGDefeiDefin.Cells[3, N] := SGDefeiDefin.Cells[3, I];
    FSGDefeiDefin.Cells[4, N] := SGDefeiDefin.Cells[4, I];
    FSGDefeiDefin.Cells[5, N] := SGDefeiDefin.Cells[5, I];
    FSGDefeiDefin.Cells[6, N] := SGDefeiDefin.Cells[6, I];
    FSGDefeiDefin.Cells[7, N] := SGDefeiDefin.Cells[7, I];
  end;
  //
  Close;
end;

procedure TFmVSClassifOneDefei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSClassifOneDefei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClassifOneDefei.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FItens := 0;
  //
  SGDefeiDefin.Cells[0,0] := 'Seq.';
  SGDefeiDefin.Cells[1,0] := 'X';
  SGDefeiDefin.Cells[2,0] := 'Y';
  SGDefeiDefin.Cells[3,0] := 'C�digo';
  SGDefeiDefin.Cells[4,0] := 'Nome do defeito';
  SGDefeiDefin.Cells[5,0] := 'S�mbolo';
  //
  UnDmkDAC_PF.AbreQuery(QrDefeitosTip, Dmod.MyDB);
  //
  Image1.Picture.Bitmap.Assign(ImgCouro.Picture.Bitmap);
end;

procedure TFmVSClassifOneDefei.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClassifOneDefei.ImgCouroClick(Sender: TObject);
var
  X, Y, Defeito, Tipo, CorBorda, CorMiolo: Integer;
  Nome: String;
begin
  X := FX;
  Y := FY;
  Defeito  := QrDefeitosTipCodigo.Value;
  Nome     := QrDefeitosTipNome.Value;
  Tipo     := QrDefeitosTipSimbolo.Value;
  CorBorda := QrDefeitosTipCorBorda.Value;
  CorMiolo := QrDefeitosTipCorMiolo.Value;
  //
  if MyObjects.FIC(Defeito = 0, DBGDefeitos,
  'Primeiro selecione um defeito v�lido!') then Exit;
  //
  FItens := FItens + 1;
  AdicionaDefeito(FItens, X, Y, Defeito, Nome, Tipo, CorBorda, CorMiolo);
  //
  VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgCouro, X, Y,
  QrDefeitosTipSimbolo.Value, QrDefeitosTipCorBorda.Value, QrDefeitosTipCorMiolo.Value);
  //
end;

procedure TFmVSClassifOneDefei.ImgCouroMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  FY := Y;
  FX := X;
end;

procedure TFmVSClassifOneDefei.QrDefeitosTipAfterOpen(DataSet: TDataSet);
begin
  QrDefeitostip.Locate('Codigo', 0, []);
end;

procedure TFmVSClassifOneDefei.SGDefeiDefinKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  I, X, Y, Simbolo, CorBorda, CorMiolo: Integer;
begin
  if Key = VK_DELETE then
  begin
    if SGDefeiDefin.Row > 0 then
    begin
      MyObjects.ExcluiLinhaStringGrid(SGDefeiDefin, True);
      ImgCouro.Picture.Bitmap.Assign(Image1.Picture.Bitmap);
      if FItens > 0 then
        FItens := FItens - 1;
      //
      for I := 1 to SGDefeiDefin.RowCount - 1 do
      begin
        X := Geral.IMV(SGDefeiDefin.Cells[1, I]);
        Y := Geral.IMV(SGDefeiDefin.Cells[2, I]);
        Simbolo := Geral.IMV(SGDefeiDefin.Cells[5, I]);
        CorBorda := Geral.IMV(SGDefeiDefin.Cells[6, I]);
        CorMiolo := Geral.IMV(SGDefeiDefin.Cells[7, I]);
        //
        VS_CRC_PF.DesenhaSimboloDefeito_Image(ImgCouro, X, Y, Simbolo, CorBorda,
          CorMiolo);
      end;
    end;
  end;
end;

end.
