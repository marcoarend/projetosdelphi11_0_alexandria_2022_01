unit VSRtbCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, UnProjGroup_Consts, UnGrl_Consts, UnAppEnums, UnProjGroup_Vars;

type
  TFmVSRtbCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSRtbCab: TmySQLQuery;
    DsVSRtbCab: TDataSource;
    QrVSRtbIts: TmySQLQuery;
    DsVSRtbIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSRtbCabCodigo: TIntegerField;
    QrVSRtbCabMovimCod: TIntegerField;
    QrVSRtbCabEmpresa: TIntegerField;
    QrVSRtbCabDtCompra: TDateTimeField;
    QrVSRtbCabDtViagem: TDateTimeField;
    QrVSRtbCabDtEntrada: TDateTimeField;
    QrVSRtbCabFornecedor: TIntegerField;
    QrVSRtbCabTransporta: TIntegerField;
    QrVSRtbCabPecas: TFloatField;
    QrVSRtbCabPesoKg: TFloatField;
    QrVSRtbCabAreaM2: TFloatField;
    QrVSRtbCabAreaP2: TFloatField;
    QrVSRtbCabLk: TIntegerField;
    QrVSRtbCabDataCad: TDateField;
    QrVSRtbCabDataAlt: TDateField;
    QrVSRtbCabUserCad: TIntegerField;
    QrVSRtbCabUserAlt: TIntegerField;
    QrVSRtbCabAlterWeb: TSmallintField;
    QrVSRtbCabAtivo: TSmallintField;
    QrVSRtbCabNO_EMPRESA: TWideStringField;
    QrVSRtbCabNO_FORNECE: TWideStringField;
    QrVSRtbCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    N2: TMenuItem;
    QrVSRtbCabValorT: TFloatField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    N1: TMenuItem;
    Atualizaestoque1: TMenuItem;
    frxDsVSRtbCab: TfrxDBDataset;
    frxDsVSRtbIts: TfrxDBDataset;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    QrVSRtbCabClienteMO: TIntegerField;
    QrVSRtbCabProcednc: TIntegerField;
    QrVSRtbCabMotorista: TIntegerField;
    QrVSRtbCabPlaca: TWideStringField;
    QrVSRtbCabNO_CLIENTEMO: TWideStringField;
    QrVSRtbCabNO_PROCEDNC: TWideStringField;
    QrVSRtbCabNO_MOTORISTA: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrVSMovDif: TmySQLQuery;
    QrVSMovDifControle: TIntegerField;
    QrVSMovDifInfPecas: TFloatField;
    QrVSMovDifInfPesoKg: TFloatField;
    QrVSMovDifInfAreaM2: TFloatField;
    QrVSMovDifInfAreaP2: TFloatField;
    QrVSMovDifInfValorT: TFloatField;
    QrVSMovDifLk: TIntegerField;
    QrVSMovDifDataCad: TDateField;
    QrVSMovDifDataAlt: TDateField;
    QrVSMovDifUserCad: TIntegerField;
    QrVSMovDifUserAlt: TIntegerField;
    QrVSMovDifAlterWeb: TSmallintField;
    QrVSMovDifAtivo: TSmallintField;
    QrVSMovDifDifPecas: TFloatField;
    QrVSMovDifDifPesoKg: TFloatField;
    QrVSMovDifDifAreaM2: TFloatField;
    QrVSMovDifDifAreaP2: TFloatField;
    QrVSMovDifDifValorT: TFloatField;
    Histrico1: TMenuItem;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    TabSheet2: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSHisFch: TmySQLQuery;
    DsVSHisFch: TDataSource;
    QrVSHisFchCodigo: TIntegerField;
    QrVSHisFchVSMovIts: TIntegerField;
    QrVSHisFchSerieFch: TIntegerField;
    QrVSHisFchFicha: TIntegerField;
    QrVSHisFchDataHora: TDateTimeField;
    QrVSHisFchNome: TWideStringField;
    QrVSHisFchObserv: TWideStringField;
    QrVSHisFchLk: TIntegerField;
    QrVSHisFchDataCad: TDateField;
    QrVSHisFchDataAlt: TDateField;
    QrVSHisFchUserCad: TIntegerField;
    QrVSHisFchUserAlt: TIntegerField;
    QrVSHisFchAlterWeb: TSmallintField;
    QrVSHisFchAtivo: TSmallintField;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    DBMemo1: TDBMemo;
    Exclui1: TMenuItem;
    Splitter2: TSplitter;
    PMVRtbIts: TPopupMenu;
    IrparajaneladegerenciamentodeFichaRMP1: TMenuItem;
    DGDados: TdmkDBGridZTO;
    FichadePallets1: TMenuItem;
    Fichadetodospalletsdestacompra1: TMenuItem;
    PackingList1: TMenuItem;
    frxDsPallets: TfrxDBDataset;
    frxWET_CURTI_106_00_A: TfrxReport;
    FichaCOMnomedoPallet1: TMenuItem;
    FichaSEMnomedoPallet1: TMenuItem;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSRtbCabTemIMEIMrt: TIntegerField;
    QrVSRtbItsCodigo: TLargeintField;
    QrVSRtbItsControle: TLargeintField;
    QrVSRtbItsMovimCod: TLargeintField;
    QrVSRtbItsMovimNiv: TLargeintField;
    QrVSRtbItsMovimTwn: TLargeintField;
    QrVSRtbItsEmpresa: TLargeintField;
    QrVSRtbItsTerceiro: TLargeintField;
    QrVSRtbItsCliVenda: TLargeintField;
    QrVSRtbItsMovimID: TLargeintField;
    QrVSRtbItsDataHora: TDateTimeField;
    QrVSRtbItsPallet: TLargeintField;
    QrVSRtbItsGraGruX: TLargeintField;
    QrVSRtbItsPecas: TFloatField;
    QrVSRtbItsPesoKg: TFloatField;
    QrVSRtbItsAreaM2: TFloatField;
    QrVSRtbItsAreaP2: TFloatField;
    QrVSRtbItsValorT: TFloatField;
    QrVSRtbItsSrcMovID: TLargeintField;
    QrVSRtbItsSrcNivel1: TLargeintField;
    QrVSRtbItsSrcNivel2: TLargeintField;
    QrVSRtbItsSrcGGX: TLargeintField;
    QrVSRtbItsSdoVrtPeca: TFloatField;
    QrVSRtbItsSdoVrtPeso: TFloatField;
    QrVSRtbItsSdoVrtArM2: TFloatField;
    QrVSRtbItsObserv: TWideStringField;
    QrVSRtbItsSerieFch: TLargeintField;
    QrVSRtbItsFicha: TLargeintField;
    QrVSRtbItsMisturou: TLargeintField;
    QrVSRtbItsFornecMO: TLargeintField;
    QrVSRtbItsCustoMOKg: TFloatField;
    QrVSRtbItsCustoMOM2: TFloatField;
    QrVSRtbItsCustoMOTot: TFloatField;
    QrVSRtbItsValorMP: TFloatField;
    QrVSRtbItsDstMovID: TLargeintField;
    QrVSRtbItsDstNivel1: TLargeintField;
    QrVSRtbItsDstNivel2: TLargeintField;
    QrVSRtbItsDstGGX: TLargeintField;
    QrVSRtbItsQtdGerPeca: TFloatField;
    QrVSRtbItsQtdGerPeso: TFloatField;
    QrVSRtbItsQtdGerArM2: TFloatField;
    QrVSRtbItsQtdGerArP2: TFloatField;
    QrVSRtbItsQtdAntPeca: TFloatField;
    QrVSRtbItsQtdAntPeso: TFloatField;
    QrVSRtbItsQtdAntArM2: TFloatField;
    QrVSRtbItsQtdAntArP2: TFloatField;
    QrVSRtbItsNotaMPAG: TFloatField;
    QrVSRtbItsPedItsFin: TLargeintField;
    QrVSRtbItsMarca: TWideStringField;
    QrVSRtbItsStqCenLoc: TLargeintField;
    QrVSRtbItsNO_PALLET: TWideStringField;
    QrVSRtbItsNO_PRD_TAM_COR: TWideStringField;
    QrVSRtbItsNO_TTW: TWideStringField;
    QrVSRtbItsID_TTW: TLargeintField;
    QrVSRtbItsReqMovEstq: TLargeintField;
    QrVSRtbItsRendKgm2: TFloatField;
    QrVSRtbItsNotaMPAG_TXT: TWideStringField;
    QrVSRtbItsRendKgm2_TXT: TWideStringField;
    QrVSRtbItsMisturou_TXT: TWideStringField;
    QrVSRtbItsNOMEUNIDMED: TWideStringField;
    QrVSRtbItsSIGLAUNIDMED: TWideStringField;
    QrVSRtbItsm2_CouroTXT: TWideStringField;
    QrVSRtbItsKgMedioCouro: TFloatField;
    QrVSRtbItsSEQ: TIntegerField;
    QrVSRtbItsClientMO: TLargeintField;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    Edemi_serie: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    Edemi_nNF: TdmkEdit;
    QrVSRtbCabide_serie: TSmallintField;
    QrVSRtbCabide_nNF: TIntegerField;
    QrVSRtbCabemi_serie: TSmallintField;
    QrVSRtbCabemi_nNF: TIntegerField;
    QrVSRtbCabNFeStatus: TIntegerField;
    QrVSRtbItsIxxMovIX: TLargeintField;
    QrVSRtbItsIxxFolha: TLargeintField;
    QrVSRtbItsIxxLinha: TLargeintField;
    TsFrCompr: TTabSheet;
    TsEnvioMO: TTabSheet;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvAvu: TmySQLQuery;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    DsVSMOEnvAVMI: TDataSource;
    PnFrCompr: TPanel;
    DBGVSMOEnvAvu: TdmkDBGridZTO;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVMI: TdmkDBGridZTO;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    AtrelamentoFreteCompra1: TMenuItem;
    N3: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    QrVSRtbItsCusFrtAvuls: TFloatField;
    QrVSRtbItsCusFrtMOEnv: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRtbCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRtbCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSRtbCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSRtbCabBeforeClose(DataSet: TDataSet);
    procedure QrVSRtbItsBeforeClose(DataSet: TDataSet);
    procedure QrVSRtbItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure frxWET_CURTI_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichaRMP1Click(Sender: TObject);
    procedure QrVSRtbItsCalcFields(DataSet: TDataSet);
    procedure PackingList1Click(Sender: TObject);
    procedure frxWET_CURTI_106_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure FichaCOMnomedoPallet1Click(Sender: TObject);
    procedure FichaSEMnomedoPallet1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSRtbIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure MostraFormVSHisFchAdd(SQLType: TSQLType);
    procedure ImprimeFichaDePallet(InfoNO_PALLET: Boolean);
    procedure ImprimeTodasFichasDePallet(InfoNO_PALLET: Boolean);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenVSRtbIts(Controle: Integer);
    procedure ReopenVSHisFch(Codigo: Integer);

  end;

var
  FmVSRtbCab: TFmVSRtbCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSRtbIts, ModuleGeral,
  Principal, VSMovImp, UnVS_PF, VSHisFchAdd, CreateVS, ModVS, ModVS_CRC,
  UnVS_CRC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSRtbCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSRtbCab.MostraFormVSHisFchAdd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSHisFchAdd, FmVSHisFchAdd, afmoNegarComAviso) then
  begin
    FmVSHisFchAdd.ImgTipo.SQLType := SQLType;
    FmVSHisFchAdd.FQrIts                  := QrVSHisFch;
    FmVSHisFchAdd.EdVSMovIts.ValueVariant := QrVSRtbItsControle.Value;
    FmVSHisFchAdd.EdSerieFch.ValueVariant := QrVSRtbItsSerieFch.Value;
    FmVSHisFchAdd.EdFicha.ValueVariant    := QrVSRtbItsFicha.Value;
    if SQLType = stUpd then
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := QrVSHisFchCodigo.Value;
      FmVSHisFchAdd.TPDataHora.Date       := QrVSHisFchDataHora.Value;
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(QrVSHisFchDataHora.Value, 100);
      FmVSHisFchAdd.EdNome.Text           := QrVSHisFchNome.Value;
      FmVSHisFchAdd.MeObserv.Text         := QrVSHisFchObserv.Value;
    end else
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := 0;
      FmVSHisFchAdd.TPDataHora.Date       := DModG.ObtemAgora();
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(DModG.ObtemAgora(), 100);
      FmVSHisFchAdd.EdNome.Text           := '';
      FmVSHisFchAdd.MeObserv.Text         := '';
    end;
    FmVSHisFchAdd.ShowModal;
    FmVSHisFchAdd.Destroy;
  end;
end;

procedure TFmVSRtbCab.MostraFormVSRtbIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSRtbIts, FmVSRtbIts, afmoNegarComAviso) then
  begin
    FmVSRtbIts.ImgTipo.SQLType := SQLType;
    FmVSRtbIts.FQrCab := QrVSRtbCab;
    FmVSRtbIts.FDsCab := DsVSRtbCab;
    FmVSRtbIts.FQrIts := QrVSRtbIts;
    FmVSRtbIts.FDataHora := QrVSRtbCabDtEntrada.Value;
    FmVSRtbIts.FEmpresa  := QrVSRtbCabEmpresa.Value;
    FmVSRtbIts.FClientMO := QrVSRtbCabClienteMO.Value;
    //
    FmVSRtbIts.EdFornecedor.ValueVariant := QrVSRtbCabFornecedor.Value;
    FmVSRtbIts.CBFornecedor.KeyValue     := QrVSRtbCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmVSRtbIts.EdCPF1.ReadOnly := False
    end else
    begin
      //
      FmVSRtbIts.EdControle.ValueVariant := QrVSRtbItsControle.Value;
      //
      FmVSRtbIts.EdGragruX.ValueVariant    := QrVSRtbItsGraGruX.Value;
      FmVSRtbIts.CBGragruX.KeyValue        := QrVSRtbItsGraGruX.Value;
      FmVSRtbIts.EdSerieFch.ValueVariant   := QrVSRtbItsSerieFch.Value;
      FmVSRtbIts.CBSerieFch.KeyValue       := QrVSRtbItsSerieFch.Value;
      FmVSRtbIts.EdFicha.ValueVariant      := QrVSRtbItsFicha.Value;
      FmVSRtbIts.EdPallet.ValueVariant     := QrVSRtbItsPallet.Value;
      FmVSRtbIts.CBPallet.KeyValue         := QrVSRtbItsPallet.Value;
      FmVSRtbIts.EdPecas.ValueVariant      := QrVSRtbItsPecas.Value;
      FmVSRtbIts.EdPesoKg.ValueVariant     := QrVSRtbItsPesoKg.Value;
      FmVSRtbIts.EdAreaM2.ValueVariant     := QrVSRtbItsAreaM2.Value;
      FmVSRtbIts.EdAreaP2.ValueVariant     := QrVSRtbItsAreaP2.Value;
      FmVSRtbIts.EdValorMP.ValueVariant    := QrVSRtbItsValorMP.Value;
      FmVSRtbIts.EdCustoMOKg.ValueVariant  := QrVSRtbItsCustoMOKg.Value;
      FmVSRtbIts.EdFornecMO.ValueVariant   := QrVSRtbItsFornecMO.Value;
      FmVSRtbIts.CBFornecMO.KeyValue       := QrVSRtbItsFornecMO.Value;
      FmVSRtbIts.EdObserv.ValueVariant     := QrVSRtbItsObserv.Value;
      FmVSRtbIts.EdStqCenLoc.ValueVariant  := QrVSRtbItsStqCenLoc.Value;
      FmVSRtbIts.CBStqCenLoc.KeyValue      := QrVSRtbItsStqCenLoc.Value;
      FmVSRtbIts.EdReqMovEstq.ValueVariant := QrVSRtbItsReqMovEstq.Value;
      FmVSRtbIts.RGIxxMovIX.ItemIndex      := QrVSRtbItsIxxMovIX.Value;
      FmVSRtbIts.EdIxxFolha.ValueVariant   := QrVSRtbItsIxxFolha.Value;
      FmVSRtbIts.EdIxxLinha.ValueVariant   := QrVSRtbItsIxxLinha.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSRtbIts.EdCustoMOTot.ValueVariant := QrVSRtbItsCustoMOTot.Value;
      FmVSRtbIts.EdValorT.ValueVariant     := QrVSRtbItsValorT.Value;
    end;
    FmVSRtbIts.ShowModal;
    FmVSRtbIts.Destroy;
  end;
end;

procedure TFmVSRtbCab.PackingList1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_106_00_A, [
  DModG.frxDsDono,
  frxDsPallets
  ]);
  MyObjects.frxMostra(frxWET_CURTI_106_00_A, 'Packing List Retrabalho');
end;

procedure TFmVSRtbCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSRtbCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSRtbCab, QrVSRtbIts);
end;

procedure TFmVSRtbCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSRtbCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSRtbIts);
  //MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrVSRtbIts, QrVSItsBxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSRtbIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSRtbItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrVSRtbIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrVSMOEnvAvu);
  AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSRtbIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
end;

procedure TFmVSRtbCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRtbCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRtbCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsrtbcab';
  VAR_GOTOMYSQLTABLE := QrVSRtbCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA');
  VAR_SQLx.Add('FROM vsrtbcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSRtbCab.Estoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSRtbCab.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSHisFchCodigo.Value;
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de hist�rico?',
  'vshisfch', 'Codigo', Codigo, DMod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSHisFch,
      QrVSHisFchCodigo, QrVSHisFchCodigo.Value);
    ReopenVSHisFch(Codigo);
  end;
end;

procedure TFmVSRtbCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.ImprimeFichaDePallet(InfoNO_PALLET: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  Empresa  := QrVSRtbItsEmpresa.Value;
  ClientMO := QrVSRtbItsClientMO.Value;
  Pallet   := QrVSRtbItsPallet.Value;
  TempTab  := Self.Name;
  VS_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_PALLET);
end;

procedure TFmVSRtbCab.ImprimeTodasFichasDePallet(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSRtbItsEmpresa.Value;
  N := 0;
  QrVSRtbIts.First;
  while not QrVSRtbIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSRtbItsPallet.Value;
    //
    QrVSRtbIts.Next;
  end;
  if N > 0 then
    VS_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSRtbCab.Inclui1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stIns);
end;

procedure TFmVSRtbCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSRtbIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, QrVSRtbCabide_Serie.Value, QrVSRtbCabide_nNF.Value);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSRtbCabMovimCod.Value, 0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.IncluiAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, QrVSRtbIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSRtbCabFornecedor.Value, QrVSRtbCabide_serie.Value, QrVSRtbCabide_nNF.Value);

  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSRtbCabMovimCod.Value, 0);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.IrparajaneladegerenciamentodeFichaRMP1Click(
  Sender: TObject);
var
  SerieFicha, Ficha: Integer;
begin
  SerieFicha := QrVSRtbItsSerieFch.Value;
  Ficha      := QrVSRtbItsFicha.Value;
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
end;

procedure TFmVSRtbCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSRtbIts(stUpd);
end;

procedure TFmVSRtbCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSRtbCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSRtbCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRtbCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrVSRtbItsCodigo.Value;
  MovimCod := QrVSRtbItsMovimCod.Value;
  Controle := QrVSRtbItsControle.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSRtbIts, TIntegerField(QrVSRtbItsControle),
  Controle, CtrlBaix, QrVSRtbItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti106)) then
  begin
    // Exclui tambem da tabela vsmovdif
    VS_PF.ExcluiVSNaoVMI('', 'vsmovdif', 'Controle', Controle, Dmod.MyDB);
    // Atualiza!
    VS_PF.AtualizaTotaisVSXxxCab('vsrtbcab', MovimCod);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSRtbCab.ReopenVSHisFch(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSHisFch, Dmod.MyDB, [
  'SELECT * ',
  'FROM vshisfch ',
  'WHERE VSMovIts=' + Geral.FF0(QrVSRtbItsControle.Value),
  'OR ( ',
  '  SerieFch=' + Geral.FF0(QrVSRtbItsSerieFch.Value),
  '  AND ',
  '  Ficha=' + Geral.FF0(QrVSRtbItsFicha.Value),
  ') ',
  '']);
  //
  if Codigo <> 0 then
    QrVSHisFch.Locate('Codigo', Codigo, []);
end;

procedure TFmVSRtbCab.ReopenVSRtbIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRtbIts, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSRtbCabMovimCod.Value),
  'ORDER BY NO_Pallet, vmi.Controle ',
  '']);
  //
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSRtbCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  //'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  '']);
  //'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSRtbCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRtbIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVsInnIts);
  //
  QrVSRtbIts.Locate('Controle', Controle, []);
end;


procedure TFmVSRtbCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRtbCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRtbCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRtbCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRtbCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRtbCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRtbCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRtbCabCodigo.Value;
  Close;
end;

procedure TFmVSRtbCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSRtbIts(stIns);
end;

procedure TFmVSRtbCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSRtbCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'vsrtbcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSRtbCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSRtbCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_serie, ide_nNF, emi_serie, emi_nNF: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_serie      := Edemi_serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsrtbcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsrtbcab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_serie', 'ide_nNF',
  'emi_serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta, (* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)
  ClienteMO, Procednc, Motorista,
  Placa, ide_serie, ide_nNF,
  ide_serie, ide_nNF
  ], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidRetrabalho, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'Terceiro', CO_DATA_HORA_VMI,
      'ClientMO'], ['MovimCod'], [
      Empresa, Terceiro, DataHora,
      ClienteMO], [MovimCod], True);
      //
      AtualizaNFeItens();
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormVSRtbIts(stIns);
  end;
end;

procedure TFmVSRtbCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsrtbcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsrtbcab', 'Codigo');
end;

procedure TFmVSRtbCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSRtbCab.BtReclasifClick(Sender: TObject);
begin
  VS_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmVSRtbCab.Altera1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stUpd);
end;

procedure TFmVSRtbCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSRtbIts, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siPositivo, 0, 0);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSRtbCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stUpd, QrVSRtbIts, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSRtbCabFornecedor.Value, QrVSRtbCabide_serie.Value, QrVSRtbCabide_nNF.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSRtbCabMovimCod.Value, 0);
  //
  LocCod(QrVSRtbCabCodigo.Value,QrVSRtbCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSRtbCab.Atualizaestoque1Click(Sender: TObject);
begin
  VS_PF.AtualizaSaldoIMEI(QrVSRtbItsControle.Value, True);
end;

procedure TFmVSRtbCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSRtbCabMovimCod.Value, TEstqMovimID.emidRetrabalho, [(**)], [eminSemNiv]);
end;

procedure TFmVSRtbCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSRtbCab, QrVSRtbCabDtCompra.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSRtbCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PCItens.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSRtbCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRtbCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSRtbCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSRtbCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSRtbCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSRtbCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSRtbCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSRtbCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRtbCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRtbCab.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi, QrVSMOEnvAvuCodigo.VAlue, 0);
end;

procedure TFmVSRtbCab.QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSRtbCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVMI(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.VAlue, 0);
end;

procedure TFmVSRtbCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

procedure TFmVSRtbCab.QrVSRtbCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRtbCab.QrVSRtbCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSRtbIts(0);
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSRtbCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSRtbCabMovimCod.Value, 0);
end;

procedure TFmVSRtbCab.FichaCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichaDePallet(True);
end;

procedure TFmVSRtbCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodasFichasDePallet(True);
end;

procedure TFmVSRtbCab.FichaSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichaDePallet(False);
end;

procedure TFmVSRtbCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodasFichasDePallet(False);
end;

procedure TFmVSRtbCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSRtbCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSRtbCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidRetrabalho, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSRtbIts(Controle);
  end;
end;

procedure TFmVSRtbCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSRtbCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRtbCab.frxWET_CURTI_006_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSRtbCab.frxWET_CURTI_106_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := QrVSRtbCabNO_EMPRESA.Value
  else
  if VarName = 'VARF_CLIENTE' then
    Value := QrVSRtbCabNO_FORNECE.Value
  else
  if VarName = 'VARF_ID' then
    Value := QrVSRtbCabCodigo.Value
  else
  if VarName ='VARF_DATA' then
    Value := Now()
end;

procedure TFmVSRtbCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSRtbCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSRtbCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsrtbcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

procedure TFmVSRtbCab.QrVSRtbCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvEnv.Close;
  QrVSMOEnvAvu.Close;
  QrVSRtbIts.Close;
end;

procedure TFmVSRtbCab.QrVSRtbCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSRtbCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSRtbCab.QrVSRtbItsAfterScroll(DataSet: TDataSet);
begin
  VS_PF.ReopenVSMovDif(QrVSMovDif, QrVSRtbItsControle.Value);
  ReopenVSHisFch(0);
end;

procedure TFmVSRtbCab.QrVSRtbItsBeforeClose(DataSet: TDataSet);
begin
  QrVSMovDif.Close;
  QrVSHisFch.Close;
end;

procedure TFmVSRtbCab.QrVSRtbItsCalcFields(DataSet: TDataSet);
begin
  QrVSRtbItsSEQ.Value := QrVSRtbIts.RecNo;
end;

end.

