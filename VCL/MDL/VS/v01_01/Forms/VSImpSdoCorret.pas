unit VSImpSdoCorret;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, CreateVS, frxClass, frxDBSet;

type
  TFmVSImpSdoCorret = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSMovIts: TMySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_MovimID: TWideStringField;
    QrVSMovItsMediaArM2: TFloatField;
    QrVSMovItsMediaPeso: TFloatField;
    DsVSMovIts: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrVSAnt: TMySQLQuery;
    QrVSAntPecas: TFloatField;
    QrVSAntPesoKg: TFloatField;
    QrVSAntAreaM2: TFloatField;
    QrVSAntAreaP2: TFloatField;
    QrVSAntVAlorT: TFloatField;
    QrVSAntSdoVrtPeca: TFloatField;
    QrVSAntSdoVrtPeso: TFloatField;
    QrVSAntSdoVrtArM2: TFloatField;
    QrVSAntSdoVrtArP2: TFloatField;
    DsVSAnt: TDataSource;
    QrVSAntCusM2: TFloatField;
    DsVSAtu: TDataSource;
    QrVSAtu: TMySQLQuery;
    PB1: TProgressBar;
    frxDsVSAtu: TfrxDBDataset;
    frxDsVSAnt: TfrxDBDataset;
    GroupBox3: TGroupBox;
    TP02DataIni: TdmkEditDateTimePicker;
    Ck02DataIni: TCheckBox;
    Ck02DataFim: TCheckBox;
    TP02DataFim: TdmkEditDateTimePicker;
    QrVSAtuControle: TLargeintField;
    QrVSAtuDataHora: TDateTimeField;
    QrVSAtuPecasInn: TFloatField;
    QrVSAtuAreaM2Inn: TFloatField;
    QrVSAtuValorTInn: TFloatField;
    QrVSAtuCustoM2Inn: TFloatField;
    QrVSAtuPecasOut: TFloatField;
    QrVSAtuAreaM2Out: TFloatField;
    QrVSAtuValorTOut: TFloatField;
    QrVSAtuCustoM2Out: TFloatField;
    QrVSAtuAcumPeca: TFloatField;
    QrVSAtuAcumPeso: TFloatField;
    QrVSAtuAcumArM2: TFloatField;
    QrVSAtuAcumArP2: TFloatField;
    QrVSAtuAcumValorT: TFloatField;
    QrVSAtuAcumCusPc: TFloatField;
    QrVSAtuAcumCusKg: TFloatField;
    QrVSAtuAcumCusM2: TFloatField;
    QrVSAtuAcumCusP2: TFloatField;
    frxWET_CURTI_248_1: TfrxReport;
    QrVSAtuPesoKgInn: TFloatField;
    QrVSAtuPesoKgOut: TFloatField;
    QrVSAtuCustoKgInn: TFloatField;
    QrVSAtuCustoKgOut: TFloatField;
    QrVSAntCusKg: TFloatField;
    QrVSAtuNO_MovimID: TWideStringField;
    frxWET_CURTI_248_2: TfrxReport;
    CkProcesso: TCheckBox;
    QrGraGruXGrandeza: TSmallintField;
    QrGraGruY: TMySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    QrGraGruYLk: TIntegerField;
    QrGraGruYDataCad: TDateField;
    QrGraGruYDataAlt: TDateField;
    QrGraGruYUserCad: TIntegerField;
    QrGraGruYUserAlt: TIntegerField;
    QrGraGruYAlterWeb: TSmallintField;
    QrGraGruYAtivo: TSmallintField;
    DsGraGruY: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    dmkDBGridZTO1: TdmkDBGridZTO;
    PCTipoRel: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Panel6: TPanel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdGraGruY: TdmkEditCB;
    CBGraGruY: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrVSAtuGraGruX: TLargeintField;
    QrVSAtuNO_PRD_TAM_COR: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_248_A_1GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    //FVSKardexAnt,
    FVSCardxSdoMov: String;
    FEmpresa, FGraGruX, FGraGruY, FGrandeza: Integer;
    FDataI_Dta, FDtaAnt_Dta: TDateTime;
    FDataI_Txt: String;
    //
    function  DefineSQLMovimID(const Obrigatorio: Boolean; const EdMovimID:
              TdmkEdit; var SQL: String): Boolean;
    procedure PesquisaLancamentos();
    procedure AtualizaEvolucao();
  public
    { Public declarations }
  end;

  var
  FmVSImpSdoCorret: TFmVSImpSdoCorret;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UnGrl_Consts,
  UnDmkProcFunc, UMySQLModule;

{$R *.DFM}

procedure TFmVSImpSdoCorret.AtualizaEvolucao();
var
  AcumPeca, AcumPeso, AcumArM2, AcumArP2, AcumValorT, AcumCusPc, AcumCusKg,
  AcumCusM2, AcumCusP2, AcumSdoVrtPeca, AcumSDoVrtPeso, AcumSdoVrtArM2,
  AcumSdoVrtArP2, AcumSdoVrtValr: Double;
  DataHora, SQL: String;
  Controle: Integer;
begin
  DataHora := Geral.FDT(FDataI_Dta - 1, 1) + ' 23:59:59';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_cardx_sdo_mov ',
  'WHERE DataHora < "' + FDataI_Txt + '"  ',
  ' ']);
  //Geral.MB_Teste(QrVSAnt.SQL.Text);
  //
  AcumPeca     := QrVSAntPecas.Value;
  AcumPeso     := QrVSAntPesoKg.Value;
  AcumArM2     := QrVSAntAreaM2.Value;
  AcumArP2     := QrVSAntAreaP2.Value;
  AcumValorT   := QrVSAntValorT.Value;
  //
  AcumSdoVrtPeca := QrVSAntSdoVrtPeca.Value;
  AcumSDoVrtPeso := QrVSAntSdoVrtPeso.Value;
  AcumSdoVrtArM2 := QrVSAntSdoVrtArM2.Value;
  AcumSdoVrtArP2 := QrVSAntSdoVrtArP2.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
  'SELECT * FROM ' + FVSCardxSdoMov,
  'WHERE DataHora > "' + DataHora + '"',
  //'ORDER BY Controle ',
  'ORDER BY DataHora, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrVSMovIts.RecordCount;
  QrVSMovIts.DisableControls;
  try
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      Controle := QrVSMovItsControle.Value;
      PB1.Position := PB1.Position + 1;
      Application.ProcessMessages;
      //
      AcumPeca     := AcumPeca   + QrVSMovItsPecas.Value;
      AcumPeso     := AcumPeso   + QrVSMovItsPesoKg.Value;
      AcumArM2     := AcumArM2   + QrVSMovItsAreaM2.Value;
      AcumArP2     := AcumArP2   + QrVSMovItsAreaP2.Value;
      AcumValorT   := AcumValorT + QrVSMovItsValorT.Value;
      //
(*
      if AcumPeca <= 0 then
      begin
        AcumPeca     :=  0.00;
        AcumPeso     :=  0.00;
        AcumArM2     :=  0.00;
        AcumArP2     :=  0.00;
        AcumValorT   :=  0.00;
      end;
*)
      if AcumPeca <> 0 then
        AcumCusPc    := AcumValorT / AcumPeca
      else
        AcumCusPc    := 0.00;

      if AcumPeso <> 0 then
        AcumCusKg    := AcumValorT / AcumPeso
      else
        AcumCusKg    := 0.00;

      if AcumArM2 <> 0 then
        AcumCusM2    := AcumValorT / AcumArM2
      else
        AcumCusM2    := 0.00;

      if AcumArP2 <> 0 then
        AcumCusP2    := AcumValorT / AcumArP2
      else
        AcumCusP2    := 0.00;
      //
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
      'UPDATE _vs_cardx_sdo_mov SET ' +
      '  AcumPeca=' + Geral.FFT_Dot(AcumPeca, 2, siNegativo) +
      ', AcumPeso=' + Geral.FFT_Dot(AcumPeso, 2, siNegativo) +
      ', AcumArM2=' + Geral.FFT_Dot(AcumArM2, 2, siNegativo) +
      ', AcumArP2=' + Geral.FFT_Dot(AcumArP2, 2, siNegativo) +
      ', AcumValorT=' + Geral.FFT_Dot(AcumValorT, 2, siNegativo) +
      ', AcumCusPc=' + Geral.FFT_Dot(AcumCusPc, 2, siNegativo) +
      ', AcumCusKg=' + Geral.FFT_Dot(AcumCusKg, 2, siNegativo) +
      ', AcumCusM2=' + Geral.FFT_Dot(AcumCusM2, 4, siNegativo) +
      ', AcumCusP2=' + Geral.FFT_Dot(AcumCusP2, 4, siNegativo) +
      '  WHERE Controle=' + IntToStr(Controle) +
      '');
      //
      QrVSMovIts.Next;
    end;
    QrVSMovIts.First;
  finally
    QrVSMovIts.EnableControls;
  end;
  //
end;

procedure TFmVSImpSdoCorret.BtOKClick(Sender: TObject);
var
  ATT_MovimID: String;
  Report: TfrxReport;
  GGY_OK: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
  if Ck02DataIni.Checked then
    FDataI_Dta  := Int(TP02DataIni.Date)
  else
    FDataI_Dta  := 0;
  //
  FDtaAnt_Dta  := FDataI_Dta - 1;
  FDataI_Txt   := Geral.FDT(FDataI_Dta, 1);
  FGraGruX     := EdGraGruX.ValueVariant;
  //FGraGruY     := QrGraGruXGraGruY.Value;
  FGraGruY     := EdGraGruY.ValueVariant;
  FGrandeza    := QrGraGruXGrandeza.Value;
  //
  case PCTipoRel.ActivePageIndex of
    // GraGruX
    0: if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
    1: if MyObjects.FIC(FGraGruY = 0, EdGraGruY, 'Informe o grupo de estoque!') then Exit;
  end;
(*
  GGY_OK := (FGraGruY = 1024) or (FGraGruY = 2048) or (FGraGruY = 3072);
  if MyObjects.FIC(GGY_OK = False, Ed02GraGruX,
  'Somente artigos "Artigo de Ribeira" est�o habilitados para este relat�rio. Para outros artigos solicite � DERMATEK!') then
   ;// Exit;
*)
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa');
  //FVSKardexAnt := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex, DModG.QrUpdPID1, False, 1, '_vs_cardex_ant');
  FVSCardxSdoMov := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex2, DModG.QrUpdPID1, False, 1, '_vs_cardx_sdo_mov');

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados');
  PesquisaLancamentos();

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando acumulados');
  AtualizaEvolucao();

  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_cardx_sdo_mov ',
  'WHERE DataHora < "' + FDataI_Txt + '"  ',
  ' ']);
*)
  //
  //
  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID_FRENDLY);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAtu, DModG.MyPID_DB, [
  'SELECT GraGruX, NO_PRD_TAM_COR, ',
  'Controle, DataHora, ',
  ATT_MovimID,
  'IF(Pecas > 0, Pecas, 0) PecasInn,',
  'IF(Pecas > 0, PesoKg, 0) PesoKgInn,',
  'IF(Pecas > 0, AreaM2, 0) AreaM2Inn,',
  'IF(Pecas > 0, ValorT, 0) ValorTInn,',
  'IF(Pecas > 0, ',
  '  IF(PesoKg > 0, ValorT / PesoKg, 0), ',
  '0) CustoKgInn,',
  'IF(Pecas > 0, ',
  '  IF(AreaM2 > 0, ValorT / AreaM2, 0), ',
  '0) CustoM2Inn,',
  'IF(Pecas < 0, -Pecas, 0) PecasOut,',
  'IF(Pecas < 0, -PesoKg, 0) PesoKgOut,',
  'IF(Pecas < 0, -AreaM2, 0) AreaM2Out,',
  'IF(Pecas < 0, -ValorT, 0) ValorTOut,',
  'IF(Pecas < 0, ',
  '  IF(PesoKg < 0, ValorT / PesoKg, 0), ',
  '0) CustoKgOut,',
  'IF(Pecas < 0, ',
  '  IF(AreaM2 < 0, ValorT / AreaM2, 0), ',
  '0) CustoM2Out,',
  'AcumPeca, AcumPeso,  AcumArM2, ',
  'AcumArP2, AcumValorT, AcumCusPc, ',
  'AcumCusKg, AcumCusM2, AcumCusP2',
  'FROM ' + FVSCardxSdoMov,
  'WHERE DataHora >= "' + FDataI_Txt + '"  ',
  'ORDER BY DataHora, Controle ',
  '']);
  //
  if CkProcesso.Checked then
    Report := frxWET_CURTI_248_2
  else
    Report := frxWET_CURTI_248_1;
  //
  MyObjects.frxDefineDataSets(Report, [
    DModG.frxDsDono,
    frxDsVSAnt,
    frxDsVSAtu
    ]);
  MyObjects.frxMostra(Report, 'Ficha Kardex');
end;

procedure TFmVSImpSdoCorret.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSImpSdoCorret.DefineSQLMovimID(const Obrigatorio: Boolean;
  const EdMovimID: TdmkEdit; var SQL: String): Boolean;
var
  MovimID: Integer;
begin
  Result := False;
  SQL := '';
  MovimID := EdMovimID.ValueVariant;
  if MyObjects.FIC((MovimID < 0) and Obrigatorio, EdMovimID,
  'Informe o ID do movimento!') then
    Exit;
  if MovimID > -1 then
    SQL := 'AND vmi.MovimID=' + Geral.FF0(MovimID);
  Result := True;
end;

procedure TFmVSImpSdoCorret.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpSdoCorret.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TP02DataIni.Date := Trunc(DModG.ObtemAgora()) - 30;
  TP02DataFim.Date := DModG.ObtemAgora();

  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  //
end;

procedure TFmVSImpSdoCorret.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpSdoCorret.frxWET_CURTI_248_A_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_ANT' then
    Value := Geral.FDT(FDtaAnt_Dta + (1439/1440), 107)
  else
  if VarName ='VARF_GGX_GGY' then
  begin
    case PCTipoRel.ActivePageIndex of
      0: Value := 'Arigo: ' + Geral.FF0(EdGraGruX.ValueVariant) + ' - ' + CBGraGruX.Text;
      1: Value := 'Grupo de estoque: ' + Geral.FF0(EdGraGruY.ValueVariant) + ' - ' + CBGraGruY.Text;
    end;
  end;
end;

procedure TFmVSImpSdoCorret.PesquisaLancamentos();
  function SQL_LJ_GGX: String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;

var
  SQL_Periodo, SQL_GGX_GGY, ATT_MovimID, SQL_MovimID: String;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  procedure GeraVSKardex();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Artigo, SQL: String;
  begin
    case FGraGruY of
      2048: SQL_Artigo :=
            Geral.ATS(['AND ( ',
                       '  NOT (MovimID=6 AND MovimNiv=14) ',
                       ')   ']);
      else SQL_Artigo := '';
    end;
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    'IF(ValorT>0.00, ValorT, IF(ValorMP <> 0, ValorMP, ValorT - CustoPQ)) CustoMP,',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    'IF(Pecas>0, AreaM2/Pecas, 0) MediaArM2, ',
    'IF(Pecas>0, PesoKg/Pecas, 0) MediaPeso, ',
    '0.00 AcumPeca, 0.00 AcumPeso, 0.00 AcumArM2, ',
    '0.00 AcumArP2, 0.00 AcumValorT, 0.00 AcumCusPc, ',
    '0.00 AcumCusKg, 0.00 AcumCusM2, 0.00 AcumCusP2 ',
    '']);
    SQL_Left := Geral.ATS([
    SQL_LJ_GGX(),
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vmi.Terceiro',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.MovimID IN (9, 17, 41) ',
    'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
    // Fazer tudo!! Separar depois!
    //SQL_Periodo,
    SQL_GGX_GGY,
    SQL_Artigo,
    '']);
    SQL_Group := '';
    //UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    SQL := Geral.ATS([
    'INSERT INTO ' + FVSCardxSdoMov,
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY NO_Pallet, Controle ',
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    //
    //Geral.MB_Teste(SQL);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  case PCTipoRel.ActivePageIndex of
    // GrasGruX
    0:
    begin
      if FGraGruX = 0 then
        SQL_GGX_GGY := ''
      else
        SQL_GGX_GGY := 'AND vmi.GraGruX=' + Geral.FF0(FGraGruX);
    end;
    // GrasGruY
    1:
    begin
      if FGraGruY = 0 then
        SQL_GGX_GGY := ''
      else
        SQL_GGX_GGY := 'AND ggx.GraGruY=' + Geral.FF0(FGraGruY);
    end;
  end;
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora', 0,
      TP02DataFim.Date, True, Ck02DataFim.Checked);

  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, FEmpresa);
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  GeraVSKardex();
  //
end;

end.
