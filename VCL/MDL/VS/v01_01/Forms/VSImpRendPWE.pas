unit VSImpRendPWE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEdit, mySQLDbTables, AppListas, dmkDBGridZTO, frxClass,
  frxDBSet, UnProjGroup_Consts, UnAppEnums;

type
  TRelatorio = (relNone=0, relPallet=1, relIMEI=2);
  TFmVSImpRendPWE = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrRend: TmySQLQuery;
    QrIndef: TmySQLQuery;
    QrIndefGraGruX: TIntegerField;
    QrIndefCouNiv1: TIntegerField;
    QrIndefCouNiv2: TIntegerField;
    QrIndefArtigoImp: TWideStringField;
    QrIndefClasseImp: TWideStringField;
    QrIndefPrevPcPal: TIntegerField;
    QrIndefMediaMinM2: TFloatField;
    QrIndefMediaMaxM2: TFloatField;
    QrIndefLk: TIntegerField;
    QrIndefDataCad: TDateField;
    QrIndefDataAlt: TDateField;
    QrIndefUserCad: TIntegerField;
    QrIndefUserAlt: TIntegerField;
    QrIndefAlterWeb: TSmallintField;
    QrIndefAtivo: TSmallintField;
    QrIndefMediaMinKg: TFloatField;
    QrIndefMediaMaxKg: TFloatField;
    QrIndefPrevAMPal: TFloatField;
    QrIndefPrevKgPal: TFloatField;
    QrIndefGrandeza: TSmallintField;
    QrIndefBastidao: TSmallintField;
    QrIndefNO_CouNiv1: TWideStringField;
    QrIndefNO_CouNiv2: TWideStringField;
    QrIndefGraGruY: TIntegerField;
    QrIndefNO_GGY: TWideStringField;
    QrIndefNO_PRD_TAM_COR: TWideStringField;
    DBGIndef: TdmkDBGridZTO;
    DsIndef: TDataSource;
    frxWET_CURTI_153_A: TfrxReport;
    frxDsRend: TfrxDBDataset;
    QrRendBastidao: TSmallintField;
    QrRendNO_FORNECE: TWideStringField;
    QrRendGGX20: TIntegerField;
    QrRendNO_PRD_TAM_COR_22: TWideStringField;
    QrRendNO_PRD_TAM_COR_20: TWideStringField;
    QrRendRendimento: TFloatField;
    QrRendNFeRem: TIntegerField;
    QrRendCodigo: TIntegerField;
    QrRendMovimCod: TIntegerField;
    QrRendCtrl22: TIntegerField;
    QrRendMovimTwn: TIntegerField;
    QrRendEmpresa: TIntegerField;
    QrRendTerceiro: TIntegerField;
    QrRendMovimID: TIntegerField;
    QrRendDataHora: TDateTimeField;
    QrRendPallet: TIntegerField;
    QrRendGGX22: TIntegerField;
    QrRendPecas_22: TFloatField;
    QrRendAreaM2_22: TFloatField;
    QrRendCtrl23: TIntegerField;
    QrRendGGX23: TIntegerField;
    QrRendPecas_23: TFloatField;
    QrRendAreaM2_23: TFloatField;
    QrRendDATA: TDateField;
    QrRendNO_Bastidao: TWideStringField;
    QrRendCtrl20: TIntegerField;
    QrZ: TmySQLQuery;
    QrZControle: TIntegerField;
    QrZCodigo: TIntegerField;
    QrZMovimCod: TIntegerField;
    QrZMovimNiv: TIntegerField;
    QrZMovimTwn: TIntegerField;
    QrZEmpresa: TIntegerField;
    QrZTerceiro: TIntegerField;
    QrZCliVenda: TIntegerField;
    QrZMovimID: TIntegerField;
    QrZDataHora: TDateTimeField;
    QrZPallet: TIntegerField;
    QrZGraGruX: TIntegerField;
    QrZPecas: TFloatField;
    QrZPesoKg: TFloatField;
    QrZAreaM2: TFloatField;
    QrZAreaP2: TFloatField;
    QrZValorT: TFloatField;
    QrZSrcMovID: TIntegerField;
    QrZSrcNivel1: TIntegerField;
    QrZSrcNivel2: TIntegerField;
    QrZSrcGGX: TIntegerField;
    QrZSdoVrtPeca: TFloatField;
    QrZSdoVrtPeso: TFloatField;
    QrZSdoVrtArM2: TFloatField;
    QrZObserv: TWideStringField;
    QrZSerieFch: TIntegerField;
    QrZFicha: TIntegerField;
    QrZMisturou: TSmallintField;
    QrZFornecMO: TIntegerField;
    QrZDstMovID: TIntegerField;
    QrZDstNivel1: TIntegerField;
    QrZDstNivel2: TIntegerField;
    QrZDstGGX: TIntegerField;
    QrZQtdGerPeca: TFloatField;
    QrZQtdGerPeso: TFloatField;
    QrZQtdGerArM2: TFloatField;
    QrZQtdGerArP2: TFloatField;
    QrZQtdAntPeca: TFloatField;
    QrZQtdAntPeso: TFloatField;
    QrZQtdAntArM2: TFloatField;
    QrZQtdAntArP2: TFloatField;
    QrZNotaMPAG: TFloatField;
    QrZMarca: TWideStringField;
    QrZReqMovEstq: TIntegerField;
    QrZStqCenLoc: TIntegerField;
    QrZItemNFe: TIntegerField;
    QrZVSMulFrnCab: TIntegerField;
    QrZClientMO: TIntegerField;
    QrZNFeSer: TSmallintField;
    QrZNFeNum: TIntegerField;
    QrZVSMulNFeCab: TIntegerField;
    QrIncl: TmySQLQuery;
    Qr_: TmySQLQuery;
    Qr_IMEIIni: TIntegerField;
    Qr_Level1: TIntegerField;
    Qr_Level2: TIntegerField;
    Qr_Level3: TIntegerField;
    Qr_Controle: TIntegerField;
    Qr_Codigo: TIntegerField;
    Qr_MovimCod: TIntegerField;
    Qr_MovimNiv: TIntegerField;
    Qr_MovimTwn: TIntegerField;
    Qr_Empresa: TIntegerField;
    Qr_Terceiro: TIntegerField;
    Qr_CliVenda: TIntegerField;
    Qr_MovimID: TIntegerField;
    Qr_DataHora: TDateTimeField;
    Qr_Pallet: TIntegerField;
    Qr_GraGruX: TIntegerField;
    Qr_Pecas: TFloatField;
    Qr_PesoKg: TFloatField;
    Qr_AreaM2: TFloatField;
    Qr_AreaP2: TFloatField;
    Qr_ValorT: TFloatField;
    Qr_SrcMovID: TIntegerField;
    Qr_SrcNivel1: TIntegerField;
    Qr_SrcNivel2: TIntegerField;
    Qr_SrcGGX: TIntegerField;
    Qr_SdoVrtPeca: TFloatField;
    Qr_SdoVrtPeso: TFloatField;
    Qr_SdoVrtArM2: TFloatField;
    Qr_Observ: TWideStringField;
    Qr_SerieFch: TIntegerField;
    Qr_Ficha: TIntegerField;
    Qr_Misturou: TSmallintField;
    Qr_FornecMO: TIntegerField;
    Qr_DstMovID: TIntegerField;
    Qr_DstNivel1: TIntegerField;
    Qr_DstNivel2: TIntegerField;
    Qr_DstGGX: TIntegerField;
    Qr_QtdGerPeca: TFloatField;
    Qr_QtdGerPeso: TFloatField;
    Qr_QtdGerArM2: TFloatField;
    Qr_QtdGerArP2: TFloatField;
    Qr_QtdAntPeca: TFloatField;
    Qr_QtdAntPeso: TFloatField;
    Qr_QtdAntArM2: TFloatField;
    Qr_QtdAntArP2: TFloatField;
    Qr_NotaMPAG: TFloatField;
    Qr_Marca: TWideStringField;
    Qr_ReqMovEstq: TIntegerField;
    Qr_StqCenLoc: TIntegerField;
    Qr_ItemNFe: TIntegerField;
    Qr_VSMulFrnCab: TIntegerField;
    Qr_ClientMO: TIntegerField;
    Qr_NFeSer: TSmallintField;
    Qr_NFeNum: TIntegerField;
    Qr_VSMulNFeCab: TIntegerField;
    Qr_PrcPeca: TIntegerField;
    Qr_PrcPeso: TIntegerField;
    Qr_PrcArM2: TIntegerField;
    Qr_PrcKnd: TIntegerField;
    Qr_PrcRend: TIntegerField;
    QrLevel2: TmySQLQuery;
    QrLevel2Level1: TIntegerField;
    QrLevel2ITENS: TLargeintField;
    QrLevel2Pecas: TFloatField;
    QrLevel2AreaM2: TFloatField;
    QrLevel2PesoKg: TFloatField;
    frxDsLevel2: TfrxDBDataset;
    QrLevel2NO_Level2: TWideStringField;
    QrLevel2Level2: TIntegerField;
    QrPallets: TmySQLQuery;
    frxDsPallets: TfrxDBDataset;
    frxWET_CURTI_153_B: TfrxReport;
    QrImeis: TmySQLQuery;
    QrImeisIMEIIni: TIntegerField;
    QrImeisSeq: TIntegerField;
    QrImeisLevel1: TIntegerField;
    QrImeisLevel2: TIntegerField;
    QrImeisLevel3: TIntegerField;
    QrImeisControle: TIntegerField;
    QrImeisCodigo: TIntegerField;
    QrImeisMovimCod: TIntegerField;
    QrImeisMovimNiv: TIntegerField;
    QrImeisMovimTwn: TIntegerField;
    QrImeisEmpresa: TIntegerField;
    QrImeisTerceiro: TIntegerField;
    QrImeisCliVenda: TIntegerField;
    QrImeisMovimID: TIntegerField;
    QrImeisDataHora: TDateTimeField;
    QrImeisPallet: TIntegerField;
    QrImeisGraGruX: TIntegerField;
    QrImeisPecas: TFloatField;
    QrImeisPesoKg: TFloatField;
    QrImeisAreaM2: TFloatField;
    QrImeisAreaP2: TFloatField;
    QrImeisValorT: TFloatField;
    QrImeisSrcMovID: TIntegerField;
    QrImeisSrcNivel1: TIntegerField;
    QrImeisSrcNivel2: TIntegerField;
    QrImeisSrcGGX: TIntegerField;
    QrImeisSdoVrtPeca: TFloatField;
    QrImeisSdoVrtPeso: TFloatField;
    QrImeisSdoVrtArM2: TFloatField;
    QrImeisObserv: TWideStringField;
    QrImeisSerieFch: TIntegerField;
    QrImeisFicha: TIntegerField;
    QrImeisMisturou: TSmallintField;
    QrImeisFornecMO: TIntegerField;
    QrImeisDstMovID: TIntegerField;
    QrImeisDstNivel1: TIntegerField;
    QrImeisDstNivel2: TIntegerField;
    QrImeisDstGGX: TIntegerField;
    QrImeisQtdGerPeca: TFloatField;
    QrImeisQtdGerPeso: TFloatField;
    QrImeisQtdGerArM2: TFloatField;
    QrImeisQtdGerArP2: TFloatField;
    QrImeisQtdAntPeca: TFloatField;
    QrImeisQtdAntPeso: TFloatField;
    QrImeisQtdAntArM2: TFloatField;
    QrImeisQtdAntArP2: TFloatField;
    QrImeisNotaMPAG: TFloatField;
    QrImeisMarca: TWideStringField;
    QrImeisReqMovEstq: TIntegerField;
    QrImeisStqCenLoc: TIntegerField;
    QrImeisItemNFe: TIntegerField;
    QrImeisVSMulFrnCab: TIntegerField;
    QrImeisClientMO: TIntegerField;
    QrImeisNFeSer: TSmallintField;
    QrImeisNFeNum: TIntegerField;
    QrImeisVSMulNFeCab: TIntegerField;
    QrImeisPrcMovID: TIntegerField;
    QrImeisPrcMovNiv: TIntegerField;
    QrImeisPrcNivel1: TIntegerField;
    QrImeisPrcNivel2: TIntegerField;
    QrImeisPrcPeca: TFloatField;
    QrImeisPrcPeso: TFloatField;
    QrImeisPrcArM2: TFloatField;
    QrImeisPrcKnd: TIntegerField;
    QrImeisPrcRend: TFloatField;
    QrImeisLvl1Ant: TIntegerField;
    QrImeisAtivo: TIntegerField;
    QrImeisNO_MovimNiv: TWideStringField;
    QrImeisNO_LOC_CEN: TWideStringField;
    QrImeisNO_PRD_TAM_COR: TWideStringField;
    frxDsImeis: TfrxDBDataset;
    frxWET_CURTI_153_C: TfrxReport;
    QrPalletsPallet: TIntegerField;
    QrPalletsGraGruX: TIntegerField;
    QrPalletsNFeSer: TSmallintField;
    QrPalletsNFeNum: TIntegerField;
    QrPalletsPrcMovID: TIntegerField;
    QrPalletsPrcMovNiv: TIntegerField;
    QrPalletsPrcNivel1: TIntegerField;
    QrPalletsPrcNivel2: TIntegerField;
    QrPalletsPrcKnd: TIntegerField;
    QrPalletsPrcRend: TFloatField;
    QrPalletsLvl1Ant: TIntegerField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsPrcPeca: TFloatField;
    QrPalletsPrcPeso: TFloatField;
    QrPalletsPrcArM2: TFloatField;
    QrPalletsNO_MovimNiv: TWideStringField;
    QrPalletsNO_LOC_CEN: TWideStringField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsMovimNiv: TIntegerField;
    QrPalletsNO_CouNiv2: TWideStringField;
    QrPalletsCouNiv2: TIntegerField;
    QrRnd2: TmySQLQuery;
    QrRnd2IMEIIni: TIntegerField;
    QrRnd2Seq: TIntegerField;
    QrRnd2Level1: TIntegerField;
    QrRnd2Level2: TIntegerField;
    QrRnd2Level3: TIntegerField;
    QrRnd2Controle: TIntegerField;
    QrRnd2Codigo: TIntegerField;
    QrRnd2MovimCod: TIntegerField;
    QrRnd2MovimNiv: TIntegerField;
    QrRnd2MovimTwn: TIntegerField;
    QrRnd2Empresa: TIntegerField;
    QrRnd2Terceiro: TIntegerField;
    QrRnd2CliVenda: TIntegerField;
    QrRnd2MovimID: TIntegerField;
    QrRnd2DataHora: TDateTimeField;
    QrRnd2Pallet: TIntegerField;
    QrRnd2GraGruX: TIntegerField;
    QrRnd2Pecas: TFloatField;
    QrRnd2PesoKg: TFloatField;
    QrRnd2AreaM2: TFloatField;
    QrRnd2AreaP2: TFloatField;
    QrRnd2ValorT: TFloatField;
    QrRnd2SrcMovID: TIntegerField;
    QrRnd2SrcNivel1: TIntegerField;
    QrRnd2SrcNivel2: TIntegerField;
    QrRnd2SrcGGX: TIntegerField;
    QrRnd2SdoVrtPeca: TFloatField;
    QrRnd2SdoVrtPeso: TFloatField;
    QrRnd2SdoVrtArM2: TFloatField;
    QrRnd2Observ: TWideStringField;
    QrRnd2SerieFch: TIntegerField;
    QrRnd2Ficha: TIntegerField;
    QrRnd2Misturou: TSmallintField;
    QrRnd2FornecMO: TIntegerField;
    QrRnd2DstMovID: TIntegerField;
    QrRnd2DstNivel1: TIntegerField;
    QrRnd2DstNivel2: TIntegerField;
    QrRnd2DstGGX: TIntegerField;
    QrRnd2QtdGerPeca: TFloatField;
    QrRnd2QtdGerPeso: TFloatField;
    QrRnd2QtdGerArM2: TFloatField;
    QrRnd2QtdGerArP2: TFloatField;
    QrRnd2QtdAntPeca: TFloatField;
    QrRnd2QtdAntPeso: TFloatField;
    QrRnd2QtdAntArM2: TFloatField;
    QrRnd2QtdAntArP2: TFloatField;
    QrRnd2NotaMPAG: TFloatField;
    QrRnd2Marca: TWideStringField;
    QrRnd2ReqMovEstq: TIntegerField;
    QrRnd2StqCenLoc: TIntegerField;
    QrRnd2ItemNFe: TIntegerField;
    QrRnd2VSMulFrnCab: TIntegerField;
    QrRnd2ClientMO: TIntegerField;
    QrRnd2NFeSer: TSmallintField;
    QrRnd2NFeNum: TIntegerField;
    QrRnd2VSMulNFeCab: TIntegerField;
    QrRnd2PrcMovID: TIntegerField;
    QrRnd2PrcMovNiv: TIntegerField;
    QrRnd2PrcNivel1: TIntegerField;
    QrRnd2PrcNivel2: TIntegerField;
    QrRnd2PrcPeca: TFloatField;
    QrRnd2PrcPeso: TFloatField;
    QrRnd2PrcArM2: TFloatField;
    QrRnd2PrcKnd: TIntegerField;
    QrRnd2PrcRend: TFloatField;
    QrRnd2Lvl1Ant: TIntegerField;
    QrRnd2Ativo: TIntegerField;
    QrRnd2NO_PRD_TAM_COR: TWideStringField;
    frxDsRnd2: TfrxDBDataset;
    QrRnd2NO_PrcKnd: TWideStringField;
    QrRnd2NO_GG1_GTI: TWideStringField;
    QrRnd2GG1_Tam: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_153_AGetValue(const VarName: string;
      var Value: Variant);
    function frxWET_CURTI_153_AUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure QrLevel2BeforeClose(DataSet: TDataSet);
    procedure QrLevel2AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FVSImpRendIMEI, FSQLNFe: String;
    FUsaNFeSer, FUsaNFeNum: Boolean;
    FTitulo: String;
    FRelatorio: TRelatorio;
    FSeq, FNFeSer, FNFeNum: Integer;
    //
    procedure MyFunc(IMEI22: Integer);

    //
    procedure GeraDadosIMEIIni(IMEIIni: Integer; UsaNFeSer, UsaNFeNum: Boolean;
              NFeSer, NFeNum: Integer);
    procedure GeraDadosArrIMEIs(IMEIsIni: array of Integer; UsaNfeSers,
              UsaNFeNums: array of Boolean; NFeSers, NFeNums: array of Integer);
    procedure CriaDadosIMEIsIni(IMEIIni: Integer);
    //
    function  InsereImeiAtual(const Qry: TmySQLQuery; const IMEIPai, Level1Ant:
              Integer; var NewLevel1: Integer): Boolean;
    procedure PreparaEInsereAtualA(Qry: TmySQLQuery; Level1Ant: Integer;
              ProcFuncOri: String);
    //
    procedure InsereID06Niv13DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID06Niv13_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID06Niv15DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID11Niv07DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID11Niv07_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID11Niv09DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID11Niv09_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID14Niv01DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID14Niv02DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID14Niv02_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID15Niv11DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID15Niv11_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID15Niv12DeMCod(      MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID15Niv12_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID19Niv20DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID19Niv21DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID19Niv20_Itens(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID20Niv22DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID20Niv22_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID24Niv01DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID24Niv02_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID25Niv28DeMCod(IMEI, MovimCod, Level1Ant: Integer; ProcFuncOri: String);
    procedure InsereID25Niv28_Itens(IMEI, Level1Ant: Integer; ProcFuncOri: String);
    //
    procedure ReopenImeis();
    procedure ReopenItens();
    procedure ReopenPallets();
    procedure ReopenRnd2();

    procedure ReopenVSImpRendImei();
  public
    { Public declarations }
    FEmpresa_Cod, FFornece_Cod, FOPIni, FOPFim, FAgrupa, FOrdem1, FOrdem2,
    FOrdem3, FOrdem4, FBastidoes: Integer;
    FEmpresa_Txt, FFornece_Txt: String;
    FDataIni, FDataFim: TDateTime;
    FUsaDtIni, FUsaDtFim, FUsaOPIni, FUsaOPFim: Boolean;
    FDBG21CouNiv2: TdmkDBGridZTO;
    FQr21CouNiv2: TmySQLQuery;
    //
    FIMEIsIni: array of Integer;
    //
    procedure ImprimeRendIMEIs(IMEIs: array of Integer; UsaNfeSers, UsaNFeNums:
              array of Boolean; NFeSers, NFeNums: array of Integer);

  end;

  var
  FmVSImpRendPWE: TFmVSImpRendPWE;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UMySQLModule, UnVS_PF,
  CreateVS;

var
  VAR_TESTE: Integer;

{$R *.DFM}

procedure TFmVSImpRendPWE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpRendPWE.CriaDadosIMEIsIni(IMEIIni: Integer);
begin
end;

procedure TFmVSImpRendPWE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpRendPWE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  frxWET_CURTI_153_A.AddFunction(
    'procedure MyFunc(IMEI22: Integer)');
  frxWET_CURTI_153_B.AddFunction(
    'procedure MyFunc(IMEI22: Integer)');
  frxWET_CURTI_153_C.AddFunction(
    'procedure MyFunc(IMEI22: Integer)');
  //
end;

procedure TFmVSImpRendPWE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpRendPWE.frxWET_CURTI_153_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_TITULO' then
    Value := FTitulo
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
end;

function TFmVSImpRendPWE.frxWET_CURTI_153_AUserFunction(
  const MethodName: string; var Params: Variant): Variant;
begin
  if MethodName = 'MYFUNC' then
    MyFunc(Params[0]);
end;

procedure TFmVSImpRendPWE.GeraDadosArrIMEIs(IMEIsIni: array of Integer;
UsaNfeSers, UsaNFeNums: array of Boolean; NFeSers, NFeNums: array of Integer);
var
  I: Integer;
begin
  for I := Low(IMEISIni) to High(IMEIsIni) do
  begin
    GeraDadosIMEIIni(
      IMEIsIni[I], UsaNfeSers[I], UsaNFeNums[I], NFeSers[I], NFeNums[I]);
  end;
end;

procedure TFmVSImpRendPWE.GeraDadosIMEIIni(IMEIIni: Integer; UsaNFeSer,
  UsaNFeNum: Boolean; NFeSer, NFeNum: Integer);
const
  Level1Ant = 0;
  sProcName = 'FmVSImpRendPWE.GeraDadosIMEIIni';
var
  (*Level1, Level2, Level3: *)
  Level1New: Integer;
  PrcPeca, PrcPeso, PrcArM2, PrcRend: Double;
  PrcKnd, PrcMovID, PrcMovNiv, PrcNivel1, PrcNivel2: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
(*
    Level1    := 0;
    Level2    := 0;
    Level3    := 0;
    PrcPeca   := 0;
    PrcPeso   := 0;
    PrcArM2   := 0;
    PrcKnd    := 0;
    PrcRend   := 0;
    PrcMovID  := 0;
    PrcMovNiv := 0;
    PrcNivel1 := 0;
    PrcNivel2 := 0;
*)
    //
    FUsaNFeSer := UsaNFeSer;
    FUsaNFeNum := UsaNFeNum;
    FNFeSer    := NFeSer;
    FNFeNum    := NFeNum;
    FSQLNFe := '';
    if FUsaNFeSer then
      FSQLNFe := FSQLNFe + 'AND vmi.NFeSer=' + Geral.FF0(FNFeSer) + sLineBreak;
    if FUsaNFeNum then
      FSQLNFe := FSQLNFe + 'AND vmi.NFeNum=' + Geral.FF0(FNFeNum) + sLineBreak;
    //
    FVSImpRendIMEI :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSImpRendIMEI, DModG.QrUpdPID1, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.Controle=' + Geral.FF0(IMEIIni),
    '']);
    Level1New := 0;
    if InsereImeiAtual(Qry, IMEIIni, Level1Ant, Level1New) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT vmi.*  ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi',
      'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEIIni),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        PreparaEInsereAtualA(Qry, Level1New, sProcName);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.ImprimeRendIMEIs(IMEIs: array of Integer; UsaNfeSers,
  UsaNFeNums: array of Boolean; NFeSers, NFeNums: array of Integer);
begin
  FSeq := 0;
  //
  FRelatorio := TRelatorio(MyObjects.SelRadioGroup('Tipo de Relat�rio',
  'Escolha o tipo de relat�rio', [
  'Itens por Pallet',
  'Itens por IMEI'],
  -1) + 1);
  //
  GeraDadosArrIMEIs(IMEIS, UsaNfeSers, UsaNFeNums, NFeSers, NFeNums);
  //
  ReopenVSImpRendImei();
  ReopenRnd2();
  //
  FTitulo := 'Rendimento de Semi Acabado - NFe ?';
  InputQuery('T�tulo do Relat�rio', 'Informe o t�tulo:', FTitulo);
  case FRelatorio of
    relPallet:
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_153_B, [
        DModG.frxDsDono,
        frxDsLevel2,
        frxDsPallets,
        frxDsRnd2
      ]);
      MyObjects.frxMostra(frxWET_CURTI_153_B, FTitulo);
    end;
    relIMEI:
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_153_C, [
        DModG.frxDsDono,
        frxDsLevel2,
        frxDsImeis
      ]);
      MyObjects.frxMostra(frxWET_CURTI_153_C, FTitulo);
    end;
  end;
end;

procedure TFmVSImpRendPWE.InsereID06Niv13DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID06Niv13DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)), // 13
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID06Niv13_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID06Niv13_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID06Niv13_Itens';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, MovimID, MovimNiv, IDNiv, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI  := Qry.FieldByName('Controle').AsInteger;
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
        case IDNIv of
          //  Semi acabado?
(*
          02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
          11007: InsereID11Niv07_Itens(NewIMEI, Level1Ant, ProcFuncOri);
*)
          14001: InsereID14Niv01DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
(*
          19020: InsereID19Niv20_Itens(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          *)
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID06Niv15DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID06Niv15DeMCod';
var
  NewIMEI, Level1New: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)), //15
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      NewIMEI := Qry.FieldByName('Controle').AsInteger ;
      //
      if InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New) then
        InsereID06Niv13DeMCod(NewIMEI, MovimCod, Level1New, sProcName);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID11Niv07DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID11Niv07DeMCod';
var
  NewIMEI, Level1New: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)), // 07
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      NewIMEI := Qry.FieldByName('Controle').AsInteger ;
      //
      if InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New) then
        InsereID11Niv09DeMCod(NewIMEI, MovimCod, Level1New, sProcName);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID11Niv07_Itens(IMEI, Level1Ant:
  Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID11Niv07_Itens';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  MovimID, MovimNiv, IDNiv: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
        case IDNIv of
          -1: ;//
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID11Niv09DeMCod(IMEI, MovimCod, Level1Ant:
  Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID11Niv09DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestOper)), // 9
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID11Niv09_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID11Niv09_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID11Niv09_Itens';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, MovimID, MovimNiv, IDNiv, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI  := Qry.FieldByName('Controle').AsInteger;
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
        case IDNIv of
          //  Semi acabado?
          02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
          11007: InsereID11Niv07_Itens(NewIMEI, Level1Ant, ProcFuncOri);
          19020: InsereID19Niv20_Itens(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID14Niv01DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID14Niv01DeMCod';
var
  NewIMEI, Level1New: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)), //01
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      NewIMEI := Qry.FieldByName('Controle').AsInteger ;
      //
      if InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New) then
        InsereID14Niv02DeMCod(NewIMEI, MovimCod, Level1New, sProcName);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID14Niv02DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID14Niv02DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)), // 02
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID14Niv02_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID14Niv02_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID14Niv02_Itens';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, MovimID, MovimNiv, IDNiv, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI  := Qry.FieldByName('Controle').AsInteger;
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
        case IDNIv of
          //  Semi acabado?
(*
          02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
          11007: InsereID11Niv07_Itens(NewIMEI, Level1Ant, ProcFuncOri);
          14001: InsereID14Niv01DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
*)
          19020: InsereID19Niv20_Itens(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
(*
          25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          *)
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID15Niv11DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID15Niv11DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)), // 11
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID15Niv11_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
    InsereID15Niv12DeMCod((*IMEI,*) MovimCod, Level1Ant, ProcFuncOri);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID15Niv11_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.InsereID15Niv11_Itens';
var
  Qry: TmySQLQuery;
  MovimID, MovimNiv, IDNiv, Level1New, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //Geral.MB_SQL(Self, Qry);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID     := Qry.FieldByName('MovimID').AsInteger;
      MovimNiv    := Qry.FieldByName('MovimNiv').AsInteger;
      NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
      IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
      Level1New := 0;
      //
      case IDNIv of
        -1: ;//
        else
        begin
          VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
            sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID15Niv12DeMCod((*IMEI*,*) MovimCod, Level1Ant:
  Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID15Niv12DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestPreReclas)), // 12
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //Geral.MB_SQL(Self, Qry);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      NewIMEI := Qry.FieldByName('Controle').AsInteger;
      //
      if InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New) then
        InsereID15Niv12_Itens(NewIMEI, Level1New, ProcFuncOri);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID15Niv12_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.InsereID15Niv12_Itens';
var
  Qry1: TmySQLQuery;
  NewIMEI, MovimID, MovimNiv, IDNiv, Level1New, NewMovimCod, MovimTwn, IMEINv2:
  Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //Geral.MB_SQL(Self, Qry1);
    //
    Qry1.First;
    while not Qry1.Eof do
    begin
      NewIMEI     := Qry1.FieldByName('Controle').AsInteger;
      MovimID     := Qry1.FieldByName('MovimID').AsInteger;
      MovimNiv    := Qry1.FieldByName('MovimNiv').AsInteger;
      NewMovimCod := Qry1.FieldByName('MovimCod').AsInteger;
      MovimTwn    := Qry1.FieldByName('MovimTwn').AsInteger;
      IDNiv       := VS_PF.GetIDNiv(MovimID, MovimNiv);
      Level1New := 0;
      //
      case IDNIv of
        24001: InsereID24Niv01DeMCod(NewIMEI, NewMovimCod, Level1Ant, ProcFuncOri);
        else
        begin
          VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
            sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
        end;
      end;
      //
      Qry1.Next;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID19Niv20DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID19Niv20DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)), // 20
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID19Niv20_Itens(NewIMEI, NewMovimCod, Level1Ant, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
    //
    //InsereID19Niv21DeMCod(IMEI, MovimCod, Level1Ant, ProcFuncOri);
    //InsereID20Niv22DeMCod(IMEI, MovimCod, Level1Ant, ProcFuncOri);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID19Niv20_Itens(IMEI, MovimCod, Level1Ant:
  Integer; ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.InsereID19Niv20_Itens';
var
  Qry: TmySQLQuery;
  MovimID, MovimNiv, IDNiv, Level1New, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    //'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    //'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestOper)), //
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID     := Qry.FieldByName('MovimID').AsInteger;
      MovimNiv    := Qry.FieldByName('MovimNiv').AsInteger;
      NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
      IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
      Level1New := 0;
      //
      case IDNIv of
        -1: ;//
        else
        begin
          VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
            sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
        end;
      end;
      //
      Qry.Next;
    end;
    InsereID19Niv21DeMCod(IMEI, MovimCod, Level1Ant, ProcFuncOri);
    InsereID20Niv22DeMCod(IMEI, MovimCod, Level1Ant, ProcFuncOri);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID19Niv21DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID19Niv21DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)), // 21
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        //Nao tem pos eh o ID20Niv23
        //InsereID19Niv21_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID20Niv22DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID20Niv22DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)), // 22
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID20Niv22_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID20Niv22_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.InsereID20Niv22_Itens';
var
  Qry: TmySQLQuery;
  NewIMEI, MovimID, MovimNiv, IDNiv, Level1New, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      NewIMEI     := Qry.FieldByName('Controle').AsInteger;
      MovimID     := Qry.FieldByName('MovimID').AsInteger;
      MovimNiv    := Qry.FieldByName('MovimNiv').AsInteger;
      MovimCod    := Qry.FieldByName('MovimCod').AsInteger;
      IDNiv       := VS_PF.GetIDNiv(MovimID, MovimNiv);
      Level1New   := 0;
      //
      case IDNIv of
        //     Acabado???
        02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
        25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
        else
        begin
          VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
            sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID24Niv01DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID24Niv01DeMCod';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  MovimID, MovimNiv, IDNiv, NewIMEI: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv IN (' + Geral.FF0(Integer(eminSorcClass)), // 01
    ',' + Geral.FF0(Integer(eminDestClass)) + ')',               // 02
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      NewIMEI   := Qry.FieldByName('Controle').AsInteger;
      MovimID   := Qry.FieldByName('MovimID').AsInteger;
      MovimNiv  := Qry.FieldByName('MovimNiv').AsInteger;
      IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
      if InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New) then
      begin
        case IDNIv of
          24001: ;//  Nada
          24002: InsereID24Niv02_Itens(NewIMEI, Level1New, ProcFuncOri);
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID24Niv02_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.InsereID24Niv02_Itens';
var
  Qry: TmySQLQuery;
  NewIMEI, MovimID, MovimNiv, IDNiv, Level1New, MovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      NewIMEI     := Qry.FieldByName('Controle').AsInteger;
      MovimID     := Qry.FieldByName('MovimID').AsInteger;
      MovimNiv    := Qry.FieldByName('MovimNiv').AsInteger;
      MovimCod    := Qry.FieldByName('MovimCod').AsInteger;
      IDNiv       := VS_PF.GetIDNiv(MovimID, MovimNiv);
      Level1New   := 0;
      //
      case IDNIv of
        //     Wet Blue???
        02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New);
        //11007: InsereID11Niv07_Itens(NewIMEI, Level1Ant, ProcFuncOri);
        11007: InsereID11Niv07DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
        19020: InsereID19Niv20_Itens(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
        25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
        else
        begin
          VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
            sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID25Niv28DeMCod(IMEI, MovimCod,
  Level1Ant: Integer; ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID25Niv28DeMCod(';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  NewIMEI, NewMovimCod: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestLocal)), // 28
    FSQLNFe,
    'ORDER BY DataHora, Controle',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New   := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI     := Qry.FieldByName('Controle').AsInteger;
        NewMovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        InsereID25Niv28_Itens(NewIMEI, Level1New, ProcFuncOri);
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpRendPWE.InsereID25Niv28_Itens(IMEI, Level1Ant: Integer;
  ProcFuncOri: String);
const
  sProcName ='FmVSImpRendPWE.InsereID25Niv28_Itens';
var
  Qry: TmySQLQuery;
  Level1New: Integer;
  MovimID, MovimNiv, MovimCod, IDNiv, NewIMEI: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Level1New := 0;
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        NewIMEI  := Qry.FieldByName('Controle').AsInteger;
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        IDNiv    := VS_PF.GetIDNiv(MovimID, MovimNiv);
        case IDNIv of
          //     WB, Semi ou acabado???
          02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
          11007: InsereID11Niv07DeMCod(IMEI, MovimCod, Level1Ant, ProcFuncOri);
          19020: InsereID19Niv20_Itens(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          25027: InsereID25Niv28DeMCod(NewIMEI, MovimCod, Level1Ant, ProcFuncOri);
          else
          begin
            VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), IMEI,
              sProcName + sLineBreak + 'Origem: ' + ProcFuncOri);
          end;
        end;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

function TFmVSImpRendPWE.InsereImeiAtual(const Qry: TmySQLQuery; const IMEIPai,
  Level1Ant: Integer; var NewLevel1: Integer): Boolean;
const
  L1 = 1;
  L2 = 2;
var
  MovimID, MovimNiv, MovimCod, Level1, Level2, Level3, IMEI: Integer;
  PrcPeca, PrcPeso, PrcArM2, PrcRend, NewArea, NewPeso: Double;
  PrcKnd, PrcMovID, PrcMovNiv,  PrcNivel1, PrcNivel2, IDNiv, MovimTwn: Integer;
begin
  Result    := False;
  IMEI      := Qry.FieldByName('Controle').AsInteger;
  UnDmkDAC_PF.AbreMySQLQuery0(QrIncl, DModG.MyPID_DB, [
  'SELECT IMEIIni ',
  'FROM ' + FVSImpRendIMEI,
  'WHERE IMEIIni=' + Geral.FF0(IMEI),
  '']);
  if QrIncl.RecordCount = 0 then
  begin
    MovimID   := Qry.FieldByName('MovimID').AsInteger;
    MovimNiv  := Qry.FieldByName('MovimNiv').AsInteger;
    MovimCod  := Qry.FieldByName('MovimCod').AsInteger;
    MovimTwn  := Qry.FieldByName('MovimTwn').AsInteger;
    IDNiv     := VS_PF.GetIDNiv(MovimID, MovimNiv);
    //
    Level1    := VS_PF.ObtemMovimSeqDeMovimIDEMovimNiv(L1, MovimID, MovimNiv, MovimCod, IMEI, Level1Ant);
    Level2    := VS_PF.ObtemMovimSeqDeMovimIDEMovimNiv(L2, MovimID, MovimNiv, MovimCod, IMEI, Level1Ant);
    Level3    := 0;
    PrcPeca   := 0;
    PrcPeso   := 0;
    PrcArM2   := 0;
    PrcKnd    := 0;
    PrcRend   := 0;
    PrcMovID  := 0;
    PrcMovNiv := 0;
    PrcNivel1 := 0;
    PrcNivel2 := 0;
    //
    FSeq      := FSeq + 1;
    //
    case IDNiv of
      20022:
      begin

        UnDMkDAC_PF.AbreMySQLQuery0(QrZ, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
        'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndBxa)),
        ' ']);
        //
        PrcPeca   := -QrZPecas.Value;
        PrcPeso   := -QrZPesoKg.Value;
        PrcArM2   := -QrZAreaM2.Value;
        PrcRend   := 0;
        PrcKnd    := Integer(TVSRendTrans.vsrtIndef);
        NewArea   := Qry.FieldByName('AreaM2').AsFloat;
        NewPeso   := Qry.FieldByName('PesoKg').AsFloat;
        //
        if NewArea <> 0 then
        begin
          if PrcArM2 <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtAreaArea);
            PrcRend := (NewArea - PrcArM2) / PrcArM2 * 100;
          end else
          if PrcPeso <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtPesoArea);
            PrcRend := PrcPeso / NewArea;
          end else
          if PrcPeca <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtPecaArea);
            PrcRend := NewArea / PrcPeca;
          end;
        end else
        if NewPeso <> 0 then
        begin
          if PrcPeso <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtPesoPeso);
            PrcRend := (NewPeso - PrcPeso) / PrcPeso * 100;
          end else
          if PrcArM2 <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtAreaPeso);
            PrcRend := PrcArM2 / NewPeso;
          end else
          if PrcPeca <> 0 then
          begin
            PrcKnd  := Integer(TVSRendTrans.vsrtPecaPeso);
            PrcRend := NewPeso / PrcPeca;
          end;
        end;
        PrcMovID  := QrZMovimID.Value;
        PrcMovNiv := QrZMovimNiv.Value;
        PrcNivel1 := QrZCodigo.Value;
        PrcNivel2 := QrZControle.Value;
      end;
    end;
    //
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FVSImpRendIMEI,
    'SELECT ' + Geral.FF0(IMEI) + ' IMEIIni, ' +
    Geral.FF0(FSeq) +' Seq, ' +
    Geral.FF0(Level1) +' Level1, ' + Geral.FF0(Level2) +' Level2, ' +
    Geral.FF0(Level3) +' Level3, ' + 'Controle, Codigo,  ',
    'MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, CliVenda,  ',
    'MovimID, DataHora, Pallet, GraGruX, Pecas, PesoKg, AreaM2,  ',
    'AreaP2,  ValorT, SrcMovID, SrcNivel1, SrcNivel2, SrcGGX,  ',
    'SdoVrtPeca, SdoVrtPeso, SdoVrtArM2, Observ, SerieFch, Ficha,  ',
    'Misturou, FornecMO, DstMovID, DstNivel1, DstNivel2, DstGGX,  ',
    'QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, QtdAntPeca,  ',
    'QtdAntPeso, QtdAntArM2, QtdAntArP2, NotaMPAG, Marca,  ',
    'ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,  ',
    'NFeSer, NFeNum, VSMulNFeCab, ',
    Geral.FF0(PrcMovID) + ' PrcMovID, ' +
    Geral.FF0(PrcMovID) + ' PrcMovNiv, ' +
    Geral.FF0(PrcNivel1) + ' PrcNivel1, ' +
    Geral.FF0(PrcNivel1) + ' PrcNivel2, ',
    Geral.FFT_Dot(PrcPeca, 3, siNegativo) + ' PrcPeca, ' +
    Geral.FFT_Dot(PrcPeso, 3, siNegativo) + ' PrcPeso, ' +
    Geral.FFT_Dot(PrcArM2, 3, siNegativo) + ' PrcArM2, ' +
    Geral.FF0(PrcKnd) + ' PrcKnd, ' +
    Geral.FFT_Dot(PrcRend, 6, siNegativo) + ' PrcRend, ',
    Geral.FF0(Level1Ant) +' Lvl1Ant, 1 Ativo ' +

    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    //
    if Result then
      NewLevel1 := Level1;
  end;
end;

procedure TFmVSImpRendPWE.PreparaEInsereAtualA(Qry: TmySQLQuery; Level1Ant:
  Integer; ProcFuncOri: String);
const
  sProcName = 'FmVSImpRendPWE.PreparaEInsereAtualA';
var
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  //Level1, Level2, Level3
  IDNiv, NewIMEI, MovimCod, Level1New: Integer;
begin
  MovimID   := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
  MovimNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
  NewIMEI   := Qry.FieldByName('Controle').AsInteger;
  MovimCod  := Qry.FieldByName('MovimCod').AsInteger;
  IDNiv     := VS_PF.GetIDNiv(Integer(MovimID), Integer(MovimNiv));
  Level1New := 0;
  //
  case IDNiv of
    02000: InsereImeiAtual(Qry, NewIMEI, Level1Ant, Level1New); // Nada. Venda.
    // Baixa de Geracao de artigo
    06015: InsereID06Niv15DeMCod(NewIMEI, MovimCod, Level1Ant, sprocName);
    //emidEmOperacao: 11
    //eminSorcOper: 07
    11007: InsereID11Niv07DeMCod(NewIMEI, MovimCod, Level1Ant, sprocName);
(*
    begin
      if InsereImeiAtual(Qry, IMEI, Level1Ant, Level1New) then
      begin
        InsereID11Niv09DeMCod(IMEI, MovimCod, Level1New, sProcName);
      end;
    end;
*)
    15011: InsereID15Niv11DeMCod(NewIMEI, MovimCod, Level1Ant, sProcName);
    19020: InsereID19Niv20DeMCod(NewIMEI, MovimCod, Level1Ant, sProcName);
    else
    VS_PF.AvisoIDNiv(TEstqMovimID(MovimID), TEstqMovimNiv(MovimNiv), NewIMEI,
      sPROCNAME + sLineBreak + 'Origem: ' + ProcFuncOri);
  end;
end;

procedure TFmVSImpRendPWE.QrLevel2AfterScroll(DataSet: TDataSet);
begin
  ReopenItens();
end;

procedure TFmVSImpRendPWE.QrLevel2BeforeClose(DataSet: TDataSet);
begin
  QrImeis.Close;
end;

procedure TFmVSImpRendPWE.ReopenImeis();
var
  ATT_MovimNiv: String;
begin
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrImeis, DModG.MyPID_DB, [
  'SELECT iri.*, ',
  ATT_MovimNiv,
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM ' + FVSImpRendIMEI + ' iri ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=iri.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=iri.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE iri.Level1=' + Geral.FF0(QrLevel2Level1.Value),
  'ORDER BY iri.MovimNiv, iri.DataHora, iri.Controle ',
  '']);
end;

procedure TFmVSImpRendPWE.ReopenItens;
begin
  QrPallets.Close;
  QrImeis.Close;
  case FRelatorio of
    relPallet: ReopenPallets();
    relIMEI: ReopenImeis();
  end;
end;

procedure TFmVSImpRendPWE.ReopenPallets();
var
  ATT_MovimNiv: String;
begin
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, DModG.MyPID_DB, [
  'SELECT iri.MovimNiv, iri.Pallet, iri.GraGruX, NFeSer, NFeNum, ',
  'PrcMovID, PrcMovNiv, PrcNivel1, PrcNivel2, PrcKnd, PrcRend, Lvl1Ant,',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
  'SUM(PrcPeca) PrcPeca, SUM(PrcPeso) PrcPeso, SUM(PrcArM2) PrcArM2, ',
  ATT_MovimNiv,
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, xco.CouNiv2, cn2.Nome NO_CouNiv2 ',
  'FROM ' + FVSImpRendIMEI + ' iri ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=iri.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=iri.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'WHERE iri.Level1=' + Geral.FF0(QrLevel2Level1.Value),
  'GROUP BY iri.MovimNiv, xco.CouNiv2, iri.Pallet, iri.GraGruX',
  'ORDER BY MovimNiv, CouNiv2, Pallet, GraGruX',
  '']);
end;

procedure TFmVSImpRendPWE.ReopenRnd2();
var
  ATT_PrcRend: String;
begin
  ATT_PrcRend := dmkPF.ArrayToTexto('iri.PrcKnd', 'NO_PrcKnd', pvPos, True,
    sVS_Rend);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRnd2, DModG.MyPID_DB, [
  'SELECT CONCAT(gg1.Nome, " ", gti.Nome) NO_GG1_GTI, ',
  'CONCAT(gg1.Nivel1, ".", gti.Controle) GG1_Tam, ',
  'iri.*, ' + ATT_PrcRend,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + FVSImpRendIMEI + ' iri ',
  'LEFT JOIN ' + TMeuDB + '.gragrux   ggx ON ggx.Controle=iri.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE iri.PrcKnd > 0 ',
  //'ORDER BY iri.MovimNiv, xco.CouNiv2, iri.DataHora, iri.Controle ',
  'ORDER BY iri.PrcKnd, GG1_Tam, NO_GG1_GTI, iri.DataHora ',
  '']);
  //
  //Geral.MB_SQL(Self, QrRnd2);
end;

procedure TFmVSImpRendPWE.ReopenVSImpRendImei();
var
  ATT_Level1: String;
begin
  ATT_Level1 := dmkPF.ELT_FIND_IN_SET('Level2', 'NO_Level2', pvPos, True,
  iVS_SMIS, sVS_SMIS);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLevel2, DModG.MyPID_DB, [
  'SELECT Level1, Level2, ',
  ATT_Level1,
  'COUNT(Controle) ITENS, SUM(Pecas) Pecas,  ',
  'SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg ',
  'FROM ' + FVSImpRendIMEI,
  'GROUP BY Level1 ',
  'ORDER BY Level1 ',
  '']);
  //Geral.MB_SQL(Self, QrLevel2);
end;

procedure TFmVSImpRendPWE.MyFunc(IMEI22: Integer);
begin
    VS_PF.MostraFormVS_Do_IMEI(IMEI22);
end;

end.
