unit VSFormulasImp_BH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs, ZCF2,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, Grids, DBGrids, AppListas,
  mySQLDbTables, ComCtrls, Menus, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkImage, MyListas, UnDmkProcFunc, dmkEditDateTimePicker,
  UnDmkEnums, frxClass, frxDBSet, dmkCheckBox, dmkEditCalc, UnProjGroup_Consts,
  UnPQ_PF, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSFormulasImp_BH = class(TForm)
    QrDefPecas: TmySQLQuery;
    DsDefPecas: TDataSource;
    DsFormulas: TDataSource;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrDefPecasLk: TIntegerField;
    Panel2: TPanel;
    QrFormulas: TmySQLQuery;
    QrFormulasNumero: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasClienteI: TIntegerField;
    QrFormulasTipificacao: TIntegerField;
    QrFormulasDataI: TDateField;
    QrFormulasDataA: TDateField;
    QrFormulasTecnico: TWideStringField;
    QrFormulasTempoR: TIntegerField;
    QrFormulasTempoP: TIntegerField;
    QrFormulasTempoT: TIntegerField;
    QrFormulasHorasR: TIntegerField;
    QrFormulasHorasP: TIntegerField;
    QrFormulasHorasT: TIntegerField;
    QrFormulasHidrica: TIntegerField;
    QrFormulasLinhaTE: TIntegerField;
    QrFormulasCaldeira: TIntegerField;
    QrFormulasSetor: TIntegerField;
    QrFormulasEspessura: TIntegerField;
    QrFormulasPeso: TFloatField;
    QrFormulasQtde: TFloatField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasHHMM_P: TWideStringField;
    QrFormulasHHMM_R: TWideStringField;
    QrFormulasHHMM_T: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    Panel3: TPanel;
    RGImpRecRib: TRadioGroup;
    GBkgTon: TGroupBox;
    RBTon: TRadioButton;
    RBkg: TRadioButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    PainelReceita: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    EdReceita: TdmkEditCB;
    CBReceita: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    PainelEscolhas: TPanel;
    TabSheet3: TTabSheet;
    PainelConfig: TPanel;
    RGTipoPreco: TRadioGroup;
    RGImprime: TRadioGroup;
    QrLotes: TmySQLQuery;
    DsLotes: TDataSource;
    QrFormulasAtivo: TSmallintField;
    QrSoma: TmySQLQuery;
    QrSomaPeso: TFloatField;
    QrSomaPecas: TFloatField;
    PainelEscolhe: TPanel;
    Panel5: TPanel;
    Label6: TLabel;
    DBGrid1: TDBGrid;
    CkMatricial: TCheckBox;
    CkGrade: TCheckBox;
    CkImpVS: TCheckBox;
    Label10: TLabel;
    EdPeso: TdmkEdit;
    Label3: TLabel;
    EdQtde: TdmkEdit;
    EdMedia: TdmkEdit;
    Label12: TLabel;
    Label9: TLabel;
    EdFulao: TdmkEdit;
    TPDataP: TdmkEditDateTimePicker;
    LaData: TLabel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    EdMemo: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtCancela: TBitBtn;
    ProgressBar1: TProgressBar;
    LaSP_A: TLabel;
    LaSP_B: TLabel;
    LaSP_C: TLabel;
    QrFormulasRetrabalho: TSmallintField;
    QrLotesControle: TIntegerField;
    QrLotesVSMovIts: TIntegerField;
    QrLotesPeso: TFloatField;
    QrLotesPecas: TFloatField;
    QrLotesSerieFch: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrLotesMarca: TWideStringField;
    QrLotesNO_SerieFch: TWideStringField;
    QrFormulasBxaEstqVS: TSmallintField;
    QrFormulasGraGruX: TIntegerField;
    DsVMIAtu: TDataSource;
    QrVMIAtu: TmySQLQuery;
    QrVMIAtuCodigo: TLargeintField;
    QrVMIAtuControle: TLargeintField;
    QrVMIAtuMovimCod: TLargeintField;
    QrVMIAtuMovimNiv: TLargeintField;
    QrVMIAtuMovimTwn: TLargeintField;
    QrVMIAtuEmpresa: TLargeintField;
    QrVMIAtuTerceiro: TLargeintField;
    QrVMIAtuCliVenda: TLargeintField;
    QrVMIAtuMovimID: TLargeintField;
    QrVMIAtuDataHora: TDateTimeField;
    QrVMIAtuPallet: TLargeintField;
    QrVMIAtuGraGruX: TLargeintField;
    QrVMIAtuPecas: TFloatField;
    QrVMIAtuPesoKg: TFloatField;
    QrVMIAtuAreaM2: TFloatField;
    QrVMIAtuAreaP2: TFloatField;
    QrVMIAtuValorT: TFloatField;
    QrVMIAtuSrcMovID: TLargeintField;
    QrVMIAtuSrcNivel1: TLargeintField;
    QrVMIAtuSrcNivel2: TLargeintField;
    QrVMIAtuSrcGGX: TLargeintField;
    QrVMIAtuSdoVrtPeca: TFloatField;
    QrVMIAtuSdoVrtPeso: TFloatField;
    QrVMIAtuSdoVrtArM2: TFloatField;
    QrVMIAtuObserv: TWideStringField;
    QrVMIAtuSerieFch: TLargeintField;
    QrVMIAtuFicha: TLargeintField;
    QrVMIAtuMisturou: TLargeintField;
    QrVMIAtuFornecMO: TLargeintField;
    QrVMIAtuCustoMOKg: TFloatField;
    QrVMIAtuCustoMOM2: TFloatField;
    QrVMIAtuCustoMOTot: TFloatField;
    QrVMIAtuValorMP: TFloatField;
    QrVMIAtuDstMovID: TLargeintField;
    QrVMIAtuDstNivel1: TLargeintField;
    QrVMIAtuDstNivel2: TLargeintField;
    QrVMIAtuDstGGX: TLargeintField;
    QrVMIAtuQtdGerPeca: TFloatField;
    QrVMIAtuQtdGerPeso: TFloatField;
    QrVMIAtuQtdGerArM2: TFloatField;
    QrVMIAtuQtdGerArP2: TFloatField;
    QrVMIAtuQtdAntPeca: TFloatField;
    QrVMIAtuQtdAntPeso: TFloatField;
    QrVMIAtuQtdAntArM2: TFloatField;
    QrVMIAtuQtdAntArP2: TFloatField;
    QrVMIAtuNotaMPAG: TFloatField;
    QrVMIAtuNO_PALLET: TWideStringField;
    QrVMIAtuNO_PRD_TAM_COR: TWideStringField;
    QrVMIAtuNO_TTW: TWideStringField;
    QrVMIAtuID_TTW: TLargeintField;
    QrVMIAtuNO_FORNECE: TWideStringField;
    QrVMIAtuReqMovEstq: TLargeintField;
    QrVMIAtuCUSTO_M2: TFloatField;
    QrVMIAtuCUSTO_P2: TFloatField;
    QrVMIAtuNO_LOC_CEN: TWideStringField;
    QrVMIAtuMarca: TWideStringField;
    QrVMIAtuPedItsLib: TLargeintField;
    QrVMIAtuStqCenLoc: TLargeintField;
    QrVMIAtuNO_FICHA: TWideStringField;
    QrVMIAtuNO_FORNEC_MO: TWideStringField;
    QrVMIAtuClientMO: TLargeintField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    CBPeca: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    EdPeca: TdmkEditCB;
    QrVMIOriIMEI: TmySQLQuery;
    QrVMIOriIMEICodigo: TLargeintField;
    QrVMIOriIMEIControle: TLargeintField;
    QrVMIOriIMEIMovimCod: TLargeintField;
    QrVMIOriIMEIMovimNiv: TLargeintField;
    QrVMIOriIMEIMovimTwn: TLargeintField;
    QrVMIOriIMEIEmpresa: TLargeintField;
    QrVMIOriIMEITerceiro: TLargeintField;
    QrVMIOriIMEICliVenda: TLargeintField;
    QrVMIOriIMEIMovimID: TLargeintField;
    QrVMIOriIMEIDataHora: TDateTimeField;
    QrVMIOriIMEIPallet: TLargeintField;
    QrVMIOriIMEIGraGruX: TLargeintField;
    QrVMIOriIMEIPecas: TFloatField;
    QrVMIOriIMEIPesoKg: TFloatField;
    QrVMIOriIMEIAreaM2: TFloatField;
    QrVMIOriIMEIAreaP2: TFloatField;
    QrVMIOriIMEIValorT: TFloatField;
    QrVMIOriIMEISrcMovID: TLargeintField;
    QrVMIOriIMEISrcNivel1: TLargeintField;
    QrVMIOriIMEISrcNivel2: TLargeintField;
    QrVMIOriIMEISrcGGX: TLargeintField;
    QrVMIOriIMEISdoVrtPeca: TFloatField;
    QrVMIOriIMEISdoVrtPeso: TFloatField;
    QrVMIOriIMEISdoVrtArM2: TFloatField;
    QrVMIOriIMEIObserv: TWideStringField;
    QrVMIOriIMEISerieFch: TLargeintField;
    QrVMIOriIMEIFicha: TLargeintField;
    QrVMIOriIMEIMisturou: TLargeintField;
    QrVMIOriIMEIFornecMO: TLargeintField;
    QrVMIOriIMEICustoMOKg: TFloatField;
    QrVMIOriIMEICustoMOM2: TFloatField;
    QrVMIOriIMEICustoMOTot: TFloatField;
    QrVMIOriIMEIValorMP: TFloatField;
    QrVMIOriIMEIDstMovID: TLargeintField;
    QrVMIOriIMEIDstNivel1: TLargeintField;
    QrVMIOriIMEIDstNivel2: TLargeintField;
    QrVMIOriIMEIDstGGX: TLargeintField;
    QrVMIOriIMEIQtdGerPeca: TFloatField;
    QrVMIOriIMEIQtdGerPeso: TFloatField;
    QrVMIOriIMEIQtdGerArM2: TFloatField;
    QrVMIOriIMEIQtdGerArP2: TFloatField;
    QrVMIOriIMEIQtdAntPeca: TFloatField;
    QrVMIOriIMEIQtdAntPeso: TFloatField;
    QrVMIOriIMEIQtdAntArM2: TFloatField;
    QrVMIOriIMEIQtdAntArP2: TFloatField;
    QrVMIOriIMEINotaMPAG: TFloatField;
    QrVMIOriIMEINO_PALLET: TWideStringField;
    QrVMIOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVMIOriIMEINO_TTW: TWideStringField;
    QrVMIOriIMEIID_TTW: TLargeintField;
    QrVMIOriIMEINO_FORNECE: TWideStringField;
    QrVMIOriIMEINO_SerieFch: TWideStringField;
    QrVMIOriIMEIReqMovEstq: TLargeintField;
    DsVMIOriIMEI: TDataSource;
    QrVMIOriIMEIVSMovIts: TLargeintField;
    LaHora: TLabel;
    EdHoraIni: TdmkEdit;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrEmitGru: TMySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    Label14: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrVMIAtuCouNiv1: TIntegerField;
    QrVMIAtuCouNiv2: TIntegerField;
    procedure BtCancelaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdReceitaChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulasCalcFields(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure QrLotesAfterOpen(DataSet: TDataSet);
    procedure QrVMIOriIMEICalcFields(DataSet: TDataSet);
    procedure EdPesoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    { Private declarations }
    FAlturaInicial: Integer;
    //
    procedure InsereEmitCus(GraGruX: Integer);
  public
    { Public declarations }
    FReceitaRibSetor: TReceitaRibSetor;
    FEmit, FMovimCod, FIMEI, FTemIMEIMrt: Integer;
    FNomeForm: String;
    FMovimInn, FMovimSrc: TEstqMovimNiv;
    procedure ReopenLotes();
    procedure ReopenVMIAtu();
    procedure ReopenReceitas();
    procedure AtualizaPesoEPecas();
  end;

var
  FmVSFormulasImp_BH: TFmVSFormulasImp_BH;
  REICalArea : Boolean;
  REISetor : ShortInt;

implementation

uses UnMyObjects, Principal, Formulas, Module, FormulasImpShow, DmkDAC_PF,
  BlueDermConsts, UMySQLModule, ModEmit, FormulasImpShowNew, UnVS_PF,
  ModuleGeral, PQx;

{$R *.DFM}

procedure TFmVSFormulasImp_BH.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSFormulasImp_BH.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FMovimInn := TEstqMovimNiv.eminSemNiv;
  FMovimSrc := TEstqMovimNiv.eminSemNiv;
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  (*if not*) Dmod.CentroDeEstoqueDefinido() (*then Exit*);
  //
  MyObjects.PreencheComponente(RGImpRecRib, sListaRecImpApresenta, 6);
  RGImpRecRib.ItemIndex := Dmod.QrControleImpRecRib.Value;
  //
  //STSP.Caption := '';
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], '', False, taCenter, 2, 10, 20);

  PageControl1.ActivePageIndex := 0;
  Agora := DmodG.ObtemAgora();
  //TPDataP.Date := Agora;
  TPDataP.SetaDataSafe(Agora);
  EdHoraIni.ValueVariant := Agora - Int(Agora);
  FAlturaInicial := Height;
  if VAR_NOMEFORMIMP_ANCESTRAL = 'FmPrincipal' then
  begin
    PageControl2.Visible := IC2_WopcoesImpRecShowLotes;
    EdMemo.Visible := IC2_WopcoesImpRecShowLotes;

    Label6.Visible := IC2_WopcoesImpRecShowMedia;
    EdMedia.Visible := IC2_WopcoesImpRecShowMedia;

{
    Label2.Visible := IC2_WopcoesImpRecShowEspessura;
    CBEspessura.Visible := IC2_WopcoesImpRecShowEspessura;

    Label4.Visible := IC2_WopcoesImpRecShowM2;
    EdAreaM.Visible := IC2_WopcoesImpRecShowM2;

    Label5.Visible := IC2_WopcoesImpRecShowP2;
    EdAreaP.Visible := IC2_WopcoesImpRecShowP2;
}

  end;

  REICalArea := False;
  //UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  if VAR_SETOR = CO_CALEIRO then REISetor := -3;
  if VAR_SETOR = CO_CURTIM then REISetor := -2;
  if VAR_SETOR = CO_RECURT then REISetor := -1;
  if VAR_SETOR = CO_ACABTO then REISetor := -4;
  case REISetor of
    -3 : CBPeca.KeyValue := -10;
    -2 : CBPeca.KeyValue := -10;
    -1 : CBPeca.KeyValue := -9;
  end;
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
end;

procedure TFmVSFormulasImp_BH.BtConfirmaClick(Sender: TObject);
var
  Codigo, Formula, BxaEstqVS, GraGruX, EmitGru, Empresa: Integer;
  DataEmis: String;
begin
  MyObjects.Informa2VAR_LABELs('Setando dados para janela de impress�o');
  //Parei Aqui
   //Rever tudo
  //Tipificacao em vez de setor
  //arrumar Custo
  //
  EmitGru := EdEmitGru.ValueVariant;
  Formula := EdReceita.ValueVariant;
  //
  if Dmod.ObrigaInformarEmitGru(FEmit <> 0, EmitGru, EdEmitGru) then Exit;

  BxaEstqVS := QrFormulasBxaEstqVS.Value;
  GraGruX   := QrFormulasGraGruX.Value;
  if MyObjects.FIC(BxaEstqVS < Integer(TBxaEstqVS.bevsNao), nil,
  'Pesagem abortada!' + sLineBreak +
  '"Baixa estq VS In Natura" indefinido no cadastro da Receita!') then Exit;
  if MyObjects.FIC(GraGruX = 0, nil,
  'Pesagem abortada!' + sLineBreak +
  '"Artigo padr�o em processo" indefinido no cadastro da Receita!') then Exit;
  //
  Formula := EdReceita.ValueVariant;
  if RGTipoPreco.ItemIndex = 2 then
    if not DmModEmit.MostraPrecosAlternativos(dmktfrmSetrEmi_MOLHADO, Formula) then
      Exit;
  //
  // ini 2024-01-01
  if UnPQx.ImpedePeloBalanco(TPDataP.Date, True) then Exit;
  // fim 2024-01-01
  //
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  if Geral.MB_Pergunta('Confirma que a receita a ser utilizada �:' +
  sLineBreak + QrFormulasNome.Value + sLineBreak + 'E o peso �: ' +
  EdPeso.Text + ' ?') = ID_YES then
  begin
    if FEmit > 0 then
    begin
      DataEmis := Geral.FDT(TPDataP.Date, 1);
      Codigo := FEmit;
      InsereEmitCus(GraGruX);
      (*
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
      'Formula', 'DataEmis',
      'BxaEstqVS', 'GraGruX'], ['Codigo'], [
      Formula, DataEmis,
      BxaEstqVS, GraGruX], [Codigo], True);
      *)
    end;
    BDC_APRESENTACAO_IMPRESSAO := RGImpRecRib.ItemIndex;
    //
    // Configuracoes


    case Dmod.QrControleVerImprRecRib.Value of
      0:
      begin
        MyObjects.Informa2VAR_LABELs('Setando dados para janela "FrFormulasImpShow"');
        FrFormulasImpShow.FReImprime    := False;
        FrFormulasImpShow.FEmit         := FEmit;
        FrFormulasImpShow.FEmitGru      := EmitGru;
        FrFormulasImpShow.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShow.FVSMovCod     := FMovimCod;
        FrFormulasImpShow.FVSMovimSrc   := FMovimSrc;
        FrFormulasImpShow.FTemIMEIMrt   := FTemIMEIMrt;
        FrFormulasImpShow.FRetrabalho   := QrFormulasRetrabalho.Value;;
        //FrFormulasImpShow.FSourcMP      := CO_SourcMP_Ribeira; // Caleiro / curtimento
        FrFormulasImpShow.FSourcMP      := CO_SourcMP_VS_BH; // Caleiro / curtimento
        FrFormulasImpShow.FDataP        := TPDataP.Date;
        FrFormulasImpShow.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShow.FEmpresa      := Empresa;
        FrFormulasImpShow.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShow.FProgressBar1 := ProgressBar1;
        FrFormulasImpShow.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShow.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShow.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShow.FFormula      := Formula;
        FrFormulasImpShow.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShow.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShow.FQtde         := EdQtde.ValueVariant;
        FrFormulasImpShow.FArea         := 0;//EdAreaM.ValueVariant;
        FrFormulasImpShow.FLinhasRebx   := '';//CBEspessura.Text;
        FrFormulasImpShow.FLinhasSemi   := '';//.Text;
        FrFormulasImpShow.FFulao        := EdFulao.Text;
        FrFormulasImpShow.FDefPeca      := CBPeca.Text;
        FrFormulasImpShow.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShow.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShow.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShow.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShow.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShow.FCod_Espess   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShow.FCod_Rebaix   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShow.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShow.FSemiAreaM2   := 0;
        FrFormulasImpShow.FSemiRendim   := 0;
        FrFormulasImpShow.FBRL_USD      := 0;
        FrFormulasImpShow.FBRL_EUR      := 0;
        FrFormulasImpShow.FDtaCambio    := 0;
        //
        FrFormulasImpShow.FObs := FmVSFormulasImp_BH.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShow.FObs := FrFormulasImpShow.FObs + FmVSFormulasImp_BH.EdMemo.Lines[6];
        //
        if FmVSFormulasImp_BH.CkMatricial.Checked then FrFormulasImpShow.FMatricialNovo := True
          else FrFormulasImpShow.FMatricialNovo := False;
        if FmVSFormulasImp_BH.CkGrade.Checked then FrFormulasImpShow.FGrade := True
          else FrFormulasImpShow.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShow.FCalcCustos := True
          else FrFormulasImpShow.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShow.FCalcPrecos := True
          else FrFormulasImpShow.FCalcPrecos := False;
        if FmVSFormulasImp_BH.RBkg.Checked then FrFormulasImpShow.FPesoCalc := 0.01
          else FrFormulasImpShow.FPesoCalc := 10;
        //
        (*
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
        FrFormulasImpShow.FMedia := QrEspessurasEMCM.Value
        else begin
        *)
          if FrFormulasImpShow.FQtde > 0 then FrFormulasImpShow.FMedia :=
          FrFormulasImpShow.FPeso / FrFormulasImpShow.FQtde
        else
          FrFormulasImpShow.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShow.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShow.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        // ini 2023-11-20
        if QrVMIAtuCouNiv1.Value < 8 then // 1 a 7 => Pe�as 8 em diante kg (sub produtos)
        // fim 2023-11-20
          if MyObjects.FIC(FrFormulasImpShow.FQtde <= 0, EdQtde,
          'Defina a quantidade!') then
            Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShow.CalculaReceita(QrVMIAtu);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShow.Show;
      end;
(*
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira deprecada!' +
        sLineBreak +
        'Altere para "1-Nova" a "Vers�o de impress�o de receita de ribeira".' +
        sLineBreak +
        'Na guia "Miscel�nea" das op��es do aplicativo!');
        Exit;
      end;
*)
      1:
      begin
        MyObjects.Informa2VAR_LABELs('Setando dados para janela "FrFormulasImpShowNew"');
        FrFormulasImpShowNew.FTpReceita    := TReceitaTipoSetor.rectipsetrRibeira;
        FrFormulasImpShowNew.FReImprime    := False;
        FrFormulasImpShowNew.FEmit         := FEmit;
        FrFormulasImpShowNew.FEmitGru      := EmitGru;
        FrFormulasImpShowNew.FEmitGru_TXT  := CBEmitGru.Text;
        FrFormulasImpShowNew.FVSMovCod     := FMovimCod;
        FrFormulasImpShowNew.FVSMovimSrc   := FMovimSrc;
        FrFormulasImpShowNew.FTemIMEIMrt   := FTemIMEIMrt;
        FrFormulasImpShowNew.FRetrabalho   := QrFormulasRetrabalho.Value;;
        FrFormulasImpShowNew.FSourcMP      := CO_SourcMP_Ribeira; // Caleiro / curtimento
        FrFormulasImpShowNew.FDataP        := TPDataP.Date;
        FrFormulasImpShowNew.FDtCorrApo    := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
        FrFormulasImpShowNew.FEmpresa      := Empresa;
        FrFormulasImpShowNew.FHoraIni      := EdHoraIni.ValueVariant;
        FrFormulasImpShowNew.FProgressBar1 := ProgressBar1;
        FrFormulasImpShowNew.FTipoPreco    := RGTipoPreco.ItemIndex;
        FrFormulasImpShowNew.FCliIntCod    := EdCliInt.ValueVariant;
        FrFormulasImpShowNew.FCliIntTxt    := CBCliInt.Text;
        FrFormulasImpShowNew.FFormula      := Formula;
        FrFormulasImpShowNew.FTipoImprime  := RGImprime.ItemIndex;
        FrFormulasImpShowNew.FPeso         := EdPeso.ValueVariant;
        FrFormulasImpShowNew.FQtde         := EdQtde.ValueVariant;
        FrFormulasImpShowNew.FArea         := 0;//EdAreaM.ValueVariant;
        FrFormulasImpShowNew.FLinhasRebx   := '';//CBEspessura.Text;
        FrFormulasImpShowNew.FLinhasSemi   := '';
        FrFormulasImpShowNew.FFulao        := EdFulao.Text;
        FrFormulasImpShowNew.FDefPeca      := CBPeca.Text;
        FrFormulasImpShowNew.FHHMM_P       := QrFormulasHHMM_P.Value;
        FrFormulasImpShowNew.FHHMM_R       := QrFormulasHHMM_R.Value;
        FrFormulasImpShowNew.FHHMM_T       := QrFormulasHHMM_T.Value;
        FrFormulasImpShowNew.FSetor        := QrFormulasSetor.Value;
        FrFormulasImpShowNew.FTipific      := QrFormulasTipificacao.Value;
        FrFormulasImpShowNew.FCod_Espess   := 0;//EdEspessura.ValueVariant;
        FrFormulasImpShowNew.FCod_Rebaix   := 0;
        FrFormulasImpShowNew.FCodDefPeca   := EdPeca.ValueVariant;
        FrFormulasImpShowNew.FSemiAreaM2   := 0;
        FrFormulasImpShowNew.FSemiRendim   := 0;
        FrFormulasImpShowNew.FBRL_USD      := 0;
        FrFormulasImpShowNew.FBRL_EUR      := 0;
        FrFormulasImpShowNew.FDtaCambio    := 0;
        //
        FrFormulasImpShowNew.FObs := FmVSFormulasImp_BH.EdMemo.Lines[0]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[1]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[2]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[3]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[4]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[5]+sLineBreak;
        FrFormulasImpShowNew.FObs := FrFormulasImpShowNew.FObs + FmVSFormulasImp_BH.EdMemo.Lines[6];
        //
        if FmVSFormulasImp_BH.CkMatricial.Checked then FrFormulasImpShowNew.FMatricialNovo := True
          else FrFormulasImpShowNew.FMatricialNovo := False;
        if FmVSFormulasImp_BH.CkGrade.Checked then FrFormulasImpShowNew.FGrade := True
          else FrFormulasImpShowNew.FGrade := False;
        //
        if RGImpRecRib.ItemIndex = 1 then FrFormulasImpShowNew.FCalcCustos := True
          else FrFormulasImpShowNew.FCalcCustos := False;
        if RGImpRecRib.ItemIndex = 2 then FrFormulasImpShowNew.FCalcPrecos := True
          else FrFormulasImpShowNew.FCalcPrecos := False;
        if FmVSFormulasImp_BH.RBkg.Checked then FrFormulasImpShowNew.FPesoCalc := 0.01
          else FrFormulasImpShowNew.FPesoCalc := 10;
        //
        (*
        if (CBEspessura.KeyValue <> NULL) and (CBEspessura.KeyValue <> 0) then
        FrFormulasImpShowNew.FMedia := QrEspessurasEMCM.Value
        else begin
        *)
          if FrFormulasImpShowNew.FQtde > 0 then FrFormulasImpShowNew.FMedia :=
          FrFormulasImpShowNew.FPeso / FrFormulasImpShowNew.FQtde
        else
          FrFormulasImpShowNew.FMedia := 0;
        //end;
        //
        if MyObjects.FIC(FrFormulasImpShowNew.FFormula <= 0, EdReceita,
        'Defina uma receita!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FCliIntCod = 0, EdCliInt,
        'Defina um cliente interno!') then
          Exit;
        if MyObjects.FIC(FrFormulasImpShowNew.FPeso <= 0, EdPeso, 'Defina o peso!') then
          Exit;
        // ini 2023-11-20
        if QrVMIAtuCouNiv1.Value < 8 then // 1 a 7 => Pe�as 8 em diante kg (sub produtos)
        // fim 2023-11-20
          if MyObjects.FIC(FrFormulasImpShowNew.FQtde <= 0, EdQtde,
          'Defina a quantidade!') then
            Exit;
        //
        //////////////////////////////////////////////////////////////////////////////
        //
        FrFormulasImpShowNew.CalculaReceita(QrLotes);
        Application.ProcessMessages;
        Hide;
        if CkGrade.Checked then FrFormulasImpShowNew.Show;




      end;
      else
      begin
        Geral.MB_Erro('Vers�o de impress�o de receita de ribeira n�o implementada [2]!');
        Exit;
      end;
    end;


(*
    if CkContinua.Checked = False then
      Close
    else
    begin
      if FEmit > 0 then FEmit := UMyMod.BuscaEmLivreY(
      Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
      //STSP.Caption := IntToStr(FEmit);
      MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
      [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
      Show;
    end;
*)
    Close;
  end;
end;

procedure TFmVSFormulasImp_BH.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmVSFormulasImp_BH.EdPesoChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdQtde.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmVSFormulasImp_BH.EdPesoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdPeso.ValueVariant := QrVMIAtuPesoKg.Value;
end;

procedure TFmVSFormulasImp_BH.EdQtdeChange(Sender: TObject);
var
  Peso, Qtde, Media : Double;
begin
  Peso := EdPeso.ValueVariant;
  Qtde := EdQtde.ValueVariant;
  if Qtde > 0 then Media := Peso / Qtde else Media := 0;
  EdMedia.ValueVariant := Media;
end;

procedure TFmVSFormulasImp_BH.EdReceitaChange(Sender: TObject);
begin
  EdCliInt.ValueVariant := QrFormulasClienteI.Value;
  CBCliInt.KeyValue := QrFormulasClienteI.Value;
end;

procedure TFmVSFormulasImp_BH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  MyObjects.Entitula(GB_L, [LaSP_A, LaSP_B, LaSP_C],
  [], Geral.FF0(FEmit), False, taCenter, 2, 10, 20);
end;

procedure TFmVSFormulasImp_BH.FormDestroy(Sender: TObject);
begin
  VAR_NOMEFORMIMP_ANCESTRAL := CO_VAZIO;
end;

procedure TFmVSFormulasImp_BH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSFormulasImp_BH.InsereEmitCus(GraGruX: Integer);
var
  DataEmis: String;
  Codigo, Controle, MPIn, Formula, MPVIts, VSPedIts, VSMovIts, BxaEstqVS: Integer;
  Pecas, Peso, Custo, AreaM2, AreaP2, PercTotCus: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns; // ImgTipo.SQLType;
  //
  Codigo         := FEmit;
  Controle       := 0;
  MPIn           := 0; // Deprecado
  Formula        := EdReceita.ValueVariant; // Depois? QrEmitNumero.Value;
  BxaEstqVS      := Integer(TBxaEstqVS.bevsNao);  // Deprecado
  DataEmis       := Geral.FDT(TPDataP.Date, 1); //'0000-00-00'; // Depois? QrEmitDataEmis.Value;
  Pecas          := EdQtde.ValueVariant;
  Peso           := EdPeso.ValueVariant;
  AreaM2         := 0.00;
  AreaP2         := 0.00;
  Custo          := 0; // Calcula Depois ?
  MPVIts         := 0; // Deprecado?
  PercTotCus     := 0; // Depois? Peso do Item / Peso Pesagem * 100 >> % do custo da pesagem;
  VSPedIts       := 0; // ????
  VSMovIts       := FIMEI;

  //
  Controle := UMyMod.BuscaEmLivreY_Def('emitcus', 'Controle', SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitcus', False, [
  'Codigo', 'MPIn', 'Formula',
  'DataEmis', 'Pecas', 'Peso',
  'Custo', 'MPVIts', 'AreaM2',
  'AreaP2', 'PercTotCus', 'VSPedIts',
  CO_FLD_TAB_VMI, 'BxaEstqVS', 'GraGruX'], [
  'Controle'], [
  Codigo, MPIn, Formula,
  DataEmis, Pecas, Peso,
  Custo, MPVIts, AreaM2,
  AreaP2, PercTotCus, VSPedIts,
  VSMovIts, BxaEstqVS, GraGruX], [
  Controle], True) then
  ;
end;

procedure TFmVSFormulasImp_BH.QrFormulasCalcFields(DataSet: TDataSet);
begin
  QrFormulasHHMM_P.Value := dmkPF.HorasMH(QrFormulasTempoP.Value, False);
  QrFormulasHHMM_R.Value := dmkPF.HorasMH(QrFormulasTempoR.Value, False);
  QrFormulasHHMM_T.Value := dmkPf.HorasMH(QrFormulasTempoT.Value, False);
end;

procedure TFmVSFormulasImp_BH.QrLotesAfterOpen(DataSet: TDataSet);
begin
  // 2016-04-25 - Evitar erros (numero e BxaEstqVS) por causa da receita!
  //PainelReceita.Enabled := QrLotes.RecordCount = 0;
end;

procedure TFmVSFormulasImp_BH.QrVMIOriIMEICalcFields(DataSet: TDataSet);
begin
  // Compatibilidade TDmModEmit.EfetuaBaixa(
  QrVMIOriIMEIVSMovIts.Value := QrVMIOriIMEIControle.Value;
end;

procedure TFmVSFormulasImp_BH.ReopenLotes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotes, Dmod.MyDB, [
  'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   ',
  'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  ',
  'FROM emitcus emc ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=emc.VSMovIts ',
  'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch ',
  'WHERE emc.Codigo=' + Geral.FF0(FEmit),
  '']);
end;

procedure TFmVSFormulasImp_BH.ReopenReceitas();
var
  SQL_Setor: String;
begin
  if FReceitaRibSetor <> recribsetNaoAplic then
    SQL_Setor :=
    'WHERE (lse.TpReceita=' + Geral.FF0(Integer(rectipsetrRibeira)) +
    ' AND lse.TpSetor=' + Geral.FF0(Integer(recribsetNaoAplic)) +
    ') OR lse.TpSetor=' + Geral.FF0(Integer(FReceitaRibSetor))
  else
    SQL_Setor := 'WHERE lse.TpReceita=' + Geral.FF0(Integer(rectipsetrRibeira));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT rec.*  ',
  'FROM formulas rec ',
  'LEFT JOIN listasetores lse ON lse.Codigo=rec.Setor ',
  SQL_Setor,
  '']);
end;

procedure TFmVSFormulasImp_BH.ReopenVMIAtu();
const
  Controle   = 0;
  TemIMEIMrt = 0;
  SQL_Limit  = '';
begin
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVMIAtu, FMovimCod, FIMEI, FTemIMEIMrt, FMovimInn);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVMIOriIMEI, FMovimCod, Controle, TemIMEIMrt, FMovimSrc, SQL_Limit);
  //EdPeso.ValueVariant := QrVMIAtuPesoKg.Value;
  EdQtde.ValueVariant := QrVMIAtuPecas.Value;
  //EdPeca.ValueVariant := ;
  //CBPeca
  //TPDataP.Date := QrVMIAtuDataHora.Value;
  TPDataP.SetaDataSafe(QrVMIAtuDataHora.Value);
end;

procedure TFmVSFormulasImp_BH.AtualizaPesoEPecas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Peso) Peso,  ',
  'SUM(Pecas) Pecas ',
  'FROM emitcus ',
  'WHERE Codigo=' + Geral.FF0(FEmit),
  '']);
  //QrSoma.Close;
  //QrSoma.Params[0].AsInteger := FEmit;
  //UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  if (QrSomaPeso.Value >= 0.001) then
  begin
    EdPeso.ValueVariant := QrSomaPeso.Value;
    EdQtde.ValueVariant := QrSomaPecas.Value;
  end;
end;

procedure TFmVSFormulasImp_BH.SpeedButton1Click(Sender: TObject);
begin
(*
  FmPrincipal.CadastrodeDefPecas();
  QrDefPecas.Close;
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
*)
end;

end.

