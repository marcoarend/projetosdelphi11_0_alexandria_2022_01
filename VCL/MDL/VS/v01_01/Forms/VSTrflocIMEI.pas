unit VSTrflocIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, BlueDermConsts, UnAppEnums;

type
  TFmVSTrflocIMEI = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    DBGrid1: TDBGrid;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    DsVSPallet: TDataSource;
    Panel3: TPanel;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    Panel5: TPanel;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label9: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    Panel6: TPanel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaPecas: TLabel;
    Label4: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    EdValorT: TdmkEdit;
    LaValorT: TLabel;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    RGTipoCouro: TRadioGroup;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    GroupBox3: TGroupBox;
    EdPsqPallet: TdmkEdit;
    Label10: TLabel;
    EdPsqFicha: TdmkEdit;
    Label11: TLabel;
    QrVSMovItsSerieFch: TIntegerField;
    Label12: TLabel;
    EdSrcGGX: TdmkEdit;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    SbValorT: TSpeedButton;
    QrVSMovItsMediaM2: TFloatField;
    Label13: TLabel;
    EdPsqIMEI: TdmkEdit;
    BtReabre: TBitBtn;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    Panel10: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label16: TLabel;
    EdObserv: TdmkEdit;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsClientMO: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrVSMovItsNFeSer: TSmallintField;
    QrVSMovItsNFeNum: TIntegerField;
    CBStqCenLocSrc: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdStqCenLocSrc: TdmkEditCB;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label39: TLabel;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    Label38: TLabel;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrStqCenLocEntiSitio: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
    procedure RGTipoCouroClick(Sender: TObject);
    procedure EdPsqPalletChange(Sender: TObject);
    procedure EdPsqFichaChange(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure ReopenVSMovIts((*GraGruX: Integer*));
    procedure HabilitaInclusao();
    procedure ReopenGGXY();
    procedure CalculaValorT();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSTrflocIMEI: TFmVSTrflocIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSTrfLocCab, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSTrflocIMEI.BtOKClick(Sender: TObject);
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, CtrlSorc, CtrlDest, MovimCod, Empresa, GraGruX, Terceiro,
  CliVenda, SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  MovimTwn, DstGGX, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  NotaMPAG: Double;
  FornecMO, ReqMovEstq, StqCenLoc, GGXRcl: Integer;
  Observ: String;
begin
  //FornecMO       := QrVSMovItsFornecMO.Value;
  FornecMO       := QrStqCenLocEntiSitio.Value;
  NotaMPAG       := QrVSMovItsNotaMPAG.Value;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  Observ         := EdObserv.Text;
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SrcGGX         := 0;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  CtrlSorc       := 0; //Ed????.ValueVariant;
  CtrlDest       := 0; //Ed????.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVSMovItsClientMO.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidTransfLoc;
  MovimNiv       := eminDestLocal;
  Pallet         := QrVSMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  FUltGGX        := GraGruX;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorMP        := EdValorT.ValueVariant;
  ValorT         := ValorMP;
  //
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Marca          := QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClientMO, Terceiro, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, nil, nil(*EdPallet*), EdFicha, EdPecas,
    EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
    nil)
  then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc,
  'Informe o local/centro de estoque') then
    Exit;
  //
  ItemNFe     := 0;
  //
  CtrlSorc := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlSorc);
  CtrlDest := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlDest);
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  //
  //Inclusao!!!
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, (*Controle*)CtrlDest, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei077(*Gera��o de couro no destino em transfer�ncia de estoque por IME-I*)) then
  begin
    //Baixa!!!
    //FornecMO       := QrVSMovItsFornecMO.Value;
    NotaMPAG       := 0;
    ReqMovEstq     := 0;
    StqCenLoc      := QrVSMovItsStqCenLoc.Value;;
    Observ         := '';
    SrcMovID       := EdSrcMovID.ValueVariant;
    SrcNivel1      := EdSrcNivel1.ValueVariant;
    SrcNivel2      := EdSrcNivel2.ValueVariant;
    SrcGGX         := EdSrcGGX.ValueVariant;
    //
    MovimNiv       := eminSorcLocal;
    Pecas          := -Pecas;
    PesoKg         := -PesoKg;
    AreaM2         := -AreaM2;
    AreaP2         := -AreaP2;
    ValorT         := -ValorT;
    //
    DstMovID       := TEstqMovimID(0);
    DstNivel1      := 0;
    DstNivel2      := 0;
    DstGGX         := 0;
    //
    if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, (*Controle*) CtrlSorc, Ficha, (*Misturou,*)
    CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
    QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
    GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei078(*Baixa de couro da origem em transfer�ncia de estoque por IME-I*)) then
    begin
      VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
      VS_CRC_PF.AtualizaSaldoIMEI(CtrlDest, False);
      VS_CRC_PF.AtualizaTotaisVSXxxCab('vstrfloccab', MovimCod);
      FmVSTrfLocCab.LocCod(Codigo, Codigo);
      ReopenVSInnIts(CtrlDest);
      FmVSTrfLocCab.AtualizaNFeItens();
      if CkContinuar.Checked then
      begin
        ImgTipo.SQLType            := stIns;
        //
        EdValorT.Enabled           := False;
        LaValorT.Enabled           := False;
        //
        EdSrcMovID.ValueVariant    := 0;
        EdSrcNivel1.ValueVariant   := 0;
        EdSrcNivel2.ValueVariant   := 0;
        EdSrcGGX.ValueVariant      := 0;
        //
        EdControle.ValueVariant    := 0;
        EdGraGruX.ValueVariant     := 0;
        CBGraGruX.KeyValue         := Null;
        EdPecas.ValueVariant       := 0;
        EdPesoKg.ValueVariant      := 0;
        EdAreaM2.ValueVariant      := 0;
        EdAreaP2.ValueVariant      := 0;
        EdValorT.ValueVariant      := 0;
        EdPallet.ValueVariant      := 0;//DefineProximoPallet();
        //
        EdGraGruX.SetFocus;
        //
        ReopenVSMovIts();
      end else
        Close;
    end;
  end;
end;

procedure TFmVSTrflocIMEI.BtReabreClick(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSTrflocIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSTrflocIMEI.CalculaValorT();
var
  Pecas, AreaM2, PesoKg, Valor: Double;
begin
  begin
    Pecas  := EdPecas.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    PesoKg := EdPesoKg.ValueVariant;
    Valor := 0;
    (*
    case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
      encPecas: ;//Valor := 0;
      encArea: ;//Valor := 0;
      encValor:
      begin
    *)
        if (QrVSMovItsAreaM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
          Valor := AreaM2 * (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
        else
        if QrVSMovItsPesoKg.Value > 0 then
          Valor := PesoKg * (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value)
        else
        if QrVSMovItsPecas.Value > 0 then
          Valor := Pecas * (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
    (*
      end;
      else
      begin
        //Valor := 0;
        Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
      end;
    end;
    *)
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmVSTrflocIMEI.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSTrflocIMEI.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  //
  //Valor := 0;
  (*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
  *)
      if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
        Valor := QrVSMovItsSdoVrtArM2.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
      else
      if QrVSMovItsPecas.Value > 0 then
        Valor := QrVSMovItsSdoVrtPeca.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  (*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
    end;
  end;
  *)
  EdValorT.ValueVariant := Valor;
end;

procedure TFmVSTrflocIMEI.DBGrid1DblClick(Sender: TObject);
begin
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;
  //
  try
    EdPecas.SetFocus;
  except
    //
  end;
end;

{
function TFmVSOutKno.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmVSTrflocIMEI.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSTrflocIMEI.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    CalculaValorT();
end;

procedure TFmVSTrflocIMEI.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSTrflocIMEI.EdAreaM2Redefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
end;

procedure TFmVSTrflocIMEI.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSTrflocIMEI.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSTrflocIMEI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmVSTrflocIMEI.EdPecasRedefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
end;

procedure TFmVSTrflocIMEI.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSTrflocIMEI.EdPsqFichaChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSTrflocIMEI.EdPsqPalletChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSTrflocIMEI.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSTrflocIMEI.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSTrflocIMEI.FormCreate(Sender: TObject);
const
  Default = 2;
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.Align := alClient;
  FUltGGX := 0;
  //
  ReopenGGXY();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vspalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  //
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  VS_CRC_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS, Default);
end;

procedure TFmVSTrflocIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSTrflocIMEI.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmVSTrflocIMEI.ReopenGGXY();
begin
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
    //'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
end;

procedure TFmVSTrflocIMEI.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSTrflocIMEI.ReopenVSMovIts();
const
  ImeiSrc   = 0;
  StqCenLoc = 0;
var
  StqCenCad, GraGruX, Empresa, Pallet, Ficha, IMEI, CouNiv1, CouNiv2: Integer;
begin
  StqCenCad  := EdStqCenLocSrc.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  VS_CRC_PF.ReopenIMEIsPositivos(QrVSMovIts, Empresa, GraGruX, Pallet, Ficha, IMEI,
    RGTipoCouro.ItemIndex, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1, CouNiv2,
    FmVSTrflocCab.QrVSTrfLocCabDtVenda.Value);
end;

procedure TFmVSTrflocIMEI.RGTipoCouroClick(Sender: TObject);
begin
  ReopenGGXY();
end;

procedure TFmVSTrflocIMEI.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_CRC_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSTrflocIMEI.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  LaValorT.Enabled := True;
  EdValorT.SetFocus;
end;

procedure TFmVSTrflocIMEI.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
