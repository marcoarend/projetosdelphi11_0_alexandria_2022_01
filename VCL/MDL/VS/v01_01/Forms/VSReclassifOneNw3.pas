unit VSReclassifOneNw3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, TypInfo,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkEditCB, dmkDBLookupComboBox, UnInternalConsts, ShellApi,
  UnProjGroup_Consts, mySQLDirectQuery, UnGrl_Geral, UnGrl_Vars, UnAppEnums;

type
  THackDBGrid = class(TDBGrid);
  TFLsItens = array of array[0..4] of String;
  TFmVSReclassifOneNw3 = class(TForm)
    PanelOC: TPanel;
    QrVSPaRclCab: TmySQLQuery;
    DsVSPaRclCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    DsVSGerArtNew: TDataSource;
    QrVSPaRclCabNO_TIPO: TWideStringField;
    QrVSPaRclCabGraGruX: TIntegerField;
    QrVSPaRclCabNome: TWideStringField;
    QrVSPaRclCabTipoArea: TSmallintField;
    QrVSPaRclCabEmpresa: TIntegerField;
    QrVSPaRclCabMovimCod: TIntegerField;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabVSGerRcl: TIntegerField;
    QrVSPaRclCabLstPal01: TIntegerField;
    QrVSPaRclCabLstPal02: TIntegerField;
    QrVSPaRclCabLstPal03: TIntegerField;
    QrVSPaRclCabLstPal04: TIntegerField;
    QrVSPaRclCabLstPal05: TIntegerField;
    QrVSPaRclCabLstPal06: TIntegerField;
    QrVSPaRclCabLk: TIntegerField;
    QrVSPaRclCabDataCad: TDateField;
    QrVSPaRclCabDataAlt: TDateField;
    QrVSPaRclCabUserCad: TIntegerField;
    QrVSPaRclCabUserAlt: TIntegerField;
    QrVSPaRclCabAlterWeb: TSmallintField;
    QrVSPaRclCabAtivo: TSmallintField;
    QrVSPaRclCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaRclCabNO_EMPRESA: TWideStringField;
    QrAll: TmySQLQuery;
    QrAllControle: TLargeintField;
    QrAllPecas: TFloatField;
    QrAllAreaM2: TFloatField;
    QrAllAreaP2: TFloatField;
    DsAll: TDataSource;
    QrAllBox: TIntegerField;
    QrAllVSPaRclIts: TIntegerField;
    QrAllVSPallet: TIntegerField;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    PnBox: TPanel;
    LaBox: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    LaArea: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    PnJaClass: TPanel;
    LaJahClasTit: TLabel;
    Panel10: TPanel;
    LaEdJaFoi_PECA: TLabel;
    DBEdJaFoi_PECA: TDBEdit;
    Panel9: TPanel;
    LaEdJaFoi_AREA: TLabel;
    DBEdJaFoi_AREA: TDBEdit;
    PnNaoClass: TPanel;
    LaFaltaClasTit: TLabel;
    PnIntMei: TPanel;
    LaEdSdoVrtPeca: TLabel;
    DBEdSdoVrtPeca: TDBEdit;
    Panel15: TPanel;
    LaEdSdoVrtArM2: TLabel;
    DBEdSdoVrtArM2: TDBEdit;
    QrAllVMI_Sorc: TIntegerField;
    QrAllVMI_Dest: TIntegerField;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    QrVSPaRclCabCacCod: TIntegerField;
    PnExtras: TPanel;
    DBGItensACP: TdmkDBGridZTO;
    PnOutros: TPanel;
    QrVSPaRclCabVSPallet: TIntegerField;
    QrItensACP: TmySQLQuery;
    DsItensACP: TDataSource;
    QrVSPallet0: TmySQLQuery;
    QrVSPallet0Codigo: TIntegerField;
    QrVSPallet0Nome: TWideStringField;
    QrVSPallet0Lk: TIntegerField;
    QrVSPallet0DataCad: TDateField;
    QrVSPallet0DataAlt: TDateField;
    QrVSPallet0UserCad: TIntegerField;
    QrVSPallet0UserAlt: TIntegerField;
    QrVSPallet0AlterWeb: TSmallintField;
    QrVSPallet0Ativo: TSmallintField;
    QrVSPallet0Empresa: TIntegerField;
    QrVSPallet0Status: TIntegerField;
    QrVSPallet0CliStat: TIntegerField;
    QrVSPallet0GraGruX: TIntegerField;
    QrVSPallet0NO_CLISTAT: TWideStringField;
    QrVSPallet0NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet0NO_STATUS: TWideStringField;
    DsVSPallet0: TDataSource;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewNO_Pallet: TWideStringField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrItensACPAreaM2: TFloatField;
    QrItensACPCodigo: TIntegerField;
    QrItensACPControle: TLargeintField;
    RGFrmaIns: TRadioGroup;
    QrItensACPAreaP2: TFloatField;
    QrItensACPCacID: TIntegerField;
    QrItensACPVMI_Dest: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrSorces: TmySQLQuery;
    QrVsiDest: TmySQLQuery;
    QrVsiDestMovimTwn: TIntegerField;
    QrVsiDestMovimID: TIntegerField;
    QrVsiDestCodigo: TIntegerField;
    QrVsiDestControle: TIntegerField;
    QrVsiDestGraGruX: TIntegerField;
    QrVsiDestMovimCod: TIntegerField;
    QrVsiDestEmpresa: TIntegerField;
    QrVsiDestTerceiro: TIntegerField;
    QrSorcesPecas: TFloatField;
    QrSorcesAreaM2: TFloatField;
    QrVsiSorc: TmySQLQuery;
    QrVsiSorcGraGruX: TIntegerField;
    QrSorcesVMI_Sorc: TIntegerField;
    QrVsiSorcAreaM2: TFloatField;
    QrVsiSorcValorT: TFloatField;
    QrVsiSorcCodigo: TIntegerField;
    QrVsiSorcControle: TIntegerField;
    QrVsiSorcFicha: TIntegerField;
    QrVsiSorcSerieFch: TIntegerField;
    QrVsiSorcMarca: TWideStringField;
    QrSorcesAreaP2: TFloatField;
    QrSumSorc: TmySQLQuery;
    QrSumSorcPecas: TFloatField;
    QrSumSorcAreaM2: TFloatField;
    QrSumSorcAreaP2: TFloatField;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    PnResponsaveis: TPanel;
    Label70: TLabel;
    EdRevisor: TdmkEditCB;
    CBRevisor: TdmkDBLookupComboBox;
    Label71: TLabel;
    EdDigitador: TdmkEditCB;
    CBDigitador: TdmkDBLookupComboBox;
    QrVSPaRclCabVSGerRclA: TIntegerField;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    Memo1: TMemo;
    PnDesnate: TPanel;
    PnCfgDesnate: TPanel;
    EdDesnate: TdmkEdit;
    SbDesnate: TSpeedButton;
    Label77: TLabel;
    CkDesnate: TCheckBox;
    DBGDesnate: TDBGrid;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsGraGruX: TIntegerField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsGraGru1: TIntegerField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacItsMediaM2PC: TFloatField;
    QrVSCacItsAgrupaTudo: TFloatField;
    QrVSCacSum: TmySQLQuery;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    DsVSCacIts: TDataSource;
    Splitter1: TSplitter;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSPaRclCabVSMovIts: TIntegerField;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    PnSubClass: TPanel;
    LaSubClass: TLabel;
    EdSubClass: TdmkEdit;
    QrAllSubClass: TWideStringField;
    PnEqualize: TPanel;
    PnCfgEqz: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    DBGNotaEqz: TDBGrid;
    Splitter2: TSplitter;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnNota: TPanel;
    DBEdNotaEqzM2: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    CkNota: TCheckBox;
    AlteraitematualSubClasse1: TMenuItem;
    N3: TMenuItem;
    QrVSPallet0FatorInt: TFloatField;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoPallet2: TMenuItem;
    ImprimirfluxodemovimentodoPallet1: TMenuItem;
    N4: TMenuItem;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    QrPalIMEIs: TmySQLQuery;
    QrPalIMEIsControle: TIntegerField;
    QrSumT: TmySQLQuery;
    DsSumT: TDataSource;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    QrVSPaRclCabLstPal07: TIntegerField;
    QrVSPaRclCabLstPal08: TIntegerField;
    QrVSPaRclCabLstPal09: TIntegerField;
    QrVSPaRclCabLstPal10: TIntegerField;
    QrVSPaRclCabLstPal11: TIntegerField;
    QrVSPaRclCabLstPal12: TIntegerField;
    QrVSPaRclCabLstPal13: TIntegerField;
    QrVSPaRclCabLstPal14: TIntegerField;
    QrVSPaRclCabLstPal15: TIntegerField;
    DqAll: TmySQLDirectQuery;
    DqItens: TmySQLDirectQuery;
    DqSumPall: TmySQLDirectQuery;
    PnAll: TPanel;
    SGAll: TStringGrid;
    Panel3: TPanel;
    EdAll: TdmkEdit;
    EdArrAll: TdmkEdit;
    GPBoxes: TGridPanel;
    N5: TMenuItem;
    TesteInclusao1: TMenuItem;
    PnMenu: TPanel;
    BtEncerra: TBitBtn;
    BtReabre: TBitBtn;
    BtImprime: TBitBtn;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    Palletdobox07: TMenuItem;
    Palletdobox08: TMenuItem;
    Palletdobox09: TMenuItem;
    Palletdobox10: TMenuItem;
    Palletdobox11: TMenuItem;
    Palletdobox12: TMenuItem;
    Palletdobox13: TMenuItem;
    Palletdobox14: TMenuItem;
    Palletdobox15: TMenuItem;
    QrVSPallet0QtdPrevPc: TIntegerField;
    Aumentarboxesdisponveis1: TMenuItem;
    N6: TMenuItem;
    EdItens: TEdit;
    Label4: TLabel;
    Excluiitensselecionadonovo1: TMenuItem;
    QrVSGerArtNewClientMO: TIntegerField;
    BtImpFchPal: TBitBtn;
    estacorrigep2param21: TMenuItem;
    QrVSGerArtNewFornecMO: TIntegerField;
    LaInverte: TLabel;
    Memo2: TMemo;
    VertextMemo1: TMenuItem;
    ApagartextoMemo1: TMenuItem;
    QrVSGerArtNewMediaMinM2: TFloatField;
    QrVSGerArtNewMediaMaxM2: TFloatField;
    Qry: TMySQLQuery;
    PnMartelo: TPanel;
    LaVSMrtCad: TLabel;
    CBVSMrtCad: TdmkDBLookupComboBox;
    EdVSMrtCad: TdmkEditCB;
    CkMartelo: TCheckBox;
    QrVSMrtCad: TMySQLQuery;
    QrVSMrtCadCodigo: TIntegerField;
    QrVSMrtCadNome: TWideStringField;
    DsVSMrtCad: TDataSource;
    CkSubClass: TCheckBox;
    SGDefeiDefin: TStringGrid;
    Label1: TLabel;
    estecomponentes1: TMenuItem;
    PnInfoOC: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    Label22: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label18: TLabel;
    Label24: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label87: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit5: TDBEdit;
    EdTempo: TEdit;
    DBEdit35: TDBEdit;
    DadosPaletsnoPaint1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaRclCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaRclCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaRclCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure RGFrmaInsClick(Sender: TObject);
    procedure QrItensACPBeforeClose(DataSet: TDataSet);
    procedure QrItensACPAfterScroll(DataSet: TDataSet);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure QrItensACPAfterOpen(DataSet: TDataSet);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure SbDesnateClick(Sender: TObject);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSubClassExit(Sender: TObject);
    procedure CBRevisorClick(Sender: TObject);
    procedure CBDigitadorClick(Sender: TObject);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure AlteraitematualSubClasse1Click(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure QrVSPallet0AfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoPallet2Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoPallet1Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure TesteInclusao1Click(Sender: TObject);
    procedure EdPecasXXChange(Sender: TObject);
    procedure Aumentarboxesdisponveis1Click(Sender: TObject);
    procedure DBEdSdoVrtPecaChange(Sender: TObject);
    procedure Excluiitensselecionadonovo1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtImpFchPalClick(Sender: TObject);
    procedure estacorrigep2param21Click(Sender: TObject);
    procedure EdAreaEnter(Sender: TObject);
    procedure EdAreaChange(Sender: TObject);
    procedure VertextMemo1Click(Sender: TObject);
    procedure ApagartextoMemo1Click(Sender: TObject);
    procedure EdAreaKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LaInverteClick(Sender: TObject);
    procedure EdSubClassRedefinido(Sender: TObject);
    procedure EdBoxEnter(Sender: TObject);
    procedure CkMarteloClick(Sender: TObject);
    procedure CkSubClassClick(Sender: TObject);
    procedure EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVSMrtCadRedefinido(Sender: TObject);
    procedure estecomponentes1Click(Sender: TObject);
    procedure EdVSMrtCadExit(Sender: TObject);
    procedure DadosPaletsnoPaint1Click(Sender: TObject);
  private
    { Private declarations }
    //FCriando:
    FLiberadoPecasNegativas, FInverte: Boolean;
    FDifTime: TDateTime;
    FLsAll: array of array[0..12] of String;
    FMartelosCod: array of Integer;
    FMartelosNom: array of String;
    //
    //
    FMaxPecas: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Double;
    FSGItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TStringGrid;
    FLsItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TFLsItens;
    FEdPalletPecas, FEdPalletArea, FEdPalletMedia:
    array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TdmkEdit;
    FQrVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FQrVSPalRclIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FPnTxLx, FPnBox, FPnArt1N, FPnArt2N, FPnArt3N, FPnBxTot, FPnNumBx, FPnIDBox,
    FPnSubXx, FPnIMEICtrl, FPnIMEIImei, FPnPalletID, FPnPalletPecas,
    FPnPalletArea, FPnPalletMedia,
    FPnIntMei: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPanel;
    FLaNumBx, FLaIMEICtrl, FLaIMEIImei, FLaPalletID, FLaPalletPecas,
    FLaPalletArea, FLaPalletMedia: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TLabel;
    FCkDuplicaArea: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TCheckBox;
    FCkSubClass: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TCheckBox;
    FDBEdArtNome, FDBEdArtNoSta, FDBEdArtNoCli, FDBEdIMEICtrl, FDBEdIMEIImei,
    FDBEdPalletID: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDBEdit;
    FGBIMEI, FGBPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TGroupBox;
    FPalletDoBox: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TMenuItem;
    FPMItens: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPopupMenu;
    FClasseVisivel: Boolean;
    FIniWidth: Integer;
    FFatorW, FFatorH: Double;
    //
    procedure AdicionaPallet(Box: Integer);
    procedure AtualizaInfoOC();
    procedure AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst: Integer);
    function  BoxDeComp(Compo: TObject): Integer;
    procedure CarregaSGAll();
    procedure CarregaSGItens(Box: Integer);
    procedure CarregaSGSumPall(Box: Integer; Acao: TAcaoMath; Pecas,
              AreaM2, AreaP2: Double);
    function  ObtemNO_MARTELO(Martelo: Integer): String;

    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure ExcluiUltimoCouro();

    procedure InsereCouroArrAll(CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri,
              VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
              Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
              Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
              FatorIntDst: Double; Controle: Int64);
    procedure LiberaDigitacao();
    procedure LiberaDigitacaoManual(Libera: Boolean);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    function  ObtemDadosBox(const Box: Integer; var VSPaRclIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
              Boolean): Boolean;
    function  ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri, VMI_Sorc,
              VSCacItsAOri: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet: TmySQLQuery): Boolean;
    //procedure RealinhaBoxes();
    procedure ReopenAll();
    procedure ReopenVSGerArtDst();
    procedure ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure ReopenSumPal(Box, VSPallet: Integer);
    procedure ReopenItensAClassificarPallet();
    procedure TentarFocarEdArea();
    procedure VerificaAreaAutomatica();
    procedure VerificaBoxes();
    // VSMod ???
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalRclIts:
              TmySQLQuery; Pallet: Integer);
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
    procedure UpdDelCouroSelecionado3(SQLTipo: TSQLType; CampoSubClas: Boolean);
    function  ZeraEstoquePalletOrigem(): Boolean;
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenDesnate(Automatico: Boolean);
    procedure ReopenEqualize(Automatico: Boolean);
    function  SubstituiOC_Antigo(): Boolean;
    function  SubstituiOC_Novo(): Boolean;
    procedure ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoPallet();
    procedure OcultaMostraClasses();
    //procedure AddMemoExe(Txt: String);
    procedure ReopenVSMrtCad();
    procedure ConfigPnSubClass();
    procedure MostraJanelaDefeitos();

  public
    { Public declarations }
    FCriando: Boolean;
    FBoxMax, FCodigo, FCacCod, FStqCenLoc: Integer;
    FMovimID: TEstqMovimID;
    //
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure DefineBoxes(Max: Integer);
    procedure ReopenVSPaRclCab();
    procedure InsereCouroAtual();
  end;

var
  FmVSReclassifOneNw3: TFmVSReclassifOneNw3;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAd3,
  MyDBCheck, UnVS_CRC_PF, AppListas, VSReclassPrePal, UnDmkProcFunc,
  {$IfDef sAllVS}UnVS_PF, {$EndIf}
  VSRclArtPrpNew, VSRclArtPrpNw3, StqCenLoc, Senha, VSClassifOneDefei, UnDmkWeb;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX_15] of Boolean;

const
  CIniWidth = 1298; // PanelOc.Width ideal
{
procedure TFmVSReclassifOneNw3.AddMemoExe(Txt: String);
begin
  Memo2.Text := Geral.FDT(Now(), 109) + ' : ' + Txt + sLineBreak +
    Memo2.Text;
end;
}

procedure TFmVSReclassifOneNw3.AdicionaPallet(Box: Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAd3, FmVSClaArtPalAd3, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAd3.FMovimIDGer              := emidReclasXXUni;
    FmVSClaArtPalAd3.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAd3.FFornecMO                := QrVSGerArtNewFornecMO.Value;
    FmVSClaArtPalAd3.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAd3.FFornecedor              := Trunc(QrVSGerArtNewTerceiro.Value);
    FmVSClaArtPalAd3.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAd3.FMovimID                 := FMovimID;
    FmVSClaArtPalAd3.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAd3.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAd3.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAd3.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAd3.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAd3.EdCodigo.ValueVariant    := QrVSPaRclCabCodigo.Value;
    FmVSClaArtPalAd3.EdSrcPallet.ValueVariant := QrVSPaRclCabVSPallet.Value;
    FmVSClaArtPalAd3.FExigeSrcPallet          := True;
    FmVSClaArtPalAd3.EdMovimCod.ValueVariant  := QrVSPaRclCabMovimCod.Value;
    FmVSClaArtPalAd3.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAd3.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAd3.FBox := Box;
    FmVSClaArtPalAd3.PnBox.Caption := Geral.FF0(Box);
    //
    FmVSClaArtPalAd3.EdStqCenLoc.ValueVariant := FStqCenLoc;
    FmVSClaArtPalAd3.CBStqCenLoc.KeyValue     := FStqCenLoc;
    //
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
      FmVSClaArtPalAd3.FPal[I] := QrVSPaRclCab.FieldByName('LstPal' +
      Geral.FFF(I, 2)).AsInteger;
    //
    //
    VS_CRC_PF.ReopenVSPallet(FmVSClaArtPalAd3.QrVSPallet,
      FmVSClaArtPalAd3.FEmpresa, FmVSClaArtPalAd3.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAd3.ShowModal;
    FmVSClaArtPalAd3.Destroy;
    //
    ReopenVSPaRclCab();
  end;
end;

procedure TFmVSReclassifOneNw3.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneNw3.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, False);
end;

procedure TFmVSReclassifOneNw3.AlteraitematualSubClasse1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, True);
end;

procedure TFmVSReclassifOneNw3.ApagartextoMemo1Click(Sender: TObject);
begin
  Memo2.Lines.Clear;
end;

procedure TFmVSReclassifOneNw3.AtualizaInfoOC();
var
  AreaT: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT Controle, VSPaRclIts, VSPallet, Pecas, ',
  'AreaM2, AreaP2, VMI_Sorc, VMI_Dest, Box, SubClass ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY Controle DESC ',
  '']);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(Pecas * FatorIntDst / FatorIntSrc) IntOuPart ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  '']);
*)
  VS_CRC_PF.ReopenQrySaldoPallet_Visual(QrSumT, QrVSPaRclCabEmpresa.Value,
  QrVSPaRclCabVSPallet.Value, QrVSPaRclCabVSMovIts.Value);
  //
  //
  if CkDesnate.Checked then
    ReopenDesnate(True);
  if CkEqualize.Checked then
    ReopenEqualize(True);
end;

procedure TFmVSReclassifOneNw3.AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst:
  Integer);
var
  Controle: Integer;
begin
  if ClaAPalOri <> 0 then
  begin
    Controle   := ClaAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], VAR_InsUpd_AWServerID);
  end;
  //
  if RclAPalOri <> 0 then
  begin
    Controle   := RclAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], VAR_InsUpd_AWServerID);
  end;
end;

procedure TFmVSReclassifOneNw3.Aumentarboxesdisponveis1Click(Sender: TObject);
var
  I, Boxes, BoxIni, BoxFim: Integer;
begin
  if FBoxMax = VAR_CLA_ART_RIB_MAX_BOX_15 then
  begin
    Geral.MB_Info('A quantidade m�xima de boxes j� foi atingida!');
  end else
  begin
    Boxes := MyObjects.SelRadioGroup('Aumento de disponibilidade de Boxes',
    'Selecione a quantidade', [
    '06', '07', '08', '09', '10', '11', '12', '13', '14', '15'], 5, FBoxMax - 5);
    Boxes := Boxes + 6;
    //
    if Boxes > FBoxMax then
    begin
      BoxIni := FBoxMax + 1;
      BoxFim := Boxes;
      for I := 1 to FBoxMax do
      begin
        //FPnTxLx[I].Destroy;
        FPnTxLx[I].Free;
      end;
      DefineBoxes(BoxFim);
      ReopenVSPaRclCab();
      if not Mostrartodosboxes1.Checked then
        Mostrartodosboxes1Click(Mostrartodosboxes1);
    end;
  end;
end;

function TFmVSReclassifOneNw3.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  CompName := Copy(CompName, Length(CompName) -1);

  Result := Geral.IMV(CompName);
end;

function TFmVSReclassifOneNw3.BoxInBoxes(Box: Integer): Boolean;
(*
var
  I: Integer;
*)
begin
(*
  Result := False;
  if Box <> 0 then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if FBoxes[I] then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
*)
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX_15) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSReclassifOneNw3.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSReclassifOneNw3.BtImpFchPalClick(Sender: TObject);
const
  Boxes: array[00..15] of String = ('??', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15');
  InfoNO_Pallet = False;
var
  Box: Integer;
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
  OK: Boolean;
begin
  OK := False;
  Box := MyObjects.SelRadioGroup('Impress�o de Pallet',
  'Selecione o BOX do pallet', Boxes, 4, -1, True);
  //
  if Box > 0 then
  begin
    if FQrVSPallet[Box] <> nil then
    begin
      if FQrVSPallet[Box].State <> dsInactive then
      begin
        Pallet   := FQrVSPallet[Box].FieldByName('Codigo').AsInteger;
        Empresa  := FQrVSPallet[Box].FieldByName('Empresa').AsInteger;
        ClientMO := FQrVSPallet[Box].FieldByName('ClientMO').AsInteger;
        TempTab  := Self.Name;
        VS_CRC_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_Pallet);
        OK := true;
      end;
    end;
    if not OK then
      Geral.MB_Aviso('Box inv�lido!');
  end;
end;

procedure TFmVSReclassifOneNw3.BtImprimeClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, 4);
end;

procedure TFmVSReclassifOneNw3.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaRclCab();
end;

procedure TFmVSReclassifOneNw3.CarregaSGAll();
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsAll);
  SGAll.RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    SGAll.Cells[00, J] := IntToStr(K - J + 1);
    SGAll.Cells[01, J] := FLsAll[I, 01];   // Box
    SGAll.Cells[02, J] := FLsAll[I, 02];   // SubClass
    SGAll.Cells[03, J] := FLsAll[I, 03];   // AreaM2
    SGAll.Cells[04, J] := FLsAll[I, 04];   // NO_MARTELO
    SGAll.Cells[05, J] := FLsAll[I, 05];   // VMI_Dest
    SGAll.Cells[06, J] := FLsAll[I, 06];   // Controle
    SGAll.Cells[07, J] := FLsAll[I, 07];   // VSPaRclIts
    SGAll.Cells[08, J] := FLsAll[I, 08];   // VSPallet
    SGAll.Cells[09, J] := FLsAll[I, 09];   // Pecas
    SGAll.Cells[10, J] := FLsAll[I, 10];   // AreaP2
    SGAll.Cells[11, J] := FLsAll[I, 11];   // VMI_Sorc
    // ini 2023-12-26
    //SGAll.Cells[12, J] := FLsAll[I, 12];   // VMI_Dest
    SGAll.Cells[12, J] := FLsAll[I, 12];   // VMI_Baix
    // fim 2023-12-26
  end;
end;

procedure TFmVSReclassifOneNw3.CarregaSGItens(Box: Integer);
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsItens[Box]);
  FSGItens[Box].RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    FSGItens[Box].Cells[00, J] := IntToStr(K - J + 1);
    case QrVSPaRclCabTipoArea.Value of
      0: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 03];
      1: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 04];
      else FSGItens[Box].Cells[01, J] := '?.??';
    end;
(*
    SGItens[Box].Cells[01, J] := FLsItens[Box][I, 01];   // Controle
    SGItens[Box].Cells[02, J] := FLsItens[Box][I, 02];   // Pecas
    SGItens[Box].Cells[03, J] := FLsItens[Box][I, 03];   // AreaM2
    SGItens[Box].Cells[04, J] := FLsItens[Box][I, 04];   // AreaP2
*)
  end;
end;

procedure TFmVSReclassifOneNw3.CarregaSGSumPall(Box: Integer; Acao: TAcaoMath;
  Pecas, AreaM2, AreaP2: Double);
var
  Area, P, A, M: Double;
begin
  if QrVSPaRclCabTipoArea.Value = 0 then
    Area := AreaM2
  else
    Area := AreaP2;
  //
  case Acao of
    amathZera:
    begin
      P := 0;
      A := 0;
    end;
    amathSubtrai:
    begin
      P := FEdPalletPecas[Box].ValueVariant - Pecas;
      A := FEdPalletArea[Box].ValueVariant - Area;
    end;
    amathSoma:
    begin
      P := FEdPalletPecas[Box].ValueVariant + Pecas;
      A := FEdPalletArea[Box].ValueVariant + Area;
    end;
    amathSubstitui:
    begin
      P := Pecas;
      A := Area;
    end;
  end;
  if P = 0 then
    M := 0
  else
    M := A / P;
  //
  FEdPalletPecas[Box].ValueVariant := P;
  FEdPalletArea[Box].ValueVariant  := A;
  FEdPalletMedia[Box].ValueVariant := M;
end;

procedure TFmVSReclassifOneNw3.CBDigitadorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CBRevisorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkMarteloClick(Sender: TObject);
begin
  PnMartelo.Visible := CkMartelo.Checked;
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSReclassifOneNw3.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClassClick(Sender: TObject);
begin
  ConfigPnSubClass();
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.ConfigPnSubClass();
var
  I: Integer;
  Check: Boolean;
begin
  //if not CkSubClass.Checked then
  begin
    Check := False;
    for I := 1 to FBoxMax do
      if (FCkSubClass[I] <> nil) and
      FCkSubClass[I].Checked and FPnBox[I].Visible then Check := True;
  end;
  PnSubClass.Visible := Check and CkSubClass.Checked;
end;

procedure TFmVSReclassifOneNw3.DadosPaletsnoPaint1Click(Sender: TObject);
var
  Dir, NomeArq, Arq: String;
begin
  Dir := 'C:\Dermatek\OCsCapt\';
  NomeArq := 'OC' + DBEdCodigo.Text +
    Geral.SoNumero_TT(Geral.FDT(DModG.ObtemAgora(), 109)) + '.jpg';
  try
    Arq := DmkWeb.CapturaTela(Dir, NomeArq);
    ShellExecute(Application.Handle, 'open', pchar(Arq), nil, nil, sw_ShowNormal);
  except
    ForceDirectories(Dir);
    raise;
  end;
end;

procedure TFmVSReclassifOneNw3.DBEdSdoVrtPecaChange(Sender: TObject);
var
  CorTexto, CorFundo: TColor;
begin
  CorTexto := clWindowText;
  CorFundo := clWindow;
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value = 0) then
  begin
    CorTexto := clYellow;
    CorFundo := clBlack;
  end else
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value < 0) then
  begin
    CorTexto := clRed;
    CorFundo := clYellow;
  end;
  DBEdSdoVrtPeca.Color := CorFundo;
  DBEdSdoVrtPeca.Font.Color := CorTexto;
  DBEdSdoVrtArM2.Color := CorFundo;
  DBEdSdoVrtArM2.Font.Color := CorTexto;
  EdArea.Color := CorFundo;
  EdArea.Font.Color := CorTexto;
  EdBox.Color := CorFundo;
  EdBox.Font.Color := CorTexto;
end;

procedure TFmVSReclassifOneNw3.DefineBoxes(Max: Integer);
var
  I, Col, Row: Integer;
begin
  GPBoxes.Visible := False;
  try
    if FBoxMax < Max then
      FBoxMax := Max;
    //
    GPBoxes.ColumnCollection.BeginUpdate;
    GPBoxes.ColumnCollection.Clear;
    GPBoxes.RowCollection.BeginUpdate;
    GPBoxes.RowCollection.Clear;
    case FBoxMax of
      00..06: begin Col := 3; Row := 2; end;
      07..09: begin Col := 3; Row := 3; end;
      10..12: begin Col := 4; Row := 3; end;
      13..15: begin Col := 5; Row := 3; end;
    end;
    for I := 0 to Col - 1 do
      GPBoxes.ColumnCollection.Add;
    for I := 0 to Row - 1 do
      GPBoxes.RowCollection.Add;
    //
    GPBoxes.ColumnCollection.EndUpdate;
    GPBoxes.RowCollection.EndUpdate;
    for I := 1 to FBoxMax do
    begin
      VS_CRC_PF.CriaBoxEmGridPanel(I, Self, GPBoxes, FPnTxLx, FPnBox, FPnArt1N,
      FPnArt2N, FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei,
      FPnIMEICtrl, FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
      FPnIntMei, FLaNumBx, FLaIMEIImei, FLaIMEICtrl, FLaPalletMedia, FLaPalletArea,
      FLaPalletPecas, FLaPalletID, FCkSubClass, FCkDuplicaArea, FDBEdArtNome, FDBEdArtNoCli,
      FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID, FGBIMEI,
      FGBPallet, FEdPalletMedia, FEdPalletArea, FEdPalletPecas, FSGItens,
      FPMItens);
      FEdPalletPecas[I].OnChange := EdPecasXXChange;
      //
      FBoxes[I] := False;
      //
      FQrVSPallet[I] := TmySQLQuery.Create(Dmod);
      FDsVSPallet[I] := TDataSource.Create(Self);
      FDsVSPallet[I].DataSet := FQrVSPallet[I];
      FDBEdPalletID[I].DataSource := FDsVSPallet[I];
      FDBEdArtNome[I].DataSource  := FDsVSPallet[I];
      FDBEdArtNoCli[I].DataSource := FDsVSPallet[I];
      FDBEdArtNoSta[I].DataSource := FDsVSPallet[I];
      //
      FQrVSPalRclIts[I] := TmySQLQuery.Create(Dmod);
      FDsVSPalClaIts[I] := TDataSource.Create(Self);
      FDsVSPalClaIts[I].DataSet := FQrVSPalRclIts[I];
      FDBEdIMEICtrl[I].DataSource := FDsVSPalClaIts[I];
      FDBEdIMEIImei[I].DataSource := FDsVSPalClaIts[I];
      //
    end;
    //for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15
    FPalletDoBox[01] := PalletDoBox01;
    FPalletDoBox[02] := PalletDoBox02;
    FPalletDoBox[03] := PalletDoBox03;
    FPalletDoBox[04] := PalletDoBox04;
    FPalletDoBox[05] := PalletDoBox05;
    FPalletDoBox[06] := PalletDoBox06;
    FPalletDoBox[07] := PalletDoBox07;
    FPalletDoBox[08] := PalletDoBox08;
    FPalletDoBox[09] := PalletDoBox09;
    FPalletDoBox[10] := PalletDoBox10;
    FPalletDoBox[11] := PalletDoBox11;
    FPalletDoBox[12] := PalletDoBox12;
    FPalletDoBox[13] := PalletDoBox13;
    FPalletDoBox[14] := PalletDoBox14;
    FPalletDoBox[15] := PalletDoBox15;
  finally
    GPBoxes.Visible := True;
  end;
  //
end;

procedure TFmVSReclassifOneNw3.BtClassesGeradasClick(Sender: TObject);
begin
  VS_CRC_PF.ImprimeReclassOC(QrVSPaRclCabCodigo.Value, QrVSPaRclCabCacCod.Value);
end;

procedure TFmVSReclassifOneNw3.EdAreaChange(Sender: TObject);
begin
    //AddMemoExe('EdAreaChange: ' + EdArea.Text);
end;

procedure TFmVSReclassifOneNw3.EdAreaEnter(Sender: TObject);
begin
  FInverte := False;
  //AddMemoExe('EdAreaEnter');
end;

procedure TFmVSReclassifOneNw3.EdAreaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //AddMemoExe('EdAreaKeyDown inicio');
  if Key = VK_F4 then
  begin
    //AddMemoExe('EdAreaKeyDown VK_F4 inicio');
    VS_CRC_PF.MostraFormVSDivCouMeio(emidReclasXXUni, FBoxes, LaTipoArea.Caption);
    //AddMemoExe('EdAreaKeyDown VK_F4 fim');
  end;
  if Key = VK_F5 then
  begin
    //AddMemoExe('EdAreaKeyDown VK_F5 inicio');
    MostraJanelaDefeitos();
    //AddMemoExe('EdAreaKeyDown VK_F5 Fim');
  end;
  // ini 2023-04-13
  if Key = VK_F12 then
  begin
    EdArea.ValueVariant := EdArea.ValueVariant * 2;
  end;
  // fim 2023-04-13
end;

procedure TFmVSReclassifOneNw3.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  if (FBoxMax > 9) and (Length(EdBox.Text) < 2) then
    Exit;
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if PnSubClass.Visible then
      EdSubClass.SetFocus
    else
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end else
  if (EdBox.Text = '-') then
    ExcluiUltimoCouro();
(*
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if RGFrmaIns.ItemIndex < 3 then
    begin
      if EdArea.ValueVariant >= 0.01 then
        //Insere CouroAtual()
      else
        Geral.MB_Erro('�rea n�o definida!');
    end else
      //EdArea.SetFocus;
      //Insere CouroAtual();
      ;
  end else
  if (EdBox.Text = '-') then
    ExcluiUltimoCouro();
*)
end;

procedure TFmVSReclassifOneNw3.EdBoxEnter(Sender: TObject);
begin
  if EdArea.ValueVariant = 0 then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
  end;
end;

procedure TFmVSReclassifOneNw3.EdBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Box: Integer;
*)
begin
(*
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
        Insere CouroAtual();
  end;
*)
  if Key = VK_F5 then
    MostraJanelaDefeitos()
end;

procedure TFmVSReclassifOneNw3.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneNw3.EdPecasXXChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := Geral.IMV(Copy(TdmkEdit(Sender).Name, 8));
  if TdmkEdit(Sender).ValueVariant >= FMaxPecas[Box] then
  begin
    TdmkEdit(Sender).Font.Color := clRed;
    TdmkEdit(Sender).Color := clYellow;
  end else
  begin
    TdmkEdit(Sender).Font.Color := clBlue;
    TdmkEdit(Sender).Color := clWindow;
  end;
end;

procedure TFmVSReclassifOneNw3.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneNw3.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSReclassifOneNw3.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSReclassifOneNw3.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
  ExigeSubClass: Boolean;
begin
  if Key = VK_F5 then
    MostraJanelaDefeitos()
  else
(* 2021-03-01
   C�digo movido para OnRedefinido pois n�o est� mais passsando por aqui!
   //
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
      //
      Insere CouroAtual();
    end;
  end;
*)
end;

procedure TFmVSReclassifOneNw3.EdSubClassRedefinido(Sender: TObject);
var
  Box: Integer;
  ExigeSubClass: Boolean;
begin
  //if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        //Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end else
      if PnMartelo.Visible then
        EdVSMrtCad.SetFocus
      else
      begin
        InsereCouroAtual();
        EdArea.SetFocus;
        EdArea.SelectAll;
      end;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.EdVSMrtCadExit(Sender: TObject);
begin
  //else
  begin
    if (CkMartelo.Checked = True) and
    (EdArea.ValueVariant >= 1) and
    (EdBox.ValueVariant >= 1) then
    begin
      if Length(Trim(EdVSMrtCad.ValueVariant)) =  0 then
      try
        EdVSMrtCad.SetFocus;
      except

      end;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    MostraJanelaDefeitos()
end;

procedure TFmVSReclassifOneNw3.EdVSMrtCadRedefinido(Sender: TObject);
begin
  // ini 2023-12-14
  if Length(Trim(EdVSMrtCad.ValueVariant)) >  0 then
    InsereCouroAtual()
  // ini 2023-12-14
end;

function TFmVSReclassifOneNw3.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  ExigeSubClass: Boolean;
  //
  var
    ReabreVSPaRclCab: Boolean;
begin
  Result := False;
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  Result := VS_CRC_PF.EncerraPalletReclassificacaoNew(QrVSPaRclCabCacCod.Value,
  QrVSPaRclCabCodigo.Value, QrVSPaRclCabVSPallet.Value, Box, VSPaClaIts,
  VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, EncerrandoTodos, Pergunta,
  ReabreVSPaRclCab);
  if ReabreVSPaRclCab then
      ReopenVSPaRclCab();
end;

procedure TFmVSReclassifOneNw3.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaRclCab();
  end;
end;

procedure TFmVSReclassifOneNw3.estacorrigep2param21Click(Sender: TObject);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE vscacitsa ',
  'SET AreaP2=AreaM2, ',
  'AreaM2=AreaM2 / 10.7639104 ',
  'WHERE AreaM2 > 15 ',
  '']);
end;

procedure TFmVSReclassifOneNw3.EstaOCOrdemdeclassificao1Click(Sender: TObject);
var
  DtHrFimCla: String;
  //
  function DefPalRclIts(Qry: TmySQLQuery): Integer;
  begin
    if (Qry <> nil) and (Qry.State <> dsInactive) then
      Result := Qry.FieldByName('Controle').AsInteger
    else
      Result := 0;
  end;
  procedure EncerraBox(Box, PalRclIts: Integer);
  var
    VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
    ExigeSubClass: Boolean;
  begin
    if PalRclIts <> 0 then
    begin
      if ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
      ExigeSubClass) then
        VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      // Encerra IME-I
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
      'DtHrFim'], ['Controle'], [DtHrFimCla], [PalRclIts], True) then
        VS_CRC_PF.AtualizaStatPall(VSPallet);
    end;
  end;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
  LstPal07 = 0;
  LstPal08 = 0;
  LstPal09 = 0;
  LstPal10 = 0;
  LstPal11 = 0;
  LstPal12 = 0;
  LstPal13 = 0;
  LstPal14 = 0;
  LstPal15 = 0;
var
  VSPalRclIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Integer;
  Codigo, I, Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
  Fecha: Boolean;
begin
  if Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    begin
      if FQrVSPalRclIts[I] <> nil then
      begin
        VSPalRclIts[I] := 0;
        if FQrVSPalRclIts[I].State <> dsInactive then
        //try
          VSPalRclIts[I] := FQrVSPalRclIts[I].FieldByName('Controle').AsInteger
        //except
          //
        //end;
      end
      else
        VSPalRclIts[I] := 0;
    end;
    //
    ZeraEstoquePalletOrigem();
     //
    (* DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaRclCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    *)
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    //Codigo     := QrVSGerArtNewCodigo.Value;
    Codigo := QrVSPaRclCabVSGerRclA.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerrcla', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaRclCabCodigo.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06',
      'LstPal07', 'LstPal08', 'LstPal09',
      'LstPal10', 'LstPal11', 'LstPal12',
      'LstPal13', 'LstPal14', 'LstPal15'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06,
      LstPal07, LstPal08, LstPal08,
      LstPal10, LstPal11, LstPal12,
      LstPal13, LstPal14, LstPal15
      ], [Codigo], True) then
      begin
        Fecha := not SubstituiOC_Novo();
        //
        for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
          EncerraBox(I, VSPalRclIts[I]);
        //
        if Fecha then
          Close
        else
          ReopenVSPaRclCab();
      end;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.estecomponentes1Click(Sender: TObject);
var
  I, N: Integer;
  s: String;
  Pai: TControl;
begin
  N := 0;
  for I := 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[I] is TControl then
    begin
      //if MyObjects.ComponenteTemAPropriedade(Self.Components[I], 'Parent') = True then
      //if IsPublishedProp(Self.Components[I], 'Parent') then
      begin
        Pai := nil;
        try
        Pai := TControl(Self.Components[I]).Parent;
        //Pai := TControl(GetObjectProp(Self.Components[I], 'Parent'));
        //Pai := GetPropValue(Self.Components[I], 'Parent');
        if Pai <> nil then
        begin
          N := N + 1;
          s := s + 'I=' + Geral.FF0(I) +' N=' + Geral.FF0(N) + ' : ' +
            Self.Components[I].Name + ' -> Parent = ' +
            Pai.Name + ' - Owner = ' +
            TControl((Self.Components[I]).Owner).Name + sLineBreak;
        end;
        except
          //
        end;
      end;
    end;
  end;
  //
  Geral.MB_Info(s);
end;

procedure TFmVSReclassifOneNw3.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel, False);
end;

procedure TFmVSReclassifOneNw3.Excluiitensselecionadonovo1Click(
  Sender: TObject);
begin
  UpdDelCouroSelecionado3(stDel, False);
end;

procedure TFmVSReclassifOneNw3.ExcluiUltimoCouro();
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Controle, Box, All_VSPaRclIts, All_VSPallet,
  Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  QrVSPallet: TmySQLQuery;
  ExigeSubClass: Boolean;
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  if VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) =
  ID_YES then
  begin
    EdBox.ValueVariant := 0;
    EdArea.ValueVariant := 0;
    //
    if not FCriando and EdArea.CanFocus then
      EdArea.SetFocus;
    if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus
    else
    if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
    EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
      EdBox.SetFocus;
    //
    ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
    AtualizaInfoOC();
  end;
end;

procedure TFmVSReclassifOneNw3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSReclassifOneNw3.FormCreate(Sender: TObject);
  function GetFontHeight(Base: Integer; LarguraMinima: Integer = 1920; AlturaMinima: Integer = 1080): Integer;
  var
    Fator: Double;
  begin
    if (AlturaMinima / 1080) < (LarguraMinima / 1920) then
      Fator := AlturaMinima / 1080
    else
      Fator := (LarguraMinima / 1920) ;
     //
    Result := -Trunc(-Base * (Screen.Width / 1920) / Fator * FFatorH);
  end;
  function ObtemWidth(Base: Integer; LarguraMinima: Integer = 1920): Integer;
  begin
    Result := Trunc(Base * (Screen.Width / 1920) / (LarguraMinima / 1920) * FFatorW);
  end;
  function ObtemHeigth(Base: Integer; AlturaMinima: Integer = 1080): Integer;
  var
    H1, H2: Integer;
  begin
    H1 := Trunc(Base * (Screen.Height / 1080) / (AlturaMinima / 1080) * FFatorH);
    //H2 := Trunc(Base * (Screen.Width * 1920));
    //if H1 < H2 then
      Result := H1
    //else
      //Result := H2;
  end;
var
  MenuItem: TMenuItem;
  IdTXT: String;
  I, OCHeightFator: Integer;
  OK: Boolean;
begin
  FLiberadoPecasNegativas := False;
  FIniWidth := CIniWidth; // PanelOC!!!
  if FIniWidth > Screen.Width then
    FIniWidth := Screen.Width; // PanelOC!!!
  if FIniWidth < CIniWidth
    then FFatorW := FIniWidth / CIniWidth
  else
    FFatorW := 1.0;
    //
  //
  OCHeightFator := Trunc(FIniWidth / Screen.Width * Screen.Height);
  FFatorH := 1.0;

  //
(*  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    FBoxes[I] := False;
*)  //
  //
  RGFrmaIns.ItemIndex := 3;
  //
  //
  //
  //
  FCriando := True;
  FClasseVisivel := True;
  FInverte := False;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  ReopenVSMrtCad();
  //
  SGAll.PopupMenu := PMAll;
  //
  TesteInclusao1.Enabled := VAR_USUARIO = -1;
  //
  SGAll.Cells[00, 0] := 'Seq';
  SGAll.Cells[01, 0] := 'Box';
  SGAll.Cells[02, 0] := 'SubClas';
  SGAll.Cells[03, 0] := 'AreaM2';
  SGAll.Cells[04, 0] := 'Martelo';
  SGAll.Cells[05, 0] := 'VMI_Dest';
  SGAll.Cells[06, 0] := 'Controle';
  SGAll.Cells[07, 0] := 'VSPaRclIts';
  SGAll.Cells[08, 0] := 'VSPallet';
  SGAll.Cells[09, 0] := 'Pecas';
  SGAll.Cells[10, 0] := 'AreaP2';
  SGAll.Cells[11, 0] := 'VMI_Sorc';
  // ini 2023-12-26
  //SGAll.Cells[12, 0] := 'VMI_Dest';
  SGAll.Cells[12, 0] := 'VMI_Baix';
  // fim 2023-12-26
  //
  SGAll.ColWidths[00] := 40;
  SGAll.ColWidths[01] := 40; //Box
  SGAll.ColWidths[02] := 72; //SubClass
  SGAll.ColWidths[03] := 48; //AreaM2
  SGAll.ColWidths[04] := 72; //NO_MARTELO
  SGAll.ColWidths[05] := 90; //VMI_Dest
  SGAll.ColWidths[06] := 90; //Controle
  SGAll.ColWidths[07] := 90; //VSPaRclIts
  SGAll.ColWidths[08] := 90; //VSPallet
  SGAll.ColWidths[09] := 40; //Pecas
  SGAll.ColWidths[10] := 40; //AreaP2
  SGAll.ColWidths[11] := 90; //VMI_Sorc
  // ini 2023-12-26
  //SGAll.ColWidths[12] := 90; //VMI_Dest
  SGAll.ColWidths[12] := 90; //VMI_Baix
  // fim 2023-12-26
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
  begin
    IdTXT := Geral.FFF(I, 2);
    //
    FPMItens[I] := TPopupMenu.Create(Self);
    FPMItens[I].Name := 'PMItens' + IdTXT;
    FPMItens[I].OnPopup := PMItensPopup;
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'AdicionarPallet' + IdTXT;
    MenuItem.Caption := '&Adicionar pallet';
    MenuItem.OnClick := AdicionarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'EncerrarPallet' + IdTXT;
    MenuItem.Caption := '&Encerrar pallet';
    MenuItem.OnClick := EncerrarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'RemoverPallet' + IdTXT;
    MenuItem.Caption := '&Remover pallet';
    MenuItem.OnClick := RemoverPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'MensagemDeDados' + IdTXT;
    MenuItem.Caption := '&Mensagem de dados';
    MenuItem.OnClick := Mensagemdedados1Click;
    FPMItens[I].Items.Add(MenuItem);
  end;
  //
  if (Screen.Width < 1920) or (Screen.Height < 1080) then
  begin
    PnAll.Width := ObtemWidth(320);
    PnAll.Font.Size := GetFontHeight(-17(*13*));
    //
    PnExtras.Width := ObtemWidth(233);
    PnExtras.Font.Size := GetFontHeight(-17(*13*));

    PnEqualize.Height := ObtemHeigth(PnEqualize.Height);
    PnCfgDesnate.Height := ObtemHeigth(PnCfgDesnate.Height);

    DBGNotaEqz.Font.Size := GetFontHeight(-11(*8*));
    DBGNotaEqz.TitleFont.Size := GetFontHeight(-11(*8*));
    TStringGrid(DBGNotaEqz).DefaultRowHeight := ObtemHeigth(24);
    for I := 0 to TStringGrid(DBGNotaEqz).ColCount - 1 do
      TStringGrid(DBGNotaEqz).ColWidths[I] :=
      ObtemWidth(TStringGrid(DBGNotaEqz).ColWidths[I]);
    DBEdNotaEqzM2.Font.Height := GetFontHeight(-32(*24*));
    //
    DBGDesnate.Font.Size := GetFontHeight(-11(*8*));
    DBGDesnate.TitleFont.Size := GetFontHeight(-11(*8*));
    TStringGrid(DBGDesnate).DefaultRowHeight := ObtemHeigth(24);
    for I := 0 to TStringGrid(DBGDesnate).ColCount - 1 do
      TStringGrid(DBGDesnate).ColWidths[I] :=
      ObtemWidth(TStringGrid(DBGDesnate).ColWidths[I]);
    //
    DBGItensACP.Font.Size := GetFontHeight(-11(*8*));
    DBGItensACP.TitleFont.Size := GetFontHeight(-11(*8*));
    TStringGrid(DBGItensACP).DefaultRowHeight := ObtemHeigth(24);
    for I := 0 to TStringGrid(DBGItensACP).ColCount - 1 do
      TStringGrid(DBGItensACP).ColWidths[I] :=
      ObtemWidth(TStringGrid(DBGItensACP).ColWidths[I]);
    //
    PnInfoBig.Font.Height := GetFontHeight(-17(*13*));
    PnInfoBig.Height := ObtemHeigth(97);
    //PnNaoClass.Height := ObtemHeigth(97);
    PnIntMei.Width := ObtemWidth(120);
    //

    for I := 0 to SGAll.ColCount - 1 do
      SGAll.ColWidths[I] := ObtemWidth(SGAll.ColWidths[I]);
    SGAll.DefaultRowHeight := ObtemHeigth(SGAll.DefaultRowHeight);
    SGAll.Font.Height := GetFontHeight(-17(*13*));
    //



    PnNaoClass.Width := ObtemWidth(320);
    LaFaltaClasTit.Top    := ObtemHeigth(LaFaltaClasTit.Top);
    LaFaltaClasTit.Left   := ObtemWidth(LaFaltaClasTit.Left);
    LaFaltaClasTit.Width  := ObtemWidth(LaFaltaClasTit.Width);
    LaFaltaClasTit.Height := ObtemHeigth(LaFaltaClasTit.Height);
    LaFaltaClasTit.Font.Height := GetFontHeight(-15(*11*));

    LaEdSdoVrtPeca.Top    := ObtemHeigth(LaEdSdoVrtPeca.Top);
    LaEdSdoVrtPeca.Left   := ObtemWidth(LaEdSdoVrtPeca.Left);
    LaEdSdoVrtPeca.Width  := ObtemWidth(LaEdSdoVrtPeca.Width);
    LaEdSdoVrtPeca.Height := ObtemHeigth(LaEdSdoVrtPeca.Height);
    LaEdSdoVrtPeca.Font.Height := GetFontHeight(-15(*11*));

    LaEdSdoVrtArM2.Top    := ObtemHeigth(LaEdSdoVrtArM2.Top);
    LaEdSdoVrtArM2.Left   := ObtemWidth(LaEdSdoVrtArM2.Left);
    LaEdSdoVrtArM2.Width  := ObtemWidth(LaEdSdoVrtArM2.Width);
    LaEdSdoVrtArM2.Height := ObtemHeigth(LaEdSdoVrtArM2.Height);
    LaEdSdoVrtArM2.Font.Height := GetFontHeight(-15(*11*));

    DBEdSdoVrtPeca.Top    := ObtemHeigth(DBEdSdoVrtPeca.Top);
    DBEdSdoVrtPeca.Left   := ObtemWidth(DBEdSdoVrtPeca.Left);
    DBEdSdoVrtPeca.Width  := ObtemWidth(DBEdSdoVrtPeca.Width);
    DBEdSdoVrtPeca.Height := ObtemHeigth(DBEdSdoVrtPeca.Height);
    DBEdSdoVrtPeca.Font.Height := GetFontHeight(-37(*28*));

    DBEdSdoVrtArM2.Top    := ObtemHeigth(DBEdSdoVrtArM2.Top);
    DBEdSdoVrtArM2.Left   := ObtemWidth(DBEdSdoVrtArM2.Left);
    DBEdSdoVrtArM2.Width  := ObtemWidth(DBEdSdoVrtArM2.Width);
    DBEdSdoVrtArM2.Height := ObtemHeigth(DBEdSdoVrtArM2.Height);
    DBEdSdoVrtArM2.Font.Height := GetFontHeight(-37(*28*));

    PnJaClass.Width := ObtemWidth(320);
    LaJahClasTit.Top    := ObtemHeigth(LaJahClasTit.Top);
    LaJahClasTit.Left   := ObtemWidth(LaJahClasTit.Left);
    LaJahClasTit.Width  := ObtemWidth(LaJahClasTit.Width);
    LaJahClasTit.Height := ObtemHeigth(LaJahClasTit.Height);
    LaJahClasTit.Font.Height := GetFontHeight(-15(*11*));

    LaEdJaFoi_PECA.Top    := ObtemHeigth(LaEdJaFoi_PECA.Top);
    LaEdJaFoi_PECA.Left   := ObtemWidth(LaEdJaFoi_PECA.Left);
    LaEdJaFoi_PECA.Width  := ObtemWidth(LaEdJaFoi_PECA.Width);
    LaEdJaFoi_PECA.Height := ObtemHeigth(LaEdJaFoi_PECA.Height);
    LaEdJaFoi_PECA.Font.Height := GetFontHeight(-15(*11*));

    LaEdJaFoi_AREA.Top    := ObtemHeigth(LaEdJaFoi_AREA.Top);
    LaEdJaFoi_AREA.Left   := ObtemWidth(LaEdJaFoi_AREA.Left);
    LaEdJaFoi_AREA.Width  := ObtemWidth(LaEdJaFoi_AREA.Width);
    LaEdJaFoi_AREA.Height := ObtemHeigth(LaEdJaFoi_AREA.Height);
    LaEdJaFoi_AREA.Font.Height := GetFontHeight(-15(*11*));

    DBEdJaFoi_PECA.Top    := ObtemHeigth(DBEdJaFoi_PECA.Top);
    DBEdJaFoi_PECA.Left   := ObtemWidth(DBEdJaFoi_PECA.Left);
    DBEdJaFoi_PECA.Width  := ObtemWidth(DBEdJaFoi_PECA.Width);
    DBEdJaFoi_PECA.Height := ObtemHeigth(DBEdJaFoi_PECA.Height);
    DBEdJaFoi_PECA.Font.Height := GetFontHeight(-37(*28*));

    DBEdJaFoi_AREA.Top    := ObtemHeigth(DBEdJaFoi_AREA.Top);
    DBEdJaFoi_AREA.Left   := ObtemWidth(DBEdJaFoi_AREA.Left);
    DBEdJaFoi_AREA.Width  := ObtemWidth(DBEdJaFoi_AREA.Width);
    DBEdJaFoi_AREA.Height := ObtemHeigth(DBEdJaFoi_AREA.Height);
    DBEdJaFoi_AREA.Font.Height := GetFontHeight(-37(*28*));



    //
    //PnJaClass.Visible := False;
    //
    PnMenu.Width := ObtemWidth(663);
    BtEncerra.Top    := ObtemHeigth(BtEncerra.Top);
    BtEncerra.Left   := ObtemWidth(BtEncerra.Left);
    BtEncerra.Width  := ObtemWidth(BtEncerra.Width);
    BtEncerra.Height := ObtemHeigth(BtEncerra.Height);
    BtImprime.Top    := ObtemHeigth(BtImprime.Top);
    BtImprime.Left   := ObtemWidth(BtImprime.Left);
    BtImprime.Width  := ObtemWidth(BtImprime.Width);
    BtImprime.Height := ObtemHeigth(BtImprime.Height);
    BtReabre.Top    := ObtemHeigth(BtReabre.Top);
    BtReabre.Left   := ObtemWidth(BtReabre.Left);
    BtReabre.Width  := ObtemWidth(BtReabre.Width);
    BtReabre.Height := ObtemHeigth(BtReabre.Height);
    CkMartelo.Top    := ObtemHeigth(CkMartelo.Top);
    CkMartelo.Left   := ObtemWidth(CkMartelo.Left);
    CkMartelo.Width  := ObtemWidth(CkMartelo.Width);
    CkMartelo.Height := ObtemHeigth(CkMartelo.Height);
    CkMartelo.Font.Height := GetFontHeight(-17(*13*));
    CkSubClass.Top    := ObtemHeigth(CkSubClass.Top);
    CkSubClass.Left   := ObtemWidth(CkSubClass.Left);
    CkSubClass.Width  := ObtemWidth(CkSubClass.Width);
    CkSubClass.Height := ObtemHeigth(CkSubClass.Height);
    CkSubClass.Font.Height := GetFontHeight(-17(*13*));

    PnDigitacao.Width := ObtemWidth(599);
    PnArea.Width := ObtemWidth(196); // 140
    LaInverte.Top    := ObtemHeigth(LaInverte.Top);
    LaInverte.Left   := ObtemWidth(LaInverte.Left);
    LaInverte.Width  := ObtemWidth(LaInverte.Width);
    LaInverte.Height := ObtemHeigth(LaInverte.Height);
    LaInverte.Font.Height := GetFontHeight(-17(*13*));
    LaArea.Top    := ObtemHeigth(LaArea.Top);
    LaArea.Left   := ObtemWidth(LaArea.Left);
    LaArea.Width  := ObtemWidth(LaArea.Width);
    LaArea.Height := ObtemHeigth(LaArea.Height);
    LaArea.Font.Height := GetFontHeight(-17(*13*));
    EdArea.Top    := ObtemHeigth(EdArea.Top);
    EdArea.Left   := ObtemWidth(EdArea.Left);
    EdArea.Width  := ObtemWidth(EdArea.Width);
    EdArea.Height := ObtemHeigth(EdArea.Height);
    EdArea.Font.Height := GetFontHeight(-37(*28*));
    LaTipoArea.Top    := ObtemHeigth(LaTipoArea.Top);
    LaTipoArea.Left   := ObtemWidth(LaTipoArea.Left);
    LaTipoArea.Width  := ObtemWidth(LaTipoArea.Width);
    LaTipoArea.Height := ObtemHeigth(LaTipoArea.Height);
    LaTipoArea.Font.Height := GetFontHeight(-17(*13*));

    PnBox.Width := ObtemWidth(75);
    LaBox.Top    := ObtemHeigth(LaBox.Top);
    LaBox.Left   := ObtemWidth(LaBox.Left);
    LaBox.Width  := ObtemWidth(LaBox.Width);
    LaBox.Height := ObtemHeigth(LaBox.Height);
    LaBox.Font.Height := GetFontHeight(-17(*13*));
    EdBox.Top    := ObtemHeigth(EdBox.Top);
    EdBox.Left   := ObtemWidth(EdBox.Left);
    EdBox.Width  := ObtemWidth(EdBox.Width);
    EdBox.Height := ObtemHeigth(EdBox.Height);
    EdBox.Font.Height := GetFontHeight(-37(*28*));

    PnSubClass.Width := ObtemWidth(83);
    LaSubClass.Top    := ObtemHeigth(LaSubClass.Top);
    LaSubClass.Left   := ObtemWidth(LaSubClass.Left);
    LaSubClass.Width  := ObtemWidth(LaSubClass.Width);
    LaSubClass.Height := ObtemHeigth(LaSubClass.Height);
    LaSubClass.Font.Height := GetFontHeight(-17(*13*));
    EdSubClass.Top    := ObtemHeigth(EdSubClass.Top);
    EdSubClass.Left   := ObtemWidth(EdSubClass.Left);
    EdSubClass.Width  := ObtemWidth(EdSubClass.Width);
    EdSubClass.Height := ObtemHeigth(EdSubClass.Height);
    EdSubClass.Font.Height := GetFontHeight(-37(*28*));

    PnMartelo.Width := ObtemWidth(243);
    LaVSMrtCad.Top    := ObtemHeigth(LaVSMrtCad.Top);
    LaVSMrtCad.Left   := ObtemWidth(LaVSMrtCad.Left);
    LaVSMrtCad.Width  := ObtemWidth(LaVSMrtCad.Width);
    LaVSMrtCad.Height := ObtemHeigth(LaVSMrtCad.Height);
    LaVSMrtCad.Font.Height := GetFontHeight(-17(*13*));
    EdVSMrtCad.Top    := ObtemHeigth(EdVSMrtCad.Top);
    EdVSMrtCad.Left   := ObtemWidth(EdVSMrtCad.Left);
    EdVSMrtCad.Width  := ObtemWidth(EdVSMrtCad.Width);
    EdVSMrtCad.Height := ObtemHeigth(EdVSMrtCad.Height);
    EdVSMrtCad.Font.Height := GetFontHeight(-37(*28*));
    CBVSMrtCad.Top    := ObtemHeigth(CBVSMrtCad.Top);
    CBVSMrtCad.Left   := ObtemWidth(CBVSMrtCad.Left);
    CBVSMrtCad.Width  := ObtemWidth(CBVSMrtCad.Width);
    CBVSMrtCad.Height := ObtemHeigth(CBVSMrtCad.Height);
    CBVSMrtCad.Font.Height := GetFontHeight(-37(*28*));

    //
    LaTipoArea.Font.Height := GetFontHeight(-32(*24*));
    //
    PanelOC.Height := ObtemHeigth(105, OCHeightFator);
    PanelOC.Font.Height := GetFontHeight(-11(*8*));

    PnOutros.Width := ObtemWidth(232, FIniWidth);
    PnOutros.Font.Height := GetFontHeight(-11(*8*));

    //PnInfoOC.Width := ObtemWidth(746);
    PnInfoOC.Font.Height := GetFontHeight(-11(*8*));

    PnResponsaveis.Width := ObtemWidth(295, FIniWidth);
    PnResponsaveis.Font.Height := GetFontHeight(-11(*8*));

    //PnCfgEqz.Width := ObtemWidth(227);
    PnCfgEqz.Font.Height := GetFontHeight(-11(*8*));
    PnCfgEqz.Height := ObtemHeigth(PnCfgEqz.Height);

    PnInfoOC.Font.Height := GetFontHeight(-11(*8*));

    for I := 0 to Self.ComponentCount - 1 do
    begin
      OK := False;
      (*
      if Components[I] is TLabel then
      begin
        //if TLabel(Self.Components[I]).FocusControl <> nil then
          OK :=
            (TLabel(Self.Components[I]).Parent = PnCfgEqz)
          or
            (TLabel(Self.Components[I]).Parent = PnCfgDesnate);
      end;// else
      *)
      if Self.Components[I] is TControl then
      begin
        if (TControl(Self.Components[I]).Parent = PnCfgEqz)  then
          OK := True;
        if (TControl(Self.Components[I]).Parent = PnCfgDesnate) then
        begin
          OK := True;
          //Geral.MB_Teste(TControl(Self.Components[I]).Name);
        end;
      end;
      if OK then
      begin
        // para panels de equalize e desnate...
        // n�o compensar largura.
        // ver aqui!
        if Self.Components[I] is TLabel then
        begin
(**)
          TLabel(Self.Components[I]).Font.Height := GetFontHeight(-11);//TLabel(Self.Components[I]).Font.Height);
          TLabel(Self.Components[I]).Top := ObtemWidth(8);//TLabel(Self.Components[I]).Top);
          TLabel(Self.Components[I]).Left := ObtemWidth(4); //TLabel(Self.Components[I]).Left);
          TLabel(Self.Components[I]).Width := ObtemWidth(44); //TLabel(Self.Components[I]).Width);
          TLabel(Self.Components[I]).Height := ObtemHeigth(13); //TLabel(Self.Components[I]).Height);
(**)    end else
        begin
          (*
          TWinControl(Self.Components[I]).Top := ObtemHeigth(TWinControl(Self.Components[I]).Top);
          TWinControl(Self.Components[I]).Left := ObtemWidth(TWinControl(Self.Components[I]).Left);
          TWinControl(Self.Components[I]).Width := ObtemWidth(TWinControl(Self.Components[I]).Width);
          TWinControl(Self.Components[I]).Height := ObtemHeigth(TWinControl(Self.Components[I]).Height);
          *)
          if Self.Components[I] is TdmkEdit then
          begin
            TdmkEdit(Self.Components[I]).Font.Height := GetFontHeight(-11);//TdmkEdit(Self.Components[I]).Font.Height);
            TdmkEdit(Self.Components[I]).Top         := ObtemHeigth(4); // TdmkEdit(Self.Components[I]).Top);
            TdmkEdit(Self.Components[I]).Left        := ObtemWidth(52); // TdmkEdit(Self.Components[I]).Left);
            TdmkEdit(Self.Components[I]).Width       := ObtemWidth(80); // TdmkEdit(Self.Components[I]).Width);
            TdmkEdit(Self.Components[I]).Height      := ObtemHeigth(21); // (TdmkEdit(Self.Components[I]).Height);
          end;
          if Self.Components[I] is TSpeedButton then
          begin
            TSpeedButton(Self.Components[I]).Font.Height := GetFontHeight(-11);//TSpeedButton(Self.Components[I]).Font.Height);
            TSpeedButton(Self.Components[I]).Top         := ObtemHeigth(3); // TSpeedButton(Self.Components[I]).Top);
            TSpeedButton(Self.Components[I]).Left        := ObtemWidth(136); // TSpeedButton(Self.Components[I]).Left);
            TSpeedButton(Self.Components[I]).Width       := ObtemWidth(23); // TSpeedButton(Self.Components[I]).Width);
            TSpeedButton(Self.Components[I]).Height      := ObtemHeigth(23); // (TSpeedButton(Self.Components[I]).Height);
          end;
          if Self.Components[I] is TCheckBox then
          begin
            TCheckBox(Self.Components[I]).Font.Height := GetFontHeight(-11); //TCheckBox(Self.Components[I]).Font.Height);
            TCheckBox(Self.Components[I]).Top         := ObtemHeigth(TCheckBox(Self.Components[I]).Top); // TCheckBox(Self.Components[I]).Top);
            TCheckBox(Self.Components[I]).Left        := ObtemWidth(164); // TCheckBox(Self.Components[I]).Left);
            TCheckBox(Self.Components[I]).Width       := ObtemWidth(58); // TCheckBox(Self.Components[I]).Width);
            TCheckBox(Self.Components[I]).Height      := ObtemHeigth(17); // (TCheckBox(Self.Components[I]).Height);
          end;
           (* *)
        end;
      end;

      (**)if OK = False then
      begin
        (*
        if Components[I] is TLabel then
        begin
          if TLabel(Components[I]).FocusControl <> nil then
            OK :=
              (TControl(TLabel(Components[I]).FocusControl).Parent = PnInfoOC)
            or
              (TControl(TLabel(Components[I]).FocusControl).Parent = PnResponsaveis)
            or
              (TControl(TLabel(Components[I]).FocusControl).Parent = PnOutros);
        end else
        if Components[I] is TWinControl then
        *)
        if Self.Components[I] is TControl then
        begin
          OK :=
            (TControl(Components[I]).Parent = PnInfoOC)
          or
            (TControl(Components[I]).Parent = PnResponsaveis)
          or
            (TControl(Components[I]).Parent = PnOutros);
        end;
        if OK then
        begin
          if Components[I] is TLabel then
          begin
            TLabel(Components[I]).Font.Height := GetFontHeight(TLabel(Components[I]).Font.Height, FIniWidth); //OCHeightFator);
            TLabel(Components[I]).Top := ObtemWidth(TLabel(Components[I]).Top, FIniWidth); //OCHeightFator); // FIniWidth); //OCHeightFator);
            TLabel(Components[I]).Left := ObtemWidth(TLabel(Components[I]).Left, FIniWidth);
            TLabel(Components[I]).Width := ObtemWidth(TLabel(Components[I]).Width, FIniWidth);
            TLabel(Components[I]).Height := ObtemHeigth(TLabel(Components[I]).Height, OCHeightFator); // FIniWidth); //OCHeightFator);
(*
            TLabel(Components[I]).Font.Height := GetFontHeight(TLabel(Components[I]).Font.Height);
            TLabel(Components[I]).Top := ObtemWidth(TLabel(Components[I]).Top);
            TLabel(Components[I]).Left := ObtemWidth(TLabel(Components[I]).Left);
            TLabel(Components[I]).Width := ObtemWidth(TLabel(Components[I]).Width);
            TLabel(Components[I]).Height := ObtemHeigth(TLabel(Components[I]).Height);
*)
          end else
          begin
            TWinControl(Components[I]).Top := ObtemHeigth(TWinControl(Components[I]).Top, OCHeightFator); // FIniWidth); //OCHeightFator);
            TWinControl(Components[I]).Left := ObtemWidth(TWinControl(Components[I]).Left, FIniWidth);
            TWinControl(Components[I]).Width := ObtemWidth(TWinControl(Components[I]).Width, FIniWidth);
            TWinControl(Components[I]).Height := ObtemHeigth(TWinControl(Components[I]).Height, OCHeightFator); // FIniWidth); //OCHeightFator);
            //
            if Components[I] is TRadioGroup then
              TRadioGroup(Components[I]).Font.Height := GetFontHeight(TRadioGroup(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TdmkEdit then
              TdmkEdit(Components[I]).Font.Height := GetFontHeight(TdmkEdit(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TdmkDBLookupComboBox then
              TdmkDBLookupComboBox(Components[I]).Font.Height := GetFontHeight(TdmkDBLookupComboBox(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TBitBtn then
              TBitBtn(Components[I]).Font.Height := GetFontHeight(TBitBtn(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TdmkDBEdit then
              TdmkDBEdit(Components[I]).Font.Height := GetFontHeight(TdmkDBEdit(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TdmkEditCB then
              TdmkEditCB(Components[I]).Font.Height := GetFontHeight(TdmkEditCB(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TDBEdit then
              TDBEdit(Components[I]).Font.Height := GetFontHeight(TDBEdit(Components[I]).Font.Height, FIniWidth);
            if Components[I] is TEdit then
              TEdit(Components[I]).Font.Height := GetFontHeight(TEdit(Components[I]).Font.Height, FIniWidth);
           end;
        end;
      end;(**)
    end;
    //
    EdRevisor.Font.Size := CBRevisor.Font.Size;
    EdDigitador.Font.Size := CBDigitador.Font.Size;
  end;
  //
end;

procedure TFmVSReclassifOneNw3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if Key = VK_MULTIPLY then
  if Key = VK_F12 then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
    OcultaMostraClasses();
  end;
end;

procedure TFmVSReclassifOneNw3.FormResize(Sender: TObject);
begin
  //RealinhaBoxes();
end;

procedure TFmVSReclassifOneNw3.ImprimeFluxoDeMovimentoDoPallet();
begin
  {$IfDef sAllVS}
  VS_PF.MostraRelatoroFluxoPallet(QrVSPallet0Codigo.Value,
    QrVSGerArtNewEmpresa.Value, QrVSPaRclCabNO_EMPRESA.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSReclassifOneNw3.ImprimirfluxodemovimentodoPallet1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoPallet();
end;

procedure TFmVSReclassifOneNw3.ImprimirfluxodemovimentodoPallet2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoPallet();
end;

procedure TFmVSReclassifOneNw3.InsereCouroArrAll(CacCod, CacID, Codigo,
  ClaAPalOri, RclAPalOri, VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix,
  VMI_Dest, Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
  Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
  FatorIntDst: Double; Controle: Int64);
var
  I: Integer;
  a, b, t: Cardinal;
begin
  EdAll.Text := '';
  EdArrAll.Text := '';
  EdItens.Text := '';
  a := GetTickCount();
  //
  I := Length(FLsAll);
  SetLength(FLsAll, I + 1);
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaRclIts);                   // VSPaRclIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  // ini 2023-12-26
  //FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 12] := IntToStr(VMI_Baix);                     // VMI_Baix
  // fim 2023-12-26
  //
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdArrAll.Text := FloatToStr(t / 1000) + 's';

  //////////////////////////////////////////////////////////////////////////////

  a := GetTickCount();
  //
  I := Length(FLsItens[Box]);
  SetLength(FLsItens[Box], I + 1);
  FLsItens[Box][I, 00] := IntToStr(I + 1);
  FLsItens[Box][I, 01] := IntToStr(Controle);                     // Conrole
  FLsItens[Box][I, 02] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsItens[Box][I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsItens[Box][I, 04] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  //
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSReclassifOneNw3.InsereCouroAtual();
const
  VSPaClaIts = 0;
  Pecas = 1;
var
  Box, Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  CacCod, CacID, ClaAPalOri, RclAPalOri, Revisor, Digitador, VSCacItsAOri,
  FrmaIns: Integer;
  PecasSrc, PecasDst, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
  QrVSPallet: TmySQLQuery;
  DataHora, SubClass: String;
  Controle: Int64;
  Tempo: TDateTime;
  ExigeSubClass: Boolean;
  //
  Ta, Tb, Tt: Cardinal;
  PecasSrc_TXT, PecasDst_TXT, AreaM2_TXT, AreaP2_TXT, ValorT_TXT: String;
  Area: Double;
  Continua: Boolean;
  Fator, Martelo: Integer;
begin
  Ta := GetTickCount();
  //
  //Geral.MB_Aviso('Fazer classificacao');
  // ini 2023-04-13
  (*
  if EdArea.ValueVariant < 0.01 then
  begin
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Enabled and EdArea.CanFocus then
    begin
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    Exit;
  end;
  *)

  // ini 2023-12-14
  if PnMartelo.Visible then
  begin
    if CBVSMrtCad.KeyValue = QrVSMrtCadNome.Value then
      Martelo := QrVSMrtCadCodigo.Value
    else
      Martelo := 0;
  end else
    Martelo := 0;
  //
  // fim 2023-12-14

  Box := EdBox.ValueVariant;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    if FCkDuplicaArea[Box].Checked then
      Fator := 2
    else
      Fator := 1;
    case QrVSPaRclCabTipoArea.Value of
      0: Area := EdArea.ValueVariant / 100 * Fator;
      1: Area := EdArea.ValueVariant / 100 * 10.7639104167097 * Fator;
      else Area := 0;
    end;

    Continua := Area >= 0.01;
    if Continua then
    begin
      Continua :=
        //(Area <= QrVSGerArtNewMediaMaxM2.Value)
        (Area <= FQrVSPAllet[Box].FieldByName('MediaMaxM2').AsFloat)
        and
        //(Area >= QrVSGerArtNewMediaMinM2.Value);
        (Area >= FQrVSPAllet[Box].FieldByName('MediaMinM2').AsFloat);
      if Continua = False then
      begin
        MyObjects.Senha(TFmSenha, FmSenha, 9, '', '', '', '', '', False, '', 0, '',
        '�rea informada fora do permitido!');
        Continua := VAR_SENHARESULT = 2;
      end;
    end;
    if Continua = False then
    begin
      if PnArea.Enabled and PnDigitacao.Enabled and
      EdArea.Enabled and EdArea.CanFocus then
      begin
        EdArea.SetFocus;
        EdArea.SelectAll;
      end;
      Exit;
    end;
    Continua := (QrSumTSdoVrtPeca.Value > 0) or (FLiberadoPecasNegativas = True);
    if Continua = False then
    begin
      MyObjects.Senha(TFmSenha, FmSenha, 9, '', '', '', '', '', False, '', 0, '',
      'Saldo de pe�as insuficiente!');
      Continua := VAR_SENHARESULT = 2;
      if Continua then
        FLiberadoPecasNegativas := True;
    end;
    if Continua = False then
      Exit;

    // fim 2023-04-13
      CacCod         := FCacCod;
    CacID          := Integer(emidReclasXXUni);
    Codigo         := FCodigo;
    Controle       := 0;
    ObtemDadosCouroOrigem(ClaAPalOri, RclAPalOri, VMI_Sorc, VSCacItsAOri);
    //VSPaRclIts     := ;   ver abaixo!!!
    //VSPallet       := ;   ver abaixo!!!
    //VMI_Sorc       := ;   ver abaixo!!!
    //VMI_Dest       := ;   ver abaixo!!!
    Box            := EdBox.ValueVariant;
    Revisor        := EdRevisor.ValueVariant;
    Digitador      := EdDigitador.ValueVariant;
    DataHora       := Geral.FDT(Now() + FDifTime, 109);
    FrmaIns        := RGFrmaIns.ItemIndex;
    SubClass       := dmkPF.SoTextoLayout(EdSubClass.Text);
    //AreaM2         := ;
    //AreaP2         := ;
    //Pecas          := 1;
    if QrVSPaRclCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100 * Fator;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100 * Fator;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet) then
      Exit;
    if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
    ExigeSubClass) then
      Exit;
    if MyObjects.FIC(ExigeSubClass and (Trim(EdSubClass.Text) = ''), nil,
    'Informe a sub classe!') then
      Exit;
    //
    FatorIntSrc    := QrVSPallet0FatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
    //
    //PecasSrc := FatorIntSrc / FatorIntDst;
    PecasSrc := Pecas / FatorIntSrc * FatorIntDst;
    PecasDst := Pecas;
    //VMI_Dest :=
    Controle := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'Martelo', CO_DATA_HORA_GRL(*, 'Sumido',
    'RclAPalDst'*), 'FrmaIns', 'SubClass',
    'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix,
    VMI_Dest, Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, Martelo, DataHora(*, Sumido,
    RclAPalDst*), FrmaIns, SubClass,
    FatorIntSrc, FatorIntDst], [
    Controle], True) then
    begin
      EdSubClass.Text := '';
      EdVSMrtCad.Text     := '';
      Tempo := Now();
        InsereCouroArrAll(CacCod, CacID, Codigo,
        ClaAPalOri, RclAPalOri, VSPaClaIts,
        VSPaRclIts, VSPallet,
        VMI_Sorc, VMI_Baix, VMI_Dest,
        Box, Pecas,
        AreaM2, AreaP2, Revisor,
        Digitador, Martelo, DataHora,
        SubClass, FatorIntSrc, FatorIntDst,
        Controle);
        //
        PecasSrc_TXT  := Geral.FFT_Dot(PecasSrc, 3, siNegativo);
        PecasDst_TXT  := Geral.FFT_Dot(PecasDst, 3, siNegativo);
        //
        AreaM2_TXT := Geral.FFT_Dot(AreaM2, 2, siNegativo);
        AreaP2_TXT := Geral.FFT_Dot(AreaP2, 2, siNegativo);
        ValorT_TXT := Geral.FFT_Dot(QrVSGerArtNewCUSTO_M2.Value * AreaM2, 2, siNegativo);
        //
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca-' + PecasSrc_TXT +
        ', SdoVrtArM2=SdoVrtArM2-' + AreaM2_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Sorc) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET Pecas=Pecas-' + PecasSrc_TXT +
        ', AreaM2=AreaM2-' + AreaM2_TXT +
        ', AreaP2=AreaP2-' + AreaP2_TXT +
        ', ValorT=ValorT-' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Baix) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca+' + PecasDst_TXT +
        ', SdoVrtArM2=SdoVrtArM2+' + AreaM2_TXT +
        ', Pecas=Pecas+' + PecasDst_TXT +
        ', AreaM2=AreaM2+' + AreaM2_TXT +
        ', AreaP2=AreaP2+' + AreaP2_TXT +
        ', ValorT=ValorT+' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Dest) +
        '');
        //
        CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
(*
      VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
        QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
*)
      Tempo := Now() - Tempo;
      EdTempo.Text := FormatDateTime('ss:zzz', Tempo);
      //
      AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, Controle);
      ReopenItensAClassificarPallet();
      //
      EdBox.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
        EdArea.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
      begin
        EdArea.SetFocus;
        EdArea.SelectAll;
      end else
      begin
        EdBox.SetFocus;
        EdBox.SelectAll;
      end;
      //
      ReopenItens(Box, VSPaRclIts, VSPallet);
      AtualizaInfoOC();
      //
      Tb := GetTickCount();
      Tt := Tb - Ta;
      EdTempo.Text := FloatToStr(Tt / 1000) + 's';
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSReclassifOneNw3.LaInverteClick(Sender: TObject);
var
  AreaA, AreaB: Double;
begin
{
 if not FInverte then
  begin
    //AddMemoExe('EdAreaKeyDown Inverte inicio');
    if (Key = Ord('i'))
    or (Key = Ord('I'))
    then
    begin
      FInverte := True;
      //Geral.MB_Info('Key Down: ' + Char(Key));
}
      AreaA := EdArea.ValueVariant / 100;
      case QrVSPaRclCabTipoArea.Value of
        0: AreaB := Geral.ConverteArea(AreaA, ctP2toM2, cfCento);
        1: AreaB := Geral.ConverteArea(AreaA, ctM2toP2, cfQuarto);
        else AreaB := 0;
      end;
      EdArea.ValueVariant := Int(AreaB * 100);
{
    end;
  //AddMemoExe('EdAreaKeyDown fim');
  end;
}
end;

procedure TFmVSReclassifOneNw3.LiberaDigitacao;
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  //
  if not FCriando and EdArea.CanFocus then
    EdArea.SetFocus;
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
  else
  if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
  EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
    EdBox.SetFocus
end;

procedure TFmVSReclassifOneNw3.LiberaDigitacaoManual(Libera: Boolean);
begin
  PnArea.Enabled := Libera;
end;

procedure TFmVSReclassifOneNw3.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  QrSum, QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
(* ver o que fazer!
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass);
  case Box of
    1:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      //EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      //EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    3:
    begin
      QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      //EdPercent := EdPercent3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      //EdPercent := EdPercent4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      //EdPercent := EdPercent5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      //EdPercent := EdPercent6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaRclCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O DO PALLET  ' + Geral.FF0(QrVSPaRclCabVSPallet.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
*)
end;

procedure TFmVSReclassifOneNw3.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSReclassifOneNw3.MostraJanelaDefeitos;
begin
  Application.CreateForm(TFmVSClassifOneDefei, FmVSClassifOneDefei);
  FmVSClassifOneDefei.FSGDefeiDefin := SGDefeiDefin;
  if Trim(SGDefeiDefin.Cells[0,1]) = '' then
    FmVSClassifOneDefei.FSeqDefIni := 1
  else
    FmVSClassifOneDefei.FSeqDefIni := SGDefeiDefin.RowCount;
  FmVSClassifOneDefei.ShowModal;
  FmVSClassifOneDefei.Destroy;
end;

procedure TFmVSReclassifOneNw3.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSReclassifOneNw3.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  Result := (FCkSubClass[Box] <> nil) and (FCkSubClass[Box].Checked);
  // ini 2023-04-13
  //if Result and (Trim(EdSubClass.Text) <> '') then
  //  Result := False;
  if Result then
    if Trim(EdSubClass.Text) <> '' then
      Result := False;
  // fim 2023-04-13
end;

function TFmVSReclassifOneNw3.ObtemDadosBox(const Box: Integer; var
VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
Boolean): Boolean;
begin
  Result := False;
  if (FQrVSPallet[Box] <> nil) and (FQrVSPallet[Box].State <> dsInactive) then
  begin
    VMI_Sorc      := QrVSGerArtNewControle.Value;
    VSPallet      := FQrVSPallet[Box].FieldByName('Codigo').AsInteger;
    VSPaRclIts    := FQrVSPalRclIts[Box].FieldByName('Controle').AsInteger;
    VMI_Baix      := FQrVSPalRclIts[Box].FieldByName('VMI_Baix').AsInteger;
    VMI_Dest      := FQrVSPalRclIts[Box].FieldByName('VMI_Dest').AsInteger;
    ExigeSubClass := FCkSubClass[Box].Checked;
    //
    Result        := True;
  end;
end;

function TFmVSReclassifOneNw3.ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri,
  VMI_Sorc, VSCacItsAOri: Integer): Boolean;
begin
  Result         := False;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  VMI_Sorc       := 0;
  VSCacItsAOri   := 0;

  if RGFrmaIns.ItemIndex < 3 then
  begin
    if (QrItensACP.State = dsInactive)
    or (QrItensACP.RecordCount = 0) then
      Exit;
  end;
  //?
  //VMI_Sorc := QrItensACPVMI_Dest.Value;
  VMI_Sorc := QrVSPaRclCabVSMovIts.Value;
  case RGFrmaIns.ItemIndex of
    0,
    1,
    2:
    begin
      Result := True;
      case TEstqMovimID(QrItensACPCacID.Value) of
        (*7*)emidClassArtXXUni: ClaAPalOri := QrItensACPControle.Value;
        (*8*)emidReclasXXUni:   RclAPalOri := QrItensACPControle.Value;
        else
        begin
          Result := False;
          Geral.MB_Erro(
          '"TEstqMovimID" n�o definido em "ObtemDadosCouroOrigem()"');
        end;
      end;
    end;
    3:
    begin
      Result := True;
    end;
    else Geral.MB_Aviso(
      'Forma de reclassifica��o n�o definida em "ObtemDadosCouroOrigem()"');
  end;
end;

function TFmVSReclassifOneNw3.ObtemNO_MARTELO(Martelo: Integer): String;
var
  I, N: Integer;
begin
  Result := '' ;
  if Martelo <> 0 then
  begin
    Result := '?????' ;
    for I := 0 to Length(FMartelosCod) - 1 do
    begin
      if Martelo = FMartelosCod[I] then
      begin
        Result := FMartelosNom[I];
        Exit;
      end;
    end;
  end;
end;

function TFmVSReclassifOneNw3.ObtemQryesBox(const Box: Integer; var QrVSPallet:
  TmySQLQuery): Boolean;
begin
  QrVSPallet := FQrVSPallet[Box];
  Result := True;
end;

procedure TFmVSReclassifOneNw3.OcultaMostraClasses();
var
  I: Integer;
begin
  FClasseVisivel := not FClasseVisivel;
  for I := 1 to FBoxMax do
  begin
    if FDBEdArtNome[I] <> nil then
      FDBEdArtNome[I].Visible  := FClasseVisivel;
  end;
end;

procedure TFmVSReclassifOneNw3.PMEncerraPopup(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
  begin
    FPalletdobox[I].Visible := True;
    FPalletdobox[I].Enabled :=
    (FQrVSPallet[I] <> nil) and
    (FQrVSPallet[I].State <> dsInactive) and
    (FQrVSPallet[I].RecordCount > 0);
  end;
  for I := FBoxMax + 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    FPalletdobox[I].Visible := False;
end;

procedure TFmVSReclassifOneNw3.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  //AdicionarPallet1.Enabled := (QrVSPallet1.State = dsInactive) or (QrVSPallet1.RecordCount = 0);
  //MyObjects.HabilitaMenuItemCabUpd(EncerrarPallet1, QrVSPallet1);
  //MyObjects.HabilitaMenuItemCabDel(RemoverPallet1, QrVSPallet1, QrItens1);
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSReclassifOneNw3.QrItensACPAfterOpen(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneNw3.QrItensACPAfterScroll(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneNw3.QrItensACPBeforeClose(DataSet: TDataSet);
begin
  if RGFrmaIns.ItemIndex < 3 then
    EdArea.ValueVariant := 0;
end;

procedure TFmVSReclassifOneNw3.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSReclassifOneNw3.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSReclassifOneNw3.QrVSPallet0AfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
    ReconfiguraPaineisIntMei(FQrVSPallet[I], FPnIntMei[I]);
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabAfterScroll(DataSet: TDataSet);
var
  I, LstPal, Controle, Codigo: Integer;
begin
  for I := 1 to FBoxMax do
  begin
    if QrVSPaRclCab.FieldByName(VS_CRC_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  for I := 1 to FBoxMax do
  begin
    if FQrVSPallet[I] <> nil then
    begin
      LstPal := QrVSPaRclCab.FieldByName('LstPal' + Geral.FFN(I, 2)).AsInteger;
      ReopenVSPallet(I, FQrVSPallet[I], FQrVSPalRclIts[I], LstPal);
      if (FQrVSPalRclIts[I].State <> dsInactive) and
      (FQrVSPallet[I].State <> dsInactive) then
      begin
        Controle := FQrVSPalRclIts[I].FieldByName('Controle').AsInteger;
        Codigo   := FQrVSPallet[I].FieldByName('Codigo').Value;
        ReopenItens(I, Controle, Codigo);
      end;
    end;
  end;
  //
  //if FUsaMemory then
    ReopenAll();
  AtualizaInfoOC();
  //
  //
  ReopenVSPallet(0, QrVSPallet0, nil, QrVSPaRclCabVSPallet.Value);
  ReopenItensAClassificarPallet();
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabBeforeClose(DataSet: TDataSet);
var
  I: Integer;
begin
  QrVSGerArtNew.Close;
  for I := 1 to FBoxMax do
  begin
    FPnBox[I].Visible := True;
    FQrVSPallet[I].Close;
    FQrVSPalRclIts[I].Close;
  end;
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrVSPaRclCabNO_TIPO.Value := 'm�';
    1: QrVSPaRclCabNO_TIPO.Value := 'ft�';
    else QrVSPaRclCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaRclCabNO_TIPO.Value;
end;

(*
procedure TFmVSReclassifOneNw3.RealinhaBoxes();
var
  H_T, W_T, H_1, W_1: Integer;
begin
  H_T := PnBoxesAll.Height;
  W_T := PnBoxesAll.Width;
  //
  H_1 := H_T div 2;
  W_1 := W_T div 3;
  //
  PnBoxesT01.Height := H_1;
  //PnBoxesT02.Height := H_1;

  PnBoxT1L1.Width := W_1;
  PnBoxT1L2.Width := W_1;
  PnBoxT1L3.Width := W_1;

  PnBoxT2L1.Width := W_1;
  PnBoxT2L2.Width := W_1;
  PnBoxT2L3.Width := W_1;
end;
*)

procedure TFmVSReclassifOneNw3.ReconfiguraPaineisIntMei(QrVsPallet: TmySQLQuery;
Panel: TPanel);
(*
var
  Habilita: Boolean;
begin
  Habilita := Qry.FieldByName('FatorInt').AsFloat < QrVSPallet0FatorInt.Value;
  if Habilita <> Panel.Visible then
    Panel.Visible := Habilita;
*)
var
  Habilita: Boolean;
begin
  if QrVSPallet <> nil then
  begin
    Habilita :=
      (QrVSPallet.State <> dsInactive) and
      (QrVSGerArtNew.State <> dsInactive) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat < QrVSPallet0FatorInt.Value);
    if (Panel <> nil) and (Habilita <> Panel.Visible) then
      Panel.Visible := Habilita;
  end;
end;

procedure TFmVSReclassifOneNw3.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo, DtHrFim: String;
  Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
  Campo := VS_CRC_PF.CampoLstPal(Box);
  Codigo := QrVSPaRclCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  if VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaRclIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_CRC_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaRclCab();
      end;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneNw3.ReopenAll();
var
  I, J: Integer;
  a, b, t: Cardinal;
begin
  a := GetTickCount();
  DqAll.Database := Dmod.MyDB;
  DqAll.SQL.Text :=
  ' SELECT cia.Controle, cia.VSPaRclIts, cia.VSPallet, cia.Pecas, ' + // 0 a 3
  ' cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ' +  // 4 a 8
    // ini 2023-12-26
  //' cia.SubClass, mrt.Nome NO_MARTELO ' +                             // 9 a 10
  ' cia.SubClass, mrt.Nome NO_MARTELO, cia.VMI_Baix ' +               // 9 a 11
    // fim 2023-12-26
  ' FROM vscacitsa cia ' +
  ' LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo' +
  ' WHERE cia.CacCod=' + Geral.FF0(FCacCod) +
  //' ORDER BY cia.Controle DESC ';
  ' ORDER BY cia.Controle ';
  //
  DqAll.Open();
  SetLength(FLsAll, DqAll.RecordCount);
  DqAll.First();
  while not DqAll.Eof do
  begin
    J := DqAll.RecNo;
    //
    FLsAll[J, 01] := DqAll.FieldValues[08];   // Box
    FLsAll[J, 02] := DqAll.FieldValues[09];   // SubClass
    FLsAll[J, 03] := DqAll.FieldValues[04];   // AreaM2
    FLsAll[J, 04] := DqAll.FieldValues[10];   // NO_MARTELO
    FLsAll[J, 05] := DqAll.FieldValues[07];   // VMI_Dest
    FLsAll[J, 06] := DqAll.FieldValues[00];   // Controle
    FLsAll[J, 07] := DqAll.FieldValues[01];   // VSPaRclIts
    FLsAll[J, 08] := DqAll.FieldValues[02];   // VSPallet
    FLsAll[J, 09] := DqAll.FieldValues[03];   // Pecas
    FLsAll[J, 10] := DqAll.FieldValues[05];   // AreaP2
    FLsAll[J, 11] := DqAll.FieldValues[06];   // VMI_Sorc
    // ini 2023-12-26
    //FLsAll[J, 12] := DqAll.FieldValues[07];   // VMI_Dest
    FLsAll[J, 12] := DqAll.FieldValues[11];   // VMI_Baix
    // fim 2023-12-26
    //
    DqAll.Next();
  end;
  DqAll.Close();
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdAll.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSReclassifOneNw3.Reopendesnate(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {$IfDef sAllVS}
  Codigo := EdDesnate.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do desnate!');
  end else
    VS_PF.ReopenDesnate(Codigo, GraGruX, QrVSCacIts, QrVSCacSum);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSReclassifOneNw3.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {$IfDef sAllVS}
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);

  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSReclassifOneNw3.ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaRclIts=' + Geral.FF0(VSPaRclIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  'ORDER BY Controle DESC ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'AND VSPaRclIts=' + Geral.FF0(VSPaRclIts),
  'AND VSPallet=' + Geral.FF0(VSPallet),
  '']);
  //
  ReopenSumPal(QrSumPal, VSPallet);
*)
var
  I, J: Integer;
  a, b, t: Cardinal;
  //
begin
  a := GetTickCount();
  DqItens.Database := Dmod.MyDB;
  DqItens.SQL.Text :=
  ' SELECT Controle, Pecas, AreaM2, AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE CacCod=' + Geral.FF0(FCacCod) +
  ' AND VSPaRclIts=' + Geral.FF0(VSPaRclIts) +
  ' AND VSPallet=' + Geral.FF0(VSPallet) +
  //' ORDER BY Controle DESC ' +
  ' ORDER BY Controle ' +
  '';

  //
  DqItens.Open();
  SetLength(FLsItens[Box], DqItens.RecordCount);
  DqItens.First();
  while not DqItens.Eof do
  begin
    J := DqItens.RecNo;
    //
    FLsItens[Box][J, 01] := DqItens.FieldValues[00];   // Conrole
    FLsItens[Box][J, 02] := DqItens.FieldValues[01];   // Pecas
    FLsItens[Box][J, 03] := DqItens.FieldValues[02];   // AreaM2
    FLsItens[Box][J, 04] := DqItens.FieldValues[03];   // AreaP2
    //
    DqItens.Next();
  end;
  DqItens.Close();
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
  //
  ReopenSumPal(Box, VSPallet);
end;

procedure TFmVSReclassifOneNw3.ReopenItensAClassificarPallet();
var
  Forma: Integer;
  SQL_Forma: String;
begin
  LiberaDigitacaoManual(False);
  case RGFrmaIns.ItemIndex of
    0: SQL_Forma := Geral.ATS(['AND Sumido=0 ','AND RclAPalDst=0 ']);
    1: SQL_Forma := Geral.ATS(['AND Sumido=1 ','AND RclAPalDst=0 ']);
    2: SQL_Forma := Geral.ATS(['AND RclAPalDst=0 ']);
    3: LiberaDigitacaoManual(True);
    else Geral.MB_Aviso(
    'Forma de digita��o n�o definida em "ReopenItensAClassificarPallet()"');
  end;
  //
  if RGFrmaIns.ItemIndex = 3 then
  begin
    QrItensACP.Close;
    EdArea.ValueVariant := 0;
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrItensACP, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(QrVSPaRclCabVSPallet.Value),
  SQL_Forma,
  'ORDER BY Controle DESC ',
  '']);
end;

procedure TFmVSReclassifOneNw3.ReopenSumPal(Box, VSPallet: Integer);
var
  J: Integer;
  Pecas, AreaM2, AreaP2: Double;
begin
  DqSumPall.Database := Dmod.MyDB;
  DqSumPall.SQL.Text :=
  ' SELECT ' +
  ' SUM(vmi.SdoVrtPeca) SdoVrtPeca, ' +
  ' SUM(vmi.SdoVrtArM2) SdoVrtArM2' +
  ' FROM ' + CO_SEL_TAB_VMI + ' vmi' +
  ' WHERE vmi.Pallet=' + Geral.FF0(VSPallet) +
  ' AND vmi.Pecas>0 ' +
(*
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
*)
  '';
  //
  DqSumPall.Open();
  Pecas  := Geral.DMV_Dot(DqSumPall.FieldValues[00]);   // Pecas
  AreaM2 := Geral.DMV_Dot(DqSumPall.FieldValues[01]);   // AreaM2
  AreaP2 := Geral.ConverteArea(AreaM2, TCalcType.ctM2toP2, TCalcFrac.cfQuarto);
  DqSumPall.Close();
  CarregaSGSumPall(Box, amathSubstitui, Pecas, AreaM2, AreaP2);
end;

procedure TFmVSReclassifOneNw3.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'cou.MediaMinM2, cou.MediaMaxM2 ',  // 2023-04-13
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN GraGruXCou cou ON ggx.Controle=cou.GraGruX ', // 2023-04-13
  'WHERE vmi.Controle=' + Geral.FF0(QrVSPaRclCabVSMovIts.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSReclassifOneNw3.ReopenVSMrtCad();
var
  I, N: Integer;
begin
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
  N := QrVSMrtCad.RecordCount;
  SetLength(FMartelosCod, N);
  SetLength(FMartelosNom, N);
  I := 0;
  while not QrVSMrtCad.Eof do
  begin
    FMartelosCod[I] := QrVSMrtCadCodigo.Value;
    FMartelosNom[I] := QrVSMrtCadNome.Value;
    //
    I := I + 1;
    QrVSMrtCad.Next;
  end;
end;

procedure TFmVSReclassifOneNw3.ReopenVSPaRclCab();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vsparclcaba pcc',
  'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
*)
(*
SELECT vga.GraGruX, vga.Nome,
vga.TpAreaRcl, vga.Empresa, vga.MovimCod, pcc.*,
CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),
IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))
NO_PRD_TAM_COR,
IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA
FROM vsparclcaba pcc
LEFT JOIN vspalleta vga ON vga.Codigo=pcc.vsPallet
LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX
LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC
LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad
LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa
WHERE pcc.Codigo=1
*)
  //
  GPBoxes.Visible := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vga.VSPallet, vga.Codigo VSGerRClA ',
  'FROM vsparclcaba pcc',
  'LEFT JOIN vsgerrcla  vga ON vga.Codigo=pcc.vsgerrcl',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  GPBoxes.Visible := True;
end;

procedure TFmVSReclassifOneNw3.RGFrmaInsClick(Sender: TObject);
begin
  if RGFrmaIns.ItemIndex <> 3 then
  begin
    RGFrmaIns.ItemIndex := 3;
    Exit;
  end;
  ReopenItensAClassificarPallet();
end;

procedure TFmVSReclassifOneNw3.SbDesnateClick(Sender: TObject);
begin
  ReopenDesnate(False);
end;

procedure TFmVSReclassifOneNw3.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

function TFmVSReclassifOneNw3.SubstituiOC_Antigo(): Boolean;
  function MostraFormVSReclassPrePal(SQLType: TSQLType; Pallet1, Pallet2,
  Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer):
  Integer;
  begin
    Result := 0;
    if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
    begin
      FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
      //
      FmVSReclassPrePal.ShowModal;
      Result := FmVSReclassPrePal.FPallet;
      FmVSReclassPrePal.Destroy;
    end;
  end;
var
  Codigo, CacCod, Pallet, Digitador, Revisor: Integer;
  MovimID: TEstqMovimID;
begin
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
(*
  VS_CRC_PF.MostraFormVSReclassPrePal(stIns,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant, );
  EXIT;
*)
  Pallet := MostraFormVSReclassPrePal(stIns,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant);
  //
  if Pallet <> 0 then
  begin
    if DBCheck.CriaFm(TFmVSRclArtPrpNew, FmVSRclArtPrpNew, afmoNegarComAviso) then
    begin
      FmVSRclArtPrpNew.ImgTipo.SQLType := stIns;
      //
      FmVSRclArtPrpNew.FFornecMO             := QrVSGerArtNewFornecMO.Value;
      //
      FmVSRclArtPrpNew.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNew.CBPallet.KeyValue     := Pallet;
      //
      FmVSRclArtPrpNew.EdPallet1.ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.EdPallet2.ValueVariant := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.EdPallet3.ValueVariant := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.EdPallet4.ValueVariant := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.EdPallet5.ValueVariant := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.EdPallet6.ValueVariant := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.CBPallet1.KeyValue := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.CBPallet2.KeyValue := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.CBPallet3.KeyValue := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.CBPallet4.KeyValue := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.CBPallet5.KeyValue := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.CBPallet6.KeyValue := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.SbPalletClick(Self);
      FmVSRclArtPrpNew.ShowModal;
      Codigo := FmVSRclArtPrpNew.FCodigo;
      CacCod := FmVSRclArtPrpNew.FCacCod;
      MovimID := FmVSRclArtPrpNew.FMovimID;
      FmVSRclArtPrpNew.Destroy;
      //
      if Codigo <> 0 then
      begin
        FCodigo := Codigo;
        FCacCod := CacCod;
        FMovimID := MovimID;
        ReopenVSPaRclCab();
        //
        EdDigitador.ValueVariant := Digitador;
        EdRevisor.ValueVariant   := Revisor;
        Result := True;
      end else
        Result := False;
    end;
  end;
end;

function TFmVSReclassifOneNw3.SubstituiOC_Novo: Boolean;
  function MostraFormVSReclassPrePal(const SQLType: TSQLType; var Pallet,
  StqCenLoc: Integer): Boolean;
  begin
    Result := False;
    if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
    begin
      FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
      //
      FmVSReclassPrePal.EdStqCenLoc.ValueVariant := StqCenLoc;
      FmVSReclassPrePal.CBStqCenLoc.KeyValue     := StqCenLoc;
      //
      FmVSReclassPrePal.ShowModal;
      //
      Pallet := FmVSReclassPrePal.FPallet;
      StqCenLoc := FmVSReclassPrePal.FStqCenLoc;
      Result := (Pallet <> 0) and (StqCenLoc <> 0);
      //
      FmVSReclassPrePal.Destroy;
    end;
  end;
var
  Codigo, CacCod, Pallet, StqCenLoc, Digitador, Revisor: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  Pallet    := 0;
  StqCenLoc := FStqCenLoc;
  //Pallet := MostraFormVSReclassPrePal(stIns);
  //
  //if Pallet <> 0 then
  if MostraFormVSReclassPrePal(stIns, Pallet, StqCenLoc) then
  begin
    if DBCheck.CriaFm(TFmVSRclArtPrpNw3, FmVSRclArtPrpNw3, afmoNegarComAviso) then
    begin
      FmVSRclArtPrpNw3.ImgTipo.SQLType := stIns;
      //
      FmVSRclArtPrpNw3.FFornecMO             := QrVSGerArtNewFornecMO.Value;
      //
      FmVSRclArtPrpNw3.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNw3.CBPallet.KeyValue     := Pallet;
      //
      FmVSRclArtPrpNw3.EdStqCenLoc.ValueVariant := StqCenLoc;
      FmVSRclArtPrpNw3.CBStqCenLoc.KeyValue     := StqCenLoc;
      //
      FmVSRclArtPrpNw3.F_Pallet01_ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNw3.EdPallet01.ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNw3.EdPallet02.ValueVariant := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNw3.EdPallet03.ValueVariant := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNw3.EdPallet04.ValueVariant := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNw3.EdPallet05.ValueVariant := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNw3.EdPallet06.ValueVariant := QrVSPaRclCabLstPal06.Value;
      FmVSRclArtPrpNw3.EdPallet07.ValueVariant := QrVSPaRclCabLstPal07.Value;
      FmVSRclArtPrpNw3.EdPallet08.ValueVariant := QrVSPaRclCabLstPal08.Value;
      FmVSRclArtPrpNw3.EdPallet09.ValueVariant := QrVSPaRclCabLstPal09.Value;
      FmVSRclArtPrpNw3.EdPallet10.ValueVariant := QrVSPaRclCabLstPal10.Value;
      FmVSRclArtPrpNw3.EdPallet11.ValueVariant := QrVSPaRclCabLstPal11.Value;
      FmVSRclArtPrpNw3.EdPallet12.ValueVariant := QrVSPaRclCabLstPal12.Value;
      FmVSRclArtPrpNw3.EdPallet13.ValueVariant := QrVSPaRclCabLstPal13.Value;
      FmVSRclArtPrpNw3.EdPallet14.ValueVariant := QrVSPaRclCabLstPal14.Value;
      FmVSRclArtPrpNw3.EdPallet15.ValueVariant := QrVSPaRclCabLstPal15.Value;
      //
      FmVSRclArtPrpNw3.CBPallet01.KeyValue := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNw3.CBPallet02.KeyValue := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNw3.CBPallet03.KeyValue := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNw3.CBPallet04.KeyValue := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNw3.CBPallet05.KeyValue := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNw3.CBPallet06.KeyValue := QrVSPaRclCabLstPal06.Value;
      FmVSRclArtPrpNw3.CBPallet07.KeyValue := QrVSPaRclCabLstPal07.Value;
      FmVSRclArtPrpNw3.CBPallet08.KeyValue := QrVSPaRclCabLstPal08.Value;
      FmVSRclArtPrpNw3.CBPallet09.KeyValue := QrVSPaRclCabLstPal09.Value;
      FmVSRclArtPrpNw3.CBPallet10.KeyValue := QrVSPaRclCabLstPal10.Value;
      FmVSRclArtPrpNw3.CBPallet11.KeyValue := QrVSPaRclCabLstPal11.Value;
      FmVSRclArtPrpNw3.CBPallet12.KeyValue := QrVSPaRclCabLstPal12.Value;
      FmVSRclArtPrpNw3.CBPallet13.KeyValue := QrVSPaRclCabLstPal13.Value;
      FmVSRclArtPrpNw3.CBPallet14.KeyValue := QrVSPaRclCabLstPal14.Value;
      FmVSRclArtPrpNw3.CBPallet15.KeyValue := QrVSPaRclCabLstPal15.Value;
      //
      FmVSRclArtPrpNw3.SbPalletClick(Self);
      FmVSRclArtPrpNw3.ShowModal;
      Codigo := FmVSRclArtPrpNw3.FCodigo;
      CacCod := FmVSRclArtPrpNw3.FCacCod;
      MovimID := FmVSRclArtPrpNw3.FMovimID;
      FmVSRclArtPrpNw3.Destroy;
      //
      if Codigo <> 0 then
      begin
        FCodigo := Codigo;
        FCacCod := CacCod;
        FMovimID := MovimID;
        ReopenVSPaRclCab();
        //
        EdDigitador.ValueVariant := Digitador;
        EdRevisor.ValueVariant   := Revisor;
        Result := True;
      end else
        Result := False;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.TentarFocarEdArea();
begin
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

procedure TFmVSReclassifOneNw3.TesteInclusao1Click(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdSdoVrtPeca.Text;
  //
  if InputQuery('Itens a reclassificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Random(7);
        if BoxInBoxes(Box) then
        begin
          OK := True;
          EdBox.ValueVariant := Box;
        end;
      end;
      //
      if RGFrmaIns.Itemindex = 3 then
      begin
        if QrSumTSdoVrtPeca.Value > 1 then
        begin
          Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
          Randomize;
          Desvio := Random(3000);
          Fator := 1 + ((Desvio - 1500) / 10000);
          Area := Area * Fator;
        end else
          Area := QrSumTSdoVrtArM2.Value;
        EdArea.ValueVariant := Area * 100;
        InsereCouroAtual();
      end;
      //
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaRclIts, All_VSPallet, Box_VSPaRclIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet: TmySQLQuery;
  Pecas, AreaM2, AreaP2, ValorUni: Double;
  Txt, SubClass: String;
  Continua: Boolean;
  ExigeSubClass: Boolean;
begin
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      if CampoSubClas then
      begin
        Txt := QrAllSubClass.Value;
        if InputQuery('Altera��o de dados', 'Altera��o da sub classe', Txt) then
        begin
          Subclass := Uppercase(dmkPF.SoTextoLayout(Txt));
          //
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'SubClass'], ['Controle'], [SubClass], [Controle], VAR_InsUpd_AWServerID);
        end;
      end else
      begin
        Pecas  := 1;
        //
        if QrVSPaRclCabTipoArea.Value = 0 then
          Txt := FloatToStr(QrAllAreaM2.Value * 100)
        else
          Txt := FloatToStr(QrAllAreaP2.Value * 100);
        //
        if InputQuery('Altera��o de dados', 'Altera��o de �rea: (Apenas n�meros sem v�rgula)', Txt) then
        begin
          AreaM2 := Geral.IMV(Txt);
          //
          if QrVSPaRclCabTipoArea.Value = 0 then
          begin
            AreaM2 := AreaM2 / 100;
            AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
          end else
          begin
            AreaP2 := AreaM2 / 100;
            AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
          end;
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
          [Controle], VAR_InsUpd_AWServerID);
        end;
      end;
    end;
    stDel:
    begin
      // ini 2023-11-15
      //Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ValorT/AreaM2 ',
      'FROM vsmovits ',
      'WHERE Controle=' + Geral.FF0(Box_VMI_Sorc),
      '']);
      ValorUni := Qry.Fields[0].AsFloat * QrAllAreaM2.Value;
      Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
      if Continua then
      begin
        if Box_VMI_Baix <> 0 then
        begin
          (*
          ini 2023-12-26 Erro!!??
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
          'UPDATE vsmovits SET ',
          '  Pecas=Pecas+' + Geral.FFT_Dot(QrAllPecas.Value, 3, siPositivo),
          ', AreaM2=AreaM2+' + Geral.FFT_Dot(QrAllAreaM2.Value, 2, siPositivo),
          ', AreaP2=AreaP2+' + Geral.FFT_Dot(QrAllAreaP2.Value, 2, siPositivo),
          ', ValorT=ValorT+' + Geral.FFT_Dot(ValorUni, 4, siPositivo),
          'WHERE Controle=' + Geral.FF0(Box_VMI_Baix),
          '']));
          Fim 2023-12-26
          *)
        end;
      end;
      // fim 2023-11-15
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_CRC_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
    Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
    //
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      //EdVSMrtCad.Text     := '';
      //CBVSMrtCad.KeyValue := Null;
      if not FCriando and EdArea.CanFocus then
        EdArea.SetFocus;
      if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
      EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
        EdArea.SetFocus
      else
      if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
      EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
        EdBox.SetFocus
    end;
    //
    ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
    AtualizaInfoOC();
  end;
  ReopenAll();
end;

procedure TFmVSReclassifOneNw3.UpdDelCouroSelecionado3(SQLTipo: TSQLType;
  CampoSubClas: Boolean);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaRclIts, All_VSPallet, Box_VSPaRclIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
  Pecas, AreaM2, AreaP2, AntM2, AntP2: Double;
  Txt: String;
  Continua: Boolean;
  //
  SelTop, SelBot, cIni, cFim, Count, I: Integer;
  //
  ExigeSubClass: Boolean;
  N: Integer;
begin
  N := 0;
  SelTop := SGAll.Selection.Top;
  SelBot := SGAll.Selection.Bottom;
  Count  := Length(FLsAll);
  cIni   := Count - SelTop + 1;
  cFim   := Count - SelBot + 1;
  Geral.MB_Info(IntToStr(cIni) + ' ate ' + IntToStr(cFim));
  //
(*
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaRclIts);                   // VSPaRclIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  // ini 2023-12-26
  //FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 12] := IntToStr(VMI_Baix);                     // VMI_Baix
  // fim 2023-12-26
*)
  //cIni := Count - cIni + 1;
  //cFim := Count - cFim + 1;
  //
  for I := cIni -1 downto cFim - 1 do
  begin
    if (I < Length(FLsAll)) then
    begin
      //Box := QrAllBox.Value;
      //Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
      Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
      if not ObtemQryesBox(Box, QrVSPallet) then
        Exit;
      if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
      Box_VMI_Baix, Box_VMI_Dest, ExigeSubclass) then
        Exit;
      //Controle       := QrAllControle.Value;
      Controle       := Geral.I64(FLsAll[I, 06]);
      //All_VSPaRclIts := QrAllVSPaRclIts.Value;
      All_VSPaRclIts := Geral.IMV(FLsAll[I, 07]);
      //All_VSPallet   := QrAllVSPallet.Value;
      All_VSPallet   := Geral.IMV(FLsAll[I, 08]);
      //
      Continua := False;
      //
      if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
      begin
        Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do ou alterado!');
        Exit;
      end;
      case SQLTipo of
        stUpd:
        begin
          Pecas  := 1;
          //
          if QrVSPaRclCabTipoArea.Value = 0 then
            //Txt := FloatToStr(QrAllAreaM2.Value * 100)
            Txt := Geral.SoNumero_TT(FLsAll[I, 03])
          else
            //Txt := FloatToStr(QrAllAreaP2.Value * 100);
            Txt := Geral.SoNumero_TT(FLsAll[I, 10]);
          //
          if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
          begin
            AreaM2 := Geral.IMV(Txt);
            //
            if QrVSPaRclCabTipoArea.Value = 0 then
            begin
              AreaM2 := AreaM2 / 100;
              AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
            end else
            begin
              AreaP2 := AreaM2 / 100;
              AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
            end;
            AntM2  := Geral.DMV_Dot(FLsAll[I, 03]);
            AntP2  := Geral.DMV_Dot(FLsAll[I, 03]);
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                          [Controle], VAR_InsUpd_AWServerID);
            FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);
            FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);
            //
            CarregaSGSumPall(Box, amathSubtrai, Pecas, AntM2, AntP2);
            CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
            //
          end;
        end;
        stDel:
        begin
          if N = 0 then
          if Geral.MB_Pergunta(
          'Deseja realmente excluir os couros selecionados?') = ID_YES then
          begin
            N := N + 1;
          end;
          if N > 0 then
          begin
            Continua := VS_CRC_PF.ExcluiVSNaoVMI('', Tabela, Campo, Controle,
            Dmod.MyDB) = ID_YES;
            CarregaSGSumPall(Box, amathSubtrai, Pecas, AreaM2, AreaP2);
          end;
        end;
        else
          Geral.MB_Erro('N�o implementado!');
      end;
      if Continua then
      begin
        VS_CRC_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
        Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
        //
        if PnDigitacao.Enabled then
        begin
          EdBox.ValueVariant  := 0;
          EdArea.ValueVariant := 0;
          //EdVSMrtCad.Text     := '';
          //CBVSMrtCad.KeyValue := Null;
          EdArea.SetFocus;
          EdArea.SelectAll;
        end;
        //
        ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
        AtualizaInfoOC();
      end;
    end else
      Geral.MB_Aviso('Item n�o existe!');
  end;
  ReopenAll();
end;

procedure TFmVSReclassifOneNw3.VerificaAreaAutomatica;
begin
  case QrVSPaRclCabTipoArea.Value of
    0: EdArea.ValueVariant := (QrItensACPAreaM2.Value * 100);
    1: EdArea.ValueVariant := (QrItensACPAreaP2.Value * 100);
    else Geral.MB_Aviso(
      'Tipo de �rea n�o definida em "QrItensACPAfterScroll()"');
  end;
  //
  if not FCriando and EdBox.CanFocus then
    EdBox.SetFocus;
end;

procedure TFmVSReclassifOneNw3.VerificaBoxes;
var
  LstPal, I: Integer;
begin
(*
  if not Mostrartodosboxes1.Checked then
  begin
    PnBox01.Visible := QrVSPaRclCabLstPal01.Value <> 0;
    PnBox02.Visible := QrVSPaRclCabLstPal02.Value <> 0;
    PnBox03.Visible := QrVSPaRclCabLstPal03.Value <> 0;
    PnBox04.Visible := QrVSPaRclCabLstPal04.Value <> 0;
    PnBox05.Visible := QrVSPaRclCabLstPal05.Value <> 0;
    PnBox06.Visible := QrVSPaRclCabLstPal06.Value <> 0;
  end else
  begin
    PnBox01.Visible := True;
    PnBox02.Visible := True;
    PnBox03.Visible := True;
    PnBox04.Visible := True;
    PnBox05.Visible := True;
    PnBox06.Visible := True;
  end;
*)
  if not Mostrartodosboxes1.Checked then
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
      begin
        LstPal := QrVSPaRclCab.FieldByName('LstPal' + Geral.FFF(I, 2)).AsInteger;
        FPnBox[I].Visible := LstPal <> 0;
      end;
    end;
  end else
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
        FPnBox[I].Visible := True;
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.VertextMemo1Click(Sender: TObject);
begin
  Geral.MB_Info(Memo2.Text);
end;

function TFmVSReclassifOneNw3.ZeraEstoquePalletOrigem(): Boolean;
var
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
  DataEHora: TDateTime;
begin
  Result := True;
  //
  if QrSumTSdoVrtPeca.Value > 0  then
  if Geral.MB_Pergunta(
  'Deseja zerar o estoque residual de ' + FloatToStr(QrSumTSdoVrtPeca.Value) +
  ' pe�as do pallet de origem ' + Geral.FF0(QrVSPaRclCabVSPallet.Value) + '?') =
  ID_YES then
  begin
    DataEHora := DModG.ObtemAgora();
    //
    BxaPecas    := QrSumTSdoVrtPeca.Value;
    BxaPesoKg   := 0;
    //
    BxaAreaM2 := QrSumTSdoVrtArM2.Value;
    BxaAreaP2 := Geral.ConverteArea(QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    //
    BxaValorT   := 0; // Ver o que fazer!
    Result := VS_CRC_PF.ZeraEstoquePalletOrigemReclas(QrVSPaRclCabVSPallet.Value,
      QrVSPaRclCabCodigo.Value, QrVSPaRclCabMovimCod.Value,
      QrVSPaRclCabEmpresa.Value, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
      BxaValorT, DataEHora, iuvpei053(*Zeramento de estoque de pallet de origem 2/3*));
  end;
end;

procedure TFmVSReclassifOneNw3.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalRclIts: TmySQLQuery; Pallet: Integer);
begin
  if QrVSPallet <> nil then
  begin
    if (Pallet > 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
      'SELECT let.*, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
      ' CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt, ',
      'xco.MediaMinM2, xco.MediaMaxM2 ',  // 2023-05-06
      'FROM vspalleta let ',
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE let.Codigo=' + Geral.FF0(Pallet),
      '']);
      ReconfiguraPaineisIntMei(QrVSPallet, FPnIntMei[Tecla]);
      //
      FMaxPecas[Tecla] := QrVSPallet.FieldByName('QtdPrevPc').AsFloat;
      if (QrVSPallet.State <> dsInactive) and (QrVSPallet.RecordCount > 0) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat <= 0.01) then
      begin
        if Tecla = 0 then
          Geral.MB_Aviso(
          'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!'
          + sLineBreak + 'Para evitar erros de estoque esta janela ser� fechada!');
          Close;
          Exit;
      end;
      //
      if QrVSPalRclIts <> nil then
        UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalRclIts, Dmod.MyDB, [
        'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
        'FROM vsparclitsa ',
        'WHERE Codigo=' + Geral.FF0(FCodigo),
        'AND Tecla=' + Geral.FF0(Tecla),
        'AND VSPallet=' + Geral.FF0(Pallet),
        'ORDER BY DtHrIni DESC, Controle DESC ',
        'LIMIT 1 ',
        '']);
    end else
    begin
      QrVSPallet.Close;
    end;
  end;
end;

end.
