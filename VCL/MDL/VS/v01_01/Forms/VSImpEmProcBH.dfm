object FmVSImpEmProcBH: TFmVSImpEmProcBH
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-151 :: Impress'#227'o de Couros em Processo BH'
  ClientHeight = 491
  ClientWidth = 734
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 734
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 686
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 638
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 472
        Height = 32
        Caption = 'Impress'#227'o de Couros em Processo BH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 472
        Height = 32
        Caption = 'Impress'#227'o de Couros em Processo BH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 472
        Height = 32
        Caption = 'Impress'#227'o de Couros em Processo BH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 377
    Width = 734
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 730
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 421
    Width = 734
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 588
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 586
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 734
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 73
      Width = 734
      Height = 256
      Align = alClient
      DataSource = DsEmit
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmis'
          Title.Caption = 'Dta emis.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaBaixa'
          Title.Caption = 'Dta baixa'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fulao'
          Title.Caption = 'Ful'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HoraIni'
          Title.Caption = 'Hora ini.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Nome da receita'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovCod'
          Title.Caption = 'IME-C'
          Width = 56
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 734
      Height = 73
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 812
      object GroupBox3: TGroupBox
        Left = 5
        Top = 2
        Width = 256
        Height = 68
        Caption = ' Per'#237'odo: '
        TabOrder = 0
        object TPDataIni: TdmkEditDateTimePicker
          Left = 8
          Top = 40
          Width = 112
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 45291.000000000000000000
          Time = 0.777157974502188200
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object CkDataIni: TCheckBox
          Left = 8
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data inicial'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkDataFim: TCheckBox
          Left = 124
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data final'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 124
          Top = 40
          Width = 112
          Height = 21
          Date = 45291.000000000000000000
          Time = 0.777203761601413100
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object frxWET_CURTI_151_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_151_01GetValue
    Left = 184
    Top = 140
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmit
        DataSetName = 'frxDsEmit'
      end
      item
        DataSet = frxDsEmProc
        DataSetName = 'frxDsEmProc'
      end
      item
        DataSet = frxDsorigem
        DataSetName = 'frxDsorigem'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEmit
        DataSetName = 'frxDsEmit'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DataField = 'Codigo'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmit."Codigo"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858270000000000
          Height = 15.118110236220470000
          DataField = 'DataEmis'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."DataEmis"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          DataField = 'Numero'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmit."Numero"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 204.094620000000000000
          Height = 15.118110236220470000
          DataField = 'NOME'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmit."NOME"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 56.692918270000000000
          Height = 15.118110240000000000
          DataField = 'Peso'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmit."Peso"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmit."Qtde"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Fulao'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."Fulao"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DataField = 'VSMovCod'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmit."VSMovCod"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 128.503937010000000000
          Height = 15.118110236220470000
          DataField = 'NOMECI'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmit."NOMECI"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Width = 30.236220470000000000
          Height = 15.118110236220470000
          DataField = 'HoraIni'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          DisplayFormat.FormatStr = 'hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmit."HoraIni"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Couros em Processo BH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pesagem')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 60.472480000000000000
          Width = 238.110390000000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 60.472480000000000000
          Width = 56.692918270000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 60.472480000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 60.472480000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ful'#227'o')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 60.472480000000000000
          Width = 128.503875980000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente Interno')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Top = 60.472480000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'h ini.')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 22.677170240000000000
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEmit."Setor"'
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315155910000000000
          Height = 22.677180000000000000
          DataField = 'NOMESETOR'
          DataSet = frxDsEmit
          DataSetName = 'frxDsEmit'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmit."NOMESETOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559055118110236000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 3.779527559055118000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692920000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEmProc
        DataSetName = 'frxDsEmProc'
        RowCount = 0
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmProc."Controle"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmProc."GraGruX"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmProc."Pecas"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEmProc."PesoKg"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataField = 'Marca'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmProc."Marca"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmProc."NO_SERIE_FICHA"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 245.669450000000000000
          Height = 13.228346460000000000
          DataField = 'Observ'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmProc."Observ"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataField = 'NO_CLIENTMO'
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmProc."NO_CLIENTMO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 13.228346460000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'GGX')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 13.228346460000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 13.228346460000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 13.228346460000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 13.228346460000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 13.228346460000000000
          Width = 226.771677950000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 13.228346460000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente MO')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        DataSet = frxDsorigem
        DataSetName = 'frxDsorigem'
        RowCount = 0
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 13.228339130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsorigem."Controle"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsorigem."GraGruX"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsorigem."Pecas">]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsorigem."PesoKg">]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataField = 'Marca'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsorigem."Marca"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsorigem."NO_SERIE_FICHA"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 226.771677950000000000
          Height = 13.228346460000000000
          DataField = 'Observ'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsorigem."Observ"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataField = 'NO_CLIENTMO'
          DataSet = frxDsorigem
          DataSetName = 'frxDsorigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsorigem."NO_CLIENTMO"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEmProc."Ativo"'
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'GGX')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 49.133890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 245.669450000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataSet = frxDsEmProc
          DataSetName = 'frxDsEmProc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente MO')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 13.228339130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEmitAfterScroll
    SQL.Strings = (
      'DROP TABLE IF EXISTS _CAL_CUR_EM_PROCESSO_; '
      ' '
      'CREATE TABLE _CAL_CUR_EM_PROCESSO_ '
      'SELECT *  '
      'FROM bluederm.vsmovits'
      'WHERE MovimID IN (26,27) '
      'AND SdoVrtPeca > 0 '
      '; '
      ' '
      'DROP TABLE IF EXISTS _CAL_CUR_VS_ORIGEM_; '
      ' '
      'CREATE TABLE _CAL_CUR_VS_ORIGEM_ '
      'SELECT *  '
      'FROM bluederm.vsmovits'
      'WHERE MovimID IN (26,27) '
      'AND QtdAntPeca > QtdGerPeca '
      '; '
      ' '
      'SELECT *  '
      'FROM bluederm.emit'
      'WHERE VSMovCod IN ( '
      '  SELECT DISTINCT MovimCod  '
      '  FROM _CAL_CUR_VS_ORIGEM_ '
      ') ')
    Left = 184
    Top = 192
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmitDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Required = True
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
      Required = True
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Required = True
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrEmitTempoR: TIntegerField
      FieldName = 'TempoR'
      Required = True
    end
    object QrEmitTempoP: TIntegerField
      FieldName = 'TempoP'
      Required = True
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
      Required = True
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
      Required = True
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
      Required = True
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
      Required = True
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
      Required = True
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
      Required = True
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
      Required = True
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
      Required = True
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
      Required = True
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
      Required = True
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
      Required = True
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
      Required = True
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
      Required = True
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
      Required = True
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
      Required = True
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
      Required = True
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
      Required = True
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
      Required = True
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
      Required = True
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
      Required = True
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
      Required = True
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrEmitHoraIni: TTimeField
      FieldName = 'HoraIni'
    end
  end
  object frxDsEmit: TfrxDBDataset
    UserName = 'frxDsEmit'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DataEmis=DataEmis'
      'Status=Status'
      'Numero=Numero'
      'NOMECI=NOMECI'
      'NOMESETOR=NOMESETOR'
      'Tecnico=Tecnico'
      'NOME=NOME'
      'ClienteI=ClienteI'
      'Tipificacao=Tipificacao'
      'TempoR=TempoR'
      'TempoP=TempoP'
      'Setor=Setor'
      'Tipific=Tipific'
      'Espessura=Espessura'
      'DefPeca=DefPeca'
      'Peso=Peso'
      'Custo=Custo'
      'Qtde=Qtde'
      'AreaM2=AreaM2'
      'Fulao=Fulao'
      'Obs=Obs'
      'SetrEmi=SetrEmi'
      'SourcMP=SourcMP'
      'Cod_Espess=Cod_Espess'
      'CodDefPeca=CodDefPeca'
      'CustoTo=CustoTo'
      'CustoKg=CustoKg'
      'CustoM2=CustoM2'
      'CusUSM2=CusUSM2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'EmitGru=EmitGru'
      'Retrabalho=Retrabalho'
      'SemiCodPeca=SemiCodPeca'
      'SemiTxtPeca=SemiTxtPeca'
      'SemiPeso=SemiPeso'
      'SemiQtde=SemiQtde'
      'SemiAreaM2=SemiAreaM2'
      'SemiRendim=SemiRendim'
      'SemiCodEspe=SemiCodEspe'
      'SemiTxtEspe=SemiTxtEspe'
      'BRL_USD=BRL_USD'
      'BRL_EUR=BRL_EUR'
      'DtaCambio=DtaCambio'
      'VSMovCod=VSMovCod'
      'HoraIni=HoraIni')
    DataSet = QrEmit
    BCDToCurrency = False
    DataSetOptions = []
    Left = 184
    Top = 240
  end
  object QrEmProc: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEmProcAfterScroll
    SQL.Strings = (
      'SELECT cep.*, '
      'CONCAT(vsf.Nome, " ", cep.Ficha) NO_SERIE_FICHA, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTMO  '
      'FROM _cal_cur_em_processo_ cep '
      
        'LEFT JOIN bluederm_colorad2.vsserfch   vsf ON vsf.Codigo=cep.Ser' +
        'ieFch '
      
        'LEFT JOIN bluederm_colorad2.entidades cli ON cli.Codigo=cep.Clie' +
        'ntMO '
      'WHERE cep.MovimCod=15')
    Left = 268
    Top = 192
    object QrEmProcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmProcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmProcMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEmProcMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEmProcMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrEmProcEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEmProcTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEmProcCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrEmProcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEmProcLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrEmProcLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrEmProcDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEmProcPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEmProcGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEmProcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmProcPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEmProcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmProcAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEmProcValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrEmProcSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrEmProcSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrEmProcSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrEmProcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEmProcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEmProcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEmProcFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrEmProcMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrEmProcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrEmProcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrEmProcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmProcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmProcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmProcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmProcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmProcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmProcAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmProcSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrEmProcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrEmProcSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrEmProcFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrEmProcValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrEmProcDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrEmProcDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrEmProcDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrEmProcDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrEmProcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrEmProcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrEmProcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrEmProcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrEmProcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrEmProcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrEmProcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrEmProcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrEmProcAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrEmProcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrEmProcMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEmProcTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrEmProcZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrEmProcEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrEmProcLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrEmProcCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrEmProcNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrEmProcFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrEmProcFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrEmProcPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrEmProcPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrEmProcPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrEmProcReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrEmProcStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrEmProcItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrEmProcVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrEmProcVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrEmProcClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrEmProcCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrEmProcNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 50
    end
    object QrEmProcNO_CLIENTMO: TWideStringField
      FieldName = 'NO_CLIENTMO'
      Size = 100
    end
  end
  object frxDsEmProc: TfrxDBDataset
    UserName = 'frxDsEmProc'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_SERIE_FICHA=NO_SERIE_FICHA'
      'NO_CLIENTMO=NO_CLIENTMO')
    DataSet = QrEmProc
    BCDToCurrency = False
    DataSetOptions = []
    Left = 268
    Top = 240
  end
  object QrOrigem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cvo.*,'
      'CONCAT(vsf.Nome, " ", cvo.Ficha) NO_SERIE_FICHA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTMO'
      'FROM _cal_cur_vs_origem_ cvo'
      
        'LEFT JOIN bluederm_colorad2.vsserfch   vsf ON vsf.Codigo=cvo.Ser' +
        'ieFch'
      
        'LEFT JOIN bluederm_colorad2.entidades cli ON cli.Codigo=cvo.Clie' +
        'ntMO'
      'WHERE cvo.MovimCod=15'
      '')
    Left = 352
    Top = 192
    object QrOrigemNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 50
    end
    object QrOrigemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOrigemControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOrigemMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrOrigemMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrOrigemMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrOrigemEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOrigemTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrOrigemCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrOrigemMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrOrigemLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrOrigemLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrOrigemDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrOrigemPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrOrigemGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOrigemPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrOrigemPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrOrigemAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrOrigemAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrOrigemValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrOrigemSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrOrigemSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrOrigemSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrOrigemSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrOrigemSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrOrigemObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOrigemFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrOrigemMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrOrigemCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrOrigemCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrOrigemLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOrigemDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOrigemDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOrigemUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOrigemUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOrigemAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOrigemAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOrigemSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrOrigemSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrOrigemSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrOrigemFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrOrigemValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrOrigemDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrOrigemDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrOrigemDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrOrigemDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrOrigemQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrOrigemQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrOrigemQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrOrigemQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrOrigemQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrOrigemQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrOrigemQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrOrigemQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrOrigemAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrOrigemNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrOrigemMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrOrigemTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrOrigemZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrOrigemEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrOrigemLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrOrigemCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrOrigemNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrOrigemFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrOrigemFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrOrigemPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrOrigemPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrOrigemPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrOrigemReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrOrigemStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrOrigemItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrOrigemVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrOrigemVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrOrigemClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrOrigemNO_CLIENTMO: TWideStringField
      FieldName = 'NO_CLIENTMO'
      Size = 100
    end
    object QrOrigemCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
  end
  object frxDsorigem: TfrxDBDataset
    UserName = 'frxDsorigem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_SERIE_FICHA=NO_SERIE_FICHA'
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'NO_CLIENTMO=NO_CLIENTMO'
      'CustoPQ=CustoPQ')
    DataSet = QrOrigem
    BCDToCurrency = False
    DataSetOptions = []
    Left = 352
    Top = 240
  end
  object QrMCs: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 412
    Top = 320
    object QrMCsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 184
    Top = 288
  end
end
