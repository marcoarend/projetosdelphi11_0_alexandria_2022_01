unit VSCurOriIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, Vcl.Menus, UnAppEnums;

type
  TFormaBaixa = (frmabxaIndef=0, frmabxaParcial=1, frmabxaTotal=2,
    frmabxaMarcaSemContar=3);
  TFmVSCurOriIMEI = class(TForm)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGGXInn: TmySQLQuery;
    QrGGXInnGraGru1: TIntegerField;
    QrGGXInnControle: TIntegerField;
    QrGGXInnNO_PRD_TAM_COR: TWideStringField;
    QrGGXInnSIGLAUNIDMED: TWideStringField;
    QrGGXInnCODUSUUNIDMED: TIntegerField;
    QrGGXInnNOMEUNIDMED: TWideStringField;
    DsGGXInn: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoquePecas: TFloatField;
    QrEstoquePesoKg: TFloatField;
    QrEstoqueAreaM2: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstoque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    QrEstoqueVSMulFrnCab: TIntegerField;
    QrEstoqueClientMO: TIntegerField;
    QrEstoqueSerieFch: TIntegerField;
    QrEstoqueFicha: TIntegerField;
    QrEstoqueMarca: TWideStringField;
    QrEstoqueNO_SerieFch: TWideStringField;
    RGArtOrigem: TRadioGroup;
    QrVMICal: TmySQLQuery;
    QrEstoqueMovimCod: TIntegerField;
    QrVMICalCodigo: TIntegerField;
    QrVMICalControle: TIntegerField;
    QrVMICalMovimCod: TIntegerField;
    QrVMICalMovimNiv: TIntegerField;
    QrVMICalMovimID: TIntegerField;
    QrVMICalPallet: TIntegerField;
    QrVMICalGraGruX: TIntegerField;
    QrVMICalTerceiro: TIntegerField;
    QrVMICalVSMulFrnCab: TIntegerField;
    QrVMICalQtdAntPeca: TFloatField;
    QrVMICalQtdAntPeso: TFloatField;
    QrVMICalQtdAntArM2: TFloatField;
    QrVMICalQtdAntArP2: TFloatField;
    QrVMICalValorT: TFloatField;
    QrVMICalSerieFch: TIntegerField;
    QrVMICalFicha: TIntegerField;
    QrVMICalMarca: TWideStringField;
    QrVMICalGraGruY: TIntegerField;
    QrVMICalClientMO: TIntegerField;
    QrVMICalQtdGerPeca: TFloatField;
    QrVMICalQtdGerPeso: TFloatField;
    QrVMICalQtdGerArM2: TFloatField;
    QrVMICalQtdGerArP2: TFloatField;
    QrVMISum: TmySQLQuery;
    QrVMISumSdoPeca: TFloatField;
    QrVMISumSdoPeso: TFloatField;
    QrVMISumSdoArM2: TFloatField;
    QrVMISumSdoArP2: TFloatField;
    QrVMISumQtdAntPeso: TFloatField;
    QrOriCal: TmySQLQuery;
    QrOriCalGraGru1: TIntegerField;
    QrOriCalControle: TIntegerField;
    QrOriCalNO_PRD_TAM_COR: TWideStringField;
    QrOriCalSIGLAUNIDMED: TWideStringField;
    QrOriCalCODUSUUNIDMED: TIntegerField;
    QrOriCalNOMEUNIDMED: TWideStringField;
    DsOriCal: TDataSource;
    QrOriCalFicha: TIntegerField;
    QrGGXJmp: TmySQLQuery;
    QrGGXJmpGraGru1: TIntegerField;
    QrGGXJmpControle: TIntegerField;
    QrGGXJmpNO_PRD_TAM_COR: TWideStringField;
    QrGGXJmpSIGLAUNIDMED: TWideStringField;
    QrGGXJmpCODUSUUNIDMED: TIntegerField;
    QrGGXJmpNOMEUNIDMED: TWideStringField;
    DsGGXJmp: TDataSource;
    QrVMICalPecas: TFloatField;
    QrVMICalAreaM2: TFloatField;
    QrVMICalPesoKg: TFloatField;
    QrVMICalStqCenLoc: TIntegerField;
    QrEstoqueSdoVrtPeca: TFloatField;
    QrEstoqueSdoVrtPeso: TFloatField;
    QrRmsGGX: TmySQLQuery;
    QrRmsGGXGraGru1: TIntegerField;
    QrRmsGGXControle: TIntegerField;
    QrRmsGGXNO_PRD_TAM_COR: TWideStringField;
    QrRmsGGXSIGLAUNIDMED: TWideStringField;
    QrRmsGGXCODUSUUNIDMED: TIntegerField;
    QrRmsGGXNOMEUNIDMED: TWideStringField;
    DsRmsGGX: TDataSource;
    QrOriCalMarca: TWideStringField;
    QrMedia: TmySQLQuery;
    PMCalcula: TPopupMenu;
    ASaberaspeasdeumaunicamarcapelopeso1: TMenuItem;
    EdClientMO: TdmkEditCB;
    Label7: TLabel;
    CBClientMO: TdmkDBLookupComboBox;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    DsCliInt: TDataSource;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    CBMarca: TdmkDBLookupComboBox;
    EdMarca: TdmkEditCB;
    Label8: TLabel;
    QrMarcasMarca: TWideStringField;
    QrMarcasMartelo: TWideStringField;
    QrMediaPecas: TFloatField;
    QrMediaPesoKg: TFloatField;
    QrMediaMedia: TFloatField;
    Panel3: TPanel;
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    Panel6: TPanel;
    Label50: TLabel;
    Label4: TLabel;
    Label63: TLabel;
    EdReqMovEstq: TdmkEdit;
    EdJmpGGX: TdmkEditCB;
    CBJmpGGX: TdmkDBLookupComboBox;
    EdRmsGGX: TdmkEditCB;
    CBRmsGGX: TdmkDBLookupComboBox;
    PnMarcaSemContarPecas: TPanel;
    GroupBox2: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    EdPesoMarcaSemContarPecas: TdmkEdit;
    EdPercGanhoPeso: TdmkEdit;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    LaPesoKg: TLabel;
    SbCalcula: TSpeedButton;
    Label6: TLabel;
    LaPecas: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdQtdAntPeso: TdmkEdit;
    EdControle: TdmkEdit;
    EdPecas: TdmkEdit;
    EdObserv: TdmkEdit;
    EdPesoKg: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    CkSoClientMO: TCheckBox;
    QrFichas: TMySQLQuery;
    PanelOriCal: TPanel;
    DBGOriCal: TdmkDBGridZTO;
    PnOriCal: TPanel;
    Label10: TLabel;
    EdEstoqueSDOPECA: TdmkEdit;
    Label12: TLabel;
    EdEstoqueSDOPESO: TdmkEdit;
    TmEstoqueAfterScrool: TTimer;
    QrEmiOri: TMySQLQuery;
    QrEmiOriDataEmis: TDateTimeField;
    QrEmiOriFulao: TWideStringField;
    QrEmiOriCodigo: TIntegerField;
    QrEmiOriNumero: TIntegerField;
    QrEmiOriNome: TWideStringField;
    QrEmiOriObs: TWideStringField;
    PnOriEmi: TPanel;
    DBGOriEmi: TDBGrid;
    DsEmiOri: TDataSource;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGArtOrigemClick(Sender: TObject);
    procedure EdQtdAntPesoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrEstoqueAfterScroll(DataSet: TDataSet);
    procedure QrEstoqueBeforeClose(DataSet: TDataSet);
    procedure QrGGXJmpAfterOpen(DataSet: TDataSet);
    procedure ASaberaspeasdeumaunicamarcapelopeso1Click(Sender: TObject);
    procedure SbCalculaClick(Sender: TObject);
    procedure CBMarcaClick(Sender: TObject);
    procedure CBGraGruXClick(Sender: TObject);
    procedure EdClientMORedefinido(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure PMCalculaPopup(Sender: TObject);
    procedure CkSoClientMOClick(Sender: TObject);
    procedure TmEstoqueAfterScroolTimer(Sender: TObject);
  private
    { Private declarations }
    FSQL_GraGruX, FSQL_SerieFch, FSQL_Ficha, FVSMovImp4(*, FVSMovImp5*): String;
    FGraGruY, FGGXJmpDst, FGGXJmpSrc: Integer;
    FFormAtivado: Boolean;
    //
    { Original
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcPecas, ParcPesoKg,
              ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
    }
    procedure InsereIMEI_DoCal(InsereTudo: Boolean; ParcPecas, ParcPesoKg,
              ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);

    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenVSGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    function  BaixaIMEIParcial_BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
    function  BaixaIMEIMarcaSemcontaPecaSequencial(): Integer;
    function  BaixaIMEIMarcaSemcontaPecaProporcional(): Integer;
    function  PesoOriginalParcial(Pecas: Double): Double;
    procedure ReopenGraGruX();
    procedure ReopenMarcas();
    procedure ReopenCliInt();
    procedure ReopenMediaMarca(Marca: String);
    procedure CalculaParcial();
    procedure RedefineComponentes();
    procedure ReopenOriCal();
    procedure ReopenEmiOrigem();
  public
    { Public declarations }
    //FVSCurOriIMEI, FVSCurOriPallet,
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FOrigMovimNiv, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea,
    (*FClientMO,*) FFornecMO, FGGXRmsDst, FGGXRmsSrc, FStqCenLoc, FVmiPai: Integer;
    FDataHora: TDateTime;
    //FParcial, FMarcaSemContarPeca: Boolean;
    FFormaBaixa: TFormaBaixa;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
    procedure ReopenVMICal();
    procedure ReopenVMISum();
  end;

  var
  FmVSCurOriIMEI: TFmVSCurOriIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModVS, VSCurCab, UnVS_PF, AppListas, CalcParc4Val, ModVS_CRC;

{$R *.DFM}

procedure TFmVSCurOriIMEI.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSCurOriIMEI.ASaberaspeasdeumaunicamarcapelopeso1Click(
  Sender: TObject);
var
  PesoTotal: String;
begin
  PesoTotal := '0';
  if InputQuery('Total de peso em kg', 'Informe o peso total:', PesoTotal) then
  begin
    ReopenMediaMarca(QrEstoqueMarca.Value);
    Application.CreateForm(TFmCalcParc4Val, FmCalcParc4Val);
    FmCalcParc4Val.FCalcExec := calcexecPecasPorKgTotalDivMediaKgOri;
    FmCalcParc4Val.FCalcAuto := False;
    //
    FmCalcParc4Val.MeHelp.Lines.Add('Linha 1: Peso marca de origem');
    FmCalcParc4Val.LaA1.Caption := 'Pe�as:';
    FmCalcParc4Val.EdA1.ValueVariant := QrMediaPecas.Value;
    FmCalcParc4Val.LaA2.Caption := 'Peso kg:';
    FmCalcParc4Val.EdA2.ValueVariant := QrMediaPesoKg.Value;
    FmCalcParc4Val.LaA3.Caption := 'M�dia kg/p�:';
    FmCalcParc4Val.EdA3.ValueVariant := QrMediaMedia.Value;
    FmCalcParc4Val.LaA4.Caption := 'Ganho %:';
    FmCalcParc4Val.EdA4.ValueVariant := 6;
    //
    FmCalcParc4Val.MeHelp.Lines.Add('Linha 2: Peso e pe�as curtimento');
    FmCalcParc4Val.LaB1.Caption := 'Pe�as:';
    FmCalcParc4Val.EdB1.ValueVariant := 0;
    FmCalcParc4Val.LaB2.Caption := 'Peso kg:';
    FmCalcParc4Val.EdB2.ValueVariant := PesoTotal;
    FmCalcParc4Val.LaB3.Caption := '...';
    FmCalcParc4Val.ShowModal;
    //
    if FmCalcParc4Val.FUsaDados then
    begin
      EdPecas.ValueVariant := FmCalcParc4Val.EdB1.ValueVariant;
      EdQtdAntPeso.ValueVariant := FmCalcParc4Val.EdB2.ValueVariant;
    end;
    FmCalcParc4Val.Destroy;
  end;
end;

function TFmVSCurOriIMEI.BaixaIMEIMarcaSemcontaPecaProporcional(): Integer;
var
  Fator, FatorProporcional: Double;
  N: Integer;
  //
  function Baixar(Valendo: Boolean): Integer;
    procedure InsereItem(ItmPecas, ItmPesoKg: Double);
      function BaixaIMEIParcial_MarcaSemcontaPecaProporcional(MyPecas, MyPesoKg: Double): Integer;
      const
        AreaM2 = 0;
        AreaP2 = 0;
      var
        Pecas, PesoKg, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
        QtdAntPeca, QtdAntPeso: Double;
      begin
        Result := 0;
        Pecas  := MyPecas; //EdPecas.ValueVariant;
        PesoKg := PesoOriginalParcial(MyPecas);  //EdPesoKg.ValueVariant;
        //AreaM2 := EdAreaM2.ValueVariant;
        //AreaP2 := EdAreaP2.ValueVariant;
        //
        SobraPecas  := Pecas;
        SobraPesoKg := PesoKg;
        SobraAreaM2 := AreaM2;
        SobraAreaP2 := AreaP2;
        //
        QtdAntPeca  := SobraPecas;
        QtdAntPeso  := MyPesoKg * Fator; //EdQtdAntPeso.ValueVariant;
        //
        if MyObjects.FIC(Pecas <= 0, nil, 'Quantidade de pe�as <= 0!') then
          Exit;
        //
        if MyObjects.FIC(Pecas > QrEstoqueSdoVrtPeca.Value, EdPecas,
          'Quantidade de pe�as excede o m�ximo em estoque real!') then
            Exit;
        InsereIMEI_DoCal(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2,
          QtdAntPeca, QtdAntPeso);
        Result := 1;
      end;
    begin
      EdQtdAntPeso.ValueVariant := ItmPesoKg;
      EdPecas.ValueVariant      := ItmPecas;
      //
      //Geral.MB_Info('OK?');
      if not Valendo then
        Exit;
      //
      //Geral.MB_Info('Baixar de verdade!');
      BaixaIMEIParcial_MarcaSemcontaPecaProporcional(ItmPecas, ItmPesoKg);
   end;
  var
    N, I: Integer;
    SaldoPesoGerando, PecasItem, PesoItem, PecasBaixar, MediaItem,
    PesoBaixar: Double;
  begin
    //Fator            := 1 + (EdPercGanhoPeso.ValueVariant / 100);
    SaldoPesoGerando := EdPesoMarcaSemContarPecas.ValueVariant / Fator;
    QrEstoque.DisableControls;
    try
      N := 0;
      with DBG04Estq.DataSource.DataSet do
      for I := 0 to DBG04Estq.SelectedRows.Count-1 do
      begin
        if SaldoPesoGerando > 0 then
        begin
          //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
          GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
          N := N + 1;
          //
          ReopenMediaMarca(QrEstoqueMarca.Value);
          PecasBaixar   := Round(QrEstoqueSdoVrtPeca.Value * FatorProporcional);
          PesoBaixar    := Round(QrEstoqueSdoVrtPeso.Value * FatorProporcional);
          InsereItem(PecasBaixar, PesoBaixar);
          (*
          PecasItem   := QrEstoqueSdoVrtPeca.Value;
          PesoItem    := QrEstoqueSdoVrtPeso.Value;
          if PecasItem > 0 then
          begin
            MediaItem   := PesoItem / PecasItem;
            if PesoItem >= SaldoPesoGerando then
            begin
              PecasBaixar := Round(SaldoPesoGerando / PesoItem * PecasItem);
              PesoBaixar  := SaldoPesoGerando;
              SaldoPesoGerando := 0;
              //
              InsereItem(PecasBaixar, PesoBaixar);
            end else
            begin
              PecasBaixar := PecasItem;
              PesoBaixar  := PesoItem;
              SaldoPesoGerando := SaldoPesoGerando - PesoBaixar;
              //
              InsereItem(PecasBaixar, PesoBaixar);
            end;
          end;
          *)
        end;
      end;
    finally
      QrEstoque.EnableControls;
    end;
    //Result := N;
    Result := N;
  end;
var
  I: Integer;
  PecasItem, PesoItem, MediaItem, PecasBaixar, PesoBaixar, PesoAGerar: Double;
begin
  GBGerar.Visible := True;
  GBGerar.Enabled := False;
  try


      PecasBaixar := 0;
      PesoBaixar  := 0;
      Fator       := 1 + (EdPercGanhoPeso.ValueVariant / 100);
      PesoAGerar  := EdPesoMarcaSemContarPecas.ValueVariant / Fator;
      //
      with DBG04Estq.DataSource.DataSet do
      for I := 0 to DBG04Estq.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
        GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
        N := N + 1;
        //
        ReopenMediaMarca(QrEstoqueMarca.Value);
        PecasItem   := QrEstoqueSdoVrtPeca.Value;
        PesoItem    := QrEstoqueSdoVrtPeso.Value;
        if PecasItem > 0 then
        begin
          MediaItem   := PesoItem / PecasItem;
          PecasBaixar := PecasBaixar + PecasItem;
          PesoBaixar  := PesoBaixar  + PesoItem;
        end;
      end;
      if (PesoBaixar < PesoAGerar) or (PesoBaixar = 0) then
      begin
        Geral.MB_Aviso('Baixa abortada! Peso a baixar insuficiente!');
        Exit;
      end;
      FatorProporcional := PesoAGerar / PesoBaixar;
      //
      (*
      Geral.MB_Info(
      'P�  Baixar: ' + Geral.FFT(PecasBaixar, 3, siNegativo) + sLineBreak +
      'Kg  Baixar: ' + Geral.FFT(PesoBaixar, 3, siNegativo) + sLineBreak +
      'Kg a gerar: ' + Geral.FFT(PesoAGerar, 3, siNegativo) + sLineBreak +
      'Fator      : ' + Geral.FFT(FatorProporcional, 10, siNegativo) + sLineBreak +
      '');
      Exit;
      *)






    //if Baixar(False) then
      N := 0;
      Result := Baixar(True);
    //else
      //Geral.MB_Aviso('Baixa abortada!');
  finally
    LiberaEdicao(False, False);
  end;
end;


function TFmVSCurOriIMEI.BaixaIMEIMarcaSemcontaPecaSequencial: Integer;
var
  Fator: Double;
  //
  function Baixar(Valendo: Boolean): Boolean;
    procedure InsereItem(ItmPecas, ItmPesoKg: Double);
      function BaixaIMEIParcial_MarcaSemcontaPecaSequencial(MyPecas, MyPesoKg: Double): Integer;
      const
        AreaM2 = 0;
        AreaP2 = 0;
      var
        Pecas, PesoKg, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
        QtdAntPeca, QtdAntPeso: Double;
      begin
        Result := 0;
        Pecas  := MyPecas; //EdPecas.ValueVariant;
        PesoKg := PesoOriginalParcial(MyPecas);  //EdPesoKg.ValueVariant;
        //AreaM2 := EdAreaM2.ValueVariant;
        //AreaP2 := EdAreaP2.ValueVariant;
        //
        SobraPecas  := Pecas;
        SobraPesoKg := PesoKg;
        SobraAreaM2 := AreaM2;
        SobraAreaP2 := AreaP2;
        //
        QtdAntPeca  := SobraPecas;
        QtdAntPeso  := MyPesoKg * Fator; //EdQtdAntPeso.ValueVariant;
        //
        if MyObjects.FIC(Pecas <= 0, nil, 'Quantidade de pe�as <= 0!') then
          Exit;
        //
        if MyObjects.FIC(Pecas > QrEstoqueSdoVrtPeca.Value, EdPecas,
          'Quantidade de pe�as excede o m�ximo em estoque real!') then
            Exit;
        InsereIMEI_DoCal(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2,
          QtdAntPeca, QtdAntPeso);
        Result := 1;
      end;
    begin
      EdQtdAntPeso.ValueVariant := ItmPesoKg;
      EdPecas.ValueVariant      := ItmPecas;
      //
      //Geral.MB_Info('OK?');
      if not Valendo then
        Exit;
      //
      //Geral.MB_Info('Baixar de verdade!');
      BaixaIMEIParcial_MarcaSemcontaPecaSequencial(ItmPecas, ItmPesoKg);
   end;
  var
    N, I: Integer;
    SaldoPesoGerando, PecasItem, PesoItem, PecasBaixar, MediaItem,
    PesoBaixar: Double;
  begin
    Fator            := 1 + (EdPercGanhoPeso.ValueVariant / 100);
    SaldoPesoGerando := EdPesoMarcaSemContarPecas.ValueVariant / Fator;
    QrEstoque.DisableControls;
    try
      N := 0;
      with DBG04Estq.DataSource.DataSet do
      for I := 0 to DBG04Estq.SelectedRows.Count-1 do
      begin
        if SaldoPesoGerando > 0 then
        begin
          //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
          GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
          N := N + 1;
          //
          ReopenMediaMarca(QrEstoqueMarca.Value);
          PecasItem   := QrEstoqueSdoVrtPeca.Value;
          PesoItem    := QrEstoqueSdoVrtPeso.Value;
          if PecasItem > 0 then
          begin
            MediaItem   := PesoItem / PecasItem;
            if PesoItem >= SaldoPesoGerando then
            begin
              PecasBaixar := Round(SaldoPesoGerando / PesoItem * PecasItem);
              PesoBaixar  := SaldoPesoGerando;
              SaldoPesoGerando := 0;
              //
              InsereItem(PecasBaixar, PesoBaixar);
            end else
            begin
              PecasBaixar := PecasItem;
              PesoBaixar  := PesoItem;
              SaldoPesoGerando := SaldoPesoGerando - PesoBaixar;
              //
              InsereItem(PecasBaixar, PesoBaixar);
            end;
          end;
        end;
      end;
    finally
      QrEstoque.EnableControls;
    end;
    //Result := N;
    Result := SaldoPesoGerando <= 0;
  end;
begin
  GBGerar.Visible := True;
  GBGerar.Enabled := False;
  try
    if Baixar(False) then
      Baixar(True)
    else
      Geral.MB_Aviso('Baixa abortada!');
  finally
    LiberaEdicao(False, False);
  end;
end;

function TFmVSCurOriIMEI.BaixaIMEIParcial_BaixaIMEIParcial(): Integer;
var
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
  QtdAntPeca, QtdAntPeso: Double;
begin
  Result := 0;
  Pecas  := EdPecas.ValueVariant;
  PesoKg := EdPesoKg.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  SobraPecas  := Pecas;
  SobraPesoKg := PesoKg;
  SobraAreaM2 := AreaM2;
  SobraAreaP2 := AreaP2;
  //
  QtdAntPeca  := SobraPecas;
  QtdAntPeso  := EdQtdAntPeso.ValueVariant;
  //
  if MyObjects.FIC(Pecas <= 0, EdPecas, 'Informe a quantidade de pe�as') then
    Exit;
  //
  if MyObjects.FIC(Pecas > QrEstoqueSdoVrtPeca.Value, EdPecas,
    'Quantidade de pe�as excede o m�ximo em estoque real!') then
      Exit;
  InsereIMEI_DoCal(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2,
    QtdAntPeca, QtdAntPeso);
  Result := 1;
end;

function TFmVSCurOriIMEI.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      ReopenVMISum();
      if (QrEstoqueSdoVrtPeca.Value < QrVMISumSdoPeca.Value) then
      begin
        Geral.MB_Info('Item com saldo real inferior ao n�o curtido!' +
        sLineBreak + 'Baixe de forma parcial!');
        // N'ao faz nada!
      end else
      begin
        case FGraGruY of
          CO_GraGruY_1365_VSProCal: InsereIMEI_DoCal(True, 0, 0, 0, 0, 0, 0);
          else
          begin
            Geral.MB_Info('"GraGruY" n�o implementado em "BaixaIMEITotal()"!');
            //InsereIMEI_Atual(True, 0, 0, 0, 0, 0, 0);
          end;
        end;
      end;
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmVSCurOriIMEI.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo, Forma: Integer;
  GBAptos_Enabled: Boolean;
  Pecas: Double;
begin
  if QrEstoqueSdoVrtPeso.Value = 0 then
  begin
    if Geral.MB_Pergunta('O Saldo Peso Kg do IMEI ' + Geral.FF0(QrEstoqueIMEI.Value)
      + ' est� zerado!' + sLineBreak + 'Deseja informar o peso?') = ID_YES then
    begin
      VS_PF.MostraFormVSInnCab(QrEstoqueCodigo.Value, QrEstoqueIMEI.Value, 0);
    end;
    RedefineComponentes();
    Exit;
  end;
  //
  case FFormaBaixa of
    frmabxaIndef:
    begin
      Geral.MB_Erro('Forma de baixar indefinida! Avise a Dermatek!');
      Exit;
    end;
    frmabxaParcial: ; // nenhuma observa��o?
    frmabxaTotal,
    frmabxaMarcaSemContar:
    begin
      if DBG04Estq.SelectedRows.Count < 1 then
      begin
        Geral.MB_Info('Nenhum item foi selecionado!');
        Exit;
      end;
    end;
  end;
  //
  GBAptos_Enabled := GBAptos.Enabled;
  GBAptos.Enabled := False;
  try
    FGGXJmpDst := EdJmpGGX.ValueVariant;
    // Inicio 2019-01-15
    //FGGXJmpSrc := FmVSCurCab.QrVSCurCabGGXSrc.Value;
    FGGXJmpSrc := QrEstoqueGraGruX.Value;
    // Fim 2019-01-15
    //
    if MyObjects.FIC(FGGXJmpDst = 0, EdJmpGGX, 'Informe a mat�ria-prima jumped!') then
      Exit;
    //
    FGGXRmsDst := EdRmsGGX.ValueVariant;
    // Inicio 2019-01-15
    //FGGXRmsSrc := FmVSCurCab.QrVSCurCabGGXSrc.Value;
    FGGXRmsSrc := QrEstoqueGraGruX.Value;
    //
    FStqCenLoc := FmVSCurCab.QrVSCurAtuStqCenLoc.Value;
    FFornecMO  := FmVSCurCab.QrVSCurAtuFornecMO.Value;
    // Fim 2019-01-15
    //
    //
    if MyObjects.FIC(FGGXRmsDst = 0, EdRmsGGX, 'Informe a mat�ria-prima PDA!') then
      Exit;
    //
    if GBGerar.Visible then
      if MyObjects.FIC(EdQtdAntPeso.ValueVariant <= 0, EdQtdAntPeso,
      'Informe o peso kg ful�o!') then
        Exit;
    //
    N := 0;
    //if FMarcaSemContarPeca then
    case FFormaBaixa of
      frmabxaMarcaSemContar:
      begin
        if MyObjects.FIC(EdPesoMarcaSemContarPecas.ValueVariant <= 0,
          EdPesoMarcaSemContarPecas, 'Informe o peso do ful�o!')
        then
          Exit;
        //
        Forma := MyObjects.SelRadioGroup('Forma de baixa',
        'Escolha a forma de baixa', [
        'Sequencial', 'Proporcional'], -1);
        case Forma of
          0:  N := BaixaIMEIMarcaSemcontaPecaSequencial();
          1:  N := BaixaIMEIMarcaSemcontaPecaProporcional();
          //MyObjects.MostraPopUpDeBotao(PMOK, BtOK)
          else
          begin
            Geral.MB_Info('Configura��o de baixa n�o implementada!');
            Exit;
          end;
        end;
      end;
      frmabxaParcial:
      begin
        Pecas  := EdPecas.ValueVariant;
        if Pecas >= QrEstoqueSdoVrtPeca.Value then
        begin
          if Geral.MB_Pergunta('A quantidade informada � igual ao saldo do estoque!' +
          sLineBreak +
          'Caso voc� confirme a baixa ser� feita pelo m�todo de baixa total e n�o parcial para evitar arredondamento indevido.'
          + sLineBreak + 'Deseja continuar e fazer a baixa total?') <> ID_Yes then
            Exit
          else
          begin
            //N := BaixaIMEITotal();
            N := N + 1;
            //
            //AdicionaPallet();
            (*
            ReopenVMISum();
            if (QrEstoqueSdoVrtPeca.Value < QrVMISumSdoPeca.Value) then
            begin
              Geral.MB_Info('Item com saldo real inferior ao n�o curtido!' +
              sLineBreak + 'Baixe de forma parcial!');
              // N'ao faz nada!
            end else
            *)
            begin
              case FGraGruY of
                CO_GraGruY_1365_VSProCal: InsereIMEI_DoCal(True, 0, 0, 0, 0, 0, 0);
                else
                begin
                  Geral.MB_Info('"GraGruY" n�o implementado em "BaixaIMEITotal()"!');
                  //InsereIMEI_Atual(True, 0, 0, 0, 0, 0, 0);
                end;
              end;
            end;
          end;
        end else
        begin
          N := BaixaIMEIParcial_BaixaIMEIParcial();
        end;
      end;
      frmabxaTotal:   N := BaixaIMEITotal();
    end;
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('VSCurcab', MovimCod);
      MovimCod := FmVSCurCab.QrVSCurCabMovimCod.Value;
      VS_PF.AtualizaTotaisVSCurCab(MovimCod);
      //
      Codigo := EdCodigo.ValueVariant;
      FmVSCurCab.LocCod(Codigo, Codigo);
      if N > 0 then
      begin
        if CkContinuar.Checked then
          RedefineComponentes()
        else
          Close;
      end else
        Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
  (*
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSCurcab', MovimCod);
    MovimCod := FmVSCurCab.QrVSCurCabMovimCod.Value;
    VS_PF.AtualizaTotaisVSCurCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmVSCurCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
        LiberaEdicao(False, False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
  *)
  finally
    //GBAptos.Enabled := GBAptos_Enabled;
  end;
end;

procedure TFmVSCurOriIMEI.RedefineComponentes();
begin
  EdControle.ValueVariant   := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdAreaM2.ValueVariant     := 0;
  EdAreaP2.ValueVariant     := 0;
  EdObserv.ValueVariant     := '';
  EdQtdAntPeso.ValueVariant := 0;
  //
  ReopenItensAptos();
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
  LiberaEdicao(False, False);
  //
  GBAptos.Enabled := True;
end;

procedure TFmVSCurOriIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCurOriIMEI.CalculaParcial;
var
  PecasTotal: String;
begin
  PecasTotal := '0';
  if InputQuery('Total de pe�as', 'Informe quantidade total:', PecasTotal) then
  begin
    Application.CreateForm(TFmCalcParc4Val, FmCalcParc4Val);
    FmCalcParc4Val.FCalcExec := calcexec2;
    FmCalcParc4Val.EdA1.ValueVariant := FmVSCurCab.QrVSCurAtuPecas.Value;
    FmCalcParc4Val.EdB1.Text := PecasTotal;
    //FmCalcParc4Val.Ed03Peca.ValueVariant := QrEstoqueSdoVrtPeca.Value;
    ReopenVMISum();
    FmCalcParc4Val.Ed03Peca.ValueVariant := QrVMISumSdoPeca.Value;
    FmCalcParc4Val.Ed03Peso.ValueVariant := QrVMISumSdoPeso.Value;
    //
    FmCalcParc4Val.EdA3.ValueVariant := FmVSCurCab.QrVSCurAtuPesoKg.Value;
    FmCalcParc4Val.EdB3.ValueVariant := 0;
    //FmCalcParc4Val.Ed03Peso.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    //
    FmCalcParc4Val.ShowModal;
    //
    if FmCalcParc4Val.FUsaDados then
    begin
      EdPecas.ValueVariant := FmCalcParc4Val.Ed04Peca.ValueVariant;
      EdQtdAntPeso.ValueVariant := FmCalcParc4Val.Ed04Peso.ValueVariant;
    end;
    FmCalcParc4Val.Destroy;
  end;
end;

procedure TFmVSCurOriIMEI.CBGraGruXClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSCurOriIMEI.CBMarcaClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSCurOriIMEI.CkSoClientMOClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSCurOriIMEI.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSCurOriIMEI.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSCurOriIMEI.DefineTipoArea();
begin
(*
  LaAreaM2.Enabled := FTipoArea = 0;
  EdAreaM2.Enabled := FTipoArea = 0;

  LaAreaP2.Enabled := FTipoArea = 1;
  EdAreaP2.Enabled := FTipoArea = 1;
*)
end;

procedure TFmVSCurOriIMEI.EdClientMORedefinido(Sender: TObject);
begin
  ReopenMarcas();
end;

procedure TFmVSCurOriIMEI.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCurOriIMEI.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCurOriIMEI.EdPecasChange(Sender: TObject);
begin
  EdPesoKg.ValueVariant := PesoOriginalParcial(EdPecas.ValueVariant);
end;

procedure TFmVSCurOriIMEI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    CalculaParcial();
  if Key = VK_F4 then
  begin
    (*ini 2023-05-14
    //EdPecas.ValueVariant := QrEstoqueSdoVrtPeca.Value;
    //EdPesoKg.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    ReopenVMISum();
    EdPecas.ValueVariant := QrVMISumSdoPeca.Value;
    EdPesoKg.ValueVariant := QrVMISumSdoPeso.Value;
    *)
    // Quando tira couro l� no caleiro no bot�o destino, o saldo do QrEstoque
    // fica menor que o do QrVMISum!
    EdPecas.ValueVariant := QrEstoqueSdoVrtPeca.Value;
    EdPesoKg.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    // fim 2023-05-14
  end;
end;

procedure TFmVSCurOriIMEI.EdQtdAntPesoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PecasTot, PecasPar, PesoTot, PesoPar: Double;
begin
  if Key = VK_F3 then
  begin
    //PecasTot    := QrEstoqueSdoVrtPeca.Value;
    ReopenVMISum();
    PecasTot    := QrVMISumSdoPeca.Value;
    PecasPar    := EdPecas.ValueVariant;
    //PesoTot     := QrEstoqueSdoVrtPeso.Value;
    PesoTot     := QrVMISumSdoPeso.Value;
    if PecasTot > 0 then
      PesoPar     := PecasPar / PecasTot * PesoTot
    else
      PesoPar := 0;
    //
    EdQtdAntPeso.ValueVariant := PesoPar;
  end;
end;

procedure TFmVSCurOriIMEI.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCurOriIMEI.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCurOriIMEI.FechaPesquisa();
var
  GraGruX, SerieFch, Ficha: Integer;
begin
  QrEstoque.Close;
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    FSQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    FSQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  SerieFch := EdSerieFch.ValueVariant;
  if SerieFch <> 0 then
    FSQL_SerieFch := 'AND vmi.SerieFch=' + Geral.FF0(SerieFch)
  else
    FSQL_SerieFch := 'AND vmi.SerieFch<>0 ';
  //
  Ficha := EdFicha.ValueVariant;
  if Ficha <> 0 then
    FSQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(Ficha)
  else
    FSQL_Ficha := '';
end;

procedure TFmVSCurOriIMEI.FormActivate(Sender: TObject);
var
  Reabre: Boolean;
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
  Reabre := not FFormAtivado;
  FFormAtivado := True;
  //
  if Reabre then
    ReopenItensAptos();
  //
  if (FFormaBaixa = frmabxaMarcaSemContar)  and
  (EdPesoMarcaSemContarPecas.Enabled)
  and (EdPesoMarcaSemContarPecas.Visible) then
    EdPesoMarcaSemContarPecas.SetFocus;
end;

procedure TFmVSCurOriIMEI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FFormaBaixa := frmabxaIndef;
  //
  FGraGruY := CO_GraGruY_1365_VSProCal;
  VS_PF.AbreVSSerFch(QrVSSerFch);
  ReopenGraGruX();
  //
  VS_PF.AbreGraGruXY(QrRmsGGX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1621_VSCouDTA));
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  ReopenCliInt();
  ReopenMarcas();
end;

procedure TFmVSCurOriIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCurOriIMEI.InsereIMEI_DoCal(InsereTudo: Boolean; ParcPecas,
  ParcPesoKg, ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  ExigeFornecedor = False;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  QtdAntArM2  = 0;
  QtdAntArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
var
  DataHora, Marca: String;
  MovimTwn, SrcMovID, SrcNivel1, SrcNivel2, CalStqCenLoc,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, ReqMovEstq, VSMulFrnCab, ClientMO, SrcMovimCod: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor, QtdAntPeca, QtdAntPeso,
  FatorPeca, FatorPeso, FatorArM2, FatorArP2, FatorArre,  PcQuebr, FatorQuebr,
  BasePeca, BasePeso, BaseArM2, BaseArP2,
  RestoPeca, RestoPeso, RestoArM2, RestoArP2, RestoAntPeca, RestoAntPeso,
  FatorItem: Double;
  BaixarTudo: Boolean;
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX, RmsStqCenLoc, FornecMO, StqCenLoc: Integer;
begin
  StqCenLoc := FStqCenLoc;
  FornecMO  := FFornecMO;
  //
  RestoPeca := ParcPecas;
  RestoPeso := ParcPesoKg;
  RestoArM2 := ParcAreaM2;
  RestoArP2 := ParcAreaP2;
  RestoAntPeca := ParcQtdAntPeca;
  RestoAntPeso := ParcQtdAntPeso;
  //
  ReopenVMICal();
  ReopenVMISum();
  //FatorPeca := ParcQtdAntPeca / QrVMISumSdoPeca.Value;
  //FatorPeso := ParcQtdAntPeso / QrVMISumSdoPeso.Value;
  //
  //Geral.MB_SQL(self, QrVMICal);
  if InsereTudo then
    BaixarTudo := True
  else
    BaixarTudo := QrVMISumSdoPeca.Value <= ParcPecas;
  //
  QrVMICal.First;
  while not QrVMICal.Eof do
  begin
    if QrVMICal.recordCount > 1 then
      FatorItem := (QrVMICalQtdAntPeca.Value - QrVMICalQtdGerPeca.Value) / QrVMISumSdoPeca.Value
    else
      FatorItem := 1;
    //
    BasePeca := (*-*)(QrVMICalQtdAntPeca.Value - QrVMICalQtdGerPeca.Value);
    BasePeso := (*-*)(QrVMICalQtdAntPeso.Value - QrVMICalQtdGerPeso.Value);
    BaseArM2 := (*-*)(QrVMICalQtdAntArM2.Value - QrVMICalQtdGerArM2.Value);
    BaseArP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    //

    FatorPeca := 0;
    FatorPeso := 0;
    FatorArM2 := 0;
    FatorArP2 := 0;
    if QrVMISumSdoPeca.Value <> 0 then
      FatorPeca := BasePeca / QrVMISumSdoPeca.Value;
    if QrVMISumSdoPeso.Value <> 0 then
      FatorPeso := BasePeso / QrVMISumSdoPeso.Value;
    if QrVMISumSdoArM2.Value <> 0 then
      FatorArM2 := BaseArM2 / QrVMISumSdoArM2.Value;
    if QrVMISumSdoArP2.Value <> 0 then
      FatorArP2 := BaseArP2 / QrVMISumSdoArP2.Value;
    //
    SrcMovimCod     := QrVMICalMovimCod.Value;
    //
    SrcMovID       := QrVMICalMovimID.Value;
    SrcNivel1      := QrVMICalCodigo.Value;
    SrcNivel2      := QrVMICalControle.Value;
    SrcGGX         := QrVMICalGraGruX.Value;
    //
    Codigo         := EdCodigo.ValueVariant;
    Controle       := 0; //EdControle.ValueVariant;
    MovimTwn       := 0; //EdMovimTwn.ValueVariant;
    MovimCod       := EdMovimCod.ValueVariant;
    Empresa        := FEmpresa;
    Terceiro       := QrVMICalTerceiro.Value;
    VSMulFrnCab    := QrVMICalVSMulFrnCab.Value;
    DataHora       := Geral.FDT(FDataHora, 109);
    MovimID        := emidEmProcCur;
    MovimNiv       := eminSorcCur;
    Pallet         := QrVMICalPallet.Value;
    GraGruX        := QrVMICalGraGruX.Value;
    //if not InsereTudo then
    if not BaixarTudo then
    begin
      if QrVMICal.RecNo = QrVMICal.RecordCount then
      begin
        Pecas  := RestoPeca;
        PesoKg := RestoPeso;
        AreaM2 := RestoArM2;
        AreaP2 := RestoArP2;
        QtdAntPeca := RestoAntPeca;
        QtdAntPeso := RestoAntPeso;
      end else
      begin
        PcQuebr      := ParcPecas  * FatorPeca;
        Pecas        := Round(PcQuebr);
        if PcQuebr <> 0 then
          FatorQuebr    := Pecas / PcQuebr
        else
          FatorQuebr    := 0;
        if QrVMICalQtdAntPeca.Value = 0 then
          FatorArre := 0
        else
          FatorArre := Pecas / QrVMICalQtdAntPeca.Value;
        //PesoKg       := -ParcPesoKg * FatorPeso * FatorArre;
        PesoKg       :=  FatorArre * QrVMICalQtdAntPeso.Value;
        //AreaM2       := -ParcAreaM2 * FatorArM2 * FatorArre;
        AreaM2       := FatorArre * QrVMICalQtdAntArM2.Value;
        AreaP2       := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
        //
        FatorArre    := ParcQtdAntPeca * FatorPeca;
        QtdAntPeca   := Round(FatorArre);
        FatorArre    := QrVMICalQtdAntPeso.Value / QrVMISumQtdAntPeso.Value;
        QtdAntPeso   := ParcQtdAntPeso * FatorArre * FatorQuebr;
      end;
      RestoPeca := RestoPeca - Pecas;
      RestoPeso := RestoPeso - PesoKg;
      RestoArM2 := RestoArM2 - AreaM2;
      RestoArP2 := RestoArP2 - AreaP2;
      RestoAntPeca := RestoAntPeca - QtdAntPeca;
      RestoAntPeso := RestoAntPeso - QtdAntPeso;
      //
      Pecas  := -Pecas;
      PesoKg := -PesoKg;
      AreaM2 := -AreaM2;
      AreaP2 := -AreaP2;
    end else
    begin
      Pecas  := -(QrVMICalQtdAntPeca.Value - QrVMICalQtdGerPeca.Value);
      PesoKg := -(QrVMICalQtdAntPeso.Value - QrVMICalQtdGerPeso.Value);
      AreaM2 := -(QrVMICalQtdAntArM2.Value - QrVMICalQtdGerArM2.Value);
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
      //
      if InsereTudo then
      begin
        QtdAntPeca     := -Pecas;
        QtdAntPeso     := -PesoKg;
      end else
      begin
        //QtdAntPeca     := ParcQtdAntPeca;
        QtdAntPeca     := ParcQtdAntPeca * FatorItem;
        //QtdAntPeso     := ParcQtdAntPeso;
        QtdAntPeso     := ParcQtdAntPeso * FatorItem;
      end;
    end;
    if (QrVMICalQtdAntPeso.Value > 0) then
      Valor := -PesoKg *
      (QrVMICalValorT.Value / QrVMICalQtdAntPeso.Value)
    else
    if (QrVMICalQtdAntArM2.Value > 0) then
      Valor := -AreaM2 *
      (QrVMICalValorT.Value / QrVMICalQtdAntArM2.Value)
    else
    if QrVMICalQtdAntPeca.Value > 0 then
      Valor := -Pecas *
      (QrVMICalValorT.Value / QrVMICalQtdAntPeca.Value)
    else
      Valor := 0;
    if Valor < 0 then
      ValorMP        := Valor
    else
      ValorMP        := -Valor;
    ValorT         := ValorMP;
    //
    SerieFch       := QrVMICalSerieFch.Value;
    Ficha          := QrVMICalFicha.Value;
    Marca          := QrVMICalMarca.Value;
    //Misturou       := QrVMICalMisturou.Value;
    DstMovID       := TEstqMovimID(FmVSCurCab.QrVSCurAtuMovimID.Value);
    DstNivel1      := FmVSCurCab.QrVSCurAtuCodigo.Value;
    DstNivel2      := FmVSCurCab.QrVSCurAtuControle.Value;
    DstGGX         := FmVSCurCab.QrVSCurAtuGraGruX.Value;
    RmsGGX         := EdRmsGGX.ValueVariant;
    //
    GraGruY        := QrVMICalGraGruY.Value;
    //
    ReqMovEstq     := EdReqMovEstq.ValueVariant;
    ClientMO       := QrVMICalClientMO.Value;
    CalStqCenLoc   := QrVMICalStqCenLoc.Value;
    RmsStqCenLoc   := QrVMICalStqCenLoc.Value;
    //
    MovimTwn := 0; //UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

    if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
    CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
    QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
    PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei011(*Mat�ria prima origem no processo de curtimento*)) then
    begin
      //  Criar lancamento de couro caleado com JmpMovID, JmpNivel1 e JmpNivel2
      // ini 2023-05-15
      (*
      VS_PF.CriaVMIJmpCal(Controle, FGGXJmpSrc, FGGXJmpDst,
       FmVSCurCab.QrVSCurCabGraGruX.Value,
      -QrVMICalPecas.Value, -QrVMICalAreaM2.Value, -QrVMICalPesoKg.Value,
      -Pecas, -AreaM2, -PesoKg, -ValorT, CalStqCenLoc, QrEstoqueMovimCod.Value,
      QrEstoqueControle.Value);
      *) //Mudei o InnPecas e InnPeso e adicionei ValorT espec�fico (InnValorT) por causa do Kardex!
      VS_PF.CriaVMIJmpCa2(Controle, FGGXJmpSrc, FGGXJmpDst,
       FmVSCurCab.QrVSCurCabGraGruX.Value,
      //-QrVMICalPecas.Value, -QrVMICalAreaM2.Value, -QrVMICalPesoKg.Value,
      QrEstoquePecas.Value, QrEstoqueAreaM2.Value, QrEstoquePesoKg.Value, QrEstoqueValorT.Value,
      -Pecas, -AreaM2, -PesoKg, -ValorT, CalStqCenLoc, QrEstoqueMovimCod.Value,
      QrEstoqueControle.Value, QrEstoqueGraGruX.Value);
      // fim 2023-05-15

      //  Criar lancamento de couro DTA com RmsMovID, RmsNivel1 e RmsNivel2
      VS_PF.CriaVMIRmsCur(Controle, FGGXRmsSrc, FGGXRmsDst,
       RmsGGX, //FmVSCalCab.QrVSCalCabGraGruX.Value,
      QrEstoquePecas.Value, QrEstoqueAreaM2.Value, QrEstoquePesoKg.Value,
      -Pecas, -AreaM2, -PesoKg, -ValorT, RmsStqCenLoc);
      //

      // N�o controla estoque pelo IMEI do Inn Natura diretamente!
      //VS_PF.AtualizaSaldoIMEI(SrcNivel2, True);
      VS_PF.AtualizaSaldoItmCal(SrcNivel2);
      DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
    end;
    //
    QrVMICal.Next;
  end;
  VS_PF.AtualizaVSCurCabGGXSrc(MovimCod);
end;

procedure TFmVSCurOriIMEI.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status, Setar: Boolean;
begin
  Setar := False;
  if FFormaBaixa = frmabxaParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    Setar := True;
  end;
  (*
  if FMarcaSemContarPeca then
  begin
    if Libera then
      Status := DBG04Estq.SelectedRows.Count > 0
    else
      Status := False;
    //
    Setar := True;
  end;
  *)
  if Setar then
  begin
    GBAptos.Enabled := (not Status) or (FFormaBaixa = frmabxaMarcaSemContar);
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled := not Status;
    EdFicha.Enabled := not Status;
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdPecas.Enabled) and (EdPecas.Visible) then
      EdPecas.SetFocus;
    //
    BtOK.Enabled := Status;
  end;
end;

function TFmVSCurOriIMEI.PesoOriginalParcial(Pecas: Double): Double;
var
  //Pecas,
  PesoKg: Double;
begin
  //Pecas  := EdPecas.ValueVariant;
  PesoKg := QrEstoquePesoKg.Value /QrEstoquePecas.Value;
  Result := Pecas * PesoKg;
end;

procedure TFmVSCurOriIMEI.PMCalculaPopup(Sender: TObject);
begin
  ASaberaspeasdeumaunicamarcapelopeso1.Enabled := FFormaBaixa <> frmabxaMarcaSemContar;
end;

procedure TFmVSCurOriIMEI.QrEstoqueAfterScroll(DataSet: TDataSet);
begin
(*
  if QrEstoque.RecordCount > 5 then
  begin
    BtReopenOriCal.Visible := True;
    DBGOriCal.Visible := False;
  end else
    ReopenOriCal();
*)
  //PnOrical.Visible:= False;
  DBGOrical.Visible:= False;
  //PnOriEmi.Visible:= False;
  DBGOriEmi.Visible:= False;
  TmEstoqueAfterScrool.Enabled := False;
  TmEstoqueAfterScrool.Enabled := True;
end;

procedure TFmVSCurOriIMEI.QrEstoqueBeforeClose(DataSet: TDataSet);
begin
  QrOriCal.Close;
  QrEmiOri.Close;
end;

procedure TFmVSCurOriIMEI.QrGGXJmpAfterOpen(DataSet: TDataSet);
begin
{
  if QrGGXJmp.RecordCount = 1 then
  begin
    EdJmpGGX.ValueVariant := QrGGXJmpControle.Value;
    CBJmpGGX.KeyValue     := QrGGXJmpControle.Value;
  end;
}
end;

procedure TFmVSCurOriIMEI.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSCurOriIMEI.ReopenCliInt;
begin
  //UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliInt, Dmod.MyDB, [
  'SELECT DISTINCT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECI, ent.Codigo',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.ClientMO',
  'WHERE (vmi.SdoVrtPeca>0 OR vmi.SdoVrtPeso> 0)',
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)),
  'AND vmi.ClientMO<>0',
  '']);
end;

procedure TFmVSCurOriIMEI.ReopenEmiOrigem();
begin
  if QrEstoqueMovimCod.Value = 0 then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmiOri, Dmod.MyDB, [
  'SELECT emi.DataEmis, emi.Fulao, emi.Codigo, ',
  'emi.Numero, emi.Nome, emi.Obs',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0(QrEstoqueMovimCod.Value),
  'ORDER BY emi.Codigo DESC ',
  '']);
end;

procedure TFmVSCurOriIMEI.ReopenGraGruX();
begin
  VS_PF.AbreGraGruXY(QrGGXInn,
    'AND ggx.GragruY IN (' +
    //Geral.FF0(CO_GraGruY_1365_VS??????) + ',' +
    Geral.FF0(FGraGruY) +
    ')');
  VS_PF.AbreGraGruXY(QrGGXJmp,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1536_VSCouCal) + //',' +
    ')' + sLineBreak +
    'AND nv2.Codigo IN (1,2) ' // Couro (1) e raspa (2)
    );
  //Geral.MB_SQL(Self, QrGGXJmp);
end;

procedure TFmVSCurOriIMEI.ReopenItensAptos();
var
  GraGruX, SerieFch, Ficha, ClientMO: Integer;
  SQL_GraGruX, SQL_SerieFch, SQL_Ficha, SQLSerieEFicha, SQL_ClientMO, Marca,
  SQL_Marca, Corda: String;
begin
  if not FFormAtivado then
    Exit;
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  Ficha := EdFicha.ValueVariant;
  SerieFch := EdSerieFch.ValueVariant;
  if (Ficha <> 0) or (SerieFch <> 0) then
  begin
    SQLSerieEFicha := '';
    if SerieFch <> 0 then
      SQL_SerieFch := '  AND vmi.SerieFch=' + Geral.FF0(SerieFch)
    else
      SQL_SerieFch := '';
    if Ficha <> 0 then
      SQL_Ficha := '  AND vmi.Ficha=' + Geral.FF0(Ficha)
    else
      SQL_Ficha := '';
{  ini 2020 09 03
    SQLSerieEFicha := Geral.ATS([
      'AND vmi.MovimCod IN ( ',
      'SELECT MovimCod ',
      '  FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
      '  WHERE QtdAntPeca>QtdGerPeca ',
      SQL_SerieFch,
      SQL_Ficha,
      ')  '])
}
     UnDmkDAC_PF.AbreMySQLQuery0(QrFichas, Dmod.MyDB, [
     'SELECT MovimCod ',
      '  FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
      '  WHERE QtdAntPeca>QtdGerPeca ',
      SQL_SerieFch,
      SQL_Ficha,
      '']);
      if QrFichas.RecordCount > 0 then
      begin
        Corda := MyObjects.CordaDeQuery(QrFichas, 'MovimCod', EmptyStr);
        SQLSerieEFicha := 'AND vmi.MovimCod IN ( ' + Corda + ')';
      end;
  end else
    SQLSerieEFicha := '';
  //
  //
  if CkSoClientMO.Checked then
    SQL_ClientMO := 'AND vmi.ClientMO=' +
      Geral.FF0(FmVSCurCab.QrVSCurAtuClientMO.Value)
  else begin
    ClientMO := EdClientMO.ValueVariant;
    if ClientMO <> 0 then
      SQL_ClientMO := '  AND vmi.ClientMO=' + Geral.FF0(ClientMO)
    else
      SQL_ClientMO := '';
  end;
  //
  Marca := CBMarca.Text;
  if Trim(Marca) <> '' then
    SQL_Marca :=
    '  AND UPPER(REPLACE(Marca, " ", ""))=UPPER(REPLACE("' + Marca + '", " ", ""))'
  else
    SQL_Marca := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.Empresa, vmi.ClientMO, ',
  'vmi.GraGruX, vmi.PesoKg, vmi.Pecas, vmi.AreaM2, ',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
  'FLOOR((vmi.SdoVrtArM2 / 0.09290304)) + ',
  'FLOOR(((MOD((vmi.SdoVrtArM2 / 0.09290304), 1)) + ',
  '0.12499) * 4) * 0.25 SdoVrtArP2, ValorT, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
  'vmi.Terceiro, ',
  //'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"" NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, ',
  'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
  'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ',
  'vmi.VSMulFrnCab, vmi.SerieFch, vmi.Ficha, vmi.Marca, ',
  'vmi.MovimCod, vsf.Nome NO_SerieFch, ',
  '1 Ativo ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades   frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab',
  'WHERE ggx.GragruY IN (' +
  //Geral.FF0(CO_GraGruY_1365_VS??????) + ',' +
  Geral.FF0(FGraGruY) + ')',
  'AND vmi.Controle <> 0 ',
  SQL_GraGruX,
  SQLSerieEFicha,
  SQL_ClientMO,
  SQL_Marca,
  'AND vmi.SdoVrtPeca > 0 ',
  'AND vmi.Pallet = 0 ',
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  ' ']);
end;

procedure TFmVSCurOriIMEI.ReopenMarcas();
var
  ClientMO: Integer;
  SQL_ClientMO: String;
begin
  ClientMO := EdClientMO.ValueVariant;
  if ClientMO <> 0 then
    SQL_ClientMO := 'AND ClientMO=' + Geral.FF0(ClientMO)
  else
    SQL_ClientMO := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrMarcas, Dmod.MyDB, [
  'SELECT Marca,  ',
  'UPPER(REPLACE(Marca, " ", "")) Martelo',
  'FROM ' + CO_SEL_TAB_VMI + '',
  'WHERE (SdoVrtPeca>0 OR SdoVrtPeso> 0) ',
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)),
  SQL_ClientMO,
  'GROUP BY Martelo ',
  'ORDER BY Martelo ',
  '']);
end;

procedure TFmVSCurOriIMEI.ReopenMediaMarca(Marca: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMedia, Dmod.MyDB, [
  'SELECT Marca,  ',
  'UPPER(REPLACE(Marca, " ", "")) Martelo,  ',
  'SUM(Pecas)Pecas, SUM(PesoKg) PesoKg, ',
  'IF(SUM(Pecas)=0,0,SUM(PesoKg)/ SUM(Pecas)) Media ',
  'FROM ' + CO_SEL_TAB_VMI + '',
  'WHERE UPPER(REPLACE(Marca, " ", "")) =',
  'UPPER(REPLACE("' + Marca + '", " ", ""))',
  'GROUP BY Martelo ',
  'ORDER BY Martelo ',
  '']);
end;

procedure TFmVSCurOriIMEI.ReopenOriCal();
var
  GraGruX, Ficha: Integer;
  SQL_GraGruX: String;
  SQL_Ficha: String;
begin
  if QrEstoqueMovimCod.Value = 0 then Exit;
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  Ficha := EdFicha.ValueVariant;
  if Ficha <> 0 then
    SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(Ficha)
  else
    SQL_Ficha := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriCal, Dmod.MyDB, [
  'SELECT vmi.Ficha, vmi.Marca, ',
  'ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  ',
  'ggx.GraGruY  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrEstoqueMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),
  SQL_GraGruX,
  SQL_Ficha,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle  ',
  '']);
  //Geral.MB_SQL(self, QrOriCal);
end;

procedure TFmVSCurOriIMEI.ReopenVMICal();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMICal, Dmod.MyDB, [
  'SELECT ggx.GraGruY, vmi.* ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCal)),  //26
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),     //29
  'AND vmi.MovimCod=' + Geral.FF0(QrEstoqueMovimCod.Value),
  'AND vmi.QtdAntPeca > vmi.QtdGerPeca ',
  FSQL_GraGruX,
  FSQL_SerieFch,
  FSQL_Ficha,
  '']);
end;

procedure TFmVSCurOriIMEI.ReopenVMISum();
begin
  if QrEstoqueMovimCod.Value = 0 then Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMISum, Dmod.MyDB, [
  'SELECT SUM(vmi.QtdAntPeso) QtdAntPeso, ',
  'SUM(vmi.QtdAntPeca - vmi.QtdGerPeca) SdoPeca, ',
  'SUM(vmi.QtdAntPeso - vmi.QtdGerPeso) SdoPeso, ',
  'SUM(vmi.QtdAntArM2 - vmi.QtdGerArM2) SdoArM2, ',
  'SUM(vmi.QtdAntArP2 - vmi.QtdGerArP2) SdoArP2  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCal)),  //26
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),     //29
  'AND vmi.MovimCod=' + Geral.FF0(QrEstoqueMovimCod.Value),
  'AND vmi.QtdAntPeca > vmi.QtdGerPeca ',
  FSQL_GraGruX,
  FSQL_SerieFch,
  FSQL_Ficha,
  '']);
end;

procedure TFmVSCurOriIMEI.RGArtOrigemClick(Sender: TObject);
begin
  case RGArtOrigem.ItemIndex of
    0: FGraGruY := CO_GraGruY_1365_VSProCal;
    1: FGraGruY := CO_GraGruY_1024_VSNatCad;
    2: FGraGruY := CO_GraGruY_1088_VSNatCon;
    else
    begin
      Geral.MB_ERRO('"Origem" n�o implementada!');
      FGraGruY := -999999999;
    end;
  end;
  QrEstoque.Close;
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
  //
  ReopengragruX();
end;

procedure TFmVSCurOriIMEI.SbCalculaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCalcula, SbCalcula);
end;

procedure TFmVSCurOriIMEI.TmEstoqueAfterScroolTimer(Sender: TObject);
begin
  TmEstoqueAfterScrool.Enabled := False;
  ReopenOriCal();
  ReopenVMISum();
  ReopenEmiOrigem(); // 2023-11-20
  //QrEstoqueSDOPECA.Value := QrVMISumSdoPeca.Value;
  //QrEstoqueSDOPESO.Value := QrVMISumSdoPeso.Value;
  EdEstoqueSDOPECA.ValueVariant := QrVMISumSdoPeca.Value;
  EdEstoqueSDOPESO.ValueVariant := QrVMISumSdoPeso.Value;
  //PnOrical.Visible:= True;
  DBGOrical.Visible:= True;
  //PnOriEmi.Visible:= True;
  DBGOriEmi.Visible:= True;
end;

(*
object QrEstoqueSdoVrtArM2: TFloatField
  FieldName = 'SdoVrtArM2'
end
object QrEstoqueSdoVrtArP2: TFloatField
  FieldName = 'SdoVrtArP2'
end
object QrEstoqueSdoVrtPeca: TFloatField
  FieldName = 'SdoVrtPeca'
end
object QrEstoqueSdoVrtPeso: TFloatField
  FieldName = 'SdoVrtPeso'
end
*)

{

//////////////
Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280_New()"
/////////////////
Erro. O registro j� existe, ou tem chave que n�o pode ser duplicada!
Nome do controle: BtOK
:
/*Dmod.QrUpd*/
/*********** SQL ***********/
INSERT INTO efdicmsipik280 SET
KndAID=6,
KndNiv=0,
COD_ITEM="108",
QTD_COR_POS=0.000000000000000,
QTD_COR_NEG=5142.857000000000000,
IND_EST="0",
COD_PART="",
Grandeza=3,
Pecas=0.000000000000000,
AreaM2=0.000000000000000,
PesoKg=0.000000000000000,
Entidade=-11,
Tipo_Item=0,
RegisPai="K275",
RegisAvo="K292",
ESTSTabSorc=1,
OriOpeProc=7,
OrigemIDKnd=0,
OriSPEDEFDKnd=3,
OriBalID=4,
OriKndReg=7,
IDSeq1=22,
DataCad="2017-11-26", UserCad=-1
, ImporExpor=3
, AnoMes=201709
, Empresa=-11
, PeriApu=1
, KndTab=1
, KndCod=4
, KndNSU=53
, KndItm=147
, DT_EST="2017-08-31"
, DebCred=1
, GraGruX=108
;
/********* FIM SQL *********/

/*****Query sem parametros*******/
}


{
object QrEstoqueSDOPECA: TFloatField
  FieldKind = fkCalculated
  FieldName = 'SDOPECA'
  Calculated = True
end
object QrEstoqueSDOPESO: TFloatField
  FieldKind = fkCalculated
  FieldName = 'SDOPESO'
  DisplayFormat = '#,###,##0.000'
  Calculated = True
end
}
end.
