object FmVSRastroSdo: TFmVSRastroSdo
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-250 :: Rastreio de Saldo de Artigo'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Rastreio de Saldo de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Rastreio de Saldo de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Rastreio de Saldo de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 488
    Width = 812
    Height = 71
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 37
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 20
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 356
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Teste &1'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
        OnClick = BtOKClick
      end
      object BtTeste2: TBitBtn
        Tag = 22
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTeste2Click
      end
      object BtAtzSdo: TBitBtn
        Tag = 18
        Left = 128
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Atauliza saldo'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtzSdoClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 440
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 40
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label8: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 56
        Width = 481
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 16
        Width = 477
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 0
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 81
      Width = 812
      Height = 240
      Align = alTop
      TabOrder = 1
      Visible = False
      object Label1: TLabel
        Left = 1
        Top = 1
        Width = 85
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Est'#225'gios de Artigo'
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 1
        Top = 14
        Width = 810
        Height = 225
        Align = alClient
        DataSource = DsMIDs
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 321
      Width = 812
      Height = 119
      Align = alClient
      TabOrder = 2
      object Label3: TLabel
        Left = 1
        Top = 1
        Width = 88
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'IMEI-s divergentes'
      end
      object dmkDBGridZTO2: TdmkDBGridZTO
        Left = 1
        Top = 14
        Width = 810
        Height = 104
        Align = alClient
        DataSource = DsDiverge
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, ggy.Nome NO_GraGruY, '
      'ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'unm.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 212
    Top = 60
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object QrGraGruXNO_GraGruY: TWideStringField
      FieldName = 'NO_GraGruY'
      Size = 255
    end
    object QrGraGruXGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 212
    Top = 112
  end
  object QrMIDs: TMySQLQuery
    Database = DModG.MyPID_DB
    AutoCalcFields = False
    SQL.Strings = (
      'DROP TABLE IF EXISTS _teste_; '
      'CREATE TABLE _teste_ '
      'SELECT (MovimID * 1000) + MovimNiv MovIDNiv,  '
      'MovimID, MovimNiv,  '
      'COUNT(Controle) Itens, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(SdoVrtPeca) SdoVrtPeca, '
      'SUM(SdoVrtArM2) SdoVrtArM2  '
      'FROM vsmovits '
      'WHERE GraGruX=205 '
      'GROUP BY MovimID, MovimNiv '
      ' '
      'UNION '
      ' '
      'SELECT 999 MovimID, 999 MovimNiv,  '
      '999999 MovIDNiv,  '
      'COUNT(Controle) Itens, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(SdoVrtPeca) SdoVrtPeca, '
      'SUM(SdoVrtArM2) SdoVrtArM2  '
      'FROM vsmovits '
      'WHERE GraGruX=205; '
      ' '
      'SELECT * FROM _teste_ '
      'ORDER BY MovimID, MovimNiv; '
      ' ')
    Left = 124
    Top = 252
    object QrMIDsMovIDNiv: TLargeintField
      FieldName = 'MovIDNiv'
      Required = True
    end
    object QrMIDsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrMIDsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrMIDsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrMIDsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMIDsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrMIDsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrMIDsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
  end
  object DsMIDs: TDataSource
    DataSet = QrMIDs
    Left = 124
    Top = 300
  end
  object QrSumA: TMySQLQuery
    Database = Dmod.MyDB
    AutoCalcFields = False
    SQL.Strings = (
      'SELECT Codigo, Controle, MovimCod, '
      'Pecas, PesoKg, AreaM2, SdoVrtPeca, '
      'SdoVrtPeso, SdoVrtArM2  '
      'FROM vsmovits '
      'WHERE GraGruX=205 '
      'AND MovimID=7 '
      'AND MovimNiv=2 ')
    Left = 124
    Top = 404
    object QrSumAPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrSumAPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrSumAAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
  end
  object QrRastroA: TMySQLQuery
    Database = Dmod.MyDB
    AutoCalcFields = False
    SQL.Strings = (
      'SELECT Codigo, Controle, MovimCod, '
      'Pecas, PesoKg, AreaM2, SdoVrtPeca, '
      'SdoVrtPeso, SdoVrtArM2  '
      'FROM vsmovits '
      'WHERE GraGruX=205 '
      'AND MovimID=7 '
      'AND MovimNiv=2 ')
    Left = 124
    Top = 352
    object QrRastroACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRastroAControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRastroAMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrRastroAPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrRastroAPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrRastroAAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrRastroASdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrRastroASdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrRastroASdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrRastroAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrRastroAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrRastroAValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrBxaAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _rastro_negat_;')
    Left = 204
    Top = 256
    object QrBxaAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBxaAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBxaAllMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrBxaAllMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrBxaAllMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrBxaAllPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrBxaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrBxaAllValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrBxaAllSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrBxaAllSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrBxaAllSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrBxaAllSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrBxaAllSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrBxaAllDstMovID: TIntegerField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrBxaAllDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrBxaAllDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
      Required = True
    end
  end
  object QrDiverge: TMySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrDivergeAfterOpen
    BeforeClose = QrDivergeBeforeClose
    SQL.Strings = (
      'SELECT _rs.*, vmi.SdoVrtPeca VmiSVPc,'
      'vmi.SdoVrtArM2 VmiSVM2 '
      'FROM _rastro_saldo_ _rs'
      ''
      'LEFT JOIN bluederm.vsmovits vmi ON vmi.Controle=_rs.Controle'
      'WHERE vmi.SdoVrtPeca<>_rs.SdoVrtPeca')
    Left = 208
    Top = 321
    object QrDivergeControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDivergeMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrDivergeMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrDivergePallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrDivergeSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrDivergeSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrDivergeVmiSVPc: TFloatField
      FieldName = 'VmiSVPc'
    end
    object QrDivergeVmiSVM2: TFloatField
      FieldName = 'VmiSVM2'
    end
  end
  object DsDiverge: TDataSource
    DataSet = QrDiverge
    Left = 208
    Top = 373
  end
  object PMAtzSdo: TPopupMenu
    Left = 278
    Top = 559
    object Atualizasaldodoitemselecionado1: TMenuItem
      Caption = 'Atualiza saldo do item selecionado'
      OnClick = Atualizasaldodoitemselecionado1Click
    end
    object Atualizasaldodetodositens1: TMenuItem
      Caption = 'Atualiza saldo de todos itens'
      OnClick = Atualizasaldodetodositens1Click
    end
  end
end
