unit VSEstqCustoIntegr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, mySQLDbTables, dmkDBLookupComboBox, dmkEditCB,
  frxClass, frxDBSet, dmkCheckGroup, UnEmpresas, dmkDBGridZTO,
  AppListas, UnDmkProcFunc, UnPQ_PF;

type
  TFmVSEstqCustoIntegr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    QrFichas: TmySQLQuery;
    QrFichasSerieFch: TIntegerField;
    QrFichasFicha: TIntegerField;
    QrVSInn: TmySQLQuery;
    QrVSInnPecas: TFloatField;
    QrVSInnPesoKg: TFloatField;
    QrVSInnAreaM2: TFloatField;
    QrVSInnValorT: TFloatField;
    QrIMECs: TmySQLQuery;
    QrIMECsMovimCod: TIntegerField;
    QrVSMovImp: TmySQLQuery;
    QrVSMovImpEmpresa: TIntegerField;
    QrVSMovImpGraGruX: TIntegerField;
    QrVSMovImpPecas: TFloatField;
    QrVSMovImpPesoKg: TFloatField;
    QrVSMovImpAreaM2: TFloatField;
    QrVSMovImpAreaP2: TFloatField;
    QrVSMovImpValorT: TFloatField;
    QrVSMovImpSdoVrtPeca: TFloatField;
    QrVSMovImpSdoVrtPeso: TFloatField;
    QrVSMovImpSdoVrtArM2: TFloatField;
    QrVSMovImpLmbVrtPeca: TFloatField;
    QrVSMovImpLmbVrtPeso: TFloatField;
    QrVSMovImpLmbVrtArM2: TFloatField;
    QrVSMovImpGraGru1: TIntegerField;
    QrVSMovImpNO_PRD_TAM_COR: TWideStringField;
    QrVSMovImpPallet: TIntegerField;
    QrVSMovImpNO_PALLET: TWideStringField;
    QrVSMovImpTerceiro: TIntegerField;
    QrVSMovImpCliStat: TIntegerField;
    QrVSMovImpStatus: TIntegerField;
    QrVSMovImpNO_FORNECE: TWideStringField;
    QrVSMovImpNO_CLISTAT: TWideStringField;
    QrVSMovImpNO_EMPRESA: TWideStringField;
    QrVSMovImpNO_STATUS: TWideStringField;
    QrVSMovImpDataHora: TDateTimeField;
    QrVSMovImpOrdGGX: TIntegerField;
    QrVSMovImpOrdGGY: TIntegerField;
    QrVSMovImpGraGruY: TIntegerField;
    QrVSMovImpNO_GGY: TWideStringField;
    QrVSMovImpPalStat: TIntegerField;
    QrVSMovImpCouNiv2: TIntegerField;
    QrVSMovImpCouNiv1: TIntegerField;
    QrVSMovImpNO_CouNiv2: TWideStringField;
    QrVSMovImpNO_CouNiv1: TWideStringField;
    QrVSMovImpFatorInt: TFloatField;
    QrVSMovImpSdoInteiros: TFloatField;
    QrVSMovImpLmbInteiros: TFloatField;
    QrVSMovImpCodigo: TIntegerField;
    QrVSMovImpIMEC: TIntegerField;
    QrVSMovImpIMEI: TIntegerField;
    QrVSMovImpMovimID: TIntegerField;
    QrVSMovImpMovimNiv: TIntegerField;
    QrVSMovImpNO_MovimID: TWideStringField;
    QrVSMovImpNO_MovimNiv: TWideStringField;
    QrVSMovImpSerieFch: TIntegerField;
    QrVSMovImpNO_SerieFch: TWideStringField;
    QrVSMovImpFicha: TIntegerField;
    QrVSMovImpMarca: TWideStringField;
    QrVSMovImpStatPall: TIntegerField;
    QrVSMovImpNO_StatPall: TWideStringField;
    QrVSMovImpReqMovEstq: TIntegerField;
    QrVSMovImpStqCenCad: TIntegerField;
    QrVSMovImpNO_StqCenCad: TWideStringField;
    QrVSMovImpStqCenLoc: TIntegerField;
    QrVSMovImpNO_LOC_CEN: TWideStringField;
    QrVSMovImpHistorico: TWideStringField;
    QrVSMovImpVSMulFrnCab: TIntegerField;
    QrVSMovImpMulFornece: TIntegerField;
    QrVSMovImpClientMO: TIntegerField;
    QrVSMovImpID_UNQ: TWideStringField;
    QrVSMovImpGrandeza: TSmallintField;
    QrVSMovImpNFeSer: TSmallintField;
    QrVSMovImpNFeNum: TIntegerField;
    QrVSMovImpVSMulNFeCab: TIntegerField;
    QrVSMovImpMovimCod: TIntegerField;
    QrVSMovImpFornecMO: TIntegerField;
    QrVSMovImpNO_ClientMO: TWideStringField;
    QrVSMovImpNO_FornecMO: TWideStringField;
    QrVSMovImpAtivo: TSmallintField;
    Qr06_14: TmySQLQuery;
    Qr06_14MovimTwn: TIntegerField;
    Qr06_15: TmySQLQuery;
    Qr27_34: TmySQLQuery;
    Qr06_15SrcNivel2: TIntegerField;
    Qr27_34MovimCod: TIntegerField;
    Panel6: TPanel;
    Ck26EstoqueEm: TCheckBox;
    TP26EstoqueEm: TdmkEditDateTimePicker;
    Qr27_35: TmySQLQuery;
    Qr27_35Controle: TIntegerField;
    Qr27_35Pecas: TFloatField;
    SpeedButton1: TSpeedButton;
    Qr27_34SrcNivel2: TIntegerField;
    Qr27_34Pecas: TFloatField;
    Qr27_34SrcNivel1: TIntegerField;
    Qr27_34SrcMovID: TIntegerField;
    Qr26_30: TmySQLQuery;
    Qr26_30Controle: TIntegerField;
    Qr26_30Pecas: TFloatField;
    Qr27_34Controle: TIntegerField;
    QrIMEI: TmySQLQuery;
    PB1: TProgressBar;
    PCGeral: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    Qr07_01: TmySQLQuery;
    Qr06_15SrcMovID: TIntegerField;
    Qr08_01: TmySQLQuery;
    Qr15_11: TmySQLQuery;
    Qr08_01SrcMovID: TIntegerField;
    Qr08_01SrcNivel1: TIntegerField;
    Qr08_01SrcNivel2: TIntegerField;
    Qr08_01Pecas: TFloatField;
    Qr15_11SrcMovID: TIntegerField;
    Qr15_11SrcNivel1: TIntegerField;
    Qr15_11SrcNivel2: TIntegerField;
    Qr15_11Pecas: TFloatField;
    Qr07_02: TmySQLQuery;
    Qr07_02MovimTwn: TIntegerField;
    Qr25_28: TmySQLQuery;
    Qr25_28MovimTwn: TIntegerField;
    Qr25_27: TmySQLQuery;
    Qr25_27SrcMovID: TIntegerField;
    Qr25_27SrcNivel2: TIntegerField;
    QrIMEIPecas: TFloatField;
    QrVmiPsq: TmySQLQuery;
    Qr26_29: TmySQLQuery;
    Qr26_29SrcMovID: TIntegerField;
    Qr26_29SrcNivel2: TIntegerField;
    Qr06_15Pecas: TFloatField;
    Qr11_07: TmySQLQuery;
    Qr11_07SrcMovID: TIntegerField;
    Qr11_07SrcNivel2: TIntegerField;
    Qr19_21: TmySQLQuery;
    Qr19_21Controle: TIntegerField;
    Qr19_21Pecas: TFloatField;
    Qr19_20: TmySQLQuery;
    Qr19_20SrcMovID: TIntegerField;
    Qr19_20SrcNivel2: TIntegerField;
    Qr20_22: TmySQLQuery;
    Qr20_22MovimID: TIntegerField;
    Qr20_22Controle: TIntegerField;
    Qr33_54: TmySQLQuery;
    Qr33_55: TmySQLQuery;
    Qr33_55Controle: TIntegerField;
    Qr33_55Pecas: TFloatField;
    Qr33_54SrcMovID: TIntegerField;
    Qr33_54SrcNivel2: TIntegerField;
    Qr14_01: TmySQLQuery;
    Qr14_01Controle: TIntegerField;
    Qr07_01Controle: TIntegerField;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    Qr24_01: TmySQLQuery;
    Qr24_01Controle: TIntegerField;
    frxEstq: TfrxReport;
    frxDsEstq: TfrxDBDataset;
    DsEstq: TDataSource;
    QrEstq: TmySQLQuery;
    QrCIOri: TmySQLQuery;
    QrCIOriCodigo: TIntegerField;
    QrCIOriNOMECI: TWideStringField;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsCIOri: TDataSource;
    DsPQ: TDataSource;
    DsListaSetores: TDataSource;
    QrListaSetores: TmySQLQuery;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    PCRelatorio: TPageControl;
    TabSheet3: TTabSheet;
    Panel23: TPanel;
    PnSetores: TPanel;
    DBGSetores: TdmkDBGridZTO;
    Panel7: TPanel;
    Label8: TLabel;
    BtTodosSetores: TBitBtn;
    BtNenhumSetor: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    LaCI1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCIDst: TdmkEditCB;
    CBCIDst: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Panel11: TPanel;
    LaPQ: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    Panel10: TPanel;
    LaCI: TLabel;
    EdCIOri: TdmkEditCB;
    CBCIOri: TdmkDBLookupComboBox;
    Panel12: TPanel;
    RGAgrupa: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    ProgressBar1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    Panel24: TPanel;
    CkGrade: TCheckBox;
    Panel13: TPanel;
    CGTipoCad: TdmkCheckGroup;
    RG00OrigemInfo: TRadioGroup;
    BtTodosTipCad: TBitBtn;
    BtNenhumTipCad: TBitBtn;
    RGPositivo1: TRadioGroup;
    RGAtivo: TRadioGroup;
    frxEstqNFs: TfrxReport;
    frxDsEstqNFs: TfrxDBDataset;
    QrEstqNFs: TmySQLQuery;
    QrEstqNFsNOMEFO: TWideStringField;
    QrEstqNFsNOMECI: TWideStringField;
    QrEstqNFsNOMEPQ: TWideStringField;
    QrEstqNFsNOMESE: TWideStringField;
    QrEstqNFsCodProprio: TWideStringField;
    QrEstqNFsNF: TIntegerField;
    QrEstqNFsprod_CFOP: TWideStringField;
    QrEstqNFsDataX: TDateField;
    QrEstqNFsCliOrig: TIntegerField;
    QrEstqNFsInsumo: TIntegerField;
    QrEstqNFsPeso: TFloatField;
    QrEstqNFsValor: TFloatField;
    QrEstqNFsSdoPeso: TFloatField;
    QrEstqNFsSdoValr: TFloatField;
    QrEstqNFsCusto: TFloatField;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    LaEmpresa: TLabel;
    QrCIDst: TmySQLQuery;
    QrCIDstCodigo: TIntegerField;
    QrCIDstNOMECI: TWideStringField;
    DsCIDst: TDataSource;
    BtImprime: TBitBtn;
    QrEstqIQ: TIntegerField;
    QrEstqNOMEFO: TWideStringField;
    QrEstqNOMECI: TWideStringField;
    QrEstqSetor: TIntegerField;
    QrEstqNOMESE: TWideStringField;
    QrEstqPQ: TIntegerField;
    QrEstqNOMEPQ: TWideStringField;
    QrEstqCtrlPqCli: TIntegerField;
    QrEstqCI: TIntegerField;
    QrEstqCodProprio: TWideStringField;
    QrEstqEmpresa: TIntegerField;
    QrEstqPclPeso: TFloatField;
    QrEstqPclValor: TFloatField;
    QrEstquVSPeso: TFloatField;
    QrEstquVsValor: TFloatField;
    QrEstqTotPeso: TFloatField;
    QrEstqTotValor: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure frxEstqGetValue(const VarName: string; var Value: Variant);
    procedure EdCIOriRedefinido(Sender: TObject);
    procedure EdCIDstRedefinido(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtTodosTipCadClick(Sender: TObject);
    procedure BtNenhumTipCadClick(Sender: TObject);
    procedure BtTodosSetoresClick(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
  private
    { Private declarations }
    FEmpresa: Integer;
    FErros: Integer;
    F_VS_Cus_Fich, F_VS_Estq_PQ: String;
    //
    FCIOri, FCIDst, FPQ: Integer;
    FSE, FTC, FSetoresCod, FSetoresTxt: String;
    //
    function  Difere_MovimID(MovIDEsperado, MovIDInformado: TEstqMovimID; IMEI:
              Integer; Informa: Boolean): Boolean;
    function  Difere_MovimNiv(MovNivEsperado, MovNivInformado: TEstqMovimNiv;
              IMEI: Integer; Informa: Boolean): Boolean;
    //
    procedure ImprimeTotaisInsumos();
    //
    procedure InsereCustosDe01InnNatura(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI:
              Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double; MovIDPsq,
              VmiPsq: Integer);
    procedure InsereCustosDe06Gerado(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI:
              Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double; MovIDPsq,
              VmiPsq: Integer);
    procedure InsereCustosDe07Classificado(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe08Reclassificado(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe11EmOperacao(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe14ClasseMul(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe15PreReclasse(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe19ProcessoWE(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe20SemiEAcabado(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe21Devolucao(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe22Retrabalho(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe24ReclasseMul(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe25Transferido(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe26Caleiro(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI:
              Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double; MovIDPsq,
              VmiPsq: Integer; Targ_Perc: Double = 100);
    procedure InsereCustosDe27EmCurtimento(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosDe33EmReprocesso(MovimID, MovimNiv, Estq_IMEI,
              Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer);
    procedure InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI:
              Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
              MovIDPsq, VmiPsq: Integer; Targ_Perc: Double = 100);

    //
    procedure InsereItens(
              Estq_IMEI: Integer; Estq_Pecas, Estq_VrtPc: Double;
              Targ_IMEI: Integer; Targ_Pecas: Double;
              Proc_IMEI: Integer; Proc_Pecas: Double;
              IMEI_IMEI: Integer; IMEI_Pecas: Double;
              MovimID, MovimNiv: Integer; Targ_Perc: Double = 100);
    procedure ReopenIMECs(Qry: TmySQLQuery; Serie, Ficha: Integer; MovimID:
              TEstqMovimID);
    procedure ReopenVmiPsq(VmiPsq: Integer);

  public
    { Public declarations }
    //
  end;

  var
  FmVSEstqCustoIntegr: TFmVSEstqCustoIntegr;

implementation

uses UnMyObjects, ModuleGeral, UnVS_PF, DmkDAC_PF, Module, CreateVS;

{$R *.DFM}

procedure TFmVSEstqCustoIntegr.BtImprimeClick(Sender: TObject);
begin
  PQ_PF.DefineVariaveisPesqusaEstoquePQ(EdPQ, EdCIDst, EdCIOri, RGOrdem1,
  RGOrdem2, RGOrdem3, CGTipoCad, FCIDst, FCIOri, FPQ, FSE, FTC);
  //
  case PCGeral.ActivePageIndex of
    0: Geral.MB_Info('Selecione a guia (relat�rio) desejado!');
    1:
    case PCRelatorio.ActivePageIndex of
       0: ImprimeTotaisInsumos();
      else Geral.MB_Erro('Relat�rio n�o definido! (1)');
    end;
    else Geral.MB_Erro('Relat�rio n�o definido! (2)');
  end;
end;

procedure TFmVSEstqCustoIntegr.BtNenhumTipCadClick(Sender: TObject);
begin
  CGTipoCad.Value := 0;
end;

procedure TFmVSEstqCustoIntegr.BtOKClick(Sender: TObject);
const
  DBG00GraGruY = nil;
  DBG00GraGruX = nil;
  DBG00CouNiv2 = nil;
  Qr00GraGruY  = nil;
  Qr00GraGruX  = nil;
  Qr00CouNiv2  = nil;
  DataCompra   = False;
  EmProcessoBH = True;
  GraCusPrc    = 0;
  NFeIni       = 0;
  NFeFim       = 0;
  UsaSerie     = False;
  Serie        = 0;
  MovimCod     = 0;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekEstoque;
  //
  Terceiro         = 0;
  Ordem1           = 3; // IME-I
  Ordem2           = 0;
  Ordem3           = 0;
  Ordem4           = 0;
  Ordem5           = 0;
  Agrupa           = 0;
  DescrAgruNoItm   = True;
  StqCenCad        = 0;
  ZeroNegat        = 0; // Somente positivos!
  NO_Empresa       = '';
  NO_StqCenCad     = '';
  NO_Terceiro      = '';
  MostraFrx        = False;
var
  Empresa, Filial, SrFch, Ficha, IMEI, MovimID, MovimNiv: Integer;
  Data,   EstoqueEm, DataRelativa: TDateTime;
  PcInn, Pecas, SdoVrtPeca: Double;
begin
  FErros := 0;
  Memo1.Lines.Clear;
  //
  Data := Trunc(TP26EstoqueEm.Date);
  //
  if FEmpresa = 0 then
    Empresa := VAR_LIB_EMPRESA_SEL
  else
    Empresa := FEmpresa;
  //
  //DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Filial         := DModG.ObtemFilialDeEntidade(Empresa);
  //SQL_PeriodoVS  := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', DiaIni, DiaFim, True, True);
  //SQL_PeriodoPQ  := dmkPF.SQL_Periodo('WHERE pqx.DataX ', DiaIni, DiaFim, True, True);
  EstoqueEm      := Data;
  DataRelativa   := EstoqueEm;
  //
  if VS_PF.ImprimeEstoqueEm(Empresa, Filial, Terceiro, Ordem1, Ordem2, Ordem3,
  Ordem4, Ordem5, Agrupa, DescrAgruNoItm, StqCenCad, ZeroNegat, NO_Empresa,
  NO_StqCenCad, NO_Terceiro, DataRelativa, DataCompra, EmProcessoBH,
  DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2, Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2, EstoqueEm, GraCusPrc, NFeIni, NFeFim, UsaSerie, Serie, MovimCod,
  LaAviso1, LaAviso2, False) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando inclus�o de insumos');
    F_VS_Cus_Fich :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSCustoFicha, DModG.QrUpdPID1, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovImp, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _vsmovimp1_ ',
    'WHERE ',
    '(',
    '  (SdoVrtPeca >0) ',
    '  OR ',
    '  (Pecas = 0 AND SdoVrtPeso >0) ',
    ')',
    'AND MovimID NOT IN (' +
    Geral.FF0(Integer(emidCompra))// 1
    + ',' + Geral.FF0(Integer(emidInventario))  //13
    + ',' + Geral.FF0(Integer(emidEntradaPlC))  //16
    + ',' + Geral.FF0(Integer(emidDevolucao))   //21
    + ',' + Geral.FF0(Integer(emidRetrabalho))  //22
    + ',' + Geral.FF0(Integer(emidGeraSubProd)) //23
    + ')',
    '']);
    //Geral.MB_SQL(Self, QrVSMovImp);
    PB1.Position := 0;
    PB1.Max := QrVSMovImp.RecordCount;
    QrVSMovImp.First;
    while not QrVSMovImp.Eof do
    begin
      IMEI       := QrVSMovImpIMEI.Value;
      //SdoVrtPeca?
      SdoVrtPeca := QrVSMovImpSdoVrtPeca.Value;
      MovimID    := QrVSMovImpMovimID.Value;
      MovimNiv   := QrVSMovImpMovimNiv.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrIMEI, Dmod.MyDB, [
      'SELECT Pecas ',
      'FROM vsmovits ',
      'WHERE Controle=' + Geral.FF0(IMEI),
      '']);
      Pecas      := QrIMEIPecas.Value;
      //
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Inserindo insumos do IMEI ' + Geral.FF0(IMEI));
      //
      InsereCustosGenerico(MovimID, MovimNiv, IMEI, IMEI, Pecas, SdoVrtPeca,
      SdoVrtPeca, MovimID, IMEI);
      //
      QrVSMovImp.Next;
    end;
    if (FErros = 0) and (Memo1.Text = '') then
    begin
      PCGeral.ActivePageIndex := 1;
      BtImprime.Enabled := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gera��o finalizada!');
    end
    else
    begin
      PCGeral.ActivePageIndex := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gera��o com erros');
      //+  Geral.FF0(FErros) + ' erros!');
    end;
  end;
end;

procedure TFmVSEstqCustoIntegr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSEstqCustoIntegr.BtTodosSetoresClick(Sender: TObject);
begin
  MyObjects.DBGridSelectAll(TDBGrid(DBGSetores));
end;

procedure TFmVSEstqCustoIntegr.BtTodosTipCadClick(Sender: TObject);
begin
  CGTipoCad.SetMaxValue;
end;

function TFmVSEstqCustoIntegr.Difere_MovimID(MovIDEsperado, MovIDInformado:
  TEstqMovimID; IMEI: Integer; Informa: Boolean): Boolean;
begin
  Result := MovIDEsperado <> MovIDInformado;
  if Result and Informa then
    Memo1.Lines.Add('MovimID ' + Geral.FF0(Integer(MovIDInformado)) +
    ' difere de ' + Geral.FF0(Integer(MovIDEsperado)) + ' >> IMEI ' +
    Geral.FF0(IMEI));
end;

function TFmVSEstqCustoIntegr.Difere_MovimNiv(MovNivEsperado,
  MovNivInformado: TEstqMovimNiv; IMEI: Integer; Informa: Boolean): Boolean;
begin
  Result := MovNivEsperado <> MovNivInformado;
  if Result and Informa then
    Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(MovNivInformado)) +
    ' difere de ' + Geral.FF0(Integer(MovNivEsperado)) + ' >> IMEI ' +
    Geral.FF0(IMEI));
end;

procedure TFmVSEstqCustoIntegr.EdCIDstRedefinido(Sender: TObject);
begin
  FCIDst := EdCIDst.ValueVariant;
end;

procedure TFmVSEstqCustoIntegr.EdCIOriRedefinido(Sender: TObject);
begin
  FCIOri := EdCIOri.ValueVariant;
end;

procedure TFmVSEstqCustoIntegr.EdEmpresaRedefinido(Sender: TObject);
begin
  FEmpresa := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
end;

procedure TFmVSEstqCustoIntegr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSEstqCustoIntegr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCGeral.ActivePageIndex := 0;
  FEmpresa           := -11;
  TP26EstoqueEm.Date := Geral.UltimoDiaDoMes(IncMonth(DModG.ObtemAgora(), -1));
  //
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  //
  MyObjects.PreencheComponente(CGTipoCad, sListaTipoCadPQ, 3);
  CGTipoCad.Value := 55;
  //
  TPIni.Date := Date;
  TPFim.Date := Date;
  PCRelatorio.ActivePageIndex := 0;
  UnDMkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrCIDst, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrCIOri, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  //UnDMkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
  //UnDMkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  //ReopenPQU;
  //
  EdCIDst.Text := dmkPF.ObtemInfoRegEdit('CI', Caption, '', ktString);
  if FCIDst <> 0 then CBCIDst.KeyValue := FCIDst;
  {
  TPIni.Date := dmkPF.ObtemInfoRegEdit('Ini', Caption, Date, ktDate);
  TPFim.Date := dmkPF.ObtemInfoRegEdit('Fim', Caption, Date, ktDate);
  }
  TPIni.Date := 0;
  TPFim.Date := Date;
  //
  CGTipoCad.SetMaxValue;
  MyObjects.DBGridSelectAll(TDBGrid(DBGSetores));
end;

procedure TFmVSEstqCustoIntegr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSEstqCustoIntegr.frxEstqGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
{
      2:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrMoviPQNomeCI.Value;
          1: Value := QrMoviPQNomeSE.Value;
          2: Value := QrMoviPQNomePQ.Value;
          3: Value := QrMoviPQNomeFO.Value;
          else Value := '';
        end;
      end;
      9:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
}
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
{
      2:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrMoviPQNomeCI.Value;
          1: Value := QrMoviPQNomeSE.Value;
          2: Value := QrMoviPQNomePQ.Value;
          3: Value := QrMoviPQNomeFO.Value;
          else Value := '';
        end;
      end;
      9:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
}
    end;
  end else if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_PQNOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBPQ.Text, EdPQ.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_CINOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBCIDst.Text, EdCIDst.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_AMOSTRAS') = 0 then
    Value := ''
  else if AnsiCompareText(VarName, 'VAR_SETORESTXT') = 0 then
    Value := FSetoresTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_GRD') = 0 then
    Value := dmkPF.BoolToInt2(CkGrade.Checked, 15, 0)
  //else if AnsiCompareText(VarName, 'VARF_PECA') = 0 then
    //Value := CBUnidade.Text;

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBCIDst.Text
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe20SemiEAcabado(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe20SemiEAcabado()';
var
  //, IMEI_IMEI, Proc_IMEI: Integer;
   MovimCod, MovNivPsq, SrcMovID, SrcNivel2: Integer;
  //IMEI_Pecas, Proc_Pecas: Double;
begin
  ReopenVmiPsq(VmiPsq);
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  //
  //IMEI_IMEI  := Targ_IMEI;
  //IMEI_Pecas := Targ_Pecas;
  //
  case TEstqMovimNiv(MovNivPsq) of
    (*22*)eminDestWEnd:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr20_22, Dmod.MyDB, [
      'SELECT Controle, MovimID',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),  // 21
      '']);
      SrcMovID  := Qr20_22MovimID.Value;
      (*
      N�o pode !!
      SrcNivel2 := Qr20_22Controle.Value;
      *)
      // Tem que ser o IMEI do ID 20, porque o MovimCod � o mesmo do 19,
      // Para que a quantidade de pe�as em produ��o seja o totoal do processo!
      SrcNivel2 := VmiPsq;
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(eminSdoArtEmOper)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe21Devolucao(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
begin
  // Nada! entrada por devolu��o definitiva!
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe22Retrabalho(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
begin
  // Nada! entrada por devolu��o para retrabalho!
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe24ReclasseMul(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe24ReclasseMul()';
var
   MovimCod, MovNivPsq, SrcMovID, SrcNivel2: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  case TEstqMovimNiv(MovNivPsq) of
    eminSorcClass: ;// Nada OK!
    eminDestClass:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr24_01, Dmod.MyDB, [
      'SELECT Controle, MovimID',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),  // 1
      '']);
      //
      ReopenVmiPsq(Qr24_01Controle.Value);
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(MovNivPsq)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  //
  SrcMovID  := QrVmiPsq.FieldByName('SrcMovID').AsInteger;
  SrcNivel2 := QrVmiPsq.FieldByName('SrcNivel2').AsInteger;
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe25Transferido(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe25Transferido()';
var
  PsqMovimID, SrcMovID, SrcNivel2, MovimTwn: Integer;
  Pecas_Transf: Double;
begin
  ReopenVmiPsq(VmiPsq);
  PsqMovimID  := QrVmiPsq.FieldByName('MovimID').AsInteger;
  if not Difere_MovimID(TEstqMovimID.emidTransfLoc, TEstqMovimID(PsqMovimID),
  VmiPsq, True) then
    MovimTwn := QrVmiPsq.FieldByName('MovimTwn').AsInteger;
  if MovimTwn = 0 then
  begin
    Memo1.Lines.Add('IMEI: ' + Geral.FF0(Estq_IMEI) + ' > Dados insuficientes em ' + sProcName);
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr25_27, Dmod.MyDB, [
  'SELECT Controle, SrcMovID, SrcNivel1, SrcNivel2, Pecas ',
  'FROM vsmovits ',
  'WHERE MovimTwn=' + Geral.FF0(MovimTwn), // >> ?
  'AND MovimID=' + Geral.FF0(Integer(emidTransfLoc)), //25
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcLocal)), //27
  '']);
  SrcMovID   := Qr25_27SrcMovID.Value;
  SrcNivel2  := Qr25_27SrcNivel2.Value;
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

{
procedure TFmVSEstqCustoIntegr.InsereCustosDe26Caleiro(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe26Caleiro() ';
  Estq_PesoKg = 0.000;
  Estq_AreaM2 = 0.00;
  Targ_PesoKg = 0.000;
  Targ_AreaM2 = 0.00;
  Proc_PesoKg = 0.000;
  Proc_AreaM2 = 0.00;
var
  //Aviso: String;
  Proc_IMEI, IMEI_IMEI, SrcMovID, SrcNivel2, Codigo: Integer;
  Proc_Pecas, IMEI_Pecas: Double;
begin
  ReopenVmiPsq(VmiPsq);
  Codigo := QrVmiPsq.FieldByName('Codigo').AsInteger;
  //
  if Difere_MovimID(TEstqMovimID.emidEmProcCal, TEstqMovimID(
  QrVmiPsq.FieldByName('MovimID').AsInteger), VmiPsq, True) then
    Exit;
  if Difere_MovimNiv(TEstqMovimNiv.eminEmCalInn, TEstqMovimNiv(
  QrVmiPsq.FieldByName('MovimNiv').AsInteger), Targ_IMEI, False) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr26_30, Dmod.MyDB, [
    'SELECT Controle, Pecas  ',
    'FROM vsmovits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND MovimID=' + Geral.FF0(Integer(emidEmProcCal)), //26  ',
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)), //30 ',
    '']);
    Proc_IMEI  := Qr26_30Controle.Value;
    Proc_Pecas := Qr26_30Pecas.Value;
  end else
  begin
    Proc_IMEI  := Targ_IMEI;
    Proc_Pecas := Targ_Pecas;
  end;
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  //
  InsereItens(
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    Proc_IMEI, Proc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv);

  // Tem outro processo (salmoura, pr�-remoho, remolho, caleiro, ...) antes deste caleiro?
  UnDmkDAC_PF.AbreMySQLQuery0(Qr26_29, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel2 ',
  'FROM vsmovits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND MovimID=' + Geral.FF0(Integer(emidEmProcCal)), //26  ',
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)), //29 ',
  '']);
  Qr26_29.First;
  while not Qr26_29.Eof do
  begin
    SrcMovID  := Qr26_29SrcMovID.Value;
    SrcNivel2 := Qr26_29SrcNivel2.Value;
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    //
    Qr26_29.Next;
  end;
end;
}

procedure TFmVSEstqCustoIntegr.InsereCustosDe26Caleiro(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer; Targ_Perc: Double);
const
  sProcName = 'InsereCustosDe26Caleiro() ';
{
  Estq_PesoKg = 0.000;
  Estq_AreaM2 = 0.00;
  Targ_PesoKg = 0.000;
  Targ_AreaM2 = 0.00;
  Proc_PesoKg = 0.000;
  Proc_AreaM2 = 0.00;
}
var
  //Codigo,
  MovimCod, MovNivPsq,
  NewTarg_IMEI, NewProc_IMEI, Proc_IMEI, IMEI_IMEI, SrcMovID, SrcNivel2: Integer;
  NewTarg_Pecas, NewProc_Pecas, Proc_Pecas, IMEI_Pecas: Double;
//var
   //SrcMovID, SrcNivel2: Integer;
begin
//eminSorcCal=29, eminEmCalInn=30, eminDestCal=31,
  ReopenVmiPsq(VmiPsq);
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  //
{
  case TEstqMovimNiv(MovNivPsq) of
    (*30*)eminEmCalInn:
    begin
      Proc_IMEI  := QrVmiPsq.FieldByName('Controle').AsInteger;
      Proc_Pecas := QrVmiPsq.FieldByName('Pecas').AsInteger;
(*
      IMEI_IMEI  := Proc_IMEI;
      IMEI_Pecas := Proc_Pecas;
*)
    end;
    (*29*)eminSorcCal:
    begin
(*
      IMEI_IMEI  := QrVmiPsq.FieldByName('Controle').AsInteger;
      IMEI_Pecas := -QrVmiPsq.FieldByName('Pecas').AsInteger;
      //
*)
}
      UnDmkDAC_PF.AbreMySQLQuery0(Qr26_30, Dmod.MyDB, [
      'SELECT Controle, Pecas ',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)),  // 30
      '']);
      //
      NewProc_IMEI  := Qr26_30Controle.Value;
      NewProc_Pecas := Qr26_30Pecas.Value;
{
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(MovNivPsq)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
}
  InsereItens(
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    NewProc_IMEI, NewProc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv, Targ_Perc);
  //
  case TEstqMovimNiv(MovNivPsq) of
    (*29*)eminSorcCal:
    begin
      SrcMovID  := QrVmiPsq.FieldByName('SrcMovID').AsInteger;
      SrcNivel2 := QrVmiPsq.FieldByName('SrcNivel2').AsInteger;
      //
      InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
      Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    end;
////////////////////////////////////////////////////////////////////////////////
//  Liberar e testar quando necess�rio!!!!  ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    (*30*)eminEmCalInn:
    begin
      // Tem outro processo (salmoura, pr�-remoho, remolho, caleiro, ...) antes deste caleiro?
      UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod), //
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)), // 29
      '']);
      Targ_Perc := IMEI_Pecas / -QrSumPecas.Value * 100;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qr26_29, Dmod.MyDB, [
      'SELECT SrcMovID, SrcNivel2 ',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimID=' + Geral.FF0(Integer(emidEmProcCal)), //26  ',
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)), //29 ',
      '']);
      Qr26_29.First;
      while not Qr26_29.Eof do
      begin
        SrcMovID  := Qr26_29SrcMovID.Value;
        SrcNivel2 := Qr26_29SrcNivel2.Value;
        NewTarg_IMEI  := Qr27_34Controle.Value;
        NewTarg_Pecas := -Qr27_34Pecas.Value;
        // Ascendente
        InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, NewTarg_IMEI, Estq_Pecas,
        Estq_VrtPc, NewTarg_Pecas, SrcMovID, SrcNivel2, Targ_Perc);
        //
        Qr26_29.Next;
      end;
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(MovNivPsq) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
{
begin
  ReopenVmiPsq(VmiPsq);
  Codigo := QrVmiPsq.FieldByName('Codigo').AsInteger;
  //
  if Difere_MovimID(TEstqMovimID.emidEmProcCal, TEstqMovimID(
  QrVmiPsq.FieldByName('MovimID').AsInteger), VmiPsq, True) then
    Exit;
  if Difere_MovimNiv(TEstqMovimNiv.eminEmCalInn, TEstqMovimNiv(
  QrVmiPsq.FieldByName('MovimNiv').AsInteger), Targ_IMEI, False) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr26_30, Dmod.MyDB, [
    'SELECT Controle, Pecas  ',
    'FROM vsmovits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND MovimID=' + Geral.FF0(Integer(emidEmProcCal)), //26  ',
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)), //30 ',
    '']);
    Proc_IMEI  := Qr26_30Controle.Value;
    Proc_Pecas := Qr26_30Pecas.Value;
  end else
  begin
    Proc_IMEI  := Targ_IMEI;
    Proc_Pecas := Targ_Pecas;
  end;
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  //
  InsereItens(
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    Proc_IMEI, Proc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv);

  // Tem outro processo (salmoura, pr�-remoho, remolho, caleiro, ...) antes deste caleiro?
  UnDmkDAC_PF.AbreMySQLQuery0(Qr26_29, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel2 ',
  'FROM vsmovits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND MovimID=' + Geral.FF0(Integer(emidEmProcCal)), //26  ',
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)), //29 ',
  '']);
  Qr26_29.First;
  while not Qr26_29.Eof do
  begin
    SrcMovID  := Qr26_29SrcMovID.Value;
    SrcNivel2 := Qr26_29SrcNivel2.Value;
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    //
    Qr26_29.Next;
  end;
}
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe27EmCurtimento(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe27EmCurtimento() ';
  Estq_PesoKg = 0.000;
  Estq_AreaM2 = 0.00;
  Targ_PesoKg = 0.000;
  Targ_AreaM2 = 0.00;
  Proc_PesoKg = 0.000;
  Proc_AreaM2 = 0.00;
var
  Aviso: String;
  MovimCod, Proc_IMEI, IMEI_IMEI,
  //NewVmiPsq, IMEI_Cal, Codigo_Cal, MovID_Cal, MovNiv_Cal: Integer;
  SrcMovID, SrcNivel2: Integer;
  Targ_Perc, Proc_Pecas, IMEI_Pecas, NewTarg_Pecas, NewVirt_Pecas: Double;
  MovNivPsq, MovTwnPsq, NewTarg_IMEI: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  MovTwnPsq := QrVmiPsq.FieldByName('MovimTwn').AsInteger;
  //MovIDPsq  := QrVmiPsq.FieldByName('MovimID').AsInteger;
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  if Difere_MovimID(TEstqMovimID.emidEmProcCur, TEstqMovimID(
  QrVmiPsq.FieldByName('MovimID').AsInteger), VmiPsq, True) then
    Exit;
  MovimCod := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  //
  if Targ_Pecas = 0 then
  begin
    Aviso := sprocName + '>> Pe�as=0 no IMEI ' + Geral.FF0(Targ_IMEI);
    Memo1.Lines.Add(Aviso);
    Exit;
  end;
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  if TEstqMovimNiv(MovNivPsq) = TEstqMovimNiv.eminEmCurInn then
  begin
    Proc_IMEI  := Targ_IMEI;
    Proc_Pecas := Targ_Pecas;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr27_35, Dmod.MyDB, [
    'SELECT Controle, Pecas ',
    'FROM vsmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimID='+ Geral.FF0(Integer(emidEmProcCur)), //27 ',
    'AND MovimNiv='+ Geral.FF0(Integer(eminEmCurInn)), //35 ', // >> 14443
    '']);
    Proc_IMEI  := Qr27_35Controle.Value;
    Proc_Pecas := Qr27_35Pecas.Value;
  end;
  //
  InsereItens(  // IMEI >> 13650
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    Proc_IMEI, Proc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv);
  //   Insumos de Caleiro!
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod), //5891
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCur)), // 34
  '']);
  Targ_Perc := IMEI_Pecas / -QrSumPecas.Value * 100;
*)
  Targ_Perc := 100;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr27_34, Dmod.MyDB, [
  'SELECT Controle, MovimCod, Pecas, SrcMovID, SrcNivel1, SrcNivel2 ',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod), //5891
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCur)), // 34
  '']);
  Qr27_34.First;
  while not Qr27_34.Eof do
  begin
    SrcMovID      := Qr27_34SrcMovID.Value;
    SrcNivel2     := Qr27_34SrcNivel2.Value; // 14255
    NewTarg_IMEI  := Qr27_34Controle.Value;
    NewVirt_Pecas := IMEI_Pecas; // > 50
    NewTarg_Pecas := -Qr27_34Pecas.Value; // > 150
    //
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, NewTarg_IMEI, Estq_Pecas,
    (*Estq_VrtPc*)NewVirt_Pecas, NewTarg_Pecas, SrcMovID, SrcNivel2, Targ_Perc);
    //
    Qr27_34.Next;
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe33EmReprocesso(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe33EmReprocesso()';
var
  MovimCod, MovNivPsq, IMEI_IMEI, Proc_IMEI, SrcMovID, SrcNivel2: Integer;
  IMEI_Pecas, Proc_Pecas: Double;
begin
  ReopenVmiPsq(VmiPsq);
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  //
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  //
  case TEstqMovimNiv(MovNivPsq) of
    (*55*)eminEmRRMInn:
    begin
      Proc_IMEI  := Targ_IMEI;
      Proc_Pecas := Targ_Pecas;
    end;
    (*56*)eminDestRRM:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr33_55, Dmod.MyDB, [
      'SELECT Controle, Pecas',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMInn)),  // 55
      '']);
      Proc_IMEI  := Qr33_55Controle.Value;
      Proc_Pecas := Qr33_55Pecas.Value;
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(eminSdoArtEmOper)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  InsereItens(
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    Proc_IMEI, Proc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv);

  //
  // A s c e n d e n t e s /////////////////////////////////////////////////////
  //

  UnDmkDAC_PF.AbreMySQLQuery0(Qr33_54, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel2' ,
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmReprRM)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcRRM)),
  '']);
  while not Qr33_54.Eof do
  begin
    SrcMovID  := Qr33_54SrcMovID.Value;
    SrcNivel2 := Qr33_54SrcNivel2.Value;
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    //
    Qr33_54.Next;
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereCustosGenerico(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer; Targ_Perc: Double);
const
  sProcName = 'InsereCustosGenerico()';
begin
  //if Estq_IMEI = 14664 then
  //if Estq_IMEI = 17 then
  //if Estq_IMEI = 79 then
  //if MovimID = 6 then
  //if Estq_IMEI = 14938 then
  //if Estq_IMEI = 94 then
  //if Estq_IMEI = 84 then
  //if Estq_IMEI = 15120 then
////////////////////////////////////////////////////////////////////////////////
  //if Estq_IMEI = 19 then
  //if Estq_IMEI = 73 then
  //if Estq_IMEI = 58 then
  begin
    case MovIDPsq of
      01: InsereCustosDe01InnNatura(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      06: InsereCustosDe06Gerado(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      07: InsereCustosDe07Classificado(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      08: InsereCustosDe08Reclassificado(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      11: InsereCustosDe11EmOperacao(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      14: InsereCustosDe14ClasseMul(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      15: InsereCustosDe15PreReclasse(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      19: InsereCustosDe19ProcessoWE(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      20: InsereCustosDe20SemiEAcabado(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      21: InsereCustosDe21Devolucao(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      22: InsereCustosDe22Retrabalho(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      24: InsereCustosDe24ReclasseMul(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      25: InsereCustosDe25Transferido(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      26: InsereCustosDe26Caleiro(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq, Targ_Perc);
      27: InsereCustosDe27EmCurtimento(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      33: InsereCustosDe33EmReprocesso(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI,
          Estq_Pecas, Estq_VrtPc, Targ_Pecas, MovIDPsq, VmiPsq);
      else
      begin
        FErros := FErros + 1;
        if VmiPsq <> Estq_IMEI then
          Memo1.Lines.Add(sProcName + ' IMEI ' + Geral.FF0(Estq_IMEI) +
          ' n�o implementado! MovimID: ' + Geral.FF0(Integer(MovimID)) +
          ' IMEI pesq. ' + Geral.FF0(VmiPsq) + ' MovimID pesq. ' +
          Geral.FF0(Integer(MovIDPsq)))
        else
          Memo1.Lines.Add(sProcName + ' IMEI ' + Geral.FF0(Estq_IMEI) +
          ' n�o implementado! MovimID: ' + Geral.FF0(Integer(MovimID)));
      end;
    end;
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereItens(Estq_IMEI: Integer; Estq_Pecas,
  Estq_VrtPc: Double; Targ_IMEI: Integer; Targ_Pecas: Double;
  Proc_IMEI: Integer; Proc_Pecas: Double; IMEI_IMEI: Integer;
  IMEI_Pecas: Double; MovimID, MovimNiv: Integer; Targ_Perc: Double);
var
  Estq_VrtPerc, Estq_ProcPerc, Estq_IMEIPerc, Targ_ProcPerc, Proc_IMEIPerc,
  IMEI_EstqPerc, FatorEstq, FatorProc: Double;
  Fator_TXT: String;
begin
(*
  Estq_VrtPerc  := Estq_VrtPc / Estq_Pecas;
  Estq_ProcPerc := Proc_Pecas / Estq_Pecas;
  Estq_IMEIPerc := IMEI_Pecas / Estq_Pecas;
  Targ_ProcPerc := Estq_Pecas / Targ_Pecas;
  Proc_IMEIPerc := IMEI_Pecas / Proc_Pecas;
  IMEI_EstqPerc := Proc_IMEIPerc * Estq_VrtPerc * (Targ_Perc / 100);
  Fator_TXT := Geral.FFT_Dot(IMEI_EstqPerc, 12, siNegativo);
*)
////////////////////////////////////////////////////////////////////////////////
  Estq_VrtPerc  := Estq_VrtPc / Estq_Pecas;
  Estq_ProcPerc := Proc_Pecas / Estq_Pecas;
  Estq_IMEIPerc := IMEI_Pecas / Estq_Pecas;
  Targ_ProcPerc := Estq_Pecas / Targ_Pecas;
  Proc_IMEIPerc := IMEI_Pecas / Proc_Pecas;
  //IMEI_EstqPerc := Proc_IMEIPerc * Estq_VrtPerc * (Targ_Perc / 100);
  FatorEstq     := Targ_Pecas / Estq_VrtPc;
  FatorProc     := IMEI_Pecas / Proc_Pecas;
  IMEI_EstqPerc := FatorEstq * FatorProc * (Targ_Perc / 100);
  //IMEI_EstqPerc := FatorProc * (Targ_Perc / 100);
  Fator_TXT := Geral.FFT_Dot(IMEI_EstqPerc, 12, siNegativo);
////////////////////////////////////////////////////////////////////////////////
  if (IMEI_EstqPerc > 1) or (IMEI_EstqPerc <= 0) then
    Memo1.Lines.Add('IMEI ' + Geral.FF0(Estq_IMEI) +
    ' com fator inv�lido! Fator = ' + Fator_TXT +
    ' MovimID: ' + Geral.FF0(Integer(QrVSMovImpMovimID.Value)));
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + F_VS_Cus_Fich,
  'SELECT ' +
  Geral.FF0(MovimID) + ' Estq_MovID, ' +
  Geral.FF0(MovimNiv) + ' Estq_MovNiv, ' +
  Geral.FF0(Estq_IMEI) + ' Estq_IMEI, ' +
  Geral.FFT_Dot(Estq_Pecas, 3, siNegativo) + ' Estq_Pecas, ',
  Geral.FFT_Dot(Estq_VrtPc, 3, siNegativo) + ' Estq_VrtPc, ',
  Geral.FFT_Dot(Estq_VrtPerc * 100, 12, siNegativo) + ' Estq_VrtPerc, ',
  Geral.FFT_Dot(Estq_ProcPerc * 100, 12, siNegativo) + ' Estq_ProcPerc, ',
  Geral.FFT_Dot(Estq_IMEIPerc * 100, 12, siNegativo) + ' Estq_IMEIPerc, ',
  Geral.FF0(Targ_IMEI) + ' Targ_IMEI, ' +
  Geral.FFT_Dot(Targ_Pecas, 3, siNegativo) + ' Targ_Pecas, ',
  Geral.FFT_Dot(Targ_ProcPerc * 100, 12, siNegativo) + ' Targ_ProcPerc, ',
  Geral.FF0(Proc_IMEI) + ' Proc_IMEI, ' +
  Geral.FFT_Dot(Proc_Pecas, 3, siNegativo) + ' Proc_Pecas, ',
  Geral.FFT_Dot(Proc_IMEIPerc * 100, 12, siNegativo) + ' Proc_IMEIPerc, ',
  Geral.FF0(IMEI_IMEI) + ' IMEI_IMEI, ' +
  Geral.FFT_Dot(IMEI_Pecas, 3, siNegativo) + ' IMEI_Pecas, ',
  Geral.FFT_Dot(IMEI_EstqPerc * 100, 12, siNegativo) + ' IMEI_EstqPerc, ',
  'emt.Setor, emt.NOMESETOR, emc.PercTotCus, emc.VSMovIts, ',
  'vmi.SerieFch, vmi.Ficha, pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo, ',
  'pqx.CliOrig, pqx.CliDest, pqx.Insumo, ',
  'SUM(-pqx.Peso) PesoTotal, SUM(-pqx.Valor) ValorTotal,',
  'SUM(-pqx.Peso*emc.PercTotCus/100*' + Fator_TXT + ') PesoLote, ',
  'SUM(-pqx.Valor*emc.PercTotCus/100*' + Fator_TXT + ') ValorLote',
  'FROM ' + TMeuDB + '.emit emt ',
  'LEFT JOIN ' + TMeuDB + '.emitcus emc ON emt.Codigo=emc.Codigo',
  'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=emc.VSMovIts',
  'LEFT JOIN ' + TMeuDB + '.pqx pqx ON pqx.Tipo=110',
  '  AND pqx.OrigemCodi=emt.Codigo',
  'WHERE vmi.Controle=' + Geral.FF0(Proc_IMEI),
  'GROUP BY NOMESETOR, pqx.Insumo',
  '']);
  //Geral.MB_SQL(Self, DModG.QrUpdPID1);
end;

procedure TFmVSEstqCustoIntegr.ImprimeTotaisInsumos();
var
  SQL_SE, SQL_PQ, SQL_CI, SQL_Ativo: String;
begin
  if not PQ_PF.ObtemSetoresSelecionados(DBGSetores, QrListaSetores, FSetoresCod,
  FSetoresTxt) then
    Exit;
  if MyObjects.FIC(FTC = '', CGTipoCad, 'Informe o(s) tipo(s) de cadastro!') then
    Exit;
  SQL_SE := '';
  SQL_PQ := '';
  SQL_CI := '';
  //
  case RGPositivo1.ItemIndex of
    //0: SQL_SE := '';
    1: SQL_SE := 'AND pcl.Peso <> 0 ';
    2: SQL_SE := 'AND pcl.Peso > 0 ';
  end;
  if FPQ <> 0 then
    SQL_PQ := 'AND pcl.PQ=' + Geral.FF0(FPQ);
  if FCIDst <> 0 then
    SQL_CI := 'AND pcl.CI=' + Geral.FF0(FCIDst);
  //
  case RGAtivo.ItemIndex of
    0: SQL_Ativo := '';
    1: SQL_Ativo := 'AND pq_.Ativo = 1';
    2: SQL_Ativo := 'AND pq_.Ativo = 0';
  end;
  //
  case RG00OrigemInfo.ItemIndex of
    0:
    begin
      F_VS_Estq_PQ :=
        UnCreateVS.RecriaTempTableNovo(ntrttVSEstqPQ, DModG.QrUpdPID1, False);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + F_VS_Estq_PQ,
      'SELECT pq_.IQ, IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI, ',
      'lse.Codigo Setor, lse.Nome NOMESE, pq_.Codigo PQ, pq_.Nome NOMEPQ, ',
      '',
      'pcl.Controle CtrlPqCli, pcl.CI, pcl.CodProprio, pcl.Empresa,',
      'pcl.Peso PclPeso, pcl.Valor PclValor, ',
      '0.000 uVSPeso, 0.000 uVsValor',
      '',
      'FROM ' + TMeuDB + '.pqcli pcl ',
      'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pcl.PQ ',
      'LEFT JOIN ' + TMeuDB + '.entidades    cli ON cli.Codigo=pcl.CI ',
      'LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=pq_.Setor ',
      'LEFT JOIN ' + TMeuDB + '.entidades    ind ON ind.Codigo=pq_.IQ ',
      'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
      'AND pcl.Empresa=' + Geral.FF0(FEmpresa),
      'AND pq_.Setor in (' + FSetoresCod + ') ',
      SQL_SE,
      SQL_PQ,
      SQL_CI,
      SQL_Ativo,
      '']);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + F_VS_Estq_PQ,
      'SELECT pq_.IQ, IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO,  ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI,  ',
      'lse.Codigo Setor, lse.Nome NOMESE, pq_.Codigo PQ, pq_.Nome NOMEPQ,  ',
      ' ',
      'pcl.Controle CtrlPqCli, pcl.CI, pcl.CodProprio, pcl.Empresa, ',
      '0.000 PclPeso, 0.000 PclValor,  ',
      'vcf.PesoLote uVSPeso, vcf.ValorLote uVsValor ',
      ' ',
      'FROM _vs_cus_fich vcf ',
      'LEFT JOIN ' + TMeuDB + '.pqcli pcl  ',
      '  ON vcf.Insumo=pcl.PQ ',
      '  AND vcf.CliDest=pcl.CI  ',
      'LEFT JOIN ' + TMeuDB + '.pq pq_ ON pq_.Codigo=pcl.PQ  ',
      'LEFT JOIN ' + TMeuDB + '.entidades    cli ON cli.Codigo=pcl.CI  ',
      'LEFT JOIN ' + TMeuDB + '.listasetores lse ON lse.Codigo=pq_.Setor  ',
      'LEFT JOIN ' + TMeuDB + '.entidades    ind ON ind.Codigo=pq_.IQ  ',
      'WHERE pq_.GGXNiv2 IN (' + FTC + ') ',
      'AND pcl.Empresa=' + Geral.FF0(FEmpresa),
      'AND pq_.Setor in (' + FSetoresCod + ') ',
      SQL_SE,
      SQL_PQ,
      SQL_CI,
      SQL_Ativo,
      '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, DModG.MyPID_DB, [
      'SELECT IQ, NOMEFO, NOMECI, Setor, NOMESE, PQ, ',
      'NOMEPQ, CtrlPqCli, CI, CodProprio, Empresa,  ',
      'SUM(PclPeso) PclPeso, SUM(PclValor) PclValor, ',
      'SUM(uVSPeso) uVSPeso, SUM(uVsValor) uVsValor,',
      'SUM(PclPeso+uVSPeso) TotPeso, ',
      'SUM(PclValor+uVSValor) TotValor ',
      'FROM ' + F_VS_Estq_PQ,
      'GROUP BY CI, Empresa, NOMEPQ',
      FSE,
      '']);
      //
      Geral.MB_SQL(Self, QrEstq);


        MyObjects.frxDefineDataSets(frxEstq, [
        DModG.frxDsDono,
        frxDsEstq
      ]);
      MyObjects.frxMostra(frxEstq, 'Estoque de Insumos');
    end;
{    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEstqNFs, Dmod.MyDB, [
      'SELECT IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI,  ',
      'pq_.Nome NOMEPQ, lse.Nome NOMESE, pcl.CodProprio, ',
      'pqe.NF, pqi.prod_CFOP, ',
      'pqx.DataX, pqx.CliOrig, pqx.Insumo, pqx.Peso, pqx.Valor, ',
      'pqx.SdoPeso, pqx.SdoValr, (pqx.Valor / pqx.Peso) Custo ',
      'FROM pqx pqx ',
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo ',
      'LEFT JOIN pqeits pqi ',
      '  ON pqi.Codigo=pqx.OrigemCodi ',
      '  AND pqi.Controle=pqx.OrigemCtrl ',
      'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo ',
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig ',
      'LEFT JOIN entidades ind ON ind.Codigo=pq_.IQ ',
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor ',
      'LEFT JOIN pqcli pcl ON pcl.PQ=pqx.Insumo AND pcl.CI=pqx.CliOrig ',
      'WHERE SdoPeso>0 ',
      'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      //'AND pq_.Ativo IN (' + FTC + ') ',
      'AND pq_.GGXNiv2 IN (' + FTC + ') ',
      'AND pqx.Empresa=' + Geral.FF0(FEmpresa),
      SQL_SE,
      SQL_PQ,
      SQL_CI,
      SQL_Ativo,
      //
      FSE, // ORDEM
      '']);
      //
      Geral.MB_SQL(Self, QrEstqNFs);
      //
      MyObjects.frxDefineDataSets(frxEstqNFs, [
        DModG.frxDsDono,
        frxDsEstqNFs
      ]);
      MyObjects.frxMostra(frxEstqNFs, 'Estoque de Insumos por NFs');
    end;
}
    else Geral.MB_Erro('"Origem das informa�es" n�o implementado!');
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe01InnNatura(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
begin
  // Nada!!  � entrada de couro verde / sa�gado!
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe06Gerado(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe06Gerado() ';
  Estq_PesoKg = 0.000;
  Estq_AreaM2 = 0.00;
  Targ_PesoKg = 0.000;
  Targ_AreaM2 = 0.00;
  Proc_PesoKg = 0.000;
  Proc_AreaM2 = 0.00;
var
  Aviso: String;
  SrcMovID, SrcNivel2, IMEI_Psq, Proc_IMEI, IMEI_IMEI: Integer;
  //IMEI_Cal, Codigo_Cal, MovID_Cal, MovNiv_Cal: Integer;
  Proc_Pecas, IMEI_Pecas: Double;
begin
  ReopenVmiPsq(VmiPsq);
  if QrVmiPsq.FieldByName('Pecas').Value = 0 then
  begin
    Aviso := sprocName + '>> Pe�as=0 no IMEI ' + Geral.FF0(VmiPsq);
    Memo1.Lines.Add(Aviso);
    Exit;
  end;
  //Descobre a origem (wet blue curtido) do Wet Blue (wet blue gerado):
  UnDmkDAC_PF.AbreMySQLQuery0(Qr06_14, Dmod.MyDB, [
  'SELECT MovimTwn ',
  'FROM vsmovits ',
  'WHERE DstNivel2=' + Geral.FF0(VmiPsq), // Wet Blue Gerado (6.13)
  'AND MovimID=' + Geral.FF0(Integer(emidIndsXX)), // 6
  '']);
  //Geral.MB_SQL(Self, Qr06_14);
  while not Qr06_14.Eof do
  begin
    //Descobre o gemeo (6.15) do wet blue curtido (6.14)
    UnDmkDAC_PF.AbreMySQLQuery0(Qr06_15, Dmod.MyDB, [
    'SELECT SrcMovID, SrcNivel2, Pecas',
    'FROM vsmovits ',
    'WHERE MovimTwn=' + Geral.FF0(Qr06_14MovimTwn.Value), //6272 >> 6.14
    'AND MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),// 15
    '']);
    //Geral.MB_SQL(Self, Qr06_15);
    SrcMovID  := Qr06_15SrcMovID.Value;
    SrcNivel2 := Qr06_15SrcNivel2.Value;
    IMEI_Pecas := -Qr06_15Pecas.Value;
{
    case TEstqMovimID(Qr06_15SrcMovID.Value) of
      (*01*)emidCompra: ;// Nada!
      (*27*)emidEmProcCur:
      begin
        // Descobre o Curtimento
        UnDmkDAC_PF.AbreMySQLQuery0(Qr27_34, Dmod.MyDB, [
        'SELECT Controle, MovimCod, Pecas, SrcMovID, SrcNivel1, SrcNivel2 ',
        'FROM vsmovits ', //  14444  |> SrcNivel2 do 6.15
        'WHERE Controle=' + Geral.FF0(Qr06_15SrcNivel2.Value),
        '']);
        IMEI_IMEI  := Qr27_34Controle.Value;
        IMEI_Pecas := -Qr27_34Pecas.Value;
        //
        SrcNivel2   := Qr27_34SrcNivel2.Value; // 14255
        SrcMovID  := Qr27_34SrcNivel2.Value;
        //Descobre o IMEI (27.35) que recebeu a pesagem!
        UnDmkDAC_PF.AbreMySQLQuery0(Qr27_35, Dmod.MyDB, [
        'SELECT Controle, Pecas ',
        'FROM vsmovits ',
        'WHERE MovimCod=' + Geral.FF0(Qr27_34MovimCod.Value), //6310
        'AND MovimID='+ Geral.FF0(Integer(emidEmProcCur)), //27 ',
        'AND MovimNiv='+ Geral.FF0(Integer(eminEmCurInn)), //35 ', // >> 14443
        '']);
        //
        Proc_IMEI  := Qr27_35Controle.Value;
        Proc_Pecas := Qr27_35Pecas.Value;
        //
        InsereItens(
          Estq_IMEI, Estq_Pecas, Estq_VrtPc,
          Targ_IMEI, Targ_Pecas,
          Proc_IMEI, Proc_Pecas,
          IMEI_IMEI, IMEI_Pecas,
          MovimID, MovimNiv);
        //
        //   Insumos de Caleiro!
        InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
        Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
        //
      end;
      else Memo1.Lines.Add('MovimID ' + Geral.FF0(Qr06_15SrcMovID.Value) +
      ' n�o implementado em ' + sProcName);
    end;
}
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, IMEI_Pecas, SrcMovID, SrcNivel2);
    //
    Qr06_14.Next;
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe07Classificado(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe07Classificado()';
var
   MovimCod, MovNivPsq, SrcMovID, SrcNivel2: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  case TEstqMovimNiv(MovNivPsq) of
    eminSorcClass: ;// Nada OK!
    eminDestClass:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr07_01, Dmod.MyDB, [
      'SELECT Controle, MovimID',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),  // 1
      '']);
      //
      ReopenVmiPsq(Qr07_01Controle.Value);
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(MovNivPsq)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  //
  SrcMovID  := QrVmiPsq.FieldByName('SrcMovID').AsInteger;
  SrcNivel2 := QrVmiPsq.FieldByName('SrcNivel2').AsInteger;
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
{
var
  SrcMovID, SrcNivel2, PsqMovimNiv, PsqMovimID, MovimTwn: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  PsqMovimID  := QrVmiPsq.FieldByName('MovimID').AsInteger;
  if not Difere_MovimID(TEstqMovimID.emidClassArtXXUni, TEstqMovimID(PsqMovimID),
  VmiPsq, True) then
    MovimTwn := QrVmiPsq.FieldByName('MovimTwn').AsInteger;
  if MovimTwn = 0 then
  begin
    Memo1.Lines.Add('IMEI: ' + Geral.FF0(Estq_IMEI) + ' > Dados insuficientes em ' + sProcName);
    Exit;
  end;
  //
  /

  UnDmkDAC_PF.AbreMySQLQuery0(Qr07_01, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel1, SrcNivel2, Pecas ',
  'FROM vsmovits ',
  'WHERE MovimTwn=' + Geral.FF0(MovimTwn), // >> 7
  'AND MovimID=' + Geral.FF0(Integer(emidClassArtXXUni)), //7
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)), //1
  '']);
  SrcMovID  := Qr07_01SrcMovID.Value;
  SrcNivel2    := Qr07_01SrcNivel2.Value;
  //
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
}
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe08Reclassificado(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe08Reclassificado()';
var
  PsqMovimID, MovimTwn, SrcMovID, SrcNivel1, SrcNivel2: Integer;
  PecasPre: Double;
begin
  ReopenVmiPsq(VmiPsq);
  PsqMovimID  := QrVmiPsq.FieldByName('MovimID').AsInteger;
  if not Difere_MovimID(TEstqMovimID.emidReclasXXUni, TEstqMovimID(PsqMovimID),
  VmiPsq, True) then
    MovimTwn := QrVmiPsq.FieldByName('MovimTwn').AsInteger;
  //
  if MovimTwn = 0 then
  begin
    Memo1.Lines.Add('IMEI: ' + Geral.FF0(Estq_IMEI) + ' > Dados insuficientes em ' + sProcName);
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr08_01, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel1, SrcNivel2, Pecas ',
  'FROM vsmovits ',
  'WHERE MovimTwn=' + Geral.FF0(MovimTwn), // >> 9
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)), //1
  '']);
  //
  SrcMovID  := Qr08_01SrcMovID.Value;
  SrcNivel2 := Qr08_01SrcNivel2.Value;
  SrcNivel1 := Qr08_01SrcNivel1.Value;
  PecasPre  := -Qr08_01Pecas.Value;
  //
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe11EmOperacao(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
var
  MovimCod, SrcMovID, SrcNivel2: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  MovimCod := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr11_07, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel2',
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  '']);
  while not Qr11_07.Eof do
  begin
    SrcMovID  := Qr11_07SrcMovID.Value;
    SrcNivel2 := Qr11_07SrcNivel2.Value;
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    //
    Qr11_07.Next;
  end;
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe14ClasseMul(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe14ClasseMul()';
var
   MovimCod, MovNivPsq, SrcMovID, SrcNivel2: Integer;
begin
  ReopenVmiPsq(VmiPsq);
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  //
  case TEstqMovimNiv(MovNivPsq) of
    eminSorcClass: ;// Nada OK!
    eminDestClass:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr14_01, Dmod.MyDB, [
      'SELECT Controle, MovimID',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),  // 1
      '']);
      //
      ReopenVmiPsq(Qr14_01Controle.Value);
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(MovNivPsq)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  //
  SrcMovID  := QrVmiPsq.FieldByName('SrcMovID').AsInteger;
  SrcNivel2 := QrVmiPsq.FieldByName('SrcNivel2').AsInteger;
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe15PreReclasse(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe15PreReclasse()';
var
  Codigo, SrcMovID, SrcNivel1, SrcNivel2: Integer;
  PecasPre: Double;
begin
  ReopenVmiPsq(VmiPsq);
  if Difere_MovimID(TEstqMovimID.emidPreReclasse, TEstqMovimID(
  QrVmiPsq.FieldByName('MovimID').AsInteger), VmiPsq, True) then
    Exit;
  Codigo := QrVmiPsq.FieldByName('Codigo').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr15_11, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND MovimID=' + Geral.FF0(Integer(emidPreReclasse)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)),
  '']);
  //
  SrcMovID  := Qr15_11SrcMovID.Value;
  SrcNivel1 := Qr15_11SrcNivel1.Value;
  SrcNivel2 := Qr15_11SrcNivel2.Value;
  PecasPre  := -Qr08_01Pecas.Value;
  //
  // Ascendente
  InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
  Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
end;

procedure TFmVSEstqCustoIntegr.InsereCustosDe19ProcessoWE(MovimID, MovimNiv,
  Estq_IMEI, Targ_IMEI: Integer; Estq_Pecas, Estq_VrtPc, Targ_Pecas: Double;
  MovIDPsq, VmiPsq: Integer);
const
  sProcName = 'InsereCustosDe19ProcessoWE()';
var
  MovimCod, MovNivPsq, IMEI_IMEI, Proc_IMEI, SrcMovID, SrcNivel2: Integer;
  IMEI_Pecas, Proc_Pecas: Double;
begin
  ReopenVmiPsq(VmiPsq);
  MovimCod  := QrVmiPsq.FieldByName('MovimCod').AsInteger;
  MovNivPsq := QrVmiPsq.FieldByName('MovimNiv').AsInteger;
  //
  IMEI_IMEI  := Targ_IMEI;
  IMEI_Pecas := Targ_Pecas;
  //
  case TEstqMovimNiv(MovNivPsq) of
    (*21*)eminEmWEndInn:
    begin
      Proc_IMEI  := Targ_IMEI;
      Proc_Pecas := Targ_Pecas;
    end;
    (*22*)eminDestWEnd:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qr19_21, Dmod.MyDB, [
      'SELECT Controle, Pecas',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),  // 21
      '']);
      Proc_IMEI  := Qr19_21Controle.Value;
      Proc_Pecas := Qr19_21Pecas.Value;
    end;
    else
    begin
      Memo1.Lines.Add('MovimNiv ' + Geral.FF0(Integer(eminSdoArtEmOper)) +
      ' n�o implementado em ' + sProcName);
      Exit;
    end;
  end;
  InsereItens(
    Estq_IMEI, Estq_Pecas, Estq_VrtPc,
    Targ_IMEI, Targ_Pecas,
    Proc_IMEI, Proc_Pecas,
    IMEI_IMEI, IMEI_Pecas,
    MovimID, MovimNiv);

  //
  // A s c e n d e n t e s /////////////////////////////////////////////////////
  //

  UnDmkDAC_PF.AbreMySQLQuery0(Qr19_20, Dmod.MyDB, [
  'SELECT SrcMovID, SrcNivel2' ,
  'FROM vsmovits ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimID=' + Geral.FF0(Integer(emidEmProcWE)),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),
  '']);
  while not Qr19_20.Eof do
  begin
    SrcMovID  := Qr19_20SrcMovID.Value;
    SrcNivel2 := Qr19_20SrcNivel2.Value;
    // Ascendente
    InsereCustosGenerico(MovimID, MovimNiv, Estq_IMEI, Targ_IMEI, Estq_Pecas,
    Estq_VrtPc, Targ_Pecas, SrcMovID, SrcNivel2);
    //
    Qr19_20.Next;
  end;
end;

procedure TFmVSEstqCustoIntegr.ReopenIMECs(Qry: TmySQLQuery; Serie,
  Ficha: Integer; MovimID: TEstqMovimID);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInn, DModG.MyPID_DB, [
  'SELECT DISTINCT MovimCod',
  'FROM vsmovits',
  'WHERE SerieFch=' + Geral.FF0(Serie),
  'AND Ficha=' + Geral.FF0(Ficha),
  'AND MovimID=' + Geral.FF0(Integer(MovimID)),
  '']);
end;

procedure TFmVSEstqCustoIntegr.ReopenVmiPsq(VmiPsq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVmiPsq, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovits',
  'WHERE Controle=' + Geral.FF0(VmiPsq),
  '']);
end;

procedure TFmVSEstqCustoIntegr.SpeedButton1Click(Sender: TObject);
begin
  TP26EstoqueEm.Date := Geral.UltimoDiaDoMes(IncMonth(TP26EstoqueEm.Date, -1));
end;

end.
