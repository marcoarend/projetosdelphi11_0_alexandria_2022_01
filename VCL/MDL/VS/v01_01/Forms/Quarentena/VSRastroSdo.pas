unit VSRastroSdo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, TypInfo, Variants, Vcl.Menus;

type
  TFmVSRastroSdo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrGraGruXGrandeza: TSmallintField;
    DsGraGruX: TDataSource;
    Label2: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrMIDs: TMySQLQuery;
    DsMIDs: TDataSource;
    PB1: TProgressBar;
    QrMIDsMovIDNiv: TLargeintField;
    QrMIDsMovimID: TLargeintField;
    QrMIDsMovimNiv: TLargeintField;
    QrMIDsItens: TLargeintField;
    QrMIDsPecas: TFloatField;
    QrMIDsAreaM2: TFloatField;
    QrMIDsSdoVrtPeca: TFloatField;
    QrMIDsSdoVrtArM2: TFloatField;
    PB2: TProgressBar;
    QrSumA: TMySQLQuery;
    QrSumAPecas: TFloatField;
    QrSumAPesoKg: TFloatField;
    QrSumAAreaM2: TFloatField;
    QrRastroA: TMySQLQuery;
    QrRastroACodigo: TIntegerField;
    QrRastroAControle: TIntegerField;
    QrRastroAMovimCod: TIntegerField;
    QrRastroAPecas: TFloatField;
    QrRastroAPesoKg: TFloatField;
    QrRastroAAreaM2: TFloatField;
    QrRastroASdoVrtPeca: TFloatField;
    QrRastroASdoVrtPeso: TFloatField;
    QrRastroASdoVrtArM2: TFloatField;
    QrRastroAEmpresa: TIntegerField;
    QrRastroAAreaP2: TFloatField;
    QrRastroAValorT: TFloatField;
    BtTeste2: TBitBtn;
    QrBxaAll: TMySQLQuery;
    QrBxaAllCodigo: TIntegerField;
    QrBxaAllControle: TIntegerField;
    QrBxaAllMovimCod: TIntegerField;
    QrBxaAllMovimID: TIntegerField;
    QrBxaAllMovimNiv: TIntegerField;
    QrBxaAllPecas: TFloatField;
    QrBxaAllAreaM2: TFloatField;
    QrBxaAllValorT: TFloatField;
    QrBxaAllSdoVrtPeca: TFloatField;
    QrBxaAllSdoVrtArM2: TFloatField;
    QrBxaAllSrcMovID: TIntegerField;
    QrBxaAllSrcNivel1: TIntegerField;
    QrBxaAllSrcNivel2: TIntegerField;
    QrBxaAllDstMovID: TIntegerField;
    QrBxaAllDstNivel1: TIntegerField;
    QrBxaAllDstNivel2: TIntegerField;
    Panel5: TPanel;
    Label1: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel6: TPanel;
    Label3: TLabel;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrDiverge: TMySQLQuery;
    DsDiverge: TDataSource;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    BtAtzSdo: TBitBtn;
    PMAtzSdo: TPopupMenu;
    Atualizasaldodoitemselecionado1: TMenuItem;
    Atualizasaldodetodositens1: TMenuItem;
    QrDivergeControle: TIntegerField;
    QrDivergeMovimID: TIntegerField;
    QrDivergeMovimNiv: TIntegerField;
    QrDivergePallet: TIntegerField;
    QrDivergeSdoVrtPeca: TFloatField;
    QrDivergeSdoVrtArM2: TFloatField;
    QrDivergeVmiSVPc: TFloatField;
    QrDivergeVmiSVM2: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
    procedure BtAtzSdoClick(Sender: TObject);
    procedure Atualizasaldodoitemselecionado1Click(Sender: TObject);
    procedure Atualizasaldodetodositens1Click(Sender: TObject);
    procedure QrDivergeBeforeClose(DataSet: TDataSet);
    procedure QrDivergeAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FEmpresa, FGraGruX: Integer;
    FVSErrRastroSdo, FsEmpresa, FsGraGruX, FVSRastroPosit, FVSRastroNegat: String;
    procedure RastreiaA(EstqMovimID: TEstqMovimID; EstqMovimNiv: TEstqMovimNiv;
              Campo: String);
    function  ReopenMIDs(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmVSRastroSdo: TFmVSRastroSdo;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateVS, ModuleGeral, UMySQLModule,
  UnVS_PF, ModVS;

{$R *.DFM}

procedure TFmVSRastroSdo.Atualizasaldodetodositens1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
  QrDiverge.First;
  while not QrDiverge.Eof do
  begin
    VS_PF.AtualizaSaldoIMEI(QrDivergeControle.Value, False);
    QrDiverge.Next;
  end;
  UnDmkDAC_PF.AbreQuery(QrDiverge, DModG.MyPID_DB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRastroSdo.Atualizasaldodoitemselecionado1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
  VS_PF.AtualizaSaldoIMEI(QrDivergeControle.Value, False);
  UnDmkDAC_PF.AbreQuery(QrDiverge, DModG.MyPID_DB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRastroSdo.BtAtzSdoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtzSdo, BtAtzSdo);
end;

procedure TFmVSRastroSdo.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmVSRastroSdo.BtOKClick()';
var
  sMID, sNiv: String;
begin
  Screen.Cursor := crHourGlass;
  try
  FVSErrRastroSdo := UnCreateVS.RecriaTempTableNovo(ntrttVSRastroSdo,
    DModG.QrUpdPid1, False);
  //
  if not ReopenMIDs() then Exit;
  //
  QrMIDs.First;
  PB1.Position := 0;
  PB1.Max := QrMIDs.RecordCount;
  while not QrMIDs.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, 'Pesquisando IDNiv ' + Geral.FF0(QrMIDsMovIDNiv.Value));
    //
    case QrMIDsMovIDNiv.Value of
       2000,
       8001,
       12000,
       15011,
       17000,
       41000,
       999999: ; // nada
       7002,
       8002,
       15012: RastreiaA(TEstqMovimID(QrMIDsMovimID.Value), TEstqMovimNiv(QrMIDsMovimNiv.Value), 'DstNivel2');
       16000: RastreiaA(TEstqMovimID(QrMIDsMovimID.Value), TEstqMovimNiv(QrMIDsMovimNiv.Value), 'SrcNivel2');
      else
      begin
        sMID := GetEnumName(TypeInfo(TEstqMovimID), Integer(VarType(QrMIDsMovimID.Value)));
        sNiv := GetEnumName(TypeInfo(TEstqMovimNiv), Integer(VarType(QrMIDsMovimNiv.Value)));
        Geral.MB_Erro(sMid + '.' + sNiv + ' n�o implementado em ' + sProcName);
      end;
    end;
    //
    QrMIDs.Next;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRastroSdo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRastroSdo.BtTeste2Click(Sender: TObject);
var
  sControle, sPecas, sAreaM2, SQL: String;
begin
(*
  Deu diferen�a de 10 couros!
  Corrigir IMEIS e ver se melhora!
*)
  Screen.Cursor := crHourGlass;
  try
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando lan�amentos por Est�gio do Artigo');
  FVSRastroPosit := '_rastro_posit_'; //UnCreateVS.RecriaTempTableNovo(ntrttVSRastroSdo, DModG.QrUpdPid1, False);
  FVSRastroNegat := '_rastro_negat_'; //UnCreateVS.RecriaTempTableNovo(ntrttVSRastroSdo, DModG.QrUpdPid1, False);
  //
  if DmModVS.DstGGXDiferentesDeGraGruX() then Exit;
  if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False) then Exit;
  if DmModVS.CustosDiferentesOrigemParaDestino(FEmpresa, FGraGruX, False, False, False) then Exit;
  if VS_PF.RegistrosComProblema(FEmpresa, FGraGruX) then Exit;
  //
  if not ReopenMIDs() then Exit;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando lan�amentos positivos');
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _rastro_posit_;',
  'CREATE TABLE _rastro_posit_',
  'SELECT Codigo, Controle, MovimCod, MovimID, ',
  'MovimNiv, Pallet, Pecas, AreaM2, ValorT,',
  'Pecas SdoVrtPeca, AreaM2 SdoVrtArM2, ',
  'SrcMovID, SrcNivel1, SrcNivel2,',
  'DstMovID, DstNivel1, DstNivel2',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE Empresa=' + FsEmpresa,
  'AND GraGruX=' + FsGraGruX,
  'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0)',
  'AND (NOT (MovimID IN (9,12,17,41)));',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //Geral.MB_Teste(SQL);

  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando lan�amentos de baixa');
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS _rastro_negat_; ',
  'CREATE TABLE _rastro_negat_ ',
  'SELECT Codigo, Controle, MovimCod, MovimID,  ',
  'MovimNiv, Pecas, AreaM2, ValorT, ',
  '0.000 SdoVrtPeca, 0.00 SdoVrtArM2,  ',
  'SrcMovID, SrcNivel1, SrcNivel2, ',
  'DstMovID, DstNivel1, DstNivel2 ',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE ( ',
  '  (Pecas < 0) ',
  '  OR ',
  '  (AreaM2 < 0)  ',
  '  OR ',
  '  (pesoKg < 0) ',
  '  OR  ',
  '  (MovimID IN (9,12,17,41)) ',
  ') ',
  'AND GraGruX=' + FsGraGruX + '; ',
  '']);

  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //Geral.MB_Teste(SQL);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando saldo dos lan�amentos positivos');
  UnDmkDAC_PF.AbreMySQLQuery0(QrBxaAll, DModG.MyPID_DB, [
  'SELECT * FROM _rastro_negat_',
  '']);
  //
  PB2.Position := 0;
  PB2.Max := QrBxaAll.RecordCount;
  QrBxaAll.First;
  while not QrBxaAll.Eof do
  begin
    sControle := Geral.FF0(QrBxaAllSrcNivel2.Value);
    sPecas    := Geral.FFT_Dot(QrBxaAllPecas.Value, 3, siNegativo);
    sAreaM2   := Geral.FFT_Dot(QrBxaAllAreaM2.Value, 2, siNegativo);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    ' UPDATE ' + FVSRastroPosit +
    ' SET SdoVrtPeca = SdoVrtPeca + ' + sPecas + ',' +
    ' SdoVrtArM2 = SdoVrtArM2 + ' + sAreaM2 +
    ' WHERE Controle = ' + sControle +
    ' ');
    //
    QrBxaAll.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDiverge, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _rastro_saldo_; ',
  'CREATE TABLE _rastro_saldo_ ',
  'SELECT Controle, MovimID, MovimNiv, Pallet, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtArM2) SdoVrtArM2  ',
  'FROM _rastro_posit_ ',
  'WHERE ( ',
  '  (SdoVrtPeca <> 0) ',
  '  /*OR ',
  '  (SdoVrtPeso <> 0)*/ ',
  '  OR ',
  '  (SdoVrtArM2 <> 0) ',
  ')   ',
  'GROUP BY Controle; ',
  '',
  'SELECT _rs.*, vmi.SdoVrtPeca VmiSVPc, ',
  'vmi.SdoVrtArM2 VmiSVM2  ',
  'FROM _rastro_saldo_ _rs ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=_rs.Controle ',
  'WHERE vmi.SdoVrtPeca<>_rs.SdoVrtPeca ',
  '']);

  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRastroSdo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRastroSdo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSRastroSdo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRastroSdo.QrDivergeAfterOpen(DataSet: TDataSet);
begin
  BtAtzSdo.Enabled := QrDiverge.RecordCount > 0;
end;

procedure TFmVSRastroSdo.QrDivergeBeforeClose(DataSet: TDataSet);
begin
  BtAtzSdo.Enabled := False;
end;

procedure TFmVSRastroSdo.RastreiaA(EstqMovimID: TEstqMovimID;
  EstqMovimNiv: TEstqMovimNiv; Campo: String);
var
  Empresa, Codigo, Controle, MovimCod, GragruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
  ErrPeca, ErrPeso, ErrArM2: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRastroA, Dmod.MyDB, [
  'SELECT Empresa, Codigo, Controle, MovimCod, ',
  'Pecas, PesoKg, AreaM2, AreaP2, AreaP2, ValorT, SdoVrtPeca, ',
  'SdoVrtPeso, SdoVrtArM2  ',
  'FROM vsmovits ',
  'WHERE GraGruX=' + FsGraGruX,
  'AND MovimID=' + Geral.FF0(Integer(EstqMovimID)),
  'AND MovimNiv=' + Geral.FF0(Integer(EstqMovimNiv)),
  '']);
  PB2.Position := 0;
  PB2.Max := QrRastroA.RecordCount;
  QrRastroA.First;
  while not QrRastroA.Eof do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumA, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, ',
    'SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2   ',
    'FROM vsmovits ',
    'WHERE ' + Campo + '=' + Geral.FF0(QrRastroAControle.Value),
    '']);
    ErrPeca  := QrRastroAPecas.Value + QrSumAPecas.Value;
    ErrPeso := QrRastroAPesoKg.Value + QrSumAPesoKg.Value;
    ErrArM2 := QrRastroAAreaM2.Value + QrSumAAreaM2.Value;
    //
    if (ErrPeca <> 0) or (ErrPeso <> 0) or (ErrArM2 <> 0) then
    begin
      Empresa      := QrRastroAEmpresa .Value;
      Codigo       := QrRastroACodigo.Value;
      Controle     := QrRastroAControle.Value;
      MovimCod     := QrRastroAMovimCod.Value;
      GraGruX      := FGraGruX;
      Pecas        := QrRastroAPecas.Value;
      PesoKg       := QrRastroAPesoKg.Value;
      AreaM2       := QrRastroAAreaM2.Value;
      AreaP2       := QrRastroAAreaP2.Value;
      ValorT       := QrRastroAValorT.Value;
      SdoVrtPeca   := QrRastroASdoVrtPeca.Value;
      SdoVrtPeso   := QrRastroASdoVrtPeso.Value;
      SdoVrtArM2   := QrRastroASdoVrtArM2.Value;
      //
      //if
      UMyMod.SQLInsUpd(DModG.QrUpdPid1, stIns, FVSErrRastroSdo, False, [
      'Empresa', 'Codigo', 'Controle',
      'MovimCod', 'MovimID', 'MovimNiv', 'GraGruX', 'Pecas',
      'PesoKg', 'AreaM2', 'AreaP2',
      'ValorT', 'SdoVrtPeca', 'SdoVrtPeso',
      'SdoVrtArM2', 'ErrPeca', 'ErrPeso',
      'ErrArM2'], [
      ], [
      Empresa, Codigo, Controle,
      MovimCod, Integer(EstqMovimID), Integer(EstqMovimNiv),
      GraGruX, Pecas,
      PesoKg, AreaM2, AreaP2,
      ValorT, SdoVrtPeca, SdoVrtPeso,
      SdoVrtArM2, ErrPeca, ErrPeso,
      ErrArM2], [
      ], False);
    end;
    QrRastroA.Next;
  end;
end;

function TFmVSRastroSdo.ReopenMIDs(): Boolean;
var
  Filial: Integer;
begin
  Result := False;
  //
  Filial    := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  FEmpresa  := DModG.QrEmpresasCodigo.Value;
  FsEmpresa := Geral.FF0(FEmpresa);
  FGraGruX  := EdGraGruX.ValueVariant;
  if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then
    Exit;
  FsGraGruX := Geral.FF0(FGraGruX);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMIDs, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _teste_; ',
  'CREATE TABLE _teste_ ',
  'SELECT (MovimID * 1000) + MovimNiv MovIDNiv,  ',
  'MovimID, MovimNiv,  ',
  'COUNT(Controle) Itens, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtArM2) SdoVrtArM2  ',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE Empresa=' + FsEmpresa,
  'AND GraGruX=' + FsGraGruX,
  'GROUP BY MovimID, MovimNiv ',
  ' ',
  'UNION ',
  ' ',
  'SELECT 999999 MovIDNiv, ',
  '999 MovimID, 999 MovimNiv, ',
  'COUNT(Controle) Itens, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtArM2) SdoVrtArM2  ',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE Empresa=' + FsEmpresa,
  'AND GraGruX=' + FsGraGruX + ';',
  ' ',
  'SELECT * FROM _teste_ ',
  'ORDER BY MovimID, MovimNiv; ',
  ' ']);
  //
  Result := True;
end;

end.
