unit VSImpMartelo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  // Chart
  frxChart, TypInfo, Vcl.ExtCtrls,
  VCLTee.TeeProcs, VCLTee.TeEngine, VCLTee.Chart, VCLTee.Series, VCLTee.TeCanvas
  //TeeProcs, TeEngine, Chart, Series, TeCanvas
 {$IFDEF TeeChartPro}, VCLTEE.TeeEdit{$IFNDEF TeeChart4}, VCLTEE.TeeEditCha{$ENDIF} {$ENDIF}
 {$IFDEF TeeChartPro}, TeeEdit{$IFNDEF TeeChart4}, TeeEditCha{$ENDIF} {$ENDIF},
  // Fim Chart
  mySQLDbTables,
  dmkGeral, UnInternalConsts, UnDmkEnums, dmkRadioGroup, dmkCheckBox,
  Vcl.StdCtrls, Vcl.ComCtrls, dmkEditDateTimePicker, Vcl.DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Vcl.Buttons, dmkImage;

type
  TFmVSImpMartelo = class(TForm)
    frxWET_CURTI_018_05_A_0: TfrxReport;
    Qr05VSCacGBY: TmySQLQuery;
    Qr05VSCacGBYVMI_Dest: TIntegerField;
    Qr05VSCacGBYGraGruX: TIntegerField;
    Qr05VSCacGBYGraGru1: TIntegerField;
    Qr05VSCacGBYNO_PRD_TAM_COR: TWideStringField;
    Qr05VSCacGBYPecas: TFloatField;
    Qr05VSCacGBYAreaM2: TFloatField;
    Qr05VSCacGBYAreaP2: TFloatField;
    Qr05VSCacGBYPercentual: TFloatField;
    frxDs05VSCacGBY: TfrxDBDataset;
    Qr05SumGBY: TmySQLQuery;
    Qr05SumGBYPecas: TFloatField;
    Qr05SumGBYAreaM2: TFloatField;
    Qr05SumGBYAreaP2: TFloatField;
    frxWET_CURTI_018_05_B_1: TfrxReport;
    Qr05RevVSCac: TmySQLQuery;
    Qr05RevVSCacRevisor: TIntegerField;
    Qr05RevVSCacNO_Revisor: TWideStringField;
    Qr05RevVSCacGraGruX: TIntegerField;
    Qr05RevVSCacGraGru1: TIntegerField;
    Qr05RevVSCacNO_PRD_TAM_COR: TWideStringField;
    Qr05RevVSCacPecas: TFloatField;
    Qr05RevVSCacAreaM2: TFloatField;
    Qr05RevVSCacAreaP2: TFloatField;
    frxDs05RevVSCac: TfrxDBDataset;
    Qr05RevVSCacVMI_Dest: TIntegerField;
    Qr05RevSum: TmySQLQuery;
    Qr05RevSumRevisor: TIntegerField;
    Qr05RevSumPecas: TFloatField;
    Qr05RevSumAreaM2: TFloatField;
    Qr05RevSumAreaP2: TFloatField;
    Qr05RevVSCacSUM_ArM2: TFloatField;
    Qr05RevVSCacPercClaByRev: TFloatField;
    Qr05GgxSum: TmySQLQuery;
    Qr05GgxSumGraGruX: TIntegerField;
    Qr05GgxSumPecas: TFloatField;
    Qr05GgxSumAreaM2: TFloatField;
    Qr05GgxSumAreaP2: TFloatField;
    Qr05RevSumNO_Revisor: TWideStringField;
    Qr05VSCacGBYFatrClase: TFloatField;
    Qr05VSCacGBYFatrClaseVal: TFloatField;
    Qr05SumGBYFatrClase: TFloatField;
    Qr05VSCacGBYFatrClasePts: TFloatField;
    frxDS05SumGBY: TfrxDBDataset;
    frxWET_CURTI_018_05_A_1: TfrxReport;
    Qr05SumGBYFatrClaseVal: TFloatField;
    QrCorda: TMySQLQuery;
    Qr05VSCacGBYOrigArM2: TFloatField;
    Qr05VSCacGBYOrigValT: TFloatField;
    Qr05VSCacGBYCustoM2: TFloatField;
    Qr05VSCacGBYCUSTO_ITEM: TFloatField;
    Qr05Fornecedor: TMySQLQuery;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    Ds05Fornecedor: TDataSource;
    Qr05VSPallet: TMySQLQuery;
    IntegerField7: TIntegerField;
    StringField7: TWideStringField;
    Ds05VSPallet: TDataSource;
    Qr05GraGruX: TMySQLQuery;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    StringField8: TWideStringField;
    StringField9: TWideStringField;
    IntegerField10: TIntegerField;
    StringField10: TWideStringField;
    Ds05GraGruX: TDataSource;
    Qr05VSSerFch: TMySQLQuery;
    Qr05VSSerFchCodigo: TIntegerField;
    Qr05VSSerFchNome: TWideStringField;
    Panel13: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label16: TLabel;
    Ed05GraGruX: TdmkEditCB;
    CB05GraGruX: TdmkDBLookupComboBox;
    Ed05VSPallet: TdmkEditCB;
    CB05VSPallet: TdmkDBLookupComboBox;
    Ed05Terceiro: TdmkEditCB;
    CB05Terceiro: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    TP05DataIni: TdmkEditDateTimePicker;
    Ck05DataIni: TCheckBox;
    Ck05DataFim: TCheckBox;
    TP05DataFim: TdmkEditDateTimePicker;
    Ed05VMI_Sorc: TdmkEdit;
    Ed05Martelo: TdmkEditCB;
    CB05Martelo: TdmkDBLookupComboBox;
    Ed05VMI_Dest: TdmkEdit;
    Ed05SerieFch: TdmkEditCB;
    CB05SerieFch: TdmkDBLookupComboBox;
    Ed05Ficha: TdmkEdit;
    Ed05Marca: TdmkEdit;
    Ed05VSCacCod: TdmkEdit;
    RG05OperClass: TdmkRadioGroup;
    RG05Ordem: TRadioGroup;
    RG05AscDesc: TRadioGroup;
    Ck05Morto: TCheckBox;
    Ds05VSSerFch: TDataSource;
    Qr05VSMrtCad: TMySQLQuery;
    Qr05VSMrtCadCodigo: TIntegerField;
    Qr05VSMrtCadNome: TWideStringField;
    Ds05VSMrtCad: TDataSource;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    Panel14: TPanel;
    PnFichas: TPanel;
    BtImprime: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Qr05GraCusPrc: TMySQLQuery;
    Qr05GraCusPrcCodigo: TIntegerField;
    Qr05GraCusPrcNome: TWideStringField;
    Ds05GraCusPrc: TDataSource;
    Label61: TLabel;
    Ed05GraCusPrc: TdmkEditCB;
    CB05GraCusPrc: TdmkDBLookupComboBox;
    frxWET_CURTI_018_05_A_2: TfrxReport;
    Ck05EstoqueEm: TCheckBox;
    TP05EstoqueEm: TdmkEditDateTimePicker;
    Qr05VSCacGBYggv_Controle: TFloatField;
    Qr05VSCacGBYCustoPreco: TFloatField;
    Qr05VSCacGBYValorItem: TFloatField;
    Qr05VSCacGBYBaseValVenda: TFloatField;
    Qr05VSCacGBYBaseCliente: TWideStringField;
    Qr05VSCacGBYBaseImpostos: TFloatField;
    Qr05VSCacGBYBasePerComis: TFloatField;
    Qr05VSCacGBYBasFrteVendM2: TFloatField;
    Qr05VSCacGBYBaseValLiq: TFloatField;
    RGRelatorio: TRadioGroup;
    EdVariosIMEIS: TdmkEdit;
    Label1: TLabel;
    Qry: TMySQLQuery;
    Qr05RevVSCacInteiros: TFloatField;
    frxWET_CURTI_018_05_B_2: TfrxReport;
    QrCompraWB: TMySQLQuery;
    QrCompraWBCodigo: TIntegerField;
    QrCompraWBDataHora: TDateTimeField;
    QrCompraWBControle: TIntegerField;
    QrCompraWBPallet: TIntegerField;
    QrCompraWBGraGruX: TIntegerField;
    QrCompraWBPecas: TFloatField;
    QrCompraWBAreaM2: TFloatField;
    QrCompraWBAreaP2: TFloatField;
    QrCompraWBSdoVrtPeca: TFloatField;
    QrCompraWBSdoVrtArM2: TFloatField;
    QrCompraWBMarca: TWideStringField;
    QrCompraWBGraGru1: TIntegerField;
    QrCompraWBNO_PRD_TAM_COR: TWideStringField;
    QrCompraWBInteiros: TFloatField;
    frxDsCompraWB: TfrxDBDataset;
    QrCmprOtrs: TMySQLQuery;
    QrCmprOtrsPallet: TIntegerField;
    QrCmprOtrsPecas: TFloatField;
    QrCmprOtrsAreaM2: TFloatField;
    QrCmprSdos: TMySQLQuery;
    QrCmprSdosPallet: TIntegerField;
    QrCmprSdosSdoVrtPeca: TFloatField;
    QrCmprSdosSdoVrtArM2: TFloatField;
    QrCompraWBOtrsPecas: TFloatField;
    QrCompraWBOtrsAreaM2: TFloatField;
    QrCompraWBSdosSdoVrtPeca: TFloatField;
    QrCompraWBSdosSdoVrtArM2: TFloatField;
    CkShwPalOri: TCheckBox;
    procedure frxWET_CURTI_018_05_A_0GetValue(const VarName: string;
      var Value: Variant);
    procedure Qr05VSCacGBYCalcFields(DataSet: TDataSet);
    procedure Qr05RevVSCacCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure RG05OperClassClick(Sender: TObject);
  private
    { Private declarations }
    FVSCacGBY, FVsVmiPsq1, FVsVmiPsq2: String;
    //FArrAxeXNom: array of String;
    FArrAxeXCod: array of Integer;
    FArrAxeYSer: array of array of Double;
    FArrAxeRevNom: array of String;
    SeriesCount, SeriesItems: Integer;
    //2
    FDataEstoque: TDateTime;
    FGraCusPrc: Integer;
    //
    procedure ConfiguraChart(Chart1: TfrxChartView);
  public
    { Public declarations }
    EdEmpresa_ValueVariant, Ed05VSCacCod_ValueVariant, Ed05VSPallet_ValueVariant,
    Ed05VMI_Sorc_ValueVariant, Ed05VMI_Dest_ValueVariant,
    Ed05Martelo_ValueVariant, Ed05Ficha_ValueVariant, Ed05GraGruX_ValueVariant,
    Ed05Terceiro_ValueVariant: Integer;
    CBEmpresa_Text, Ed05Marca_Text, CB05GraGruX_Text, CB05Terceiro_Text,
    CB05SerieFch_Text: String;
    TP05DataIni_Date, TP05DataFim_Date: TDateTime;
    //Ck05Revisores_Checked,
    Ck05DataIni_Checked, Ck05DataFim_Checked: Boolean;
    RG05OperClass_ItemIndex, RG05Ordem_ItemIndex, RG05AscDesc_ItemIndex: Integer;
    //Ck05MostraPontos_Checked,
    Ck05Morto_Checked: Boolean;
    FEntidade: Integer;
    //
    procedure ImprimePesquisaMartelos();
  end;

var
  FmVSImpMartelo: TFmVSImpMartelo;

implementation

{$R *.dfm}

uses ModuleGeral, UnDmkProcFunc, CreateVS, DmkDAC_PF, UnMyObjects,
  UnVS_PF, Module;

{ TFmVSImpMartelo }

const
  FGraGruVal = '_gragruval_05_';

procedure TFmVSImpMartelo.BtImprimeClick(Sender: TObject);
begin
  if MyObjects.FIC(RGRelatorio.ItemIndex = -1, RGRelatorio,
  'Selecione o relat�rio!') then Exit;
  //
  FGraCusPrc := Ed05GraCusPrc.ValueVariant;
  if Ck05EstoqueEm.Checked then
    FDataEstoque := Trunc(TP05EstoqueEm.Date)
  else
    FDataEstoque := Trunc(Date());
  //
  Ed05VSCacCod_ValueVariant := Ed05VSCacCod.ValueVariant;
  Ed05VSPallet_ValueVariant := Ed05VSPallet.ValueVariant;
  Ed05VMI_Sorc_ValueVariant := Ed05VMI_Sorc.ValueVariant;
  Ed05VMI_Dest_ValueVariant := Ed05VMI_Dest.ValueVariant;
  Ed05Martelo_ValueVariant  := Ed05Martelo.ValueVariant;
  Ed05Ficha_ValueVariant    := Ed05Ficha.ValueVariant;
  Ed05GraGruX_ValueVariant  := Ed05GraGruX.ValueVariant;
  Ed05Terceiro_ValueVariant := Ed05Terceiro.ValueVariant;
  //CBEmpresa_Text            := CBEmpresa_Text;
  Ed05Marca_Text            := Ed05Marca.Text;
  CB05GraGruX_Text          := CB05GraGruX.Text;
  CB05Terceiro_Text         := CB05Terceiro.Text;
  CB05SerieFch_Text         := CB05SerieFch.Text;
  TP05DataIni_Date          := TP05DataIni.Date;
  TP05DataFim_Date          := TP05DataFim.Date;
  Ck05DataIni_Checked       := Ck05DataIni.Checked;
  Ck05DataFim_Checked       := Ck05DataFim.Checked;
  //Ck05Revisores_Checked     := Ck05Revisores.Checked;
  RG05OperClass_ItemIndex   := RG05OperClass.ItemIndex;
  RG05Ordem_ItemIndex       := RG05Ordem.ItemIndex;
  RG05AscDesc_ItemIndex     := RG05AscDesc.ItemIndex;
  //Ck05MostraPontos_Checked  := RGRelatorio.ItemIndex = 1;
  Ck05Morto_Checked         := Ck05Morto.Checked;
  //
  ImprimePesquisaMartelos();
  //
end;

procedure TFmVSImpMartelo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpMartelo.ConfiguraChart(Chart1: TfrxChartView);
const
  myclSerieAreaLinHE = clGray;
  myclSerieAreaLinVI = clGray;
var
  I, J: Integer;
  YSource: String;
  iSerie, ChartSerie: TfrxChartSeries;
begin
  //Chart1 := frxReport.FindObject('Chart1') as TfrxChartView;
  // Remover series (gr�ficos)
  //while Chart1.SeriesCount > 0 do
    //Chart1.Series[0].Free;
  while Chart1.SeriesData.Count > 0 do
    Chart1.SeriesData[0].Free;
  while Chart1.Chart.SeriesCount > 0 do
    Chart1.Chart.Series[0].Free;
  // Desfazer 3D
  //Chart1.View3D := False;
  Chart1.Chart.View3D := False;
  // Tirar legendas do lado
  //Chart1.Legend.Visible := False;
  Chart1.Chart.Legend.Visible := True;
  //Chart1.Title.Font.Size := 14;




  Chart1.Chart.Legend.ShadowSize := 0;




  Chart1.Chart.Title.Font.Size := 14;
  //Chart1.Title.Font.Name := 'Calibri';
  Chart1.Chart.Title.Font.Name := 'Calibri';
  //Chart1.Title.Font.Color := VAR_COR_TIT_C;
  Chart1.Chart.Title.Font.Color := VAR_COR_TIT_C;

  // Configurar Gr�fico em si (sem nenhuma s�rie - fundo vazio)
  //Chart1.Color := myclChartAreaFundo;
  Chart1.Chart.Color := clWhite; // clRed;
  Chart1.Frame.Color := clGreen;

  //Chart1.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  //Chart1.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  //Chart1.LeftAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.Chart.LeftAxis.Grid.Color     := myclSerieAreaLinHE;
  //Chart1.LeftAxis.Grid.DrawEvery := 3; //
  Chart1.Chart.LeftAxis.Grid.DrawEvery := 3; //
  //Chart1.LeftAxis.Grid.Style     := TPenStyle.psSolid;
  Chart1.Chart.LeftAxis.Grid.Style     := TPenStyle.psSolid;

  //Chart1.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  //Chart1.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  //Chart1.BottomAxis.MinorTicks.Visible := False;
  Chart1.Chart.BottomAxis.MinorTicks.Visible := False;
  //Chart1.BottomAxis.Grid.Visible   := True;
  Chart1.Chart.BottomAxis.Grid.Visible   := True;
  //Chart1.BottomAxis.Grid.DrawEvery := 1;
  Chart1.Chart.BottomAxis.Grid.DrawEvery := 1;
  //Chart1.BottomAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.Chart.BottomAxis.Grid.Color     := myclSerieAreaLinHE;
  //Chart1.BottomAxis.Grid.Style     := TPenStyle.psSolid;
  Chart1.Chart.BottomAxis.Grid.Style     := TPenStyle.psSolid;

  //Chart1.BottomAxis.Ticks.Color    := myclSerieAreaLinVI;
  Chart1.Chart.BottomAxis.Ticks.Color    := myclSerieAreaLinVI;
  //Chart1.BottomAxis.Ticks.Width    := 1;
  Chart1.Chart.BottomAxis.Ticks.Width    := 1;



  //Chart1.TopAxis.Axis.Color := clWhite;
  Chart1.Chart.TopAxis.Axis.Color := clWhite;
  //Chart1.TopAxis.Axis.Width := 1;
  Chart1.Chart.TopAxis.Axis.Width := 1;

  //Chart1.RightAxis.Axis.Color := clWhite;
  Chart1.Chart.RightAxis.Axis.Color := clWhite;
  //Chart1.RightAxis.Axis.Width := 1;
  Chart1.Chart.RightAxis.Axis.Width := 1;

 //
  // Evitar erro no Destroy;
  //iSerie := nil;
  //ChartSerie := nil;
end;

procedure TFmVSImpMartelo.FormActivate(Sender: TObject);
begin
    MyObjects.CorIniComponente();
end;

procedure TFmVSImpMartelo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  TP05DataIni.Date := Date - 30;
  TP05DataFim.Date := Date;
  //
  TP05EstoqueEm.Date := Geral.PrimeiroDiaDoMes(Date()) - 1;
  //
  if Qr05GraGruX.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05GraGruX, Dmod.MyDB);
  if Qr05VSPallet.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05VSPallet, Dmod.MyDB);
  if Qr05VSMrtCad.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05VSMrtCad, Dmod.MyDB);
  if Qr05Fornecedor.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05Fornecedor, Dmod.MyDB);
  if Qr05VSSerFch.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05VSSerFch, Dmod.MyDB);
  if Qr05GraCusPrc.State = dsInactive then
    UnDmkDAC_PF.AbreQuery(Qr05GraCusPrc, Dmod.MyDB);
end;

procedure TFmVSImpMartelo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpMartelo.frxWET_CURTI_018_05_A_0GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa_Text, Geral.FF0(FEntidade), 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_ShwPalOri' then
    Value := CkShwPalOri.Checked
end;

procedure TFmVSImpMartelo.ImprimePesquisaMartelos();
  const
    sProcName = 'TFmVSImpMartelo.ImprimePesquisaMartelos()';
  //
  procedure MsgRelNaoImpl();
  begin
    Geral.MB_Erro('Relat�rio "' + RGRelatorio.Items[RGRelatorio.ItemIndex] + '" n�o implementado em ' + sProcName);
  end;
  var
    frxReport: TfrxReport;
  //
  function I2T(Variable: String; Codigo: Integer; Texto: String): String;
  var
    Txt: String;
  begin
    if Codigo = 0 then
      Txt := 'Qualquer'
    else
      Txt := Texto;
    //frxWET_CURTI_018_05_A.Variables[Variable] := QuotedStr(Txt);
    frxReport.Variables[Variable] := QuotedStr(Txt);
  end;
  function T2T(Variable, Texto: String): String;
  var
    Txt: String;
  begin
    if Trim(Texto) = '' then
      Txt := 'Qualquer'
    else
      Txt := Texto;
    //frxWET_CURTI_018_05_A.Variables[Variable] := QuotedStr(Txt);
    frxReport.Variables[Variable] := QuotedStr(Txt);
  end;
(*
  function T1T(Variable, Texto: String): String;
  begin
    frxWET_CURTI_018_05_A.Variables[Variable] := QuotedStr(Texto);
  end;
*)
var
  CacCod, CacID, Codigo, VSPallet, VMI_Sorc, VMI_Dest, VMI_Baix, Box, Revisor,
  Digitador, Martelo, Ficha, SerieFicha, GraGruX, Terceiro, Item, I, J: Integer;
  Valor: Double;
  Marca, Periodo,
  LJN_VMI_SRC, SQL_Marca,
  SQL_CacCod, SQL_CacID, SQL_Codigo, SQL_VSPallet, SQL_VMI_Sorc, SQL_VMI_Dest,
  SQL_VMI_Baix, SQL_Box, SQL_Revisor, SQL_Digitador, SQL_Martelo, SQL_Ficha,
  SQL_SerieFicha, SQL_GraGruX, SQL_Terceiro, SQL_Periodo, FldVsVmiPsq,
  YSource, XSource, sOrdem, sGrupo, SQL_Period16, Corda1, Corda2, Corda3, SQL_VSMovItx: String;
  Chart1: TfrxChartView;
  PieSeries: TPieSeries;
  AscDesc: String;
  //
  function GeraSQLVSMovItxA(TabMov, TabCac: TTabToWork; Campo: String): String;
  var
    Une: Boolean;
    SQL, Corda: String;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    if (not Ck05Morto_Checked) and Une then
    begin
      Result := '';
      Exit;
    end;

    if Campo = 'VMI_Sorc' then
      SQL := 'Controle, Terceiro, Pallet, GraGruX, SerieFch, Ficha, Marca, ' +
      ' IF(AreaM2 > 0, ValorT/AreaM2, 0) CUSTOM2, AreaM2, ValorT, '
    else
      SQL := 'Controle, GraGruX, ';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
    '  SELECT DISTINCT cia.' + Campo + ' ',
    '  FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
    '  WHERE cia.VMI_Sorc<>0  ',
    SQL_CacCod,
    SQL_CacID,
    SQL_Codigo,
    //SQL_VSPallet,
    SQL_VMI_Sorc,
    SQL_VMI_Dest,
    SQL_VMI_Baix,
    SQL_Box,
    SQL_Revisor,
    SQL_Digitador,
    SQL_Martelo,
    SQL_SerieFicha,
    //SQL_Ficha,
    //SQL_GraGruX,
    //SQL_Terceiro,
    //SQL_Marca,
    SQL_Periodo,
    '']);
    //Geral.MB_teste(QrCorda.SQL.Text);
    Corda := MyObjects.CordaDeQuery(QrCorda, Campo, EmptyStr);
    //
    if Corda = EmptyStr then
    begin
      Result := EmptyStr;
    end else
    begin
      Result := Geral.ATS([
      'SELECT DISTINCT  ' + SQL + ' 1 Ativo ',
      'FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
      'WHERE Controle IN ( ' + Corda,
  (*
      '  SELECT DISTINCT cia.' + Campo + ' ',
      '  FROM ' + TMeuDB + '.' + VS_PF.TabCacVS_Tab(TabCac) + ' cia ',
      '  WHERE cia.VMI_Sorc<>0  ',
      SQL_CacCod,
      SQL_CacID,
      SQL_Codigo,
      //SQL_VSPallet,
      SQL_VMI_Sorc,
      SQL_VMI_Dest,
      SQL_VMI_Baix,
      SQL_Box,
      SQL_Revisor,
      SQL_Digitador,
      SQL_Martelo,
      SQL_SerieFicha,
      //SQL_Ficha,
      //SQL_GraGruX,
      //SQL_Terceiro,
      //SQL_Marca,
      SQL_Periodo,
  *)
      ') ',
      Geral.ATS_If(Une, ['', 'UNION']),
      '']);
    end;
  end;
  function GeraSQLVSMovItxB(TabMov, TabCac: TTabToWork; Campo, Corda: String): String;
  var
    Une: Boolean;
    SQL: String;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    if (not Ck05Morto_Checked) and Une then
    begin
      Result := '';
      Exit;
    end;
    //
    if Campo = 'VMI_Sorc' then
      SQL := 'Controle, Terceiro, Pallet, GraGruX, SerieFch, Ficha, Marca, ' +
      ' IF(AreaM2 > 0, ValorT/AreaM2, 0) CUSTOM2, AreaM2, ValorT, '
    else
      SQL := 'Controle, GraGruX, ';
    //
    Result := Geral.ATS([
    'SELECT DISTINCT  ' + SQL + ' 1 Ativo ',
    'FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    'WHERE Controle IN ( ' + Corda,
    ') ',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
  function GeraSQLVSMovItxC(TabMov, TabCac: TTabToWork; Campo, Corda: String): String;
  var
    Une: Boolean;
    SQL: String;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    if (not Ck05Morto_Checked) and Une then
    begin
      Result := '';
      Exit;
    end;

    SQL := 'Controle, GraGruX, ';
    //
    Result := Geral.ATS([
    'SELECT DISTINCT  ' + SQL + ' 1 Ativo ',
    'FROM ' + TMeuDB + '.' + VS_PF.TabMovVS_Tab(TabMov) + ' vmi ',
    'WHERE Controle IN ( ' + Corda,
    ') ',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
var
  SQL_Valores, LJ_Valores, VariosIMEIS: String;
begin
  FVSCacGBY :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSCacGBY, DModG.QrUpdPID1, False);
  FVsVmiPsq1 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVsVmiPsq1, DModG.QrUpdPID1, False);
  FVsVmiPsq2 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVsVmiPsq2, DModG.QrUpdPID1, False);
    //
  begin
    SQL_Valores := ', 0.000 ggv_Controle, 0.000000 CustoPreco, 0.00 ValorItem';
    LJ_Valores  := '';
  end;
  LJN_VMI_SRC    := '';
  //
  SQL_CacCod     := '';
  SQL_CacID      := '';
  SQL_Codigo     := '';
  SQL_VSPallet   := '';
  SQL_VMI_Sorc   := '';
  SQL_VMI_Dest   := '';
  SQL_VMI_Baix   := '';
  SQL_Box        := '';
  SQL_Revisor    := '';
  SQL_Digitador  := '';
  SQL_Martelo    := '';
  SQL_Periodo    := '';
  SQL_Ficha      := '';
  SQL_SerieFicha := '';
  SQL_GraGruX    := '';
  SQL_Terceiro   := '';
  SQL_Marca      := '';
  //
  CacCod         := Ed05VSCacCod_ValueVariant;
  CacID          := 0;//
  Codigo         := 0;//
  VSPallet       := Ed05VSPallet_ValueVariant;
  VMI_Sorc       := Ed05VMI_Sorc_ValueVariant;
  VMI_Dest       := Ed05VMI_Dest_ValueVariant;
  VMI_Baix       := 0;//Ed05VMI_Baix.ValueVariant;
  Box            := 0;//Ed05Box.ValueVariant;
  Revisor        := 0;//
  Digitador      := 0;//
  Martelo        := Ed05Martelo_ValueVariant;
  Ficha          := Ed05Ficha_ValueVariant;
  GraGruX        := Ed05GraGruX_ValueVariant;
  Terceiro       := Ed05Terceiro_ValueVariant;
  Marca          := Ed05Marca_Text;
  Periodo        := dmkPF.PeriodoImp1(TP05DataIni_Date, TP05DataFim_Date,
    Ck05DataIni_Checked, Ck05DataFim_Checked, '', 'at�', '');
  //
  if CacCod <> 0 then
    SQL_CacCod := 'AND cia.CacCod=' + Geral.FF0(CacCod);
  //SQL_CacID     := 'AND cia.CacID IN (7,8)';
  //SQL_Codigo    := 'AND cia.Codigo = 5';
  //if VSPallet <> 0 then
  //  SQL_VSPallet  := 'AND cia.VSPallet=' + Geral.FF0(VSPallet);
  if VMI_Sorc <> 0 then
    SQL_VMI_Sorc  := 'AND cia.VMI_Sorc=' + Geral.FF0(VMI_Sorc);
  // ini 2022-03-28
  VariosIMEIS := Trim(EdVariosIMEIS.Text);
  if VariosIMEIS <> EmptyStr then
  begin
    SQL_VMI_Sorc  := 'AND cia.VMI_Sorc IN (' + VariosIMEIS + ')';
  end;
  // ini 2022-03-28
  if VMI_Dest <> 0 then
    SQL_VMI_Dest  := 'AND cia.VMI_Dest=' + Geral.FF0(VMI_Dest);
  if VMI_Baix <> 0 then
    SQL_VMI_Baix  := 'AND cia.VMI_Baix=' + Geral.FF0(VMI_Baix);
  if Box <> 0 then
    SQL_Box  := 'AND cia.Box=' + Geral.FF0(Box);
  if Revisor <> 0 then
    SQL_Revisor  := 'AND cia.Revisor=' + Geral.FF0(Revisor);
  if Digitador <> 0 then
    SQL_Digitador  := 'AND cia.Digitador=' + Geral.FF0(Digitador);
  if Martelo <> 0 then
    SQL_Martelo  := 'AND cia.Martelo=' + Geral.FF0(Martelo);
  SQL_Periodo := dmkPF.SQL_Periodo('AND cia.DataHora ',
    TP05DataIni_Date, TP05DataFim_Date, Ck05DataIni_Checked, Ck05DataFim_Checked);
  if VSPallet <> 0 then
    SQL_VSPallet  := 'AND src.Pallet=' + Geral.FF0(VSPallet);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND src.Ficha=' + Geral.FF0(Ficha);
  if SerieFicha <> 0 then
    SQL_SerieFicha  := 'AND src.SerieFicha=' + Geral.FF0(SerieFicha);
  if GraGruX <> 0 then
    SQL_GraGruX  := 'AND src.GraGruX=' + Geral.FF0(GraGruX);
  if Terceiro <> 0 then
    SQL_Terceiro  := 'AND src.Terceiro=' + Geral.FF0(Terceiro);
  if Marca <> '' then
    SQL_Marca  := 'AND src.Marca = "' + Marca + '"';
  case RG05OperClass_ItemIndex of
    0: SQL_CacID := 'AND cia.CacID=7';
    1: SQL_CacID := 'AND cia.CacID=8';
    2:
    begin
      // Obter IME-Is de Compra do per�odo
      SQL_Period16 := dmkPF.SQL_Periodo('AND cab.DtEntrada ',  TP05DataIni_Date,
        TP05DataFim_Date, Ck05DataIni_Checked, Ck05DataFim_Checked);
      // IME-Is de Compra do per�odo
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      // Pesquisar por Pallet e n�o por IME-I, pois o usu�rio pode gerar mais de
      // uma pr�-reclasse por IME-I, mas o pallet de origem permanece o mesmo!
      // E quando for fazer a segunda reclasse em diante os pallets ser�o outros
      // posteriores!
      //
      //'SELECT vmi.Controle  ',
      'SELECT vmi.Pallet ',
      'FROM vsmovits vmi ',
      'LEFT JOIN vsplccab cab ON cab.MovimCod=vmi.MovimCod ',
      'WHERE vmi.MovimID=16 ',
      SQL_Period16,
      '']);
      //Geral.MB_Teste(Qry.SQL.Text);
      //Corda1 := MyObjects.CordaDeQuery(Qry, 'Controle', '-999999999');
      Corda1 := MyObjects.CordaDeQuery(Qry, 'Pallet', '-999999999');
      //Geral.MB_Teste(Corda1);
      //
      // IME-Cs de Pr� reclasse dos IME-Is de compra:
      // Na pesquisa por pallet n�o � necess�rio saber os IME-Cs de pr�-reclasse...
      (*UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MovimCod  ',
      'FROM vsmovits ',
      'WHERE SrcMovID=16 ',
      'AND SrcNivel2 IN (' + Corda1 + ') ',
      '']);
      //Geral.MB_Teste(Qry.SQL.Text);
      Corda2 := MyObjects.CordaDeQuery(Qry, 'MovimCod', '-999999999');
      //Geral.MB_Teste(Corda2);*)
      //
      // IME-Is de Ordem de Classifica��o dos IME-Is de compra
      // ...na pesquisa por pallet a pesquisa � feita diretamete nas
      // pr�-reclasses dos pallets de entrada.
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
(*
      'SELECT Controle ',
      'FROM vsmovits ',
      'WHERE MovimID=15 ',
      'AND MovimNiv=12 ',
      'AND MovimCod IN (' + Corda2 + ') ',
*)
      'SELECT vmi.Controle ',
      'FROM vsmovits vmi ',
      'WHERE MovimID=15 ',
      'AND MovimNiv=12 ',
      'AND Pallet IN (' + Corda1 + ')',
      '']);
      //Geral.MB_Teste(Qry.SQL.Text);
      Corda3 := MyObjects.CordaDeQuery(Qry, 'Controle', '-999999999');
      //Geral.MB_Teste(Corda3);
      //
       SQL_CacID := 'AND cia.VMI_Sorc IN (' + Corda3 + ')';
    end;
    3: SQL_CacID := '';
    else SQL_CacID := 'AND cia.CacID=???';
  end;
  //
  //if (VMI_Sorc <> 0) or (Ficha <> 0) or (SerieFicha <> 0) or (GraGruX <> 0)
  //or (VSPallet <> 0) or (GraGruX <> 0) or (Terceiro <> 0) or (Marca <> "") then
    LJN_VMI_SRC   := 'LEFT JOIN ' + FVsVmiPsq1 + ' src ON src.Controle=cia.VMI_Sorc';
  //
  // Criar tabela copiando dados uteis das VSMovIt'x' para LEFT JOIN da pesquisa
  // principal feita posteriormente (VSCacGBY)
  FldVsVmiPsq := 'VMI_Sorc';
  if RG05OperClass_ItemIndex = 2 then
  begin
    SQL_VSMovItx := Geral.ATS([
    GeraSQLVSMovItxB(ttwB, ttwB, FldVsVmiPsq, Corda3),
    GeraSQLVSMovItxB(ttwB, ttwA, FldVsVmiPsq, Corda3),
    GeraSQLVSMovItxB(ttwA, ttwB, FldVsVmiPsq, Corda3),
    GeraSQLVSMovItxB(ttwA, ttwA, FldVsVmiPsq, Corda3),
    '']);
    //
    SQL_Periodo := '';
  end else
    SQL_VSMovItx := Geral.ATS([
    GeraSQLVSMovItxA(ttwB, ttwB, FldVsVmiPsq),
    GeraSQLVSMovItxA(ttwB, ttwA, FldVsVmiPsq),
    GeraSQLVSMovItxA(ttwA, ttwB, FldVsVmiPsq),
    GeraSQLVSMovItxA(ttwA, ttwA, FldVsVmiPsq),
    '']);
  //
  //Geral.MB_Teste(SQL_VSMovItx);
  if Trim(SQL_VSMovItx) = EmptyStr then
  begin
    Geral.MB_Info('N�o h� movimento para esta configura��o!');
    Exit;
  end;
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVsVmiPsq1,
  '',
  SQL_VSMovItx,
  ';']);
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVSCacGBY,
  '',
  'SELECT cia.CacCod, cia.CacID, cia.Codigo, cia.VSPallet, cia.VMI_Sorc, ',
  'cia.VMI_Dest, cia.VMI_Baix, cia.Box, cia.Revisor, cia.Digitador, ',
  'cia.Martelo, SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2, ',
  'SUM(cia.AreaP2) AreaP2, 0 Tamanho, SUM(src.AreaM2) OrigArM2, SUM(src.ValorT) OrigValT, 1 Ativo ',
  'FROM ' + TMeuDB + '.vscacitsa cia ',
  LJN_VMI_SRC,
  'WHERE cia.Controle<>0 ',
  SQL_CacCod,
  SQL_CacID,
  SQL_Codigo,
  SQL_VSPallet,
  SQL_VMI_Sorc,
  SQL_VMI_Dest,
  SQL_VMI_Baix,
  SQL_Box,
  SQL_Revisor,
  SQL_Digitador,
  SQL_Martelo,
  SQL_SerieFicha,
  SQL_Ficha,
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_Marca,
  SQL_Periodo,
  'GROUP BY CacCod, CacID, Codigo, VSPallet, VMI_Sorc, VMI_Dest, ',
  'VMI_Baix, Box, Revisor, Digitador, Martelo ',
  '']);
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);




  //
  //
  // Criar tabela copiando dados uteis das VSMovIt'x' para LEFT JOIN da pesquisa
  // principal feita posteriormente (VSCacGBY)
  // ini 2023-11-17
  FldVsVmiPsq := 'VMI_Dest';
  SQL_VSMovItx := Geral.ATS([
  GeraSQLVSMovItxA(ttwB, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwB, ttwA, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwA, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwA, ttwA, FldVsVmiPsq),
  '']);
  // fim 2023-11-17
  if Trim(SQL_VSMovItx) = EmptyStr then
  begin
    Geral.MB_Info('N�o h� movimento para esta configura��o!');
    Exit;
  end;
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVsVmiPsq2,
  '',
  // ini 2023-11-17
  (*
  GeraSQLVSMovItxA(ttwB, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwB, ttwA, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwA, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItxA(ttwA, ttwA, FldVsVmiPsq),
  *)
  SQL_VSMovItx,
  ';']);
  // fim 2023-11-17
  //
  //
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);


  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  if GraGruX <> 0 then
    SQL_GraGruX  := 'AND src.GraGruX=' + Geral.FF0(GraGruX);
  // Deve ser antes por causa do CalcFields
  UnDmkDAC_PF.AbreMySQLQuery0(Qr05SumGBY, DModG.MyPID_DB, [
  'SELECT SUM(gby.Pecas) Pecas, ',
  'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2, ',
  'SUM(gby.AreaM2 * cou.FatrClase) FatrClaseVal, ',
  'SUM(gby.AreaM2 * cou.FatrClase) / SUM(gby.AreaM2) FatrClase ',
  'FROM ' + FVSCacGBY + ' gby ',
  'LEFT JOIN ' + FVsVmiPsq2 + ' vmi ON vmi.Controle=gby.VMI_Dest ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  '']);
  //Geral.MB_Teste(Qr05SumGBY.SQL.Text);
  //
  //
{
  // Criar tabela copiando dados uteis das VSMovIt'x' para LEFT JOIN da pesquisa
  // principal feita posteriormente (VSCacGBY)
  FldVsVmiPsq := 'VMI_Dest';
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVsVmiPsq2,
  '',
  GeraSQLVSMovItx(ttwB, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItx(ttwB, ttwA, FldVsVmiPsq),
  GeraSQLVSMovItx(ttwA, ttwB, FldVsVmiPsq),
  GeraSQLVSMovItx(ttwA, ttwA, FldVsVmiPsq),
  ';']);
  //
}
  case RG05Ordem_ItemIndex of
    0: sOrdem := 'ORDER BY AreaM2';     //�rea
    1: sOrdem := 'ORDER BY FatrClase';  //Fator (ordem) classe
    2: sOrdem := 'ORDER BY NO_PRD_TAM_COR'; //Nome
    else sOrdem := 'ORDER BY ???';
  end;
  if RG05AscDesc_ItemIndex = 1 then
    sOrdem := sOrdem + ' DESC';
  //
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr05VSCacGBY, DModG.MyPID_DB, [
  'SELECT gby.VMI_Dest, vmi.GraGruX, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'SUM(gby.Pecas) Pecas, ',
  'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2, ',
  'cou.FatrClase, SUM(gby.AreaM2 * cou.FatrClase) FatrClaseVal, ',
  '(SUM(gby.AreaM2 * cou.FatrClase * 100) / ' +
    Geral.FFT_Dot(Qr05SumGBYFatrClaseVal.Value, 6, siNegativo) + ') FatrClasePts, ',
  'cou.BaseValVenda, cou.BaseCliente, cou.BaseImpostos, cou.BasePerComis, ',
  'cou.BasFrteVendM2, cou.BaseValLiq, ',
  'SUM(gby.OrigArM2) OrigArM2, SUM(gby.OrigValT) OrigValT, ',
  'IF(SUM(gby.OrigArM2) > 0, SUM(gby.OrigValT) / SUM(gby.OrigArM2), 0) CustoM2, ',
  'SUM(gby.AreaM2) * IF(SUM(gby.OrigArM2) > 0, SUM(gby.OrigValT) / SUM(gby.OrigArM2), 0) CUSTO_ITEM ',

  SQL_VALORES,

  'FROM ' + FVSCacGBY + ' gby ',
  'LEFT JOIN ' + FVsVmiPsq2 + ' vmi ON vmi.Controle=gby.VMI_Dest ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=ggx.Controle ',

  LJ_VALORES,

  'GROUP BY vmi.GraGruX ',
  //'ORDER BY AreaM2 DESC ',
  sOrdem,
  '']);
  //Geral.MB_Teste(Qr05VSCacGBY.SQL.Text);
  //
  //if not Ck05Revisores_Checked then
  if RGRelatorio.ItemIndex <> 3 then
  begin
    case RGRelatorio.ItemIndex of
      0: frxReport := frxWET_CURTI_018_05_A_0;
      //if Ck05MostraPontos_Checked then
      1: frxReport := frxWET_CURTI_018_05_A_1;
      2: frxReport := frxWET_CURTI_018_05_A_2;
      else MsgRelNaoImpl();
    end;
    //
    //Geral.MB_Info(Qr05VSCacGBY.SQL.Text);
    if Qr05VSCacGBY.RecordCount = 0 then
    begin
      Geral.MB_Info('N�o h� informa��o a ser mostrada!');
      Exit;
    end;
    I2T('VARF_GraGruX', GraGruX, CB05GraGruX_Text);
    I2T('VARF_VSPallet', VSPallet, Geral.FF0(VSPallet));
    I2T('VARF_Terceiro', Terceiro, CB05Terceiro_Text);
    I2T('VARF_SerieFch', SerieFicha, CB05SerieFch_Text);
    I2T('VARF_Ficha',    Ficha, Geral.FF0(Ficha));
    T2T('VARF_Marca',    Marca);
    I2T('VARF_VSCacCod', CacCod, Geral.FF0(CacCod));
    I2T('VARF_VMI_Sorc', VMI_Sorc, Geral.FF0(VMI_Sorc));
    I2T('VARF_VMI_Dest', VMI_Dest, Geral.FF0(VMI_Dest));
    I2T('VARF_Martelo',  Martelo, Geral.FF0(Martelo));
    T2T('VARF_Periodo',  Periodo);
    //
    MyObjects.frxDefineDataSets(frxReport, [
      DModG.frxDsDono,
      frxDs05VSCacGBY,
      frxDS05SumGBY
    ]);
    if RGRelatorio.ItemIndex <> 2 then
    begin
      Chart1 := frxReport.FindObject('ChartPie0') as TfrxChartView;
      Chart1.Chart.Legend.Visible := False;
      //Chart1.Chart.Legend.Alignment := laBottom;
      //Chart1.Chart.Legend.Shadow.Visible := False;
      Chart1.Chart.View3D := False;

      // do VCLTee.Chart.CustomChart:
      Chart1.Chart.Series[0].Marks.Font.Size := 6;
      Chart1.Chart.Series[0].Marks.Transparent := True;
      Chart1.Chart.Series[0].Marks.Shadow.Visible := False;
      PieSeries := TPieSeries(Chart1.Chart.Series[0]);
      PieSeries.Circled := True;

      // do frxChat
      //Chart1.SeriesData[0].TopN := 20;
      //Chart1.SeriesData[0].TopNCaption := FOutrosChart;
      //Chart1.SeriesData.Items[0].Circled := True;
      //
    end;
    MyObjects.frxMostra(frxReport, 'Resultado de Classifica��o');
  end
  else
  begin
    if RG05OperClass.ItemIndex = 2 then
    begin
      frxReport := frxWET_CURTI_018_05_B_2;
      //
      // ==============================================
      // ini devem ser antes pr causa do lookup do QrCompraWB
      // Faltas, sobras ou saldo a classificar nas reclasses n�o encerradas
      UnDmkDAC_PF.AbreMySQLQuery0(QrCmprSdos, Dmod.MyDB, [
      'SELECT Pallet, SUM(SdoVrtPeca) SdoVrtPeca,  ',
      'SUM(SdoVrtArM2) SdoVrtArM2 ',
      'FROM vsmovits ',
      //'/*WHERE Controle IN (209, 217, 223)*/ ',
      //'WHERE Controle IN (23, 43, 59, 73, 87, 106, 120, 161, 176) ',
      'WHERE Controle IN (' + Corda3 + ') ',
      'GROUP BY Pallet ',
      '']);
      // Outras movimenta��o dos pallets de entrada (compra) que n�o sejam
      // compra, pr� reclasse e reclasse
      UnDmkDAC_PF.AbreMySQLQuery0(QrCmprOtrs, Dmod.MyDB, [
      'SELECT /*MovimID,*/ Pallet, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
      'FROM vsmovits ',
      //'/*WHERE MovimID IN (9,12,13,17,41)*/ '
      'WHERE NOT (MovimID IN (16,15,08)) ',
      //'/*AND Pallet IN (40)*/ '
      //'AND Pallet IN (6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 28, 29, 30, 31, 32, 33, 34, 35, 36) '
      'AND Pallet IN (' + Corda1 + ') ',
      'GROUP BY /*MovimID,*/ Pallet ',
      '']);
      // fim devem ser antes pr causa do lookup do QrCompraWB
      // ==============================================

      // 1� Reclasse de compra de WB: COUROS DE ORIGEM:
      // ==============================================
      UnDmkDAC_PF.AbreMySQLQuery0(QrCompraWB, Dmod.MyDB, [
      'SELECT vmi.Codigo, vmi.DataHora, vmi.Controle,  ',
      'vmi.Pallet, vmi.GraGruX, vmi.Pecas, vmi.AreaM2, ',
      'vmi.AreaP2, vmi.SdoVrtPeca, vmi.SdoVrtArM2, ',
      'vmi.Marca, ggx.GraGru1, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, ',
      'vmi.Pecas * IF(nv1.FatorInt IS NULL, 0, nv1.FatorInt) Inteiros  ',
      'FROM vsmovits vmi ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 ',
      //////////////////////////////////////////////////////////////////////////
      // pesquisa par IME-Is
      //'WHERE vmi.Controle IN (' + Corda1 + ') ',
      //////////////////////////////////////////////////////////////////////////
      // Pesquisa por pallets
      'WHERE vmi.Pallet IN (' + Corda1 + ') ',
      'AND MovimID=16 ',
      //////////////////////////////////////////////////////////////////////////
      '']);
    end else
      frxReport := frxWET_CURTI_018_05_B_1;
    //
    if RG05AscDesc.ItemIndex = 0 then
      AscDesc := ''
    else
      AscDesc := ' DESC ';
    if RG05OperClass_ItemIndex = 2 then
    begin
      sGrupo := 'GROUP BY vmi.GraGruX ';
      //
      case RG05Ordem_ItemIndex of
        0: sOrdem := 'ORDER BY AreaM2 ' + AscDesc + ', NO_PRD_TAM_COR ';     //�rea
        1: sOrdem := 'ORDER BY FatrClase' + AscDesc + ', AreaM2 ' + AscDesc + ', NO_PRD_TAM_COR ';  //Fator (ordem) classe
        2: sOrdem := 'ORDER BY NO_PRD_TAM_COR' + AscDesc; //Nome
        else sOrdem := 'ORDER BY ???';
      end;
    end else
    begin
      sGrupo := 'GROUP BY gby.Revisor, vmi.GraGruX ';
      //
      case RG05Ordem_ItemIndex of
        0: sOrdem := 'ORDER BY gby.Revisor, AreaM2 ' + AscDesc + ', NO_PRD_TAM_COR ';     //�rea
        1: sOrdem := 'ORDER BY gby.Revisor, FatrClase' + AscDesc + ', AreaM2 ' + AscDesc + ', NO_PRD_TAM_COR ';  //Fator (ordem) classe
        2: sOrdem := 'ORDER BY gby.Revisor, NO_PRD_TAM_COR' + AscDesc; //Nome
        else sOrdem := 'ORDER BY ???';
      end;
    end;
    // Deve ser antes!!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qr05RevSum, DModG.MyPID_DB, [
    'SELECT gby.Revisor, ',
    'IF(cla.Tipo=0, cla.RazaoSocial, cla.Nome) NO_Revisor,   ',
    'SUM(gby.Pecas) Pecas,  ',
    'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2 ',
    'FROM _vscacgby_ gby ',
    'LEFT JOIN ' + TMeuDB + '.entidades cla ON cla.Codigo=gby.Revisor ',
    'GROUP BY Revisor ',
    'ORDER BY AreaM2 ',
    '']);
    // deve ser depois!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qr05RevVSCac, DModG.MyPID_DB, [
    'SELECT gby.Revisor, ',
    'gby.VMI_Dest, vmi.GraGruX, ',
    'IF(cla.Tipo=0, cla.RazaoSocial, cla.Nome) NO_Revisor, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ',
    'SUM(gby.Pecas) Pecas, ',
    // ini 2023-11-16
    'SUM(gby.Pecas) * IF(nv1.FatorInt IS NULL, 0, nv1.FatorInt) Inteiros, ',
    // fim 2023-11-16
    'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2 ',
    'FROM ' + FVSCacGBY + ' gby ',
    'LEFT JOIN ' + FVsVmiPsq2 + ' vmi ON vmi.Controle=gby.VMI_Dest ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.entidades  cla ON cla.Codigo=gby.Revisor ',
    // ini 2023-11-16
    'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=cou.CouNiv1 ',
    // fim 2023-11-16
    //'GROUP BY vmi.GraGruX, gby.Revisor ',
    //'GROUP BY gby.Revisor, vmi.GraGruX ',
    sGrupo,
    //'ORDER BY gby.Revisor, NO_PRD_TAM_COR, AreaM2 DESC ',
    sOrdem,
    '']);
    //Geral.MB_Teste(Qr05RevVSCac.SQL.Text);

///////////////////////////////////// Inicio Chart /////////////////////////////
    Chart1 := frxReport.FindObject('Chart1') as TfrxChartView;
    ConfiguraChart(Chart1);
    //
    SeriesCount := Qr05VSCacGBY.RecordCount;
    SeriesItems := 0;
    //SetLength(FArrAxeXNom, SeriesCount);
    SetLength(FArrAxeXCod, SeriesCount);
    SetLength(FArrAxeYSer, SeriesCount);
    //
    XSource := '';
    Qr05VSCacGBY.First;
    while not Qr05VSCacGBY.Eof do
    begin
      //FArrAxeXNom[Qr05VSCacGBY.RecNo - 1] := Qr05VSCacGBYNO_PRD_TAM_COR.Value;
      XSource := XSource + ';' + Qr05VSCacGBYNO_PRD_TAM_COR.Value;
      FArrAxeXCod[Qr05VSCacGBY.RecNo - 1] := Qr05VSCacGBYGraGruX.Value;
      //
      Qr05VSCacGBY.Next;
    end;
    if Length(XSource) > 0 then
      XSource := Copy(XSource, 2);
    //
    SetLength(FArrAxeRevNom, Qr05RevSum.RecordCount);
    //
    for I := 0 to SeriesCount - 1 do
    begin
      SetLength(FArrAxeYSer[I], Qr05RevSum.RecordCount);
      //
      for J := 0 to Qr05RevSum.RecordCount - 1 do
        FArrAxeYSer[I][J] := 0;
    end;
    //
    Qr05RevSum.First;
    while not Qr05RevSum.Eof do
    begin
      FArrAxeRevNom[Qr05RevSum.RecNo - 1] := Qr05RevSumNO_Revisor.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qr05GgxSum, DModG.MyPID_DB, [
      'SELECT vmi.GraGruX, ',
      'SUM(gby.Pecas) Pecas, ',
      'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2 ',
      'FROM _vscacgby_ gby ',
      'LEFT JOIN _vsvmipsq2_ vmi ON vmi.Controle=gby.VMI_Dest ',
      'WHERE Revisor=' + Geral.FF0(Qr05RevSumRevisor.Value),
      'GROUP BY vmi.GraGruX ',
      'ORDER BY AreaM2 DESC ',
      '']);
      Qr05GgxSum.First;
      while not Qr05GgxSum.Eof do
      begin
        Item := -1;
        for I := 0 to SeriesCount - 1 do
        begin
          if FArrAxeXCod[I] = Qr05GgxSumGraGruX.Value then
            Item := I;
        end;
        Valor := 0;
        if Item > -1 then
        begin
          if Qr05RevSumAreaM2.Value > 0 then
            Valor := Qr05GgxSumAreaM2.Value / Qr05RevSumAreaM2.Value * 100;
        end else
          Geral.MB_Erro('Reduzido ' + Geral.FF0(Qr05GgxSumGraGruX.Value) +
          ' n�o encontrado para o Revisor ' +
          Geral.FF0(Qr05RevSumRevisor.Value));
        //
        FArrAxeYSer[Item][Qr05RevSum.RecNo - 1] := Valor;
        //
        Qr05GgxSum.Next;
      end;
      //
      Qr05RevSum.Next;
    end;
    //
    Chart1.Chart.LeftAxis.Title.Font.Name := 'Univers Light Condensed';
    Chart1.Chart.LeftAxis.Title.Font.Size := 8;
    Chart1.Chart.LeftAxis.Title.Caption := '% da classe do que o revisor classificou'; //'Temperature / �C';
    //Chart1.Chart.BottomAxis.LabelsMultiLine := True;
    for I := 0 to Length(FArrAxeRevNom) - 1 do
    begin
      YSource := '';
      for J := 0 to Length(FArrAxeYSer) - 1 do
      begin
        YSource := YSource + ';' + Geral.FFT(FArrAxeYSer[J][I], 2, siNegativo);
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);
      //
      //Chart1.AddSeries(TfrxChartSeries.csArea);
      Chart1.AddSeries(TfrxChartSeries.csBar);

      // do VCLTee.Chart.CustomChart:
      //Chart1.Chart.Series[I].Pen.Width := 4;
      Chart1.Chart.Series[I].Pen.Width := 1;
      Chart1.Chart.Series[I].LegendTitle := FArrAxeRevNom[I];
      //Chart1.Chart.Series[I].Color := FCores[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      //
      Chart1.SeriesData[I].XSource := XSource;(*'value0; value1; value2; value3'*);
      Chart1.SeriesData[I].YSource := YSource;(*'0;31.5;28.54;31.58'*);
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
      //
      //TBarSeries(Chart1.Chart.Series[I]).Marks.Style := smsValue;
      Chart1.Chart.Series[I].Marks.Style := smsValue;
      //TBarSeries(Chart1.Chart.Series[I]).Marks.ShadowSize := 0;
      Chart1.Chart.Series[I].Marks.ShadowSize := 0;
      //TBarSeries(Chart1.Chart.Series[I]).Marks.Font.Size := 5;
      Chart1.Chart.Series[I].Marks.Font.Size := 6;
      //TBarSeries(Chart1.Chart.Series[I]).Marks.Frame.Width := 0;
      Chart1.Chart.Series[I].Marks.Frame.Width := 0;
      Chart1.Chart.Series[I].Marks.Shadow.Visible := False;
      Chart1.Chart.Series[I].Marks.Transparent := True;
      Chart1.Chart.Series[I].Marks.MultiLine := True; // desmarcar??
      //
      //
    end;
    Chart1.Chart.BottomAxis.LabelsMultiLine := True;
    Chart1.Chart.BottomAxis.LabelsFont.Name := 'Univers Light Condensed';
    Chart1.Chart.BottomAxis.LabelsFont.Size := 5;
    Chart1.Chart.BottomAxis.Grid.Visible := False;

///////////////////////////////////// Fim Chart ////////////////////////////////

    if Qr05RevVSCac.RecordCount = 0 then
    begin
      Geral.MB_Info('N�o h� informa��o a ser mostrada!');
      Exit;
    end;
    I2T('VARF_GraGruX', GraGruX, CB05GraGruX_Text);
    I2T('VARF_VSPallet', VSPallet, Geral.FF0(VSPallet));
    I2T('VARF_Terceiro', Terceiro, CB05Terceiro_Text);
    I2T('VARF_SerieFch', SerieFicha, CB05SerieFch_Text);
    I2T('VARF_Ficha',    Ficha, Geral.FF0(Ficha));
    T2T('VARF_Marca',    Marca);
    I2T('VARF_VSCacCod', CacCod, Geral.FF0(CacCod));
    I2T('VARF_VMI_Sorc', VMI_Sorc, Geral.FF0(VMI_Sorc));
    I2T('VARF_VMI_Dest', VMI_Dest, Geral.FF0(VMI_Dest));
    I2T('VARF_Martelo',  Martelo, Geral.FF0(Martelo));
    T2T('VARF_Periodo',  Periodo);
    //
    MyObjects.frxDefineDataSets((*frxWET_CURTI_018_05_B*)frxReport, [
      DModG.frxDsDono,
      frxDs05RevVSCac,
      frxDsCompraWB
    ]);
{
    Chart1 := frxWET_CURTI_018_05_A.FindObject('ChartPie0') as TfrxChartView;
    Chart1.Chart.Legend.Visible := False;
    //Chart1.Chart.Legend.Alignment := laBottom;
    //Chart1.Chart.Legend.Shadow.Visible := False;
    Chart1.Chart.View3D := False;

    // do VCLTee.Chart.CustomChart:
    Chart1.Chart.Series[0].Marks.Font.Size := 6;
    Chart1.Chart.Series[0].Marks.Transparent := True;
    Chart1.Chart.Series[0].Marks.Shadow.Visible := False;
    PieSeries := TPieSeries(Chart1.Chart.Series[0]);
    PieSeries.Circled := True;



      // do frxChat
    //Chart1.SeriesData[0].TopN := 20;
    //Chart1.SeriesData[0].TopNCaption := FOutrosChart;
    //Chart1.SeriesData.Items[0].Circled := True;
}
    //
    MyObjects.frxMostra((*frxWET_CURTI_018_05_B*)frxReport, 'Resultado de Classifica��o por Classificador');

  end;
end;


procedure TFmVSImpMartelo.Qr05RevVSCacCalcFields(DataSet: TDataSet);
begin
  if Qr05RevVSCacSUM_ArM2.Value = 0 then
    Qr05RevVSCacPercClaByRev.Value := 0
  else
    Qr05RevVSCacPercClaByRev.Value :=
    Qr05RevVSCacAreaM2.Value / Qr05RevVSCacSUM_ArM2.Value * 100;
end;

procedure TFmVSImpMartelo.Qr05VSCacGBYCalcFields(DataSet: TDataSet);
begin
  if Qr05SumGBYAreaM2.Value = 0 then
    Qr05VSCacGBYPercentual.Value := 0
  else
    Qr05VSCacGBYPercentual.Value := Qr05VSCacGBYAreaM2.Value /
    Qr05SumGBYAreaM2.Value * 100;
end;

procedure TFmVSImpMartelo.RG05OperClassClick(Sender: TObject);
begin
  if RG05OperClass.ItemIndex = 2 then
    RGRelatorio.ItemIndex := 3;
  CkShwPalOri.Visible := RG05OperClass.ItemIndex = 2;
  CkShwPalOri.Checked := RG05OperClass.ItemIndex = 2;
end;

end.
