unit VSLoadCRCCab2;
{

AtrelaEOuInsere....

  Atrelou, Inseriu: Boolean;
begin
  Atrelou        := ObtemAtrelou(AtrelouInseriu);
  Inseriu        := ObtemInseriu(AtrelouInseriu);
  //
  OriXxxxxx      := Query.FieldByName('Xxxxxx').AsInteger;
  ...
  DstXxxxxx      := 0;
  ...
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstXxxxxx) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Xxxxxx, DstXxxxxx);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('?????1, sProcName, Ori?????1,
    Dst?????1) then  Exit;
    //
    if not ObtemValorDstAtrelado01Index('?????2, sProcName, Ori?????2,
    Dst?????2) then  Exit;
    //
    ...
  end;
  if (not Inseriu) and (not Atrelou) then
    DstXxxxxx := UMyMod.BPGS1I32(Tabela, 'Xxxxxx, '', '', tsPos, stIns, 0);
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Xxxxxx, OriXxxxxx, DstXxxxxx, AWServerID,
    AWStatSinc, AtrelouInseriu);
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Xxxxxx, OriXxxxxx, DstXxxxxx, [
    '?????1, '?????2', ...], [
    Dst?????1, Dst?????2, ...]);
  //
  NeutralizaOrigem(Tabela, ['Xxxxxx], [OriXxxxxx]);
end;
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  Variants, Grids, DBGrids, TypInfo, Vcl.ComCtrls,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, UnDmkProcFunc, UnDmkENums,
  dmkEditDateTimePicker, dmkDBGridZTO, UnProjGroup_Consts, UnProjGroup_PF;

type
  TArrResIdx = array [0..9] of Variant;
  TRecriaRegistro = (recrregNao=0, recrregSim=1);
  TFmVSLoadCRCCab2 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    QrVSLoadCRCCab: TmySQLQuery;
    DsVSLoadCRCCab: TDataSource;
    QrVSLoadCRCTbs: TmySQLQuery;
    DsVSLoadCRCTbs: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSLoadCRCCabCodigo: TIntegerField;
    QrVSLoadCRCCabDataHora: TDateTimeField;
    QrVSLoadCRCCabLk: TIntegerField;
    QrVSLoadCRCCabDataCad: TDateField;
    QrVSLoadCRCCabDataAlt: TDateField;
    QrVSLoadCRCCabUserCad: TIntegerField;
    QrVSLoadCRCCabUserAlt: TIntegerField;
    QrVSLoadCRCCabAlterWeb: TSmallintField;
    QrVSLoadCRCCabAWServerID: TIntegerField;
    QrVSLoadCRCCabAWStatSinc: TSmallintField;
    QrVSLoadCRCCabAtivo: TSmallintField;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrItensPorTab: TmySQLQuery;
    QrItensPorTabCodigo: TIntegerField;
    QrItensPorTabNome: TWideStringField;
    QrItensPorTabItens: TLargeintField;
    QrItensPorTabAtivo: TSmallintField;
    DsItensPorTab: TDataSource;
    QrTabelas: TmySQLQuery;
    QrRegistros: TmySQLQuery;
    PB1: TProgressBar;
    DBEdit1: TDBEdit;
    BtImporta: TBitBtn;
    QrIdx: TmySQLQuery;
    QrVSLoadCRCCabOriServrID: TIntegerField;
    EdOriServrID: TdmkEdit;
    Label5: TLabel;
    QrSel: TmySQLQuery;
    DsSel: TDataSource;
    QrPsqCross: TmySQLQuery;
    ItsImporta1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GBImportacao: TGroupBox;
    Panel6: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    DBGSel: TDBGrid;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DGDados: TDBGrid;
    PB2: TProgressBar;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Pesquisanovosdados1: TMenuItem;
    QrPNI1: TmySQLQuery;
    QrPNI2: TmySQLQuery;
    Mostraitensdatabela1: TMenuItem;
    Query1: TmySQLQuery;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Panel7: TPanel;
    RGPalletsAtrelados: TRadioGroup;
    Query2: TmySQLQuery;
    QrVSLoadCRCWrn: TmySQLQuery;
    QrVSLoadCRCWrnCodigo: TIntegerField;
    QrVSLoadCRCWrnControle: TIntegerField;
    DsVSLoadCRCCabWrn: TDataSource;
    QrVSLoadCRCWrnNome: TWideStringField;
    QrVSLoadCRCWrnLinha: TIntegerField;
    QrVSLoadCRCWrnLk: TIntegerField;
    QrVSLoadCRCWrnDataCad: TDateField;
    QrVSLoadCRCWrnDataAlt: TDateField;
    QrVSLoadCRCWrnUserCad: TIntegerField;
    QrVSLoadCRCWrnUserAlt: TIntegerField;
    QrVSLoadCRCWrnAlterWeb: TSmallintField;
    QrVSLoadCRCWrnAWServerID: TIntegerField;
    QrVSLoadCRCWrnAWStatSinc: TSmallintField;
    QrVSLoadCRCWrnAtivo: TSmallintField;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrPsqTbs: TmySQLQuery;
    QrPsqTbsCodigo: TIntegerField;
    QrPsqTbsControle: TIntegerField;
    QrPsqTbsTabela: TWideStringField;
    QrPsqTbsRegistros: TIntegerField;
    QrVSLoadCRCTbsCodigo: TIntegerField;
    QrVSLoadCRCTbsControle: TIntegerField;
    QrVSLoadCRCTbsTabela: TWideStringField;
    QrVSLoadCRCTbsRegistros: TIntegerField;
    Corrigir011: TMenuItem;
    Mostraitemjimportadodoitemdatabelanoreimportado1: TMenuItem;
    GroupBox1: TGroupBox;
    QrItReInn: TmySQLQuery;
    DsItReInn: TDataSource;
    PnReInn: TPanel;
    DBGReInn: TDBGrid;
    Splitter1: TSplitter;
    QrDBMQeLnk: TmySQLQuery;
    QrDBMQeLnkKLAskrTab: TWideStringField;
    QrDBMQeLnkKLAskrCol: TWideStringField;
    QrDBMQeLnkKLRplyTab: TWideStringField;
    QrDBMQeLnkKLRplyCol: TWideStringField;
    QrDBMQeLnkAlterWeb: TSmallintField;
    QrDBMQeLnkAWServerID: TIntegerField;
    QrDBMQeLnkAWStatSinc: TSmallintField;
    QrDBMQeLnkAtivo: TSmallintField;
    QrDBMQeLnkKLPurpose: TSmallintField;
    QrDBMQeLnkTbPurpose: TSmallintField;
    QrDBMQeLnkTbManage: TSmallintField;
    QrDBMQeLnkKLMTbsCol: TWideStringField;
    Query: TmySQLQuery;
    CkDescreve: TCheckBox;
    PesquisanovosdadoseImporta1: TMenuItem;
    TabSheet4: TTabSheet;
    MeDescreve: TMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure ItsImporta1Click(Sender: TObject);
    procedure Pesquisanovosdados1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSLoadCRCCabAfterOpen(DataSet: TDataSet);
    procedure QrVSLoadCRCCabAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSLoadCRCCabBeforeClose(DataSet: TDataSet);
    procedure QrVSLoadCRCCabBeforeOpen(DataSet: TDataSet);
    procedure Mostraitensdatabela1Click(Sender: TObject);
    procedure QrItensPorTabAfterScroll(DataSet: TDataSet);
    procedure QrItensPorTabBeforeClose(DataSet: TDataSet);
    procedure QrVSLoadCRCWrnAfterOpen(DataSet: TDataSet);
    procedure QrItensPorTabAfterOpen(DataSet: TDataSet);
    procedure Corrigir011Click(Sender: TObject);
    procedure Mostraitemjimportadodoitemdatabelanoreimportado1Click(
      Sender: TObject);
    procedure QrSelAfterScroll(DataSet: TDataSet);
    procedure QrSelBeforeClose(DataSet: TDataSet);
    procedure QrItReInnAfterOpen(DataSet: TDataSet);
    procedure QrItReInnBeforeClose(DataSet: TDataSet);
    procedure PesquisanovosdadoseImporta1Click(Sender: TObject);
  private
    FStrActios: String;
    FStrActLig: Boolean;
    FMemoLinesCount, FTabLstCount: Integer;
    FTabRead, FTabWrite, FSQLCorda, FFldsPriComERP, FFldsPriSemERP,
    FSQLsPriComERP, FSQLsPriSemERP: array of String;
    FCamposName: array of array of String;
    FLstTabs: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure InsUpdVSLoadCRCTbs(const NomeTab: String(*; var Controle: Integer*));
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure AdicionaAoMemo(Texto: String);
    //
    procedure AtrelaEInsereVSCacCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
    procedure AtrelaEInsereVSXXXCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx);
    procedure AtrelaEInsereVSMovCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
    procedure AtrelaEInsereVSPaRclCabA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
    procedure AtrelaEInsereVSMulFrnIts(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
    //
    function  AtrelaEOuInsereVSGerArtA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  AtrelaEOuInsereVSPaClaCabA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    procedure AtrelaEOuInsereVSPaClaItsA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx);
    procedure AtrelaEOuInsereVSPalletA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
    procedure AtrelaEOuInsereVSPaRclItsA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
              ArrResIdx: TArrResIdx);
    //
    function  ApenasAtrelaVMI(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
              ArrResIdx: TArrResIdx): Boolean;
    function  ApenasAtrelaTabelaIdx01Int(Tabela, Campo: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
              ArrResIdx: TArrResIdx): Boolean;
    //
    function  ApenasInsereVMI(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ApenasInsereVSGerRclA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ApenasInsereVSMulFrnCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    //

    function  AtrelaTodaTabela_IndicePri01Int(Tabela, Campo: String): Boolean;
    function  AtrelaTodaTabela_CampoNaoIdx(Tabela, Campo: String): Boolean;
    function  InsereTodaTabelaAtrelada(Tabela: String):
              Boolean;
    //
    procedure DefineTamanhosArrayTabelas(K: Integer);
    //
    function  ExcluiAtreladoEInserido(const Atrelou: Boolean; var Inseriu:
              Boolean; const Tabela, FldIdx01: String; const DstIdxInt01:
              Integer): Boolean;
    //
    function  ExcluiVSMovIts_de_VSMovItz(TabFromRead, TabToDel: String;
              IndiceToDel: Integer; AtrelouInseriuToDel: TERPAtrelIns;
              QueryFromRead: TmySQLQuery; ArrResIdxToDel: TArrResIdx):
              Boolean;
    //
    function  ImportaItemTabela(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VMI(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSCacCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery): Boolean;
    function  ImportaItemTabela_VSCacItsA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSEntiMP(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSEscCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSGerArtA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSGerRclA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSInnCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSMovCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery): Boolean;
    function  ImportaItemTabela_VSMovItz(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSOpeCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSOutCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPalletA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPrePalCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPWECab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSMulFrnCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSMulFrnIts(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPaClaCabA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPaClaItsA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPaRclCabA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSPaRclItsA(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    function  ImportaItemTabela_VSTrfLocCab(Tabela: String; Indice: Integer;
              AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
              TArrResIdx): Boolean;
    //
    function  ImportaTabela(Tabela: String): Boolean;
    //
    procedure InsereCabecalhoGenerico_Arr01(Tabela: String; Indice: Integer;
              Query: TmySQLQuery; ArrResIdx: TArrResIdx; AtrelouInseriu:
              TERPAtrelIns; RecriaRegistro: TRecriaRegistro);
    procedure InsereCabecalhoGeneIMEC_Arr01(Tabela: String; Indice: Integer;
              Query: TmySQLQuery; ArrResIdx: TArrResIdx; AtrelouInseriu:
              TERPAtrelIns);
    procedure InsereRegistro_Array(Tabela: String; Indice: Integer; FldIdx1:
              String; ValIdx1Ori, ValIdx1Dst: Integer; Campos: array of String;
              Valores: array of Integer);
    procedure InsereDadosTabelaEmIndice(Tabela: String; Index:
              Integer = -1);
    //
    procedure MemoAdd_NoAI_Tab(Tabela: String; AI: TERPAtrelIns; ProcName: String; ArrResIdx: TArrResIdx);
    procedure MemoAdd_NoAI_Oth(Tabela: String; AIO: TERPAtrelIns; ProcName: String);
    procedure MemoAdd_JaAtrIns(Tabela: String; ProcName: String);
    procedure MemoAdd_NoReImpl_Idx(Tabela, ProcName: String);
    procedure MemoAdd_NoARI_Int01(Tabela, ProcName: String; Int01: Integer);
    //
    procedure MostraVSLoadCRCIts(SQLType: TSQLType);
    function  PesquisaNovosDadosAImportar(ServerID: Integer): Boolean;
    //
    function  ObtemAtrelou(AtrelIns: TERPAtrelIns): Boolean;
    function  ObtemInseriu(AtrelIns: TERPAtrelIns): Boolean;
    function  ObtemAtrelouInseriu_Indice(const Tabela: String; const QrPsq:
              TmySQLQuery; const Indice: Integer; const OthrIdx:
              array of Integer; var AtrelIns: TERPAtrelIns): TArrResIdx;
    function  ObtemIndiceDeTabela(Tabela: String; AddOnNoFind: Boolean = True): Integer;
    function  ObtemValorDstAtrelado01Index(const Tabela, OriProcName: String;
              const OriCodigo: Integer; (*const AvisaNoARI: Boolean;*)
              var DstCodigo: Integer): Boolean;
    function  ObtemValorDstAtrelado01FldInt(const TabSync, TabPsq, FldName,
              OriProcName: String; const Ori_Int: Integer; var Dst_Int:
              Integer): Boolean;
    function  ObtemValoresDstIDNiv1Niv2(const Prefix, ProcName: String; const
              OriXxxMovID, OriXxxNivel1, OriXxxNivel2: Integer; var DstXxxMovID,
              DstXxxNivel1, DstXxxNivel2: Integer): Boolean;
    function  ObtemValorArrResIdxInt01(const ProcName, Tabela: String; const
              ARI: TArrResIdx; var ValDstInt: Integer): Boolean;
    function  ObtemValorArrResIdxIntAtrIns01(const Atrelou, Inseriu: Boolean;
              const Tabela: String; const Indice: Integer; const Query:
              TmySQLQuery; const ArrResIdx: TArrResIdx; var DstInt01: Integer):
              Boolean;
    procedure PreDefineTabelasCamposETextosSQL();
    function  QtdIndicesARI(ARI: TArrResIdx): Integer;
    procedure ReopenSel(Indice: Integer; SQLExtra: String);
    function  VerificaIdenticidadeDeTabelas(const DBOri: TmySQLDataBase; const
              TabOri: String; const DBDst: TmySQLDatabase; const TabDst: String;
              var SQLDst: String; var SQLOri: String): Boolean;
    procedure SubstituiAWStatSincEmSQL(var SQL: String);

///////////////////////////////// N O V O //////////////////////////////////////
    //function  PesquisaNovosDados(): Boolean;
    function  AtrelaTabela(Tabela: String): Boolean;
{
    function  ExcluiInserido(var Inseriu: Boolean; const Tabela: String; const
              IdxFlds: array of String; const IdxVals: array of Variant):
              Boolean;
}
    function  ExcluiAtreladoEInserid2(const Atrelou: Boolean; var Inseriu:
              Boolean; const Tabela: String; const TabIndex: Integer; const
              ArrResIdx: TArrResIdx): Boolean;
    procedure ExecutaImportacao();
    function  InsereTabela(Tabela: String): Boolean;
    function  InsereRegistro_Arr2(Tabela: String; TabIndex: Integer; QrPsq:
              TmySQLQuery; CamposDst: array of String;
              ValoresDst: array of Variant): Boolean;
    function  InsereRegistro_Arr3(Tabela: String; TabIndex: Integer; SQL_WHR:
              String;  CamposDst: array of String; ValoresDst:
              array of Variant): Boolean;
    function  NeutralizaOrige2(Tabela: String; SQL_WHR: String): Boolean;
    function  ObtemInseriu_TabIndex(const Tabela: String; const QrPsq:
              TmySQLQuery; const TabIndex: Integer): Boolean;
    function  ObtemInseriu_ArrIdx(const Tabela: String; const IdxFlds:
              array of String; const IdxVals: array of Variant): Boolean;
    function  ObtemValorSubstituto(const QrPsq: TmySQLQuery; const Indice:
              Integer; var ValorRes: Variant): Boolean;
  public
    { Public declarations }
    FSeq, FCabIni(*, FCodOnImport*): Integer;
    FLocIni: Boolean;
    //
    function  AtrelaTabelas_Codigo(Tabela: String; Ori_Codigo, Dst_Codigo,
              AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
    function  AtrelaTabelas_Int(Tabela, Campo: String; Ori_int, Dst_Int,
              AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSLoadCRCTbs(Controle: Integer);
    procedure ReopenVSLoadCRCWrn(Controle: Integer);
    function  NeutralizaOrigem(Tabela: String; Campos: array of String;
              Valores: array of Variant): Boolean;
    function  OlvidaOrigem(Tabela: String; Campos: array of String;
              Valores: array of Variant): Boolean;
    procedure OlvidaTodaTabelaVSCacItsA();
  end;

var
  FmVSLoadCRCCab2: TFmVSLoadCRCCab2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSLoadCRCIts, UnVS_CRC_PF,
  CreateGeral, ModuleGeral, UnERPSinc_Tabs, UnVS_Jan, GetValor;

{$R *.DFM}

const
  TabelasIdxDifere: array[0..1] of String = ('vspalleta', 'VSEntiMP');

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSLoadCRCCab2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSLoadCRCCab2.MemoAdd_JaAtrIns(Tabela, ProcName: String);
begin
  AdicionaAoMemo('Tabela ' + Tabela + '. Item j� inserido e j� atrelado em ' +
  ProcName);
end;

procedure TFmVSLoadCRCCab2.MemoAdd_NoAI_Oth(Tabela: String; AIO: TERPAtrelIns;
  ProcName: String);
begin
  AdicionaAoMemo('Tabela ' + Tabela + '. A��o indefinida para "AtrlInsOthr" ' +
  GetEnumName(TypeInfo(TERPAtrelIns), Integer(AIO)) + ' em ' + ProcName);
end;

procedure TFmVSLoadCRCCab2.MemoAdd_NoAI_Tab(Tabela: String; AI: TERPAtrelIns;
  ProcName: String; ArrResIdx: TArrResIdx);
var
  Indices: String;
  I: Integer;
begin
  Indices   := '';
  for I := 1 to ArrResIdx[0] do
    Indices := Indices + ' - Indice ' + Geral.FF0(I) + ': ' + Geral.FF0(ArrResIdx[I]);
  AdicionaAoMemo('Tabela: ' + Tabela + ' n�o implementada em importa tabela ' +
  GetEnumName(TypeInfo(TERPAtrelIns), Integer(AI)) + ' em ' + ProcName + Indices);
end;

procedure TFmVSLoadCRCCab2.MemoAdd_NoARI_Int01(Tabela, ProcName: String; Int01:
  Integer);
begin
  AdicionaAoMemo('Tabela ' + Tabela +
  '. �ndice ' + Geral.FF0(Int01) + ' n�o encontrado em ' + ProcName);
end;

procedure TFmVSLoadCRCCab2.MemoAdd_NoReImpl_Idx(Tabela, ProcName: String);
begin
  AdicionaAoMemo('Tabela ' + Tabela +
  '. �ndice modificado e n�o re-implementado em ' + ProcName);
end;

procedure TFmVSLoadCRCCab2.Mostraitemjimportadodoitemdatabelanoreimportado1Click(
  Sender: TObject);
var
  Qry: TmySQLQuery;
  sSQL, W_A, Fld: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MySyncDB, [
    'SHOW INDEX FROM ' + QrItensPorTabNome.Value,
    'WHERE Key_name="PRIMARY" ',
    '']);
    //
    sSQL :=  '';
    W_A := 'WHERE ';
    while not Qry.Eof do
    begin
      Fld := Qry.FieldByName('Column_name').AsString;
      if Fld <> 'AWServerID' then
      begin
        sSQL := sSQL + W_A + Fld + '="' +
        QrSel.FieldByName(Fld).AsString + '"';
        W_A := 'AND ';
      end;
      //
      Qry.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrItReInn, Dmod.MyDB, [
    'SELECT * FROM ' + QrItensPorTabNome.Value,
    sSQL,
    '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSLoadCRCCab2.Mostraitensdatabela1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MySyncDB, [
  'SELECT * ',
  'FROM ' + Lowercase(QrItensPorTabNome.Value),
  'WHERE AWServerID <> 0 ',
  //'AND AWStatSinc NOT IN (8,10) ',
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  '']);
  //

end;

procedure TFmVSLoadCRCCab2.MostraVSLoadCRCIts(SQLType: TSQLType);
begin
  //  Nada!!
end;

function TFmVSLoadCRCCab2.NeutralizaOrige2(Tabela, SQL_WHR: String): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
  SQL_Update, SQL_UPD: String;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnSinc); // 8!
  //
  SQL_UPD        := 'SET AWStatSinc=8 ';
  SQL_Update := Geral.ATS(['UPDATE ' + LowerCase(Tabela),
    SQL_UPD,
    SQL_WHR, //'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  //
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Update,
  '']);
{
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
}
end;

function TFmVSLoadCRCCab2.NeutralizaOrigem(Tabela: String;
  Campos: array of String; Valores: array of Variant): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnSinc); // 8!
  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
end;

function TFmVSLoadCRCCab2.ObtemAtrelou(AtrelIns: TERPAtrelIns): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemAtrelou()';
begin
  case AtrelIns of
    //TERPAtrelIns.atrinsIndef
    TERPAtrelIns.atrinsNoAtrelNoIns:  Result := False;
    TERPAtrelIns.atrinsNoAtrelButIns: Result := False;
    TERPAtrelIns.atrinsNoInsButAtrel: Result := True;
    TERPAtrelIns.atrinsAtrelAndIns:   Result := True;
    else
    begin
      Result := False;
      AdicionaAoMemo('AtrelIns indefinido em "' + sProcName + '"');
    end;
  end;
end;

function TFmVSLoadCRCCab2.ObtemInseriu_ArrIdx(const Tabela: String; const
  IdxFlds: array of String; const IdxVals: array of Variant): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.ObtemInseriu_ArrIdx()';
var
  I: Integer;
  SQLSel: String;
begin
  Result := False;
  if (Length(IdxFlds) <> Length(IdxVals))
  or (Length(IdxFlds) = 0) then
  begin
    Geral.MB_Aviso('A quantidade de itens da campo n�o confere com a quantidade de valores em ' +
    sProcName);
    Exit;
  end;
  SQLSel := 'SELECT ' + Geral.VariavelToString(IdxFlds[0]) + sLineBreak +
  'FROM  ' + Lowercase(Tabela) + sLineBreak +
  'WHERE ' + IdxFlds[0] + '=' + Geral.VariavelToString(IdxVals[0]) +
  sLineBreak;
  for I := 1 to High(IdxFlds) do
  begin
    SQLSel := SQLSel + 'AND ' + IdxFlds[I] + '=' +
    Geral.VariavelToString(IdxVals[I]) + sLineBreak;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  SQLSel,
  '']);
  Geral.MB_SQL(Self, Query);
  //
  Result := Query.RecordCount > 0;
end;

function TFmVSLoadCRCCab2.ObtemInseriu_TabIndex(const Tabela: String;
  const QrPsq: TmySQLQuery; const TabIndex: Integer): Boolean;
var
  SQLPsqCross, SQLPsqInsertedIts, IdxTxt: String;
  I: Integer;
  IdxDifere: Boolean;
begin
  Result := False;
  IdxTxt := QrPsq.FieldByName(FCamposName[TabIndex][0]).AsString;
  //
  SQLPsqInsertedIts := 'WHERE ' + FCamposName[TabIndex][0] + '="' + IdxTxt + '"';
  //
  for I := 1 to High(FCamposName[TabIndex]) (*CamposCount - 1*) do
  begin
    IdxTxt := QrPsq.FieldByName(FCamposName[TabIndex][I]).AsString;
    //
    SQLPsqInsertedIts := SQLPsqInsertedIts + sLineBreak +
    'AND ' + FCamposName[TabIndex][I] + '="' + IdxTxt + '"';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM ' + Lowercase(Tabela),
  SQLPsqInsertedIts,
  '']);
  //
  Geral.MB_SQL(Self, QrPsqCross);
  Result := QrPsqCross.FieldByName('ITENS').AsInteger > 0;
  //
end;

function TFmVSLoadCRCCab2.ObtemAtrelouInseriu_Indice(const Tabela: String;
  const QrPsq: TmySQLQuery; const Indice: Integer; const OthrIdx:
  array of Integer; var AtrelIns: TERPAtrelIns): TArrResIdx;
var
  Atrelou, Inseriu: Boolean;
  SQLPsqCross, SQLPsqInsertedIts, IdxTxt: String;
  I: Integer;
  IdxDifere: Boolean;
begin
  IdxDifere := True;
  for I := Low(TabelasIdxDifere) to High(TabelasIdxDifere) do
    if Lowercase(Tabela) = Lowercase(TabelasIdxDifere[I]) then
      IdxDifere := False;
  //
  Result[0]     := 0; // Quantidade de �ndices
  Result[1]     := 0; // �ndice 1
  Result[2]     := 0; // �ndice 2
  Result[3]     := 0; // etc...
  Result[4]     := 0;
  Result[5]     := 0;
  Result[6]     := 0;
  Result[7]     := 0;
  Result[8]     := 0;
  Result[9]     := 0;
  AtrelIns      := TERPAtrelIns.atrinsIndef;
  Atrelou       := False;
  Inseriu       := False;
  //
  if QrPsq <> nil then
    IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString
  else
    IdxTxt := Geral.FF0(OthrIdx[0]);
  //
  SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + FCamposName[Indice][0] + '="' + IdxTxt + '"';
  //
  //SQLPsqInsertedIts := 'WHERE ' + FCamposName[Indice][0] + '="' + IdxTxt + '"';
  //
  for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
  begin
    IdxTxt := Geral.FF0(OthrIdx[I]);
    //
    SQLPsqCross := SQLPsqCross + sLineBreak +
    'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    //
    (*SQLPsqInsertedIts := SQLPsqInsertedIts + sLineBreak +
    'AND ' + FCamposName[Indice][I] + '="' + IdxTxt + '"';*)
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
  FSQLsPriComERP[Indice], //SQLIndexQry,
  SQLPsqCross,
  '']);
  Atrelou := QrPsqCross.RecordCount > 0;
  if Atrelou then
  begin
    Result[0]     := Length(FCamposName[Indice]); // Quantidade de �ndices
    for I := 0 to Result[0] - 1 do
    begin
      Result[I + 1] := QrPsqCross.Fields[I].Value;
    end;
  end;
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
  FSQLsPriSemERP[Indice], //SQLPsqInsertedCab,
  SQLPsqInsertedIts,
  '']);
  //
*)
  if (not IdxDifere) or Atrelou then
  begin
    if IdxDifere then
      IdxTxt := Geral.FF0(Integer(Result[1]))
    else
    begin
      if QrPsq <> nil then
        IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString
      else
        IdxTxt := Geral.FF0(OthrIdx[0]);
    end;
    //
    SQLPsqInsertedIts := 'WHERE ' + FCamposName[Indice][0] + '="' + IdxTxt + '"';
    //
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      if IdxDifere then
        IdxTxt := Geral.FF0(Integer(Result[I+1]))
      else
      begin
        if QrPsq <> nil then
          IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString
        else
          IdxTxt := Geral.FF0(OthrIdx[I]);
      end;
      //
      SQLPsqInsertedIts := SQLPsqInsertedIts + sLineBreak +
      'AND ' + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    FSQLsPriSemERP[Indice], //SQLPsqInsertedCab,
    SQLPsqInsertedIts,
    '']);
    //
    Inseriu := QrPsqCross.RecordCount > 0;
    //
  end;
  //
  if Atrelou then
  begin
    if Inseriu then
      AtrelIns := TERPAtrelIns.atrinsAtrelAndIns (*4*)
    else
      AtrelIns := TERPAtrelIns.atrinsNoInsButAtrel(*3*)
  end else
  if Inseriu then
    AtrelIns := TERPAtrelIns.atrinsNoAtrelButIns(*2*)
  else
     AtrelIns := TERPAtrelIns.atrinsNoAtrelNoIns;(*1*)
  //
end;

function TFmVSLoadCRCCab2.ObtemIndiceDeTabela(Tabela: String; AddOnNoFind: Boolean): Integer;
  function Obtem(var N: Integer): Boolean;
  var
    I: Integer;
  begin
    N := -1;
    for I := Low(FTabRead) to High(FTabRead) do
      if Lowercase(FTabRead[I]) = LowerCase(Tabela) then
      begin
        N := I;
        Exit;
      end;
  end;
  //
begin
  Obtem(Result);
  //
  if (Result = -1) and  AddOnNoFind then
  begin
    InsereDadosTabelaEmIndice(Tabela);
    Obtem(Result);
  end;
end;

function TFmVSLoadCRCCab2.ObtemInseriu(AtrelIns: TERPAtrelIns): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemInseriu()';
begin
  case AtrelIns of
    //TERPAtrelIns.atrinsIndef
    TERPAtrelIns.atrinsNoAtrelNoIns:  Result := False;
    TERPAtrelIns.atrinsNoAtrelButIns: Result := True;
    TERPAtrelIns.atrinsNoInsButAtrel: Result := False;
    TERPAtrelIns.atrinsAtrelAndIns:   Result := True;
    else
    begin
      Result := False;
      AdicionaAoMemo('AtrelIns indefinido em "' + sProcName + '"');
    end;
  end;
end;

function TFmVSLoadCRCCab2.ObtemValorArrResIdxInt01(const ProcName, Tabela:
  String; const ARI: TArrResIdx; var ValDstInt: Integer): Boolean;
const
  sprocName = 'FmVSLoadCRCCab.ObtemValorArrResIdxInt01()';
var
  Indices: String;
  I, qIdx: Integer;
begin
  if QtdIndicesARI(ARI) = 1 then
  begin
    ValDstInt := ARI[1];
    Result := ValDstInt <> 0;
  end else
  begin
    Result    := False;
    ValDstInt := 0;
    Indices   := '';
    for I := 1 to ARI[0] do
      Indices := Indices + ' - Indice ' + Geral.FF0(I) + ': ' + Geral.FF0(ARI[I]);
    AdicionaAoMemo('Valor de �ndice zerado! ' + sProcName + ' Tabela : ' +
    Tabela + ' > ' + ProcName + Indices);
  end;
end;

function TFmVSLoadCRCCab2.ObtemValorArrResIdxIntAtrIns01(const Atrelou,
  Inseriu: Boolean; const Tabela: String; const Indice: Integer; const Query:
  TmySQLQuery; const ArrResIdx: TArrResIdx; var DstInt01: Integer): Boolean;
const
  sprocName = 'FmVSLoadCRCCab.ObtemValorArrResIdxIntAtrIns01()';
begin
  Result := False;
  DstInt01 := 0;
  //
  if Atrelou then
    Result := ObtemValorArrResIdxInt01(sProcName, Tabela, ArrResIdx, DstInt01)
  else
  begin
    if Inseriu then
    begin
      MemoAdd_JaAtrIns(Tabela, sProcName);
      Exit;
    end else
    begin
      DstInt01 := 0;
      Result   := True;
    end;
  end;
end;

function TFmVSLoadCRCCab2.ObtemValorSubstituto(const QrPsq: TmySQLQuery;
  const Indice: Integer; var ValorRes: Variant): Boolean;
  //ObtemValorComutadoSimples(KLAskrCol: String): Variant;
  function ObtemValorComutadoSimples(KLAskrTab, KLAskrCol: String): Variant;
  var
    SQLPsqCross, IdxTxt, Corda: String;
    I: Integer;
  begin
    Result := Null;
    Corda  := '';
    IdxTxt := QrPsq.FieldByName(FCamposName[Indice][0]).AsString;
    //
    if FStrActLig then
      Corda := FCamposName[Indice][0];
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + FCamposName[Indice][0] + '="' + IdxTxt + '"';
    //
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      if FStrActLig then
        Corda := Corda + '+' + FCamposName[Indice][I];
      //
      IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString;
      //
      SQLPsqCross := SQLPsqCross + sLineBreak +
      'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    FSQLsPriComERP[Indice], //SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName('Dst_' + KLAskrCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLAskrTab + '.' + Corda + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;
  //

  //ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol: String): Variant;
  function ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol:
  String): Variant;
  const
   sProcName = 'ObtemValorComutadoMulTabs()';
  var
    SQLIndexQry, SQLPsqCross, IdxTxt, Tabela: String;
    I: Integer;
  begin
    Result := Null;
////////////////////////////////////////////////////////////////////////////////
    if Lowercase(KLRplyTab) = Lowercase(CO_VSXxxTab) then
    begin
      Tabela := ProjGroup_PF.ObtemNomeTabelaMulTabs(TypeInfo(TEstqMovimID),
        QrPsq.FieldByName(KLMTbsCol).AsInteger, True);
    end else
    if Lowercase(KLRplyTab) = Lowercase('TEstqDefMulFldVS') then
    begin
      Tabela := ProjGroup_PF.ObtemNomeTabelaMulTabs(TypeInfo(TEstqDefMulFldVS),
        QrPsq.FieldByName(KLMTbsCol).AsInteger, True);
    end else
    begin
      Tabela := KLRplyTab;
      Geral.MB_Erro('Tabela "' + KLRplyTab + '" n�o implementada em ' + sProcName);
    end;
////////////////////////////////////////////////////////////////////////////////
    IdxTxt := QrPsq.FieldByName(KLAskrCol).AsString;
    //
    SQLIndexQry := 'SELECT ' + CO_ERPSync_FldDst + KLRplyCol + sLineBreak +
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + Tabela);
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + KLRplyCol + '="' + IdxTxt + '"';
{
    for I := 1 to High(FCamposName[Indice]) (*CamposCount - 1*) do
    begin
      IdxTxt := QrPsq.FieldByName(FCamposName[Indice][I]).AsString;
      //
      SQLPsqCross := SQLPsqCross + sLineBreak +
      'AND ' + CO_ERPSync_FldOri + FCamposName[Indice][I] + '="' + IdxTxt + '"';
    end;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName(CO_ERPSync_FldDst + KLRplyCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + Tabela + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;

  //ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLRplyCol: String): Variant;
  function ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLRplyCol: String): Variant;
  var
    SQLIndexQry, SQLPsqCross, IdxTxt: String;
    I: Integer;
  begin
    Result := Null;
    IdxTxt := QrPsq.FieldByName(KLAskrCol).AsString;
    //
    SQLIndexQry := 'SELECT ' + CO_ERPSync_FldDst + KLRplyCol + sLineBreak +
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + KLRplyTab);
    SQLPsqCross := 'WHERE ' + CO_ERPSync_FldOri + KLRplyCol + '="' + IdxTxt + '"';
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCross, Dmod.MyDB, [
    SQLIndexQry,
    SQLPsqCross,
    '']);
    //Geral.MB_SQL(Self, QrPsqCross);
    if QrPsqCross.RecordCount > 0 then
    begin
      Result := QrPsqCross.FieldByName(CO_ERPSync_FldDst + KLRplyCol).AsVariant;
      //
      if FStrActLig then
        FStrActios :=  FStrActios + KLRplyTab + '.' + KLRplyCol + '= "' +
        Geral.VariavelToString(Result) + '"';
    end;
  end;

const
  sProcName = 'FmVSLoadCRCCab2.ObtemValorSubstituto()';
var
  KLAskrTab, KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol: String;
  KLPurpose, TbPurpose, TbManage: Integer;
begin
  Result := False;
  KLAskrTab := QrDBMQeLnkKLAskrTab.Value;
  KLAskrCol := QrDBMQeLnkKLAskrCol.Value;
  if QrPsq.FieldByName(KLAskrCol).AsVariant = 0 then
  begin
    Result := True;
    Exit;
  end;
  KLRplyTab := QrDBMQeLnkKLRplyTab.Value;
  KLRplyCol := QrDBMQeLnkKLRplyCol.Value;
  KLMTbsCol := QrDBMQeLnkKLMTbsCol.Value;
  KLPurpose := QrDBMQeLnkKLPurpose.Value;
  TbPurpose := QrDBMQeLnkTbPurpose.Value;
  TbManage  := QrDBMQeLnkTbManage.Value;
  case TItemTuplePurpose(TbPurpose) of
    // Indices prim�rios com necessidade de comutar no ERP.
    (*01*)TItemTuplePurpose.itpCDRIncremSync,
    (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
    begin
      //QtdFldIdx := QtdFldIdx + 1;
      ValorRes := ObtemValorComutadoSimples(KLAskrTab, KLAskrCol);
      Result := True;
    end;
    // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
    (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
    (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
    begin
      //QtdFldIdx := QtdFldIdx + 1;
      Result := True;
    end;
    // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
    (*02*)TItemTuplePurpose.itpERPRelatnSync,
    (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
    begin
      Result := True;
    end;
    // Campos com relacionamento e com necessidade de comutar
    (*04*)TItemTuplePurpose.itpCDRRelatnSync,
    (*10*)TItemTuplePurpose.itpCDRRelSncOrfao:
    begin
      //QtdFldSyn := QtdFldSyn + 1;
      ValorRes := ObtemValorComutadoOneTab(KLAskrCol, KLRplyTab, KLRplyCol);
      Result := True;
    end;
    (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
    begin
      ValorRes := ObtemValorComutadoMulTabs(KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol);
      Result := True;
    end;
    // Outros campos sem necessidade de comutar
    (*05*)TItemTuplePurpose.itpUsrPrimtivData,
    (*06*)TItemTuplePurpose.itpSysOrCalcData,
    (*07*)TItemTuplePurpose.itpERPSync,
    (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
    (*12*)TItemTuplePurpose.itpDeprecado,
    (*13*)TItemTuplePurpose.itpInutilizado,
    (*15*)TItemTuplePurpose.itpCDRDeleteSync:
    begin
      // Nada!
      Result := True;
    end;
    // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
    // Somente copiar ao ERP se n�o existir.
    (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
    begin
      ValorRes := ObtemValorComutadoSimples(KLAskrTab, KLAskrCol);
      Result := True;
    end;
    // (*00*)TItemTuplePurpose.itpIndef,
    else
    begin
      // Nada! J� � falso por default
    end;
  end;
  if Result = False then
    AdicionaAoMemo('KLAskrTab ' + KLAskrTab + '. ' + KLAskrCol +
    GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkTbPurpose.Value) +
    ' n�o implementado em ' + sProcName);
end;

function TFmVSLoadCRCCab2.ObtemValorDstAtrelado01FldInt(const TabSync, TabPsq,
  FldName, OriProcName: String; const Ori_Int: Integer; var Dst_Int:
  Integer): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemValorDstAtrelado01FldInt)';
begin
  Result  := False;
  //
  if Ori_Int = 0 then
  begin
    Dst_Int := 0;
    Result  := True;
    Exit;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPNI2, Dmod.MyDB, [
    'SELECT Dst_' + FldName,
    'FROM ' + LowerCase(TabSync),
    'WHERE Ori_' + FldName + '=' + Geral.FF0(Ori_Int),
    '']);
    Dst_Int := QrPNI2.FieldByName('Dst_' + FldName).AsInteger;
    Result  := Dst_Int <> 0;
  end;
end;

function TFmVSLoadCRCCab2.ObtemValorDstAtrelado01Index(const Tabela, OriProcName:
  String; const OriCodigo: Integer; (*const AvisaNoARI: Boolean;*) var DstCodigo:
  Integer): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemValorDstAtrelado01Index()';
var
  IdxVsX, qIdx: Integer;
  ARI: TArrResIdx;
  AtrlInsOthr: TERPAtrelIns;
begin
  Result := False;
  if OriCodigo = 0 then
  begin
    DstCodigo := 0;
    Result := True;
    Exit;
  end else
  begin
    IdxVsX := ObtemIndiceDeTabela(LowerCase(Tabela));
    ARI    := ObtemAtrelouInseriu_Indice(Tabela, nil, IdxVsX, [OriCodigo], AtrlInsOthr);
    qIdx   := QtdIndicesARI(ARI);
    if (qIdx = 0) then
    begin
      MemoAdd_NoARI_Int01(Tabela, sProcName, OriCodigo);
    end else
    begin
      if not ObtemValorArrResIdxInt01(sProcName, Tabela, ARI, DstCodigo) then
        Exit;
    end;
    Result := DstCodigo <> 0;
  end;
end;

function TFmVSLoadCRCCab2.ObtemValoresDstIDNiv1Niv2(const Prefix, ProcName:
  String; const OriXxxMovID, OriXxxNivel1, OriXxxNivel2: Integer; var
  DstXxxMovID, DstXxxNivel1, DstXxxNivel2: Integer): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ObtemValoresDstIDNiv1Niv2()';
var
  XxxTabela: String;
begin
  Result := False;
  try
    DstXxxMovID := OriXxxMovID;
    if (OriXxxMovID <> 0) and (OriXxxNivel1 <> 0) then
    begin
      XxxTabela := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(TEstqMovimID(OriXxxMovID));
      //
      ObtemValorDstAtrelado01Index(XxxTabela, sProcName + sLineBreak +
      ProcName, OriXxxNivel1, DstXxxNivel1);
    end;
    if (OriXxxNivel2 <> 0) then
    begin
      ObtemValorDstAtrelado01Index('vsmovits', sProcName + sLineBreak +
      ProcName, OriXxxNivel2, DstXxxNivel2);
    end;
    Result := True;
  finally
    //
  end;
end;

function TFmVSLoadCRCCab2.OlvidaOrigem(Tabela: String; Campos: array of String;
  Valores: array of Variant): Boolean;
var
  Codigo, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stUpd;
  AWStatSinc     := Integer(stDwnNoSinc); // 10!
  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdSync, SQLType, LowerCase(Tabela), False, [
  'AWStatSinc'], Campos, [
  AWStatSinc],
  Valores, False);
end;

procedure TFmVSLoadCRCCab2.OlvidaTodaTabelaVSCacItsA;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  'UPDATE vscacitsa ',
  'SET AWStatSinc=' + Geral.FF0(Integer(stDwnNoSinc)),
  'WHERE AWServerID=' + Geral.FF0(QrVSLoadCRCCabOriServrID.Value),
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  '']);
end;

procedure TFmVSLoadCRCCab2.Pesquisanovosdados1Click(Sender: TObject);
begin
  PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value);
end;

function TFmVSLoadCRCCab2.PesquisaNovosDadosAImportar(ServerID: Integer): Boolean;
begin
  FMemoLinesCount := 0;
  Memo1.Lines.Clear;
  Result := VS_CRC_PF.PesquisaNovosDadosCDR(stDwnSinc, FLstTabs,
  DModG.MySyncDB, QrItensPorTab, (*QrTabelas,*) QrRegistros, PB1,
  LaAviso1, LaAviso2, (*CkEnviarTudo.Checked*)False, ServerID);
  //
  BtConfirma.Enabled := Result;
  if not Result then
    Geral.MB_Info('N�o h� dados para serem importados ao ERP!');
end;

procedure TFmVSLoadCRCCab2.PesquisanovosdadoseImporta1Click(Sender: TObject);
begin
  if PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value) then
    if QrItensPorTab.State <> dsInactive then
      ExecutaImportacao();
end;

procedure TFmVSLoadCRCCab2.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSLoadCRCCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSLoadCRCCab, QrVSLoadCRCTbs);
end;

procedure TFmVSLoadCRCCab2.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSLoadCRCCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSLoadCRCTbs);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSLoadCRCTbs);
  //
  MyObjects.HabilitaMenuItemItsDel(ItsImporta1, QrItensPorTab);
  MyObjects.HabilitaMenuItemItsIns(Pesquisanovosdados1, QrVSLoadCRCCab);
  //
  Mostraitemjimportadodoitemdatabelanoreimportado1.Enabled :=
    (QrSel.State <> dsInactive) and (QrSel.RecordCount > 0);
end;

procedure TFmVSLoadCRCCab2.PreDefineTabelasCamposETextosSQL();
var
  I, CamposCount: Integer;
  TabOri, TabDst, (*SQLDst, *)SQLOri: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa');
  FTabLstCount := QrItensPorTab.RecordCount;
  DefineTamanhosArrayTabelas(FTabLstCount);
  //
  while not QrItensPorTab.Eof do
  begin
    I      := QrItensPorTab.RecNo - 1;
    InsereDadosTabelaEmIndice(QrItensPorTabNome.Value, I);
    //
    QrItensPorTab.Next;
  end;
  //
  MyObjects.UpdPBOnly(PB1);
end;

procedure TFmVSLoadCRCCab2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmVSLoadCRCCab2.VerificaIdenticidadeDeTabelas(const DBOri:
  TmySQLDataBase; const TabOri: String; const DBDst: TmySQLDatabase; const
  TabDst: String; var SQLDst: String; var SQLOri: String): Boolean;
var
  CamposOri, CamposDst: String;
  OkOri, OkDst: Boolean;
begin
  OkOri := False;
  OkDst := False;
  CamposOri := '';
  CamposDst := '';
  //
  CamposOri := UMyMod.ObtemCamposDeTabelaIdentica(DBOri, TabOri, '');
  SQLOri := Geral.ATS(['SELECT ', CamposOri, 'FROM ' + TabDst]);
  UnDmkDAC_PF.AbreMySQLQuery0(Query1, DBDst, [SQLOri, 'LIMIT 1']);
  OkOri := Query1.State <> dsInactive;
  //
  CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(DBDst, TabDst, '');
  SQLDst := Geral.ATS(['SELECT ', CamposDst, 'FROM ' + TabOri]);
  UnDmkDAC_PF.AbreMySQLQuery0(Query1, DBOri, [SQLDst, 'LIMIT 1']);
  OkDst := Query1.State <> dsInactive;
  //
  Result := OkOri and OkDst;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSLoadCRCCab2.DefParams;
begin
  VAR_GOTOTABELA := 'vsloadcrccab';
  VAR_GOTOMYSQLTABLE := QrVSLoadCRCCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsloadcrccab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

function TFmVSLoadCRCCab2.ExcluiAtreladoEInserido(const Atrelou: Boolean; var
  Inseriu: Boolean; const Tabela, FldIdx01: String; const DstIdxInt01: Integer): Boolean;
begin
  Result := False;
  if Atrelou and Inseriu then
  begin
    if DstIdxInt01 <> 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM ' + TMeuDB + '.' + LowerCase(Tabela),
      'WHERE ' + FldIdx01 + '=' + Geral.FF0(DstIdxInt01),
      '']);
      //
      Inseriu := False; // Inserir de novo!
      //
      Result := True;
    end;
  end else
    Result := True;
end;

function TFmVSLoadCRCCab2.ExcluiAtreladoEInserid2(const Atrelou: Boolean;
  var Inseriu: Boolean; const Tabela: String; const TabIndex: Integer;
  const ArrResIdx: TArrResIdx): Boolean;
var
  I: Integer;
  SQLDel: String;
begin
  Result := False;
  if Atrelou and Inseriu then
  begin
    SQLDel := 'DELETE FROM ' + Tabela + sLineBreak +
    'WHERE ' + FCamposName[TabIndex][0] + '=' +
    Geral.VariavelToString(ArrResIdx[1]);
    for I := 2 to ArrResIdx[0] do
    begin
      SQLDel := SQLDel + sLineBreak + 'AND ' + FCamposName[TabIndex][I-1] + '=' +
      Geral.VariavelToString(ArrResIdx[I]);
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    SQLDel,
    '']);
    //
    Inseriu := False; // Inserir de novo!
    //
    Result := True;
  end else
    Result := True;
end;

{
function TFmVSLoadCRCCab2.ExcluiInserido(var Inseriu: Boolean;
  const Tabela: String; const IdxFlds: array of String;
  const IdxVals: array of Variant): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.ExcluiInserido()';
var
  I: Integer;
  SQLDel: String;
begin
  Result := False;
  if (Length(IdxFlds) <> Length(IdxVals))
  or (Length(IdxFlds) = 0) then
  begin
    Geral.MB_Aviso('A quantidade de itens da campo n�o confere com a quantidade de valores em ' +
    sProcName);
    Exit;
  end;
  if (*Atrelou and*) Inseriu then
  begin
    SQLDel := 'DELETE FROM  ' + Lowercase(Tabela) + sLineBreak + 'WHERE ' +
    IdxFlds[0] + '=' + Geral.VariavelToString(IdxVals[0]) + sLineBreak;
    for I := 1 to High(IdxFlds) do
    begin
      SQLDel := SQLDel + 'AND ' + IdxFlds[I] + '=' + Geral.VariavelToString(IdxVals[I]) +
      sLineBreak;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    SQLDel,
    '']);
    Geral.MB_SQL(Self, Dmod.QrUpd);
    Inseriu := False; // Inserir de novo!
    //
    Result := True;
  end else
    Result := True;
end;
}

function TFmVSLoadCRCCab2.ExcluiVSMovIts_de_VSMovItz(TabFromRead, TabToDel: String;
  IndiceToDel: Integer; AtrelouInseriuToDel: TERPAtrelIns; QueryFromRead: TmySQLQuery;
  ArrResIdxToDel: TArrResIdx): Boolean;
var
  OriControle, DstControle: Integer;
begin
  DstControle := ArrResIdxToDel[1];
  Result := VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(DstControle,
    Integer(TEstqMotivDel.emtdWetCurti208), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
  if Result then
  begin
    OriControle := QueryFromRead.FieldByName('Controle').AsInteger;
    NeutralizaOrigem(TabFromRead, ['Controle'], [OriControle]);
  end;
end;

procedure TFmVSLoadCRCCab2.ExecutaImportacao();
const
  sProcName = 'FmVSLoadCRCCab2.ExecutaImportacao()';
var
  I, Codigo: Integer;
begin
  try
    Codigo := QrVSLoadCRCCabCodigo.Value;
    PB1.Position := 0;
    PB1.Max := QrItensPorTab.RecordCount * 2;
    PreDefineTabelasCamposETextosSQL();
    //
    AdicionaAoMemo('Desmarcar quanto pronto! "AtrelaTabela()"');
  { Parei Aqui! Desmarcar quanto pronto!}
    QrItensPorTab.First;
    while not QrItensPorTab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      if not AtrelaTabela(QrItensPorTabNome.Value) then Exit;
      //
      QrItensPorTab.Next;
    end;
    //
  {}
    FStrActios := '';
    QrItensPorTab.First;
    while not QrItensPorTab.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      if FStrActLig then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + 'Tabela: ' + QrItensPorTabNome.Value + sLineBreak;
      end;
      //if Lowercase(QrItensPorTabNome.Value) = 'vsmulfrncab' then
        Geral.MB_Info(QrItensPorTabNome.Value);
      if not InsereTabela(QrItensPorTabNome.Value) then Exit;
      //
      if FStrActLig then
      begin
        FStrActios :=  FStrActios + sLineBreak +
        '////////////////////////////////////////////////////////////////////////////////'
        + sLineBreak + ' Abortado para an�lise de tabela �nica!!!!';
        Exit;
      end;
      QrItensPorTab.Next;
    end;

  {
    for I := Integer(Low(TCRCTableManage)) to Integer(High(TCRCTableManage)) do
    begin
      case TCRCTableManage(I) of
        (*0*)crctmIndef,
        (*1*)crctmStandBy,               // N�o usado no CRC
        (*2*)crctmERPUpload,             // Enviado do ERP ao CRC
        (*4*)crctmCRCUpNotSyncToward:    // Enviado do CRC ao CDR mas n�o sincronizado no ERP
        begin
          // nada!
        end;
        (*3*)crctmCRCUpAndSyncToward,    // Enviado do CRC ao CDR e sincronizado no ERP
        (*5*)crctmAllUpAndSyncToward:    // Enviado de qualquer servidor para qualquer servidor e sincronizado
        begin
          QrItensPorTab.First;
          while not QrItensPorTab.Eof do
          begin
            MyObjects.UpdPBOnly(PB1);
            if Lowercase(QrItensPorTabNome.Value) = 'vsmovits' then
              Geral.MB_Info(QrItensPorTabNome.Value);
            if not InsereTabela(QrItensPorTabNome.Value) then Exit;
            //
            QrItensPorTab.Next;
          end;
          //
        end;
        (*6*)crctmCRCUpAndSyncDelSelf,   // Enviado do CRC ao CDR e sincronizar item deletado com tabela propria (gemea) de dele��o
        (*7*)crctmCRCUpAndSyncDelGnrc:   // Enviado do CRC ao CDR e sincronizar item deletado - tabela gen�rica de dele��o
        (*8*)crctmAllUpAndSyncOnlyIns    // Enviado de qualquer servidor para qualquer servidor e sincronizado apenas se n�o existe valendo a info do ERP!      begin
          // Nada!
        end;
        else
        begin
          AdicionaAoMemo('A��o indefinida para ' +
          GetEnumName(TypeInfo(TCRCTableManage), I) + ' em ' + sProcName);
        end;
      end;
    end;
    //
  }
  finally
    if FStrActios <> '' then
    begin
      MeDescreve.Text := FStrActios;
      PageControl1.ActivePageIndex := 3;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSLoadCRCCab2.FormCreate(Sender: TObject);
begin
  //FCodOnImport := 0;
  ImgTipo.SQLType := stLok;
  PageControl1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  BtImporta.Enabled := False;
  FLstTabs := UnCreateGeral.RecriaTempTableNovo(ntrtt_ItensPorCod,
    DModG.QrUpdPID1, False, 1, '_Lst_Tabs_CRC_Load_');
  //
end;

function TFmVSLoadCRCCab2.ImportaTabela(Tabela: String): Boolean;
var
  Atrelou, Inseriu: Boolean;
  AtrelouInseriu: TERPAtrelIns;
  I, Indice, LoadCRCCod: Integer;
  SQLPsqCross, SQLPsqInsertedIts: String;
  ARI: TArrResIdx;
begin
  Result  := False;
  Atrelou := False;
  Inseriu := False;
  Indice := ObtemIndiceDeTabela(Tabela, False);
  if Indice >= 0 then
  begin
    ReopenSel(Indice, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Atualizando ' + Tabela);
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, Indice, [], AtrelouInseriu);
      //
      //ImportaItemTabela(Tabela, FCamposName[Indice], AtrelouInseriu, (*SQLDst*) FSQLCorda[Indice]);
      ImportaItemTabela(Tabela, Indice, AtrelouInseriu, QrSel, ARI);
      //
      QrSel.Next;
    end;
  end;
  //
  ///////
  //.....
  //////
  // no final.... se tudo der certo!
  Result := True;
  MyObjects.UpdPBOnly(PB1);
end;

procedure TFmVSLoadCRCCab2.InsereCabecalhoGeneIMEC_Arr01(Tabela: String;
  Indice: Integer; Query: TmySQLQuery; ArrResIdx: TArrResIdx;
  AtrelouInseriu: TERPAtrelIns);
const
  sProcName = 'FmVSLoadCRCCab.InsereCabecalhoGeneIMEC_Arr01()';
  Campo     = 'Codigo';
var
  SQL, SQL_Insert: String;
  OriCodigo, DstCodigo, OriMovimCod, DstMovimCod: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  OriCodigo   := Query.FieldByName(Campo).AsInteger;
  OriMovimCod := Query.FieldByName('MovimCod').AsInteger;
  DstCodigo   := ArrResIdx[1];
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, Campo, DstCodigo);
  //
  if not Inseriu then
  begin
    SQL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[Indice], Campo, DstCodigo);
    //
    if ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovimCod,
    DstMovimCod) then
    begin
      InsereRegistro_Array(Tabela, Indice, Campo, OriCodigo, DstCodigo,
        ['MovimCod'], [DstMovimCod]);
      NeutralizaOrigem(Tabela, [Campo], [OriCodigo]);
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.InsereCabecalhoGenerico_Arr01(Tabela: String;
  Indice: Integer; Query: TmySQLQuery; ArrResIdx: TArrResIdx; AtrelouInseriu:
  TERPAtrelIns; RecriaRegistro: TRecriaRegistro);
const
  sProcName = 'FmVSLoadCRCCab.InsereCabecalhoGenerico_Arr01()';
var
  Campo, SQL, SQL_Insert: String;
  Ori_IdxInt, Dst_IdxInt, AWServerID, AWStatSinc: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  if FFldsPriSemERP[Indice] = 'Codigo' then
    Campo := FFldsPriSemERP[Indice]
  else
    Campo := '';
  //
  if Campo <> '' then
  begin
    Ori_IdxInt := Query.FieldByName(Campo).AsInteger;
    AWServerID := Query.FieldByName('AWServerID').AsInteger;
    AWStatSinc := Query.FieldByName('AWStatSinc').AsInteger;
    Dst_IdxInt := ArrResIdx[1];
    //
    case RecriaRegistro of
      TRecriaRegistro.recrregNao: Dst_IdxInt := Ori_IdxInt; // nada!!
      TRecriaRegistro.recrregSim:
        ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, Campo, Dst_IdxInt);
      else AdicionaAoMemo('TRecriaRegistro n�o implementado em ' + sProcName);
    end;
    //
    if not Inseriu then
    begin
      SQL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[Indice], Campo, Dst_IdxInt);
      //
      SQL_Insert := Geral.ATS(['INSERT INTO ' + TMeuDB + '.' + LowerCase(Tabela),
        SQL,
        'WHERE ' + Campo + '=' + Geral.FF0(Ori_IdxInt),
        '']);
      SubstituiAWStatSincEmSQL(SQL_Insert);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
      SQL_Insert,
      '']);
    end;
    //
    if not Atrelou then
    begin
      AtrelaTabelas_Int(Tabela, Campo, Ori_IdxInt, Dst_IdxInt, AWServerID,
        AWStatSinc, TERPAtrelIns.atrinsAtrelAndIns);
    end;
    //
    NeutralizaOrigem(Tabela, [Campo], [Ori_IdxInt]);
  end else
    AdicionaAoMemo('FFldsPriSemERP = ' + FFldsPriSemERP[Indice] +
    ' n�o implementado em ' + sProcName);
  //
end;

function TFmVSLoadCRCCab2.InsereRegistro_Arr2(Tabela: String; TabIndex:
  Integer; QrPsq: TmySQLQuery; CamposDst: array of String;
  ValoresDst: array of Variant): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.InsereRegistro_Arra2()';
var
  ValIdx: Variant;
  FldIdx, SQL_SEL, SQL_Insert, SQL_WHR: String;
  I: Integer;
begin
  Result := False;
  if Length(CamposDst) <> Length(ValoresDst) then
  begin
    AdicionaAoMemo(sprocName + sLineBreak + 'Quantidade de itens de campos (' +
    Geral.FF0(Length(CamposDst)) + ') difere de valores(' +
    Geral.FF0(Length(ValoresDst)) + ')');
    Exit;
  end;

  SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[TabIndex], FldIdx, ValIdx);
  //
  FldIdx := FCamposName[TabIndex][0];
  ValIdx := QrPsq.FieldByName(FCamposName[TabIndex][0]).AsString;
  SQL_WHR := 'WHERE ' + FldIdx + '=' + Geral.FF0(ValIdx);
  for I := 1 to High(FCamposName[TabIndex]) (*CamposCount - 1*) do
  begin
    FldIdx := FCamposName[TabIndex][I];
    ValIdx := QrPsq.FieldByName(FCamposName[TabIndex][I]).AsString;
    SQL_WHR := SQL_WHR + ' AND ' + FldIdx + '=' + Geral.FF0(ValIdx);
  end;
  //

  for I := Low(CamposDst) to High(CamposDst) do
  begin
    SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(SQL_SEL, CamposDst[I], ValoresDst[I]);
  end;
  //
  SQL_Insert := Geral.ATS(['INSERT INTO ' + TMeuDB + '.' + LowerCase(Tabela),
    SQL_SEL,
    SQL_WHR, //'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  SubstituiAWStatSincEmSQL(SQL_Insert);
  //
  Geral.MB_Info(SQL_Insert);
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Insert,
  '']);
end;

function TFmVSLoadCRCCab2.InsereRegistro_Arr3(Tabela: String; TabIndex: Integer;
  SQL_WHR: String; CamposDst: array of String; ValoresDst: array of Variant): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.InsereRegistro_Arra3()';
var
  SQL_SEL, SQL_Insert: String;
  I: Integer;
begin
  Result := False;
  if Length(CamposDst) <> Length(ValoresDst) then
  begin
    AdicionaAoMemo(sprocName + sLineBreak + 'Quantidade de itens de campos (' +
    Geral.FF0(Length(CamposDst)) + ') difere de valores(' +
    Geral.FF0(Length(ValoresDst)) + ')');
    Exit;
  end;

  //SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[TabIndex], FldIdx, ValIdx);
  SQL_SEL := FSQLCorda[TabIndex];
  //
  for I := Low(CamposDst) to High(CamposDst) do
  begin
    SQL_SEL := dmkPF.SQLStringReplaceFldByValFld(SQL_SEL, CamposDst[I], ValoresDst[I]);
  end;
  //
  SQL_Insert := Geral.ATS(['INSERT INTO ' + TMeuDB + '.' + LowerCase(Tabela),
    SQL_SEL,
    SQL_WHR, //'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  SubstituiAWStatSincEmSQL(SQL_Insert);
  //
  //Geral.MB_Info(SQL_Insert);
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Insert,
  '']);
end;

procedure TFmVSLoadCRCCab2.InsereRegistro_Array(Tabela: String; Indice: Integer;
  FldIdx1: String; ValIdx1Ori, ValIdx1Dst: Integer; Campos: array of String;
  Valores: array of Integer);
const
  sProcName = 'FmVSLoadCRCCab.InsereRegistro_Array()';
var
  SQL, SQL_Insert: String;
  I: Integer;
begin
  if Length(Campos) <> Length(Valores) then
  begin
    AdicionaAoMemo(sprocName + sLineBreak + 'Quantidade de itens de campos (' +
    Geral.FF0(Length(Campos)) + ') difere de valores(' +
    Geral.FF0(Length(Valores)) + ')');
    Exit;
  end;
  SQL := dmkPF.SQLStringReplaceFldByValFld(FSQLCorda[Indice], FldIdx1, ValIdx1Dst);
  for I := Low(Campos) to High(Campos) do
  begin
    SQL := dmkPF.SQLStringReplaceFldByValFld(SQL, Campos[I], Valores[I]);
  end;
  //
  SQL_Insert := Geral.ATS(['INSERT INTO ' + TMeuDB + '.' + LowerCase(Tabela),
    SQL,
    'WHERE ' + FldIdx1 + '=' + Geral.FF0(ValIdx1Ori),
    '']);
  SubstituiAWStatSincEmSQL(SQL_Insert);
  //
  //Geral.MB_Info(SQL_Insert);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdSync, DModG.MySyncDB, [
  SQL_Insert,
  '']);
end;

procedure TFmVSLoadCRCCab2.InsereDadosTabelaEmIndice(Tabela: String;
  Index: Integer);
var
  CamposCount, I: Integer;
  TabOri, TabDst, TabERP, SQLOri: String;
begin
  if Index = -1 then
  begin
    FTabLstCount := FTabLstCount + 1;
    DefineTamanhosArrayTabelas(FTabLstCount);
    I := FTabLstCount - 1;
  end else
    I := Index;
  //

  TabOri := Tabela;
  TabDst := Tabela;
  if LowerCase(Tabela) = LowerCase('VSMovItz') then
    TabERP := CO_TAB_VMI
  else
    TabERP := Tabela;
  //
  FTabRead[I]       := Tabela;
  FTabWrite[I]      := TabDst;
  FSQLCorda[I]      := '';
  FFldsPriSemERP[I] := '';
  FFldsPriComERP[I] := '';
  FSQLsPriSemERP[I] := '';
  FSQLsPriComERP[I] := '';
  //
  begin
    if not VerificaIdenticidadeDeTabelas(DModG.MySyncDB, TabOri,
    Dmod.MyDB, TabDst, FSQLCorda[I](*SQLDst*), SQLOri) then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIdx, Dmod.MyDB, [
    'SHOW INDEX FROM ' + TabERP,
    'WHERE Key_name="PRIMARY" ',
    '']);
    CamposCount := 0;
    QrIdx.First;
    while not QrIdx.Eof do
    begin
      if FFldsPriSemERP[I] <> '' then
      begin
        FFldsPriSemERP[I] := FFldsPriSemERP[I] + ', ';
        FFldsPriComERP[I] := FFldsPriComERP[I] + ', ';
      end;
      FFldsPriComERP[I] := FFldsPriComERP[I] + CO_ERPSync_FldDst + QrIdx.FieldByName('Column_name').AsString;
      FFldsPriSemERP[I] := FFldsPriSemERP[I] + QrIdx.FieldByName('Column_name').AsString;
      //SQLOriIndex := SQLOriIndex + '=' + //
      //
      CamposCount := CamposCount + 1;
      SetLength(FCamposName[I], CamposCount);
      FCamposName[I][CamposCount - 1] := QrIdx.FieldByName('Column_name').AsString;
      //
      QrIdx.Next;
    end;
    FSQLsPriComERP[I] := Geral.ATS(['SELECT ' + FFldsPriComERP[I],
    'FROM ' + Lowercase(CO_ERPSync_TbPrefix + TabERP)]);
    //
    FSQLsPriSemERP[I] := Geral.ATS(['SELECT ' + FFldsPriSemERP[I],
    'FROM ' + Lowercase(TabERP)]);
    //
  end;
end;

function TFmVSLoadCRCCab2.InsereTabela(Tabela: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.InsereTabela()';
var
  Atrelou, Inseriu: Boolean;
  ARI: TArrResIdx;
  AtrelouInseriu: TERPAtrelIns;
{
  QtdFldIdx, I, LoadCRCCod: Integer;
  SQLPsqCross, SQLPsqInsertedIts: String;
  //
  Purpose: TItemTuplePurpose;
  CamposOri, CamposDst: array of String;
  ValoresOri, ValoresDst, T: array of Variant;
  Campo: String;
  VT: TVarType;
  //ContinuaBool: Boolean;
  ContinuaQtde: Integer;
}
  TabIndex, QtdFldIdx, QtdFldSyn(*, ItmFldIdx, ItmFldSyn*), Itens: Integer;
  ERPIncOver: Boolean;
  Campo: String;
  Valor: Variant;
  Campos: array of String;
  Valores: array of Variant;
  vt: TVarType;
  //
  SQL_WHR, FldIdx: String;
  ValIdx: Variant;
  I: Integer;
  AllUpAndSyncOnlyIns: Boolean;
  //
  function DefineCamposEValores(): Boolean;
  begin
    Campo := QrDBMQeLnkKLAskrCol.Value;
    Valor := QrSel.FieldByName(QrDBMQeLnkKLAskrCol.Value).AsVariant;
    if FStrActLig then
      FStrActios :=  FStrActios + Tabela + '.' + Campo + '= "' +
      Geral.VariavelToString(Valor) + '" >> ';
    if (VarType(Valor) = 3) and (Valor = 0) then
    begin
      Result := True;
      if FStrActLig then
        FStrActios :=  FStrActios + '<<' + sLineBreak;
    end else
    begin
      if ObtemValorSubstituto(QrSel, TabIndex, Valor) then
      begin
        Itens := Itens + 1;
        SetLength(Campos, Itens);
        SetLength(Valores, Itens);
        Campos[Itens - 1]  := Campo;
        Valores[Itens - 1] := Valor;
        Result := True;
        //
        if FStrActLig then
          FStrActios :=  FStrActios + sLineBreak;
      end
      else
        Result := False;
    end;
  end;
begin
  Result  := True;
  UnDmkDAC_PF.AbreMySQLQuery0(QrDBMQeLnk, Dmod.MyDB, [
  'SELECT * ',
  'FROM dbmqeilnk ',
  'WHERE LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
  '']);
  if not (TCRCTableManage(QrDBMQeLnkTbManage.Value) in CO_SeqCRCTableManageInsUpd) then
    Exit;
  AllUpAndSyncOnlyIns := TCRCTableManage(QrDBMQeLnkTbManage.Value) = crctmAllUpAndSyncOnlyIns;
  MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True, 'Inserindo ' + Tabela);
  //
  Inseriu  := False;
  TabIndex := ObtemIndiceDeTabela(Tabela, False);
  if TabIndex >= 0 then
  begin
    QtdFldIdx  := 0;
    QtdFldSyn  := 0;
    ERPIncOver := False;
    ReopenSel(TabIndex, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    //
    QrDBMQeLnk.First;
    while not QrDBMQeLnk.Eof do
    begin
      case TItemTuplePurpose(QrDBMQeLnkTbPurpose.Value) of
        // Indices prim�rios com necessidade de comutar no ERP.
        (*01*)TItemTuplePurpose.itpCDRIncremSync,
        (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
        begin
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
        (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
        (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
        begin
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
        (*02*)TItemTuplePurpose.itpERPRelatnSync,
        (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
        begin
        end;
        // Campos com relacionamento e com necessidade de comutar
        (*04*)TItemTuplePurpose.itpCDRRelatnSync,
        (*10*)TItemTuplePurpose.itpCDRRelSncOrfao,
        (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
        begin
          QtdFldSyn := QtdFldSyn + 1;
        end;
        // Outros campos sem necessidade de comutar
        (*05*)TItemTuplePurpose.itpUsrPrimtivData,
        (*06*)TItemTuplePurpose.itpSysOrCalcData,
        (*07*)TItemTuplePurpose.itpERPSync,
        (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
        (*12*)TItemTuplePurpose.itpDeprecado,
        (*13*)TItemTuplePurpose.itpInutilizado,
        (*15*)TItemTuplePurpose.itpCDRDeleteSync:
        begin
          // Nada!
        end;
        // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
        // Somente copiar ao ERP se n�o existir.
        (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
        begin
          //ver o que fazer!
          ERPIncOver := True;
          QtdFldIdx := QtdFldIdx + 1;
        end;
        // (*00*)TItemTuplePurpose.itpIndef,
        else begin
          AdicionaAoMemo('Tabela ' + Tabela + '. ' +
          GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkTbPurpose.Value) +
          ' n�o implementado em ' + sProcName);
          Exit;
        end;
      end;
      //
      QrDBMQeLnk.Next;
    end;
    if QtdFldIdx = 0 then
    begin
      AdicionaAoMemo('Tabela ' + Tabela + '. Nenhum �ndice prim�rio definido!');
    end;
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
      //ItmFldIdx := 0;
      //ItmFldSyn := 0;
      Itens := 0;
      SetLength(Campos, 0);
      SetLength(Valores, 0);
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, TabIndex, [], AtrelouInseriu);
      Atrelou := ObtemAtrelou(AtrelouInseriu);
      Inseriu := ObtemInseriu(AtrelouInseriu);
      //
      case AtrelouInseriu of
        TERPAtrelIns.atrinsNoAtrelNoIns,
        TERPAtrelIns.atrinsNoAtrelButIns,
        TERPAtrelIns.atrinsNoInsButAtrel,
        TERPAtrelIns.atrinsAtrelAndIns:
        begin
          QrDBMQeLnk.First;
          while not QrDBMQeLnk.Eof do
          begin
            case TItemTuplePurpose(QrDBMQeLnkTbPurpose.Value) of
              // Indices prim�rios com necessidade de comutar no ERP.
              (*01*)TItemTuplePurpose.itpCDRIncremSync,
              (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              // Indices prim�rios mas sem necess�dade de comutar no ERP, apenas copiar.
              (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
              (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
              begin
                //
              end;
              // Campos com relacionamento mas sem necessidade de comutar (j� corretos)
              (*02*)TItemTuplePurpose.itpERPRelatnSync,
              (*03*)TItemTuplePurpose.itpSysRelatnPrDf:
              begin
                // Nada
              end;
              // Campos com relacionamento e com necessidade de comutar
              (*04*)TItemTuplePurpose.itpCDRRelatnSync,
              (*10*)TItemTuplePurpose.itpCDRRelSncOrfao:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              (*14*)TItemTuplePurpose.itpCDRRelSncMulTab:
              begin
                if not DefineCamposEValores() then
                  Exit;
              end;
              // Outros campos sem necessidade de comutar
              (*05*)TItemTuplePurpose.itpUsrPrimtivData,
              (*06*)TItemTuplePurpose.itpSysOrCalcData,
              (*07*)TItemTuplePurpose.itpERPSync,
              (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
              (*12*)TItemTuplePurpose.itpDeprecado,
              (*13*)TItemTuplePurpose.itpInutilizado,
              (*15*)TItemTuplePurpose.itpCDRDeleteSync:
              begin
                // Nada!
              end;
              // �ndice prim�rio que se cadastrado no ERP sobrep�e ao CDR.
              // Somente copiar ao ERP se n�o existir.
              (*9*)TItemTuplePurpose.itpERPSrvrIncOver:
              begin
                ERPIncOver := True;
                //
                if not DefineCamposEValores() then
                  Exit;
              end;
              // (*00*)TItemTuplePurpose.itpIndef,
              else begin
                AdicionaAoMemo('Tabela ' + Tabela + '. ' +
                GetEnumName(TypeInfo(TItemTuplePurpose), QrDBMQeLnkTbPurpose.Value) +
                ' n�o implementado em ' + sProcName);
                Exit;
              end;
            end;
            //
            QrDBMQeLnk.Next;
          end;
          // Parei Aqui
          FldIdx := FCamposName[TabIndex][0];
          ValIdx := QrSel.FieldByName(FCamposName[TabIndex][0]).AsString;
          SQL_WHR := 'WHERE ' + FldIdx + '=' + Geral.FF0(ValIdx);
          for I := 1 to High(FCamposName[TabIndex]) (*CamposCount - 1*) do
          begin
            FldIdx := FCamposName[TabIndex][I];
            ValIdx := QrSel.FieldByName(FCamposName[TabIndex][I]).AsString;
            SQL_WHR := SQL_WHR + ' AND ' + FldIdx + '=' + Geral.FF0(ValIdx);
          end;
          if Inseriu and AllUpAndSyncOnlyIns then
          begin
            // Nada! Apenas neutralliza!
            //Cadastro j� existe no servidor e n�o deve ser sobrescrito!
            NeutralizaOrige2(Tabela, SQL_WHR);
          end else
          begin
            if ExcluiAtreladoEInserid2(Atrelou, Inseriu, Tabela, TabIndex, ARI) then
              //if InsereRegistro_Arr2(Tabela, TabIndex, QrSel, Campos, Valores) then
              if InsereRegistro_Arr3(Tabela, TabIndex, SQL_WHR, Campos, Valores) then
                  NeutralizaOrige2(Tabela, SQL_WHR);
          end;
        end;
        else
        begin
          Result := False;
          AdicionaAoMemo('Tabela ' + Tabela + ' TERPAtrelIns n�o implementado em ' + sProcName);
        end
      end;
      //
      QrSel.Next;
    end;
  end;
  // no final.... se tudo der certo!
  Result := True;
end;

function TFmVSLoadCRCCab2.InsereTodaTabelaAtrelada(Tabela: String): Boolean;
begin
  ImportaTabela(LowerCase(Tabela));
end;

procedure TFmVSLoadCRCCab2.InsUpdVSLoadCRCTbs(const NomeTab: String);
var
  SQLType: TSQLType;
  Controle, Registros, Codigo: Integer;
begin
  Controle := 0;
  Codigo := QrVSLoadCRCCabCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqTbs, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrctbs ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND LOWER(Tabela)=LOWER("' + Trim(NomeTab) + '") ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM ' + Lowercase(NomeTab),
  'WHERE LoadCRCCod=' + Geral.FF0(Codigo),
  '']);
  Registros := Dmod.QrAux.FieldByName('ITENS').AsInteger;
  //
  if QrPsqTbs.RecordCount > 0 then
  begin
    Controle := QrPsqTbsControle.Value;
    if Controle = 0 then Exit;
    SQLType := stUpd;
  end;
  if Controle = 0 then
  begin
    SQLType  := stIns;
    Controle := UMyMod.BPGS1I32('vsloadcrctbs', 'Controle', '', '', tsPos, SQLType, 0);
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrctbs', False, [
  'Codigo', 'Tabela', 'Registros'], [
  'Controle'], [
  Codigo, NomeTab, Registros], [
  Controle], True);
end;

procedure TFmVSLoadCRCCab2.ItsImporta1Click(Sender: TObject);
begin
  ExecutaImportacao();
end;
{
procedure TFmVSLoadCRCCab2.ItsImporta1Click(Sender: TObject);
var
  Codigo: Integer;
const
  MaxPBPosition = 26;
  sProcName = 'FmVSLoadCRCCab.ItsImporta1Click()';
  ArrTabelas:  array[0..MaxPBPosition-2] of String = ('VSEntiMP', 'VSMovCab',
  'VSInnCab', 'VSEscCab', 'VSPrePalCab', 'VSCacCab', 'VSGerArtA', 'VSGerRclA',
  'VSPalletA', CO_TAB_VMI, 'MovimTwn', 'VSMulFrnCab', CO_TAB_VMI, 'VSMulFrnCab',
  'VSMulFrnIts', 'VSPaClaCabA', 'VSPaClaItsA', 'VSPaRclCabA', 'VSPaRclItsA',
  'VSOpeCab', 'VSPWECab', 'VSTrfLocCab', 'VSOutCab', 'VSMovItz', 'VSCacItsA');
var
  Tabela: String;
  I: Integer;
  Qry: TmySQLQuery;
  Achou: Boolean;
begin
  'SELECT DISTINCT KLAskrTab, COUNT(Purpose) ITENS ',
  'FROM dbmqeilnk ',
  'WHERE Purpose IN (1,8,9,16,17) ',
  'GROUP BY KLAskrTab ',
  'ORDER BY KLAskrTab ',

  Codigo := QrVSLoadCRCCabCodigo.Value;
  PB1.Position := 0;
  PB1.Max := MaxPBPosition;
  PreDefineTabelasCamposETextosSQL();
  //FNaoImportou := 0;
  // 1A :: Cadastros sem Vinculo
  ImportaTabela(LowerCase('VSEntiMP'));
  // 1B :: Cadastro com vinculos de outros cadastros que foram importados
  // Nada!
  //
  // 2A :: Lan�amentos Cadastro com vinculos de cadastros que foram importados
  //
  // 2B :: Lan�amentos Cadastro com vinculos de lan�amentos cadastros que foram criados
  //
  // 2C :: Lan�amentos Cadastro com vinculos de Cabe�alhos que foram criados
  ImportaTabela(LowerCase('VSMovCab'));
  ///
  // 3A :: Cabe�alhos
  ImportaTabela(LowerCase('VSInnCab'));
  ImportaTabela(LowerCase('VSEscCab'));
  ImportaTabela(LowerCase('VSPrePalCab'));
  ImportaTabela(LowerCase('VSCacCab'));
  ImportaTabela(LowerCase('VSGerArtA'));
  ImportaTabela(LowerCase('VSGerRclA'));
  //....
  //
  // 3B :: Cadastro com vinculos de cabe�alhos que j� foram importados
  ImportaTabela(LowerCase('VSPalletA'));
  //
  // 11A :: Lan�amentos de itens
  AtrelaTodaTabela_IndicePri01Int(LowerCase(CO_TAB_VMI), 'Controle');
  AtrelaTodaTabela_CampoNaoIdx(LowerCase(CO_TAB_VMI), 'MovimTwn');
  AtrelaTodaTabela_IndicePri01Int(Lowercase('VSMulFrnCab'), 'Codigo');
  InsereTodaTabelaAtrelada(LowerCase(CO_TAB_VMI));
  InsereTodaTabelaAtrelada(LowerCase('VSMulFrnCab'));
  ///
  ImportaTabela(LowerCase('VSMulFrnIts'));
  //
  ImportaTabela(LowerCase('VSPaClaCabA'));
  ImportaTabela(LowerCase('VSPaClaItsA'));
  ImportaTabela(LowerCase('VSPaRclCabA'));
  ImportaTabela(LowerCase('VSPaRclItsA'));
  //
  ImportaTabela(LowerCase('VSOpeCab'));
  ImportaTabela(LowerCase('VSPWECab'));
  ImportaTabela(LowerCase('VSTrfLocCab'));
  //
  ImportaTabela(LowerCase('VSOutCab'));
  //...
  // No final ....
  ImportaTabela(LowerCase('VSMovItz'));
  //
  OlvidaTodaTabelaVSCacItsA();
  //
  Tabela := '...';
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Verificando altera��es na tabela' + Tabela);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW TABLES LIKE "erpsync_%"',
    '']);
    PB2.Position := 0;
    PB2.Max      := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      Tabela := Qry.Fields[0].AsString;
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Verificando altera��es na tabela' + Tabela);
      //
      InsUpdVSLoadCRCTbs(Tabela);
      //
      Qry.Next;
    end;
  except
    Qry.Free;
  end;
  //
  QrItensPorTab.First;
  while not QrItensPorTab.Eof do
  begin
    Achou := False;
    for I := Low(ArrTabelas) to High(ArrTabelas) do
    begin
      if LowerCase(ArrTabelas[I]) = LowerCase(QrItensPorTabNome.Value) then
      begin
        Achou := True;
        //QrItensPorTab.Last;
      end;
    end;
    if not Achou then
      AdicionaAoMemo('Tabela n�o implementada: ' + QrItensPorTabNome.Value + '. ' + sProcName);
    //
    QrItensPorTab.Next;
  end;
  //
  ImgTipo.SQLType := stLok;
  PnEdita.Visible := False;
  //
  // N�o! Limpa o Memo1!
  //PesquisaNovosDadosAImportar();
  if PB1.Position <> MaxPBPosition - 1 then
    Geral.MB_Info('Alterar MaxPBPosition para ' + Geral.FF0(PB1.Position + 1));
  MyObjects.UpdPBOnly(PB1);
  //
  LocCod(Codigo, Codigo);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;
}

function TFmVSLoadCRCCab2.ImportaItemTabela(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx: TArrResIdx):
  Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela()';
begin
  //
  if LowerCase(Tabela) = LowerCase('VSEntiMP') then
    ImportaItemTabela_VSEntiMP(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  //.....................
  else
  if LowerCase(Tabela) = LowerCase('VSPalletA') then
    ImportaItemTabela_VSPalletA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSMovCab') then
    ImportaItemTabela_VSMovCab(Tabela, Indice, AtrelouInseriu, Query)
  else
  if LowerCase(Tabela) = LowerCase('VSInnCab') then
    ImportaItemTabela_VSInnCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSEscCab') then
    ImportaItemTabela_VSEscCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPrePalCab') then
    ImportaItemTabela_VSPrePalCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSCacCab') then
    ImportaItemTabela_VSCacCab(Tabela, Indice, AtrelouInseriu, Query)
  else
  if LowerCase(Tabela) = LowerCase('VSGerArtA') then
    ImportaItemTabela_VSGerArtA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSGerRclA') then
    ImportaItemTabela_VSGerRclA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase(CO_TAB_VMI) then
    ImportaItemTabela_VMI(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSMulFrnCab') then
    ImportaItemTabela_VSMulFrnCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSMulFrnIts') then
    ImportaItemTabela_VSMulFrnIts(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPaClaCabA') then
    ImportaItemTabela_VSPaClaCabA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPaClaItsA') then
    ImportaItemTabela_VSPaClaItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPaRclCabA') then
    ImportaItemTabela_VSPaRclCabA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPaRclItsA') then
    ImportaItemTabela_VSPaRclItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  //.....................
  else
  if LowerCase(Tabela) = LowerCase('VSOpeCab') then
    ImportaItemTabela_VSOpeCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSPWECab') then
    ImportaItemTabela_VSPWECab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSTrfLocCab') then
    ImportaItemTabela_VSTrfLocCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
  if LowerCase(Tabela) = LowerCase('VSOutCab') then
    ImportaItemTabela_VSOutCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  //.....................
  else
  if LowerCase(Tabela) = LowerCase('VSMovItz') then
    ImportaItemTabela_VSMovItz(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  //.....................
  else
  if LowerCase(Tabela) = LowerCase('VSCacItsA') then
    ImportaItemTabela_VSCacItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx)
  else
    AdicionaAoMemo('Tabela n�o implementada: ' + Tabela + sLineBreak +
    sProcName);
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx: TArrResIdx):
  Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VMI(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VMI()';
var
  ARI: TArrResIdx;
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      ApenasInsereVMI(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSCacCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSCacCab()';
var
  ArrResIdx: TArrResIdx;
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns: AtrelaEInsereVSCacCab(Tabela, Indice, AtrelouInseriu, Query);

    // Nao atrelou mas inseriu
    //TERPAtrelIns.atrinsNoAtrelButIns:
    // Atrelou mas nao inseriu
    //TERPAtrelIns.atrinsNoInsButAtrel:
    // Atrelou e inseriu
    //TERPAtrelIns.atrinsAtrelAndIns:
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSCacItsA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
  TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSCacItsA()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSEntiMP(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSEntiMP()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns,
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsNoAtrelButIns,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      InsereCabecalhoGenerico_Arr01(Tabela, Indice, Query, ArrResIdx,
      AtrelouInseriu, TRecriaRegistro.recrregNao);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSEscCab(Tabela: String; Indice:
  Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSEscCab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      InsereCabecalhoGenerico_Arr01(Tabela, Indice, Query, ArrResIdx,
      AtrelouInseriu, TRecriaRegistro.recrregSim);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSGerArtA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSGerArtA()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns,
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEOuInsereVSGerArtA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSGerRclA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
  TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSGerRclA()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    //TERPAtrelIns.atrinsNoAtrelNoIns:
    // Nao atrelou mas inseriu
    //TERPAtrelIns.atrinsNoAtrelButIns:
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel:
    begin
      ApenasInsereVSGerRclA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // Atrelou e inseriu
    //TERPAtrelIns.atrinsAtrelAndIns:
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSInnCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSInnCab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      InsereCabecalhoGeneIMEC_Arr01(Tabela, Indice, Query, ArrResIdx, AtrelouInseriu);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSMovCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSMovCab()';
var
  ArrResIdx: TArrResIdx;
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSMovCab(Tabela, Indice, AtrelouInseriu, Query);
    end;
    // Nao atrelou mas inseriu
    //TERPAtrelIns.atrinsNoAtrelButIns:
    // Atrelou mas nao inseriu
    //TERPAtrelIns.atrinsNoInsButAtrel:
    // Atrelou e inseriu
    //TERPAtrelIns.atrinsAtrelAndIns:
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSMovItz(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSMovItz()';
var
  OriControle: Integer;
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      OriControle := Query.FieldByName('Controle').AsInteger;
      OlvidaOrigem(Tabela, ['Controle'], [OriControle]);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel:
    begin
      OriControle := Query.FieldByName('Controle').AsInteger;
      OlvidaOrigem(Tabela, ['Controle'], [OriControle]);
    end;
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      ExcluiVSMovIts_de_VSMovItz(Tabela, CO_TAB_VMI, Indice(*ToDel*),
      AtrelouInseriu(*ToDel*), Query(*FromRead*), ArrResIdx(*ToDel*));
    end;
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSMulFrnCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSMulFrnCab()';
var
  ARI: TArrResIdx;
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    //TERPAtrelIns.atrinsNoAtrelNoIns:
    // Nao atrelou mas inseriu
    //TERPAtrelIns.atrinsNoAtrelButIns:
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel:
    begin
      ApenasInsereVSMulfrnCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // Atrelou e inseriu
    //TERPAtrelIns.atrinsAtrelAndIns:
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSMulFrnIts(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSMulFrnIts()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSMulFrnIts(Tabela, Indice, AtrelouInseriu, Query);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSOpeCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSOpeCab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSOutCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSOutCab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      InsereCabecalhoGeneIMEC_Arr01(Tabela, Indice, Query, ArrResIdx, AtrelouInseriu);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPaClaCabA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPaClaCabA()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns,
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEOuInsereVSPaClaCabA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPaClaItsA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPaClaItsA()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEOuInsereVSPaClaItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEOuInsereVSPaClaItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPalletA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
  TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPalletA()';
var
  FormaPJC, OriCodigo, DstCodigo, AWServerID, AWStatSinc: Integer;
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEOuInsereVSPalletA(Tabela, Indice, AtrelouInseriu, Query);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns:
    begin
      OriCodigo  := Query.FieldByName('Codigo').AsInteger;
      DstCodigo  := OriCodigo;
      //FNaoImportou := FNaoImportou + 1;
      if RGPalletsAtrelados.ItemIndex = 0 then
      begin
        FormaPJC := MyObjects.SelRadioGroup(' Pallets j� Cadastrados',
        'Escolha a a��o para pallets j� cadastrados', [
        'Ver antes de agir (um a um dos j� cadastrados)',
        'Apenas atrelar (todos j� cadastrados)'], -1);
        RGPalletsAtrelados.ItemIndex := FormaPJC + 1;
      end;
      case RGPalletsAtrelados.ItemIndex  of
        0: Result := False;
        1: VS_Jan.MostraFormVSLoadCRCPalletA(OriCodigo);
        2:
        begin
          AWServerID := Query.FieldByName('AWServerID').AsInteger;
          AWStatSinc := Query.FieldByName('AWStatSinc').AsInteger;
          AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID,
            AWStatSinc, AtrelouInseriu);
          (*
          InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [], []);
          *)
          //
          NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
        end;
        else
        begin
          Result := False;
          AdicionaAoMemo('RGPalletsAtrelados.ItemIndex indefinido!');
        end;
      end;
    end;
    // Atrelou mas nao inseriu
    //TERPAtrelIns.atrinsNoInsButAtrel:
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEOuInsereVSPalletA(Tabela, Indice, AtrelouInseriu, Query);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPaRclCabA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPaRclCabA()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSPaRclCabA(Tabela, Indice, AtrelouInseriu, Query);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPaRclItsA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPaRclItsA()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEOuInsereVSPaRclItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // Nao atrelou mas inseriu
    TERPAtrelIns.atrinsNoAtrelButIns: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel: MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
    // Atrelou e inseriu
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEOuInsereVSPaRclItsA(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    // ???
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPrePalCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
  TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPrePalCab()';
begin
  case AtrelouInseriu of
    // Nao atrelou nem inseriu
    TERPAtrelIns.atrinsNoAtrelNoIns,

    //TERPAtrelIns.atrinsNoAtrelNoIns:
    // Nao atrelou mas inseriu
    //TERPAtrelIns.atrinsNoAtrelButIns:
    // Atrelou mas nao inseriu
    TERPAtrelIns.atrinsNoInsButAtrel:
    begin
      InsereCabecalhoGenerico_Arr01(Tabela, Indice, Query, ArrResIdx,
      AtrelouInseriu, TRecriaRegistro.recrregSim)
    end;
    // Atrelou e inseriu
    //TERPAtrelIns.atrinsAtrelAndIns:
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSPWECab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSPWECab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

function TFmVSLoadCRCCab2.ImportaItemTabela_VSTrfLocCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ImportaItemTabela_VSTrfLocCab()';
begin
  case AtrelouInseriu of
    TERPAtrelIns.atrinsNoAtrelNoIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns:
    begin
      AtrelaEInsereVSXxxCab(Tabela, Indice, AtrelouInseriu, Query, ArrResIdx);
    end;
    else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ArrResIdx);
  end;
end;

procedure TFmVSLoadCRCCab2.ItsAltera1Click(Sender: TObject);
begin
  MostraVSLoadCRCIts(stUpd);
end;

procedure TFmVSLoadCRCCab2.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSLoadCRCCab2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSLoadCRCCab2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSLoadCRCCab2.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSLoadCRCIts', 'Controle', QrVSLoadCRCItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSLoadCRCIts,
      QrVSLoadCRCItsControle, QrVSLoadCRCItsControle.Value);
    ReopenVSLoadCRCTbs(Controle);
  end;
}
end;

procedure TFmVSLoadCRCCab2.ReopenSel(Indice: Integer; SQLExtra: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MySyncDB, [
  (*SQLDst*)FSQLCorda[Indice],
  'WHERE AWServerID=' + Geral.FF0(QrVSLoadCRCCabOriServrID.Value),
(*
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stUpSinc)) + ') ', //stDwnSync e stUpSinc
*)
  'AND AWStatSinc NOT IN (' +
  Geral.FF0(Integer(stDwnSinc)) + ',' +
  Geral.FF0(Integer(stDwnNoSinc)) + ')', //8 e 10
  SQLExtra,
  '']);
end;

procedure TFmVSLoadCRCCab2.ReopenVSLoadCRCTbs(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSLoadCRCTbs, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrctbs ',
  'WHERE Codigo=' + Geral.FF0(QrVSLoadCRCCabCodigo.Value),
  '']);
  //
  QrVSLoadCRCTbs.Locate('Controle', Controle, []);
end;

procedure TFmVSLoadCRCCab2.ReopenVSLoadCRCWrn(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSLoadCRCWrn, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsloadcrcwrn ',
  'WHERE Codigo=' + Geral.FF0(QrVSLoadCRCCabCodigo.Value),
  '']);
  //
  QrVSLoadCRCWrn.Locate('Controle', Controle, []);
end;

procedure TFmVSLoadCRCCab2.DefineONomeDoForm;
begin
end;

procedure TFmVSLoadCRCCab2.DefineTamanhosArrayTabelas(K: Integer);
begin
  SetLength(FTabRead, K);
  SetLength(FTabWrite, K);
  SetLength(FSQLCorda, K);
  SetLength(FFldsPriSemERP, K);
  SetLength(FFldsPriComERP, K);
  SetLength(FSQLsPriSemERP, K);
  SetLength(FSQLsPriComERP, K);
  SetLength(FCamposName, K);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSLoadCRCCab2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSLoadCRCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLoadCRCCab2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSLoadCRCCab2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSLoadCRCCab2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSLoadCRCCab2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSLoadCRCCab2.SubstituiAWStatSincEmSQL(var SQL: String);
begin
  SQL := dmkPF.SQLStringReplaceFldByValFld(
    SQL, 'AWStatSinc', Integer(stDwnSinc)(*8*));
//  SQL := Geral.Substitui(SQL, ', AWStatSinc', ', 8 AWStatSinc');
end;

procedure TFmVSLoadCRCCab2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSLoadCRCCab2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSLoadCRCCabCodigo.Value;
  Close;
end;

procedure TFmVSLoadCRCCab2.ItsInclui1Click(Sender: TObject);
begin
  MostraVSLoadCRCIts(stIns);
end;

procedure TFmVSLoadCRCCab2.CabAltera1Click(Sender: TObject);
begin
  //FCodOnImport := 0;
  BtConfirma.Enabled := False;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSLoadCRCCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsloadcrccab');
  //TPData.Date              := QrVSLoadCRCCabDataHora.Value;
  //EdHora.ValueVariant      := QrVSLoadCRCCabDataHora.Value;
  //
  PesquisaNovosDadosAImportar(QrVSLoadCRCCabOriServrID.Value);
end;

procedure TFmVSLoadCRCCab2.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSLoadCRCCab2.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  Codigo, OriServrID, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  OriServrID     := EdOriServrID.ValueVariant;
  if MyObjects.FIC(OriServrID = 0, EdOriServrID,
  'Informe o Server ID de origem!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsloadcrccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrccab', False, [
  'DataHora', 'OriServrID'], [
  'Codigo'], [
  DataHora, OriServrID], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stPsq;
(**)
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
(**)
    //FCodOnImport := Codigo;
    BtImporta.Enabled := True;
    BtConfirma.Enabled := False;
  end;
end;

procedure TFmVSLoadCRCCab2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  //FCodOnImport := 0;
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsloadcrccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsloadcrccab', 'Codigo');
end;

procedure TFmVSLoadCRCCab2.BtItsClick(Sender: TObject);
begin
  FStrActLig :=  CkDescreve.Checked;
  //
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSLoadCRCCab2.AdicionaAoMemo(Texto: String);
var
  Nome: String;
  Codigo, Controle, Linha, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
begin
  FMemoLinesCount := FMemoLinesCount + 1;
  Memo1.Lines.Add(Geral.FF0(FMemoLinesCount) + '. ' + Texto);
  //
  SQLType        := stIns;
  Codigo         := QrVSLoadCRCCabCodigo.Value;
  Controle       := 0;
  Linha          := FMemoLinesCount;
  Nome           := Copy(Texto, 1, 255);
  //
  Controle := UMyMod.BPGS1I32('vsloadcrcwrn', 'Controle', '', '', tsPos, SQLType, Controle);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsloadcrcwrn', False, [
  'Codigo', 'Linha', 'Nome'], [
  'Controle'], [
  Codigo, Linha, Nome], [
  Controle], True);
end;

function TFmVSLoadCRCCab2.ApenasAtrelaTabelaIdx01Int(Tabela, Campo: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ApenasAtrelaTabelaIdx01Int()';
var
  OriInt01, DstInt01, AWServerID, AWStatSinc: Integer;
begin
  OriInt01 := Query.FieldByName(Campo).AsInteger;
  DstInt01 := 0;
  AWServerID  := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc  := Query.FieldByName('AWStatSinc').AsInteger;
  if AtrelouInseriu = TERPAtrelIns.atrinsNoAtrelNoIns then
    DstInt01 := UMyMod.BPGS1I32(Tabela, Campo, '', '', tsPos, stIns, 0)
  else
    DstInt01 := ArrResIdx[1];
  //
  Result := AtrelaTabelas_Int(Tabela, Campo, OriInt01, DstInt01,
      AWServerID, AWStatSinc, AtrelouInseriu);
end;

function TFmVSLoadCRCCab2.ApenasAtrelaVMI(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ApenasAtrelaVMI()';
var
  OriControle, DstControle, AWServerID, AWStatSinc: Integer;
begin
  OriControle := Query.FieldByName('Controle').AsInteger;
  DstControle := 0;
  AWServerID  := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc  := Query.FieldByName('AWStatSinc').AsInteger;
  if AtrelouInseriu = TERPAtrelIns.atrinsNoAtrelNoIns then
    DstControle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0)
  else
    DstControle := ArrResIdx[1];
  //
  Result := AtrelaTabelas_Int(Tabela, 'Controle', OriControle, DstControle,
      AWServerID, AWStatSinc, AtrelouInseriu);
end;

function TFmVSLoadCRCCab2.ApenasInsereVMI(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ApenasInsereVMI()';
var
  OriTabela: String;
  Inocuo, AWServerID, AWStatSinc,
  OriCodigo, DstCodigo, OriControle, DstControle, OriMovimCod, DstMovimCod,
  OriSrcMovID, OriSrcNivel1, OriSrcNivel2, DstSrcMovID, DstSrcNivel1, DstSrcNivel2,
  OriDstMovID, OriDstNivel1, OriDstNivel2, DstDstMovID, DstDstNivel1, DstDstNivel2,
  OriJmpMovID, OriJmpNivel1, OriJmpNivel2, DstJmpMovID, DstJmpNivel1, DstJmpNivel2,
  OriRmsMovID, OriRmsNivel1, OriRmsNivel2, DstRmsMovID, DstRmsNivel1, DstRmsNivel2,
  OriGSPJmpMovID, OriGSPJmpNiv2, DstGSPJmpMovID, DstGSPJmpNiv2,
  OriGSPSrcMovID, OriGSPSrcNiv2, DstGSPSrcMovID, DstGSPSrcNiv2,
  MovimID, OriMovimTwn, DstMovimTwn, OriMovCodPai, DstMovCodPai,
  OriVSMulFrnCab, DstVSMulFrnCab: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  OriControle  := Query.FieldByName('Controle').AsInteger;
  AWServerID   := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc   := Query.FieldByName('AWStatSinc').AsInteger;
  OriCodigo    := Query.FieldByName('Codigo').AsInteger;
  DstCodigo    := 0;
  OriMovimCod  := Query.FieldByName('MovimCod').AsInteger;
  DstMovimCod  := 0;
  OriMovimTwn  := Query.FieldByName('MovimTwn').AsInteger;
  DstMovimTwn  := 0;
  MovimID      := Query.FieldByName('MovimID').AsInteger;
  OriTabela    := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(TEstqMovimID(MovimID), False);
  if OriTabela = CO_FIVE_ASKS then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query2, DModG.MySyncDB, [
    'SELECT MovimID  ',
    'FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(OriMovimCod),
    'AND CodigoID=' + Geral.FF0(OriCodigo),
    '']);
    OriTabela    := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(
      TEstqMovimID(Query2.FieldByName('MovimID').AsInteger));
  end;
  OriMovCodPai := Query.FieldByName('MovCodPai').AsInteger;
  DstMovCodPai := 0;

  //
  OriSrcMovID  := Query.FieldByName('SrcMovID').AsInteger;
  OriSrcNivel1 := Query.FieldByName('SrcNivel1').AsInteger;
  OriSrcNivel2 := Query.FieldByName('SrcNivel2').AsInteger;
  DstSrcMovID  := 0;
  DstSrcNivel1 := 0;
  DstSrcNivel2 := 0;

  OriDstMovID  := Query.FieldByName('DstMovID').AsInteger;
  OriDstNivel1 := Query.FieldByName('DstNivel1').AsInteger;
  OriDstNivel2 := Query.FieldByName('DstNivel2').AsInteger;
  DstDstMovID  := 0;
  DstDstNivel1 := 0;
  DstDstNivel2 := 0;

  OriJmpMovID  := Query.FieldByName('JmpMovID').AsInteger;
  OriJmpNivel1 := Query.FieldByName('JmpNivel1').AsInteger;
  OriJmpNivel2 := Query.FieldByName('JmpNivel2').AsInteger;
  DstJmpMovID  := 0;
  DstJmpNivel1 := 0;
  DstJmpNivel2 := 0;

  OriRmsMovID  := Query.FieldByName('RmsMovID').AsInteger;
  OriRmsNivel1 := Query.FieldByName('RmsNivel1').AsInteger;
  OriRmsNivel2 := Query.FieldByName('RmsNivel2').AsInteger;
  DstRmsMovID  := 0;
  DstRmsNivel1 := 0;
  DstRmsNivel2 := 0;

  OriGSPSrcMovID  := Query.FieldByName('GSPSrcMovID').AsInteger;
  //OriGSPSrcNiv1 := Query.FieldByName('GSPSrcNiv1').AsInteger;
  OriGSPSrcNiv2 := Query.FieldByName('GSPSrcNiv2').AsInteger;
  DstGSPSrcMovID  := 0;
  //DstGSPSrcNiv1 := 0;
  DstGSPSrcNiv2 := 0;

  OriGSPJmpMovID  := Query.FieldByName('GSPJmpMovID').AsInteger;
  //OriGSPJmpNiv1 := Query.FieldByName('GSPJmpNiv1').AsInteger;
  OriGSPJmpNiv2 := Query.FieldByName('GSPJmpNiv2').AsInteger;
  DstGSPJmpMovID  := 0;
  //DstGSPJmpNiv1 := 0;
  DstGSPJmpNiv2 := 0;

  OriVSMulFrnCab := Query.FieldByName('VSMulFrnCab').AsInteger;
  DstVSMulFrnCab := 0;

  //
  //
  Inocuo := 0;
  if not ObtemValorArrResIdxInt01(sProcName, Tabela, ArrResIdx, DstControle) then
    Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Controle', DstControle);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index(OriTabela, sProcName, OriCodigo,
    DstCodigo) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovimCod,
    DstMovimCod) then Exit;
    //
    if not ObtemValorDstAtrelado01FldInt('erpsync_vsmovimtwn', 'VSMovIts',
    'MovimTwn', sProcName, OriMovimTwn, DstMovimTwn) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('Src', sProcName, OriSrcMovID, OriSrcNivel1,
    OriSrcNivel2, DstSrcMovID, DstSrcNivel1, DstSrcNivel2) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('Dst', sProcName, OriDstMovID,
    OriDstNivel1, OriDstNivel2, DstDstMovID, DstDstNivel1, DstDstNivel2) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('Jmp', sProcName, OriJmpMovID, OriJmpNivel1,
    OriJmpNivel2, DstJmpMovID, DstJmpNivel1, DstJmpNivel2) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('Rms', sProcName, OriRmsMovID,
    OriRmsNivel1, OriRmsNivel2, DstRmsMovID, DstRmsNivel1, DstRmsNivel2) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('GSPSrc', sProcName, OriGSPSrcMovID, 0,
    OriGSPSrcNiv2, DstGSPSrcMovID, Inocuo, DstGSPSrcNiv2) then Exit;
    //
    if not ObtemValoresDstIDNiv1Niv2('GSPJmp', sProcName, OriGSPJmpMovID,
    0, OriGSPJmpNiv2, DstGSPJmpMovID, Inocuo, DstGSPJmpNiv2) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovCodPai,
    DstMovCodPai) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMulFrnCab', sProcName, OriVSMulFrnCab,
    DstVSMulFrnCab) then Exit;
    //
  end;
  if (not Inseriu) and (not Atrelou) then
    DstControle := UMyMod.BPGS1I32(Tabela, 'Controle', '', '', tsPos, stIns, 0);
{  J� atrelou
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Controle', OriControle, DstControle, AWServerID,
    AWStatSinc, AtrelouInseriu);
}
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Controle',
    OriControle, DstControle,
    [
      'MovimCod', 'Codigo',
      'SrcMovID', 'SrcNivel1', 'SrcNivel2',
      'DstMovID', 'DstNivel1', 'DstNivel2',
      'JmpMovID', 'JmpNivel1', 'JmpNivel2',
      'RmsMovID', 'RmsNivel1', 'RmsNivel2',
      'GSPJmpMovID', 'GSPJmpNiv2',
      'GSPSrcMovID', 'GSPSrcNiv2',
      'MovimTwn', 'MovCodPai', 'VSMulFrnCab'
    ], [
      DstMovimCod, DstCodigo,
      DstSrcMovID, DstSrcNivel1, DstSrcNivel2,
      DstDstMovID, DstDstNivel1, DstDstNivel2,
      DstJmpMovID, DstJmpNivel1, DstJmpNivel2,
      DstRmsMovID, DstRmsNivel1, DstRmsNivel2,
      DstGSPJmpMovID, DstGSPJmpNiv2,
      DstGSPSrcMovID, DstGSPSrcNiv2,
      DstMovimTwn, DstMovCodPai, DstVSMulFrnCab
    ]);
  //
  NeutralizaOrigem(Tabela, ['Controle'], [OriControle]);
end;

function TFmVSLoadCRCCab2.ApenasInsereVSGerRclA(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx: TArrResIdx):
  Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ApenasInsereVSGerRclA()';
var
  OriCodigo, DstCodigo, AWServerID, AWStatSinc,
  OriMovimCod, DstMovimCod, OriCacCod, DstCacCod: Integer;
begin
  if not ObtemValorArrResIdxInt01(sProcName, Tabela, ArrResIdx, DstCodigo) then
    Exit;
  //
  OriCodigo   := Query.FieldByName('Codigo').AsInteger;
  AWServerID  := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc  := Query.FieldByName('AWStatSinc').AsInteger;
  OriMovimCod := Query.FieldByName('MovimCod').AsInteger;
  DstMovimCod := 0;
  OriCacCod   := Query.FieldByName('CacCod').AsInteger;
  DstCacCod   := 0;
  //
  if ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovimCod,
  DstMovimCod) then
  begin
    if ObtemValorDstAtrelado01Index('VSCacCab', sProcName, OriCacCod,
    DstCacCod) then
    begin
      (* J� atrelou!
      AtrelaTabelas_Int(Tabela, 'Codigo'OriCodigo, DstCodigo, AWServerID, AWStatSinc,
        AtrelouInseriu);
      *)
      InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo,
        ['MovimCod', 'CacCod'], [DstMovimCod, DstCacCod]);
      //
      NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
    end;
  end;
end;

function TFmVSLoadCRCCab2.ApenasInsereVSMulFrnCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.ApenasInsereVSMulFrnCab()';
var
  //AWServerID, AWStatSinc,
  Inocuo1, Inocuo2, OriCodigo, DstCodigo, OriMovTypeCod, DstMovTypeCod,
  MovimType: Integer;
  Continua: Boolean;
begin
  Inocuo1 := 0;
  Inocuo2 := 0;
  if not ObtemValorArrResIdxInt01(sProcName, Tabela, ArrResIdx, DstCodigo) then
    Exit;
  //
  OriCodigo     := Query.FieldByName('Codigo').AsInteger;
  //AWServerID    := Query.FieldByName('AWServerID').AsInteger;
  //AWStatSinc    := Query.FieldByName('AWStatSinc').AsInteger;
  MovimType     := Query.FieldByName('MovimType').AsInteger;
  OriMovTypeCod := Query.FieldByName('MovTypeCod').AsInteger;
  DstMovTypeCod := 0;
  case TEstqDefMulFldVS(MovimType) of
    //edmfIndef=0,
    (*1*)edmfSrcNiv2,
    (*3*)edmfIMEI:
      Continua := ObtemValoresDstIDNiv1Niv2('Src', sProcName, (*OriSrcMovID*)0,
      (*OriSrcNivel1*)0, OriMovTypeCod, (*DstSrcMovID*)Inocuo1, (*DstSrcNivel1*)
      Inocuo2, DstMovTypeCod);
    (*2*)edmfMovCod:
      Continua := ObtemValorDstAtrelado01Index('VSMovCab', sProcName,
      OriMovTypeCod, DstMovTypeCod);
    else
    begin
      Continua := False;
      AdicionaAoMemo('"TEstqDefMulFldVS(MovimType)" indefinido em ' + sProcName);
    end;
  end;
  //
  if Continua then
  begin
    InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo,
    ['MovTypeCod'], [DstMovTypeCod]);
    //
    NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
  end;
end;

procedure TFmVSLoadCRCCab2.AtrelaEInsereVSCacCab(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEInsereVSCacCab()';
var
  TbOthr: String;
  //
  OriCodigo, DstCodigo, MovimID, OriCodigoID, DstCodigoID, AWServerID, AWStatSinc,
  IdxVsX: Integer;
  AtrlInsOthr: TERPAtrelIns;
  ARI: TArrResIdx;
begin
  DstCodigoID := 0;
  DstCodigo   := 0;
  // Codigo > ID do VSMovCab = MovimCod em cada tabela
  OriCodigo     := Query.FieldByName('Codigo').AsInteger;
  // MovimID > ID Tipo (Tabela)
  MovimID    := Query.FieldByName('MovimID').AsInteger;
  // CodigoID > Codigo em cada tabela
  OriCodigoID   := Query.FieldByName('CodigoID').AsInteger;
  AWServerID := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc := Query.FieldByName('AWStatSinc').AsInteger;
  //
  TbOthr      := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(TEstqMovimID(MovimID));
  IdxVsX      := ObtemIndiceDeTabela(TbOthr);
  ARI := ObtemAtrelouInseriu_Indice(TbOthr, nil, IdxVsX, [OriCodigoID], AtrlInsOthr);
  //
  case AtrlInsOthr of
    //TERPAtrelIns.atrinsNoAtrelNoIns:
    //TERPAtrelIns.atrinsNoAtrelButIns:
    TERPAtrelIns.atrinsNoInsButAtrel:
    begin
    if not ObtemValorArrResIdxInt01(sProcName, Tabela, ARI, DstCodigoID) then
      Exit;
(*
      if QtdIndicesARI(ARI) = 1 then
      begin
        DstCodigoID := ARI[1];
      end else
      begin
        MemoAdd_NoReImpl_Idx(Tabela, sProcName);
        Exit;
      end;
*)
    end;
    //TERPAtrelIns.atrinsAtrelAndIns:
    else MemoAdd_NoAI_Oth(Tabela, AtrlInsOthr, sProcName);
  end;
  //
  DstCodigo := UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, stIns, 0);
  //
  AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID,
    AWStatSinc, AtrelouInseriu);
  InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo,
    ['CodigoID'], [DstCodigoID]);
  //
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

procedure TFmVSLoadCRCCab2.AtrelaEInsereVSXxxCab(Tabela: String; Indice: Integer;
  AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx: TArrResIdx);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEInsereVSXxxCab()';
var
  OriCodigo, DstCodigo, OriMovimCod, DstMovimCod,
  AWServerID, AWStatSinc: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  DstCodigo      := 0;
  DstMovimCod    := 0;
  //
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriMovimCod    := Query.FieldByName('MovimCod').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstCodigo) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Codigo', DstCodigo);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovimCod,
    DstMovimCod) then Exit;
  end;
  if (not Inseriu) and (not Atrelou) then
    DstCodigo := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, stIns, 0);
    //
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID, AWStatSinc,
      AtrelouInseriu);
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [
    'MovimCod'], [
    DstMovimCod]);
    //
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

function TFmVSLoadCRCCab2.AtrelaEOuInsereVSGerArtA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEOuInsereVSGerArtA()';
var
  OriCodigo, DstCodigo, OriMovimCod, DstMovimCod, OriCacCod, DstCacCod,
  AWServerID, AWStatSinc: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriMovimCod    := Query.FieldByName('MovimCod').AsInteger;
  OriCacCod      := Query.FieldByName('CacCod').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  DstMovimCod    := 0;
  DstCacCod      := 0;
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstCodigo) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Codigo', DstCodigo);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('VSMovCab', sProcName, OriMovimCod,
    DstMovimCod) then  Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSCacCab', sProcName, OriCacCod,
    DstCacCod) then Exit;
    //
  end;
  if (not Inseriu) and (not Atrelou) then
    DstCodigo := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, stIns, 0);
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID,
    AWStatSinc, AtrelouInseriu);
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [
    'MovimCod', 'CacCod'], [
    DstMovimCod, DstCacCod]);
  //
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

function TFmVSLoadCRCCab2.AtrelaEOuInsereVSPaClaCabA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEOuInsereVSPaClaCabA()';
var
  OriCodigo, DstCodigo, OriVSGerArt, DstVSGerArt, OriVSMovIts, DstVSMovIts,
  OriCacCod, DstCacCod, AWServerID, AWStatSinc, OriVSEscCab, DstVSEscCab:
  Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou        := ObtemAtrelou(AtrelouInseriu);
  Inseriu        := ObtemInseriu(AtrelouInseriu);
  //
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriVSGerArt    := Query.FieldByName('VSGerArt').AsInteger;
  OriVSMovIts    := Query.FieldByName('VSMovIts').AsInteger;
  OriCacCod      := Query.FieldByName('CacCod').AsInteger;
  OriVSEscCab    := Query.FieldByName('VSEscCab').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  DstVSGerArt    := 0;
  DstVSMovIts    := 0;
  DstCacCod      := 0;
  DstVSEscCab    := 0;
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstCodigo) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Codigo', DstCodigo);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('VSGerArtA', sProcName, OriVSGerArt,
    DstVSGerArt) then  Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVSMovIts,
    DstVSMovIts) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSCacCab', sProcName, OriCacCod,
    DstCacCod) then Exit;
    //
    // N�o Usa!!!???
    if not ObtemValorDstAtrelado01Index('VSEscCab', sProcName, OriVSEscCab,
    DstVSEscCab) then Exit;
    //
  end;
  if (not Inseriu) and (not Atrelou) then
    DstCodigo := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, stIns, 0);
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID,
    AWStatSinc, AtrelouInseriu);
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [
    'VSGerArtA', 'VSMovIts', 'CacCod', 'OriVSEscCab'], [
    DstVSGerArt, DstVSMovIts, DstCacCod, OriVSEscCab]);
  //
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

procedure TFmVSLoadCRCCab2.AtrelaEInsereVSMovCab(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEInsereVSMovCab()';
var
  TbOthr, SQL_Insert: String;
  OriCodigo, DstCodigo, OriCodigoID, DstCodigoID, AWServerID, AWStatSinc,
  MovimID, IdxVsX: Integer;
  AtrlInsOthr: TERPAtrelIns;
  ARI: TArrResIdx;
begin
  DstCodigo     := 0;
  DstCodigoID   := 0;
  // Codigo > ID do VSMovCab = MovimCod em cada tabela
  OriCodigo     := Query.FieldByName('Codigo').AsInteger;
  // MovimID > ID Tipo (Tabela)
  MovimID       := Query.FieldByName('MovimID').AsInteger;
  // CodigoID > Codigo em cada tabela
  OriCodigoID   := Query.FieldByName('CodigoID').AsInteger;
  AWServerID    := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc    := Query.FieldByName('AWStatSinc').AsInteger;
  //
  TbOthr        := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(TEstqMovimID(MovimID));
  IdxVsX        := ObtemIndiceDeTabela(TbOthr);
  ARI           := ObtemAtrelouInseriu_Indice(TbOthr, nil, IdxVsX, [OriCodigoID], AtrlInsOthr);
  //
  case AtrlInsOthr of
    TERPAtrelIns.atrinsNoAtrelNoIns,
    TERPAtrelIns.atrinsNoAtrelButIns:
    begin
      DstCodigoID := UMyMod.BPGS1I32(TbOthr, 'Codigo', '', '', tsPos, stIns, 0);
      AtrelaTabelas_Int(TbOthr, 'Codigo', OriCodigoID, DstCodigoID, AWServerID,
        AWStatSinc, AtrlInsOthr);
    end;
    TERPAtrelIns.atrinsNoInsButAtrel,
    TERPAtrelIns.atrinsAtrelAndIns: ; // Nada
    else AdicionaAoMemo('A��o indefinida para "AtrlInsOthr" em ' + sProcName);
  end;
  //
  DstCodigo := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, stIns, 0);
  //VS_CRC_PF.InsereVSMovCab(MovimCod, TEstqMovimID(MovimID), Dst_Codigo);
  AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo,
    AWServerID, AWStatSinc, AtrelouInseriu);
  InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo,
    ['CodigoID'], [DstCodigoID]);
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

procedure TFmVSLoadCRCCab2.AtrelaEInsereVSMulFrnIts(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEInsereVSMulFrnIts()';
var
  OriControle, DstControle, OriCodigo, DstCodigo, OriCodRemix, DstCodRemix,
  AWServerID, AWStatSinc: Integer;
begin
  DstControle    := 0;
  DstCodigo      := 0;
  DstCodRemix    := 0;
  //
  OriControle    := Query.FieldByName('Controle').AsInteger;
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriCodRemix    := Query.FieldByName('CodRemix').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  if ObtemValorDstAtrelado01Index('VSMulFrnCab', sProcName, OriCodigo, DstCodigo) then
  begin
    if ObtemValorDstAtrelado01Index('VSMulFrnCab', sProcName, OriCodRemix, DstCodRemix) then
    begin
      DstControle := UMyMod.BPGS1I32(Tabela, 'Controle', '', '', tsPos, stIns, 0);
      //
      AtrelaTabelas_Int(Tabela, 'Controle', OriControle, DstControle,
        AWServerID, AWStatSinc, AtrelouInseriu);
      InsereRegistro_Array(Tabela, Indice, 'Controle', OriControle,
      DstControle, [
      'Codigo', 'CodRemid'], [
      DstCodigo, DstCodRemix]);
      //
      NeutralizaOrigem(Tabela, ['Controle'], [OriControle]);
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.AtrelaEOuInsereVSPaClaItsA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery; ArrResIdx:
  TArrResIdx);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEOuInsereVSPaClaItsA()';
var
  OriControle, DstControle, OriCodigo, DstCodigo, OriVMI_Sorc, DstVMI_Sorc,
  OriVMI_Baix, DstVMI_Baix, OriVMI_Dest, DstVMI_Dest, AWServerID, AWStatSinc:
  Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  OriControle    := Query.FieldByName('Controle').AsInteger;
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriVMI_Sorc    := Query.FieldByName('VMI_Sorc').AsInteger;
  OriVMI_Baix    := Query.FieldByName('VMI_Baix').AsInteger;
  OriVMI_Dest    := Query.FieldByName('VMI_Dest').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  DstControle    := 0;
  DstCodigo      := 0;
  DstVMI_Sorc    := 0;
  DstVMI_Baix    := 0;
  DstVMI_Dest    := 0;
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstControle) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Controle', DstCodigo);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('VSPaClaCabA', sProcName, OriCodigo,
    DstCodigo) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Sorc,
    DstVMI_Sorc) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Baix,
    DstVMI_Baix) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Dest,
    DstVMI_Dest) then Exit;
    //
  end;
  if (not Inseriu) and (not Atrelou) then
    DstControle := UMyMod.BPGS1I32(Tabela, 'Controle', '', '', tsPos, stIns, 0);
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Controle', OriControle, DstControle,
    AWServerID, AWStatSinc, AtrelouInseriu);
  if (not Inseriu) then
    InsereRegistro_Array(Tabela, Indice, 'Controle', OriControle,
    DstControle, [
    'Codigo', 'VMI_Sorc', 'VMI_Baix', 'VMI_Dest'], [
    DstCodigo, DstVMI_Sorc, DstVMI_Baix, DstVMI_Dest]);
  //
  NeutralizaOrigem(Tabela, ['Controle'], [OriControle]);
end;

procedure TFmVSLoadCRCCab2.AtrelaEOuInsereVSPalletA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEOuInsereVSPalletA()';
var
  SQL_Delete, SQL_Insert: String;
  OriCodigo, DstCodigo, OriGerRclCab, DstGerRclCab, AWServerID, AWStatSinc: Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou := ObtemAtrelou(AtrelouInseriu);
  Inseriu := ObtemInseriu(AtrelouInseriu);
  //
  OriCodigo    := Query.FieldByName('Codigo').AsInteger;
  DstCodigo    := OriCodigo; // O mesmo!!!
  OriGerRclCab := Query.FieldByName('GerRclCab').AsInteger;
  DstGerRclCab := 0;
  AWServerID   := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc   := Query.FieldByName('AWStatSinc').AsInteger;
  //
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Codigo', DstCodigo);
  //
  if not Inseriu then
  begin
    if not ObtemValorDstAtrelado01Index('VSGerRclA', sProcName, OriGerRclCab, DstGerRclCab) then
      Exit;
    InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [
    'GerRclCab'], [DstGerRclCab]);
  end;
  //
  if not Atrelou then
  begin
    AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID,
      AWStatSinc, TERPAtrelIns.atrinsAtrelAndIns);
  end;
  //
  NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
end;

procedure TFmVSLoadCRCCab2.AtrelaEInsereVSPaRclCabA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEInsereVSPaRclCabA()';
var
  OriCodigo, DstCodigo, OriVSGerRcl, DstVSGerRcl, OriVSMovIts, DstVSMovIts,
  OriCacCod, DstCacCod, AWServerID, AWStatSinc: Integer;
begin
  DstCodigo := 0;
  DstVSGerRcl    := 0;
  DstVSMovIts    := 0;
  DstCacCod      := 0;
  //
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriVSGerRcl    := Query.FieldByName('VSGerRcl').AsInteger;
  OriVSMovIts    := Query.FieldByName('VSMovIts').AsInteger;
  OriCacCod      := Query.FieldByName('CacCod').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  if ObtemValorDstAtrelado01Index('VSGerRclA', sProcName, OriVSGerRcl, DstVSGerRcl) then
  begin
    if ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVSMovIts, DstVSMovIts) then
    begin
      if ObtemValorDstAtrelado01Index('VSCacCab', sProcName, OriCacCod, DstCacCod) then
      begin
        DstCodigo := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, stIns, 0);
        //
        AtrelaTabelas_Int(Tabela, 'Codigo', OriCodigo, DstCodigo, AWServerID, AWStatSinc,
          AtrelouInseriu);
        InsereRegistro_Array(Tabela, Indice, 'Codigo', OriCodigo, DstCodigo, [
        'VSGerRclA', 'VSMovIts', 'CacCod'], [
        DstVSGerRcl, DstVSMovIts, DstCacCod]);
        //
        NeutralizaOrigem(Tabela, ['Codigo'], [OriCodigo]);
      end;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.AtrelaEOuInsereVSPaRclItsA(Tabela: String;
  Indice: Integer; AtrelouInseriu: TERPAtrelIns; Query: TmySQLQuery;
  ArrResIdx: TArrResIdx);
const
  sProcName = 'FmVSLoadCRCCab.AtrelaEOuInsereVSPaRclItsA()';
var
  OriControle, DstControle, OriCodigo, DstCodigo, OriVMI_Sorc, DstVMI_Sorc,
  OriVMI_Baix, DstVMI_Baix, OriVMI_Dest, DstVMI_Dest, AWServerID, AWStatSinc:
  Integer;
  Atrelou, Inseriu: Boolean;
begin
  Atrelou        := ObtemAtrelou(AtrelouInseriu);
  Inseriu        := ObtemInseriu(AtrelouInseriu);
  //
  DstControle    := 0;
  DstCodigo      := 0;
  DstVMI_Sorc    := 0;
  DstVMI_Baix    := 0;
  DstVMI_Dest    := 0;
  //
  OriControle    := Query.FieldByName('Controle').AsInteger;
  OriCodigo      := Query.FieldByName('Codigo').AsInteger;
  OriVMI_Sorc    := Query.FieldByName('VMI_Sorc').AsInteger;
  OriVMI_Baix    := Query.FieldByName('VMI_Baix').AsInteger;
  OriVMI_Dest    := Query.FieldByName('VMI_Dest').AsInteger;
  AWServerID     := Query.FieldByName('AWServerID').AsInteger;
  AWStatSinc     := Query.FieldByName('AWStatSinc').AsInteger;
  //
  if not ObtemValorArrResIdxIntAtrIns01(Atrelou, Inseriu, Tabela, Indice, Query,
    ArrResIdx, DstControle) then Exit;
  //
  ExcluiAtreladoEInserido(Atrelou, Inseriu, Tabela, 'Controle', DstControle);
  //
  if (not Inseriu) then
  begin
    if not ObtemValorDstAtrelado01Index('VSPaRclCabA', sProcName, OriCodigo,
    DstCodigo) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Sorc,
    DstVMI_Sorc) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Baix,
    DstVMI_Baix) then Exit;
    //
    if not ObtemValorDstAtrelado01Index('VSMovIts', sProcName, OriVMI_Dest,
    DstVMI_Dest) then Exit;
    //
  end;
  //
  if (not Inseriu) and (not Atrelou) then
    DstControle := UMyMod.BPGS1I32(Tabela, 'Controle', '', '', tsPos, stIns, 0);
  //
  if (not Atrelou) then
    AtrelaTabelas_Int(Tabela, 'Controle', OriControle, DstControle,
      AWServerID, AWStatSinc, AtrelouInseriu);
    InsereRegistro_Array(Tabela, Indice, 'Controle', OriControle,
    DstControle, [
    'Codigo', 'VMI_Sorc', 'VMI_Baix', 'VMI_Dest'], [
    DstCodigo, DstVMI_Sorc, DstVMI_Baix, DstVMI_Dest]);
  //
  NeutralizaOrigem(Tabela, ['Controle'], [OriControle]);
end;

function TFmVSLoadCRCCab2.AtrelaTodaTabela_CampoNaoIdx(Tabela, Campo: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.AtrelaTodaTabela_CampoNaoIdx()';
var
  AtrelouInseriu: TERPAtrelIns;
  Indice, Ori_Int, Dst_Int, AWServerID, AWStatSinc: Integer;
  ARI: TArrResIdx;
  MyTab: String;
begin
  Result  := False;
  Indice := ObtemIndiceDeTabela(Tabela, False);
  if Indice >= 0 then
  begin
    ReopenSel(Indice, 'AND MovimTwn <> 0');
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Atrelando Campo ' + Campo + ' da tabela ' + Tabela);
      //
      Ori_Int        := QrSel.FieldByName(Campo).AsInteger;
      if Ori_Int <> 0 then
      begin
        AWServerID     := QrSel.FieldByName('AWServerID').AsInteger;
        AWStatSinc     := QrSel.FieldByName('AWStatSinc').AsInteger;
        AtrelouInseriu := atrinsNoAtrelNoIns;
        MyTab          := Lowercase('VS' + Campo);

        UnDmkDAC_PF.AbreMySQLQuery0(QrPNI1, Dmod.MyDB, [
        'SELECT Dst_' + Campo,
        'FROM ' + LowerCase(CO_ERPSync_TbPrefix + MyTab),
        'WHERE Ori_' + Campo + '=' + Geral.FF0(Ori_Int),
        '']);
        Dst_Int := QrPNI1.FieldByName('Dst_' + Campo).AsInteger;
        //
        if Dst_Int = 0 then
        begin
          Dst_Int := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, Dst_Int);
          AtrelaTabelas_Int(MyTab, Campo, Ori_Int, Dst_Int, AWServerID, AWStatSinc, AtrelouInseriu);
        end;
      end;
      //
      QrSel.Next;
    end;
  end;
  //
  ///////
  //.....
  //////
  // no final.... se tudo der certo!
  Result := True;
  MyObjects.UpdPBOnly(PB1);
end;

function TFmVSLoadCRCCab2.AtrelaTodaTabela_IndicePri01Int(Tabela, Campo: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab.AtrelaTodaTabela_IndicePri01Int()';
var
  AtrelouInseriu: TERPAtrelIns;
  {I,} Indice: Integer;
  {SQLPsqCross, SQLPsqInsertedIts: String;}
  ARI: TArrResIdx;
begin
  Result  := False;
  Indice := ObtemIndiceDeTabela(Tabela, False);
  if Indice >= 0 then
  begin
    ReopenSel(Indice, '');
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Atrelando ' + Tabela);
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, Indice, [], AtrelouInseriu);
      //
      case AtrelouInseriu of
        // Nao atrelou nem inseriu
        TERPAtrelIns.atrinsNoAtrelNoIns,
        // Nao atrelou mas inseriu
        TERPAtrelIns.atrinsNoAtrelButIns:
        begin
          {
          if Tabela = LowerCase(CO_TAB_VMI) then
            ApenasAtrelaVMI(Tabela, Indice, AtrelouInseriu, QrSel, ARI)
          else AdicionaAoMemo(Tabela + ': tabela n�o implementada em ' + sProcName);
          }
          ApenasAtrelaTabelaIdx01Int(Tabela, Campo, Indice, AtrelouInseriu, QrSel, ARI)
        end;
        // Atrelou mas nao inseriu
        TERPAtrelIns.atrinsNoInsButAtrel,
        // Atrelou e inseriu
        TERPAtrelIns.atrinsAtrelAndIns:
        begin
          // Nada
        end;
        // ???
        else MemoAdd_NoAI_Tab(Tabela, AtrelouInseriu, sProcName, ARI);
      end;
      //
      QrSel.Next;
    end;
  end;
  //
  ///////
  //.....
  //////
  // no final.... se tudo der certo!
  Result := True;
  MyObjects.UpdPBOnly(PB1);
end;

function TFmVSLoadCRCCab2.AtrelaTabela(Tabela: String): Boolean;
const
  sProcName = 'FmVSLoadCRCCab2.AtrelaTabela()';
var
  Atrelou, Inseriu: Boolean;
  AtrelouInseriu: TERPAtrelIns;
  QtdFldIdx, I, Indice, LoadCRCCod: Integer;
  SQLPsqCross, SQLPsqInsertedIts: String;
  ARI: TArrResIdx;
  //
  Purpose: TItemTuplePurpose;
  CamposOri, CamposDst: array of String;
  ValoresOri, ValoresDst, T: array of Variant;
  Campo: String;
  VT: TVarType;
  //ContinuaBool: Boolean;
  ContinuaQtde: Integer;
begin
  Result  := True;
  Atrelou := False;
  Inseriu := False;
  Indice := ObtemIndiceDeTabela(Tabela, False);
  if Indice >= 0 then
  begin
    ReopenSel(Indice, '');
    //Geral.MB_SQL(Self, QrSel);
    PB2.Position := 0;
    PB2.Max := QrSel.RecordCount;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDBMQeLnk, Dmod.MyDB, [
    'SELECT * ',
    'FROM dbmqeilnk ',
    'WHERE LOWER(KLAskrTab)=LOWER("' + Tabela + '")',
    '']);
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Atrelando ' + Tabela);
      //
      ARI := ObtemAtrelouInseriu_Indice(Tabela, QrSel, Indice, [], AtrelouInseriu);
      QtdFldIdx := Length(FCamposName[Indice]);
      //
      case AtrelouInseriu of
        TERPAtrelIns.atrinsNoAtrelNoIns,
        TERPAtrelIns.atrinsNoAtrelButIns:
        begin
          SetLength(CamposOri, QtdFldIdx);
          SetLength(CamposDst, QtdFldIdx);
          SetLength(ValoresOri, QtdFldIdx);
          SetLength(ValoresDst, QtdFldIdx);
          ContinuaQtde := 0;
          for I := 0 to QtdFldIdx -1 do
          begin
            //ContinuaBool := False;
            Campo := FCamposName[Indice][I];
            CamposOri[I] := 'Ori_' + Campo;
            CamposDst[I] := 'Dst_' + Campo;
            //
            ValoresOri[I] := QrSel.FieldByName(Campo).AsVariant;
            //ValoresDst[I] := Null;
            (*
            VT := VarType(ValoresOri[I]);
            if VT <> vtInteger then
            begin
              Geral.MB_Erro('Valor difere de integer em ' + sProcName);
              Result := False;
              Exit;
            end;
            *)
            if QrDBMQeLnk.Locate('KLAskrCol', Campo, [loCaseInsensitive]) then
            begin
              if QrDBMQeLnkKLPurpose.Value <> QrDBMQeLnkTbPurpose.Value then
              begin
                // 15 e 01
                if (TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value) =
                TItemTuplePurpose.itpCDRDeleteSync)
                and (TItemTuplePurpose(QrDBMQeLnkTbPurpose.Value) =
                TItemTuplePurpose.itpCDRIncremSync) then
                begin
                  // Nada!. Primary que ser� exclu�do! n�o precisa atrelar!
                end else
                  AdicionaAoMemo(sprocName + ' KLPurpose = ' +
                  Geral.FF0(QrDBMQeLnkKLPurpose.Value) + ' difere de TbPurpose = '
                  + Geral.FF0(QrDBMQeLnkTbPurpose.Value));
              end else
              begin
                //ContinuaBool := True;
                ContinuaQtde := ContinuaQtde + 1;
(*
              end;
              if ContinuaBool then
              begin
*)
                Purpose := TItemTuplePurpose(QrDBMQeLnkKLPurpose.Value);
                if (Purpose in CO_PrimaryIncremenPurpose) then
                  ValoresDst[I] := UMyMod.BPGS1I32(Tabela, FCamposName[Indice][I], '', '', tsPos, stIns, 0)
                else
                  ValoresDst[I] := ValoresOri[I];
              end;
            end;
          end;
          if ContinuaQtde > 0 then
          begin
            //Mais := QtdFldIdx + 4;
            SetLength(CamposOri, QtdFldIdx + 4);
            SetLength(ValoresOri, QtdFldIdx + 4);
            //
            CamposOri[QtdFldIdx + 0] := 'AWServerID';
            ValoresOri[QtdFldIdx + 0] := QrSel.FieldByName('AWServerID').AsInteger;
            //
            CamposOri[QtdFldIdx + 1] := 'AWStatSinc';
            ValoresOri[QtdFldIdx + 1] := QrSel.FieldByName('AWStatSinc').AsInteger;
            //
            CamposOri[QtdFldIdx + 2] := 'AtrelIns';
            ValoresOri[QtdFldIdx + 2] := Integer(AtrelouInseriu);
            //
            CamposOri[QtdFldIdx + 3] := 'LoadCRCCod';
            ValoresOri[QtdFldIdx + 3] := QrVSLoadCRCCabCodigo.Value;
            //
            Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'erpsync_' + LowerCase(Tabela), False,
            CamposOri, CamposDst, ValoresOri, ValoresDst, False);
          end;
        end;
        TERPAtrelIns.atrinsNoInsButAtrel,
        TERPAtrelIns.atrinsAtrelAndIns: ; // Nada
        else
        begin
          Result := False;
          AdicionaAoMemo('Tabela ' + Tabela + ' TERPAtrelIns n�o implementado em ' + sProcName);
        end
      end;
      //
      QrSel.Next;
    end;
  end;
  // no final.... se tudo der certo!
  Result := True;
end;

function  TFmVSLoadCRCCab2.AtrelaTabelas_Codigo(Tabela: String; Ori_Codigo,
  Dst_Codigo, AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
var
  SQLType: TSQLType;
  LoadCRCCod: Integer;
begin
  Result         := False;
  SQLType        := stIns;
  LoadCRCCod     := QrVSLoadCRCCabCodigo.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'erpsync_' + LowerCase(Tabela), False, [
  'Ori_Codigo', 'AWServerID', 'AWStatSinc',
  'AtrelIns', 'LoadCRCCod'], [
  'Dst_Codigo'], [
  Ori_Codigo, AWServerID, AWStatSinc,
  AtrelIns, LoadCRCCod], [
  Dst_Codigo], False);
  if Result then
  begin
    //?
  end;
end;

function  TFmVSLoadCRCCab2.AtrelaTabelas_Int(Tabela, Campo: String; Ori_Int,
  Dst_Int, AWServerID, AWStatSinc: Integer; AtrelIns: TERPAtrelIns): Boolean;
var
  SQLType: TSQLType;
  LoadCRCCod: Integer;
begin
  Result         := False;
  SQLType        := stIns;
  LoadCRCCod     := QrVSLoadCRCCabCodigo.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'erpsync_' + LowerCase(Tabela), False, [
  'Ori_' + Campo, 'AWServerID', 'AWStatSinc',
  'AtrelIns', 'LoadCRCCod'], [
  'Dst_' + Campo], [
  Ori_Int, AWServerID, AWStatSinc,
  AtrelIns, LoadCRCCod], [
  Dst_Int], False);
  if Result then
  begin
    //?
  end;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabAfterOpen(DataSet: TDataSet);
begin
  if QrItensPorTab.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabAfterScroll(DataSet: TDataSet);
begin
  QrSel.Close;
end;

procedure TFmVSLoadCRCCab2.QrItensPorTabBeforeClose(DataSet: TDataSet);
begin
  QrSel.Close;
end;

procedure TFmVSLoadCRCCab2.QrItReInnAfterOpen(DataSet: TDataSet);
begin
  DBGReInn.Visible := True;
end;

procedure TFmVSLoadCRCCab2.QrItReInnBeforeClose(DataSet: TDataSet);
begin
  DBGReInn.Visible := False;
end;

procedure TFmVSLoadCRCCab2.QrSelAfterScroll(DataSet: TDataSet);
begin
  QrItReInn.Close;
end;

procedure TFmVSLoadCRCCab2.QrSelBeforeClose(DataSet: TDataSet);
begin
  QrItReInn.Close;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSLoadCRCTbs(0);
  ReopenVSLoadCRCWrn(0);
end;

procedure TFmVSLoadCRCCab2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSLoadCRCCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSLoadCRCCab2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSLoadCRCCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsloadcrccab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSLoadCRCCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSLoadCRCCab2.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
  ServerID: Integer;
  vSrvrID: Variant;
begin
  //FCodOnImport := 0;
  ServerID := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, ServerID, 0, 0,
  '1', '', True, 'Server ID', 'Informe o ID do Servidor ClareCo: ', 0, vSrvrID)
  then begin
    ServerID := vSrvrID;
    if ServerID > 0 then
    begin
      if PesquisaNovosDadosAImportar(ServerID) then
      begin
        UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSLoadCRCCab, [PnDados],
        [PnEdita], TPData, ImgTipo, 'vsloadcrccab');
        //
        EdOriServrID.ValueVariant := ServerID;
        Agora := DModG.ObtemAgora();
        TPData.Date         := Agora;
        EdHora.ValueVariant := Agora;
      end;
    end;
  end;
end;

procedure TFmVSLoadCRCCab2.Corrigir011Click(Sender: TObject);
var
  Tabela: String;
  Qry: TmySQLQuery;
begin
  Tabela := '...';
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Verificando altera��es na tabela' + Tabela);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW TABLES LIKE "erpsync_%"',
    '']);
    PB2.Position := 0;
    PB2.Max      := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      Tabela := Qry.Fields[0].AsString;
      MyObjects.Informa2EUpdPB(PB2, LaAviso1, LaAviso2, True,
        'Verificando altera��es na tabela' + Tabela);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + Tabela,
      'SET LoadCRCCod=1 ',
      'WHERE LoadCRCCod=0 ',
      '']);
      //
      InsUpdVSLoadCRCTbs(Tabela);
      //
      Qry.Next;
    end;
  except
    Qry.Free;
  end;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabBeforeClose(
  DataSet: TDataSet);
begin
  QrItensPorTab.Close;
  QrVSLoadCRCTbs.Close;
  QrVSLoadCRCWrn.Close;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSLoadCRCCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSLoadCRCCab2.QrVSLoadCRCWrnAfterOpen(DataSet: TDataSet);
begin
  if (QrVSLoadCRCWrn.RecordCount > 0)
  and (
    (QrItensPorTab.State = dsInactive)
    or
    (QrItensPorTab.RecordCount = 0)
  ) then
    PageControl1.ActivePageIndex := 2;
end;

function TFmVSLoadCRCCab2.QtdIndicesARI(ARI: TArrResIdx): Integer;
begin
  Result := Integer(ARI[0]);
end;

(*
1. Desmarcar quanto pronto! "AtrelaTabela()"
2. vsentimp.MPVImpOpc: 3 = 0 itpCDRRelatnSync
3. vsesccab.MovimCod: 3 = 344 itpCDRRelatnSync
4. vsmulfrncab.MovTypeCod: 3 = 439 itpCDRRelatnSync
5. vsmulfrnits.Codigo: 3 = 4 itpCDRRelatnSync
6. vsmovits.DstNivel2: 3 = 0 itpCDRRelatnSync
7. vsopecab.CacCod: 3 = 0 itpCDRRelatnSync
8. vsoutcab.MovimCod: 3 = 441 itpCDRRelatnSync
9. vspalleta.GerRclCab: 3 = 0 itpCDRRelatnSync
10. vspwecab.CacCod: 3 = 0 itpCDRRelatnSync
11. vsgerrcla.CacCod: 3 = 111 itpCDRRelatnSync
12. vspaclaitsa.Codigo: 3 = 10 itpCDRRelatnSync
13. vsparclcaba.CacCod: 3 = 111 itpCDRRelatnSync
14. vsparclitsa.Codigo: 3 = 97 itpCDRRelatnSync
*)

end.

