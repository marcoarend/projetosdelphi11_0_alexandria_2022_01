unit VSCPMRSBIt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, AppListas;

type
  TFmVSCPMRSBIt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    DsGraGruX: TDataSource;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdDivKgSPpcCou: TdmkEditCB;
    Label4: TLabel;
    EdPercKgSPKgCou: TdmkEditCB;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSCPMRSBIt(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSCPMRSBIt: TFmVSCPMRSBIt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSCPMRSBIt.BtOKClick(Sender: TObject);
var
  Codigo, Controle, GraGruX: Integer;
  DivKgSPpcCou, PercKgSPKgCou: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DivKgSPpcCou   := EdDivKgSPpcCou.ValueVariant;
  PercKgSPKgCou  := EdPercKgSPKgCou.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('vscpmrsbit', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscpmrsbit', False, [
  'Codigo', 'GraGruX', 'DivKgSPpcCou',
  'PercKgSPKgCou'], [
  'Controle'], [
  Codigo, GraGruX, DivKgSPpcCou,
  PercKgSPKgCou], [
  Controle], True) then
  begin
    ReopenVSCPMRSBIt(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType              := stIns;
      EdControle.ValueVariant      := 0;
      EdGraGruX.ValueVariant       := 0;
      CBGraGruX.KeyValue           := Null;
      EdPercKgSPKgCou.ValueVariant := 0.000;
      EdDivKgSPpcCou.ValueVariant  := 0.0000;
      //
      EdGraGruX.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSCPMRSBIt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCPMRSBIt.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSCPMRSBIt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0512_VSSubPrd));
end;

procedure TFmVSCPMRSBIt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCPMRSBIt.ReopenVSCPMRSBIt(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
