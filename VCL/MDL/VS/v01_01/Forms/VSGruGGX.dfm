object FmVSGruGGX: TFmVSGruGGX
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-154 :: Grupos de Artigos VS'
  ClientHeight = 420
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 273
    Width = 617
    Height = 33
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 215
    ExplicitHeight = 28
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 225
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 96
      Top = 16
      Width = 28
      Height = 13
      Caption = 'Nome'
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 96
      Height = 13
      Caption = 'Integral sem laminar:'
    end
    object SbGGXIntegrl: TSpeedButton
      Left = 584
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXIntegrlClick
    end
    object Label2: TLabel
      Left = 12
      Top = 96
      Width = 138
      Height = 13
      Caption = 'Laminado (Integral laminado):'
    end
    object SbGGXLaminad: TSpeedButton
      Left = 584
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXLaminadClick
    end
    object Label3: TLabel
      Left = 12
      Top = 136
      Width = 81
      Height = 13
      Caption = 'Dividido em tripa:'
    end
    object SbGGXDivTrip: TSpeedButton
      Left = 584
      Top = 152
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXDivTripClick
    end
    object Label4: TLabel
      Left = 12
      Top = 176
      Width = 102
      Height = 13
      Caption = 'Dividido ap'#243's curtido:'
    end
    object SbGGXDivCurt: TSpeedButton
      Left = 584
      Top = 192
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXDivCurtClick
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 96
      Top = 32
      Width = 509
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBGGXIntegrl: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 513
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXIntegrl
      TabOrder = 3
      dmkEditCB = EdGGXIntegrl
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGGXIntegrl: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXIntegrl
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdGGXLaminad: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXLaminad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXLaminad: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 513
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXLaminad
      TabOrder = 5
      dmkEditCB = EdGGXLaminad
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGGXDivTrip: TdmkEditCB
      Left = 12
      Top = 152
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXDivTrip
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXDivTrip: TdmkDBLookupComboBox
      Left = 68
      Top = 152
      Width = 513
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXDivTrip
      TabOrder = 7
      dmkEditCB = EdGGXDivTrip
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGGXDivCurt: TdmkEditCB
      Left = 12
      Top = 192
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXDivCurt
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXDivCurt: TdmkDBLookupComboBox
      Left = 68
      Top = 192
      Width = 513
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXDivCurt
      TabOrder = 9
      dmkEditCB = EdGGXDivCurt
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 262
        Height = 32
        Caption = 'Grupos de Artigos VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 262
        Height = 32
        Caption = 'Grupos de Artigos VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 262
        Height = 32
        Caption = 'Grupos de Artigos VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 306
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 243
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 350
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 287
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGGXIntegrl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 160
    Top = 56
    object QrGGXIntegrlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXIntegrlNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
  end
  object DsGGXIntegrl: TDataSource
    DataSet = QrGGXIntegrl
    Left = 160
    Top = 104
  end
  object QrGGXLaminad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 240
    Top = 56
    object QrGGXLaminadControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXLaminadNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
  end
  object DsGGXLaminad: TDataSource
    DataSet = QrGGXLaminad
    Left = 240
    Top = 104
  end
  object QrGGXDivTrip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 324
    Top = 56
    object QrGGXDivTripControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDivTripNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
  end
  object DsGGXDivTrip: TDataSource
    DataSet = QrGGXDivTrip
    Left = 324
    Top = 104
  end
  object QrGGXDivCurt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 400
    Top = 60
    object QrGGXDivCurtControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDivCurtNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
  end
  object DsGGXDivCurt: TDataSource
    DataSet = QrGGXDivCurt
    Left = 400
    Top = 108
  end
end
