unit VSImpOrdem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  mySQLDbTables, Data.DB,
  UnDmkEnums, DmkGeral, frxClass, frxDBSet, AppListas, UnGrl_Consts,
  UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSImpOrdem = class(TForm)
    QrVSOrdem: TmySQLQuery;
    QrVSOrdemNO_VSCOPCab: TWideStringField;
    QrVSOrdemPecasINI: TFloatField;
    QrVSOrdemAreaINIM2: TFloatField;
    QrVSOrdemAreaINIP2: TFloatField;
    QrVSOrdemPesoKgINI: TFloatField;
    QrVSOrdemNO_EMPRESA: TWideStringField;
    QrVSOrdemNO_Cliente: TWideStringField;
    QrVSOrdemNO_Operacoes: TWideStringField;
    QrVSOrdemNO_EmitGru: TWideStringField;
    QrVSOrdemCodigo: TIntegerField;
    QrVSOrdemMovimCod: TIntegerField;
    QrVSOrdemCacCod: TIntegerField;
    QrVSOrdemGraGruX: TIntegerField;
    QrVSOrdemNome: TWideStringField;
    QrVSOrdemEmpresa: TIntegerField;
    QrVSOrdemDtHrAberto: TDateTimeField;
    QrVSOrdemDtHrLibOpe: TDateTimeField;
    QrVSOrdemDtHrCfgOpe: TDateTimeField;
    QrVSOrdemDtHrFimOpe: TDateTimeField;
    QrVSOrdemPesoKgMan: TFloatField;
    QrVSOrdemPecasMan: TFloatField;
    QrVSOrdemAreaManM2: TFloatField;
    QrVSOrdemAreaManP2: TFloatField;
    QrVSOrdemValorTMan: TFloatField;
    QrVSOrdemPesoKgSrc: TFloatField;
    QrVSOrdemPecasSrc: TFloatField;
    QrVSOrdemAreaSrcM2: TFloatField;
    QrVSOrdemAreaSrcP2: TFloatField;
    QrVSOrdemValorTSrc: TFloatField;
    QrVSOrdemPesoKgDst: TFloatField;
    QrVSOrdemPecasDst: TFloatField;
    QrVSOrdemAreaDstM2: TFloatField;
    QrVSOrdemAreaDstP2: TFloatField;
    QrVSOrdemValorTDst: TFloatField;
    QrVSOrdemPesoKgBxa: TFloatField;
    QrVSOrdemPecasBxa: TFloatField;
    QrVSOrdemAreaBxaM2: TFloatField;
    QrVSOrdemAreaBxaP2: TFloatField;
    QrVSOrdemValorTBxa: TFloatField;
    QrVSOrdemPesoKgSdo: TFloatField;
    QrVSOrdemPecasSdo: TFloatField;
    QrVSOrdemAreaSdoM2: TFloatField;
    QrVSOrdemAreaSdoP2: TFloatField;
    QrVSOrdemValorTSdo: TFloatField;
    QrVSOrdemTipoArea: TSmallintField;
    QrVSOrdemCustoManMOKg: TFloatField;
    QrVSOrdemCustoManMOTot: TFloatField;
    QrVSOrdemValorManMP: TFloatField;
    QrVSOrdemValorManT: TFloatField;
    QrVSOrdemCliente: TIntegerField;
    QrVSOrdemNFeRem: TIntegerField;
    QrVSOrdemLPFMO: TWideStringField;
    QrVSOrdemTemIMEIMrt: TSmallintField;
    QrVSOrdemKgCouPQ: TFloatField;
    QrVSOrdemGGXDst: TIntegerField;
    QrVSOrdemNFeStatus: TIntegerField;
    QrVSOrdemGGXSrc: TIntegerField;
    QrVSOrdemSerieRem: TIntegerField;
    QrVSOrdemOperacoes: TIntegerField;
    QrVSOrdemVSCOPCab: TIntegerField;
    QrVSOrdemEmitGru: TIntegerField;
    QrVSOri: TmySQLQuery;
    QrVSOriCodigo: TLargeintField;
    QrVSOriControle: TLargeintField;
    QrVSOriMovimCod: TLargeintField;
    QrVSOriMovimNiv: TLargeintField;
    QrVSOriMovimTwn: TLargeintField;
    QrVSOriEmpresa: TLargeintField;
    QrVSOriTerceiro: TLargeintField;
    QrVSOriCliVenda: TLargeintField;
    QrVSOriMovimID: TLargeintField;
    QrVSOriDataHora: TDateTimeField;
    QrVSOriPallet: TLargeintField;
    QrVSOriGraGruX: TLargeintField;
    QrVSOriPecas: TFloatField;
    QrVSOriPesoKg: TFloatField;
    QrVSOriAreaM2: TFloatField;
    QrVSOriAreaP2: TFloatField;
    QrVSOriValorT: TFloatField;
    QrVSOriSrcMovID: TLargeintField;
    QrVSOriSrcNivel1: TLargeintField;
    QrVSOriSrcNivel2: TLargeintField;
    QrVSOriSrcGGX: TLargeintField;
    QrVSOriSdoVrtPeca: TFloatField;
    QrVSOriSdoVrtPeso: TFloatField;
    QrVSOriSdoVrtArM2: TFloatField;
    QrVSOriObserv: TWideStringField;
    QrVSOriSerieFch: TLargeintField;
    QrVSOriFicha: TLargeintField;
    QrVSOriMisturou: TLargeintField;
    QrVSOriFornecMO: TLargeintField;
    QrVSOriCustoMOKg: TFloatField;
    QrVSOriCustoMOM2: TFloatField;
    QrVSOriCustoMOTot: TFloatField;
    QrVSOriValorMP: TFloatField;
    QrVSOriDstMovID: TLargeintField;
    QrVSOriDstNivel1: TLargeintField;
    QrVSOriDstNivel2: TLargeintField;
    QrVSOriDstGGX: TLargeintField;
    QrVSOriQtdGerPeca: TFloatField;
    QrVSOriQtdGerPeso: TFloatField;
    QrVSOriQtdGerArM2: TFloatField;
    QrVSOriQtdGerArP2: TFloatField;
    QrVSOriQtdAntPeca: TFloatField;
    QrVSOriQtdAntPeso: TFloatField;
    QrVSOriQtdAntArM2: TFloatField;
    QrVSOriQtdAntArP2: TFloatField;
    QrVSOriNotaMPAG: TFloatField;
    QrVSOriNO_PALLET: TWideStringField;
    QrVSOriNO_PRD_TAM_COR: TWideStringField;
    QrVSOriNO_TTW: TWideStringField;
    QrVSOriID_TTW: TLargeintField;
    QrVSOriNO_FORNECE: TWideStringField;
    QrVSOriNO_SerieFch: TWideStringField;
    QrVSOriReqMovEstq: TLargeintField;
    QrVSDst: TmySQLQuery;
    QrVSDstCodigo: TLargeintField;
    QrVSDstControle: TLargeintField;
    QrVSDstMovimCod: TLargeintField;
    QrVSDstMovimNiv: TLargeintField;
    QrVSDstMovimTwn: TLargeintField;
    QrVSDstEmpresa: TLargeintField;
    QrVSDstTerceiro: TLargeintField;
    QrVSDstCliVenda: TLargeintField;
    QrVSDstMovimID: TLargeintField;
    QrVSDstDataHora: TDateTimeField;
    QrVSDstPallet: TLargeintField;
    QrVSDstGraGruX: TLargeintField;
    QrVSDstPecas: TFloatField;
    QrVSDstPesoKg: TFloatField;
    QrVSDstAreaM2: TFloatField;
    QrVSDstAreaP2: TFloatField;
    QrVSDstValorT: TFloatField;
    QrVSDstSrcMovID: TLargeintField;
    QrVSDstSrcNivel1: TLargeintField;
    QrVSDstSrcNivel2: TLargeintField;
    QrVSDstSrcGGX: TLargeintField;
    QrVSDstSdoVrtPeca: TFloatField;
    QrVSDstSdoVrtPeso: TFloatField;
    QrVSDstSdoVrtArM2: TFloatField;
    QrVSDstObserv: TWideStringField;
    QrVSDstSerieFch: TLargeintField;
    QrVSDstFicha: TLargeintField;
    QrVSDstMisturou: TLargeintField;
    QrVSDstFornecMO: TLargeintField;
    QrVSDstCustoMOKg: TFloatField;
    QrVSDstCustoMOM2: TFloatField;
    QrVSDstCustoMOTot: TFloatField;
    QrVSDstValorMP: TFloatField;
    QrVSDstDstMovID: TLargeintField;
    QrVSDstDstNivel1: TLargeintField;
    QrVSDstDstNivel2: TLargeintField;
    QrVSDstDstGGX: TLargeintField;
    QrVSDstQtdGerPeca: TFloatField;
    QrVSDstQtdGerPeso: TFloatField;
    QrVSDstQtdGerArM2: TFloatField;
    QrVSDstQtdGerArP2: TFloatField;
    QrVSDstQtdAntPeca: TFloatField;
    QrVSDstQtdAntPeso: TFloatField;
    QrVSDstQtdAntArM2: TFloatField;
    QrVSDstQtdAntArP2: TFloatField;
    QrVSDstNotaMPAG: TFloatField;
    QrVSDstNO_PALLET: TWideStringField;
    QrVSDstNO_PRD_TAM_COR: TWideStringField;
    QrVSDstNO_TTW: TWideStringField;
    QrVSDstID_TTW: TLargeintField;
    QrVSDstNO_FORNECE: TWideStringField;
    QrVSDstNO_SerieFch: TWideStringField;
    QrVSDstReqMovEstq: TLargeintField;
    QrVSDstPedItsFin: TLargeintField;
    QrVSDstMarca: TWideStringField;
    QrVSDstStqCenLoc: TLargeintField;
    QrVSDstNO_LOC_CEN: TWideStringField;
    frxWET_CURTI_183_01: TfrxReport;
    frxDsVSOrdem: TfrxDBDataset;
    frxDsVSOri: TfrxDBDataset;
    frxDsVSDst: TfrxDBDataset;
    QrVSOrdemNO_ACAO_COURO: TWideStringField;
    QrVSAtu: TmySQLQuery;
    QrVSAtuCodigo: TLargeintField;
    QrVSAtuControle: TLargeintField;
    QrVSAtuMovimCod: TLargeintField;
    QrVSAtuMovimNiv: TLargeintField;
    QrVSAtuMovimTwn: TLargeintField;
    QrVSAtuEmpresa: TLargeintField;
    QrVSAtuTerceiro: TLargeintField;
    QrVSAtuCliVenda: TLargeintField;
    QrVSAtuMovimID: TLargeintField;
    QrVSAtuDataHora: TDateTimeField;
    QrVSAtuPallet: TLargeintField;
    QrVSAtuGraGruX: TLargeintField;
    QrVSAtuPecas: TFloatField;
    QrVSAtuPesoKg: TFloatField;
    QrVSAtuAreaM2: TFloatField;
    QrVSAtuAreaP2: TFloatField;
    QrVSAtuValorT: TFloatField;
    QrVSAtuSrcMovID: TLargeintField;
    QrVSAtuSrcNivel1: TLargeintField;
    QrVSAtuSrcNivel2: TLargeintField;
    QrVSAtuSrcGGX: TLargeintField;
    QrVSAtuSdoVrtPeca: TFloatField;
    QrVSAtuSdoVrtPeso: TFloatField;
    QrVSAtuSdoVrtArM2: TFloatField;
    QrVSAtuObserv: TWideStringField;
    QrVSAtuSerieFch: TLargeintField;
    QrVSAtuFicha: TLargeintField;
    QrVSAtuMisturou: TLargeintField;
    QrVSAtuFornecMO: TLargeintField;
    QrVSAtuCustoMOKg: TFloatField;
    QrVSAtuCustoMOM2: TFloatField;
    QrVSAtuCustoMOTot: TFloatField;
    QrVSAtuValorMP: TFloatField;
    QrVSAtuDstMovID: TLargeintField;
    QrVSAtuDstNivel1: TLargeintField;
    QrVSAtuDstNivel2: TLargeintField;
    QrVSAtuDstGGX: TLargeintField;
    QrVSAtuQtdGerPeca: TFloatField;
    QrVSAtuQtdGerPeso: TFloatField;
    QrVSAtuQtdGerArM2: TFloatField;
    QrVSAtuQtdGerArP2: TFloatField;
    QrVSAtuQtdAntPeca: TFloatField;
    QrVSAtuQtdAntPeso: TFloatField;
    QrVSAtuQtdAntArM2: TFloatField;
    QrVSAtuQtdAntArP2: TFloatField;
    QrVSAtuNotaMPAG: TFloatField;
    QrVSAtuNO_PALLET: TWideStringField;
    QrVSAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSAtuNO_TTW: TWideStringField;
    QrVSAtuID_TTW: TLargeintField;
    QrVSAtuNO_FORNECE: TWideStringField;
    QrVSAtuReqMovEstq: TLargeintField;
    QrVSAtuCUSTO_M2: TFloatField;
    QrVSAtuCUSTO_P2: TFloatField;
    QrVSAtuNO_LOC_CEN: TWideStringField;
    QrVSAtuMarca: TWideStringField;
    QrVSAtuPedItsLib: TLargeintField;
    QrVSAtuStqCenLoc: TLargeintField;
    QrVSAtuNO_FICHA: TWideStringField;
    QrVSAtuNO_FORNEC_MO: TWideStringField;
    QrVSAtuClientMO: TLargeintField;
    QrVSAtuCustoPQ: TFloatField;
    frxDsVSAtu: TfrxDBDataset;
    procedure frxWET_CURTI_183_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSOrdemCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenOri();
    procedure ReopenDst();
    procedure ReopenAtu();
  public
    { Public declarations }
    FEstqMovimID: TEstqMovimID;
    FEmpresa, FCodigo, FMovimCod: Integer;
    //
    procedure ImprimeOrdemPreenchida();
  end;

var
  FmVSImpOrdem: TFmVSImpOrdem;

implementation

{$R *.dfm}

uses Module, DmkDAC_PF, UnVS_CRC_PF, ModuleGeral, UnMyObjects;

{ TFmVSImpOrdem }

procedure TFmVSImpOrdem.frxWET_CURTI_183_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
end;

procedure TFmVSImpOrdem.ImprimeOrdemPreenchida();
var
  TabCab: String;
begin
  TabCab := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(FEstqMovimID);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOrdem, Dmod.MyDB, [
  'SELECT vcc.Nome NO_VSCOPCab,  ',
  'IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI, ',
  'IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2, ',
  'IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcP2) AreaINIP2, ',
  'IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente,  ',
  'ope.Nome NO_Operacoes, egr.Nome NO_EmitGru,  ',
  'voc.* ',
  'FROM ' + TabCab + ' voc ',
  'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa ',
  'LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente ',
  'LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab ',
  'LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru ',
  'LEFT JOIN operacoes ope ON ope.Codigo=voc.Operacoes ',
  'WHERE voc.MovimCod=' + Geral.FF0(FMovimCod),
  '']);
  //
  ReopenAtu();
  ReopenOri();
  ReopenDst();
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_183_01, [
    DModG.frxDsDono,
    frxDsVSOrdem,
    frxDsVSAtu,
    frxDsVSOri,
    frxDsVSDst
  ]);
  MyObjects.frxMostra(frxWET_CURTI_183_01, 'Ordem de Produ��o Preenchida');
end;

procedure TFmVSImpOrdem.QrVSOrdemCalcFields(DataSet: TDataSet);
begin
  QrVSOrdemNO_ACAO_COURO.Value := sEstqMovimID[Integer(FEstqMovimID)];
end;

procedure TFmVSImpOrdem.ReopenAtu();
var
  MovimNiv: TEstqMovimNiv;
begin
  MovimNiv := VS_CRC_PF.ObtemMovimNivInnDeMovimID(FEstqMovimID);
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSAtu, QrVSOrdemMovimCod.Value, 0,
  QrVSOrdemTemIMEIMrt.Value, MovimNiv);
end;

procedure TFmVSImpOrdem.ReopenDst();
var
  MovimNiv: TEstqMovimNiv;
begin
  MovimNiv := VS_CRC_PF.ObtemMovimNivDstDeMovimID(FEstqMovimID);
  //
  VS_CRC_PF.ReopenVSOpePrcDst(QrVSDst, QrVSOrdemMovimCod.Value, 0,
  QrVSOrdemTemIMEIMrt.Value, MovimNiv);
end;

procedure TFmVSImpOrdem.ReopenOri();
//procedure TUnVS_CRC_PF.ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI, QrVSXxxOriPallet:
  //TmySQLQuery; Controle, Pallet: Integer; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
  MovimNiv: TEstqMovimNiv;
begin
  MovimNiv := VS_CRC_PF.ObtemMovimNivSrcDeMovimID(FEstqMovimID);
  //
  TemIMEIMrt := QrVSOrdemTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOrdemMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),  // eminSorc...
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOri, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
end;

end.
