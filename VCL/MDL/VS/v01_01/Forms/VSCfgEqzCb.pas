unit VSCfgEqzCb;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Variants;

type
  TFmVSCfgEqzCb = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSCfgEqzCb: TmySQLQuery;
    DsVSCfgEqzCb: TDataSource;
    QrVSCfgEqzIt: TmySQLQuery;
    DsVSCfgEqzIt: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    Label4: TLabel;
    EdBasNotZero: TdmkEdit;
    Label3: TLabel;
    QrVSCfgEqzCbCodigo: TIntegerField;
    QrVSCfgEqzCbNome: TWideStringField;
    QrVSCfgEqzCbBasNotZero: TFloatField;
    QrVSCfgEqzCbLk: TIntegerField;
    QrVSCfgEqzCbDataCad: TDateField;
    QrVSCfgEqzCbDataAlt: TDateField;
    QrVSCfgEqzCbUserCad: TIntegerField;
    QrVSCfgEqzCbUserAlt: TIntegerField;
    QrVSCfgEqzCbAlterWeb: TSmallintField;
    QrVSCfgEqzCbAtivo: TSmallintField;
    QrVSCfgEqzItCodigo: TIntegerField;
    QrVSCfgEqzItControle: TIntegerField;
    QrVSCfgEqzItSubClass: TWideStringField;
    QrVSCfgEqzItGraGruX: TIntegerField;
    QrVSCfgEqzItBasNota: TFloatField;
    QrVSCfgEqzItFatorEqz: TFloatField;
    QrVSCfgEqzItLk: TIntegerField;
    QrVSCfgEqzItDataCad: TDateField;
    QrVSCfgEqzItDataAlt: TDateField;
    QrVSCfgEqzItUserCad: TIntegerField;
    QrVSCfgEqzItUserAlt: TIntegerField;
    QrVSCfgEqzItAlterWeb: TSmallintField;
    QrVSCfgEqzItAtivo: TSmallintField;
    DBEdit1: TDBEdit;
    QrVSCfgEqzItNO_PRD_TAM_COR: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCfgEqzCbAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCfgEqzCbBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCfgEqzCbAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSCfgEqzCbBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSCfgEqzIt(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSCfgEqzIt(Controle: Integer);

  end;

var
  FmVSCfgEqzCb: TFmVSCfgEqzCb;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSCfgEqzIt, UnVS_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSCfgEqzCb.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCfgEqzCb.MostraFormVSCfgEqzIt(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrVSCfgEqzCbCodigo.Value;
  if DBCheck.CriaFm(TFmVSCfgEqzIt, FmVSCfgEqzIt, afmoNegarComAviso) then
  begin
    FmVSCfgEqzIt.ImgTipo.SQLType := SQLType;
    FmVSCfgEqzIt.FQrCab := QrVSCfgEqzCb;
    FmVSCfgEqzIt.FDsCab := DsVSCfgEqzCb;
    FmVSCfgEqzIt.FQrIts := QrVSCfgEqzIt;
    if SQLType = stIns then
      //
    else
    begin
      FmVSCfgEqzIt.EdControle.ValueVariant := QrVSCfgEqzItControle.Value;
      //
      FmVSCfgEqzIt.EdGraGruX.ValueVariant   := QrVSCfgEqzItGraGruX.Value;
      FmVSCfgEqzIt.CBGraGruX.KeyValue       := QrVSCfgEqzItGraGruX.Value;
      FmVSCfgEqzIt.EdSubClass.ValueVariant  := QrVSCfgEqzItSubClass.Value;
      FmVSCfgEqzIt.EdBasNota.ValueVariant   := QrVSCfgEqzItBasNota.Value;
      FmVSCfgEqzIt.EdFatorEqz.ValueVariant  := QrVSCfgEqzItFatorEqz.Value;
    end;
    FmVSCfgEqzIt.ShowModal;
    VS_PF.AtualizaNotaVSCfgEqzCb(Codigo);
    LocCod(Codigo, Codigo);
    FmVSCfgEqzIt.Destroy;
  end;
end;

procedure TFmVSCfgEqzCb.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCfgEqzCb);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSCfgEqzCb, QrVSCfgEqzIt);
end;

procedure TFmVSCfgEqzCb.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSCfgEqzCb);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSCfgEqzIt);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSCfgEqzIt);
end;

procedure TFmVSCfgEqzCb.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCfgEqzCbCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCfgEqzCb.DefParams;
begin
  VAR_GOTOTABELA := 'vscfgeqzcb';
  VAR_GOTOMYSQLTABLE := QrVSCfgEqzCb;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vscfgeqzcb');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSCfgEqzCb.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSCfgEqzIt(stUpd);
end;

procedure TFmVSCfgEqzCb.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSCfgEqzCb.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSCfgEqzCb.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCfgEqzCb.ItsExclui1Click(Sender: TObject);
var
  Controle, Codigo: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vscfgeqzit', 'Controle', QrVSCfgEqzItControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := QrVSCfgEqzCbCodigo.Value;
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSCfgEqzIt,
      QrVSCfgEqzItControle, QrVSCfgEqzItControle.Value);
    VS_PF.AtualizaNotaVSCfgEqzCb(Codigo);
    LocCod(Codigo, Codigo);
    ReopenVSCfgEqzIt(Controle);
  end;
end;

procedure TFmVSCfgEqzCb.ReopenVSCfgEqzIt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCfgEqzIt, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome,       ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, cei.* ',
  'FROM vscfgeqzit cei ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cei.GraGruX       ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC       ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad       ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI       ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE cei.Codigo=' + Geral.FF0(QrVSCfgEqzCbCodigo.Value),
  '']);
  //
  QrVSCfgEqzIt.Locate('Controle', Controle, []);
end;


procedure TFmVSCfgEqzCb.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCfgEqzCb.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCfgEqzCb.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCfgEqzCb.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCfgEqzCb.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCfgEqzCb.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEqzCb.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCfgEqzCbCodigo.Value;
  Close;
end;

procedure TFmVSCfgEqzCb.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSCfgEqzIt(stIns);
end;

procedure TFmVSCfgEqzCb.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCfgEqzCb, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscfgeqzcb');
end;

procedure TFmVSCfgEqzCb.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  BasNotZero: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  BasNotZero     := EdBasNotZero.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('vscfgeqzcb', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vscfgeqzcb', False, [
  'Nome', 'BasNotZero'], [
  'Codigo'], [
  Nome, BasNotZero], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSCfgEqzCb.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscfgeqzcb', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscfgeqzcb', 'Codigo');
end;

procedure TFmVSCfgEqzCb.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSCfgEqzCb.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCfgEqzCb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSCfgEqzCb.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCfgEqzCbCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCfgEqzCb.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCfgEqzCb.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSCfgEqzCbCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCfgEqzCb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCfgEqzCb.QrVSCfgEqzCbAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCfgEqzCb.QrVSCfgEqzCbAfterScroll(DataSet: TDataSet);
begin
  ReopenVSCfgEqzIt(0);
end;

procedure TFmVSCfgEqzCb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCfgEqzCbCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCfgEqzCb.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSCfgEqzCbCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vscfgeqzcb', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSCfgEqzCb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCfgEqzCb.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCfgEqzCb, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscfgeqzcb');
end;

procedure TFmVSCfgEqzCb.QrVSCfgEqzCbBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCfgEqzIt.Close;
end;

procedure TFmVSCfgEqzCb.QrVSCfgEqzCbBeforeOpen(DataSet: TDataSet);
begin
  QrVSCfgEqzCbCodigo.DisplayFormat := FFormatFloat;
end;

end.

