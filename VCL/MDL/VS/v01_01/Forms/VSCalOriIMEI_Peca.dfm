object FmVSCalOriIMEI_Peca: TFmVSCalOriIMEI_Peca
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-267 :: Origem de Artigo em Processo de Caleiro - Pe'#231'as'
  ClientHeight = 612
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 473
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 4
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label14: TLabel
      Left = 608
      Top = 20
      Width = 51
      Height = 13
      Caption = 'ID Movim.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label1: TLabel
      Left = 676
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID Gera'#231#227'o:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label17: TLabel
      Left = 744
      Top = 20
      Width = 61
      Height = 13
      Caption = 'ID It.Gerado:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 540
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Red. It.Ger.:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdSrcNivel2: TdmkEdit
      Left = 744
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcNivel1: TdmkEdit
      Left = 676
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcMovID: TdmkEdit
      Left = 608
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcGGX: TdmkEdit
      Left = 540
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo em Processo de Caleiro - Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo em Processo de Caleiro - Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo em Processo de Caleiro - Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 498
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 292
        Height = 17
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 542
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 7
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 271
    Align = alClient
    Caption = ' Filtros: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaGraGruX: TLabel
        Left = 164
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
      end
      object LaTerceiro: TLabel
        Left = 680
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object LaFicha: TLabel
        Left = 608
        Top = 0
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
        Enabled = False
      end
      object Label11: TLabel
        Left = 468
        Top = 0
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Ficha RMP:'
        Enabled = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 164
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 220
        Top = 16
        Width = 241
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 4
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTerceiro: TdmkEditCB
        Left = 680
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 736
        Top = 16
        Width = 133
        Height = 21
        DropDownRows = 2
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 6
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFicha: TdmkEdit
        Left = 608
        Top = 16
        Width = 68
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFichaChange
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 876
        Top = 2
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object EdSerieFch: TdmkEditCB
        Left = 468
        Top = 16
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSerieFchChange
        DBLookupComboBox = CBSerieFch
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSerieFch: TdmkDBLookupComboBox
        Left = 508
        Top = 16
        Width = 97
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSSerFch
        TabOrder = 1
        dmkEditCB = EdSerieFch
        QryCampo = 'SerieFch'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkSoClientMO: TCheckBox
        Left = 4
        Top = 16
        Width = 157
        Height = 17
        Caption = 'Somente do cliente de MO.'
        Checked = True
        State = cbChecked
        TabOrder = 8
        OnClick = EdGraGruXRedefinido
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 2
      Top = 57
      Width = 1004
      Height = 212
      Align = alClient
      DataSource = DsEstoque
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnAfterMultiselect = DBG04EstqAfterMultiselect
      OnDblClick = DBG04EstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'IMEI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 161
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdGGX'
          Title.Caption = 'Ordem'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo Pe'#231'as'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo Peso kg'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Width = 43
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do terceiro'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Total kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end>
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 413
    Width = 1008
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 60
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object LaPecas: TLabel
        Left = 96
        Top = 16
        Width = 54
        Height = 13
        Caption = 'Pe'#231'as: [F4]'
      end
      object LaPesoKg: TLabel
        Left = 172
        Top = 16
        Width = 68
        Height = 13
        Caption = 'Peso kg ful'#227'o:'
      end
      object Label9: TLabel
        Left = 248
        Top = 16
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object LaAreaM2: TLabel
        Left = 720
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 788
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label3: TLabel
        Left = 644
        Top = 17
        Width = 63
        Height = 13
        Caption = 'Peso kg Bxa:'
        Enabled = False
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 96
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPecasChange
        OnKeyDown = EdPecasKeyDown
      end
      object EdQtdAntPeso: TdmkEdit
        Left = 172
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'QtdGerPeso'
        UpdCampo = 'QtdGerPeso'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 248
        Top = 32
        Width = 393
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 720
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 788
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 644
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 383
    Width = 1008
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    TabStop = True
    object Label50: TLabel
      Left = 10
      Top = 8
      Width = 42
      Height = 13
      Caption = 'N'#176' RME:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label49: TLabel
      Left = 680
      Top = 7
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
      Enabled = False
    end
    object SbStqCenLoc: TSpeedButton
      Left = 1113
      Top = 9
      Width = 21
      Height = 21
      Caption = '...'
    end
    object Label63: TLabel
      Left = 156
      Top = 8
      Width = 91
      Height = 13
      Caption = 'Mat'#233'ria-prima PDA:'
    end
    object EdReqMovEstq: TdmkEdit
      Left = 55
      Top = 4
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdStqCenLoc_: TdmkEditCB
      Left = 744
      Top = 4
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc_
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBStqCenLoc_: TdmkDBLookupComboBox
      Left = 799
      Top = 4
      Width = 200
      Height = 21
      Enabled = False
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 2
      dmkEditCB = EdStqCenLoc_
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdRmsGGX: TdmkEditCB
      Left = 256
      Top = 4
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBRmsGGX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBRmsGGX: TdmkDBLookupComboBox
      Left = 312
      Top = 4
      Width = 361
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsRmsGGX
      TabOrder = 4
      dmkEditCB = EdRmsGGX
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 76
    Top = 220
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 76
    Top = 268
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 152
    Top = 220
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 152
    Top = 268
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 224
    Top = 220
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 224
    Top = 268
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.Empresa, wmi.GraGruX, wmi.SdoVrtPeca,  '
      'wmi.SdoVrtPeso, wmi.SdoVrtArM2,  '
      'FLOOR((wmi.SdoVrtArM2 / 0.09290304)) + '
      'FLOOR(((MOD((wmi.SdoVrtArM2 / 0.09290304), 1)) + '
      '0.12499) * 4) * 0.25 AreaP2, 0.00 ValorT,  '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wmi.Pallet, vsp.Nome NO_Pallet, '
      'wmi.Terceiro, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      '"" NO_STATUS, '
      '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, '
      'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, '
      'wmi.Codigo, wmi.MovimCod IMEC, wmi.Controle IMEI, '
      'wmi.MovimID, wmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, '
      '1 Ativo '
      'FROM bluederm_2_1_cialeather.vsmovits wmi '
      
        'LEFT JOIN bluederm_2_1_cialeather.vspalleta  vsp ON vsp.Codigo=w' +
        'mi.Pallet   '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragrux    ggx ON ggx.Controle' +
        '=IF(wmi.Pallet <> 0, vsp.GraGruX, wmi.GraGruX) '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruy    ggy ON ggy.Codigo=g' +
        'gx.GraGruY '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragruc    ggc ON ggc.Controle' +
        '=ggx.GraGruC '
      
        'LEFT JOIN bluederm_2_1_cialeather.gracorcad  gcc ON gcc.Codigo=g' +
        'gc.GraCorCad '
      
        'LEFT JOIN bluederm_2_1_cialeather.gratamits  gti ON gti.Controle' +
        '=ggx.GraTamI '
      
        'LEFT JOIN bluederm_2_1_cialeather.gragru1    gg1 ON gg1.Nivel1=g' +
        'gx.GraGru1 '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  ent ON ent.Codigo=w' +
        'mi.Terceiro '
      
        'LEFT JOIN bluederm_2_1_cialeather.entidades  emp ON emp.Codigo=w' +
        'mi.Empresa '
      'WHERE wmi.Controle <> 0 '
      'AND wmi.GraGruX<>0 '
      'AND wmi.SdoVrtPeca > 0'
      'AND wmi.Pallet = 0'
      'AND wmi.Empresa=-11')
    Left = 360
    Top = 220
    object QrEstoqueControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstoqueEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstoqueGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstoquePecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstoquePesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstoqueAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstoqueValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEstoqueGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstoqueNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEstoquePallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstoqueNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrEstoqueTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstoqueNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstoqueNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstoqueNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Required = True
      Size = 0
    end
    object QrEstoqueDataHora: TWideStringField
      FieldName = 'DataHora'
      Required = True
      Size = 19
    end
    object QrEstoqueOrdGGX: TLargeintField
      FieldName = 'OrdGGX'
      Required = True
    end
    object QrEstoqueOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEstoqueCodiGGY: TIntegerField
      FieldName = 'CodiGGY'
      Required = True
    end
    object QrEstoqueNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEstoqueCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstoqueIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrEstoqueIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrEstoqueMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstoqueMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstoqueNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Required = True
      Size = 0
    end
    object QrEstoqueNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Required = True
      Size = 0
    end
    object QrEstoqueAtivo: TLargeintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEstoqueGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstoqueVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrEstoqueClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrEstoqueSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrEstoqueFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrEstoqueMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEstoqueNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrEstoqueSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstoqueSdoVrtArP2: TFloatField
      FieldName = 'SdoVrtArP2'
    end
    object QrEstoqueSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstoqueSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrEstoqueGGXInProc: TIntegerField
      FieldName = 'GGXInProc'
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 360
    Top = 268
  end
  object QrRmsGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 288
    Top = 220
    object QrRmsGGXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrRmsGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRmsGGXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrRmsGGXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrRmsGGXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrRmsGGXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsRmsGGX: TDataSource
    DataSet = QrRmsGGX
    Left = 288
    Top = 268
  end
  object QrStqCenLoc_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 428
    Top = 220
    object QrStqCenLoc_Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLoc_NO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc_
    Left = 428
    Top = 268
  end
end
