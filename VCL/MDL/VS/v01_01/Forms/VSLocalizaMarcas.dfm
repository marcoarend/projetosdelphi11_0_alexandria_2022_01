object FmVSLocalizaMarcas: TFmVSLocalizaMarcas
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-271 :: Localiza Marcas'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 197
        Height = 32
        Caption = 'Localiza Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 197
        Height = 32
        Caption = 'Localiza Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 197
        Height = 32
        Caption = 'Localiza Marcas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtIMEI: TBitBtn
        Tag = 611
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'IME-&I'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIMEIClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 260
        Top = 4
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object EdPesq: TdmkEdit
        Left = 260
        Top = 20
        Width = 173
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
      object RGPrecisao: TRadioGroup
        AlignWithMargins = True
        Left = 8
        Top = 0
        Width = 245
        Height = 45
        Margins.Left = 8
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        Caption = ' Precis'#227'o da pesquisa:'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Igual a'
          'Contendo')
        TabOrder = 1
        OnClick = RGPrecisaoClick
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 45
      Width = 812
      Height = 422
      Align = alClient
      DataSource = DsVSMovIts
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Tabela'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data/Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimNiv'
          Title.Caption = 'Nivel mov.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Empresa'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGru1'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome produto'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_LOC_CEN'
          Title.Caption = 'Centro / local'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso Kg'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoM2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFeSer'
          Title.Caption = 'S'#233'rie NFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFeNum'
          Title.Caption = 'N'#186' NFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtArM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ClientMO'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CliVenda'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOKg'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOTot'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntArM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorMP'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntArP2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeca'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtCorrApo'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FornecMO'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GGXRcl'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_TTW'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemNFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Misturou'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimTwn'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_DstMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_EstqMovimID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PALLET'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SrcMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaMPAG'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsFin'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsLib'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsVda'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ReqMovEstq'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StqCenLoc'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMulFrnCab'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMulNFeCab'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Width = 62
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSMovItsAfterOpen
    BeforeClose = QrVSMovItsBeforeClose
    OnCalcFields = QrVSMovItsCalcFields
    SQL.Strings = (
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, '
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, '
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, '
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, '
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, '
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, '
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, '
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, '
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, '
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, '
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      ''
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1, '
      'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,'
      'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, '
      'CAST(vsp.Nome AS CHAR) NO_Pallet, '
      'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN '
      ''
      ''
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc '
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo '
      ''
      ''
      'WHERE vmi.Controle > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'AND vmi.Controle=:P0'
      ''
      '')
    Left = 156
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = 'vsserfch.Nome'
      Size = 60
    end
    object QrVSMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrVSMovItsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVSMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVSMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVSMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVSMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVSMovItsLnkNivXtr1: TLargeintField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrVSMovItsLnkNivXtr2: TLargeintField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrVSMovItsClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrVSMovItsVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrVSMovItsNFeSer: TLargeintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrVSMovItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVSMovItsVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrVSMovItsJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrVSMovItsJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrVSMovItsJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrVSMovItsRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrVSMovItsRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrVSMovItsRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrVSMovItsGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrVSMovItsRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrVSMovItsJmpGGX: TLargeintField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrVSMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSMovItsCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSMovItsCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 156
    Top = 281
  end
  object PMIMEI: TPopupMenu
    Left = 180
    Top = 520
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Visible = False
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      Visible = False
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      Visible = False
    end
    object Irparajaneladomovimento1: TMenuItem
      Caption = 'Ir para janela do &movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object MostrarjaneladoIMEI1: TMenuItem
      Caption = 'Mostrar janela do &IME-I'
      OnClick = MostrarjaneladoIMEI1Click
    end
  end
end
