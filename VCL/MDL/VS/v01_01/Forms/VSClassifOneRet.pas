unit VSClassifOneRet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  mySQLDbTables;

type
  TFmVSClassifOneRet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnPartida: TPanel;
    LaVSRibCad: TLabel;
    SpeedButton1: TSpeedButton;
    EdFicha: TdmkEdit;
    DBGEmClasse: TdmkDBGridZTO;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNO_TERCEIRO: TWideStringField;
    QrVSMovItsNO_FICHA: TWideStringField;
    QrVSMovItsCUSTO_M2: TFloatField;
    QrVSMovItsCUSTO_P2: TFloatField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    DsVSMovIts: TDataSource;
    QrVSPaClaCab: TmySQLQuery;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_TERCEIRO: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    DsVSGerArtNew: TDataSource;
    DBGAClassificar: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGEmClasseDblClick(Sender: TObject);
    procedure DBGAClassificarDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAbertos();
    procedure SelecionaImei();
    procedure ExecutaPesquisa();
    procedure GeraNovoIMEI();
    procedure NeutralizaAcaoUsu(Neutraliza: Boolean);
  public
    { Public declarations }
    FCodigo, FCacCod: Integer;
    FPallet1, FPallet2, FPallet3, FPallet4, FPallet5, FPallet6: Integer;
    FMovimID: TEstqMovimID;
    FForm: TForm;
  end;

  var
  FmVSClassifOneRet: TFmVSClassifOneRet;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, VSClassifOneNew;

{$R *.DFM}

procedure TFmVSClassifOneRet.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  FCacCod := 0;
  //FMovimID := 0;
  Close;
end;

procedure TFmVSClassifOneRet.DBGAClassificarDblClick(Sender: TObject);
begin
  GeraNovoIMEI();
end;

procedure TFmVSClassifOneRet.DBGEmClasseDblClick(Sender: TObject);
begin
  if (QrVSMovIts.State <> dsInactive) and (QrVSMovIts.RecordCount > 0) then
    SelecionaIMEI();
end;

procedure TFmVSClassifOneRet.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    ExecutaPesquisa();
end;

procedure TFmVSClassifOneRet.ExecutaPesquisa();
const
  Controle = 0;
var
  Ficha: Integer;
begin
  Ficha := EdFicha.ValueVariant;
  ReopenAbertos();
  VS_PF.ReopenVSGerArtDst_ToClass(QrVSGerArtNew, Ficha, Controle);
  //
  if (QrVSMovIts.RecordCount = 1) and (QrVSGerArtNew.RecordCount = 0) then
     SelecionaIMEI()
  else
  if (QrVSMovIts.RecordCount = 0) and (QrVSGerArtNew.RecordCount = 1) then
    GeraNovoIMEI();
end;

procedure TFmVSClassifOneRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClassifOneRet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  //
  ReopenAbertos();
end;

procedure TFmVSClassifOneRet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClassifOneRet.GeraNovoIMEI();
const
  Automatico = True;
begin
  if (QrVSGerArtNew.State <> dsInactive) and (QrVSGerArtNew.RecordCount > 0) then
  begin
    NeutralizaAcaoUsu(True);
    VS_PF.MostraFormVSClaArtPrp(FPallet1, FPallet2, FPallet3, FPallet4,
      FPallet5, FPallet6, FForm, QrVSGerArtNewControle.Value);
    //
    Close;
  end;
end;

procedure TFmVSClassifOneRet.NeutralizaAcaoUsu(Neutraliza: Boolean);
begin
  if Neutraliza then
    Visible := False
  else
    Visible := True;
  DBGEmClasse.Visible := not Neutraliza;
  DBGAClassificar.Visible := not Neutraliza;
  Application.ProcessMessages;
end;

procedure TFmVSClassifOneRet.ReopenAbertos;
var
  Ficha: Integer;
  SQL_Ficha: String;
begin
  Ficha := EdFicha.ValueVariant;
  if Ficha = 0 then
    //QrVSMovIts.Close
    SQL_Ficha := ''
  else
    SQL_Ficha := 'AND wmi.Ficha=' + Geral.FF0(Ficha);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(wmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_TERCEIRO, ',
  'IF(wmi.Ficha=0, "V�rias", wmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(wmi.Controle, " ", gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  'IF(wmi.Ficha=0, "", CONCAT(" - S�rie / Ficha RMP ", wmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM vsmovits wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND wmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)),
  'AND wmi.MovimCod IN ( ',
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrCfgCla > "1900-01-01" ',
  '  AND DtHrFimCla < "1900-01-01" ',
  ') ',
  SQL_Ficha,
  '']);
end;

procedure TFmVSClassifOneRet.SelecionaImei;
var
  VSMovIts, VSGerArt: Integer;
begin
  NeutralizaAcaoUsu(True);
  VSGerArt := QrVSMovItsCodigo.Value;
  VSMovIts := QrVSMovItsControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vspaclacaba ',
  'WHERE VSMovIts=' + Geral.FF0(VSMovIts),
  'AND VSGerArt=' + Geral.FF0(VSGerArt),
  '']);
  //
  FCodigo := 0;
  FCacCod := 0;
  case QrVSPaClaCab.RecordCount of
    0: Geral.MB_Aviso('Configura��o de classifica��o n�o localizada!');
    1:
    begin
      FCodigo := QrVSPaClaCabCodigo.Value;
      FCacCod := QrVSPaClaCabCacCod.Value;
      if VSGerArt > 0 then
        FMovimID := emidClassArtXXUni
      else
        FMovimID := emidReclasVS;
        //
      TFmVSClassifOneNew(FForm).FCodigo := FCodigo;
      TFmVSClassifOneNew(FForm).FCacCod := FCacCod;
      TFmVSClassifOneNew(FForm).FMovimID := FMovimID;
      //
      TFmVSClassifOneNew(FForm).ReopenVSPaClaCab();
    end;
    else Geral.MB_Aviso('Foram localizadas ' + Geral.FF0(QrVSPaClaCab.RecordCount) +
    ' configura��es de classifica��o!' + sLineBreak +
    'Por seguran�a nenhuma ser� ulilizada' + sLineBreak +
    'Avise a DERMATEK!');
  end;
  if FCodigo <> 0 then
    Close
  else
    NeutralizaAcaoUsu(False);
end;

procedure TFmVSClassifOneRet.SpeedButton1Click(Sender: TObject);
begin
  ExecutaPesquisa();
end;

end.
