unit VSCalOriIMEI_Peso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSCalOriIMEI_Peso = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaPecas: TLabel;
    LaPesoKg: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdPeso: TdmkEdit;
    EdQtdAntPeso: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoquePecas: TFloatField;
    QrEstoquePesoKg: TFloatField;
    QrEstoqueAreaM2: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstoque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    QrEstoqueVSMulFrnCab: TIntegerField;
    QrEstoqueClientMO: TIntegerField;
    EdPesoKg: TdmkEdit;
    Label3: TLabel;
    QrEstoqueSerieFch: TIntegerField;
    QrEstoqueFicha: TIntegerField;
    QrEstoqueMarca: TWideStringField;
    QrEstoqueNO_SerieFch: TWideStringField;
    QrEstoqueSdoVrtArM2: TFloatField;
    QrEstoqueSdoVrtArP2: TFloatField;
    QrEstoqueSdoVrtPeca: TFloatField;
    QrEstoqueSdoVrtPeso: TFloatField;
    QrRmsGGX: TmySQLQuery;
    QrRmsGGXGraGru1: TIntegerField;
    QrRmsGGXControle: TIntegerField;
    QrRmsGGXNO_PRD_TAM_COR: TWideStringField;
    QrRmsGGXSIGLAUNIDMED: TWideStringField;
    QrRmsGGXCODUSUUNIDMED: TIntegerField;
    QrRmsGGXNOMEUNIDMED: TWideStringField;
    DsRmsGGX: TDataSource;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrEstoqueGGXInProc: TIntegerField;
    CkSoClientMO: TCheckBox;
    QrStqCenLoc_: TmySQLQuery;
    QrStqCenLoc_Controle: TIntegerField;
    QrStqCenLoc_NO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    EdStqCenLoc_: TdmkEditCB;
    Label49: TLabel;
    CBStqCenLoc_: TdmkDBLookupComboBox;
    SbStqCenLoc: TSpeedButton;
    Label63: TLabel;
    EdRmsGGX: TdmkEditCB;
    CBRmsGGX: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdPesoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    //
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcPecas, ParcPesoKg,
              ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
    procedure FechaPesquisa();
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    procedure RedefineComponentes();
    function  BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
  public
    { Public declarations }
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FOrigMovimNiv, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea,
    FGGXRmsDst, FGGXRmsSrc, FStqCenLoc, FFornecMO: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    procedure ReopenItensAptos();
  end;

  var
  FmVSCalOriIMEI_Peso: TFmVSCalOriIMEI_Peso;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModVS, VSCalCab, UnVS_PF, AppListas, UnDmkProcFunc, ModVS_CRC, UnVS_CRC_PF,
  StqCenLoc;

{$R *.DFM}

procedure TFmVSCalOriIMEI_Peso.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

function TFmVSCalOriIMEI_Peso.BaixaIMEIParcial(): Integer;
var
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2: Double;
  QtdAntPeca, QtdAntPeso: Double;
begin
  Result := 0;
  Pecas  := 0.00;//EdPecas.ValueVariant;
  PesoKg := EdPesoKg.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  SobraPecas  := Pecas;
  SobraPesoKg := PesoKg;
  SobraAreaM2 := AreaM2;
  SobraAreaP2 := AreaP2;
  //
  QtdAntPeca  := SobraPecas;
  QtdAntPeso  := EdQtdAntPeso.ValueVariant;
  //
  if MyObjects.FIC(PesoKg <= 0, EdPeso, 'Informe o peso kg!') then
    Exit;
  InsereIMEI_Atual(False, SobraPecas, SobraPesoKg, SobraAreaM2, SobraAreaP2,
    QtdAntPeca, QtdAntPeso);
  Result := 1;
end;

function TFmVSCalOriIMEI_Peso.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      InsereIMEI_Atual(True, 0, 0, 0, 0, 0, 0);
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmVSCalOriIMEI_Peso.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  FGGXRmsDst := EdRmsGGX.ValueVariant;
  FGGXRmsSrc := FmVSCalCab.QrVSCalCabGGXSrc.Value;
  //FStqCenLoc := EdStqCenLoc.ValueVariant;
  FStqCenLoc := FmVSCalCab.QrVSCalAtuStqCenLoc.Value;
  FFornecMO  := FmVSCalCab.QrVSCalAtuFornecMO.Value;

  //
  if MyObjects.FIC(FGGXRmsDst = 0, EdRmsGGX, 'Informe a mat�ria-prima PDA!') then Exit;
  //if MyObjects.FIC(FStqCenLoc = 0, EdStqCenLoc, 'Informe a localiza��o!') then Exit;
  if MyObjects.FIC(FStqCenLoc = 0, nil, 'Informe a localiza��o no cabe�alho!') then Exit;
  if MyObjects.FIC(FFornecMO = 0, nil, 'Informe o fornecedor do servi�o no cabe�alho!') then Exit;
  //
  if QrEstoqueSdoVrtPeso.Value = 0 then
  begin
    if Geral.MB_Pergunta('O Saldo Peso Kg do IMEI ' + Geral.FF0(QrEstoqueIMEI.Value)
      + ' est� zerado!' + sLineBreak + 'Deseja informar o peso?') = ID_YES then
    begin
      VS_PF.MostraFormVSInnCab(QrEstoqueCodigo.Value, QrEstoqueIMEI.Value, 0);
    end;
    RedefineComponentes();
    Exit;
  end;
  //
  if FParcial then
    N := BaixaIMEIParcial()
  else
    N := BaixaIMEITotal();
  //
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('VSCalcab', MovimCod);
    MovimCod := FmVSCalCab.QrVSCalCabMovimCod.Value;
    VS_PF.AtualizaTotaisVSCalCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmVSCalCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
        RedefineComponentes()
      else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
(*
  // Nao se aplica. Calcula com function propria a seguir.
  //VS_PF.AtualizaTotaisVSXxxCab('VSCalcab', MovimCod);
  MovimCod := FmVSCalCab.QrVSCalCabMovimCod.Value;
  VS_PF.AtualizaTotaisVSCalCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmVSCalCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
end;

procedure TFmVSCalOriIMEI_Peso.RedefineComponentes();
begin
  EdControle.ValueVariant   := 0;
  //EdPecas.ValueVariant      := 0;
  EdPeso.ValueVariant       := 0;
  EdPesoKg.ValueVariant     := 0;
  EdAreaM2.ValueVariant     := 0;
  EdAreaP2.ValueVariant     := 0;
  EdObserv.ValueVariant     := '';
  EdQtdAntPeso.ValueVariant := 0;
  //
  ReopenItensAptos();
  //
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
  //
  LiberaEdicao(False, False);
end;

procedure TFmVSCalOriIMEI_Peso.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCalOriIMEI_Peso.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  I, J, OK, N, GraGruX: Integer;
  LstReduz(*, LstCount*): array of Integer;
begin
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    //
    GraGruX := QrEstoqueGraGruX.Value;
    OK := -1;
    for J := Low(LstReduz) to High(LstReduz) do
      if LstReduz[J] = GraGruX then
        OK := J;
    if OK = -1 then
    begin
      N := Length(LstReduz);
      SetLength(LstReduz, N + 1);
      LstReduz[N] := GraGruX
    end;
  end;
  if Length(LstReduz) = 1 then
  begin
    EdRmsGGX.ValueVariant := QrEstoqueGGXInProc.Value;
    CBRmsGGX.KeyValue     := QrEstoqueGGXInProc.Value;
  end else
  begin
    EdRmsGGX.ValueVariant := 0;
    CBRmsGGX.KeyValue     := Null;
  end;
end;

procedure TFmVSCalOriIMEI_Peso.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmVSCalOriIMEI_Peso.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCalOriIMEI_Peso.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSCalOriIMEI_Peso.EdPesoChange(Sender: TObject);
(*
var
  Pecas, PesoKg: Double;
begin
  Pecas  := EdPecas.ValueVariant;
  PesoKg := QrEstoquePesoKg.Value /QrEstoquePecas.Value;
  EdPesoKg.ValueVariant := Pecas * PesoKg;
*)
begin
  EdPesoKg.ValueVariant := EdPeso.ValueVariant;
end;

procedure TFmVSCalOriIMEI_Peso.EdPesoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    //EdPecas.ValueVariant := QrEstoqueSdoVrtPeca.Value;
    EdPeso.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    EdQtdAntPeso.ValueVariant := QrEstoqueSdoVrtPeso.Value;
    EdPesoKg.ValueVariant := QrEstoqueSdoVrtPeso.Value;
  end;
end;

procedure TFmVSCalOriIMEI_Peso.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCalOriIMEI_Peso.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSCalOriIMEI_Peso.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmVSCalOriIMEI_Peso.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmVSCalOriIMEI_Peso.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  VS_PF.AbreVSSerFch(QrVSSerFch);
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    //Geral.FF0(CO_GraGruY_1365_VS??????) + ',' +
    Geral.FF0(CO_GraGruY_1024_VSNatCad) + ',' +
    Geral.FF0(CO_GraGruY_1088_VSNatCon) +
    ')');
  //
  VS_PF.AbreGraGruXY(QrRmsGGX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1195_VSNatPDA));
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0(*, TEstqMovimID.???*));
end;

procedure TFmVSCalOriIMEI_Peso.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCalOriIMEI_Peso.InsereIMEI_Atual(InsereTudo: Boolean;
 ParcPecas, ParcPesoKg, ParcAreaM2, ParcAreaP2, ParcQtdAntPeca, ParcQtdAntPeso: Double);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  QtdAntArM2  = 0;
  QtdAntArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, ReqMovEstq, VSMulFrnCab, ClientMO, StqCenLoc, FornecMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor, QtdAntPeca, QtdAntPeso: Double;
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX, RmsStqCenLoc: Integer;
begin
  FornecMO       := FFornecMO;
  StqCenLoc      := FStqCenLoc;
  SrcMovID       := QrEstoqueMovimID.Value;
  SrcNivel1      := QrEstoqueCodigo.Value;
  SrcNivel2      := QrEstoqueControle.Value;
  SrcGGX         := QrEstoqueGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  Terceiro       := QrEstoqueTerceiro.Value;
  VSMulFrnCab    := QrEstoqueVSMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmProcCal;
  MovimNiv       := eminSorcCal;
  Pallet         := QrEstoquePallet.Value;
  GraGruX        := QrEstoqueGraGruX.Value;
  if not InsereTudo then
  begin
    Pecas        := -ParcPecas;
    PesoKg       := -ParcPesoKg;
    AreaM2       := -ParcAreaM2;
    AreaP2       := -ParcAreaP2;
    QtdAntPeca   := ParcQtdAntPeca;
    QtdAntPeso   := ParcQtdAntPeso;
  //
  end else
  begin
    Pecas          := -QrEstoqueSdoVrtPeca.Value;
    PesoKg         := -QrEstoqueSdoVrtPeso.Value;
    AreaM2         := -QrEstoqueSdoVrtArM2.Value;
    AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    QtdAntPeca     := QrEstoqueSdoVrtPeca.Value;
    QtdAntPeso     := QrEstoqueSdoVrtPeso.Value;
  end;
  if (QrEstoquePesoKg.Value > 0) then
    Valor := -PesoKg *
    (QrEstoqueValorT.Value / QrEstoquePesoKg.Value)
  else
  if (QrEstoqueAreaM2.Value > 0) then
    Valor := -AreaM2 *
    (QrEstoqueValorT.Value / QrEstoqueAreaM2.Value)
  else
  if QrEstoquePecas.Value > 0 then
    Valor := -Pecas *
    (QrEstoqueValorT.Value / QrEstoquePecas.Value)
  else
    Valor := 0;
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := QrEstoqueSerieFch.Value;
  Ficha          := QrEstoqueFicha.Value;
  Marca          := QrEstoqueMarca.Value;
  //Misturou       := QrEstoqueMisturou.Value;
  DstMovID       := TEstqMovimID(FmVSCalCab.QrVSCalAtuMovimID.Value);
  DstNivel1      := FmVSCalCab.QrVSCalAtuCodigo.Value;
  DstNivel2      := FmVSCalCab.QrVSCalAtuControle.Value;
  DstGGX         := FmVSCalCab.QrVSCalAtuGraGruX.Value;
  RmsGGX         := EdRmsGGX.ValueVariant;
  //
  GraGruY        := QrEstoqueGraGruY.Value;
  //
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  ClientMO       := QrEstoqueClientMO.Value;
  RmsStqCenLoc   := StqCenLoc;
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei003(*Mat�ria prima origem no processo de caleiro*)) then
  begin
    //  Criar lancamento de couro PDA com RmsMovID, RmsNivel1 e RmsNivel2
    VS_PF.CriaVMIRmsCal(Controle, FGGXRmsSrc, FGGXRmsDst,
     RmsGGX, //FmVSCalCab.QrVSCalCabGraGruX.Value,
    QrEstoquePecas.Value, QrEstoqueAreaM2.Value, QrEstoquePesoKg.Value,
    //-Pecas, -AreaM2, -PesoKg, -ValorT, RmsStqCenLoc);
    QtdAntPeca, QtdAntArM2, QtdAntPeso, -ValorT, RmsStqCenLoc, Codigo, MovimCod,
    // ini 2023-11-20 Baixa de sub produto por peso
    PesoKg
    // fim 2023-11-20 Baixa de sub produto por peso
    );
    //
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, True);
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
  end;
end;

procedure TFmVSCalOriIMEI_Peso.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled    := not Status;
    EdFicha.Enabled    := not Status;
    LaGraGruX.Enabled  := not Status;
    EdGraGruX.Enabled  := not Status;
    CBGraGruX.Enabled  := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled   := not Status;
    //
    if Libera then
    begin
      if QrRmsGGX.Locate('Controle', QrEstoqueGGXInProc.Value, []) then
      begin
        EdRmsGGX.ValueVariant := QrEstoqueGGXInProc.Value;
        CBRmsGGX.KeyValue     := QrEstoqueGGXInProc.Value;
        //if (EdPecas.Enabled) and (EdPecas.Visible) then
          //EdPecas.SetFocus;
        if (EdPeso.Enabled) and (EdPeso.Visible) then
          EdPeso.SetFocus;
      end
      else
      if (EdRmsGGX.Enabled) and (EdRmsGGX.Visible) then
        EdRmsGGX.SetFocus;
    end;
  end;
end;

procedure TFmVSCalOriIMEI_Peso.ReopenItensAptos();
const
  StqCenCad = 0;
  Terceiro = 0;
var
  Empresa, ClientMO, GraGruX: Integer;
begin
  Empresa := FEmpresa;
  GraGruX := EdGraGruX.ValueVariant;
  if CkSoClientMO.Checked then
    ClientMO := FmVSCalCab.QrVSCalAtuClientMO.Value
  else
    ClientMO := 0;
  //
  VS_CRC_PF.ReopenItensAptos_Cal(QrEstoque, Empresa, StqCenCad, Terceiro,
    ClientMO, GraGruX, TModoBxaEstq.mbePesoKg);
end;

end.
