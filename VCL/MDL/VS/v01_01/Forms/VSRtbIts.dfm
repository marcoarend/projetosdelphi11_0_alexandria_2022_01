object FmVSRtbIts: TFmVSRtbIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-108 :: Entrada de Artigo Vendido Para Retrabalho'
  ClientHeight = 628
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 489
    Width = 727
    Height = 25
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 727
    Height = 101
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    TabOrder = 0
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 723
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 0
        Width = 53
        Height = 13
        Caption = 'ID entrada:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 96
        Top = 0
        Width = 55
        Height = 13
        Caption = 'ID estoque:'
        FocusControl = DBEdMovimCod
      end
      object Label3: TLabel
        Left = 180
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdMovimCod: TdmkDBEdit
        Left = 96
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 180
        Top = 16
        Width = 45
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 57
      Width = 723
      Height = 42
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 149
    Width = 727
    Height = 340
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 119
      Height = 13
      Caption = 'Artigo (F4 - '#250'ltima usada):'
    end
    object LaPecas: TLabel
      Left = 12
      Top = 136
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object LaAreaM2: TLabel
      Left = 104
      Top = 136
      Width = 39
      Height = 13
      Caption = #193'rea m'#178':'
    end
    object LaAreaP2: TLabel
      Left = 200
      Top = 136
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
    end
    object LaPeso: TLabel
      Left = 304
      Top = 136
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object Label4: TLabel
      Left = 244
      Top = 96
      Width = 29
      Height = 13
      Caption = 'Pallet:'
    end
    object SBPallet: TSpeedButton
      Left = 584
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBPalletClick
    end
    object Label7: TLabel
      Left = 504
      Top = 176
      Width = 36
      Height = 13
      Caption = '$ Total:'
      Enabled = False
    end
    object Label10: TLabel
      Left = 12
      Top = 56
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
      Enabled = False
    end
    object Label8: TLabel
      Left = 400
      Top = 136
      Width = 76
      Height = 13
      Caption = '$ Total M-prima:'
    end
    object Label12: TLabel
      Left = 504
      Top = 136
      Width = 62
      Height = 13
      Caption = '$/kg M-obra:'
      Enabled = False
    end
    object Label13: TLabel
      Left = 12
      Top = 176
      Width = 68
      Height = 13
      Caption = '$ total M-obra:'
      Enabled = False
    end
    object Label14: TLabel
      Left = 116
      Top = 176
      Width = 240
      Height = 13
      Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      Enabled = False
    end
    object Label11: TLabel
      Left = 12
      Top = 96
      Width = 83
      Height = 13
      Caption = 'S'#233'rie Ficha RMP:'
    end
    object Label15: TLabel
      Left = 148
      Top = 96
      Width = 77
      Height = 13
      Caption = 'Ficha RMP [F4]:'
    end
    object Label9: TLabel
      Left = 12
      Top = 216
      Width = 33
      Height = 13
      Caption = 'Marca:'
    end
    object Label16: TLabel
      Left = 108
      Top = 216
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object SBNewPallet: TSpeedButton
      Left = 608
      Top = 112
      Width = 21
      Height = 21
      Caption = '+'
      OnClick = SBNewPalletClick
    end
    object Label17: TLabel
      Left = 608
      Top = 136
      Width = 61
      Height = 13
      Caption = '$/m'#178' M-obra:'
      Enabled = False
    end
    object Label49: TLabel
      Left = 12
      Top = 256
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 512
      Top = 256
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 453
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 2
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdGraGruX
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdGraGruX: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdGraGruXKeyDown
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdPecas: TdmkEdit
      Left = 12
      Top = 152
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdAreaM2: TdmkEditCalc
      Left = 104
      Top = 152
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 200
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdPesoKg: TdmkEdit
      Left = 304
      Top = 152
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdPesoKgChange
    end
    object EdPallet: TdmkEditCB
      Left = 244
      Top = 112
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pallet'
      UpdCampo = 'Pallet'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPallet
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPallet: TdmkDBLookupComboBox
      Left = 288
      Top = 112
      Width = 297
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsVSPallet
      TabOrder = 9
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdPallet
      QryCampo = 'Pallet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdValorT: TdmkEdit
      Left = 504
      Top = 192
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 19
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdFornecedor: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Fornecedor'
      UpdCampo = 'Fornecedor'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecedor
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 537
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsFornecedor
      TabOrder = 4
      dmkEditCB = EdFornecedor
      QryCampo = 'Fornecedor'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdValorMP: TdmkEdit
      Left = 400
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'ValorMP'
      UpdCampo = 'ValorMP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorMPChange
    end
    object EdCustoMOKg: TdmkEdit
      Left = 504
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'CustoMOKg'
      UpdCampo = 'CustoMOKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCustoMOKgChange
    end
    object EdCustoMOTot: TdmkEdit
      Left = 12
      Top = 192
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CustoMOTot'
      UpdCampo = 'CustoMOTot'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorMPChange
    end
    object EdFornecMO: TdmkEditCB
      Left = 116
      Top = 192
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'FornecMO'
      UpdCampo = 'FornecMO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecMO
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFornecMO: TdmkDBLookupComboBox
      Left = 172
      Top = 192
      Width = 329
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsPrestador
      TabOrder = 18
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdFornecMO
      QryCampo = 'Pallet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdSerieFch: TdmkEditCB
      Left = 12
      Top = 112
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'SerieFch'
      UpdCampo = 'SerieFch'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSerieFch
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBSerieFch: TdmkDBLookupComboBox
      Left = 52
      Top = 112
      Width = 93
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsVSSerFch
      TabOrder = 6
      dmkEditCB = EdSerieFch
      QryCampo = 'SerieFch'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdFicha: TdmkEdit
      Left = 148
      Top = 112
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdFichaKeyDown
    end
    object EdMarca: TdmkEdit
      Left = 12
      Top = 232
      Width = 93
      Height = 21
      TabOrder = 20
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Marca'
      UpdCampo = 'Marca'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdObserv: TdmkEdit
      Left = 108
      Top = 232
      Width = 497
      Height = 21
      TabOrder = 21
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCustoMOM2: TdmkEdit
      Left = 608
      Top = 152
      Width = 86
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 22
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'CustoMOM2'
      UpdCampo = 'CustoMOM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCustoMOKgChange
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 272
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 23
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 68
      Top = 272
      Width = 441
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 24
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReqMovEstq: TdmkEdit
      Left = 512
      Top = 272
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 25
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Panel9: TPanel
      Left = 2
      Top = 297
      Width = 723
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 26
      object Label20: TLabel
        Left = 423
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Folha:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label21: TLabel
        Left = 515
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Linha:'
        Color = clBtnFace
        ParentColor = False
      end
      object RGIxxMovIX: TRadioGroup
        Left = 0
        Top = 0
        Width = 416
        Height = 41
        Align = alLeft
        Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Entrada de couro'
          'Classe / reclasse'
          'Sa'#237'da de couro')
        TabOrder = 0
      end
      object EdIxxFolha: TdmkEdit
        Left = 456
        Top = 12
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdIxxLinha: TdmkEdit
        Left = 548
        Top = 12
        Width = 33
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 727
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 679
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 631
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 521
        Height = 32
        Caption = 'Entrada de Artigo Vendido Para Retrabalho'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 521
        Height = 32
        Caption = 'Entrada de Artigo Vendido Para Retrabalho'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 521
        Height = 32
        Caption = 'Entrada de Artigo Vendido Para Retrabalho'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 514
    Width = 727
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 723
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 558
    Width = 727
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 581
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Grandeza'
      'FROM defpecas')
    Left = 440
    Top = 56
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 444
    Top = 104
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Codigo, pla.GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), '
      '" ", pla.Nome)  '
      'NO_PRD_TAM_COR  '
      'FROM vspalleta pla  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'WHERE pla.Ativo=1  '
      'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ')
    Left = 316
    Top = 56
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 316
    Top = 104
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 248
    Top = 104
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 248
    Top = 56
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 376
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXCouNiv2: TFloatField
      FieldName = 'CouNiv2'
    end
    object QrGraGruXGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 376
    Top = 104
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 508
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 508
    Top = 104
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 576
    Top = 56
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 576
    Top = 104
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 648
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 648
    Top = 104
  end
end
