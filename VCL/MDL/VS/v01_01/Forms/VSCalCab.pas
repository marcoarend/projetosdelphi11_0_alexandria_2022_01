unit VSCalCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects,
  UnProjGroup_Consts, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  TFmVSCalCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSCalCab: TmySQLQuery;
    DsVSCalCab: TDataSource;
    QrVSCalAtu: TmySQLQuery;
    DsVSCalAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSCalCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSCalCabEmpresa: TIntegerField;
    QrVSCalCabDtHrAberto: TDateTimeField;
    QrVSCalCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSCalCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSCalOriIMEI: TmySQLQuery;
    DsVSCalOriIMEI: TDataSource;
    QrVSCalCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSCalCabLk: TIntegerField;
    QrVSCalCabDataCad: TDateField;
    QrVSCalCabDataAlt: TDateField;
    QrVSCalCabUserCad: TIntegerField;
    QrVSCalCabUserAlt: TIntegerField;
    QrVSCalCabAlterWeb: TSmallintField;
    QrVSCalCabAtivo: TSmallintField;
    QrVSCalCabPecasMan: TFloatField;
    QrVSCalCabAreaManM2: TFloatField;
    QrVSCalCabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSCalCabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSCalCabNO_TIPO: TWideStringField;
    QrVSCalCabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSCalCabNO_DtHrLibOpe: TWideStringField;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSCalCabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSCalCabCacCod: TIntegerField;
    QrVSCalCabGraGruX: TIntegerField;
    QrVSCalCabCustoManMOKg: TFloatField;
    QrVSCalCabCustoManMOTot: TFloatField;
    QrVSCalCabValorManMP: TFloatField;
    QrVSCalCabValorManT: TFloatField;
    QrVSCalCabDtHrLibOpe: TDateTimeField;
    QrVSCalCabDtHrCfgOpe: TDateTimeField;
    QrVSCalCabDtHrFimOpe: TDateTimeField;
    QrVSCalCabPecasSrc: TFloatField;
    QrVSCalCabAreaSrcM2: TFloatField;
    QrVSCalCabAreaSrcP2: TFloatField;
    QrVSCalCabPecasDst: TFloatField;
    QrVSCalCabAreaDstM2: TFloatField;
    QrVSCalCabAreaDstP2: TFloatField;
    QrVSCalCabPecasSdo: TFloatField;
    QrVSCalCabAreaSdoM2: TFloatField;
    QrVSCalCabAreaSdoP2: TFloatField;
    QrVSCalDst: TmySQLQuery;
    DsVSCalDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    AtualizaestoqueEmProcesso1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSCalCabPesoKgSrc: TFloatField;
    QrVSCalCabPesoKgMan: TFloatField;
    QrVSCalCabPesoKgDst: TFloatField;
    QrVSCalCabPesoKgSdo: TFloatField;
    QrVSCalCabValorTMan: TFloatField;
    QrVSCalCabValorTSrc: TFloatField;
    QrVSCalCabValorTSdo: TFloatField;
    QrVSCalCabPecasINI: TFloatField;
    QrVSCalCabAreaINIM2: TFloatField;
    QrVSCalCabAreaINIP2: TFloatField;
    QrVSCalCabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSCalCabPesoKgBxa: TFloatField;
    QrVSCalCabPecasBxa: TFloatField;
    QrVSCalCabAreaBxaM2: TFloatField;
    QrVSCalCabAreaBxaP2: TFloatField;
    QrVSCalCabValorTBxa: TFloatField;
    PCCalOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSCalOriPallet: TmySQLQuery;
    DsVSCalOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    Total2: TMenuItem;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdCustoMOM2: TdmkEdit;
    Label48: TLabel;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label51: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label52: TLabel;
    DBEdit41: TDBEdit;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrVSCalCabCliente: TIntegerField;
    QrVSCalCabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrVSCalCabNFeRem: TIntegerField;
    QrVSCalCabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrVSCalCabTemIMEIMrt: TIntegerField;
    QrVSCalAtuCodigo: TLargeintField;
    QrVSCalAtuControle: TLargeintField;
    QrVSCalAtuMovimCod: TLargeintField;
    QrVSCalAtuMovimNiv: TLargeintField;
    QrVSCalAtuMovimTwn: TLargeintField;
    QrVSCalAtuEmpresa: TLargeintField;
    QrVSCalAtuTerceiro: TLargeintField;
    QrVSCalAtuCliVenda: TLargeintField;
    QrVSCalAtuMovimID: TLargeintField;
    QrVSCalAtuDataHora: TDateTimeField;
    QrVSCalAtuPallet: TLargeintField;
    QrVSCalAtuGraGruX: TLargeintField;
    QrVSCalAtuPecas: TFloatField;
    QrVSCalAtuPesoKg: TFloatField;
    QrVSCalAtuAreaM2: TFloatField;
    QrVSCalAtuAreaP2: TFloatField;
    QrVSCalAtuValorT: TFloatField;
    QrVSCalAtuSrcMovID: TLargeintField;
    QrVSCalAtuSrcNivel1: TLargeintField;
    QrVSCalAtuSrcNivel2: TLargeintField;
    QrVSCalAtuSrcGGX: TLargeintField;
    QrVSCalAtuSdoVrtPeca: TFloatField;
    QrVSCalAtuSdoVrtPeso: TFloatField;
    QrVSCalAtuSdoVrtArM2: TFloatField;
    QrVSCalAtuObserv: TWideStringField;
    QrVSCalAtuSerieFch: TLargeintField;
    QrVSCalAtuFicha: TLargeintField;
    QrVSCalAtuMisturou: TLargeintField;
    QrVSCalAtuFornecMO: TLargeintField;
    QrVSCalAtuCustoMOKg: TFloatField;
    QrVSCalAtuCustoMOM2: TFloatField;
    QrVSCalAtuCustoMOTot: TFloatField;
    QrVSCalAtuValorMP: TFloatField;
    QrVSCalAtuDstMovID: TLargeintField;
    QrVSCalAtuDstNivel1: TLargeintField;
    QrVSCalAtuDstNivel2: TLargeintField;
    QrVSCalAtuDstGGX: TLargeintField;
    QrVSCalAtuQtdGerPeca: TFloatField;
    QrVSCalAtuQtdGerPeso: TFloatField;
    QrVSCalAtuQtdGerArM2: TFloatField;
    QrVSCalAtuQtdGerArP2: TFloatField;
    QrVSCalAtuQtdAntPeca: TFloatField;
    QrVSCalAtuQtdAntPeso: TFloatField;
    QrVSCalAtuQtdAntArM2: TFloatField;
    QrVSCalAtuQtdAntArP2: TFloatField;
    QrVSCalAtuNotaMPAG: TFloatField;
    QrVSCalAtuNO_PALLET: TWideStringField;
    QrVSCalAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSCalAtuNO_TTW: TWideStringField;
    QrVSCalAtuID_TTW: TLargeintField;
    QrVSCalAtuNO_FORNECE: TWideStringField;
    QrVSCalAtuReqMovEstq: TLargeintField;
    QrVSCalAtuCUSTO_M2: TFloatField;
    QrVSCalAtuCUSTO_P2: TFloatField;
    QrVSCalAtuNO_LOC_CEN: TWideStringField;
    QrVSCalAtuMarca: TWideStringField;
    QrVSCalAtuPedItsLib: TLargeintField;
    QrVSCalAtuStqCenLoc: TLargeintField;
    QrVSCalAtuNO_FICHA: TWideStringField;
    QrVSCalOriIMEICodigo: TLargeintField;
    QrVSCalOriIMEIControle: TLargeintField;
    QrVSCalOriIMEIMovimCod: TLargeintField;
    QrVSCalOriIMEIMovimNiv: TLargeintField;
    QrVSCalOriIMEIMovimTwn: TLargeintField;
    QrVSCalOriIMEIEmpresa: TLargeintField;
    QrVSCalOriIMEITerceiro: TLargeintField;
    QrVSCalOriIMEICliVenda: TLargeintField;
    QrVSCalOriIMEIMovimID: TLargeintField;
    QrVSCalOriIMEIDataHora: TDateTimeField;
    QrVSCalOriIMEIPallet: TLargeintField;
    QrVSCalOriIMEIGraGruX: TLargeintField;
    QrVSCalOriIMEIPecas: TFloatField;
    QrVSCalOriIMEIPesoKg: TFloatField;
    QrVSCalOriIMEIAreaM2: TFloatField;
    QrVSCalOriIMEIAreaP2: TFloatField;
    QrVSCalOriIMEIValorT: TFloatField;
    QrVSCalOriIMEISrcMovID: TLargeintField;
    QrVSCalOriIMEISrcNivel1: TLargeintField;
    QrVSCalOriIMEISrcNivel2: TLargeintField;
    QrVSCalOriIMEISrcGGX: TLargeintField;
    QrVSCalOriIMEISdoVrtPeca: TFloatField;
    QrVSCalOriIMEISdoVrtPeso: TFloatField;
    QrVSCalOriIMEISdoVrtArM2: TFloatField;
    QrVSCalOriIMEIObserv: TWideStringField;
    QrVSCalOriIMEISerieFch: TLargeintField;
    QrVSCalOriIMEIFicha: TLargeintField;
    QrVSCalOriIMEIMisturou: TLargeintField;
    QrVSCalOriIMEIFornecMO: TLargeintField;
    QrVSCalOriIMEICustoMOKg: TFloatField;
    QrVSCalOriIMEICustoMOM2: TFloatField;
    QrVSCalOriIMEICustoMOTot: TFloatField;
    QrVSCalOriIMEIValorMP: TFloatField;
    QrVSCalOriIMEIDstMovID: TLargeintField;
    QrVSCalOriIMEIDstNivel1: TLargeintField;
    QrVSCalOriIMEIDstNivel2: TLargeintField;
    QrVSCalOriIMEIDstGGX: TLargeintField;
    QrVSCalOriIMEIQtdGerPeca: TFloatField;
    QrVSCalOriIMEIQtdGerPeso: TFloatField;
    QrVSCalOriIMEIQtdGerArM2: TFloatField;
    QrVSCalOriIMEIQtdGerArP2: TFloatField;
    QrVSCalOriIMEIQtdAntPeca: TFloatField;
    QrVSCalOriIMEIQtdAntPeso: TFloatField;
    QrVSCalOriIMEIQtdAntArM2: TFloatField;
    QrVSCalOriIMEIQtdAntArP2: TFloatField;
    QrVSCalOriIMEINotaMPAG: TFloatField;
    QrVSCalOriIMEINO_PALLET: TWideStringField;
    QrVSCalOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSCalOriIMEINO_TTW: TWideStringField;
    QrVSCalOriIMEIID_TTW: TLargeintField;
    QrVSCalOriIMEINO_FORNECE: TWideStringField;
    QrVSCalOriIMEINO_SerieFch: TWideStringField;
    QrVSCalOriIMEIReqMovEstq: TLargeintField;
    QrVSCalOriPalletPallet: TLargeintField;
    QrVSCalOriPalletGraGruX: TLargeintField;
    QrVSCalOriPalletPecas: TFloatField;
    QrVSCalOriPalletAreaM2: TFloatField;
    QrVSCalOriPalletAreaP2: TFloatField;
    QrVSCalOriPalletPesoKg: TFloatField;
    QrVSCalOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSCalOriPalletNO_Pallet: TWideStringField;
    QrVSCalOriPalletSerieFch: TLargeintField;
    QrVSCalOriPalletFicha: TLargeintField;
    QrVSCalOriPalletNO_TTW: TWideStringField;
    QrVSCalOriPalletID_TTW: TLargeintField;
    QrVSCalOriPalletSeries_E_Fichas: TWideStringField;
    QrVSCalOriPalletTerceiro: TLargeintField;
    QrVSCalOriPalletMarca: TWideStringField;
    QrVSCalOriPalletNO_FORNECE: TWideStringField;
    QrVSCalOriPalletNO_SerieFch: TWideStringField;
    QrVSCalOriPalletValorT: TFloatField;
    QrVSCalOriPalletSdoVrtPeca: TFloatField;
    QrVSCalOriPalletSdoVrtPeso: TFloatField;
    QrVSCalOriPalletSdoVrtArM2: TFloatField;
    QrVSCalDstCodigo: TLargeintField;
    QrVSCalDstControle: TLargeintField;
    QrVSCalDstMovimCod: TLargeintField;
    QrVSCalDstMovimNiv: TLargeintField;
    QrVSCalDstMovimTwn: TLargeintField;
    QrVSCalDstEmpresa: TLargeintField;
    QrVSCalDstTerceiro: TLargeintField;
    QrVSCalDstCliVenda: TLargeintField;
    QrVSCalDstMovimID: TLargeintField;
    QrVSCalDstDataHora: TDateTimeField;
    QrVSCalDstPallet: TLargeintField;
    QrVSCalDstGraGruX: TLargeintField;
    QrVSCalDstPecas: TFloatField;
    QrVSCalDstPesoKg: TFloatField;
    QrVSCalDstAreaM2: TFloatField;
    QrVSCalDstAreaP2: TFloatField;
    QrVSCalDstValorT: TFloatField;
    QrVSCalDstSrcMovID: TLargeintField;
    QrVSCalDstSrcNivel1: TLargeintField;
    QrVSCalDstSrcNivel2: TLargeintField;
    QrVSCalDstSrcGGX: TLargeintField;
    QrVSCalDstSdoVrtPeca: TFloatField;
    QrVSCalDstSdoVrtPeso: TFloatField;
    QrVSCalDstSdoVrtArM2: TFloatField;
    QrVSCalDstObserv: TWideStringField;
    QrVSCalDstSerieFch: TLargeintField;
    QrVSCalDstFicha: TLargeintField;
    QrVSCalDstMisturou: TLargeintField;
    QrVSCalDstFornecMO: TLargeintField;
    QrVSCalDstCustoMOKg: TFloatField;
    QrVSCalDstCustoMOM2: TFloatField;
    QrVSCalDstCustoMOTot: TFloatField;
    QrVSCalDstValorMP: TFloatField;
    QrVSCalDstDstMovID: TLargeintField;
    QrVSCalDstDstNivel1: TLargeintField;
    QrVSCalDstDstNivel2: TLargeintField;
    QrVSCalDstDstGGX: TLargeintField;
    QrVSCalDstQtdGerPeca: TFloatField;
    QrVSCalDstQtdGerPeso: TFloatField;
    QrVSCalDstQtdGerArM2: TFloatField;
    QrVSCalDstQtdGerArP2: TFloatField;
    QrVSCalDstQtdAntPeca: TFloatField;
    QrVSCalDstQtdAntPeso: TFloatField;
    QrVSCalDstQtdAntArM2: TFloatField;
    QrVSCalDstQtdAntArP2: TFloatField;
    QrVSCalDstNotaMPAG: TFloatField;
    QrVSCalDstNO_PALLET: TWideStringField;
    QrVSCalDstNO_PRD_TAM_COR: TWideStringField;
    QrVSCalDstNO_TTW: TWideStringField;
    QrVSCalDstID_TTW: TLargeintField;
    QrVSCalDstNO_FORNECE: TWideStringField;
    QrVSCalDstNO_SerieFch: TWideStringField;
    QrVSCalDstReqMovEstq: TLargeintField;
    QrVSCalDstPedItsFin: TLargeintField;
    QrVSCalDstMarca: TWideStringField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    Label60: TLabel;
    DBEdit45: TDBEdit;
    N4: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    QrVSCalAtuNO_FORNEC_MO: TWideStringField;
    Localdoestoque1: TMenuItem;
    Cliente1: TMenuItem;
    QrVSCalDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSCalAtuClientMO: TLargeintField;
    EmitePesagem1: TMenuItem;
    N5: TMenuItem;
    BtPesagem: TBitBtn;
    QrVSCalOriIMEICustoPQ: TFloatField;
    TabSheet3: TTabSheet;
    QrEmit: TmySQLQuery;
    DsEmit: TDataSource;
    DBGrid4: TDBGrid;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    PB1: TProgressBar;
    QrVSCalAtuCUSTO_KG: TFloatField;
    Label31: TLabel;
    DBEdit24: TDBEdit;
    QrVSCalAtuCustoPQ: TFloatField;
    QrVSCalDstNO_MovimNiv: TWideStringField;
    QrVSCalDstNO_MovimID: TWideStringField;
    BtSubProduto: TBitBtn;
    PMSubProduto: TPopupMenu;
    IncluiSubProduto1: TMenuItem;
    AlteraSubProduto1: TMenuItem;
    ExcluiSubProduto1: TMenuItem;
    QrVSSubPrdIts: TmySQLQuery;
    QrVSSubPrdItsCodigo: TLargeintField;
    QrVSSubPrdItsControle: TLargeintField;
    QrVSSubPrdItsMovimCod: TLargeintField;
    QrVSSubPrdItsMovimNiv: TLargeintField;
    QrVSSubPrdItsMovimTwn: TLargeintField;
    QrVSSubPrdItsEmpresa: TLargeintField;
    QrVSSubPrdItsTerceiro: TLargeintField;
    QrVSSubPrdItsCliVenda: TLargeintField;
    QrVSSubPrdItsMovimID: TLargeintField;
    QrVSSubPrdItsDataHora: TDateTimeField;
    QrVSSubPrdItsPallet: TLargeintField;
    QrVSSubPrdItsGraGruX: TLargeintField;
    QrVSSubPrdItsPecas: TFloatField;
    QrVSSubPrdItsPesoKg: TFloatField;
    QrVSSubPrdItsAreaM2: TFloatField;
    QrVSSubPrdItsAreaP2: TFloatField;
    QrVSSubPrdItsValorT: TFloatField;
    QrVSSubPrdItsSrcMovID: TLargeintField;
    QrVSSubPrdItsSrcNivel1: TLargeintField;
    QrVSSubPrdItsSrcNivel2: TLargeintField;
    QrVSSubPrdItsSrcGGX: TLargeintField;
    QrVSSubPrdItsSdoVrtPeca: TFloatField;
    QrVSSubPrdItsSdoVrtPeso: TFloatField;
    QrVSSubPrdItsSdoVrtArM2: TFloatField;
    QrVSSubPrdItsObserv: TWideStringField;
    QrVSSubPrdItsSerieFch: TLargeintField;
    QrVSSubPrdItsFicha: TLargeintField;
    QrVSSubPrdItsMisturou: TLargeintField;
    QrVSSubPrdItsFornecMO: TLargeintField;
    QrVSSubPrdItsCustoMOKg: TFloatField;
    QrVSSubPrdItsCustoMOM2: TFloatField;
    QrVSSubPrdItsCustoMOTot: TFloatField;
    QrVSSubPrdItsValorMP: TFloatField;
    QrVSSubPrdItsDstMovID: TLargeintField;
    QrVSSubPrdItsDstNivel1: TLargeintField;
    QrVSSubPrdItsDstNivel2: TLargeintField;
    QrVSSubPrdItsDstGGX: TLargeintField;
    QrVSSubPrdItsQtdGerPeca: TFloatField;
    QrVSSubPrdItsQtdGerPeso: TFloatField;
    QrVSSubPrdItsQtdGerArM2: TFloatField;
    QrVSSubPrdItsQtdGerArP2: TFloatField;
    QrVSSubPrdItsQtdAntPeca: TFloatField;
    QrVSSubPrdItsQtdAntPeso: TFloatField;
    QrVSSubPrdItsQtdAntArM2: TFloatField;
    QrVSSubPrdItsQtdAntArP2: TFloatField;
    QrVSSubPrdItsNotaMPAG: TFloatField;
    QrVSSubPrdItsNO_FORNECE: TWideStringField;
    QrVSSubPrdItsNO_PRD_TAM_COR: TWideStringField;
    QrVSSubPrdItsID_TTW: TLargeintField;
    QrVSSubPrdItsNO_TTW: TWideStringField;
    DsVSSubPrdIts: TDataSource;
    DBGrid1: TDBGrid;
    QrVSCalOriIMEIVSMulFrnCab: TLargeintField;
    QrVSCalOriIMEIClientMO: TLargeintField;
    IncluiSubProdutorateandoentretodasorigens1: TMenuItem;
    CourosemprocessoBH1: TMenuItem;
    Label57: TLabel;
    EdSerieRem: TdmkEdit;
    Label59: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    QrVSCalCabSerieRem: TSmallintField;
    irparajaneladomovimento1: TMenuItem;
    Corrigepesodebaixa1: TMenuItem;
    PMPesagem: TPopupMenu;
    EmitePesagem2: TMenuItem;
    MenuItem1: TMenuItem;
    Reimprimereceita1: TMenuItem;
    N8: TMenuItem;
    ExcluiPesagem1: TMenuItem;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    N7: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    Recalculacusto1: TMenuItem;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    TabSheet4: TTabSheet;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    QrVSCalCabEmitGru: TIntegerField;
    QrVSCalCabValorTDst: TFloatField;
    QrVSCalCabKgCouPQ: TFloatField;
    QrVSCalCabGGXDst: TIntegerField;
    QrVSCalCabNFeStatus: TIntegerField;
    QrVSCalCabGGXSrc: TIntegerField;
    QrVSCalCabOperacoes: TIntegerField;
    QrVSCalCabVSCOPCab: TIntegerField;
    QrVSCalOriIMEIRmsMovID: TLargeintField;
    QrVSCalOriIMEIRmsNivel1: TLargeintField;
    QrVSCalOriIMEIRmsNivel2: TLargeintField;
    ItsExcluiOriIMEI2: TMenuItem;
    QrEmitDtCorrApo_TXT: TWideStringField;
    QrEmitDtaBaixa: TDateField;
    QrEmitDtaBaixa_TXT: TWideStringField;
    QrEmitDtCorrApo: TDateTimeField;
    QrVSCalInd: TmySQLQuery;
    QrVSCalIndCodigo: TLargeintField;
    QrVSCalIndControle: TLargeintField;
    QrVSCalIndMovimCod: TLargeintField;
    QrVSCalIndMovimNiv: TLargeintField;
    QrVSCalIndMovimTwn: TLargeintField;
    QrVSCalIndEmpresa: TLargeintField;
    QrVSCalIndTerceiro: TLargeintField;
    QrVSCalIndCliVenda: TLargeintField;
    QrVSCalIndMovimID: TLargeintField;
    QrVSCalIndDataHora: TDateTimeField;
    QrVSCalIndPallet: TLargeintField;
    QrVSCalIndGraGruX: TLargeintField;
    QrVSCalIndPecas: TFloatField;
    QrVSCalIndPesoKg: TFloatField;
    QrVSCalIndAreaM2: TFloatField;
    QrVSCalIndAreaP2: TFloatField;
    QrVSCalIndValorT: TFloatField;
    QrVSCalIndSrcMovID: TLargeintField;
    QrVSCalIndSrcNivel1: TLargeintField;
    QrVSCalIndSrcNivel2: TLargeintField;
    QrVSCalIndSrcGGX: TLargeintField;
    QrVSCalIndSdoVrtPeca: TFloatField;
    QrVSCalIndSdoVrtPeso: TFloatField;
    QrVSCalIndSdoVrtArM2: TFloatField;
    QrVSCalIndObserv: TWideStringField;
    QrVSCalIndSerieFch: TLargeintField;
    QrVSCalIndFicha: TLargeintField;
    QrVSCalIndMisturou: TLargeintField;
    QrVSCalIndFornecMO: TLargeintField;
    QrVSCalIndCustoMOKg: TFloatField;
    QrVSCalIndCustoMOM2: TFloatField;
    QrVSCalIndCustoMOTot: TFloatField;
    QrVSCalIndValorMP: TFloatField;
    QrVSCalIndDstMovID: TLargeintField;
    QrVSCalIndDstNivel1: TLargeintField;
    QrVSCalIndDstNivel2: TLargeintField;
    QrVSCalIndDstGGX: TLargeintField;
    QrVSCalIndQtdGerPeca: TFloatField;
    QrVSCalIndQtdGerPeso: TFloatField;
    QrVSCalIndQtdGerArM2: TFloatField;
    QrVSCalIndQtdGerArP2: TFloatField;
    QrVSCalIndQtdAntPeca: TFloatField;
    QrVSCalIndQtdAntPeso: TFloatField;
    QrVSCalIndQtdAntArM2: TFloatField;
    QrVSCalIndQtdAntArP2: TFloatField;
    QrVSCalIndNotaMPAG: TFloatField;
    QrVSCalIndNO_PALLET: TWideStringField;
    QrVSCalIndNO_PRD_TAM_COR: TWideStringField;
    QrVSCalIndNO_TTW: TWideStringField;
    QrVSCalIndID_TTW: TLargeintField;
    QrVSCalIndNO_FORNECE: TWideStringField;
    QrVSCalIndNO_SerieFch: TWideStringField;
    QrVSCalIndReqMovEstq: TLargeintField;
    QrVSCalIndPedItsFin: TLargeintField;
    QrVSCalIndMarca: TWideStringField;
    QrVSCalIndStqCenLoc: TLargeintField;
    QrVSCalIndNO_MovimNiv: TWideStringField;
    QrVSCalIndNO_MovimID: TWideStringField;
    DsVSCalInd: TDataSource;
    DBGrid5: TDBGrid;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    SbStqCenLoc: TSpeedButton;
    Datadeabertura1: TMenuItem;
    QrVSCalOriIMEIMarca: TWideStringField;
    Atualizacusto1: TMenuItem;
    AdicionaIMEIParcialPeso1: TMenuItem;
    AdicionaIMEITotalPeso1: TMenuItem;
    N3: TMenuItem;
    N6: TMenuItem;
    N9: TMenuItem;
    ItsIncluiMul: TMenuItem;
    ItsIncluiUni: TMenuItem;
    Rtuloderastreabilidade1: TMenuItem;
    Desteprocesso1: TMenuItem;
    Qualquerprocesso1: TMenuItem;
    QrVSCalAtuCouNiv2: TIntegerField;
    QrVSSubPrdItsSeqSifDipoa: TLargeintField;
    QrVSCalDstSeqSifDipoa: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCalCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCalCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCalCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSCalCabBeforeClose(DataSet: TDataSet);
    procedure QrVSCalCabCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmProcesso1Click(Sender: TObject);
    procedure QrVSCalDstBeforeClose(DataSet: TDataSet);
    procedure QrVSCalDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure Total2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Localdoestoque1Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure EmitePesagem1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure Reimprimereceita1Click(Sender: TObject);
    procedure ExcluiPesagem1Click(Sender: TObject);
    procedure BtSubProdutoClick(Sender: TObject);
    procedure PMSubProdutoPopup(Sender: TObject);
    procedure IncluiSubProduto1Click(Sender: TObject);
    procedure AlteraSubProduto1Click(Sender: TObject);
    procedure ExcluiSubProduto1Click(Sender: TObject);
    procedure QrVSCalOriIMEIAfterScroll(DataSet: TDataSet);
    procedure QrVSCalOriIMEIBeforeClose(DataSet: TDataSet);
    procedure IncluiSubProdutorateandoentretodasorigens1Click(Sender: TObject);
    procedure CourosemprocessoBH1Click(Sender: TObject);
    procedure irparajaneladomovimento1Click(Sender: TObject);
    procedure Corrigepesodebaixa1Click(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure ItsExcluiOriIMEI2Click(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure Datadeabertura1Click(Sender: TObject);
    procedure Atualizacusto1Click(Sender: TObject);
    procedure AdicionaIMEITotalPeso1Click(Sender: TObject);
    procedure AdicionaIMEIParcialPeso1Click(Sender: TObject);
    procedure ItsIncluiMulClick(Sender: TObject);
    procedure Rtuloderastreabilidade1Click(Sender: TObject);
    procedure Desteprocesso1Click(Sender: TObject);
    procedure Qualquerprocesso1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure ItsIncluiUniClick(Sender: TObject);
  private
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSCalOriIMEI_Peca(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSCalOriIMEI_Peso(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSCalOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSCalDst1(SQLType: TSQLType);
    procedure MostraVSCalDst2(SQLType: TSQLType);
    procedure MostraVSCalDstMul(SQLType: TSQLType);
    procedure InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
              GraGruX, CliVenda: Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSCalOris(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
    procedure PesquisaArtigoJaFeito(Key: Word);
    procedure ReopenVSSubPrdIts(Controle: Integer);
    procedure RecalculaCusto();
  //
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEmit();
  end;

var
  FmVSCalCab: TFmVSCalCab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, ModuleGeral, VSCalDst, UnVS_PF, UnVS_CRC_PF,
  VSCalOriIMEI_Peca, VSCalOriIMEI_Peso, AppListas, UnGrade_PF, ModVS, UnPQ_PF,
  UnEntities, ModVS_CRC, VSCalDst2, VSCalDstMul, UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCalCab.Localdoestoque1Click(Sender: TObject);
begin
  if VS_PF.AlteraVMI_StqCenLoc(QrVSCalAtuControle.Value,
  QrVSCalAtuStqCenLoc.Value) then
    LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCalCab.MostraVSCalDst1(SQLType: TSQLType);
  function CalculoValorM2(): Double;
  begin
    if QrVSCalAtuAreaM2.Value > 0 then
      Result := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
    else
      Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmVSCalDst, FmVSCalDst, afmoNegarComAviso) then
  begin
    FmVSCalDst.ImgTipo.SQLType := SQLType;
    FmVSCalDst.FQrCab := QrVSCalCab;
    FmVSCalDst.FDsCab := DsVSCalCab;
    FmVSCalDst.FQrIts := QrVSCalDst;
    FmVSCalDst.FCouNiv2EmProc := QrVSCalAtuCouNiv2.Value;
    FmVSCalDst.ReopenGraGruX();
    //
    //FmVSCalDst.FDataHora    := QrVSCalCabDtHrAberto.Value;
    FmVSCalDst.FEmpresa     := QrVSCalCabEmpresa.Value;
    FmVSCalDst.FClientMO    := QrVSCalAtuClientMO.Value;
    FmVSCalDst.FFornecMO    := QrVSCalAtuFornecMO.Value;
    FmVSCalDst.FValM2       := CalculoValorM2();
    //FmVSCalDst.FFatorIntSrc := QrVSCalAtuFatorInt.Value;
    FmVSCalDst.FGraGruXSrc  := QrVSCalAtuGraGruX.Value;
    //
    //FmVSCalDst.EdCustoMOM2.ValueVariant := QrVSCalAtuCustoMOM2.Value;
    FmVSCalDst.EdCustoMOKg.ValueVariant := QrVSCalAtuCustoMOKg.Value;
    FmVSCalDst.FVmiPai := QrVSCalAtuControle.Value;
    if SQLType = stIns then
    begin
      FmVSCalDst.FSdoPecas  := QrVSCalAtuSdoVrtPeca.Value;
      FmVSCalDst.FSdoPesoKg := QrVSCalAtuSdoVrtPeso.Value;
      FmVSCalDst.FSdoReaM2  := QrVSCalAtuSdoVrtArM2.Value;
      //
      if  QrVSCalAtuPesoKg.Value <> 0 then
        FmVSCalDst.FCustoKg := QrVSCalAtuValorT.Value / QrVSCalAtuPesoKg.Value
      else
        FmVSCalDst.FCustoKg := 0;
      //
      if  QrVSCalAtuAreaM2.Value <> 0 then
        FmVSCalDst.FCustoM2 := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
      else
        FmVSCalDst.FCustoM2 := 0;
      //
      //FmVSCalDst.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSCalDst.QrVSPedIts, QrVSCalAtuControle.Value,
      QrVSCalAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSCalDst.EdPedItsFin.ValueVariant := QrVSCalAtuPedItsLib.Value;
      FmVSCalDst.CBPedItsFin.KeyValue     := QrVSCalAtuPedItsLib.Value;
      //
      //FmVSCalDst.TPData.Date              := QrVSCalCabDtHrAberto.Value;
      //FmVSCalDst.EdHora.ValueVariant      := QrVSCalCabDtHrAberto.Value;
      FmVSCalDst.EdSerieFch.ValueVariant := QrVSCalOriIMEISerieFch.Value;
      FmVSCalDst.CBSerieFch.KeyValue     := QrVSCalOriIMEISerieFch.Value;
      FmVSCalDst.EdFicha.ValueVariant    := QrVSCalOriIMEIFicha.Value;
      FmVSCalDst.EdMarca.ValueVariant    := QrVSCalOriIMEIMarca.Value;
    end else
    begin
      FmVSCalDst.FSdoPecas  := 0;
      FmVSCalDst.FSdoPesoKg := 0;
      FmVSCalDst.FSdoReaM2  := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSCalDst.QrVSPedIts, QrVSCalAtuControle.Value,
       QrVSCalDstControle.Value);
      FmVSCalDst.EdCtrl1.ValueVariant      := QrVSCalDstControle.Value;
      //FmVSCalDst.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSCalDstMovimCod.Value, QrVSCalDstMovimTwn.Value, emidEmProcCal,
        //eminEmCalBxa);
      FmVSCalDst.EdMovimTwn.ValueVariant   := QrVSCalDstMovimTwn.Value;
      FmVSCalDst.EdGragruX.ValueVariant    := QrVSCalDstGraGruX.Value;
      FmVSCalDst.CBGragruX.KeyValue        := QrVSCalDstGraGruX.Value;
      FmVSCalDst.EdPecas.ValueVariant      := QrVSCalDstPecas.Value;
      FmVSCalDst.EdPesoKg.ValueVariant     := QrVSCalDstPesoKg.Value;
      FmVSCalDst.EdAreaM2.ValueVariant     := QrVSCalDstAreaM2.Value;
      FmVSCalDst.EdAreaP2.ValueVariant     := QrVSCalDstAreaP2.Value;
      FmVSCalDst.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      FmVSCalDst.EdObserv.ValueVariant     := QrVSCalDstObserv.Value;
      FmVSCalDst.EdSerieFch.ValueVariant   := QrVSCalDstSerieFch.Value;
      FmVSCalDst.CBSerieFch.KeyValue       := QrVSCalDstSerieFch.Value;
      FmVSCalDst.EdFicha.ValueVariant      := QrVSCalDstFicha.Value;
      FmVSCalDst.EdMarca.ValueVariant      := QrVSCalDstMarca.Value;
      //FmVSCalDst.RGMisturou.ItemIndex    := QrVSCalDstMisturou.Value;
      FmVSCalDst.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDst.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      //
      FmVSCalDst.TPData.Date               := QrVSCalDstDataHora.Value;
      FmVSCalDst.EdHora.ValueVariant       := QrVSCalDstDataHora.Value;
      FmVSCalDst.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDst.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      FmVSCalDst.EdPallet.ValueVariant     := QrVSCalDstPallet.Value;
      FmVSCalDst.CBPallet.KeyValue         := QrVSCalDstPallet.Value;
      FmVSCalDst.EdStqCenLoc.ValueVariant  := QrVSCalDstStqCenLoc.Value;
      FmVSCalDst.CBStqCenLoc.KeyValue      := QrVSCalDstStqCenLoc.Value;
      FmVSCalDst.EdReqMovEstq.ValueVariant := QrVSCalDstReqMovEstq.Value;
      //FmVSCalDst.EdCustoMOM2.ValueVariant  := QrVSCalDstCustoMOM2.Value;
      FmVSCalDst.EdCustoMOkg.ValueVariant  := QrVSCalDstCustoMOKg.Value;
      FmVSCalDst.EdCustoMOTot.ValueVariant := QrVSCalDstCustoMOTot.Value;
      FmVSCalDst.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      //FmVSCalDst.EdRendimento.ValueVariant := QrVSCalDstRendimento.Value;
      //
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDst.EdGraGruX.Enabled        := False;
        FmVSCalDst.CBGraGruX.Enabled        := False;
      end;
(*
      if QrVSCalBxa.RecordCount > 0 then
      begin
        FmVSCalDst.CkBaixa.Checked          := True;
        FmVSCalDst.EdBxaPecas.ValueVariant  := -QrVSCalBxaPecas.Value;
        FmVSCalDst.EdBxaPesoKg.ValueVariant := -QrVSCalBxaPesoKg.Value;
        FmVSCalDst.EdBxaAreaM2.ValueVariant := -QrVSCalBxaAreaM2.Value;
        FmVSCalDst.EdBxaAreaP2.ValueVariant := -QrVSCalBxaAreaP2.Value;
        FmVSCalDst.EdBxaValorT.ValueVariant := -QrVSCalBxaValorT.Value;
        FmVSCalDst.EdCtrl2.ValueVariant     := QrVSCalBxaControle.Value;
        FmVSCalDst.EdBxaObserv.ValueVariant := QrVSCalBxaObserv.Value;
      end;
*)
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDst.EdGraGruX.Enabled  := False;
        FmVSCalDst.CBGraGruX.Enabled  := False;
        FmVSCalDst.EdSerieFch.Enabled := False;
        FmVSCalDst.CBSerieFch.Enabled := False;
        FmVSCalDst.EdFicha.Enabled    := False;
      end;
    end;
    //
    (* Liberar dar� problema no Kardex?
    FmVSCalDst.LaBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDst.EdBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDst.LaBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst.EdBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst.LaBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst.EdBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    *)
    //
    FmVSCalDst.ShowModal;
    FmVSCalDst.Destroy;
    //
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCalCab.MostraVSCalDst2(SQLType: TSQLType);
  function CalculoValorM2(): Double;
  begin
    if QrVSCalAtuAreaM2.Value > 0 then
      Result := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
    else
      Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmVSCalDst2, FmVSCalDst2, afmoNegarComAviso) then
  begin
    FmVSCalDst2.ImgTipo.SQLType := SQLType;
    FmVSCalDst2.FQrCab := QrVSCalCab;
    FmVSCalDst2.FDsCab := DsVSCalCab;
    FmVSCalDst2.FQrIts := QrVSCalDst;
    //
    FmVSCalDst2.FSrcMovID := TEstqMovimID(QrVSCalOriIMEIMovimID.Value);
    FmVSCalDst2.FSrcNivel1 := QrVSCalOriIMEICodigo.Value;
    FmVSCalDst2.FSrcNivel2 := QrVSCalOriIMEIControle.Value;
    //
    //FmVSCalDst2.FDataHora    := QrVSCalCabDtHrAberto.Value;
    FmVSCalDst2.FEmpresa     := QrVSCalCabEmpresa.Value;
    FmVSCalDst2.FClientMO    := QrVSCalAtuClientMO.Value;
    FmVSCalDst2.FFornecMO    := QrVSCalAtuFornecMO.Value;
    FmVSCalDst2.FValM2       := CalculoValorM2();
    //FmVSCalDst2.FFatorIntSrc := QrVSCalAtuFatorInt.Value;
    FmVSCalDst2.FGraGruXSrc  := QrVSCalAtuGraGruX.Value;
    //
    //FmVSCalDst2.EdCustoMOM2.ValueVariant := QrVSCalAtuCustoMOM2.Value;
    FmVSCalDst2.EdCustoMOKg.ValueVariant := QrVSCalAtuCustoMOKg.Value;
    FmVSCalDst2.FVmiPai := QrVSCalAtuControle.Value;
    if SQLType = stIns then
    begin
      (*
      FmVSCalDst2.FSdoPecas  := QrVSCalAtuSdoVrtPeca.Value;
      FmVSCalDst2.FSdoPesoKg := QrVSCalAtuSdoVrtPeso.Value;
      FmVSCalDst2.FSdoAreaM2  := QrVSCalAtuSdoVrtArM2.Value;
      *)
      FmVSCalDst2.FSdoPecas := QrVSCalOriIMEISdoVrtPeca.Value;
      FmVSCalDst2.FSdoPesoKg := QrVSCalOriIMEISdoVrtPeso.Value;
      FmVSCalDst2.FSdoAreaM2 := QrVSCalOriIMEISdoVrtArM2.Value;
      //
      if  QrVSCalAtuPesoKg.Value <> 0 then
        FmVSCalDst2.FCustoKg := QrVSCalAtuValorT.Value / QrVSCalAtuPesoKg.Value
      else
        FmVSCalDst2.FCustoKg := 0;
      //
      if  QrVSCalAtuAreaM2.Value <> 0 then
        FmVSCalDst2.FCustoM2 := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
      else
        FmVSCalDst2.FCustoM2 := 0;
      //
      //FmVSCalDst2.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSCalDst2.QrVSPedIts, QrVSCalAtuControle.Value,
      QrVSCalAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSCalDst2.EdPedItsFin.ValueVariant := QrVSCalAtuPedItsLib.Value;
      FmVSCalDst2.CBPedItsFin.KeyValue     := QrVSCalAtuPedItsLib.Value;
      //
      //FmVSCalDst2.TPData.Date              := QrVSCalCabDtHrAberto.Value;
      //FmVSCalDst2.EdHora.ValueVariant      := QrVSCalCabDtHrAberto.Value;
    end else
    begin
      FmVSCalDst2.FSdoPecas  := 0;
      FmVSCalDst2.FSdoPesoKg := 0;
      FmVSCalDst2.FSdoAreaM2 := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSCalDst2.QrVSPedIts, QrVSCalAtuControle.Value,
       QrVSCalDstControle.Value);
      FmVSCalDst2.EdCtrl1.ValueVariant      := QrVSCalDstControle.Value;
      //FmVSCalDst2.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSCalDstMovimCod.Value, QrVSCalDstMovimTwn.Value, emidEmProcCal,
        //eminEmCalBxa);
      FmVSCalDst2.EdMovimTwn.ValueVariant   := QrVSCalDstMovimTwn.Value;
      FmVSCalDst2.EdGragruX.ValueVariant    := QrVSCalDstGraGruX.Value;
      FmVSCalDst2.CBGragruX.KeyValue        := QrVSCalDstGraGruX.Value;
      FmVSCalDst2.EdPecas.ValueVariant      := QrVSCalDstPecas.Value;
      FmVSCalDst2.EdPesoKg.ValueVariant     := QrVSCalDstPesoKg.Value;
      FmVSCalDst2.EdAreaM2.ValueVariant     := QrVSCalDstAreaM2.Value;
      FmVSCalDst2.EdAreaP2.ValueVariant     := QrVSCalDstAreaP2.Value;
      FmVSCalDst2.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      FmVSCalDst2.EdObserv.ValueVariant     := QrVSCalDstObserv.Value;
      FmVSCalDst2.EdSerieFch.ValueVariant   := QrVSCalDstSerieFch.Value;
      FmVSCalDst2.CBSerieFch.KeyValue       := QrVSCalDstSerieFch.Value;
      FmVSCalDst2.EdFicha.ValueVariant      := QrVSCalDstFicha.Value;
      FmVSCalDst2.EdMarca.ValueVariant      := QrVSCalDstMarca.Value;
      //FmVSCalDst2.RGMisturou.ItemIndex    := QrVSCalDstMisturou.Value;
      FmVSCalDst2.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDst2.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      //
      FmVSCalDst2.TPData.Date               := QrVSCalDstDataHora.Value;
      FmVSCalDst2.EdHora.ValueVariant       := QrVSCalDstDataHora.Value;
      FmVSCalDst2.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDst2.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      FmVSCalDst2.EdPallet.ValueVariant     := QrVSCalDstPallet.Value;
      FmVSCalDst2.CBPallet.KeyValue         := QrVSCalDstPallet.Value;
      FmVSCalDst2.EdStqCenLoc.ValueVariant  := QrVSCalDstStqCenLoc.Value;
      FmVSCalDst2.CBStqCenLoc.KeyValue      := QrVSCalDstStqCenLoc.Value;
      FmVSCalDst2.EdReqMovEstq.ValueVariant := QrVSCalDstReqMovEstq.Value;
      //FmVSCalDst2.EdCustoMOM2.ValueVariant  := QrVSCalDstCustoMOM2.Value;
      FmVSCalDst2.EdCustoMOkg.ValueVariant  := QrVSCalDstCustoMOKg.Value;
      FmVSCalDst2.EdCustoMOTot.ValueVariant := QrVSCalDstCustoMOTot.Value;
      FmVSCalDst2.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      //FmVSCalDst2.EdRendimento.ValueVariant := QrVSCalDstRendimento.Value;
      //
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDst2.EdGraGruX.Enabled        := False;
        FmVSCalDst2.CBGraGruX.Enabled        := False;
      end;
(*
      if QrVSCalBxa.RecordCount > 0 then
      begin
        FmVSCalDst2.CkBaixa.Checked          := True;
        FmVSCalDst2.EdBxaPecas.ValueVariant  := -QrVSCalBxaPecas.Value;
        FmVSCalDst2.EdBxaPesoKg.ValueVariant := -QrVSCalBxaPesoKg.Value;
        FmVSCalDst2.EdBxaAreaM2.ValueVariant := -QrVSCalBxaAreaM2.Value;
        FmVSCalDst2.EdBxaAreaP2.ValueVariant := -QrVSCalBxaAreaP2.Value;
        FmVSCalDst2.EdBxaValorT.ValueVariant := -QrVSCalBxaValorT.Value;
        FmVSCalDst2.EdCtrl2.ValueVariant     := QrVSCalBxaControle.Value;
        FmVSCalDst2.EdBxaObserv.ValueVariant := QrVSCalBxaObserv.Value;
      end;
*)
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDst2.EdGraGruX.Enabled  := False;
        FmVSCalDst2.CBGraGruX.Enabled  := False;
        FmVSCalDst2.EdSerieFch.Enabled := False;
        FmVSCalDst2.CBSerieFch.Enabled := False;
        FmVSCalDst2.EdFicha.Enabled    := False;
      end;
    end;
    //
    (* Liberar dar� problema no Kardex?
    FmVSCalDst2.LaBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDst2.EdBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDst2.LaBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst2.EdBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst2.LaBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDst2.EdBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    *)
    //
    FmVSCalDst2.ShowModal;
    FmVSCalDst2.Destroy;
    //
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCalCab.MostraVSCalDstMul(SQLType: TSQLType);
  function CalculoValorM2(): Double;
  begin
    if QrVSCalAtuAreaM2.Value > 0 then
      Result := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
    else
      Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmVSCalDstMul, FmVSCalDstMul, afmoNegarComAviso) then
  begin
    FmVSCalDstMul.ImgTipo.SQLType := SQLType;
    FmVSCalDstMul.FQrCab := QrVSCalCab;
    FmVSCalDstMul.FDsCab := DsVSCalCab;
    FmVSCalDstMul.FQrIts := QrVSCalDst;
    FmVSCalDstMul.FCouNiv2EmProc := QrVSCalAtuCouNiv2.Value;
    FmVSCalDstMul.ReopenGraGruX();
    //
    //FmVSCalDstMul.FDataHora    := QrVSCalCabDtHrAberto.Value;
    FmVSCalDstMul.FEmpresa     := QrVSCalCabEmpresa.Value;
    FmVSCalDstMul.FClientMO    := QrVSCalAtuClientMO.Value;
    FmVSCalDstMul.FFornecMO    := QrVSCalAtuFornecMO.Value;
    FmVSCalDstMul.FValM2       := CalculoValorM2();
    //FmVSCalDstMul.FFatorIntSrc := QrVSCalAtuFatorInt.Value;
    FmVSCalDstMul.FGraGruXSrc  := QrVSCalAtuGraGruX.Value;
    //
    (*FmVSCalDstMul.FOrigMovID     := QrVSCalOriIMEIMovimID.Value;
    FmVSCalDstMul.FOrigCodigo    := QrVSCalOriIMEICodigo.Value;
    FmVSCalDstMul.FOrigControle  := QrVSCalOriIMEIControle.Value;
    FmVSCalDstMul.FOrigGGX       := QrVSCalOriIMEIGraGruX.Value;*)

    //
    //FmVSCalDstMul.EdCustoMOM2.ValueVariant := QrVSCalAtuCustoMOM2.Value;
    FmVSCalDstMul.EdCustoMOKg.ValueVariant := QrVSCalAtuCustoMOKg.Value;
    FmVSCalDstMul.FVmiPai := QrVSCalAtuControle.Value;
    if SQLType = stIns then
    begin
      FmVSCalDstMul.FSdoPecas  := QrVSCalAtuSdoVrtPeca.Value;
      FmVSCalDstMul.FSdoPesoKg := QrVSCalAtuSdoVrtPeso.Value;
      FmVSCalDstMul.FSdoReaM2  := QrVSCalAtuSdoVrtArM2.Value;
      //
      if  QrVSCalAtuPesoKg.Value <> 0 then
        FmVSCalDstMul.FCustoKg := QrVSCalAtuValorT.Value / QrVSCalAtuPesoKg.Value
      else
        FmVSCalDstMul.FCustoKg := 0;
      //
      if  QrVSCalAtuAreaM2.Value <> 0 then
        FmVSCalDstMul.FCustoM2 := QrVSCalAtuValorT.Value / QrVSCalAtuAreaM2.Value
      else
        FmVSCalDstMul.FCustoM2 := 0;
      //
      //FmVSCalDstMul.EdCPF1.ReadOnly := False
      VS_PF.ReopenPedItsXXX(FmVSCalDstMul.QrVSPedIts, QrVSCalAtuControle.Value,
      QrVSCalAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSCalDstMul.EdPedItsFin.ValueVariant := QrVSCalAtuPedItsLib.Value;
      FmVSCalDstMul.CBPedItsFin.KeyValue     := QrVSCalAtuPedItsLib.Value;
      //
      FmVSCalDstMul.TPData.Date              := DModG.ObtemAgora();
      FmVSCalDstMul.EdHora.ValueVariant      := DModG.ObtemAgora();
    end else
    begin
{
      FmVSCalDstMul.FSdoPecas  := 0;
      FmVSCalDstMul.FSdoPesoKg := 0;
      FmVSCalDstMul.FSdoReaM2  := 0;
      //
      VS_PF.ReopenPedItsXXX(FmVSCalDstMul.QrVSPedIts, QrVSCalAtuControle.Value,
       QrVSCalDstControle.Value);
      FmVSCalDstMul.EdCtrl1.ValueVariant      := QrVSCalDstControle.Value;
      //FmVSCalDstMul.EdCtrl2.ValueVariant     := VS_PF.ObtemControleMovimTwin(
        //QrVSCalDstMovimCod.Value, QrVSCalDstMovimTwn.Value, emidEmProcCal,
        //eminEmCalBxa);
      FmVSCalDstMul.EdMovimTwn.ValueVariant   := QrVSCalDstMovimTwn.Value;
      FmVSCalDstMul.EdGragruX.ValueVariant    := QrVSCalDstGraGruX.Value;
      FmVSCalDstMul.CBGragruX.KeyValue        := QrVSCalDstGraGruX.Value;
      FmVSCalDstMul.EdPecas.ValueVariant      := QrVSCalDstPecas.Value;
      FmVSCalDstMul.EdPesoKg.ValueVariant     := QrVSCalDstPesoKg.Value;
      FmVSCalDstMul.EdAreaM2.ValueVariant     := QrVSCalDstAreaM2.Value;
      FmVSCalDstMul.EdAreaP2.ValueVariant     := QrVSCalDstAreaP2.Value;
      FmVSCalDstMul.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      FmVSCalDstMul.EdObserv.ValueVariant     := QrVSCalDstObserv.Value;
      FmVSCalDstMul.EdSerieFch.ValueVariant   := QrVSCalDstSerieFch.Value;
      FmVSCalDstMul.CBSerieFch.KeyValue       := QrVSCalDstSerieFch.Value;
      FmVSCalDstMul.EdFicha.ValueVariant      := QrVSCalDstFicha.Value;
      FmVSCalDstMul.EdMarca.ValueVariant      := QrVSCalDstMarca.Value;
      //FmVSCalDstMul.RGMisturou.ItemIndex    := QrVSCalDstMisturou.Value;
      FmVSCalDstMul.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDstMul.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      //
      FmVSCalDstMul.TPData.Date               := QrVSCalDstDataHora.Value;
      FmVSCalDstMul.EdHora.ValueVariant       := QrVSCalDstDataHora.Value;
      FmVSCalDstMul.EdPedItsFin.ValueVariant  := QrVSCalDstPedItsFin.Value;
      FmVSCalDstMul.CBPedItsFin.KeyValue      := QrVSCalDstPedItsFin.Value;
      FmVSCalDstMul.EdPallet.ValueVariant     := QrVSCalDstPallet.Value;
      FmVSCalDstMul.CBPallet.KeyValue         := QrVSCalDstPallet.Value;
      FmVSCalDstMul.EdStqCenLoc.ValueVariant  := QrVSCalDstStqCenLoc.Value;
      FmVSCalDstMul.CBStqCenLoc.KeyValue      := QrVSCalDstStqCenLoc.Value;
      FmVSCalDstMul.EdReqMovEstq.ValueVariant := QrVSCalDstReqMovEstq.Value;
      //FmVSCalDstMul.EdCustoMOM2.ValueVariant  := QrVSCalDstCustoMOM2.Value;
      FmVSCalDstMul.EdCustoMOkg.ValueVariant  := QrVSCalDstCustoMOKg.Value;
      FmVSCalDstMul.EdCustoMOTot.ValueVariant := QrVSCalDstCustoMOTot.Value;
      FmVSCalDstMul.EdValorT.ValueVariant     := QrVSCalDstValorT.Value;
      //FmVSCalDstMul.EdRendimento.ValueVariant := QrVSCalDstRendimento.Value;
      //
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDstMul.EdGraGruX.Enabled        := False;
        FmVSCalDstMul.CBGraGruX.Enabled        := False;
      end;
(*
      if QrVSCalBxa.RecordCount > 0 then
      begin
        FmVSCalDstMul.CkBaixa.Checked          := True;
        FmVSCalDstMul.EdBxaPecas.ValueVariant  := -QrVSCalBxaPecas.Value;
        FmVSCalDstMul.EdBxaPesoKg.ValueVariant := -QrVSCalBxaPesoKg.Value;
        FmVSCalDstMul.EdBxaAreaM2.ValueVariant := -QrVSCalBxaAreaM2.Value;
        FmVSCalDstMul.EdBxaAreaP2.ValueVariant := -QrVSCalBxaAreaP2.Value;
        FmVSCalDstMul.EdBxaValorT.ValueVariant := -QrVSCalBxaValorT.Value;
        FmVSCalDstMul.EdCtrl2.ValueVariant     := QrVSCalBxaControle.Value;
        FmVSCalDstMul.EdBxaObserv.ValueVariant := QrVSCalBxaObserv.Value;
      end;
*)
      if QrVSCalDstSdoVrtPeca.Value < QrVSCalDstPecas.Value then
      begin
        FmVSCalDstMul.EdGraGruX.Enabled  := False;
        FmVSCalDstMul.CBGraGruX.Enabled  := False;
        FmVSCalDstMul.EdSerieFch.Enabled := False;
        FmVSCalDstMul.CBSerieFch.Enabled := False;
        FmVSCalDstMul.EdFicha.Enabled    := False;
      end;
}
    end;
    //
    (* Liberar dar� problema no Kardex?
    FmVSCalDstMul.LaBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDstMul.EdBxaPesoKg.Enabled  := QrVSCalCabPesoKgINI.Value <> 0;
    FmVSCalDstMul.LaBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDstMul.EdBxaAreaM2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDstMul.LaBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    FmVSCalDstMul.EdBxaAreaP2.Enabled  := QrVSCalCabAreaINIM2.Value <> 0;
    *)
    //
    FmVSCalDstMul.ShowModal;
    FmVSCalDstMul.Destroy;
    //
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCalCab.MostraVSCalOriIMEI_Peca(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSCalOriIMEI_Peca, FmVSCalOriIMEI_Peca, afmoNegarComAviso) then
  begin
    FmVSCalOriIMEI_Peca.ImgTipo.SQLType := SQLType;
    FmVSCalOriIMEI_Peca.FQrCab          := QrVSCalCab;
    FmVSCalOriIMEI_Peca.FDsCab          := DsVSCalCab;
    FmVSCalOriIMEI_Peca.FEmpresa        := QrVSCalCabEmpresa.Value;
    FmVSCalOriIMEI_Peca.FTipoArea       := QrVSCalCabTipoArea.Value;
    FmVSCalOriIMEI_Peca.FOrigMovimNiv   := QrVSCalAtuMovimNiv.Value;
    FmVSCalOriIMEI_Peca.FOrigMovimCod   := QrVSCalAtuMovimCod.Value;
    FmVSCalOriIMEI_Peca.FOrigCodigo     := QrVSCalAtuCodigo.Value;
    FmVSCalOriIMEI_Peca.FNewGraGruX     := QrVSCalAtuGraGruX.Value;
    //
    FmVSCalOriIMEI_Peca.EdCodigo.ValueVariant    := QrVSCalCabCodigo.Value;
    FmVSCalOriIMEI_Peca.EdMovimCod.ValueVariant  := QrVSCalCabMovimCod.Value;
    FmVSCalOriIMEI_Peca.FDataHora                := QrVSCalCabDtHrAberto.Value;
    //
    FmVSCalOriIMEI_Peca.EdSrcMovID.ValueVariant  := QrVSCalAtuMovimID.Value;
    FmVSCalOriIMEI_Peca.EdSrcNivel1.ValueVariant := QrVSCalAtuCodigo.Value;
    FmVSCalOriIMEI_Peca.EdSrcNivel2.ValueVariant := QrVSCalAtuControle.Value;
    FmVSCalOriIMEI_Peca.EdSrcGGX.ValueVariant    := QrVSCalAtuGraGruX.Value;
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSCalOriIMEI_Peca.EdControle.ValueVariant := QrVSCalCabOldControle.Value;
      //
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSCalOriIMEI_Peca.FParcial := True;
        FmVSCalOriIMEI_Peca.DBG04Estq.Options := FmVSCalOriIMEI_Peca.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSCalOriIMEI_Peca.FParcial := False;
        FmVSCalOriIMEI_Peca.DBG04Estq.Options := FmVSCalOriIMEI_Peca.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSCalOriIMEI_Peca.FParcial := False;
        FmVSCalOriIMEI_Peca.DBG04Estq.Options := FmVSCalOriIMEI_Peca.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSCalOriIMEI_Peca.ReopenItensAptos();
    FmVSCalOriIMEI_Peca.ShowModal;
    FmVSCalOriIMEI_Peca.Destroy;
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCalCab.MostraVSCalOriIMEI_Peso(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSCalOriIMEI_Peso, FmVSCalOriIMEI_Peso, afmoNegarComAviso) then
  begin
    FmVSCalOriIMEI_Peso.ImgTipo.SQLType := SQLType;
    FmVSCalOriIMEI_Peso.FQrCab          := QrVSCalCab;
    FmVSCalOriIMEI_Peso.FDsCab          := DsVSCalCab;
    FmVSCalOriIMEI_Peso.FEmpresa        := QrVSCalCabEmpresa.Value;
    FmVSCalOriIMEI_Peso.FTipoArea       := QrVSCalCabTipoArea.Value;
    FmVSCalOriIMEI_Peso.FOrigMovimNiv   := QrVSCalAtuMovimNiv.Value;
    FmVSCalOriIMEI_Peso.FOrigMovimCod   := QrVSCalAtuMovimCod.Value;
    FmVSCalOriIMEI_Peso.FOrigCodigo     := QrVSCalAtuCodigo.Value;
    FmVSCalOriIMEI_Peso.FNewGraGruX     := QrVSCalAtuGraGruX.Value;
    //
    FmVSCalOriIMEI_Peso.EdCodigo.ValueVariant    := QrVSCalCabCodigo.Value;
    FmVSCalOriIMEI_Peso.EdMovimCod.ValueVariant  := QrVSCalCabMovimCod.Value;
    FmVSCalOriIMEI_Peso.FDataHora                := QrVSCalCabDtHrAberto.Value;
    //
    FmVSCalOriIMEI_Peso.EdSrcMovID.ValueVariant  := QrVSCalAtuMovimID.Value;
    FmVSCalOriIMEI_Peso.EdSrcNivel1.ValueVariant := QrVSCalAtuCodigo.Value;
    FmVSCalOriIMEI_Peso.EdSrcNivel2.ValueVariant := QrVSCalAtuControle.Value;
    FmVSCalOriIMEI_Peso.EdSrcGGX.ValueVariant    := QrVSCalAtuGraGruX.Value;
    //
    if SQLType = stIns then
      //
    else
    begin
{
      FmVSCalOriIMEI_Peso.EdControle.ValueVariant := QrVSCalCabOldControle.Value;
      //
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSCalOriIMEI_Peso.FParcial := True;
        FmVSCalOriIMEI_Peso.DBG04Estq.Options := FmVSCalOriIMEI_Peso.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSCalOriIMEI_Peso.FParcial := False;
        FmVSCalOriIMEI_Peso.DBG04Estq.Options := FmVSCalOriIMEI_Peso.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSCalOriIMEI_Peso.FParcial := False;
        FmVSCalOriIMEI_Peso.DBG04Estq.Options := FmVSCalOriIMEI_Peso.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSCalOriIMEI_Peso.ReopenItensAptos();
    FmVSCalOriIMEI_Peso.ShowModal;
    FmVSCalOriIMEI_Peso.Destroy;
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSCalCab.MostraVSCalOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
{
  if DBCheck.CriaFm(TFmVSCalOriPall, FmVSCalOriPall, afmoNegarComAviso) then
  begin
    FmVSCalOriPall.ImgTipo.SQLType := SQLType;
    FmVSCalOriPall.FQrCab          := QrVSCalCab;
    FmVSCalOriPall.FDsCab          := DsVSCalCab;
    //FmVSCalOriPall.FVSCalOriPallIMEI   := QrVSCalOriPallIMEI;
    //FmVSCalOriPall.FVSCalOriPallPallet := QrVSCalOriPallPallet;
    FmVSCalOriPall.FEmpresa        := QrVSCalCabEmpresa.Value;
    FmVSCalOriPall.FClientMO       := QrVSCalAtuClientMO.Value;
    FmVSCalOriPall.FTipoArea       := QrVSCalCabTipoArea.Value;
    FmVSCalOriPall.FOrigMovimNiv   := QrVSCalAtuMovimNiv.Value;
    FmVSCalOriPall.FOrigMovimCod   := QrVSCalAtuMovimCod.Value;
    FmVSCalOriPall.FOrigCodigo     := QrVSCalAtuCodigo.Value;
    FmVSCalOriPall.FNewGraGruX     := QrVSCalAtuGraGruX.Value;
    //
    FmVSCalOriPall.EdCodigo.ValueVariant    := QrVSCalCabCodigo.Value;
    FmVSCalOriPall.EdMovimCod.ValueVariant  := QrVSCalCabMovimCod.Value;
    FmVSCalOriPall.FDataHora                := QrVSCalCabDtHrAberto.Value;
    //
    FmVSCalOriPall.EdSrcMovID.ValueVariant  := QrVSCalAtuMovimID.Value;
    FmVSCalOriPall.EdSrcNivel1.ValueVariant := QrVSCalAtuCodigo.Value;
    FmVSCalOriPall.EdSrcNivel2.ValueVariant := QrVSCalAtuControle.Value;
    FmVSCalOriPall.EdSrcGGX.ValueVariant    := QrVSCalAtuGraGruX.Value;
    //
    FmVSCalOriPall.ReopenItensAptos();
    FmVSCalOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
      //
    else
    begin
(*
      FmVSCalOriPall.EdControle.ValueVariant := QrVSCalCabOldControle.Value;
      //
      FmVSCalOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSCalCabOldCNPJ_CPF.Value);
      FmVSCalOriPall.EdNomeEmiSac.Text := QrVSCalCabOldNome.Value;
      FmVSCalOriPall.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSCalOriPall.FParcial := True;
        FmVSCalOriPall.DBG04Estq.Options := FmVSCalOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSCalOriPall.FParcial := False;
        FmVSCalOriPall.DBG04Estq.Options := FmVSCalOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSCalOriPall.FParcial := False;
        FmVSCalOriPall.DBG04Estq.Options := FmVSCalOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSCalOriPall.ShowModal;
    FmVSCalOriPall.Destroy;
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmVSCalCab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = False;
var
  Codigo: Integer;
begin
  PCCalOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSCalCabMovimCod.Value,
  QrVSCalCabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSCalCabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
end;

procedure TFmVSCalCab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
  PCCalOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
end;

procedure TFmVSCalCab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
  VS_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmProcCal], True, False, '', 0);
end;

procedure TFmVSCalCab.otal1Click(Sender: TObject);
begin
  MostraVSCalOriPall(stIns, ptTotal);
end;

procedure TFmVSCalCab.Total2Click(Sender: TObject);
begin
  MostraVSCalOriIMEI_Peca(stIns, ptTotal);
end;

procedure TFmVSCalCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSCalCab.Parcial1Click(Sender: TObject);
begin
  MostraVSCalOriPall(stIns, ptParcial);
end;

procedure TFmVSCalCab.Parcial2Click(Sender: TObject);
begin
  MostraVSCalOriIMEI_Peca(stIns, ptParcial);
end;

procedure TFmVSCalCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCalCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCalCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEC(emidEmProcCal);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCalCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_PF.LocalizaPeloIMEI(emidEmProcCal);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCalCab.PesquisaArtigoJaFeito(Key: Word);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  if Key = VK_F4 then
  begin
    Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCal, [eminEmCalInn], stCpy,
    Codigo, Controle, False);
    //
    if (Controle <> 0) and (DmModVS.QrIMEI.State <> dsInactive) then
    begin
      EdGraGruX.ValueVariant   := DmModVS.QrIMEIGraGruX.Value;
      CBGraGruX.KeyValue       := DmModVS.QrIMEIGraGruX.Value;
      EdFornecMO.ValueVariant  := DmModVS.QrIMEIFornecMO.Value;
      CBFornecMO.KeyValue      := DmModVS.QrIMEIFornecMO.Value;
      EdStqCenLoc.ValueVariant := DmModVS.QrIMEIStqCenLoc.Value;
      CBStqCenLoc.KeyValue     := DmModVS.QrIMEIStqCenLoc.Value;
      EdCustoMOM2.ValueVariant := DmModVS.QrIMEICustoMOM2.Value;
      EdCliente.ValueVariant   := DmModVS.QrIMEICliVenda.Value;
      CBCliente.KeyValue       := DmModVS.QrIMEICliVenda.Value;
    end;
  end;
end;

procedure TFmVSCalCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCalCab);
  MyObjects.HabilitaMenuItemCabDelC1I4(CabExclui1, QrVSCalCab, QrVSCalOriIMEI,
    QrEmit, QrVSCalDst, QrVSCalInd);
  DataDeAbertura1.Enabled := (QrVSCalCabDtHrFimOpe.Value > 0) and
    (QrVSCalCabDtHrAberto.Value > QrVSCalCabDtHrFimOpe.Value);
end;

procedure TFmVSCalCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiMul, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiUni, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSCalDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrVSCalDst);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSCalDst);
  if (QrVSCalCabDtHrLibOpe.Value > 2) or (QrVSCalCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsIncluiMul.Enabled := False;
    ItsIncluiUni.Enabled := False;
    ItsAlteraDst.Enabled := False;
    //ItsExcluiDst.Enabled := False;
  end;
  VS_PF.HabilitaComposVSAtivo(QrVSCalDstID_TTW.Value, [ItsExcluiDst]);
end;

procedure TFmVSCalCab.PMImprimePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Desteprocesso1, QrVSCalCab);
end;

procedure TFmVSCalCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI2, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(Corrigepesodebaixa1, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSCalOriPallet);
  if (QrVSCalCabDtHrLibOpe.Value > 2) or (QrVSCalCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  case PCCalOri.ActivePageIndex of
    0:
    begin
      ItsExcluiOriIMEI.Enabled := False;
      Corrigepesodebaixa1.Enabled := False;
    end;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCCalOri.ActivePageIndex = 1);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSCalOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_PF.HabilitaComposVSAtivo(QrVSCalOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  ItsExcluiOriIMEI2.Enabled := ItsExcluiOriIMEI2.Enabled and (ItsExcluiOriIMEI.Enabled = False);
end;

procedure TFmVSCalCab.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSCalCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSCalCab.PMSubProdutoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSubProduto1, QrVSCalOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSubProduto1, QrVSSubPrdIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSubProduto1, QrVSSubPrdIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSSubPrdItsID_TTW.Value, [AlteraSubProduto1, ExcluiSubProduto1]);
end;

procedure TFmVSCalCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCalCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCalCab.DefParams;
begin
  VAR_GOTOTABELA := 'vscalcab';
  VAR_GOTOMYSQLTABLE := QrVSCalCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(PecasMan<>0, PecasMan, -PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(AreaManM2<>0, AreaManM2, -AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(AreaManP2<>0, AreaManP2, -AreaSrcM2) AreaINIP2,');
  VAR_SQLx.Add('IF(PesoKgMan<>0, PesoKgMan, -PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vscalcab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSCalCab.Desteprocesso1Click(Sender: TObject);
var
  Corda1, Corda2: String;
begin
  Corda1 := MyObjects.CordaDeQuery(QrVSCalOriIMEI, 'SrcNivel2', '-999999999');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
(*
  'SELECT GROUP_CONCAT(DISTINCT(Pallet)) Pallets',
  'FROM vsmovits',
  'WHERE MovimCod=' + Geral.FF0(QrVSCalCabMovimCod.Value),
  'AND Pallet<>0',
*)
  'SELECT GROUP_CONCAT(DISTINCT(Pallet)) Pallets',
  'FROM vsmovits',
  'WHERE Pallet<>0 ',
  'AND ',
  '(',
  '  (MovimCod=' + Geral.FF0(QrVSCalCabMovimCod.Value) + ')',
  '  OR',
  '  (GSPSrcNiv2 IN (' + Corda1 + '))',
  ')',
  '']);
  //Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Pallets', EmptyStr);
  Corda2 := Dmod.QrAux.Fields[0].AsString;
  if Corda2 <> EmptyStr then
    VS_Jan.MostraFormVSSifDipoaImp(Corda2)
  else
    Geral.MB_Aviso('Este processo n�o possui nenhum pallet!');
end;

procedure TFmVSCalCab.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCalCab.EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCalCab.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCalCab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCCalOri.ActivePageIndex := 3;
end;

procedure TFmVSCalCab.EmitePesagem1Click(Sender: TObject);
begin
  //VAR_SETOR := CO_CALEIRO;
  PQ_PF.MostraFormVSFormulasImp_BH(QrVSCalCabMovimCod.Value,
    QrVSCalAtuControle.Value, QrVSCalCabTemIMEIMrt.Value,
    eminEmCalInn, eminSorcCal, recribsetCaleiro);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.EmitePesagem2Click(Sender: TObject);
begin
  PCCalOri.ActivePageIndex := 2;
end;

procedure TFmVSCalCab.Excluigrupo1Click(Sender: TObject);
begin
  PCCalOri.ActivePageIndex := 3;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
end;

procedure TFmVSCalCab.ExcluiImeiDeOrigem(ExigeSenhaAdmin: Boolean);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimID, MovimNiv, MovimCod: Integer;
  Qry: TmySQLQuery;
begin
  if ExigeSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  //
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrVSCalCabCodigo.Value;
    CtrlDel   := QrVSCalOriIMEIControle.Value;
    CtrlOri   := QrVSCalOriIMEISrcNivel2.Value;
    MovimID   := QrVSCalAtuMovimID.Value;
    MovimNiv  := QrVSCalAtuMovimNiv.Value;
    MovimCod  := QrVSCalAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti142), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vscalcab', MovimCod);
      VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
      //
      if QrVSCalOriIMEIRmsNivel2.Value <> 0 then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE RmsMovID=' + Geral.FF0(MovimID),
          'AND RmsNivel1=' + Geral.FF0(Codigo),
          'AND RmsNivel2=' + Geral.FF0(CtrlDel),
          '']);
          Qry.First;
          while not Qry.Eof do
          begin
            CtrlDel := Qry.FieldByName('Controle').AsInteger;
            VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti142), Dmod.QrUpd, Dmod.MyDB,
            CO_MASTER);
            //
            Qry.Next;
          end;
          //
        finally
          Qry.Free;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCalOriIMEI,
        TIntegerField(QrVSCalOriIMEIControle), QrVSCalOriIMEIControle.Value);
      //
      VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSCalOris(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSCalCab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
  PCCalOri.ActivePageIndex := 3;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
end;

procedure TFmVSCalCab.ExcluiPesagem1Click(Sender: TObject);
begin
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, PB1) then
    LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.ExcluiSubProduto1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  Controle := QrVSSubPrdItsControle.Value;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '',
    'UNION',
    '',
    'SELECT Controle ',
    'FROM ' + CO_TAB_VMB,
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    '']);
    //
    if MyObjects.FIC(Qry.RecordCount > 0, nil,
    'O item n�o pode ser exclu�do pois j� possui baixas!') then
      Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do sub produto?') <> ID_YES then
      Exit;
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti142), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSCalCab.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSCalCab.Cliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSCalCabCodigo.Value;
  if VS_PF.AlteraVMI_CliVenda(QrVSCalAtuControle.Value,
  QrVSCalAtuCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
    'Cliente'], [
    'Codigo'], [
    QrVSCalAtuCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSCalCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  // N�o pode mais!
  // Modificado em 2023-11-25
  // Artigo de destino para cada item de origem!
(*
  IMEIAtu     := QrVSCalAtuControle.Value;
  MovimCod    := QrVSCalCabMovimCod.Value;
  Codigo      := QrVSCalCabCodigo.Value;
  MovimNivSrc := eminSorcCal;
  MovimNivDst := eminDestCal;
  VS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
*)
end;

procedure TFmVSCalCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSCalCab.Corrigepesodebaixa1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  //
  if VS_PF.AlteraVMI_PesoKg('PesoKg', QrVSCalOriIMEIMovimID.Value,
  QrVSCalOriIMEIMovimNiv.Value, QrVSCalOriIMEIControle.Value,
  QrVSCalOriIMEIPesoKg.Value, siNegativo) then
    LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.CorrigirFornecedor1Click(Sender: TObject);
begin
  VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  VS_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcCal, QrVSCalCabMovimCod.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
  //
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.CourosemprocessoBH1Click(Sender: TObject);
begin
  // ini 2023-12-31
  //VS_PF.ImprimeVSEmProcBH();
  VS_Jan.MostraFormVSImpEmProcBH();
  // fim 2023-12-31
end;

procedure TFmVSCalCab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSCalCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSCalCabNFeRem.Value)
  else
    NFeRem := '';
  VS_PF.ImprimeIMEI([QrVSCalAtuControle.Value], viikOrdemOperacao,
    QrVSCalCabLPFMO.Value, NFeRem, QrVSCalCab);
end;

procedure TFmVSCalCab.IncluiSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  Controle   = 0;
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  VSMulFrnCab: Integer;
  Qry: TmySQLQuery;
  Marca: String;
  GGXInProc: Integer;
begin
  if DmModVS.NaoEhCouroEmProcesso(QrVSCalAtuGraGruX.Value, True) then Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(QrVSCalOriIMEISrcNivel2.Value),
    '']);
    DataHora    := DModG.ObtemAgora();
    GSPSrcMovID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    GSPSrcNiv2  := Qry.FieldByName('Controle').AsInteger;
    Codigo      := 0;
    MovimCod    := 0;
    Empresa     := Qry.FieldByName('Empresa').AsInteger;
    ClientMO    := Qry.FieldByName('ClientMO').AsInteger;
    Terceiro    := Qry.FieldByName('Terceiro').AsInteger;
    VSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
    Ficha       := Qry.FieldByName('Ficha').AsInteger;
    SerieFch    := Qry.FieldByName('SerieFch').AsInteger;
    Marca       := Qry.FieldByName('Marca').AsString;
    //
    GGXInProc   := QrVSCalAtuGraGruX.Value;
    //
    VS_PF.MostraFormVSCalSubPrdIts_Uni(stIns, DataHora, QrCab, QrVSSubPrdIts, DsCab,
    GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
    VSMulFrnCab, Ficha, SerieFch, Controle, Marca, GGXInProc);
    //ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSCalCab.IncluiSubProdutorateandoentretodasorigens1Click(
  Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  Controle   = 0;
var
  DataHora: TDateTime;
  GSPSrcMovID, GSPJmpMovID: TEstqMovimID;
  GSPSrcNiv2, GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha,
  SerieFch, VSMulFrnCab, MulOriMovimCod, TemIMEIMrt: Integer;
  Qry: TmySQLQuery;
  Marca: String;
  CouNiv2EmProc: Integer;
begin
  if DmModVS.NaoEhCouroEmProcesso(QrVSCalAtuGraGruX.Value, True) then Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(QrVSCalOriIMEISrcNivel2.Value),
    '']);
    DataHora    := DModG.ObtemAgora();
    GSPSrcMovID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    GSPSrcNiv2  := Qry.FieldByName('Controle').AsInteger;
    GSPJmpMovID := TEstqMovimID(QrVSCalOriIMEIMovimID.Value);
    GSPJmpNiv2  := QrVSCalOriIMEIControle.Value;
    Codigo      := 0;
    MovimCod    := 0;
    Empresa     := Qry.FieldByName('Empresa').AsInteger;
    ClientMO    := Qry.FieldByName('ClientMO').AsInteger;
    Terceiro    := Qry.FieldByName('Terceiro').AsInteger;
    VSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
    Ficha       := Qry.FieldByName('Ficha').AsInteger;
    SerieFch    := Qry.FieldByName('SerieFch').AsInteger;
    Marca       := Qry.FieldByName('Marca').AsString;
    //
    MulOriMovimCod := QrVSCalCabMovimCod.Value;
    TemIMEIMrt     := QrVSCalCabTemIMEIMrt.Value;
    //
    CouNiv2EmProc      := QrVSCalAtuCouNiv2.Value;
    //
    VS_PF.MostraFormVSCalSubPrdIts_Mul(stIns, DataHora, QrCab, QrVSSubPrdIts,
    DsCab, GSPSrcMovID, GSPSrcNiv2, GSPJmpMovID, GSPJmpNiv2, Codigo, MovimCod,
    Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha, SerieFch, Controle,
    MulOriMovimCod, TemIMEIMrt, True, Marca, CouNiv2EmProc);
    //ReopenVSSubPrdIts(0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSCalCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSCalOriIMEIControle.Value,
    QrVSCalOriIMEIReqMovEstq.Value, QrVSCalOriIMEI);
end;

procedure TFmVSCalCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_PF.InfoReqMovEstq(QrVSCalDstControle.Value,
    QrVSCalDstReqMovEstq.Value, QrVSCalDst);
end;

procedure TFmVSCalCab.InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
GraGruX, CliVenda: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOM2, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmProcCal;
  MovimNiv       := eminEmCalInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei002(*Cabe�alho de processo de caleiro*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('vscalcab', MovimCod);
    VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
    if GBVSPedIts.Visible then
      VS_PF.AtualizaVSPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;

procedure TFmVSCalCab.irparajaneladomovimento1Click(Sender: TObject);
begin
  if (QrVSCalDst.State <> dsInactive) and (QrVSCalDst.RecordCount > 0) then
    VS_PF.MostraFormVS_Do_IMEI(QrVSCalDstControle.Value);
end;

procedure TFmVSCalCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrVSCalDstPallet.Value);
end;

procedure TFmVSCalCab.ItsAlteraDstClick(Sender: TObject);
begin
  MostraVSCalDst1(stUpd);
end;

procedure TFmVSCalCab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraVSGArtOri(stUpd);
end;

procedure TFmVSCalCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSCalCabCodigo.Value;
  VS_PF.ExcluiCabEIMEI_OpeCab(QrVSCalCabMovimCod.Value,
    QrVSCalAtuControle.Value, emidEmProcCal, TEstqMotivDel.emtdWetCurti142);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSCalCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSCalCab.Qualquerprocesso1Click(Sender: TObject);
begin
  VS_Jan.MostraFormVSSifDipoaImp(EmptyStr);
end;

procedure TFmVSCalCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCalCab.RecalculaCusto;
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.Recalculacusto1Click(Sender: TObject);
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
  VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.Reimprimereceita1Click(Sender: TObject);
begin
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TReceitaTipoSetor.rectipsetrRibeira, PB1);
end;

procedure TFmVSCalCab.ReopenEmit();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0(QrVSCalCabMovimCod.Value),
  '']);
end;

procedure TFmVSCalCab.ReopenVSCalOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSCalCabTemIMEIMrt.Value;
  //
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVSCalOriIMEI, QrVSCalCabMovimCod.Value, Controle,
  TemIMEIMrt, eminSorcCal, SQL_Limit);
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCalCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalOriPallet, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSCalOriPallet.Locate('Pallet', Pallet, []);
  //
  //Geral.MB_Teste(QrVSCalOriPallet.SQL.Text);
end;

procedure TFmVSCalCab.ReopenVSSubPrdIts(Controle: Integer);
var
  GSPInnNiv2, TemIMEIMrt: Integer;
begin
  GSPInnNiv2 := QrVSCalOriIMEISrcNivel2.Value;
  TemIMEIMrt := QrVSCalCabTemIMEIMrt.Value;
  VS_PF.ReopenVSSubPrdIts(QrVSSubPrdIts, GSPInnNiv2, TemIMEIMrt, Controle);
end;

procedure TFmVSCalCab.Rtuloderastreabilidade1Click(Sender: TObject);
begin
// parei aqui 2023-12-27
end;

procedure TFmVSCalCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
(*
  if QrVSCalAtuSdoVrtPeca.Value < QrVSCalAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSCalCabCodigo.Value;
    CtrlDel  := QrVSCalDstControle.Value;
    CtrlDst  := QrVSCalDstSrcNivel2.Value;
    MovimNiv := QrVSCalAtuMovimNiv.Value;
    MovimCod := QrVSCalAtuMovimCod.Value;
    //
    if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti142), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_PF.ObtemControleMovimTwin(
        QrVSCalDstMovimCod.Value, QrVSCalDstMovimTwn.Value, emidCaleado,
        eminEmCalBxa);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti142), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCalDst,
        TIntegerField(QrVSCalDstControle), QrVSCalDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_PF.AtualizaTotaisVSXxxCab('vscalcab', MovimCod);
      VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
      //
      VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      (*
      VS_PF.ReopenVSOpePrcDst(QrVSCalDst, QrVSCalCabMovimCod.Value, CtrlNxt,
      QrVSCalCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestCal);
      *)
      VS_EFD_ICMS_IPI.ReopenVSPrcPrcDst(QrVSCalDst, QrVSCalCabCodigo.Value,
      QrVSCalCabMovimCod.Value, CtrlNxt,
      QrVSCalCabTemIMEIMrt.Value, TEstqMovimID.emidCaleado, eminDestCal);
    end;
  end;
end;

procedure TFmVSCalCab.ItsExcluiOriIMEI2Click(Sender: TObject);
begin
  ExcluiImeiDeOrigem(True);
end;

procedure TFmVSCalCab.ItsExcluiOriIMEIClick(Sender: TObject);
begin
  ExcluiImeiDeOrigem(False);
end;

procedure TFmVSCalCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if MyObjects.FIC(QrVSCalOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrVSCalAtuSdoVrtPeca.Value < QrVSCalAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSCalOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSCalOriIMEI.First;
    while not QrVSCalOriIMEI.Eof do
    begin
      if QrVSCalOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSCalOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSCalOriIMEI.First;
        while not QrVSCalOriIMEI.Eof do
        begin
          if QrVSCalOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSCalCabCodigo.Value;
            CtrlDel   := QrVSCalOriIMEIControle.Value;
            CtrlOri   := QrVSCalOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSCalAtuMovimNiv.Value;
            MovimCod  := QrVSCalAtuMovimCod.Value;
            //
            if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti142),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_PF.AtualizaTotaisVSXxxCab('vscalcab', MovimCod);
              VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
            end;
            //
          end;
          QrVSCalOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSCalOriIMEI,
      TIntegerField(QrVSCalOriIMEIControle), QrVSCalOriIMEIControle.Value);
      //
      VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      ReopenVSCalOris(CtrlNxt, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSCalCab.Datadeabertura1Click(Sender: TObject);
var
  Data, Hora: TDateTime;
  Codigo: Integer;
  DtHrAberto: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  if not DBCheck.ObtemData(Date, Data, Date, Hora, True) then Exit;
  DtHrAberto := Geral.FDT(Data, 109);
  //
  Codigo := QrVSCalCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
  'DtHrAberto'], ['Codigo'], [DtHrAberto], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSCalCab.DefineONomeDoForm;
begin
end;

procedure TFmVSCalCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCalCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCalCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCalCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCalCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCalCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCalCab.BtSubProdutoClick(Sender: TObject);
begin
  PCCalOri.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMSubProduto, BtSubProduto);
end;

procedure TFmVSCalCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCalCabCodigo.Value;
  Close;
end;

procedure TFmVSCalCab.ItsIncluiDstClick(Sender: TObject);
var
  I, N, IMEIOri: Integer;
  IMEIs: array of String;
begin
  // 2023-05-14
  MostraVSCalDst1(stIns);
(*
  N := 0;
  if QrVSCalOriIMEI.RecordCount = 1 then
    IMEIOri := QrVSCalOriIMEIControle.Value
  else
  begin
    SetLength(IMEIs, QrVSCalOriIMEI.RecordCount);
    QrVSCalOriIMEI.First;
    while not QrVSCalOriIMEI.Eof do
    begin
      if QrVSCalOriIMEISdoVrtPeca.Value > 0 then
      begin
        IMEIs[QrVSCalOriIMEI.RecNo-1] := Geral.FF0(QrVSCalOriIMEIControle.Value);
        N := N + 1;
      end;
      QrVSCalOriIMEI.Next;
    end;
    if N > 0 then
    begin
      SetLength(IMEIs, N);
      I := MyObjects.SelRadioGroup('Selecione o IME-I de origem', '', IMEIS, -1);
        if I < 0 then
          Exit;
      ImeiOri := Geral.IMV(IMEIs[I]);
    end else
    begin
      Geral.MB_Aviso('N�o h� IME-Is com saldo para baixa neste processo!');
      Exit;
    end;
  end;
  if QrVSCalOriIMEI.Locate('Controle', ImeiOri, []) then
  begin
    if QrVSCalOriIMEISdoVrtPeca.Value > 0 then
      MostraVSCalDst2(stIns)
    else
      Geral.MB_Aviso('O IME-I selecionado n�o tem mais saldo para baixa!');
  end;
  // fim 2023-05-14
*)
end;

procedure TFmVSCalCab.CabAltera1Click(Sender: TObject);
var
  Empresa(*, VSPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCalCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vscalcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSCalCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrVSCalAtuClientMO.Value;
  CBClientMO.KeyValue        := QrVSCalAtuClientMO.Value;
  //
  EdCliente.ValueVariant    := QrVSCalCabCliente.Value;
  CBCliente.KeyValue        := QrVSCalCabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrVSCalCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSCalCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSCalCabLPFMO.Value;
  //
  EdControle.ValueVariant   := QrVSCalAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSCalAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSCalAtuGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrVSCalAtuCustoMOTot.Value;
  EdCustoMOM2.ValueVariant  := QrVSCalAtuCustoMOM2.Value;
  EdPecas.ValueVariant      := QrVSCalAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSCalAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSCalAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSCalAtuObserv.Value;
  //
  VS_PF.ReopenPedItsXXX(QrVSPedIts, QrVSCalAtuControle.Value, QrVSCalAtuControle.Value);
  Habilita := QrVSCalAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSCalAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSCalAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSCalAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSCalAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSCalAtuReqMovEstq.Value;
  //
  EdFornecMO.ValueVariant   := QrVSCalAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSCalAtuFornecMO.Value;
end;

procedure TFmVSCalCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, (*GGXSrc,*)
  GGXDst, Forma, Quant, Cliente, CliVenda, StqCenLoc, SerieRem, NFeRem: Integer;
  Nome, DtHrAberto, DataHora, LPFMO: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  //GGXSrc         := EdGGXSrc.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  //if MyObjects.FIC(GGXSrc = 0, EdGGXSrc, 'Informe o artigo ap�s divisora!') then
    //Exit;
  //
  if not VS_PF.ValidaCampoNF(7, EdNFeRem, True) then Exit;
  //
  GGXDst := GraGruX;
  Codigo := UMyMod.BPGS1I32('vscalcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscalcab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', (*'GGXSrc',*) 'GGXDst'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, (*GGXSrc,*) GGXDst], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_PF.InsereVSMovCab(MovimCod, emidEmProcCal, Codigo)
    else
    begin
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO, GraGruX, Cliente, DtHrAberto);

    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSCalOriIMEI.RecordCount = 0) then
    begin
      //MostraVSCalOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      //'Pallet Total', 'Pallet Parcial',
      'IME-I Total por Pe�as', 'IME-I Parcial por Pe�as',
      'IME-I Total por Peso', 'IME-I Parcial por Peso'], -1);
      case Forma of
        //0: MostraVSCalOriPall(stIns, ptTotal);
        //1: MostraVSCalOriPall(stIns, ptParcial);
        0: MostraVSCalOriIMEI_Peca(stIns, ptTotal);
        1: MostraVSCalOriIMEI_Peca(stIns, ptParcial);
        2: MostraVSCalOriIMEI_Peso(stIns, ptTotal);
        3: MostraVSCalOriIMEI_Peso(stIns, ptParcial);
      end;
    end;
    VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
  end;
end;

procedure TFmVSCalCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscalcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscalcab', 'Codigo');
end;

procedure TFmVSCalCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSCalCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSCalCab.BtPesagemClick(Sender: TObject);
begin
  if not PCcalOri.ActivePageIndex in [2,3] then
    PCCalOri.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSCalCab.ItsIncluiMulClick(Sender: TObject);
begin
  MostraVSCalDstMul(stIns);
end;

procedure TFmVSCalCab.ItsIncluiUniClick(Sender: TObject);
begin
  MostraVSCalDst1(stIns);
end;

procedure TFmVSCalCab.AdicionaIMEIParcialPeso1Click(Sender: TObject);
begin
   MostraVSCalOriIMEI_Peso(stIns, ptParcial);
end;

procedure TFmVSCalCab.AdicionaIMEITotalPeso1Click(Sender: TObject);
begin
  MostraVSCalOriIMEI_Peso(stIns, ptTotal);
end;

procedure TFmVSCalCab.AlteraSubProduto1Click(Sender: TObject);
const
  QrCab = nil;
  DsCab = nil;
  (*
  OriGGX      = 0;
  OriSerieFch = 0;
  OriFicha    = 0;
  OriMarca    = '';
  *)
var
  DataHora: TDateTime;
  GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, Ficha, SerieFch,
  Controle, VSMulFrnCab: Integer;
  OriMarca: String;
  GGXInProc: Integer;
begin
  DataHora    := 0;
  GSPSrcMovID := TEstqMovimID(0);
  GSPSrcNiv2  := 0;
  Codigo      := 0;
  MovimCod    := 0;
  Empresa     := 0;
  ClientMO    := 0;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  Ficha       := 0;
  SerieFch    := 0;
  Controle    := QrVSSubPrdItsControle.Value;
  OriMarca    := '';
  //
  GGXInProc   := QrVSCalAtuGraGruX.Value;
  //
  VS_PF.MostraFormVSCalSubPrdIts_Uni(stUpd, DataHora, QrCab, QrVSSubPrdIts, DsCab,
  GSPSrcMovID, GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
  VSMulFrnCab, Ficha, SerieFch, Controle, (*OriGGX, OriSerieFch, OriFicha,*)
  OriMarca, GGXInProc);
  //ReopenVSSubPrdIts(0);
end;

procedure TFmVSCalCab.Atualizacusto1Click(Sender: TObject);
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
end;

procedure TFmVSCalCab.AtualizaestoqueEmProcesso1Click(Sender: TObject);
begin
  VS_PF.AtualizaTotaisVSCalCab(QrVSCalCabMovimCod.Value);
  LocCod(QrVSCalCabCodigo.Value, QrVSCalCabCodigo.Value);
end;

procedure TFmVSCalCab.AtualizaFornecedoresDeDestino;
var
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vscalcab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSCalCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry1.First;
    while not Qry1.Eof do
    begin
      if not FAtualizando then
        Qry1.Last
      else
      begin
        Codigo := Qry1.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_PF.AtualizaFornecedorCal(QrVSCalCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSCalCabMovimCod.Value);
        VS_PF.AtualizaVSCalCabGGxSrc(QrVSCalCabMovimCod.Value);
        //
      end;
      Qry1.Next;
    end;
  finally
    Qry1.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSCalCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSCalCab, QrVSCalCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCalCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  QrVSCalCab.Database  := Dmod.MyDB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PCCalOri.ActivePageIndex := 1;
  //GBEdita.Align := alClient;
  PnDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1365_VSProCal));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, emidEmProcCal);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
end;

procedure TFmVSCalCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSCalCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCalCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSCalCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSCalCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSCalCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCalCab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSCalCab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSCalCab.QrVSCalCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCalCab.QrVSCalCabAfterScroll(DataSet: TDataSet);
{
const
  Controle = 0;
begin
  // Em processo
  VS_PF.ReopenVSOpePrcAtu(QrVSCalAtu, QrVSCalCabMovimCod.Value, Controle,
  QrVSCalCabTemIMEIMrt.Value, eminEmCalInn);
  // Origem
  ReopenVSCalOris(0, 0);
  // Destino
  VS_PF.ReopenVSPrcPrcDst(QrVSCalDst, QrVSCalCabCodigo.Value,
  QrVSCalCabMovimCod.Value, Controle,
  QrVSCalCabTemIMEIMrt.Value, TEstqMovimID.emidCaleado, eminDestCal);
  //ReopenForcados(0);
  VS_PF.ReopenVSOpePrcForcados(QrForcados, 0, QrVSCalCabCodigo.Value,
    QrVSCalAtuControle.Value, QrVSCalCabTemIMEIMrt.Value, emidEmProcCal);
  // Insumos
  ReopenEmit();
  PQ_PF.ReopenPQOVS(QrPQO, QrVSCalCabMovimCod.Value, 0);
}
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSCalAtu, QrVSCalOriIMEI, QrVSCalOriPallet,
  QrVSCalDst, QrVSCalInd, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  QrEmit, QrPQO, QrVSCalCabCodigo.Value,
  QrVSCalCabMovimCod.Value, QrVSCalCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmProcCal, emidCaleado, emidCaleado,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmCalInn, eminSorcCal, eminDestCal);
end;

procedure TFmVSCalCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCalCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCalCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq(emidEmProcCal, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //ReopenVSCalIts(Controle);
  end;
end;

procedure TFmVSCalCab.SbStqCenLocClick(Sender: TObject);
begin
  VS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidEmProcCal);
end;

procedure TFmVSCalCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCalCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSCalCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  if QrGraGruX.RecordCount > 1 then
  begin
    EdGraGruX.ValueVariant    := 0;
    CBGraGruX.KeyValue        := 0;
  end;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  if QrPrestador.RecordCount > 1 then
  begin
    EdFornecMO.ValueVariant   := 0;
    CBFornecMO.KeyValue       := 0;
  end;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCalCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vscalcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  Agora := DModG.ObtemAgora();
  //TPData.Date := Agora;
  //EdHora.ValueVariant := Agora;
  VS_PF.DefineDataHoraOuDtMinima(Agora, TPData, EdHora);
  VS_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSCalCab.QrVSCalCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCalAtu.Close;
  QrVSCalOriIMEI.Close;
  QrVSCalOriPallet.Close;
  QrVSCalDst.Close;
  QrVSCalInd.Close;
  QrForcados.Close;
  QrEmit.Close;
  QrPQO.Close;
end;

procedure TFmVSCalCab.QrVSCalCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSCalCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSCalCab.QrVSCalCabCalcFields(DataSet: TDataSet);
begin
  case QrVSCalCabTipoArea.Value of
    0: QrVSCalCabNO_TIPO.Value := 'm�';
    1: QrVSCalCabNO_TIPO.Value := 'ft�';
    else QrVSCalCabNO_TIPO.Value := '???';
  end;
  QrVSCalCabNO_DtHrLibOpe.Value := Geral.FDT(QrVSCalCabDtHrLibOpe.Value, 106, True);
  QrVSCalCabNO_DtHrFimOpe.Value := Geral.FDT(QrVSCalCabDtHrFimOpe.Value, 106, True);
  QrVSCalCabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSCalCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSCalCab.QrVSCalDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSCalCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSCalCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmCalBxa)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSCalDstMovimTwn.Value),
  '']);
  SQL_Group := '';
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCalBxa, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
*)
  //
end;

procedure TFmVSCalCab.QrVSCalDstBeforeClose(DataSet: TDataSet);
begin
(*
  QrVSCalBxa.Close;
*)
end;

procedure TFmVSCalCab.QrVSCalOriIMEIAfterScroll(DataSet: TDataSet);
begin
  ReopenVSSubPrdIts(0);
end;

procedure TFmVSCalCab.QrVSCalOriIMEIBeforeClose(DataSet: TDataSet);
begin
  QrVSSubPrdIts.Close;
end;

//Pesagem!
//Data n�o pode ser menor que a data m�nima: 01/12/2018

end.

