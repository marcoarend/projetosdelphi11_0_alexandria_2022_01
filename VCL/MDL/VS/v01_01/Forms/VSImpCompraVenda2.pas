unit VSImpCompraVenda2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, frxClass, frxDBSet,
  dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB;

type
  TFmVSImpCompraVenda2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Qr12Inn: TMySQLQuery;
    Qr12InnCodigo: TLargeintField;
    Qr12InnControle: TLargeintField;
    Qr12InnMovimCod: TLargeintField;
    Qr12InnMovimNiv: TLargeintField;
    Qr12InnMovimTwn: TLargeintField;
    Qr12InnEmpresa: TLargeintField;
    Qr12InnTerceiro: TLargeintField;
    Qr12InnCliVenda: TLargeintField;
    Qr12InnMovimID: TLargeintField;
    Qr12InnDataHora: TDateTimeField;
    Qr12InnPallet: TLargeintField;
    Qr12InnGraGruX: TLargeintField;
    Qr12InnPecas: TFloatField;
    Qr12InnPesoKg: TFloatField;
    Qr12InnAreaM2: TFloatField;
    Qr12InnAreaP2: TFloatField;
    Qr12InnValorT: TFloatField;
    Qr12InnSrcMovID: TLargeintField;
    Qr12InnSrcNivel1: TLargeintField;
    Qr12InnSrcNivel2: TLargeintField;
    Qr12InnSrcGGX: TLargeintField;
    Qr12InnSdoVrtPeca: TFloatField;
    Qr12InnSdoVrtPeso: TFloatField;
    Qr12InnSdoVrtArM2: TFloatField;
    Qr12InnObserv: TWideStringField;
    Qr12InnSerieFch: TLargeintField;
    Qr12InnFicha: TLargeintField;
    Qr12InnMisturou: TLargeintField;
    Qr12InnFornecMO: TLargeintField;
    Qr12InnCustoMOKg: TFloatField;
    Qr12InnCustoMOM2: TFloatField;
    Qr12InnCustoMOTot: TFloatField;
    Qr12InnValorMP: TFloatField;
    Qr12InnDstMovID: TLargeintField;
    Qr12InnDstNivel1: TLargeintField;
    Qr12InnDstNivel2: TLargeintField;
    Qr12InnDstGGX: TLargeintField;
    Qr12InnQtdGerPeca: TFloatField;
    Qr12InnQtdGerPeso: TFloatField;
    Qr12InnQtdGerArM2: TFloatField;
    Qr12InnQtdGerArP2: TFloatField;
    Qr12InnQtdAntPeca: TFloatField;
    Qr12InnQtdAntPeso: TFloatField;
    Qr12InnQtdAntArM2: TFloatField;
    Qr12InnQtdAntArP2: TFloatField;
    Qr12InnNotaMPAG: TFloatField;
    Qr12InnPedItsFin: TLargeintField;
    Qr12InnMarca: TWideStringField;
    Qr12InnStqCenLoc: TLargeintField;
    Qr12InnNO_PALLET: TWideStringField;
    Qr12InnNO_PRD_TAM_COR: TWideStringField;
    Qr12InnNO_TTW: TWideStringField;
    Qr12InnID_TTW: TLargeintField;
    Qr12InnReqMovEstq: TLargeintField;
    Qr12InnRendKgm2: TFloatField;
    Qr12InnNotaMPAG_TXT: TWideStringField;
    Qr12InnRendKgm2_TXT: TWideStringField;
    Qr12InnMisturou_TXT: TWideStringField;
    Qr12InnNOMEUNIDMED: TWideStringField;
    Qr12InnSIGLAUNIDMED: TWideStringField;
    Qr12Innm2_CouroTXT: TWideStringField;
    Qr12InnKgMedioCouro: TFloatField;
    Qr12InnNO_TERCEIRO: TWideStringField;
    frxDs12Inn: TfrxDBDataset;
    frxWET_CURTI_018_12_A: TfrxReport;
    Qr12Vda: TMySQLQuery;
    Qr12VdaCodigo: TLargeintField;
    Qr12VdaMovimCod: TLargeintField;
    Qr12VdaNFV: TLargeintField;
    Qr12VdaCliente: TLargeintField;
    Qr12VdaPallet: TLargeintField;
    Qr12VdaPecas: TFloatField;
    Qr12VdaAreaM2: TFloatField;
    Qr12VdaNO_CLI: TWideStringField;
    Qr12VdaInteiros: TFloatField;
    Qr12VdaDtVenda: TDateTimeField;
    frxDs12Vda: TfrxDBDataset;
    GroupBox11: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    Panel5: TPanel;
    Label7: TLabel;
    Label54: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataRelativa: TdmkEditDateTimePicker;
    EdFornecedor: TdmkEditCB;
    Label1: TLabel;
    CBFornecedor: TdmkDBLookupComboBox;
    QrFornecedor: TMySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    CkMorto: TCheckBox;
    QrCliente: TMySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkCliente: TCheckBox;
    RGRelatorio: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWET_CURTI_018_12_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ImprimeMovimento();
  public
    { Public declarations }
  end;

  var
  FmVSImpCompraVenda2: TFmVSImpCompraVenda2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkProcFunc, UnGrl_Consts,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSImpCompraVenda2.BtOKClick(Sender: TObject);
begin
  ImprimeMovimento();
end;

procedure TFmVSImpCompraVenda2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpCompraVenda2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpCompraVenda2.FormCreate(Sender: TObject);
var
  Data: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  //
  Data := trunc(DModG.ObtemAgora());
  TPDataIni.Date := Data - 30;
  TPDataFim.Date := Data;
end;

procedure TFmVSImpCompraVenda2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpCompraVenda2.frxWET_CURTI_018_12_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPDataIni.Date, TPDataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_FORNECEDOR' then
    Value := dmkPF.CampoReportTxt(CBFornecedor.Text, 'TODOS')
end;

procedure TFmVSImpCompraVenda2.ImprimeMovimento;
var
  SQL_Periodo, SQL_Cliente: String;
  //
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ',
      'CAST(voc.DtVenda AS DATETIME) DtVenda, ',
  //  Codigo                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(voc.Codigo AS SIGNED) Codigo, ',
  //  MovimCod                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(voc.MovimCod AS SIGNED) MovimCod, ',
  //  NFV                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(voc.NFV AS SIGNED) NFV, ',
  //  Cliente                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(voc.Cliente AS SIGNED) Cliente, ',
  //Pallet                         int(11)      NOT NULL  DEFAULT "0"
      'CAST(vmi.Pallet AS SIGNED) Pallet, ',
  //Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"
      'CAST(-SUM(vmi.Pecas) AS DECIMAL (15,3)) Pecas, ',
  //AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(-SUM(vmi.AreaM2) AS DECIMAL (15,2)) AreaM2, ',
  //Inteiros                         double(15,2) NOT NULL  DEFAULT "0.00"
      'CAST(SUM(IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) * -vmi.Pecas) AS DECIMAL (15,3)) Inteiros, ',
  // NO_CLI
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsoutcab   voc ON voc.MovimCod=vmi.MovimCod ',
    'LEFT JOIN entidades  ent ON ent.Codigo=voc.Cliente ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    SQL_Periodo,
    SQL_Cliente,
    'GROUP BY voc.MovimCod, vmi.Pallet ',
    Geral.ATS_If(Tab <> ttwA, ['UNION']),
    '']);
  end;
//const
  //TemIMEIMrt = 1; // Ver o que fazer!
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Terceiro: String;
  Terceiro, TemIMEIMrt, Cliente: Integer;
  Report: TfrxReport;
  Page: TfrxReportPage;
  BlobStream: TStream;
begin
  if MyObjects.FIC(RGRelatorio.ItemIndex = 0, RGRelatorio,
    'Defina o relat�rio!') then Exit;
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND DataHora ',  TPDataIni.Date,
  TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  TemIMEIMrt := Geral.BoolToInt(CkMorto.Checked);
  Terceiro := EdFornecedor.ValueVariant;
  Cliente := EdCliente.ValueVariant;
  SQL_Cliente := EmptyStr;
  SQL_Terceiro := EmptyStr;
  if Terceiro <> 0 then
    SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro);
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN entidades  ter ON ter.Codigo=vmi.Terceiro ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)),
  SQL_Periodo,
  SQL_Terceiro,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qr12Inn, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY DataHora, Ficha, SerieFch',
  '']);
  //Geral.MB_Teste(Qr12Inn.SQL.Text);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE voc.DtVenda ',  TPDataIni.Date,
  TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  if CkCliente.Checked then
    SQL_Cliente  := 'AND vmi.CliVenda=' + Geral.FF0(Cliente);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr12Vda, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB, TemIMEIMrt),
  GeraSQLVSMovItx(ttwA),
  'ORDER BY DtVenda, Pallet ',
  '']);
  //Geral.MB_Teste(Qr12Vda.SQL.Text);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_12_A, [
    DModG.frxDsDono,
    frxDs12Inn,
    frxDs12Vda
  ]);
  BlobStream := TMemoryStream.Create();
  Report := TfrxReport.Create(Self);
  try
    frxWET_CURTI_018_12_A.SaveToStream(BlobStream);
    //Report := frxWET_CURTI_018_12_A;
    BlobStream.Position := 0;
    Report.LoadFromStream(BlobStream);
    case RGRelatorio.ItemIndex of
      //0:
      1:
      begin
        //Page := TfrxReportPage(Report.Pages[1]);
        Report.Pages[2].Destroy;
        //Report.Pages[0].Clear;
      end;
      2: Report.Pages[1].Destroy;
      3: ; // nada
      else
      begin
        Geral.MB_Aviso('Relat�rio n�o definido!');
        Exit;
      end;
    end;
    Report.OngetValue := frxWET_CURTI_018_12_AGetValue;
    MyObjects.frxMostra(Report, 'Movimento de couros');
  finally
    BlobStream.Free;
    Report.Free;
  end;
end;

end.
