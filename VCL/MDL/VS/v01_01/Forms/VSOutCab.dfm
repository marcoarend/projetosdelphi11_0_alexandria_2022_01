object FmVSOutCab: TFmVSOutCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-019 :: Sa'#237'da de Wet Blue do Estoque'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 169
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 91
        Height = 13
        Caption = 'Data / hora venda:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 97
        Height = 13
        Caption = 'Data / hora entrega:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label5: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaPecas: TLabel
        Left = 16
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 104
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 192
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 280
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label20: TLabel
        Left = 368
        Top = 136
        Width = 68
        Height = 13
        Caption = 'S'#233'rie e N'#186' NF:'
      end
      object Label21: TLabel
        Left = 16
        Top = 95
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object LaCliente: TLabel
        Left = 72
        Top = 56
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtVenda: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 44728.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtVenda: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 44728.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'FimVisPrv'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrega: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 44728.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrega: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdClienteRedefinido
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 73
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 13
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPecas: TdmkEdit
        Left = 16
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 104
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 192
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 280
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        DropDownRows = 7
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNFV: TdmkEdit
        Left = 400
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NFV'
        UpdCampo = 'NFV'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieV: TdmkEdit
        Left = 368
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieV'
        UpdCampo = 'SerieV'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 111
        Width = 972
        Height = 21
        MaxLength = 100
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox9: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 33
      Align = alTop
      TabOrder = 0
      object Label56: TLabel
        Left = 312
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label23: TLabel
        Left = 12
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object Label65: TLabel
        Left = 834
        Top = 8
        Width = 32
        Height = 13
        Caption = 'Data:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SpeedButton5: TSpeedButton
        Left = 156
        Top = 4
        Width = 24
        Height = 24
        Caption = '?'
        OnClick = SpeedButton5Click
      end
      object DBEdit18: TDBEdit
        Left = 360
        Top = 4
        Width = 53
        Height = 21
        DataField = 'Filial'
        DataSource = DsVSPedCab
        Enabled = False
        TabOrder = 1
      end
      object DBEdit19: TDBEdit
        Left = 416
        Top = 4
        Width = 409
        Height = 21
        DataField = 'NOMEEMP'
        DataSource = DsVSPedCab
        Enabled = False
        TabOrder = 2
      end
      object EdPedido: TdmkEdit
        Left = 60
        Top = 4
        Width = 93
        Height = 24
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pedido'
        UpdCampo = 'Pedido'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdPedidoRedefinido
      end
      object DBEdit21: TDBEdit
        Left = 871
        Top = 4
        Width = 129
        Height = 21
        DataField = 'DataF'
        DataSource = DsVSPedCab
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object DBEdit20: TDBEdit
        Left = 184
        Top = 4
        Width = 125
        Height = 21
        DataField = 'PedidCli'
        DataSource = DsVSPedCab
        Enabled = False
        TabOrder = 4
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 33
      Width = 1008
      Height = 136
      Align = alTop
      TabOrder = 1
      object Label24: TLabel
        Left = 8
        Top = 12
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object DBText1: TDBText
        Left = 832
        Top = 12
        Width = 45
        Height = 17
        DataField = 'NOME_TIPO_DOC'
        DataSource = DsCli
      end
      object Label25: TLabel
        Left = 8
        Top = 84
        Width = 40
        Height = 13
        Caption = 'Entrega:'
      end
      object DBEdit22: TDBEdit
        Left = 60
        Top = 8
        Width = 72
        Height = 21
        DataField = 'Codigo'
        DataSource = DsCli
        Enabled = False
        TabOrder = 0
      end
      object DBEdit23: TDBEdit
        Left = 132
        Top = 8
        Width = 693
        Height = 21
        DataField = 'NOME_ENT'
        DataSource = DsCli
        Enabled = False
        TabOrder = 1
      end
      object DBMemo1: TDBMemo
        Left = 60
        Top = 32
        Width = 932
        Height = 47
        DataField = 'E_ALL'
        DataSource = DsCli
        Enabled = False
        TabOrder = 2
      end
      object DBEdit24: TDBEdit
        Left = 876
        Top = 8
        Width = 112
        Height = 21
        DataField = 'CNPJ_TXT'
        DataSource = DsCli
        Enabled = False
        TabOrder = 3
      end
      object MeEnderecoEntrega1: TMemo
        Left = 60
        Top = 80
        Width = 932
        Height = 53
        TabStop = False
        ReadOnly = True
        TabOrder = 4
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 86
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 260
        Top = 15
        Width = 746
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 613
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtTribIncIts: TBitBtn
          Tag = 502
          Left = 492
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtTribIncItsClick
        end
        object BtVSOutNFeCab: TBitBtn
          Tag = 456
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe Cab'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtVSOutNFeCabClick
        end
        object BtVSOutNFeIts: TBitBtn
          Tag = 575
          Left = 370
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'N&Fe Itm'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtVSOutNFeItsClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 205
      Width = 1008
      Height = 171
      Align = alTop
      Caption = ' Itens da sa'#237'da: '
      TabOrder = 1
      object PCItens: TPageControl
        Left = 2
        Top = 15
        Width = 1004
        Height = 154
        ActivePage = TabSheet2
        Align = alClient
        DockSite = True
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Por Pallet'
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 996
            Height = 126
            Align = alClient
            DataSource = DsPallets
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid1DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'Item'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXT_ARTIGO'
                Title.Caption = 'Artigo'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ClasseImp'
                Title.Caption = 'Classe'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MediaM2'
                Title.Caption = 'M'#233'dia m'#178'/pc'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FaixaMediaM2'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Faixa m'#233'dia'
                Width = 60
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Por IME-I'
          ImageIndex = 1
          object DGDados: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 996
            Height = 126
            Align = alClient
            DataSource = DsVSOutIts
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = DGDadosDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxMovIX'
                Title.Caption = 'IXX'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxFolha'
                Title.Caption = 'Folha'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxLinha'
                Title.Caption = 'Lin'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GGXRcl'
                Title.Caption = 'Red.NFe'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Title.Caption = 'Ficha RMP'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Pallet'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMP'
                Title.Caption = 'Valor MP'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtAvuls'
                Title.Caption = 'Frete sa'#237'da'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Itens NF'
          ImageIndex = 2
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 205
            Height = 126
            Align = alLeft
            DataSource = DsVSOutNFeCab
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_serie'
                Title.Caption = 'S'#233'rie'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_nNF'
                Title.Caption = 'N'#186' NFe'
                Visible = True
              end>
          end
          object DBGrid2: TDBGrid
            Left = 205
            Top = 0
            Width = 502
            Height = 126
            Align = alClient
            DataSource = DsVSOutNFeIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigo / tamanho / cor'
                Width = 254
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorU'
                Title.Caption = 'Valor Unit.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor Total'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object GroupBox3: TGroupBox
            Left = 707
            Top = 0
            Width = 289
            Height = 126
            Align = alRight
            Caption = ' Impostos do Item: '
            TabOrder = 2
            object DBGrid4: TDBGrid
              Left = 2
              Top = 15
              Width = 285
              Height = 109
              Align = alClient
              DataSource = DsTribIncIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_Tributo'
                  Title.Caption = 'Tributo'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Percent'
                  Title.Caption = '%'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrTrib'
                  Title.Caption = 'Valor '
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
        end
        object TsFrCompr: TTabSheet
          Caption = ' Dados frete '
          ImageIndex = 3
          object PnFrCompr: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 126
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 183
            object DBGVSMOEnvAvu: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 529
              Height = 126
              Align = alLeft
              DataSource = DsVSMOEnvAvu
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_nCT'
                  Title.Caption = 'N'#186' CT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end>
            end
            object DBGVSMOEnvAVMI: TdmkDBGridZTO
              Left = 529
              Top = 0
              Width = 467
              Height = 126
              Align = alClient
              DataSource = DsVSMOEnvAVMI
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorFrete'
                  Title.Caption = '$ Frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object PCDados: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 205
      ActivePage = TabSheet5
      Align = alTop
      TabOrder = 2
      object TabSheet4: TTabSheet
        Caption = 'Dados de sa'#237'da'
        object GBDados: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 177
          Align = alClient
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdCodigo
          end
          object Label2: TLabel
            Left = 76
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label10: TLabel
            Left = 476
            Top = 16
            Width = 88
            Height = 13
            Caption = 'Data / hora fatura:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label11: TLabel
            Left = 628
            Top = 16
            Width = 88
            Height = 13
            Caption = 'Data / hora sa'#237'da:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label12: TLabel
            Left = 780
            Top = 16
            Width = 103
            Height = 13
            Caption = 'Data / hora chegada:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label13: TLabel
            Left = 932
            Top = 16
            Width = 56
            Height = 13
            Caption = 'ID Estoque:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label14: TLabel
            Left = 16
            Top = 56
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label15: TLabel
            Left = 508
            Top = 56
            Width = 69
            Height = 13
            Caption = 'Transportador:'
          end
          object Label16: TLabel
            Left = 16
            Top = 135
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label17: TLabel
            Left = 104
            Top = 135
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label18: TLabel
            Left = 192
            Top = 135
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label19: TLabel
            Left = 280
            Top = 135
            Width = 27
            Height = 13
            Caption = 'Peso:'
          end
          object Label9: TLabel
            Left = 368
            Top = 135
            Width = 68
            Height = 13
            Caption = 'S'#233'rie e N'#186' NF:'
            FocusControl = DBEdit15
          end
          object Label22: TLabel
            Left = 16
            Top = 95
            Width = 66
            Height = 13
            Caption = 'Observa'#231#245'es:'
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 16
            Top = 32
            Width = 56
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsVSOutCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdit1: TDBEdit
            Left = 76
            Top = 32
            Width = 56
            Height = 21
            DataField = 'Empresa'
            DataSource = DsVSOutCab
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 132
            Top = 32
            Width = 341
            Height = 21
            DataField = 'NO_EMPRESA'
            DataSource = DsVSOutCab
            TabOrder = 2
          end
          object DBEdit3: TDBEdit
            Left = 476
            Top = 32
            Width = 148
            Height = 21
            DataField = 'DtVenda'
            DataSource = DsVSOutCab
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 628
            Top = 32
            Width = 148
            Height = 21
            DataField = 'DtViagem'
            DataSource = DsVSOutCab
            TabOrder = 4
          end
          object DBEdit5: TDBEdit
            Left = 780
            Top = 32
            Width = 148
            Height = 21
            DataField = 'DtEntrega'
            DataSource = DsVSOutCab
            TabOrder = 5
          end
          object DBEdit6: TDBEdit
            Left = 932
            Top = 32
            Width = 64
            Height = 21
            DataField = 'MovimCod'
            DataSource = DsVSOutCab
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 16
            Top = 72
            Width = 56
            Height = 21
            DataField = 'Cliente'
            DataSource = DsVSOutCab
            TabOrder = 7
          end
          object DBEdit8: TDBEdit
            Left = 72
            Top = 72
            Width = 432
            Height = 21
            DataField = 'NO_CLIENTE'
            DataSource = DsVSOutCab
            TabOrder = 8
          end
          object DBEdit9: TDBEdit
            Left = 508
            Top = 72
            Width = 56
            Height = 21
            DataField = 'Transporta'
            DataSource = DsVSOutCab
            TabOrder = 9
          end
          object DBEdit10: TDBEdit
            Left = 564
            Top = 72
            Width = 433
            Height = 21
            DataField = 'NO_TRANSPORTA'
            DataSource = DsVSOutCab
            TabOrder = 10
          end
          object DBEdit11: TDBEdit
            Left = 16
            Top = 151
            Width = 84
            Height = 21
            DataField = 'Pecas'
            DataSource = DsVSOutCab
            TabOrder = 12
          end
          object DBEdit12: TDBEdit
            Left = 280
            Top = 151
            Width = 84
            Height = 21
            DataField = 'PesoKg'
            DataSource = DsVSOutCab
            TabOrder = 15
          end
          object DBEdit13: TDBEdit
            Left = 104
            Top = 151
            Width = 84
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsVSOutCab
            TabOrder = 13
          end
          object DBEdit14: TDBEdit
            Left = 192
            Top = 151
            Width = 84
            Height = 21
            DataField = 'AreaP2'
            DataSource = DsVSOutCab
            TabOrder = 14
          end
          object DBEdit15: TDBEdit
            Left = 400
            Top = 151
            Width = 84
            Height = 21
            DataField = 'NFV'
            DataSource = DsVSOutCab
            TabOrder = 17
          end
          object DBEdit16: TDBEdit
            Left = 368
            Top = 151
            Width = 32
            Height = 21
            DataField = 'SerieV'
            DataSource = DsVSOutCab
            TabOrder = 16
          end
          object DBEdit17: TDBEdit
            Left = 16
            Top = 111
            Width = 980
            Height = 21
            DataField = 'Nome'
            DataSource = DsVSOutCab
            TabOrder = 11
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' Dados do pedido '
        ImageIndex = 1
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 37
          Align = alTop
          TabOrder = 0
          object Label26: TLabel
            Left = 280
            Top = 8
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label27: TLabel
            Left = 4
            Top = 8
            Width = 36
            Height = 13
            Caption = 'Pedido:'
          end
          object Label37: TLabel
            Left = 848
            Top = 8
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object DBEdit25: TDBEdit
            Left = 328
            Top = 4
            Width = 53
            Height = 21
            DataField = 'Filial'
            DataSource = DsVSPedCab
            Enabled = False
            TabOrder = 0
          end
          object DBEdit26: TDBEdit
            Left = 384
            Top = 4
            Width = 457
            Height = 21
            DataField = 'NOMEEMP'
            DataSource = DsVSPedCab
            Enabled = False
            TabOrder = 1
          end
          object DBEdit57: TDBEdit
            Left = 52
            Top = 4
            Width = 93
            Height = 26
            DataField = 'Pedido'
            DataSource = DsVSOutCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit58: TDBEdit
            Left = 884
            Top = 4
            Width = 112
            Height = 21
            DataField = 'DataF'
            DataSource = DsVSPedCab
            TabOrder = 3
          end
          object DBEdit27: TDBEdit
            Left = 148
            Top = 4
            Width = 125
            Height = 21
            DataField = 'PedidCli'
            DataSource = DsVSPedCab
            Enabled = False
            TabOrder = 4
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 37
          Width = 1000
          Height = 136
          Align = alTop
          TabOrder = 1
          object Label35: TLabel
            Left = 8
            Top = 12
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object DBText2: TDBText
            Left = 832
            Top = 12
            Width = 45
            Height = 17
            DataField = 'NOME_TIPO_DOC'
            DataSource = DsCli
          end
          object Label38: TLabel
            Left = 8
            Top = 84
            Width = 40
            Height = 13
            Caption = 'Entrega:'
          end
          object DBEdit29: TDBEdit
            Left = 56
            Top = 8
            Width = 72
            Height = 21
            DataField = 'Codigo'
            DataSource = DsCli
            Enabled = False
            TabOrder = 0
          end
          object DBEdit30: TDBEdit
            Left = 132
            Top = 8
            Width = 693
            Height = 21
            DataField = 'NOME_ENT'
            DataSource = DsCli
            Enabled = False
            TabOrder = 1
          end
          object DBMemo3: TDBMemo
            Left = 56
            Top = 32
            Width = 932
            Height = 47
            DataField = 'E_ALL'
            DataSource = DsCli
            Enabled = False
            TabOrder = 2
          end
          object DBEdit31: TDBEdit
            Left = 876
            Top = 8
            Width = 112
            Height = 21
            DataField = 'CNPJ_TXT'
            DataSource = DsCli
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object MeEnderecoEntrega2: TMemo
            Left = 56
            Top = 80
            Width = 932
            Height = 53
            TabStop = False
            ReadOnly = True
            TabOrder = 4
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 374
        Height = 32
        Caption = 'Sa'#237'da de Wet Blue do Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 374
        Height = 32
        Caption = 'Sa'#237'da de Wet Blue do Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 374
        Height = 32
        Caption = 'Sa'#237'da de Wet Blue do Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrVSOutCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSOutCabBeforeOpen
    AfterOpen = QrVSOutCabAfterOpen
    BeforeClose = QrVSOutCabBeforeClose
    AfterScroll = QrVSOutCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 684
    Top = 1
    object QrVSOutCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSOutCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSOutCabDtVenda: TDateTimeField
      FieldName = 'DtVenda'
    end
    object QrVSOutCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSOutCabDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
    end
    object QrVSOutCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSOutCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSOutCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;#,###,###,##0.000; '
    end
    object QrVSOutCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;#,###,###,##0.000; '
    end
    object QrVSOutCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrVSOutCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrVSOutCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSOutCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSOutCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrVSOutCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSOutCabNFV: TIntegerField
      FieldName = 'NFV'
    end
    object QrVSOutCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSOutCabSerieV: TSmallintField
      FieldName = 'SerieV'
    end
    object QrVSOutCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSOutCabPedido: TIntegerField
      FieldName = 'Pedido'
    end
  end
  object DsVSOutCab: TDataSource
    DataSet = QrVSOutCab
    Left = 688
    Top = 45
  end
  object QrVSOutIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSOutItsAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 768
    Top = 1
    object QrVSOutItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSOutItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSOutItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSOutItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSOutItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSOutItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSOutItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSOutItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSOutItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSOutItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSOutItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSOutItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSOutItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOutItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOutItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSOutItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSOutItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSOutItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSOutItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSOutItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOutItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSOutItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSOutItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSOutItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSOutItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSOutItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSOutItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSOutItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSOutItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSOutItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSOutItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOutItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSOutItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOutItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOutItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSOutItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSOutItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSOutItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSOutItsNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSOutItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSOutItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSOutItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSOutItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSOutItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSOutItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
    end
    object QrVSOutItsGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
    end
    object QrVSOutItsIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrVSOutItsIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrVSOutItsIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
    object QrVSOutItsCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSOutItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
    end
  end
  object DsVSOutIts: TDataSource
    DataSet = QrVSOutIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 436
    Top = 572
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object PorPallet1: TMenuItem
        Caption = 'Por &Pallet'
        OnClick = PorPallet1Click
      end
      object PorIMEI1: TMenuItem
        Caption = 'Por &IME-I'
        OnClick = PorIMEI1Click
      end
      object Porkgsubproduto1: TMenuItem
        Caption = 'Por kg (sub produto)'
        object otal1: TMenuItem
          Caption = '&Total (um ou v'#225'rios)'
          OnClick = otal1Click
        end
        object Parcial1: TMenuItem
          Caption = '&Parcial (um de cada vez)'
          OnClick = Parcial1Click
        end
      end
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object AtrelamentoFreteCompra1: TMenuItem
      Caption = 'Atrelamento Frete'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Corrigeartigodebaixa1: TMenuItem
      Caption = 'Verifica artigo de baixa'
      Enabled = False
      OnClick = Corrigeartigodebaixa1Click
    end
    object ItsExcluiIMEI1: TMenuItem
      Caption = '&Remove o IME-I'
      Enabled = False
      OnClick = ItsExcluiIMEI1Click
    end
    object ItsExcluiPallet1: TMenuItem
      Caption = 'Remove o &Pallet'
      Enabled = False
      OnClick = ItsExcluiPallet1Click
    end
    object AntigoAdd1: TMenuItem
      Caption = 'Antigo Add'
      Visible = False
      object ItemComrastreio1: TMenuItem
        Caption = '&Item &Com rastreio'
        OnClick = ItemComrastreio1Click
      end
      object ItemSemrastreio1: TMenuItem
        Caption = 'Item &Sem rastreio'
        Enabled = False
        OnClick = ItemSemrastreio1Click
      end
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      OnClick = InformaNmerodaRME1Click
    end
    object InformaItemdeNFe1: TMenuItem
      Caption = '&Informa Item de NFe (NFi)'
      OnClick = InformaItemdeNFe1Click
    end
    object IMEIdeorigem1: TMenuItem
      Caption = 'IME-I de origem'
      Enabled = False
      OnClick = IMEIdeorigem1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 308
    Top = 572
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CorrigeFornecedores1: TMenuItem
      Caption = 'Corrige Fornecedores'
      OnClick = CorrigeFornecedores1Click
    end
    object CorrigeNFeitensVS1: TMenuItem
      Caption = 'Corrige NFe itens VS'
      OnClick = CorrigeNFeitensVS1Click
    end
    object RecalculaTotais1: TMenuItem
      Caption = 'Recalcula Totais'
      OnClick = RecalculaTotais1Click
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'OR ent.Cliente2="V"'
      'OR Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 856
    Top = 4
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClienteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 856
    Top = 48
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 924
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 924
    Top = 48
  end
  object PMImprime: TPopupMenu
    Left = 12
    object SadaxPedido1: TMenuItem
      Caption = 'Sa'#237'da x Pedido'
      OnClick = SadaxPedido1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object PackingList1: TMenuItem
      Caption = 'Packing List - Modelo A1'
      OnClick = PackingList1Click
    end
    object PackingListModeloA21: TMenuItem
      Caption = 'Packing List - Modelo A2'
      OnClick = PackingListModeloA21Click
    end
    object PackingListModelo21: TMenuItem
      Caption = 'Packing List - Modelo B1'
      OnClick = PackingListModelo21Click
    end
  end
  object QrPallets: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPalletsCalcFields
    SQL.Strings = (
      'SELECT IF(cou.ArtigoImp = "", '
      '  CONCAT(gg1.Nome, '
      '  IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      
        '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) ' +
        'TXT_ARTIGO,'
      'cou.ArtigoImp, cou.ClasseImp, '
      'wmi.Pallet, SUM(wmi.Pecas) Pecas, '
      'SUM(wmi.AreaM2) AreaM2, SUM(wmi.AreaP2) AreaP2, '
      
        'IF(SUM(wmi.Pecas) = 0, 0, SUM(wmi.AreaM2) / SUM(wmi.Pecas)) Medi' +
        'aM2, '
      'CASE '
      
        'WHEN SUM(wmi.AreaM2) / SUM(wmi.Pecas) < cou.MediaMinM2 THEN 0.00' +
        '0 '
      
        'WHEN SUM(wmi.AreaM2) / SUM(wmi.Pecas) > cou.MediaMaxM2 THEN 2.00' +
        '0 '
      'ELSE 1.000 END FaixaMediaM2, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'GROUP BY wmi.Pallet '
      'ORDER BY wmi.Pallet')
    Left = 152
    Top = 352
    object QrPalletsPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrPalletsGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrPalletsFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrPalletsNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrPalletsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrPalletsArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrPalletsClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrPalletsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPalletsMediaM2: TFloatField
      FieldName = 'MediaM2'
      DisplayFormat = '0.00'
    end
    object QrPalletsFaixaMediaM2: TFloatField
      FieldName = 'FaixaMediaM2'
    end
    object QrPalletsTXT_ARTIGO: TWideStringField
      FieldName = 'TXT_ARTIGO'
      Size = 512
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 152
    Top = 400
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'ArtigoImp=ArtigoImp'
      'ClasseImp=ClasseImp'
      'SEQ=SEQ'
      'MediaM2=MediaM2'
      'FaixaMediaM2=FaixaMediaM2'
      'TXT_ARTIGO=TXT_ARTIGO')
    DataSet = QrPallets
    BCDToCurrency = False
    DataSetOptions = []
    Left = 60
    Top = 388
  end
  object frxWET_CURTI_019_00_A1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42668.603069016200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_019_00_A1GetValue
    Left = 60
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsVSOutCab
        DataSetName = 'frxDsVSOutCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 177.637895350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000010000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List [VARF_CODIGO]  -  NF [VARF_NF]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 154.960730000000000000
          Width = 215.433014720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 154.960730000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 154.960730000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 154.960730000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 154.960730000000000000
          Width = 151.181102360000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 154.960730000000000000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora fatura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtViagem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtEntrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora chegada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 680.315356060000000000
          Height = 22.677165350000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Obs.: [frxDsVSOutCab."Nome"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 215.433014720000000000
          Height = 22.677165350000000000
          DataField = 'TXT_ARTIGO'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."TXT_ARTIGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."Pecas">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."AreaM2">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 151.181102360000000000
          Height = 22.677165354330710000
          DataField = 'ClasseImp'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."ClasseImp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 22.677165354330710000
          DataField = 'SEQ'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795285350000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 498.897764720000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD002)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 15.118119999999980000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."AreaM2">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 15.118119999999980000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."Pecas">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrTribIncIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 444
    Top = 344
    object QrTribIncItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTribIncItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrTribIncItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTribIncItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTribIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTribIncItsData: TDateField
      FieldName = 'Data'
    end
    object QrTribIncItsHora: TTimeField
      FieldName = 'Hora'
    end
    object QrTribIncItsValorFat: TFloatField
      FieldName = 'ValorFat'
    end
    object QrTribIncItsBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTribIncItsPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '0.00'
    end
    object QrTribIncItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTribIncItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTribIncItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTribIncItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTribIncItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTribIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTribIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTribIncItsTributo: TIntegerField
      FieldName = 'Tributo'
    end
    object QrTribIncItsVUsoCalc: TFloatField
      FieldName = 'VUsoCalc'
    end
    object QrTribIncItsOperacao: TSmallintField
      FieldName = 'Operacao'
    end
    object QrTribIncItsNO_Tributo: TWideStringField
      FieldName = 'NO_Tributo'
      Size = 60
    end
    object QrTribIncItsFatorDC: TSmallintField
      FieldName = 'FatorDC'
    end
  end
  object DsTribIncIts: TDataSource
    DataSet = QrTribIncIts
    Left = 444
    Top = 392
  end
  object PMTribIncIts: TPopupMenu
    Left = 780
    Top = 572
    object IncluiTributo1: TMenuItem
      Caption = '&Inclui Tributo'
      OnClick = IncluiTributo1Click
    end
    object AlteraTributoAtual1: TMenuItem
      Caption = '&Altera Tributo Atual'
      OnClick = AlteraTributoAtual1Click
    end
    object ExcluiTributoAtual1: TMenuItem
      Caption = '&Exclui Tributo Atual'
      OnClick = ExcluiTributoAtual1Click
    end
  end
  object QrVSOutNFeCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSOutNFeCabBeforeClose
    AfterScroll = QrVSOutNFeCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM vsoutnfecab'
      'WHERE MovimCod=:P0')
    Left = 272
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSOutNFeCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSOutNFeCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSOutNFeCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSOutNFeCabOriCod: TIntegerField
      FieldName = 'OriCod'
    end
    object QrVSOutNFeCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeCab: TDataSource
    DataSet = QrVSOutNFeCab
    Left = 272
    Top = 392
  end
  object QrVSOutNFeIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSOutNFeItsBeforeClose
    AfterScroll = QrVSOutNFeItsAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, voi.* '
      'FROM vsoutnfi voi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE voi.Codigo=0')
    Left = 364
    Top = 344
    object QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSOutNFeItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSOutNFeItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSOutNFeItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSOutNFeItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSOutNFeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSOutNFeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsTpCalcVal: TSmallintField
      FieldName = 'TpCalcVal'
    end
    object QrVSOutNFeItsValorU: TFloatField
      FieldName = 'ValorU'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSOutNFeItsTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
    object QrVSOutNFeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeIts: TDataSource
    DataSet = QrVSOutNFeIts
    Left = 364
    Top = 392
  end
  object PMVSOutNFeCab: TPopupMenu
    OnPopup = PMVSOutNFeCabPopup
    Left = 544
    Top = 556
    object IncluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Inclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = IncluiSerieNumeroDeNFe1Click
    end
    object AlteraSerieNumeroDeNFe1: TMenuItem
      Caption = 'Aterai s'#233'rie/ n'#250'mero de NFe'
      OnClick = AlteraSerieNumeroDeNFe1Click
    end
    object ExcluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Exclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = ExcluiSerieNumeroDeNFe1Click
    end
  end
  object PMVSOutNFeIts: TPopupMenu
    OnPopup = PMVSOutNFeItsPopup
    Left = 672
    Top = 564
    object IncluiItemdeNFeIts1: TMenuItem
      Caption = '&Inclui item de NF'
      OnClick = IncluiitemdeNF1Click
    end
    object AlteraItemdeNFeIts1: TMenuItem
      Caption = '&Altera item de NF'
      OnClick = AlteraitemdeNF1Click
    end
    object ExcluiItemdeNFeIts1: TMenuItem
      Caption = '&Exclui item de NF'
      OnClick = ExcluiItemdeNFeIts1Click
    end
  end
  object frxWET_CURTI_019_00_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_019_00_A1GetValue
    Left = 60
    Top = 488
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsVSOutCab
        DataSetName = 'frxDsVSOutCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 177.637895350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000010000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List [VARF_CODIGO]  -  NF [VARF_NF]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 154.960730000000000000
          Width = 143.621944720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 154.960730000000000000
          Width = 177.637866060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 154.960730000000000000
          Width = 64.251951420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 154.960730000000000000
          Width = 75.590551180000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 154.960730000000000000
          Width = 90.708654090000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 154.960629921259800000
          Width = 30.236210710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sq.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 154.960730000000000000
          Width = 98.267731180000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora fatura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtViagem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtEntrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora chegada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 680.315356060000000000
          Height = 22.677165350000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Obs.: [frxDsVSOutCab."Nome"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795275590000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 143.621944720000000000
          Height = 37.795275590000000000
          DataField = 'TXT_ARTIGO'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."TXT_ARTIGO"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 86.929146060000000000
          Height = 37.795275590000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Width = 64.251951420000000000
          Height = 37.795275590000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."Pecas">]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 75.590551180000000000
          Height = 37.795275590000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."AreaM2">]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 90.708654090000000000
          Height = 37.795275590000000000
          DataField = 'ClasseImp'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."ClasseImp"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236210710000000000
          Height = 37.795275590000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 98.267718980000000000
          Height = 37.795275590000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."PesoKg">]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Width = 90.708676060000000000
          Height = 37.795275590000000000
          DataField = 'NO_Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."NO_Pallet"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913395590000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 442.204814720000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD002)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."AreaM2">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 15.118119999999980000
          Width = 64.251961180000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."Pecas">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 15.118119999999980000
          Width = 98.267731180000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."PesoKg">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxWET_CURTI_019_00_A2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_019_00_A1GetValue
    Left = 208
    Top = 444
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsVSOutCab
        DataSetName = 'frxDsVSOutCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 177.637795270000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000010000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List [VARF_CODIGO]  -  NF [VARF_NF]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 154.960730000000000000
          Width = 215.433014720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 154.960730000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 154.960730000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 154.960730000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 154.960730000000000000
          Width = 151.181102360000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 154.960629921259800000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora fatura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtViagem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 98.267780000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutCab."DtEntrega"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 75.590600000000000000
          Width = 226.771726770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora chegada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 680.315356060000000000
          Height = 22.677165350000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Obs.: [frxDsVSOutCab."Nome"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354330710000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 215.433014720000000000
          Height = 45.354330710000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."TXT_ARTIGO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 45.354330708661420000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 45.354330708661420000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."Pecas">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 45.354330708661420000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsPallets."AreaM2">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 151.181102360000000000
          Height = 45.354330708661420000
          DataField = 'ClasseImp'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."ClasseImp"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 45.354330708661420000
          DataField = 'SEQ'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795285350000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118119999999980000
          Width = 498.897764720000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD002)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 15.118119999999980000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."AreaM2">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 15.118119999999980000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<SUM(<frxDsPallets."Pecas">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeNFeitensVSapartirdesteprocesso1: TMenuItem
      Caption = '&Corrige NFe itens VS a partir desta sa'#237'da'
      OnClick = CorrigeNFeitensVSapartirdesteprocesso1Click
    end
    object CorrigeMovimIDapartirdestavenda1: TMenuItem
      Caption = 'Corrige MovimID a partir desta sa'#237'da'
      OnClick = CorrigeMovimIDapartirdestavenda1Click
    end
  end
  object frxDsVSOutCab: TfrxDBDataset
    UserName = 'frxDsVSOutCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtVenda=DtVenda'
      'DtViagem=DtViagem'
      'DtEntrega=DtEntrega'
      'Cliente=Cliente'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_CLIENTE=NO_CLIENTE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'NFV=NFV'
      'TemIMEIMrt=TemIMEIMrt'
      'SerieV=SerieV'
      'Nome=Nome')
    DataSet = QrVSOutCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 616
    Top = 88
  end
  object QrVSMOEnvAvu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvAvuBeforeClose
    AfterScroll = QrVSMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 660
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrVSMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrVSMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrVSMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrVSMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrVSMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrVSMOEnvAvuCFTMA_Pecas: TFloatField
      FieldName = 'CFTMA_Pecas'
    end
    object QrVSMOEnvAvuCFTMA_PesoKg: TFloatField
      FieldName = 'CFTMA_PesoKg'
    end
    object QrVSMOEnvAvuCFTMA_AreaM2: TFloatField
      FieldName = 'CFTMA_AreaM2'
    end
    object QrVSMOEnvAvuCFTMA_AreaP2: TFloatField
      FieldName = 'CFTMA_AreaP2'
    end
    object QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
    end
    object QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
    end
    object QrVSMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvAvu: TDataSource
    DataSet = QrVSMOEnvAvu
    Left = 660
    Top = 384
  end
  object QrVSMOEnvAVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 660
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField
      FieldName = 'VSMOEnvAvu'
    end
    object QrVSMOEnvAVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvAVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvAVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvAVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvAVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvAVMI: TDataSource
    DataSet = QrVSMOEnvAVMI
    Left = 660
    Top = 484
  end
  object DsCli: TDataSource
    DataSet = QrCli
    Left = 820
    Top = 340
  end
  object QrCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCliAfterOpen
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0' +
        '.000 NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      
        'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END + 0.000 UF,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0' +
        '.000 Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0' +
        '.000 CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ' '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 820
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrEntregaCli: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCliCalcFields
    SQL.Strings = (
      'SELECT en.L_CNPJ, en.L_CPF,'
      'en.L_Nome,'
      'en.LLograd  Lograd,'
      'lll.Nome    NOMELOGRAD,'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCodMunici,'
      'mun.Nome NO_MUNICI, '
      'ufl.Nome    NOMEUF,'
      'en.LCEP     CEP,'
      'en.LCodiPais    CodiPais,'
      'pai.Nome NO_PAIS, '
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LEmail     Email,'
      'en.L_IE'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'WHERE en.Codigo=:P0')
    Left = 820
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaCliL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrEntregaCliL_CPF: TWideStringField
      FieldName = 'L_CPF'
      Size = 18
    end
    object QrEntregaCliL_Nome: TWideStringField
      FieldName = 'L_Nome'
      Size = 60
    end
    object QrEntregaCliLograd: TSmallintField
      FieldName = 'Lograd'
    end
    object QrEntregaCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaCliNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaCliLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrEntregaCliNO_MUNICI: TWideStringField
      FieldName = 'NO_MUNICI'
      Size = 100
    end
    object QrEntregaCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaCliCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaCliCodiPais: TIntegerField
      FieldName = 'CodiPais'
    end
    object QrEntregaCliNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
    object QrEntregaCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaCliEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntregaCliL_IE: TWideStringField
      FieldName = 'L_IE'
    end
  end
  object QrEntregaEnti: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaEntiCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 820
    Top = 392
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrEntregaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrEntregaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntregaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntregaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEntregaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEntregaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntregaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntregaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntregaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrEntregaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEntregaEntiNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrEntregaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntregaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrEntregaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaEntiCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEntregaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntregaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEntregaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEntregaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntregaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntregaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntregaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntregaEntiE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 512
      Calculated = True
    end
    object QrEntregaEntiCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaEntiTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
  end
  object QrVSPedCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPedCabAfterOpen
    BeforeClose = QrVSPedCabBeforeClose
    SQL.Strings = (
      'SELECT'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP,'
      'pc.Nome NO_CONDICAOPG,'
      'vpc.*'
      'FROM vspedcab vpc'
      'LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor'
      'LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg'
      'WHERE vpc.Codigo > 0'
      '')
    Left = 888
    Top = 389
    object QrVSPedCabNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrVSPedCabNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrVSPedCabNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrVSPedCabNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrVSPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPedCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSPedCabVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrVSPedCabDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrVSPedCabValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPedCabObz: TWideStringField
      FieldName = 'Obz'
      Size = 255
    end
    object QrVSPedCabTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrVSPedCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrVSPedCabPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
    object QrVSPedCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSPedCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPedCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPedCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPedCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPedCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPedCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPedCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QrVSPedCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QrVSPedCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00 '
    end
    object QrVSPedCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00 '
    end
    object QrVSPedCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPedCabNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrVSPedCabEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
    end
    object QrVSPedCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrVSPedCabL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
  end
  object DsVSPedCab: TDataSource
    DataSet = QrVSPedCab
    Left = 888
    Top = 433
  end
  object QrCorda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Codigo'
      'FROM vspedits'
      'WHERE Status < 9')
    Left = 88
    Top = 168
    object QrCordaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxWET_CURTI_019_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 45052.695313622690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_019_00_A1GetValue
    Left = 368
    Top = 144
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVmiNfIt
        DataSetName = 'frxDsVmiNfIt'
      end
      item
        DataSet = frxDsVmiNfImeis
        DataSetName = 'frxDsVmiNfImeis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370130000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000000000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List: [VARF_CODIGO]  -  Pedido: [VARF_PEDIDO_CLI]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVmiNfIt
        DataSetName = 'frxDsVmiNfIt'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item NFe')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'ItemNFe'
          DataSet = frxDsVmiNfIt
          DataSetName = 'frxDsVmiNfIt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVmiNfIt."ItemNFe"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'It.Pedido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'PedItsVda'
          DataSet = frxDsVmiNfIt
          DataSetName = 'frxDsVmiNfIt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVmiNfIt."PedItsVda"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 498.897598740000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo NFe')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 18.897650000000000000
          Width = 498.897598740000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVmiNfIt
          DataSetName = 'frxDsVmiNfIt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVmiNfIt."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'GGXRcl'
          DataSet = frxDsVmiNfIt
          DataSetName = 'frxDsVmiNfIt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVmiNfIt."GGXRcl"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DD001: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVmiNfImeis
        DataSetName = 'frxDsVmiNfImeis'
        RowCount = 0
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'Pallet'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."Pallet"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Width = 56.692588740000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."Pecas"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'GraGruX'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."GraGruX"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."PesoKg"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 64.251968503937010000
          Height = 18.897650000000000000
          DataField = 'AreaM2'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."AreaM2"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataField = 'AreaP2'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."AreaP2"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataField = 'PrecoVal'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."PrecoVal"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataField = 'TotalItem'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."TotalItem"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'Simbolo'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."Simbolo"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'MediaP2'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."MediaP2"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 41.574803149606300000
          Height = 18.897650000000000000
          DataField = 'MediaM2'
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVmiNfImeis."MediaM2"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Width = 56.692588740000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduz.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 64.251968503937010000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PRECO_UNIT]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Moeda')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 52.913385826771650000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ft'#178'/p'#231)
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 41.574803149606300000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Width = 56.692588740000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVmiNfImeis."Pecas">,DD001)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVmiNfImeis."PesoKg">,DD001)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVmiNfImeis."AreaM2">,DD001)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVmiNfImeis."AreaP2">,DD001)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590238740000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVmiNfImeis."TotalItem">,DD001)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsVmiNfImeis."AreaP2">,DD001) / SUM(<frxDsVmiNfImeis."P' +
              'ecas">,DD001)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          DataSet = frxDsVmiNfImeis
          DataSetName = 'frxDsVmiNfImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsVmiNfImeis."AreaM2">,DD001) / SUM(<frxDsVmiNfImeis."P' +
              'ecas">,DD001)]')
          ParentFont = False
        end
      end
    end
  end
  object QrVmiNfIt: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrVmiNfItAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT vmi.ItemNFe, vmi.GGXRcl,'
      'CONCAT(gg1.Nome,'
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),'
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome' +
        ')))'
      'NO_PRD_TAM_COR'
      'FROM vsmovits vmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GGXRcl'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE vmi.MovimID=2'
      'AND vmi.Codigo=31'
      'ORDER BY vmi.ItemNFe, vmi.GGXRcl')
    Left = 368
    Top = 197
    object QrVmiNfItItemNFe: TIntegerField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVmiNfItPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVmiNfItGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrVmiNfItNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object frxDsVmiNfIt: TfrxDBDataset
    UserName = 'frxDsVmiNfIt'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ItemNFe=ItemNFe'
      'PedItsVda=PedItsVda'
      'GGXRcl=GGXRcl'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrVmiNfIt
    BCDToCurrency = False
    DataSetOptions = []
    Left = 368
    Top = 248
  end
  object QrVmiNfImeis: TMySQLQuery
    SQL.Strings = (
      'SELECT MovimCod, vmi.Pallet, vmi.GraGruX, SUM(vmi.Pecas) Pecas,'
      'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2,'
      'SUM(vmi.AreaP2) AreaP2, mda.Sigla, mda.Simbolo, pvi.PrecoVal,'
      '('
      '  CASE PrecoTipo'
      '    WHEN 0 THEN 0.0000000000'
      '    WHEN 1 THEN vmi.Pecas'
      '    WHEN 2 THEN vmi.PesoKg'
      '    WHEN 3 THEN vmi.AreaM2'
      '    WHEN 4 THEN vmi.AreaP2'
      '    ELSE 0.0000000000'
      '  END * pvi.PrecoVal'
      ') TotalItem'
      'FROM vsmovits vmi'
      'LEFT JOIN vspedits pvi ON pvi.Controle=vmi.PedItsVda'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvi.PrecoMoeda'
      'WHERE vmi.MovimCod>0'
      'GROUP BY vmi.MovimCod, vmi.Pallet')
    Left = 456
    Top = 197
    object QrVmiNfImeisPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVmiNfImeisGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVmiNfImeisPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVmiNfImeisPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVmiNfImeisAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVmiNfImeisAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVmiNfImeisSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrVmiNfImeisSimbolo: TWideStringField
      FieldName = 'Simbolo'
      Size = 15
    end
    object QrVmiNfImeisPrecoVal: TFloatField
      FieldName = 'PrecoVal'
    end
    object QrVmiNfImeisTotalItem: TFloatField
      FieldName = 'TotalItem'
    end
    object QrVmiNfImeisMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrVmiNfImeisMediaP2: TFloatField
      FieldName = 'MediaP2'
    end
    object QrVmiNfImeisPrecoTipo: TIntegerField
      FieldName = 'PrecoTipo'
    end
  end
  object frxDsVmiNfImeis: TfrxDBDataset
    UserName = 'frxDsVmiNfImeis'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Sigla=Sigla'
      'Simbolo=Simbolo'
      'PrecoVal=PrecoVal'
      'TotalItem=TotalItem'
      'MediaM2=MediaM2'
      'MediaP2=MediaP2'
      'PrecoTipo=PrecoTipo')
    DataSet = QrVmiNfImeis
    BCDToCurrency = False
    DataSetOptions = []
    Left = 456
    Top = 248
  end
end
