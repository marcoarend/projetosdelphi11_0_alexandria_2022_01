object FmVSImpMOEnvRet: TFmVSImpMOEnvRet
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-168 :: Relat'#243'rio de Controle de Cobran'#231'a de MO'
  ClientHeight = 645
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 483
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 105
        Align = alTop
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 88
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label35: TLabel
            Left = 496
            Top = 4
            Width = 240
            Height = 13
            Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            QryName = 'QrVSGerArt'
            QryCampo = 'Empresa'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrVSGerArt'
            QryCampo = 'Empresa'
            UpdCampo = 'Empresa'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdFornecMO: TdmkEditCB
            Left = 496
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornecMO
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornecMO: TdmkDBLookupComboBox
            Left = 552
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador
            TabOrder = 3
            dmkEditCB = EdFornecMO
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGRelatorio: TRadioGroup
            Left = 520
            Top = 44
            Width = 457
            Height = 45
            Caption = ' Relat'#243'rio: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'NFe remessa MP'
              'NFe cobran'#231'a MO'
              'IME-Is orf'#227'os')
            TabOrder = 5
          end
          object RGNFSorc: TRadioGroup
            Left = 8
            Top = 43
            Width = 349
            Height = 45
            Caption = ' NFe a ser filtrada (s'#233'rie e n'#186' inicial ao lado): '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'NFe remessa MP'
              'NFe cobran'#231'a MO')
            TabOrder = 4
          end
        end
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 186
        Width = 1008
        Height = 297
        Align = alClient
        DataSource = DsOrfaos
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
      end
      object Panel55: TPanel
        Left = 0
        Top = 105
        Width = 1008
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object GroupBox23: TGroupBox
          Left = 8
          Top = 4
          Width = 257
          Height = 73
          Caption = ' Per'#237'odo: '
          TabOrder = 0
          object TP24DataIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 120
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object Ck24DataIni: TCheckBox
            Left = 8
            Top = 20
            Width = 120
            Height = 17
            Caption = 'Data inicial'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object Ck24DataFim: TCheckBox
            Left = 132
            Top = 20
            Width = 120
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
          end
          object TP24DataFim: TdmkEditDateTimePicker
            Left = 132
            Top = 40
            Width = 120
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox24: TGroupBox
          Left = 272
          Top = 4
          Width = 253
          Height = 73
          Caption = ' Intervalo de NF-es emitidas: '
          TabOrder = 1
          object Panel57: TPanel
            Left = 2
            Top = 15
            Width = 249
            Height = 56
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label57: TLabel
              Left = 12
              Top = 4
              Width = 30
              Height = 13
              Caption = 'Inicial:'
            end
            object Label58: TLabel
              Left = 92
              Top = 4
              Width = 25
              Height = 13
              Caption = 'Final:'
            end
            object SpeedButton1: TSpeedButton
              Left = 224
              Top = 20
              Width = 21
              Height = 21
              Caption = '?'
              OnClick = SpeedButton1Click
            end
            object Ed24NFeIni: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object Ed24NFeFim: TdmkEdit
              Left = 92
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object Ck24Serie: TCheckBox
              Left = 176
              Top = 0
              Width = 53
              Height = 17
              Caption = 'S'#233'rie:'
              TabOrder = 2
            end
            object Ed24Serie: TdmkEdit
              Left = 176
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 531
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 575
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 65528
    Top = 65523
  end
  object QrVSMOEnvRet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.VSVMI_Controle=4668')
    Left = 208
    Top = 300
    object QrVSMOEnvRetVSVMI_SerNF: TIntegerField
      FieldName = 'VSVMI_SerNF'
    end
    object QrVSMOEnvRetVSVMI_nNF: TIntegerField
      FieldName = 'VSVMI_nNF'
    end
    object QrVSMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMOEnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
    end
    object QrVSMOEnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
    end
    object QrVSMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
    end
    object QrVSMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
    end
    object QrVSMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMOEnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
    end
    object QrVSMOEnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
    end
    object QrVSMOEnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
    end
    object QrVSMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMOEnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
    end
    object QrVSMOEnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
    end
    object QrVSMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
    end
    object QrVSMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMOEnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
    end
    object QrVSMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
    end
    object QrVSMOEnvRetVSVMI_Controle: TIntegerField
      FieldName = 'VSVMI_Controle'
    end
    object QrVSMOEnvRetVSVMI_Codigo: TIntegerField
      FieldName = 'VSVMI_Codigo'
    end
    object QrVSMOEnvRetVSVMI_MovimID: TIntegerField
      FieldName = 'VSVMI_MovimID'
    end
    object QrVSMOEnvRetVSVMI_MovimNiv: TIntegerField
      FieldName = 'VSVMI_MovimNiv'
    end
    object QrVSMOEnvRetVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvRetVSVMI_Empresa: TIntegerField
      FieldName = 'VSVMI_Empresa'
    end
    object QrVSMOEnvRetLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvRetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvRetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvRetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvRetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvRetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvRetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/* MySQL Error Code: (1054)'
      'Unknown column '#39'scl.Nome'#39' in '#39'field list'#39
      ''
      'Owner: FmVSMovIts'
      ' */'
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'vsf.Nome NO_SerieFch, '
      'vsp.Nome NO_Pallet, '
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, '
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1,'
      'vmi.Marca,'
      'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,'
      'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2'
      ''
      ''
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc '
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo '
      ''
      'WHERE vmi.Controle > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'AND vmi.Controle=27493')
    Left = 124
    Top = 301
    object QrVSMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = 'vsserfch.Nome'
      Size = 60
    end
    object QrVSMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrVSMovItsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVSMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVSMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVSMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVSMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVSMovItsMarca_1: TWideStringField
      FieldName = 'Marca_1'
    end
    object QrVSMovItsLnkNivXtr1: TLargeintField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrVSMovItsLnkNivXtr2: TLargeintField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrVSMovItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
    end
    object QrVSMovItsNFeSer: TIntegerField
      FieldName = 'NFeSer'
    end
  end
  object QrVSPWEBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 292
    Top = 297
    object QrVSPWEBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object QrVSMO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM _vs_mo_')
    Left = 36
    Top = 204
    object QrVSMOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMONFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMONFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMONFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMONFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMONFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMONFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMONFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
    end
    object QrVSMONFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
    end
    object QrVSMONFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
    end
    object QrVSMONFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
    end
    object QrVSMONFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
    end
    object QrVSMONFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
    end
    object QrVSMONFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
    end
    object QrVSMONFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMONFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMONFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMONFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMONFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMONFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMONFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
    end
    object QrVSMONFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
    end
    object QrVSMONFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
    end
    object QrVSMONFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
    end
    object QrVSMONFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
    end
    object QrVSMOVSVMI_Controle: TIntegerField
      FieldName = 'VSVMI_Controle'
    end
    object QrVSMOVSVMI_Codigo: TIntegerField
      FieldName = 'VSVMI_Codigo'
    end
    object QrVSMOVSVMI_MovimID: TIntegerField
      FieldName = 'VSVMI_MovimID'
    end
    object QrVSMOVSVMI_MovimNiv: TIntegerField
      FieldName = 'VSVMI_MovimNiv'
    end
    object QrVSMOVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOVSVMI_Empresa: TIntegerField
      FieldName = 'VSVMI_Empresa'
    end
    object QrVSMOVSVMI_SerNF: TIntegerField
      FieldName = 'VSVMI_SerNF'
    end
    object QrVSMOVSVMI_nNF: TIntegerField
      FieldName = 'VSVMI_nNF'
    end
    object QrVSMOVSVMI_Pecas: TFloatField
      FieldName = 'VSVMI_Pecas'
    end
    object QrVSMOVSVMI_PesoKg: TFloatField
      FieldName = 'VSVMI_PesoKg'
    end
    object QrVSMOVSVMI_AreaM2: TFloatField
      FieldName = 'VSVMI_AreaM2'
    end
    object QrVSMOVSVMI_AreaM2WB: TFloatField
      FieldName = 'VSVMI_AreaM2WB'
    end
    object QrVSMOVSVMI_PercRendim: TFloatField
      FieldName = 'VSVMI_PercRendim'
    end
    object QrVSMOGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSMONO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSMOPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMODataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOVSVMI_PesoKgWB: TFloatField
      FieldName = 'VSVMI_PesoKgWB'
    end
    object QrVSMOGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVSMOVSVMI_M2Virtual: TFloatField
      FieldName = 'VSVMI_M2Virtual'
    end
    object QrVSMOSerieNF_VMI: TWideStringField
      FieldName = 'SerieNF_VMI'
      Size = 255
    end
    object QrVSMOSerieNF_CMO: TWideStringField
      FieldName = 'SerieNF_CMO'
      Size = 255
    end
    object QrVSMOSerieNF_RMP: TWideStringField
      FieldName = 'SerieNF_RMP'
      Size = 255
    end
  end
  object frxWET_CURTI_168_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '*)                                            '
      'end.')
    OnGetValue = frxWET_CURTI_168_01GetValue
    Left = 36
    Top = 296
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSMO
        DataSetName = 'frxDsVSMO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Cobran'#231'a de M'#227'o de Obra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 64.252010000000000000
          Width = 245.669362130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg WB')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitNF: TfrxMemoView
          AllowVectorExport = True
          Left = 721.890230000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Top = 64.252010000000000000
          Width = 41.574729920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF MP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178' WB')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Rend.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819420000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 922.205320000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Fornecedor de MO: [VARF_FORNEC_MO]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178' virtual')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        DataSet = frxDsVSMO
        DataSetName = 'frxDsVSMO'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 245.669362130000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSMO."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'VSVMI_Pecas'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,###.##;-#,###,###,###.##; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'VSVMI_PesoKgWB'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_PesoKgWB"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSMO."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'VSVMI_Controle'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNumNF: TfrxMemoView
          AllowVectorExport = True
          Left = 721.890230000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#;-#;??????'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."NFCMO_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_nNF'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#;-#;??????'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."NFRMP_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_AreaM2WB"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_PercRendim"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819420000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."NFCMO_AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'NFCMO_CusMOM2'
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."NFCMO_CusMOM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 922.205320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."NFCMO_ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSMO."VSVMI_M2Virtual"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 366.614410000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 321.260050000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 351.496094720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 3.779530000000022000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,###.##;-#,###,###,###.##; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_PesoKgWB">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_AreaM2WB">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 721.890230000000000000
          Top = 3.779530000000022000
          Width = 86.929160710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819420000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."NFCMO_AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 922.205320000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."NFCMO_ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Top = 3.779530000000022000
          Width = 45.354316060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1) = 0, 0,'
            '  ('
            '    SUM(<frxDsVSMO."NFCMO_ValorT">,MD002,1)'
            '    /'
            '    SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)'
            '  )'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 3.779530000000022000
          Width = 45.354316060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1) = 0, 0,'
            '  ('
            '    ('
            '      ('
            '        SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)'
            '       -'
            '        SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1)'
            '      )'
            '      /'
            '      (SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1))'
            '    )'
            '   * 100'
            '  )'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsVSMO."VSVMI_nNF"'
        object MeGrupo1Head: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'NF: [frxDsVSMO."VSVMI_nNF"] [VARF_SALDO_ABERTO_NF]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 990.236860000000000000
        object MeGrupo1Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 351.496094720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total da NF [frxDsVSMO."VSVMI_nNF"]:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,###.##;-#,###,###,###.##; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_AreaM2WB">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 45.354316060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1) = 0, 0,'
            '  ('
            '    ('
            '      ('
            '        SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)'
            '       -'
            '        SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1)'
            '      )'
            '      /'
            '      (SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1))'
            '    )'
            '   * 100'
            '  )'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 721.890230000000000000
          Width = 86.929160710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 808.819420000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."NFCMO_AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 922.205320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."NFCMO_ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Width = 45.354316060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1) = 0, 0,'
            '  ('
            '    SUM(<frxDsVSMO."NFCMO_ValorT">,MD002,1)'
            '    /'
            '    SUM(<frxDsVSMO."VSVMI_AreaM2">,MD002,1)'
            '  )'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSMO."VSVMI_M2Virtual">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsVSMO: TfrxDBDataset
    UserName = 'frxDsVSMO'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NFCMO_FatID=NFCMO_FatID'
      'NFCMO_FatNum=NFCMO_FatNum'
      'NFCMO_Empresa=NFCMO_Empresa'
      'NFCMO_nItem=NFCMO_nItem'
      'NFCMO_SerNF=NFCMO_SerNF'
      'NFCMO_nNF=NFCMO_nNF'
      'NFCMO_Pecas=NFCMO_Pecas'
      'NFCMO_PesoKg=NFCMO_PesoKg'
      'NFCMO_AreaM2=NFCMO_AreaM2'
      'NFCMO_AreaP2=NFCMO_AreaP2'
      'NFCMO_CusMOM2=NFCMO_CusMOM2'
      'NFCMO_CusMOKG=NFCMO_CusMOKG'
      'NFCMO_ValorT=NFCMO_ValorT'
      'NFRMP_FatID=NFRMP_FatID'
      'NFRMP_FatNum=NFRMP_FatNum'
      'NFRMP_Empresa=NFRMP_Empresa'
      'NFRMP_nItem=NFRMP_nItem'
      'NFRMP_SerNF=NFRMP_SerNF'
      'NFRMP_nNF=NFRMP_nNF'
      'NFRMP_Pecas=NFRMP_Pecas'
      'NFRMP_PesoKg=NFRMP_PesoKg'
      'NFRMP_AreaM2=NFRMP_AreaM2'
      'NFRMP_AreaP2=NFRMP_AreaP2'
      'NFRMP_ValorT=NFRMP_ValorT'
      'VSVMI_Controle=VSVMI_Controle'
      'VSVMI_Codigo=VSVMI_Codigo'
      'VSVMI_MovimID=VSVMI_MovimID'
      'VSVMI_MovimNiv=VSVMI_MovimNiv'
      'VSVMI_MovimCod=VSVMI_MovimCod'
      'VSVMI_Empresa=VSVMI_Empresa'
      'VSVMI_SerNF=VSVMI_SerNF'
      'VSVMI_nNF=VSVMI_nNF'
      'VSVMI_Pecas=VSVMI_Pecas'
      'VSVMI_PesoKg=VSVMI_PesoKg'
      'VSVMI_AreaM2=VSVMI_AreaM2'
      'VSVMI_AreaM2WB=VSVMI_AreaM2WB'
      'VSVMI_PercRendim=VSVMI_PercRendim'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'DataHora=DataHora'
      'Ativo=Ativo'
      'VSVMI_PesoKgWB=VSVMI_PesoKgWB'
      'Grandeza=Grandeza'
      'VSVMI_M2Virtual=VSVMI_M2Virtual'
      'SerieNF_VMI=SerieNF_VMI'
      'SerieNF_CMO=SerieNF_CMO'
      'SerieNF_RMP=SerieNF_RMP')
    DataSet = QrVSMO
    BCDToCurrency = False
    DataSetOptions = []
    Left = 36
    Top = 252
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 368
    Top = 208
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 368
    Top = 252
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 224
    object QrSaldoPecasSdo: TFloatField
      FieldName = 'PecasSdo'
    end
    object QrSaldoAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
    end
    object QrSaldoPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
    end
  end
  object QrOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 364
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 316
    Top = 412
  end
end
