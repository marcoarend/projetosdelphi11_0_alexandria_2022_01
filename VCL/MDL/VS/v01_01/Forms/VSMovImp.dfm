object FmVSMovImp: TFmVSMovImp
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-018 :: Relat'#243'rios de Ribeira'
  ClientHeight = 799
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Relat'#243'rios de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Relat'#243'rios de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Relat'#243'rios de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 626
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 626
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 626
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 8
            Top = 0
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label54: TLabel
            Left = 888
            Top = 0
            Width = 63
            Height = 13
            Caption = 'Data relativa:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 385
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TPDataRelativa: TdmkEditDateTimePicker
            Left = 888
            Top = 16
            Width = 112
            Height = 21
            Date = 42199.000000000000000000
            Time = 0.636796111110015800
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object PCRelatorio: TPageControl
          Left = 2
          Top = 57
          Width = 1004
          Height = 567
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          OnChange = PCRelatorioChange
          object TabSheet1: TTabSheet
            Caption = ' Estoque'
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 723
              Height = 539
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object ScrollBox1: TScrollBox
                Left = 0
                Top = 0
                Width = 723
                Height = 539
                Margins.Left = 2
                Margins.Top = 2
                Margins.Right = 2
                Margins.Bottom = 2
                HorzScrollBar.Margin = 15
                HorzScrollBar.Visible = False
                VertScrollBar.ButtonSize = 15
                Align = alClient
                TabOrder = 0
                object Panel24: TPanel
                  Left = 0
                  Top = 0
                  Width = 704
                  Height = 522
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Panel25: TPanel
                    Left = 0
                    Top = 0
                    Width = 704
                    Height = 173
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel47: TPanel
                      Left = 0
                      Top = 0
                      Width = 389
                      Height = 173
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Panel46: TPanel
                        Left = 0
                        Top = 0
                        Width = 389
                        Height = 173
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label53: TLabel
                          Left = 4
                          Top = 4
                          Width = 90
                          Height = 13
                          Caption = 'Centro de estoque:'
                        end
                        object Label55: TLabel
                          Left = 4
                          Top = 31
                          Width = 57
                          Height = 13
                          Caption = 'Fornecedor:'
                        end
                        object Label10: TLabel
                          Left = 4
                          Top = 56
                          Width = 61
                          Height = 13
                          Caption = 'Cliente M.O.:'
                        end
                        object Ed00StqCenCad: TdmkEditCB
                          Left = 104
                          Top = 2
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB00StqCenCad
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB00StqCenCad: TdmkDBLookupComboBox
                          Left = 160
                          Top = 2
                          Width = 225
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'Nome'
                          ListSource = Ds00StqCenCad
                          TabOrder = 1
                          dmkEditCB = Ed00StqCenCad
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object Ck00DataCompra: TCheckBox
                          Left = 8
                          Top = 84
                          Width = 381
                          Height = 17
                          Caption = 
                            'N'#227'o considerar na soma as compras que n'#227'o chegaram na empresa ai' +
                            'nda.'
                          TabOrder = 2
                        end
                        object RG00ZeroNegat: TRadioGroup
                          Left = 0
                          Top = 129
                          Width = 389
                          Height = 44
                          Align = alBottom
                          Caption = ' Estoques a serem considerados: '
                          Columns = 3
                          ItemIndex = 0
                          Items.Strings = (
                            'Somente positivos'
                            'Positivos e negativos'
                            'Todos inclusive zerados')
                          TabOrder = 4
                        end
                        object Ed00Terceiro: TdmkEditCB
                          Left = 68
                          Top = 27
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 5
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB00Terceiro
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB00Terceiro: TdmkDBLookupComboBox
                          Left = 124
                          Top = 27
                          Width = 261
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'NOMEENTIDADE'
                          ListSource = Ds00Fornecedor
                          TabOrder = 6
                          dmkEditCB = Ed00Terceiro
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object Ck00EmProcessoBH: TCheckBox
                          Left = 8
                          Top = 104
                          Width = 265
                          Height = 17
                          Caption = 'Mostrar estoque em processo de ribeira.'
                          TabOrder = 3
                        end
                        object CBClientMO: TdmkDBLookupComboBox
                          Left = 124
                          Top = 53
                          Width = 261
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'NOMECI'
                          ListSource = DsClientMO
                          TabOrder = 7
                          dmkEditCB = EdClientMO
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object EdClientMO: TdmkEditCB
                          Left = 68
                          Top = 53
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 8
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CBClientMO
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                      end
                    end
                    object GroupBox19: TGroupBox
                      Left = 389
                      Top = 0
                      Width = 315
                      Height = 173
                      Align = alClient
                      Caption = ' Tipo de material: '
                      TabOrder = 1
                      object DBG00CouNiv2: TdmkDBGridZTO
                        Left = 2
                        Top = 15
                        Width = 311
                        Height = 116
                        Align = alClient
                        DataSource = Ds00CouNiv2
                        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                        TabOrder = 0
                        TitleFont.Charset = ANSI_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Codigo'
                            Width = 39
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Nome'
                            Width = 148
                            Visible = True
                          end>
                      end
                      object Panel69: TPanel
                        Left = 2
                        Top = 131
                        Width = 311
                        Height = 40
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 1
                        object Label1: TLabel
                          Left = 4
                          Top = 16
                          Width = 32
                          Height = 13
                          Caption = 'IME-C:'
                        end
                        object Ed00MovimCod: TdmkEdit
                          Left = 44
                          Top = 12
                          Width = 80
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                        end
                      end
                    end
                  end
                  object Panel26: TPanel
                    Left = 0
                    Top = 173
                    Width = 704
                    Height = 264
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object RG00_Ordem1: TRadioGroup
                      Left = 0
                      Top = 0
                      Width = 117
                      Height = 264
                      Align = alLeft
                      Caption = ' Ordem 1:'
                      ItemIndex = 5
                      Items.Strings = (
                        'MP / Artigo'
                        'Fornecedor'
                        'Pallet'
                        'IME-I'
                        'Ficha RMP'
                        'Marca'
                        'Centro'
                        'Local'
                        'Tipo Material'
                        'Est'#225'gio Artigo'
                        'NFe'
                        'IME-C'
                        'Movimento'
                        'Cliente M.O.'
                        'Fornecedor M.O.')
                      TabOrder = 0
                    end
                    object RG00_Ordem2: TRadioGroup
                      Left = 117
                      Top = 0
                      Width = 117
                      Height = 264
                      Align = alLeft
                      Caption = ' Ordem 2:'
                      ItemIndex = 8
                      Items.Strings = (
                        'MP / Artigo'
                        'Fornecedor'
                        'Pallet'
                        'IME-I'
                        'Ficha RMP'
                        'Marca'
                        'Centro'
                        'Local'
                        'Tipo Material'
                        'Est'#225'gio Artigo'
                        'NFe'
                        'IME-C'
                        'Movimento'
                        'Cliente M.O.'
                        'Fornecedor M.O.')
                      TabOrder = 1
                    end
                    object RG00_Ordem3: TRadioGroup
                      Left = 234
                      Top = 0
                      Width = 117
                      Height = 264
                      Align = alLeft
                      Caption = ' Ordem 3:'
                      ItemIndex = 0
                      Items.Strings = (
                        'MP / Artigo'
                        'Fornecedor'
                        'Pallet'
                        'IME-I'
                        'Ficha RMP'
                        'Marca'
                        'Centro'
                        'Local'
                        'Tipo Material'
                        'Est'#225'gio Artigo'
                        'NFe'
                        'IME-C'
                        'Movimento'
                        'Cliente M.O.'
                        'Fornecedor M.O.')
                      TabOrder = 2
                    end
                    object Panel49: TPanel
                      Left = 585
                      Top = 0
                      Width = 119
                      Height = 264
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 4
                      object RG00_Agrupa: TRadioGroup
                        Left = 0
                        Top = 0
                        Width = 119
                        Height = 61
                        Align = alTop
                        Caption = ' Agrupamentos:'
                        Columns = 3
                        ItemIndex = 2
                        Items.Strings = (
                          '0'
                          '1'
                          '2'
                          '3'
                          '4')
                        TabOrder = 0
                      end
                      object Panel50: TPanel
                        Left = 0
                        Top = 61
                        Width = 119
                        Height = 203
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 1
                        object Panel58: TPanel
                          Left = 0
                          Top = 0
                          Width = 119
                          Height = 45
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 0
                          object Ck00EstoqueEm: TCheckBox
                            Left = 4
                            Top = 0
                            Width = 97
                            Height = 17
                            Caption = 'Estoque em...:'
                            TabOrder = 0
                          end
                          object TP00EstoqueEm: TdmkEditDateTimePicker
                            Left = 4
                            Top = 20
                            Width = 112
                            Height = 21
                            CalColors.TextColor = clMenuText
                            Date = 37636.000000000000000000
                            Time = 0.777157974502188200
                            TabOrder = 1
                            ReadOnly = False
                            DefaultEditMask = '!99/99/99;1;_'
                            AutoApplyEditMask = True
                            UpdType = utYes
                            DatePurpose = dmkdpNone
                          end
                        end
                        object RG00DescrAgruNoItm: TRadioGroup
                          Left = 0
                          Top = 45
                          Width = 119
                          Height = 158
                          Align = alClient
                          Caption = ' Descri'#231#227'o do item:'
                          ItemIndex = 0
                          Items.Strings = (
                            'N'#227'o definido'
                            'Artigo'
                            #218'ltimo agrupam.')
                          TabOrder = 1
                        end
                      end
                    end
                    object RG00_Ordem4: TRadioGroup
                      Left = 351
                      Top = 0
                      Width = 117
                      Height = 264
                      Align = alLeft
                      Caption = ' Ordem 4:'
                      ItemIndex = 2
                      Items.Strings = (
                        'MP / Artigo'
                        'Fornecedor'
                        'Pallet'
                        'IME-I'
                        'Ficha RMP'
                        'Marca'
                        'Centro'
                        'Local'
                        'Tipo Material'
                        'Est'#225'gio Artigo'
                        'NFe'
                        'IME-C'
                        'Movimento'
                        'Cliente M.O.'
                        'Fornecedor M.O.')
                      TabOrder = 3
                    end
                    object RG00_Ordem5: TRadioGroup
                      Left = 468
                      Top = 0
                      Width = 117
                      Height = 264
                      Align = alClient
                      Caption = ' Ordem 5:'
                      ItemIndex = 1
                      Items.Strings = (
                        'MP / Artigo'
                        'Fornecedor'
                        'Pallet'
                        'IME-I'
                        'Ficha RMP'
                        'Marca'
                        'Centro'
                        'Local'
                        'Tipo Material'
                        'Est'#225'gio Artigo'
                        'NFe'
                        'IME-C'
                        'Movimento'
                        'Cliente M.O.'
                        'Fornecedor M.O.')
                      TabOrder = 5
                    end
                  end
                  object Panel63: TPanel
                    Left = 0
                    Top = 437
                    Width = 704
                    Height = 85
                    Align = alClient
                    TabOrder = 2
                    object GroupBox28: TGroupBox
                      Left = 388
                      Top = 4
                      Width = 237
                      Height = 61
                      Caption = ' Intervalo de NF-es emitidas: '
                      TabOrder = 0
                      object Panel65: TPanel
                        Left = 2
                        Top = 15
                        Width = 233
                        Height = 44
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Label64: TLabel
                          Left = 12
                          Top = 4
                          Width = 30
                          Height = 13
                          Caption = 'Inicial:'
                        end
                        object Label65: TLabel
                          Left = 96
                          Top = 4
                          Width = 25
                          Height = 13
                          Caption = 'Final:'
                        end
                        object Ed00NFeIni: TdmkEdit
                          Left = 8
                          Top = 20
                          Width = 80
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          OnRedefinido = Ed20NFeIniRedefinido
                        end
                        object Ed00NFeFim: TdmkEdit
                          Left = 96
                          Top = 20
                          Width = 80
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                        end
                        object Ck00Serie: TCheckBox
                          Left = 180
                          Top = 0
                          Width = 53
                          Height = 17
                          Caption = 'S'#233'rie:'
                          TabOrder = 2
                        end
                        object Ed00Serie: TdmkEdit
                          Left = 180
                          Top = 20
                          Width = 45
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 3
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                        end
                      end
                    end
                    object RG00Relatorio: TRadioGroup
                      Left = 8
                      Top = 4
                      Width = 377
                      Height = 61
                      Caption = ' Relat'#243'rio de Estoque: '
                      Columns = 4
                      ItemIndex = 0
                      Items.Strings = (
                        'Nenhum'
                        'Custo'
                        'Base Venda'
                        'Base Custo')
                      TabOrder = 1
                    end
                  end
                end
              end
            end
            object Panel43: TPanel
              Left = 723
              Top = 0
              Width = 273
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Splitter1: TSplitter
                Left = 0
                Top = 201
                Width = 273
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitWidth = 275
              end
              object GroupBox16: TGroupBox
                Left = 0
                Top = 0
                Width = 273
                Height = 201
                Align = alTop
                Caption = ' Grupos de Estoque: '
                TabOrder = 0
                object DBG00GraGruY: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 269
                  Height = 184
                  Align = alClient
                  DataSource = Ds00GraGruY
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Width = 39
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Width = 238
                      Visible = True
                    end>
                end
              end
              object GroupBox18: TGroupBox
                Left = 0
                Top = 204
                Width = 273
                Height = 335
                Align = alClient
                Caption = ' Artigos: '
                TabOrder = 1
                object DBG00GraGruX: TdmkDBGridZTO
                  Left = 2
                  Top = 57
                  Width = 269
                  Height = 276
                  Align = alClient
                  DataSource = Ds00GraGruX
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 43
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR'
                      Title.Caption = 'Nome'
                      Visible = True
                    end>
                end
                object Panel75: TPanel
                  Left = 2
                  Top = 15
                  Width = 269
                  Height = 42
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel52: TPanel
                    Left = 105
                    Top = 0
                    Width = 164
                    Height = 42
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label8: TLabel
                      Left = 8
                      Top = 0
                      Width = 51
                      Height = 13
                      Caption = 'Descri'#231#227'o:'
                    end
                    object Ed00NomeGG1: TEdit
                      Left = 8
                      Top = 16
                      Width = 257
                      Height = 21
                      TabOrder = 0
                      OnChange = Ed00NomeGG1Change
                    end
                  end
                  object Panel76: TPanel
                    Left = 0
                    Top = 0
                    Width = 105
                    Height = 42
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object Label9: TLabel
                      Left = 8
                      Top = 0
                      Width = 59
                      Height = 13
                      Caption = 'Reduzido(s):'
                    end
                    object EdGGXs: TEdit
                      Left = 8
                      Top = 16
                      Width = 90
                      Height = 21
                      TabOrder = 0
                      OnChange = Ed00NomeGG1Change
                    end
                  end
                end
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Hist'#243'rico'
            ImageIndex = 1
          end
          object TabSheet3: TTabSheet
            Caption = 'Lan'#231'amentos'
            ImageIndex = 2
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 109
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label2: TLabel
                  Left = 8
                  Top = 0
                  Width = 66
                  Height = 13
                  Caption = 'Mat'#233'ria-prima:'
                end
                object Label3: TLabel
                  Left = 8
                  Top = 40
                  Width = 29
                  Height = 13
                  Caption = 'Pallet:'
                end
                object Label5: TLabel
                  Left = 8
                  Top = 88
                  Width = 144
                  Height = 13
                  Caption = 'Terceiro (fornecedor / cliente):'
                end
                object Label6: TLabel
                  Left = 540
                  Top = 88
                  Width = 28
                  Height = 13
                  Caption = 'IME-I:'
                end
                object Label37: TLabel
                  Left = 656
                  Top = 88
                  Width = 69
                  Height = 13
                  Caption = 'ID Movimento:'
                end
                object Ed02GraGruX: TdmkEditCB
                  Left = 8
                  Top = 16
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Cargo'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02GraGruX
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02GraGruX: TdmkDBLookupComboBox
                  Left = 64
                  Top = 16
                  Width = 469
                  Height = 21
                  KeyField = 'Controle'
                  ListField = 'NO_PRD_TAM_COR'
                  ListSource = Ds02GraGruX
                  TabOrder = 1
                  dmkEditCB = Ed02GraGruX
                  QryCampo = 'Cargo'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object Ed02Pallet: TdmkEditCB
                  Left = 8
                  Top = 56
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Pallet'
                  UpdCampo = 'Pallet'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02Pallet
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02Pallet: TdmkDBLookupComboBox
                  Left = 64
                  Top = 56
                  Width = 469
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = Ds02VSPallet
                  TabOrder = 3
                  dmkEditCB = Ed02Pallet
                  QryCampo = 'Pallet'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object Ed02Terceiro: TdmkEditCB
                  Left = 160
                  Top = 84
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Fornecedor'
                  UpdCampo = 'Fornecedor'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CB02Terceiro
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CB02Terceiro: TdmkDBLookupComboBox
                  Left = 216
                  Top = 84
                  Width = 317
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NOMEENTIDADE'
                  ListSource = Ds02Fornecedor
                  TabOrder = 5
                  dmkEditCB = Ed02Terceiro
                  QryCampo = 'Fornecedor'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object GroupBox3: TGroupBox
                  Left = 540
                  Top = 4
                  Width = 245
                  Height = 73
                  Caption = ' Per'#237'odo: '
                  TabOrder = 6
                  object TP02DataIni: TdmkEditDateTimePicker
                    Left = 8
                    Top = 40
                    Width = 112
                    Height = 21
                    CalColors.TextColor = clMenuText
                    Date = 37636.000000000000000000
                    Time = 0.777157974502188200
                    TabOrder = 1
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                  object Ck02DataIni: TCheckBox
                    Left = 8
                    Top = 20
                    Width = 112
                    Height = 17
                    Caption = 'Data inicial'
                    Checked = True
                    State = cbChecked
                    TabOrder = 0
                  end
                  object Ck02DataFim: TCheckBox
                    Left = 124
                    Top = 20
                    Width = 112
                    Height = 17
                    Caption = 'Data final'
                    TabOrder = 2
                  end
                  object TP02DataFim: TdmkEditDateTimePicker
                    Left = 124
                    Top = 40
                    Width = 112
                    Height = 21
                    Date = 37636.000000000000000000
                    Time = 0.777203761601413100
                    TabOrder = 3
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                end
                object Ed02Controle: TdmkEdit
                  Left = 572
                  Top = 84
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object Ed02MovimID: TdmkEdit
                  Left = 728
                  Top = 84
                  Width = 24
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '-1'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = -1
                  ValWarn = False
                end
              end
              object Panel10: TPanel
                Left = 0
                Top = 494
                Width = 996
                Height = 45
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object Bt02AtzSdoVrt: TBitBtn
                  Tag = 128
                  Left = 12
                  Top = 3
                  Width = 120
                  Height = 40
                  Caption = '&Atz saldo'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = Bt02AtzSdoVrtClick
                end
                object Bt02Janela: TBitBtn
                  Left = 132
                  Top = 3
                  Width = 120
                  Height = 40
                  Caption = '&Jan.Movim.'
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = Bt02JanelaClick
                end
              end
              object DG02VSMovIts: TdmkDBGridZTO
                Left = 0
                Top = 109
                Width = 996
                Height = 385
                Align = alClient
                DataSource = DsVSMovIts
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 2
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = DG02VSMovItsDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_TTW'
                    Title.Caption = 'Arquivo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_MovimID'
                    Title.Caption = 'Movimento'
                    Width = 58
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / Hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Descri'#231#227'o Pallet'
                    Width = 81
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MediaArM2'
                    Title.Caption = 'M'#233'dia m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MediaPeso'
                    Title.Caption = 'M'#233'dia Kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtPeca'
                    Title.Caption = 'Sdo virtual p'#231
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtArM2'
                    Title.Caption = 'Sdo virtual m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'IME-C'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MovimCod'
                    Title.Caption = 'Movim.C'#243'd.'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcMovID'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel1'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel2'
                    Width = 60
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'IME-I'
            ImageIndex = 3
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBG03Estq: TdmkDBGridZTO
                Left = 0
                Top = 233
                Width = 996
                Height = 306
                Align = alClient
                DataSource = DsEstqR3
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Pallet'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'OrdGGX'
                    Title.Caption = 'Ordem'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end>
              end
              object Panel44: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 233
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object DBG03GraGruY: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 316
                  Height = 233
                  Align = alLeft
                  DataSource = Ds03GraGruY
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Width = 39
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Visible = True
                    end>
                end
                object DBG03GraGruX: TdmkDBGridZTO
                  Left = 316
                  Top = 0
                  Width = 680
                  Height = 233
                  Align = alClient
                  DataSource = Ds03GraGruX
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 43
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR'
                      Title.Caption = 'Nome'
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Pallet'
            ImageIndex = 4
            object PC04_Pallets: TPageControl
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              ActivePage = TabSheet15
              Align = alClient
              TabOrder = 0
              object TabSheet15: TTabSheet
                Caption = 'Em Estoque'
                object Panel15: TPanel
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 197
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel23: TPanel
                    Left = 0
                    Top = 0
                    Width = 741
                    Height = 197
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object GroupBox10: TGroupBox
                      Left = 0
                      Top = 101
                      Width = 741
                      Height = 96
                      Align = alClient
                      Caption = ' Impress'#227'o: '
                      TabOrder = 0
                      object Panel22: TPanel
                        Left = 2
                        Top = 15
                        Width = 737
                        Height = 79
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object RG04Ordem1: TRadioGroup
                          Left = 0
                          Top = 0
                          Width = 216
                          Height = 79
                          Align = alLeft
                          Caption = ' Ordem 1: '
                          Columns = 2
                          ItemIndex = 0
                          Items.Strings = (
                            'Pallet'
                            'Artigo'
                            'Fornecedor'
                            'M'#233'dia m'#178'/p'#231
                            'Status do pallet'
                            'Tipo de material'
                            'Parte do material')
                          TabOrder = 0
                        end
                        object RG04Ordem2: TRadioGroup
                          Left = 216
                          Top = 0
                          Width = 216
                          Height = 79
                          Align = alLeft
                          Caption = ' Ordem 2: '
                          Columns = 2
                          ItemIndex = 1
                          Items.Strings = (
                            'Pallet'
                            'Artigo'
                            'Fornecedor'
                            'M'#233'dia m'#178'/p'#231
                            'Status do pallet'
                            'Tipo de material'
                            'Parte do material')
                          TabOrder = 1
                        end
                        object RG04Agrupa: TRadioGroup
                          Left = 648
                          Top = 0
                          Width = 89
                          Height = 79
                          Align = alClient
                          Caption = ' Agrupamentos: '
                          ItemIndex = 0
                          Items.Strings = (
                            '0'
                            '1'
                            '2')
                          TabOrder = 2
                        end
                        object RG04Ordem3: TRadioGroup
                          Left = 432
                          Top = 0
                          Width = 216
                          Height = 79
                          Align = alLeft
                          Caption = ' Ordem 3: '
                          Columns = 2
                          ItemIndex = 3
                          Items.Strings = (
                            'Pallet'
                            'Artigo'
                            'Fornecedor'
                            'M'#233'dia m'#178'/p'#231
                            'Status do pallet'
                            'Tipo de material'
                            'Parte do material')
                          TabOrder = 3
                        end
                      end
                    end
                    object GroupBox9: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 741
                      Height = 101
                      Align = alTop
                      Caption = ' Pesquisa: '
                      TabOrder = 1
                      object Panel21: TPanel
                        Left = 2
                        Top = 15
                        Width = 737
                        Height = 84
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Label15: TLabel
                          Left = 8
                          Top = 0
                          Width = 66
                          Height = 13
                          Caption = 'Mat'#233'ria-prima:'
                          Enabled = False
                        end
                        object Label38: TLabel
                          Left = 8
                          Top = 40
                          Width = 78
                          Height = 13
                          Caption = 'Tipo de material:'
                        end
                        object Label39: TLabel
                          Left = 276
                          Top = 40
                          Width = 82
                          Height = 13
                          Caption = 'Parte do material:'
                        end
                        object Label4: TLabel
                          Left = 352
                          Top = 0
                          Width = 90
                          Height = 13
                          Caption = 'Centro de estoque:'
                        end
                        object Ed04GraGruX: TdmkEditCB
                          Left = 8
                          Top = 16
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          Enabled = False
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          QryCampo = 'Cargo'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          OnChange = Ed04GraGruXChange
                          DBLookupComboBox = CB04GraGruX
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB04GraGruX: TdmkDBLookupComboBox
                          Left = 64
                          Top = 16
                          Width = 285
                          Height = 21
                          Enabled = False
                          KeyField = 'Controle'
                          ListField = 'NO_PRD_TAM_COR'
                          ListSource = Ds04GraGruX
                          TabOrder = 1
                          dmkEditCB = Ed04GraGruX
                          QryCampo = 'Cargo'
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object Ed04CouNiv2: TdmkEditCB
                          Left = 8
                          Top = 56
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 2
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          QryName = 'QrGrGruXCou'
                          QryCampo = 'CouNiv2'
                          UpdCampo = 'CouNiv2'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB04CouNiv2
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB04CouNiv2: TdmkDBLookupComboBox
                          Left = 64
                          Top = 56
                          Width = 208
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'Nome'
                          ListSource = Ds04CouNiv2
                          TabOrder = 3
                          dmkEditCB = Ed04CouNiv2
                          QryName = 'QrGrGruXCou'
                          QryCampo = 'CouNiv2'
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object Ed04CouNiv1: TdmkEditCB
                          Left = 276
                          Top = 56
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 4
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          QryName = 'QrGrGruXCou'
                          QryCampo = 'CouNiv1'
                          UpdCampo = 'CouNiv1'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB04CouNiv1
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB04CouNiv1: TdmkDBLookupComboBox
                          Left = 332
                          Top = 56
                          Width = 208
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'Nome'
                          ListSource = Ds04CouNiv1
                          TabOrder = 5
                          dmkEditCB = Ed04CouNiv1
                          QryName = 'QrGrGruXCou'
                          QryCampo = 'CouNiv1'
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object CkStatusReal: TCheckBox
                          Left = 548
                          Top = 40
                          Width = 97
                          Height = 17
                          Caption = 'Status Real'
                          Checked = True
                          Enabled = False
                          State = cbChecked
                          TabOrder = 6
                        end
                        object CkFichaPallet: TCheckBox
                          Left = 548
                          Top = 59
                          Width = 185
                          Height = 17
                          Caption = 'Imp. nome da empresa na ficha'
                          Checked = True
                          State = cbChecked
                          TabOrder = 7
                          OnClick = CkFichaPalletClick
                        end
                        object Ed04StqCenCad: TdmkEditCB
                          Left = 352
                          Top = 16
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 8
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB04StqCenCad
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB04StqCenCad: TdmkDBLookupComboBox
                          Left = 408
                          Top = 16
                          Width = 321
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'Nome'
                          ListSource = Ds04StqCenCad
                          TabOrder = 9
                          dmkEditCB = Ed04StqCenCad
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                      end
                    end
                  end
                  object DBG04GraGruY: TdmkDBGridZTO
                    Left = 741
                    Top = 0
                    Width = 247
                    Height = 197
                    Align = alClient
                    DataSource = Ds04GraGruY
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 1
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Codigo'
                        Width = 39
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Visible = True
                      end>
                  end
                end
                object GroupBox8: TGroupBox
                  Left = 749
                  Top = 197
                  Width = 239
                  Height = 314
                  Align = alRight
                  Caption = ' Limbo: '
                  TabOrder = 1
                  object dmkDBGridZTO1: TdmkDBGridZTO
                    Left = 2
                    Top = 15
                    Width = 235
                    Height = 297
                    Align = alClient
                    DataSource = DsNimboRcl
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'VSPallet'
                        Title.Caption = 'Pallet'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Pecas'
                        Title.Caption = 'Pe'#231'as'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'AreaM2'
                        Title.Caption = #193'rea m'#178
                        Visible = True
                      end>
                  end
                end
                object DBG04Estq: TdmkDBGridZTO
                  Left = 0
                  Top = 197
                  Width = 496
                  Height = 314
                  Align = alClient
                  DataSource = DsEstqR4
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  PopupMenu = PM04Estq
                  TabOrder = 2
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  OnDrawColumnCell = DBG04EstqDrawColumnCell
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'FaixaMediaM2'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      Title.Caption = 'Faixa m'#233'dia'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pallet'
                      Title.Caption = 'ID Pallet'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PALLET'
                      Title.Caption = 'Pallet'
                      Width = 76
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR'
                      Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                      Width = 220
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OrdGGX'
                      Title.Caption = 'Ordem'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PalVrtPeca'
                      Title.Caption = 'Pe'#231'as pallet'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PalVrtArM2'
                      Title.Caption = #193'rea m'#178' Pallet'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Media'
                      Title.Caption = 'M'#233'dia Pallet'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PalStat'
                      Title.Caption = 'ID Status'
                      Width = 47
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PalStat'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      Title.Caption = 'Status Pallet'
                      Width = 112
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SdoVrtPeca'
                      Title.Caption = 'Sdo vrt Pe'#231'as'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Title.Caption = 'Pe'#231'as'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SdoVrtArM2'
                      Title.Caption = 'Sdo vrt '#193'rea m'#178
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SdoVrtArP2'
                      Title.Caption = 'Sdo vrt '#193'rea ft'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SdoVrtPeso'
                      Title.Caption = 'Peso kg'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'LmbVrtPeca'
                      Title.Caption = 'Pe'#231'as classificando'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'LmbVrtArM2'
                      Title.Caption = 'm'#178' classificando'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Title.Caption = 'Valor total'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Terceiro'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_FORNECE'
                      Title.Caption = 'Nome do terceiro'
                      Width = 220
                      Visible = True
                    end>
                end
                object Panel34: TPanel
                  Left = 496
                  Top = 197
                  Width = 253
                  Height = 314
                  Align = alRight
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 3
                  object Panel35: TPanel
                    Left = 0
                    Top = 273
                    Width = 253
                    Height = 41
                    Align = alBottom
                    Caption = 'Panel35'
                    TabOrder = 0
                    object Bt04GraGruXNenhum: TBitBtn
                      Tag = 128
                      Left = 124
                      Top = 1
                      Width = 120
                      Height = 40
                      Caption = '&Nenhum'
                      Enabled = False
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = Bt04GraGruXNenhumClick
                    end
                    object Bt04GraGruXTodos: TBitBtn
                      Tag = 127
                      Left = 4
                      Top = 1
                      Width = 120
                      Height = 40
                      Caption = '&Todos'
                      Enabled = False
                      NumGlyphs = 2
                      TabOrder = 1
                      OnClick = Bt04GraGruXTodosClick
                    end
                  end
                  object DBG04Artigos: TdmkDBGridZTO
                    Left = 0
                    Top = 30
                    Width = 253
                    Height = 243
                    Align = alClient
                    DataSource = Ds04GraGruX
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 1
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Width = 43
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_PRD_TAM_COR'
                        Title.Caption = 'Nome'
                        Visible = True
                      end>
                  end
                  object Panel56: TPanel
                    Left = 0
                    Top = 0
                    Width = 253
                    Height = 30
                    Align = alTop
                    Caption = 'Panel52'
                    ParentBackground = False
                    TabOrder = 2
                    object Ed04NomeGG1: TEdit
                      Left = 8
                      Top = 4
                      Width = 257
                      Height = 21
                      TabOrder = 0
                      OnChange = Ed04NomeGG1Change
                    end
                  end
                end
              end
              object TabSheet16: TTabSheet
                Caption = 'Blend VS'
                ImageIndex = 1
                object Panel29: TPanel
                  Left = 0
                  Top = 0
                  Width = 992
                  Height = 25
                  Align = alTop
                  ParentBackground = False
                  TabOrder = 0
                  object Label42: TLabel
                    Left = 4
                    Top = 4
                    Width = 446
                    Height = 16
                    Caption = 'Informe na aba "Em estoque" os pallets que ser'#227'o pesquisados!'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clRed
                    Font.Height = -15
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                end
                object DBG04_01BlendVS: TdmkDBGridZTO
                  Left = 0
                  Top = 25
                  Width = 992
                  Height = 493
                  Align = alClient
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Martelo '
            ImageIndex = 5
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Tamanho'
            ImageIndex = 6
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 309
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label20: TLabel
                Left = 8
                Top = 0
                Width = 65
                Height = 13
                Caption = 'Couro origem:'
              end
              object Label21: TLabel
                Left = 8
                Top = 80
                Width = 65
                Height = 13
                Caption = 'Pallet Origem:'
              end
              object Label22: TLabel
                Left = 8
                Top = 240
                Width = 38
                Height = 13
                Caption = 'Martelo:'
              end
              object Label23: TLabel
                Left = 476
                Top = 200
                Width = 62
                Height = 13
                Caption = 'IME-I origem:'
              end
              object Label24: TLabel
                Left = 8
                Top = 160
                Width = 57
                Height = 13
                Caption = 'Fornecedor:'
              end
              object Label25: TLabel
                Left = 560
                Top = 200
                Width = 65
                Height = 13
                Caption = 'IME-I destino:'
              end
              object Label26: TLabel
                Left = 7
                Top = 200
                Width = 83
                Height = 13
                Caption = 'S'#233'rie Ficha RMP:'
              end
              object Label27: TLabel
                Left = 224
                Top = 200
                Width = 56
                Height = 13
                Caption = 'Ficha RMP:'
              end
              object Label28: TLabel
                Left = 308
                Top = 200
                Width = 33
                Height = 13
                Caption = 'Marca:'
              end
              object Label29: TLabel
                Left = 392
                Top = 200
                Width = 18
                Height = 13
                Caption = 'OC:'
              end
              object Label30: TLabel
                Left = 644
                Top = 200
                Width = 51
                Height = 13
                Caption = 'm'#178' m'#237'nimo:'
              end
              object Label31: TLabel
                Left = 708
                Top = 200
                Width = 52
                Height = 13
                Caption = 'm'#178' m'#225'ximo:'
              end
              object Label32: TLabel
                Left = 8
                Top = 120
                Width = 68
                Height = 13
                Caption = 'Pallet Destino:'
              end
              object Label33: TLabel
                Left = 236
                Top = 240
                Width = 78
                Height = 13
                Caption = 'Tipo de material:'
              end
              object Label34: TLabel
                Left = 504
                Top = 240
                Width = 82
                Height = 13
                Caption = 'Parte do material:'
              end
              object Label41: TLabel
                Left = 8
                Top = 40
                Width = 68
                Height = 13
                Caption = 'Couro destino:'
              end
              object Ed06GraGruXSrc: TdmkEditCB
                Left = 8
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06GraGruXSrc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06GraGruXSrc: TdmkDBLookupComboBox
                Left = 64
                Top = 16
                Width = 469
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = Ds06GraGruX
                TabOrder = 1
                dmkEditCB = Ed06GraGruXSrc
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06VSPalletSrc: TdmkEditCB
                Left = 8
                Top = 96
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Pallet'
                UpdCampo = 'Pallet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06VSPalletSrc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06VSPalletSrc: TdmkDBLookupComboBox
                Left = 64
                Top = 96
                Width = 469
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds06VSPalletSrc
                TabOrder = 3
                dmkEditCB = Ed06VSPalletSrc
                QryCampo = 'Pallet'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06Terceiro: TdmkEditCB
                Left = 8
                Top = 176
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Fornecedor'
                UpdCampo = 'Fornecedor'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06Terceiro
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06Terceiro: TdmkDBLookupComboBox
                Left = 64
                Top = 176
                Width = 469
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = Ds06Fornecedor
                TabOrder = 7
                dmkEditCB = Ed06Terceiro
                QryCampo = 'Fornecedor'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object GroupBox5: TGroupBox
                Left = 540
                Top = 4
                Width = 245
                Height = 73
                Caption = ' Per'#237'odo: '
                TabOrder = 8
                object TP06DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.000000000000000000
                  Time = 0.777157974502188200
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck06DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  TabOrder = 0
                end
                object Ck06DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP06DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.000000000000000000
                  Time = 0.777203761601413100
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object Ed06VMI_Sorc: TdmkEdit
                Left = 476
                Top = 216
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 15
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed06Martelo: TdmkEditCB
                Left = 8
                Top = 256
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 19
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Fornecedor'
                UpdCampo = 'Fornecedor'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06Martelo
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06Martelo: TdmkDBLookupComboBox
                Left = 64
                Top = 256
                Width = 168
                Height = 21
                KeyField = 'Nome'
                ListField = 'Nome'
                ListSource = Ds06VSMrtCad
                TabOrder = 20
                dmkEditCB = Ed06Martelo
                QryCampo = 'Fornecedor'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06VMI_Dest: TdmkEdit
                Left = 560
                Top = 216
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 16
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed06SerieFch: TdmkEditCB
                Left = 7
                Top = 216
                Width = 40
                Height = 21
                Alignment = taRightJustify
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'SerieFch'
                UpdCampo = 'SerieFch'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06SerieFch
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06SerieFch: TdmkDBLookupComboBox
                Left = 47
                Top = 216
                Width = 174
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                TabOrder = 11
                dmkEditCB = Ed06SerieFch
                QryCampo = 'SerieFch'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06Ficha: TdmkEdit
                Left = 224
                Top = 216
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Pecas'
                UpdCampo = 'Pecas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed06Marca: TdmkEdit
                Left = 308
                Top = 216
                Width = 80
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Marca'
                UpdCampo = 'Marca'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Ed06VSCacCod: TdmkEdit
                Left = 392
                Top = 216
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed06Pequeno: TdmkEdit
                Left = 644
                Top = 216
                Width = 60
                Height = 21
                Alignment = taRightJustify
                TabOrder = 17
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '3,20'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 3.200000000000000000
                ValWarn = False
              end
              object Ed06Grande: TdmkEdit
                Left = 708
                Top = 216
                Width = 60
                Height = 21
                Alignment = taRightJustify
                TabOrder = 18
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '3,80'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 3.800000000000000000
                ValWarn = False
              end
              object Ed06VSPalletDst: TdmkEditCB
                Left = 8
                Top = 136
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Pallet'
                UpdCampo = 'Pallet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06VSPalletDst
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06VSPalletDst: TdmkDBLookupComboBox
                Left = 64
                Top = 136
                Width = 469
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds06VSPalletSrc
                TabOrder = 5
                dmkEditCB = Ed06VSPalletDst
                QryCampo = 'Pallet'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06CouNiv2: TdmkEditCB
                Left = 236
                Top = 256
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 21
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv2'
                UpdCampo = 'CouNiv2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06CouNiv2
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06CouNiv2: TdmkDBLookupComboBox
                Left = 292
                Top = 256
                Width = 208
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds06CouNiv2
                TabOrder = 22
                dmkEditCB = Ed06CouNiv2
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv2'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed06CouNiv1: TdmkEditCB
                Left = 504
                Top = 256
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 23
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv1'
                UpdCampo = 'CouNiv1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06CouNiv1
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06CouNiv1: TdmkDBLookupComboBox
                Left = 560
                Top = 256
                Width = 208
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds06CouNiv1
                TabOrder = 24
                dmkEditCB = Ed06CouNiv1
                QryName = 'QrGrGruXCou'
                QryCampo = 'CouNiv1'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object RGCacID: TRadioGroup
                Left = 544
                Top = 88
                Width = 241
                Height = 69
                Caption = ' Forma de classifica'#231#227'o: '
                ItemIndex = 2
                Items.Strings = (
                  'Classifica'#231#227'o'
                  'Reclassifica'#231#227'o'
                  'Qualquer')
                TabOrder = 9
              end
              object Ed06GraGruXDst: TdmkEditCB
                Left = 8
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 25
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB06GraGruXDst
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB06GraGruXDst: TdmkDBLookupComboBox
                Left = 64
                Top = 56
                Width = 469
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = Ds06GraGruXDst
                TabOrder = 26
                dmkEditCB = Ed06GraGruXDst
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ck06Pallets: TCheckBox
                Left = 8
                Top = 284
                Width = 273
                Height = 17
                Caption = 'Pallets selecionados na guia "Pallet".'
                TabOrder = 27
              end
            end
          end
          object TabSheet8: TTabSheet
            Caption = 'Todos Pallets'
            ImageIndex = 7
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox6: TGroupBox
                Left = 0
                Top = 0
                Width = 996
                Height = 61
                Align = alTop
                Caption = ' Intervalo de Pallets: '
                TabOrder = 0
                object Panel18: TPanel
                  Left = 2
                  Top = 15
                  Width = 992
                  Height = 44
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label35: TLabel
                    Left = 12
                    Top = 4
                    Width = 30
                    Height = 13
                    Caption = 'Inicial:'
                  end
                  object Label36: TLabel
                    Left = 96
                    Top = 4
                    Width = 25
                    Height = 13
                    Caption = 'Final:'
                  end
                  object Ed07PallIni: TdmkEdit
                    Left = 12
                    Top = 20
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnExit = Ed07PallIniExit
                  end
                  object Ed07PallFim: TdmkEdit
                    Left = 96
                    Top = 20
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
              end
              object DBG07Estq: TdmkDBGridZTO
                Left = 0
                Top = 61
                Width = 996
                Height = 478
                Align = alClient
                DataSource = Ds07PrevPal
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 1
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'VSPallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Pallet'
                    Width = 76
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CliStat'
                    Title.Caption = 'Cliente'
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet9: TTabSheet
            Caption = 'Diferen'#231'as'
            ImageIndex = 8
            object Panel19: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel20: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 539
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel38: TPanel
                  Left = 0
                  Top = 0
                  Width = 645
                  Height = 539
                  Align = alLeft
                  Caption = 'Panel38'
                  TabOrder = 0
                  object PC08: TPageControl
                    Left = 1
                    Top = 1
                    Width = 643
                    Height = 537
                    ActivePage = TabSheet21
                    Align = alClient
                    TabOrder = 0
                    object TabSheet20: TTabSheet
                      Caption = 'Compra'
                      object Panel39: TPanel
                        Left = 0
                        Top = 0
                        Width = 635
                        Height = 77
                        Align = alTop
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        ExplicitWidth = 547
                        object Label40: TLabel
                          Left = 4
                          Top = 56
                          Width = 57
                          Height = 13
                          Caption = 'Fornecedor:'
                        end
                        object RG08TipoMovim: TRadioGroup
                          Left = 4
                          Top = 0
                          Width = 149
                          Height = 41
                          Caption = ' Tipo de Movimento: '
                          ItemIndex = 0
                          Items.Strings = (
                            'Entrada In Natura')
                          TabOrder = 0
                        end
                        object RG08Ordem: TRadioGroup
                          Left = 160
                          Top = 0
                          Width = 313
                          Height = 41
                          Caption = ' Ordem: '
                          Columns = 2
                          ItemIndex = 0
                          Items.Strings = (
                            'Data, Fornecedor'
                            'Fornecedor, Data')
                          TabOrder = 1
                        end
                        object RG08Agrupa: TRadioGroup
                          Left = 476
                          Top = 0
                          Width = 61
                          Height = 73
                          Caption = ' Agrupa: '
                          ItemIndex = 1
                          Items.Strings = (
                            'N'#227'o'
                            'Sim')
                          TabOrder = 2
                        end
                        object Ed08Terceiro: TdmkEditCB
                          Left = 64
                          Top = 52
                          Width = 56
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 3
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          QryCampo = 'Fornecedor'
                          UpdCampo = 'Fornecedor'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CB08Terceiro
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CB08Terceiro: TdmkDBLookupComboBox
                          Left = 120
                          Top = 52
                          Width = 281
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'NOMEENTIDADE'
                          ListSource = Ds08Fornecedor
                          TabOrder = 4
                          dmkEditCB = Ed08Terceiro
                          QryCampo = 'Fornecedor'
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object Ck08Simples: TdmkCheckBox
                          Left = 408
                          Top = 52
                          Width = 61
                          Height = 17
                          Caption = 'Simples.'
                          Checked = True
                          State = cbChecked
                          TabOrder = 5
                          UpdType = utYes
                          ValCheck = #0
                          ValUncheck = #0
                          OldValor = #0
                        end
                      end
                    end
                    object TabSheet21: TTabSheet
                      Caption = 'Movimento'
                      ImageIndex = 1
                      object Panel13: TPanel
                        Left = 0
                        Top = 0
                        Width = 635
                        Height = 321
                        Align = alTop
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object PC_Dif_Movim: TPageControl
                          Left = 0
                          Top = 0
                          Width = 635
                          Height = 321
                          ActivePage = TabSheet34
                          Align = alClient
                          TabOrder = 0
                          object TabSheet33: TTabSheet
                            Caption = '  Corre'#231#227'o de estoque '
                            object Panel40: TPanel
                              Left = 0
                              Top = 0
                              Width = 627
                              Height = 293
                              Align = alClient
                              BevelOuter = bvNone
                              ParentBackground = False
                              TabOrder = 0
                              ExplicitHeight = 237
                              object CG08_01_MovimIDSign: TdmkCheckGroup
                                Left = 0
                                Top = 0
                                Width = 185
                                Height = 293
                                Align = alLeft
                                Caption = ' Tipos corretivos: '
                                Items.Strings = (
                                  'Entrada Indefinida'
                                  'Sa'#237'da Indefinida'
                                  'Entrada For'#231'ada '
                                  'Sa'#237'da For'#231'ada'
                                  'Entrada Residual'
                                  'Sa'#237'da Residual'
                                  'Entrada Invent'#225'rio'
                                  'Sa'#237'da Invent'#225'rio'
                                  'Entrada Extra '
                                  'Sa'#237'da Extra')
                                TabOrder = 0
                                UpdType = utYes
                                Value = 0
                                OldValor = 0
                                ExplicitHeight = 237
                              end
                              object Bt08_01_Todos: TBitBtn
                                Tag = 127
                                Left = 196
                                Top = 8
                                Width = 120
                                Height = 40
                                Caption = '&Todos'
                                NumGlyphs = 2
                                TabOrder = 1
                                OnClick = Bt08_01_TodosClick
                              end
                              object Bt08_01_Nenhum: TBitBtn
                                Tag = 128
                                Left = 196
                                Top = 48
                                Width = 120
                                Height = 40
                                Caption = '&Nenhum'
                                NumGlyphs = 2
                                TabOrder = 2
                                OnClick = Bt08_01_NenhumClick
                              end
                            end
                          end
                          object TabSheet34: TTabSheet
                            Caption = '  Excesso de estoque (baixou mais que tinha)'
                            ImageIndex = 1
                            object Panel72: TPanel
                              Left = 0
                              Top = 0
                              Width = 627
                              Height = 293
                              Align = alClient
                              BevelOuter = bvNone
                              ParentBackground = False
                              TabOrder = 0
                              object Panel73: TPanel
                                Left = 0
                                Top = 0
                                Width = 627
                                Height = 61
                                Align = alTop
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object BtNenhumResidual: TBitBtn
                                  Tag = 128
                                  Left = 132
                                  Top = 12
                                  Width = 120
                                  Height = 40
                                  Caption = '&Nenhum'
                                  NumGlyphs = 2
                                  TabOrder = 0
                                  OnClick = BtNenhumResidualClick
                                end
                                object BrTodosResidual: TBitBtn
                                  Tag = 127
                                  Left = 8
                                  Top = 12
                                  Width = 120
                                  Height = 40
                                  Caption = '&Todos'
                                  NumGlyphs = 2
                                  TabOrder = 1
                                  OnClick = BrTodosResidualClick
                                end
                              end
                              object CG08_01_MovimIDResi: TdmkCheckGroup
                                Left = 0
                                Top = 61
                                Width = 627
                                Height = 228
                                Align = alTop
                                Caption = ' Tipos de movimentos normais: '
                                Columns = 3
                                Items.Strings = (
                                  '01 - Compra In Natura'
                                  '06 - Gera'#231#227'o de Artigo'
                                  '07 - Clalssifica'#231#227'o couro a couro'
                                  '08 - Reclassifica'#231#227'o unit'#225'ria'
                                  '10 - Sem Origem'
                                  '11 - Em Opera'#231#227'o'
                                  '13 - Ajuste em Invent'#225'rio'
                                  '14 - Classe M'#250'ltipla'
                                  '15 - Pr'#233' Reclasse (Aglomera'#231#227'o)'
                                  '16 - Compra de Classificado'
                                  '19 - Semi Acabado em Processo'
                                  '20 - Couro Acabado'
                                  '21 - Devolu'#231#227'o'
                                  '22 - Retrabalho'
                                  '23 - Gera'#231#227'o de Sub Produto'
                                  '24 - Reclassifica'#231#227'o M'#250'ltipla'
                                  '25 - Transfer'#234'ncia de Local'
                                  '26 - Em processo de caleiro'
                                  '27 - Em processo de curtimento'
                                  '28 - Desclassifica'#231#227'o'
                                  '29 - Couro caleado'
                                  '30 - Couro pr'#233'-descarnado e aparado'
                                  '31 - Couro tripa (div./lam./integr.)'
                                  '32 - Processamento de subproduto'
                                  '33 - Reprocesso / reparo de material'
                                  '34 - Couro curtido'
                                  '36 - Entrada sem Cobertura'
                                  '37 - Sa'#237'da sem Cobertura'
                                  '38 - Industrializa'#231#227'o')
                                TabOrder = 1
                                UpdType = utYes
                                Value = 0
                                OldValor = 0
                              end
                            end
                          end
                        end
                      end
                      object Panel74: TPanel
                        Left = 0
                        Top = 321
                        Width = 635
                        Height = 188
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object RG08_01_Ordem1: TRadioGroup
                          Left = 16
                          Top = 8
                          Width = 161
                          Height = 105
                          Caption = ' Ordem: '
                          ItemIndex = 0
                          Items.Strings = (
                            'Sentido (sa'#237'da / entrada)'
                            'Grupo de estoque'
                            'Processo /opera'#231#227'o'
                            'Artigo'
                            'Data')
                          TabOrder = 0
                        end
                        object RG08_01_Ordem2: TRadioGroup
                          Left = 176
                          Top = 4
                          Width = 161
                          Height = 105
                          Caption = ' Ordem: '
                          ItemIndex = 1
                          Items.Strings = (
                            'Sentido (sa'#237'da / entrada)'
                            'Grupo de estoque'
                            'Processo /opera'#231#227'o'
                            'Artigo'
                            'Data')
                          TabOrder = 1
                        end
                        object RG08_01_Agrupa: TRadioGroup
                          Left = 344
                          Top = 4
                          Width = 61
                          Height = 73
                          Caption = ' Agrupa: '
                          ItemIndex = 1
                          Items.Strings = (
                            '0'
                            '1'
                            '2')
                          TabOrder = 2
                        end
                      end
                    end
                  end
                end
                object Panel41: TPanel
                  Left = 645
                  Top = 0
                  Width = 351
                  Height = 539
                  Align = alClient
                  TabOrder = 1
                  object GroupBox7: TGroupBox
                    Left = 6
                    Top = 24
                    Width = 245
                    Height = 73
                    Caption = ' Per'#237'odo: '
                    TabOrder = 0
                    object TP08DataIni: TdmkEditDateTimePicker
                      Left = 8
                      Top = 40
                      Width = 112
                      Height = 21
                      CalColors.TextColor = clMenuText
                      Date = 37636.000000000000000000
                      Time = 0.777157974502188200
                      TabOrder = 1
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object Ck08DataIni: TCheckBox
                      Left = 8
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'Data inicial'
                      Checked = True
                      State = cbChecked
                      TabOrder = 0
                    end
                    object Ck08DataFim: TCheckBox
                      Left = 124
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'Data final'
                      TabOrder = 2
                    end
                    object TP08DataFim: TdmkEditDateTimePicker
                      Left = 124
                      Top = 40
                      Width = 112
                      Height = 21
                      Date = 37636.000000000000000000
                      Time = 0.777203761601413100
                      TabOrder = 3
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                  end
                  object Ck08VSMovItb: TCheckBox
                    Left = 8
                    Top = 100
                    Width = 181
                    Height = 17
                    Caption = 'Incluir arquivo morto na pesquisa.'
                    TabOrder = 1
                  end
                end
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = 'Erros'
            ImageIndex = 9
            object PageControl1: TPageControl
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              ActivePage = TabSheet11
              Align = alClient
              TabOrder = 0
              object TabSheet11: TTabSheet
                Caption = 'CacCia Dest indef'
                object dmkDBGridZTO3: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 988
                  Height = 511
                  Align = alClient
                  DataSource = DsCiaDst
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
          end
          object TabSheet12: TTabSheet
            Caption = 'Fichas RMP'
            ImageIndex = 10
            object Panel27: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DG10FichasRMP: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 996
                Height = 539
                Align = alClient
                DataSource = Ds10FichasRMP
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                PopupMenu = PM10FichasRMP
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_SERIEFCH'
                    Title.Caption = 'S'#233'rie'
                    Width = 146
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ficha'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ITENS'
                    Title.Caption = 'Itens'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'RefFirst'
                    Title.Caption = 'Primeira refer'#234'ncia'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'RefLast'
                    Title.Caption = #218'ltima refer'#234'ncia'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Fornecedor'
                    Width = 420
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = 'Estoque por grupo'
            ImageIndex = 11
            object Me13: TMemo
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Lucida Console'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet14: TTabSheet
            Caption = 'Movimento'
            ImageIndex = 12
            object Panel28: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet17: TTabSheet
            Caption = 'Fluxo'
            ImageIndex = 13
            object Panel30: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGrid1: TDBGrid
                Left = 0
                Top = 177
                Width = 996
                Height = 362
                Align = alClient
                DataSource = Ds13VSSeqIts
                PopupMenu = PM13Grade
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DG02VSMovItsDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_MovimID'
                    Title.Caption = 'Movimento'
                    Width = 58
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / Hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Title.Caption = 'ID Pallet'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_Pallet'
                    Title.Caption = 'Descri'#231#227'o Pallet'
                    Width = 81
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtPeca'
                    Title.Caption = 'Sdo virtual p'#231
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SdoVrtArM2'
                    Title.Caption = 'Sdo virtual m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Nome do terceiro'
                    Width = 220
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'IME-C'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MovimCod'
                    Title.Caption = 'Movim.C'#243'd.'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcMovID'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel1'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SrcNivel2'
                    Width = 60
                    Visible = True
                  end>
              end
              object Panel32: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 177
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object Panel31: TPanel
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 177
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object RG13_GragruY: TRadioGroup
                    Left = 0
                    Top = 0
                    Width = 193
                    Height = 177
                    Align = alLeft
                    Caption = ' Tipo de Artigo:'
                    ItemIndex = 0
                    Items.Strings = (
                      '0000 - N'#227'o definido'
                      '1024 - Mat'#233'ria prima In Natura'
                      '2048 - Artigo de ribeira'
                      '3072 - Artigo classificado'
                      '4096 - Artigo em opera'#231#227'o'
                      '5120 - Artigo Semi Acabado'
                      '6144 - Artigo Acabado'
                      '0512 - Sub Produto')
                    TabOrder = 0
                    OnClick = RG13_GragruYClick
                  end
                  object Panel33: TPanel
                    Left = 193
                    Top = 0
                    Width = 803
                    Height = 177
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label43: TLabel
                      Left = 8
                      Top = 0
                      Width = 66
                      Height = 13
                      Caption = 'Mat'#233'ria-prima:'
                    end
                    object La13_MaxInteiros: TLabel
                      Left = 552
                      Top = 88
                      Width = 147
                      Height = 13
                      Caption = 'Quantidade m'#225'xima de inteiros:'
                      Visible = False
                    end
                    object Label45: TLabel
                      Left = 264
                      Top = 84
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label46: TLabel
                      Left = 348
                      Top = 84
                      Width = 28
                      Height = 13
                      Caption = 'IME-I:'
                    end
                    object Label47: TLabel
                      Left = 263
                      Top = 44
                      Width = 83
                      Height = 13
                      Caption = 'S'#233'rie Ficha RMP:'
                    end
                    object Label49: TLabel
                      Left = 488
                      Top = 44
                      Width = 56
                      Height = 13
                      Caption = 'Ficha RMP:'
                    end
                    object Ed13GraGruX: TdmkEditCB
                      Left = 8
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Cargo'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CB13GraGruX
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CB13GraGruX: TdmkDBLookupComboBox
                      Left = 64
                      Top = 16
                      Width = 477
                      Height = 21
                      KeyField = 'Controle'
                      ListField = 'NO_PRD_TAM_COR'
                      TabOrder = 1
                      dmkEditCB = Ed13GraGruX
                      QryCampo = 'Cargo'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object GroupBox12: TGroupBox
                      Left = 552
                      Top = 8
                      Width = 245
                      Height = 73
                      Caption = ' Per'#237'odo: '
                      TabOrder = 2
                      object TP13DataIni: TdmkEditDateTimePicker
                        Left = 8
                        Top = 40
                        Width = 112
                        Height = 21
                        CalColors.TextColor = clMenuText
                        Date = 44712.000000000000000000
                        Time = 0.777157974502188200
                        Enabled = False
                        TabOrder = 1
                        Visible = False
                        ReadOnly = False
                        DefaultEditMask = '!99/99/99;1;_'
                        AutoApplyEditMask = True
                        UpdType = utYes
                        DatePurpose = dmkdpNone
                      end
                      object Ck13DataIni: TCheckBox
                        Left = 8
                        Top = 20
                        Width = 112
                        Height = 17
                        Caption = 'Data inicial'
                        Checked = True
                        Enabled = False
                        State = cbChecked
                        TabOrder = 0
                        Visible = False
                      end
                      object Ck13DataFim: TCheckBox
                        Left = 124
                        Top = 20
                        Width = 112
                        Height = 17
                        Caption = 'Data final'
                        TabOrder = 2
                      end
                      object TP13DataFim: TdmkEditDateTimePicker
                        Left = 124
                        Top = 40
                        Width = 112
                        Height = 21
                        Date = 37636.000000000000000000
                        Time = 0.777203761601413100
                        TabOrder = 3
                        ReadOnly = False
                        DefaultEditMask = '!99/99/99;1;_'
                        AutoApplyEditMask = True
                        UpdType = utYes
                        DatePurpose = dmkdpNone
                      end
                    end
                    object RG13_AnaliSinte: TRadioGroup
                      Left = 8
                      Top = 40
                      Width = 101
                      Height = 73
                      Caption = ' Tipo de pesquisa: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Indefinido'
                        'Anal'#237'tico'
                        'Sint'#233'tico')
                      TabOrder = 3
                      OnClick = RG13_AnaliSinteClick
                    end
                    object Ed13_MaxInteiros: TdmkEdit
                      Left = 716
                      Top = 84
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      Visible = False
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 1
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '9,0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 9.000000000000000000
                      ValWarn = False
                    end
                    object RG13TipoMov: TRadioGroup
                      Left = 112
                      Top = 40
                      Width = 141
                      Height = 73
                      Caption = 'Tipo de movimento: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Qualquer movimento'
                        'Entradas e sa'#237'das.'
                        'Somente ajustes')
                      TabOrder = 5
                    end
                    object Ed13_Pallet: TdmkEdit
                      Left = 264
                      Top = 100
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 6
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object Ed13_IMEI: TdmkEdit
                      Left = 348
                      Top = 100
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 7
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object Ed13SerieFch: TdmkEditCB
                      Left = 263
                      Top = 60
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 8
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'SerieFch'
                      UpdCampo = 'SerieFch'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CB13SerieFch
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CB13SerieFch: TdmkDBLookupComboBox
                      Left = 303
                      Top = 60
                      Width = 182
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      TabOrder = 9
                      dmkEditCB = Ed13SerieFch
                      QryCampo = 'SerieFch'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object Ed13Ficha: TdmkEdit
                      Left = 488
                      Top = 60
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 10
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 3
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Pecas'
                      UpdCampo = 'Pecas'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
          object TabSheet18: TTabSheet
            Caption = 'Nota MPAG'
            ImageIndex = 14
            object Panel36: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 205
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label44: TLabel
                Left = 8
                Top = 0
                Width = 104
                Height = 13
                Caption = 'Mat'#233'ria prima / Artigo:'
              end
              object Label48: TLabel
                Left = 8
                Top = 40
                Width = 57
                Height = 13
                Caption = 'Fornecedor:'
              end
              object Label50: TLabel
                Left = 11
                Top = 84
                Width = 83
                Height = 13
                Caption = 'S'#233'rie Ficha RMP:'
              end
              object Label51: TLabel
                Left = 228
                Top = 84
                Width = 56
                Height = 13
                Caption = 'Ficha RMP:'
              end
              object Label52: TLabel
                Left = 312
                Top = 84
                Width = 33
                Height = 13
                Caption = 'Marca:'
              end
              object Ed14GraGruX: TdmkEditCB
                Left = 8
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB14GraGruX
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB14GraGruX: TdmkDBLookupComboBox
                Left = 64
                Top = 16
                Width = 469
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                TabOrder = 1
                dmkEditCB = Ed14GraGruX
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed14Terceiro: TdmkEditCB
                Left = 8
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Fornecedor'
                UpdCampo = 'Fornecedor'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB14Terceiro
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB14Terceiro: TdmkDBLookupComboBox
                Left = 64
                Top = 56
                Width = 469
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                TabOrder = 3
                dmkEditCB = Ed14Terceiro
                QryCampo = 'Fornecedor'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object GroupBox13: TGroupBox
                Left = 132
                Top = 124
                Width = 245
                Height = 69
                Caption = ' Per'#237'odo: '
                TabOrder = 4
                object TP14DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.000000000000000000
                  Time = 0.777157974502188200
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck14DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  TabOrder = 0
                end
                object Ck14DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP14DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.000000000000000000
                  Time = 0.777203761601413100
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object Ed14SerieFch: TdmkEditCB
                Left = 11
                Top = 100
                Width = 40
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'SerieFch'
                UpdCampo = 'SerieFch'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB14SerieFch
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB14SerieFch: TdmkDBLookupComboBox
                Left = 51
                Top = 100
                Width = 174
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                TabOrder = 7
                dmkEditCB = Ed14SerieFch
                QryCampo = 'SerieFch'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object Ed14Ficha: TdmkEdit
                Left = 228
                Top = 100
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Pecas'
                UpdCampo = 'Pecas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed14Marca: TdmkEdit
                Left = 312
                Top = 100
                Width = 80
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Marca'
                UpdCampo = 'Marca'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RG14FontePsq: TRadioGroup
                Left = 12
                Top = 124
                Width = 113
                Height = 69
                Caption = ' Forma de pesquisa: '
                ItemIndex = 0
                Items.Strings = (
                  'Mat'#233'ria prima'
                  'Artigo gerado')
                TabOrder = 5
              end
              object GroupBox14: TGroupBox
                Left = 544
                Top = 0
                Width = 452
                Height = 205
                Align = alRight
                Caption = ' Impress'#227'o: '
                TabOrder = 10
                object Panel37: TPanel
                  Left = 2
                  Top = 15
                  Width = 448
                  Height = 188
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object RG14Ordem1: TRadioGroup
                    Left = 0
                    Top = 0
                    Width = 116
                    Height = 188
                    Align = alLeft
                    Caption = ' Ordem 1: '
                    ItemIndex = 0
                    Items.Strings = (
                      'Fornecedor'
                      'Mat'#233'ria prima / artigo'
                      'Per'#237'odo*')
                    TabOrder = 0
                  end
                  object RG14Ordem2: TRadioGroup
                    Left = 116
                    Top = 0
                    Width = 116
                    Height = 188
                    Align = alLeft
                    Caption = ' Ordem 2: '
                    ItemIndex = 1
                    Items.Strings = (
                      'Fornecedor'
                      'Mat'#233'ria prima / artigo'
                      'Per'#237'odo*')
                    TabOrder = 1
                  end
                  object RG14Agrupa: TRadioGroup
                    Left = 348
                    Top = 0
                    Width = 100
                    Height = 188
                    Align = alClient
                    Caption = ' Agrupamentos: '
                    ItemIndex = 2
                    Items.Strings = (
                      '0'
                      '1'
                      '2')
                    TabOrder = 2
                  end
                  object RG14Ordem3: TRadioGroup
                    Left = 232
                    Top = 0
                    Width = 116
                    Height = 188
                    Align = alLeft
                    Caption = ' Ordem 3: '
                    ItemIndex = 2
                    Items.Strings = (
                      'Fornecedor'
                      'Mat'#233'ria prima / artigo'
                      'Per'#237'odo*')
                    TabOrder = 3
                  end
                end
              end
              object RG14Periodo: TRadioGroup
                Left = 380
                Top = 124
                Width = 149
                Height = 69
                Caption = ' Per'#237'odo* a agrupar: '
                Columns = 2
                ItemIndex = 2
                Items.Strings = (
                  'Dia'
                  'Sem'
                  'M'#234's'
                  'Ano')
                TabOrder = 11
              end
              object Ck14SoMPAGNo0: TCheckBox
                Left = 400
                Top = 100
                Width = 97
                Height = 17
                Caption = 'S'#243' Nota OK'
                TabOrder = 12
              end
              object Ck14Cor: TCheckBox
                Left = 480
                Top = 100
                Width = 53
                Height = 17
                Caption = 'Colorir'
                TabOrder = 13
              end
            end
            object DBG14VSMovIts: TdmkDBGridZTO
              Left = 0
              Top = 205
              Width = 996
              Height = 334
              Align = alClient
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Visible = False
              RowColors = <>
            end
          end
          object TabSheet19: TTabSheet
            Caption = 'CG IMEIs (deprecado)'
            ImageIndex = 15
            object Me15ListaIMEIs: TMemo
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet22: TTabSheet
            Caption = ' int>div'
            ImageIndex = 16
            object Panel42: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 89
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              object GroupBox15: TGroupBox
                Left = 16
                Top = 4
                Width = 245
                Height = 73
                Caption = ' Per'#237'odo: '
                TabOrder = 0
                object TP16DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.000000000000000000
                  Time = 0.777157974502188200
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck16DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                end
                object Ck16DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP16DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.000000000000000000
                  Time = 0.777203761601413100
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
            end
            object DBG16Fatores: TdmkDBGridZTO
              Left = 0
              Top = 89
              Width = 996
              Height = 450
              Align = alClient
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PM16Fatores
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
          end
          object TabSheet23: TTabSheet
            Caption = 'Requisi'#231#227'o'
            ImageIndex = 17
            object Panel45: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 381
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              object GroupBox17: TGroupBox
                Left = 8
                Top = 4
                Width = 245
                Height = 73
                Caption = ' Per'#237'odo: '
                TabOrder = 0
                object TP17DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.000000000000000000
                  Time = 0.777157974502188200
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck17DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  TabOrder = 0
                end
                object Ck17DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP17DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.000000000000000000
                  Time = 0.777203761601413100
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object Ck17VsMovItb: TdmkCheckBox
                Left = 12
                Top = 84
                Width = 225
                Height = 17
                Caption = 'Pesquisar no arquivo morto tamb'#233'm.'
                TabOrder = 1
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
          end
          object TabSheet24: TTabSheet
            Caption = 'Ordens'
            ImageIndex = 18
            object GroupBox20: TGroupBox
              Left = 0
              Top = 0
              Width = 996
              Height = 100
              Align = alTop
              Caption = ' Impress'#227'o: '
              TabOrder = 0
              ExplicitWidth = 999
              object Panel48: TPanel
                Left = 2
                Top = 15
                Width = 995
                Height = 83
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object RG18Ordem1: TRadioGroup
                  Left = 101
                  Top = 0
                  Width = 216
                  Height = 84
                  Align = alLeft
                  Caption = ' Ordem 1: '
                  ItemIndex = 1
                  Items.Strings = (
                    'Cliente'
                    'Fornecedor MO'
                    'Artigo'
                    'N'#186' Ordem')
                  TabOrder = 0
                end
                object RG18Ordem2: TRadioGroup
                  Left = 317
                  Top = 0
                  Width = 216
                  Height = 84
                  Align = alLeft
                  Caption = ' Ordem 2: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Cliente'
                    'Fornecedor MO'
                    'Artigo'
                    'N'#186' Ordem')
                  TabOrder = 1
                end
                object RG18Agrupa: TRadioGroup
                  Left = 749
                  Top = 0
                  Width = 97
                  Height = 84
                  Align = alLeft
                  Caption = ' Agrupamentos: '
                  ItemIndex = 2
                  Items.Strings = (
                    '0'
                    '1'
                    '2')
                  TabOrder = 2
                end
                object RG18Ordem3: TRadioGroup
                  Left = 533
                  Top = 0
                  Width = 216
                  Height = 84
                  Align = alLeft
                  Caption = ' Ordem 3: '
                  ItemIndex = 2
                  Items.Strings = (
                    'Cliente'
                    'Fornecedor MO'
                    'Artigo'
                    'N'#186' Ordem')
                  TabOrder = 3
                end
                object CG18Ordens: TdmkCheckGroup
                  Left = 0
                  Top = 0
                  Width = 101
                  Height = 84
                  Align = alLeft
                  Caption = ' Ordem:'
                  ItemIndex = 1
                  Items.Strings = (
                    'Opera'#231#227'o'
                    'Produ'#231#227'o')
                  TabOrder = 4
                  UpdType = utYes
                  Value = 2
                  OldValor = 0
                end
                object Ck18Cor: TCheckBox
                  Left = 852
                  Top = 12
                  Width = 97
                  Height = 17
                  Caption = 'Colorir.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 5
                end
              end
            end
            object GroupBox21: TGroupBox
              Left = 0
              Top = 100
              Width = 996
              Height = 61
              Align = alTop
              Caption = ' Intervalo de Pallets: '
              TabOrder = 1
              ExplicitWidth = 999
              object Panel51: TPanel
                Left = 2
                Top = 15
                Width = 995
                Height = 46
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object SpeedButton1: TSpeedButton
                  Left = 968
                  Top = 20
                  Width = 23
                  Height = 22
                  OnClick = SpeedButton1Click
                end
                object Ed18Especificos: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 953
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = Ed07PallIniExit
                end
                object Ck18Especificos: TCheckBox
                  Left = 8
                  Top = 0
                  Width = 273
                  Height = 17
                  Caption = 'IMEC-S Espec'#237'ficos. Ex.: (1-10,13)'
                  TabOrder = 1
                end
              end
            end
            object Panel54: TPanel
              Left = 0
              Top = 161
              Width = 996
              Height = 52
              Align = alTop
              ParentBackground = False
              TabOrder = 2
              ExplicitWidth = 999
              object Label56: TLabel
                Left = 8
                Top = 8
                Width = 134
                Height = 13
                Caption = 'Fornecedor de m'#227'o-de-obra:'
              end
              object Ed18Fornecedor: TdmkEditCB
                Left = 8
                Top = 24
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB18Fornecedor
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB18Fornecedor: TdmkDBLookupComboBox
                Left = 64
                Top = 24
                Width = 425
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = Ds18Fornecedor
                TabOrder = 1
                dmkEditCB = Ed18Fornecedor
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
          end
          object TabSheet25: TTabSheet
            Caption = 'Di'#225'rio'
            ImageIndex = 19
            object Panel53: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 373
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              object GroupBox22: TGroupBox
                Left = 540
                Top = 4
                Width = 245
                Height = 73
                Caption = ' Per'#237'odo: '
                TabOrder = 0
                object TP19DataIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 40
                  Width = 112
                  Height = 21
                  CalColors.TextColor = clMenuText
                  Date = 37636.000000000000000000
                  Time = 0.777157974502188200
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object Ck19DataIni: TCheckBox
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data inicial'
                  TabOrder = 0
                end
                object Ck19DataFim: TCheckBox
                  Left = 124
                  Top = 20
                  Width = 112
                  Height = 17
                  Caption = 'Data final'
                  TabOrder = 2
                end
                object TP19DataFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 40
                  Width = 112
                  Height = 21
                  Date = 37636.000000000000000000
                  Time = 0.777203761601413100
                  TabOrder = 3
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
            end
          end
          object TabSheet26: TTabSheet
            Caption = 'NFes emitidas VS'
            ImageIndex = 20
            object Panel55: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 129
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object CG20VmcWrn: TdmkCheckGroup
                Left = 257
                Top = 0
                Width = 148
                Height = 129
                Align = alLeft
                Caption = ' Informa'#231#245'es do movimento: '
                Items.Strings = (
                  '00 - OK'
                  '16 - Info faltando'
                  '32 - Info parcial'
                  '48 - Info dubia'
                  '64 - Info errada')
                TabOrder = 0
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object RG20Ordem1: TRadioGroup
                Left = 521
                Top = 0
                Width = 90
                Height = 129
                Align = alLeft
                Caption = 'Ordem 1:'
                ItemIndex = 0
                Items.Strings = (
                  'Tem Aviso'
                  'NFe  '
                  'Data'
                  'IME-C'
                  'Indexa'#231#227'o')
                TabOrder = 1
              end
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 257
                Height = 129
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 2
                object GroupBox23: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 257
                  Height = 65
                  Align = alTop
                  Caption = ' Per'#237'odo: '
                  TabOrder = 0
                  object TP20DataIni: TdmkEditDateTimePicker
                    Left = 8
                    Top = 40
                    Width = 120
                    Height = 21
                    CalColors.TextColor = clMenuText
                    Date = 37636.000000000000000000
                    Time = 0.777157974502188200
                    TabOrder = 1
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                  object Ck20DataIni: TCheckBox
                    Left = 8
                    Top = 20
                    Width = 120
                    Height = 17
                    Caption = 'Data inicial'
                    TabOrder = 0
                  end
                  object Ck20DataFim: TCheckBox
                    Left = 132
                    Top = 20
                    Width = 120
                    Height = 17
                    Caption = 'Data final'
                    TabOrder = 2
                  end
                  object TP20DataFim: TdmkEditDateTimePicker
                    Left = 132
                    Top = 40
                    Width = 120
                    Height = 21
                    Date = 37636.000000000000000000
                    Time = 0.777203761601413100
                    TabOrder = 3
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                end
                object GroupBox24: TGroupBox
                  Left = 0
                  Top = 65
                  Width = 257
                  Height = 64
                  Align = alClient
                  Caption = ' Intervalo de NF-es emitidas: '
                  TabOrder = 1
                  object Panel57: TPanel
                    Left = 2
                    Top = 15
                    Width = 253
                    Height = 47
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label57: TLabel
                      Left = 12
                      Top = 4
                      Width = 30
                      Height = 13
                      Caption = 'Inicial:'
                    end
                    object Label58: TLabel
                      Left = 96
                      Top = 4
                      Width = 25
                      Height = 13
                      Caption = 'Final:'
                    end
                    object Ed20NFeIni: TdmkEdit
                      Left = 8
                      Top = 20
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnRedefinido = Ed20NFeIniRedefinido
                    end
                    object Ed20NFeFim: TdmkEdit
                      Left = 96
                      Top = 20
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object Ck20Serie: TCheckBox
                      Left = 180
                      Top = 0
                      Width = 53
                      Height = 17
                      Caption = 'S'#233'rie:'
                      TabOrder = 2
                    end
                    object Ed20Serie: TdmkEdit
                      Left = 180
                      Top = 20
                      Width = 45
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
              object RG20Ordem2: TRadioGroup
                Left = 611
                Top = 0
                Width = 90
                Height = 129
                Align = alLeft
                Caption = 'Ordem 2:'
                ItemIndex = 1
                Items.Strings = (
                  'Tem Aviso'
                  'NFe  '
                  'Data'
                  'IME-C'
                  'Indexa'#231#227'o')
                TabOrder = 3
              end
              object RG20Ordem3: TRadioGroup
                Left = 701
                Top = 0
                Width = 90
                Height = 129
                Align = alLeft
                Caption = 'Ordem 3:'
                ItemIndex = 2
                Items.Strings = (
                  'Tem Aviso'
                  'NFe  '
                  'Data'
                  'IME-C'
                  'Indexa'#231#227'o')
                TabOrder = 4
              end
              object RG20Ordem4: TRadioGroup
                Left = 791
                Top = 0
                Width = 90
                Height = 129
                Align = alLeft
                Caption = 'Ordem 4:'
                ItemIndex = 3
                Items.Strings = (
                  'Tem Aviso'
                  'NFe  '
                  'Data'
                  'IME-C'
                  'Indexa'#231#227'o')
                TabOrder = 5
              end
              object RG20Direcao: TRadioGroup
                Left = 881
                Top = 0
                Width = 48
                Height = 129
                Align = alLeft
                Caption = 'Ordem:'
                ItemIndex = 1
                Items.Strings = (
                  #9650
                  #9660)
                TabOrder = 6
              end
              object CG20VmcSta: TdmkCheckGroup
                Left = 405
                Top = 0
                Width = 116
                Height = 129
                Align = alLeft
                Caption = ' Status oinforma'#231#227'o: '
                Items.Strings = (
                  '0 - Considerar'
                  '1 - Ignorar')
                TabOrder = 7
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
            end
            object DBGNFes: TdmkDBGridZTO
              Left = 0
              Top = 129
              Width = 996
              Height = 410
              Align = alClient
              DataSource = DsNFes
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PM20NFes
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDrawColumnCell = DBGNFesDrawColumnCell
              OnDblClick = DBGNFesDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MovimCod'
                  Title.Caption = 'IME-C'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'cStat'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Title.Caption = 'Status'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_cStat'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Title.Caption = 'Descri'#231#227'o status'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSVmcWrn'
                  Title.Caption = 'Aviso'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_VmcWarn'
                  Title.Caption = 'Descri'#231#227'o do aviso'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSVmcSeq'
                  Title.Caption = 'Indexa'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Serie'
                  Title.Caption = 'S'#233'rie'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFe'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso Kg'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = 'Area m'#178
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor Total'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Width = 200
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'TxtSaldo'
                  Title.Caption = 'Saldo (Txt)'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSVmcObs'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Terceiro'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovimID'
                  Title.Caption = 'ID Movim.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_MovimID'
                  Title.Caption = 'Descri'#231#227'o movimento'
                  Visible = True
                end>
            end
          end
          object TabSheet27: TTabSheet
            Caption = 'Rendim.Semi'
            ImageIndex = 21
            object Panel59: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              ExplicitHeight = 545
              object Label59: TLabel
                Left = 8
                Top = 360
                Width = 54
                Height = 13
                Caption = 'IMEI inicial:'
              end
              object RadioGroup1: TRadioGroup
                Left = 0
                Top = 495
                Width = 996
                Height = 44
                Align = alBottom
                Caption = ' Estoques a serem considerados: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Somente positivos'
                  'Positivos e negativos'
                  'Todos inclusive zerados')
                TabOrder = 0
                ExplicitTop = 501
                ExplicitWidth = 999
              end
              object Panel60: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 189
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitWidth = 999
                object GroupBox27: TGroupBox
                  Left = 445
                  Top = 0
                  Width = 551
                  Height = 189
                  Align = alClient
                  Caption = ' Tipo de material: '
                  TabOrder = 0
                  ExplicitWidth = 554
                  object DBG21CouNiv2: TdmkDBGridZTO
                    Left = 2
                    Top = 15
                    Width = 550
                    Height = 173
                    Align = alClient
                    DataSource = Ds21CouNiv2
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Codigo'
                        Width = 39
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Width = 148
                        Visible = True
                      end>
                  end
                end
                object Panel62: TPanel
                  Left = 0
                  Top = 0
                  Width = 445
                  Height = 189
                  Align = alLeft
                  Caption = 'Panel62'
                  TabOrder = 1
                  object Label60: TLabel
                    Left = 8
                    Top = 4
                    Width = 57
                    Height = 13
                    Caption = 'Fornecedor:'
                  end
                  object Ed21Terceiro: TdmkEditCB
                    Left = 8
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CB21Terceiro
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CB21Terceiro: TdmkDBLookupComboBox
                    Left = 64
                    Top = 20
                    Width = 313
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NOMEENTIDADE'
                    ListSource = Ds21Fornecedor
                    TabOrder = 1
                    dmkEditCB = Ed21Terceiro
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object GroupBox25: TGroupBox
                    Left = 8
                    Top = 44
                    Width = 245
                    Height = 73
                    Caption = ' Per'#237'odo: '
                    TabOrder = 2
                    object TP21DataIni: TdmkEditDateTimePicker
                      Left = 8
                      Top = 40
                      Width = 112
                      Height = 21
                      CalColors.TextColor = clMenuText
                      Date = 37636.000000000000000000
                      Time = 0.777157974502188200
                      TabOrder = 1
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object Ck21DataIni: TCheckBox
                      Left = 8
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'Data inicial'
                      Checked = True
                      State = cbChecked
                      TabOrder = 0
                    end
                    object Ck21DataFim: TCheckBox
                      Left = 124
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'Data final'
                      TabOrder = 2
                    end
                    object TP21DataFim: TdmkEditDateTimePicker
                      Left = 124
                      Top = 40
                      Width = 112
                      Height = 21
                      Date = 37636.000000000000000000
                      Time = 0.777203761601413100
                      TabOrder = 3
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                  end
                  object CG21Bastidao: TdmkCheckGroup
                    Left = 8
                    Top = 120
                    Width = 437
                    Height = 65
                    Caption = ' Bastid'#227'o: '
                    Columns = 4
                    Items.Strings = (
                      'N/D'
                      'Integral'
                      'Laminado'
                      'Dividido tripa'
                      'Dividido curtido'
                      'Rebaixado '
                      'Dividido semi'
                      'Rebaix. em semi')
                    TabOrder = 3
                    UpdType = utYes
                    Value = 0
                    OldValor = 0
                  end
                  object GroupBox26: TGroupBox
                    Left = 256
                    Top = 44
                    Width = 189
                    Height = 73
                    Caption = ' Per'#237'odo: '
                    TabOrder = 4
                    object Ck21OPIni: TCheckBox
                      Left = 8
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'OP inicial'
                      Checked = True
                      State = cbChecked
                      TabOrder = 0
                    end
                    object Ck21OPFim: TCheckBox
                      Left = 92
                      Top = 20
                      Width = 112
                      Height = 17
                      Caption = 'OP final'
                      TabOrder = 1
                    end
                    object Ed21OPIni: TdmkEdit
                      Left = 8
                      Top = 40
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object Ed21OPFim: TdmkEdit
                      Left = 92
                      Top = 40
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
              object Panel61: TPanel
                Left = 0
                Top = 189
                Width = 996
                Height = 156
                Align = alTop
                TabOrder = 2
                ExplicitWidth = 999
                object RG21Ordem1: TRadioGroup
                  Left = 1
                  Top = 1
                  Width = 168
                  Height = 154
                  Align = alLeft
                  Caption = ' Ordem 1: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Fornecedor'
                    'Serie e N'#176' NFe'
                    'Data'
                    'Artigo origem'
                    'Artigo destino'
                    'Bastid'#227'o'
                    'OP'
                    'IME-I origem'
                    'IME-I destino')
                  TabOrder = 0
                end
                object RG21Ordem2: TRadioGroup
                  Left = 169
                  Top = 1
                  Width = 168
                  Height = 154
                  Align = alLeft
                  Caption = ' Ordem 2: '
                  ItemIndex = 1
                  Items.Strings = (
                    'Fornecedor'
                    'Serie e N'#176' NFe'
                    'Data'
                    'Artigo origem'
                    'Artigo destino'
                    'Bastid'#227'o'
                    'OP'
                    'IME-I origem'
                    'IME-I destino')
                  TabOrder = 1
                end
                object RG21Ordem3: TRadioGroup
                  Left = 337
                  Top = 1
                  Width = 168
                  Height = 154
                  Align = alLeft
                  Caption = ' Ordem 3: '
                  ItemIndex = 2
                  Items.Strings = (
                    'Fornecedor'
                    'Serie e N'#176' NFe'
                    'Data'
                    'Artigo origem'
                    'Artigo destino'
                    'Bastid'#227'o'
                    'OP'
                    'IME-I origem'
                    'IME-I destino')
                  TabOrder = 2
                end
                object RG21Agrupa: TRadioGroup
                  Left = 673
                  Top = 1
                  Width = 96
                  Height = 154
                  Align = alLeft
                  Caption = ' Agrupamentos: '
                  ItemIndex = 0
                  Items.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                  TabOrder = 3
                end
                object RG21Ordem4: TRadioGroup
                  Left = 505
                  Top = 1
                  Width = 168
                  Height = 154
                  Align = alLeft
                  Caption = ' Ordem 4: '
                  ItemIndex = 3
                  Items.Strings = (
                    'Fornecedor'
                    'Serie e N'#176' NFe'
                    'Data'
                    'Artigo origem'
                    'Artigo destino'
                    'Bastid'#227'o'
                    'OP'
                    'IME-I origem'
                    'IME-I destino')
                  TabOrder = 4
                end
              end
              object Ed21ImeiIni: TdmkEdit
                Left = 8
                Top = 376
                Width = 80
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '4414'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '4414'
                ValWarn = False
              end
            end
          end
          object TabSheet28: TTabSheet
            Caption = 'Compara Pallets'
            ImageIndex = 22
            object Panel64: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 53
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              object Label62: TLabel
                Left = 8
                Top = 8
                Width = 39
                Height = 13
                Caption = 'Pallet A:'
              end
              object Label63: TLabel
                Left = 92
                Top = 8
                Width = 39
                Height = 13
                Caption = 'Pallet B:'
              end
              object Ed22PalletA: TdmkEdit
                Left = 8
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed22PalletB: TdmkEdit
                Left = 92
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object TabSheet29: TTabSheet
            Caption = 'NF Ret. MO'
            ImageIndex = 23
            object Panel66: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              ExplicitHeight = 543
              object Panel67: TPanel
                Left = 0
                Top = 0
                Width = 999
                Height = 40
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Ck23Indef: TCheckBox
                  Left = 8
                  Top = 4
                  Width = 97
                  Height = 17
                  Caption = 'S'#243' indefinidos.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                end
              end
              object DBG23VSMORet: TdmkDBGridZTO
                Left = 0
                Top = 40
                Width = 999
                Height = 503
                Align = alClient
                DataSource = Ds23VSMORet
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 1
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = DBG23VSMORetDblClick
              end
            end
          end
          object TabSheet30: TTabSheet
            Caption = 'Rendimento e MO'
            ImageIndex = 24
            object Panel68: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              ExplicitHeight = 543
            end
          end
          object TabSheet31: TTabSheet
            Caption = 'Classifica'#231#245'es inconsistentes'
            ImageIndex = 25
            object Panel70: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              ExplicitHeight = 543
            end
          end
          object TabSheet32: TTabSheet
            Caption = 'Estoque Custo Integrado '
            ImageIndex = 26
            object Panel71: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 539
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 999
              ExplicitHeight = 543
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 674
    Width = 1008
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 729
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel14: TPanel
        Left = 0
        Top = 0
        Width = 181
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtOK: TBitBtn
          Tag = 14
          Left = 16
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtOKClick
        end
        object BtDesfazOrdenacao: TBitBtn
          Tag = 329
          Left = 137
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesfazOrdenacaoClick
        end
      end
      object PnFichas: TPanel
        Left = 181
        Top = 0
        Width = 679
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object BtImprime: TBitBtn
          Tag = 5
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Imprime'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtImprimeClick
        end
        object BtTudo: TBitBtn
          Tag = 127
          Left = 140
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Todos'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtTudoClick
        end
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 260
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Nenhum'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtNenhumClick
        end
        object BtIncosist: TBitBtn
          Tag = 95
          Left = 388
          Top = 4
          Width = 160
          Height = 40
          Caption = '&Verifica inconsist'#234'ncas'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BtIncosistClick
        end
      end
    end
  end
  object Qr02Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 344
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds02Fornecedor: TDataSource
    DataSet = Qr02Fornecedor
    Left = 344
    Top = 60
  end
  object Qr02VSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 412
    Top = 8
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds02VSPallet: TDataSource
    DataSet = Qr02VSPallet
    Left = 412
    Top = 52
  end
  object Qr02GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 484
    Top = 8
    object Qr02GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr02GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr02GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr02GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr02GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr02GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds02GraGruX: TDataSource
    DataSet = Qr02GraGruX
    Left = 484
    Top = 52
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      
        ' ELT(wmi.MovimID + 1,"Ajuste","Compra","Venda","Reclasse","Baixa' +
        '") NO_MovimID,'
      'vsp.Nome NO_Pallet,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet'
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'WHERE wmi.Empresa=-11'
      'AND wmi.DataHora  BETWEEN "2013-09-09" AND "2013-12-08 23:59:59"'
      'ORDER BY wmi.DataHora, wmi.Pecas DESC; ')
    Left = 104
    Top = 576
    object QrVSMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsNO_MovimID: TWideStringField
      DisplayWidth = 25
      FieldName = 'NO_MovimID'
      Origin = 'NO_MovimID'
      Size = 8
    end
    object QrVSMovItsMediaArM2: TFloatField
      FieldName = 'MediaArM2'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrVSMovItsMediaPeso: TFloatField
      FieldName = 'MediaPeso'
      DisplayFormat = '0.000;-0.000; '
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 104
    Top = 620
  end
  object QrEstqR3: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEstqR3AfterOpen
    BeforeClose = QrEstqR3BeforeClose
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 236
    Top = 576
    object QrEstqR3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR3Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrEstqR3PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR3NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR3Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR3NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR3Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR3NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR3ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR3Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR3OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR3NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR3Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsEstqR3: TDataSource
    DataSet = QrEstqR3
    Left = 236
    Top = 620
  end
  object QrGGXPalTer: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'PalVrtPeca > 0'
    Filtered = True
    OnCalcFields = QrGGXPalTerCalcFields
    SQL.Strings = (
      '/*FmVSMovImp.QrGGXPalTer*/'
      '/*********** SQL ***********/'
      
        'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.P' +
        'esoKg) PesoKg, '
      
        'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ' +
        'ValorT,  '
      
        'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso, ' +
        ' '
      'Sum(mi4.SdoVrtArM2) SdoVrtArM2, '
      'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca,  '
      'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso, '
      'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2,  '
      'SUM(mi4.LmbVrtPeca) LmbVrtPeca,  '
      
        'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2, ' +
        ' '
      
        'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.' +
        'Terceiro, '
      
        'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO' +
        '_EMPRESA, '
      
        'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY' +
        ', '
      
        'mi4.NO_GGY, IF(plb.MontPalt IS NULL, SUM(mi4.PalStat), 4) PalSta' +
        't, '
      
        'ELT(IF(plb.MontPalt IS NULL, SUM(mi4.PalStat), 4) + 1, "Encerrad' +
        'o", '
      
        '"Removido", "Desmontando", "Remo+Desmo", "Montando", "Multi[5+]"' +
        ') '
      'NO_PalStat, mi4.Ativo,'
      'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0, '
      
        'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbV' +
        'rtPeca), 0) '
      'Media, IF(plb.MontPalt IS NULL, 0, 4) MontPalt '
      'FROM  _vsmovimp4_fmvsmovimp mi4'
      
        'LEFT JOIN _vslstpalbox_fmvsmovimp plb ON plb.MontPalt=mi4.Pallet' +
        ' '
      'WHERE (mi4.SdoVrtPeca > 0 OR mi4.LmbVrtPeca) > 0  '
      'AND mi4.Pallet IN ('
      '457,'
      '453,'
      '452,'
      '451,'
      '449,'
      '448,'
      '447,'
      '446,'
      '445,'
      '444,'
      '443,'
      '440,'
      '438,'
      '437,'
      '434,'
      '433,'
      '432,'
      '429,'
      '424,'
      '422,'
      '419,'
      '415,'
      '347,'
      '346,'
      '345) '
      'GROUP BY mi4.Pallet '
      'ORDER BY NO_CouNiv1, NO_PRD_TAM_COR, Pallet DESC, OrdGGX '
      '/********* FIM SQL *********/'
      ''
      '/*****Query sem parametros*******/')
    Left = 304
    Top = 576
    object QrGGXPalTerEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrGGXPalTerGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGGXPalTerPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrGGXPalTerPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXPalTerNO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrGGXPalTerTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrGGXPalTerNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrGGXPalTerPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrGGXPalTerNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrGGXPalTerValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGGXPalTerAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrGGXPalTerOrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrGGXPalTerNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrGGXPalTerSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrGGXPalTerSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrGGXPalTerSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrGGXPalTerSdoVrtArP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SdoVrtArP2'
      Calculated = True
    end
    object QrGGXPalTerPalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object QrGGXPalTerPalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object QrGGXPalTerPalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object QrGGXPalTerStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrGGXPalTerNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrGGXPalTerDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrGGXPalTerOrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object QrGGXPalTerGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGGXPalTerNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrGGXPalTerLmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object QrGGXPalTerLmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object QrGGXPalTerLmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object QrGGXPalTerCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrGGXPalTerNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrGGXPalTerNO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object QrGGXPalTerMedia: TFloatField
      FieldName = 'Media'
    end
    object QrGGXPalTerCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrGGXPalTerNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrGGXPalTerCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrGGXPalTerNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrGGXPalTerInteiros: TFloatField
      FieldName = 'Inteiros'
      Required = True
    end
    object QrGGXPalTerPalStat: TIntegerField
      FieldName = 'PalStat'
    end
  end
  object frxDsGGXPalTer: TfrxDBDataset
    UserName = 'frxDsGGXPalTer'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Terceiro=Terceiro'
      'NO_FORNECE=NO_FORNECE'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'ValorT=ValorT'
      'Ativo=Ativo'
      'OrdGGX=OrdGGX'
      'NO_STATUS=NO_STATUS'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtArP2=SdoVrtArP2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'Status=Status'
      'NO_EMPRESA=NO_EMPRESA'
      'DataHora=DataHora'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'CliStat=CliStat'
      'NO_CLISTAT=NO_CLISTAT'
      'PalStat=PalStat'
      'NO_PalStat=NO_PalStat'
      'Media=Media'
      'CouNiv2=CouNiv2'
      'NO_CouNiv2=NO_CouNiv2'
      'CouNiv1=CouNiv1'
      'NO_CouNiv1=NO_CouNiv1'
      'Inteiros=Inteiros')
    DataSet = QrGGXPalTer
    BCDToCurrency = False
    DataSetOptions = []
    Left = 304
    Top = 620
  end
  object PM04Imprime: TPopupMenu
    Left = 396
    Top = 564
    object Fichas1: TMenuItem
      Caption = '&Fichas'
      object Argox1: TMenuItem
        Caption = 'T'#233'rmica 100x150 (Zebra, Argox, TSC)'
        OnClick = Argox1Click
      end
      object rmica95x22ZebraArgoxTSC1: TMenuItem
        Caption = 'T'#233'rmica 095x220 (Zebra, Argox, TSC)'
        OnClick = rmica95x22ZebraArgoxTSC1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object FichasCOMonomedoPallet1: TMenuItem
        Caption = 'Fichas &COM o nome do Pallet'
        OnClick = FichasCOMonomedoPallet1Click
      end
      object FichasSEMonomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM o nome do Pallet'
        OnClick = FichasSEMonomedoPallet1Click
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object Etiquetas8porpgina1: TMenuItem
        Caption = 'Etiquetas (8 por p'#225'gina)'
        OnClick = Etiquetas8porpgina1Click
      end
    end
    object PackLists1: TMenuItem
      Caption = '&Pack Lists'
      object Vertical1: TMenuItem
        Caption = '&Vertical (colunas) m'#178
        OnClick = Vertical1Click
      end
      object Verticalcolunasft1: TMenuItem
        Caption = '&Vertical (colunas) ft'#178
        OnClick = Verticalcolunasft1Click
      end
      object Horizontal1: TMenuItem
        Caption = '&Horizontal (linhas) m'#178
        OnClick = Horizontal1Click
      end
      object Horizontallinhasft1: TMenuItem
        Caption = '&Horizontal (linhas) ft'#178
        OnClick = Horizontallinhasft1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Lista1: TMenuItem
      Caption = '&Lista sem nota'
      OnClick = Lista1Click
    end
    object ListacomNota1: TMenuItem
      Caption = 'Lista com &Nota'
      OnClick = ListacomNota1Click
    end
    object ListacomRevisor1: TMenuItem
      Caption = 'Lista com Revisor'
      OnClick = ListacomRevisor1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PalletxFornecedor1: TMenuItem
      Caption = 'Pallet x Fornecedor'
      object otalizarporfornecedor1: TMenuItem
        Caption = 'Totalizar por fornecedor'
        OnClick = otalizarporfornecedor1Click
      end
      object otalizarporPallet1: TMenuItem
        Caption = 'Totalizar por Pallet'
        OnClick = otalizarporPallet1Click
      end
    end
  end
  object QrEstqR4: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'PalVrtPeca > 0'
    Filtered = True
    AfterOpen = QrEstqR4AfterOpen
    BeforeClose = QrEstqR4BeforeClose
    OnCalcFields = QrEstqR4CalcFields
    SQL.Strings = (
      'SELECT Empresa, GraGruX, SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, '
      'Sum(AreaM2) AreaM2, SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,  '
      'SUM(SdoVrtPeca) SdoVrtPeca, SUM(SdoVrtPeso) SdoVrtPeso,  '
      'Sum(SdoVrtArM2) SdoVrtArM2, SUM(LmbVrtPeca) LmbVrtPeca,  '
      'SUM(LmbVrtPeso) LmbVrtPeso, SUM(LmbVrtArM2) LmbVrtArM2,  '
      'GraGru1, NO_PRD_TAM_COR, Pallet, NO_PALLET, Terceiro, CliStat,  '
      'Status, NO_FORNECE,  NO_CLISTAT, NO_EMPRESA, NO_STATUS,  '
      
        'DataHora, OrdGGX, OrdGGY, GraGruY, NO_GGY, SUM(PalStat) PalStat,' +
        ' '
      'Ativo '
      'FROM  _vsmovimp4_fmvsmovimp '
      'WHERE (Pecas >= 0.001 OR Pecas <= -0.001)  '
      'AND SdoVrtPeca > 0  '
      'GROUP BY Pallet '
      'ORDER BY Pallet DESC, PalStat  ')
    Left = 152
    Top = 388
    object QrEstqR4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR4Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrEstqR4PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR4NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR4Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR4Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR4ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR4SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR4SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrEstqR4SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4SdoVrtArP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SdoVrtArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
    object QrEstqR4Status: TIntegerField
      FieldName = 'Status'
    end
    object QrEstqR4NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstqR4DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR4OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object QrEstqR4GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstqR4NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrEstqR4LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object QrEstqR4LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object QrEstqR4LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrEstqR4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrEstqR4NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 50
    end
    object QrEstqR4PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object QrEstqR4PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object QrEstqR4PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4Media: TFloatField
      FieldName = 'Media'
      DisplayFormat = '#,##0.00'
    end
    object QrEstqR4NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrEstqR4NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object QrEstqR4MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstqR4MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstqR4IMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrEstqR4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqR4IMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrEstqR4SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrEstqR4NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrEstqR4Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrEstqR4PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object QrEstqR4FaixaMediaM2: TFloatField
      FieldName = 'FaixaMediaM2'
    end
  end
  object DsEstqR4: TDataSource
    DataSet = QrEstqR4
    Left = 152
    Top = 432
  end
  object frxWET_CURTI_018_03_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxWET_CURTI_018_XX_AGetValue
    Left = 304
    Top = 532
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 483.779840000000000000
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 90.811070000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.811070000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 90.811070000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 20.015770000000000000
          Top = 128.795300000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'PALLET  [frxDsGGXPalTer."Pallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 1.118120000000000000
          Top = 176.574830000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 1.118120000000000000
          Top = 173.015770000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 268.448828660000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 268.448828660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 226.574830000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078740160000000000
          Top = 362.937034720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 306.244106690000000000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          DataField = 'DataHora'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 306.244106690000000000
          Width = 491.338582680000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Terceiro"] - [frxDsGGXPalTer."NO_FORNECE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944881890000000000
          Top = 226.574830000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"] - [frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsGGXPalTer."' +
              'Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsGGXPalTer."' +
              'PesoKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078740160000000000
          Top = 400.732312760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944881890000000000
          Top = 180.795300000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          DataField = 'NO_EMPRESA'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Top = 180.795300000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Top = 56.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 128.606370000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PALLET"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object Qr04GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 300
    Top = 388
    object Qr04GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr04GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr04GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr04GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr04GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr04GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds04GraGruX: TDataSource
    DataSet = Qr04GraGruX
    Left = 300
    Top = 432
  end
  object frxWET_CURTI_018_03_B2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '                                        '
      'end.')
    OnGetValue = frxWET_CURTI_018_XX_AGetValue
    Left = 304
    Top = 484
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Mat'#233'ria-Prima para o Recurtimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 64.252010000000000000
          Width = 181.417352130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 64.252010000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 64.252010000000000000
          Width = 257.007966770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Top = 64.252010000000000000
          Width = 75.590502360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status do pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Top = 64.252010000000000000
          Width = 45.354328270000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 990.236860000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 181.417352130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Width = 257.007966770000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Media'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Media"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Width = 75.590502360000000000
          Height = 15.118110240000000000
          DataField = 'NO_PalStat'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PalStat"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 45.354286770000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 453.543600000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 408.189240000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 574.488364720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE PALLETS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo1Head: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 325.039580000000000000
        Width = 990.236860000000000000
        object MeGrupo1Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 128.503824720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallets: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo2Head: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 990.236860000000000000
        object MeGrupo2Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 128.503824720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallets: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object Qr06Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 856
    Top = 288
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds06Fornecedor: TDataSource
    DataSet = Qr06Fornecedor
    Left = 856
    Top = 332
  end
  object Qr06VSPalletSrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspalleta'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 924
    Top = 288
    object Qr06VSPalletSrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06VSPalletSrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds06VSPalletSrc: TDataSource
    DataSet = Qr06VSPalletSrc
    Left = 924
    Top = 332
  end
  object Qr06GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle>0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 796
    Top = 384
    object IntegerField5: TIntegerField
      FieldName = 'GraGru1'
    end
    object IntegerField11: TIntegerField
      FieldName = 'Controle'
    end
    object StringField5: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object StringField11: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object IntegerField12: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object StringField12: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds06GraGruX: TDataSource
    DataSet = Qr06GraGruX
    Left = 796
    Top = 428
  end
  object Qr06VSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 860
    Top = 384
    object Qr06VSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06VSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds06VSSerFch: TDataSource
    DataSet = Qr06VSSerFch
    Left = 861
    Top = 429
  end
  object frxWET_CURTI_018_06_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_018_XX_AGetValue
    Left = 492
    Top = 484
    Datasets = <
      item
        DataSet = frxDs06VSCacGBY
        DataSetName = 'frxDs06VSCacGBY'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 139.842600240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Separa'#231#227'o por Tamanhos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 124.724490000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% (m'#178')')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 60.472480000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VSCacCod]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 60.472480000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OC')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VSPalletSrc]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 75.590600000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 90.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VMI_Sorc]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 90.708720000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I Origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VMI_Dest]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I Destino')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Martelo]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 75.590600000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Martelo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 75.590600000000000000
          Width = 306.141861650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Periodo]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Top = 75.590600000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 60.472480000000000000
          Width = 68.031498500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SerieFch]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 60.472480000000000000
          Width = 64.251961180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 60.472480000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Ficha]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 90.708720000000000000
          Width = 306.141861650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Terceiro]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Top = 90.708720000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 60.472480000000000000
          Width = 105.826791180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_GraGruX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Top = 60.472480000000000000
          Width = 64.251961180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 105.826840000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_Marca]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178'/pe'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 90.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_VSPalletDst]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 90.708720000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet destino')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CacID]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 60.472480000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma classif.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 105.826840000000000000
          Width = 117.165381180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CouNiv1]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Parte material')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 105.826840000000000000
          Width = 120.944911180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CouNiv2]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 298.582870000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo material')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        DataSet = frxDs06VSCacGBY
        DataSetName = 'frxDs06VSCacGBY'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000010000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          DataField = 'NO_TAMANHO'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."NO_TAMANHO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Tamanho'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."Tamanho"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Percentual'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."Percentual"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'MediaM2'
          DataSet = frxDs06VSCacGBY
          DataSetName = 'frxDs06VSCacGBY'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs06VSCacGBY."MediaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 835.276130000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 782.362710000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000430000
          Width = 302.362204720000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs06VSCacGBY."Percentual">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs06VSCacGBY."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs06VSCacGBY."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs06VSCacGBY."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118119999999980000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDs06VSCacGBY."AreaM2">,MD002,1) / SUM(<frxDs06VSCacGBY.' +
              '"Pecas">,MD002,1) ]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 464.881892200000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object ChartPie0: TfrxChartView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 461.102362200000000000
          HighlightColor = clBlack
          Frame.Typ = []
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C65080B4178697356697369626C65080D4672616D652E5669736962
            6C6508175669657733444F7074696F6E732E456C65766174696F6E033B011856
            69657733444F7074696F6E732E4F7274686F676F6E616C08195669657733444F
            7074696F6E732E50657273706563746976650200165669657733444F7074696F
            6E732E526F746174696F6E0368010B56696577334457616C6C73080A42657665
            6C4F75746572070662764E6F6E6505436F6C6F720707636C57686974650D4465
            6661756C7443616E766173060E54474449506C757343616E76617311436F6C6F
            7250616C65747465496E646578020D000A545069655365726965730753657269
            6573310C486F766572456C656D656E740B00114D61726B732E5461696C2E4D61
            7267696E02020D5856616C7565732E4F72646572070B6C6F417363656E64696E
            670C5956616C7565732E4E616D6506035069650D5956616C7565732E4F726465
            7207066C6F4E6F6E651A4672616D652E496E6E657242727573682E4261636B43
            6F6C6F720705636C526564224672616D652E496E6E657242727573682E477261
            6469656E742E456E64436F6C6F720706636C47726179224672616D652E496E6E
            657242727573682E4772616469656E742E4D6964436F6C6F720707636C576869
            7465244672616D652E496E6E657242727573682E4772616469656E742E537461
            7274436F6C6F720440404000214672616D652E496E6E657242727573682E4772
            616469656E742E56697369626C65091B4672616D652E4D6964646C6542727573
            682E4261636B436F6C6F720708636C59656C6C6F77234672616D652E4D696464
            6C6542727573682E4772616469656E742E456E64436F6C6F7204828282002346
            72616D652E4D6964646C6542727573682E4772616469656E742E4D6964436F6C
            6F720707636C5768697465254672616D652E4D6964646C6542727573682E4772
            616469656E742E5374617274436F6C6F720706636C47726179224672616D652E
            4D6964646C6542727573682E4772616469656E742E56697369626C65091A4672
            616D652E4F7574657242727573682E4261636B436F6C6F720707636C47726565
            6E224672616D652E4F7574657242727573682E4772616469656E742E456E6443
            6F6C6F720440404000224672616D652E4F7574657242727573682E4772616469
            656E742E4D6964436F6C6F720707636C5768697465244672616D652E4F757465
            7242727573682E4772616469656E742E5374617274436F6C6F720708636C5369
            6C766572214672616D652E4F7574657242727573682E4772616469656E742E56
            697369626C65090B4672616D652E57696474680204194F74686572536C696365
            2E4C6567656E642E56697369626C6508000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs06VSCacGBY
              DataSetName = 'frxDs06VSCacGBY'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs06VSCacGBY."NO_TAMANHO"'
              Source2 = 'frxDs06VSCacGBY."Percentual"'
              XSource = 'frxDs06VSCacGBY."NO_TAMANHO"'
              YSource = 'frxDs06VSCacGBY."Percentual"'
            end>
        end
      end
    end
  end
  object Qr06VSCacGBY: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = Qr06VSCacGBYCalcFields
    SQL.Strings = (
      'SELECT gby.VMI_Dest, vmi.GraGruX,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, '
      'SUM(gby.AreaM2) Teste,'
      'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2'
      'FROM _vscacgby_ gby'
      
        'LEFT JOIN bluederm_2_1_cialeather.vsmovits vmi ON vmi.Controle=g' +
        'by.VMI_Dest'
      
        'LEFT JOIN bluederm_2_cialeather.gragrux    ggx ON ggx.Controle=v' +
        'mi.GraGruX'
      
        'LEFT JOIN bluederm_2_cialeather.gragruy    ggy ON ggy.Codigo=ggx' +
        '.GraGruY'
      
        'LEFT JOIN bluederm_2_cialeather.gragruc    ggc ON ggc.Controle=g' +
        'gx.GraGruC'
      
        'LEFT JOIN bluederm_2_cialeather.gracorcad  gcc ON gcc.Codigo=ggc' +
        '.GraCorCad'
      
        'LEFT JOIN bluederm_2_cialeather.gratamits  gti ON gti.Controle=g' +
        'gx.GraTamI'
      
        'LEFT JOIN bluederm_2_cialeather.gragru1    gg1 ON gg1.Nivel1=ggx' +
        '.GraGru1'
      'GROUP BY vmi.GraGruX'
      'ORDER BY AreaM2 DESC')
    Left = 492
    Top = 532
    object Qr06VSCacGBYVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object Qr06VSCacGBYGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr06VSCacGBYGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr06VSCacGBYNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr06VSCacGBYPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr06VSCacGBYAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr06VSCacGBYAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr06VSCacGBYPercentual: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Percentual'
      Calculated = True
    end
    object Qr06VSCacGBYNO_TAMANHO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TAMANHO'
      Size = 255
      Calculated = True
    end
    object Qr06VSCacGBYTamanho: TIntegerField
      FieldName = 'Tamanho'
    end
    object Qr06VSCacGBYMediaM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MediaM2'
      Calculated = True
    end
  end
  object frxDs06VSCacGBY: TfrxDBDataset
    UserName = 'frxDs06VSCacGBY'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VMI_Dest=VMI_Dest'
      'GraGruX=GraGruX'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Percentual=Percentual'
      'NO_TAMANHO=NO_TAMANHO'
      'Tamanho=Tamanho'
      'MediaM2=MediaM2')
    DataSet = Qr06VSCacGBY
    BCDToCurrency = False
    DataSetOptions = []
    Left = 496
    Top = 580
  end
  object Qr06SumGBY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(gby.Pecas) Pecas,'
      'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2'
      'FROM _vscacgby_ gby')
    Left = 496
    Top = 624
    object Qr06SumGBYPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr06SumGBYAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr06SumGBYAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object Ds06VSPalletDst: TDataSource
    DataSet = Qr06VSPalletDst
    Left = 992
    Top = 336
  end
  object Qr06VSPalletDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspalleta'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 992
    Top = 292
    object Qr06VSPalletDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06VSPalletDstNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Qr06CouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 380
    Top = 388
    object Qr06CouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06CouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds06CouNiv1: TDataSource
    DataSet = Qr06CouNiv1
    Left = 380
    Top = 436
  end
  object Qr06CouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 448
    Top = 388
    object Qr06CouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06CouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds06CouNiv2: TDataSource
    DataSet = Qr06CouNiv2
    Left = 448
    Top = 436
  end
  object Qr07PrevPal: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = Qr07PrevPalAfterOpen
    BeforeClose = Qr07PrevPalBeforeClose
    SQL.Strings = (
      'SELECT cia.VSPallet,  '
      'SUM(cia.Pecas) Pecas,  '
      'SUM(cia.AreaM2) AreaM2, '
      'SUM(cia.AreaP2) AreaP2, '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),  '
      'IF(pal.Nome <> "", CONCAT(" (", pal.Nome, ")"), ""))   '
      'NO_PRD_TAM_COR, pal.*   '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pal ON pal.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'WHERE cia.VSPallet BETWEEN 221 AND 245 '
      'GROUP BY cia.VSPallet '
      'ORDER BY cia.VSPallet DESC ')
    Left = 984
    Top = 392
    object Qr07PrevPalVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object Qr07PrevPalPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr07PrevPalAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr07PrevPalAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr07PrevPalNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 220
    end
    object Qr07PrevPalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr07PrevPalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object Qr07PrevPalEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr07PrevPalStatus: TIntegerField
      FieldName = 'Status'
    end
    object Qr07PrevPalCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr07PrevPalGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr07PrevPalDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object Qr07PrevPalGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object Qr07PrevPalDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object Qr07PrevPalMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object Qr07PrevPalLk: TIntegerField
      FieldName = 'Lk'
    end
    object Qr07PrevPalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object Qr07PrevPalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object Qr07PrevPalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object Qr07PrevPalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object Qr07PrevPalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object Qr07PrevPalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr07PrevPalQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object Ds07PrevPal: TDataSource
    DataSet = Qr07PrevPal
    Left = 984
    Top = 440
  end
  object PM07Imprime: TPopupMenu
    Left = 396
    Top = 616
    object Fichas2: TMenuItem
      Caption = '&Fichas'
      OnClick = Fichas2Click
    end
    object ListacomRevisor2: TMenuItem
      Caption = 'Hist'#243'rico (com % por classificador)'
      OnClick = ListacomRevisor2Click
    end
  end
  object Qr04CouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 16
    Top = 436
    object Qr04CouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr04CouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds04CouNiv1: TDataSource
    DataSet = Qr04CouNiv1
    Left = 16
    Top = 484
  end
  object Qr04CouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 84
    Top = 388
    object Qr04CouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr04CouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds04CouNiv2: TDataSource
    DataSet = Qr04CouNiv2
    Left = 84
    Top = 436
  end
  object Qr08Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 916
    Top = 388
    object Qr08FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr08FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds08Fornecedor: TDataSource
    DataSet = Qr08Fornecedor
    Left = 916
    Top = 432
  end
  object QrNimboRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rca.VSPallet,'
      'SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2'
      'FROM vscacitsa cia'
      'LEFT JOIN vsparclcaba rca ON rca.Codigo=cia.Codigo'
      'WHERE cia.VSPaRclIts IN ('
      '     SELECT Controle'
      '     FROM vsparclitsa pri'
      '     WHERE DtHrFim < "1900-01-01"'
      ')'
      'GROUP BY rca.VSPallet'
      'ORDER BY rca.VSPallet')
    Left = 800
    Top = 288
    object QrNimboRclVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrNimboRclPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNimboRclAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNimboRcl: TDataSource
    DataSet = QrNimboRcl
    Left = 800
    Top = 336
  end
  object PM04Estq: TPopupMenu
    OnPopup = PM04EstqPopup
    Left = 708
    Top = 256
    object Irparaajaneladepallets1: TMenuItem
      Caption = 'Ir para a janela de pallets'
      OnClick = Irparaajaneladepallets1Click
    end
  end
  object Qr04GraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 92
    Top = 304
    object Qr04GraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr04GraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object Qr04GraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr04GraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object Ds04GraGruY: TDataSource
    DataSet = Qr04GraGruY
    Left = 92
    Top = 352
  end
  object Qr00GraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 24
    Top = 308
    object Qr00GraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00GraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object Qr00GraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr00GraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object Ds00GraGruY: TDataSource
    DataSet = Qr00GraGruY
    Left = 24
    Top = 352
  end
  object Qr06GraGruXDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle>0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 812
    Top = 488
    object Qr06GraGruXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr06GraGruXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr06GraGruXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr06GraGruXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr06GraGruXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr06GraGruXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds06GraGruXDst: TDataSource
    DataSet = Qr06GraGruXDst
    Left = 812
    Top = 532
  end
  object QrCiaDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _sum_vmi_dest_;'
      'CREATE TABLE _sum_vmi_dest_'
      'SELECT cia.VSPallet, cia.VMI_Dest, '
      'SUM(cia.Pecas) CiaPecas, vmi.Pecas DstPecas'
      'FROM vscacitsa cia'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=cia.VMI_Dest'
      'GROUP BY cia.VMI_Dest;'
      'SELECT * FROM _sum_vmi_dest_'
      'WHERE CiaPecas <> DstPecas')
    Left = 588
    Top = 484
    object QrCiaDstVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrCiaDstVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrCiaDstCiaPecas: TFloatField
      FieldName = 'CiaPecas'
    end
    object QrCiaDstDstPecas: TFloatField
      FieldName = 'DstPecas'
    end
  end
  object DsCiaDst: TDataSource
    DataSet = QrCiaDst
    Left = 588
    Top = 532
  end
  object Qr10FichasRMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.SerieFch, vmi.Ficha,   '
      '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   '
      'COUNT(vmi.Codigo) ITENS,   '
      'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  '
      'vsf.Nome NO_SERIEFCH   '
      'FROM vsmovits vmi  '
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  '
      'GROUP BY vmi.SerieFch, vmi.Ficha'
      'ORDER BY FICHA DESC, SerieFch')
    Left = 900
    Top = 488
    object Qr10FichasRMPSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr10FichasRMPFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr10FichasRMPFCH_SRE: TFloatField
      FieldName = 'FCH_SRE'
    end
    object Qr10FichasRMPITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object Qr10FichasRMPRefFirst: TDateTimeField
      FieldName = 'RefFirst'
    end
    object Qr10FichasRMPRefLast: TDateTimeField
      FieldName = 'RefLast'
    end
    object Qr10FichasRMPNO_SERIEFCH: TWideStringField
      FieldName = 'NO_SERIEFCH'
      Size = 60
    end
    object Qr10FichasRMPNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object Ds10FichasRMP: TDataSource
    DataSet = Qr10FichasRMP
    Left = 900
    Top = 536
  end
  object PM10FichasRMP: TPopupMenu
    OnPopup = PM10FichasRMPPopup
    Left = 516
    Top = 348
    object IrparajaneladegerenciamentodeFichasRMP1: TMenuItem
      Caption = '&Ir para janela de gerenciamento de Fichas RMP'
      OnClick = IrparajaneladegerenciamentodeFichasRMP1Click
    end
  end
  object frxWET_CURTI_018_11_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_018_XX_AGetValue
    Left = 740
    Top = 588
    Datasets = <
      item
        DataSet = frxDs11_1
        DataSetName = 'frxDs11_1'
      end
      item
        DataSet = frxDs11_2
        DataSetName = 'frxDs11_2'
      end
      item
        DataSet = frxDs11_3
        DataSetName = 'frxDs11_3'
      end
      item
        DataSet = frxDs11_4
        DataSetName = 'frxDs11_4'
      end
      item
        DataSet = frxDs11_T
        DataSetName = 'frxDs11_T'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Peles e Couros da Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        DataSet = frxDs11_1
        DataSetName = 'frxDs11_1'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs11_1."NO_SerieFch"] [frxDs11_1."Ficha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeca'
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtArM2'
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeso'
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL couros In Natura: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeca">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtArM2">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeso">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."Inteiros">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs11_1."Terceiro"'
        object Me_GH0: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs11_1."Terceiro"] - [frxDs11_1."NO_FORNECE"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeca">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT0: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952780310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."Terceiro"] - [frxDs11_1."NO_FORNECE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0AreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtArM2">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu0PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeso">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."Inteiros">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs11_1."GraGruX"'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs11_1."GraGruX"] - [frxDs11_1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeca">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952780310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_1."GraGruX"] - [frxDs11_1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1AreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtArM2">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu1PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."PalVrtPeso">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_1
          DataSetName = 'frxDs11_1'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_1."Inteiros">,MD001,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Peles e Couros da Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 64.252010000000000000
          Width = 226.771604720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 75.590404720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 64.252010000000000000
          Width = 75.590404720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs11_2
        DataSetName = 'frxDs11_2'
        RowCount = 0
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 226.771604720000000000
          Height = 15.118110240000000000
          DataField = 'IMEC'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs11_2."IMEC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeca'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtArM2'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeso'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590404720000000000
          Height = 15.118110240000000000
          DataField = 'IMEI'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs11_2."IMEI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 75.590404720000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary2: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL couros Artigo a Classificar: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs11_2."GraGruX"'
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs11_2."GraGruX"] - [frxDs11_2."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952780310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_2."GraGruX"] - [frxDs11_2."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_2
          DataSetName = 'frxDs11_2'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_2."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageHeader2: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Peles e Couros da Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs11_3
        DataSetName = 'frxDs11_3'
        RowCount = 0
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs11_3."Pallet"]   [frxDs11_2."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeca'
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_3."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtArM2'
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_3."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeso'
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_3."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_3."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary3: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL Couros Classificados: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtPeca">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtArM2">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtPeso">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."Inteiros">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs11_3."GraGruX"'
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDs11_3."GraGruX"] - [frxDs11_3."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtPeca">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952780310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_3."GraGruX"] - [frxDs11_3."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtArM2">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."PalVrtPeso">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_3
          DataSetName = 'frxDs11_3'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_3."Inteiros">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Peles e Couros da Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD004: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs11_4
        DataSetName = 'frxDs11_4'
        RowCount = 0
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs11_4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeca'
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_4."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtArM2'
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_4."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeso'
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_4."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_4."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter4: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary4: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000000000
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL Couros em Opera'#231#227'o: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtPeca">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtArM2">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtPeso">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 15.118120000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."Inteiros">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs11_4."Ativo"'
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Couros em Opera'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtPeca">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952780310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Couros em Opera'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtArM2">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."PalVrtPeso">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_4
          DataSetName = 'frxDs11_4'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs11_4."Inteiros">,MD004,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page5: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Peles e Couros da Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
      end
      object MD00T: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDs11_T
        DataSetName = 'frxDs11_T'
        RowCount = 0
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952804720000000000
          Height = 15.118110240000000000
          DataSet = frxDs11_T
          DataSetName = 'frxDs11_T'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL DE COUROS')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeca'
          DataSet = frxDs11_T
          DataSetName = 'frxDs11_T'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_T."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtArM2'
          DataSet = frxDs11_T
          DataSetName = 'frxDs11_T'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_T."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PalVrtPeso'
          DataSet = frxDs11_T
          DataSetName = 'frxDs11_T'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_T."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs11_T
          DataSetName = 'frxDs11_T'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs11_T."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter5: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object Qr11_1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp1_'
      'WHERE Ativo=1'
      'ORDER BY GraGruX, Ficha, SerieFch')
    Left = 740
    Top = 636
    object Qr11_1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr11_1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr11_1PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr11_1AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr11_1AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr11_1ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr11_1SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr11_1SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr11_1SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr11_1PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object Qr11_1PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object Qr11_1PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object Qr11_1LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object Qr11_1LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object Qr11_1LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object Qr11_1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr11_1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object Qr11_1Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr11_1NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object Qr11_1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr11_1CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr11_1Status: TIntegerField
      FieldName = 'Status'
    end
    object Qr11_1NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr11_1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object Qr11_1NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr11_1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object Qr11_1DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr11_1OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object Qr11_1OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object Qr11_1GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr11_1NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr11_1NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object Qr11_1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr11_1Media: TFloatField
      FieldName = 'Media'
    end
    object Qr11_1NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object Qr11_1NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object Qr11_1MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr11_1MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr11_1IMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object Qr11_1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr11_1IMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object Qr11_1SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr11_1NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr11_1Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr11_1Inteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr11_1PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object Qr11_1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object frxDs11_1: TfrxDBDataset
    UserName = 'frxDs11_1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat')
    DataSet = Qr11_1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 740
    Top = 684
  end
  object Qr11_2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp1_'
      'WHERE Ativo=1'
      'ORDER BY GraGruX, Ficha, SerieFch')
    Left = 792
    Top = 636
    object Qr11_2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr11_2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr11_2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr11_2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr11_2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr11_2ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr11_2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr11_2SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr11_2SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr11_2PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object Qr11_2PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object Qr11_2PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object Qr11_2LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object Qr11_2LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object Qr11_2LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object Qr11_2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr11_2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object Qr11_2Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr11_2NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object Qr11_2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr11_2CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr11_2Status: TIntegerField
      FieldName = 'Status'
    end
    object Qr11_2NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr11_2NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object Qr11_2NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr11_2NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object Qr11_2DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr11_2OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object Qr11_2OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object Qr11_2GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr11_2NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr11_2NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object Qr11_2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr11_2Media: TFloatField
      FieldName = 'Media'
    end
    object Qr11_2NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object Qr11_2NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object Qr11_2MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr11_2MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr11_2IMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object Qr11_2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr11_2IMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object Qr11_2SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr11_2NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr11_2Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr11_2Inteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr11_2PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object Qr11_2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object frxDs11_2: TfrxDBDataset
    UserName = 'frxDs11_2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'Empresa=Empresa')
    DataSet = Qr11_2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 792
    Top = 684
  end
  object Qr11_3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp1_'
      'WHERE Ativo=1'
      'ORDER BY GraGruX, Ficha, SerieFch')
    Left = 848
    Top = 636
    object Qr11_3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr11_3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr11_3PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr11_3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr11_3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr11_3ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr11_3SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr11_3SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr11_3SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr11_3PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object Qr11_3PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object Qr11_3PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object Qr11_3LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object Qr11_3LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object Qr11_3LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object Qr11_3GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr11_3NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object Qr11_3Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr11_3NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object Qr11_3Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr11_3CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr11_3Status: TIntegerField
      FieldName = 'Status'
    end
    object Qr11_3NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr11_3NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object Qr11_3NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr11_3NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object Qr11_3DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr11_3OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object Qr11_3OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object Qr11_3GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr11_3NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr11_3NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object Qr11_3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr11_3Media: TFloatField
      FieldName = 'Media'
    end
    object Qr11_3NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object Qr11_3NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object Qr11_3MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr11_3MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr11_3IMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object Qr11_3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr11_3IMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object Qr11_3SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr11_3NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr11_3Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr11_3Inteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr11_3PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object Qr11_3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object Qr11_4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp1_'
      'WHERE Ativo=1'
      'ORDER BY GraGruX, Ficha, SerieFch')
    Left = 900
    Top = 636
    object Qr11_4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr11_4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr11_4PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr11_4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr11_4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr11_4ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr11_4SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr11_4SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr11_4SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr11_4PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object Qr11_4PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object Qr11_4PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object Qr11_4LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object Qr11_4LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object Qr11_4LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object Qr11_4GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr11_4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object Qr11_4Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr11_4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object Qr11_4Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr11_4CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr11_4Status: TIntegerField
      FieldName = 'Status'
    end
    object Qr11_4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr11_4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object Qr11_4NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr11_4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object Qr11_4DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr11_4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object Qr11_4OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object Qr11_4GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr11_4NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr11_4NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object Qr11_4Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr11_4Media: TFloatField
      FieldName = 'Media'
    end
    object Qr11_4NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object Qr11_4NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object Qr11_4MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr11_4MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr11_4IMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object Qr11_4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr11_4IMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object Qr11_4SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr11_4NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr11_4Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr11_4Inteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr11_4PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object Qr11_4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object frxDs11_3: TfrxDBDataset
    UserName = 'frxDs11_3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'Empresa=Empresa')
    DataSet = Qr11_3
    BCDToCurrency = False
    DataSetOptions = []
    Left = 848
    Top = 684
  end
  object frxDs11_4: TfrxDBDataset
    UserName = 'frxDs11_4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'Empresa=Empresa')
    DataSet = Qr11_4
    BCDToCurrency = False
    DataSetOptions = []
    Left = 900
    Top = 684
  end
  object Qr11_T: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp1_'
      'WHERE Ativo=1'
      'ORDER BY GraGruX, Ficha, SerieFch')
    Left = 956
    Top = 636
    object Qr11_TGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr11_TPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr11_TPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr11_TAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr11_TAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr11_TValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr11_TSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr11_TSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr11_TSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr11_TPalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
    end
    object Qr11_TPalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
    end
    object Qr11_TPalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
    end
    object Qr11_TLmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object Qr11_TLmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object Qr11_TLmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object Qr11_TGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr11_TNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object Qr11_TPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr11_TNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object Qr11_TTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr11_TCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr11_TStatus: TIntegerField
      FieldName = 'Status'
    end
    object Qr11_TNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr11_TNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object Qr11_TNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr11_TNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object Qr11_TDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr11_TOrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object Qr11_TOrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object Qr11_TGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr11_TNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr11_TNO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object Qr11_TAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr11_TMedia: TFloatField
      FieldName = 'Media'
    end
    object Qr11_TNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object Qr11_TNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 50
    end
    object Qr11_TMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr11_TMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr11_TIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object Qr11_TCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr11_TIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object Qr11_TSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr11_TNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr11_TFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr11_TInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr11_TPalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object Qr11_TEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object frxDs11_T: TfrxDBDataset
    UserName = 'frxDs11_T'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'Empresa=Empresa')
    DataSet = Qr11_T
    BCDToCurrency = False
    DataSetOptions = []
    Left = 960
    Top = 684
  end
  object Qr04_01Sum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 36
    object Qr04_01SumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr04_01SumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object Qr13GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 852
    Top = 8
    object Qr13GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr13GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr13GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr13GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr13GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr13GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds13GraGruX: TDataSource
    DataSet = Qr13GraGruX
    Left = 852
    Top = 176
  end
  object Qr13VSSeqIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.*, '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse","For'#231'ado","Sem O' +
        'rigem","Em Operacao","Residual","Ajuste","Classe Mult.","Pr'#233' rec' +
        'lasse","Compra de Classificado","Baixa extra") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Origem gera'#231#227'o de art' +
        'igo","Destino gera'#231#227'o de artigo","Baixa gera'#231#227'o de artigo") NO_M' +
        'ovimNiv,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, '
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, '
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'vps.Nome NO_STATUS, '
      'vsf.Nome NO_SerieFch '
      'FROM _vsseqits_fmvsmovimp vmi '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   '
      
        'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp' +
        '.GraGruX, vmi.GraGruX) '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro '
      'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  '
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      '')
    Left = 616
    Top = 232
    object Qr13VSSeqItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr13VSSeqItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr13VSSeqItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object Qr13VSSeqItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr13VSSeqItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object Qr13VSSeqItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr13VSSeqItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr13VSSeqItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object Qr13VSSeqItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr13VSSeqItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object Qr13VSSeqItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object Qr13VSSeqItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr13VSSeqItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr13VSSeqItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr13VSSeqItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr13VSSeqItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr13VSSeqItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr13VSSeqItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr13VSSeqItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr13VSSeqItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr13VSSeqItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr13VSSeqItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr13VSSeqItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object Qr13VSSeqItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr13VSSeqItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr13VSSeqItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr13VSSeqItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object Qr13VSSeqItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr13VSSeqItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr13VSSeqItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object Qr13VSSeqItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object Qr13VSSeqItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object Qr13VSSeqItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object Qr13VSSeqItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object Qr13VSSeqItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object Qr13VSSeqItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object Qr13VSSeqItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object Qr13VSSeqItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object Qr13VSSeqItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object Qr13VSSeqItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object Qr13VSSeqItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object Qr13VSSeqItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object Qr13VSSeqItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object Qr13VSSeqItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object Qr13VSSeqItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object Qr13VSSeqItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object Qr13VSSeqItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object Qr13VSSeqItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object Qr13VSSeqItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object Qr13VSSeqItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object Qr13VSSeqItsInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr13VSSeqItsAcumInteir: TFloatField
      FieldName = 'AcumInteir'
    end
    object Qr13VSSeqItsSequencia: TIntegerField
      FieldName = 'Sequencia'
    end
    object Qr13VSSeqItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr13VSSeqItsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object Qr13VSSeqItsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 31
    end
    object Qr13VSSeqItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr13VSSeqItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object Qr13VSSeqItsVSP_CliStat: TLargeintField
      FieldName = 'VSP_CliStat'
    end
    object Qr13VSSeqItsVSP_STATUS: TLargeintField
      FieldName = 'VSP_STATUS'
    end
    object Qr13VSSeqItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr13VSSeqItsNO_CliStat: TWideStringField
      FieldName = 'NO_CliStat'
      Size = 100
    end
    object Qr13VSSeqItsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr13VSSeqItsNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object Qr13VSSeqItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr13VSSeqItsNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 70
    end
  end
  object Ds13VSSeqIts: TDataSource
    DataSet = Qr13VSSeqIts
    Left = 616
    Top = 280
  end
  object frxWET_CURTI_018_03_B3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '                                        '
      'end.')
    OnGetValue = frxWET_CURTI_018_XX_AGetValue
    Left = 172
    Top = 480
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque de Mat'#233'ria-Prima para o Recurtimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 64.252010000000000000
          Width = 181.417352130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Top = 64.252010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 64.252010000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia m'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Top = 64.252010000000000000
          Width = 75.590502360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status do pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 241.889920000000000000
        Width = 990.236860000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 181.417352130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."PalVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 113.385826771653500000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Media'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Media"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 914.646260000000000000
          Width = 75.590502360000000000
          Height = 15.118110240000000000
          DataField = 'NO_PalStat'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PalStat"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000010000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 4210752
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_NotaVS]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 472.441250000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 427.086890000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 574.488364720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE PALLETS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo1Head: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 990.236860000000000000
        object MeGrupo1Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 128.503824720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallets: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo2Head: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 298.582870000000000000
        Width = 990.236860000000000000
        object MeGrupo2Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."PalVrtPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 128.503824720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallets: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsGGXPalTer."PalVrtPeca">,MD002,1) = 0, 0, SUM(<frx' +
              'DsGGXPalTer."PalVrtArM2">,MD002,1) / SUM(<frxDsGGXPalTer."PalVrt' +
              'Peca">,MD002,1))]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrNotaVS: TMySQLQuery
    Database = Dmod.MyDB
    Left = 76
    Top = 484
    object QrNotaVSNotaVS: TFloatField
      FieldName = 'NotaVS'
    end
    object QrNotaVSSiglaVS: TWideStringField
      FieldName = 'SiglaVS'
      Size = 15
    end
    object QrNotaVSTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrNotaVSPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaVSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object PM13Grade: TPopupMenu
    Left = 512
    Top = 396
    object IrparajeneladoIMEIselecionado1: TMenuItem
      Caption = 'Ir para jenela do IME-I selecionado'
      OnClick = IrparajeneladoIMEIselecionado1Click
    end
    object Fluxo1: TMenuItem
      Caption = 'Fluxo'
      object Remover1: TMenuItem
        Caption = 'Remover'
        OnClick = Remover1Click
      end
      object Adicionar1: TMenuItem
        Caption = 'Adicionar'
        OnClick = Adicionar1Click
      end
    end
  end
  object PM08_01_Todos: TPopupMenu
    Left = 708
    Top = 300
    object Sadas1: TMenuItem
      Caption = '&Sa'#237'das'
      OnClick = Sadas1Click
    end
    object Entradas1: TMenuItem
      Caption = '&Entradas'
      OnClick = Entradas1Click
    end
    object odos1: TMenuItem
      Caption = '&Todos'
      OnClick = odos1Click
    end
  end
  object PM16Fatores: TPopupMenu
    OnPopup = PM16FatoresPopup
    Left = 512
    Top = 444
    object IrparajaneladeOC1: TMenuItem
      Caption = 'Ir para janela de OC'
      OnClick = IrparajaneladeOC1Click
    end
    object Irparajaneladepalletdeorigem1: TMenuItem
      Caption = 'Ir parajanela de pallet de origem'
      OnClick = Irparajaneladepalletdeorigem1Click
    end
    object Irparajaneladepalletdedestino1: TMenuItem
      Caption = 'Ir parajanela de pallet de destino'
      OnClick = Irparajaneladepalletdedestino1Click
    end
  end
  object QrRevisores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Revisor, SUM(Pecas) Pecas, MAX(DataHora) DataHora, '
      'DATE_FORMAT(MAX(DataHora), "%d/%m/%Y %H:%i:%s" ) DataHora_TXT,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_REVISOR  '
      'FROM vscacitsa cia '
      'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor  '
      'WHERE VSPallet=1311 '
      'GROUP BY Revisor ')
    Left = 684
    Top = 488
    object QrRevisoresRevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrRevisoresPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrRevisoresDataHora_TXT: TWideStringField
      FieldName = 'DataHora_TXT'
      Size = 24
    end
    object QrRevisoresDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrRevisoresNO_REVISOR: TWideStringField
      FieldName = 'NO_REVISOR'
      Size = 100
    end
  end
  object Qr03GraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 160
    Top = 304
    object Qr03GraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr03GraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object Qr03GraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr03GraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object Ds03GraGruY: TDataSource
    DataSet = Qr03GraGruY
    Left = 160
    Top = 352
  end
  object Qr03GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 236
    Top = 304
    object Qr03GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr03GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr03GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr03GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr03GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr03GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds03GraGruX: TDataSource
    DataSet = Qr03GraGruX
    Left = 236
    Top = 352
  end
  object Qr00GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 596
    Top = 580
    object Qr00GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr00GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr00GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr00GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr00GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr00GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds00GraGruX: TDataSource
    DataSet = Qr00GraGruX
    Left = 596
    Top = 624
  end
  object Qr00StqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 320
    Top = 192
    object Qr00StqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr00StqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object Ds00StqCenCad: TDataSource
    DataSet = Qr00StqCenCad
    Left = 320
    Top = 236
  end
  object Qr00CouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 212
    Top = 196
    object Qr00CouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00CouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds00CouNiv2: TDataSource
    DataSet = Qr00CouNiv2
    Left = 212
    Top = 240
  end
  object Qr00Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 508
    Top = 180
    object Qr00FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds00Fornecedor: TDataSource
    DataSet = Qr00Fornecedor
    Left = 512
    Top = 224
  end
  object PM03Imprime: TPopupMenu
    Left = 396
    Top = 516
    object Etiqueta1: TMenuItem
      Caption = '&Etiqueta'
      OnClick = Etiqueta1Click
    end
    object PackingList1: TMenuItem
      Caption = 'Packing List &Vertical'
      OnClick = PackingList1Click
    end
    object PackingListHorizontal1: TMenuItem
      Caption = 'Packing List &Horizontal'
      OnClick = PackingListHorizontal1Click
    end
  end
  object QrEstqR1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEstqR1CalcFields
    SQL.Strings = (
      'DROP TABLE IF EXISTS _TESTE_;'
      'CREATE TABLE _TESTE_'
      
        'SELECT mi1.Empresa, mi1.GraGruX, SUM(mi1.Pecas) Pecas, SUM(mi1.P' +
        'esoKg) PesoKg, '
      
        'Sum(mi1.AreaM2) AreaM2, SUM(mi1.AreaP2) AreaP2, SUM(mi1.ValorT) ' +
        'ValorT, '
      'SUM(mi1.SdoVrtPeca) SdoVrtPeca, SUM(mi1.SdoVrtPeso) SdoVrtPeso, '
      'Sum(mi1.SdoVrtArM2) SdoVrtArM2, '
      'SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca) PalVrtPeca, '
      'SUM(mi1.SdoVrtPeso+mi1.LmbVrtPeso) PalVrtPeso, '
      'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) PalVrtArM2, '
      'SUM(mi1.LmbVrtPeca) LmbVrtPeca, '
      'SUM(mi1.LmbVrtPeso) LmbVrtPeso, SUM(mi1.LmbVrtArM2) LmbVrtArM2, '
      
        'mi1.GraGru1, mi1.NO_PRD_TAM_COR, mi1.Pallet, mi1.NO_PALLET, mi1.' +
        'Terceiro, '
      
        'mi1.CliStat, mi1.Status, mi1.NO_FORNECE,  mi1.NO_CLISTAT, mi1.NO' +
        '_EMPRESA, '
      
        'mi1.NO_STATUS, mi1.DataHora, mi1.OrdGGX, mi1.OrdGGY, mi1.GraGruY' +
        ', '
      'mi1.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat, '
      'IF(SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca)  <> 0, '
      
        'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) / SUM(mi1.SdoVrtPeca+mi1.LmbV' +
        'rtPeca), 0) '
      'Media, '
      'mi1.NO_MovimNiv, mi1.NO_MovimID, mi1.MovimNiv, '
      'mi1.MovimID, mi1.IMEC, mi1.Codigo, mi1.IMEI, '
      'mi1.SerieFch, mi1.NO_SerieFch, mi1.Ficha, '
      'SUM(mi1.SdoInteiros + mi1.LmbInteiros) Inteiros, '
      'mi1.ReqMovEstq, mi1.StqCenCad, mi1.NO_StqCenCad, '
      'mi1.StqCenLoc, mi1.NO_LOC_CEN,'
      'mi1.Ativo '
      'FROM  _vsmovimp1_ mi1 '
      
        'WHERE (SdoVrtPeca > 0 OR LmbVrtPeca <> 0 OR (SdoVrtPeso > 0 AND ' +
        'MovimID=23)) '
      'GROUP BY GraGruY, GraGruX, Terceiro, Pallet'
      '; '
      'SELECT * FROM _TESTE_'
      'WHERE PalVrtPeca > 0 OR (SdoVrtPeso > 0 AND MovimID=23)'
      'ORDER BY OrdGGY, NO_GGY, NO_PRD_TAM_COR, NO_FORNECE, Pallet')
    Left = 28
    Top = 532
    object QrEstqR1Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_teste_.Empresa'
    end
    object QrEstqR1GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_teste_.GraGruX'
    end
    object QrEstqR1Pecas: TFloatField
      FieldName = 'Pecas'
      Origin = '_teste_.Pecas'
    end
    object QrEstqR1PesoKg: TFloatField
      FieldName = 'PesoKg'
      Origin = '_teste_.PesoKg'
    end
    object QrEstqR1AreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = '_teste_.AreaM2'
    end
    object QrEstqR1AreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = '_teste_.AreaP2'
    end
    object QrEstqR1ValorT: TFloatField
      FieldName = 'ValorT'
      Origin = '_teste_.ValorT'
    end
    object QrEstqR1SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Origin = '_teste_.SdoVrtPeca'
    end
    object QrEstqR1SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Origin = '_teste_.SdoVrtPeso'
    end
    object QrEstqR1SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Origin = '_teste_.SdoVrtArM2'
    end
    object QrEstqR1PalVrtPeca: TFloatField
      FieldName = 'PalVrtPeca'
      Origin = '_teste_.PalVrtPeca'
    end
    object QrEstqR1PalVrtPeso: TFloatField
      FieldName = 'PalVrtPeso'
      Origin = '_teste_.PalVrtPeso'
    end
    object QrEstqR1PalVrtArM2: TFloatField
      FieldName = 'PalVrtArM2'
      Origin = '_teste_.PalVrtArM2'
    end
    object QrEstqR1LmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
      Origin = '_teste_.LmbVrtPeca'
    end
    object QrEstqR1LmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
      Origin = '_teste_.LmbVrtPeso'
    end
    object QrEstqR1LmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
      Origin = '_teste_.LmbVrtArM2'
    end
    object QrEstqR1GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_teste_.GraGru1'
    end
    object QrEstqR1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Origin = '_teste_.NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR1Pallet: TIntegerField
      FieldName = 'Pallet'
      Origin = '_teste_.Pallet'
    end
    object QrEstqR1NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = '_teste_.NO_PALLET'
      Size = 60
    end
    object QrEstqR1Terceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = '_teste_.Terceiro'
    end
    object QrEstqR1CliStat: TIntegerField
      FieldName = 'CliStat'
      Origin = '_teste_.CliStat'
    end
    object QrEstqR1Status: TIntegerField
      FieldName = 'Status'
      Origin = '_teste_.Status'
    end
    object QrEstqR1NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Origin = '_teste_.NO_FORNECE'
      Size = 100
    end
    object QrEstqR1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Origin = '_teste_.NO_CLISTAT'
      Size = 100
    end
    object QrEstqR1NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Origin = '_teste_.NO_EMPRESA'
      Size = 100
    end
    object QrEstqR1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Origin = '_teste_.NO_STATUS'
    end
    object QrEstqR1DataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = '_teste_.DataHora'
    end
    object QrEstqR1OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
      Origin = '_teste_.OrdGGX'
    end
    object QrEstqR1OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
      Origin = '_teste_.OrdGGY'
    end
    object QrEstqR1GraGruY: TIntegerField
      FieldName = 'GraGruY'
      Origin = '_teste_.GraGruY'
    end
    object QrEstqR1NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Origin = '_teste_.NO_GGY'
      Size = 255
    end
    object QrEstqR1NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Origin = '_teste_.NO_PalStat'
      Size = 11
    end
    object QrEstqR1Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_teste_.Ativo'
    end
    object QrEstqR1Media: TFloatField
      FieldName = 'Media'
      Origin = '_teste_.Media'
    end
    object QrEstqR1NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Origin = '_teste_.NO_MovimNiv'
      Size = 40
    end
    object QrEstqR1NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Origin = '_teste_.NO_MovimID'
      Size = 30
    end
    object QrEstqR1MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Origin = '_teste_.MovimNiv'
    end
    object QrEstqR1MovimID: TIntegerField
      FieldName = 'MovimID'
      Origin = '_teste_.MovimID'
    end
    object QrEstqR1IMEC: TIntegerField
      FieldName = 'IMEC'
      Origin = '_teste_.IMEC'
    end
    object QrEstqR1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = '_teste_.Codigo'
    end
    object QrEstqR1IMEI: TIntegerField
      FieldName = 'IMEI'
      Origin = '_teste_.IMEI'
    end
    object QrEstqR1SerieFch: TIntegerField
      FieldName = 'SerieFch'
      Origin = '_teste_.SerieFch'
    end
    object QrEstqR1NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = '_teste_.NO_SerieFch'
      Size = 60
    end
    object QrEstqR1Ficha: TIntegerField
      FieldName = 'Ficha'
      Origin = '_teste_.Ficha'
    end
    object QrEstqR1Inteiros: TFloatField
      FieldName = 'Inteiros'
      Origin = '_teste_.Inteiros'
    end
    object QrEstqR1PalStat: TIntegerField
      FieldName = 'PalStat'
      Origin = '_teste_.PalStat'
    end
    object QrEstqR1CUS_UNIT_M2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUS_UNIT_M2'
      Calculated = True
    end
    object QrEstqR1CUS_UNIT_KG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUS_UNIT_KG'
      Calculated = True
    end
    object QrEstqR1ReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
      Origin = '_teste_.ReqMovEstq'
    end
    object QrEstqR1StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_teste_.StqCenCad'
    end
    object QrEstqR1NO_StqCenCad: TWideStringField
      FieldName = 'NO_StqCenCad'
      Origin = '_teste_.NO_StqCenCad'
      Size = 50
    end
    object QrEstqR1StqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Origin = '_teste_.StqCenLoc'
    end
    object QrEstqR1NO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Origin = '_teste_.NO_LOC_CEN'
      Size = 120
    end
  end
  object frxDsEstqR1: TfrxDBDataset
    UserName = 'frxDsEstqR1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'PalVrtPeca=PalVrtPeca'
      'PalVrtPeso=PalVrtPeso'
      'PalVrtArM2=PalVrtArM2'
      'LmbVrtPeca=LmbVrtPeca'
      'LmbVrtPeso=LmbVrtPeso'
      'LmbVrtArM2=LmbVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'Media=Media'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'Ficha=Ficha'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'CUS_UNIT_M2=CUS_UNIT_M2'
      'CUS_UNIT_KG=CUS_UNIT_KG'
      'ReqMovEstq=ReqMovEstq'
      'StqCenCad=StqCenCad'
      'NO_StqCenCad=NO_StqCenCad'
      'StqCenLoc=StqCenLoc'
      'NO_LOC_CEN=NO_LOC_CEN')
    DataSet = QrEstqR1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 28
    Top = 576
  end
  object DsEstqR1: TDataSource
    DataSet = QrEstqR1
    Left = 28
    Top = 620
  end
  object Qr18Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 644
    Top = 144
    object Qr18FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr18FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds18Fornecedor: TDataSource
    DataSet = Qr18Fornecedor
    Left = 644
    Top = 188
  end
  object QrNFes: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFesAfterOpen
    BeforeClose = QrNFesBeforeClose
    SQL.Strings = (
      'DROP TABLE IF EXISTS _nfes_;'
      'CREATE TABLE _nfes_'
      ''
      'SELECT cab.NFeStatus, cab.NFV NFe, cab.MovimCod, 2.000 MovimID, '
      'cab.DtVenda Data, cab.Cliente Terceiro, -cab.Pecas Pecas, '
      '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT'
      'FROM bluederm_meza.vsoutcab cab'
      'WHERE cab.NFV > 0'
      ''
      'UNION '
      ''
      'SELECT nfe.NFeStatus, nfe.ide_nNF NFe, cab.MovimCod, '
      '2.000 MovimID, cab.DtVenda Data, '
      'cab.Cliente Terceiro, -cab.Pecas Pecas, '
      '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT'
      'FROM bluederm_meza.vsoutnfecab nfe '
      'LEFT JOIN bluederm_meza.vsoutcab cab ON cab.Codigo=nfe.OriCod'
      'WHERE nfe.ide_nNF <> 0 '
      'AND nfe.ide_nNF <> cab.NFV'
      'AND cab.MovimCod=nfe.MovimCod'
      ''
      'UNION'
      ''
      'SELECT nfe.NFeStatus, nfe.ide_nNF NFe, cab.MovimCod, '
      '25.000 MovimID, cab.DtVenda Data, '
      'cab.Cliente Terceiro, cab.Pecas Pecas, '
      'cab.PesoKg PesoKg, cab.AreaM2 AreaM2, ValorT ValorT'
      'FROM bluederm_meza.vsoutnfecab nfe '
      'LEFT JOIN bluederm_meza.vstrfloccab cab ON cab.Codigo=nfe.OriCod'
      'WHERE nfe.ide_nNF <> 0 '
      'AND cab.MovimCod=nfe.MovimCod'
      ''
      'UNION '
      ''
      'SELECT nfe.NFeStatus, nfe.ide_nNF NFe, cab.MovimCod, '
      '6.000 MovimID, cab.DtHrAberto Data, '
      'vmi.FornecMO Terceiro, Pecas, PesoKg, AreaM2, ValorT'
      'FROM bluederm_meza.vsoutnfecab nfe '
      'LEFT JOIN bluederm_meza.vsgerarta cab ON cab.Codigo=nfe.OriCod'
      'LEFT JOIN bluederm_meza.vsmovits vmi '
      '  ON vmi.MovimCod=cab.MovimCod '
      'WHERE vmi.MovimNiv=13 AND vmi.MovimID=6'
      'AND cab.MovimCod=nfe.MovimCod'
      ''
      'UNION '
      ''
      
        'SELECT cab.NFeStatus, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID,' +
        ' '
      'cab.DtHrAberto Data, vmi.FornecMO Terceiro, '
      '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, '
      '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT'
      'FROM bluederm_meza.vsopecab cab'
      'LEFT JOIN bluederm_meza.vsmovits vmi '
      '  ON vmi.MovimCod=cab.MovimCod '
      'WHERE vmi.MovimNiv=8 AND vmi.MovimID=11'
      'AND cab.NFeRem >0'
      ' '
      'UNION '
      ' '
      
        'SELECT cab.NFeStatus, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID,' +
        ' '
      'cab.DtHrAberto Data, vmi.FornecMO Terceiro, '
      '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, '
      '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT '
      'FROM bluederm_meza.vspwecab cab'
      'LEFT JOIN bluederm_meza.vsmovits vmi '
      '  ON vmi.MovimCod=cab.MovimCod '
      'WHERE vmi.MovimNiv=21 AND vmi.MovimID=19'
      'AND cab.NFeRem >0 '
      ''
      'UNION '
      ' '
      'SELECT NFeStatus, ide_nNF NFe, MovimCod, 1.000 MovimID, '
      'DtCompra Data, Fornecedor Terceiro, '
      'Pecas, PesoKg, AreaM2, ValorT'
      'FROM bluederm_meza.vsinncab '
      'WHERE ide_nNF > 0'
      ''
      ' '
      'UNION '
      ' '
      'SELECT NFeStatus, ide_nNF NFe, MovimCod, 16.000 MovimID, '
      'DtCompra Data, Fornecedor Terceiro, '
      'Pecas, PesoKg, AreaM2, ValorT'
      'FROM bluederm_meza.vsplccab '
      'WHERE ide_nNF > 0'
      ' '
      'UNION '
      ' '
      'SELECT NFeStatus, ide_nNF NFe, MovimCod, 21.000 MovimID, '
      'DtCompra Data, Fornecedor Terceiro, '
      'Pecas, PesoKg, AreaM2, ValorT'
      'FROM bluederm_meza.vsdvlcab '
      'WHERE ide_nNF > 0'
      ' '
      'UNION '
      ' '
      'SELECT NFeStatus, ide_nNF NFe, MovimCod, 22.000 MovimID, '
      'DtCompra Data, Fornecedor Terceiro, '
      'Pecas, PesoKg, AreaM2, ValorT'
      'FROM bluederm_meza.vsrtbcab '
      'WHERE ide_nNF > 0'
      ''
      ''
      'ORDER BY NFe DESC '
      ';'
      ''
      'SELECT nfe.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM _nfes_ nfe'
      'LEFT JOIN bluederm_meza.entidades ent ON ent.Codigo=nfe.Terceiro'
      ''
      'ORDER BY NFe DESC, Data DESC'
      '')
    Left = 412
    Top = 664
    object QrNFescStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFesNO_cStat: TWideStringField
      FieldName = 'NO_cStat'
      Size = 60
    end
    object QrNFesSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrNFesNFe: TIntegerField
      FieldName = 'NFe'
    end
    object QrNFesMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrNFesMovimID: TFloatField
      FieldName = 'MovimID'
    end
    object QrNFesData: TDateTimeField
      FieldName = 'Data'
    end
    object QrNFesTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrNFesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNFesPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrNFesAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFesValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFesNO_ENT: TWideStringField
      DisplayLabel = 'Nome terceiro'
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrNFesNO_MovimID: TWideStringField
      DisplayLabel = 'ID Movimento'
      FieldName = 'NO_MovimID'
      Size = 60
    end
    object QrNFesTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 50
    end
    object QrNFesCampo: TWideStringField
      FieldName = 'Campo'
    end
    object QrNFesNO_VmcWarn: TWideStringField
      FieldName = 'NO_VmcWarn'
      Size = 60
    end
    object QrNFesVSVmcWrn: TIntegerField
      FieldName = 'VSVmcWrn'
    end
    object QrNFesVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrNFesTxtSaldo: TWideStringField
      FieldName = 'TxtSaldo'
      Size = 255
    end
    object QrNFesVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrNFesVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
    end
  end
  object DsNFes: TDataSource
    DataSet = QrNFes
    Left = 412
    Top = 712
  end
  object PM20NFes: TPopupMenu
    Left = 508
    Top = 296
    object Janeladomovimento1: TMenuItem
      Caption = 'Janela do movimento'
      OnClick = Janeladomovimento1Click
    end
    object AlteraStatusNFe1: TMenuItem
      Caption = 'Altera Status NFe'
      OnClick = AlteraStatusNFe1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object NFeavulsa1: TMenuItem
      Caption = 'NFe Outros'
      OnClick = NFeavulsa1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Informaesdomovimento1: TMenuItem
      Caption = 'Informa'#231#245'es do movimento (Status)'
      OnClick = Informaesdomovimento1Click
    end
  end
  object Qr21Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 928
    Top = 156
    object Qr21FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr21FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds21Fornecedor: TDataSource
    DataSet = Qr21Fornecedor
    Left = 928
    Top = 200
  end
  object Qr21CouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 760
    Top = 224
    object Qr21CouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr21CouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds21CouNiv2: TDataSource
    DataSet = Qr21CouNiv2
    Left = 760
    Top = 268
  end
  object PM10Imprime: TPopupMenu
    Left = 392
    Top = 676
    object PesquisaFichaRMP1: TMenuItem
      Caption = '&Pesquisa Ficha RMP'
      OnClick = PesquisaFichaRMP1Click
    end
    object RelatriodeResultados1: TMenuItem
      Caption = '&Relat'#243'rio de Resultados'
      OnClick = RelatriodeResultados1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 332
    Top = 316
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 360
    Top = 316
  end
  object Qr23VSMORet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, NFCMO_Pecas, NFCMO_PesoKg, '
      'NFCMO_AreaM2, VSVMI_Controle, VSVMI_Codigo, '
      'VSVMI_MovimID, VSVMI_MovimNiv, VSVMI_MovimCod, '
      'VSVMI_SerNF, VSVMI_nNF  '
      'FROM vsmoenvret '
      'WHERE NFCMO_FatID=-1 '
      'OR NFCMO_FatNum=-1 '
      'OR NFCMO_nNF=-1 '
      'OR NFRMP_FatID=-1 '
      'OR NFRMP_FatNum=-1 '
      'OR NFRMP_nNF=-1 ')
    Left = 48
    Top = 192
    object Qr23VSMORetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr23VSMORetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
    end
    object Qr23VSMORetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
    end
    object Qr23VSMORetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
    end
    object Qr23VSMORetVSVMI_Controle: TIntegerField
      FieldName = 'VSVMI_Controle'
    end
    object Qr23VSMORetVSVMI_Codigo: TIntegerField
      FieldName = 'VSVMI_Codigo'
    end
    object Qr23VSMORetVSVMI_MovimID: TIntegerField
      FieldName = 'VSVMI_MovimID'
    end
    object Qr23VSMORetVSVMI_MovimNiv: TIntegerField
      FieldName = 'VSVMI_MovimNiv'
    end
    object Qr23VSMORetVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object Qr23VSMORetVSVMI_SerNF: TIntegerField
      FieldName = 'VSVMI_SerNF'
    end
    object Qr23VSMORetVSVMI_nNF: TIntegerField
      FieldName = 'VSVMI_nNF'
    end
  end
  object Ds23VSMORet: TDataSource
    DataSet = Qr23VSMORet
    Left = 48
    Top = 240
  end
  object frxWET_CURTI_018_20: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_018_20GetValue
    Left = 480
    Top = 664
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFes
        DataSetName = 'frxDsNFes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913410240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 37.795300000000000000
          Width = 56.692862130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status Info')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 37.795300000000000000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000100000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 37.795300000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 37.795300000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo (Txt)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 37.795300000000000000
          Width = 22.677143390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 37.795300000000000000
          Width = 83.149572130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Indexa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 37.795300000000000000
          Width = 71.810982130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Top = 37.795300000000000000
          Width = 287.244192130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico Info:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 132.283550000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFes
        DataSetName = 'frxDsNFes'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 56.692862130000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."NO_VmcWarn"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          DisplayFormat.FormatStr = '#,###,###,###.##;-#,###,###,###.##; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFes."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFes."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NFe'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFes."NFe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000100000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFes."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 287.244192130000000000
          Height = 15.118110240000000000
          DataField = 'VSVmcObs'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."VSVmcObs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          DataField = 'NO_ENT'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'TxtSaldo'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFes."TxtSaldo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Width = 22.677143390000000000
          Height = 15.118110240000000000
          DataField = 'cStat'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."cStat"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 83.149572130000000000
          Height = 15.118110240000000000
          DataField = 'VSVmcSeq'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."VSVmcSeq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Width = 71.810982130000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsNFes
          DataSetName = 'frxDsNFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFes."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 207.874150000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsNFes: TfrxDBDataset
    UserName = 'frxDsNFes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'cStat=cStat'
      'NO_cStat=NO_cStat'
      'Serie=Serie'
      'NFe=NFe'
      'MovimCod=MovimCod'
      'MovimID=MovimID'
      'Data=Data'
      'Terceiro=Terceiro'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'ValorT=ValorT'
      'NO_ENT=NO_ENT'
      'NO_MovimID=NO_MovimID'
      'Tabela=Tabela'
      'Campo=Campo'
      'NO_VmcWarn=NO_VmcWarn'
      'VSVmcWrn=VSVmcWrn'
      'VSVmcObs=VSVmcObs'
      'TxtSaldo=TxtSaldo'
      'VSVmcSeq=VSVmcSeq')
    DataSet = QrNFes
    BCDToCurrency = False
    DataSetOptions = []
    Left = 480
    Top = 712
  end
  object Qr04StqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 416
    Top = 196
    object Qr04StqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr04StqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object Ds04StqCenCad: TDataSource
    DataSet = Qr04StqCenCad
    Left = 416
    Top = 244
  end
  object Qr06VSMrtCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmrtcad'
      'ORDER BY Nome')
    Left = 760
    Top = 12
    object Qr06VSMrtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr06VSMrtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds06VSMrtCad: TDataSource
    DataSet = Qr06VSMrtCad
    Left = 760
    Top = 56
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 592
    Top = 4
    object QrClientMONOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrClientMOCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 592
    Top = 52
  end
end
