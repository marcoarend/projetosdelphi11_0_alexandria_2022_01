object FmVSGerArtFil: TFmVSGerArtFil
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-009 :: Item de Gera'#231#227'o de Artigo de Ribeira'
  ClientHeight = 625
  ClientWidth = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 476
    Width = 860
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 695
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 860
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 695
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 335
    Width = 860
    Height = 141
    Align = alBottom
    Caption = ' Dados do item: '
    TabOrder = 1
    ExplicitWidth = 695
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object LaPecas: TLabel
      Left = 132
      Top = 56
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object LaPeso: TLabel
      Left = 208
      Top = 56
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object Label10: TLabel
      Left = 284
      Top = 56
      Width = 50
      Height = 13
      Caption = 'Valor total:'
    end
    object Label9: TLabel
      Left = 12
      Top = 96
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdPecas: TdmkEdit
      Left = 132
      Top = 72
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPesoKg: TdmkEdit
      Left = 208
      Top = 72
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValorT: TdmkEdit
      Left = 284
      Top = 72
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGMisturou: TdmkRadioGroup
      Left = 364
      Top = 56
      Width = 241
      Height = 49
      Caption = ' Misturou com outro lote no processamneto: '
      Columns = 2
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 4
      QryCampo = 'Misturou'
      UpdCampo = 'Misturou'
      UpdType = utYes
      OldValor = 0
    end
    object EdObserv: TdmkEdit
      Left = 12
      Top = 112
      Width = 593
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 860
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 695
    object GB_R: TGroupBox
      Left = 812
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 647
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 764
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 599
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 450
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 450
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 450
        Height = 32
        Caption = 'Item de Gera'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 511
    Width = 860
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitWidth = 695
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 856
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 691
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 555
    Width = 860
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitWidth = 695
    object PnSaiDesis: TPanel
      Left = 714
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 549
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 712
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 547
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 112
    Width = 860
    Height = 189
    Align = alTop
    Caption = ' Filtros: '
    TabOrder = 6
    ExplicitWidth = 695
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 856
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
      end
      object Label2: TLabel
        Left = 296
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label8: TLabel
        Left = 616
        Top = 0
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 229
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTerceiro: TdmkEditCB
        Left = 296
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 352
        Top = 16
        Width = 261
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFicha: TdmkEdit
        Left = 616
        Top = 16
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFichaChange
      end
      object BitBtn1: TBitBtn
        Tag = 20
        Left = 692
        Top = 2
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BitBtn1Click
      end
    end
    object DBGrid1: TDBGrid
      Left = 2
      Top = 57
      Width = 856
      Height = 120
      Align = alTop
      DataSource = DsAptos
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Title.Caption = 'Ficha RMP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Pele In Natura'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Sdo. Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Sdo kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Codi. entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Ctrl. entrada'
          Visible = True
        end>
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 108
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vsmovits wmi '
      'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE wmi.MovimID=1'
      'AND SdoVrtPeca>0 '
      'AND Empresa=-11'
      'ORDER BY wmi.Controle ')
    Left = 172
    Top = 296
    object QrAptosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAptosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAptosMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrAptosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrAptosMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrAptosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAptosTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrAptosCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrAptosMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrAptosLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrAptosLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrAptosDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrAptosPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrAptosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrAptosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAptosPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrAptosAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrAptosAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrAptosValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrAptosSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrAptosSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrAptosSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrAptosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrAptosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrAptosObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrAptosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAptosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAptosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAptosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAptosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAptosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAptosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAptosFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrAptosMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrAptosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrAptosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrAptosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrAptosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 172
    Top = 344
  end
end
